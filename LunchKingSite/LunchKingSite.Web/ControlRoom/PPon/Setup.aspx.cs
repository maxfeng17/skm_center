﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.BizLogic.Models.SellerModels;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using JsonSerializer = LunchKingSite.Core.JsonSerializer;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using NewtonsoftJson = Newtonsoft.Json;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.BizLogic.Models.PchomeChannel;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class Setup : RolePage, IPponSetupView
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private ISerializer json = ProviderFactory.Instance().GetSerializer();

        private const string _SMS_HEADER = "17Life:";

        #region IPponSetupView Members

        #region Events

        public event EventHandler<DataEventArgs<PponSetupViewModel>> SaveDeal;

        public event EventHandler<ComboDealEventArgs> CopyDeal;

        public event EventHandler CreateOrder;

        public event EventHandler GenerateCoupon;

        public event EventHandler DeleteDeal;

        public event EventHandler CreateDealTimeSlot;

        public event EventHandler SetBBH;

        public event EventHandler<DataEventArgs<Guid>> ManualDealClose;

        public event EventHandler<DataEventArgs<Guid>> SyncPChome1;

        public event EventHandler<DataEventArgs<Guid>> SyncPChome2;

        public event EventHandler<DataEventArgs<KeyValuePair<string, int>>> ImportBusinessOrderInfo;

        public event EventHandler ProposalImport;

        public event EventHandler RreceiveType_TextChanged;

        public event EventHandler<DataEventArgs<string>> CheckFlag;

        public event EventHandler DeleteCDNImages;

        public event EventHandler SetShoppingCart;

        #endregion Events

        #region Props
        protected bool ComboSetupEnabled
        {
            get
            {
                if (Request["bid"] == null)
                {
                    return false;
                }
                return true;
            }
        }

        private PponSetupPresenter _presenter;

        public PponSetupPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public Guid BusinessHourId
        {
            get
            {
                if (string.IsNullOrEmpty(Request.QueryString["bid"]))
                {
                    return Guid.Empty;
                }

                try
                {
                    return new Guid(Request.QueryString["bid"]);
                }
                catch
                {
                    return Guid.Empty;
                }
            }
        }

        public Guid SellerId
        {
            get
            {
                if (string.IsNullOrEmpty(Request.QueryString["sid"]))
                {
                    return Guid.Empty;
                }

                try
                {
                    return new Guid(Request.QueryString["sid"]);
                }
                catch
                {
                    return Guid.Empty;
                }
            }
        }

        public string purchasePrice
        {
            get { return txtPurchasePrice.Text; }
        }

        public string slottingFeeQuantity
        {
            get { return txtSlottingFeeQuantity.Text; }
        }

        public bool isSub
        {
            get;
            set;
        }

        public bool IsMergeCount
        {
            get
            {
                var result = isCombineSetting.Controls.OfType<RadioButton>().SingleOrDefault(x => x.GroupName == "ItemsStatistics" && x.Checked);

                switch (result.ID)
                {
                    case "isCombine":
                        return true;
                    case "isSingle":
                        return false;
                    default:
                        return true;
                }
            }
            set
            {
                foreach (var item in isCombineSetting.Controls.OfType<RadioButton>().Where(x => x.GroupName == "ItemsStatistics"))
                {
                    item.Checked = false;
                }

                switch (value)
                {
                    case true:
                        isCombine.Checked = true;
                        break;
                    case false:
                        isSingle.Checked = true;
                        break;
                    default:
                        isCombine.Checked = true;
                        break;
                }
            }
        }

        //全家商品類型
        public int FamilyDealType
        {
            get
            {
                if (rdoFamilyGeneralType.Checked)
                {
                    return (int)LunchKingSite.Core.FamilyDealType.General;
                }
                else
                {
                    return (int)LunchKingSite.Core.FamilyDealType.BeaconEvent;
                }
            }
            set
            {
                if (value == (int)LunchKingSite.Core.FamilyDealType.BeaconEvent)
                {
                    rdoFamilyBeaconType.Checked = true;

                }
                else
                {
                    rdoFamilyGeneralType.Checked = true;
                }
            }
        }
        //全家商品鎖定
        public bool FamilyIsLock
        {
            get
            {
                return (rdoLock.Checked);
            }
            set
            {

                if (value)
                {
                    rdoLock.Checked = true;
                }
                else
                {
                    rdoUnLock.Checked = true;
                }


            }
        }

        public bool IsBankDeal
        {
            get { return cbx_BankDeal.Checked; }
            set { cbx_BankDeal.Checked = value; }
        }
        public bool IsPromotionDeal
        {
            get
            {
                return cbx_PromotionDeal.Checked;
            }
            set
            {
                cbx_PromotionDeal.Checked = value;
            }
        }

        public bool IsExhibitionDeal
        {
            get
            {
                return cbx_ExhibitionDeal.Checked;
            }
            set
            {
                cbx_ExhibitionDeal.Checked = value;
            }
        }

        public bool IsCChannel
        {
            get
            {
                return cbx_CChannel.Checked;
            }
            set
            {
                cbx_CChannel.Checked = value;
            }
        }

        public string CChannelLink
        {
            get
            {
                return txtCChannelLink.Text.Trim();
            }
            set
            {
                txtCChannelLink.Text = value;
            }
        }

        public bool IsGame
        {
            get
            {
                return cbx_Game.Checked;
            }
            set
            {
                cbx_Game.Checked = value;
            }
        }

        public bool IsTaishinChosen
        {
            get
            {
                return cbx_TaiShinChosen.Checked;
            }
            set
            {
                cbx_TaiShinChosen.Checked = value;
            }
        }

        public bool IsChannelGift
        {
            get
            {
                return cbx_ChannelGift.Checked;
            }
            set
            {
                cbx_ChannelGift.Checked = value;
            }
        }

        public bool IsWms
        {
            get
            {
                return cbx_Pc24H.Checked;
            }
            set
            {
                cbx_Pc24H.Checked = value;
            }
        }

        public bool AllowGuestBuy
        {
            get { return cbx_allowGuestBuy.Checked; }
            set { cbx_allowGuestBuy.Checked = value; }
        }

        public bool SkmDeal
        {
            get
            {
                return cbx_SKMDeal.Checked;
            }
            set
            {
                cbx_SKMDeal.Checked = value;
            }
        }

        public bool SkmExperience
        {
            get
            {
                if (cbx_SKMDeal.Checked)
                {
                    return rbExperience.Checked;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                rbExperience.Checked = value;
                rbCoupon.Checked = !value;
            }
        }

        public int SkmDiscountType
        {
            get
            {
                int type = 0;
                int.TryParse(rblDiscountType.SelectedValue, out type);
                return cbx_setting_display.Checked ? type : 0;
            }
            set
            {
                if (value > 0)
                {
                    cbx_setting_display.Checked = true;
                    rblDiscountType.SelectedValue = value.ToString();
                }
            }
        }

        public int SkmDiscount
        {
            get
            {
                int discount = 0;
                int.TryParse(hdDiscount.Value, out discount);
                return cbx_setting_display.Checked ? discount : 0;
            }
            set
            {
                hdDiscount.Value = value > 0 ? value.ToString() : string.Empty;
            }
        }

        public string SkmTags
        {
            set
            {
                hdSkmDealType.Value = value;
            }
        }

        public bool IsCloseMenu
        {
            get
            {
                return chkCloseMenu.Checked;
            }
            set
            {
                chkCloseMenu.Checked = value;
            }
        }
        public bool IsMenuLogo
        {
            get
            {
                return chkMenuLogo.Checked;
            }
            set
            {
                chkMenuLogo.Checked = value;
            }
        }

        //子檔同步項目
        public bool IsSaveComboDeals
        {
            get
            {
                return (cbx_SaveComboDeals_m.Checked);
            }
        }
        public bool IsSaveComboDeals_Sales
        {
            get
            {
                return (cbx_SaveComboDeals_Sales.Checked);
            }
        }
        public bool IsSaveComboDeals_Channel
        {
            get
            {
                return (cbx_SaveComboDeals_Channel.Checked);
            }
        }
        public bool IsSaveComboDeals_SEO
        {
            get
            {
                return (cbx_SaveComboDeals_SEO.Checked);
            }
        }
        public bool IsSaveComboDeals_NotDeliveryIslands
        {
            get
            {
                return (cbx_SaveComboDeals_NotDeliveryIslands.Checked);
            }
        }
        public bool IsSaveComboDeals_AccBusGroup
        {
            get
            {
                return (cbx_SaveComboDeals_AccBusGroup.Checked);
            }
        }
        public bool IsSaveComboDeals_SalesBelong
        {
            get
            {
                return (cbx_SaveComboDeals_SalesBelong.Checked);
            }
        }
        public bool IsSaveComboDeals_DealType
        {
            get
            {
                return (cbx_SaveComboDeals_DealType.Checked);
            }
        }
        public bool IsSaveComboDeals_EntrustSell
        {
            get
            {
                return (cbx_SaveComboDeals_EntrustOnSell.Checked);
            }
        }

        public bool IsSaveComboDeals_IsLongContract
        {
            get { return (cbx_SaveComboDeals_IsLongContract.Checked); }
        }

        public bool IsSaveComboDeals_PaySet
        {
            get
            {
                return (cbx_SaveComboDeals_PaySet.Checked);
            }
        }
        public bool IsSaveComboDeals_OS
        {
            get
            {
                return (cbx_SaveComboDeals_OS.Checked);
            }
        }
        public bool IsSaveComboDeals_OE
        {
            get
            {
                return (cbx_SaveComboDeals_OE.Checked);
            }
        }
        public bool IsSaveComboDeals_UseDiscount
        {
            get
            {
                return (cbx_SaveComboDeals_UseDiscount.Checked);
            }
        }
        public bool IsSaveComboDeals_DS
        {
            get
            {
                return (cbx_SaveComboDeals_DS.Checked);
            }
        }
        public bool IsSaveComboDeals_DE
        {
            get
            {
                return (cbx_SaveComboDeals_DE.Checked);
            }
        }
        public bool IsSaveComboDeals_ChangedExpireDate
        {
            get
            {
                return (cbx_SaveComboDeals_ChangedExpireDate.Checked);
            }
        }
        public bool IsSaveComboDeals_NoRefund
        {
            get
            {
                return (cbx_SaveComboDeals_NoRefund.Checked);
            }
        }
        public bool IsSaveComboDeals_DaysNoRefund
        {
            get
            {
                return (cbx_SaveComboDeals_DaysNoRefund.Checked);
            }
        }
        public bool IsSaveComboDeals_NoRefundBeforeDays
        {
            get
            {
                return (cbx_SaveComboDeals_NoRefundBeforeDays.Checked);
            }
        }
        public bool IsSaveComboDeals_ExpireNoRefund
        {
            get
            {
                return (cbx_SaveComboDeals_ExpireNoRefund.Checked);
            }
        }
        public bool IsSaveComboDealsPeZevent
        {
            get
            {
                return cbx_SaveComboDeals_cbx_PEZevent.Checked;
            }
        }
        public bool IsSaveComboDeals_PEZeventCouponDownload
        {
            get
            {
                return (cbx_SaveComboDeals_PEZeventCouponDownload.Checked);
            }
        }
        public bool IsSaveComboDealsDepositCoffee
        {
            get
            {
                return cbx_SaveComboDeals_DepositCoffee.Checked;
            }
        }
        public bool IsSaveComboDealsVerifyActionType
        {
            get
            {
                return cbx_SaveComboDeals_VerifyActionType.Checked;
            }
        }
        public bool IsSaveComboDeals_ReserveLock
        {
            get
            {
                return (cbx_SaveComboDeals_ReserveLock.Checked);
            }
        }

        public bool IsSaveComboDeals_BuyoutCoupon
        {
            get
            {
                return cbx_SaveComboDeals_BuyoutCoupon.Checked;
            }
        }
        public bool IsSaveComboDeals_DonotInstallment
        {
            get
            {
                return cbx_SaveComboDeals_DonotInstallment.Checked;
            }
        }
        public bool IsSaveComboDeals_ShipType
        {
            get
            {
                return cbx_SaveComboDeals_ShipType.Checked;
            }
        }
        public bool IsSaveComboDeals_TaiShinCreditCard
        {
            get
            {
                return cbx_SaveComboDeals_TaiShinCreditCard.Checked;
            }
        }

        public bool IsHiddenFromRecentDeals
        {
            get
            {
                return true;
            }
        }

        public bool IsAppLimitedEdition
        {
            get { return cbx_IsAppLimitedEdition.Checked; }
            set { cbx_IsAppLimitedEdition.Checked = value; }
        }

        public string BusinessHourUniqueId
        {
            set
            {
                lblBusinessHourUniqueId.Text = value;
            }
            get
            {
                return lblBusinessHourUniqueId.Text;
            }
        }

        public string DevelopeSales
        {
            get
            {
                return tbDevelopeSales.Text;
            }
            set
            {
                tbDevelopeSales.Text = value;
            }
        }

        public string OperationSales
        {
            get
            {
                return tbOperationSales.Text;
            }
            set
            {
                tbOperationSales.Text = value;
            }
        }
        private string _MenuContent;
        public string MenuContent
        {
            get
            {
                return _MenuContent;
            }
            set
            {
                _MenuContent = value;
            }
        }
        private string _MuneUpdImage;
        public string MuneUpdImage
        {
            get
            {
                return _MuneUpdImage;
            }
            set
            {
                _MuneUpdImage = value;
            }
        }
        private string _MuneImage;
        public string MuneImage
        {
            get
            {
                return _MuneImage;
            }
            set
            {
                _MuneImage = value;
            }
        }
        public Dictionary<int, int[]> SelectedCommercialCategoryId
        {
            get
            {
                Dictionary<int, int[]> selected = new Dictionary<int, int[]>();
                for (int i = 0; i < rptCommercialCategory.Items.Count; i++)
                {
                    HiddenField hidCommercialCategoryId = (HiddenField)rptCommercialCategory.Items[i].FindControl("hidCommercialCategoryId");
                    CheckBoxList chklCommercialCategory = (CheckBoxList)rptCommercialCategory.Items[i].FindControl("chklCommercialCategory");
                    int cid = int.TryParse(hidCommercialCategoryId.Value, out cid) ? cid : 0;
                    int[] categorys = chklCommercialCategory.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => Convert.ToInt32(item.Value)).ToArray();
                    if (!cid.Equals(0) && categorys.Length > 0)
                    {
                        selected.Add(cid, categorys);
                    }
                }
                return selected;
            }
            set
            {
                if (value.Count > 0)
                {
                    for (int i = 0; i < rptCommercialCategory.Items.Count; i++)
                    {
                        HiddenField hidCommercialCategoryId = (HiddenField)rptCommercialCategory.Items[i].FindControl("hidCommercialCategoryId");
                        CheckBoxList chklCommercialCategory = (CheckBoxList)rptCommercialCategory.Items[i].FindControl("chklCommercialCategory");
                        int cid = int.TryParse(hidCommercialCategoryId.Value, out cid) ? cid : 0;
                        if (value.ContainsKey(cid))
                        {
                            for (int j = 0; j < chklCommercialCategory.Items.Count; j++)
                            {
                                if (value[cid] != null)
                                {
                                    chklCommercialCategory.Items[j].Selected = value[cid].Contains(Convert.ToInt32(chklCommercialCategory.Items[j].Value));
                                }
                            }
                        }
                    }
                }
            }
        }

        public Dictionary<int, List<int>> SelectedChannelCategories
        {
            get
            {
                Dictionary<int, List<int>> selected = new Dictionary<int, List<int>>();
                List<int> selectedList = new JsonSerializer().Deserialize<List<int>>(hiddenChannelChecked.Value);
                List<List<int>> selectedSubList = new JsonSerializer().Deserialize<List<List<int>>>(hiddenSubChannelChecked.Value);
                if (selectedList == null || selectedList.Count == 0 || selectedSubList == null || selectedSubList.Count == 0)
                {
                    return selected;
                }
                for (int i = 0; i < selectedList.Count; i++)
                {
                    selected.Add(selectedList[i], selectedSubList[i]);
                }
                return selected;
            }

            set
            {
                List<int> selectChannelIds = new List<int>();
                List<List<int>> selectSubChannelIds = new List<List<int>>();
                foreach (var data in value)
                {
                    selectChannelIds.Add(data.Key);
                    selectSubChannelIds.Add(data.Value);
                }
                hiddenChannelChecked.Value = new JsonSerializer().Serialize(selectChannelIds);
                hiddenSubChannelChecked.Value = new JsonSerializer().Serialize(selectSubChannelIds);
            }
        }

        public Dictionary<int, List<int>> SelectedChannelSpecialRegionCategories
        {
            get
            {
                Dictionary<int, List<int>> selected = new Dictionary<int, List<int>>();
                List<List<int>> selectedList = new JsonSerializer().Deserialize<List<List<int>>>(hiddenSubChannelChecked.Value);
                List<List<int>> selectedSubList = new JsonSerializer().Deserialize<List<List<int>>>(hiddenSubSpecialRegionChecked.Value);

                if (selectedList == null || selectedList.Count == 0 || selectedSubList == null || selectedSubList.Count == 0)
                {
                    return selected;
                }

                List<int> subchannel = new List<int>();

                foreach (var channelset in selectedList)
                {
                    subchannel.AddRange(channelset.Select(x => x));
                }

                for (int i = 0; i < subchannel.Count; i++)
                {
                    selected.Add(subchannel[i], selectedSubList[i]);
                }


                return selected;
            }

            set
            {
                List<int> selectChannelIds = new List<int>();
                List<List<int>> selectSubChannelIds = new List<List<int>>();
                foreach (var data in value)
                {
                    selectChannelIds.Add(data.Key);
                    selectSubChannelIds.Add(data.Value);
                }
                hiddenSubSpecialRegionChecked.Value = new JsonSerializer().Serialize(selectSubChannelIds);
            }
        }
        public List<int> SelectedChannelSpecialRegionSubCategories
        {
            get
            {
                List<int> selected = new List<int>();
                List<int> selectedSPRSubList = new JsonSerializer().Deserialize<List<int>>(hiddenSubSpecialSubRegionChecked.Value); //景點商圈子區域
                foreach (var subRegion in selectedSPRSubList)
                {
                    if (!selected.Contains(subRegion)) //避免重複
                    {
                        selected.Add(subRegion);
                    }
                }
                return selected;
            }

            set
            {
                List<int> selectSubSPRIds = new List<int>();
                foreach (var data in value)
                {
                    if (!selectSubSPRIds.Contains(data)) //避免重複
                    {
                        selectSubSPRIds.Add(data);
                    }
                }
                hiddenSubSpecialSubRegionChecked.Value = new JsonSerializer().Serialize(selectSubSPRIds);
            }
        }

        public List<int> SelectedDealCategories
        {
            get { return new JsonSerializer().Deserialize<List<int>>(hiddenDealCategory.Value); }
            set { hiddenDealCategory.Value = new JsonSerializer().Serialize(value); }
        }
        /// <summary>
        /// 回傳取貨方式的分類ID
        /// </summary>
        public int SelectedDeliveryCategoryId { get { return int.Parse(hiddenDeliveryCategory.Value); } }

        public List<int> SelectedSpecialCategories
        {
            get { return new JsonSerializer().Deserialize<List<int>>(hiddenDeliveryCategoryChecked.Value); }
            set { hiddenDeliveryCategoryChecked.Value = new JsonSerializer().Serialize(value); }
        }

        public List<int> SelectedDefaultIcon
        {
            set
            {
                if (value.Count > 0)
                {
                    for (int i = 0; i < cbl_DefaultIcon.Items.Count; i++)
                    {
                        cbl_DefaultIcon.Items[i].Selected = value != null && value.Any(x => x == int.Parse(cbl_DefaultIcon.Items[i].Value));
                    }
                }
            }
            get
            {
                var temp = new List<int>();
                if (string.IsNullOrEmpty(cbl_DefaultIcon.SelectedValue))
                {
                    return temp;
                }
                temp = cbl_DefaultIcon.SelectedValue.Split(',').Select(int.Parse).ToList();
                return temp;
            }
        }

        #region Selected DealLabel

        public int[] SelectedCouponDealTag
        {
            get
            {
                List<int> selected = new List<int>();
                for (int i = 0; i < cbl_couponDealTag.Items.Count; i++)
                {
                    if (cbl_couponDealTag.Items[i].Selected)
                    {
                        selected.Add(int.Parse(cbl_couponDealTag.Items[i].Value));
                    }
                }

                return selected.ToArray();
            }

            set
            {
                if (value.Length > 0)
                {
                    for (int i = 0; i < cbl_couponDealTag.Items.Count; i++)
                    {
                        cbl_couponDealTag.Items[i].Selected = value != null && value.Any(x => x == int.Parse(cbl_couponDealTag.Items[i].Value));
                    }
                }
            }
        }

        public int[] SelectedCouponTravelDealTag
        {
            get
            {
                List<int> selected = new List<int>();
                for (int i = 0; i < cbl_couponTravelDealTag.Items.Count; i++)
                {
                    if (cbl_couponTravelDealTag.Items[i].Selected)
                    {
                        selected.Add(int.Parse(cbl_couponTravelDealTag.Items[i].Value));
                    }
                }

                return selected.ToArray();
            }

            set
            {
                if (value.Length > 0)
                {
                    for (int i = 0; i < cbl_couponTravelDealTag.Items.Count; i++)
                    {
                        cbl_couponTravelDealTag.Items[i].Selected = value != null && value.Any(x => x == int.Parse(cbl_couponTravelDealTag.Items[i].Value));
                    }
                }
            }
        }

        public int[] SelectedDeliveryDealTag
        {
            get
            {
                List<int> selected = new List<int>();
                for (int i = 0; i < cbl_deliveryDealTag.Items.Count; i++)
                {
                    if (cbl_deliveryDealTag.Items[i].Selected)
                    {
                        selected.Add(int.Parse(cbl_deliveryDealTag.Items[i].Value));
                    }
                }

                return selected.ToArray();
            }

            set
            {
                if (value.Length > 0)
                {
                    for (int i = 0; i < cbl_deliveryDealTag.Items.Count; i++)
                    {
                        cbl_deliveryDealTag.Items[i].Selected = value != null && value.Any(x => x == int.Parse(cbl_deliveryDealTag.Items[i].Value));
                    }
                }
            }
        }

        public int[] SelectedDeliveryTravelDealTag
        {
            get
            {
                List<int> selected = new List<int>();
                for (int i = 0; i < cbl_deliveryTravelDealTag.Items.Count; i++)
                {
                    if (cbl_deliveryTravelDealTag.Items[i].Selected)
                    {
                        selected.Add(int.Parse(cbl_deliveryTravelDealTag.Items[i].Value));
                    }
                }

                return selected.ToArray();
            }

            set
            {
                if (value.Length > 0)
                {
                    for (int i = 0; i < cbl_deliveryTravelDealTag.Items.Count; i++)
                    {
                        cbl_deliveryTravelDealTag.Items[i].Selected = value != null && value.Any(x => x == int.Parse(cbl_deliveryTravelDealTag.Items[i].Value));
                    }
                }
            }
        }

        public string CustomTag
        {
            set
            {
                txt_CustomTag.Text = value;
                if (!string.IsNullOrEmpty(value))
                {
                    cb_CustomTag.Checked = true;
                }
            }
            get { return txt_CustomTag.Text.Trim(); }
        }

        #endregion

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        public string SMSContent
        {
            get
            {
                string smsContent = string.IsNullOrEmpty(txtSMSContent.Text) ? tbUse.Text : txtSMSContent.Text;
                return string.Format("{0}|{1}|{2}", _SMS_HEADER, smsContent, hidSmsFooter.Value);
            }
        }

        public bool DisableSMS
        {
            get
            {
                return cbx_DisableSMS.Checked;
            }
            set
            {
                cbx_DisableSMS.Checked = value;
            }
        }

        public bool NoRefund
        {
            get
            {
                return cbx_NoRefund.Checked;
            }
            set
            {
                cbx_NoRefund.Checked = value;
            }
        }

        public bool DaysNoRefund
        {
            get
            {
                return chkDaysNoRefund.Checked;
            }
            set
            {
                chkDaysNoRefund.Checked = value;
            }
        }

        public bool ExpireNoRefund
        {
            get
            {
                return cbx_ExpireNoRefund.Checked;
            }
            set
            {
                cbx_ExpireNoRefund.Checked = value;
            }
        }

        public bool NoRefundBeforeDays
        {
            get
            {
                return chkNoRefundBeforeDays.Checked;
            }
            set
            {
                chkNoRefundBeforeDays.Checked = value;
            }
        }


        public string QuantityToShow
        {
            get
            {
                return "";
            }
        }

        public bool IsInputTaxNotRequired
        {
            get
            {
                return cbInputTax.Checked;
            }
            set
            {
                cbInputTax.Checked = value;
            }
        }

        public bool NoTax
        {
            get
            {
                return cbx_NoTax.Checked;
            }
            set
            {
                cbx_NoTax.Checked = value;
            }
        }

        public bool NoInvoice
        {
            get
            {
                return cbx_NoInvoice.Checked;
            }
            set
            {
                cbx_NoInvoice.Checked = value;
            }
        }

        public bool PEZevent
        {
            get
            {
                return cbx_PEZevent.Checked;
            }
            set
            {
                cbx_PEZevent.Checked = value;
            }
        }

        public bool PEZeventCouponDownload
        {
            get
            {
                return cbx_PEZeventCouponDownload.Checked;
            }
            set
            {
                cbx_PEZeventCouponDownload.Checked = value;
            }
        }

        public bool ZeroActivityShowCoupon
        {
            get { return cbx_ZeroActivityShowCoupon.Checked; }
            set { cbx_ZeroActivityShowCoupon.Checked = value; }
        }

        public bool PEZeventWithUserId
        {
            get
            {
                return cbx_PEZeventWithUserId.Checked;
            }
            set
            {
                cbx_PEZeventWithUserId.Checked = value;
            }
        }

        public bool IsDepositCoffee
        {
            get
            {
                return cbx_DepositCoffee.Checked;
            }
            set
            {
                cbx_DepositCoffee.Checked = value;
            }
        }

        public bool HiLifeDeal
        {
            get
            {
                return cbx_HiLifeDeal.Checked;
            }
            set
            {
                cbx_HiLifeDeal.Checked = value;
            }
        }

        public bool IsReverseVerify
        {
            get { return cbx_IsReverseVerify.Checked; }
            set { cbx_IsReverseVerify.Checked = value; }
        }

        public bool FamiDeal
        {
            get
            {
                return cbx_FamiDeal.Checked;
            }
            set
            {
                cbx_FamiDeal.Checked = value;
            }
        }

        public int FamiDealType
        {
            get
            {
                if (rdoFamiPincode.Checked)
                {
                    return (int)CouponCodeType.Pincode;
                }
                if(rdoFamiBarcode.Checked)
                {
                    return (int)CouponCodeType.BarcodeEAN13;
                }
                return (int) CouponCodeType.FamiItemCode;
            }
            set
            {
                if (value == (int)CouponCodeType.Pincode)
                {
                    rdoFamiPincode.Checked = true;
                }
                else if (value == (int)CouponCodeType.BarcodeEAN13)
                {
                    rdoFamiBarcode.Checked = true;
                }
                else if (value == (int) CouponCodeType.FamiItemCode)
                {
                    rdoFamiItemCode.Checked = true;
                }
            }
        }

        public decimal ExchangePrice
        {
            get
            {
                decimal exchangePrice;
                if (decimal.TryParse(tbx_FamiExchangePrice.Text, out exchangePrice))
                {
                    return exchangePrice;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                tbx_FamiExchangePrice.Text = value.ToString();
            }
        }

        public bool IsSinglePinCode
        {
            get
            {
                return cbx_SinglePinCode.Checked;
            }
            set
            {
                cbx_SinglePinCode.Checked = value;
            }
        }

        public bool IsKindDeal
        {
            get
            {
                return cbx_KindDeal.Checked;
            }
            set
            {
                cbx_KindDeal.Checked = value;
            }
        }

        public bool WeeklyPay
        {
            get
            {
                return rbAchWeekly.Checked;
            }
        }

        public bool PriceZeorShowOriginalPrice
        {
            get
            {
                return cbx_PriceZeorShowOriginalPrice.Checked;
            }
            set
            {
                cbx_PriceZeorShowOriginalPrice.Checked = value;
            }
        }

        public bool NoDiscountShown
        {
            get
            {
                return cbx_NoDiscountShown.Checked;
            }
            set
            {
                cbx_NoDiscountShown.Checked = value;
            }
        }

        public string IsMulti
        {
            set
            {
                HidIsMulti.Value = value;
            }
        }

        public decimal GrossMarginVal
        {
            set
            {
                hidGrossLimitVal.Value = value.ToString();
            }
        }
        public decimal MinGrossMarginVal
        {
            set
            {
                hidMinGrossMarginVal.Value = value.ToString();
            }
        }

        public decimal GrossMarginCriterion
        {
            get { return config.GrossMargin; }
        }

        public bool IsEnableVbsNewShipFile
        {
            get { return config.IsEnableVbsNewShipFile; }
        }

        public bool LowGrossMarginAllowedDiscount
        {
            get
            {
                return cbx_LowGrossMarginAllowedDiscount.Checked;
            }
            set
            {
                cbx_LowGrossMarginAllowedDiscount.Checked = value;
            }
        }
        public bool NotAllowedDiscount
        {
            get
            {
                return cbx_NotAllowedDiscount.Checked;
            }
            set
            {
                cbx_NotAllowedDiscount.Checked = value;
            }
        }

        public bool BuyoutCoupon
        {
            get
            {
                return cbx_BuyoutCoupon.Checked;
            }
            set
            {
                cbx_BuyoutCoupon.Checked = value;
            }
        }
        public bool DonotInstallment
        {
            get
            {
                return cbx_DonotInstallment.Checked;
            }
            set
            {
                cbx_DonotInstallment.Checked = value;
            }
        }

        public bool TmallDeal
        {
            get
            {
                return cbx_TmallDeal.Checked;
            }
            set
            {
                cbx_TmallDeal.Checked = value;
            }
        }

        public decimal TmallRmbPrice
        {
            get
            {
                decimal rmb_price;
                if (decimal.TryParse(tbx_TmallRmbPrice.Text, out rmb_price))
                {
                    return rmb_price;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                tbx_TmallRmbPrice.Text = value.ToString();
            }
        }

        public decimal TmallRmbExchangeRate
        {
            get
            {
                decimal rmb_exchange_rate;
                if (decimal.TryParse(tbx_TmallRmbExchangeRate.Text, out rmb_exchange_rate))
                {
                    return rmb_exchange_rate;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                tbx_TmallRmbExchangeRate.Text = value.ToString();
            }
        }

        public bool NoShippingFeeMessage
        {
            get
            {
                return cbx_NoShippingFeeMessage.Checked;
            }
            set
            {
                cbx_NoShippingFeeMessage.Checked = value;
            }
        }

        public bool NotDeliveryIslands
        {
            get
            {
                return cbx_NotDeliveryIslands.Checked;
            }
            set
            {
                cbx_NotDeliveryIslands.Checked = value;
            }
        }

        public string btnNotInBBH
        {
            set
            {
                bBBH.Text = value;
            }
        }

        public int AccBusinessGroupNo
        {
            get
            {
                try
                {
                    return int.Parse(ddlAccBusGroup.SelectedValue);
                }
                catch
                {
                    return -1;
                }
            }
            set
            {
                ddlAccBusGroup.SelectedValue = value.ToString();
            }
        }

        public int DealTypeNewValue
        {
            get
            {
                try
                {
                    return int.Parse(hdDealTypeNew.Value);
                }
                catch
                {
                    return -1;
                }
            }
            set { hdDealTypeNew.Value = value.ToString(); }
        }

        public EntrustSellType EntrustSell
        {
            get
            {
                if (rbKindReceipt.Checked)
                {
                    return EntrustSellType.KindReceipt;
                }
                else
                {
                    return EntrustSellType.No;
                }
            }
            set
            {
                switch (value)
                {
                    case EntrustSellType.No:
                        rbNoEntrustSell.Checked = true;
                        break;

                    case EntrustSellType.KindReceipt:
                        rbKindReceipt.Checked = true;
                        break;

                    default:
                        break;
                }
            }
        }

        public TrustProvider TrustType
        {
            get
            {
                if (rbBMMohistSystem.Checked)
                    return TrustProvider.Sunny;
                else
                    return TrustProvider.TaiShin;
            }
            set
            {
                rbBMMohistSystem.Checked = value.Equals(TrustProvider.Sunny);
            }
        }

        public string TravelPlace
        {
            get
            {
                return txtTravelPlace.Text;
            }
            set
            {
                txtTravelPlace.Text = value;
            }
        }

        public int SellerVerifyType
        {
            get
            {
                try
                {
                    return int.Parse(ddlVerifyType.SelectedValue);
                }
                catch
                {
                    return -1;
                }
            }
            set
            {
                ddlVerifyType.SelectedValue = value.ToString();
            }
        }

        public string DeptId
        {
            get
            {
                return ddlDept.SelectedValue;
            }
            set
            {
                ddlDept.SelectedValue = value;
            }
        }

        public int AccCityId
        {
            get
            {
                try
                {
                    return int.Parse(ddlAccCity.SelectedValue);
                }
                catch (Exception)
                {
                    return -1;
                }
            }
            set
            {
                ddlAccCity.SelectedValue = value.ToString();
            }
        }

        //public DealAccountingPayType PayToCompany
        //{
        //    get
        //    {
        //        return (rbPayToStore.Checked) ? DealAccountingPayType.PayToStore : DealAccountingPayType.PayToCompany;
        //    }
        //    set
        //    {
        //        switch (value)
        //        {
        //            case DealAccountingPayType.PayToStore:
        //                rbPayToStore.Checked = true;
        //                break;

        //            case DealAccountingPayType.PayToCompany:
        //                rbPayToCompany.Checked = true;
        //                break;

        //            default:
        //                break;
        //        }
        //    }
        //}

        /// <summary>
        /// 出帳方式控制項的值
        /// </summary>
        public RemittanceType PaidType
        {
            get
            {
                if (rbAchWeekly.Checked)
                {
                    return RemittanceType.AchWeekly;
                }
                if (rbPartially.Checked)
                {
                    return RemittanceType.ManualPartially;
                }
                if (rbManualWeekly.Checked)
                {
                    return RemittanceType.ManualWeekly;
                }
                if (rbManualMonthly.Checked)
                {
                    return RemittanceType.ManualMonthly;
                }
                if (rbFlexible.Checked)
                {
                    return RemittanceType.Flexible;
                }
                if (rbWeekly.Checked)
                {
                    return RemittanceType.Weekly;
                }
                if (rbMonthly.Checked)
                {
                    return RemittanceType.Monthly;
                }
                if (config.IsRemittanceFortnightly)
                {
                    if (rbFortnightly.Checked)
                    {
                        return RemittanceType.Fortnightly;
                    }
                }


                return RemittanceType.Others;
            }
            set
            {
                rbAchWeekly.Checked = rbPartially.Checked = rbManualWeekly.Checked = rbManualMonthly.Checked = rbOthers.Checked
                     = rbFlexible.Checked = rbMonthly.Checked = rbWeekly.Checked = false;
                if (config.IsRemittanceFortnightly)
                {
                    rbFortnightly.Checked = false;
                }

                switch (value)
                {
                    case RemittanceType.AchWeekly:
                        rbAchWeekly.Checked = true;
                        break;

                    case RemittanceType.ManualPartially:
                        rbPartially.Checked = true;
                        break;

                    case RemittanceType.ManualWeekly:
                        rbManualWeekly.Checked = true;
                        break;

                    case RemittanceType.ManualMonthly:
                        rbManualMonthly.Checked = true;
                        break;

                    case RemittanceType.Flexible:
                        rbFlexible.Checked = true;
                        break;

                    case RemittanceType.AchMonthly:
                    case RemittanceType.Monthly:
                        rbMonthly.Checked = true;
                        break;

                    case RemittanceType.Weekly:
                        rbWeekly.Checked = true;
                        break;
                    case RemittanceType.Fortnightly:
                        if (config.IsRemittanceFortnightly)
                        {
                            rbFortnightly.Checked = true;
                        }
                        break;
                    default:
                        rbOthers.Checked = true;
                        break;
                }
            }
        }

        public VendorBillingModel BillingModel
        {
            get
            {
                if (rbBMBalanceSheetSystem.Checked)
                {
                    return VendorBillingModel.BalanceSheetSystem;
                }
                else if (rbBMMohistSystem.Checked)
                {
                    return VendorBillingModel.MohistSystem;
                }
                else
                {
                    return VendorBillingModel.None;
                }
            }
            set
            {
                rbBMNone.Checked = rbBMBalanceSheetSystem.Checked = rbBMMohistSystem.Checked = false;

                switch (value)
                {
                    case VendorBillingModel.BalanceSheetSystem:
                        rbBMBalanceSheetSystem.Checked = true;
                        break;

                    case VendorBillingModel.MohistSystem:
                        rbBMMohistSystem.Checked = true;
                        break;

                    default:
                        rbBMNone.Checked = true;
                        spnBMNone.Visible = true;
                        break;
                }
            }
        }

        public VendorReceiptType ReceiptType
        {
            get
            {
                if (rbRecordInvoice.Checked)
                {
                    return VendorReceiptType.Invoice;
                }
                else if (rbRecordReceipt.Checked)
                {
                    return VendorReceiptType.Receipt;
                }
                else
                {
                    return VendorReceiptType.Other;
                }
            }
            set
            {
                rbRecordInvoice.Checked = rbRecordReceipt.Checked = rbRecordOthers.Checked = false;

                switch (value)
                {
                    case VendorReceiptType.Invoice:
                        rbRecordInvoice.Checked = true;
                        break;

                    case VendorReceiptType.Receipt:
                        rbRecordReceipt.Checked = true;
                        break;

                    default:
                        rbRecordOthers.Checked = true;
                        break;
                }
            }
        }

        public string AccountingMessage
        {
            get
            {
                return tbAccountingMessage.Text;
            }
            set
            {
                tbAccountingMessage.Text = value;
            }
        }

        /// <summary>
        /// 運費收入
        /// </summary>
        public List<PponSetupViewFreight> FreightIncome
        {
            get
            {
                var rtn = json.Deserialize<List<PponSetupViewFreight>>(hdFreightIncome.Value);
                return rtn ?? new List<PponSetupViewFreight>();
            }
            set
            {
                hdFreightIncome.Value = json.Serialize(value);
                hdFIMax.Value = value.Count > 0 ? value[0].StartAmount.ToString() : "0";
            }
        }

        /// <summary>
        /// 運費支出
        /// </summary>
        public List<PponSetupViewFreight> FreightCost
        {
            get
            {
                var rtn = json.Deserialize<List<PponSetupViewFreight>>(hdFreightCost.Value);
                return rtn ?? new List<PponSetupViewFreight>();
            }
            set
            {
                hdFreightCost.Value = json.Serialize(value);
                hdFCMax.Value = value.Count > 0 ? value[0].StartAmount.ToString() : "0";
            }
        }

        /// <summary>
        /// 進貨價
        /// </summary>
        public List<PponSetupViewMultiCost> MultiCost
        {
            get
            {
                var rtn = json.Deserialize<List<PponSetupViewMultiCost>>(hdMultiCost.Value);
                return rtn ?? new List<PponSetupViewMultiCost>();
            }
            set
            {
                hdMultiCost.Value = json.Serialize(value);
                if (value.Count > 0)
                {
                    txtSlottingFeeQuantity.Text = (value.Count > 1) ? value[0].Cumulative_quantity.ToString() : "0";
                    var cost = value[value.Count - 1].Cost;
                    txtPurchasePrice.Text = cost != null ? cost.Value.ToString("0.#") : "0";
                    hdCumulativeQuantity.Value = value[value.Count - 1].Cumulative_quantity.ToString();
                }
                else
                {
                    hdCumulativeQuantity.Value = "0";
                }
            }
        }

        /// <summary>
        /// 調整過的有效截止日
        /// </summary>
        public DateTime? ChangedExpireDate
        {
            get
            {
                DateTime? returnedDt = null;
                DateTime dt;
                if (DateTime.TryParse(formChangedExpireDate.Text, out dt))
                {
                    TimeSpan ts = new TimeSpan(int.Parse(dlEDh.SelectedValue), int.Parse(dlEDm.SelectedValue), 59);
                    dt = dt.Date + ts;
                    returnedDt = dt;
                }
                return returnedDt;
            }
            set
            {
                if (value.HasValue)
                {
                    formChangedExpireDate.Text = value.Value.ToString("yyyy/MM/dd");
                    dlEDh.SelectedValue = value.Value.Hour.ToString();
                    dlEDm.SelectedValue = value.Value.Minute.ToString();
                }
            }
        }

        public DateTime OrderTimeE
        {
            get
            {
                DateTime returnedDt = new DateTime();
                DateTime dt;
                if (DateTime.TryParse(this.tbOE.Text + " " + dlOEh.SelectedValue + ":" + dlOEm.SelectedValue, out dt))
                {
                    returnedDt = dt;
                }
                return returnedDt;
            }
            set
            {
                DateTime dt = value;
                tbOE.Text = dt.Date.ToString("yyyy/MM/dd");
                dlOEh.SelectedValue = dt.Hour.ToString();
                dlOEm.SelectedValue = dt.Minute.ToString();
            }
        }

        public DateTime OrderTimeS
        {
            get
            {
                DateTime returnedDt = new DateTime();
                DateTime dt;
                if (DateTime.TryParse(this.tbOS.Text + " " + dlOSh.SelectedValue + ":" + dlOSm.SelectedValue, out dt))
                {
                    returnedDt = dt;
                }
                return returnedDt;
            }
            set
            {
                DateTime dt = value;
                tbOS.Text = dt.Date.ToString("yyyy/MM/dd");
                dlOSh.SelectedValue = dt.Hour.ToString();
                dlOSm.SelectedValue = dt.Minute.ToString();
            }
        }

        public DateTime? DeliverTimeE
        {
            get
            {
                DateTime returnedDt = new DateTime();
                DateTime dt;
                if (DateTime.TryParse(this.tbDE.Text, out dt))
                {
                    TimeSpan ts = new TimeSpan(int.Parse(dlDEh.SelectedValue), int.Parse(dlDEm.SelectedValue), 59);
                    dt = dt.Date + ts;
                    returnedDt = dt;
                }
                return returnedDt;
            }
            set
            {
                DateTime? dt = value;
                if (dt != null)
                {
                    tbDE.Text = dt.Value.Date.ToString("yyyy/MM/dd");
                    dlDEh.SelectedValue = dt.Value.Hour.ToString();
                    dlDEm.SelectedValue = dt.Value.Minute.ToString();
                }
            }
        }

        public DateTime? DeliverTimeS
        {
            get
            {
                DateTime returnedDt = new DateTime();
                DateTime dt;
                if (DateTime.TryParse(this.tbDS.Text, out dt))
                {
                    TimeSpan ts = new TimeSpan(int.Parse(dlDSh.SelectedValue), int.Parse(dlDSm.SelectedValue), 0);
                    dt = dt.Date + ts;
                    returnedDt = dt;
                }
                return returnedDt;
            }
            set
            {
                DateTime? dt = value;
                if (dt != null)
                {
                    tbDS.Text = dt.Value.Date.ToString("yyyy/MM/dd");
                    dlDSh.SelectedValue = dt.Value.Hour.ToString();
                    dlDSm.SelectedValue = dt.Value.Minute.ToString();
                }
            }
        }

        /// <summary>
        /// 每日清算時間
        /// </summary>
        public DateTime? SettlementTime
        {
            get
            {
                DateTime? returnedDt = null;
                DateTime dt;
                if (cbx_Settlement.Checked)
                {
                    dt = string.IsNullOrEmpty(hdSTMDate.Value) ? DeliverTimeS.Value : DateTime.Parse(hdSTMDate.Value);
                    TimeSpan ts = new TimeSpan(int.Parse(dlSTMh.SelectedValue), int.Parse(dlSTMm.SelectedValue), 0);
                    dt = dt.Date + ts;
                    returnedDt = dt;
                }
                return returnedDt;
            }
            set
            {
                DateTime? dt = value;
                if (dt != null)
                {
                    dlSTMh.SelectedValue = dt.Value.Hour.ToString();
                    dlSTMm.SelectedValue = dt.Value.Minute.ToString();
                    cbx_Settlement.Checked = true;
                    hdSTMDate.Value = value.Value.ToString();
                }
            }
        }

        public bool IsDealOpened
        {
            get
            {
                return DateTime.Now > OrderTimeS;
            }
        }


        public bool IsDealClosed
        {
            get
            {
                bool isClosed;
                if (bool.TryParse(isDealClosed.Value, out isClosed))
                {
                    return isClosed;
                }

                return false;
            }
            set
            {
                isDealClosed.Value = value.ToString();
            }
        }

        public bool IsAllowedToSetCost
        {
            get
            {
                return CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.SetCost);
            }
        }

        public string SalesManNameList = string.Empty;

        public string[] SalesmanNameArray
        {
            set
            {
                var rtn = string.Empty;
                foreach (var s in value)
                {
                    if (rtn == string.Empty)
                    {
                        rtn = string.Format("'{0}'", s);
                    }
                    else
                    {
                        rtn = string.Format("{0},'{1}'", rtn, s);
                    }
                }
                SalesManNameList = rtn;
            }
        }

        public int ProposalId
        {
            get
            {
                int id = int.TryParse(hidProposalId.Value, out id) ? id : 0;
                return id;
            }
        }

        public int ProposalCreateType
        {
            get
            {
                int type = int.TryParse(hidProposalCreateType.Value, out type) ? type : 0;
                return type;
            }
        }

        public int ProposalConsignment
        {
            get
            {
                int consignment = int.TryParse(hidProposalConsignment.Value, out consignment) ? consignment : 0;
                return consignment;
            }
        }


        public int? SellerPaidType
        {
            get
            {
                if (string.IsNullOrEmpty(hidSellerPaidType.Value))
                {
                    return null;
                }
                return int.Parse(hidSellerPaidType.Value);
            }
            set
            {
                hidSellerPaidType.Value = value.ToString();
            }
        }

        public int? SellerRemittanceType
        {
            get
            {
                int? remittanceType = null;
                if (BusinessHourId != Guid.Empty)
                {
                    remittanceType = sp.SellerGetByBid(BusinessHourId).RemittanceType;
                }
                return remittanceType;
            }
        }

        /// <summary>
        /// 是否具有權限設定[低毛利率可使用折價券/本檔不可使用折價券]
        /// </summary>
        public bool HasUseDiscountSetPrivilege
        {
            get
            {
                return CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.UserDiscount);
            }
        }

        public void SetSelectableCommercialCategory(Dictionary<Category, List<Category>> categories)
        {
            rptCommercialCategory.DataSource = categories;
            rptCommercialCategory.DataBind();
        }

        public void SetProposalImport(ViewProposalSeller pro)
        {
            divProposal.Visible = true;
            divProposalFlag.Visible = true;
            hidProposalId.Value = pro.Id.ToString();
            hidProposalCreateType.Value = pro.ProposalCreatedType.ToString();
            hidProposalConsignment.Value = pro.Consignment.ToString();
            hkProposal.Text = "前往提案單" + pro.Id.ToString();
            hkProposal.NavigateUrl = ResolveUrl("~/sal/ProposalContent.aspx?pid=" + pro.Id);
            rptProposalFlag.DataSource = SellerFacade.GetProposalFlags(pro)[ProposalStatus.Editor].ToDictionary(x => x, y => ProposalStatus.Editor);
            rptProposalFlag.DataBind();

        }

        public void GetProposalData(ViewProposalSeller pro)
        {
            //新版宅配提案單不給編輯多重選項
            if (pro.ProposalSourceType == (int)ProposalSourceType.House)
                txtMult.Enabled = false;
        }

        public void SetSendPageConfirmMailButton(ViewProposalSeller pro) {
            btnSendPageConfirmMail.Visible = true;
        }

        /// <summary>
        /// 處理好康區域及其子項目類別的設定畫面
        /// </summary>
        /// <param name="channelTypeNode"></param>
        /// <param name="allViewCategoryDependencies"></param>
        public void SetSelectableChannel(CategoryTypeNode channelTypeNode, List<ViewCategoryDependency> allViewCategoryDependencies)
        {
            JsonSerializer json = new JsonSerializer();
            //將頻道的資料儲存於隱藏的欄位
            List<int> channelList = channelTypeNode.CategoryNodes.Select(x => x.CategoryId).ToList();
            hiddenChannelJSon.Value = json.Serialize(channelList);
            //將頻道所屬的分類之資料儲存於隱藏的欄位
            List<List<int>> channelCategoryList = GetChannelCategoryList(CategoryType.DealCategory, channelTypeNode.CategoryNodes);
            hiddenChannelCategoryJSon.Value = json.Serialize(channelCategoryList);
            //將頻道所屬的icon之資料儲存於隱藏的欄位
            List<List<int>> channelSpecialCategoryList = GetChannelCategoryList(CategoryType.DealSpecialCategory, channelTypeNode.CategoryNodes);
            hiddenChannelSpecialCategoryJSon.Value = json.Serialize(channelSpecialCategoryList);

            channelCheckArea.DataSource = channelTypeNode.CategoryNodes.OrderBy(x => x.Seq);
            channelCheckArea.DataBind();

            this.AllViewCategoryDependencies = allViewCategoryDependencies;
            dealCategoryArea.DataSource = GetCategoryGetListWithoutSubDealCategory(CategoryManager.CategoryGetListByType(CategoryType.DealCategory));

            dealCategoryArea.DataBind();

            chkAgentChannels.DataSource = ProposalFacade.GetAgentChannels();
            chkAgentChannels.DataTextField = "Value";
            chkAgentChannels.DataValueField = "Key";
            chkAgentChannels.DataBind();
        }

        private static List<List<int>> GetChannelCategoryList(CategoryType categoryType, List<CategoryNode> categoryNodes)
        {
            List<List<int>> channelCategoryList = new List<List<int>>();
            foreach (var node in categoryNodes)
            {
                List<int> dealCategories = new List<int>();
                List<CategoryTypeNode> typeNodes = node.NodeDatas.Where(x => x.NodeType == categoryType).ToList();
                if (typeNodes.Count > 0)
                {
                    dealCategories = typeNodes.First().CategoryNodes.Select(x => x.CategoryId).ToList();
                }
                channelCategoryList.Add(dealCategories);
            }
            return channelCategoryList;
        }

        public void SetGroupCouponAppStyleList(Dictionary<int, string> list)
        {
            cbl_GroupCouponAppStyle.DataSource = list;
            cbl_GroupCouponAppStyle.DataTextField = "Value";
            cbl_GroupCouponAppStyle.DataValueField = "Key";
            cbl_GroupCouponAppStyle.DataBind();
        }

        /// <summary>
        /// 設定預設的ICON顯示列表
        /// </summary>
        /// <param name="codeList"></param>
        public void SetDefaultIconSystemCode(List<KeyValuePair<int, string>> codeList)
        {
            cbl_DefaultIcon.DataSource = codeList;
            cbl_DefaultIcon.DataBind();
            foreach (ListItem item in cbl_DefaultIcon.Items)
            {
                if (item.Value == Convert.ToString((int)DealLabelSystemCode.SellOverOneThousand)
                    || item.Value == Convert.ToString((int)DealLabelSystemCode.SoldEndAfterOneDay)
                    || item.Value == Convert.ToString((int)DealLabelSystemCode.SoldEndAfterThreeDay)
                    || item.Value == Convert.ToString((int)DealLabelSystemCode.GroupCoupon)
                    )
                {
                    item.Enabled = false;
                    item.Attributes.CssStyle.Add("color",
                                                 System.Drawing.ColorTranslator.ToHtml(System.Drawing.Color.Gray));
                }
                else if (item.Value == Convert.ToString((int)DealLabelSystemCode.AppLimitedEdition))
                {
                    item.Attributes.Add("class", "appLimitedEdition");
                }
            }
        }

        /// <summary>
        /// 列出所有能設定的DealSpecialCategory
        /// </summary>
        /// <param name="list"></param>
        public void SetSelectableDealSpecialCategory(List<Category> list)
        {
            // category id:317 即將售完 不顯示    id:138 24H到貨 不顯示
            IconArea.DataSource = CategoryManager.CategoryGetListByType(CategoryType.DealSpecialCategory).Where(x => x.Id != 317 && x.Id != 138);

            IconArea.DataBind();
        }
        /// <summary>
        /// 列出目前能選的項目，其他不能選取
        /// </summary>
        /// <param name="deliveryCategory">目前設定的取貨方式</param>
        /// <param name="list">此取貨方式可設定之DealSpecialCategory</param>
        public void SetUnhiddenDealSpecialCategory(Category deliveryCategory, List<Category> list)
        {
            //設定目前取貨設定的categoryId
            hiddenDeliveryCategory.Value = deliveryCategory.Id.ToString();
            //設定此取貨設定能夠選取的ICON Category設定
            hiddenDeliveryIconIdList.Value = new JsonSerializer().Serialize(list.Select(x => x.Id).ToList());
        }

        public void SetSellerTreeView(IEnumerable<SellerNode> sellerTrees)
        {
            repSellerTree.DataSource = sellerTrees.Skip(0).Take(1);
            repSellerTree.DataBind();
        }

        #region Set Deal Label (檔次icon/tag/travel標籤)

        public void SetDealLabel(DealLabelCollection dealLabels)
        {
            //cbl_DefaultIcon.Items.Clear();
            //cbl_CouponIcon.Items.Clear();
            cbl_couponDealTag.Items.Clear();
            cbl_couponTravelDealTag.Items.Clear();
            //cbl_DeliveryIcon.Items.Clear();
            cbl_deliveryDealTag.Items.Clear();
            cbl_deliveryTravelDealTag.Items.Clear();

            foreach (DealLabel dealLabel in dealLabels)
            {
                CheckBoxList list = GetDealLabelCheckBoxList(dealLabel);
                if (list != null)
                {
                    list.Items.Add(GetDealLabelListItem(dealLabel));
                }
            }
        }

        public void SetExpireRedirectDisplay()
        {
            ddlExpireRedirectDisplay.Items.Clear();

            foreach (var display in Enum.GetValues(typeof(ExpireRedirectDisplay)))
            {
                ddlExpireRedirectDisplay.Items.Add(new ListItem(Helper.GetEnumDescription((ExpireRedirectDisplay)display), ((int)display).ToString()));
            }
        }

        private CheckBoxList GetDealLabelCheckBoxList(DealLabel dealLabel)
        {
            string checkBoxListId = string.Empty;

            switch ((DealLabelType)Enum.ToObject(typeof(DealLabelType), dealLabel.DealType))
            {
                case DealLabelType.Coupon:
                    switch (GetDealLabelShowType(dealLabel))
                    {
                        //case DealLabelShowType.icon:
                        //    checkBoxListId = cbl_DefaultIcon.UniqueID;
                        //    break;
                        case DealLabelShowType.tag:
                            checkBoxListId = cbl_couponDealTag.UniqueID;
                            break;
                        case DealLabelShowType.travel:
                            checkBoxListId = cbl_couponTravelDealTag.UniqueID;
                            break;
                    }
                    break;
                case DealLabelType.Delivery:
                    switch (GetDealLabelShowType(dealLabel))
                    {
                        //case DealLabelShowType.icon:
                        //    checkBoxListId = cbl_DefaultIcon.UniqueID;
                        //    break;
                        case DealLabelShowType.tag:
                            checkBoxListId = cbl_deliveryDealTag.UniqueID;
                            break;
                        case DealLabelShowType.travel:
                            checkBoxListId = cbl_deliveryTravelDealTag.UniqueID;
                            break;
                    }
                    break;
            }

            return (CheckBoxList)Page.FindControl(checkBoxListId);
        }

        private DealLabelShowType GetDealLabelShowType(DealLabel dealLabel)
        {
            return (DealLabelShowType)Enum.Parse(typeof(DealLabelShowType), dealLabel.ShowType);
        }

        private ListItem GetDealLabelListItem(DealLabel dealLabel)
        {
            ListItem dealLabelListItem = new ListItem();
            dealLabelListItem.Text = SystemCodeManager.GetDealLabelCodeName((int)dealLabel.SystemCodeId);
            dealLabelListItem.Value = dealLabel.SystemCodeId.ToString();

            //檔次標籤與其他舊有選項連動者預設反灰不讓使用者勾選, 與其他選項一併連動
            switch ((DealLabelSystemCode)Enum.ToObject(typeof(DealLabelSystemCode), dealLabel.SystemCodeId))
            {
                case DealLabelSystemCode.NAOutlyingIslands:
                case DealLabelSystemCode.Tickets:
                case DealLabelSystemCode.SellOverOneThousand:
                case DealLabelSystemCode.OnlyPaperFormatCoupon:
                    dealLabelListItem.Attributes.Add("onclick", "return false;");
                    dealLabelListItem.Attributes.Add("class", "readonly");
                    break;
                case DealLabelSystemCode.PayOnDelivery:
                    dealLabelListItem.Attributes.Add("style", "display:none");
                    dealLabelListItem.Attributes.Add("onclick", "return false;");
                    dealLabelListItem.Attributes.Add("class", "readonly");
                    break;
            }

            //預設需要打勾的檔次標籤
            switch ((DealLabelSystemCode)Enum.ToObject(typeof(DealLabelSystemCode), dealLabel.SystemCodeId))
            {
                case DealLabelSystemCode.NAOutlyingIslands:
                case DealLabelSystemCode.Tickets:
                    dealLabelListItem.Selected = true;
                    break;
            }

            return dealLabelListItem;
        }

        #endregion

        public TimeSpan DefaultOnTime
        {
            set
            {
                dlOSh.SelectedValue = value.Hours.ToString();
                dlOSm.SelectedValue = value.Minutes.ToString();
            }
        }
        public TimeSpan DefaultEndTime
        {
            set
            {
                dlOEh.SelectedValue = value.Hours.ToString();
                dlOEm.SelectedValue = value.Minutes.ToString();
            }
        }

        public string AncestorBusinessHourGuid
        {
            get
            {
                return AncestorBid.Text;
            }
            set
            {
                AncestorBid.Text = value;
            }
        }

        public string AncestorSequenceBusinessHourGuid
        {
            get
            {
                return AncestorSeqBid.Text;
            }
            set
            {
                AncestorSeqBid.Text = value;
            }
        }

        public string AncestorCouponCount
        {
            set
            {
                ancCouponCount.Text = value;
            }
        }

        public string AncestorOrderedQuantity
        {
            set
            {
                ancOrderedQuantity.Text = value;
            }
        }

        public bool Is_Continued_Sequence
        {
            get
            {
                return cbx_Is_Continued_Sequence.Checked;
            }
            set
            {
                cbx_Is_Continued_Sequence.Checked = value;
            }
        }

        public bool Is_Continued_Sequence_Visible
        {
            set
            {
                cbx_Is_Continued_Sequence.Visible = value;
            }
        }

        public bool Is_Continued_Quantity
        {
            get
            {
                return cbx_Is_Continued_Quantity.Checked;
            }
            set
            {
                cbx_Is_Continued_Quantity.Checked = value;
            }
        }

        public bool Is_Continued_Quantity_Visible
        {
            set
            {
                cbx_Is_Continued_Quantity.Visible = value;
            }
        }

        public bool IsDailyRstriction
        {
            set
            {
                chkIsDailyRstriction.Checked = value;
            }
            get
            {
                return chkIsDailyRstriction.Checked;
            }
        }

        /// <summary>
        /// 0元好康活動完成頁大圖連結
        /// </summary>
        public string ActivityUrl
        {
            set
            {
                txtActivityUrl.Text = Server.UrlDecode(value);
            }
            get
            {
                return Server.UrlEncode(txtActivityUrl.Text);
            }
        }

        public string SuccessorCouponCount
        {
            set
            {
                sucCouponCount.Text = value;
            }
        }

        public string SuccessorOrderedQuantity
        {
            set
            {
                sucOrderedQuantity.Text = value;
            }
        }

        //消費方式2012.02.04 add may
        public DeliveryType TheDeliveryType
        {
            get
            {
                return (DeliveryType)int.Parse(ddlRreceiveType.SelectedValue);
            }
            set
            {
                ddlRreceiveType.SelectedValue = ((int)value).ToString();
            }
        }

        //是否使用多重選項
        public bool DShoppingCart
        {
            get
            {
                return cbx_ShoppingCar.Checked;
            }
            set
            {
                cbx_ShoppingCar.Checked = (Convert.ToInt32(value) == 1);
            }
        }

        public bool MultipleBranch
        {
            get
            {
                return cbx_multiBranch.Checked;
            }
            set
            {
                cbx_multiBranch.Checked = value;
            }
        }

        public int ComboPackCount
        {
            get
            {
                double i;
                if (double.TryParse(tbCPC.Text, out i))
                {
                    return Convert.ToInt32(Math.Truncate(i));
                }
                else
                {
                    //亂打或是不打, 則調回預設值 1
                    return 1;
                }
            }
            set
            {
                tbCPC.Text = value.ToString();
            }
        }

        public bool IsPrintWatermark
        {
            get
            {
                return chkIsPrintWatermark.Checked;
            }
        }

        public int? QuantityMultiplier
        {
            get
            {
                if (chkQuantityMultiplier.Checked == false && chkIsAveragePrice.Checked == false)
                {
                    return null;
                }
                int result;
                return int.TryParse(txtQuantityMultiplier.Text, out result) ? result : 1;
            }
            set
            {
                //chkQuantityMultiplier.Checked = (value != null);
                //chkIsAveragePrice.Checked = (value != null);
                txtQuantityMultiplier.Text = value == null ? "" : value.ToString();
            }
        }

        public bool IsQuantityMultiplier
        {
            get { return chkQuantityMultiplier.Checked; }
            set { chkQuantityMultiplier.Checked = value; }
        }
        public bool IsAveragePrice
        {
            get { return chkIsAveragePrice.Checked; }
            set { chkIsAveragePrice.Checked = value; }
        }



        public string BusinessOrderId
        {
            set
            {
                txtBusinessOrderId.Text = value;
            }
            get
            {
                string businessOrderId = txtBusinessOrderId.Text.Trim();
                if (businessOrderId.Split('-').Length > 2)
                {
                    businessOrderId = businessOrderId.Split('-')[0] + "-" + businessOrderId.Split('-')[1];
                }
                return businessOrderId;
            }
        }

        /// <summary>
        /// 運費為多少
        /// </summary>
        protected decimal Freight
        {
            get
            {
                decimal freight;
                if (!decimal.TryParse(tbFt.Text.Trim(), out freight))
                {
                    freight = 0;
                }

                return freight;
            }
            set
            {
                tbFt.Text = value.ToString("N0");
            }
        }

        /// <summary>
        /// 賣家sid
        /// </summary>
        public Guid SellerGuid
        {
            get
            {

                if (string.IsNullOrEmpty(hdSellerGuid.Value))
                {
                    return Guid.Empty;
                }

                try
                {
                    return new Guid(hdSellerGuid.Value);
                }
                catch
                {
                    return Guid.Empty;
                }
            }
            set
            {
                hlStoreAdd.NavigateUrl = string.Format("../seller/seller_add.aspx?sid={0}&&thin=1", value);
                hdSellerGuid.Value = value.ToString();
            }
        }

        /// <summary>
        /// 賣家名稱
        /// </summary>
        public string SellerName
        {
            set
            {
                hlStoreAdd.Text = value;
            }
        }

        //對開發票
        public bool IsMutualInvoice
        {
            get
            {
                return rbMutualYes.Checked;
            }
            set
            {
                if (value)
                {
                    rbMutualYes.Checked = true;
                    rbMutualNo.Checked = false;
                    tbCommission.Enabled = true;
                }
                else
                {
                    rbMutualNo.Checked = true;
                    rbMutualYes.Checked = false;
                    tbCommission.Enabled = false;
                }
            }
        }

        public int Commission
        {
            get
            {
                int val = int.TryParse(tbCommission.Text, out val) ? val : 0;
                return val;
            }
            set
            {
                tbCommission.Text = value.ToString();
            }
        }

        public string ItemName
        {
            get
            {
                return this.tbUse.Text;
            }
        }

        public int DealContentNameLength
        {
            get { return 400; }
        }

        //是否允許修改結帳倍數。如果已有訂單，不允許修改
        public bool IsAllowedToReviseCPC
        {
            set
            {
                tbCPC.Enabled = value;
                NoCPC.Visible = !value;
                //成套票券檔次
                tbBuyQuantity.Enabled = value;
                tbPresentQuantity.Enabled = value;
                ddlGroupCouponType.Enabled = value;
                //通用券檔次設定 已有成立訂單 不允許修改
                cbx_NoRestrictedStore.Enabled = value;
            }
        }

        //成套商品券產生憑證數
        public int SaleMultipleBase
        {
            get
            {
                int bq = int.TryParse(tbBuyQuantity.Text, out bq) ? GroupCoupon && config.IsGroupCouponOn ? bq : 0 : 0;
                int pq = int.TryParse(tbPresentQuantity.Text, out pq) ? GroupCoupon && config.IsGroupCouponOn ? pq : 0 : 0;
                return bq + pq;
            }
            set
            {
                int pq = PresentQuantity;
                tbBuyQuantity.Text = (value - pq).ToString();
            }
        }

        //成套商品券贈送
        public int PresentQuantity
        {
            get
            {
                int val = int.TryParse(tbPresentQuantity.Text, out val) ? GroupCoupon && config.IsGroupCouponOn ? val : 0 : 0;
                return val;
            }
            set
            {
                tbPresentQuantity.Text = value.ToString();
            }
        }

        //成套商品券
        public bool GroupCoupon
        {
            get
            {
                return cbx_GroupCoupon.Checked && config.IsGroupCouponOn;
            }
            set
            {
                cbx_GroupCoupon.Checked = value;
            }
        }

        public int GroupCouponType
        {
            get
            {
                return GroupCoupon ? Int32.Parse(ddlGroupCouponType.SelectedValue) : (int)GroupCouponDealType.None;
            }
            set
            {
                ddlGroupCouponType.SelectedValue = value.ToString();
            }
        }

        //成套商品券App頻道
        public int GroupCouponAppStyle
        {
            get
            {
                var items = cbl_GroupCouponAppStyle.Items.Cast<ListItem>();
                string val = items.Count(x => x.Selected) > 0 ? items.FirstOrDefault(x => x.Selected).Value : "0";
                return cbx_GroupCoupon.Checked ? int.Parse(val) : 0;
            }
            set
            {
                if (value > 0)
                {
                    cbl_GroupCouponAppStyle.Items.FindByValue(value.ToString()).Selected = true;
                }
            }
        }

        #region 訂位系統設定

        public int BookingSystemType
        {
            get
            {
                if (!cb_IsUsingbookingSystem.Checked)
                {
                    return 0;
                }
                else if (rdoBookingSystemSeat.Checked)
                {
                    return 1;
                }
                else if (rdoBookingSystemRoom.Checked)
                {
                    return 2;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                rdoBookingSystemSeat.Checked = rdoBookingSystemRoom.Checked = false;
                switch (value)
                {
                    case 0:
                        cb_IsUsingbookingSystem.Checked = false;
                        break;
                    case 1:
                    case 2:
                        cb_IsUsingbookingSystem.Checked = true;
                        if (value == 1)
                        {
                            rdoBookingSystemSeat.Checked = true;
                        }
                        else
                        {
                            rdoBookingSystemRoom.Checked = true;
                        }
                        break;
                }
            }
        }

        public int AdvanceReservationDays
        {
            get
            {
                int val = 0;
                if (cb_IsUsingbookingSystem.Checked)
                {
                    int.TryParse(txtAdvanceReservationDays.Text.Trim(), out val);
                }
                return val;
            }
            set
            {
                txtAdvanceReservationDays.Text = value.ToString();
            }
        }

        public int CouponUsers
        {
            get
            {
                if (cb_IsUsingbookingSystem.Checked)
                {
                    if (rdoSingleUser.Checked)
                    {
                        return 1;
                    }
                    else if (rdoMultiUsers.Checked)
                    {
                        int val = 0;
                        int.TryParse(txtCouponUsers.Text.Trim(), out val);
                        return val;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                if (BookingSystemType > 0)
                {
                    rdoSingleUser.Checked = rdoMultiUsers.Checked = false;
                    txtCouponUsers.Text = "";
                    if (value > 1)
                    {
                        rdoMultiUsers.Checked = true;
                        txtCouponUsers.Text = value.ToString();
                    }
                    else
                    {
                        rdoSingleUser.Checked = true;
                    }
                }
            }
        }

        public bool IsReserveLock
        {
            get
            {
                return cbIsReserveLock.Checked;
            }
            set
            {
                cbIsReserveLock.Checked = value;
            }
        }

        #endregion 訂位系統設定

        /// <summary>
        /// 長期約
        /// </summary>
        public bool IsLongContract
        {
            set { cbx_isLongContract.Checked = value; }
            get { return cbx_isLongContract.Checked; }
        }

        /// <summary>
        /// 完全複製檔
        /// </summary>
        public bool CompleteCopy
        {
            set { cbx_CompleteCopy.Checked = value; }
            get { return cbx_CompleteCopy.Checked; }
        }

        public string AgentChannels
        {
            get
            {
                List<int> list = new List<int>();
                for (int idx = 0; idx < chkAgentChannels.Items.Count; idx++)
                {
                    if (chkAgentChannels.Items[idx].Selected)
                    {
                        string v = chkAgentChannels.Items[idx].Value;
                        int s = 0;
                        if (int.TryParse(v, out s))
                        {
                            list.Add(s);
                        }
                    }
                }

                return string.Join(",", list);
            }
            set
            {
                string[] agentChannels = value.Split(",");
                for (int idx = 0; idx < chkAgentChannels.Items.Count; idx++)
                {
                    if (Array.IndexOf(agentChannels.ToArray(), chkAgentChannels.Items[idx].Value) >= 0)
                    {
                        chkAgentChannels.Items[idx].Selected = true;
                    }
                }
            }
        }


        /// <summary>
        /// 限刷台新信用卡
        /// </summary>
        public LimitBankIdModel LimitCreditCardBankId
        {
            get
            {
                return (cbx_TaiShin.Checked) ? LimitBankIdModel.TaiShinBank : 0;
            }
            set
            {
                switch (value)
                {
                    case LimitBankIdModel.TaiShinBank:
                        cbx_TaiShin.Checked = true;
                        break;

                    default:
                        break;
                }

            }
        }

        /// <summary>
        /// 出貨方式
        /// </summary>
        public DealShipType OrderShipType
        {
            get
            {
                return (rboFast.Checked) ? DealShipType.Ship72Hrs : (rboNormal.Checked) ? DealShipType.Normal : (rboLastShipment.Checked) ? DealShipType.LastShipment : (rboLastDelivery.Checked) ? DealShipType.LastDelivery : (rboWms.Checked) ? DealShipType.Wms : DealShipType.Other; //預設值為一般出貨
            }
            set
            {
                switch (value)
                {
                    case DealShipType.Normal:
                        rboNormal.Checked = true;
                        break;

                    case DealShipType.Ship72Hrs:
                        rboFast.Checked = true;
                        break;

                    case DealShipType.LastShipment:
                        rboLastShipment.Checked = true;
                        break;

                    case DealShipType.LastDelivery:
                        rboLastDelivery.Checked = true;
                        break;
                    case DealShipType.Wms:
                        rboWms.Checked = true;
                        break;
                    case DealShipType.Other:
                        rboOther.Checked = true;
                        break;

                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// 出貨日期格式
        /// </summary>
        public DealShippingDateType DealShippingDateType
        {
            get
            {
                return (rboUniShipDay.Checked) ? DealShippingDateType.Special : DealShippingDateType.Normal; //預設值為一般出貨
            }
            set
            {
                switch (value)
                {
                    case DealShippingDateType.Normal:
                        hidSubShipType.Value = "Normal";
                        rboNormalShipDay.Checked = true;
                        break;

                    case DealShippingDateType.Special:
                        hidSubShipType.Value = "Special";
                        rboUniShipDay.Checked = true;
                        break;

                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// 出貨日期
        /// </summary>
        public int ShippingDate
        {
            get
            {
                int val = int.TryParse(txtNormalShipDay.Text, out val) ? val : 7;
                return val;
            }
            set
            {
                txtNormalShipDay.Text = value.ToString();
            }
        }

        /// <summary>
        /// 最早出貨日出貨日期
        /// </summary>
        public int ProductUseDateStartSet
        {
            get
            {
                int val = int.TryParse(txtEarliestShipDate.Text, out val) ? val : 0;
                return val;
            }
            set
            {
                txtEarliestShipDate.Text = value.ToString();
            }
        }

        /// <summary>
        /// 最晚出貨日出貨日期
        /// </summary>
        public int ProductUseDateEndSet
        {
            get
            {
                int val = int.TryParse(txtLatestShipDate.Text, out val) ? val : 7;
                return val;
            }
            set
            {
                txtLatestShipDate.Text = value.ToString();
            }
        }

        public bool NoRestrictedStore
        {
            set { cbx_NoRestrictedStore.Checked = value; }
            get { return cbx_NoRestrictedStore.Checked; }
        }

        public bool NotShowDealDetailOnWeb
        {
            set { cbx_NotShowDealDetailOnWeb.Checked = value; }
            get { return cbx_NotShowDealDetailOnWeb.Checked; }
        }

        public List<ViewCategoryDependency> AllViewCategoryDependencies { get; set; }

        public bool HasRemittanceTypeSetRight {
            get { return CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.RemittanceTypeSet); }
        }

        public bool Consignment
        {
            set { cbx_Consignment.Checked = value; }
            get { return cbx_Consignment.Checked; }
        }
        public int? CouponSeparateDigits
        {
            get
            {
                int val = int.TryParse(tbCouponSeparateDigits.Text, out val) ? val : 0;
                return val;
            }
            set
            {
                tbCouponSeparateDigits.Text = value.ToString();
                int val = int.TryParse(tbCouponSeparateDigits.Text, out val) ? val : 0;
                cbxCouponSeparateDigits.Checked = val> 0 ? true:false;
            }
        }

        public bool IsSetExpireDateNoLimit
        {
            get
            {
                return CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.SetExpireDateNoLimit);
            }
        }

        public bool IsAgreeHouseNewContractSeller
        {
            get
            {
                return SellerFacade.IsAgreeNewContractSeller(SellerGuid, (int)DeliveryType.ToHouse);
            }
        }


        public bool IsAgreePponNewContractSeller
        {
            get
            {
                return SellerFacade.IsAgreeNewContractSeller(SellerGuid, (int)DeliveryType.ToShop);
            }
        }

        /// <summary>
        /// 不顯示折後價於前台
        /// </summary>
        public bool IsDealDiscountPriceBlacklist
        {
            set { cbx_isDealDiscountPriceBlacklist.Checked = value; }
            get { return cbx_isDealDiscountPriceBlacklist.Checked; }
        }

        public string DealTypeJson
        {
            get
            {
                var jsonIgnoreSetting = new JsonIgnoreSetting(typeof(DealTypeNode), "Parent");
                List<DealTypeNode> categories = DealTypeFacade.GetDealTypeNodes(true);
                return json.Serialize(categories, false, jsonIgnoreSetting);
            }
        }

        #endregion Props

        protected void repComboItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }

            ViewComboDeal vcd = (ViewComboDeal)e.Item.DataItem;

            /*企劃確認購買人(份)數不做退貨完成後數量處理, 但要在此修正上線前的檔次都要照原邏輯執行*/
            bool stopRefundQuantityCalculateDate = vcd.BusinessHourOrderTimeS >= config.StopRefundQuantityCalculateDate;

            Literal litComboItemSalesQuantity = (Literal)e.Item.FindControl("litComboItemSalesQuantity");
            litComboItemSalesQuantity.Text = stopRefundQuantityCalculateDate ? vcd.OrderedIncludeRefundQuantity.ToString() : vcd.OrderedQuantity.ToString();

            Literal litComboItemQuantityMultiplier = (Literal)e.Item.FindControl("litComboItemQuantityMultiplier");
            litComboItemQuantityMultiplier.Text = (vcd.QuantityMultiplier.HasValue) ? vcd.QuantityMultiplier.Value.ToString() : string.Empty;

            Literal litComboItemIsAveragePrice = (Literal)e.Item.FindControl("litComboItemIsAveragePrice");
            if (!vcd.IsAveragePrice)
            {
                litComboItemIsAveragePrice.Text = vcd.IsAveragePrice.ToString().ToLower();
            }
            else
            {
                if ((vcd.IsAveragePrice && vcd.QuantityMultiplier != null && vcd.QuantityMultiplier >= 1))
                {
                    litComboItemIsAveragePrice.Text = (Math.Ceiling(vcd.ItemPrice / vcd.QuantityMultiplier.Value)).ToString("F0");
                }
                else
                {
                    litComboItemIsAveragePrice.Text = vcd.ItemPrice.ToString("F0");
                }
            }


            TextBox txtComboItemTitle = (TextBox)e.Item.FindControl("txtComboItemTitle");
            txtComboItemTitle.Text = vcd.Title;
        }


        public void SetInitStores(IEnumerable<Seller> sellers)
        {
            //12/30 因應新光上千個櫃位問題導致效能不好 改以repeater抓第一筆分店後由api撈取剩下部分
            repStore.DataSource = sellers.Skip(0).Take(1);
            repStore.DataBind();
        }

        public void SetInitComboDeals(ViewPponDealCollection deals, PponDeal theDeal, PponDeal mainDeal,
                                      List<ViewComboDeal> comboDeals, List<PponDeal> comboEntityDeals,
                                      CategoryDealCollection mainCategory, List<CategoryDealCollection> comboCategoryDeals,
                                      DealAccounting mainAccounting, List<DealAccounting> comboAccountingDeals,
                                      List<ViewPponDeal> comboViewPponDeal, ViewPponDeal pponOrder, List<Category> categoryList)
        {
            bool isMain = (theDeal.Deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0;
            isSub = (theDeal.Deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0;
            hidisSub.Value = isSub.ToString();
            repComboItems.DataSource = comboDeals;
            repComboItems.DataBind();
            if (isMain || isSub)
            {
                if (isMain)
                {
                    linkComboMainTitle.CssClass = "comboItemActived noBtnUi";

                    phCopyComboDeals.Visible = true;
                    divPdfItemName.Visible = false;
                    plMoveSeller.Visible = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.SellerMove);
                    spnMoveSellerResult.Text = IsDealOpened ? "已開檔之檔次無法搬移賣家！" : string.Empty;
                }

                if (isSub)
                {
                    ddlGroupCouponType.Visible = false;
                    phNewComboSub.Visible = false;
                    if (config.IsConsignment)
                    {
                        cbx_Consignment.Visible = false;
                    }

                    // hide delete checkbox phDeleteComboItem
                    foreach (var ctrl in repComboItems.Controls)
                    {
                        RepeaterItem item = ctrl as RepeaterItem;
                        if (item.ItemType == ListItemType.Header)
                        {
                            item.FindControl("litDeleteComboItemCaption").Visible = false;
                        }
                        else if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                        {
                            item.FindControl("phDeleteComboItem").Visible = false;
                        }
                    }

                    //子檔同步項目(子檔不需要設定同步子檔)
                    phCopyComboDeals.Visible = false;

                    plMoveSeller.Visible = false;
                }

                lockFinanceSection(false);

                linkComboMainTitle.Visible = true;
                linkComboMainTitle.Text = string.Format("({0}){1}", mainDeal.Property.UniqueId, mainDeal.DealContent.CouponUsage);
                linkComboMainTitle.NavigateUrl = "setup.aspx?bid=" + mainDeal.Deal.Guid;
            }
            else
            {
                phNewComboSub.Visible = true;
                linkComboMainTitle.Visible = false;
                //子檔同步項目(單一檔次不需要設定同步子檔)
                phCopyComboDeals.Visible = false;

                plMoveSeller.Visible = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.SellerMove);
                spnMoveSellerResult.Text = IsDealOpened ? "已開檔之檔次無法搬移賣家！" : string.Empty;
            }

            if (isSub)
            {
                //子檔
                chk_dealMain.Checked = false;
                chkCloseMenu.Checked = true;
            }
            else
            {
                if (theDeal.DealContent.IsCloseMenu != false)
                {
                    //true or null都關閉
                    chkCloseMenu.Checked = true;
                }
                if (theDeal.DealContent.IsMenuLogo ?? false)
                {
                    chkMenuLogo.Checked = true;
                }
                //單檔或母檔
                chk_dealMain.Checked = false;
                if (string.IsNullOrEmpty(theDeal.DealContent.MenuF))
                {
                    if (comboDeals.Count > 0)
                    {
                        MenuContent = getMenuContent(comboDeals, theDeal.Property.LabelTagList);
                        tbMenuF.Text = MenuContent;
                    }
                    else
                    {
                        List<ViewComboDeal> vcds = new List<ViewComboDeal>();
                        try
                        {
                            vcds.Add(new ViewComboDeal
                            {
                                BusinessHourGuid = theDeal.DealContent.BusinessHourGuid,
                                CouponUsage = theDeal.DealContent.CouponUsage,
                                ItemOrigPrice = theDeal.ItemDetail.ItemOrigPrice,
                                ItemPrice = theDeal.ItemDetail.ItemPrice,
                                QuantityMultiplier = theDeal.Property.QuantityMultiplier,
                                IsAveragePrice = theDeal.Property.IsAveragePrice,
                            });
                        }
                        catch { }

                        MenuContent = getMenuContent(vcds, theDeal.Property.LabelTagList);
                        tbMenuF.Text = MenuContent;
                    }
                }
            }

            if (mainDeal != null)
            {
                spanComboMainTitle.Attributes["bid"] = mainDeal.Deal.Guid.ToString();
                spanComboMainTitle.Attributes["title"] = mainDeal.DealContent.CouponUsage;
            }
            else
            {
                spanComboMainTitle.Attributes["bid"] = this.BusinessHourId.ToString();
                spanComboMainTitle.Attributes["title"] = "主檔";
            }

            ddlComboSub.Items.Add(new ListItem { Text = "", Value = "" });

            foreach (var deal in deals.OrderByDescending(x => x.BusinessHourDeliverTimeE).ToList())
            {
                string dealLinkTitle = string.Format("({0}){1}",
                    deal.UniqueId,
                    deal.CouponUsage);

                string dealFullTitle = string.Format("{0}-({1}){2}",
                    deal.BusinessHourOrderTimeS.ToString("yyyy/MM/dd"),
                    deal.UniqueId,
                    deal.CouponUsage);

                ListItem li = new ListItem
                {
                    Text = dealFullTitle,
                    Value = deal.BusinessHourGuid.ToString(),
                };
                li.Attributes["linktitle"] = dealLinkTitle;
                li.Attributes["couponusage"] = deal.CouponUsage;
                ddlComboSub.Items.Add(li);
            }

            //validate
            if (mainDeal != null)
            {
                Func<Guid, int, string, string> GeComboSubLink = delegate (Guid bid, int uniqueId, string couponUsage)
                {
                    return string.Format(
                            "<a style='margin-left:24px' class='noBtnUi' target='_blank' href='setup.aspx?bid={0}'>({1}){2}</a><br/>",
                            bid, uniqueId, couponUsage.Replace("{", "{{").Replace("}", "}}"));
                };
                StringBuilder sbComboErrMsg = new StringBuilder();
                decimal diffTotal =
                    mainDeal.Deal.OrderTotalLimit.GetValueOrDefault() - comboDeals.Sum(x => x.OrderTotalLimit).GetValueOrDefault();
                if (diffTotal != 0)
                {
                    sbComboErrMsg.AppendFormat("<span style=\"color:blue;\">主檔和子檔總數相差:</span>{0}<br/>",
                        diffTotal.ToString("F0"));
                }


                ViewComboDeal[] diffStartDeals =
                    comboDeals.Where(t => t.BusinessHourOrderTimeS != mainDeal.Deal.BusinessHourOrderTimeS).ToArray();
                if (diffStartDeals.Length > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔開檔日不同:</span><br />");
                    foreach (ViewComboDeal deal in diffStartDeals)
                    {
                        sbComboErrMsg.AppendFormat(GeComboSubLink(deal.BusinessHourGuid, deal.UniqueId, deal.CouponUsage));
                    }
                }

                ViewComboDeal[] diffEndDeals = comboDeals.Where(
                    t => t.BusinessHourOrderTimeE != mainDeal.Deal.BusinessHourOrderTimeE).ToArray();
                if (diffEndDeals.Length > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔結檔日不同:</span></br>");
                    foreach (ViewComboDeal deal in diffEndDeals)
                    {
                        sbComboErrMsg.AppendFormat(GeComboSubLink(deal.BusinessHourGuid, deal.UniqueId, deal.CouponUsage));
                    }
                }

                ViewComboDeal[] diffDeliveryTimeDeals = comboDeals.Where(
                        t => t.BusinessHourDeliverTimeS != mainDeal.Deal.BusinessHourDeliverTimeS ||
                            t.BusinessHourDeliverTimeE != mainDeal.Deal.BusinessHourDeliverTimeE).ToArray();
                if (diffDeliveryTimeDeals.Length > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔配送日期不同:</span></br>");
                    foreach (ViewComboDeal deal in diffDeliveryTimeDeals)
                    {
                        sbComboErrMsg.AppendFormat(GeComboSubLink(deal.BusinessHourGuid, deal.UniqueId, deal.CouponUsage));
                    }
                }
                /*
                 * 調整有效期限截止日
                 * */
                IEnumerable<PponDeal> diffChangedExpireDateDeals = comboEntityDeals.Where(t => t.Deal.ChangedExpireDate != mainDeal.Deal.ChangedExpireDate);
                if (diffChangedExpireDateDeals.Count() > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔調整有效期限截止日不同:</span></br>");
                    foreach (PponDeal deal in diffChangedExpireDateDeals)
                    {
                        sbComboErrMsg.AppendFormat(GeComboSubLink(deal.Property.BusinessHourGuid, deal.Property.UniqueId, deal.DealContent.Name));
                    }
                }
                /*
                 * 業務
                 * */
                IEnumerable<PponDeal> diffEmpNameDeals = comboEntityDeals.Where(t => t.Property.DealEmpName != mainDeal.Property.DealEmpName);
                if (diffEmpNameDeals.Count() > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔業務不同:</span></br>");
                    foreach (PponDeal deal in diffEmpNameDeals)
                    {
                        sbComboErrMsg.AppendFormat(GeComboSubLink(deal.Property.BusinessHourGuid, deal.Property.UniqueId, deal.DealContent.Name));
                    }
                }
                /*
                 * 上檔頻道(依排檔區域勾選)
                 * */
                List<Guid> diffCategoryDeals = new List<Guid>();
                foreach (CategoryDealCollection ccdc in comboCategoryDeals)
                {
                    foreach (CategoryDeal mcd in mainCategory)
                    {
                        List<Category> vcd = categoryList.Where(x => x.Id.Equals(mcd.Cid)).ToList();

                        if (vcd.Count() > 0)
                        {
                            if (vcd[0].Type.Equals(-1) || vcd[0].Type.Equals(3) || vcd[0].Type.Equals(4) || vcd[0].Type.Equals(5) || vcd[0].Type.Equals(6) || vcd[0].Type.Equals(7) || vcd[0].Type.Equals(8))
                            {
                                CategoryDeal[] cd = ccdc.Where(t => t.Cid.Equals(mcd.Cid)).ToArray();
                                if (cd.Length == 0)
                                {
                                    if (ccdc.Count() > 0)
                                    {
                                        if (!diffCategoryDeals.Contains(ccdc[0].Bid))
                                        {
                                            diffCategoryDeals.Add(ccdc[0].Bid);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                foreach (CategoryDealCollection ccdc in comboCategoryDeals)
                {
                    foreach (CategoryDeal mcd in ccdc)
                    {
                        List<Category> vcd = categoryList.Where(x => x.Id.Equals(mcd.Cid)).ToList();

                        if (vcd.Count() > 0)
                        {
                            if (vcd[0].Type.Equals(-1) || vcd[0].Type.Equals(3) || vcd[0].Type.Equals(4) || vcd[0].Type.Equals(5) || vcd[0].Type.Equals(6) || vcd[0].Type.Equals(7) || vcd[0].Type.Equals(8))
                            {
                                CategoryDeal[] cd = mainCategory.Where(t => t.Cid.Equals(mcd.Cid)).ToArray();
                                if (cd.Length == 0)
                                {
                                    if (ccdc.Count() > 0)
                                    {
                                        if (!diffCategoryDeals.Contains(ccdc[0].Bid))
                                        {
                                            diffCategoryDeals.Add(ccdc[0].Bid);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //比較母子檔的筆數是否一致
                //int mc = mainCategory.Count();
                //foreach (CategoryDealCollection ccdc in comboCategoryDeals){
                //    int cc = ccdc.Count();
                //    if (cc != mc)
                //    {
                //        if (ccdc.Count() > 0) {
                //            if (!diffCategoryDeals.Contains(ccdc[0].Bid))
                //            {
                //                diffCategoryDeals.Add(ccdc[0].Bid);
                //            }
                //        }

                //    }
                //}
                //比較Icon
                List<ViewPponDeal> diffIconList = comboViewPponDeal.Where(t => t.LabelIconList != mainDeal.Property.LabelIconList).ToList();
                if (diffIconList.Count() > 0)
                {
                    foreach (ViewPponDeal cd in diffIconList)
                    {
                        if (!diffCategoryDeals.Contains(cd.BusinessHourGuid))
                        {
                            diffCategoryDeals.Add(cd.BusinessHourGuid);
                        }

                    }
                }

                if (diffCategoryDeals.Count() > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔上檔頻道(依排檔區域勾選)不同:</span></br>");
                    foreach (Guid g in diffCategoryDeals)
                    {
                        PponDeal[] _pponDeal = comboEntityDeals.Where(t => t.Property.BusinessHourGuid.Equals(g)).ToArray();
                        if (_pponDeal.Count() > 0)
                        {
                            sbComboErrMsg.AppendFormat(GeComboSubLink(_pponDeal[0].Property.BusinessHourGuid, _pponDeal[0].Property.UniqueId, _pponDeal[0].DealContent.Name));
                        }
                    }
                }

                /*
                 * SEO設定
                 * */
                IEnumerable<PponDeal> diffPageDescDeals = comboEntityDeals.Where(t => t.Deal.PageTitle != mainDeal.Deal.PageTitle ||
                    t.Deal.PageDesc != mainDeal.Deal.PageDesc ||
                    t.Deal.PicAlt != mainDeal.Deal.PicAlt);
                if (diffPageDescDeals.Count() > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔SEO設定不同:</span></br>");
                    foreach (PponDeal deal in diffPageDescDeals)
                    {
                        sbComboErrMsg.AppendFormat(GeComboSubLink(deal.Property.BusinessHourGuid, deal.Property.UniqueId, deal.DealContent.Name));
                    }
                }
                /*
                 * 無法配送外島    
                 * */
                List<Guid> lstNotDeliveryIslands = new List<Guid>();
                bool diffNotDeliveryIslands = Helper.IsFlagSet(pponOrder.BusinessHourStatus, BusinessHourStatus.NotDeliveryIslands);
                foreach (ViewPponDeal cd in comboViewPponDeal)
                {
                    bool diffcomboNotDeliveryIslands = Helper.IsFlagSet(cd.BusinessHourStatus, BusinessHourStatus.NotDeliveryIslands);
                    if (diffNotDeliveryIslands != diffcomboNotDeliveryIslands)
                    {
                        lstNotDeliveryIslands.Add(cd.BusinessHourGuid);
                    }
                }
                if (lstNotDeliveryIslands.Count() > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔無法配送外島不同:</span></br>");

                    foreach (Guid g in lstNotDeliveryIslands)
                    {
                        PponDeal[] _pponDeal = comboEntityDeals.Where(t => t.Property.BusinessHourGuid.Equals(g)).ToArray();
                        if (_pponDeal.Count() > 0)
                        {
                            sbComboErrMsg.AppendFormat(GeComboSubLink(_pponDeal[0].Property.BusinessHourGuid, _pponDeal[0].Property.UniqueId, _pponDeal[0].DealContent.Name));
                        }
                    }
                }

                /*
                 * 館別
                 * */
                IEnumerable<PponDeal> diffDealAccBusinessGroupIdDeals = comboEntityDeals.Where(t => t.Property.DealAccBusinessGroupId != mainDeal.Property.DealAccBusinessGroupId);
                if (diffDealAccBusinessGroupIdDeals.Count() > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔館別不同:</span></br>");
                    foreach (PponDeal deal in diffDealAccBusinessGroupIdDeals)
                    {
                        sbComboErrMsg.AppendFormat(GeComboSubLink(deal.Property.BusinessHourGuid, deal.Property.UniqueId, deal.DealContent.Name));
                    }
                }
                /*
                 * 業績歸屬
                 * */
                IEnumerable<DealAccounting> diffDeptIdDeals = comboAccountingDeals.Where(t => t.DeptId != mainAccounting.DeptId);
                if (diffDeptIdDeals.Count() > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔業績歸屬不同:</span></br>");
                    foreach (DealAccounting deal in diffDeptIdDeals)
                    {
                        PponDeal[] _pponDeal = comboEntityDeals.Where(t => t.Property.BusinessHourGuid.Equals(deal.BusinessHourGuid)).ToArray();
                        if (_pponDeal.Count() > 0)
                        {
                            sbComboErrMsg.AppendFormat(GeComboSubLink(_pponDeal[0].Property.BusinessHourGuid, _pponDeal[0].Property.UniqueId, _pponDeal[0].DealContent.Name));
                        }
                    }
                }

                /*
                 * 分類
                 * */
                IEnumerable<PponDeal> diffDealTypeDeals = comboEntityDeals.Where(t => t.Property.DealType != mainDeal.Property.DealType);
                if (diffDealTypeDeals.Count() > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔分類不同:</span></br>");
                    foreach (PponDeal deal in diffDealTypeDeals)
                    {
                        sbComboErrMsg.AppendFormat(GeComboSubLink(deal.Property.BusinessHourGuid, deal.Property.UniqueId, deal.DealContent.Name));
                    }
                }
                /*
                 * 收據資料
                 * */
                IEnumerable<PponDeal> diffEntrustSellDeals = comboEntityDeals.Where(t => t.Property.EntrustSell != mainDeal.Property.EntrustSell);
                if (diffEntrustSellDeals.Count() > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔收據資料:</span></br>");
                    foreach (PponDeal deal in diffEntrustSellDeals)
                    {
                        sbComboErrMsg.AppendFormat(GeComboSubLink(deal.Property.BusinessHourGuid, deal.Property.UniqueId, deal.DealContent.Name));
                    }
                }
                /*
                 * 核銷與帳戶設定
                 * */
                IEnumerable<DealAccounting> diffPaytocompanyDeals = comboAccountingDeals.Where(t => t.Paytocompany != mainAccounting.Paytocompany ||
                                                                                    t.VendorBillingModel != mainAccounting.VendorBillingModel ||
                                                                                    t.RemittanceType != mainAccounting.RemittanceType ||
                                                                                    t.Message != mainAccounting.Message ||
                                                                                    t.IsInputTaxRequired != mainAccounting.IsInputTaxRequired ||
                                                                                    t.VendorReceiptType != mainAccounting.VendorReceiptType);
                if (diffPaytocompanyDeals.Count() > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔核銷與帳戶設定不同:</span></br>");
                    foreach (DealAccounting deal in diffPaytocompanyDeals)
                    {
                        PponDeal[] _pponDeal = comboEntityDeals.Where(t => t.Property.BusinessHourGuid.Equals(deal.BusinessHourGuid)).ToArray();
                        if (_pponDeal.Count() > 0)
                        {
                            sbComboErrMsg.AppendFormat(GeComboSubLink(_pponDeal[0].Property.BusinessHourGuid, _pponDeal[0].Property.UniqueId, _pponDeal[0].DealContent.Name));
                        }
                    }
                }
                /*
                 * 不接受退貨(捐款檔次)
                 * */
                List<Guid> lstNoRefund = new List<Guid>();
                bool diffNoRefund = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.NoRefund);
                bool diffKindDeal = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal);
                foreach (ViewPponDeal cd in comboViewPponDeal)
                {
                    bool diffcomboNoRefund = Helper.IsFlagSet(cd.GroupOrderStatus ?? 0, GroupOrderStatus.NoRefund);
                    bool diffcomboKindDeal = Helper.IsFlagSet(cd.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal);
                    if ((diffNoRefund != diffcomboNoRefund) || (diffKindDeal != diffcomboKindDeal))
                    {
                        lstNoRefund.Add(cd.BusinessHourGuid);
                    }
                }
                if (lstNoRefund.Count() > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔不接受退貨(捐款檔次)不同:</span></br>");

                    foreach (Guid g in lstNoRefund)
                    {
                        PponDeal[] _pponDeal = comboEntityDeals.Where(t => t.Property.BusinessHourGuid.Equals(g)).ToArray();
                        if (_pponDeal.Count() > 0)
                        {
                            sbComboErrMsg.AppendFormat(GeComboSubLink(_pponDeal[0].Property.BusinessHourGuid, _pponDeal[0].Property.UniqueId, _pponDeal[0].DealContent.Name));
                        }
                    }
                }

                /*
                * 結檔後七天不能退貨
                * */
                List<Guid> lstDaysNoRefund = new List<Guid>();
                bool diffDaysNoRefund = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.DaysNoRefund);
                foreach (ViewPponDeal cd in comboViewPponDeal)
                {
                    bool diffcomboDaysNoRefund = Helper.IsFlagSet(cd.GroupOrderStatus ?? 0, GroupOrderStatus.DaysNoRefund);
                    if (diffDaysNoRefund != diffcomboDaysNoRefund)
                    {
                        lstDaysNoRefund.Add(cd.BusinessHourGuid);
                    }
                }
                if (lstDaysNoRefund.Count() > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔結檔後七天不能退貨不同:</span></br>");

                    foreach (Guid g in lstDaysNoRefund)
                    {
                        PponDeal[] _pponDeal = comboEntityDeals.Where(t => t.Property.BusinessHourGuid.Equals(g)).ToArray();
                        if (_pponDeal.Count() > 0)
                        {
                            sbComboErrMsg.AppendFormat(GeComboSubLink(_pponDeal[0].Property.BusinessHourGuid, _pponDeal[0].Property.UniqueId, _pponDeal[0].DealContent.Name));
                        }
                    }
                }
                /*
                 * 演出時間前十日內不能退貨
                 * */
                List<Guid> lstNoRefundBeforeDays = new List<Guid>();
                bool diffNoRefundBeforeDays = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.NoRefundBeforeDays);
                foreach (ViewPponDeal cd in comboViewPponDeal)
                {
                    bool diffcomboNoRefundBeforeDays = Helper.IsFlagSet(cd.GroupOrderStatus ?? 0, GroupOrderStatus.NoRefundBeforeDays);
                    if (diffNoRefundBeforeDays != diffcomboNoRefundBeforeDays)
                    {
                        lstNoRefundBeforeDays.Add(cd.BusinessHourGuid);
                    }
                }
                if (lstNoRefundBeforeDays.Count() > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔演出時間前十日內不能退貨不同:</span></br>");

                    foreach (Guid g in lstNoRefundBeforeDays)
                    {
                        PponDeal[] _pponDeal = comboEntityDeals.Where(t => t.Property.BusinessHourGuid.Equals(g)).ToArray();
                        if (_pponDeal.Count() > 0)
                        {
                            sbComboErrMsg.AppendFormat(GeComboSubLink(_pponDeal[0].Property.BusinessHourGuid, _pponDeal[0].Property.UniqueId, _pponDeal[0].DealContent.Name));
                        }
                    }
                }
                /*
                 * 過期不能退貨
                 * */
                List<Guid> lstExpireNoRefund = new List<Guid>();
                bool diffExpireNoRefunds = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.ExpireNoRefund);
                foreach (ViewPponDeal cd in comboViewPponDeal)
                {
                    bool diffcomboExpireNoRefund = Helper.IsFlagSet(cd.GroupOrderStatus ?? 0, GroupOrderStatus.ExpireNoRefund);
                    if (diffExpireNoRefunds != diffcomboExpireNoRefund)
                    {
                        lstExpireNoRefund.Add(cd.BusinessHourGuid);
                    }
                }
                if (lstExpireNoRefund.Count() > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔過期不能退貨不同:</span></br>");

                    foreach (Guid g in lstExpireNoRefund)
                    {
                        PponDeal[] _pponDeal = comboEntityDeals.Where(t => t.Property.BusinessHourGuid.Equals(g)).ToArray();
                        if (_pponDeal.Count() > 0)
                        {
                            sbComboErrMsg.AppendFormat(GeComboSubLink(_pponDeal[0].Property.BusinessHourGuid, _pponDeal[0].Property.UniqueId, _pponDeal[0].DealContent.Name));
                        }
                    }
                }
                /*
                  * 0元或廠商提序號檔次，提供下載序號(不含簡訊)
                  * */
                List<Guid> lstPEZeventCouponDownload = new List<Guid>();
                bool diffPEZeventCouponDownload = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.PEZeventCouponDownload);
                foreach (ViewPponDeal cd in comboViewPponDeal)
                {
                    bool diffcomboPEZeventCouponDownload = Helper.IsFlagSet(cd.GroupOrderStatus ?? 0, GroupOrderStatus.PEZeventCouponDownload);
                    if (diffPEZeventCouponDownload != diffcomboPEZeventCouponDownload)
                    {
                        lstPEZeventCouponDownload.Add(cd.BusinessHourGuid);
                    }
                }
                if (lstPEZeventCouponDownload.Count() > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔0元或廠商提序號檔次，提供下載序號(不含簡訊)不同:</span></br>");

                    foreach (Guid g in lstPEZeventCouponDownload)
                    {
                        PponDeal[] _pponDeal = comboEntityDeals.Where(t => t.Property.BusinessHourGuid.Equals(g)).ToArray();
                        if (_pponDeal.Count() > 0)
                        {
                            sbComboErrMsg.AppendFormat(GeComboSubLink(_pponDeal[0].Property.BusinessHourGuid, _pponDeal[0].Property.UniqueId, _pponDeal[0].DealContent.Name));
                        }
                    }
                }



                //住宿類，商家系統提供「訂房鎖定」功能
                IEnumerable<PponDeal> diffIsReserveLockDeals = comboEntityDeals.Where(t => t.Property.IsReserveLock != mainDeal.Property.IsReserveLock);
                if (diffIsReserveLockDeals.Count() > 0)
                {
                    sbComboErrMsg.Append("<span style=\"color:blue;\">主檔和子檔住宿類，商家系統提供「訂房鎖定」功能不同:</span></br>");
                    foreach (PponDeal deal in diffIsReserveLockDeals)
                    {
                        sbComboErrMsg.AppendFormat(GeComboSubLink(deal.Property.BusinessHourGuid, deal.Property.UniqueId, deal.DealContent.Name));
                    }
                }




                litComboErrMsg.Text = sbComboErrMsg.ToString();
            }
        }

        public void SetDealView(PponDeal deal, string accessoryText, CouponEventContent cec, PponStoreCollection pponStores, bool isPChomeChannelProd)
        {
            tbN.Text = deal.DealContent.Name;
            txtIntro.Text = deal.DealContent.Intro;
            txtPriceDesc.Text = deal.DealContent.PriceDesc;
            chkHideContent.Checked = (deal.DealContent.IsHideContent ?? true);
            tTitle.Text = deal.DealContent.Title;
            tbP.Text = deal.ItemDetail.ItemPrice.ToString("N0");
            tbOP.Text = deal.ItemDetail.ItemOrigPrice.ToString("N0");
            tbOS.Text = deal.Deal.BusinessHourOrderTimeS.ToString("yyyy/MM/dd");
            tbOE.Text = deal.Deal.BusinessHourOrderTimeE.ToString("yyyy/MM/dd");
            dlOSh.SelectedValue = deal.Deal.BusinessHourOrderTimeS.Hour.ToString();
            dlOEh.SelectedValue = deal.Deal.BusinessHourOrderTimeE.Hour.ToString();
            dlOSm.SelectedValue = deal.Deal.BusinessHourOrderTimeS.Minute.ToString();
            dlOEm.SelectedValue = deal.Deal.BusinessHourOrderTimeE.Minute.ToString();
            tbOMin.Text = deal.Deal.BusinessHourOrderMinimum.ToString("N0");
            tbOMax.Text = deal.Deal.OrderTotalLimit == null ? "" : deal.Deal.OrderTotalLimit.Value.ToString("N0");
            tbMaxO.Text = deal.ItemDetail.MaxItemCount != null ? deal.ItemDetail.MaxItemCount.Value.ToString() : string.Empty;
            tbMaxP.Text = deal.ItemDetail.ItemDefaultDailyAmount != null ? deal.ItemDetail.ItemDefaultDailyAmount.Value.ToString() : string.Empty;
            tbAtm.Text = hidAtmQty.Value = deal.Deal.BusinessHourAtmMaximum.ToString();
            tbPdtl.Text = deal.DealContent.Restrictions;
            tbSlrInt.Text = deal.DealContent.Description;
            tbProSpc.Text = deal.DealContent.ProductSpec;
            string[] remarks = !string.IsNullOrEmpty(deal.DealContent.Remark) ? deal.DealContent.Remark.Split(new[] { "||" }, StringSplitOptions.None) : new string[3];
            rmkSub.Text = remarks[1];
            rmkTxt.Text = remarks[2];
            SetMutiTabInfo(remarks.Skip(3).ToArray());
            tbAppTitle.Text = deal.DealContent.AppTitle;
            tbUse.Text = deal.ItemDetail.ItemName;
            tbPdf.Text = deal.ItemDetail.PdfItemName ?? string.Empty;
            DeliverTimeS = deal.Deal.BusinessHourDeliverTimeS;
            DeliverTimeE = deal.Deal.BusinessHourDeliverTimeE;

            tbMenuD.Text = deal.DealContent.MenuD;
            tbMenuE.Text = deal.DealContent.MenuE;
            tbMenuF.Text = deal.DealContent.MenuF;
            tbMenuG.Text = deal.DealContent.MenuG;

            if (config.IsConsignment)
            {
                if (int.Parse(ddlRreceiveType.SelectedValue).Equals((int)DeliveryType.ToHouse))
                {
                    pProduct.Visible = true;
                    if (deal.Property.ShipType != null)
                    {
                        OrderShipType = (DealShipType)deal.Property.ShipType;
                        if (OrderShipType == DealShipType.Normal)
                        {
                            DealShippingDateType = (DealShippingDateType)deal.Property.ShippingdateType;
                            if (deal.Property.Shippingdate != null)
                            {
                                ShippingDate = (int)deal.Property.Shippingdate;
                            }
                            txtEarliestShipDate.Text = deal.Property.ProductUseDateStartSet.ToString();
                            txtLatestShipDate.Text = deal.Property.ProductUseDateEndSet.ToString();
                        }
                        else if (OrderShipType == DealShipType.Ship72Hrs)
                        {
                            rboFastShip1.Checked = true;
                        }
                    }

                    cbx_Consignment.Visible = true;

                }
                else
                {
                    cbx_Consignment.Visible = false;
                }
            }
            else
            {
                if (int.Parse(ddlRreceiveType.SelectedValue).Equals((int)DeliveryType.ToHouse))
                {
                    pProduct.Visible = true;
                    if (deal.Property.ShipType != null)
                    {
                        OrderShipType = (DealShipType)deal.Property.ShipType;
                        if (OrderShipType == DealShipType.Normal)
                        {
                            DealShippingDateType = (DealShippingDateType)deal.Property.ShippingdateType;
                            if (deal.Property.Shippingdate != null)
                            {
                                ShippingDate = (int)deal.Property.Shippingdate;
                            }
                            txtEarliestShipDate.Text = deal.Property.ProductUseDateStartSet.ToString();
                            txtLatestShipDate.Text = deal.Property.ProductUseDateEndSet.ToString();
                        }
                        else if (OrderShipType == DealShipType.Ship72Hrs)
                        {
                            rboFastShip1.Checked = true;
                        }
                    }
                }

            }

            if (!string.IsNullOrEmpty(deal.DealContent.Availability))
            {
                avR.DataSource = ProviderFactory.Instance().GetSerializer().Deserialize<AvailableInformation[]>(deal.DealContent.Availability);
                avR.DataBind();
            }
            string[] images = ImageFacade.GetMediaPathsFromRawData(deal.DealContent.ImagePath, MediaType.PponDealPhoto);
            ri.DataSource = images.Select((x, i) => new { ImgPath = x, Index = i }).Where(x => x.Index != 1 && x.Index != 2 && x.ImgPath != string.Empty);
            ri.DataBind();
            ri_2.DataSource = images.Where((x, i) => i == 1 && x != string.Empty);
            ri_2.DataBind();
            ri_3.DataSource = images.Where((x, i) => i == 2 && x != string.Empty);
            ri_3.DataBind();
            //特殊規格圖片的顯示
            string[] spImage = ImageFacade.GetMediaPathsFromRawData(deal.DealContent.SpecialImagePath, MediaType.PponDealPhoto);
            rpSpImg.DataSource = spImage.Select((x, i) => new { ImgPath = x, Index = i }).Where(x => x.ImgPath != string.Empty);
            rpSpImg.DataBind();
            //TravelEDMSpecialBigPic顯示
            string[] TravelEdmSpImage = ImageFacade.GetMediaPathsFromRawData(deal.DealContent.TravelEdmSpecialImagePath, MediaType.PponDealPhoto);
            rpTravelEDMSpImg.DataSource = TravelEdmSpImage.Select((x, i) => new { ImgPath = x, Index = i }).Where(x => x.ImgPath != string.Empty);
            rpTravelEDMSpImg.DataBind();

            //顯示 app deal pic
            string appDealPic = ImageFacade.GetMediaPathsFromRawData(
                deal.DealContent.AppDealPic, MediaType.PponDealPhoto).FirstOrDefault();
            hidAppDealPicOld.Value = appDealPic;
            hidAppDealPic.Value = deal.DealContent.AppDealPic;

            Freight = deal.Deal.BusinessHourDeliveryCharge;

            ChangedExpireDate = deal.Deal.ChangedExpireDate;
            SettlementTime = deal.Deal.SettlementTime;

            txtMult.Text = accessoryText;
            //有好康才可以進行複製，顯示複製按鈕
            btnCopyDeal.Visible = true;
            btnCopySubDeal.Visible =
                !Helper.IsFlagSet(deal.Deal.BusinessHourStatus, BusinessHourStatus.ComboDealSub);
            btnCopyComboDeals.Visible =
                Helper.IsFlagSet(deal.Deal.BusinessHourStatus, BusinessHourStatus.ComboDealMain);
            //有限期限截止日的權限
            bool canChangeExpireDate = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.SetExpireDate);
            formChangedExpireDate.Enabled = canChangeExpireDate;
            formChangedExpireDate.Attributes.Add("onmousedown", "window.alert('您將調整有效期限截止日！');$(this).focus();");

            divSyncPChome.Visible = isPChomeChannelProd;

            foreach (RepeaterItem item in repSellerTree.Items)
            {
                var cbVerify = (CheckBox)item.FindControl("cbVerify");
                var cbAccounting = (CheckBox)item.FindControl("cbAccounting");
                var cbViewBalanceSheet = (CheckBox)item.FindControl("cbViewBalanceSheet");
                var storeChangedExpireDate = (TextBox)item.FindControl("txtStoreChangedExpireDate");
                var txtStoreUseTime = (TextBox)item.FindControl("txtStoreUseTime");
                var hidSellerGuid = (HiddenField)item.FindControl("hidSellerGuid");
                var lbQuantity = (Label)item.FindControl("lbQuantity");
                var tbStoreCount = (TextBox)item.FindControl("txtStoreCount");
                var btnHideBalanceSheet = (System.Web.UI.WebControls.Image)item.FindControl("btnHideBalanceSheet");
                var hidIsHideBalanceSheet = (HiddenField)item.FindControl("hidIsHideBalanceSheet");
                var cbVerifyShop = (CheckBox)item.FindControl("cbVerifyShop");
                var hideVerifyShop = (CheckBox)item.FindControl("cbHideVerifyShop");

                storeChangedExpireDate.Enabled = storeChangedExpireDate.Enabled && canChangeExpireDate;
                btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock_disabled.png");
                hidIsHideBalanceSheet.Value = false.ToString();
                var findPponStore = pponStores.FirstOrDefault(x => x.StoreGuid.ToString() == hidSellerGuid.Value.Trim());
                if (findPponStore != null)
                {
                    cbVerify.Checked = cbVerify.Enabled && Helper.IsFlagSet((int)VbsRightFlag.Verify, findPponStore.VbsRight);
                    cbAccounting.Checked = cbAccounting.Enabled && Helper.IsFlagSet((int)VbsRightFlag.Accouting, findPponStore.VbsRight);
                    cbViewBalanceSheet.Checked = cbViewBalanceSheet.Enabled && Helper.IsFlagSet((int)VbsRightFlag.ViewBalanceSheet, findPponStore.VbsRight);
                    var isHideBalanceSheet = Helper.IsFlagSet((int)VbsRightFlag.BalanceSheetHideFromDealSeller, findPponStore.VbsRight);
                    hidIsHideBalanceSheet.Value = isHideBalanceSheet.ToString();
                    cbVerifyShop.Checked = cbVerifyShop.Enabled && Helper.IsFlagSet((int)VbsRightFlag.VerifyShop, findPponStore.VbsRight);
                    hideVerifyShop.Checked = hideVerifyShop.Enabled && Helper.IsFlagSet((int)VbsRightFlag.HideFromVerifyShop, findPponStore.VbsRight);
                    //檢視對帳單不可勾選 則顯示連鎖定對帳單功能也disable 檔次賣家為最大權限管理者 故也不須使用鎖定功能
                    if (cbViewBalanceSheet.Enabled && findPponStore.StoreGuid != deal.Deal.SellerGuid)
                    {
                        btnHideBalanceSheet.ImageUrl = isHideBalanceSheet
                                                        ? ResolveUrl("~/Themes/default/images/17Life/G2/lock.png")
                                                        : ResolveUrl("~/Themes/default/images/17Life/G2/unlock.png");
                    }

                    storeChangedExpireDate.Text = findPponStore.ChangedExpireDate.HasValue ? findPponStore.ChangedExpireDate.Value.ToString("yyyy/MM/dd") : string.Empty;
                    storeChangedExpireDate.Attributes.Add("onmousedown", "window.alert('您將調整有效期限截止日！');$(this).focus();");
                    txtStoreUseTime.Text = findPponStore.UseTime;
                    lbQuantity.Text = findPponStore.OrderedQuantity.HasValue ? findPponStore.OrderedQuantity.ToString() : "0";
                    tbStoreCount.Text = findPponStore.TotalQuantity == null ? "" : findPponStore.TotalQuantity.Value.ToString();
                }
            }

            //階梯式運費
            hdFCMax.Value = "0";
            hdFIMax.Value = "0";
            hdFreightCost.Value = "[]";
            hdFreightIncome.Value = "[]";
            hdMultiCost.Value = "[]";

            //seo
            tbTitle.Text = deal.Deal.PageTitle;
            tbDesc.Text = deal.Deal.PageDesc;
            tbAlt.Text = deal.Deal.PicAlt;
            lIP.Text = deal.ItemDetail.ItemPrice.ToString("F0");

            if (IsDealClosed && (deal.Deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
            {
                //結檔之後才能改資料
                ddlExpireRedirectDisplay.Enabled = true;
                txtExpireRedirectUrl.Enabled = true;
                cbUseExpireRedirectUrlAsCanonical.Enabled = true;

                ddlExpireRedirectDisplay.SelectedValue =
                (deal.Deal.ExpireRedirectDisplay ?? 1).ToString();
                txtExpireRedirectUrl.Text = deal.Deal.ExpireRedirectUrl;
                cbUseExpireRedirectUrlAsCanonical.Checked = (deal.Deal.UseExpireRedirectUrlAsCanonical ?? false);
            }
            else
            {
                ddlExpireRedirectDisplay.Enabled = false;
                txtExpireRedirectUrl.Enabled = false;
                cbUseExpireRedirectUrlAsCanonical.Enabled = false;
            }


            //開檔後無法修改付給店家或分店
            if (IsDealOpened)
            {
                //進貨價欄位開檔後只給有權限者修改
                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.SetCost))
                {
                    //tablePurchaseCost.Visible = true;
                    txtSlottingFeeQuantity.Enabled = true;
                    txtPurchasePrice.Enabled = true;
                    lbCostMessage.Visible = false;
                }
                else
                {
                    //tablePurchaseCost.Visible = false;
                    txtSlottingFeeQuantity.Enabled = false;
                    txtPurchasePrice.Enabled = false;
                    lbCostMessage.Visible = true;
                }

                //好康名稱和業務 開檔後鎖權限不能異動
                tbUse.Enabled = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.UpdateItemName);
                tbDevelopeSales.Enabled = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.UpdateSalesName);
                tbOperationSales.Enabled = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.UpdateSalesName);

                //開檔後僅允許財會異動買斷票券設定
                cbx_BuyoutCoupon.Enabled = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.SetBuyoutCoupon);

                //結帳倍數開檔後只允許有權限使用者異動
                tbCPC.Enabled = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.SetComboPackCount);

                //通用券檔次設定 開檔後 只允許有權限的人員調整設定 結檔後則不允許異動
                cbx_NoRestrictedStore.Enabled = !IsDealClosed && CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.SetIsRestrictedStore);
                if (!cbx_NoRestrictedStore.Enabled)
                    cbx_NoRestrictedStore.CssClass = "aspnet_checkbox";

                rbAchWeekly.Enabled = false;
                rbManualMonthly.Enabled = false;
                rbManualWeekly.Enabled = false;
                rbPartially.Enabled = false;
                rbOthers.Enabled = false;
                rbFlexible.Enabled = false;
                rbMonthly.Enabled = false;
                rbWeekly.Enabled = false;
                if (config.IsRemittanceFortnightly)
                    rbFortnightly.Enabled = false;

                rbBMBalanceSheetSystem.Enabled = false;
                rbBMMohistSystem.Enabled = false;

                //開檔後只允許有權限的人員調整設定 結檔後則不允許異動
                if (config.IsConsignment)
                    cbx_Consignment.Enabled = !IsDealClosed && CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.SetIsConsignment);


                //出貨方式
                rboNormal.Enabled = false;
                rboFast.Enabled = false;
                rboLastShipment.Enabled = false;
                rboLastDelivery.Enabled = false;
                rboUniShipDay.Enabled = false;
                rboNormalShipDay.Enabled = false;
                rboWms.Enabled = false;
                rboOther.Enabled = false;
                txtEarliestShipDate.Enabled = false;
                txtNormalShipDay.Enabled = false;
                rboFastShip1.Enabled = false;

                //活動遊戲檔
                cbx_Game.Enabled = false;
                //代銷贈品檔
                //cbx_ChannelGift.Enabled = false;


            }
            else
            {
                //tablePurchaseCost.Visible = true;
                txtSlottingFeeQuantity.Enabled = true;
                txtPurchasePrice.Enabled = true;
                lbCostMessage.Visible = false;
                tbUse.Enabled = true;
                //業務長度若大於8個字 則鎖權限不能異動
                tbDevelopeSales.Enabled = deal.Property.DealEmpName.Length <= 8 || CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.UpdateSalesName);
                tbOperationSales.Enabled = deal.Property.DealEmpName.Length <= 8 || CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.UpdateSalesName);

                if (config.IsConsignment)
                    cbx_Consignment.Enabled = true;
                /* 一併移到前台做處理，否則會有bug，對帳方式與出帳方式無法及時同步儲存
                if (int.Parse(ddlRreceiveType.SelectedValue).Equals((int)DeliveryType.ToShop))
                {
                    rbPartially.Enabled = false;
                    rbOthers.Enabled = true;

                    if (BillingModel == VendorBillingModel.BalanceSheetSystem)
                    {
                        rbAchWeekly.Enabled = true;
                        rbManualMonthly.Enabled = true;
                        rbManualWeekly.Enabled = true;
                        rbFlexible.Enabled = true;
                        rbMonthly.Enabled = true;
                        rbWeekly.Enabled = true;
                    }
                    else if (BillingModel == VendorBillingModel.MohistSystem)
                    {
                        rbAchWeekly.Enabled = false;
                        rbManualMonthly.Enabled = false;
                        rbManualWeekly.Enabled = false;
                        rbFlexible.Enabled = false;
                        rbMonthly.Enabled = false;
                        rbWeekly.Enabled = false;
                        rbPartially.Enabled = false;
                        rbWeekly.Enabled = false;
                        rbMonthly.Enabled = false;
                        rbFlexible.Enabled = false;
                    }
                    else
                    {
                        rbAchWeekly.Enabled = true;
                        rbManualMonthly.Enabled = true;
                        rbManualWeekly.Enabled = false;
                        rbFlexible.Enabled = false;
                        rbMonthly.Enabled = false;
                        rbWeekly.Enabled = false;
                    }
                }
                else
                {
                    // 非店面(宅配)如果是旅遊且有選墨攻時，一樣只能選其他付款(rbother)
                    if (BillingModel == VendorBillingModel.MohistSystem)
                    {
                        rbAchWeekly.Enabled = false;
                        rbManualMonthly.Enabled = false;
                        rbManualWeekly.Enabled = false;
                        rbFlexible.Enabled = false;
                        rbMonthly.Enabled = false;
                        rbWeekly.Enabled = false;
                        rbPartially.Enabled = false;
                        rbWeekly.Enabled = false;
                        rbMonthly.Enabled = false;
                        rbFlexible.Enabled = false;
                    }
                    else
                    {
                        rbAchWeekly.Enabled = false;
                        rbManualWeekly.Enabled = false;
                        rbManualMonthly.Enabled = false;
                        rbPartially.Enabled = true;
                        rbOthers.Enabled = true;
                        rbFlexible.Enabled = true;
                        rbMonthly.Enabled = true;
                        rbWeekly.Enabled = true;
                        lockFinanceSection(false);
                    }
                }
                */

            }

            cbx_PromotionDeal.Enabled = true;
            cbx_ExhibitionDeal.Enabled = true;

            //如果已結檔, 則必須有 "復原結檔" 的權限才可以修改 "結檔日期" (購買截止日)
            if (IsDealClosed)
            {
                //結檔後二個日曆天(含)，可由創意指定人員進行 (17P好康-好康設定(2天內復原結檔)
                if (DateTime.Now < deal.Deal.BusinessHourOrderTimeE.AddDays(2) && !CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.RecoverPponDealClose))
                {
                    this.tbOE.Enabled = this.dlOEh.Enabled = this.dlOEm.Enabled = false;
                }
                //結檔後第三個日曆天起，原權限從創意指定人員改為財務指定人員 (17P好康-好康設定(2天後復原結檔)
                else if (DateTime.Now > deal.Deal.BusinessHourOrderTimeE.AddDays(2) && !CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.RecoverPponDealClose3Day))
                {
                    this.tbOE.Enabled = this.dlOEh.Enabled = this.dlOEm.Enabled = false;
                }

                cbx_PromotionDeal.Enabled = false;
                cbx_ExhibitionDeal.Enabled = false;
            }

            //如果是修改檔次，網頁標題顯示好康劵名稱
            if (String.IsNullOrEmpty(deal.DealContent.CouponUsage) == false)
            {
                this.Page.Title = "後台-" + deal.DealContent.CouponUsage;
            }

            //後台檔次設定限制宅配檔次配送結束日已過系統時間無法異動，避免影響延遲出貨廠商罰款的問題
            if (TheDeliveryType == DeliveryType.ToHouse && !string.IsNullOrEmpty(tbDE.Text))
            {
                if (DateTime.Parse(tbDE.Text) <= DateTime.Now)
                {
                    //新的出帳模式：在結檔之後鎖定出貨日期
                    if (IsDealClosed && (rbWeekly.Checked || rbMonthly.Checked || rbFlexible.Checked))
                    {
                        tbDS.Enabled = false;
                        tbDE.Enabled = false;
                        dlDEh.Enabled = false;
                        dlDEm.Enabled = false;
                    }
                    else
                    {
                        tbDS.Enabled = true;
                        tbDE.Enabled = true;
                        dlDEh.Enabled = true;
                        dlDEm.Enabled = true;
                    }
                }
            }
            //if (TheDeliveryType == DeliveryType.ToHouse)
            //    tbOMax.Enabled = false;

            MuneUpdImage = cec.MenuUploadImagePath;
            MuneImage = cec.MenuImagePath;
        }

        private void SetMutiTabInfo(string[] remarks)
        {
            SetMutiTabText(remarks, txtMutiTabTitle1, txtMutiTab1, 0);
            SetMutiTabText(remarks, txtMutiTabTitle2, txtMutiTab2, 1);
            SetMutiTabText(remarks, txtMutiTabTitle3, txtMutiTab3, 2);
            SetMutiTabText(remarks, txtMutiTabTitle4, txtMutiTab4, 3);
            SetMutiTabText(remarks, txtMutiTabTitle5, txtMutiTab5, 4);
            SetMutiTabText(remarks, txtMutiTabTitle6, txtMutiTab6, 5);
        }

        private static void SetMutiTabText(string[] remarks, TextBox title, TextBox content, int index)
        {
            if (remarks.Length > index)
            {
                title.Text = remarks[index].Split(new[] { "##" }, StringSplitOptions.None)[0];
                content.Text = remarks[index].Split(new[] { "##" }, StringSplitOptions.None)[1];
            }
        }

        public void SetAccountingData(DealAccounting dAccounting, DealCostCollection dCost)
        {
            DevelopeSales = dAccounting.SalesId;
            OperationSales = dAccounting.OperationSalesId;
            if (dAccounting.DeptId != null)
            {
                DeptId = dAccounting.DeptId;
            }
            if (dAccounting.AccCity != null)
            {
                AccCityId = dAccounting.AccCity.Value;
            }

            //PayToCompany = (DealAccountingPayType)dAccounting.Paytocompany;
            IsInputTaxNotRequired = !(dAccounting.IsInputTaxRequired);

            PaidType = SellerPaidType.HasValue ? (RemittanceType)SellerPaidType.Value : (RemittanceType)dAccounting.RemittanceType;
            BillingModel = (VendorBillingModel)dAccounting.VendorBillingModel;
            ReceiptType = (VendorReceiptType)dAccounting.VendorReceiptType;
            AccountingMessage = dAccounting.Message;

            //發票對開
            IsMutualInvoice = dAccounting.Commission > 0;
            Commission = dAccounting.Commission > 0 ? dAccounting.Commission : 0;
            tbCommission.Enabled = IsMutualInvoice;
        }

        public void SetSMSContent(SmsContent sms)
        {
            if (sms.Content == null)
            {
                return;
            }

            string[] contents = sms.Content.Split('|');
            if (contents.Length >= 3)
            {
                //skip 0 , contents[0] should be _SMS_HEADER
                txtSMSContent.Text = contents[1] == tbUse.Text ? string.Empty : contents[1];
                hidSmsFooter.Value = contents[2];
            }
        }

        public void JumpTo(PponSetupMode mode, string argument)
        {
            switch (mode)
            {
                case PponSetupMode.DealNotFound:
                    Response.Redirect("~/error.aspx?action=deal-not-found");
                    break;

                case PponSetupMode.SellerNotFound:
                    Response.Redirect("../seller/seller_list.aspx");
                    break;

                case PponSetupMode.DealPage:
                    Response.Redirect("setup.aspx?bid=" + argument);
                    break;

                case PponSetupMode.ProposalUnCheck:
                    Response.Redirect("~/sal/BusinessContent.aspx?bid=" + argument);
                    break;

                default:
                    Response.Redirect("~/error.aspx");
                    break;
            }
        }

        public void SetDealStage(PponDealStage stage)
        {
            switch (stage)
            {
                case PponDealStage.NotExist:
                    bSend.Text = I18N.Phrase.AddNew;
                    btnImportBusinessOrderInfo.Visible = true;
                    break;

                case PponDealStage.Created:
                //之前建立的檔次會有這個狀態
                case PponDealStage.Ready:
                    bStart.Visible = bCG.Visible = false;
                    btnImportBusinessOrderInfo.Visible = true;
                    break;

                case PponDealStage.Running:
                case PponDealStage.RunningAndOn:
                case PponDealStage.RunningAndFull:
                    bStart.Visible = bCG.Visible = false;
                    divProposal.Visible = false;
                    break;

                case PponDealStage.ClosedAndOn:
                    bCG.Visible = bCG.Enabled = true;
                    bStart.Visible = false;
                    divProposal.Visible = false;
                    break;

                case PponDealStage.CouponGenerated:
                    bStart.Visible = bCG.Visible = false;
                    divProposal.Visible = false;
                    break;

                default:
                    break;
            }

            //判斷是否有執行手動結檔的權限
            btnDealClose.Visible = CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.ManualPponDealClose);
        }

        public void SetSaveConfirm(string message)
        {
            bSend.Attributes.Add("onclick ", "return confirm('" + message + "');");
        }

        public void SetSuccessorBid(DealPropertyCollection bids)
        {
            SuccessorBids.DataSource = bids;
            SuccessorBids.DataBind();

            if (int.Equals(0, bids.Count))
            {
                PanelSuccessor.Visible = false;
            }
        }

        public void ShowMessage(string msg)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + msg + "');", true);
        }

        public void CopyDealLink(Guid copydealguid)
        {
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "copyDealBlockUI", "showcopydeals(\'" + copydealguid.ToString() + "\')", true);
            Lit_copydeallink.Text += string.Format("<br /><a href='setup.aspx?bid={0}' target='_blank'>檔次已複製-{0}</a>", copydealguid.ToString());
        }

        public void SetAccField(AccBusinessGroupCollection accGroups, List<City> citys, DepartmentCollection depts)
        {
            ddlAccBusGroup.Items.Clear();
            ddlAccBusGroup.Items.Add(new ListItem("=請選擇館別=", "-1"));
            foreach (var group in accGroups)
            {
                ddlAccBusGroup.Items.Add(new ListItem(group.AccBusinessGroupName, group.AccBusinessGroupId.ToString()));
            }

            ddlAccCity.Items.Clear();
            ddlAccCity.Items.Add(new ListItem("=請選擇城市選項=", "-1"));
            foreach (var city in citys)
            {
                ddlAccCity.Items.Add(new ListItem(city.CityName, city.Id.ToString()));
            }

            ddlDept.Items.Clear();
            ddlDept.Items.Add(new ListItem("=請選擇單位選項=", "-1"));
            foreach (var dept in depts)
            {
                ddlDept.Items.Add(new ListItem(dept.DeptName, dept.DeptId));
            }
        }

        public void SetDealType(int parentId, int codeId, List<SystemCode> dealTypes)
        {
            #region 檔次母分類

            hdDealTypeNew.Value = codeId.ToString();

            #endregion
        }

        public void SetSellerField(SystemCodeCollection sellerVerifyType)
        {
            ddlVerifyType.Items.Clear();

            foreach (var code in sellerVerifyType)
            {
                if (code.IsDefault)
                {
                    ddlVerifyType.Items.Add(new ListItem(code.CodeName, code.CodeId.ToString()));
                    ddlVerifyType.SelectedValue = code.CodeId.ToString();
                }
                else
                {
                    ddlVerifyType.Items.Add(new ListItem(code.CodeName, code.CodeId.ToString()));
                }
            }
        }

        public void SetBusinessOrderInfo(BusinessOrder businessOrder, VacationCollection recentHolidays)
        {
            tbOS.Text = businessOrder.BusinessHourTimeS != null ? businessOrder.BusinessHourTimeS.Value.ToLocalTime().ToString("yyyy/MM/dd") : string.Empty;

            // 旅遊類
            if (businessOrder.IsTravelType)
            {
                if (string.IsNullOrWhiteSpace(tbSlrInt.Text))
                {
                    tbSlrInt.Text = string.Format("1. 民宿登記證字號:{0}<br />2. 所在地警察局電話:{1}", businessOrder.HotelId, businessOrder.PoliceTel);
                }
            }

            IsLongContract = businessOrder.IsLongContract;

            // 生活商圈
            this.SelectedCommercialCategoryId = businessOrder.CommercialCategory;

            int accBusinessGroupId = 0;
            int.TryParse(businessOrder.AccBusinessGroupId, out accBusinessGroupId);
            AccBusinessGroupNo = accBusinessGroupId;

            //App限定購買
            cbx_IsAppLimitedEdition.Checked = businessOrder.IsAppLimitedEdition;
            cbl_DefaultIcon.Items.FindByValue(Convert.ToString((int)DealLabelSystemCode.AppLimitedEdition)).Selected = businessOrder.IsAppLimitedEdition;

            //關鍵字
            tbAlt.Text = businessOrder.Keywords;

            switch (businessOrder.BusinessType)
            {
                case BusinessType.Ppon:
                    pnFreight.Visible = false;
                    ddlRreceiveType.SelectedValue = ((int)DeliveryType.ToShop).ToString();
                    CheckReceiveType();

                    List<int> pponSpecialCategoryIds = SelectedSpecialCategories;
                    //即買即用 Icon
                    if (businessOrder.PponVerifyType == PponVerifyType.OnlineUse)
                    {
                        if (!SelectedSpecialCategories.Contains(CategoryManager.CanBeUsedImmediately.Id))
                        {
                            pponSpecialCategoryIds.Add(CategoryManager.CanBeUsedImmediately.Id);
                        }
                    }
                    //前台有圖片的ICON區域
                    if (!string.IsNullOrWhiteSpace(businessOrder.LabelIconList))
                    {
                        List<int> labelIconList = new List<int>(businessOrder.LabelIconList.Split(',').Select(int.Parse));
                        foreach (var i in labelIconList)
                        {
                            int? cid = CategoryManager.GetCategoryIdByDealLabelSystemCode((DealLabelSystemCode)i);
                            if (cid.HasValue)
                            {
                                pponSpecialCategoryIds.Add(cid.Value);
                            }
                        }
                    }
                    SelectedSpecialCategories = pponSpecialCategoryIds;

                    SetCheckBoxList(cbl_couponDealTag, businessOrder.LabelTagList);
                    SetCheckBoxList(cbl_couponTravelDealTag, businessOrder.LabelTagList);

                    break;

                case BusinessType.Product:
                    pnFreight.Visible = true;
                    ddlRreceiveType.SelectedValue = ((int)DeliveryType.ToHouse).ToString();
                    CheckReceiveType();

                    #region 時程資訊

                    if (businessOrder.BusinessHourTimeS != null)
                    {
                        // 上檔日
                        DateTime businessOrderStart = businessOrder.BusinessHourTimeS.Value.ToLocalTime();

                        int orderTimeEndDays; // 結檔日=上檔日+指定天數
                        int endUseDays; // 結束使用=上檔日+指定天數

                        DateTime businessOrderEnd; // 結檔日
                        DateTime businessUseStart; // 開始使用
                        DateTime businessUseEnd; // 結束使用

                        if (businessOrder.ShipType == ShipType.ArriveIn24Hrs)
                        {
                            // 結檔日=上檔日+10
                            orderTimeEndDays = 10;
                            businessOrderEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, businessOrderStart, businessOrderStart.AddDays(orderTimeEndDays));

                            // 開始使用=上檔日
                            businessUseStart = businessOrderStart;

                            // 結束使用=結檔日+1
                            endUseDays = 1;
                            businessUseEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, businessOrderEnd, businessOrderEnd.AddDays(endUseDays));
                        }
                        else if (businessOrder.ShipType == ShipType.ArriveIn72Hrs)
                        {
                            // 結檔日=上檔日+21
                            orderTimeEndDays = 21;
                            businessOrderEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, businessOrderStart, businessOrderStart.AddDays(orderTimeEndDays));

                            // 開始使用=上檔日
                            businessUseStart = businessOrderStart;

                            // 結束使用=結檔日+1
                            endUseDays = 1;
                            businessUseEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, businessOrderEnd, businessOrderEnd.AddDays(endUseDays));
                        }
                        else
                        {
                            // 判斷是否為宅配旅遊
                            if (businessOrder.IsTravelType)
                            {
                                // 結檔日=上檔日+5
                                orderTimeEndDays = 5;
                                businessOrderEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, businessOrderStart, businessOrderStart.AddDays(orderTimeEndDays));

                                // 開始使用=結檔日+3
                                int startUseDays = 3;
                                businessUseStart = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, businessOrderEnd, businessOrderEnd.AddDays(startUseDays));

                                // 結束使用=開始使用+4
                                endUseDays = 4;
                                businessUseEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, businessUseStart, businessUseStart.AddDays(endUseDays));
                            }
                            else
                            {
                                // 結檔日=上檔日+5
                                orderTimeEndDays = 5;
                                businessOrderEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, businessOrderStart, businessOrderStart.AddDays(orderTimeEndDays));

                                if (businessOrder.ShippingDateType == ShippingDateType.Normal)
                                {
                                    // 開始使用 = 最早出貨日 = 上檔日+指定天數
                                    businessUseStart = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, businessOrderStart, businessOrderStart.AddDays(businessOrder.ProductUseDateStartSet));

                                    // 結束使用 = 最晚出貨日 = 結檔日+指定天數
                                    businessUseEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, businessOrderEnd, businessOrderEnd.AddDays(businessOrder.ProductUseDateEndSet));
                                }
                                else //訂單成立後X個工作天出貨完畢
                                {
                                    // 開始使用=上檔日
                                    businessUseStart = businessOrderStart;

                                    // 結束使用=結檔日+指定天數
                                    businessUseEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, businessOrderEnd, businessOrderEnd.AddDays(businessOrder.ShippingDate));
                                }
                            }
                        }

                        tbOE.Text = businessOrderEnd.ToString("yyyy/MM/dd");
                        DeliverTimeS = businessUseStart;
                        DeliverTimeE = businessUseEnd;
                    }

                    #endregion

                    switch (businessOrder.FreightType)
                    {
                        case "0":
                            freightCostPayTo.SelectedValue = CouponFreightPayTo.Supplier.ToString();
                            break;

                        case "1":
                            freightCostPayTo.SelectedValue = CouponFreightPayTo.Freighter.ToString();
                            break;

                        default:
                            break;
                    }

                    tbSlrInt.Text += businessOrder.IsParallelProduct ? "<br />●商品來源：平行輸入" : string.Empty;
                    tbSlrInt.Text += businessOrder.IsExpiringItems ? string.Format("<br />●此商品為即期品，保存期限：{0}～{1}", businessOrder.ExpiringItemsDateStart, businessOrder.ExpiringItemsDateEnd) : string.Empty;
                    tbSlrInt.Text += !string.IsNullOrEmpty(businessOrder.ProductPreservation) ? "<br />●製造日期/有效期限/保存方式：" + businessOrder.ProductPreservation : string.Empty;
                    tbSlrInt.Text += !string.IsNullOrEmpty(businessOrder.ProductUse) ? "<br />●功能 / 使用方法：" + businessOrder.ProductUse : string.Empty;
                    tbSlrInt.Text += !string.IsNullOrEmpty(businessOrder.ProductSpec) ? "<br />●規格(重量/淨重/內容量/數量/產地)：<br />" + businessOrder.ProductSpec.Replace("\n", "<br />") : string.Empty;
                    tbSlrInt.Text += !string.IsNullOrEmpty(businessOrder.ProductIngredients) ? "<br />●主要成份：" + businessOrder.ProductIngredients : string.Empty;
                    tbSlrInt.Text += !string.IsNullOrEmpty(businessOrder.SanctionNumber) ? "<br />●核準字號：" + businessOrder.SanctionNumber : string.Empty;
                    tbSlrInt.Text += "<br />●檢驗規定：" + (!string.IsNullOrEmpty(businessOrder.InspectRule) ? "有" : "無");

                    //宅配到貨方式
                    List<int> productSpecialCategoryIds = new List<int>();
                    switch (businessOrder.ShipType)
                    {

                        case ShipType.ArriveIn72Hrs:
                            productSpecialCategoryIds.Add(PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CategoryId);
                            break;
                        default:
                            //宅配商品目前沒有其他狀態
                            break;
                    }
                    //前台有圖片的ICON區域
                    if (!string.IsNullOrWhiteSpace(businessOrder.LabelIconList))
                    {
                        List<int> labelIconList = new List<int>(businessOrder.LabelIconList.Split(',').Select(int.Parse));
                        foreach (var i in labelIconList)
                        {
                            int? cid = CategoryManager.GetCategoryIdByDealLabelSystemCode((DealLabelSystemCode)i);
                            if (cid.HasValue)
                            {
                                productSpecialCategoryIds.Add(cid.Value);
                            }
                        }
                    }
                    SelectedSpecialCategories = productSpecialCategoryIds;

                    //宅配+旅遊 Tag
                    if (accBusinessGroupId == (int)LunchKingSite.Core.AccBusinessGroupNo.Travel)
                    {
                        SetCheckBoxList(cbl_deliveryTravelDealTag, businessOrder.LabelTagList);
                    }

                    if (businessOrder.ShipType == ShipType.ArriveIn24Hrs)
                    {
                        tbPdtl.Text += @"●24hr出貨說明<br />
                                        1.訂單成立後24小時出貨完畢，出貨後配送約2~3個工作日，恕無法指定送達時間。(限台灣本島配送)<br />
                                        2.週休假日或例假日成立之訂單，均視為次一工作天之訂單，預計於該工作天之隔天到貨。<br />
                                        3.預計到貨日若為週休假日或例假日，得順延至次一工作天到貨。<br />
                                        4.因收貨人資訊不完整或臨時更改、因收貨人自身因素致無法收貨、遇颱風地震等天災或公共工程施工等不可抗力之因素、或遇系統設備維護或盤點，可能造成出貨到貨延後之情形。於此情形將不在此到貨時間保證內。";
                    }
                    else if (businessOrder.ShipType == ShipType.ArriveIn72Hrs)
                    {
                        tbPdtl.Text += @"●24hr出貨說明<br />
                                        1.訂單成立後24小時出貨完畢，出貨後配送約2~3個工作日，恕無法指定送達時間。(限台灣本島配送)<br />
                                        2.週休假日或例假日成立之訂單，均視為次一工作天之訂單。<br />
                                        3.預計到貨日若為週休假日或例假日，得順延至次一工作天到貨。<br />
                                        4.因收貨人資訊不完整或臨時更改、因收貨人自身因素致無法收貨、遇颱風地震等天災或公共工程施工等不可抗力之因素、或遇系統設備維護或盤點，可能造成出貨到貨延後之情形。於此情形將不在此到貨時間保證內。<br />";
                    }

                    break;

                case BusinessType.None:
                default:
                    break;
            }


            //實體票券tag條件 = 宅配類+旅遊館別
            if (ddlRreceiveType.SelectedValue == ((int)DeliveryType.ToHouse).ToString() && accBusinessGroupId == (int)LunchKingSite.Core.AccBusinessGroupNo.Travel)
            {
                cbl_deliveryTravelDealTag.Items.FindByValue(Convert.ToString((int)DealLabelSystemCode.Tickets)).Selected = true;
            }

            #region 分類2 新版城市的選取

            if (businessOrder.SelectedChannelCategories.Count > 0)
            {
                SelectedChannelCategories = businessOrder.SelectedChannelCategories;
                SelectedChannelSpecialRegionCategories = businessOrder.SelectedChannelSpecialRegionCategories;
            }
            else
            {
                //處理城市的選取
                List<PponCity> pponCities = new List<PponCity>();
                foreach (PponCity pponCity in PponCityGroup.DefaultPponCityGroup)
                {
                    if (businessOrder.PponCity.Contains(pponCity.CityId.ToString()))
                    {
                        pponCities.Add(pponCity);
                    }
                }

                Dictionary<int, List<int>> selectedChannelCategories = new Dictionary<int, List<int>>();
                foreach (CategoryNode node in CategoryManager.PponChannelCategoryTree.CategoryNodes)
                {
                    //有子項目
                    List<CategoryNode> subList = node.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                    if (subList != null && subList.Count > 0)
                    {
                        //旅遊度假需另外處理
                        if (node.CategoryId == PponCityGroup.DefaultPponCityGroup.Travel.CategoryId)
                        {
                            if (businessOrder.PponCity.Split(',').Contains(PponCityGroup.DefaultPponCityGroup.Travel.CityId.ToString()))
                            {
                                //有勾選城市
                                List<int> areaList = new List<int>();
                                //檢查有無小區域的部分
                                foreach (Category c in ViewPponDealManager.DefaultManager.TravelCategories)
                                {
                                    if (businessOrder.TravelCategory.Contains(Convert.ToInt32(c.Id)))
                                    {
                                        Category newTravelCategory = CategoryManager.GetPponChannelAreaByTravel(c.Id);
                                        if (newTravelCategory != null)
                                        {
                                            areaList.Add(newTravelCategory.Id);
                                        }
                                    }
                                }
                                selectedChannelCategories.Add(node.CategoryId, areaList);
                            }
                        }
                        else if (node.CategoryId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CategoryId)
                        {
                            if (pponCities.Any(x => x.CategoryId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CategoryId))
                            {
                                selectedChannelCategories.Add(node.CategoryId, new List<int>());
                            }
                        }
                        else
                        {
                            //有子項目，判斷城市是否有勾選子項目的區塊，若有新增此區塊於新分類顯示
                            List<int> areaList = new List<int>();
                            foreach (var categoryNode in subList)
                            {
                                if (pponCities.Any(x => x.CategoryId == categoryNode.CategoryId))
                                {
                                    areaList.Add(categoryNode.CategoryId);
                                }
                            }
                            if (areaList.Count > 0)
                            {
                                selectedChannelCategories.Add(node.CategoryId, areaList);
                            }
                        }
                    }
                    else
                    {
                        //無子項目，直接判斷主區塊是否選取
                        if (pponCities.Any(x => x.CategoryId == node.CategoryId))
                        {
                            selectedChannelCategories.Add(node.CategoryId, new List<int>());
                        }
                    }
                }
                SelectedChannelCategories = selectedChannelCategories;
            }

            #endregion 分類2 新版城市的選取

            SelectedDealCategories = businessOrder.SelectedDealCategories;

            PaidType = SellerPaidType.HasValue ? (RemittanceType)SellerPaidType.Value : businessOrder.RemittancePaidType;

            //PayToCompany = businessOrder.PayToCompany;

            if (ddlDept.Items.FindByValue(businessOrder.AuthorizationGroup) != null)
            {
                ddlDept.SelectedValue = businessOrder.AuthorizationGroup;
            }

            if (businessOrder.ProvisionList.Values.Count > 0)
            {
                tbPdtl.Text += "<br />●" + string.Join("<br />●", businessOrder.ProvisionList.Values);
            }

            switch (businessOrder.ReturnType)
            {
                case ReturnType.NoRefund:
                    cbx_NoRefund.Checked = true;
                    tbPdtl.Text += "<br />●本憑證售出後即視同已使用，恕無法再向相關單位退費(17life、主辦單位)<br />若有退貨相關問題，請洽客服人員";
                    break;

                case ReturnType.DaysNoRefund:
                    chkDaysNoRefund.Checked = true;
                    tbPdtl.Text += "<br />●本憑證於結檔後7個日曆天(yyyy/mm/dd)後恕無法再向相關單位申請退費(17life)";
                    break;

                case ReturnType.ExpireNoRefund:
                    cbx_ExpireNoRefund.Checked = true;
                    tbPdtl.Text += "<br />●本憑證於展期時間內可向17Life申請退費，展期結束後恕不接受退費";
                    break;

                case ReturnType.NoRefundBeforeDays:
                    chkNoRefundBeforeDays.Checked = true;
                    tbPdtl.Text += "<br />●本憑證於演出時間前十日以前均可申請退費";
                    break;

                case ReturnType.Access:
                default:
                    break;
            }
            // SEO/關鍵字搜尋
            if (!string.IsNullOrWhiteSpace(businessOrder.Keywords))
            {
                if (!string.IsNullOrWhiteSpace(tbAlt.Text))
                {
                    // 檢查是否有重複的關鍵字
                    List<string> keywords = tbAlt.Text.Split("/").ToList();
                    foreach (string str in businessOrder.Keywords.Split('/'))
                    {
                        if (!keywords.Contains(str))
                        {
                            keywords.Add(str);
                        }
                    }

                    // 串接關鍵字內容，並檢查是否超過長度
                    if (keywords.Count > 0)
                    {
                        while (string.Join("/", keywords).Length > 20)
                        {
                            keywords.RemoveAt(keywords.Count - 1);
                        }
                        tbAlt.Text = string.Join("/", keywords);
                    }
                }
                else
                {
                    tbAlt.Text = businessOrder.Keywords;
                }
            }

            // 接續設定
            //cbx_Is_Continued_Sequence.Checked = businessOrder.IsContinuedSequence;
            //cbx_Is_Continued_Quantity.Checked = businessOrder.IsContinuedQuantity;
            // 接續設定
            if (businessOrder.AncestorQuantityBusinessHourGuid != Guid.Empty)
            {
                AncestorBid.Text = businessOrder.AncestorQuantityBusinessHourGuid.ToString();
            }
            if (businessOrder.AncestorSequenceBusinessHourGuid != Guid.Empty)
            {
                //AncestorSeqBid.Text = businessOrder.AncestorSequenceBusinessHourGuid.ToString();
            }
            //cbx_Is_Continued_Sequence.Checked = businessOrder.IsContinuedSequence;//序號
            cbx_Is_Continued_Quantity.Checked = businessOrder.IsContinuedQuantity;//數量


            // 店家開立單據方式
            switch (businessOrder.Invoice.Type)
            {
                case InvoiceType.General:
                    ReceiptType = VendorReceiptType.Invoice;
                    break;

                case InvoiceType.DuplicateUniformInvoice:
                    ReceiptType = VendorReceiptType.Invoice;
                    cbInputTax.Checked = cbx_NoTax.Checked = businessOrder.Invoice.InTax.Equals("0");
                    break;

                case InvoiceType.NonInvoice:
                    ReceiptType = VendorReceiptType.Receipt;
                    break;

                case InvoiceType.Other:
                    ReceiptType = VendorReceiptType.Other;
                    tbAccountingMessage.Text = businessOrder.Invoice.Name;
                    break;

                case InvoiceType.EntertainmentTax:
                default:
                    break;
            }

            if (!string.IsNullOrWhiteSpace(rmkTxt.Text))
            {
                rmkTxt.Text += "<br />";
            }

            rmkTxt.Text += "●" + businessOrder.Important1 + "<br />●" + businessOrder.Important2 + "<br />●" + businessOrder.Important3 + (!string.IsNullOrEmpty(businessOrder.Important4) ? "<br />●" + businessOrder.Important4 : string.Empty);

            // 串接分店
            if (businessOrder.BusinessType != BusinessType.Product)
            {
                foreach (RepeaterItem rep in repStore.Items)
                {
                    CheckBox cbStore = (CheckBox)rep.FindControl("cbStore");
                    Label lbStore = (Label)rep.FindControl("lbStore");
                    TextBox tbStoreCount = (TextBox)rep.FindControl("tbStoreCount");
                    HiddenField hidCompareStoreInfo = (HiddenField)rep.FindControl("hidCompareStoreInfo");
                    if (lbStore.Text.Split(':')[0].Equals(businessOrder.StoreName))
                    {
                        cbStore.Checked = true;
                    }
                    SalesStore store = businessOrder.Store.Where(x => (x.StoreID + x.StoreAccount + x.StoreTel) == hidCompareStoreInfo.Value).FirstOrDefault();
                    if (store != null)
                    {
                        cbStore.Checked = true;
                        tbStoreCount.Text = store.Quantity.ToString();
                    }
                }
            }
            cbx_DisableSMS.Checked = businessOrder.IsNotAccessSms;
            cbl_couponDealTag.Items.FindByValue(Convert.ToString((int)DealLabelSystemCode.OnlyPaperFormatCoupon)).Selected = businessOrder.IsNotAccessSms;

            if (businessOrder.Deals.Count > 0)
            {
                SetBusinesssOrderDeal(businessOrder, recentHolidays, 0);
            }
        }

        public void SetProposalInfo(Proposal pro, VacationCollection recentHolidays)
        {
            if (pro.OrderTimeS.HasValue)
            {
                #region 時程資訊

                DateTime orderStart = pro.OrderTimeS.Value; // 上檔日
                OrderTimeS = orderStart.AddMinutes(config.BusinessOrderTimeSMinute);
                if (pro.DeliveryType == (int)DeliveryType.ToShop)
                {
                    #region 憑證

                    if (pro.OrderTimeS != null)
                    {
                        DateTime orderEnd; // 結檔日
                        DateTime useStart; // 開始使用
                        DateTime useEnd; // 結束使用

                        int startUseValue = 0;
                        int.TryParse(pro.StartUseText, out startUseValue);
                        DateTime startUseDate;
                        DateTime.TryParse(pro.StartUseText, out startUseDate);
                        ProposalStartUseUnitType unit = (ProposalStartUseUnitType)pro.StartUseUnit;

                        if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.OnlineUse))
                        {
                            #region 即買即用

                            // 結檔日=上檔日+10天
                            orderEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderStart, orderStart.AddDays(10));

                            // 開始使用=上檔日
                            useStart = orderStart;

                            // 結束使用=開始使用+ {指定天數} or 指定日期
                            useEnd = GetUseTimeEnd(useStart, startUseValue, startUseDate, unit, recentHolidays);

                            #endregion
                        }
                        else
                        {
                            if (pro.DealType == (int)ProposalDealType.Travel)
                            {
                                #region 旅遊

                                // 結檔日=上檔日+45天
                                orderEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderStart, orderStart.AddDays(45));

                                // 開始使用=上檔日
                                useStart = orderStart;

                                // 結束使用=開始使用+ {指定天數} or 指定日期
                                useEnd = GetUseTimeEnd(useStart, startUseValue, startUseDate, unit, recentHolidays);

                                #endregion
                            }
                            else
                            {
                                #region 好康

                                // 開始使用=上檔日+3天
                                useStart = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderStart, orderStart.AddDays(3));

                                // 結束使用 = 開始使用 + 指定天數
                                useEnd = GetUseTimeEnd(useStart, startUseValue, startUseDate, unit, recentHolidays);

                                // 結檔日=結束使用前一週
                                orderEnd = useEnd.AddDays(-7);

                                #endregion
                            }
                        }

                        tbDE.Text = useEnd.ToString("yyyy/MM/dd");
                        OrderTimeE = orderEnd.AddMinutes(config.BusinessOrderTimeEMinute);
                        tbDS.Text = useStart.ToString("yyyy/MM/dd");
                        //DeliverTimeS = deal.Deal.BusinessHourDeliverTimeS.Value;
                        //DeliverTimeE = deal.Deal.BusinessHourDeliverTimeE.Value;
                    }

                    #endregion
                }
                else if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    #region 宅配

                    if (pro.OrderTimeS != null)
                    {
                        DateTime orderEnd; // 結檔日
                        DateTime useStart; // 開始使用
                        DateTime useEnd; // 結束使用

                        if (pro.ShipType == (int)DealShipType.Ship72Hrs)
                        {
                            #region 快速出貨

                            // 結檔日=上檔日+10
                            orderEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderStart, orderStart.AddDays(10));

                            // 開始使用=上檔日
                            useStart = orderStart;

                            // 結束使用=結檔日+1
                            useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderEnd, orderEnd.AddDays(1));

                            #endregion
                        }
                        else
                        {
                            #region 一般出貨

                            if (pro.DealType == (int)ProposalDealType.Travel)
                            {
                                #region 宅配旅遊

                                // 結檔日=上檔日+45
                                orderEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderStart, orderStart.AddDays(45));

                                // 開始使用=上檔日
                                useStart = orderStart;

                                // 結束使用=開始使用+4
                                useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, useStart, useStart.AddDays(4));

                                #endregion
                            }
                            else
                            {
                                #region 宅配

                                // 結檔日=上檔日+5
                                orderEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderStart, orderStart.AddDays(5));

                                // 開始使用=上檔日+指定天數
                                int useDateStartSet = int.TryParse(pro.ShipText1, out useDateStartSet) ? useDateStartSet : 0;
                                useStart = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderStart, orderStart.AddDays(useDateStartSet));

                                // 結束使用=結檔日+指定天數
                                int useDateEndSet = int.TryParse(pro.ShipText2, out useDateEndSet) ? useDateEndSet : 0;
                                useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderEnd, orderEnd.AddDays(useDateEndSet));

                                #endregion
                            }

                            #endregion
                        }

                        OrderTimeE = orderEnd.AddMinutes(config.BusinessOrderTimeEMinute);
                        tbDS.Text = useStart.ToString("yyyy/MM/dd");
                        tbDE.Text = useEnd.ToString("yyyy/MM/dd");
                    }



                    #endregion
                }


                #endregion
            }
        }

        private DateTime GetUseTimeEnd(DateTime useStart, int startUseValue, DateTime startUseDate, ProposalStartUseUnitType unit, VacationCollection recentHolidays)
        {
            switch (unit)
            {
                case ProposalStartUseUnitType.Day:
                    return OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, useStart, useStart.AddDays(startUseValue));
                case ProposalStartUseUnitType.Month:
                    return useStart.AddMonths(startUseValue);
                case ProposalStartUseUnitType.BeforeDay:
                    return startUseDate;
                default:
                    return DateTime.MinValue;
            }
        }

        public void SetBusinesssOrderDeal(BusinessOrder businessOrder, VacationCollection recentHolidays, int index)
        {
            switch (businessOrder.BusinessType)
            {
                case BusinessType.Ppon:

                    #region 時程資訊

                    if (businessOrder.BusinessHourTimeS != null)
                    {
                        // 上檔日
                        DateTime businessOrderStart = businessOrder.BusinessHourTimeS.Value.ToLocalTime();

                        int orderTimeEndDays; // 結檔日=上檔日+指定天數
                        int startUseDays; // 開始使用=結檔日+指定天數

                        DateTime businessOrderEnd; // 結檔日
                        DateTime businessUseStart; // 開始使用

                        if (businessOrder.PponVerifyType == PponVerifyType.OnlineUse)
                        {
                            // 結檔日=上檔日+10天
                            orderTimeEndDays = 10;
                            businessOrderEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, businessOrderStart, businessOrderStart.AddDays(orderTimeEndDays));

                            // 開始使用=上檔日
                            businessUseStart = businessOrderStart;
                        }
                        else
                        {
                            if (businessOrder.IsTravelType)
                            {
                                // 旅遊
                                orderTimeEndDays = 5; // 結檔日=上檔日+5天
                                startUseDays = 3; // 開始使用=結檔日+3天
                            }
                            else
                            {
                                // 好康
                                orderTimeEndDays = 4; // 結檔日=上檔日+4天
                                startUseDays = 5; // 開始使用=結檔日+5天
                            }
                            businessOrderEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, businessOrderStart, businessOrderStart.AddDays(orderTimeEndDays));
                            businessUseStart = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, businessOrderEnd, businessOrderEnd.AddDays(startUseDays));
                        }
                        tbOE.Text = businessOrderEnd.ToString("yyyy/MM/dd");
                        tbDS.Text = businessUseStart.ToString("yyyy/MM/dd");

                        // 結束使用=開始使用+指定天數
                        int startUseValue = 0;
                        int.TryParse(businessOrder.Deals[index].StartMonth, out startUseValue);
                        switch (businessOrder.Deals[index].StartUseUnit)
                        {
                            case StartUseUnitType.Day:
                                tbDE.Text = businessUseStart.AddDays(startUseValue).ToString("yyyy/MM/dd"); //截止使用
                                break;

                            case StartUseUnitType.Month:
                                tbDE.Text = businessUseStart.AddMonths(startUseValue).ToString("yyyy/MM/dd"); //截止使用
                                break;

                            case StartUseUnitType.BeforeDay:
                                DateTime startUseDate;
                                DateTime.TryParse(businessOrder.Deals[index].StartMonth, out startUseDate);
                                tbDE.Text = startUseDate.ToString("yyyy/MM/dd");
                                break;

                            default:
                                break;
                        }
                    }

                    #endregion

                    #region 檔次設定

                    IsReserveLock = businessOrder.Deals[index].IsReserveLock;
                    BookingSystemType = businessOrder.Deals[index].BookingSystemType;
                    AdvanceReservationDays = businessOrder.Deals[index].AdvanceReservationDays;
                    CouponUsers = businessOrder.Deals[index].CouponUsers;

                    #endregion

                    break;

                case BusinessType.Product:

                    #region 檔次設定

                    hidBusinessOrderFreight.Value = businessOrder.Deals[index].Freight;
                    hidBusinessOrderNonFreightLimit.Value = businessOrder.Deals[index].NonFreightLimit;
                    //cbx_NoShippingFeeMessage.Checked = businessOrder.Deals[index].IsFreightToCollect;
                    //cbl_deliveryDealTag.Items.FindByValue(Convert.ToString((int)DealLabelSystemCode.PayOnDelivery)).Selected = businessOrder.Deals[index].IsFreightToCollect; 
                    cbx_NotDeliveryIslands.Checked = businessOrder.Deals[index].IsNotDeliveryIslands;
                    cbl_deliveryDealTag.Items.FindByValue(Convert.ToString((int)DealLabelSystemCode.NAOutlyingIslands)).Selected = businessOrder.Deals[index].IsNotDeliveryIslands;

                    #endregion

                    #region 訂單出貨狀態
                    pProduct.Visible = true;
                    if (businessOrder.ShipType == ShipType.Normal) //一般出貨
                    {
                        rboNormal.Checked = true;
                        rboFast.Checked = false;

                        if (businessOrder.ShippingDateType == ShippingDateType.Special)
                        {
                            rboNormalShipDay.Checked = true;
                            rboUniShipDay.Checked = false;
                            txtNormalShipDay.Text = businessOrder.ShippingDate.ToString();
                            txtEarliestShipDate.Text = "";
                            txtLatestShipDate.Text = "";

                        }
                        else if (businessOrder.ShippingDateType == ShippingDateType.Normal)
                        {
                            rboUniShipDay.Checked = true;
                            rboNormalShipDay.Checked = false;
                            txtNormalShipDay.Text = "";
                            txtEarliestShipDate.Text = businessOrder.ProductUseDateStartSet.ToString();
                            txtLatestShipDate.Text = businessOrder.ProductUseDateEndSet.ToString();
                        }
                    }
                    else //非一般出貨即為快速出貨
                    {
                        rboFast.Checked = true;
                        rboNormal.Checked = false;
                        rboFastShip1.Checked = true;
                        rboUniShipDay.Checked = false;
                        rboNormalShipDay.Checked = false;
                        txtNormalShipDay.Text = "";
                        txtEarliestShipDate.Text = "";
                        txtLatestShipDate.Text = "";
                    }

                    #endregion
                    break;

                case BusinessType.None:
                default:
                    break;
            }

            IsLongContract = businessOrder.IsLongContract;

            #region 優惠活動

            tbN.Text = businessOrder.Deals[index].ItemName;

            #endregion

            #region 檔次設定

            IsMutualInvoice = !businessOrder.Deals[index].Commission.Equals(0);
            Commission = businessOrder.Deals[index].Commission;
            tbOP.Text = businessOrder.Deals[index].OrignPrice;
            tbP.Text = businessOrder.Deals[index].ItemPrice;
            txtSlottingFeeQuantity.Text = businessOrder.Deals[index].SlottingFeeQuantity;
            txtPurchasePrice.Text = businessOrder.Deals[index].PurchasePrice;
            cbx_BuyoutCoupon.Checked = businessOrder.Deals[index].BuyoutCoupon;
            tbOMin.Text = businessOrder.Deals[index].MinQuentity;
            tbOMax.Text = businessOrder.Deals[index].MaxQuentity;
            tbMaxP.Text = businessOrder.Deals[index].LimitPerQuentity;
            tbAlt.Text = businessOrder.Keywords;

            #endregion

            #region ATM

            if (businessOrder.Deals[index].IsATM)
            {
                tbAtm.Text = hidAtmQty.Value = businessOrder.Deals[index].AtmQuantity;
            }
            else
            {
                tbAtm.Text = "0";
            }

            #endregion

            #region 多重選項

            SetMutiOptionInfo(businessOrder.Deals[index].MultOption);

            #endregion

        }

        private void SetMutiOptionInfo(string multiOptionInfo)
        {
            if (!string.IsNullOrEmpty(multiOptionInfo))
            {
                txtMult.Text = multiOptionInfo;
            }
        }

        /// <summary>
        /// 購物車設定
        /// </summary>
        public void SetShoppingCartOption(List<KeyValuePair<string, string>> listitem, string selectValue)
        {
            if (!config.ShoppingCartV2Enabled)
            {
                return;
            }

            rbl_ShoppingCartOptions.DataTextField = "Key";
            rbl_ShoppingCartOptions.DataValueField = "Value";
            rbl_ShoppingCartOptions.DataSource = listitem;
            rbl_ShoppingCartOptions.DataBind();

            if (rbl_ShoppingCartOptions.Items.FindByValue(selectValue) != null)
            {
                rbl_ShoppingCartOptions.Items.FindByValue(selectValue).Selected = true;
            }
        }
        #endregion IPponSetupView Members

        #region page events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Preparation();
                phCopyComboDeals.Visible = false;
                if (!_presenter.OnViewInitialized())
                {
                    return;
                }
                PponContentGet();
            }

            if (config.EnableGrossMarginRestrictions)
            {
                pCbxMinGrossMargin.Visible = plbGrossMargin.Visible = true;
                grossMarginMessage.Text = "毛利率" + config.GrossMargin + "%以下(含)，無法使用折價券，欲開啟請洽相關人員";
                //var GrossMarginValtmp = PponFacade.GetMinimumGrossMarginFromBids(BusinessHourId, true) * 100;
                var GrossMarginValtmp = PponFacade.GetDealMinimumGrossMargin(BusinessHourId) * 100;
                hidMinGrossMarginVal.Value = GrossMarginValtmp.ToString("f2");
            }

            #region 確認是否有"可設定使用折價券"權限，連動 "低毛利率可使用折價券"、"本檔次不可使用折價券" checkbox
            cbx_LowGrossMarginAllowedDiscount.Enabled = CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.UserDiscount);
            cbx_NotAllowedDiscount.Enabled = CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.UserDiscount);
            #endregion

            hdCumulativeQuantity.Value = hdCumulativeQuantity.Value == "" ? "0" : hdCumulativeQuantity.Value;

            _presenter.OnViewLoaded();
            if (SellerId != Guid.Empty)
            {
                hlStoreAdd.NavigateUrl = string.Format("../seller/seller_add.aspx?sid={0}&&thin=1", SellerId);
                hdSellerGuid.Value = SellerId.ToString();
            }

            //由於會使用到SellerGuid...但新增與修改的塞值時間點不一...故用事件放置在此
            if (!Page.IsPostBack && SetShoppingCart != null) SetShoppingCart(this, new EventArgs());

            if (int.Parse(ddlRreceiveType.SelectedValue).Equals((int)DeliveryType.ToShop))
            {
                tbFt.Enabled = false;
                tbFt.Text = string.Empty;
                pnFreight.Enabled = false;
                pnFreight.Visible = false;
                AncestorSeqBid.Visible = true;  //接續憑證BID
                lit_AncestorSeqBid.Visible = true;
                cbx_Is_Continued_Sequence.Visible = true;
                delivery_Init(true);
            }
            else
            {
                tbFt.Enabled = true;
                pnFreight.Enabled = true;
                pnFreight.Visible = true;
                AncestorSeqBid.Visible = false;  //接續憑證BID
                AncestorSeqBid.Text = "";
                cbx_Is_Continued_Sequence.Checked = false;
                cbx_Is_Continued_Sequence.Visible = false;
                lit_AncestorSeqBid.Visible = false;
                delivery_Init(false);
                if (BusinessHourId == Guid.Empty)
                {
                    cbx_NotDeliveryIslands.Checked = true;
                    cbl_deliveryDealTag.Items.FindByValue(Convert.ToString((int)DealLabelSystemCode.NAOutlyingIslands)).Selected = true;
                }
                else
                {
                    cbl_deliveryDealTag.Items.FindByValue(Convert.ToString((int)DealLabelSystemCode.NAOutlyingIslands)).Selected = NotDeliveryIslands;
                    //cbl_deliveryDealTag.Items.FindByValue(Convert.ToString((int)DealLabelSystemCode.PayOnDelivery)).Selected = NoShippingFeeMessage;
                    cbl_couponDealTag.Items.FindByValue(Convert.ToString((int)DealLabelSystemCode.OnlyPaperFormatCoupon)).Selected = DisableSMS;
                }
                rbAchWeekly.Checked = false;
            }
            if (!string.IsNullOrEmpty(hdSkmDealType.Value))
            {
                foreach (ListItem item in cblSkmDealType.Items)
                {
                    if (hdSkmDealType.Value.Trim('[', ']').Split(",").Contains(item.Value))
                    {
                        item.Selected = true;
                    }
                    item.Enabled = false;
                }
            }
            hdBid.Value = Request.QueryString["bid"];

            pnMainShoppingCartSetting.Visible = config.ShoppingCartV2Enabled; //購物車設定顯示                    
            pnProSpc.Visible = config.ShoppingCartV2Enabled;


        }
        /// <summary>
        /// 設定『更多權益說明』初始值
        /// 消費方式:憑證消費 時 新增兌換日期
        /// 消費方式:宅配 時 不帶初始值
        /// 當異動過裡面資料
        /// 消費方式無論何種都不再次異動
        /// </summary>
        /// <param name="isReceipt"></param>
        private void delivery_Init(bool isReceipt)
        {
            if (SellerId != Guid.Empty)
            {
                string initReceiptWord = "";
                if (isReceipt && tbPdtl.Text.IndexOf("優惠期間") < 0)
                {
                    initReceiptWord = "<span style=color:#f00;><span style=font-size: 14px;>優惠期間</span></span><br />●日期：<span id=initDeliveryTimeStart>START_DATE</span> 至 <span id=initDeliveryTimeEnd>END_DATE</span>&nbsp;<span style=color: rgb(128, 0, 128);>&nbsp;</span><br />";
                }
                else
                {
                    initReceiptWord = "";
                }

                string sMsg = HttpUtility.HtmlDecode(tbPdtl.Text).Replace("\"", "");
                tbPdtl.Text = initReceiptWord + sMsg.Replace("<span style=color:#f00;><span style=font-size:>優惠期間</span></span><br />\r\n●日期：<span id=initDeliveryTimeStart>START_DATE</span> 至 <span id=initDeliveryTimeEnd>END_DATE</span>&nbsp;<span style=color:>&nbsp;</span><br />\r\n", "");
            }
        }

        protected void bSend_Click(object sender, EventArgs e)
        {
            // 新增簡訊與好康選項內容是否包含 { 與 }
            if (Enum.Equals(DeliveryType.ToShop, TheDeliveryType) && !cbx_DisableSMS.Checked && (!string.IsNullOrWhiteSpace(txtSMSContent.Text) || !string.IsNullOrWhiteSpace(txtMult.Text)))
            {
                if (txtMult.Text.IndexOf('{') >= 0 || txtMult.Text.IndexOf('}') >= 0 || txtSMSContent.Text.IndexOf('{') >= 0 || txtSMSContent.Text.IndexOf('}') >= 0)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(tTitle, typeof(ImageButton), "alert", "alert('多重選項與簡訊內容請勿包含 { 與 }');", true);
                    return;
                }
            }

            if (cbx_TmallDeal.Checked)
            {
                string errormessage = "天貓檔次";
                bool check_tmall = true;
                int price;                
                if (ddlRreceiveType.SelectedValue == "2")
                {
                    check_tmall = false;
                    errormessage += "消費方式應為憑證消費;";
                }
                if (cbx_ShoppingCar.Checked)
                {
                    check_tmall = false;
                    errormessage += "不可使用多重選項功能;";
                }
                if (int.TryParse(tbP.Text, out price) && price != 0)
                {
                    check_tmall = false;
                    errormessage += "賣價固定為0;";
                }
                if (TmallRmbExchangeRate == 0 || TmallRmbPrice == 0)
                {
                    check_tmall = false;
                    errormessage += "請填入人民幣賣價和兌換匯率;";
                }
                if (!check_tmall)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(tTitle, typeof(ImageButton), "alert", "alert('" + errormessage + "');", true);
                    return;
                }
            }

            if (cbx_FamiDeal.Checked && FamiDealType != (int)CouponCodeType.FamiItemCode && ExchangePrice == 0)
            {
                System.Web.UI.ScriptManager.RegisterStartupScript(tTitle, typeof(ImageButton), "alert", "alert('此檔次為全家檔次，請填寫全家兌換價');", true);
                return;
            }

            if (!string.IsNullOrEmpty(txtExpireRedirectUrl.Text) && !CheckExpireRedirectUrlFormat(txtExpireRedirectUrl.Text) )
            {
                System.Web.UI.ScriptManager.RegisterStartupScript(tTitle, typeof(ImageButton), "alert", "alert('結檔後，轉檔網址格式不符');", true);
                return;
            }

            if (SaveDeal != null)
            {
                var json = ProviderFactory.Instance().GetSerializer();
                var comboData = json.Deserialize<ComboData>(hidComboData.Value);
                int cropX;
                Int32.TryParse(hidAppDealPicX.Value, out cropX);
                PponSetupViewModel argsModel = new PponSetupViewModel(
                    GetDealFromInterface(),
                    fuI.PostedFile.ContentLength > 0 ? fuI.PostedFile : null,
                    txtMult.Text.Trim(),
                    GetSelectedPponStore(),
                    comboData,
                    SelectImageUploadType,
                    new ImageData { AppDealPic = hidAppDealPic.Value, CropX = cropX },
                    string.IsNullOrEmpty(rbl_ShoppingCartOptions.SelectedValue) ? null : (int?)Convert.ToInt32(rbl_ShoppingCartOptions.SelectedValue));
                SaveDeal(this, new DataEventArgs<PponSetupViewModel>(argsModel));
            }
            ScriptManager.RegisterStartupScript(tTitle, typeof(ImageButton), "unblockUI", "$.unblockUI();", true);
            var GrossMargin = PponFacade.GetMinimumGrossMarginFromBids(BusinessHourId, true) * 100;
            MinGrossMarginVal = GrossMargin;

        }

        private bool CheckExpireRedirectUrlFormat(string url)
        {
            List<string> correctUrl = new List<string>()
            {
                config.SSLSiteUrl + "/event/brandevent",
                config.SSLSiteUrl + "/deal/",
                config.SSLSiteUrl + "/ppon/pponsearch.aspx?search",
            };

            return correctUrl.Any(x => url.StartsWith(x));
        }

        //store
        protected List<PponSetupPponStore> GetSelectedPponStore()
        {
            var jobj = (JArray)JsonConvert.DeserializeObject(hdStoreSellected.Value);
            var pponStores = jobj.Select(x =>
            {
                var locationCheck = bool.Parse(((JValue)((JObject)x)["LocationCheck"]).ToString());
                var verifyCheck = bool.Parse(((JValue)((JObject)x)["VerifyCheck"]).ToString());
                var accountingCheck = bool.Parse(((JValue)((JObject)x)["AccountingCheck"]).ToString());
                var viewBalanceSheetCheck = bool.Parse(((JValue)((JObject)x)["ViewBalanceSheetCheck"]).ToString());
                var hideBalanceSheet = bool.Parse(((JValue)((JObject)x)["HideBalanceSheet"]).ToString());
                var verifyShopCheck = bool.Parse(((JValue)((JObject)x)["VerifyShopCheck"]).ToString());
                var hideVerifyShopCheck = bool.Parse(((JValue)((JObject)x)["HideVerifyShopCheck"]).ToString());
                var vbsRight = 0;

                if (locationCheck)
                {
                    vbsRight = (int)Helper.SetFlag(true, vbsRight, VbsRightFlag.Location);
                }
                if (verifyCheck)
                {
                    vbsRight = (int)Helper.SetFlag(true, vbsRight, VbsRightFlag.Verify);
                }
                if (accountingCheck)
                {
                    vbsRight = (int)Helper.SetFlag(true, vbsRight, VbsRightFlag.Accouting);
                }
                if (viewBalanceSheetCheck)
                {
                    vbsRight = (int)Helper.SetFlag(true, vbsRight, VbsRightFlag.ViewBalanceSheet);
                    vbsRight = hideBalanceSheet
                                ? (int)Helper.SetFlag(true, vbsRight, VbsRightFlag.BalanceSheetHideFromDealSeller)
                                : vbsRight;
                }
                if (verifyShopCheck)
                {
                    vbsRight = (int)Helper.SetFlag(true, vbsRight, VbsRightFlag.VerifyShop);
                    vbsRight = hideVerifyShopCheck
                                ? (int)Helper.SetFlag(true, vbsRight, VbsRightFlag.HideFromVerifyShop)
                                : vbsRight;
                }

                DateTime? expireDt = null;
                DateTime dt;
                if (DateTime.TryParse(((JValue)((JObject)x)["ChangedExpireTime"]).ToString(), out dt))
                {
                    expireDt = dt;
                }

                return new PponSetupPponStore
                {
                    StoreGuid = new Guid(((JValue)((JObject)x)["StoreGuid"]).ToString()),
                    SortOrder = (int?)((JValue)((JObject)x)["SortOrder"]),
                    ChangedExpireTime = expireDt,
                    UseTime = ((JValue)((JObject)x)["UseTime"]).ToString(),
                    TotalQuantity = string.IsNullOrEmpty(((JValue)((JObject)x)["TotalQuantity"]).ToString()) ? null : (int?)((JValue)((JObject)x)["TotalQuantity"]),
                    VbsRight = vbsRight
                };
            }).ToList();

            return pponStores;
        }

        [Obsolete]
        protected void bStart_Click(object sender, EventArgs e)
        {
            if (SaveDeal != null) // 判斷 SaveDeal ???
            {
                CreateOrder(this, null);
            }
        }

        protected void bCG_Click(object sender, EventArgs e)
        {
            if (SaveDeal != null)
            {
                GenerateCoupon(this, null);
            }
        }

        protected void bDD_Click(object sender, EventArgs e)
        {
            if (DeleteDeal != null)
            {
                DeleteDeal(this, null);
            }
        }

        protected void btnSetSolt_Click(object sender, EventArgs e)
        {
            if (CreateDealTimeSlot != null)
            {
                CreateDealTimeSlot(this, null);
            }
        }
        /// <summary>
        /// 好康檔次暫存資料(ViewPponDealManage)更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnViewPponDeal_Click(object sender, EventArgs e)
        {
            //DateTime StartDate = DateTime.Parse(tbOS.Text.Trim());
            //ViewPponDealCollection vpdc = pp.ViewPponDealGetListByDayWithNoLock(StartDate.AddDays(-1), StartDate.AddDays(1));
            //ViewPponDealManager.PponDeal = vpdc;
            //ViewPponDealManager.ReloadDefaultManager(true);
        }

        protected void btnImportBusinessOrderInfo_Click(object sender, EventArgs e)
        {
            if (ImportBusinessOrderInfo != null)
            {
                int businessHourUniqueId = int.TryParse(lblBusinessHourUniqueId.Text, out businessHourUniqueId) ? businessHourUniqueId : 0;
                ImportBusinessOrderInfo(this, new DataEventArgs<KeyValuePair<string, int>>(new KeyValuePair<string, int>(txtBusinessOrderId.Text.Trim(), businessHourUniqueId)));
            }
        }

        protected void btnProposalImport_Click(object sender, EventArgs e)
        {
            if (ProposalImport != null)
            {
                ProposalImport(this, e);
            }
        }

        protected void btnDealClose_Click(object sender, EventArgs e)
        {
            ManualDealClose(this, new DataEventArgs<Guid>(BusinessHourId));
            ScriptManager.RegisterStartupScript(tTitle, typeof(ImageButton), "unblockUI", "$.unblockUI();", true);
        }

        protected void btnSyncPChome1_Click(object sender, EventArgs e)
        {
            SyncPChome1(this, new DataEventArgs<Guid>(BusinessHourId));
            ScriptManager.RegisterStartupScript(tTitle, typeof(ImageButton), "unblockUI", "$.unblockUI();", true);
        }

        protected void btnSyncPChome2_Click(object sender, EventArgs e)
        {
            SyncPChome2(this, new DataEventArgs<Guid>(BusinessHourId));
            ScriptManager.RegisterStartupScript(tTitle, typeof(ImageButton), "unblockUI", "$.unblockUI();", true);
        }

        protected void btnCopyDeal_Click(object sender, EventArgs e)
        {
            if (CopyDeal != null)
            {
                CopyDeal(this, new ComboDealEventArgs { IsCopyAsSubDeal = false, IsCopyComboDeals = false });
            }
        }

        protected void btnCopySubDeal_Click(object sender, EventArgs e)
        {
            if (CopyDeal != null)
            {
                CopyDeal(this, new ComboDealEventArgs { IsCopyAsSubDeal = true, IsCopyComboDeals = false });
            }
        }

        protected void bBBH_Click(object sender, EventArgs e)
        {
            SetBBH(this, e);
        }

        protected void btnCopyComboDeals_Click(object sender, EventArgs e)
        {
            if (CopyDeal != null)
            {
                CopyDeal(this, new ComboDealEventArgs { IsCopyAsSubDeal = false, IsCopyComboDeals = true });
            }
        }

        protected void rptCommercialCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<Category, List<Category>>)
            {
                KeyValuePair<Category, List<Category>> dataItem = (KeyValuePair<Category, List<Category>>)e.Item.DataItem;

                HiddenField hidCommercialCategoryId = (HiddenField)e.Item.FindControl("hidCommercialCategoryId");
                CheckBox chkCommercialArea = (CheckBox)e.Item.FindControl("chkCommercialArea");
                CheckBoxList chklCommercialCategory = (CheckBoxList)e.Item.FindControl("chklCommercialCategory");

                hidCommercialCategoryId.Value = dataItem.Key.Id.ToString();
                chkCommercialArea.Text = dataItem.Key.Name;
                chklCommercialCategory.DataSource = dataItem.Value;
                chklCommercialCategory.DataValueField = "Id";
                chklCommercialCategory.DataTextField = "Name";
                chklCommercialCategory.DataBind();
            }
        }

        protected void RepStoreItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (!(e.Item.DataItem is Seller)) return;

            var data = (Seller)e.Item.DataItem;
            var tbStoreGuid = (TextBox)e.Item.FindControl("tbStoreGuid");
            var tbStoreStatus = (TextBox)e.Item.FindControl("tbStoreStatus");
            var lbStore = (Label)e.Item.FindControl("lbStore");
            var hidCompareStoreInfo = (HiddenField)e.Item.FindControl("hidCompareStoreInfo");
            var lbStoreAddress = (Label)e.Item.FindControl("lbStoreAddress");
            lbStore.Text = string.Format("{0} {1}", data.SellerId, data.SellerName);
            tbStoreGuid.Text = data.Guid.ToString();
            tbStoreStatus.Text = data.StoreStatus.ToString();
            hidCompareStoreInfo.Value = data.CompanyID + data.CompanyAccount + data.StoreTel;
            lbStoreAddress.Text = CityManager.CityTownShopStringGet(data.StoreTownshipId == null ? -1 : data.StoreTownshipId.Value) + data.StoreAddress;
        }

        protected void RepFreightIncomeDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (!(e.Item.DataItem is CouponFreight)) return;

            var data = (CouponFreight)e.Item.DataItem;
            var labFAmt = (Label)e.Item.FindControl("labFAmt");
            var labStartAmt = (Label)e.Item.FindControl("labStartAmt");
            labFAmt.Text = data.FreightAmount.ToString();
            labStartAmt.Text = data.StartAmount.ToString();
        }

        protected void SuccessorBids_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is DealProperty)
            {
                HyperLink bidLink = (HyperLink)e.Item.FindControl("successorBid");
                bidLink.NavigateUrl = "?bid=" + e.Item.DataItem.GetPropertyValue("BusinessHourGuid").ToString();
                bidLink.Text = e.Item.DataItem.GetPropertyValue("BusinessHourGuid").ToString();
            }
        }

        protected void ddlRreceiveType_TextChanged(object sender, EventArgs e)
        {
            CheckReceiveType();
            if (RreceiveType_TextChanged != null)
            {
                RreceiveType_TextChanged(this, e);
            }
        }

        protected void channelCheckArea_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode node = (CategoryNode)e.Item.DataItem;
                CheckBoxList subChannelCheck = (CheckBoxList)e.Item.FindControl("subChannelCheck");
                HiddenField channelId = (HiddenField)e.Item.FindControl("channelCheckId");
                Repeater subChannelCheckArea = (Repeater)e.Item.FindControl("subChannelCheckArea");

                channelId.Value = node.CategoryId.ToString();
                List<CategoryTypeNode> areaCategories = node.NodeDatas.Where(x => x.NodeType == CategoryType.PponChannelArea).ToList();
                if (areaCategories.Count > 0)
                {
                    CategoryTypeNode areaCategory = areaCategories.First();
                    subChannelCheckArea.DataSource = areaCategory.CategoryNodes;
                    subChannelCheckArea.DataBind();

                }
                Repeater subChannelSPRegionCheckArea = (Repeater)e.Item.FindControl("subChannelSPRegionCheckArea");
                if (areaCategories.Count > 0)
                {
                    CategoryTypeNode areaCategory = areaCategories.First();
                    subChannelSPRegionCheckArea.DataSource = areaCategory.CategoryNodes;
                    subChannelSPRegionCheckArea.DataBind();
                }
            }
        }

        protected void subChannelSPRegionCheckArea_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode node = (CategoryNode)e.Item.DataItem;
                Repeater subChannelSpecialRegionCheckArea = (Repeater)e.Item.FindControl("subChannelSpecialRegionCheckArea");

                Literal llSpecialRegion = (Literal)e.Item.FindControl("llSpecialRegion");

                List<CategoryNode> specialRegionCategories = node.GetSubCategoryTypeNode(CategoryType.PponChannelArea);

                if ((specialRegionCategories.Any(x => x.CategoryName.Contains("商圈‧景點"))))
                {
                    var fatherCategoryNode = specialRegionCategories.Where(x => x.CategoryName.Contains("商圈‧景點")).FirstOrDefault();
                    subChannelSpecialRegionCheckArea.DataSource = fatherCategoryNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                    subChannelSpecialRegionCheckArea.DataBind();

                    //商圈景點子區域
                    if ((specialRegionCategories.Any(x => x.Parent.Parent.Parent.Parent.CategoryName == "旅遊")))
                    {
                        if (fatherCategoryNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea).Any())
                        {
                            Repeater subSPRegionCheckArea = (Repeater)e.Item.FindControl("subSPRegionCheckArea");
                            subSPRegionCheckArea.DataSource = fatherCategoryNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                            subSPRegionCheckArea.DataBind();
                        }
                    }
                }

                if ((specialRegionCategories.Any(x => x.Parent.Parent.Parent.Parent.CategoryName == "新光三越")))
                {
                    var fatherCategoryNode = specialRegionCategories.Where(x => x.Parent.Parent.Parent.Parent.CategoryName == "新光三越").First();
                    subChannelSpecialRegionCheckArea.DataSource = fatherCategoryNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea);

                    subChannelSpecialRegionCheckArea.DataBind();
                }

                //List<CategoryTypeNode> standardRegionCategories = node.NodeDatas.Where(x => x.NodeType == CategoryType.StandardRegion).ToList();

                //if (standardRegionCategories.Count > 0)
                //{
                //    CategoryTypeNode standardRegion = standardRegionCategories.First();

                //    List<CategoryNode> allsubStandardRegion = new List<CategoryNode>();
                //    foreach (var categoryNode in standardRegion.CategoryNodes)
                //    {
                //       allsubStandardRegion.AddRange(categoryNode.NodeDatas.Where(x => x.NodeType == CategoryType.StandardRegion).First().CategoryNodes);
                //    }

                //}

                //llSpecialRegion.Text =(specialRegionCategories.Count > 0 && standardRegionCategories.Count > 0)?"<br />":null;
            }
        }
        protected void subSPRegionCheckArea_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode node = (CategoryNode)e.Item.DataItem;
                Repeater subSpecialRegionCheckArea = (Repeater)e.Item.FindControl("subSpecialRegionCheckArea");
                List<CategoryNode> specialRegionCategories = node.GetSubCategoryTypeNode(CategoryType.PponChannelArea);

                if (specialRegionCategories.Any())
                {
                    subSpecialRegionCheckArea.DataSource = specialRegionCategories;
                    subSpecialRegionCheckArea.DataBind();
                }
            }
        }

        protected void dealCategoryArea_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is Category)
            {
                Category category = (Category)e.Item.DataItem;
                Repeater subDealCategoryArea = (Repeater)e.Item.FindControl("subDealCategoryArea");

                List<ViewCategoryDependency> subCategoryCol =
                    PponFacade.ViewCategoryDependencyGetByParentId(category.Id, this.AllViewCategoryDependencies);

                List<Category> subDealCategories = new List<Category>();

                foreach (var subCategory in subCategoryCol.Where(x => x.CategoryStatus == 1))
                {
                    //Category subnode = PponFacade.SubDealCategoryGet(subCategory.CategoryId);
                    Category subnode = ViewPponDealManager.DefaultManager.GetCategory(subCategory.CategoryId);

                    if (subnode.IsLoaded)
                    {
                        subDealCategories.Add(subnode);
                    }
                }

                if (subDealCategories.Count > 0)
                {
                    subDealCategoryArea.DataSource = subDealCategories;
                    subDealCategoryArea.DataBind();
                }

            }
        }

        protected void rptProposalFlag_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<KeyValuePair<Enum, bool>, ProposalStatus>)
            {
                KeyValuePair<KeyValuePair<Enum, bool>, ProposalStatus> dataItem = (KeyValuePair<KeyValuePair<Enum, bool>, ProposalStatus>)e.Item.DataItem;
                Button btnFlag = (Button)e.Item.FindControl("btnFlag");
                KeyValuePair<Type, string> flagType = SellerFacade.GetProposalFlagsTypeByStatus(dataItem.Value);
                if (flagType.Key != null)
                {
                    bool enabled = false;

                    // 檢核狀態權限檢查
                    SystemFunctionType funcType = SystemFunctionType.All;
                    if (SystemFunctionType.TryParse(dataItem.Key.Key.ToString(), out funcType) && funcType != SystemFunctionType.All)
                    {
                        //編輯檢核要有被指派且是自己
                        ViewEmployee emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, UserName);
                        ProposalEditorFlag flag = (ProposalEditorFlag)Enum.Parse(typeof(ProposalEditorFlag), funcType.ToString());
                        ProposalAssignLog palc = ProposalFacade.ProposalAssignLog(ProposalId).Where(x => x.AssignFlag == (int)flag && x.AssignEmail == emp.EmpName).FirstOrDefault();
                        if (palc != null)
                            enabled = true;
                    }
                    btnFlag.OnClientClick = enabled ? string.Empty : "alert('您無此權限。'); return false;";

                    string text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, dataItem.Key.Key);
                    btnFlag.Text = text;
                    btnFlag.CommandArgument = Convert.ToInt32(dataItem.Key.Key).ToString() + "|" + flagType.Value + "|" + text + "|" + dataItem.Key.Value.ToString();
                    btnFlag.Visible = !dataItem.Key.Value;
                }
            }
        }

        protected void rptProposalFlag_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (this.CheckFlag != null)
            {
                this.CheckFlag(this, new DataEventArgs<string>(e.CommandArgument.ToString()));
            }
        }

        protected void RepFreightCostDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (!(e.Item.DataItem is CouponFreight)) return;

            var data = (CouponFreight)e.Item.DataItem;
            var labFAmt = (Label)e.Item.FindControl("labFAmt");
            var labStartAmt = (Label)e.Item.FindControl("labStartAmt");
            labFAmt.Text = data.FreightAmount.ToString();
            labStartAmt.Text = data.StartAmount.ToString();
        }

        protected void btnDeleteCDNImages_Click(object sender, EventArgs e)
        {
            if (DeleteCDNImages != null)
            {
                DeleteCDNImages(this, null);
            }
        }

        protected void RepSellerTreeItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (!(e.Item.DataItem is SellerNode)) return;

            var data = (SellerNode)e.Item.DataItem;
            var hidSellerGuid = (HiddenField)e.Item.FindControl("hidSellerGuid");
            var lbSellerName = (Label)e.Item.FindControl("lbSellerName");
            var cbVerify = (CheckBox)e.Item.FindControl("cbVerify");
            var cbAccounting = (CheckBox)e.Item.FindControl("cbAccounting");
            var cbViewBalanceSheet = (CheckBox)e.Item.FindControl("cbViewBalanceSheet");
            var txtStoreCount = (TextBox)e.Item.FindControl("txtStoreCount");
            var txtStoreChangedExpireDate = (TextBox)e.Item.FindControl("txtStoreChangedExpireDate");
            var txtStoreUseTime = (TextBox)e.Item.FindControl("txtStoreUseTime");
            var btnHideBalanceSheet = (System.Web.UI.WebControls.Image)e.Item.FindControl("btnHideBalanceSheet");
            var hidIsHideBalanceSheet = (HiddenField)e.Item.FindControl("hidIsHideBalanceSheet");
            var cbVerifyShop = (CheckBox)e.Item.FindControl("cbVerifyShop");
            var hideVerifyShop = (CheckBox)e.Item.FindControl("cbHideVerifyShop");
            lbSellerName.Text = data.Text;
            hidSellerGuid.Value = data.Id.ToString();
            hidIsHideBalanceSheet.Value = false.ToString();
            cbVerify.Enabled = cbAccounting.Enabled = cbViewBalanceSheet.Enabled =
            txtStoreCount.Enabled = txtStoreChangedExpireDate.Enabled = txtStoreUseTime.Enabled =
            cbVerifyShop.Enabled = hideVerifyShop.Enabled = data.State == TreeState.Open;
            if (cbViewBalanceSheet.Enabled && hidSellerGuid.Value != SellerId.ToString())
            {
                btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock.png");
            }
            else
            {
                btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock_disabled.png");
            }
        }

        #endregion page events

        #region page Props
        /// <summary>
        /// 選取的上傳圖片用途，無法判別回傳PponSetupImageUploadType.None
        /// </summary>
        private PponSetupImageUploadType SelectImageUploadType
        {
            get
            {
                string rdo = Request.Form["rdo_img"];
                PponSetupImageUploadType uploadType = PponSetupImageUploadType.None;
                if (Enum.TryParse(rdo, out uploadType))
                {
                    return uploadType;
                }
                else
                {
                    return PponSetupImageUploadType.None;
                }
            }
        }
        #endregion page Props

        #region private methods

        private void SetCheckBoxList(CheckBoxList checkboxlist, string checkItems)
        {
            if (!string.IsNullOrEmpty(checkItems))
            {
                foreach (string codeId in checkItems.Split(','))
                {
                    if (checkboxlist.Items.FindByValue(codeId) != null)
                    {
                        checkboxlist.Items.FindByValue(codeId).Selected = true;
                    }
                }
            }
        }

        private PponDeal GetDealFromInterface()
        {
            PponDeal ret;
            if (BusinessHourId == Guid.Empty)
            {
                ret = new PponDeal(true);
            }
            else
            {
                ret = new PponDeal();
            }
            //天貓檔次新增標示
            if (cbx_TmallDeal.Checked)
            {
                tbUse.Text = tbUse.Text.StartsWith("[天貓]") ? tbUse.Text : "[天貓]" + tbUse.Text;
                tbN.Text = tbN.Text.StartsWith("[天貓]") ? tbN.Text : "[天貓]" + tbN.Text;
            }
            ret.Deal.BusinessHourOnline = true;
            ret.DealContent.Name = tbN.Text.Trim().TrimToMaxLength(DealContentNameLength);
            ret.DealContent.Intro = txtIntro.Text.Trim();   //引言
            ret.DealContent.PriceDesc = txtPriceDesc.Text.Trim();   //價格描述
            ret.DealContent.IsHideContent = chkHideContent.Checked; //前台不顯示內文
            ret.DealContent.IsCloseMenu = chkCloseMenu.Checked; //關閉MENU功能
            ret.DealContent.IsMenuLogo = chkMenuLogo.Checked; //自動壓LOGO
            ret.DealContent.MenuD = HttpUtility.HtmlDecode(tbMenuD.Text);
            ret.DealContent.MenuE = HttpUtility.HtmlDecode(tbMenuE.Text);
            ret.DealContent.MenuF = HttpUtility.HtmlDecode(tbMenuF.Text);
            ret.DealContent.MenuG = HttpUtility.HtmlDecode(tbMenuG.Text);
            ret.DealContent.Title = tTitle.Text.Trim().TrimToMaxLength(50);
            ret.ItemDetail.ItemPrice = decimal.Parse(tbP.Text.Trim());
            ret.ItemDetail.ItemOrigPrice = decimal.Parse(tbOP.Text.Trim());
            SetMultiCost();
            ret.Deal.BusinessHourOrderTimeS = DateTime.Parse(tbOS.Text.Trim()).AddHours(int.Parse(dlOSh.SelectedValue)).AddMinutes(int.Parse(dlOSm.SelectedValue));
            ret.Deal.BusinessHourOrderTimeE = DateTime.Parse(tbOE.Text.Trim()).AddHours(int.Parse(dlOEh.SelectedValue)).AddMinutes(int.Parse(dlOEm.SelectedValue));
            ret.Deal.BusinessHourOrderMinimum = decimal.Parse(tbOMin.Text.Trim());
            string t = tbOMax.Text.Trim();
            ret.Deal.OrderTotalLimit = string.IsNullOrEmpty(t) ? (decimal?)99999 : decimal.Parse(t);
            int maxItemCount = int.TryParse(tbMaxP.Text.Trim(), out maxItemCount) ? maxItemCount : 20;
            ret.ItemDetail.ItemDefaultDailyAmount = ret.ItemDetail.MaxItemCount = maxItemCount;

            string restrictions = HttpUtility.HtmlDecode(tbPdtl.Text.Trim()).Replace("\n","");
            bool onlySpaceOrBreak = string.IsNullOrWhiteSpace(restrictions.Replace("<br />", "").Replace("<br >", "").Replace("<br>", ""));
            if (onlySpaceOrBreak)
            {
                ret.DealContent.Introduction = ret.DealContent.Restrictions = tbPdtl.Text = "";
            }
            else
            {
                ret.DealContent.Introduction = ret.DealContent.Restrictions = tbPdtl.Text = restrictions;
            }

            ret.DealContent.Description = tbSlrInt.Text = HttpUtility.HtmlDecode(tbSlrInt.Text.Trim());
            ret.DealContent.AppDescription = ViewPponDealManager.RemoveSpecialHtmlForApp(ret.DealContent.Description);
            ret.DealContent.ProductSpec = HttpUtility.HtmlDecode(tbProSpc.Text.Trim());
            ret.DealContent.Remark = (0 + "||" + HttpUtility.HtmlDecode(rmkSub.Text.Trim()) + "||" + HttpUtility.HtmlDecode(rmkTxt.Text.Trim()) + "||" + GetMutiTabInfo("||"));
            rmkSub.Text = HttpUtility.HtmlDecode(rmkSub.Text.Trim());
            rmkTxt.Text = HttpUtility.HtmlDecode(rmkTxt.Text.Trim());//解決問題：過去曾有無法接續後，回到原頁面，其文字編輯產生亂碼的情況
            ret.ItemDetail.ItemName = tbUse.Text.Trim();
            ret.ItemDetail.PdfItemName = tbPdf.Text.Trim();
            ret.DealContent.CouponUsage = tbUse.Text.Trim();
            ret.DealContent.AppTitle = tbAppTitle.Text.Trim();
            ret.Deal.BusinessHourDeliverTimeS = DeliverTimeS;
            ret.Deal.BusinessHourDeliverTimeE = DeliverTimeE;
            ret.Deal.ChangedExpireDate = DateTime.MinValue; //dummy to force into dirty column
            ret.Deal.ChangedExpireDate = this.ChangedExpireDate;
            ret.Deal.SettlementTime = this.SettlementTime;
            ret.DealContent.ImagePath = Request.Form["is"] != null ? Request.Form["is"] : string.Empty; // the data is the order of images
            ret.DealContent.Availability = Request.Form["hav"]; // TODO: take care issues of truncated json
            ret.DealContent.SpecialImagePath = Request.Form["spIs"] ?? string.Empty; // the data is the order of special images
            ret.DealContent.TravelEdmSpecialImagePath = Request.Form["trspIs"] ?? string.Empty;
            ret.Deal.BusinessHourDeliveryCharge = Freight;

            int atm = int.TryParse(tbAtm.Text.Trim(), out atm) ? atm : 0;
            ret.Deal.BusinessHourAtmMaximum = atm;

            //seo
            ret.Deal.PageTitle = tbTitle.Text.Trim();
            ret.Deal.PageDesc = tbDesc.Text.Trim();
            ret.Deal.PicAlt = tbAlt.Text.Trim();
            ret.Deal.ExpireRedirectDisplay = string.IsNullOrEmpty(ddlExpireRedirectDisplay.SelectedValue)
                ? 0
                : int.Parse(ddlExpireRedirectDisplay.SelectedValue);
            ret.Deal.ExpireRedirectUrl = txtExpireRedirectUrl.Text.Trim();
            ret.Deal.UseExpireRedirectUrlAsCanonical = cbUseExpireRedirectUrlAsCanonical.Checked;
            if (!string.IsNullOrEmpty(txtExpireRedirectUrl.Text.Trim()))
            {
                ret.Deal.ExpireRedirectChangeDate = DateTime.Now;
                ret.Deal.ExpireRedirectManual = true;
            }
            else
            {
                ret.Deal.ExpireRedirectChangeDate = null;
                ret.Deal.ExpireRedirectManual = false;
            }


            return ret;
        }

        private string GetMutiTabInfo(string joinstring)
        {
            List<string> dataList = new List<string>();
            dataList.Add(txtMutiTabTitle1.Text + "##" + HttpUtility.HtmlDecode(txtMutiTab1.Text.Trim()));
            dataList.Add(txtMutiTabTitle2.Text + "##" + HttpUtility.HtmlDecode(txtMutiTab2.Text.Trim()));
            dataList.Add(txtMutiTabTitle3.Text + "##" + HttpUtility.HtmlDecode(txtMutiTab3.Text.Trim()));
            dataList.Add(txtMutiTabTitle4.Text + "##" + HttpUtility.HtmlDecode(txtMutiTab4.Text.Trim()));
            dataList.Add(txtMutiTabTitle5.Text + "##" + HttpUtility.HtmlDecode(txtMutiTab5.Text.Trim()));
            dataList.Add(txtMutiTabTitle6.Text + "##" + HttpUtility.HtmlDecode(txtMutiTab6.Text.Trim()));
            return string.Join(joinstring, dataList);
        }

        private void lockFinanceSection(bool isEnable)
        {
            //子檔不允許更改財務相關資料
            if (int.Parse(ddlRreceiveType.SelectedValue).Equals((int)DeliveryType.ToHouse) && isSub)
            {
                //匯款資料：
                //rbPayToCompany.Enabled = isEnable;
                //rbPayToStore.Enabled = isEnable;
                //對帳方式：
                rbBMNone.Enabled = isEnable;
                rbBMBalanceSheetSystem.Enabled = isEnable;
                rbBMMohistSystem.Enabled = isEnable;
                //出帳方式：
                rbAchWeekly.Enabled = isEnable;
                rbManualMonthly.Enabled = isEnable;
                rbManualWeekly.Enabled = isEnable;
                rbPartially.Enabled = isEnable;
                rbOthers.Enabled = isEnable;
                rbFlexible.Enabled = isEnable;
                rbMonthly.Enabled = isEnable;
                rbWeekly.Enabled = isEnable;
                if (config.IsRemittanceFortnightly)
                    rbFortnightly.Enabled = isEnable;
                //店家單據開立方式：
                rbRecordInvoice.Enabled = isEnable;
                rbRecordReceipt.Enabled = isEnable;
                rbRecordOthers.Enabled = isEnable;
                //免稅設定：
                cbInputTax.Enabled = isEnable;
                cbx_NoTax.Enabled = isEnable;
            }
        }

        //活動搶購時間
        private void Preparation()
        {
            for (int i = 0; i < 24; i++)
            {
                dlOSh.Items.Add(new ListItem(i.ToString("00"), i.ToString()));
                dlOEh.Items.Add(new ListItem(i.ToString("00"), i.ToString()));
                dlDSh.Items.Add(new ListItem(i.ToString("00"), i.ToString()));
                dlDEh.Items.Add(new ListItem(i.ToString("00"), i.ToString()));
                dlEDh.Items.Add(new ListItem(i.ToString("00"), i.ToString()));
                dlSTMh.Items.Add(new ListItem(i.ToString("00"), i.ToString()));
            }
            dlDEh.SelectedValue = "23";

            for (int i = 0; i < 60; i += 5)
            {
                dlOSm.Items.Add(new ListItem(i.ToString("00"), i.ToString()));
                dlOEm.Items.Add(new ListItem(i.ToString("00"), i.ToString()));
                dlDSm.Items.Add(new ListItem(i.ToString("00"), i.ToString()));
                dlDEm.Items.Add(new ListItem(i.ToString("00"), i.ToString()));
                dlEDm.Items.Add(new ListItem(i.ToString("00"), i.ToString()));
                dlSTMm.Items.Add(new ListItem(i.ToString("00"), i.ToString()));
            }

            #region add 59 minute

            string tempMinute = "59";
            dlOSm.Items.Add(new ListItem(tempMinute, tempMinute));
            dlOEm.Items.Add(new ListItem(tempMinute, tempMinute));
            dlDSm.Items.Add(new ListItem(tempMinute, tempMinute));
            dlDEm.Items.Add(new ListItem(tempMinute, tempMinute));
            dlEDm.Items.Add(new ListItem(tempMinute, tempMinute));
            dlSTMm.Items.Add(new ListItem(tempMinute, tempMinute));
            dlDEm.SelectedValue = tempMinute;

            #endregion

            freightCostPayTo.Items.Add(new ListItem("", ""));
            foreach (CouponFreightPayTo pt in Enum.GetValues(typeof(CouponFreightPayTo)))
            {
                freightCostPayTo.Items.Add(new ListItem(Helper.GetLocalizedEnum(pt), pt.ToString()));
            }

            rblDiscountType.Items.Add(new ListItem
            {
                Selected = true,
                Text = "優惠價格 <input id='txtDiscountPrice' type='text' tabindex='1' class='number' Style='display: inline; width: 60px' />元",
                Value = ((int)DiscountType.DiscountPrice).ToString()
            });
            rblDiscountType.Items.Add(new ListItem
            {
                Selected = true,
                Text = "折抵現金 <input id='txtDiscountCash' type='text' tabindex='2' class='number' Style='display: inline; width: 60px' />元",
                Value = ((int)DiscountType.DiscountCash).ToString()
            });
            rblDiscountType.Items.Add(new ListItem
            {
                Selected = true,
                Text = "打折優惠 <input id='txtDiscountPercent' type='text' tabindex='3' class='number' Style='display: inline; width: 60px' />折",
                Value = ((int)DiscountType.DiscountPercent).ToString()
            });
            rblDiscountType.SelectedIndex = 0;
            foreach (SkmDealTypes type in Enum.GetValues(typeof(SkmDealTypes)))
            {
                cblSkmDealType.Items.Add(new ListItem(Helper.GetLocalizedEnum(type), ((int)type).ToString()));
            }
            ddlGroupCouponType.Items.Add(new ListItem("=請選擇=", ((int)GroupCouponDealType.None).ToString()));
            foreach (GroupCouponDealType type in Enum.GetValues(typeof(GroupCouponDealType)))
            {
                if (!string.IsNullOrEmpty(Helper.GetDescription(type)))
                {
                    ddlGroupCouponType.Items.Add(new ListItem(Helper.GetDescription(type), ((int)type).ToString()));
                    if (!config.EnabledGroupCouponTypeB && type == GroupCouponDealType.CostAssign)
                    {
                        ddlGroupCouponType.Items[ddlGroupCouponType.Items.Count - 1].Attributes.Add("disabled", "disabled");
                    }
                }
            }
        }

        private void PponContentGet()
        {
            if (BusinessHourId != Guid.Empty)
            {
                int _RreceiveType = 0;
                int _tbOP = 0;
                int _tbP = 0;
                int.TryParse(ddlRreceiveType.SelectedValue, out _RreceiveType);
                int.TryParse(decimal.Parse(tbOP.Text.Trim()).ToString(), out _tbOP);
                int.TryParse(decimal.Parse(tbP.Text.Trim()).ToString(), out _tbP);

                //好像跟前端產生重工了??
                //lblContent.Text = PponFacade.PponContentGet(BusinessHourId, _RreceiveType, txtIntro.Text, txtPriceDesc.Text,
                //    tbUse.Text, _tbOP, _tbP, chkIsAveragePrice.Checked,
                //    cbl_deliveryDealTag.Items.FindByValue(Convert.ToString((int)DealLabelSystemCode.IncludedDeliveryCharge)).Selected);
            }
        }

        private List<Category> GetCategoryGetListWithoutSubDealCategory(List<Category> categories)
        {
            List<int> subCategorylist = PponFacade.ViewCategoryDependencyGetByParentType(CategoryType.DealCategory);
            return categories.Where(x => !subCategorylist.Contains(x.Id)).ToList();
        }

        private void SetMultiCost()
        {
            List<PponSetupViewMultiCost> list = new List<PponSetupViewMultiCost>();
            decimal purchasePrice = decimal.Parse(txtPurchasePrice.Text);
            int slottingFeeQuantity = string.IsNullOrEmpty(txtSlottingFeeQuantity.Text.Trim()) ? 0 : int.Parse(txtSlottingFeeQuantity.Text.Trim());
            list.Add(new PponSetupViewMultiCost { });
            if (slottingFeeQuantity > 0)
                list.Add(new PponSetupViewMultiCost { });

            list.ForEach(i =>
            {
                i.Cost = (i != list.FirstOrDefault()) ? purchasePrice : (slottingFeeQuantity > 0) ? 0 : purchasePrice;
                i.Quantity = (i != list.FirstOrDefault()) ? 99999 - slottingFeeQuantity : (slottingFeeQuantity > 0) ? slottingFeeQuantity : 99999;
                i.Cumulative_quantity = (i != list.FirstOrDefault()) ? 99999 : (slottingFeeQuantity > 0) ? slottingFeeQuantity : 99999;
                i.Lower_cumulative_quantity = (i != list.FirstOrDefault()) ? slottingFeeQuantity : 0;
            });
            hdMultiCost.Value = new JsonSerializer().Serialize(list);
        }
        private string getMenuContent(List<ViewComboDeal> deals, string LabelTagList)
        {
            string builder = "";
            foreach (ViewComboDeal cd in deals)
            {
                decimal discount = 0;
                if (cd.ItemOrigPrice > 0)
                {
                    discount = Math.Floor((cd.ItemPrice / cd.ItemOrigPrice) * 100) / 10;
                }
                decimal average = 0;
                if ((cd.QuantityMultiplier ?? 1) > 0)
                {
                    average = Math.Round(cd.ItemPrice / (cd.QuantityMultiplier ?? 1), 0);
                }
                string title = "";
                string[] CouponUsages = cd.CouponUsage.Split("-");

                if (CouponUsages.Length > 1)
                {
                    for (int i = 0; i < CouponUsages.Length; i++)
                    {
                        if (i == 0)
                        {
                            continue;
                        }
                        title += CouponUsages[i] + "-";
                    }
                    if (title.Length > 1)
                    {
                        title = title.Substring(0, title.Length - 1);
                    }
                }
                else
                {
                    title = CouponUsages[0];
                }

                builder += string.Format(@"<div style='width: 98%;margin: 10px auto;padding: 0;border: 1px solid #6e6e6e;cursor: pointer;background: #F7F7F7;'>
                           <div class=''>
                               <div style='width: 450px;padding: 5px 12px 5px 6px;float: left;'>
                                   <div style='font-size: 16px;height: 24px;text-align: left;overflow: hidden;'>{0}</div>
                                   <div style='font-size: 13px;height: 40px;'>
                                       <div style='float: left;margin-right: 10px;color: #999;'>{1} 折</div>
                                       <div style='float: left;margin-right: 10px;color: #999;'>原價 ${2}</div>
                                        <div style='float: left;margin-right: 10px;color: #999;'>{3} {4}</div>

                                ",
                                title,
                                discount,
                                cd.ItemOrigPrice.ToString("N0"),
                                LabelTagList == null ? "" : (LabelTagList.IndexOf("14") >= 0 ? "免運費" : ""),
                                LabelTagList == null ? "" : (LabelTagList.IndexOf("16") >= 0 ? "含運費" : "")
                                );
                if (cd.IsAveragePrice)
                {
                    builder += "<div class='mgs-buyer mgs-brcolor-org' style='float: right;color: #f60;margin-left: 10px;'>均價&nbsp;$" + average + "</div>";
                }

                builder += string.Format(@"</div>
                               </div>
                               <div>
                                   <div style='color: #BF0000; margin-top: 2px; line-height: 48px; font-size: 19px;'>${0}</div>
                               </div>
                               <div class='clearfix'>
                               </div>
                           </div>
                         </div>",
                                cd.ItemPrice.ToString("N0")
                                );
            }

            builder += "<br />";

            return builder.Replace("\r", "").Replace("\n", "");
        }
        #endregion private methods

        private void CheckReceiveType()
        {
            lockFinanceSection(true);

            if (int.Parse(ddlRreceiveType.SelectedValue).Equals((int)DeliveryType.ToShop))
            {
                tbFt.Enabled = false;
                tbFt.Text = string.Empty;
                pnFreight.Enabled = false;
                pnFreight.Visible = false;
                cbx_NotDeliveryIslands.Checked = false;
                cbl_deliveryDealTag.Items.FindByValue(Convert.ToString((int)DealLabelSystemCode.NAOutlyingIslands)).Selected = false;
                cbx_GroupCoupon.Visible = true;
                cbx_NoRestrictedStore.Visible = true;
                if (config.IsConsignment)
                {
                    cbx_Consignment.Visible = false;
                }

                rbAchWeekly.Enabled = true;
                rbManualMonthly.Enabled = true;
                rbManualWeekly.Enabled = true;
                rbPartially.Enabled = false;//憑證不支援暫付
                rbOthers.Enabled = true;
                rbFlexible.Enabled = true;
                rbMonthly.Enabled = true;
                rbWeekly.Enabled = true;
                if (config.IsRemittanceFortnightly)
                {
                    rbFortnightly.Enabled = false;
                }


                //如果是新開的檔次且賣家無預設出帳方式的話要預設出帳方式
                if (string.IsNullOrEmpty(Request.QueryString["bid"]) && !SellerPaidType.HasValue)
                {
                    rbAchWeekly.Checked = true;//預設
                    rbManualWeekly.Checked = false;
                    rbManualMonthly.Checked = false;
                    rbPartially.Checked = false;
                    rbOthers.Checked = false;
                    rbFlexible.Checked = false;
                    rbMonthly.Checked = false;
                    rbWeekly.Checked = false;
                    if (config.IsRemittanceFortnightly)
                        rbFortnightly.Checked = false;
                }

                pProduct.Visible = false; //訂單出貨狀態顯示
            }
            else
            {
                tbFt.Enabled = true;
                pnFreight.Enabled = true;
                pnFreight.Visible = true;
                cbx_NotDeliveryIslands.Checked = true;
                cbl_deliveryDealTag.Items.FindByValue(Convert.ToString((int)DealLabelSystemCode.NAOutlyingIslands)).Selected = true;
                cbx_GroupCoupon.Checked = false;
                cbx_GroupCoupon.Visible = false;
                cbx_NoRestrictedStore.Checked = false;
                cbx_NoRestrictedStore.Visible = false;
                if (config.IsConsignment)
                {

                    if (ProposalCreateType == (int)ProposalCreatedType.Seller || hidisSub.Value == "True")
                        cbx_Consignment.Visible = false;
                    else
                        cbx_Consignment.Visible = true;
                }


                rbAchWeekly.Enabled = false;//宅配不支援ACH和人工
                rbManualWeekly.Enabled = false;
                rbManualMonthly.Enabled = false;
                rbPartially.Enabled = true;
                rbOthers.Enabled = true;
                rbFlexible.Enabled = true;
                rbMonthly.Enabled = true;

                rbWeekly.Enabled = true;
                if (config.IsRemittanceFortnightly)
                {
                    rbFortnightly.Enabled = true;
                }


                //如果是新開的檔次且賣家無預設出帳方式的話要預設出帳方式
                if (string.IsNullOrEmpty(Request.QueryString["bid"]) && !SellerPaidType.HasValue)
                {
                    //宅配預設出帳方式
                    rbAchWeekly.Checked = false;
                    rbManualWeekly.Checked = false;
                    rbManualMonthly.Checked = false;
                    rbPartially.Checked = true;//預設
                    rbOthers.Checked = false;
                    rbFlexible.Checked = false;
                    rbMonthly.Checked = false;
                    rbWeekly.Checked = false;
                    if (config.IsRemittanceFortnightly)
                        rbFortnightly.Checked = false;
                }

                AncestorSeqBid.Visible = false;  //接續憑證BID
                AncestorSeqBid.Text = "";
                cbx_Is_Continued_Sequence.Checked = false;
                cbx_Is_Continued_Sequence.Visible = false;

                lockFinanceSection(false);

                pProduct.Visible = true; //訂單出貨狀態顯示
            }

            //fix 當"消費方式"切換時，ckeditor 內容重複編碼的問題
            //其它按鈕功能是透過presenter且有做資料重置動作，所以沒有這個問題
            new[]
            {
                tbSlrInt, tbProSpc, rmkTxt, tbPdtl, txtMutiTab1, txtMutiTab2, txtMutiTab3, txtMutiTab4, txtMutiTab5, txtMutiTab6
            }.ForEach(t => t.Text = HttpUtility.HtmlDecode(t.Text));
        }

        [WebMethod]
        public static string MenuImageUpload()
        {
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var httpPostedFile = HttpContext.Current.Request.Files["fileMenu"];

                if (httpPostedFile != null)
                {
                }
            }
            return "";
        }

        [WebMethod]
        public static bool PponContentSet(
            Guid bid,
            string content_name
            )
        {
            PponFacade.PponContentSet(bid, content_name);
            ViewPponDeal ppon = pp.ViewPponDealGetByBusinessHourGuid(bid);

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (NewtonsoftJson.JsonWriter writer = new NewtonsoftJson.JsonTextWriter(sw))
            {
                writer.Formatting = NewtonsoftJson.Formatting.None;
                writer.WriteStartObject();
                writer.WritePropertyName("ContentName");
                writer.WriteValue(ppon.ContentName);
                writer.WriteEndObject();
            }

            pp.ChangeLogInsert("ViewPponDeal", bid.ToString(), sb.ToString());

            return true;
        }

        [WebMethod]
        public static string PreviewMenu(string bid, string base64, bool CloseMunuContent)
        {
            byte[] bytes = Convert.FromBase64String(base64.Split(',')[1]);
            Guid BusinessHourId = Guid.Empty;
            Guid.TryParse(bid, out BusinessHourId);
            return PponFacade.CreatePreviewMenu(BusinessHourId, bytes, CloseMunuContent);
        }

        [WebMethod]
        public static string GetMoreStore(string sid, string bid, int n)
        {


            Guid s_guid = !string.IsNullOrEmpty(sid) ? Guid.Parse(sid) : Guid.Empty;
            Guid b_guid = !string.IsNullOrEmpty(bid) ? Guid.Parse(bid) : Guid.Empty;
            if (PponFacade.IsSkmDeal(b_guid))
            {
                return new JsonSerializer().Serialize(new List<Seller>());
            }

            PponStoreCollection pponStore = !string.IsNullOrEmpty(bid) ? pp.PponStoreGetListByBusinessHourGuid(b_guid) : new PponStoreCollection();
            int storeDefaultCount = 100;
            int skip = n < 0 ? (pponStore.Count > storeDefaultCount ? pponStore.Count : storeDefaultCount) : 1; //判斷頁面載入or更多
            n = n < 0 ? n : (pponStore.Count > storeDefaultCount ? pponStore.Count : n);
            var stores = SellerFacade.GetSellerTreeSellers(s_guid);

            var st = stores.Select(x => new
            {
                Guid = x.Guid.ToString(),
                StoreName = string.Format("{0} {1}", x.SellerId, x.SellerName),
                StoreAddress = CityManager.CityTownShopStringGet(x.StoreTownshipId == null ? -1 : x.StoreTownshipId.Value) + x.StoreAddress,
                StoreStatus = x.StoreStatus.ToString(),
                CompareStoreInfo = x.CompanyID + x.CompanyAccount + x.StoreTel,
                CheckStore = pponStore.Any(s => s.StoreGuid == x.Guid && Helper.IsFlagSet(s.VbsRight, VbsRightFlag.Location)),
                SortOrder = pponStore.Any(s => s.StoreGuid == x.Guid && s.SortOrder != null) ? pponStore.FirstOrDefault(s => s.StoreGuid == x.Guid).SortOrder.Value.ToString() : string.Empty
            }).OrderByDescending(s => s.CheckStore).ThenBy(s => !string.IsNullOrEmpty(s.SortOrder) ? int.Parse(s.SortOrder) : stores.Count());
            var scc = n < 0 ? st.Skip(skip) : st.Take(n);
            return new JsonSerializer().Serialize(scc);
        }

        [WebMethod]
        public static string GetMoreSellerTree(string sid, string bid, int n, bool enabledSelf, bool isDealOpenOrClose)
        {
            Guid s_guid = !string.IsNullOrEmpty(sid) ? Guid.Parse(sid) : Guid.Empty;
            Guid b_guid = !string.IsNullOrEmpty(bid) ? Guid.Parse(bid) : Guid.Empty;

            if (PponFacade.IsSkmDeal(b_guid))
            {
                return new JsonSerializer().Serialize(new List<Seller>());
            }

            var pponStore = !string.IsNullOrEmpty(bid) ? pp.PponStoreGetListByBusinessHourGuid(b_guid).ToDictionary(x => x.StoreGuid, x => x) : new Dictionary<Guid, PponStore>();
            int storeDefaultCount = 10000;
            int skip = n < 0 ? (pponStore.Count > storeDefaultCount ? pponStore.Count : storeDefaultCount) : 1; //判斷頁面載入or更多
            n = n < 0 ? n : (pponStore.Count > storeDefaultCount ? pponStore.Count : n);
            var stores = SellerFacade.GetSellerTreeNode(s_guid, enabledSelf);
            var scc = n < 0 ? stores.Skip(skip) : stores.Take(n);
            var st = scc.Select(x =>
            {
                var pStore = pponStore.ContainsKey(x.Id) ? pponStore[x.Id] : null;
                var hideBalanceSheet = pStore != null
                                        ? Helper.IsFlagSet(pStore.VbsRight, VbsRightFlag.BalanceSheetHideFromDealSeller)
                                        : false;
                return new
                {
                    Guid = x.Id.ToString(),
                    StoreName = x.Text,
                    OrderedQuantity = pStore != null ? (pStore.OrderedQuantity ?? 0) : 0,
                    StoreCount = pStore != null && pStore.TotalQuantity.HasValue ? (pStore.TotalQuantity).ToString() : string.Empty,
                    ChangedExpireDate = pStore != null && pStore.ChangedExpireDate.HasValue ? pStore.ChangedExpireDate.Value.ToString("yyyy/MM/dd") : string.Empty,
                    StoreUseTime = pStore != null && !string.IsNullOrEmpty(pStore.UseTime) ? pStore.UseTime : string.Empty,
                    Disabled = x.State == TreeState.Closed,
                    CheckVerify = pStore != null ? Helper.IsFlagSet(pStore.VbsRight, VbsRightFlag.Verify) : false,
                    CheckAccounting = pStore != null ? Helper.IsFlagSet(pStore.VbsRight, VbsRightFlag.Accouting) : false,
                    CheckViewBalanceSheet = pStore != null ? Helper.IsFlagSet(pStore.VbsRight, VbsRightFlag.ViewBalanceSheet) : false,
                    HideBalanceSheet = hideBalanceSheet,
                    HideBalanceSheetImageUrl = x.State == TreeState.Open && x.Id.ToString() != sid
                                            ? hideBalanceSheet
                                                ? "lock.png"
                                                : "unlock.png"
                                            : "unlock_disabled.png",
                    CheckVerifyShop = pStore != null ? Helper.IsFlagSet(pStore.VbsRight, VbsRightFlag.VerifyShop) : false,
                    CheckHideVerifyShop = pStore != null ? Helper.IsFlagSet(pStore.VbsRight, VbsRightFlag.HideFromVerifyShop) : false,
                    HideVerifyShopDisabled = isDealOpenOrClose
                };
            });
            return new JsonSerializer().Serialize(st);
        }

    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="PponReturnList.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.PponReturnList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript" src="//code.jquery.com/ui/1.9.2/jquery-ui.js"></script>    
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $('#tbApplyTimeS').datetimepicker({ dateFormat: 'yy/mm/dd', stepMinute: 1 });
            $('#tbApplyTimeE').datetimepicker({ dateFormat: 'yy/mm/dd', stepMinute: 1 });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 100%;">
        <tr>
            <td align="left" nowrap="nowrap" style="width: 10%;">
                <table style="width: 750px; border: double 3px black">
                    <tr>
                        <td colspan="2">           
                            <a href="../Order/ReturnFormList">退貨訂單列表</a>&nbsp;&nbsp;
                            <a href="../piinlife/returnedList.aspx">PiinLife退貨列表</a>
                        </td>     
                    <tr>
                        <td colspan="4" style="font-size: 16px; font-weight: bold" align="center">
                            <asp:Label ID="Label5" runat="server" Text="好康換貨訂單列表"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1" nowrap="nowrap">
                            <asp:Label ID="Label8" runat="server" Text="銷售類型："></asp:Label>
                        </td>
                        <td align="left" class="style2" valign="middle" colspan="3">
                            <asp:RadioButtonList ID="rbDepartment" runat="server" RepeatDirection="Horizontal"
                                RepeatLayout="Flow" DataTextField="Value" DataValueField="Key" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style1" nowrap="nowrap" style="vertical-align: top;">
                            <asp:Label ID="Label6" runat="server" Text="處理進度："></asp:Label>
                        </td>
                        <td align="left" class="style2" valign="middle" colspan="3">
                            <asp:RadioButtonList ID="rbStatus" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"
                                DataTextField="Value" DataValueField="Key" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddlST" runat="server" DataTextField="Value" DataValueField="Key"
                                Width="103px" />
                        </td>
                        <td>
                            <asp:TextBox ID="tbSearch" runat="server" Width="145px" onkeypress="en(event)"></asp:TextBox><br />
                        </td>
                    </tr>
                    <tr>
                        <td class="style1" nowrap="nowrap">
                            <asp:Label ID="Label3" runat="server" Text="申請時間："></asp:Label>
                        </td>
                        <td class="style2">
                            <asp:TextBox ID="tbApplyTimeS" runat="server" ClientIDMode="Static"></asp:TextBox>
                            ～
                            <asp:TextBox ID="tbApplyTimeE" runat="server" ClientIDMode="Static"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" rowspan="1" valign="middle" nowrap="nowrap">
                            <asp:Button ID="Button1" runat="server" Text="查詢" OnClick="Search_Click" />
                        </td>
                        <td align="left" rowspan="1" colspan="3" valign="middle">
                            <asp:Label ID="lErr" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left">
                <table style="border: double 3px black">
                    <tr>
                        <th style="width: 194px">
                            列表狀態
                        </th>
                    </tr>
                    <tr>
                        <td style="width: 194px">
                            自動更新 (秒):
                            <asp:DropDownList ID="ddlTmr" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTmr_SIC">
                                <asp:ListItem Text="無" Value="0" Selected="True" />
                                <asp:ListItem Text="10" Value="10" />
                                <asp:ListItem Text="30" Value="30" />
                                <asp:ListItem Text="60" Value="60" />
                            </asp:DropDownList>
                            <aspajax:Timer ID="tmr1" runat="server" Enabled="false" OnTick="tmr1_Tick" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 194px">
                            每頁顯示:
                            <asp:DropDownList ID="ddlPS" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPS_SIC">
                                <asp:ListItem Text="15" Value="15" Selected="true" />
                                <asp:ListItem Text="30" Value="30" />
                                <asp:ListItem Text="60" Value="60" />
                                <asp:ListItem Text="100" Value="100" />
                                <asp:ListItem Text="200" Value="200" />
                            </asp:DropDownList>
                            筆資料
                        </td>
                    </tr>
                    </table>
            </td>
            <td align="left" rowspan="1" valign="bottom" nowrap="nowrap">
                <asp:CheckBox ID="cbSelectAll" Text="同時顯示所有退換貨狀態" runat="server" />
            </td>
        </tr>
    </table>
    <asp:GridView ID="gvRetrn" runat="server" AutoGenerateColumns="False" OnSorting="gvRetrn_Sorting"
        AllowSorting="True" DataKeyNames="OrderGuid" GridLines="Horizontal" ForeColor="Black"
        CellPadding="4" BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE" BackColor="White"
        EmptyDataText="無符合的資料" Font-Size="Smaller">
        <FooterStyle BackColor="#CCCC99"></FooterStyle>
        <RowStyle BackColor="#F7F7DE" Height="50px"></RowStyle>
        <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
        <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="30px">
        </HeaderStyle>
        <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField ReadOnly="True" DataField="uniqueId" HeaderText="檔號">
                <ItemStyle Width="60px" />
            </asp:BoundField>
            <asp:HyperLinkField HeaderText="訂單編號" DataTextField="OrderId" DataNavigateUrlFormatString="../Order/order_detail.aspx?oid={0}"
                DataNavigateUrlFields="OrderGuid">
                <ItemStyle Wrap="False" Width="130px" Font-Size="14px" />
            </asp:HyperLinkField>
            <asp:HyperLinkField HeaderText="檔次名稱" 
                DataTextField="CouponUsage"
                DataNavigateUrlFormatString="../Ppon/setup.aspx?bid={0}"
                DataNavigateUrlFields="BusinessHourGuid"
                >
                <ItemStyle Wrap="True" Width="200px" Font-Size="14px" />
            </asp:HyperLinkField>
            <asp:TemplateField HeaderText="憑證編號">
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# GetCouponList(DataBinder.Eval(Container, "DataItem.OrderGuid").ToString()) %>'
                        Font-Size="14px"></asp:Label>
                    <asp:HiddenField ID="hfOrderGuid" runat="server" Value='<%# Eval("OrderGuid") %>' />
                </ItemTemplate>
                <ItemStyle Wrap="False" Width="70px" />
            </asp:TemplateField>
            <asp:HyperLinkField HeaderText="申請時間" DataTextField="ReturnCreateTime">
                <ItemStyle Width="200px" />
            </asp:HyperLinkField>
            <asp:HyperLinkField HeaderText="處理時間" DataTextField="ReturnModifyTime">
                <ItemStyle Width="200px" />
            </asp:HyperLinkField>
            <asp:TemplateField HeaderText="結檔日期">
                <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text='<%# GetTheDate(DataBinder.Eval(Container, "DataItem.BusinessHourOrderTimeE").ToString()) %>'
                        Font-Size="14px"></asp:Label>
                </ItemTemplate>
                <ItemStyle Wrap="False" Width="70px" />
            </asp:TemplateField>
            <asp:HyperLinkField HeaderText="廠商名稱" DataTextField="SellerName" DataNavigateUrlFormatString="../Seller/seller_add.aspx?sid={0}"
                DataNavigateUrlFields="SellerGuid">
                <ItemStyle Width="200px" />
            </asp:HyperLinkField>
            <asp:TemplateField HeaderText="訂單狀況">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# GetOrderSituation(int.Parse(DataBinder.Eval(Container, "DataItem.DeliveryType").ToString()),
                                    DateTime.Parse(DataBinder.Eval(Container, "DataItem.ReturnCreateTime").ToString()),
                                    DateTime.Parse(DataBinder.Eval(Container, "DataItem.BusinessHourDeliverTimeE").ToString())) %>'>
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Wrap="False" Width="70px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="金額">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# decimal.Parse(DataBinder.Eval(Container, "DataItem.Total").ToString()).ToString("F0") %>'>
                    </asp:Label>
                </ItemTemplate>
                <ItemStyle Wrap="False" Width="50px" HorizontalAlign="Right" Font-Size="13px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="取消理由">
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# GetRefundReason(DataBinder.Eval(Container, "DataItem.OrderGuid").ToString()) %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="140px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="數量">
                <ItemTemplate>
                    <asp:Label ID="lbQty" runat="server" Text='<%# Eval("Quantity") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="40px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="處理備註">
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ReturnMessage", null).ToString() %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="120px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="處理進度">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# GetRefundProcess(DataBinder.Eval(Container, "DataItem.ReturnStatus").ToString(),DataBinder.Eval(Container, "DataItem.ReturnFlag").ToString()) %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="180px" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <uc1:Pager ID="gridPager" runat="server" PageSize="15" OnGetCount="RetrieveTransCount"
        OnUpdate="UpdateHandler"></uc1:Pager>
    <asp:Button ID="btnExportToExcel" runat="server" Text="匯出Excel" OnClick="btnExportToExcel_Click" />
</asp:Content>

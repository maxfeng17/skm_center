﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PponInsertSerialKey.aspx.cs"
    Inherits="LunchKingSite.Web.ControlRoom.Ppon.PponInsertSerialKey" MasterPageFile="~/ControlRoom/backend.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <style type="text/css">
        .bgOrange
        {
            background-color: #FFC690;
            text-align: center;
        }
        .bgBlue
        {
            background-color: #B3D5F0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table style="background-color: gray; width: 700px">
            <tr>
                <td colspan="2" style="background-color: Green; color: White; font-weight: bolder;
                    text-align: center">
                    廠商序號檔次
                </td>
            </tr>
            <tr>
                <td class="bgOrange" style="width: 25%">
                    Bid
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_Bid" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_Bid" runat="server" ErrorMessage="*" Display="Dynamic"
                        ControlToValidate="tbx_Bid" ValidationGroup="A" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:Button ID="btn_Search" runat="server" Text="查詢" ValidationGroup="A" OnClick="SearchCurrentCount" />
                </td>
            </tr>
            <tr>
                <td class="bgOrange">
                    序號搭配種類
                </td>
                <td class="bgBlue">
                    <asp:RadioButtonList ID="rbl_SerialKeyDealType" runat="server">
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="bgOrange">
                    上傳檔案
                </td>
                <td class="bgBlue">
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                    <br />
                    (txt檔，請以逗號分隔欄位，以空行或分號分隔列)
                </td>
            </tr>
            <tr style="display: none">
                <td class="bgOrange">
                    預留份數
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_Blank" runat="server"></asp:TextBox>(預防超賣，序號會以空白預留)
                    <asp:RangeValidator ID="rv_Blank" runat="server" ErrorMessage="*" ValidationGroup="A"
                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Blank" MaximumValue="2000"
                        MinimumValue="1"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="bgBlue" style="text-align: center">
                    <asp:Button ID="btn_Insert" runat="server" Text="加入序號" OnClick="InsertSerialKey"
                        ValidationGroup="A" />
                </td>
            </tr>
            <tr>
                <td class="bgOrange">
                    目前資訊
                </td>
                <td class="bgBlue">
                    <asp:Label ID="lab_Info" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center" class="bgBlue">
                    <asp:Label ID="lab_Message" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

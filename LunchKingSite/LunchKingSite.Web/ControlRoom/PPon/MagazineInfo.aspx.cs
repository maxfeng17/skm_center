﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using System.Net;
using System.IO;
using System.Text;
using MongoDB.Bson;
using Microsoft.SqlServer.Types;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class MagazineInfo : RolePage, IMagazineInfoView
    {
        ISysConfProvider configs = ProviderFactory.Instance().GetConfig();

        #region props
        private MagazineInfoPresenter _presenter;
        public MagazineInfoPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string SellerName
        {
            get { return txtSellerName.Text.Trim(); }
        }

        public string BusinessOrderId
        {
            get { return txtBusinessOrderId.Text.Trim(); }
        }

        public DateTime PreferentialDateTime
        {
            get
            {
                int year;
                int.TryParse(ddlYear.SelectedValue, out year);
                int month;
                int.TryParse(ddlMonth.SelectedValue, out month);
                return new DateTime(year + 1911, month, 1);
            }
        }

        public DateTime CreateTimeStart
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtCreateTimeS.Text, out date);
                return date;
            }
        }

        public DateTime CreateTimeEnd
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtCreateTimeE.Text, out date);
                return date;
            }
        }

        public int PageSize
        {
            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }
        }

        public DateTime ModifyTimeStart
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtModifyTimeS.Text, out date);
                return date;
            }
        }

        public DateTime ModifyTimeEnd
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtModifyTimeE.Text, out date);
                return date;
            }
        }

        public IEnumerable<string> DeptIdList
        {
            get
            {
                return chklDept.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value);
            }
        }

        public ExportType Type
        {
            get
            {
                ExportType type = ExportType.Magazine;
                ExportType.TryParse(rdlType.SelectedValue, out type);
                return type;
            }
        }

        private string protectRule = @"(此優惠不得與店家其他優惠合併使用) (需於結帳前出示<span class=style1><font color='blue'>特約優惠卡 </font>或<font color='blue'>貼/印有PayEasy標誌之台新銀行信用卡</font>方可享有優惠)註：出示貼/印有PayEasy Logo之台新信用卡，且刷卡消費者，需刷此張卡片方可享有優惠。";
        #endregion

        #region event
        public event EventHandler OnSearchClicked;
        public event EventHandler OnPezStoreSearchClicked;
        public event EventHandler OnExportToExcelClicked;
        public event EventHandler OnPezStoreExportToExcelClicked;
        public event EventHandler<DataEventArgs<int>> OnGetCount;
        public event EventHandler<DataEventArgs<int>> PageChanged;
        #endregion

        #region method
        public void SetDepartmentData(DepartmentCollection deptList)
        {
            chklDept.DataSource = deptList.Select(x => new { DeptId = x.DeptId, DeptName = x.DeptName.Replace("業務部", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty) });
            chklDept.DataValueField = "DeptId";
            chklDept.DataTextField = "DeptName";
            chklDept.DataBind();
        }

        public void GetBusinessList(Dictionary<Preferential, BusinessOrder> dataList)
        {
            gvMagazine.DataSource = dataList;
            gvMagazine.DataBind();
        }

        public void GetBusinessPezStoreList(IEnumerable<BusinessOrder> businessOrderList)
        {
            Dictionary<SalesStore, BusinessOrder> dataList = new Dictionary<SalesStore, BusinessOrder>();
            foreach (BusinessOrder item in businessOrderList)
            {
                if (item.Store.Count.Equals(0))
                    dataList.Add(new SalesStore(), item);
                else
                {
                    dataList.Add(new SalesStore(), item);
                    foreach (SalesStore store in item.Store)
                    {
                        dataList.Add(store, item);
                    }
                }
            }
            gvPezStore.DataSource = dataList;
            gvPezStore.DataBind();
        }

        public void SetGridViewListData(Dictionary<Preferential, BusinessOrder> dataList)
        {
            string fileName = string.Format("{0}年度0{1}月號生活誌店家總表_{2}.xls", ddlYear.SelectedValue, ddlMonth.SelectedValue, DateTime.Now.ToString("MMdd"));
            Export(fileName, dataList);
        }

        public void ExportPezStoreToExcel(IEnumerable<BusinessOrder> dataList)
        {
            DateTime dateStart;
            DateTime.TryParse(txtCreateTimeS.Text, out dateStart);
            DateTime dateEnd;
            DateTime.TryParse(txtCreateTimeE.Text, out dateEnd);
            string fileName = string.Format("{0}至{1}特約優惠商店_{1}.xls", dateStart.ToString("yyyyMMdd"), dateEnd.ToString("yyyyMMdd"), DateTime.Now.ToString("yyyyMMdd"));
            ExportPezStore(fileName, dataList);
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<int> yearList = new List<int>();
                int startYear = DateTime.Now.AddYears(-3).Year - 1911;
                for (int i = 0; i < 5; i++)
                {
                    yearList.Add(startYear + i);
                }
                ddlYear.DataSource = yearList;
                ddlYear.DataBind();

                List<int> monthList = new List<int>();
                for (int i = 0; i < 12; i++)
                {
                    monthList.Add(i + 1);
                }
                ddlMonth.DataSource = monthList;
                ddlMonth.DataBind();

                ucPager.ResolvePagerView(1, true);
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (Type.Equals(ExportType.Magazine))
            {
                if (this.OnSearchClicked != null)
                    this.OnSearchClicked(this, e);
                ucPager.ResolvePagerView(1, true);
                divMagazine.Visible = true;
                divPezStore.Visible = false;
            }
            else
            {
                if (this.OnPezStoreSearchClicked != null)
                    this.OnPezStoreSearchClicked(this, e);
                ucPezStorePager.ResolvePagerView(1, true);
                divMagazine.Visible = false;
                divPezStore.Visible = true;
            }
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            if (Type.Equals(ExportType.Magazine))
            {
                if (this.OnExportToExcelClicked != null)
                    this.OnExportToExcelClicked(this, e);
            }
            else
            {
                if (this.OnPezStoreExportToExcelClicked != null)
                    this.OnPezStoreExportToExcelClicked(this, e);
            }
        }

        protected void gvMagazine_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is KeyValuePair<Preferential, BusinessOrder>)
            {
                KeyValuePair<Preferential, BusinessOrder> item = (KeyValuePair<Preferential, BusinessOrder>)e.Row.DataItem;

                Label lblBusinessOrderId = ((Label)e.Row.FindControl("lblBusinessOrderId"));
                Label lblMagazineDate = ((Label)e.Row.FindControl("lblMagazineDate"));
                Label lblSellerName = ((Label)e.Row.FindControl("lblSellerName"));
                Label lblCompanyName = ((Label)e.Row.FindControl("lblCompanyName"));
                Label lblBossAddress = ((Label)e.Row.FindControl("lblBossAddress"));
                Label lblGuiNumber = ((Label)e.Row.FindControl("lblGuiNumber"));
                Label lblBossName = ((Label)e.Row.FindControl("lblBossName"));
                Label lblPersonalId = ((Label)e.Row.FindControl("lblPersonalId"));
                Label lblContactPerson = ((Label)e.Row.FindControl("lblContactPerson"));
                Label lblBossTel = ((Label)e.Row.FindControl("lblBossTel"));
                Label lblBossPhone = ((Label)e.Row.FindControl("lblBossPhone"));
                Label lblBossFax = ((Label)e.Row.FindControl("lblBossFax"));
                Label lblBossEmail = ((Label)e.Row.FindControl("lblBossEmail"));
                Label lblWebUrl = ((Label)e.Row.FindControl("lblWebUrl"));
                Label lblSellerAddress = ((Label)e.Row.FindControl("lblSellerAddress"));
                Label lblReservationTel = ((Label)e.Row.FindControl("lblReservationTel"));
                Label lblOpeningTime = ((Label)e.Row.FindControl("lblOpeningTime"));
                Label lblCloseDate = ((Label)e.Row.FindControl("lblCloseDate"));
                Label lblVehicleInfo = ((Label)e.Row.FindControl("lblVehicleInfo"));
                Label lblIsEDC = ((Label)e.Row.FindControl("lblIsEDC"));
                Label lblCreditCardReaderId = ((Label)e.Row.FindControl("lblCreditCardReaderId"));
                Label lblPerCustomerTrans = ((Label)e.Row.FindControl("lblPerCustomerTrans"));
                Label lblImportant = ((Label)e.Row.FindControl("lblImportant"));
                Label lblSellerTemplate = ((Label)e.Row.FindControl("lblSellerTemplate"));
                Label lblSellerDescription = ((Label)e.Row.FindControl("lblSellerDescription"));
                Label lblBusinessHourGuid = ((Label)e.Row.FindControl("lblBusinessHourGuid"));
                Label lblEnableUse = ((Label)e.Row.FindControl("lblEnableUse"));
                Label lblPreferentialName = ((Label)e.Row.FindControl("lblPreferentialName"));
                Label lblPreferentialDesc = ((Label)e.Row.FindControl("lblPreferentialDesc"));
                Label lblNoticeDescList = ((Label)e.Row.FindControl("lblNoticeDescList"));
                Label lblMagazineNotice1 = ((Label)e.Row.FindControl("lblMagazineNotice1"));
                Label lblMagazineNotice2 = ((Label)e.Row.FindControl("lblMagazineNotice2"));
                Label lblSalesName = ((Label)e.Row.FindControl("lblSalesName"));
                Label lblSalesPhone = ((Label)e.Row.FindControl("lblSalesPhone"));
                Label lblModifyTime = ((Label)e.Row.FindControl("lblModifyTime"));

                lblBusinessOrderId.Text = item.Value.BusinessOrderId;
                if (item.Key.PreferentialDateTime.Count > 0)
                    lblMagazineDate.Text = string.Join(",", item.Key.PreferentialDateTime.Select(x => x.ToLocalTime()).Select(x => string.Format("{0}/{1}", (x.Year - 1911), x.Month)));
                else
                    lblMagazineDate.Text = string.Join(",", item.Value.MagazineMonth.Select(x => string.Format("{0}/{1}", item.Value.MagazineYear, x)));
                lblSellerName.Text = item.Value.SellerName;
                lblCompanyName.Text = item.Value.CompanyName;
                lblBossAddress.Text = item.Value.BossAddress;
                lblGuiNumber.Text = item.Value.GuiNumber;
                lblBossName.Text = item.Value.BossName;
                lblPersonalId.Text = item.Value.PersonalId;
                lblContactPerson.Text = item.Value.ContactPerson;
                lblBossTel.Text = item.Value.BossTel;
                lblBossPhone.Text = item.Value.BossPhone;
                lblBossFax.Text = item.Value.BossFax;
                lblBossEmail.Text = item.Value.BossEmail;
                lblWebUrl.Text = item.Value.WebUrl;
                lblSellerAddress.Text = CityManager.CityTownShopStringGet(item.Value.AddressTownshipId) + item.Value.SellerAddress;
                lblReservationTel.Text = item.Value.ReservationTel;
                lblOpeningTime.Text = item.Value.OpeningTime;
                lblCloseDate.Text = item.Value.CloseDate;
                lblVehicleInfo.Text = item.Value.VehicleInfo;
                lblIsEDC.Text = item.Value.IsEDC ? "是" : "否";
                lblCreditCardReaderId.Text = item.Value.CreditCardReaderId;
                lblPerCustomerTrans.Text = item.Value.PerCustomerTransDesc;
                lblImportant.Text = string.Format("1.{0}2.{1}3.{2}", item.Value.Important1, item.Value.Important2, item.Value.Important3);
                lblSellerTemplate.Text = item.Value.SellerTemplateDesc;
                lblSellerDescription.Text = item.Value.SellerDescription;
                if (!item.Value.BusinessHourGuid.Equals(Guid.Empty))
                    lblBusinessHourGuid.Text = configs.SiteUrl + "/" + item.Value.BusinessHourGuid.ToString();
                else
                    lblBusinessHourGuid.Text = configs.SiteUrl + "/piinlife/HiDeal.aspx?did=" + item.Value.DealId.ToString();
                lblEnableUse.Text = item.Key.EnableUse ? "是" : "否";
                lblPreferentialName.Text = item.Key.Name;
                lblPreferentialDesc.Text = item.Key.PreferentialDesc;
                for (int i = 0; i < item.Key.NoticeDescList.Count; i++)
                {
                    lblNoticeDescList.Text += (i + 1).ToString() + "." + item.Key.NoticeDescList[i] + " ";
                }
                lblMagazineNotice1.Text = item.Value.MagazineNotice1;
                lblMagazineNotice2.Text = item.Value.MagazineNotice2;
                lblSalesName.Text = item.Value.SalesName;
                lblSalesPhone.Text = item.Value.SalesPhone;
                if (item.Value.ModifyTime != null)
                    lblModifyTime.Text = item.Value.ModifyTime.Value.ToLocalTime().ToString("yyyy/MM/dd HH:mm");
            }
        }

        protected void gvPezStore_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is KeyValuePair<SalesStore, BusinessOrder>)
            {
                KeyValuePair<SalesStore, BusinessOrder> item = (KeyValuePair<SalesStore, BusinessOrder>)e.Row.DataItem;
                Label lblGuiNumber = ((Label)e.Row.FindControl("lblGuiNumber"));
                Label lblSellerName = ((Label)e.Row.FindControl("lblSellerName"));
                Label lblBranchName = ((Label)e.Row.FindControl("lblBranchName"));
                Label lblPezStoreNotice = ((Label)e.Row.FindControl("lblPezStoreNotice"));
                Label lblCity = ((Label)e.Row.FindControl("lblCity"));
                Label lblTownship = ((Label)e.Row.FindControl("lblTownship"));
                Label lblAddress = ((Label)e.Row.FindControl("lblAddress"));
                Label lblOV = ((Label)e.Row.FindControl("lblOV"));
                Label lblStoreTel = ((Label)e.Row.FindControl("lblStoreTel"));
                Label lblOpeningTime = ((Label)e.Row.FindControl("lblOpeningTime"));
                Label lblCloseDate = ((Label)e.Row.FindControl("lblCloseDate"));
                Label lblWebUrl = ((Label)e.Row.FindControl("lblWebUrl"));
                Label lblSellerDescription = ((Label)e.Row.FindControl("lblSellerDescription"));
                Label lblPezStoreDateStart = ((Label)e.Row.FindControl("lblPezStoreDateStart"));
                Label lblPezStoreDateEnd = ((Label)e.Row.FindControl("lblPezStoreDateEnd"));
                Label lblPerCustomerTransDesc = ((Label)e.Row.FindControl("lblPerCustomerTransDesc"));
                Label lblLatitude = ((Label)e.Row.FindControl("lblLatitude"));
                Label lblLongtitude = ((Label)e.Row.FindControl("lblLongtitude"));
                Label lblContactPerson = ((Label)e.Row.FindControl("lblContactPerson"));
                Label lblBossPhone = ((Label)e.Row.FindControl("lblBossPhone"));
                Label lblBossTel = ((Label)e.Row.FindControl("lblBossTel"));
                Label lblBossEmail = ((Label)e.Row.FindControl("lblBossEmail"));
                Label lblBossFax = ((Label)e.Row.FindControl("lblBossFax"));
                Label lblSalesName = ((Label)e.Row.FindControl("lblSalesName"));

                lblSellerName.Text = item.Value.SellerName; // "商店名稱*

                int index = item.Value.Store.FindIndex(x => x.Equals(item.Key));
                if (item.Value.Store.Count.Equals(0)) // 無分店
                {
                    lblGuiNumber.Text = item.Value.GuiNumber; // "識別代碼*
                    lblBranchName.Text = item.Value.StoreName; // "分店名
                }
                else if (index.Equals(-1))
                {
                    lblGuiNumber.Text = item.Value.GuiNumber + "(1)"; // "識別代碼*
                    lblBranchName.Text = item.Value.StoreName; // "分店名
                }
                else
                {
                    lblGuiNumber.Text = item.Value.GuiNumber + "(" + (index + 2) + ")"; // "識別代碼*
                    lblBranchName.Text = item.Key.BranchName; // "分店名
                }
                lblPezStoreNotice.Text = item.Value.PezStore + item.Value.PezStoreNotice + protectRule; // "優惠內容 + 注意事項 + 保護條款
                City storeCity = CityManager.CityGetById(item.Key.AddressCityId);
                lblCity.Text = storeCity != null ? storeCity.CityName : string.Empty; // "縣市*
                City storeTownship = CityManager.TownShipGetById(item.Key.AddressTownshipId);
                lblTownship.Text = storeTownship != null ? storeTownship.CityName : string.Empty; // "鄉鎮市區*
                lblAddress.Text = item.Key.StoreAddress; // "地址(道路門牌)*
                lblOV.Text = item.Key.OV; // "交通資訊
                lblStoreTel.Text = item.Key.StoreTel; // "電話*
                lblOpeningTime.Text = item.Key.OpeningTime; // "營業時間
                lblCloseDate.Text = item.Key.CloseDate; // "公休日
                lblWebUrl.Text = item.Key.WebUrl; // "官方網站
                lblSellerDescription.Text = item.Value.SellerDescription; // "商店簡介
                lblPezStoreDateStart.Text = item.Value.PezStoreDateStart != null ? item.Value.PezStoreDateStart.Value.ToLocalTime().ToString("yyyy/MM/dd") : string.Empty; // "合約生效日期
                lblPezStoreDateEnd.Text = item.Value.PezStoreDateEnd != null ? item.Value.PezStoreDateEnd.Value.ToLocalTime().ToString("yyyy/MM/dd") : string.Empty; // "合約結束日期
                lblPerCustomerTransDesc.Text = item.Value.PerCustomerTransDesc; // "平均消費
                string[] storeArray = GetLatitudeAndLongtitude(item.Value.SellerAddress);
                lblLatitude.Text = storeArray[0]; // "定位緯度(其實是經度)
                lblLongtitude.Text = storeArray[1]; // "定位經度(其實是緯度)
                lblContactPerson.Text = item.Value.ContactPerson; // "商店聯絡人*
                lblBossPhone.Text = item.Value.BossPhone; // "商店聯絡人電話*
                lblBossTel.Text = item.Value.BossTel; // "商店聯絡人手機號碼*
                lblBossEmail.Text = item.Value.BossEmail; // "商店聯絡人Email*
                lblBossFax.Text = item.Value.BossFax; // "商店聯絡人傳真
                lblSalesName.Text = item.Value.SalesName; // "業務來源
            }
        }

        protected void UpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.PageChanged != null)
                this.PageChanged(this, e);
        }

        protected int GetCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetCount != null)
                OnGetCount(this, e);
            return e.Data;
        }
        #endregion

        #region private method
        private void Export(string fileName, Dictionary<Preferential, BusinessOrder> dataList)
        {
            if (dataList.Count > 0)
            {
                Workbook workbook = new HSSFWorkbook();

                #region 全省店家
                // 新增試算表
                Sheet businessOrder = workbook.CreateSheet("全省店家");
                Row HeaderRow = businessOrder.CreateRow(0);
                HeaderRow.CreateCell(0).SetCellValue("工單編號");
                HeaderRow.CreateCell(1).SetCellValue("刊登日期");
                HeaderRow.CreateCell(2).SetCellValue("品牌名稱");
                HeaderRow.CreateCell(3).SetCellValue("簽約公司名稱");
                HeaderRow.CreateCell(4).SetCellValue("簽約公司地址");
                HeaderRow.CreateCell(5).SetCellValue("統一編號");
                HeaderRow.CreateCell(6).SetCellValue("負責人");
                HeaderRow.CreateCell(7).SetCellValue("身分證字號");
                HeaderRow.CreateCell(8).SetCellValue("聯絡人姓名");
                HeaderRow.CreateCell(9).SetCellValue("聯絡人手機");
                HeaderRow.CreateCell(10).SetCellValue("聯絡人電話");
                HeaderRow.CreateCell(11).SetCellValue("聯絡人傳真");
                HeaderRow.CreateCell(12).SetCellValue("聯絡人Email");
                HeaderRow.CreateCell(13).SetCellValue("官方網站");
                HeaderRow.CreateCell(14).SetCellValue("地址");
                HeaderRow.CreateCell(15).SetCellValue("電話");
                HeaderRow.CreateCell(16).SetCellValue("營業時間");
                HeaderRow.CreateCell(17).SetCellValue("公休日");
                HeaderRow.CreateCell(18).SetCellValue("交通資訊");
                HeaderRow.CreateCell(19).SetCellValue("是否有刷卡機");
                HeaderRow.CreateCell(20).SetCellValue("刷卡機商店代碼");
                HeaderRow.CreateCell(21).SetCellValue("平均消費");
                HeaderRow.CreateCell(22).SetCellValue("介紹版位");
                HeaderRow.CreateCell(23).SetCellValue("介紹文案(建議重點)");
                HeaderRow.CreateCell(24).SetCellValue("店家介紹");
                HeaderRow.CreateCell(25).SetCellValue("檔次連結");
                HeaderRow.CreateCell(26).SetCellValue("循環使用");
                HeaderRow.CreateCell(27).SetCellValue("本期最優惠");
                HeaderRow.CreateCell(28).SetCellValue("優惠內容");
                HeaderRow.CreateCell(29).SetCellValue("使用說明");
                HeaderRow.CreateCell(30).SetCellValue("需另提供或確認事項");
                HeaderRow.CreateCell(31).SetCellValue("需特別注意或排除事項");
                HeaderRow.CreateCell(32).SetCellValue("負責業務");
                HeaderRow.CreateCell(33).SetCellValue("業務電話");
                HeaderRow.CreateCell(34).SetCellValue("最後修改日期");

                Dictionary<BusinessOrder, List<SalesStore>> salesStoreList = new Dictionary<BusinessOrder, List<SalesStore>>();

                Cell cell = null;
                int rowCount = 0;
                int rowIndex = 1;
                foreach (KeyValuePair<Preferential, BusinessOrder> item in dataList)
                {
                    #region 組成連鎖分店清單
                    if (!salesStoreList.ContainsKey(item.Value))
                        salesStoreList.Add(item.Value, item.Value.Store);
                    #endregion

                    Row dataRow = businessOrder.CreateRow(rowIndex);

                    cell = dataRow.CreateCell(0);
                    cell.SetCellValue(item.Value.BusinessOrderId);


                    cell = dataRow.CreateCell(1);
                    if (item.Key.PreferentialDateTime.Count > 0)
                        cell.SetCellValue(string.Join(",", item.Key.PreferentialDateTime.Select(x => x.ToLocalTime()).Select(x => string.Format("{0}/{1}", (x.Year - 1911), x.Month))));
                    else
                        cell.SetCellValue(string.Join(",", item.Value.MagazineMonth.Select(x => string.Format("{0}/{1}", item.Value.MagazineYear, x))));
                    cell = dataRow.CreateCell(2);
                    cell.SetCellValue(item.Value.SellerName);
                    cell = dataRow.CreateCell(3);
                    cell.SetCellValue(item.Value.CompanyName);
                    cell = dataRow.CreateCell(4);
                    cell.SetCellValue(item.Value.BossAddress);
                    cell = dataRow.CreateCell(5);
                    cell.SetCellValue(item.Value.GuiNumber);
                    cell = dataRow.CreateCell(6);
                    cell.SetCellValue(item.Value.BossName);
                    cell = dataRow.CreateCell(7);
                    cell.SetCellValue(item.Value.PersonalId);
                    cell = dataRow.CreateCell(8);
                    cell.SetCellValue(item.Value.ContactPerson);
                    cell = dataRow.CreateCell(9);
                    cell.SetCellValue(item.Value.BossTel);
                    cell = dataRow.CreateCell(10);
                    cell.SetCellValue(item.Value.BossPhone);
                    cell = dataRow.CreateCell(11);
                    cell.SetCellValue(item.Value.BossFax);
                    cell = dataRow.CreateCell(12);
                    cell.SetCellValue(item.Value.BossEmail);
                    cell = dataRow.CreateCell(13);
                    cell.SetCellValue(item.Value.WebUrl);
                    cell = dataRow.CreateCell(14);
                    cell.SetCellValue(CityManager.CityTownShopStringGet(item.Value.AddressTownshipId) + item.Value.SellerAddress);
                    cell = dataRow.CreateCell(15);
                    cell.SetCellValue(item.Value.ReservationTel);
                    cell = dataRow.CreateCell(16);
                    cell.SetCellValue(item.Value.OpeningTime);
                    cell = dataRow.CreateCell(17);
                    cell.SetCellValue(item.Value.CloseDate);
                    cell = dataRow.CreateCell(18);
                    cell.SetCellValue(item.Value.VehicleInfo);
                    cell = dataRow.CreateCell(19);
                    cell.SetCellValue(item.Value.IsEDC ? "是" : "否");
                    cell = dataRow.CreateCell(20);
                    cell.SetCellValue(item.Value.CreditCardReaderId);
                    cell = dataRow.CreateCell(21);
                    cell.SetCellValue(item.Value.PerCustomerTransDesc);
                    cell = dataRow.CreateCell(22);
                    cell.SetCellValue(item.Value.SellerTemplateDesc);
                    cell = dataRow.CreateCell(23);
                    cell.SetCellValue(item.Value.SellerDescription);
                    cell = dataRow.CreateCell(24);
                    cell.SetCellValue(string.Format("1.{0}2.{1}3.{2}", item.Value.Important1, item.Value.Important2, item.Value.Important3));
                    cell = dataRow.CreateCell(25);
                    if (!item.Value.BusinessHourGuid.Equals(Guid.Empty))
                        cell.SetCellValue(configs.SiteUrl + "/" + item.Value.BusinessHourGuid.ToString());
                    else
                        cell.SetCellValue(configs.SiteUrl + "/piinlife/HiDeal.aspx?did=" + item.Value.DealId.ToString());
                    cell = dataRow.CreateCell(26);
                    cell.SetCellValue(item.Key.EnableUse ? "是" : "否");
                    cell = dataRow.CreateCell(27);
                    cell.SetCellValue(item.Key.Name);
                    cell = dataRow.CreateCell(28);
                    cell.SetCellValue(item.Key.PreferentialDesc);
                    cell = dataRow.CreateCell(29);
                    string descList = string.Empty;
                    for (int i = 0; i < item.Key.NoticeDescList.Count; i++)
                    {
                        descList += (i + 1).ToString() + "." + item.Key.NoticeDescList[i] + " ";
                    }
                    cell.SetCellValue(descList);
                    cell = dataRow.CreateCell(30);
                    cell.SetCellValue(item.Value.MagazineNotice1);
                    cell = dataRow.CreateCell(31);
                    cell.SetCellValue(item.Value.MagazineNotice2);
                    cell = dataRow.CreateCell(32);
                    cell.SetCellValue(item.Value.SalesName);
                    cell = dataRow.CreateCell(33);
                    cell.SetCellValue(item.Value.SalesPhone);
                    cell = dataRow.CreateCell(34);
                    cell.SetCellValue(item.Value.ModifyTime != null ? item.Value.ModifyTime.Value.ToLocalTime().ToString("yyyy/MM/dd HH:mm") : string.Empty);

                    rowIndex++;
                    rowCount++;
                }
                #endregion

                #region 連鎖分店
                // 新增試算表
                Sheet store = workbook.CreateSheet("連鎖分店");
                Row storeHeaderRow = store.CreateRow(0);
                storeHeaderRow.CreateCell(0).SetCellValue("工單編號");
                storeHeaderRow.CreateCell(1).SetCellValue("連鎖店家名稱");
                storeHeaderRow.CreateCell(2).SetCellValue("分店名");
                storeHeaderRow.CreateCell(3).SetCellValue("地址");
                storeHeaderRow.CreateCell(4).SetCellValue("電話");
                storeHeaderRow.CreateCell(5).SetCellValue("營業時間");
                storeHeaderRow.CreateCell(6).SetCellValue("公休日");

                Cell storeCell = null;
                int storeRowCount = 0;
                int storeRowIndex = 1;
                foreach (KeyValuePair<BusinessOrder, List<SalesStore>> item in salesStoreList)
                {
                    foreach (SalesStore salesStore in item.Value)
                    {
                        Row dataRow = store.CreateRow(storeRowIndex);

                        storeCell = dataRow.CreateCell(0);
                        storeCell.SetCellValue(item.Key.BusinessOrderId);
                        storeCell = dataRow.CreateCell(1);
                        storeCell.SetCellValue(item.Key.SellerName);
                        storeCell = dataRow.CreateCell(2);
                        storeCell.SetCellValue(salesStore.BranchName);
                        storeCell = dataRow.CreateCell(3);
                        storeCell.SetCellValue(CityManager.CityTownShopStringGet(salesStore.AddressTownshipId) + salesStore.StoreAddress);
                        storeCell = dataRow.CreateCell(4);
                        storeCell.SetCellValue(salesStore.StoreTel);
                        storeCell = dataRow.CreateCell(5);
                        storeCell.SetCellValue(salesStore.OpeningTime);
                        storeCell = dataRow.CreateCell(6);
                        storeCell.SetCellValue(salesStore.CloseDate);

                        storeRowIndex++;
                        storeRowCount++;
                    }
                }
                #endregion

                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode(fileName)));
                workbook.Write(HttpContext.Current.Response.OutputStream);
            }
        }

        private void ExportPezStore(string fileName, IEnumerable<BusinessOrder> dataList)
        {
            if (dataList.Count() > 0)
            {
                Workbook workbook = new HSSFWorkbook();

                // 新增試算表
                Sheet store = workbook.CreateSheet("sheet1");

                #region 標頭
                Row storeHeaderRow = store.CreateRow(0);
                storeHeaderRow.CreateCell(0).SetCellValue("識別代碼*");
                storeHeaderRow.CreateCell(1).SetCellValue("有效相片序號*");
                storeHeaderRow.CreateCell(2).SetCellValue("分類名稱*");
                storeHeaderRow.CreateCell(3).SetCellValue("商店名稱*");
                storeHeaderRow.CreateCell(4).SetCellValue("分店名");
                storeHeaderRow.CreateCell(5).SetCellValue("優惠內容");
                storeHeaderRow.CreateCell(6).SetCellValue("縣市*");
                storeHeaderRow.CreateCell(7).SetCellValue("鄉鎮市區*");
                storeHeaderRow.CreateCell(8).SetCellValue("地址(道路門牌)*");
                storeHeaderRow.CreateCell(9).SetCellValue("交通資訊");
                storeHeaderRow.CreateCell(10).SetCellValue("電話*");
                storeHeaderRow.CreateCell(11).SetCellValue("營業時間");
                storeHeaderRow.CreateCell(12).SetCellValue("公休日");
                storeHeaderRow.CreateCell(13).SetCellValue("官方網站");
                storeHeaderRow.CreateCell(14).SetCellValue("推薦商品");
                storeHeaderRow.CreateCell(15).SetCellValue("商店簡介");
                storeHeaderRow.CreateCell(16).SetCellValue("其它資訊");
                storeHeaderRow.CreateCell(17).SetCellValue("分類標籤");
                storeHeaderRow.CreateCell(18).SetCellValue("是否開放於列表*");
                storeHeaderRow.CreateCell(19).SetCellValue("合約生效日期");
                storeHeaderRow.CreateCell(20).SetCellValue("合約結束日期");
                storeHeaderRow.CreateCell(21).SetCellValue("平均消費");
                storeHeaderRow.CreateCell(22).SetCellValue("刷卡");
                storeHeaderRow.CreateCell(23).SetCellValue("特約停車場");
                storeHeaderRow.CreateCell(24).SetCellValue("停車場備註");
                storeHeaderRow.CreateCell(25).SetCellValue("外送服務");
                storeHeaderRow.CreateCell(26).SetCellValue("外送服務備註");
                storeHeaderRow.CreateCell(27).SetCellValue("吸煙");
                storeHeaderRow.CreateCell(28).SetCellValue("可預約");
                storeHeaderRow.CreateCell(29).SetCellValue("包廂");
                storeHeaderRow.CreateCell(30).SetCellValue("包廂備註");
                storeHeaderRow.CreateCell(31).SetCellValue("會議設施");
                storeHeaderRow.CreateCell(32).SetCellValue("會議設施備註");
                storeHeaderRow.CreateCell(33).SetCellValue("可否攜帶寵物");
                storeHeaderRow.CreateCell(34).SetCellValue("宴會");
                storeHeaderRow.CreateCell(35).SetCellValue("宴會最大人數");
                storeHeaderRow.CreateCell(36).SetCellValue("24H營業");
                storeHeaderRow.CreateCell(37).SetCellValue("可上網");
                storeHeaderRow.CreateCell(38).SetCellValue("特色商品1");
                storeHeaderRow.CreateCell(39).SetCellValue("價位1");
                storeHeaderRow.CreateCell(40).SetCellValue("特賣優惠1");
                storeHeaderRow.CreateCell(41).SetCellValue("商品說明1");
                storeHeaderRow.CreateCell(42).SetCellValue("特色商品2");
                storeHeaderRow.CreateCell(43).SetCellValue("價位2");
                storeHeaderRow.CreateCell(44).SetCellValue("特賣優惠2");
                storeHeaderRow.CreateCell(45).SetCellValue("商品說明2");
                storeHeaderRow.CreateCell(46).SetCellValue("特色商品3");
                storeHeaderRow.CreateCell(47).SetCellValue("價位3");
                storeHeaderRow.CreateCell(48).SetCellValue("特賣優惠3");
                storeHeaderRow.CreateCell(49).SetCellValue("商品說明3");
                storeHeaderRow.CreateCell(50).SetCellValue("定位緯度");
                storeHeaderRow.CreateCell(51).SetCellValue("定位經度");
                storeHeaderRow.CreateCell(52).SetCellValue("商店聯絡人*");
                storeHeaderRow.CreateCell(53).SetCellValue("商店聯絡人電話*");
                storeHeaderRow.CreateCell(54).SetCellValue("商店聯絡人手機號碼*");
                storeHeaderRow.CreateCell(55).SetCellValue("商店聯絡人Email*");
                storeHeaderRow.CreateCell(56).SetCellValue("商店聯絡人傳真");
                storeHeaderRow.CreateCell(57).SetCellValue("業務來源");
                storeHeaderRow.CreateCell(58).SetCellValue("維護業務");
                storeHeaderRow.CreateCell(59).SetCellValue("地區");
                storeHeaderRow.CreateCell(60).SetCellValue("Kidowi(二項+/)");
                storeHeaderRow.CreateCell(61).SetCellValue("台新分類-1(選一種+地區)");
                storeHeaderRow.CreateCell(62).SetCellValue("台新專案代碼");
                storeHeaderRow.CreateCell(63).SetCellValue("優惠簡介");
                #endregion

                Cell storeCell = null;
                int storeRowCount = 0;
                int storeRowIndex = 1;
                foreach (BusinessOrder item in dataList)
                {
                    Row dataRow = store.CreateRow(storeRowIndex);

                    #region 總店
                    storeCell = dataRow.CreateCell(0);
                    storeCell.SetCellValue(string.Format("{0}{1}", item.GuiNumber, (item.Store.Count > 0 ? "(1)" : string.Empty))); // "識別代碼*
                    storeCell = dataRow.CreateCell(1);
                    storeCell.SetCellValue(string.Empty); // "有效相片序號*
                    storeCell = dataRow.CreateCell(2);
                    storeCell.SetCellValue(string.Empty); // "分類名稱*
                    storeCell = dataRow.CreateCell(3);
                    storeCell.SetCellValue(item.SellerName); // "商店名稱*
                    storeCell = dataRow.CreateCell(4);
                    storeCell.SetCellValue(item.StoreName); // "分店名
                    storeCell = dataRow.CreateCell(5);
                    storeCell.SetCellValue(item.PezStore + item.PezStoreNotice + protectRule); // "優惠內容 + 注意事項 + 保護條款
                    storeCell = dataRow.CreateCell(6);
                    City city = CityManager.CityGetById(item.AddressCityId);
                    storeCell.SetCellValue(city != null ? city.CityName : string.Empty); // "縣市*
                    storeCell = dataRow.CreateCell(7);
                    City township = CityManager.TownShipGetById(item.AddressTownshipId);
                    storeCell.SetCellValue(township != null ? township.CityName : string.Empty); // "鄉鎮市區*
                    storeCell = dataRow.CreateCell(8);
                    storeCell.SetCellValue(item.SellerAddress); // "地址(道路門牌)*
                    storeCell = dataRow.CreateCell(9);
                    storeCell.SetCellValue(item.VehicleInfo); // "交通資訊
                    storeCell = dataRow.CreateCell(10);
                    storeCell.SetCellValue(item.ReservationTel); // "電話*
                    storeCell = dataRow.CreateCell(11);
                    storeCell.SetCellValue(item.OpeningTime); // "營業時間
                    storeCell = dataRow.CreateCell(12);
                    storeCell.SetCellValue(item.CloseDate); // "公休日
                    storeCell = dataRow.CreateCell(13);
                    storeCell.SetCellValue(item.WebUrl); // "官方網站
                    storeCell = dataRow.CreateCell(14);
                    storeCell.SetCellValue(string.Empty); // "推薦商品
                    storeCell = dataRow.CreateCell(15);
                    storeCell.SetCellValue(item.SellerDescription); // "商店簡介
                    storeCell = dataRow.CreateCell(16);
                    storeCell.SetCellValue(string.Empty); // "其它資訊
                    storeCell = dataRow.CreateCell(17);
                    storeCell.SetCellValue(string.Empty); // "分類標籤
                    storeCell = dataRow.CreateCell(18);
                    storeCell.SetCellValue("是"); // "是否開放於列表*
                    storeCell = dataRow.CreateCell(19);
                    storeCell.SetCellValue(item.PezStoreDateStart != null ? item.PezStoreDateStart.Value.ToLocalTime().ToString("yyyy/MM/dd") : string.Empty); // "合約生效日期
                    storeCell = dataRow.CreateCell(20);
                    storeCell.SetCellValue(item.PezStoreDateEnd != null ? item.PezStoreDateEnd.Value.ToLocalTime().ToString("yyyy/MM/dd") : string.Empty); // "合約結束日期
                    storeCell = dataRow.CreateCell(21);
                    storeCell.SetCellValue(item.PerCustomerTransDesc); // "平均消費
                    storeCell = dataRow.CreateCell(22);
                    storeCell.SetCellValue(string.Empty); // "刷卡
                    storeCell = dataRow.CreateCell(23);
                    storeCell.SetCellValue(string.Empty); // "特約停車場
                    storeCell = dataRow.CreateCell(24);
                    storeCell.SetCellValue(string.Empty); // "停車場備註
                    storeCell = dataRow.CreateCell(25);
                    storeCell.SetCellValue(string.Empty); // "外送服務
                    storeCell = dataRow.CreateCell(26);
                    storeCell.SetCellValue(string.Empty); // "外送服務備註
                    storeCell = dataRow.CreateCell(27);
                    storeCell.SetCellValue(string.Empty); // "吸煙
                    storeCell = dataRow.CreateCell(28);
                    storeCell.SetCellValue(string.Empty); // "可預約
                    storeCell = dataRow.CreateCell(29);
                    storeCell.SetCellValue(string.Empty); // "包廂
                    storeCell = dataRow.CreateCell(30);
                    storeCell.SetCellValue(string.Empty); // "包廂備註
                    storeCell = dataRow.CreateCell(31);
                    storeCell.SetCellValue(string.Empty); // "會議設施
                    storeCell = dataRow.CreateCell(32);
                    storeCell.SetCellValue(string.Empty); // "會議設施備註
                    storeCell = dataRow.CreateCell(33);
                    storeCell.SetCellValue(string.Empty); // "可否攜帶寵物
                    storeCell = dataRow.CreateCell(34);
                    storeCell.SetCellValue(string.Empty); // "宴會
                    storeCell = dataRow.CreateCell(35);
                    storeCell.SetCellValue(string.Empty); // "宴會最大人數
                    storeCell = dataRow.CreateCell(36);
                    storeCell.SetCellValue(string.Empty); // "24H營業
                    storeCell = dataRow.CreateCell(37);
                    storeCell.SetCellValue(string.Empty); // "可上網
                    storeCell = dataRow.CreateCell(38);
                    storeCell.SetCellValue(string.Empty); // "特色商品1
                    storeCell = dataRow.CreateCell(39);
                    storeCell.SetCellValue(string.Empty); // "價位1
                    storeCell = dataRow.CreateCell(40);
                    storeCell.SetCellValue(string.Empty); // "特賣優惠1
                    storeCell = dataRow.CreateCell(41);
                    storeCell.SetCellValue(string.Empty); // "商品說明1
                    storeCell = dataRow.CreateCell(42);
                    storeCell.SetCellValue(string.Empty); // "特色商品2
                    storeCell = dataRow.CreateCell(43);
                    storeCell.SetCellValue(string.Empty); // "價位2
                    storeCell = dataRow.CreateCell(44);
                    storeCell.SetCellValue(string.Empty); // "特賣優惠2
                    storeCell = dataRow.CreateCell(45);
                    storeCell.SetCellValue(string.Empty); // "商品說明2
                    storeCell = dataRow.CreateCell(46);
                    storeCell.SetCellValue(string.Empty); // "特色商品3
                    storeCell = dataRow.CreateCell(47);
                    storeCell.SetCellValue(string.Empty); // "價位3
                    storeCell = dataRow.CreateCell(48);
                    storeCell.SetCellValue(string.Empty); // "特賣優惠3
                    storeCell = dataRow.CreateCell(49);
                    storeCell.SetCellValue(string.Empty); // "商品說明3
                    string[] array = GetLatitudeAndLongtitude(item.SellerAddress);
                    storeCell = dataRow.CreateCell(50);
                    storeCell.SetCellValue(array[1]); // "定位緯度(其實是經度)
                    storeCell = dataRow.CreateCell(51);
                    storeCell.SetCellValue(array[0]); // "定位經度(其實是緯度)
                    storeCell = dataRow.CreateCell(52);
                    storeCell.SetCellValue(item.ContactPerson); // "商店聯絡人*
                    storeCell = dataRow.CreateCell(53);
                    storeCell.SetCellValue(item.BossPhone); // "商店聯絡人電話*
                    storeCell = dataRow.CreateCell(54);
                    storeCell.SetCellValue(item.BossTel); // "商店聯絡人手機號碼*
                    storeCell = dataRow.CreateCell(55);
                    storeCell.SetCellValue(item.BossEmail); // "商店聯絡人Email*
                    storeCell = dataRow.CreateCell(56);
                    storeCell.SetCellValue(item.BossFax); // "商店聯絡人傳真
                    storeCell = dataRow.CreateCell(57);
                    storeCell.SetCellValue(item.SalesName); // "業務來源
                    storeCell = dataRow.CreateCell(58);
                    storeCell.SetCellValue(string.Empty); // "維護業務
                    storeCell = dataRow.CreateCell(59);
                    storeCell.SetCellValue(string.Empty); // "地區
                    storeCell = dataRow.CreateCell(60);
                    storeCell.SetCellValue(string.Empty); // "Kidowi(二項+/)
                    storeCell = dataRow.CreateCell(61);
                    storeCell.SetCellValue(string.Empty); // "台新分類-1(選一種+地區)
                    storeCell = dataRow.CreateCell(62);
                    storeCell.SetCellValue(string.Empty); // "台新專案代碼
                    storeCell = dataRow.CreateCell(63);
                    storeCell.SetCellValue(string.Empty); // "優惠簡介
                    #endregion

                    storeRowIndex++;
                    storeRowCount++;

                    for (int i = 0; i < item.Store.Count; i++)
                    {

                        Row storeDataRow = store.CreateRow(storeRowIndex);

                        #region 分店
                        SalesStore salesStore = item.Store[i];
                        storeCell = storeDataRow.CreateCell(0);
                        storeCell.SetCellValue(string.Format("{0}({1})", item.GuiNumber, i + 2)); // "識別代碼*
                        storeCell = storeDataRow.CreateCell(1);
                        storeCell.SetCellValue(string.Empty); // "有效相片序號*
                        storeCell = storeDataRow.CreateCell(2);
                        storeCell.SetCellValue(string.Empty); // "分類名稱*
                        storeCell = storeDataRow.CreateCell(3);
                        storeCell.SetCellValue(item.SellerName); // "商店名稱*
                        storeCell = storeDataRow.CreateCell(4);
                        storeCell.SetCellValue(salesStore.BranchName); // "分店名
                        storeCell = storeDataRow.CreateCell(5);
                        storeCell.SetCellValue(item.PezStore + item.PezStoreNotice + protectRule); // "優惠內容 + 注意事項 + 保護條款
                        storeCell = storeDataRow.CreateCell(6);
                        City storeCity = CityManager.CityGetById(salesStore.AddressCityId);
                        storeCell.SetCellValue(storeCity != null ? storeCity.CityName : string.Empty); // "縣市*
                        storeCell = storeDataRow.CreateCell(7);
                        City storeTownship = CityManager.TownShipGetById(salesStore.AddressTownshipId);
                        storeCell.SetCellValue(storeTownship != null ? storeTownship.CityName : string.Empty); // "鄉鎮市區*
                        storeCell = storeDataRow.CreateCell(8);
                        storeCell.SetCellValue(salesStore.StoreAddress); // "地址(道路門牌)*
                        storeCell = storeDataRow.CreateCell(9);
                        storeCell.SetCellValue(salesStore.OV); // "交通資訊
                        storeCell = storeDataRow.CreateCell(10);
                        storeCell.SetCellValue(salesStore.StoreTel); // "電話*
                        storeCell = storeDataRow.CreateCell(11);
                        storeCell.SetCellValue(salesStore.OpeningTime); // "營業時間
                        storeCell = storeDataRow.CreateCell(12);
                        storeCell.SetCellValue(salesStore.CloseDate); // "公休日
                        storeCell = storeDataRow.CreateCell(13);
                        storeCell.SetCellValue(salesStore.WebUrl); // "官方網站
                        storeCell = storeDataRow.CreateCell(14);
                        storeCell.SetCellValue(string.Empty); // "推薦商品
                        storeCell = storeDataRow.CreateCell(15);
                        storeCell.SetCellValue(item.SellerDescription); // "商店簡介
                        storeCell = storeDataRow.CreateCell(16);
                        storeCell.SetCellValue(string.Empty); // "其它資訊
                        storeCell = storeDataRow.CreateCell(17);
                        storeCell.SetCellValue(string.Empty); // "分類標籤
                        storeCell = storeDataRow.CreateCell(18);
                        storeCell.SetCellValue("是"); // "是否開放於列表*
                        storeCell = storeDataRow.CreateCell(19);
                        storeCell.SetCellValue(item.PezStoreDateStart != null ? item.PezStoreDateStart.Value.ToLocalTime().ToString("yyyy/MM/dd") : string.Empty); // "合約生效日期
                        storeCell = storeDataRow.CreateCell(20);
                        storeCell.SetCellValue(item.PezStoreDateEnd != null ? item.PezStoreDateEnd.Value.ToLocalTime().ToString("yyyy/MM/dd") : string.Empty); // "合約結束日期
                        storeCell = storeDataRow.CreateCell(21);
                        storeCell.SetCellValue(item.PerCustomerTransDesc); // "平均消費
                        storeCell = storeDataRow.CreateCell(22);
                        storeCell.SetCellValue(string.Empty); // "刷卡
                        storeCell = storeDataRow.CreateCell(23);
                        storeCell.SetCellValue(string.Empty); // "特約停車場
                        storeCell = storeDataRow.CreateCell(24);
                        storeCell.SetCellValue(string.Empty); // "停車場備註
                        storeCell = storeDataRow.CreateCell(25);
                        storeCell.SetCellValue(string.Empty); // "外送服務
                        storeCell = storeDataRow.CreateCell(26);
                        storeCell.SetCellValue(string.Empty); // "外送服務備註
                        storeCell = storeDataRow.CreateCell(27);
                        storeCell.SetCellValue(string.Empty); // "吸煙
                        storeCell = storeDataRow.CreateCell(28);
                        storeCell.SetCellValue(string.Empty); // "可預約
                        storeCell = storeDataRow.CreateCell(29);
                        storeCell.SetCellValue(string.Empty); // "包廂
                        storeCell = storeDataRow.CreateCell(30);
                        storeCell.SetCellValue(string.Empty); // "包廂備註
                        storeCell = storeDataRow.CreateCell(31);
                        storeCell.SetCellValue(string.Empty); // "會議設施
                        storeCell = storeDataRow.CreateCell(32);
                        storeCell.SetCellValue(string.Empty); // "會議設施備註
                        storeCell = storeDataRow.CreateCell(33);
                        storeCell.SetCellValue(string.Empty); // "可否攜帶寵物
                        storeCell = storeDataRow.CreateCell(34);
                        storeCell.SetCellValue(string.Empty); // "宴會
                        storeCell = storeDataRow.CreateCell(35);
                        storeCell.SetCellValue(string.Empty); // "宴會最大人數
                        storeCell = storeDataRow.CreateCell(36);
                        storeCell.SetCellValue(string.Empty); // "24H營業
                        storeCell = storeDataRow.CreateCell(37);
                        storeCell.SetCellValue(string.Empty); // "可上網
                        storeCell = storeDataRow.CreateCell(38);
                        storeCell.SetCellValue(string.Empty); // "特色商品1
                        storeCell = storeDataRow.CreateCell(39);
                        storeCell.SetCellValue(string.Empty); // "價位1
                        storeCell = storeDataRow.CreateCell(40);
                        storeCell.SetCellValue(string.Empty); // "特賣優惠1
                        storeCell = storeDataRow.CreateCell(41);
                        storeCell.SetCellValue(string.Empty); // "商品說明1
                        storeCell = storeDataRow.CreateCell(42);
                        storeCell.SetCellValue(string.Empty); // "特色商品2
                        storeCell = storeDataRow.CreateCell(43);
                        storeCell.SetCellValue(string.Empty); // "價位2
                        storeCell = storeDataRow.CreateCell(44);
                        storeCell.SetCellValue(string.Empty); // "特賣優惠2
                        storeCell = storeDataRow.CreateCell(45);
                        storeCell.SetCellValue(string.Empty); // "商品說明2
                        storeCell = storeDataRow.CreateCell(46);
                        storeCell.SetCellValue(string.Empty); // "特色商品3
                        storeCell = storeDataRow.CreateCell(47);
                        storeCell.SetCellValue(string.Empty); // "價位3
                        storeCell = storeDataRow.CreateCell(48);
                        storeCell.SetCellValue(string.Empty); // "特賣優惠3
                        storeCell = storeDataRow.CreateCell(49);
                        storeCell.SetCellValue(string.Empty); // "商品說明3
                        string[] storeArray = GetLatitudeAndLongtitude(item.SellerAddress);
                        storeCell = storeDataRow.CreateCell(50);
                        storeCell.SetCellValue(storeArray[1]); // "定位緯度(其實是經度)
                        storeCell = storeDataRow.CreateCell(51);
                        storeCell.SetCellValue(storeArray[0]); // "定位經度(其實是緯度)
                        storeCell = storeDataRow.CreateCell(52);
                        storeCell.SetCellValue(item.ContactPerson); // "商店聯絡人*
                        storeCell = storeDataRow.CreateCell(53);
                        storeCell.SetCellValue(item.BossPhone); // "商店聯絡人電話*
                        storeCell = storeDataRow.CreateCell(54);
                        storeCell.SetCellValue(item.BossTel); // "商店聯絡人手機號碼*
                        storeCell = storeDataRow.CreateCell(55);
                        storeCell.SetCellValue(item.BossEmail); // "商店聯絡人Email*
                        storeCell = storeDataRow.CreateCell(56);
                        storeCell.SetCellValue(item.BossFax); // "商店聯絡人傳真
                        storeCell = storeDataRow.CreateCell(57);
                        storeCell.SetCellValue(item.SalesName); // "業務來源
                        storeCell = storeDataRow.CreateCell(58);
                        storeCell.SetCellValue(string.Empty); // "維護業務
                        storeCell = storeDataRow.CreateCell(59);
                        storeCell.SetCellValue(string.Empty); // "地區
                        storeCell = storeDataRow.CreateCell(60);
                        storeCell.SetCellValue(string.Empty); // "Kidowi(二項+/)
                        storeCell = storeDataRow.CreateCell(61);
                        storeCell.SetCellValue(string.Empty); // "台新分類-1(選一種+地區)
                        storeCell = storeDataRow.CreateCell(62);
                        storeCell.SetCellValue(string.Empty); // "台新專案代碼
                        storeCell = storeDataRow.CreateCell(63);
                        storeCell.SetCellValue(string.Empty); // "優惠簡介
                        #endregion

                        storeRowIndex++;
                        storeRowCount++;
                    }
                }

                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode(fileName)));
                workbook.Write(HttpContext.Current.Response.OutputStream);
            }
        }

        /// <summary>
        /// 查詢經緯度(緯度:array[0] 經度:array[1])
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        private string[] GetLatitudeAndLongtitude(string address)
        {
            string[] result = new string[] { };
            SqlGeography geo = LocationFacade.GetGeography(address);
            if (geo != null)
            {
                result = new string[] { geo.Lat.ToString(), geo.Long.ToString() };
            }
            return result;
        }
        #endregion
    }
}
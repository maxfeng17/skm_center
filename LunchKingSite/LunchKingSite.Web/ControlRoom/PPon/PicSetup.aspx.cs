﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class PicSetup : RolePage, IPicSetupView
    {
        #region property
        public Guid BusinessHourGuid
        {
            get
            {
                Guid bid;
                if (Request.QueryString["bid"] != null && Guid.TryParse(Request.QueryString["bid"].ToString(), out bid))
                {
                    tbx_Bid.Text = bid.ToString();
                    return bid;
                }
                else if (Guid.TryParse(tbx_Bid.Text, out bid))
                {
                    return bid;
                }
                else
                {
                    return Guid.Empty;
                }
            }
        }

        public bool IsPrintWatermark
        {
            get
            {
                return chkIsPrintWatermark.Checked;
            }
        }

        private PicSetupPresenter _presenter;

        public PicSetupPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public PponSetupImageUploadType SelectImageUploadType
        {
            get
            {
                PponSetupImageUploadType uploadType;
                if (Enum.TryParse(rbl_Type.SelectedValue, out uploadType))
                {
                    return uploadType;
                }
                else
                {
                    return PponSetupImageUploadType.None;
                }
            }
        }

        public string ImagePath
        {
            get
            {
                return Request.Form["is"] ?? string.Empty;
            }
        }

        public string SpecialImagePath
        {
            get
            {
                return Request.Form["spIs"] ?? string.Empty;
            }
        }

        public string TravelEdmSpecialImagePath
        {
            get
            {
                return Request.Form["trspIs"] ?? string.Empty;
            }
        }

        public List<HttpPostedFile> PostedFiles
        {
            get
            {
                List<HttpPostedFile> fileList = new List<HttpPostedFile>();
                if (fuI.HasFile)
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        if (Request.Files.AllKeys[i].Contains(fuI.ID, StringComparison.OrdinalIgnoreCase))
                        {
                            fileList.Add(Request.Files[i]);
                        }
                    }
                }
                return fileList;

            }
        }
        #endregion

        #region event
        public event EventHandler SavePic;
        public event EventHandler GetCouponEventContent;
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!_presenter.OnViewInitialized())
                {
                    return;
                }
                rbl_Type.DataSource = new ListItemCollection() { 
                    new ListItem("按順序上圖(1,2,3,其他輪播大圖)", ((int)PponSetupImageUploadType.ByORder).ToString())
                ,new ListItem("上輪播大圖",((int)PponSetupImageUploadType.BigPic).ToString())
                ,new ListItem("旅遊eDM專屬大圖740x230",((int)PponSetupImageUploadType.TravelEdmSpecialBigPic).ToString())};
                rbl_Type.DataTextField = "Text";
                rbl_Type.DataValueField = "Value";
                rbl_Type.DataBind();
                rbl_Type.SelectedIndex = 0;
            }
            _presenter.OnViewLoaded();
        }

        protected void SearchCouponEventContent(object sender, EventArgs e)
        {
            if (GetCouponEventContent != null)
            {
                GetCouponEventContent(sender, e);
            }
        }

        protected void Save(object sender, EventArgs e)
        {
            if (SavePic != null)
            {
                SavePic(sender, e);
            }
        }
        #endregion

        #region method
        public void GetCouponEventContentInfo(CouponEventContent content, string message)
        {
            string[] images = ImageFacade.GetMediaPathsFromRawData(content.ImagePath, MediaType.PponDealPhoto);
            ri.DataSource = images.Select((x, i) => new { ImgPath = x, Index = i }).Where(x => x.Index != 1 && x.Index != 2 && x.ImgPath != string.Empty);
            ri.DataBind();
            ri_2.DataSource = images.Where((x, i) => i == 1 && x != string.Empty);
            ri_2.DataBind();
            ri_3.DataSource = images.Where((x, i) => i == 2 && x != string.Empty);
            ri_3.DataBind();
            //特殊規格圖片的顯示
            string[] spImage = ImageFacade.GetMediaPathsFromRawData(content.SpecialImagePath, MediaType.PponDealPhoto);
            rpSpImg.DataSource = spImage.Select((x, i) => new { ImgPath = x, Index = i }).Where(x => x.ImgPath != string.Empty);
            rpSpImg.DataBind();
            //TravelEDMSpecialBigPic顯示
            string[] TravelEdmSpImage = ImageFacade.GetMediaPathsFromRawData(content.TravelEdmSpecialImagePath, MediaType.PponDealPhoto);
            rpTravelEDMSpImg.DataSource = TravelEdmSpImage.Select((x, i) => new { ImgPath = x, Index = i }).Where(x => x.ImgPath != string.Empty);
            rpTravelEDMSpImg.DataBind();
            lab_Message.Text = lab_Message2.Text = message;
            lab_Name.Text = content.Name;
        }
        #endregion
    }
}
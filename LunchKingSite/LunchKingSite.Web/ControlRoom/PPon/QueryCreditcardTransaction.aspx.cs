﻿using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Data;
using System.Web.UI;
using PaymentType = LunchKingSite.Core.PaymentType;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class QueryCreditcardTransaction : RolePage, ICreditcardTransactionView
    {
        public event EventHandler SearchClicked;

        public event EventHandler SearchPCashClicked;

        public event EventHandler RefundPCashClicked;

        public event EventHandler<DataEventArgs<int>> GetTransCount;

        public event EventHandler<DataEventArgs<int>> PageChanged;

        public event EventHandler<DataEventArgs<string[]>> CancelPcash;

        public event EventHandler<DataEventArgs<string[]>> ChargePcash;

        public event EventHandler<DataEventArgs<string>> ChargePcashBatch;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["tid"]))
                {
                    TransId = Request.QueryString["tid"].ToString();
                    Button1_Click(sender, e);
                }
                this.Presenter.OnViewInitialized();
                //讓Pager 不要在剛開啟網頁時就SetPaging, 因為資料龐大會造成效能低落
                gridPager.SetPagingWhenPageLoad = false;
            }
            this.Presenter.OnViewLoaded();
        }

        #region props

        private CreditcardTransactionPresenter _presenter;

        public CreditcardTransactionPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string CurrentUser
        {
            get
            {
                return User.Identity.Name;
            }
        }

        public string OrderId
        {
            get
            {
                return tOI.Text.Trim();
            }
            set
            {
                tOI.Text = value;
            }
        }

        public string Oid
        {
            get
            {
                return (ViewState != null && ViewState["oid"] != null) ? ViewState["oid"].ToString() : string.Empty;
            }
            set
            {
                ViewState["oid"] = value;
            }
        }

        public Guid OrderGuid
        {
            get
            {
                return (ViewState != null && ViewState["orderguid"] != null) ? (Guid)ViewState["orderguid"] : Guid.Empty;
            }
            set
            {
                ViewState["orderguid"] = value;
            }
        }

        public string TransId
        {
            get
            {
                return tTI.Text.Trim();
            }
            set
            {
                tTI.Text = value;
            }
        }

        public string CreateId
        {
            get
            {
                return tCI.Text.Trim();
            }
            set
            {
                tCI.Text = value;
            }
        }

        public string TransTimeBegin
        {
            get
            {
                return tTB.Text.Trim();
            }
            set
            {
                tTB.Text = value;
            }
        }

        public string TransTimeEnd
        {
            get
            {
                return tTE.Text.Trim();
            }
            set
            {
                tTE.Text = value;
            }
        }

        public string Msg
        {
            get
            {
                return tMS.Text.Trim();
            }
            set
            {
                tMS.Text = value;
            }
        }

        public string AuthCode
        {
            get
            {
                return tbAuthcode.Text.Trim();
            }
            set
            {
                tbAuthcode.Text = value;
            }
        }

        public int? Classification
        {
            get
            {
                if (rbClassification.SelectedValue == "")
                {
                    return null;
                }
                return int.Parse(rbClassification.SelectedValue);
            }
            set
            {
                rbClassification.SelectedValue = value.ToString();
            }
        }

        public string ErrShow
        {
            get
            {
                return lErr.Text;
            }
            set
            {
                lErr.Text = value;
            }
        }

        public int PageSize
        {
            get
            {
                return gridPager.PageSize;
            }
            set
            {
                gridPager.PageSize = value;
            }
        }

        public string BatchResult
        {
            get
            {
                return lbBatchResult.Text;
            }
            set
            {
                lbBatchResult.Text = value;
            }
        }

        #endregion props

        public void SetPCashGridView(PaymentTransactionCollection ptc)
        {
            GridView1.DataSource = ptc;
            GridView1.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (this.SearchClicked != null)
            {
                this.SearchClicked(this, e);
            }
            gridPager.ResolvePagerView(1, true);
        }

        public void SetGridTrans(ViewPaymentTransactionLeftjoinOrderCollection orders)
        {
            gridTrans.DataSource = orders;
            gridTrans.DataBind();
        }

        protected string GetPayType(string payType)
        {
            int p = int.Parse(payType);
            switch (p)
            {
                case (int)PaymentType.Creditcard:
                    return Phrase.PayTypeCreditcard;

                case (int)PaymentType.PCash:
                    return Phrase.PayTypePcash;

                case (int)PaymentType.BonusPoint:
                    return Phrase.PayTypeBonus;

                default:
                    return payType;
            }
        }

        protected string GetTransType(string transType)
        {
            int t = int.Parse(transType);
            switch (t)
            {
                case (int)PayTransType.Authorization:
                    return Phrase.PayTransAuthenrization;

                case (int)PayTransType.Charging:
                    return Phrase.PayTransCharging;

                case (int)PayTransType.Refund:
                    return Phrase.PayTransRefund;

                default:
                    return transType;
            }
        }

        protected string GetPayStatus(string status)
        {
            int s = int.Parse(status) & 7;

            switch (s)
            {
                case (int)PayTransPhase.Created:
                    return Phrase.PayStatusCreated;

                case (int)PayTransPhase.Requested:
                    return Phrase.PayStatusRequested;

                case (int)PayTransPhase.Canceled:
                    return Phrase.PayStatusCanceled;

                case (int)PayTransPhase.Successful:
                    return Phrase.PayStatusSuccessful;

                case (int)PayTransPhase.Failed:
                    return Phrase.PayStatusFailed;

                case (int)PayTransPhase.Transfered:
                    return Phrase.PayStatusTransfered;

                default:
                    return status;
            }
        }

        protected string GetPayResult(string result)
        {
            int r = int.Parse(result);
            switch (r)
            {
                case (int)PayTransResponseType.Initial:
                    return Phrase.PayResultInitial;

                case (int)PayTransResponseType.OK:
                    return Phrase.PayResultOk;

                default:
                    return result;
            }
        }

        protected string GetApiProvider(string apiProvider) 
        {
            int pro;
            if (int.TryParse(apiProvider, out pro))
            {
                switch (pro) 
                { 
                    case (int)PaymentAPIProvider.ForgionCard:
                        return PaymentAPIProvider.ForgionCard.ToString();
                    case (int)PaymentAPIProvider.HiTrust:
                        return PaymentAPIProvider.HiTrust.ToString();
                    case (int)PaymentAPIProvider.HiTrustPayeasyTravel:
                        return PaymentAPIProvider.HiTrustPayeasyTravel.ToString();
                    case (int)PaymentAPIProvider.HiTrustPiinLife:
                        return PaymentAPIProvider.HiTrustPiinLife.ToString();
                    case (int)PaymentAPIProvider.HiTrustPiinLifeUnionPay:
                        return PaymentAPIProvider.HiTrustPiinLifeUnionPay.ToString();
                    case (int)PaymentAPIProvider.HiTrustComTest:
                        return PaymentAPIProvider.HiTrustComTest.ToString();
                    case (int)PaymentAPIProvider.HiTrustDotNetTest:
                        return PaymentAPIProvider.HiTrustDotNetTest.ToString();
                    case (int)PaymentAPIProvider.HiTrustUnionPay:
                        return PaymentAPIProvider.HiTrustUnionPay.ToString();
                    case (int)PaymentAPIProvider.Mock:
                        return PaymentAPIProvider.Mock.ToString();
                    case (int)PaymentAPIProvider.NCCC:
                        return PaymentAPIProvider.NCCC.ToString();
                    case (int)PaymentAPIProvider.Neweb:
                        return PaymentAPIProvider.Neweb.ToString();
                    case (int)PaymentAPIProvider.NewebPiinLife:
                        return PaymentAPIProvider.NewebPiinLife.ToString();
                    case (int)PaymentAPIProvider.NewebTest:
                        return PaymentAPIProvider.NewebTest.ToString();
                    case (int)PaymentAPIProvider.PezWebService:
                        return PaymentAPIProvider.PezWebService.ToString();
                    case (int)PaymentAPIProvider.NewebContactWithCVV2:
                        return PaymentAPIProvider.NewebContactWithCVV2.ToString();
                    case (int)PaymentAPIProvider.NewebContactWithoutCVV2:
                        return PaymentAPIProvider.NewebContactWithoutCVV2.ToString();
                    case (int)PaymentAPIProvider.NewebContactInstallment:
                        return PaymentAPIProvider.NewebContactInstallment.ToString();
                    case (int)PaymentAPIProvider.HiTrustContactWithCVV2:
                        return PaymentAPIProvider.HiTrustContactWithCVV2.ToString();
                    case (int)PaymentAPIProvider.HiTrustContactWithoutCVV2:
                        return PaymentAPIProvider.HiTrustContactWithoutCVV2.ToString();
                    default:
                        return string.Empty;
                }
            }
            else 
            {
                return string.Empty;
            }
        }

        public void SetPcashResult(string result)
        {
            lblResult.Text = result;
        }

        public void SetPcashResultC(string result)
        {
            lblResultC.Text = result;
        }

        protected int RetrieveTransCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (GetTransCount != null)
            {
                GetTransCount(this, e);
            }

            return e.Data;
        }

        protected void UpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.PageChanged != null)
            {
                this.PageChanged(this, e);
            }
        }

        protected void tmr1_Tick(object sender, EventArgs e)
        {
            if (this.PageChanged != null)
            {
                this.PageChanged(this, new DataEventArgs<int>(gridPager.CurrentPage));
            }
            gridPager.ResolvePagerView(gridPager.CurrentPage, true);
        }

        protected void ddlTmr_SIC(object sender, EventArgs e)
        {
            if (ddlTmr.SelectedValue == "0")
            {
                tmr1.Enabled = false;
            }
            else
            {
                tmr1.Enabled = true;
                tmr1.Interval = int.Parse(ddlTmr.SelectedValue) * 1000;
            }
        }

        protected void ddlPS_SIC(object sender, EventArgs e)
        {
            this.PageSize = int.Parse(ddlPS.SelectedValue);
            if (this.PageChanged != null)
            {
                this.PageChanged(this, new DataEventArgs<int>(gridPager.CurrentPage));
            }
            gridPager.ResolvePagerView(gridPager.CurrentPage, true);
        }

        protected void btnCancelPcash_Click(object sender, EventArgs e)
        {
            string[] sTemp = new string[3];
            sTemp[0] = txtTransId.Text;
            sTemp[1] = txtAuthCode.Text;
            sTemp[2] = txtAmount.Text;
            if (this.CancelPcash != null)
            {
                this.CancelPcash(this, new DataEventArgs<string[]>(sTemp));
            }
        }

        protected void btnChargePcash_Click(object sender, EventArgs e)
        {
            string[] sTemp = new string[2];
            sTemp[0] = txtTransIdC.Text;
            sTemp[1] = txtAmountC.Text;
            if (this.ChargePcash != null)
            {
                this.ChargePcash(this, new DataEventArgs<string[]>(sTemp));
            }
        }

        protected void btnChargePcashBatch_Click(object sender, EventArgs e)
        {
            if (this.ChargePcashBatch != null)
            {
                this.ChargePcashBatch(this, new DataEventArgs<string>(tbTransId.Text));
            }
        }

        protected void btnRefundPCash_Click(object sender, EventArgs e)
        {
            if (this.RefundPCashClicked != null)
            {
                this.RefundPCashClicked(this, null);
            }
        }

        protected void btnSearchPcash_Click(object sender, EventArgs e)
        {
            if (this.SearchPCashClicked != null)
            {
                this.SearchPCashClicked(this, null);
            }
        }

        public void SetRefundPcash(DataTable dt)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
}
﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    /// <summary>
    /// 後台檔次設定頁，Menu頁籤使用Ajax上傳圖檔時使用
    /// </summary>
    public class MenuFileHandler : IHttpHandler
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IPponProvider _pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISellerProvider _sellerProv = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        public void ProcessRequest(HttpContext context)
        {
            string param = context.Request["param"];
            string bid = context.Request["bid"];
            Guid BusinessHourId = Guid.Empty;
            Guid.TryParse(bid, out BusinessHourId);

            string filePath = "";
            try
            {
                if (context.Request.Files.Count > 0)
                {
                    Bitmap oBitmap = null;
                    BusinessHour bh = _pponProv.BusinessHourGet(BusinessHourId);
                    string sellerId = "";
                    int chgWidth = 560;
                    if (bh != null)
                    {
                        Seller seller = _sellerProv.SellerGet(bh.SellerGuid);
                        sellerId = seller.SellerId;
                    }
                    string baseDirectoryPath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/media/{0}/", sellerId));

                    if (!System.IO.Directory.Exists(baseDirectoryPath))
                    {
                        System.IO.Directory.CreateDirectory(baseDirectoryPath);
                    }

                    if (File.Exists(baseDirectoryPath + bid + "_Menu_Upload_S.png"))
                    {
                        File.Delete(baseDirectoryPath + bid + "_Menu_Upload_S.png");
                    }
                    if (File.Exists(baseDirectoryPath + bid + "_Menu_Upload.png"))
                    {
                        File.Delete(baseDirectoryPath + bid + "_Menu_Upload.png");
                    }


                    HttpPostedFile file = context.Request.Files[0];
                    file.SaveAs(baseDirectoryPath + BusinessHourId + "_Menu_Upload_S.png");

                    oBitmap = new Bitmap(baseDirectoryPath + BusinessHourId + "_Menu_Upload_S.png");

                    System.Drawing.Image image = System.Drawing.Image.FromFile(baseDirectoryPath + BusinessHourId + "_Menu_Upload_S.png");
                    System.Drawing.Image smallImage = image;
                    int oriWidth = image.Width;
                    int oriHeight = image.Height;
                    //縮圖
                    if (oriWidth > chgWidth)
                    {
                        decimal pctWidth = (Convert.ToDecimal(oriWidth) - Convert.ToDecimal(chgWidth)) / Convert.ToDecimal(oriWidth);
                        int chgHeight = Convert.ToInt32(oriHeight * pctWidth);

                        if (chgHeight > 0)
                        {
                            using (var newImage = new Bitmap(chgWidth, chgHeight))
                            {
                                newImage.SetResolution(image.HorizontalResolution, newImage.VerticalResolution);
                                var graphic = Graphics.FromImage(newImage);
                                graphic.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                                graphic.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                                graphic.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                                graphic.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;

                                graphic.DrawImage(image, 0, 0, chgWidth, chgHeight);
                                using (var encoderParameters = new EncoderParameters(1))
                                {
                                    encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, 100L);
                                    newImage.Save(baseDirectoryPath + BusinessHourId + "_Menu_Upload.png",
                                        ImageCodecInfo.GetImageEncoders().Where(x => x.FilenameExtension.Contains("png".ToUpperInvariant())).FirstOrDefault(),
                                        encoderParameters);
                                }

                                graphic.Dispose();
                            }
                        }
                    }
                    else
                    {
                        //不執行縮圖
                        oBitmap.Save(baseDirectoryPath + BusinessHourId + "_Menu_Upload.png", ImageFormat.Png);
                    }

                    filePath = string.Format("{0}/{1}/{2}_Menu_Upload.png", config.MediaBaseUrl, sellerId, BusinessHourId);

                    CouponEventContent cec = _pponProv.CouponEventContentGetByBid(BusinessHourId);
                    if (cec != null)
                    {
                        cec.MenuUploadImagePath = filePath;
                        _pponProv.CouponEventContentSet(cec);
                    }
                }
            }
            catch
            {
                filePath = "";
            }

            context.Response.ContentType = "text/plain";
            context.Response.Write(filePath);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
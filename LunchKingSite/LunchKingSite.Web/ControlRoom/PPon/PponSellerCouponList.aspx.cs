﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Data;
using System.Web;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class PponSellerCouponList : RolePage
    {
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Guid bid;
                if (!string.IsNullOrEmpty(Request["bid"]) && Guid.TryParse(Request["bid"], out bid))
                {
                    SetGrid(bid);
                    DealProperty dp = pp.DealPropertyGet(bid);
                    if (dp.AncestorBusinessHourGuid.HasValue)
                    {
                        ViewPponDeal anc_deal = pp.ViewPponDealGetByBusinessHourGuid(dp.AncestorBusinessHourGuid.Value);
                        lit_Continued.Text = string.Format("(接續檔號：{0}，", anc_deal.UniqueId);
                        if (dp.IsContinuedSequence)
                        {
                            int oCount = OrderFacade.GetAncestorCouponCount(bid);
                            lit_Continued.Text += string.Format("接續憑證號碼，接續號碼：{0};", (oCount + 1));
                        }

                        if (dp.IsContinuedQuantity)
                        {
                            ViewPponDeal deal = pp.ViewPponDealGetByBusinessHourGuid(bid);
                            lit_Continued.Text += string.Format("接續銷售數量，接續數量：{0};", ((deal.ContinuedQuantity ?? 0) + 1));
                        }
                        lit_Continued.Text += ")";
                    }
                }
                else
                {
                    Response.Write("<script type='text/javascript'>alert('Bid錯誤');</script>");
                }
            }
        }

        private void SetGrid(Guid bid)
        {
            DataTable dt = pp.GetSellerCouponList(bid);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void btnToExcel_Click(object sender, EventArgs e)
        {
            Guid bid = new Guid(Request["bid"]);
            //CashTrustLogCollection ctlCols = mp.CashTrustLogGetListByBid(bid);
            pp.UpdateSellerCouponListExportLog(bid, User.Identity.Name);
            ViewPponDeal vp = pp.ViewPponDealGetByBusinessHourGuid(bid);
            DataTable dt = pp.GetSellerCouponList(bid);
            string _couponusage = string.Empty;
            _couponusage = vp.CouponUsage;

            if (!string.IsNullOrEmpty(_couponusage) && _couponusage.Length > 50)
            {
                _couponusage = _couponusage.Substring(0, 49) + "...";
            }

            Export(vp.BusinessHourOrderTimeS.ToString("yyyy-MM-dd") + "【" + _couponusage + "】-商家Coupon對應名單.xls", dt);
        }

        public static void Export(string fileName, DataTable sdt)
        {
            if (sdt.Rows.Count > 0)
            {
                Workbook workbook = new HSSFWorkbook();

                // 新增試算表。
                Sheet sheet = workbook.CreateSheet("參加名單");
                Row HeaderRow = sheet.CreateRow(0);
                foreach (DataColumn column in sdt.Columns)
                {
                    HeaderRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);
                }

                Cell c = null;
                int rowCount = 0;
                int rowIndex = 1;
                foreach (DataRow row in sdt.Rows)
                {
                    Row dataRow = sheet.CreateRow(rowIndex);
                    foreach (DataColumn column in sdt.Columns)
                    {
                        c = dataRow.CreateCell(column.Ordinal);
                        c.SetCellValue(column.ColumnName == "status" ? GetCashTrustLogStatus(Convert.ToInt32(row[column])) : row[column].ToString());
                    }

                    rowIndex++;
                    rowCount++;
                }

                string browser = HttpContext.Current.Request.UserAgent.ToUpper();
                //修正FireFox 中文檔名顯示問題
                if (browser.Contains("IE") == true || browser.Contains("CHROME") == true)
                {
                    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlPathEncode(fileName)));
                }
                else if (browser.Contains("FIREFOX") == true || browser.Contains("SAFARI") == true)
                {
                    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", fileName));
                }
                else if (browser.Contains("OPERA") == true)
                {
                    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", fileName));
                    HttpContext.Current.Response.ContentType = "application/ms-excel";  //Opera 無此行會變成Html
                }
                else
                {
                    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlPathEncode(fileName)));
                }

                workbook.Write(HttpContext.Current.Response.OutputStream);
            }
        }

        private static string GetCashTrustLogStatus(int status) 
        {
            switch (status) 
            { 
                case (int)TrustStatus.Refunded:
                    return "已退貨";
                case (int)TrustStatus.Returned:
                    return "已退貨";
                default:
                    return string.Empty;
            }
        }
    }
}
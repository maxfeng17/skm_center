﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using System;
using System.Web.UI;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class PponReturnApplication : RolePage
    {
        protected IOrderProvider op = null;
        protected IPponProvider pp = null;
        protected IMemberProvider mp = null;
        protected ISysConfProvider config;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
                pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
                mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
                config = ProviderFactory.Instance().GetConfig();

                if (!string.IsNullOrEmpty(Request.QueryString["oid"]))
                {
                    Guid orderId = new Guid(Request.QueryString["oid"]);
                    DataOrm.Order o = op.OrderGet(orderId);
                    ViewPponCouponCollection coupons = pp.ViewPponCouponGetList(ViewPponCoupon.Columns.OrderGuid, orderId);
                    ViewPponDeal deal = pp.ViewPponDealGet(o.ParentOrderId.Value);
                    Member m = mp.MemberGet(o.UserId);
                    DealProperty dealproperty = pp.DealPropertyGet(orderId);
                    ViewPponOrderStatusLogCollection returnLog = op.ViewPponOrderStatusLogGetListPaging(1, -1,
                                            ViewPponOrderStatusLog.Columns.OrderLogCreateTime + " desc ",
                                            ViewPponOrderStatusLog.Columns.OrderGuid + " = " + orderId,
                                            ViewPponOrderStatusLog.Columns.OrderLogStatus + " = " + (int)OrderLogStatus.Processing);

                    tbApplyDate.Text = DateTime.Now.ToString("D");
                    tbOrderId.Text = o.OrderId;
                    tbSequenceNumber.Text = GetCouponList(coupons);
                    tbEventName.Text = deal.EventName;
                    tbMemberEmail.Text = m.UserEmail;
                    tbMemberName.Text = o.MemberName;
                    tbPhone.Text = o.PhoneNumber;
                    tbMobile.Text = o.MobileNumber;

                    if (dealproperty.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToHouse, dealproperty.DeliveryType.Value))
                    {
                        tbAddress.Text = o.DeliveryAddress;
                    }
                    else
                    {
                        tbAddress.Text = m.CompanyAddress;
                    }

                    tbContactEmail.Text = m.UserEmail;
                    tbReturnReason.Text = returnLog[0].OrderLogReason;

                    if (o.OrderMemo.Split('|')[0] != ((int)DonationReceiptsType.DoNotContribute).ToString())
                    {
                        rbInvoice2.Checked = true;
                    }
                }
            }
        }

        private string GetCouponList(ViewPponCouponCollection coupons)
        {
            string cList = string.Empty;
            foreach (var c in coupons)
            {
                cList += c.SequenceNumber + " ";
            }

            return cList;
        }
    }
}
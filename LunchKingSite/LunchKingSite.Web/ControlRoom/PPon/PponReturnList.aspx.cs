﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class PponReturnList : RolePage, IPponReturnListView
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public event EventHandler<DataEventArgs<int>> PageChanged;

        public event EventHandler<DataEventArgs<Guid>> GetProcessMessage;

        public event EventHandler<DataEventArgs<Guid>> GetOrderCoupons;

        public event EventHandler<DataEventArgs<Guid>> GetApplicationTime;

        public event EventHandler SortClicked;

        public event EventHandler SearchClicked;

        public event EventHandler ExportToExcel;

        public event EventHandler<DataEventArgs<Guid>> GetReason;

        protected void Page_Load(object sender, EventArgs e)
        {
            gvRetrn.Columns[1].SortExpression = ViewPponOrderReturnList.Columns.OrderId;
            gvRetrn.Columns[3].SortExpression = ViewPponOrderReturnList.Columns.ReturnCreateTime;
            gvRetrn.Columns[4].SortExpression = ViewPponOrderReturnList.Columns.BusinessHourDeliverTimeE;
            gvRetrn.Columns[5].SortExpression = ViewPponOrderReturnList.Columns.SellerName;
            gvRetrn.Columns[6].SortExpression = ViewPponOrderReturnList.Columns.BusinessHourDeliverTimeE;
            gvRetrn.Columns[7].SortExpression = ViewPponOrderReturnList.Columns.Total;
            gvRetrn.Columns[10].SortExpression = ViewPponOrderReturnList.Columns.ReturnStatus;

            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();

                if (rbDepartment.Items.Count > 0)
                {
                    rbDepartment.Items[0].Selected = true;
                }

                if (rbStatus.Items.Count > 0)
                {
                    rbStatus.Items[0].Selected = true;
                }
            }
            this.Presenter.OnViewLoaded();
        }

        #region props

        private PponReturnListPresenter _presenter;

        public PponReturnListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public int PageSize
        {
            get
            {
                return gridPager.PageSize;
            }
            set
            {
                gridPager.PageSize = value;
            }
        }

        private OrderLogStatus _returnProcess;

        public OrderLogStatus ReturnProcess
        {
            get
            {
                return _returnProcess;
            }
            set
            {
                _returnProcess = value;
            }
        }

        private string _processMessage;

        public string ProcessMessage
        {
            get
            {
                return _processMessage;
            }
            set
            {
                _processMessage = value;
            }
        }

        private string _itemName;

        public string ItemName
        {
            get
            {
                return _itemName;
            }
            set
            {
                _itemName = value;
            }
        }

        private string _at;

        public string ApplicationTime
        {
            get
            {
                return _at;
            }
            set
            {
                _at = value;
            }
        }

        private string _rs;

        public string Reason
        {
            get
            {
                return _rs;
            }
            set
            {
                _rs = value;
            }
        }

        private int _osLogCount;

        public int OsLogCount
        {
            get
            {
                return _osLogCount;
            }
            set
            {
                _osLogCount = value;
            }
        }

        private ViewPponCouponCollection _cp;

        public ViewPponCouponCollection Coupons
        {
            get
            {
                return _cp;
            }
            set
            {
                _cp = value;
            }
        }

        public string SortExpression
        {
            get
            {
                return (ViewState != null && ViewState["se"] != null)
                           ? (string)ViewState["se"]
                           : ViewPponOrderReturnList.Columns.ReturnCreateTime + " desc";
            }
            set
            {
                ViewState["se"] = value;
            }
        }

        public string ApplyTimeS
        {
            get
            {
                return tbApplyTimeS.Text.Trim();
            }
        }

        public string ApplyTimeE
        {
            get
            {
                return tbApplyTimeE.Text.Trim();
            }
        }

        public bool SelectAll
        {
            get
            {
                return cbSelectAll.Checked;
            }
        }

        public Dictionary<int, string> StatusFilter
        {
            get
            {
                Dictionary<int, string> data = new Dictionary<int, string>();

                data.Add(-1, "全部");
                data.Add((int)OrderReturnStatus.ExchangeProcessing, Resources.Localization.ExchangeProcessing);
                data.Add((int)OrderReturnStatus.ExchangeSuccess, Resources.Localization.ExchangeSuccess);
                data.Add((int)OrderReturnStatus.ExchangeFailure, Resources.Localization.ExchangeFailure);
                data.Add((int)OrderReturnStatus.ExchangeCancel, Resources.Localization.ExchangeCancel);

                return data;
            }
        }

        public Dictionary<int, string> DepartmentFilter
        {
            get
            {
                Dictionary<int, string> data = new Dictionary<int, string>();

                data.Add(-1, "全部");
                data.Add(0, "憑證");
                data.Add(1, "宅配");

                return data;
            }
        }

        public int StatusSelected
        {
            get
            {
                int tryParse;
                return int.TryParse(rbStatus.SelectedValue, out tryParse) ? tryParse : -1;
            }
        }

        public int DepartmentSelected
        {
            get
            {
                int department;
                int.TryParse(rbDepartment.SelectedValue, out department);
                
                switch (department)
                {
                    case 0:
                        return (int)DeliveryType.ToShop;

                    case 1:
                        return (int)DeliveryType.ToHouse;

                    default:
                        return -1;
                }
            }
        }

        #endregion props

        public void SetStatusFilter(Dictionary<int, string> status)
        {
            rbStatus.DataSource = status;
            rbStatus.DataBind();
        }

        public void SetDepartmentFilter(Dictionary<int, string> department)
        {
            rbDepartment.DataSource = department;
            rbDepartment.DataBind();
        }

        public void SetGridReturn(ViewPponOrderReturnListCollection orders)
        {
            FilterItemName(orders);

            gvRetrn.DataSource = orders;
            gvRetrn.DataBind();
        }
        
        private void FilterItemName(ViewPponOrderReturnListCollection orders)
        {
            foreach (var o in orders.Where(x => !string.IsNullOrEmpty(x.LabelIconList)))
            {
                string fastget = "";
                foreach (var s in o.LabelIconList.Split(','))
                {
                    if (s == ((int) DealLabelSystemCode.ArriveIn24Hrs).ToString() ||
                        s == ((int) DealLabelSystemCode.ArriveIn72Hrs).ToString())
                    {
                        fastget += string.Format("[{0}]", SystemCodeManager.GetDealLabelCodeName(Convert.ToInt32(s)));
                    }
                }
                o.CouponUsage = fastget + o.CouponUsage;
            }
        }

        protected string GetOrderSituation(int d, DateTime returnTime, DateTime validTime)
        {
            if ((DeliveryType)d == DeliveryType.ToShop)
            {
                if (returnTime > validTime)
                {
                    return Phrase.CouponExpired;
                }
                else
                {
                    return Phrase.CouponNotExpired;
                }
            }
            else if ((DeliveryType)d == DeliveryType.ToHouse)
            {
                if (returnTime > validTime)
                {
                    return Phrase.GoodsExpired;
                }
                else
                {
                    return Phrase.GoodsNotExpired;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        protected string GetRefundReason(string oid)
        {
            if (this.GetReason != null)
            {
                this.GetReason(this, new DataEventArgs<Guid>(new Guid(oid)));
            }

            return Reason;
        }

        protected string GetRefundProcess(string status, string flag)
        {
            string strStatus = @"[";

            // 人工 or 自動
            if (Helper.IsFlagSet(Convert.ToInt32(flag), (int)OrderReturnFlags.RefundManual))
            {
                strStatus += Resources.Localization.RefundManual;
            }
            else
            {
                strStatus += Resources.Localization.RefundAuto;
            }

            // 退購物金 or 刷退/退ATM
            if (Helper.IsFlagSet(Convert.ToInt32(flag), (int)OrderReturnFlags.RefundScashOnly))
            {
                strStatus += Resources.Localization.RefundScashOnly;
            }
            else
            {
                strStatus += Resources.Localization.RefundATM;
            }

            strStatus += @"] ";

            switch ((OrderReturnStatus)int.Parse(status))
            {
                case OrderReturnStatus.Processing:
                    strStatus += Resources.Localization.ReturnedProcessing;
                    break;

                case OrderReturnStatus.Success:
                    strStatus += Resources.Localization.ReturnedSuccess;
                    break;

                case OrderReturnStatus.Failure:
                    strStatus += Resources.Localization.ReturnedFailure;
                    break;

                case OrderReturnStatus.ExchangeProcessing:
                    strStatus = Resources.Localization.ExchangeProcessing;
                    break;

                case OrderReturnStatus.ExchangeSuccess:
                    strStatus = Resources.Localization.ExchangeSuccess;
                    break;

                case OrderReturnStatus.ExchangeFailure:
                    strStatus = Resources.Localization.ExchangeFailure;
                    break;

                case OrderReturnStatus.ExchangeCancel:
                    strStatus = Resources.Localization.ExchangeCancel;
                    break;

                case OrderReturnStatus.SendToSeller:
                    strStatus = Resources.Localization.SendToSeller;
                    break;

                default:
                    break;
            }

            return strStatus;
        }

        protected string GetTheDate(string time)
        {
            return DateTime.Parse(time).ToString("yyyy/MM/dd");
        }

        protected string GetMessage(string oid)
        {
            if (this.GetProcessMessage != null)
            {
                this.GetProcessMessage(this, new DataEventArgs<Guid>(new Guid(oid)));
            }

            return ProcessMessage;
        }

        protected string GetItemName(string oid)
        {
            if (this.GetProcessMessage != null)
            {
                this.GetProcessMessage(this, new DataEventArgs<Guid>(new Guid(oid)));
            }

            return ItemName;
        }

        protected string GetRefundApplicationTime(string oid)
        {
            if (this.GetApplicationTime != null)
            {
                this.GetApplicationTime(this, new DataEventArgs<Guid>(new Guid(oid)));
            }

            return ApplicationTime;
        }

        protected string GetCouponList(string oid)
        {
            ViewPponCouponCollection coupons = GetCoupons(new Guid(oid));
            string cList = string.Empty;

            if (coupons.Any())
            {
                cList = string.Join("<br/>", coupons.Select(x => x.SequenceNumber));
            }

            return cList;
        }

        private ViewPponCouponCollection GetCoupons(Guid orderGuid)
        {
            if (this.GetOrderCoupons != null)
            {
                this.GetOrderCoupons(this, new DataEventArgs<Guid>(orderGuid));
            }

            return Coupons;
        }

        protected int RetrieveTransCount()
        {
            return OsLogCount;
        }

        protected void UpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.PageChanged != null)
            {
                this.PageChanged(this, e);
            }
        }

        protected void tmr1_Tick(object sender, EventArgs e)
        {
            if (this.PageChanged != null)
            {
                this.PageChanged(this, new DataEventArgs<int>(gridPager.CurrentPage));
            }

            gridPager.ResolvePagerView(gridPager.CurrentPage, true);
        }

        protected void ddlTmr_SIC(object sender, EventArgs e)
        {
            if (ddlTmr.SelectedValue == "0")
            {
                tmr1.Enabled = false;
            }
            else
            {
                tmr1.Enabled = true;
                tmr1.Interval = int.Parse(ddlTmr.SelectedValue) * 1000;
            }
        }

        protected void ddlPS_SIC(object sender, EventArgs e)
        {
            this.PageSize = int.Parse(ddlPS.SelectedValue);
            if (this.PageChanged != null)
            {
                this.PageChanged(this, new DataEventArgs<int>(gridPager.CurrentPage));
            }
            gridPager.ResolvePagerView(gridPager.CurrentPage, true);
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            if (this.ExportToExcel != null)
            {
                this.ExportToExcel(this, e);
            }
        }

        protected void gvRetrn_Sorting(object sender, GridViewSortEventArgs e)
        {
            ViewState["sd"] = ((string)ViewState["sd"] == " ASC") ? " DESC" : " ASC";
            SortExpression = e.SortExpression + (string)ViewState["sd"];
            if (this.SortClicked != null)
            {
                SortClicked(this, new EventArgs());
            }
            gridPager.ResolvePagerView(1);
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            if (this.SearchClicked != null)
            {
                this.SearchClicked(this, e);
            }
            gridPager.ResolvePagerView(1, true);
        }

        public void Export(ViewPponOrderReturnListCollection osCol)
        {
            if (osCol.Count() > 0)
            {
                DataTable dt = new DataTable();
                DataRow drHeader = dt.NewRow();
                int counter = 0;

                string[] headers = new string[] { "處理進度", "檔號", "訂單編號", "申請時間", "結檔日期", "廠商名稱", "商品名稱"
                , "收件人", "收件人電話", "配送地址", "數量", "金額", "取消理由", "處理備註" , "退換貨連絡電子信箱", "業務" };

                foreach (string s in headers)
                {
                    DataColumn dc = new DataColumn(s);
                    dt.Columns.Add(dc);
                    drHeader[counter] = dc.ColumnName;
                    counter++;
                }

                dt.Rows.Add(drHeader);

                FilterItemName(osCol);

                foreach (var os in osCol)
                {
                    DealProperty dp = PponFacade.DealPropertyGetByBid(os.BusinessHourGuid);
                    ViewPponCouponCollection coupons = GetCoupons(os.OrderGuid);
                    if (coupons.Count > 0)
                    {
                        foreach (var coupon in coupons)
                        {
                            DataRow dr = dt.NewRow();
                            dr[0] = GetRefundProcess(os.ReturnStatus.ToString(), os.ReturnFlag.ToString());
                            dr[1] = os.UniqueId;
                            dr[2] = os.OrderId;
                            dr[3] = os.ReturnCreateTime.ToString("yyyy/MM/dd");
                            dr[4] = os.BusinessHourOrderTimeE.ToString("yyyy/MM/dd");
                            dr[5] = os.SellerName;
                            dr[6] = os.CouponUsage + "-" + coupon.ItemName;
                            dr[7] = os.MemberName;
                            dr[8] = os.MobileNumber;
                            dr[9] = os.DeliveryAddress;
                            dr[10] = Convert.ToInt32(coupon.ItemQuantity);
                            dr[11] = Convert.ToInt32(coupon.ItemUnitPrice);
                            dr[12] = GetRefundReason(os.OrderGuid.ToString());
                            dr[13] = GetMessage(os.OrderGuid.ToString());
                            dr[14] = os.ReturnedPersonEmail;
                            dr[15] = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.UserId, dp.DevelopeSalesId).EmpName +
                                (dp.OperationSalesId != null ? "/" + HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.UserId, dp.DevelopeSalesId).EmpName : "");
                            dt.Rows.Add(dr);
                        }
                    }
                    else
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = GetRefundProcess(os.ReturnStatus.ToString(), os.ReturnFlag.ToString());
                        dr[1] = os.UniqueId;
                        dr[2] = os.OrderId;
                        dr[3] = os.ReturnCreateTime.ToString("yyyy/MM/dd");
                        dr[4] = os.BusinessHourOrderTimeE.ToString("yyyy/MM/dd");
                        dr[5] = os.SellerName;
                        dr[6] = os.CouponUsage + "-" + GetItemName(os.OrderGuid.ToString());
                        dr[7] = os.MemberName;
                        dr[8] = os.MobileNumber;
                        dr[9] = os.DeliveryAddress;
                        dr[10] = Convert.ToInt32(os.Quantity);
                        dr[11] = Convert.ToInt32(os.Total);
                        dr[12] = GetRefundReason(os.OrderGuid.ToString());
                        dr[13] = GetMessage(os.OrderGuid.ToString());
                        dr[14] = os.ReturnedPersonEmail;
                        dr[15] = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.UserId, dp.DevelopeSalesId).EmpName +
                                (dp.OperationSalesId != null ? "/" + HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.UserId, dp.DevelopeSalesId).EmpName : "");
                        dt.Rows.Add(dr);
                    }
                }

                Workbook workbook = new HSSFWorkbook();

                // 新增試算表。
                Sheet sheet = workbook.CreateSheet("退貨報表");

                Cell c = null;
                int rowCount = 0;
                int rowIndex = 0;
                foreach (DataRow row in dt.Rows)
                {
                    Row dataRow = sheet.CreateRow(rowIndex);
                    foreach (DataColumn column in dt.Columns)
                    {
                        c = dataRow.CreateCell(column.Ordinal);
                        c.SetCellValue(row[column].ToString());
                    }
                    rowIndex++;
                    rowCount++;
                }

                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode("RefundReport.xls")));
                workbook.Write(HttpContext.Current.Response.OutputStream);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), string.Empty, "沒有資料可供匯出!", true);
            }
        }

        public Dictionary<string, string> FilterTypes
        {
            get
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add(ViewPponOrderReturnList.Columns.OrderId, Resources.Localization.OrderId);
                data.Add(ViewPponOrderReturnList.Columns.EventName, Resources.Localization.EventName);
                data.Add(ViewPponOrderReturnList.Columns.UniqueId, Resources.Localization.UniqueId);
                data.Add(ViewPponOrderReturnList.Columns.DealEmpName, Resources.Localization.SalesName);
                data.Add(ViewPponOrderReturnList.Columns.SellerName, Resources.Localization.SellerName);
                return data;
            }
        }

        public void SetFilterTypeDropDown(Dictionary<string, string> types)
        {
            ddlST.DataSource = types;
            ddlST.DataBind();
        }

        public string FilterType
        {
            get
            {
                return ddlST.SelectedValue;
            }
            set
            {
                ddlST.SelectedValue = value;
            }
        }

        public string FilterUser
        {
            get
            {
                return tbSearch.Text;
            }
            set
            {
                tbSearch.Text = value;
            }
        }
    }
}
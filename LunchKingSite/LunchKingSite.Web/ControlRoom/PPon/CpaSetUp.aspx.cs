﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class CpaSetUp : RolePage
    {
        IPponProvider pp;
        protected void Page_Load(object sender, EventArgs e)
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            if (!IsPostBack)
            {
                //預設搜尋時間於今日前後7日
                tbx_ListEnd.Text = DateTime.Now.AddDays(7).ToString("MM/dd/yyyy");
                tbx_ListStart.Text = DateTime.Now.AddDays(-7).ToString("MM/dd/yyyy");
                SearchCpaListByDate(sender, e);
                //新版cpa的類別
                foreach (var item in Enum.GetValues(typeof(CpaType)))
                {
                    ddl_Type.Items.Add(new ListItem(Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, (CpaType)item), ((int)item).ToString()));
                }
                for (int i = 60; i >= 0; i = i - 10)
                {
                    ddl_MaxValue.Items.Add(i.ToString());
                }
                for (int i = 100; i >= 0; i = i - 10)
                {
                    ddl_Ratio.Items.Add(i.ToString());
                }
            }
        }

        /// <summary>
        /// 依日期尋找cpa設定主檔
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchCpaListByDate(object sender, EventArgs e)
        {
            DateTime startdate, enddate;
            if (DateTime.TryParse(tbx_ListStart.Text, out startdate) && DateTime.TryParse(tbx_ListEnd.Text, out enddate))
            {
                CpaMainCollection list = pp.CpaMainGetListByDate(startdate, enddate);
                rpt_List.DataSource = list;
                rpt_List.DataBind();
            }
            else
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsuccess", "alert('datetime format error!!');", true);
        }

        /// <summary>
        /// 依名稱搜尋(like)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchCpaListByCodeName(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbx_CodeName.Text))
            {
                CpaMainCollection list = pp.CpaMainGetListByCodeName(tbx_CodeName.Text);
                rpt_List.DataSource = list;
                rpt_List.DataBind();
            }
        }

        /// <summary>
        /// 編輯已存在的主檔
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptList_Command(object sender, RepeaterCommandEventArgs e)
        {
            int id;
            if (e.CommandName == "EditCpaMain" && int.TryParse(e.CommandArgument.ToString(), out id))
            {
                lab_Title.Text = "Edit";
                CpaMain main = pp.CpaMainGetById(id);
                lab_Id.Text = id.ToString();
                tbx_Name.Text = main.CpaName;
                tbx_Code.Text = main.CpaCode;
                tbx_StartDate.Text = main.StartDate.ToString("MM/dd/yyyy");
                tbx_EndDate.Text = main.EndDate.ToString("MM/dd/yyyy");
                cbx_Enabled.Checked = main.Enabled;
                ddl_Ratio.SelectedValue = main.Ratio.ToString();
                ddl_MaxValue.SelectedValue = "0";
                ddl_Type.SelectedValue = main.Type.ToString();
                tbx_Code.Enabled = ddl_Type.Enabled = false;
                pan_AddorEdit.Visible = true;
                pan_List.Visible = false;
            }
        }

        /// <summary>
        /// 切換視窗(新增、列表)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddCpa(object sender, EventArgs e)
        {
            pan_AddorEdit.Visible = true;
            pan_List.Visible = false;
            CleanContent();
            lab_Title.Text = "Add";
        }

        /// <summary>
        /// 取消編輯或新增，並返回列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CancelAction(object sender, EventArgs e)
        {
            CleanContent();
            pan_AddorEdit.Visible = false;
            pan_List.Visible = true;
            ddl_Type.SelectedIndex = 0;
            SearchCpaListByDate(sender, e);
        }

        /// <summary>
        /// 確認新增或更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ConfirmAction(object sender, EventArgs e)
        {
            //是否重複cpa代碼
            bool already_repeat_code = false;
            int id, type, max, ratio;
            DateTime startdate, enddate;
            if (DateTime.TryParse(tbx_StartDate.Text, out startdate) && DateTime.TryParse(tbx_EndDate.Text, out enddate)
                && int.TryParse(ddl_MaxValue.SelectedValue, out max) && int.TryParse(ddl_Ratio.SelectedValue, out ratio) && int.TryParse(ddl_Type.SelectedValue, out type))
            {
                CpaMain main;
                if (int.TryParse(lab_Id.Text, out id))
                    main = pp.CpaMainGetById(id);
                else
                {
                    main = new CpaMain();
                    main.Creator = User.Identity.Name;
                    main.CreateTime = DateTime.Now;
                    //判斷是否存在相同代碼
                    already_repeat_code = pp.CpaMainGetListByCodeName(tbx_Code.Text).Count > 0;
                }
                if (already_repeat_code)
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alreadyexist", "alert('the code is existed!!');", true);
                else
                {
                    main.CpaName = tbx_Name.Text;
                    main.CpaCode = tbx_Code.Text;
                    main.StartDate = startdate;
                    main.EndDate = enddate;
                    main.Enabled = cbx_Enabled.Checked;
                    main.Type = type;
                    main.MaxCount = main.MaxTime = 0;
                    main.Ratio = ratio;
                    pp.CpaMainSet(main);
                    lab_Id.Text = main.Id.ToString();
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsuccess", "alert('save successfully!!');", true);
                }
            }
            else
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsuccess", "alert('format error!!');", true);
        }

        /// <summary>
        /// 清除輸入的資料
        /// </summary>
        private void CleanContent()
        {
            lab_Id.Text = tbx_Name.Text = tbx_Code.Text = tbx_StartDate.Text = tbx_EndDate.Text = string.Empty;
            tbx_Name.Enabled = tbx_Code.Enabled = ddl_Type.Enabled = true;
            tbx_Code.Enabled = ddl_Type.Enabled = true;
        }
    }
}
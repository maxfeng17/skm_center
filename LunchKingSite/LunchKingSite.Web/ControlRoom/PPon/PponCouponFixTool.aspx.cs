﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class PponCouponFixTool : RolePage
    {
        private IPponProvider pp;

        public bool IsFamiGroupCouponDeal
        {
            get { return FamiGroupCoupon.Checked; }
            set { FamiGroupCoupon.Checked = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (IsFamiGroupCouponDeal)
            {
                Guid og;
                if (Guid.TryParse(TextBox1.Text.Trim(), out og))
                {
                    if (og != Guid.Empty && OrderFacade.FixFamiGroupCoupon(og))
                    {
                        Label1.Text = "全家寄杯訂單修復完成";
                    }
                    else
                    {
                        Label1.Text = "全家寄杯訂單修復失敗";
                    }
                }
            }
            else
            {
                pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

                int times = 0;
                string[] details = TextBox1.Text.Replace("\n", ",").Replace("\r", ",").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                List<string> results = new List<string>();
                foreach (var OrderDetail in details)
                {
                    if (OrderDetail.Length > 0)
                    {
                        times = pp.CheckCouponDetail(OrderDetail);
                        string result = OrderFacade.GenerateCouponByOrderDetail(new Guid(OrderDetail), times);
                        if (!string.IsNullOrEmpty(result))
                        {
                            results.Add(result);
                        }
                    }
                }
                if (results.Count > 0)
                    Label1.Text = "錯誤:" + results.Aggregate((x, y) => x + "," + y);
                else
                    Label1.Text = "共" + details.Count() + "筆OrderDetail修復";
            }
        }

        protected void FixAllRecentData_Click(object sender, EventArgs e)
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            List<Guid> orderDetailGuids = pp.CouponlessOrderDetailGuidGetList();
            foreach(Guid odGuid in orderDetailGuids)
            {
                int times = pp.CheckCouponDetail(odGuid.ToString());
                OrderFacade.GenerateCouponByOrderDetail(odGuid, times);
            }
        }
    }
}
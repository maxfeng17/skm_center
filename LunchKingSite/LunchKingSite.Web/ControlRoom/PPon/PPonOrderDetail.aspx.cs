﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using System;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class PponOrderDetail : RolePage
    {
        private IPponProvider pp;
        private IOrderProvider op;
        private ISellerProvider sp;

        public PponOrderDetail()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected string GetTransType(object type)
        {
            if (type != null)
            {
                switch ((PayTransType)type)
                {
                    case PayTransType.Authorization:
                        return Resources.Localization.PayTransAuthorization;

                    case PayTransType.Charging:
                        return Resources.Localization.PayTransCharging;

                    case PayTransType.Refund:
                        return Resources.Localization.PayTransRefund;

                    default:
                        return string.Empty;
                }
            }
            return string.Empty;
        }

        protected string GetTransStatus(object status)
        {
            PayTransPhase ptp = Helper.GetPaymentTransactionPhase((int)status);

            switch (ptp)
            {
                case PayTransPhase.Created:
                    return Resources.Localization.PayTransCreated;

                case PayTransPhase.Requested:
                    return Resources.Localization.PayTransRequested;

                case PayTransPhase.Canceled:
                    return Resources.Localization.PayTransCanceled;

                case PayTransPhase.Successful:
                    return Resources.Localization.PayTransSuccessful;

                case PayTransPhase.Failed:
                    return Resources.Localization.PayTransFailed;

                default:
                    return string.Empty;
            }
        }

        protected void btnSetup_Click(object sender, EventArgs e)
        {
            ViewPponDeal vp = pp.ViewPponDealGet(new Guid(Request["pid"]));
            DealProperty dealProperty = pp.DealPropertyGet(vp.BusinessHourGuid);

            Response.Redirect("~/controlroom/ppon/setup.aspx?bid=" + vp.BusinessHourGuid);
        }

        protected void btnCoupon_Click(object sender, EventArgs e)
        {
            ViewPponDeal vp = pp.ViewPponDealGet(new Guid(Request["pid"]));
            DealProperty dealProperty = pp.DealPropertyGet(vp.BusinessHourGuid);

            if (dealProperty.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToHouse, dealProperty.DeliveryType.Value))
            {
                Response.Redirect("~/controlroom/ppon/PponSellerItemList.aspx?bid=" + vp.BusinessHourGuid);
            }
            else
            {
                Response.Redirect("~/controlroom/ppon/PponSellerCouponList.aspx?bid=" + vp.BusinessHourGuid);
            }
        }

        protected void btnPeauty_Click(object sender, EventArgs e)
        {
            ViewPponDeal vp = pp.ViewPponDealGet(new Guid(Request["pid"]));

            if (vp.Department == (int)DepartmentTypes.Peauty)
            {
                Response.Redirect("~/controlroom/ppon/PponSellerItemList.aspx?bid=" + vp.BusinessHourGuid + "&itemname=" + vp.ItemName);
            }
        }

        protected void btnInvoice_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/controlroom/ppon/PponSellerInvoiceList.aspx?bid=" + new Guid(Request["pid"]));
        }

        protected void btnAccounting_Click(object sender, EventArgs e)
        {
            ViewPponDeal vp = pp.ViewPponDealGet(new Guid(Request["pid"]));
            Response.Redirect("~/controlroom/ppon/PponAccounting.aspx?bid=" + vp.BusinessHourGuid);
        }

        protected void btnCom_Click(object sender, EventArgs e)
        {
            ViewPponDeal vp = pp.ViewPponDealGet(new Guid(Request["pid"]));
            Response.Redirect("~/controlroom/ppon/PponComReport.aspx?bid=" + vp.BusinessHourGuid);
        }
    }
}
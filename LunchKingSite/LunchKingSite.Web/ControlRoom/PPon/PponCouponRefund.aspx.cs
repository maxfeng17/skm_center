﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class PponCouponRefund : RolePage, IPponCouponRefundView
    {
        #region Property

        private PponCouponRefundPresenter _presenter;
        public PponCouponRefundPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        
        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        public Guid DealGuid
        {
            get
            {
                Guid dealGuid;
                return Guid.TryParse(tbBid.Text, out dealGuid) ? dealGuid : Guid.Empty;
            }
            set
            {
                tbBid.Text = value.ToString();
            }
        }

        public int DealType
        {
            get
            {
                int dealType;
                return int.TryParse(ddlDeliveryType.SelectedValue, out dealType) ? dealType : 0;
            }
            set
            {
                ddlDeliveryType.SelectedValue = value.ToString();
            }
        }

        public string OrderList
        {
            get
            {
                return tbCoupons.Text;
            }
            set
            {
                tbCoupons.Text = value;
            }
        }

        public string ReturnReason
        {
            get
            {
                return tbReason.Text;
            }
            set
            {
                tbReason.Text = value;
            }
        }

        public string Message
        {
            set
            {
                lbMessage.Text = value;
            }
        }

        #endregion Property

        #region Event

        public event EventHandler<DataEventArgs<bool>> RequestRefund;

        #endregion Event

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                foreach (ReturnDealType type in Enum.GetValues(typeof(ReturnDealType)))
                {
                    if (!string.IsNullOrEmpty(Helper.GetDescription(type)))
                    {
                        ddlDeliveryType.Items.Add(new System.Web.UI.WebControls.ListItem(Helper.GetDescription(type), ((int)type).ToString()));
                    }
                }
                this.Presenter.OnViewInitialized();
            }

            this.Presenter.OnViewLoaded();
        }

        protected void btRefund_Click(object sender, EventArgs e)
        {
            lbMessage.Text = string.Empty;
            string columnName;

            if (!CheckInputData(out columnName))
            {
                lbMessage.Text = "請輸入" + columnName;
                return;
            }

            bool isRefundSCash = cbIsRefundSCashOnly.Checked;
            if (RequestRefund != null)
            {
                RequestRefund(sender, new DataEventArgs<bool>(isRefundSCash));
            }
        }

        protected void btRefundCash_Click(object sender, EventArgs e)
        {
            return;
        }

        private bool CheckInputData(out string columnName)
        {
            columnName = string.Empty;

            if (string.IsNullOrEmpty(tbBid.Text) && DealType != (int)ReturnDealType.CouponWithoutBid)
            {
                columnName = "BID";
                return false;
            }
            
            if (ddlDeliveryType.SelectedValue == "0")
            {
                columnName = "退貨方式";
                return false;
            }

            if (string.IsNullOrEmpty(tbCoupons.Text))
            {
                if (DealType == (int)ReturnDealType.Coupon)
                {
                    columnName = "憑證編號";
                }     
                else if (DealType == (int)ReturnDealType.Home)
                {
                    columnName = "訂單編號";
                }

                return false;
            }

            if (string.IsNullOrWhiteSpace(tbReason.Text))
            {
                columnName = "退貨原因";
                return false;
            }

            return true;
        }

        protected enum ReturnDealType
        {
            [Description("請選擇退貨方式")]
            None,
            [Description("依憑證編號(好康)")]
            Coupon = 1,
            [Description("依訂單號碼(商品)")]
            Home = 2,
            [Description("依訂單號碼(好康noBid)")]
            CouponWithoutBid = 3
        }
    }

    
}
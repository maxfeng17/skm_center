﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.SsBLL;
using LunchKingSite.DataOrm;
using SubSonic;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Facade;


namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class FamiPortManager : RolePage, IFamiPortManagerView
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region props
        private FamiPortManagerPresenter _presenter;
        public FamiPortManagerPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public Guid BusinessHourGuid
        {
            get
            {
                Guid bid = Guid.TryParse(txtBusinessHourGuid.Text.Trim(), out bid) ? bid : Guid.Empty;
                return bid;
            }
        }

        public string EventId
        {
            get { return txtEventId.Text.Trim(); }
        }

        public string Password
        {
            get { return txtPassword.Text.Trim(); }
        }

        public string LoginUser
        {
            get { return Page.User.Identity.Name; }
        }

        public int PageSize
        {
            get { return ucPager.PageSize; }
        }
        #endregion

        #region event
        public event EventHandler OnSearchClicked;
        public event EventHandler OnSaveClicked;
        public event EventHandler<DataEventArgs<string>> OnDeleteClicked;
        public event EventHandler<DataEventArgs<int>> OnGetCount;
        public event EventHandler<DataEventArgs<int>> OnPageChanged;
        #endregion

        #region method
        public void GetFamiPortList(Dictionary<Famiport, KeyValuePair<Guid, string>> dataList)
        {
            gvFamiPort.DataSource = dataList;
            gvFamiPort.DataBind();
        }

        public void ShowMessage(string msg)
        {
            lblErrorMsg.Visible = true;
            lblErrorMsg.Text = msg;
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ucPager.ResolvePagerView(1, true);
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            lblErrorMsg.Visible = false;
            if (this.OnSaveClicked != null)
                this.OnSaveClicked(this, e);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            lblErrorMsg.Visible = false;
            if (this.OnSearchClicked != null)
                this.OnSearchClicked(this, e);
            ucPager.ResolvePagerView(1, true);
        }

        protected void gvFamiPort_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is KeyValuePair<Famiport, KeyValuePair<Guid, string>>)
            {
                KeyValuePair<Famiport, KeyValuePair<Guid, string>> dataItem = (KeyValuePair<Famiport, KeyValuePair<Guid, string>>)e.Row.DataItem;
                Label lblId = e.Row.FindControl("lblId") as Label;
                HyperLink hlDeal = e.Row.FindControl("hlDeal") as HyperLink;
                HiddenField hidBid = e.Row.FindControl("hidBid") as HiddenField;
                Label lblEventId = e.Row.FindControl("lblEventId") as Label;
                Label lblPassword = e.Row.FindControl("lblPassword") as Label;
                Label lblCreateId = e.Row.FindControl("lblCreateId") as Label;
                Label lblCreateTime = e.Row.FindControl("lblCreateTime") as Label;
                Label lblRemark = e.Row.FindControl("lblRemark") as Label;
                Label lblIsImport = e.Row.FindControl("lblIsImport") as Label;
                Button btnUpdate = e.Row.FindControl("btnUpdate") as Button;
                Button btnDelete = e.Row.FindControl("btnDelete") as Button;
                Button btnImport = e.Row.FindControl("btnImport") as Button;

                lblId.Text = dataItem.Key.Id.ToString();
                hlDeal.Text = dataItem.Value.Value;
                hlDeal.NavigateUrl = config.SiteUrl + ResolveUrl("/controlroom/ppon/setup.aspx?bid=" + dataItem.Value.Key);
                hidBid.Value = dataItem.Value.Key.ToString();
                lblEventId.Text = dataItem.Key.EventId;
                lblPassword.Text = dataItem.Key.Password;
                lblCreateId.Text = dataItem.Key.CreateId;
                lblCreateTime.Text = dataItem.Key.CreateTime.ToString();
                lblRemark.Text = dataItem.Key.Remark;
                lblIsImport.Text = dataItem.Key.IsImportSequencenumber.ToString();
                btnUpdate.CommandArgument = e.Row.DataItemIndex.ToString();
                btnDelete.CommandArgument = dataItem.Key.Id.ToString();
                btnImport.CommandArgument = dataItem.Key.Id.ToString();
                btnImport.Enabled = btnImport.Visible = !dataItem.Key.IsImportSequencenumber;
                lblIsImport.Visible = dataItem.Key.IsImportSequencenumber;
            }
        }

        protected void gvFamiPort_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName.ToString())
            {
                case "DEL":
                    if (this.OnDeleteClicked != null)
                        this.OnDeleteClicked(this, new DataEventArgs<string>(e.CommandArgument.ToString()));
                    break;
                case "UPD":
                    int index = int.TryParse(e.CommandArgument.ToString(), out index) ? index : 0;
                    txtBusinessHourGuid.Text = ((HiddenField)gvFamiPort.Rows[index].FindControl("hidBid")).Value;
                    txtEventId.Text = ((Label)gvFamiPort.Rows[index].FindControl("lblEventId")).Text;
                    txtPassword.Text = ((Label)gvFamiPort.Rows[index].FindControl("lblPassword")).Text;
                    break;
                case "IMP":
                    int id;
                    if (int.TryParse(e.CommandArgument.ToString(), out id))
                    {
                        int i = OrderFacade.FamiImportSequenceNumber(id);
                        if (i < 0)
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('something wrong -____-!!');", true);
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('共新增" + i + "筆序號');", true);
                            btnSearch_Click(sender, e);
                        }
                    }
                    break;
            }
        }

        protected void UpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.OnPageChanged != null)
                this.OnPageChanged(this, e);
        }

        protected int GetCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetCount != null)
                OnGetCount(this, e);
            return e.Data;
        }
        #endregion

        #region private method
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using System.Net;
using System.IO;
using System.Text;
using MongoDB.Bson;
using LunchKingSite.BizLogic.Facade;
using System.Web.Services;
using System.Web.Script.Services;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class YahooBigSetup : RolePage, IYahooBigSetupView
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region props
        private YahooBigSetupPresenter _presenter;
        public YahooBigSetupPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserId
        {
            get { return Page.User.Identity.Name; }
        }

        public YahooPropertyType SearchType
        {
            get
            {
                YahooPropertyType type = YahooPropertyType.Ppon;
                YahooPropertyType.TryParse(rdlType.SelectedValue, out type);
                return type;
            }
        }

        public string DealId
        {
            get { return txtDealId.Text.Trim(); }
        }

        public string DealName
        {
            get { return txtDealName.Text.Trim(); }
        }

        public DateTime DealStartDate
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtDealStartDate.Text, out date);
                return date.AddDays(1);
            }
        }

        public DateTime DealEndDate
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtDealEndDate.Text, out date);
                return date.AddDays(1);
            }
        }

        public int PageSize
        {
            get { return ucPponPager.PageSize; }
            set { ucPponPager.PageSize = value; }
        }

        public int CurrentPage
        {
            get
            {
                if (ViewState["CurrentPage"] == null)
                {
                    return 1;
                }
                else
                {
                    return (int)ViewState["CurrentPage"];
                }
            }
            set
            {
                ViewState["CurrentPage"] = value;
                ucPponPager.CurrentPage = value;
            }
        }

        /// <summary>
        /// Yahoo大團購動作狀態
        /// </summary>
        public YahooPropertyAction Action
        {
            set { this.ViewState["Action"] = value; }
            get
            {
                if (this.ViewState["Action"] == null)
                {
                    this.ViewState["Action"] = YahooPropertyAction.Ordinary;
                }
                return ((YahooPropertyAction)(this.ViewState["Action"] ?? YahooPropertyAction.Ordinary));
            }
        }

        public bool IsMarkDelete
        {
            get { return chkIsMarkDelete.Checked; }
            set { chkIsMarkDelete.Checked = value; }
        }

        public bool IsPrintDiscountMark
        {
            get { return chkIsPrintDiscountMark.Checked; }
        }

        public bool IsPrintLogoWaterMark
        {
            get { return chkIsPrintLogoWaterMark.Checked; }
        }

        public bool IsCutCenter
        {
            get { return chkIsCutCenter.Checked; }
        }

        public FileUpload ImgUploadFile
        {
            get
            {
                return upImg;
            }
        }

        public bool IsShowYahooPropertyEditPanel
        {
            set { divYahooPropertyEdit.Visible = value; }
        }

        public string SearchMode
        {
            set { this.ViewState["mode"] = value; }
            get
            {
                if (this.ViewState["mode"] == null)
                {
                    this.ViewState["mode"] = "Grid";
                }
                return this.ViewState["mode"].ToString();
            }
        }

        private const int areaDefaultSort = 1000;
        #endregion

        #region event
        public event EventHandler OnSearchClicked;
        public event EventHandler OnSortClicked;
        public event EventHandler<DataEventArgs<int>> GetPponYahooPropertyBySort;
        public event EventHandler<DataEventArgs<int>> CheckPponYahooPropertyAction;
        public event EventHandler<DataEventArgs<List<int>>> CheckSelectedPponYahooPropertyAction;
        public event EventHandler<DataEventArgs<Dictionary<int, int?[]>>> OnSortSaveClicked;
        public event EventHandler<DataEventArgs<Guid>> GetPponYahooProperty;
        public event EventHandler<DataEventArgs<int>> GetPiinlifeYahooProperty;
        public event EventHandler<DataEventArgs<KeyValuePair<string, string>>> SaveYahooProperty;
        public event EventHandler<DataEventArgs<int>> OnPponGetCount;
        public event EventHandler<DataEventArgs<int>> PponPageChanged;
        public event EventHandler<DataEventArgs<List<string>>> ImportBidList;
        #endregion

        #region method
        public void SetPponDealList(Dictionary<ViewPponDeal, KeyValuePair<YahooProperty, string[]>> dataList)
        {
            gvPponDeal.DataSource = dataList.OrderBy(x => x.Value.Key.IsLoaded).ThenBy(x => x.Key.BusinessHourOrderTimeS);
            gvPponDeal.DataBind();
        }
        public void SetPiinlifeDealList(Dictionary<HiDealProduct, KeyValuePair<YahooProperty, string[]>> dataList)
        {
            gvPiinlifeDeal.DataSource = dataList.OrderBy(x => x.Value.Key.IsLoaded).ThenBy(x => x.Key.StartTime);
            gvPiinlifeDeal.DataBind();
        }
        public void SetSortData(IEnumerable<YahooProperty> ypsN, IEnumerable<YahooProperty> ypsC, IEnumerable<YahooProperty> ypsS)
        {
            divSort.Visible = true;
            rptSortN.DataSource = ypsN;
            rptSortN.DataBind();

            rptSortC.DataSource = ypsC;
            rptSortC.DataBind();

            rptSortS.DataSource = ypsS;
            rptSortS.DataBind();
        }
        public void SetYahooProperty(YahooProperty yp)
        {
            Action = (YahooPropertyAction)yp.Action;
            chkIsPrintDiscountMark.Checked = chkIsPrintLogoWaterMark.Checked = true;
            chkIsCutCenter.Checked = false;
            chkIsMarkDelete.Visible = yp.IsLoaded; // 有資料的才可以註記為刪除
            lblNo.Text = yp.Id.ToString();
            hidType.Value = yp.Type.ToString();
            lblType.Text = Helper.GetLocalizedEnum((YahooPropertyType)yp.Type);
            switch ((YahooPropertyType)yp.Type)
            {
                case YahooPropertyType.Ppon:
                    lblDealId.Text = yp.Bid.ToString();
                    hlLink.NavigateUrl = hlLink.Text = config.SiteUrl + "/" + yp.Bid;
                    lkSetupLink.NavigateUrl = config.SiteUrl + "/controlroom/ppon/setup.aspx?bid=" + yp.Bid;
                    break;
                case YahooPropertyType.Piinlife:
                    lblDealId.Text = yp.Pid.ToString();
                    hlLink.NavigateUrl = hlLink.Text = config.SiteUrl + "/piinlife/HiDeal.aspx?pid=" + yp.Pid;
                    lkSetupLink.NavigateUrl = config.SiteUrl + "/controlroom/piinlife/productsetup.aspx?pid=" + yp.Pid;
                    break;
                default:
                    break;
            }
            lblAction.Text = Helper.GetLocalizedEnum((YahooPropertyAction)yp.Action);
            if (!string.IsNullOrWhiteSpace(yp.ImgUrl))
            {
                img.ImageUrl = ImageFacade.GetMediaBaseUrl() + yp.ImgUrl;
            }
            else
            {
                img.ImageUrl = string.Empty;
            }
            if (yp.Category.HasValue)
            {
                rdlCategory.SelectedValue = yp.Category.ToString();
            }
            else
            {
                rdlCategory.SelectedIndex = 0;
            }
            txtTitle.Text = yp.Title;
            txtDealDesc.Text = yp.DealDesc;
            if (yp.AreaN.HasValue)
            {
                rdlAreaN.SelectedValue = yp.AreaN.Value.ToString();
            }
            else
            {
                rdlAreaN.SelectedIndex = -1; ;
            }
            if (yp.AreaC.HasValue)
            {
                rdlAreaC.SelectedValue = yp.AreaC.Value.ToString();
            }
            else
            {
                rdlAreaC.SelectedIndex = -1; ;
            }
            if (yp.AreaS.HasValue)
            {
                rdlAreaS.SelectedValue = yp.AreaS.Value.ToString();
            }
            else
            {
                rdlAreaS.SelectedIndex = -1; ;
            }

            //新版區域
            foreach (ListItem item in cklArea.Items)
            {
                item.Selected = Helper.IsFlagSet(yp.AreaFlag ?? 0, int.Parse(item.Value));
            }

            chkIsMarkDelete.Checked = yp.Action == (int)YahooPropertyAction.Delete;
        }
        public void ShowMessage(string msg)
        {
            lblAction.Visible = true;
            lblErrorMsg.Text = msg;
        }
        public YahooProperty GetYahooPropertyData(YahooProperty yp)
        {
            int type = int.TryParse(hidType.Value, out type) ? type : 0;
            yp.Type = type;
            Guid bid = Guid.TryParse(lblDealId.Text, out bid) ? bid : Guid.Empty;
            if (!Guid.Equals(bid, Guid.Empty))
            {
                yp.Bid = bid;
            }
            int pid = int.TryParse(lblDealId.Text, out pid) ? pid : 0;
            if (!int.Equals(pid, 0))
            {
                yp.Pid = pid;
            }
            yp.Action = (int)Action;
            if (!string.IsNullOrWhiteSpace(rdlCategory.SelectedValue))
            {
                int category = int.TryParse(rdlCategory.SelectedValue, out category) ? category : 0;
                yp.Category = category;
            }
            yp.Title = txtTitle.Text;
            yp.DealDesc = txtDealDesc.Text;

            int sumAreaFlag = default(int);          //新版區域
            foreach (ListItem item in cklArea.Items)
            {
                if (item.Selected)
                {
                    sumAreaFlag += int.Parse(item.Value);

                    switch (int.Parse(item.Value))
                    {
                        case 1: rdlAreaN.SelectedIndex = (int)YahooPropertyAreaNorth.taipei; rdlAreaC.SelectedIndex = (int)YahooPropertyAreaCentral.taichung; ; rdlAreaS.SelectedIndex = (int)YahooPropertyAreaSouth.kaohsiung; ; break;
                        case 2: rdlAreaN.SelectedIndex = (int)YahooPropertyAreaNorth.taipei; break;
                        case 4: rdlAreaN.SelectedIndex = (int)YahooPropertyAreaNorth.taoyuan; break;
                        case 8: rdlAreaN.SelectedIndex = (int)YahooPropertyAreaNorth.hsinchu; break;
                        case 16: rdlAreaN.SelectedIndex = (int)YahooPropertyAreaNorth.keelung; break;

                        case 32: rdlAreaC.SelectedIndex = (int)YahooPropertyAreaCentral.taichung; break;
                        case 64: rdlAreaC.SelectedIndex = (int)YahooPropertyAreaCentral.miaoli; break;
                        case 128: rdlAreaC.SelectedIndex = (int)YahooPropertyAreaCentral.changhua; break;
                        case 256: rdlAreaC.SelectedIndex = (int)YahooPropertyAreaCentral.nantou; break;
                        case 512: rdlAreaC.SelectedIndex = (int)YahooPropertyAreaCentral.yunlin; break;

                        case 1024: rdlAreaS.SelectedIndex = (int)YahooPropertyAreaSouth.kaohsiung; break;
                        case 2048: rdlAreaS.SelectedIndex = (int)YahooPropertyAreaSouth.tainan; break;
                        case 4096: rdlAreaS.SelectedIndex = (int)YahooPropertyAreaSouth.chiayi; break;
                        case 8192: rdlAreaS.SelectedIndex = (int)YahooPropertyAreaSouth.pingtung; break;
                    }

                }
            }

            if (sumAreaFlag > 0)
            {
                yp.AreaFlag = sumAreaFlag;
            }
            else
            {
                yp.AreaFlag = null;
            }



            if (!string.IsNullOrWhiteSpace(rdlAreaN.SelectedValue))
            {
                int areaN = int.TryParse(rdlAreaN.SelectedValue, out areaN) ? areaN : 0;
                yp.AreaN = areaN;
            }
            if (!string.IsNullOrWhiteSpace(rdlAreaC.SelectedValue))
            {
                int areaC = int.TryParse(rdlAreaC.SelectedValue, out areaC) ? areaC : 0;
                yp.AreaC = areaC;
            }
            if (!string.IsNullOrWhiteSpace(rdlAreaS.SelectedValue))
            {
                int areaS = int.TryParse(rdlAreaS.SelectedValue, out areaS) ? areaS : 0;
                yp.AreaS = areaS;
            }


            return yp;
        }
        public void AlertImportErrorMsg(string errorMsg)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alertImportErrorMsg", string.Format("AlertImportErrorMsg(\"{0}\");", errorMsg), true);
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Presenter.OnViewInitialized();
                IntialControls();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.OnSearchClicked != null)
                this.OnSearchClicked(this, e);

            if (SearchType.Equals(YahooPropertyType.Ppon))
            {
                ucPponPager.ResolvePagerView(1, true);
                divPponDeal.Visible = true;
                divPiinlifeDeal.Visible = false;
            }
            else
            {
                divPponDeal.Visible = false;
                divPiinlifeDeal.Visible = true;
            }
            divGrid.Visible = true;
            divYahooPropertyEdit.Visible = false;
            divSort.Visible = false;
            SearchMode = "Grid";
        }

        protected void btnSort_Click(object sender, EventArgs e)
        {
            if (this.OnSortClicked != null)
                this.OnSortClicked(this, e);

            divYahooPropertyEdit.Visible = false;
            divGrid.Visible = false;
            SearchMode = "Sort";
        }

        protected void rptSort_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (string.Equals(e.CommandName, "UPD", StringComparison.OrdinalIgnoreCase))
            {
                int id = int.TryParse(e.CommandArgument.ToString(), out id) ? id : 0;
                if (GetPponYahooPropertyBySort != null)
                {
                    GetPponYahooPropertyBySort(this, new DataEventArgs<int>(id));
                }
                divYahooPropertyEdit.Visible = true;
            }
        }

        protected void btnSortSave_Click(object sender, EventArgs e)
        {
            var json = new JsonSerializer();
            Dictionary<int, int?[]> sort = new Dictionary<int, int?[]>();
            Dictionary<int, int> sortN = json.Deserialize<Dictionary<int, int>>(hidSortN.Value);
            if (sortN != null)
            {
                JoinSortList(sortN, ref sort, 0);
            }
            Dictionary<int, int> sortC = json.Deserialize<Dictionary<int, int>>(hidSortC.Value);
            if (sortC != null)
            {
                JoinSortList(sortC, ref sort, 1);
            }
            Dictionary<int, int> sortS = json.Deserialize<Dictionary<int, int>>(hidSortS.Value);
            if (sortS != null)
            {
                JoinSortList(sortS, ref sort, 2);
            }

            if (this.OnSortSaveClicked != null)
                this.OnSortSaveClicked(this, new DataEventArgs<Dictionary<int, int?[]>>(sort));
        }

        protected void gvPponDeal_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is KeyValuePair<ViewPponDeal, KeyValuePair<YahooProperty, string[]>>)
            {
                KeyValuePair<ViewPponDeal, KeyValuePair<YahooProperty, string[]>> item = (KeyValuePair<ViewPponDeal, KeyValuePair<YahooProperty, string[]>>)e.Row.DataItem;

                Label lblBusinessHourGuid = ((Label)e.Row.FindControl("lblBusinessHourGuid"));
                Label lblBusinessHourOrderTime = ((Label)e.Row.FindControl("lblBusinessHourOrderTime"));
                Image dealImage = ((Image)e.Row.FindControl("dealImage"));
                LinkButton lkDealEdit = ((LinkButton)e.Row.FindControl("lkDealEdit"));
                Label lblItemOrigPrice = ((Label)e.Row.FindControl("lblItemOrigPrice"));
                Label lblItemPrice = ((Label)e.Row.FindControl("lblItemPrice"));
                Label lblCategorys = ((Label)e.Row.FindControl("lblCategorys"));
                Label lblRequestTime = ((Label)e.Row.FindControl("lblRequestTime"));
                Label lblYahooSetting = ((Label)e.Row.FindControl("lblYahooSetting"));
                Button btnCheck = ((Button)e.Row.FindControl("btnCheck"));
                CheckBox cbxCheck = ((CheckBox)e.Row.FindControl("chkVerification"));
                HiddenField hdField = ((HiddenField)e.Row.FindControl("hdYahooPropertyId"));
                
 
                // Action
                YahooProperty yp = item.Value.Key;
                if (yp.IsLoaded)
                {
                    YahooPropertyAction action = (YahooPropertyAction)yp.Action;
                    // 已移除(不可再操作)
                    if (action == YahooPropertyAction.Remove)
                    {
                        e.Row.ForeColor = System.Drawing.Color.Gray;
                        lkDealEdit.Enabled = false;
                    }
                    else if (action == YahooPropertyAction.Delete)
                    {
                        lblYahooSetting.ForeColor = System.Drawing.Color.Red;
                    }
                    lblYahooSetting.Text = Helper.GetLocalizedEnum((YahooPropertyAction)action);

                    if (!yp.RequestTime.HasValue)
                    {
                        lblRequestTime.Text = "檔次資料尚未傳送，無法更新!!";
                        //btnCheck.Visible = true;
                        //btnCheck.Enabled = false;
                        cbxCheck.Visible = true;
                        cbxCheck.Enabled = false;

                    }
                    else if (action != YahooPropertyAction.Ordinary && action != YahooPropertyAction.Remove)
                    {
                        lblRequestTime.Text = yp.RequestTime.Value.ToString("yyyy/MM/dd HH:mm");
                        btnCheck.CommandArgument = yp.Id.ToString();
                        hdField.Value = yp.Id.ToString();
                        //btnCheck.Visible = btnCheck.Enabled = true;
                        cbxCheck.Visible = cbxCheck.Enabled = true;
                    }
                    dealImage.ImageUrl = ImageFacade.GetMediaBaseUrl() + yp.ImgUrl;
                }
                else
                {
                    e.Row.BackColor = System.Drawing.Color.Yellow;
                    dealImage.Visible = false;
                }

                lblBusinessHourGuid.Text = item.Key.BusinessHourGuid.ToString();
                lblBusinessHourOrderTime.Text = string.Format("{0}<br />|<br />{1}", item.Value.Value[0], item.Value.Value[1]);
                lkDealEdit.Text += item.Key.ItemName ;
                lkDealEdit.CommandArgument = item.Key.BusinessHourGuid.ToString();
                lblItemOrigPrice.Text = item.Key.ItemOrigPrice.ToString("N0");
                lblItemPrice.Text = item.Key.ItemPrice.ToString("N0");
                lblCategorys.Text = item.Value.Value[2];
            }
        }

        protected void gvPponDeal_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "UPD":
                    Guid bid = Guid.TryParse(e.CommandArgument.ToString(), out bid) ? bid : Guid.Empty;
                    if (GetPponYahooProperty != null)
                    {
                        GetPponYahooProperty(this, new DataEventArgs<Guid>(bid));
                    }
                    divYahooPropertyEdit.Visible = true;
                    chkIsPrintDiscountMark.Checked = chkIsPrintLogoWaterMark.Checked = true;
                    chkIsCutCenter.Checked = false;
                    break;
                case "CHK":
                    int id = int.TryParse(e.CommandArgument.ToString(), out id) ? id : 0;
                    if (CheckPponYahooPropertyAction != null)
                    {
                        CheckPponYahooPropertyAction(this, new DataEventArgs<int>(id));
                    }
                    break;
                case "CHKSELECTED":
                    List<int> idList = new List<int>();

                    foreach (GridViewRow row in gvPponDeal.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox chkRow = (row.Cells[6].FindControl("chkVerification") as CheckBox);
                            if (chkRow.Checked)
                            {
                                HiddenField hdField = (row.Cells[6].FindControl("hdYahooPropertyId") as HiddenField);
                                if (hdField != null)
                                {
                                    idList.Add(int.Parse(hdField.Value));
                                }
                                
                            }
                        }
                    }

                    if (idList.Any())
                    {
                        if (CheckSelectedPponYahooPropertyAction != null)
                        {
                            CheckSelectedPponYahooPropertyAction(this, new DataEventArgs<List<int>>(idList));
                        }
                    }
                    break;
                default:
                    break;
            }
            ucPponPager.ResolvePagerView(CurrentPage);
        }

        protected void gvPiinlifeDeal_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is KeyValuePair<HiDealProduct, KeyValuePair<YahooProperty, string[]>>)
            {
                KeyValuePair<HiDealProduct, KeyValuePair<YahooProperty, string[]>> item = (KeyValuePair<HiDealProduct, KeyValuePair<YahooProperty, string[]>>)e.Row.DataItem;

                Label lblProductId = ((Label)e.Row.FindControl("lblProductId"));
                Label lblDealStartDate = ((Label)e.Row.FindControl("lblDealStartDate"));
                Image dealImage = ((Image)e.Row.FindControl("dealImage"));
                LinkButton lkDealEdit = ((LinkButton)e.Row.FindControl("lkDealEdit"));
                Label lblItemOrigPrice = ((Label)e.Row.FindControl("lblItemOrigPrice"));
                Label lblItemPrice = ((Label)e.Row.FindControl("lblItemPrice"));
                Label lblRequestTime = ((Label)e.Row.FindControl("lblRequestTime"));
                Label lblYahooSetting = ((Label)e.Row.FindControl("lblYahooSetting"));
                Button btnCheck = ((Button)e.Row.FindControl("btnCheck"));

                // Action
                YahooProperty yp = item.Value.Key;
                if (yp.IsLoaded)
                {
                    YahooPropertyAction action = (YahooPropertyAction)yp.Action;
                    // 已移除(不可再操作)
                    if (action == YahooPropertyAction.Remove)
                    {
                        e.Row.ForeColor = System.Drawing.Color.Gray;
                        lkDealEdit.Enabled = false;
                    }
                    else if (action == YahooPropertyAction.Delete)
                    {
                        lblYahooSetting.ForeColor = System.Drawing.Color.Red;
                    }
                    lblYahooSetting.Text = Helper.GetLocalizedEnum((YahooPropertyAction)action);

                    if (!yp.RequestTime.HasValue)
                    {
                        lblRequestTime.Text = "檔次資料尚未傳送，無法更新!!";
                        btnCheck.Visible = true;
                        btnCheck.Enabled = false;
                    }
                    else if (action != YahooPropertyAction.Ordinary && action != YahooPropertyAction.Remove)
                    {
                        lblRequestTime.Text = yp.RequestTime.Value.ToString("yyyy/MM/dd HH:mm");
                        btnCheck.CommandArgument = yp.Id.ToString();
                        btnCheck.Visible = btnCheck.Enabled = true;
                    }
                    dealImage.ImageUrl = ImageFacade.GetMediaBaseUrl() + yp.ImgUrl;
                }
                else
                {
                    e.Row.BackColor = System.Drawing.Color.Yellow;
                    dealImage.Visible = false;
                }

                lblProductId.Text = item.Key.Id.ToString();
                lblDealStartDate.Text = string.Format("{0}<br />|<br />{1}", item.Value.Value[0], item.Value.Value[1]);
                lkDealEdit.Text = item.Value.Value[2] ;
                lkDealEdit.CommandArgument = item.Key.Id.ToString();
                lblItemOrigPrice.Text = item.Key.OriginalPrice.HasValue ? item.Key.OriginalPrice.Value.ToString("N0") : string.Empty;
                lblItemPrice.Text = item.Key.Price.HasValue ? item.Key.Price.Value.ToString("N0") : string.Empty;
            }
        }

        protected void gvPiinlifeDeal_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "UPD":
                    int pid = int.TryParse(e.CommandArgument.ToString(), out pid) ? pid : 0;
                    if (GetPiinlifeYahooProperty != null)
                    {
                        GetPiinlifeYahooProperty(this, new DataEventArgs<int>(pid));
                    }
                    divYahooPropertyEdit.Visible = true;
                    chkIsPrintDiscountMark.Checked = chkIsPrintLogoWaterMark.Checked = true;
                    chkIsCutCenter.Checked = false;
                    break;
                case "CHK":
                    int id = int.TryParse(e.CommandArgument.ToString(), out id) ? id : 0;
                    if (CheckPponYahooPropertyAction != null)
                    {
                        CheckPponYahooPropertyAction(this, new DataEventArgs<int>(id));
                    }
                    break;
                default:
                    break;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int id = int.TryParse(lblNo.Text, out id) ? id : 0;
            if (SaveYahooProperty != null)
            {
                int type = int.TryParse(hidType.Value, out type) ? type : 0;
                string columnName = string.Empty;
                switch ((YahooPropertyType)type)
                {
                    case YahooPropertyType.Ppon:
                        columnName = YahooProperty.Columns.Bid;
                        break;
                    case YahooPropertyType.Piinlife:
                        columnName = YahooProperty.Columns.Pid;
                        break;
                    default:
                        break;
                }
                SaveYahooProperty(this, new DataEventArgs<KeyValuePair<string, string>>(new KeyValuePair<string, string>(columnName, lblDealId.Text)));
            }
            if (SearchMode == "Grid")
            {
                divGrid.Visible = true;
                divSort.Visible = false;
            }
            else if (SearchMode == "Sort")
            {
                divGrid.Visible = false;
                divSort.Visible = true;
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            divYahooPropertyEdit.Visible = false;
        }

        protected void btnUpdateAllAction_Click(object sender, EventArgs e)
        {
            PponFacade.YahooPropertyActionUpdateAll(UserId);
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                List<string> result = new List<string>();
                using (StreamReader reader = new StreamReader(FileUpload1.FileContent, System.Text.Encoding.Default))
                {
                    do
                    {
                        string textLine = reader.ReadLine();
                        if (textLine.IndexOf(',') > 0)
                        {
 
                        }
                        else
                        {
                            result.Add(textLine);
                        }
                    } while (reader.Peek() != -1);
                    reader.Close();
                }
                if (this.ImportBidList != null)
                {
                    this.ImportBidList(this, new DataEventArgs<List<string>>(result));
                }
            }
        }

        protected void PponUpdateHandler(int pageNumber)
        {
            if (this.PponPageChanged != null)
            {
                this.PponPageChanged(this, new DataEventArgs<int>(pageNumber));
                CurrentPage = pageNumber;
            }
        }

        protected int GetPponCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnPponGetCount != null)
            {
                OnPponGetCount(this, e);
            }
            return e.Data;
        }
        #endregion

        #region private method
        private void IntialControls()
        {
            txtDealStartDate.Text = DateTime.Now.Date.ToShortDateString();
            txtDealEndDate.Text = DateTime.Now.AddDays(1).Date.ToShortDateString();

            rdlType.DataSource = GetDealTypeDictionary();
            rdlType.DataValueField = "Key";
            rdlType.DataTextField = "Value";
            rdlType.DataBind();
            rdlType.SelectedIndex = 0;

            cklYahooAction.DataSource = GetYahooActionDictionary();
            cklYahooAction.DataValueField = "Key";
            cklYahooAction.DataTextField = "Value";
            cklYahooAction.DataBind();

            rdlCategory.DataSource = GetYahooPropertyCategoryDictionary();
            rdlCategory.DataValueField = "Key";
            rdlCategory.DataTextField = "Value";
            rdlCategory.DataBind();

            rdlAreaN.DataSource = GetYahooPropertyAreaNorthDictionary();
            rdlAreaN.DataValueField = "Key";
            rdlAreaN.DataTextField = "Value";
            rdlAreaN.DataBind();

            rdlAreaC.DataSource = GetYahooPropertyAreaCentralDictionary();
            rdlAreaC.DataValueField = "Key";
            rdlAreaC.DataTextField = "Value";
            rdlAreaC.DataBind();

            rdlAreaS.DataSource = GetYahooPropertyAreaSouthDictionary();
            rdlAreaS.DataValueField = "Key";
            rdlAreaS.DataTextField = "Value";
            rdlAreaS.DataBind();

            cklArea.DataSource = GetYahooPropertyAreaDictionary();
            cklArea.DataValueField = "Key";
            cklArea.DataTextField = "Value";
            cklArea.DataBind();


        }

        /// <summary>
        /// 取得Yahoo大團購檔次類型
        /// </summary>
        /// <returns></returns>
        private Dictionary<int, string> GetDealTypeDictionary()
        {
            Dictionary<int, string> dealType = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(YahooPropertyType)))
            {
                dealType[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (YahooPropertyType)item);
            }
            return dealType;
        }

        /// <summary>
        /// 取得Yahoo大團購檔次分類
        /// </summary>
        /// <returns></returns>
        private static Dictionary<int, string> GetYahooPropertyCategoryDictionary()
        {
            Dictionary<int, string> dealCategory = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(YahooPropertyCategory)))
            {
                dealCategory[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (YahooPropertyCategory)item);
            }
            return dealCategory;
        }

        /// <summary>
        /// 取得Yahoo大團購檔次區域(北區)
        /// </summary>
        /// <returns></returns>
        private static Dictionary<int, string> GetYahooPropertyAreaNorthDictionary()
        {
            Dictionary<int, string> dealArea = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(YahooPropertyAreaNorth)))
            {
                dealArea[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (YahooPropertyAreaNorth)item);
            }
            return dealArea;
        }

        /// <summary>
        /// 取得Yahoo大團購檔次區域(中區)
        /// </summary>
        /// <returns></returns>
        private static Dictionary<int, string> GetYahooPropertyAreaCentralDictionary()
        {
            Dictionary<int, string> dealArea = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(YahooPropertyAreaCentral)))
            {
                dealArea[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (YahooPropertyAreaCentral)item);
            }
            return dealArea;
        }

        /// <summary>
        /// 取得Yahoo大團購檔次區域(南區)
        /// </summary>
        /// <returns></returns>
        private static Dictionary<int, string> GetYahooPropertyAreaSouthDictionary()
        {
            Dictionary<int, string> dealArea = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(YahooPropertyAreaSouth)))
            {
                dealArea[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (YahooPropertyAreaSouth)item);
            }
            return dealArea;
        }

        private static Dictionary<int, string> GetYahooPropertyAreaDictionary()
        {
            Dictionary<int, string> dealArea = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(YahooPropertyArea)))
            {
                dealArea[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (YahooPropertyArea)item);
            }
            return dealArea;
        }

        private static Dictionary<int,string>GetYahooActionDictionary()
        {
            Dictionary<int, string> yahooAction = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(YahooPropertyAction)))
            {
                yahooAction[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (YahooPropertyAction)item);
            }
            return yahooAction;            
        } 


        private void CheckDir(string strDirPath)
        {
            if (!System.IO.Directory.Exists(strDirPath))
            {
                System.IO.Directory.CreateDirectory(strDirPath);
            }
        }

        private static void JoinSortList(Dictionary<int, int> sub_sort, ref Dictionary<int, int?[]> sort, int area)
        {
            foreach (KeyValuePair<int, int> n in sub_sort)
            {
                int?[] seq;
                if (!sort.TryGetValue(n.Key, out seq))
                {
                    seq = new int?[] { null, null, null };
                    sort.Add(n.Key, seq);
                }
                seq[area] = n.Value;
                sort[n.Key] = seq;
            }
        }
        #endregion
    }
}
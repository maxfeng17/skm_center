﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PponDealTimeSlot.aspx.cs" EnableViewState="false"
    Inherits="LunchKingSite.Web.ControlRoom.Ppon.PponDealTimeSlot" MasterPageFile="~/ControlRoom/backend.master" %>

<%@ Register Src="../Controls/PponDealTimeSlotRowForCity.ascx" TagName="PponDealTimeSlotRowForCity"
    TagPrefix="uc1" %>
<asp:Content ID="S" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="//www.google.com/jsapi"></script>    
    <script type="text/javascript">google.load("jqueryui", "1.8.4");</script>
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/js/collapser.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>    
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            console.log($('.slotItem').size());
            $(".slotItem").each(function () {
                $(this).find(".unit").on("mouseover mouseout", function (event) {
                    if (event.type == "mouseover") {
                        toggleAvailMode(this, true);
                    } else {
                        toggleAvailMode(this, false);
                    }
                });

                //手動排序
                $(this).bind("sortupdate", function (event, ui) {
                    //宅配改用一次性存檔
                    if (this.id.indexOf('AllCountry') < 0) {
                        var beforeSeqNo = $(ui.item).children("span").text();
                        var afterSeqNo = (ui.item.index() + 1).toString();
                        setSlotDataV2(this, beforeSeqNo, afterSeqNo, "手動排序");
                    }
                });

                console.log($('.unit', this).size());
                $('.unit', this).each(function () {
                    var dataId = $(this).attr('dataId');
                    var bid = dataId.split('|')[0];
                    
                    $(this).find('.url').attr('href', '../ppon/setup.aspx?bid=' + bid);                   
                });

                $(this).sortable(
                    {
                        items: 'div:not(.fixed)',
                        deactivate: function (event, ui) { //beforeStop deactivate
                            $(ui.item).parent().find('#no').each(function (x) { $(this).html(x + 1); });
                        }
                    }
                );

                //隱藏異動
                $(".noShow", this).bind("change", function () {
                    var dataId = $(this).parent().parent().parent().attr("dataid");
                    SetShowType($(this).is(':checked'), dataId);
                });

                //鎖定異動
                $(".lockSeq", this).bind("change", function () {
                    var slotItem = $(this).closest('.slotItem');
                    var dataId = $(this).closest('.unit').attr("dataid");
                    //var isToday = slotItem.parent().attr("class") === "day0";
                    //if (isToday) {
                        //鎖定今日檔次時，也一併更新Server排序，以確保目前檔次順序如眼前所見(即使同時間自動排序Job已更新Server排序，仍以Cline端順序為主)
                        //setSlotDataV2(slotItem, "", "", "更新排序"); 
                    //}
                    setLockSeq($(this).is(':checked'), dataId);
                    setLockStyle($(this), $(this).parent('label').children('.lockSpan'));
                    slotItem.sortable("destroy");
                    slotItem.sortable(
                        {
                            items: 'div:not(.fixed)',
                            deactivate: function (event, ui) { //beforeStop deactivate
                                $(ui.item).parent().find('#no').each(function (x) { $(this).html(x + 1); });
                            }
                        }
                    );
                });
            });
            
            //熱銷檔次設定異動
            $(".hotdealelement").bind("change keyup", function () {
                $("input[id*=saveHotDealSetting]").removeAttr("disabled");
                $("input[id*=saveAndSortHotDeal]").removeAttr("disabled");
            });

            //權重異動
            $(".number.element").bind("keyup", function () {
                $("input[id*=saveAndSort]").removeAttr("disabled");
            });

            //熱銷設定隱藏或顯示
            $(".hotdeal-ctrl").bind("mousedown", function () {
                if ($(this).html() == "顯示") {
                    $(".hotdeal-body").slideDown(400);//.fadeIn(1000);//.show();
                    $(this).html("隱藏");
                }
                else
                {
                    $(".hotdeal-body").slideUp(400);//.hide();//.fadeOut();
                    $(this).html("顯示");
                }
            });

            //自動排序設定隱藏或顯示
            $(".autosort-ctrl").bind("mousedown", function () {
                if ($(this).html() == "顯示") {
                    $(".autosort-body").slideDown(1000);//.show();
                    $(this).html("隱藏");
                }
                else {
                    $(".autosort-body").slideUp(1000);//.hide();
                    $(this).html("顯示");
                }
            });
            
            //宅配顯示儲存今日排序
            if ($(".slotItem").length > 0) {
                var slotItem = $(".slotItem")[0];
                if (slotItem.id.indexOf('AllCountry') >= 0) {
                    $('.saveTodaySeq').show();
                }
                else {
                    $('.saveTodaySeq').hide();
                }
            }

            //顯示參考昨天
            if ($('#ddlCity').val() == '-1' || $('#ddlCity').val() == '0') {
                $('.smart').remove();
            } else {
                $('.smart').show();
            }

            var d = new Date();
            if (d.getHours < 12) {
                $('.manual-tips-yesterday').show();
            }

            //參考昨天
            $('.smart').click(function() {
                var boxL = $('.' + $(this).attr('refBoxL')).find('.slotItem');
                var boxR = $('.' + $(this).attr('refBoxR')).find('.slotItem');
                var unitsL = boxL.find('.unit');
                var unitsR = boxR.find('.unit');

                var dealsL = [];
                var dealsR = [];
                var baseRank = 10;

                unitsL.each(function() {
                    var dataId = $(this).attr('dataId');
                    var title = $.trim($(this).find('.url').text());
                    var parts = dataId.split('|');
                    var item = { 'bid': parts[0], 'dataId': dataId, 'title': title, 'rank': baseRank };
                    dealsL.push(item);
                    baseRank++;
                });

                var pos = 0;
                unitsR.each(function() {
                    var dataId = $(this).attr('dataId');
                    var title = $.trim($(this).find('.url').text());
                    var parts = dataId.split('|');
                    var bid = parts[0];

                    var rank = 0;
                    var findPreDeals = jQuery.grep(dealsL, function(t) {
                        return t.bid == bid;
                    });

                    if (findPreDeals.length > 0) {
                        rank = findPreDeals[0].rank;
                    }

                    var item = { 'bid': bid, 'dataId': dataId, 'title': title, 'rank': rank, 'pos': pos };
                    dealsR.push(item);
                    pos++;
                });

                for (var i in dealsL) {
                    console.log(dealsL[i].bid + ' / ' + dealsL[i].rank + ' / ' + dealsL[i].title);
                }
                console.log('.....................');
                //sort data
                dealsR.sort(function(a, b) {
                    if (a.rank == b.rank) return 0;
                    if (a.rank > b.rank) {
                        return 1;
                    } else {
                        return -1;
                    }
                });

                for (var i in dealsR) {
                    console.log(dealsR[i].bid + ' / ' + dealsR[i].rank + ' / ' + dealsR[i].title);
                }
                //重排頁面上的檔次順序
                for (var i in dealsR) {
                    var dataId = dealsR[i].dataId;
                    var movableUnit = boxR.find('.unit[dataId="' + dataId + '"]').parent(); // 抓它的上層 div
                    boxR.append(movableUnit);
                }
                //update
                setSlotDataV2(boxR, "", "", "參考昨天");
            });
            
            //儲存今日排序
            $('.saveTodaySeq').click(function () {
                var slotItem = $('.' + $(this).attr('refBox')).find('.slotItem');
                setSlotDataV2(slotItem, "", "", "儲存今日排序");
            });

            if ($("#ddlCity").val() == '448') {//448 is piinlife city id
                $("#visa_hint").show();
            }
        });
        
        //儲存熱銷檔次設定
        function saveHotDealSetting(isSort) {
            $.blockUI({ message: $('.divMessage') });
            
            var cityId = $('.cityList :selected').val();
            var topN = $("input[id*=topN]").val();
            var days = $("input[id*=nDays]").val();
            var multiple = $("input[id*=nMultiple]").val();
            var enable = $("input[id*=hotDealEnable]").is(':checked');
            
            if (topN == "")
            {
                alert("請填入取多少排名!");
                $.unblockUI();
                return false;
            }
            if (days == "") {
                alert("請填入最近幾天!");
                $.unblockUI();
                return false;
            }
            if (multiple == "") {
                alert("請填入串插倍數!");
                $.unblockUI();
                return false;
            }
            if (topN == 0 || days == 0 || multiple == "0") {
                alert("填入值不可為 0 !");
                $.unblockUI();
                return false;
            }

            $.ajax({
                type: "POST",
                url: "PponDealTimeSlot.aspx/SaveHotDealSetting",
                data: '{"cityId":"' + cityId + '","topN":"' + topN + '","days":"' + days + '","multiple":"' + multiple +
                    '","enable":"' + enable + '","isSort":"' + isSort + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    if (msg.d != '') {
                        alert(msg.d);
                    }
                    $.unblockUI();
                }
            });
            return false;
        }

        //更新權重並排序
        function saveAndSortWeights() {
            $.blockUI({ message: $('.divMessage') });

            var cityId = $('.cityList :selected').val();
            var cityName = $('.cityList :selected').text();
            var elements = [];
            var isSort = false;

            $(".element").each(function () {
                var value = parseFloat($(this).val() == "" ? 0 : $(this).val());
                elements.push(value);
                if (value > 0) isSort = true;
            });

            if (isSort) {
                $.ajax({
                    type: "POST",
                    url: "PponDealTimeSlot.aspx/SaveAndSortWeights",
                    data: '{"cityId":"' + cityId + '","cityName":"' + cityName + '","elements":"' + elements + '"}',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        if (msg.d != '') {
                            alert(msg.d);
                        }
                        $.unblockUI();
                    }
                });
            } else {
                alert("權重不可全部為0!");
                $.unblockUI();
                return false;
            }
            return true;
        }

        //更新排序 v1
        function setSlotData(slotItem, beforeSeqNo, afterSeqNo, operation) {
            $.blockUI({ message: $('.divMessage') });
            console.log(slotItem.tagName + '/' + $(slotItem).find('.unit').size());
            var dataIds = new Array();
            $(slotItem).find('.unit').each(function () {
                console.log(this.tagName);
                var dataId = $(this).attr('dataId');
                if (dataId != '') {
                    dataIds.push(dataId);
                }
            });
            $.ajax({
                type: "POST",
                url: "PponDealTimeSlot.aspx/SetSlotData",
                data: '{"data":"' + dataIds.join(',') + '","beforeSeqNo":"' + beforeSeqNo + '","afterSeqNo":"' + afterSeqNo + '","operation":"' + operation + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    if (msg.d != '') {
                        alert(msg.d);
                    }
                },
                complete: function () {
                    $.unblockUI();
                }
            });
            return true;
        }

        //更新排序 v2
        function setSlotDataV2(slotItem, beforeSeqNo, afterSeqNo, operation) {
            $.blockUI({ message: $('.divMessage') });
            var dataIds = new Array();
            $(slotItem).find('.unit').each(function () {
                console.log(this.tagName);
                var dataId = $(this).attr('dataId');
                if (dataId != '') {
                    dataIds.push(dataId);
                }
            });
            $.ajax({
                type: "POST",
                url: "PponDealTimeSlot.aspx/SetSlotDataV2",
                data: JSON.stringify({
                    dataList: dataIds,
                    beforeSeqNo,
                    afterSeqNo,
                    operation
                    }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    if (result.d != null) {
                        if (operation == "儲存今日排序") {
                            if (result.d.Code == '1000') {
                                alert('儲存今日排序成功!');
                            }
                            else {
                                alert('儲存今日排序失敗，請洽技術部!');
                            }
                        }
                        else {
                            if (result.d.Code != '1000' && result.d.Message != null) {
                                alert(result.Message);
                            }
                        }
                    }
                },
                error: function () {
                    
                },
                complete: function () {
                    $.unblockUI();
                }
            });
        }

        //隱藏顯示檔次
        function SetShowType(chkNoShow, dataId) {
            var show = !chkNoShow;
            $.blockUI({ message: $('.divMessage') });
            $.ajax({
                type: "POST",
                url: "PponDealTimeSlot.aspx/SetDealTimeSlotStatus",
                data: '{"dataId":"' + dataId + '","isShow":"' + show + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    if (msg.d.result == false) {
                        alert(msg.d.message);
                    } else {
                        for (var key in msg.d.data) {
                            var effectDataId = msg.d.data[key];
                            console.log(effectDataId);
                            var unit = $(".unit[dataId='" + effectDataId +  "']");
                            setShowStyle(unit, show);
                        }
                    }
                },
                complete: function() {
                    $.unblockUI();
                }
            });
        }

        function setShowStyle(unit, show) {
            var target = unit.find('.noShow');
            if (show) {
                target.removeAttr('checked');
                unit.removeClass('unit-hidden');
                $(target).parent().parent().children().children('span').removeClass('span_hidden');
            } else {
                target.attr('checked', true);
                unit.addClass('unit-hidden');
                $(target).parent().parent().children().children('span').addClass('span_hidden');
            }
        }

        //鎖定排序
        function setLockSeq(isLockSeq, dataId) {
            //$.blockUI({ message: $('.divMessage') });
            $.ajax({
                type: "POST",
                url: "PponDealTimeSlot.aspx/SetDealTimeSlotLockSeq",
                data: '{"dataId":"' + dataId + '","isLockSeq":"' + isLockSeq + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    if (msg.d != '') {
                        alert(msg.d);
                    } 
                },
                complete: function () {
                    //$.unblockUI();
                }
            });
        }

        function setLockStyle(target1, target2) {
            if (target1.is(':checked')) {
                target2.addClass("lock_seq_lb");
                target1.parent().parent().parent().parent().addClass('fixed');
            } else {
                target2.removeClass("lock_seq_lb");
                target1.parent().parent().parent().parent().removeClass('fixed');
            }
        }
        
        function SetNumberOnly() {
            if (!(event.keyCode == 46 || (event.keyCode >= 48 && event.keyCode <= 57))) { event.returnValue = false; }
        }

        function toggleAvailMode(dom, on) {
            if (on) {
                $(dom).addClass("hover");
            } else {
                $(dom).removeClass("hover");
            }
        }

    </script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
    <style type="text/css">
        body
        {
            font-size: 10px;
        }
        .field
        {
            margin: 5px 0px 5px 0px;
            border: solid 1px gray;
            width: 1000px;
        }
        .field legend
        {
            color: blue;
            font-weight: bolder;
        }
        .innerfield
        {
            margin: 0px 0px 0px 0px;
            border: solid 1px gray;
            width: 800px;
        }
        .innerfield legend
        {
            color: #4F8AFF;
            font-weight: bolder;
        }
        ul
        {
            list-style-type: none;
            margin: 0 0 0 0;
        }
        li
        {
            color: #4D4D4D;
            font-weight: bold;
        }
        
        ul.status li
        {
            float: left;
        }
        ul.target input
        {
            width: 80px;
        }
        
        label
        {
            color: black;
            font-size: medium;
            padding-right: 10px;
        }
        input
        {
            border: solid 1px grey;
        }
        textarea
        {
            border: solid 1px grey;
        }
        
        input.dealName
        {
            width: 300px;
        }
        input.date
        {
            width: 80px;
        }
        
        .price
        {
            color: red;
            font-weight: bolder;
        }
        .orderInfo
        {
            width: auto;
            float: right;
        }
        .orderInfo td
        {
            border: dotted 1px grey;
            padding: 3px 3px 3px 3px;
        }
        .orderInfo td td
        {
            border: none;
        }
        .orderInfo li
        {
            color: black;
            font-weight: bold;
            margin: 3px 3px 3px 3px;
        }
        .intro
        {
            float: left;
            margin: 5px 0px 5px 0px;
            border: none;
        }
        .intro legend
        {
            color: #4F8AFF;
            font-weight: bolder;
        }
        .intro textarea
        {
            width: 500px;
        }
        
        .watermarkOn
        {
            color: #AAAAAA;
            font-style: italic;
        }
        
        .slotItem ul
        {
            margin-bottom: 3px;
            border: dashed 1px gray;
            padding: 2px 2px 2px 5px;
        }
        
        .slotItem ul.hover
        {
            background-color: #E5ECF9;
        }
        
        .slotItem li
        {
            font-weight: normal;
            font-size: smaller;
            margin: 0px;
        }
        .slotItem li.eb, .slotItem li.db, .slotItem li.cb
        {
            display: none;
            float: left;
            padding-right: 15px;
            text-decoration: underline;
            color: blue;
            cursor: pointer;
        }
        .btn_left
        {
            float: left;
        }
        .btn_mid
        {
            float: left;
            margin-left: 100px;
        }
        .btn_right
        {
            float: right;
        }
        .btn_right a
        {
            margin: 0px 10px 0px 10px;
        }
        table
        {
            white-space: normal;
            line-height: normal;
            font-weight: normal;
            font-size: medium;
            font-variant: normal;
            font-style: normal;
            color: -webkit-text;
        }
        label
        {
            font-weight: normal;
            font-size: smaller;
            letter-spacing: 1px;
        }
        .unit {
            width: 200px; 
            border-radius:3px;
        }
        .unit-yesterday-new 
        {
            background-color: #bef18c;
            border-radius:3px;
        }
        .unit-today-new 
        {
            background-color: #FFFF66;
            border-radius:3px;
        }
        .unit-tomorrow-new 
        {
            background-color: #66CCFF;
            border-radius:3px;
        }
        .unit-hidden a, .unit-hidden label, .unit-hidden label .lockSpan 
        {
            color: #B8B8B8 !important;
        }
        .unit-hidden label .hotSpan
        {
            background-color: #ffb3b3 !important;
        }
        .unit-visa
        {
            background: url(<%=this.ResolveUrl("~/Themes/HighDeal/images/index/VISA_f.png")%>) ;
            background-position:right bottom;
            background-repeat: no-repeat;
        }
        .unit-visa2
        {
            background: url(<%=this.ResolveUrl("~/Themes/HighDeal/images/index/VISA_f2.png")%>) ;
            background-position:right bottom;
            background-repeat: no-repeat;
        }
        .lock_seq_lb 
        {
            color: red;
        }
        .span_hidden {
            color: #800000;
        }
        .autosort-ctrl, .hotdeal-ctrl {
            color: #3399FF;
            font-size:9pt;
        }
        .autosort-body 
        {
            display: none;
        }
        .autosort-body .tips {
            width: 90%;
            font-size: small;
            color: #3399FF;
        }
        .hotdeal-body {
            display: none;
        }
        .manual-tips-icon 
        {
            border-radius:3px;
            border-style:dotted;
            border-width:1px;
        }
        .auto-style3 {
            width: 90px;
            text-align: center;
        }
        .dealtimeslotCtrlTable {
            font-size: small;
            width: 70%;
            border: 1px solid #AAAAAA;
            padding:5px;
        }
        .dealtimeslotCtrlTable tr:first-child td{            
            text-align: center;
            background-color: #FF9999;
        }
        .aa {
            color: #000099;
        }
        .day0, .day1, .day2, .day3, .day4, .day5, .day6 {
            display: block; 
            vertical-align: top;
        }
        .div-left{ 
            float: left;
            width: 20px;
            display: block;
            text-align:center;
        } 
        .div-right {
            float: right;
            width: 210px;
        } 
        .divMessage {
            display: none;
        }
        .lbTitle, .lbCol {
            font: small verdana;
        }
        .auto-style4 {
            font-size: small;
        }
        .hotSpan {
            background-color: #e60000;
            color: white;
            font-size: 6pt;
            padding: 0 2px 0 2px;
            border-radius:5px;
        }
        #btnResetCache {
            opacity:0.3;
        }
        #messageBox {
            position: absolute;
            right: 80px;
            top: 5px;
            width: 200px;
            background-color: aliceblue;
            padding: 5px;
            font-size: 13px;
            border:1px solid rgb(138, 138, 250);
            display: none;
        }
        /*遮罩*/
        .mask-wrapper {
            position: relative;
            overflow: hidden;
        }
        .mask-inner {
            position: absolute;
            left: 0;
            width: 100%;
            height: 100%;
            /*background: rgba(0,0,0,.1);*/
            -moz-transition: top ease 200ms;
            -o-transition: top ease 200ms;
            -webkit-transition: top ease 200ms;
            transition: top ease 200ms;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
     <div class="divMessage" >資料處理中，請稍後...</div>
    <div >
    <span style="float:left; display:inline-block">
        <asp:DropDownList ID="ddlCity" runat="server" OnSelectedIndexChanged="SelectCityChanged" class ="cityList"
            AutoPostBack="true" DataTextField="Value" DataValueField="Key" ClientIDMode="Static" />
    </span>
    <input id="HiddenAuthor" type="hidden" runat="server"/>
    <span style="font-size: 11px; vertical-align: top; float: left; display: inline-block; padding-top:3px; display:none" id="visa_hint">
        <img src="/Themes/HighDeal/images/index/VISA_f.png">
        不會顯示在EDM中。不會顯示在各分類與 "全部" 分類中。     
    </span>
        <br />
    </div>

    <%--熱銷排序--%>
    <asp:Panel ID="hotDealSettingPanel" runat="server" Visible="False">
        <div style="text-align:left;margin:15px 0 10px 0;font-size:12pt">
            <b>熱銷排序設定</b>&nbsp;&nbsp;&nbsp;<a class="hotdeal-ctrl url">顯示</a>
        </div>
        <div class="hotdeal-body">
            <asp:CheckBox ID="hotDealEnable" runat="server" Text="開啟" class="hotdealelement" /><span class="auto-style4">，
                最近 <asp:TextBox ID="nDays" runat="server" Width="30px" style="text-align:right" onKeypress="SetNumberOnly()" class="hotdealelement"></asp:TextBox>天，排名前 <%--<asp:Label ID="topNLab" runat="server" Text=""></asp:Label>--%><asp:TextBox ID="topN" runat="server" Width="30px" style="text-align:right" onKeypress="SetNumberOnly()" class="hotdealelement"></asp:TextBox> 檔熱銷檔次，
                以 <asp:TextBox ID="nMultiple" runat="server" Width="30px" style="text-align:right" onKeypress="SetNumberOnly()" class="hotdealelement"></asp:TextBox>&nbsp;的倍數穿插呈現。</span>
                &nbsp;&nbsp;&nbsp;
            <asp:Button ID="saveHotDealSetting" runat="server" Text="儲存" disabled="true" OnClientClick="return saveHotDealSetting(false)" />　　
            <asp:Button ID="saveAndSortHotDeal" runat="server" Text="儲存並馬上排序" OnClientClick="return saveHotDealSetting(true)" disabled="true" />
        </div>
    </asp:Panel>

    <%--自動排序--%>
    <asp:Panel ID="weightPanel" runat="server" Visible="False">
        <div style="text-align:left;margin:15px 0 10px 0;font-size:12pt">
            <b>自動排序設定</b>&nbsp;&nbsp;&nbsp;<a class="autosort-ctrl url">顯示</a>
        </div>
        <div class="autosort-body">
            <span class="tips">(每整點執行自動排序)</span>
            <table class="dealtimeslotCtrlTable" rules="all">
                <tr >
                    <td>項目</td>
                    <td>權重<br />(未填則為0)</td>
                    <td>計算方式</td>
                </tr>
                <tr>
                    <td class="auto-style3">毛利率</td>
                    <td class="auto-style3"><asp:TextBox ID="elementWeight_1" runat="server" Width="80px" style="text-align:right" onKeypress="SetNumberOnly()" class="number element"></asp:TextBox>
                    </td>
                    <td><span class="aa">(單價－成本)／單價</span><br /><asp:Label ID="rankScore_1" runat="server" Text=""></asp:Label></td>
                </tr>
                <tr>
                    <td class="auto-style3">毛利額</td>
                    <td class="auto-style3"><asp:TextBox ID="elementWeight_2" runat="server" Width="80px" style="text-align:right" onKeypress="SetNumberOnly()" class="number element" ></asp:TextBox></td>
                    <td><span class="aa">毛利率＊單價＊銷量</span><br /><asp:Label ID="rankScore_2" runat="server" Text=""></asp:Label></td>
                </tr>
                <tr>
                    <td class="auto-style3">銷量</td>
                    <td class="auto-style3"><asp:TextBox ID="elementWeight_3" runat="server" Width="80px" style="text-align:right" onKeypress="SetNumberOnly()" class="number element" ></asp:TextBox></td>
                    <td><span class="aa">不算續接份數與倍數</span><br /><asp:Label ID="rankScore_3" runat="server" Text=""></asp:Label></td>
                </tr>
                <tr>
                    <td class="auto-style3">折數</td>
                    <td class="auto-style3"><asp:TextBox ID="elementWeight_4" runat="server" Width="80px" style="text-align:right" onKeypress="SetNumberOnly()" class="number element" ></asp:TextBox></td>
                    <td><span class="aa">單價／原價</span><br /><asp:Label ID="rankScore_4" runat="server" Text=""></asp:Label></td>
                </tr>
                <tr>
                    <td class="auto-style3">營收</td>
                    <td class="auto-style3"><asp:TextBox ID="elementWeight_5" runat="server" Width="80px" style="text-align:right" onKeypress="SetNumberOnly()" class="number element" ></asp:TextBox></td>
                    <td><span class="aa">單價＊銷量</span><br/><asp:Label ID="rankScore_5" runat="server" Text=""></asp:Label></td>
                </tr>
                <tr>
                    <td class="auto-style3">上架天數</td>
                    <td class="auto-style3"><asp:TextBox ID="elementWeight_6" runat="server" Width="80px" style="text-align:right" onKeypress="SetNumberOnly()" class="number element" ></asp:TextBox></td>
                    <td><asp:Label ID="rankScore_6" runat="server" Text=""></asp:Label></td>
                </tr>
                <tr>
                    <td class="auto-style3">單價</td>
                    <td class="auto-style3"><asp:TextBox ID="elementWeight_7" runat="server" Width="80px" style="text-align:right" onKeypress="SetNumberOnly()" class="number element" ></asp:TextBox></td>
                    <td><asp:Label ID="rankScore_7" runat="server" Text=""></asp:Label></td>
                </tr>
                <tr>
                    <td class="auto-style3">賣家規模</td>
                    <td class="auto-style3"><asp:TextBox ID="elementWeight_8" runat="server" Width="80px" style="text-align:right" onKeypress="SetNumberOnly()" class="number element" ></asp:TextBox></td>
                    <td><asp:Label ID="rankScore_8" runat="server" Text=""></asp:Label></td>
                </tr>
            </table>
            <br />
            <table style="width:70%"><tr><td style="text-align:right">
                <asp:Button ID="saveAndSort" runat="server" Text="儲存並馬上排序" OnClientClick="return saveAndSortWeights()" disabled="true" />
                </td></tr></table>
            
        </div>
    </asp:Panel>
    
    <%--手動排序--%>
    <div style="text-align:left;margin:15px 0 10px 0;font-size:12pt">
        <b>手動排序調整</b>&nbsp;&nbsp;&nbsp;<a class="manual-ctrl url"></a>
        <label><span class="manual-tips-yesterday" style="display:none;"><span class="unit-yesterday-new manual-tips-icon">&nbsp;　</span>&nbsp;昨日上檔　</span>
            <span class="unit-today-new manual-tips-icon">&nbsp;　</span>&nbsp;今日上檔　
            <span class="unit-tomorrow-new manual-tips-icon">&nbsp;　</span>&nbsp;明日上檔　
            <span class="hotSpan">HOT</span>&nbsp;熱銷檔次
        </label>
    </div>
    <div class="mask-wrapper">
        <uc1:PponDealTimeSlotRowForCity ID="clTitle" runat="server" />
        <uc1:PponDealTimeSlotRowForCity ID="clAllCountry" runat="server" />
        <uc1:PponDealTimeSlotRowForCity ID="clTaipei" runat="server" />
        <uc1:PponDealTimeSlotRowForCity ID="clNewTaipeiCity" runat="server" />
        <uc1:PponDealTimeSlotRowForCity ID="clTaoyuan" runat="server" />
        <uc1:PponDealTimeSlotRowForCity ID="clHsinchu" runat="server" />
        <uc1:PponDealTimeSlotRowForCity ID="clTaichung" runat="server" />
        <uc1:PponDealTimeSlotRowForCity ID="clTainan" runat="server" />
        <uc1:PponDealTimeSlotRowForCity ID="clKaohsiung" runat="server" />
        <uc1:PponDealTimeSlotRowForCity ID="clTravel" runat="server" />
        <uc1:PponDealTimeSlotRowForCity ID="clPeauty" runat="server" />
        <uc1:PponDealTimeSlotRowForCity ID="clCom" runat="server" />
        <uc1:PponDealTimeSlotRowForCity ID="clDepositCoffee" runat="server" />
        <uc1:PponDealTimeSlotRowForCity ID="clFamily" runat="server" />
        <uc1:PponDealTimeSlotRowForCity ID="clSkm" runat="server" />
        <uc1:PponDealTimeSlotRowForCity ID="clTmall" runat="server" />
        <uc1:PponDealTimeSlotRowForCity ID="clPiinlife" runat="server" />
        <div class="mask-inner">
        </div>
    </div>
    <button id="btnResetCache" type="button" style="position:absolute; right:10px; top: 5px; cursor:pointer">清暫存</button>
    <span id="messageBox">
    </span>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $('#btnResetCache').hover(
                function () {
                    $(this).css('opacity', '1.0');
                },
                function () {
                    $(this).css('opacity', '0.3');
                });
            $('#btnResetCache').click(function () {
                $('#messageBox').hide();
                $.blockUI({ message: $('.divMessage') });
                $.ajax({
                    type: "POST",
                    url: "PponDealTimeSlot.aspx/ResetCache",
                    data: '{}',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        if (msg.d !== '') {
                            $('#messageBox').show().html(msg.d);
                            window.setTimeout(function () {
                                $('#messageBox').fadeOut('slow');
                            }, 3000);
                        }
                    }
                }).done(function() {
                    $.unblockUI();
                });
            });

            if ($('#<%= HiddenAuthor.ClientID %>').val() == "true") {
                console.log('true');
                $(".mask-inner").css("top", "100%");
            } else {
                console.log('false');
                $(".mask-inner").css("top", 0);
            }
            
        });
    </script>
</asp:Content>

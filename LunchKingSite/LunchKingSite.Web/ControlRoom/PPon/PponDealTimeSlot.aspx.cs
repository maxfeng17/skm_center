﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using LunchKingSite.BizLogic.Component;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class PponDealTimeSlot : RolePage, IPponDealTimeSlotView
    {
        protected static ILog log = LogManager.GetLogger(typeof(PponDealTimeSlot));
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region event

        public event EventHandler<DataEventArgs<PponDealTimeSlotViewSearchRequest>> SelectCityIdChange = null;

        #endregion event

        #region props

        private PponDealTimeSlotPresenter _presenter;
        public PponDealTimeSlotPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public void ShowPponDealTimeSlotData(PponDealCalendarPage page)
        {
            ShowSlotDate(page);
            ShowCityName(page);
            ShowSlotDetail(page);
        }

        public void ShowHotDealSetting(HotDealConfig hotDealConf)
        {
            if (!config.EnableHotDealSetting) return;
            if (!hotDealConf.IsLoaded) return;

            hotDealEnable.Checked = hotDealConf.Enable;
            nDays.Text = hotDealConf.NDays.ToString();
            topN.Text = hotDealConf.TopN.ToString();
            nMultiple.Text = hotDealConf.NMultiple.ToString();

            hotDealSettingPanel.Visible = true;
        }

        public void ShowPponDealTimeSlotWeights(List<DealTimeSlotSortElement> weights, List<DealTimeSlotSortElementRank> scoreRank)
        {
            if (weights == null) return;

            foreach (var w in weights)
            {
                switch (w.ElementId)
                {
                    case (int)DealTimeSlotElement.GrossMargin:
                        elementWeight_1.Text = ((float)w.ElementWeight).ToString("G");
                        rankScore_1.Text = SetScoreRankDiscription("%(含)以下", "%(含)", "%(不含)以上", 100, scoreRank.Where(x => x.ElementId == (int)DealTimeSlotElement.GrossMargin).ToList());
                        break;
                    case (int)DealTimeSlotElement.GrossProfit:
                        elementWeight_2.Text = ((float)w.ElementWeight).ToString("G");
                        rankScore_2.Text = SetScoreRankDiscription(" 以下", "", " 以上", 1, scoreRank.Where(x => x.ElementId == (int)DealTimeSlotElement.GrossProfit).ToList());
                        break;
                    case (int)DealTimeSlotElement.Quantity:
                        elementWeight_3.Text = ((float)w.ElementWeight).ToString("G");
                        rankScore_3.Text = SetScoreRankDiscription(" 以下", "", " 以上", 1, scoreRank.Where(x => x.ElementId == (int)DealTimeSlotElement.Quantity).ToList());
                        break;
                    case (int)DealTimeSlotElement.DiscountRate:
                        elementWeight_4.Text = ((float)w.ElementWeight).ToString("G");
                        rankScore_4.Text = SetScoreRankDiscription("折(不含)", "折(含)", "折(含)", 10, scoreRank.Where(x => x.ElementId == (int)DealTimeSlotElement.DiscountRate).ToList());
                        break;
                    case (int)DealTimeSlotElement.Revenue:
                        elementWeight_5.Text = ((float)w.ElementWeight).ToString("G");
                        rankScore_5.Text = SetScoreRankDiscription(" 以下", "", " 以上", 1, scoreRank.Where(x => x.ElementId == (int)DealTimeSlotElement.Revenue).ToList());
                        break;
                    case (int)DealTimeSlotElement.Days:
                        elementWeight_6.Text = ((float)w.ElementWeight).ToString("G");
                        rankScore_6.Text = SetScoreRankDiscription("天 以上", "天", "1天 以下", 1, scoreRank.Where(x => x.ElementId == (int)DealTimeSlotElement.Days).ToList());
                        break;
                    case (int)DealTimeSlotElement.Price:
                        elementWeight_7.Text = ((float)w.ElementWeight).ToString("G");
                        rankScore_7.Text = SetScoreRankDiscription(" 以下", "", " 以上", 1, scoreRank.Where(x => x.ElementId == (int)DealTimeSlotElement.Price).ToList());
                        break;
                    case (int)DealTimeSlotElement.SellerLevel:
                        elementWeight_8.Text = ((float)w.ElementWeight).ToString("G");
                        rankScore_8.Text = SetScoreRankDiscription("", "", "", 1, scoreRank.Where(x => x.ElementId == (int)DealTimeSlotElement.SellerLevel).ToList());
                        break;
                }
            }

            weightPanel.Visible = true;
            var enableSaveAndSort = CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.SavingAndSorting);
            saveAndSort.Visible = saveHotDealSetting.Visible = saveAndSortHotDeal.Visible = enableSaveAndSort;
        }

        #endregion

        public PponDealTimeSlot()
        {
            this.Init += delegate (object sender, EventArgs e)
             {
                SetCityDropDownList();
            };
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            clTitle.Visible = false;
            clAllCountry.Visible = false;
            clTaipei.Visible = false;
            clNewTaipeiCity.Visible = false;
            clTaoyuan.Visible = false;
            clHsinchu.Visible = false;
            clTaichung.Visible = false;
            clTainan.Visible = false;
            clKaohsiung.Visible = false;
            clTravel.Visible = false;
            clPeauty.Visible = false;
            clCom.Visible = false;
            clDepositCoffee.Visible = false;
            clFamily.Visible = false;
            clTmall.Visible = false;
            clTmall.Visible = false;
            clPiinlife.Visible = false;
            clSkm.Visible = false;

            int cityId;
            HiddenAuthor.Value = "true";
            if (config.DealTimeSlotEnableSortLockHide)
            {
                if (Int32.TryParse(ddlCity.SelectedValue, out cityId))
                    HiddenAuthor.Value = HasSortLockHideAuthor(User.Identity.Name, cityId) ? "true" : "false";
            }

            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
            }
            this.Presenter.OnViewLoaded();
        }

        private void SetCityDropDownList()
        {
            List<KeyValuePair<int, string>> cityList = PponCityGroup.DefaultPponCityGroup.GetPponCityForSetting().Select(x => new KeyValuePair<int, string>(x.CityId, x.CityName)).OrderBy(x => x.Key).ToList();
            cityList.Insert(0, new KeyValuePair<int, string>(-1, "請選擇"));
            ddlCity.DataSource = cityList;
            ddlCity.DataBind();
        }

        private static string SetScoreRankDiscription(string firstKeyWords, string middleKeyWords, string lastKeyWords, decimal multiplier, List<DealTimeSlotSortElementRank> elementScoreRank)
        {
            var elementId = elementScoreRank.Select(x => x.ElementId).First();
            var returns = new StringBuilder();

            for (var i = 0; i < elementScoreRank.Count(); i++)
            {
                if (i == 0)
                {
                    switch (elementId)
                    {
                        case (int)DealTimeSlotElement.Days:
                            returns.Append(
                                (float)(elementScoreRank[i].RankStart * multiplier) + firstKeyWords + ": " + (float)elementScoreRank[i].Score + "分，");
                            break;
                        case (int)DealTimeSlotElement.SellerLevel:
                            returns.Append((SellerLevel)((int)elementScoreRank[i].RankEnd) + ": " + (float)elementScoreRank[i].Score + "分，");
                            break;
                        case (int)DealTimeSlotElement.GrossProfit:
                        case (int)DealTimeSlotElement.Quantity:
                        case (int)DealTimeSlotElement.Revenue:
                        case (int)DealTimeSlotElement.Price:
                            returns.Append((float)(elementScoreRank[i].RankEnd) + firstKeyWords + ": " + (float)elementScoreRank[i].Score + "分，");
                            break;
                        case (int)DealTimeSlotElement.DiscountRate:
                            returns.Append((float)(elementScoreRank[i].RankStart * multiplier) + firstKeyWords + "~原價" + ": " + (float)elementScoreRank[i].Score + "分，");
                            break;
                        case (int)DealTimeSlotElement.GrossMargin:
                            returns.Append(
                                (float)(elementScoreRank[i].RankEnd * multiplier) + firstKeyWords + ": " + (float)elementScoreRank[i].Score + "分，");
                            break;
                    }
                }
                else if (i == elementScoreRank.Count() - 1)
                {
                    switch (elementId)
                    {
                        case (int)DealTimeSlotElement.Days:
                            returns.Append(
                                lastKeyWords + ": " + (float)elementScoreRank[i].Score + "分");
                            break;
                        case (int)DealTimeSlotElement.SellerLevel:
                            returns.Append((SellerLevel)((int)elementScoreRank[i].RankEnd) + ": " + (float)elementScoreRank[i].Score + "分");
                            break;
                        case (int)DealTimeSlotElement.GrossProfit:
                        case (int)DealTimeSlotElement.Quantity:
                        case (int)DealTimeSlotElement.Revenue:
                        case (int)DealTimeSlotElement.Price:
                            returns.Append((float)(elementScoreRank[i].RankStart + 1) + lastKeyWords + ": " + (float)elementScoreRank[i].Score + "分");
                            break;
                        case (int)DealTimeSlotElement.DiscountRate:
                            returns.Append("0~" + (float)(elementScoreRank[i].RankEnd * multiplier) + lastKeyWords + ": " + (float)elementScoreRank[i].Score + "分");
                            break;
                        case (int)DealTimeSlotElement.GrossMargin:
                            returns.Append((float)(elementScoreRank[i].RankStart * multiplier) + lastKeyWords + ": " + (float)elementScoreRank[i].Score + "分");
                            break;
                    }
                }
                else
                {
                    switch (elementId)
                    {
                        case (int)DealTimeSlotElement.Days:
                            returns.Append((float)(elementScoreRank[i].RankStart + 1) + "~" + (float)(elementScoreRank[i].RankEnd) + middleKeyWords + ": " + (float)elementScoreRank[i].Score + "分，");
                            break;
                        case (int)DealTimeSlotElement.SellerLevel:
                            returns.Append((SellerLevel)((int)elementScoreRank[i].RankEnd) + ": " + (float)elementScoreRank[i].Score + "分，");
                            break;
                        case (int)DealTimeSlotElement.GrossProfit:
                        case (int)DealTimeSlotElement.Quantity:
                        case (int)DealTimeSlotElement.Revenue:
                        case (int)DealTimeSlotElement.Price:
                            returns.Append((float)(elementScoreRank[i].RankStart + 1) + "~" + (float)(elementScoreRank[i].RankEnd) + ": " + (float)elementScoreRank[i].Score + "分，");
                            break;
                        case (int)DealTimeSlotElement.DiscountRate:
                            returns.Append((float)(elementScoreRank[i].RankStart * multiplier) + firstKeyWords + "~" + (float)(elementScoreRank[i].RankEnd * multiplier) +
                            middleKeyWords + ": " + (float)elementScoreRank[i].Score + "分，");
                            break;
                        case (int)DealTimeSlotElement.GrossMargin:
                            returns.Append((float)(elementScoreRank[i].RankStart * multiplier) + "~" + (float)(elementScoreRank[i].RankEnd * multiplier) +
                            middleKeyWords + ": " + (float)elementScoreRank[i].Score + "分，");
                            break;
                    }
                }
            }
            return returns.ToString();
        }

        protected void ShowCityName(PponDealCalendarPage page)
        {
            clAllCountry.SetCityName(page.AllCountryData.CityName);
            clTaipei.SetCityName(page.TaipeiData.CityName);
            clNewTaipeiCity.SetCityName(page.NewTaipeiCityData.CityName);
            clTaoyuan.SetCityName(page.TaoyuanData.CityName);
            clHsinchu.SetCityName(page.HsinchuData.CityName);
            clTaichung.SetCityName(page.TaichungData.CityName);
            clTainan.SetCityName(page.TainanData.CityName);
            clKaohsiung.SetCityName(page.KaohsiungData.CityName);
            clTravel.SetCityName(page.TravelData.CityName);
            clPeauty.SetCityName(page.PeautyData.CityName);
            clCom.SetCityName(page.ComData.CityName);
            clDepositCoffee.SetCityName(page.DepositCoffeeData.CityName);
            clFamily.SetCityName(page.FamilyData.CityName);
            clTmall.SetCityName(page.SkmData.CityName);
            clTmall.SetCityName(page.Tmall.CityName);
            clPiinlife.SetCityName(page.Piinlife.CityName);
            clSkm.SetCityName(page.SkmData.CityName);
        }

        protected void ShowSlotDate(PponDealCalendarPage page)
        {
            clTitle.IsTitle = true;
            for (int i = 0; i < page.SlotDateLeagth; i++)
            {
                clTitle.SetTitle(page.PponColumns[i].TitleName, i);
            }
        }

        protected void ShowSlotDetail(PponDealCalendarPage page)
        {
            clTitle.Visible = true;
            clAllCountry.Visible = true;
            clTaipei.Visible = true;
            clNewTaipeiCity.Visible = true;
            clTaoyuan.Visible = true;
            clHsinchu.Visible = true;
            clTaichung.Visible = true;
            clTainan.Visible = true;
            clKaohsiung.Visible = true;
            clTravel.Visible = true;
            clPeauty.Visible = true;
            clCom.Visible = true;
            clDepositCoffee.Visible = true;
            clFamily.Visible = true;
            clTmall.Visible = true;
            clTmall.Visible = true;
            clPeauty.Visible = true;
            clSkm.Visible = true;
            for (int i = 0; i < page.SlotDateLeagth; i++)
            {
                ShowDealTimeSlotData(page.AllCountryData.TimeSlots[i], PponCityGroup.DefaultPponCityGroup.AllCountry, i, "AllCountry" + i.ToString(), page.NewDealData[i]);
                ShowDealTimeSlotData(page.TaipeiData.TimeSlots[i], PponCityGroup.DefaultPponCityGroup.TaipeiCity, i, "Taipei" + i.ToString(), page.NewDealData[i]);
                ShowDealTimeSlotData(page.NewTaipeiCityData.TimeSlots[i], PponCityGroup.DefaultPponCityGroup.NewTaipeiCity, i, "NewTaipeiCityData" + i.ToString(), page.NewDealData[i]);
                ShowDealTimeSlotData(page.TaoyuanData.TimeSlots[i], PponCityGroup.DefaultPponCityGroup.Taoyuan, i, "Taoyuan" + i.ToString(), page.NewDealData[i]);
                ShowDealTimeSlotData(page.HsinchuData.TimeSlots[i], PponCityGroup.DefaultPponCityGroup.Hsinchu, i, "Hsinchu" + i.ToString(), page.NewDealData[i]);
                ShowDealTimeSlotData(page.TaichungData.TimeSlots[i], PponCityGroup.DefaultPponCityGroup.Taichung, i, "Taichung" + i.ToString(), page.NewDealData[i]);
                ShowDealTimeSlotData(page.TainanData.TimeSlots[i], PponCityGroup.DefaultPponCityGroup.Tainan, i, "Tainan" + i.ToString(), page.NewDealData[i]);
                ShowDealTimeSlotData(page.KaohsiungData.TimeSlots[i], PponCityGroup.DefaultPponCityGroup.Kaohsiung, i, "Kaohsiung" + i.ToString(), page.NewDealData[i]);
                ShowDealTimeSlotData(page.TravelData.TimeSlots[i], PponCityGroup.DefaultPponCityGroup.Travel, i, "Travel" + i.ToString(), page.NewDealData[i]);
                ShowDealTimeSlotData(page.PeautyData.TimeSlots[i], PponCityGroup.DefaultPponCityGroup.PBeautyLocation, i, "Peauty" + i.ToString(), page.NewDealData[i]);
                ShowDealTimeSlotData(page.ComData.TimeSlots[i], PponCityGroup.DefaultPponCityGroup.PEZ, i, "Com" + i.ToString(), page.NewDealData[i]);
                ShowDealTimeSlotData(page.DepositCoffeeData.TimeSlots[i], PponCityGroup.DefaultPponCityGroup.DepositCoffee, i, "DepositCoffee" + i.ToString(), page.NewDealData[i]);
                ShowDealTimeSlotData(page.FamilyData.TimeSlots[i], PponCityGroup.DefaultPponCityGroup.Family, i, "Family" + i.ToString(), page.NewDealData[i]);
                ShowDealTimeSlotData(page.Tmall.TimeSlots[i], PponCityGroup.DefaultPponCityGroup.Tmall, i, "Tmall" + i.ToString(), page.NewDealData[i]);
                ShowDealTimeSlotData(page.Piinlife.TimeSlots[i], PponCityGroup.DefaultPponCityGroup.Piinlife, i, "Piinlife" + i.ToString(), page.NewDealData[i]);
                ShowDealTimeSlotData(page.SkmData.TimeSlots[i], PponCityGroup.DefaultPponCityGroup.Skm, i, "Skm" + i.ToString(), page.NewDealData[i]);
            }

            clAllCountry.Visible = page.AllCountryData.GetDealTimeSlotTotalDataCount() > 0;
            clTaipei.Visible = page.TaipeiData.GetDealTimeSlotTotalDataCount() > 0;
            clNewTaipeiCity.Visible = page.NewTaipeiCityData.GetDealTimeSlotTotalDataCount() > 0;
            clTaoyuan.Visible = page.TaoyuanData.GetDealTimeSlotTotalDataCount() > 0;
            clHsinchu.Visible = page.HsinchuData.GetDealTimeSlotTotalDataCount() > 0;
            clTaichung.Visible = page.TaichungData.GetDealTimeSlotTotalDataCount() > 0;
            clTainan.Visible = page.TainanData.GetDealTimeSlotTotalDataCount() > 0;
            clKaohsiung.Visible = page.KaohsiungData.GetDealTimeSlotTotalDataCount() > 0;
            clTravel.Visible = page.TravelData.GetDealTimeSlotTotalDataCount() > 0;
            clPeauty.Visible = page.PeautyData.GetDealTimeSlotTotalDataCount() > 0;
            clCom.Visible = page.ComData.GetDealTimeSlotTotalDataCount() > 0;
            clDepositCoffee.Visible = page.DepositCoffeeData.GetDealTimeSlotTotalDataCount() > 0;
            clFamily.Visible = page.FamilyData.GetDealTimeSlotTotalDataCount() > 0;
            clTmall.Visible = page.Tmall.GetDealTimeSlotTotalDataCount() > 0;
            clPiinlife.Visible = page.Piinlife.GetDealTimeSlotTotalDataCount() > 0;
            clSkm.Visible = page.SkmData.GetDealTimeSlotTotalDataCount() > 0;
        }

        protected void ShowDealTimeSlotData(ViewPponDealTimeSlotCollection dataCol, PponCity city, int colNum, string divId, IList<Guid> newDeals)
        {
            if (city.CityId == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)
            {
                clAllCountry.SetDayData(dataCol, colNum, divId, newDeals);
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId)
            {
                clTaipei.SetDayData(dataCol, colNum, divId, newDeals);
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.NewTaipeiCity.CityId)
            {
                clNewTaipeiCity.SetDayData(dataCol, colNum, divId, newDeals);
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Taoyuan.CityId)
            {
                clTaoyuan.SetDayData(dataCol, colNum, divId, newDeals);
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Hsinchu.CityId)
            {
                clHsinchu.SetDayData(dataCol, colNum, divId, newDeals);
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Taichung.CityId)
            {
                clTaichung.SetDayData(dataCol, colNum, divId, newDeals);
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Tainan.CityId)
            {
                clTainan.SetDayData(dataCol, colNum, divId, newDeals);
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Kaohsiung.CityId)
            {
                clKaohsiung.SetDayData(dataCol, colNum, divId, newDeals);
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
            {
                clTravel.SetDayData(dataCol, colNum, divId, newDeals);
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId)
            {
                clPeauty.SetDayData(dataCol, colNum, divId, newDeals);
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.PEZ.CityId)
            {
                clCom.SetDayData(dataCol, colNum, divId, newDeals);
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.DepositCoffee.CityId)
            {
                clDepositCoffee.SetDayData(dataCol, colNum, divId, newDeals);
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Family.CityId)
            {
                clFamily.SetDayData(dataCol, colNum, divId, newDeals);
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Tmall.CityId)
            {
                clTmall.SetDayData(dataCol, colNum, divId, newDeals);
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Piinlife.CityId)
            {
                clPiinlife.SetDayData(dataCol, colNum, divId, newDeals);
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Skm.CityId)
            {
                clSkm.SetDayData(dataCol, colNum, divId, newDeals);
            }
        }

        protected void SelectCityChanged(object sender, EventArgs e)
        {
            int selectRegion;
            if (int.TryParse(ddlCity.SelectedValue, out selectRegion))
            {
                if (SelectCityIdChange != null)
                {
                    PponDealTimeSlotViewSearchRequest request = new PponDealTimeSlotViewSearchRequest();
                    if (ddlCity.SelectedValue == "-1")
                    {
                        return;
                    }

                    request.CityId = int.Parse(ddlCity.SelectedValue);

                    SelectCityIdChange(sender, new DataEventArgs<PponDealTimeSlotViewSearchRequest>(request));
                }
            }
        }

        #region WebMethod

        /// <summary>
        /// 儲存權重並排序
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="cityName"></param>
        /// <param name="elements"></param>
        /// <returns></returns>
        [WebMethod]
        public static string SaveAndSortWeights(string cityId, string cityName, string elements)
        {
            try
            {
                var city = Int32.Parse(cityId);
                var elementList = elements.Split(',');

                var weights = new List<DealTimeSlotSortElement>();
                for (var i = 1; i <= elementList.Count(); i++)
                {
                    weights.Add(new DealTimeSlotSortElement()
                    {
                        CityId = city,
                        ElementId = i,
                        ElementWeight = Decimal.Parse(elementList[i - 1])
                    });
                }

                var dtsseCol = PponFacade.DealTimeSlotSortElementGetByCity(city);
                var updateWeightCnt = 0;
                foreach (var w in weights)
                {
                    if (w.ElementWeight != dtsseCol.Where(x => x.ElementId == w.ElementId).Select(x => x.ElementWeight).SingleOrDefault())
                    {
                        PponFacade.DealTimeSlotSortElementUpdate(w);
                        updateWeightCnt++;
                    }
                }

                var updateSeqCnt = 0;
                if (updateWeightCnt > 0)
                {
                    //log.Info("DealTimeSlot 「" + cityName + "」排序開始 " + (DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.ffff")));
                    updateSeqCnt = PponFacade.SortDealTimeSlotByOperatingInfo(city, "儲存權重並排序");
                    //log.Info("DealTimeSlot 「" + cityName + "」排序結束 " + (DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.ffff")) + "，共計排序 " + updateSeqCnt.ToString() + " 筆");

                    if (config.IsLogOperationOfDealTimeSlot)
                    {
                        var content = new JsonSerializer().Serialize(
                            new { Operation = "儲存並排序", CityId = cityId, Elements = elements });
                        VourcherFacade.ChangeLogAdd("DealTimeSlot", Guid.Empty, content, false);
                    }
                    return string.Format("「{0}」排序完畢! \n 共計更新 {1} 筆權重，更新 {2} 筆排序。", cityName, updateWeightCnt, updateSeqCnt);
                }
                return string.Format("「{0}」未排序! \n 更新 {1} 筆權重，更新 {2} 筆排序。", cityName, updateWeightCnt, updateSeqCnt);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("DealTimeSlot 更新權重失敗!! {0}", ex);
                if (ex.ToString().IndexOf("級距資料設定", StringComparison.Ordinal) > 0)
                {
                    return "級距資料設定錯誤!";
                }
                return "系統錯誤!";
            }
        }

        /// <summary>
        /// 更新排序
        /// </summary>
        /// <param name="data"></param>
        /// <param name="beforeSeqNo"></param>
        /// <param name="afterSeqNo"></param>
        /// <param name="operation"></param>
        /// <returns></returns>
        [WebMethod]
        public static string SetSlotData(string data, string beforeSeqNo, string afterSeqNo, string operation)
        {
            Guid guid = Guid.Empty;
            int cityId = 0;
            DateTime effStart = DateTime.MinValue;
            try
            {
                int newSeq = 1;
                string[] dealList = data.Split(',');
                var topOneData = dealList[0].Split('|');
                var slotCityId = int.Parse(topOneData[1]);
                var slotEffStart = DateTime.ParseExact(topOneData[2].Trim(), "yyyy/MM/dd HH:mm:ss", null);

                using (var tx = TransactionScopeBuilder.CreateReadCommitted())
                {
                    foreach (string dealSlotId in dealList)
                    {
                        if (!string.IsNullOrEmpty(dealSlotId))
                        {
                            string[] slotIdColData = dealSlotId.Split('|');
                            guid = new Guid(slotIdColData[0].Trim());
                            cityId = slotCityId;
                            effStart = slotEffStart;
                            OrderFacade.SetSeqToDealTimeSlotData(guid, cityId, effStart, newSeq);
                            newSeq++;
                        }
                    }

                    tx.Complete();
                }

                if (config.IsLogOperationOfDealTimeSlot)
                {
                    var content = new JsonSerializer().Serialize(
                        new
                        {
                            Operation = operation,
                            CityId = cityId,
                            EffectiveStart = effStart.ToString("yyyyMMdd HH:mm:ss"),
                            OperatingResult = beforeSeqNo + ">" + afterSeqNo
                        });
                    VourcherFacade.ChangeLogAdd("DealTimeSlot", Guid.Empty, content, false);
                }
                return "";

            }
            catch (Exception ex)
            {
                log.ErrorFormat("DealTimeSlot 更新排序失敗! guid={0}, cityId={1}, effStart={2}, ex={3}", guid, cityId, effStart, ex);
                return string.Format("系統錯誤!!!請洽IT處理");
            }
        }

        [WebMethod]
        public static ApiResult SetSlotDataV2(string [] dataList, string beforeSeqNo, string afterSeqNo, string operation)
        {
            Guid guid = Guid.Empty;
            int cityId = 0;
            DateTime effStart = DateTime.MinValue;
            try
            {
                int newSeq = 1;
                SortDealTimeSlotCollection sortDealCol = new SortDealTimeSlotCollection();

                using (var tx = TransactionScopeBuilder.CreateReadCommitted())
                {
                    foreach (var data in dataList)
                    {
                        string[] rowData = data.Split('|');
                        sortDealCol.Add(new SortDealTimeSlot
                        {
                            Bid = new Guid(rowData[0].Trim()),
                            Cityid = int.Parse(rowData[1]),
                            Effstart = DateTime.ParseExact(rowData[2].Trim(), "yyyy/MM/dd HH:mm:ss", null),
                            Seq = newSeq
                        });
                        newSeq++;
                    }

                    if (config.DealTimeSlotEnableSortLockHide &&
                        !HasSortLockHideAuthor(HttpContext.Current.User.Identity.Name, sortDealCol.First().Cityid))
                    {
                        return new ApiResult
                        {
                            Code = ApiResultCode.Success,
                            Message = "沒有權限"
                        };
                    }

                    //大量更新 DealTimeSlot
                    OrderFacade.SaveSeqToDealTimeSlotData(sortDealCol);
                    tx.Complete();
                }

                if (config.IsLogOperationOfDealTimeSlot)
                {
                    var content = new JsonSerializer().Serialize(
                        new
                        {
                            Operation = operation,
                            CityId = cityId,
                            EffectiveStart = effStart.ToString("yyyyMMdd HH:mm"),
                            OperatingResult = beforeSeqNo + ">" + afterSeqNo
                        });
                    VourcherFacade.ChangeLogAdd("DealTimeSlot", Guid.Empty, content, false);
                }
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "更新完畢"
                };

            }
            catch (Exception ex)
            {
                log.ErrorFormat("DealTimeSlot 更新排序失敗! guid={0}, cityId={1}, effStart={2}, ex={3}", guid, cityId, effStart, ex);
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "更新排序失敗"
                };
            }
        }

        /// <summary>
        /// 隱藏檔次
        /// </summary>
        /// <param name="dataId"></param>
        /// <param name="isShow"></param>
        /// <returns></returns>
        [WebMethod]
        public static object SetDealTimeSlotStatus(string dataId, string isShow)
        {
            try
            {
                if (!string.IsNullOrEmpty(dataId))
                {
                    string[] slotIdColData = dataId.Split('|');
                    Guid guid = new Guid(slotIdColData[0]);
                    int cityId = int.Parse(slotIdColData[1]);

                    if (config.DealTimeSlotEnableSortLockHide &&
                        !HasSortLockHideAuthor(HttpContext.Current.User.Identity.Name, cityId))
                    {
                        return new { result = false, message = "沒有權限" };
                    }

                    DateTime effStart = DateTime.ParseExact(slotIdColData[2], "yyyy/MM/dd HH:mm:ss", null);
                    DealTimeSlotStatus status = DealTimeSlotStatus.Default;
                    string operation;
                    if (isShow != "true")
                    {
                        status = DealTimeSlotStatus.NotShowInPponDefault;
                        operation = "Hide";
                    }
                    else
                    {
                        IViewPponDeal deal = PponFacade.ViewPponDealGetByGuid(guid);
                        if ((deal.BusinessHourStatus & (int)BusinessHourStatus.TmallDeal) > 0)
                        {
                            status = DealTimeSlotStatus.NotShowInPponDefault;
                        }
                        operation = "Show";
                    }
                    OrderFacade.SetStatusToDealTimeSlotData(guid, cityId, effStart, status, true);

                    if (config.IsLogOperationOfDealTimeSlot)
                    {
                        var content = new JsonSerializer().Serialize(
                            new
                            {
                                Operation = operation,
                                CityId = cityId,
                                EffectiveStart = effStart.ToString("yyyyMMdd HH:mm:ss")
                            });
                        VourcherFacade.ChangeLogAdd("DealTimeSlot", guid, content, false);
                    }
                    List<string> effectDataIds = new List<string>();
                    for (int i = 0; i < 7; i++)
                    {
                        string effectDataId = string.Format("{0}|{1}|{2}", guid, cityId, effStart.AddDays(i).ToString("yyyy/MM/dd HH:mm:ss"));
                        effectDataIds.Add(effectDataId);
                    }
                    return new {result = true, data = effectDataIds};
                }
                log.Error("DealTimeSlot 隱藏檔次失敗! 沒有傳入dataId.");
                return new { result = false, message = "沒有傳入dataId" };
            }
            catch (Exception ex)
            {
                log.Error("DealTimeSlot 隱藏檔次失敗! " + ex);
                return new {result = false, message = ex.Message};
            }
        }

        /// <summary>
        /// 以下類別需驗証是否有排序、鎖定排序、隱藏權限
        /// </summary>
        /// <param name="identityName">使用者名稱</param>
        /// <param name="cityId">類別</param>
        /// <returns></returns>
        private static bool HasSortLockHideAuthor(string identityName, int cityId)
        {
            const string privilegeUri = "/controlroom/ppon/PponDealTimeSlot.aspx";
            var category = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityId);
            //台北
            if (category == PponCityGroup.DefaultPponCityGroup.TaipeiCity)
                return CommonFacade.IsInSystemFunctionPrivilege(identityName,
                    SystemFunctionType.SortLockHideTaipei, privilegeUri);
            //桃園
            if (category == PponCityGroup.DefaultPponCityGroup.Taoyuan)
                return CommonFacade.IsInSystemFunctionPrivilege(identityName,
                    SystemFunctionType.SortLockHideTaoyuan, privilegeUri);
            //新竹
            if (category == PponCityGroup.DefaultPponCityGroup.Hsinchu)
                return CommonFacade.IsInSystemFunctionPrivilege(identityName,
                    SystemFunctionType.SortLockHideHsinchu, privilegeUri);
            //台中
            if (category == PponCityGroup.DefaultPponCityGroup.Taichung)
                return CommonFacade.IsInSystemFunctionPrivilege(identityName,
                    SystemFunctionType.SortLockHideTaichung, privilegeUri);
            //台南
            if (category == PponCityGroup.DefaultPponCityGroup.Tainan)
                return CommonFacade.IsInSystemFunctionPrivilege(identityName,
                    SystemFunctionType.SortLockHideTainan, privilegeUri);
            //高雄
            if (category == PponCityGroup.DefaultPponCityGroup.Kaohsiung)
                return CommonFacade.IsInSystemFunctionPrivilege(identityName,
                    SystemFunctionType.SortLockHideKaohsiung, privilegeUri);
            //旅行
            if (category == PponCityGroup.DefaultPponCityGroup.Travel)
                return CommonFacade.IsInSystemFunctionPrivilege(identityName,
                    SystemFunctionType.SortLockHideTravel, privilegeUri);
            //玩美、休閒
            if (category == PponCityGroup.DefaultPponCityGroup.PBeautyLocation)
                return CommonFacade.IsInSystemFunctionPrivilege(identityName,
                    SystemFunctionType.SortLockHidePeauty, privilegeUri);
            //宅配
            if (category == PponCityGroup.DefaultPponCityGroup.AllCountry)
                return CommonFacade.IsInSystemFunctionPrivilege(identityName,
                    SystemFunctionType.SortLockHideAllCountry, privilegeUri);
            //全家
            if (category == PponCityGroup.DefaultPponCityGroup.Family)
                return CommonFacade.IsInSystemFunctionPrivilege(identityName,
                    SystemFunctionType.SortLockHideFamily, privilegeUri);
            //品生活
            if (category == PponCityGroup.DefaultPponCityGroup.Piinlife)
                return CommonFacade.IsInSystemFunctionPrivilege(identityName,
                    SystemFunctionType.SortLockHidePiinlife, privilegeUri);

            return true;
        }

        /// <summary>
        /// 鎖定排序
        /// </summary>
        /// <param name="dataId"></param>
        /// <param name="isLockSeq"></param>
        /// <returns></returns>
        [WebMethod]
        public static string SetDealTimeSlotLockSeq(string dataId, string isLockSeq)
        {
            try
            {
                if (!string.IsNullOrEmpty(dataId))
                {
                    var slotIdColData = dataId.Split('|');
                    var guid = new Guid(slotIdColData[0]);
                    var cityId = int.Parse(slotIdColData[1]);

                    if (config.DealTimeSlotEnableSortLockHide &&
                        !HasSortLockHideAuthor(HttpContext.Current.User.Identity.Name, cityId))
                    {
                        return "";
                    }

                    var effStart = DateTime.ParseExact(slotIdColData[2], "yyyy/MM/dd HH:mm:ss", null);
                    var operation = (isLockSeq == "true") ? "Lock" : "Unlock";
                    var content = new JsonSerializer().Serialize(
                        new { Operation = operation, CityId = cityId, EffectiveStart = effStart.ToString("yyyyMMdd HH:mm:ss") });

                    PponFacade.SetLockSeqToDealTimeSlotData(guid, cityId, effStart, isLockSeq == "true");
                    if (config.IsLogOperationOfDealTimeSlot)
                    {
                        VourcherFacade.ChangeLogAdd("DealTimeSlot", guid, content, false);
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                log.Error("DealTimeSlot 鎖定排序失敗! " + "dataId:" + dataId + "isLockSeq:" + isLockSeq + "\n\r" + ex);
                return ex.Message;
            }
        }


        /// <summary>
        /// 儲存熱銷檔次設定
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="topN"></param>
        /// <param name="days"></param>
        /// <param name="multiple"></param>
        /// <param name="enable"></param>
        /// <param name="isSort"></param>
        /// <returns></returns>
        [WebMethod]
        public static string SaveHotDealSetting(int cityId, int topN, int days, int multiple, bool enable, bool isSort)
        {
            try
            {
                var msg = PponFacade.SaveHotDealSetting(cityId, topN, days, multiple, enable, isSort);
                var content = new JsonSerializer().Serialize(
                            new
                            {
                                Operation = isSort ? "儲存熱銷檔次設定並排序" : "儲存熱銷檔次設定",
                                CityId = cityId,
                                Multiple = multiple
                            });
                VourcherFacade.ChangeLogAdd("HotDealConfig", Guid.Empty, content, enable);
                return msg;
            }
            catch (Exception ex)
            {
                log.Error("DealTimeSlot 儲存熱銷檔次設定失敗! " + ex);
                return "系統錯誤!!!請洽IT處理";
            }
        }


        [WebMethod]
        public static string ResetCache()
        {
            try
            {
                SortedDictionary<string, string> serverIPs =
                    ProviderFactory.Instance().GetSerializer().Deserialize<SortedDictionary<string, string>>(config.ServerIPs);
                string result = SystemFacade.SyncCommand(serverIPs, SystemFacade._RESET_FOR_DEAL_TIME_SLOTS_CHANGE, string.Empty, string.Empty);
                return result.Replace("\r\n", "<br />");
            }
            catch (Exception ex)
            {                
                return "系統錯誤!!!請洽IT處理";
            }
        }

        #endregion


    }
}
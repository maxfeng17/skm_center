﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="VacationSetup.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.VacationSetup" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <fieldset>
        <legend>國定假日查詢</legend>期間：
        <asp:TextBox ID="txtDateTimeS" runat="server" Columns="8" />
        <cc1:CalendarExtender ID="ceDS" TargetControlID="txtDateTimeS" runat="server" DaysModeTitleFormat="yyyy/MM/dd"
            Format="yyyy/MM/dd">
        </cc1:CalendarExtender>
        ～
        <asp:TextBox ID="txtDateTimeE" runat="server" Columns="8" />
        <cc1:CalendarExtender ID="ceDE" TargetControlID="txtDateTimeE" runat="server" DaysModeTitleFormat="yyyy/MM/dd"
            Format="yyyy/MM/dd">
        </cc1:CalendarExtender>
        <asp:Button ID="btnSearch" runat="server" Text="搜尋" OnClick="btnSearch_Click" />
    </fieldset>
    <fieldset>
        <legend>新增國定假日</legend>
        日期：<asp:TextBox ID="txtHoliday" runat="server" Columns="8" />
        <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtHoliday" runat="server"
            DaysModeTitleFormat="yyyy/MM/dd" Format="yyyy/MM/dd">
        </cc1:CalendarExtender>
        名稱：<asp:TextBox ID="txtName" runat="server"></asp:TextBox>
        <asp:Button ID="btnVacationAdd" runat="server" Text="新增" OnClick="btnVacationAdd_Click" />
    </fieldset>
    <asp:GridView ID="gvVacationList" runat="server" OnRowCommand="gvVacationList_RowCommand" AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField HeaderText="Id" DataField="Id" />
            <asp:BoundField HeaderText="日期" DataField="Holiday" />
            <asp:BoundField HeaderText="名稱" DataField="Name" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="btnDelete" runat="server" Text="刪除" CommandArgument="<%# ((Vacation)Container.DataItem).Id%>"
                        CommandName="DEL" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>

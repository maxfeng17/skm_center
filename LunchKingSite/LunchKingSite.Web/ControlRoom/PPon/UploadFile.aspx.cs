﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.UI;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class UploadFile : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<dynamic> fileItems = new List<dynamic>();

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFile file = Request.Files[i];

                string fileExt = Helper.GetExtensionByContentType(file.ContentType);
                string fileId = string.Format("{0}.{1}", Guid.NewGuid(), fileExt);
                string tempFileName = GetTempImagePath(fileId);
                file.SaveAs(tempFileName);

                fileItems.Add(new {FileId = fileId, Name = file.FileName, Size = file.ContentLength});
            }
            Response.ContentType = "application/json";
            string jsonStr = ProviderFactory.Instance().GetSerializer().Serialize(fileItems);
            Response.Write(jsonStr);
            Response.End();
        }

        protected string GetTempImagePath(string fileId)
        {
            
            string imageTempFolder = Path.Combine(
                ProviderFactory.Instance().GetConfig().WebTempPath, WebStorage._IMAGE_TEMP);
            if (Directory.Exists(imageTempFolder) == false)
            {
                Directory.CreateDirectory(imageTempFolder);
            }
            return Path.Combine(imageTempFolder, fileId);
        }
    }
}
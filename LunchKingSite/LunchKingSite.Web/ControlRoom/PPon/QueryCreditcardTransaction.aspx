﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QueryCreditcardTransaction.aspx.cs"
    Inherits="LunchKingSite.Web.ControlRoom.Ppon.QueryCreditcardTransaction" MasterPageFile="~/ControlRoom/backend.master" %>
<%@ Import Namespace="LunchKingSite.Core" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="../Controls/Pager.ascx" tagname="Pager" tagprefix="uc1" %>
<script runat="server">
    protected string GetOrderDetailUrl(int classification, string orderGuid)
    {
        if (classification == (int) OrderClassification.LkSite)
        {
            return "../Order/order_detail.aspx?oid=" + orderGuid;   
        }
        else if (classification == (int) OrderClassification.HiDeal)
        {
            return "../piinlife/hidealorderdetaillist.aspx?oid=" + orderGuid;
            
        }
        return "";
    }
</script>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <table style="width: 100%;">
        <tr valign="top">
            <td align="left" nowrap="nowrap"  style="width: 10%;">
                <table style="width: 381px; border: double 3px black" >
                <tr >
                <td colspan = "4" style="font-size: 16px; font-weight: bold" align="center">
                    <asp:Label ID="Label5" runat="server" Text="交易列表"></asp:Label>
                </td>
                </tr>
                    <tr>
                        <td class="style1" nowrap="nowrap">
                            <asp:Label ID="Label1" runat="server" Text="訂單編號："></asp:Label>
                        </td>
                        <td align="left" class="style2" valign="middle" colspan="3">
                            <asp:TextBox ID="tOI" runat="server" Width="270px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1" nowrap="nowrap">
                            <asp:Label ID="Label2" runat="server" Text="交易編號："></asp:Label>
                        </td>
                        <td class="style2" colspan="3">
                            <asp:TextBox ID="tTI" runat="server" Width="270px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1" nowrap="nowrap">
                            <asp:Label ID="Label6" runat="server" Text="建立者："></asp:Label>
                        </td>
                        <td class="style2" colspan="3">
                            <asp:TextBox ID="tCI" runat="server" Width="270px"></asp:TextBox>
                        </td>
                    </tr>
<%--                    <tr>
                        <td class="style1" nowrap="nowrap">
                            <asp:Label ID="Label3" runat="server" Text="交易狀態："></asp:Label>
                        </td>
                        <td class="style2">
                            <asp:DropDownList ID="ddlSt" runat="server" />
                        </td>
                        <td class="style3">
                            &nbsp;
                        </td>
                        <td class="style4">
                            &nbsp;
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="style1" nowrap="nowrap">
                            <asp:Label ID="Label4" runat="server" Text="交易時間："></asp:Label>
                        </td>
                        <td class="style2">
                            <asp:TextBox ID="tTB" runat="server"> </asp:TextBox>
                            <cc1:CalendarExtender ID="ceDS" TargetControlID="tTB" runat="server" Format="yyyy/MM/dd">
                            </cc1:CalendarExtender>
                        </td>
                        <td class="style3">
                            ～
                        </td>
                        <td class="style4">
                            <asp:TextBox ID="tTE" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="ceDE" TargetControlID="tTE" runat="server" Format="yyyy/MM/dd">
                            </cc1:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1" nowrap="nowrap">
                            <asp:Label ID="Label7" runat="server" Text="訊息："></asp:Label>
                        </td>
                        <td class="style2" colspan="3">
                            <asp:TextBox ID="tMS" runat="server" Width="270px"></asp:TextBox>
                        </td>
                    </tr>

                     <tr>
                        <td class="style1" nowrap="nowrap">
                            <asp:Label ID="Label8" runat="server" Text="確認碼："></asp:Label>
                        </td>
                        <td class="style2" colspan="3">
                            <asp:TextBox ID="tbAuthcode" runat="server" Width="270px"></asp:TextBox>
                        </td>
                    </tr>
                    
                     <tr>
                        <td class="style1" nowrap="nowrap">
                            <asp:Label ID="Label3" runat="server" Text="訂單分類："></asp:Label>
                        </td>
                        <td class="style2" colspan="3">                            
                            <asp:RadioButtonList ID="rbClassification" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="全部" Value="" Selected="true"/>
                                <asp:ListItem Text="P好康" Value="1" />
                                <asp:ListItem Text="品生活" Value="2"/>
                            </asp:RadioButtonList>
                        </td>
                    </tr>

                    <tr>
                        <td align="left" rowspan="1" valign="middle" nowrap="nowrap">
                            <asp:Button ID="Button1" runat="server" Text="查詢" OnClick="Button1_Click" />
                        </td>
                        <td align="left" rowspan="1" colspan="3" valign="middle">
                            <asp:Label ID="lErr" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <table style="border: double 3px black">
                    <tr>
                        <th style="width: 174px">
                            列表狀態</th>
                    </tr>
                    <tr>
                        <td style="width: 174px">
                            自動更新 (秒):
                            <asp:DropDownList ID="ddlTmr" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTmr_SIC">
                                <asp:ListItem Text="無" Value="0" Selected="True" />
                                <asp:ListItem Text="10" Value="10" />
                                <asp:ListItem Text="30" Value="30" />
                                <asp:ListItem Text="60" Value="60" />
                            </asp:DropDownList>
                            <aspajax:Timer ID="tmr1" runat="server" Enabled="false" OnTick="tmr1_Tick" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 174px">
                            每頁顯示:
                            <asp:DropDownList ID="ddlPS" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPS_SIC">
                                <asp:ListItem Text="15" Value="15" Selected="true" />
                                <asp:ListItem Text="30" Value="30" />
                                <asp:ListItem Text="60" Value="60" />
                                <asp:ListItem Text="100" Value="100" />
                                <asp:ListItem Text="200" Value="200" />
                            </asp:DropDownList>
                            筆資料
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan = "2" align="left">
                <asp:GridView ID="gridTrans" runat="server" AutoGenerateColumns="False"
                    CellPadding="4" ForeColor="#333333" GridLines="None">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField HeaderText="ID" DataField="PaymentTransactionId" />
                        <asp:BoundField HeaderText="交易編號" DataField="TransId" />

                        <asp:TemplateField HeaderText="訂單編號">
                            <ItemTemplate>
                                <a href='<%#GetOrderDetailUrl(int.Parse(Eval("OrderClassification").ToString()), Convert.ToString(Eval("OrderGuid")))%>'>
                                    <%# Eval("OrderID") %>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
      
                        <asp:BoundField HeaderText="交易金額" DataField="Amount" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,###,##0}" >
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField HeaderText="確認碼" DataField="AuthCode" />            
                        <asp:BoundField HeaderText="交易時間" DataField="TransTime" DataFormatString="{0:yyyy/MM/dd HH:mm}" />
                        <asp:HyperLinkField HeaderText="建立者" DataTextField="CreateId" 
                            DataNavigateUrlFormatString="../User/users_edit.aspx?username={0}" DataNavigateUrlFields="CreateId" />
                        <asp:TemplateField HeaderText="付費類別">
                            <ItemTemplate>
                                <asp:Label ID="lPayType" runat="server" Text='<%# GetPayType(DataBinder.Eval(Container, "DataItem.PaymentType").ToString()) %>'  />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="交易類別">
                            <ItemTemplate>
                                <asp:Label ID="lTransType" runat="server" Text='<%# GetTransType(DataBinder.Eval(Container, "DataItem.TransType").ToString()) %>'  />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="交易狀態">
                            <ItemTemplate>
                                <asp:Label ID="lPayStatus" runat="server" Text='<%# GetPayStatus(DataBinder.Eval(Container, "DataItem.PaymentStatus").ToString()) %>'  />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="交易結果">
                            <ItemTemplate>
                                <asp:Label ID="lPayResult" runat="server" Text='<%# GetPayResult(DataBinder.Eval(Container, "DataItem.Result").ToString()) %>'  />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="金流廠商">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# GetApiProvider(Eval("ApiProvider") == null ? string.Empty : Eval("ApiProvider").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="交易訊息" DataField="Message" />
                    </Columns>
                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                </asp:GridView>
            <uc1:Pager ID="gridPager" runat="server" PageSize="15" OnGetCount="RetrieveTransCount"
                OnUpdate="UpdateHandler"></uc1:Pager>
            </td>
        </tr>
        <tr height="100px"><td></td></tr>
        <tr>
            <td>
                取消PCash
            </td>
            <td>
                交易編號:<asp:TextBox ID="txtTransId" runat="server" TextMode="MultiLine" Height="30"  Width="250"></asp:TextBox>
                確認碼:<asp:TextBox ID="txtAuthCode" runat="server"></asp:TextBox>
                金額:<asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                <asp:Button ID="btnCancelPcash" runat="server" Text="確認取消" 
                    onclick="btnCancelPcash_Click" />
                <asp:Label ID="lblResult" runat="server" Text="" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                補請PCash
            </td>
            <td>
                交易編號:<asp:TextBox ID="txtTransIdC" runat="server"></asp:TextBox>
                金額:<asp:TextBox ID="txtAmountC" runat="server"></asp:TextBox>
                <asp:Button ID="btnChargePcash" runat="server" Text="確認請款" 
                    onclick="btnChargePcash_Click" />
                <asp:Label ID="lblResultC" runat="server" Text="" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                退還被Lock的PCash
            </td>
            <td>
                <asp:Button ID="btnSearchPcash" runat="server" Text="查詢" 
                    onclick="btnSearchPcash_Click" />
                <asp:Button ID="btnRefundPCash" runat="server" Text="全部退掉(請勿亂按" 
                    onclick="btnRefundPCash_Click" />
                <asp:GridView ID="GridView1" runat="server">
                </asp:GridView>
            </td>
        </tr>

    </table>
    <asp:TextBox ID="tbTransId" runat="server" Text="" TextMode="MultiLine" Height="100"  Width="200"></asp:TextBox>
     <asp:Button ID="btPcashBatch" runat="server" Text="批次補請pcash" onclick="btnChargePcashBatch_Click" />
     <asp:Label ID="lbBatchResult" runat="server" Text="" ForeColor="Red"></asp:Label>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.Web.ControlRoom.Controls;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Facade;
using PaymentType = LunchKingSite.Core.PaymentType;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.Web.ControlRoom.piinlife
{
    public partial class HidealOrderDetailList : RolePage, IHidealOrderDetailListView
    {
        #region property
        private HidealOrderDetailListPresenter _presenter;
        public HidealOrderDetailListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        /// <summary>
        /// 操作人員帳號
        /// </summary>
        public string UserName
        {
            get { return User.Identity.Name; }
        }

        public Guid OrderGuid
        {
            get
            {
                Guid oGuid = Guid.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["oid"]))
                {
                    try { oGuid = new Guid(Request.QueryString["oid"]); }
                    catch { oGuid = Guid.Empty; }
                }
                return oGuid;
            }
        }

        public int OrderPk
        {
            get
            {
                int orderPk;
                return int.TryParse(hidOrderPk.Value, out orderPk) ? Convert.ToInt32(hidOrderPk.Value) : orderPk;
            }
        }

        public int ProductId
        {
            get
            {
                int productId;
                return int.TryParse(hidProductId.Value, out productId) ? Convert.ToInt32(hidProductId.Value) : productId;
            }
        }

        public IAuditBoardView AuditBoardView
        {
            get { return abHidealOrderLog; }
        }

        public string RefundFormUrl
        {
            get
            {
                return ResolveClientUrl("~/service/hidealrefundform.ashx");
            }
        }

        public bool IsRefundCashOnly
        {
            get { return (bool)ViewState["isRefundCashOnly"]; }
            set { ViewState["isRefundCashOnly"] = value; }
        }

        private ShipCompanyCollection _shipCompanyCol;
        public ShipCompanyCollection ShipCompanyCol
        {
            get { return _shipCompanyCol; }
            set { _shipCompanyCol = value; }
        }

        public string alertMessage
        {
            set { AjaxControlToolkit.ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), string.Empty, "alert('" + value + "')", true); }
        }

        public string CurrentUser
        {
            get { return User.Identity.Name; }
        }

        public string ShipCompanyName { get; set; }

        public int GoodsReturnStatus
        {
            get { return (int)ViewState["GoodsReturnStatus"]; }
            set { ViewState["GoodsReturnStatus"] = value; }
        }

        public DateTime ShipStartDate { get; set; }

        public int ShipMemoLimit
        {
            get { return 30; }
        }

        public HiDealReturnedCollection HiDealReturnedCol
        {
            get { return (HiDealReturnedCollection)ViewState["HiDealReturnedCol"]; }
            set { ViewState["HiDealReturnedCol"] = value; }
        }

        static ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public ISysConfProvider SystemConfig
        {
            get { return cp; }
        }

        public bool IsLockRefund
        {
            set { hfIsHaveLockReservationRecord.Value = value.ToString(); }
            get { return bool.Parse(hfIsHaveLockReservationRecord.Value); }
        }

        public DropDownList ddlServiceCategory 
        {
            get { return ddlCategory; }
        }

        public DropDownList ddlServiceSubCategory
        {
            get { return ddlSubCategory; }
        }

        #endregion

        #region event
        public event EventHandler<DataEventArgs<int>> ReturnCash = null;
        public event EventHandler<DataEventArgs<int>> ReturnComplete = null;
        /// <summary>
        /// 強制退貨，退回現金(刷退貨ATM  ATM還未實作)
        /// </summary>
        public event EventHandler<DataEventArgs<Guid>> ReturnAllPassAndCashBack = null;

        /// <summary>
        /// 強制退貨，退回購物金
        /// </summary>
        public event EventHandler<DataEventArgs<Guid>> ReturnAllPassAndPointBack = null;

        /// <summary>
        /// 建立退貨單，走正常退貨流程。
        /// </summary>
        public event EventHandler<DataEventArgs<HidealOrderDetailListViewMakeReturnedForm>> MakeReturnedForm = null;

        /// <summary>
        /// 發票相關修正
        /// </summary>
        public event EventHandler<DataEventArgs<InvoiceUpdateData>> InvoiceUpdate = null;
        /// <summary>
        /// 取消退貨單作業。
        /// </summary>
        public event EventHandler<DataEventArgs<Guid>> ReturnedFormCancel = null;
        /// <summary>
        /// 客服紀錄新增作業。
        /// </summary>
        public event EventHandler<DataEventArgs<ServiceMessage>> OnSetServiceMsgClicked;

        public event EventHandler<DataEventArgs<OrderShipArgs>> AddOrderShip;
        public event EventHandler<DataEventArgs<OrderShipArgs>> DeleteOrderShip;
        public event EventHandler<DataEventArgs<OrderShipArgs>> UpdateOrderShip;

        public event EventHandler<DataEventArgs<RefundFormPageInfo>> SendRefundApplicationFormNotification = null;
        public event EventHandler<DataEventArgs<RefundFormPageInfo>> SendDiscountSingleFormNotification = null;
        public event EventHandler<DataEventArgs<EInvoiceMainInfo>> UpdateEInvoiceMainInfo = null;
        /// <summary>
        /// 退貨單資料更新
        /// </summary>
        public event EventHandler<DataEventArgs<RefundFormBackInfo>> UpdateReturned = null;
        #endregion

        #region method

        public void ShowPageData(HiDealOrderDetailCollection hodc, HiDealOrder ho, Member m,
            ViewHiDealCouponTrustStatusCollection couponList, HiDealReturnedCollection hdrc,
            EinvoiceMainCollection invCol, PaymentTransactionCollection paymentTransColl
            , HiDealProduct product, List<HiDealInvoiceGoodsInfo> hiDealInvoiceGoodsInfos,
            List<HiDealInvoiceCouponInfo> hiDealInvoiceCouponInfos)
        {
            SetHiDealOrderDetailList(hodc);
            SetHiDealOrder(ho);
            SetMember(m);
            SetReturnedList(hdrc);
            SetInvoiceGoodsList(hiDealInvoiceGoodsInfos);
            SetInvoiceCouponList(hiDealInvoiceCouponInfos);
            SetEInvoiceMainData(invCol);
            lbChargingSituation.Text = GetChargingSituation(paymentTransColl);

            //退貨通知書URL
            var returned = (hdrc.OrderByDescending(x => x.ApplicationTime).ToList());
            if (returned.Count > 0)
            {
                string pageId = HiDealReturnedManager.GetRefundFormPageId(returned.First().Id, returned.First().OrderPk, returned.First().CreateId);
                hl_ShowReturnApplicationForm.NavigateUrl = SystemConfig.SiteUrl + "/service/hidealrefundform.ashx?FormType=RAF&id=" + pageId;
            }
            else
            {
                hl_ShowReturnApplicationForm.NavigateUrl = "#";
            }

            // 設定商品狀態
            string productSituation = string.Empty;
            if (product.IsNoRefund)
                productSituation += "不可退貨／";
            if (product.IsDaysNoRefund)
                productSituation += "結檔後七天不可退貨／";
            if (product.IsExpireNoRefund)
                productSituation += "使用期限到期不可退貨／";
            lblProductSituation.Text = productSituation;

            //判斷是否可強制退貨
            string message;
            if (HiDealReturnedManager.CheckOrderDataForAllReturn(ho, true, out message))
            {
                btnReturnAllPass.Enabled = true;
                btnReturnAllPass.ForeColor = System.Drawing.Color.Red;
                btnMakeReturnForm.Enabled = true;
                btnMakeReturnForm.ForeColor = System.Drawing.Color.Red;
                cbBackSCash.Enabled = true;
            }
            else
            {
                btnReturnAllPass.Enabled = false;
                btnReturnAllPass.ForeColor = System.Drawing.Color.Gray;
                btnMakeReturnForm.Enabled = false;
                btnMakeReturnForm.ForeColor = System.Drawing.Color.Gray;
                cbBackSCash.Enabled = false;
            }

            #region 出貨紀錄

            divDeliverManage.Visible = product.IsHomeDelivery == true;
            if (product.VendorBillingModel == (int)VendorBillingModel.None)
                divDeliverManage.Visible = false;

            #endregion

            if (IsRefundCashOnly)
                cbBackSCash.Enabled = false;

            //檢查是否顯示取消退貨按鈕
            if (hdrc.Where(x => x.Status == (int)HiDealReturnedStatus.Create).Count() > 0)
            {
                btnReturnCancel.Enabled = true;
            }
            else
            {
                btnReturnCancel.Enabled = false;
            }
            //顯示退貨設定
            if (product.IsNoRefund)
            {
                labNoRefund.Text = I18N.Phrase.HiDealProductIsNoRefund;
            }
            else if (product.IsExpireNoRefund)
            {
                labNoRefund.Text = I18N.Phrase.HiDealProductIsExpireNoRefund;
            }
            else
            {
                labNoRefund.Text = string.Empty;
            }
            //顯示不開立發票註記
            CbNoInvoiceCreate.Checked = !product.IsInvoiceCreate;
            ShipStartDate = Convert.ToDateTime(product.UseStartTime);
        }

        public void ShowMessage(string message)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                lbMessage.Text = string.Empty;
                lbMessage.Visible = false;

            }
            else
            {
                lbMessage.Text = message;
                lbMessage.Visible = true;
            }

        }

        #region 出貨紀錄

        protected void addDeliverData_Click(object sender, EventArgs e)
        {
            if (this.GoodsReturnStatus != (int)OrderGoodsReturnStatus.AllReturned)
            {
                if (this.AddOrderShip != null)
                {
                    OrderShipArgs osa = new OrderShipArgs();
                    DateTime ShipTime;
                    if (DateTime.TryParse(tbAddShipTime.Text, out ShipTime))
                    {
                        osa.ShipTime = ShipTime;
                    }
                    else
                    {
                        this.alertMessage = "請輸入正確的出貨日期!";
                        return;
                    }
                    if (tbAddShipNo.Text.Trim() == string.Empty)
                    {
                        this.alertMessage = "請輸入運單編號!";
                        return;
                    }
                    osa.ShipNo = tbAddShipNo.Text;
                    osa.ShipCompanyId = Convert.ToInt32(ddlAddShipCompanyId.SelectedValue);
                    osa.ShipMemo = tbShipMemo.Text.Trim();
                    this.AddOrderShip(this, new DataEventArgs<OrderShipArgs>(osa));
                    this._presenter.OnViewInitialized();
                }
            }
            else
            {
                this.alertMessage = "已退貨的訂單無法新增出貨資訊!";
            }
        }

        public void SetDeliverData(ViewOrderShipListCollection osCol)
        {
            if (osCol.Any() && osCol[0].OrderShipModifyTime != null)
            {
                addDeliver.Visible = false; //有出貨資訊, 故隱藏新增功能
                gvDeliver.Visible = true;
                gvDeliver.DataSource = osCol;
                gvDeliver.DataBind();
            }
            else
            {
                addDeliver.Visible = true;
                gvDeliver.Visible = false;
                BindDdlShipCompany(ref ddlAddShipCompanyId, _shipCompanyCol);
            }
        }

        public void BindDdlShipCompany(ref DropDownList ddl, ShipCompanyCollection scCol)
        {
            foreach (ShipCompany sc in scCol)
            {
                ddl.Items.Add(new ListItem(sc.ShipCompanyName, sc.Id.ToString()));
            }
        }

        #endregion

        #region 客服紀錄
        protected void btnSetServiceMsg_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(lblIOrder.Text.Trim()))
            {
                if (this.OnSetServiceMsgClicked != null)
                {
                    ServiceMessage ServiceMessageData = GetServiceMessageData();
                    this.OnSetServiceMsgClicked(sender, new DataEventArgs<ServiceMessage>(GetServiceMessageData()));

                    CommonFacade.AddAudit(HiddenOrderGuid.Value, AuditType.Member, ServiceMessageData.MessageType + "-"
                    + MemberFacade.ServiceMessageCategoryGetByCategoryId((int)ServiceMessageData.Category).CategoryName + "-"
                    + MemberFacade.ServiceMessageCategoryGetByCategoryId((int)ServiceMessageData.SubCategory).CategoryName, User.Identity.Name, true);

                    ddlCategory.SelectedIndex = 0;
                    lblMessage.Text = "客服資料新增成功";
                    //記錄頁面reload 已顯示最新紀錄
                    AuditBoardView.Presenter.OnViewInitialized();
                }
                else
                {
                    lblMessage.Text = "客服資料新增失敗";
                }
            }
            else
            {
                lblMessage.Text = "無此訂單編號!!";
            }
        }

        protected void btnAddServiceMsgClose_Click(object sender, EventArgs e)
        {
            divAddServiceLog.Visible = false;
        }

        protected void bAddServiceLog_Click(object sender, EventArgs e)
        {
            divAddServiceLog.Visible = true;
        }

        protected void bServiceIntegrate_Click(object sender, EventArgs e)
        {
            Response.Redirect("../User/ServiceIntegrate.aspx?name=" + lblIEmail.Text);
        }

        private ServiceMessage GetServiceMessageData()
        {
            ServiceMessage sm = new ServiceMessage();
            sm.Email = lblIEmail.Text;
            sm.Phone = lblIPhone.Text;
            sm.Name = lblIName.Text;
            sm.OrderId = lblIOrder.Text;
            if (!string.IsNullOrEmpty(rdlWorkType.SelectedValue))
            {
                int workType;
                int.TryParse(rdlWorkType.SelectedValue, out workType);
                sm.WorkType = workType;
            }
            sm.Message = txtIMessage.Text;
            sm.MessageType = ddlItype.SelectedValue;
            sm.Type = Convert.ToInt32(HiddenAddType.Value);

            int temp;
            sm.Category = int.TryParse(ddlCategory.SelectedValue, out temp) ? temp : 0;
            sm.SubCategory = int.TryParse(hdSubCategory.Text, out temp) ? temp : 0;

            sm.Status = ddlIStatus.SelectedValue;
            sm.WorkUser = Page.User.Identity.Name;
            sm.CreateId = Page.User.Identity.Name;
            sm.CreateTime = DateTime.Now;
            sm.ModifyId = Page.User.Identity.Name;
            sm.ModifyTime = DateTime.Now;

            return sm;
        }

        public void BuildServiceCategory(ServiceMessageCategoryCollection serviceCates, DropDownList ddl)
        {
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("請選擇", "0"));
            foreach (var cate in serviceCates)
            {
                ddl.Items.Add(new ListItem(cate.CategoryName, cate.CategoryId.ToString()));
            }
        }

        [WebMethod]
        public static string GetServiceMessageCategory(int parentId)
        {
            var serviceCates = MemberFacade.ServiceMessageCategoryGetListByParentId(parentId).ToList();
            return new JsonSerializer().Serialize(serviceCates);
        }

        #endregion

        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
                //給關聯訂單狀態紀錄表之Order_Guid值 以顯示訂單狀態異動紀錄內容
                abHidealOrderLog.ReferenceId = this.OrderGuid.ToString();
                CheckIsLockRefund();
            }
            else
            {
                this.BuildSubCategory();
            }
            _presenter.OnViewLoaded();
        }

        /// <summary>
        /// 確認是否有鎖定憑證
        /// </summary>
        private void CheckIsLockRefund()
        {
            if (IsLockRefund)
            {
                btnMakeReturnForm.Enabled = false;
                cbBackSCash.Enabled = false;
                btnReturnAllPass.Enabled = false;
                litRefundMsg.Text = "有憑證預約已鎖定，恕無法退貨";
            }
        }

        protected void gvCouponList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var coupon = (ViewHiDealCouponTrustStatus)e.Row.DataItem;

                Label lblSequence = (Label)e.Row.FindControl("lblSequence");
                Label lblStatus = (Label)e.Row.FindControl("lblStatus");

                lblSequence.Text = coupon.Prefix + coupon.Sequence;
                if (coupon.LogSataus != null)
                {
                    lblStatus.Text = Helper.GetLocalizedEnum((TrustStatus)coupon.LogSataus);
                }
                else
                {
                    lblStatus.Text = "";
                }
            }
        }

        protected void gvInvoiceCouponList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var hiDealInvoiceCouponInfo = (HiDealInvoiceCouponInfo)e.Row.DataItem;

                ViewBookingSystemCouponReservationCollection vbscrCol = BookingSystemFacade.GetViewBookingSystemCouponReservationBySequenceNumber(OrderGuid.ToString(), hiDealInvoiceCouponInfo.CouponSequence.Substring(hiDealInvoiceCouponInfo.CouponSequence.Length - 11, 11));
                if (vbscrCol.Any())
                {
                    ((Label)e.Row.FindControl("lblReservationCreateDate")).Text = vbscrCol.FirstOrDefault().CreateDatetime.ToString("yyyy/MM/dd HH:mm:ss");
                    ((Label)e.Row.FindControl("lblReservationDateTime")).Text = ((DateTime)vbscrCol.FirstOrDefault().ReservationDateTime).ToString("yyyy/MM/dd HH:mm");

                    if (vbscrCol.FirstOrDefault().IsCancel)
                    {
                        ((Label)e.Row.FindControl("lblReservationStatus")).Text = "已取消(" + ((DateTime)vbscrCol.FirstOrDefault().CancelDate).ToString("yyyy/MM/dd") + ")";
                    }
                    if (vbscrCol.FirstOrDefault().ReservationDate < System.DateTime.Today)
                    {
                        ((Label)e.Row.FindControl("lblReservationStatus")).Text = "已過期";
                    }
                }

                if (hiDealInvoiceCouponInfo.IsReservationLock)
                {
                    ((Label)e.Row.FindControl("lblReservationStatus")).Text = "已鎖定";
                    if (hiDealInvoiceCouponInfo.CouponStatus != null && ((int)(TrustStatus)hiDealInvoiceCouponInfo.CouponStatus.Value) < (int)TrustStatus.Verified)
                    {
                        IsLockRefund = true;
                    }
                }

                Label lblSequence = (Label)e.Row.FindControl("lblSequence");
                Label lblStatus = (Label)e.Row.FindControl("lblStatus");

                //憑證編號
                lblSequence.Text = hiDealInvoiceCouponInfo.CouponSequence;
                //憑證狀態
                if (hiDealInvoiceCouponInfo.CouponStatus != null)
                {
                    lblStatus.Text = Helper.GetLocalizedEnum((TrustStatus)hiDealInvoiceCouponInfo.CouponStatus);
                    if (!string.IsNullOrEmpty(hid_RefundId.Value) && (int)(TrustStatus)hiDealInvoiceCouponInfo.CouponStatus < (int)TrustStatus.Verified)
                    {
                        //有退貨單，調整顯示文字
                        lblStatus.Text = "退貨中";
                    }
                }
                else
                {
                    lblStatus.Text = "";
                }
                //中獎發票
                ((CheckBox)e.Row.FindControl("cb_InvoiceWinning")).Checked = hiDealInvoiceCouponInfo.InvoiceWinning;
                //折讓單PDF
                ((HyperLink)e.Row.FindControl("hl_ShowDiscountSingleForm")).NavigateUrl = hiDealInvoiceCouponInfo.DiscountSingleFormURL;
                //寄發折讓單EMail
                // ((LinkButton)e.Row.FindControl("lb_SendDiscountSingleFormNotification")).PostBackUrl=hiDealInvoiceCouponInfo.DiscountSingleFormURL;
                //折讓單是否已寄回
                ((CheckBox)e.Row.FindControl("cb_isReturnDiscountSingleForm")).Checked = hiDealInvoiceCouponInfo.IsInvoiceMailbackAllowance.GetValueOrDefault(false);
                //發票是否已寄回
                ((CheckBox)e.Row.FindControl("cb_isReturnEInvoice")).Checked = hiDealInvoiceCouponInfo.IsInvoiceMailbackPaper.GetValueOrDefault(false);
                //發票Id
                ((Label)e.Row.FindControl("lblEInvoiceId")).Text = hiDealInvoiceCouponInfo.EInvoiceId.ToString();

                //未開發票 及 已核銷的憑證，折讓單功能等鎖定
                if (string.IsNullOrEmpty(hiDealInvoiceCouponInfo.InvoiceNumber) || string.IsNullOrEmpty(hid_RefundId.Value) || ((hiDealInvoiceCouponInfo.CouponStatus != null) && (int)(TrustStatus)hiDealInvoiceCouponInfo.CouponStatus > (int)TrustStatus.Trusted))
                {
                    ((HyperLink)e.Row.FindControl("hl_ShowDiscountSingleForm")).Enabled = false;
                    ((LinkButton)e.Row.FindControl("lb_SendDiscountSingleFormNotification")).Enabled = false;
                    ((CheckBox)e.Row.FindControl("cb_isReturnDiscountSingleForm")).Enabled = false;
                    ((Button)e.Row.FindControl("bt_ChangeReturnDiscountSingleFormStatus")).Enabled = false;
                    ((CheckBox)e.Row.FindControl("cb_isReturnEInvoice")).Enabled = false;
                    ((Button)e.Row.FindControl("bt_ChangeReturnEInvoiceStatus")).Enabled = false;
                }
            }

        }

        protected void gvInvoiceCouponList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "SendDiscountSingleFormNotification")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = gvInvoiceCouponList.Rows[index];
                if (SendDiscountSingleFormNotification != null)
                {
                    HiDealReturnedCollection hdrc = HiDealReturnedCol;
                    if (hdrc != null && hdrc.Count > 0)
                    {
                        var data = new RefundFormPageInfo()
                        {
                            ReturnedId = hdrc.First().Id,
                            OrderPk = hdrc.First().OrderPk,
                            UserName = hdrc.First().CreateId,
                            EinvoiceId = int.Parse(((Label)row.FindControl("lblEInvoiceId")).Text),
                        };
                        SendDiscountSingleFormNotification(sender, new DataEventArgs<RefundFormPageInfo>(data));
                        ShowMessage("折讓單已寄出。");
                    }
                    else
                    {
                        ShowMessage("查無退貨單。");
                    }
                }
            }

            if (e.CommandName == "SetInvoiceMailbackAllowance" || e.CommandName == "SetInvoiceMailbackPaper")
            {

                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = gvInvoiceCouponList.Rows[index];
                if (UpdateEInvoiceMainInfo != null)
                {
                    var data = new EInvoiceMainInfo()
                    {
                        EInvoiceId = int.Parse(((Label)row.FindControl("lblEInvoiceId")).Text),
                        IsInvoiceMailbackAllowance = (e.CommandName == "SetInvoiceMailbackAllowance") ? !((CheckBox)row.FindControl("cb_isReturnDiscountSingleForm")).Checked : ((CheckBox)row.FindControl("cb_isReturnDiscountSingleForm")).Checked,
                        IsInvoiceMailbackPaper = (e.CommandName == "SetInvoiceMailbackPaper") ? !((CheckBox)row.FindControl("cb_isReturnEInvoice")).Checked : ((CheckBox)row.FindControl("cb_isReturnEInvoice")).Checked
                    };
                    UpdateEInvoiceMainInfo(sender, new DataEventArgs<EInvoiceMainInfo>(data));
                    ShowMessage("發票資料已更新。");
                }

            }

        }

        protected void gvInvoiceGoodsList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var hiDealInvoiceGoodsInfo = (HiDealInvoiceGoodsInfo)e.Row.DataItem;
                Label lblOrderItem = (Label)e.Row.FindControl("lblOrderItem");
                Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                //憑證狀態
                if (hiDealInvoiceGoodsInfo.CouponStatus != null)
                {
                    lblStatus.Text = Helper.GetLocalizedEnum((TrustStatus)hiDealInvoiceGoodsInfo.CouponStatus);
                    if (!string.IsNullOrEmpty(hid_RefundId.Value) && (int)(TrustStatus)hiDealInvoiceGoodsInfo.CouponStatus < (int)TrustStatus.Verified)
                    {
                        //有退貨單，調整顯示文字
                        lblStatus.Text = "退貨中";
                    }
                }
                else
                {
                    lblStatus.Text = "";
                }

                //訂單商品名稱
                lblOrderItem.Text = hiDealInvoiceGoodsInfo.OrderItemName;
                //中獎發票
                ((CheckBox)e.Row.FindControl("cb_InvoiceWinning")).Checked = hiDealInvoiceGoodsInfo.InvoiceWinning;
                //折讓單PDF
                ((HyperLink)e.Row.FindControl("hl_ShowDiscountSingleForm")).NavigateUrl = hiDealInvoiceGoodsInfo.DiscountSingleFormURL;
                //寄發折讓單EMail
                // ((LinkButton)e.Row.FindControl("lb_SendDiscountSingleFormNotification")).PostBackUrl=hiDealInvoiceCouponInfo.DiscountSingleFormURL;
                //折讓單是否已寄回
                ((CheckBox)e.Row.FindControl("cb_isReturnDiscountSingleForm")).Checked = hiDealInvoiceGoodsInfo.IsInvoiceMailbackAllowance.GetValueOrDefault(false);
                //發票是否已寄回
                ((CheckBox)e.Row.FindControl("cb_isReturnEInvoice")).Checked = hiDealInvoiceGoodsInfo.IsInvoiceMailbackPaper.GetValueOrDefault(false);
                //發票Id
                ((Label)e.Row.FindControl("lblEInvoiceId")).Text = hiDealInvoiceGoodsInfo.EInvoiceId.ToString();

                //未開發票 或 無退貨單，折讓單功能等鎖定
                if (string.IsNullOrEmpty(hiDealInvoiceGoodsInfo.InvoiceNumber) || string.IsNullOrEmpty(hid_RefundId.Value) || ((hiDealInvoiceGoodsInfo.CouponStatus != null) && (int)(TrustStatus)hiDealInvoiceGoodsInfo.CouponStatus > (int)TrustStatus.Trusted))
                {
                    ((HyperLink)e.Row.FindControl("hl_ShowDiscountSingleForm")).Enabled = false;
                    ((LinkButton)e.Row.FindControl("lb_SendDiscountSingleFormNotification")).Enabled = false;
                    ((CheckBox)e.Row.FindControl("cb_isReturnDiscountSingleForm")).Enabled = false;
                    ((Button)e.Row.FindControl("bt_ChangeReturnDiscountSingleFormStatus")).Enabled = false;
                    ((CheckBox)e.Row.FindControl("cb_isReturnEInvoice")).Enabled = false;
                    ((Button)e.Row.FindControl("bt_ChangeReturnEInvoiceStatus")).Enabled = false;
                }


            }
        }

        protected void gvInvoiceGoodsList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "SendDiscountSingleFormNotification")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = gvInvoiceGoodsList.Rows[index];
                if (SendDiscountSingleFormNotification != null)
                {
                    HiDealReturnedCollection hdrc = HiDealReturnedCol;
                    if (hdrc != null && hdrc.Count > 0)
                    {
                        var data = new RefundFormPageInfo()
                        {
                            ReturnedId = hdrc.First().Id,
                            OrderPk = hdrc.First().OrderPk,
                            UserName = hdrc.First().CreateId,
                            EinvoiceId = int.Parse(((Label)row.FindControl("lblEInvoiceId")).Text),
                        };
                        SendDiscountSingleFormNotification(sender, new DataEventArgs<RefundFormPageInfo>(data));
                        ShowMessage("折讓單已寄出。");
                    }
                    else
                    {
                        ShowMessage("查無退貨單。");
                    }
                }
            }

            if (e.CommandName == "SetInvoiceMailbackAllowance" || e.CommandName == "SetInvoiceMailbackPaper")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = gvInvoiceGoodsList.Rows[index];

                if (UpdateEInvoiceMainInfo != null)
                {
                    var data = new EInvoiceMainInfo()
                    {
                        EInvoiceId = int.Parse(((Label)row.FindControl("lblEInvoiceId")).Text),
                        IsInvoiceMailbackAllowance = (e.CommandName == "SetInvoiceMailbackAllowance") ? !((CheckBox)row.FindControl("cb_isReturnDiscountSingleForm")).Checked : ((CheckBox)row.FindControl("cb_isReturnDiscountSingleForm")).Checked,
                        IsInvoiceMailbackPaper = (e.CommandName == "SetInvoiceMailbackPaper") ? !((CheckBox)row.FindControl("cb_isReturnEInvoice")).Checked : ((CheckBox)row.FindControl("cb_isReturnEInvoice")).Checked
                    };
                    UpdateEInvoiceMainInfo(sender, new DataEventArgs<EInvoiceMainInfo>(data));

                    ShowMessage("發票資料已更新。");
                }
            }
        }

        protected void gvReturnLsit_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiDealReturned returned = (HiDealReturned)e.Row.DataItem;


                Label lblCashBack = (Label)e.Row.FindControl("lblCashBack");
                Label lblCash = (Label)e.Row.FindControl("lblCash");
                Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                Button btnReturnCash = (Button)e.Row.FindControl("btnReturnCash");
                Button btnReturnComplete = (Button)e.Row.FindControl("btnReturnComplete");

                if (returned.ReturnCashBack)
                {
                    lblCashBack.Text = "是";
                    btnReturnCash.Visible = false;
                }
                else
                {
                    lblCashBack.Text = "否";
                    btnReturnCash.Visible = true;
                    btnReturnCash.CommandArgument = returned.Id.ToString();
                }
                if (returned.Status == (int)HiDealReturnedStatus.Create)
                {
                    btnReturnComplete.Visible = true;
                    btnReturnComplete.CommandArgument = returned.Id.ToString();
                }
                else
                {
                    btnReturnComplete.Visible = false;
                }
                lblCash.Text = "PEZ購物金: " + returned.Pcash.ToString("N0") +
                               "<br />17Life購物金: " + returned.Scash.ToString("N0") +
                               "<br />17Life紅利: " + returned.Bcash.ToString("N0") +
                               "<br />刷卡: " + returned.CreditCardAmt.ToString("N0") +
                               "<br />折價券: " + returned.DiscountAmount.ToString("N0") +
                               "<br />ATM: " + returned.AtmAmount.ToString("N0") +
                               "<br /><b>總額: NT$" + returned.TotalAmount.ToString("N0") + "</b>";
                lblStatus.Text = SystemCodeManager.GetHiDealReturnedStatusCodeName(returned.Status);

            }
        }

        protected void gvReturnLsit_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "ReturnCash")
            {
                //購物金轉刷退
                if (ReturnCash != null)
                {
                    int returnId;
                    if (int.TryParse(e.CommandArgument.ToString(), out returnId))
                    {
                        ReturnCash(sender, new DataEventArgs<int>(returnId));
                    }
                }
            }
            else if (e.CommandName == "ReturnComplete")
            {
                //核准退貨申請
                if (ReturnComplete != null)
                {
                    int returnId;
                    if (int.TryParse(e.CommandArgument.ToString(), out returnId))
                    {
                        ReturnComplete(sender, new DataEventArgs<int>(returnId));
                    }
                }

            }

        }

        #region 出貨紀錄

        protected void gvDeliver_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddl = (DropDownList)e.Row.FindControl("ddlShipCompanyId");
                if (ddl != null)
                {
                    BindDdlShipCompany(ref ddl, _shipCompanyCol);
                    ddl.SelectedValue = ((ViewOrderShipList)e.Row.DataItem).ShipCompanyId != null ? ((ViewOrderShipList)e.Row.DataItem).ShipCompanyId.ToString() : string.Empty;
                }
                Label lb = (Label)e.Row.FindControl("lbShipCompanyId");
                if (lb != null)
                {
                    lb.Text = ((ViewOrderShipList)e.Row.DataItem).ShipCompanyName != null ? ((ViewOrderShipList)e.Row.DataItem).ShipCompanyName.ToString() : string.Empty;
                }
                TextBox tb = (TextBox)e.Row.FindControl("tbShipMemo");
                if (tb != null)
                {
                    tb.Attributes.Add("maxlength", ShipMemoLimit.ToString());
                }
            }
        }
        protected void gvDeliver_RowEditing(object sender, GridViewEditEventArgs e)
        {
            if (this.GoodsReturnStatus != (int)OrderGoodsReturnStatus.AllReturned)
            {
                gvDeliver.EditIndex = e.NewEditIndex;
                this._presenter.OnViewInitialized();
            }
            else
            {
                this.alertMessage = "已退貨的訂單無法編輯出貨資訊!";
            }
        }
        protected void gvDeliver_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvDeliver.EditIndex = -1;
            this._presenter.OnViewInitialized();
        }
        protected void gvDeliver_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            if (this.UpdateOrderShip != null)
            {
                OrderShipArgs os = new OrderShipArgs();
                os.Id = (int)gvDeliver.DataKeys[e.RowIndex].Value;
                DropDownList ddlShipCompanyId = (DropDownList)gvDeliver.Rows[e.RowIndex].FindControl("ddlShipCompanyId");
                os.ShipCompanyId = ddlShipCompanyId != null ? Convert.ToInt32(ddlShipCompanyId.SelectedValue) : 0;
                TextBox tbShipTime = (TextBox)gvDeliver.Rows[e.RowIndex].FindControl("tbShipTime");
                DateTime shipTime;
                TextBox txtShipMemo = (TextBox)gvDeliver.Rows[e.RowIndex].FindControl("tbShipMemo");
                if (tbShipTime != null && DateTime.TryParse(tbShipTime.Text, out shipTime))
                {
                    os.ShipTime = shipTime;
                }
                else
                {
                    this.alertMessage = "請輸入正確的出貨日期!";
                    return;
                }
                TextBox tbShipNo = (TextBox)gvDeliver.Rows[e.RowIndex].FindControl("tbShipNo");
                if (tbShipNo != null && tbShipNo.Text.Trim() != string.Empty)
                {
                    os.ShipNo = tbShipNo != null ? tbShipNo.Text.Trim() : string.Empty;
                }
                else
                {
                    this.alertMessage = "請輸入運單編號!";
                    return;
                }
                os.ShipMemo = txtShipMemo.Text;
                this.UpdateOrderShip(this, new DataEventArgs<OrderShipArgs>(os));
                gvDeliver.EditIndex = -1;
                this._presenter.OnViewInitialized();
            }
        }
        protected void gvDeliver_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (this.GoodsReturnStatus != (int)OrderGoodsReturnStatus.AllReturned)
            {
                if (this.DeleteOrderShip != null)
                {
                    OrderShipArgs os = new OrderShipArgs();
                    os.Id = (int)gvDeliver.DataKeys[e.RowIndex].Value;
                    this.DeleteOrderShip(this, new DataEventArgs<OrderShipArgs>(os));
                    gvDeliver.EditIndex = -1;
                }
            }
            else
            {
                this.alertMessage = "已退貨的訂單無法刪除出貨資訊!";
            }
            this._presenter.OnViewInitialized();
        }

        #endregion

        /// <summary>
        /// 產生退貨單並強制退貨的click事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnReturnAllPassOnClick(object sender, EventArgs e)
        {
            if (cbBackSCash.Checked)
            {
                if (ReturnAllPassAndPointBack != null)
                {
                    ReturnAllPassAndPointBack(sender, new DataEventArgs<Guid>(OrderGuid));
                }
            }
            else
            {
                if (ReturnAllPassAndCashBack != null)
                {
                    ReturnAllPassAndCashBack(sender, new DataEventArgs<Guid>(OrderGuid));
                }
            }
        }

        protected void BtnMakeReturnFormOnClick(object sender, EventArgs e)
        {
            if (MakeReturnedForm != null)
            {
                var data = new HidealOrderDetailListViewMakeReturnedForm()
                {
                    OrderGuid = OrderGuid,
                    IsCashBack = !cbBackSCash.Checked
                };
                MakeReturnedForm(sender, new DataEventArgs<HidealOrderDetailListViewMakeReturnedForm>(data));
            }
        }
        /// <summary>
        /// 發票部分的資料異動
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnInvoiceUpdate(object sender, EventArgs e)
        {
            if (InvoiceUpdate != null)
            {
                var data = new InvoiceUpdateData()
                {
                    AllowanceFormBack = cbAllowance.Checked,
                    BuyerAddress = invCtrl.BuyerAddress,
                    BuyerName = invCtrl.BuyerName,
                    CompanyId = invCtrl.CompanyId,
                    CompanyName = invCtrl.CompanyName,
                    InvoiceFormBack = cbInvoice.Checked,
                    Message = tbInvoiceMesssage.Text.Trim()
                };
                InvoiceMode2 newMode;
                if (Enum.TryParse(invCtrl.InvoiceMode, out newMode))
                {
                    data.InvoiceType = newMode;
                }
                else
                {
                    data.InvoiceType = InvoiceMode2.Undefined;
                }
                if (invCtrl.Version == InvoiceVersion.CarrierEra)
                {
                    data.CarrierId = invCtrl.CarrierId;
                    data.CarrierType = invCtrl.CarrierType;
                    if (invCtrl.CarrierType == CarrierType.Member)
                    {
                        data.CarrierId = invCtrl.UserId.ToString();
                    }
                    else if (invCtrl.CarrierType == CarrierType.None)
                    {
                        data.CarrierId = null;
                    }
                }
                InvoiceUpdate(this, new DataEventArgs<InvoiceUpdateData>(data));
            }
        }
        /// <summary>
        /// 更新退貨單-退貨申請書之值
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnUpdateReturned(object sender, EventArgs e)
        {
            if (UpdateReturned != null)
            {
                int a = int.Parse(hid_RefundId.Value);
                var data = new RefundFormBackInfo()
                {
                    ReturnedId = int.Parse(hid_RefundId.Value),
                    IsRefundFormBack = cbRefundFormBack.Checked
                };

                UpdateReturned(this, new DataEventArgs<RefundFormBackInfo>(data));
            }
        }

        /// <summary>
        /// 取消退貨紀錄。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnReturnCancelOnClick(object sender, EventArgs e)
        {
            if (ReturnedFormCancel != null)
            {
                ReturnedFormCancel(sender, new DataEventArgs<Guid>(OrderGuid));
            }
        }

        public void SetHiDealOrderDetailList(HiDealOrderDetailCollection hodc)
        {
            foreach (HiDealOrderDetail hod in hodc)
            {
                if (hod.ProductType.Equals((int)HiDealProductType.Product))
                {
                    hidOrderGuid.Value = hod.HiDealOrderGuid.ToString();
                    HiddenOrderGuid.Value = hod.HiDealOrderGuid.ToString();

                    hkSellerName.Text = hod.SellerName;
                    hkSellerName.NavigateUrl = ResolveUrl("~/controlroom/seller/seller_add.aspx?sid=") + hod.SellerGuid + "&Tc=" + (int)SellerTabPage.PiinLifeSetUp;
                    hkProductName.Text = hod.ProductName;
                    hkProductName.NavigateUrl = ResolveUrl("~/piinlife/HiDeal.aspx?pid=") + hod.ProductId;
                    hidProductId.Value = hod.ProductId.ToString();
                    lblProductUnitPrice.Text = hod.UnitPrice.ToString("N2");
                    lblProductItemQuantity.Text = hod.ItemQuantity.ToString("F0");
                    lblProductDetailTotalAmt.Text = hod.DetailTotalAmt.ToString("N2");
                    lblCategoryName1.Text = !string.IsNullOrEmpty(hod.CatgName1) ? hod.CatgName1 + ": " + hod.OptionName1 : string.Empty;
                    lblCategoryName2.Text = !string.IsNullOrEmpty(hod.CatgName2) ? hod.CatgName2 + ": " + hod.OptionName2 : string.Empty;
                    lblCategoryName3.Text = !string.IsNullOrEmpty(hod.CatgName3) ? hod.CatgName3 + ": " + hod.OptionName3 : string.Empty;
                    lblCategoryName4.Text = !string.IsNullOrEmpty(hod.CatgName4) ? hod.CatgName4 + ": " + hod.OptionName4 : string.Empty;
                    lblCategoryName5.Text = !string.IsNullOrEmpty(hod.CatgName5) ? hod.CatgName5 + ": " + hod.OptionName5 : string.Empty;
                    #region 目前單一訂單只會有單一商品，於此處顯示 送貨方式相關資訊
                    //目前單一訂單只會有單一商品，於此處顯示 送貨方式相關資訊
                    if (hod.DeliveryType.Equals((int)HiDealDeliveryType.ToHouse))
                    {
                        lblAddressName.Text = hod.AddresseeName;
                        lblAddressPhone.Text = hod.AddresseePhone;
                        lblDeliveryAddress.Text = hod.DeliveryAddress;
                    }
                    hidDeliveryType.Value = hod.DeliveryType.ToString();
                    lblDeliveryType.Text = SystemCodeManager.GetSystemCodeNameByEnumValue((HiDealDeliveryType)hod.DeliveryType);
                    #endregion 目前單一訂單只會有單一商品，於此處顯示 送貨方式相關資訊

                }
                else
                {
                    lblFreightUnitPrice.Text = hod.UnitPrice.ToString("N2");
                }
            }
        }

        public void SetHiDealOrder(HiDealOrder ho)
        {
            hidOrderPk.Value = ho.Pk.ToString();
            lblOrderId.Text = ho.OrderId;
            lblCreateTime.Text = ho.CreateTime.ToString();
            lblModifyId.Text = ho.ModifyId;
            lblModifyTime.Text = (ho.ModifyTime != null) ? ho.ModifyTime.Value.ToLongDateString() : string.Empty;
            lblOrderTotalAmount.Text = ho.TotalAmount.ToString("N2");
            lblPcash.Text = ho.Pcash.ToString("N2");
            lblScash.Text = ho.Scash.ToString("N2");
            lblBcash.Text = ho.Bcash.ToString("N2");
            lblDiscountAmount.Text = ho.DiscountAmount.ToString("N2");
            lblCreditCardAmt.Text = ho.CreditCardAmt.ToString("N2");
            lblAtmAmount.Text = ho.AtmAmount.ToString("N2");
            lblIOrder.Text = ho.OrderId;
        }

        public void SetMember(Member m)
        {
            hkUserName.Text = m.DisplayName;
            hkUserName.NavigateUrl = ResolveUrl("~/controlroom/user/users_edit.aspx?username=") + m.UserName;
            lblCompanyTel.Text = m.CompanyTel + (!string.IsNullOrEmpty(m.CompanyTelExt) ? " # " + m.CompanyTelExt : string.Empty);
            lblMobile.Text = m.Mobile;
            lblIName.Text = m.DisplayName;
            lblIEmail.Text = m.UserName;
            if (!string.IsNullOrEmpty(m.Mobile))
                lblIPhone.Text = m.Mobile;
            else
                lblIPhone.Text = lblCompanyTel.Text;
            divAddServiceLog.Visible = false;
        }

        public void SetInvoiceCouponList(List<HiDealInvoiceCouponInfo> hiDealInvoiceCouponInfos)
        {
            //發票憑證列表
            if (hiDealInvoiceCouponInfos.Any())
            {
                divInvoiceCouponList.Visible = true;
                gvInvoiceCouponList.DataSource = hiDealInvoiceCouponInfos;
                gvInvoiceCouponList.DataBind();
            }

        }
        public void SetInvoiceGoodsList(List<HiDealInvoiceGoodsInfo> hiDealInvoiceGoodsInfos)
        {
            if (hiDealInvoiceGoodsInfos.Any())
            {
                //發票商品列表
                divInvoiceGoodsList.Visible = true;
                gvInvoiceGoodsList.DataSource = hiDealInvoiceGoodsInfos;
                gvInvoiceGoodsList.DataBind();
            }
        }

        public void SetReturnedList(HiDealReturnedCollection hdrc)
        {
            HiDealReturnedCol = hdrc;
            gvReturnLsit.DataSource = hdrc;
            gvReturnLsit.DataBind();
            //檢查退貨單紀錄，有新建的退貨單，顯示退貨申請書的回收狀況
            cbRefundFormBack.Checked = false; //預設為尚未寄回
            hid_RefundId.Value = string.Empty;
            var returneds = hdrc.Where(x => x.Status == (int)HiDealReturnedStatus.Create).OrderByDescending(x => x.ApplicationTime).ToList();
            if (returneds.Count > 0)
            {
                //有退貨單，退貨申請單等功能開啟
                cbRefundFormBack.Enabled = true;
                hl_ShowReturnApplicationForm.Enabled = true;
                lb_SendReturnApplicationFormNotification.Enabled = true;
                cbRefundFormBack.Visible = true;
                hl_ShowReturnApplicationForm.Visible = true;
                lb_SendReturnApplicationFormNotification.Visible = true;
                btnUpdateEInvoice.Visible = true;
                hid_RefundId.Value = returneds.First().Id.ToString();

                if (returneds.First().RefundFormBack)
                {
                    cbRefundFormBack.Checked = true;
                }
            }
        }

        public void SetEInvoiceMainData(EinvoiceMainCollection invCol)
        {
            if (invCol.Count > 0)
            {
                //優先顯示未開立發票的資料供修改
                EinvoiceMain einvoice = invCol.FirstOrDefault(t => string.IsNullOrEmpty(t.InvoiceNumber));
                if (einvoice == null)
                {
                    if (invCol.Count > 0)
                    {
                        einvoice = invCol[0];
                    }
                    else
                    {
                        einvoice = new EinvoiceMain();
                    }
                }
                cbHaveInvoice.Checked = true;
                lbl_EinvoiceNum.Text = string.IsNullOrWhiteSpace(einvoice.InvoiceNumber) ? "未開發票" : einvoice.InvoiceNumber;
                hif_EinvoiceId.Value = einvoice.Id.ToString();
                cbx_EinvoicePapered.Checked = einvoice.InvoicePapered;
                lab_EinvoicePapered.Text = einvoice.InvoicePaperedTime == null
                                               ? string.Empty
                                               : ("印製:" + einvoice.InvoicePaperedTime.Value.ToString("yyyy/MM/dd HH:mm"));
                lab_EinvoiceRequested.Text = einvoice.InvoiceRequestTime == null
                                                 ? string.Empty
                                                 : ("申請:" +
                                                    einvoice.InvoiceRequestTime.Value.ToString("yyyy/MM/dd HH:mm"));
                cbAllowance.Checked = einvoice.InvoiceMailbackAllowance ?? false;
                cbInvoice.Checked = einvoice.InvoiceMailbackPaper ?? false;

                tbInvoiceMesssage.Text = einvoice.Message;

                invCtrl.SetData(einvoice, invCol);

                cbx_EinvoiceWinner.Checked = einvoice.InvoiceWinning;
                InvoiceVersion version = (InvoiceVersion)einvoice.Version;
                if (version == InvoiceVersion.Old)
                {
                    lblInvoiceVersion.Text = "[舊電子發票]";
                }
                else if (version == InvoiceVersion.CarrierEra)
                {
                    lblInvoiceVersion.Text = "[發票載具]";
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="payments"></param>
        /// <returns></returns>
        private string GetChargingSituation(PaymentTransactionCollection payments)
        {
            string pList = string.Empty;
            string pType = string.Empty;
            string tType = string.Empty;
            string pStatus = string.Empty;

            List<PaymentType> selType = new List<PaymentType>
                                            {
                                                PaymentType.Creditcard,
                                                PaymentType.PCash,
                                                PaymentType.BonusPoint
                                            };

            foreach (var p in payments.Where(x => selType.Contains((PaymentType)x.PaymentType)))
            {
                if (p != null)
                {
                    switch ((PaymentType)p.PaymentType)
                    {
                        case PaymentType.Creditcard:
                            pType = Resources.Localization.CreditCard;
                            break;
                        case PaymentType.PCash:
                            pType = Resources.Localization.PCash;
                            break;
                        case PaymentType.BonusPoint:
                            pType = Resources.Localization.BonusPoint;
                            break;
                        default:
                            break;
                    }
                    switch ((PayTransType)p.TransType)
                    {
                        case PayTransType.Authorization:
                            if ((PaymentType)p.PaymentType == PaymentType.Creditcard)
                                tType = Resources.Localization.PayTransAuthorization;
                            else
                                tType = Resources.Localization.PayTransCharging;
                            break;
                        case PayTransType.Charging:
                            tType = Resources.Localization.PayTransCharging;
                            break;
                        case PayTransType.Refund:
                            tType = Resources.Localization.PayTransRefund;
                            break;
                        default:
                            break;
                    }
                    switch ((PayTransPhase)(p.Status & (int)PayTransStatusFlag.PhaseMask))
                    {
                        case PayTransPhase.Created:
                            pStatus = Resources.Localization.PayTransCreated;
                            break;
                        case PayTransPhase.Requested:
                            pStatus = Resources.Localization.PayTransRequested;
                            break;
                        case PayTransPhase.Canceled:
                            pStatus = Resources.Localization.PayTransCanceled;
                            break;
                        case PayTransPhase.Successful:
                            pStatus = Resources.Localization.PayTransSuccessful;
                            break;
                        case PayTransPhase.Failed:
                            pStatus = Resources.Localization.PayTransFailed;
                            break;
                        default:
                            break;
                    }
                    pList += pType + tType + pStatus + "<br/>";
                }
            }
            return pList;
        }

        protected void BuildSubCategory()
        {
            int cate = 0, subCate = 0;
            //有選擇客服紀錄的母類別，才要建置子類別
            if (int.TryParse(ddlCategory.SelectedValue, out cate) && cate > 0)
            {
                this.BuildServiceCategory(MemberFacade.ServiceMessageCategoryGetListByParentId(cate), ddlSubCategory);
                //利用Hidden把子類別的值記下來，並塞回下拉式選單
                if (int.TryParse(hdSubCategory.Text, out subCate))
                {
                    ddlSubCategory.SelectedValue = subCate.ToString();
                }
            }
        }

        #endregion


        protected void lb_SendReturnApplicationFormNotification_Click(object sender, EventArgs e)
        {
            if (SendRefundApplicationFormNotification != null)
            {
                HiDealReturnedCollection hdrc = HiDealReturnedCol;

                if (hdrc != null && hdrc.Count > 0)
                {
                    var data = new RefundFormPageInfo()
                    {
                        ReturnedId = hdrc.First().Id,
                        OrderPk = hdrc.First().OrderPk,
                        UserName = hdrc.First().CreateId,
                    };
                    SendRefundApplicationFormNotification(sender, new DataEventArgs<RefundFormPageInfo>(data));
                    ShowMessage("退貨申請書已寄出。");
                }
                else
                {
                    ShowMessage("查無退貨單。");

                }

            }
        }

    }
}
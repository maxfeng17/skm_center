﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="setup.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.piinlife.setup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" />
    <style type="text/css">
        fieldset legend
        {
            color: #1C94C4;
            font-size: 100%;
        }
        .mainLabel
        {
            font-size: smaller;
            color: #1C94C4;
            padding-top: 2px;
            padding-bottom: 2px;
        }
        label, .optionItem, select
        {
            font-size: smaller !important;
        }
        .inlineOption
        {
            padding-left: 20px;
        }
        .formValidationError
        {
            background: red;
        }
        .tblProducts, .tblStoreSettings
        {
            border-width: 1px;
            border-style: solid;
            border-color: #333333;
            border-collapse: collapse;
            margin-top: 10px;
            margin-left: 4px;
        }
        .tblProducts th, .tblStoreSettings th
        {
            background-color: #F6A828;
            border-color: #333333;
            border-style: solid;
            border-bottom-width: 1px;
            border-left-width: 0px;
            border-right-width: 0px;
            border-top-width: 1px;
            padding: 2px;
            padding-left: 6px;
            padding-right: 6px;
        }
        .tblProducts td, .tblStoreSettings td
        {
            border-bottom-width: 1px;
            border-left-width: 0px;
            border-right-width: 0px;
            border-top-width: 1px;
            background-color: #F5F5F5;
            padding: 2px;
        }
        .tblProducts tbody tr:hover td, .tblStoreSettings tbody tr:hover td
        {
            background-color: #EDBD3E;
        }
        .divGroupItem
        {
            padding-top: 4px;
            padding-bottom: 4px;
        }
        .horizontalPadding
        {
            padding-left: 20px;
        }
        .chkListClass label
        {
            margin-right: 15px;
        }
        #divSmallPictures
        {
            position: relative;
        }
        #divSmallPictures ul
        {
            margin-left: 0;
            padding-left: 0;
        }
        #divSmallPictures ul li
        {
            float: left;
            list-style-type: none;
            padding-top: 6px;
            padding-bottom: 6px;
            margin-left: 4px;
            padding-left: 4px;
        }
        #divSmallPictures img
        {
            padding-left: 24px;
        }
        
        .tblStoreCoupons
        {
            border-width: 1px;
            border-style: solid;
            border-color: #333333;
            border-collapse: collapse;
            margin-top: 10px;
            margin-left: 4px;
        }
        .tblStoreCoupons th
        {
            background-color: #F6A828;
            border-color: #333333;
            border-style: solid;
            border-bottom-width: 1px;
            border-left-width: 0px;
            border-right-width: 0px;
            border-top-width: 1px;
            padding: 2px;
            padding-left: 6px;
            padding-right: 6px;
        }
        .tblStoreCoupons td
        {
            border-bottom-width: 1px;
            border-left-width: 0px;
            border-right-width: 0px;
            border-top-width: 1px;
            background-color: #F5F5F5;
            padding: 2px;
        }
        .tblStoreCoupons tbody tr:hover td
        {
            background-color: #EDBD3E;
        }
        .addproduct
        {
            width: 500px;
            height: 50px;
            background-color: #EBDDC7;
            border: 2px solid #000000;
        }
        .addproduct-Top
        {
            width: 500px;
        }
        .addproduct-X-button
        {
            background-image: url(../../Themes/default/images/17Life/G2/A2-Multi-grade-Setting_xx.png);
            height: 28px;
            width: 27px;
            float: right;
            margin-top: -9px;
            margin-right: -9px;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabs').tabs();

            $('.datePicker').datepicker({ dateFormat: 'yy/mm/dd' });
            $('.datePicker').keydown(function () { $(this).blur(); });
            //        $('.timePicker').timepicker(
            //                { timeOnly: true,
            //                    timeOnlyTitle: '選擇時間',
            //                    timeText: '時間',
            //                    hourText: '時',
            //                    minuteText: '分',
            //                    currentText: '現在',
            //                    closeText: '確定',
            //                    hour: 12
            //                });
            //        $('.timePicker').keydown(function () { $(this).blur(); });
            $('.tblStoreSettings tbody').sortable();

            $('#divSmallPictures input:radio').change(updateSelectedSecondaryPictureUrl);

            //所有有設定 maxlegth 和 wordCountSpan 的 textbox, 加上字數輸入提示
            $('input[maxlength]').each(function (index, textbox) {
                $(textbox).focus(function () {
                    $('span[id="' + $(textbox).attr('wordCountSpan') + '"]').css('display', 'inline');
                    $('span[id="' + $(textbox).attr('wordCountSpan') + '"] span').text($(textbox).attr('maxlength') - $(textbox).val().length);
                });
                $(textbox).blur(function () {
                    $('span[id="' + $(textbox).attr('wordCountSpan') + '"]').css('display', 'none');
                });
                $(textbox).keyup(function () {
                    $('span[id="' + $(textbox).attr('wordCountSpan') + '"] span').text($(textbox).attr('maxlength') - $(textbox).val().length);
                });
            });

            $('.tblProducts tbody').sortable();
            $('#divSmallPictures ul').sortable();   //商品圖要可以排序

            $('#divSmallPictures input:hidden').each(function (index, hid) {
                if ($(hid).val() == $('#hidSelectedPicUrl').val()) {
                    $(hid).parent().find('input:radio', this).attr('checked', 'checked');
                }
            });

            if ($('#hidBigPicUpload').val() == 'true')
                $('a.picUpload').click();
            if ($('#hidSmallPicUpload').val() == 'true') {
                $('a.picUpload').click();
                window.location.hash = 'smallPicUpload';
            }

            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            //  "優惠設定" 的項目名稱若與 "類別" 的項目名稱相同時, 同步兩邊的勾選狀態
            $('.basicSetting_Category input').change(function () {
                var isChecked = $(this).attr('checked') == 'checked';
                var id = $(this).attr('id');
                var lblTxt = $('.basicSetting_Category label[for=' + id + ']').text();
                var discountLabels = $('.basicSetting_Discount label');
                discountLabels.each(function (index, lbl) {
                    if ($(lbl).text() == lblTxt) {
                        var discountChk = $('.basicSetting_Discount input[id=' + $(lbl).attr('for') + ']');
                        if (isChecked) {
                            $(discountChk).attr('checked', 'checked');
                        }
                        else {
                            $(discountChk).removeAttr('checked');
                        }
                    }
                });
            });

            /////////////////////////////////////////////////////////////////////////////////////////////////////////

            if (getSid() === 0) {
                tagManager.loadTagContents($('#hidDealTags').val());
            }

            var availableTags = [<%=SalesManNameList%>];
        $('#<%=tbSalesName.ClientID %>').autocomplete({
            source: availableTags
        });
    });
    function updatePictureSequence() {
        var picArray = [];
        $('#divSmallPictures ul li').each(
            function (index, li) {
                picArray[index] = $(li).find('input[type="hidden"]').val();
            });
        $('#hidSmallPicSequence').val(picArray.join(','));
    }

    function updateOrder() {
        //產品順序
        var trs = $('.tblProducts tbody tr');
        var str = '';
        trs.each(function (index, tr) {
            if (str != '')
                str += ',';
            str += $(tr).find('.cellProdName input:hidden').val();
        });
        $('#<%= hidProducts.ClientID %>').attr('value', str);
    }

    function updateOrderedStoreValues() {
        //format: "store_guid1, 1; store_guid2, 2"
        var trs = $('.tblStoreSettings tbody tr');
        var str = '';
        var seq = 0;
        trs.each(function (index, tr) {
            if ($(tr).find('input:checked', this).length != 0) {
                seq++;
                str += $(tr).find('td:eq(0) input', this).attr('value') + ',' + seq + ',' + $(tr).find('td:eq(2) textarea', this).val() + ';';
            }
        });
        str = str.substring(0, str.length - 1);
        $('#<%= hidOrderedStoreValues.ClientID %>').attr('value', str);
    }

    function getDid() {
        var did;
        $(window.location.search.substring(1).split('&')).each(function (index, queryParam) {
            if (queryParam.split('=')[0] == 'did') {
                did = queryParam.split('=')[1];
            }
        });
        if (did == undefined) {
            return 0;
        }
        else {
            return did;
        }
    }

    function getSid() {
        var sid;
        $(window.location.search.substring(1).split('&')).each(function (index, queryParam) {
            if (queryParam.split('=')[0] == 'sid') {
                sid = queryParam.split('=')[1];
            }
        });
        if (sid == undefined) {
            return 0;
        }
        else {
            return sid;
        }
    }

    function updateSelectedSecondaryPictureUrl() {
        var selectedRadio = $('#divSmallPictures input:checked');
        $('#hidSelectedPicUrl').val($(selectedRadio).parent().find('input[type="hidden"]', this).val());
    }

    function checkSecodaryPictureIsSelected() {
        if ($('#divSmallPictures li input:checked').length === 0)
            $('#divSmallPictures li input').first().attr('checked', 'checked');
        return;
    }

    function prePostbackEncoding() {
        //encoding is moved to tagManager.saveToHiddenField()
        //However, loaded data and saved data is store at different hidden fields, 
        //therefore the hidden field with the loaded data needs to be encoded or cleared.
        $('#hidDealTags').val('');
    }


    var isAddNewProductClick = false;
    function btnNewProductClick() {
        prePostbackEncoding();
        tagManager.saveToHiddenField('hidDealTags');
        updateOrder();
        if ($('.classProductName').val().length == 0) {
            showErrorMsg("商品名稱不得為空");
            return false;
        }
        if (isAddNewProductClick) {
            showErrorMsg("商品新增中請稍候...");
            return false;
        }
        isAddNewProductClick = true;
        return true;
    }

    function showErrorMsg(message) {
        $.blockUI({
            message: "<div class='addproduct' ><div class='addproduct-Top'><div class='addproduct-X-button' onclick='$.unblockUI();' style='cursor: pointer'></div></div><h3>" + message + "</h3></div>",
            css: { border: 'none', background: 'transparent', width: '0px', height: '0px', cursor: 'default', top: ($(window).height() - 350) / 2 + 'px', left: ($(window).width() - 700) / 2 + 'px' }
        });

    }

    <%--
    //////////////////////////////////////////////////tag implementation////////////////////////////////////////////////////////////////////
    /*
    <Tags>
        <ProdDesc title="產品介紹">這個產品的介紹</ProdDesc>
        <OptTag title="" isNotVisible="true">title不可為空白的選擇性頁籤內容</OptTag>
        <OptTag title="" isNotVisible="false">title不可為空白的選擇性頁籤內容</OptTag>
        <TermsOfUse>權益說明</TermsOfUse>
    </Tags>
    */ 
    --%>
        var tagManager = {
            optTagCount: 0,
            optIdUsage: { id1: true, id2: false, id3: false, id4: false, id5: true },  //1: 產品介紹; 5: 權益說明
            changeTitle: function (idName) {
                var newTitle = prompt('請輸入新標題', $('#divDealTabs ul.tagIndex a[href="#dealTabs-' + idName + '"]').text());
                if (newTitle == null) {
                    return;
                } else {
                    if (newTitle.length == 0) {
                        this.changeTitle();
                        return;
                    }
                }
                $('#divDealTabs ul.tagIndex a[href="#dealTabs-' + idName + '"]').text(newTitle);
            },
            newTag: function (title) {
                var that = this;
                var idName = that.getAvailableId();
                if (idName === null)
                    return;

                that.setUsedId(idName);

                var btnStr = "<button onclick= 'tagManager.changeTitle(\"" + idName + "\"); return false;' style='margin-right: 8px;'>改變標題</button>";
                var chkStr = "<input type='checkbox' id='" + idName + "Chk' style='margin-left: 8px;'/><label for='" + idName + "Chk'>隱藏</label>";
                var contentStr = "<textarea id='" + idName + "Content' style='clear:both;' class='ckedt'></textarea>";
                var divTag = $('<div id="dealTabs-' + idName + '" class="divTagArea"/>').html("<div style='margin-bottom:10px;'>" + btnStr + chkStr + "</div>" + contentStr);
                divTag.appendTo('#divDealTabs');

                var imageupload = '../../Tools/ckeditor/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images&did=' + getDid();
                CKEDITOR.replace(idName + 'Content', {
                    filebrowserImageUploadUrl: imageupload,
                    customConfig: '../../Tools/ckeditor/config_Hi.js'
                });

                var idx;
                if (idName == 'id2')
                    idx = 1;
                if (idName == 'id3')
                    idx = 2;
                if (idName == 'id4')
                    idx = 3;

                $('#divDealTabs').tabs("add", "#dealTabs-" + idName, title, idx);
            },
            loadTagContents: function (hidVal) {
                // iv. html decode?
                ////////////////////////properties////////////////////////////////////////////////////////////
                var that = this;
                var loadTag = function (title, content, isNotVisible, isProdDesc, isTermsOfUse) {
                    var idName;
                    if (isProdDesc) {
                        idName = 'id1';
                    } else if (isTermsOfUse) {
                        idName = 'id5';
                    } else {
                        idName = that.getAvailableId();
                        that.setUsedId(idName);
                    }

                    //add li (link for jquery tabs)
                    var liStr = "<a href='#dealTabs-" + idName + "'>" + title + "</a>";
                    var liEle = $(document.createElement('li')).html(liStr);
                    if (isProdDesc || isTermsOfUse)
                        $(liEle).addClass('fixPosition');
                    liEle.appendTo('#divDealTabs ul.tagIndex');

                    //add div
                    var btnStr = "";  //change title button
                    if (!isTermsOfUse)
                        btnStr = "<button onclick= 'tagManager.changeTitle(\"" + idName + "\"); return false;' style='margin-right: 8px;'>改變標題</button>";

                    var chkStr = "";  //"隱藏" checkbox     **isVisible = 顯示 => 隱藏不可勾
                    if (!isTermsOfUse && !isProdDesc) {
                        if (isNotVisible) {
                            chkStr = "<input type='checkbox' id='" + idName + "Chk' checked='checked' style='margin-left: 8px;'/><label for='" + idName + "Chk'>隱藏</label>";
                        }
                        else {
                            chkStr = "<input type='checkbox' id='" + idName + "Chk' style='margin-left: 8px;'/><label for='" + idName + "Chk'>隱藏</label>";
                        }
                    }

                    //內容
                    var contentStr = "<textarea id='" + idName + "Content' style='clear:both;' class='ckedt'>" + content + "</textarea>";
                    var divTag = $('<div id="dealTabs-' + idName + '" class="divTagArea"/>').html("<div style='margin-bottom:10px;'>" + btnStr + chkStr + "</div>" + contentStr);

                    divTag.appendTo('#divDealTabs');

                    var imageupload = '../../Tools/ckeditor/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images&did=' + getDid();
                    CKEDITOR.replace(idName + 'Content', {
                        filebrowserImageUploadUrl: imageupload,
                        customConfig: '../../Tools/ckeditor/config_Hi.js'
                    });
                };
                ////////////////////////properties end////////////////////////////////////////////////////////////
                //decode
                //$('#< %=tbTag1Context.ClientID%>').text($('<div/>').html($('#< %=tbTag1Context.ClientID%>').text()).text());
                var str = $('<div/>').html(hidVal).text();
                str = str.replace(/\[/g, '<');
                str = str.replace(/\]/g, '>');
                str = str.replace(/\{91}/g, '[');
                str = str.replace(/\{93}/g, ']');
                var xmlDoc = $.parseXML(str);
                var root = xmlDoc.getElementsByTagName('Tags')[0];

                //產品介紹頁籤
                var prodDescTag = root.getElementsByTagName('ProdDesc')[0];
                var prodTitle = prodDescTag.getAttribute('title');
                var prodContent = prodDescTag.firstChild === null ? '' : prodDescTag.firstChild.nodeValue;
                loadTag(prodTitle, prodContent, true, true, false);

                //其他頁籤
                var optTags = root.getElementsByTagName('OptTag');
                if (optTags.length > 0) {
                    $(optTags).each(function (index, tag) {
                        var title = tag.getAttribute('title');
                        var isVisible = tag.getAttribute('isNotVisible') === 'true';
                        var content = tag.firstChild === null ? '' : tag.firstChild.nodeValue;
                        loadTag(title, content, isVisible, false, false);
                    });
                }

                //權益說明頁籤
                var termsOfUseTag = root.getElementsByTagName('TermsOfUse')[0];
                var temrsOfUseContent = termsOfUseTag.firstChild === null ? '' : termsOfUseTag.firstChild.nodeValue;
                loadTag('權益說明', temrsOfUseContent, true, false, true);

                $('#divDealTabs').tabs().find('.ui-tabs-nav').sortable({ items: 'li:not(.fixPosition)' });

                //新增頁籤用的按鈕
                var inputEle = $(document.createElement('input')).html('');
                $(inputEle).attr('type', 'button');
                $(inputEle).attr('id', 'btnAddTag');
                $(inputEle).attr('value', '+');
                $(inputEle).attr('style', 'margin-top:7px;');
                $(inputEle).click(function () {
                    if (tagManager.getAvailableId() !== null) {
                        var newTitle = prompt('請輸入標題:', '');
                        if (newTitle !== null && newTitle !== '') { tagManager.newTag(newTitle); }
                    }
                });
                inputEle.appendTo('#divDealTabs ul.tagIndex');
            },

            getAvailableId: function () {
                if (this.optIdUsage['id2'] == false) {
                    return 'id2';
                }
                if (this.optIdUsage['id3'] == false) {
                    return 'id3';
                }
                if (this.optIdUsage['id4'] == false) {
                    return 'id4';
                }
                return null;
            },

            setUsedId: function (idName) {
                this.optIdUsage[idName] = true;
            },

            saveToHiddenField: function (hiddenFieldId) {
                var itemsCount = $('#divDealTabs ul li').length;

                //set ckeditor value into textarea
                $('textarea.ckedt').each(function (index, txt) {
                    $(txt).text(CKEDITOR.instances[$(txt).attr('id')].getData());
                    var str = $(txt).val();
                    str = $('<div/>').text(str).html();  //html encode
                    str = str.replace(/\[/g, '{91}');
                    str = str.replace(/]/g, '{93}');
                    str = str.replace(/</g, '[');
                    str = str.replace(/>/g, ']');
                    $(txt).text(str);
                });

                var xml = '<Tags></Tags>';
                var xmlDoc = $.parseXML(xml);
                var root = xmlDoc.getElementsByTagName('Tags')[0];

                //prod desc tag
                var prodDescTitle = $('#divDealTabs ul.tagIndex li').first().find('a', this).text();
                var prodDescContent = $('#divDealTabs div.divTagArea:eq(0)').find('textarea', this).text();
                //if double HtmlEncode then HtmlDecode again.
                if (prodDescContent.indexOf('&amp;lt;') > -1) {
                    prodDescContent = HTMLDecode(prodDescContent);
                }
                var prodDescElem = xmlDoc.createElement('ProdDesc');
                prodDescElem.setAttribute('title', prodDescTitle);
                var prodDescText = xmlDoc.createTextNode(prodDescContent);
                prodDescElem.appendChild(prodDescText);
                root.appendChild(prodDescElem);

                //other tags
                if (itemsCount > 2) {
                    $('#divDealTabs ul.tagIndex li').each(function (index, li) {
                        if (index == itemsCount - 1 || index == 0)
                            return;
                        var title = $(li).find('a', this).text();
                        divId = $(li).find('a', this).attr('href');
                        var content = $('#divDealTabs div.#' + divId).find('textarea', this).text();
                        var isNotVisible = $('#divDealTabs div.#' + divId).find('input[type="checkbox"]', this).is(':checked');
                        var tagElem = xmlDoc.createElement('OptTag');
                        tagElem.setAttribute('title', title);
                        tagElem.setAttribute('isNotVisible', isNotVisible ? "true" : "false");
                        var tagText = xmlDoc.createTextNode(content);
                        tagElem.appendChild(tagText);
                        root.appendChild(tagElem);
                    });
                }
                //terms of use tag
                var termsOfUseContent = $('#divDealTabs div.divTagArea:eq(' + (itemsCount - 1) + ')').find('textarea', this).text();
                var termsOfUseElem = xmlDoc.createElement('TermsOfUse');
                var termsOfUseText = xmlDoc.createTextNode(termsOfUseContent);
                termsOfUseElem.appendChild(termsOfUseText);
                root.appendChild(termsOfUseElem);

                //encoding?
                var str = (new XMLSerializer()).serializeToString(xmlDoc);
                str = str.replace(/</g, '[');
                str = str.replace(/>/g, ']');
                $('#' + hiddenFieldId).val(str);
            }
        };
        //////////////////////////////////////////////////////end of tagManager object////////////////////////////////////////////////////////////////////
        function checkInput() {
            var result = true;
            var errMsg = '';

            if ($('input:checked', '#cblCategories').size() == 0) {
                errMsg += '"類別"至少要要設定一個!\r\n';
                result = false;
            }
            var hd = $('#<%=hdSalesCatg2Id.ClientID%>');
            if (hd.val() == '' || hd.val() == '0')
            {
                errMsg += "請選擇銷售分析裡的分類!\r\n";
                result = false;
            }

            if (!result)
                alert(errMsg);

            return result;
        }
        function HTMLDecode(input) {
            var converter = document.createElement("DIV");
            converter.innerHTML = input;
            var output = converter.innerText;
            converter = null;
            return output;
        }

        function SetSalesCatg2()
        {
            var ddlMain = $("#<%=ddlSalesCatg1.ClientID%>");
            var ddlSub = $("#<%=ddlSalesCatg2.ClientID%>");

            $.ajax({
                type: "POST",
                url: "setup.aspx/GetSalesCatg2",
                data: "{parentId: " + ddlMain.val() + "}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    ddlSub.empty();
                    ddlSub.append($('<option></option>').val('0').html('請選擇'));
                    $.each(cates, function (i, item) {
                        ddlSub.append($('<option></option>').val(item.CodeId).html(item.CodeName));
                    })
                    $('#<%=hdSalesCatg2Id.ClientID%>').val('');
                    $('#<%=hdSalesCatg2Name.ClientID%>').val('');
                },
                error: function () {
                    ddlSub.empty();
                    ddlSub.append($('<option></option>').val('0').html('請選擇'));
                }
            });
        }

        function SetSalesCatg2InHidden()
        {
            $("#<%=hdSalesCatg2Id.ClientID%>").val($("#<%=ddlSalesCatg2.ClientID%>").val());
            $("#<%=hdSalesCatg2Name.ClientID%>").val($("#<%=ddlSalesCatg2.ClientID%> option:selected").text());
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="position: absolute; width: 84%; right: 1%;">
        <asp:UpdatePanel runat="server" ID="couponQuantityNotMatchUpdatePanel">
            <ContentTemplate>
                <asp:Panel ID="couponQuantityNotMatchPanel" runat="server" Visible="true">
                    <asp:Label ID="tbCouponQuantityNotMatchErrorMessage" runat="server" Text=""
                        ForeColor="Red" Font-Bold="True"></asp:Label>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnGenCoupon" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">檔次設定</a></li>
                <li><a href="#tabs-2" class="picUpload">圖片上傳</a></li>
                <li><a href="#tabs-3">編輯資訊</a></li>
                <li><a href="#tabs-4">銷售分析</a></li>
                <li><a href="#tabs-5">憑證資訊</a></li>
                <%--<li style="float: right;"><a class="funcMenu">功能表</a></li>--%>
            </ul>
            <%--<asp:HiddenField ID="hidCurrentTab" runat="server" />--%>
            <%-- 檔次設定 --%>
            <div id="tabs-1">
                <%-- 基本設定 --%>
                <fieldset class="ui-corner-all">
                    <legend>基本設定</legend>
                    <p>
                        <asp:Label ID="Label1" runat="server" CssClass="mainLabel"><% =Resources.Localization.HiDealId%>：</asp:Label>
                        <asp:Label ID="lbDealId" runat="server" Text=""></asp:Label>
                    </p>
                    <p>
                        <asp:Label ID="Label2" runat="server" AssociatedControlID="tbDealName" CssClass="mainLabel">主標：</asp:Label>
                        <asp:TextBox ID="tbDealName" runat="server" Width="40%" MaxLength="50" wordCountSpan="spanDealName" />
                        <span id="spanDealName" style="display: none;">(理想字數20個中文字)尚餘可輸入字數: <span style="color: Red">
                        </span></span>
                    </p>
                    <p>
                        <asp:Label ID="Label3" runat="server" AssociatedControlID="tbPromoName" CssClass="mainLabel">行銷標：</asp:Label>
                        <asp:TextBox ID="tbPromoName" runat="server" Width="40%" MaxLength="44" wordCountSpan="spanPromoName" />
                        <span id="spanPromoName" style="display: none;">(理想字數40個中文字)尚餘可輸入字數: <span style="color: Red">
                        </span></span>
                    </p>
                    <p>
                        <asp:Label ID="Label4" runat="server" AssociatedControlID="tbPromoShortName" CssClass="mainLabel">行銷短標：</asp:Label>
                        <asp:TextBox ID="tbPromoShortName" runat="server" Width="40%" MaxLength="18" wordCountSpan="spanPromoShortName" />
                        <span id="spanPromoShortName" style="display: none;">(理想字數15個中文字)尚餘可輸入字數: <span style="color: Red">
                        </span></span>
                    </p>
                    <!--
                    <p>
                        <asp:Label runat="server" AssociatedControlID="tbEdmTitle" CssClass="mainLabel">EDM標：</asp:Label>
                        <asp:TextBox ID="tbEdmTitle" runat="server" Width="40%" />
                    </p>
                    <p>
                        <asp:Label runat="server" AssociatedControlID="tbEdmSubject" CssClass="mainLabel">EDM主旨：</asp:Label>
                        <asp:TextBox ID="tbEdmSubject" runat="server" Width="40%" />
                    </p>
                    -->
                    <p>
                        <asp:Label ID="Label5" runat="server" AssociatedControlID="tbPromoShortName" CssClass="mainLabel">區域：</asp:Label>
                        <asp:CheckBoxList ID="cblCities" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal"
                            CssClass="chkListClass">
                        </asp:CheckBoxList>
                    </p>
                    <p class="basicSetting_Category">
                        <asp:Label ID="Label6" runat="server" AssociatedControlID="tbPromoShortName" CssClass="mainLabel">類別：</asp:Label>
                        <asp:CheckBoxList ID="cblCategories" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal"
                            CssClass="chkListClass, chkListCatg" ClientIDMode="Static">
                        </asp:CheckBoxList>
                    </p>
                    <p class="basicSetting_Discount">
                        <asp:Label ID="Label7" runat="server" AssociatedControlID="tbPromoShortName" CssClass="mainLabel">優惠設定：</asp:Label>
                        <asp:DropDownList ID="ddlSpecialDiscounts" runat="server" />
                    </p>
                    <p>
                        <asp:Label ID="Label8" runat="server" AssociatedControlID="tbPromoShortName" CssClass="mainLabel">顯示狀態：</asp:Label>
                        <asp:DropDownList ID="ddlIsOpen" runat="server" CssClass="optionItem">
                            <asp:ListItem Value="true" Selected="true">上線</asp:ListItem>
                            <asp:ListItem Value="false">下線</asp:ListItem>
                        </asp:DropDownList>
                        <asp:CheckBox ID="cbAlwaysMain" runat="server" CssClass="inlineOption" />
                        <asp:Label ID="Label9" runat="server" AssociatedControlID="cbAlwaysMain">永遠設為主要檔次</asp:Label>
                    </p>
                </fieldset>
                <%-- 時程設定 --%>
                <fieldset class="ui-corner-all">
                    <legend>時程設定</legend>
                    <p>
                        <asp:Label ID="Label10" runat="server" CssClass="mainLabel">購買時間：</asp:Label>
                        <asp:TextBox ID="tbDealStartDate" runat="server" CssClass="datePicker" Width="120px" />
                        <asp:TextBox ID="tbDealStartTime" runat="server" CssClass="timePicker" Width="60px">12:00</asp:TextBox>
                        <span style="margin-left: 4px; margin-right: 4px;">~</span>
                        <asp:TextBox ID="tbDealEndDate" runat="server" CssClass="datePicker" Width="120px" />
                        <asp:TextBox ID="tbDealEndTime" runat="server" CssClass="timePicker" Width="60px">12:00</asp:TextBox>
                    </p>
                </fieldset>
                <%-- 商品設定 --%>
                <div runat="server" id="divProduct">
                    <fieldset class="ui-corner-all">
                        <legend>商品設定</legend>
                        <asp:UpdatePanel runat="server" ID="UpdatePanel2">
                            <ContentTemplate>
                                <asp:Label ID="Label11" runat="server" CssClass="mainLabel" AssociatedControlID="tbProductName">商品名稱：</asp:Label>
                                <asp:TextBox ID="tbProductName" runat="server" MaxLength="14" CssClass="classProductName"
                                    wordCountSpan="spanProductName" />
                                <asp:Button runat="server" ID="btnNewProduct" Text="新增" OnClientClick="if(!btnNewProductClick()){ return false;}"
                                    OnClick="BtnNewProduct_Click" />
                                <span id="spanProductName" style="display: none;">尚餘可輸入字數: <span style="color: Red">
                                </span></span>
                                <asp:HiddenField runat="server" ID="hidProducts" />
                                <asp:ListView runat="server" ID="lvProducts" OnItemDataBound="lvProducts_OnItemDataBound">
                                    <LayoutTemplate>
                                        <table class="tblProducts">
                                            <thead>
                                                <th>
                                                    狀態
                                                </th>
                                                <th>
                                                    排序
                                                </th>
                                                <th>
                                                    商品名稱
                                                </th>
                                            </thead>
                                            <tbody>
                                                <tr runat="server" id="itemPlaceholder">
                                                </tr>
                                            </tbody>
                                        </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <asp:HiddenField ID="hidProdId" Value='<%# Eval("Id")%>' runat="server" />
                                            <td style="text-align: center;">
                                                <asp:Literal ID="litProductIsShow" runat="server" Text='<%# bool.Parse(Eval("IsShow").ToString()) ? "顯示" : "隱藏"%>' />
                                            </td>
                                            <td style="text-align: center;">
                                                <asp:Literal ID="litProductSeq" runat="server" Text='<%# Eval("Seq") %>' />
                                            </td>
                                            <td style="text-align: right;" class="cellProdName">
                                                <asp:HiddenField ID="hidProdName" Value='<%# Eval("Name")%>' runat="server" />
                                                <span class="linklessProdName">
                                                    <asp:Literal runat="server" ID="litProdName" Text='<%# Eval("Name")%>' /></span>
                                                <asp:HyperLink runat="server" ID="linkProdName" Text='<%# Eval("Name")%>' CssClass="linkProdName" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <EmptyDataTemplate>
                                    </EmptyDataTemplate>
                                </asp:ListView>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnNewProduct" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </fieldset>
                </div>
            </div>
            <%-- 圖片上傳 --%>
            <div id="tabs-2">
                <%-- 用來判斷是否上傳圖片造成postback (上傳圖片無法使用updatepanel => postback後要回到圖片上傳的頁籤)--%>
                <asp:HiddenField runat="server" ID="hidBigPicUpload" Value="false" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="hidSmallPicUpload" Value="false" ClientIDMode="Static" />
                <fieldset class="ui-corner-all">
                    <legend>上傳圖片</legend>
                    <div>
                        <asp:Label ID="Label12" runat="server" CssClass="mainLabel">1. 首頁大圖</asp:Label><span style="color: gray;">（700
                            x 280）</span>
                        <br />
                        <asp:FileUpload ID="fileUpload_BigPic" runat="server" />
                        <asp:Button ID="btnBigPicUpload" runat="server" OnClick="BtnBigPicUpload_Click" OnClientClick="prePostbackEncoding();tagManager.saveToHiddenField('hidDealTags');"
                            Text="上傳" />
                        <div>
                            <asp:PlaceHolder ID="placeHolder_BigPicture" runat="server"></asp:PlaceHolder>
                        </div>
                    </div>
                    <br />
                    <hr />
                    <br />
                    <div>
                        <a name="smallPicUpload"></a>
                        <asp:Label ID="Label13" runat="server" CssClass="mainLabel">2. 商品圖</asp:Label>
                        <span style="color: gray;">（650 x 360）</span>
                        <br />
                        <asp:FileUpload ID="fileUpload_SmallPic" runat="server" multiple="true" />
                        <asp:Button ID="btnSmallPicUpload" runat="server" OnClick="BtnSmallPicUpload_Click"
                            OnClientClick="prePostbackEncoding();tagManager.saveToHiddenField('hidDealTags');"
                            Text="上傳" />
                        <asp:HiddenField ID="hidSelectedPicUrl" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hidSmallPicSequence" runat="server" ClientIDMode="Static" />
                        <asp:ListView ID="lvSmallPics" runat="server" OnItemCommand="lvSmallPics_ItemCommand">
                            <LayoutTemplate>
                                <div id="divSmallPictures">
                                    <ul>
                                        <li runat="server" id="itemPlaceholder"></li>
                                    </ul>
                                </div>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <li>
                                    <%# Container.DataItemIndex + 1%>.&nbsp;
                                    <input type="radio" name="gpSmallPics" /><label style="margin-right: 10px;">設為首頁</label>
                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("Url") %>' />
                                    <asp:Button runat="server" ID="btnDelete" OnClientClick="prePostbackEncoding();tagManager.saveToHiddenField('hidDealTags');"
                                        Text="刪除" CommandName="Del" CommandArgument='<%# Eval("Url") %>' />
                                    <br />
                                    <asp:Image runat="server" ID="img" ImageUrl='<%# Eval("Url") %>' Width="150px" Height="83px" />
                                </li>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                                <div id="divSmallPictures">
                                </div>
                            </EmptyDataTemplate>
                        </asp:ListView>
                    </div>
                </fieldset>
            </div>
            <%-- 編輯資訊 --%>
            <div id="tabs-3">
                <%-- 位置資訊 --%>
                <fieldset class="ui-corner-all">
                    <legend>位置資訊</legend>
                    <asp:HiddenField runat="server" ID="hidOrderedStoreValues" />
                    <asp:ListView runat="server" ID="lvStore">
                        <LayoutTemplate>
                            <table class="tblStoreSettings">
                                <thead>
                                    <tr>
                                        <th>
                                            排序
                                        </th>
                                        <th>
                                            分店
                                        </th>
                                        <th>
                                            兌換時間
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr runat="server" id="itemPlaceholder" />
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="text-align: center;">
                                    <asp:HiddenField runat="server" ID="hidStoreGuid" Value='<%# Eval("StoreGuid")  %>' />
                                    <span>
                                        <asp:Literal runat="server" ID="litSeq" Text='<%# Eval("Sequence")  %>' /></span>
                                </td>
                                <td>
                                    <input type="checkbox" runat="server" id="chkStore" checked='<%# string.IsNullOrEmpty(Eval("Sequence").ToString()) ? bool.Parse("false") : bool.Parse("true") %>'
                                        class="storeCheck" />
                                    <asp:Label ID="Label14" runat="server" AssociatedControlID="chkStore" />
                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("StoreName").ToString() + Eval("StoreAddress").ToString()  %>' />
                                </td>
                                <td style="width: 480px">
                                    <asp:TextBox ID="txtUseTime" runat="server" TextMode="MultiLine" Height="50px" Width="460px" Text='<%# Eval("UseTime").ToString()  %>'></asp:TextBox>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </fieldset>
                <%-- 頁籤設定 --%>
                <div runat="server" id="divTagTabsArea">
                    <fieldset class="ui-corner-all">
                        <legend>頁籤內容</legend>
                        <asp:HiddenField runat="server" ID="hidDealTags" ClientIDMode="Static" />
                        <div id="divDealTabs">
                            <ul class="tagIndex">
                            </ul>
                        </div>
                    </fieldset>
                </div>
                <%--預覽--%>
                <fieldset class="ui-corner-all">
                    <legend>預覽</legend>
                    <input type="button" value="商品頁預覽" <%=string.Format("onclick=\"window.open('{0}');\"", VirtualPathUtility.ToAbsolute(string.Format("~/piinlife/default.aspx?dealgid={0}&mode=preview", DealGuid ))) %> />
                </fieldset>
            </div>
            <%-- 銷售分析 --%>
            <div id="tabs-4">
                <%-- 核銷方式 --%>
                <fieldset class="ui-corner-all">
                    <legend>核銷方式</legend>
                    <p>
                        <asp:RadioButton ID="rbVerifyByList" runat="server" Text="清冊" GroupName="verifyMethodGroup" />
                        <asp:RadioButton ID="rbVerifyByPad" runat="server" Text="線上" GroupName="verifyMethodGroup" 
                            Style="padding-left: 20px;" />
                    </p>
                </fieldset>
                <%-- 銷售分析 --%>
                <fieldset class="ui-corner-all">
                    <legend>銷售分析</legend>
                    <div class="divGroupItem">
                        <asp:Label ID="Label15" runat="server" CssClass="mainLabel">業務：</asp:Label>
                        <asp:TextBox runat="server" ID="tbSalesName" />
                    </div>
                    <div class="divGroupItem">
                        <asp:Label ID="Label16" runat="server" CssClass="mainLabel">館別：</asp:Label>
                        <asp:DropDownList runat="server" ID="ddlSalesAccBU">
                        </asp:DropDownList>
                    </div>
                    <!--<div>-->
                    <%--<asp:Label runat="server" CssClass="mainLabel" >業績歸屬部門：</asp:Label>--%>
                    <asp:DropDownList runat="server" ID="ddlSalesDepartment" Enabled="False" Visible="False">
                    </asp:DropDownList>
                    <!--</div>
            <div class="divGroupItem">-->
                    <%--<asp:Label runat="server" CssClass="mainLabel" >業績歸屬城市：</asp:Label>--%>
                    <asp:DropDownList runat="server" ID="ddlSalesAccCity" Enabled="False" Visible="False">
                    </asp:DropDownList>
                    <!--</div>-->
                    <div class="divGroupItem">
                        <asp:Label ID="Label17" runat="server" CssClass="mainLabel">分類：</asp:Label>
                        <asp:DropDownList runat="server" ID="ddlSalesCatg1" onchange="SetSalesCatg2()"></asp:DropDownList>&nbsp;&nbsp;
                        <asp:DropDownList runat="server" ID="ddlSalesCatg2" onchange="SetSalesCatg2InHidden()"></asp:DropDownList> <asp:HiddenField ID="hdSalesCatg2Id" runat="server" /><asp:HiddenField ID="hdSalesCatg2Name" runat="server" />
                    </div>
                </fieldset>
            </div>
            <%-- 憑證資訊 --%>
            <div id="tabs-5">
                <%-- 商品憑證資訊 --%>
                <asp:UpdatePanel runat="server" ID="UpdatePanel_divProductCouponCount">
                    <ContentTemplate>
                        <div id="divProductCouponCount">
                            <fieldset class="ui-corner-all">
                                <legend>商品憑證資訊</legend>
                                <asp:ListView runat="server" ID="lvProductCounponCount">
                                    <LayoutTemplate>
                                        <table class="tblStoreCoupons">
                                            <thead>
                                                <th>
                                                    檔次名
                                                </th>
                                                <th>
                                                    商品名
                                                </th>
                                                <th>
                                                    分店名
                                                </th>
                                                <th>
                                                    可用憑證數
                                                </th>
                                                <th>
                                                    設定總數
                                                </th>
                                                <th>
                                                    申請退貨未完成數
                                                </th>
                                            </thead>
                                            <tbody>
                                                <tr runat="server" id="itemPlaceholder">
                                                </tr>
                                            </tbody>
                                        </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="text-align: center;">
                                                <asp:Literal ID="liDealName" runat="server" Text='<%# Eval("DealName") %>' />
                                            </td>
                                            <td style="text-align: center;">
                                                <asp:Literal ID="liProductName" runat="server" Text='<%# Eval("ProductName") %>' />
                                            </td>
                                            <td style="text-align: center;">
                                                <asp:Literal ID="liStoreName" runat="server" Text='<%# replaceHiDealCouponStoreName(Eval("StoreName")) %>' />
                                            </td>
                                            <td style="text-align: center;">
                                                <asp:Literal ID="liGenerateCouponAmount" runat="server" Text='<%# Eval("CouponAmount") %>' />
                                            </td>
                                            <td style="text-align: center;">
                                                <asp:Literal ID="LiCouponAmountSetting" runat="server" Text='<%# Eval("CouponAmountSetting") %>' />
                                            </td>
                                            <td style="text-align: center;">
                                                <asp:Literal ID="LiRefundProcessingCouponAmount" runat="server" Text='<%# Eval("RefundProcessingCouponAmount") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <EmptyDataTemplate>
                                        <table class="tblStoreCoupons">
                                            <thead>
                                                <th>
                                                    檔次名
                                                </th>
                                                <th>
                                                    商品名
                                                </th>
                                                <th>
                                                    分店名
                                                </th>
                                                <th>
                                                    可用憑證數
                                                </th>
                                                <th>
                                                    設定總數
                                                </th>
                                                <th>
                                                    申請退貨未完成數
                                                </th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="6" align="center">
                                                        <span style="color: Red">該檔次尚未產生憑證資訊</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </EmptyDataTemplate>
                                </asp:ListView>
                            </fieldset>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnGenCoupon" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <asp:Button ID="btnSave" runat="server" OnClientClick="if(!checkInput()) {return false;}; prePostbackEncoding(); updateOrder(); updateOrderedStoreValues(); checkSecodaryPictureIsSelected();updateSelectedSecondaryPictureUrl();tagManager.saveToHiddenField('hidDealTags');updatePictureSequence();"
            OnClick="BtnSave_Click" Text="Save" />
        <asp:Button ID="btnGenCoupon" runat="server" OnClientClick="prePostbackEncoding();tagManager.saveToHiddenField('hidDealTags');"
            OnClick="OnGenCoupon" Text="產生憑證" />
        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tbDealStartDate" ErrorMessage="請提供檔次購買的開始日期"></asp:RequiredFieldValidator>--%>
        <asp:Literal runat="server" ID="scriptMessageContainer"></asp:Literal>
</asp:Content>

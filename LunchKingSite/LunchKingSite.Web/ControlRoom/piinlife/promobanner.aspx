﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="~/ControlRoom/System/randomcms.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.randomcms" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="HTMLEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>
    <script src="//www.google.com/jsapi" type="text/javascript"></script>
    <script type="text/javascript">
        google.load("jquery", "1.4.2");
        google.load("jqueryui", "1.8");

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .head
        {
            text-align: left;
            font-weight: bolder;
            background-color: #F0AF13;
            color: white;
        }
        .innerhead
        {
            background-color: #688E45;
            font-weight: bolder;
            color: White;
        }
    </style>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <cc1:Accordion ID="MyAccordion" runat="Server" SelectedIndex="0" HeaderCssClass="accordionHeader"
                HeaderSelectedCssClass="accordionHeaderSelected" ContentCssClass="accordionContent"
                AutoSize="None" FadeTransitions="true" TransitionDuration="100" RequireOpenedPane="true"
                SuppressHeaderPostbacks="true">
                <Panes>
                    <cc1:AccordionPane runat="server" ID="apn_timeline">
                        <Header>
                            TimeSlot
                        </Header>
                        <Content>
                            <asp:Label ID="lab_timeslot" runat="server"></asp:Label>
                        </Content>
                    </cc1:AccordionPane>
                </Panes>
                <Panes>
                    <cc1:AccordionPane runat="server" ID="apn_Eidt">
                        <Header>
                            Banner
                        </Header>
                        <Content>
                            <table style="width: 970px">
                                <tr>
                                    <td class="head">
                                        標題
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbx_Title" runat="server"></asp:TextBox>
                                    </td>
                                    <td class="head">
                                        Banner
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddl_Target" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="head">
                                        狀態
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="cbx_ContentStatus" runat="server" Checked="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" class="head">
                                        內容
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <HTMLEditor:Editor runat="server" Width="970" Height="300" AutoFocus="true" ID="editor_Html" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <asp:Button ID="btn_Save" runat="server" Text="新增" OnClick="ClickSaveCmsRandomContent" />
                                        <asp:Button ID="btn_Cancel" runat="server" Text="取消" ForeColor="Red" OnClick="CancelAction" />
                                        &nbsp;&nbsp;
                                        <asp:Button ID="btn_Delete" runat="server" Text="刪除" ForeColor="Red" OnClick="ClickSaveCmsRandomContent"
                                            OnClientClick="return confirm('確定要刪除?')" Visible="false" />
                                        <asp:HiddenField ID="hif_ID" runat="server" />
                                        <asp:HiddenField ID="hif_Mode" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </Content>
                    </cc1:AccordionPane>
                </Panes>
                <Panes>
                    <cc1:AccordionPane runat="server" ID="apn_City">
                        <Header>
                            City
                        </Header>
                        <Content>
                            <table style="width: 970px">
                                <tr>
                                    <td class="head" style="width: 25%">
                                        Banner
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddl_Target2" runat="server" OnSelectedIndexChanged="TargetChange"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="head" style="width: 25%">
                                        標題
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddl_Banners" runat="server" OnSelectedIndexChanged="BannerChangeGetContent"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lab_Content" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="head">
                                        城市
                                    </td>
                                    <td colspan="3">
                                        <asp:Label ID="lbl_City" runat="server" Visible="false"></asp:Label>
                                        <asp:CheckBoxList ID="cbx_City" runat="server" RepeatDirection="Horizontal" Font-Size="Smaller">
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="head">
                                        加權
                                    </td>
                                    <td colspan="3">
                                        <asp:CheckBox ID="cbx_CityPriorty" runat="server" Visible="false" />
                                        <asp:CheckBox ID="cbx_CityStatus" runat="server" Text="狀態" Visible="false" />
                                        <asp:CheckBoxList ID="cbx_Priority" runat="server" RepeatDirection="Horizontal" Font-Size="Smaller">
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="head">
                                        輪播區間
                                    </td>
                                    <td colspan="3">
                                        <cc1:CalendarExtender ID="ce_Start" TargetControlID="tbx_StartTime" runat="server"
                                            Format="yyyy/MM/dd">
                                        </cc1:CalendarExtender>
                                        <asp:TextBox ID="tbx_StartTime" runat="server"></asp:TextBox>&nbsp;
                                        <asp:DropDownList ID="ddl_StartHour" runat="server">
                                        </asp:DropDownList>
                                        :
                                        <asp:DropDownList ID="ddl_StartMinute" runat="server">
                                        </asp:DropDownList>
                                        ~
                                        <cc1:CalendarExtender ID="ce_End" TargetControlID="tbx_EndTime" runat="server" Format="yyyy/MM/dd">
                                        </cc1:CalendarExtender>
                                        <asp:TextBox ID="tbx_EndTime" runat="server"></asp:TextBox>&nbsp;
                                        <asp:DropDownList ID="ddl_EndHour" runat="server">
                                        </asp:DropDownList>
                                        :
                                        <asp:DropDownList ID="ddl_EndMinute" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Button ID="btn_Save2" runat="server" Text="新增" OnClick="ClickSaveCmsRandomCity" />
                                        <asp:Button ID="btn_EditCity" runat="server" Text="更新" OnClick="UpdateCity" Visible="false" />
                                        <asp:Button ID="btn_Cancel2" runat="server" Text="取消" ForeColor="Red" OnClick="CancelAction2" />
                                    </td>
                                </tr>
                            </table>
                        </Content>
                    </cc1:AccordionPane>
                </Panes>
                <Panes>
                    <cc1:AccordionPane runat="server" ID="apn_view">
                        <Header>
                            List
                        </Header>
                        <Content>
                            <table style="width: 970px;">
                                <tr>
                                    <td class="head">
                                        Banner
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddl_TargetView" runat="server" OnSelectedIndexChanged="TargetChange"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="head">
                                        標題
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddl_BannersView" runat="server" OnSelectedIndexChanged="BannerChangeGetContent"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="head">
                                        狀態
                                    </td>
                                    <td>
                                        <asp:HiddenField ID="hif_ContentId" runat="server" />
                                        <asp:CheckBox ID="cbx_Status" runat="server" Enabled="false" />
                                    </td>
                                    <td class="head">
                                        編輯
                                    </td>
                                    <td>
                                        <asp:Button ID="btn_EditCmsContent" runat="server" Text="修改" OnClick="EditCmsContent" />
                                        &nbsp;
                                        <asp:Button ID="btn_ClearCache" runat="server" Text="清除Cache" OnClick="ClearCache" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Label ID="lbl_Body" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:GridView ID="gv_ViewData" runat="server" AutoGenerateColumns="false" OnRowCommand="gv_Command">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table style="width: 970px">
                                                            <tr>
                                                                <td class="innerhead">
                                                                    修改
                                                                </td>
                                                                <td class="innerhead">
                                                                    狀態
                                                                </td>
                                                                <td class="innerhead">
                                                                    城市
                                                                </td>
                                                                <td class="innerhead">
                                                                    加權
                                                                </td>
                                                                <td class="innerhead">
                                                                    區間
                                                                </td>
                                                            </tr>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:LinkButton ID="lbt_Edit" runat="server" CommandArgument="<%# ((CmsRandomCity)Container.DataItem).Id%>"
                                                                    Text="編輯" CommandName="EditId"></asp:LinkButton>
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="cbx_CityStatus" runat="server" Checked="<%# ((CmsRandomCity)Container.DataItem).Status%>"
                                                                    Enabled="false" />
                                                            </td>
                                                            <td>
                                                                <%# ((CmsRandomCity)Container.DataItem).CityName%>
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="cbx_Priority" runat="server" Enabled="false" Checked="<%# ((CmsRandomCity)Container.DataItem).Ratio>1%>" />
                                                            </td>
                                                            <td>
                                                                <%# ((CmsRandomCity)Container.DataItem).StartTime.ToString("yyyy/MM/dd HH:mm")%>~
                                                                <%# ((CmsRandomCity)Container.DataItem).EndTime.ToString("yyyy/MM/dd HH:mm")%>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </Content>
                    </cc1:AccordionPane>
                </Panes>
            </cc1:Accordion>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

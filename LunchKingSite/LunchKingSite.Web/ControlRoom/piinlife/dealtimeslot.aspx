﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="dealtimeslot.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.piinlife.dealtimeslot" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        function block_ui(target, iwidth, iheight) {
            $.blockUI({
                message: $(target),
                css: {
                    backgroundcolor: 'transparent', border: 'none',
                    top: '20px', left: ($(window).width() - 700) / 2 + 'px', width: iwidth, height: iheight
                }
            });
        }
        function unblock_ui() {
            $('#target').empty();
            $('#target2').empty();
            $.unblockUI();
            return false;
        }

        function copytoblockui(o) {
            var seqlist = '';
            $('#<%=hif_Seq.ClientID%>').val(seqlist);
            var json = [];
            var total = $(o).find('tr').length;
            var tableform1 = '<table class="table1" style="width:100%">';
            var tableform2 = '<table class="table2" style="width:100%">';
            $(o).find('tr').each(function (i) {
                var id = $(this).find('td:first span:.ID').text();
                var name = $(this).find('td:first a:.NAME').text();
                var visaClass = $(this).find('td:first span.ExtAttrs').attr('visaClass');
                var create = '<span>' + $(this).find('td:first a:.NAME').text() + '</span>';
                create += '<select class="itemSelect" onchange="selectitem(this);">';
                if (i % 2 == 0) {
                    create = '<tr><td class="itemCell evenCell ' + visaClass + '">' + create;
                } else {
                    create = '<tr><td class="itemCell  oddCell ' + visaClass + '">' + create;
                }
                $(o).find('tr').each(function (j) {
                    if (i == j) {
                        var item = {};
                        item.id = id;
                        item.seq = (j + 1);
                        item.name = name;
                        item.visaClass = visaClass;
                        json.push(item);
                        create += '<option value="' + id + '" visaClass="'+visaClass +'" selected="selected" keyname="' + name + '">' + (j + 1) + '</option>';
                    } else {
                        create += '<option value="' + id + '" visaClass="'+visaClass+'" keyname="' + name + '">' + (j + 1) + '</option>';
                    }
                });
                create += '</select></td></tr>';
                if (i < 10) {
                    tableform1 += create;
                } else {
                    tableform2 += create;
                }
                //jQuery('#target').append(create);
            });
            tableform1 += '</table>';
            tableform2 += '</table>';
            var tableform = '<table style=\"width:100%\"><tr><td style=\"width:50%\">' + tableform1 + '</td><td style="width:50%; vertical-align:top">' + tableform2 + '</td></tr></table>';
            jQuery('#target').append(tableform);
            var u = '<ul style="display:none">';
            $.each(json, function (index, item) {
                u += '<li keyid="' + item.id + '" keyseq="' + item.seq + '">' + item.name + '</li>';
                seqlist += item.id + ',' + item.seq + ';';
            });
            u += '</ul>';
            jQuery('#target2').append(u);
            block_ui($('#blockpanel'), 500);
            $('#<%=hif_Seq.ClientID%>').val(seqlist);
        }

        function selectitem(o) {
            var seqlist = '';
            $('#<%=hif_Seq.ClientID%>').val(seqlist);
            var json = [];
            var seq;
            var t = parseInt($(o).find('option:selected').text());
            var v = $(o).find('option:selected').attr('value');
            var n = $(o).find('option:selected').attr('keyname');
            var visaClass = $(o).find('option:selected').attr('visaClass');
            $('ul li').each(function () {
                if ($(this).attr('keyid') == v) {
                    seq = parseInt($(this).attr('keyseq'));
                }
            });

            $('#blockpanel select').not(o).find('option:selected').each(function () {
                var item = {};
                item.id = $(this).attr('value');
                item.seq = parseInt($(this).text());
                item.name = $(this).attr('keyname');
                item.visaClass = $(this).attr('visaClass');
                json.push(item);
            });
            if (t > seq) {
                var item = {};
                item.id = v;
                item.seq = (parseInt(t) + 0.5) + '';
                item.name = n;
                item.visaClass = visaClass;
                json.push(item);
            }
            else {
                var item = {};
                item.id = v;
                item.seq = (parseInt(t) - 0.5) + '';
                item.name = n;
                item.visaClass = visaClass;
                json.push(item);
            }
            var sortedjson = $(json).sort(function (a, b) {
                if (a.seq == b.seq) {
                    return 0;
                }
                return a.seq > b.seq ? 1 : -1;
            });
            $('#target2').empty();
            var u = '<ul style="display:none">';
            $.each(sortedjson, function (index, item) {
                u += '<li keyid="' + item.id + '" keyseq="' + (index + 1) + '">' + item.name + '</li>';
                seqlist += item.id + ',' + (index + 1) + ';';
            });
            u += '</ul>';
            jQuery('#target2').append(u);
            $('#target').empty();
            var tableform1 = '<table class="table1" style="width:100%">';
            var tableform2 = '<table class="table2" style="width:100%">';
            for (var i = 0; i < sortedjson.length; i++) {
                var create = '<span>' + sortedjson[i].name + '</span>';
                create += '<select class="itemSelect" onchange="selectitem(this);">';
                if (i % 2 == 0) {
                    create = '<tr valign=top><td class="itemCell evenCell '+sortedjson[i].visaClass+'">' + create;
                }
                else {
                    create = '<tr valign=top><td class="itemCell oddCell ' + sortedjson[i].visaClass + '">' + create;
                }
                for (var j = 0; j < sortedjson.length; j++) {
                    if (i == j) {
                        create += '<option value="' + sortedjson[i].id + '" visaClass="'+sortedjson[i].visaClass+'" selected="selected"  keyname="' + sortedjson[i].name + '">' + (j + 1) + '</option>';
                    }
                    else {
                        create += '<option value="' + sortedjson[i].id + '" visaClass="'+sortedjson[i].visaClass+'" keyname="' + sortedjson[i].name + '">' + (j + 1) + '</option>';
                    }
                }
                create += '</select></td></tr>';
                if (i < 10) {
                    tableform1 += create;
                }
                else {
                    tableform2 += create;
                }
                //                jQuery('#target').append(create);
            }
            tableform1 += '</table>';
            tableform2 += '</table>';
            var tableform = '<table style=\"width:100%\"><tr><td style=\"width:50%\">' + tableform1 + '</td><td style="width:50%; vertical-align:top">' + tableform2 + '</td></tr></table>';
            jQuery('#target').append(tableform);
            $('#<%=hif_Seq.ClientID%>').val(seqlist);
        }
    </script>
    <style type="text/css">
        .freshVisa 
        {
            background-image: url(../../Themes/HighDeal/images/index/VISA_f.png);
            background-repeat:no-repeat;
            background-position:right top;
        }
        .staleVisa 
        {
            background-image: url(../../Themes/HighDeal/images/index/VISA_f2.png);
            background-repeat:no-repeat;
            background-position:right top;
        }
        .table1 .itemCell
        {
            border-bottom: 1px solid rgb(46, 43, 104);
            text-align: left;
            vertical-align: top;
            position: relative;
            padding-right: 40px;
            height: 45px;
            background-color: #B3D5F0;
            font-size: 15px;
            padding-top: 3px;
        }
        .table1 .oddCell
        {
            background-color: #B3D5F0;
        }
        .table1 .evenCell
        {
            background-color: #fff;
        }
        .table2 .itemCell
        {
            border-bottom: 1px solid rgb(46, 43, 104);
            text-align: left;
            vertical-align: top;
            position: relative;
            padding-right: 40px;
            height: 45px;
            background-color: #B3D5F0;
            font-size: 15px;
            padding-top: 3px;
        }
        .table2 .oddCell
        {
            background-color: #fff;
        }
        .table2 .evenCell
        {
            background-color: #B3D5F0;
        }        
        .itemSelect {
            position: absolute; right:0px;bottom: 0px;
        }
        .ID
        {
            display: none;
        }
        .SEQ
        {
            display: none;
        }
        .extAttrs
        {
            display: none;
        }        
        .NAME
        {
            color: Blue;
        }
        .TIME
        {
            color: Gray;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="blockpanel" style="display: none">
        <div id="target">
        </div>
        <div id="target2" style="display: none">
        </div>
        <div style="margin-top:10px">
        <asp:LinkButton ID="lbt_Confirm" runat="server" OnClick="ChangeSequence">確認</asp:LinkButton>
        <span onclick="unblock_ui();" style="cursor: pointer; text-decoration: underline; margin-left:20px;
                                                                       color: Red">取消</span>
        </div>
    </div>
    <asp:HiddenField ID="hif_Seq" runat="server" />
    <table>
        <tr>
            <td style="font-size:11px; vertical-align:top">
                <asp:DropDownList ID="ddlRegion" runat="server" OnSelectedIndexChanged="SelectRegionChanged"
                    AutoPostBack="true" />
            </td>
            <td style="font-size:11px; vertical-align:top">
                <img src="<%=ResolveUrl("~/Themes/HighDeal/images/index/VISA_f2.png") %>" /> 原Visa優先檔。會顯示在EDM中，會顯示在各分類與 "全部" 分類中。<br />
                <img src="<%=ResolveUrl("~/Themes/HighDeal/images/index/VISA_f.png") %>" /> 不會顯示在EDM中。不會顯示在各分類與 "全部" 分類中。                 
            </td>
        </tr>
    </table>
    <asp:Literal ID="lit_html" runat="server"></asp:Literal>
    <asp:Repeater ID="litTimeSlot" runat="server">
        
    </asp:Repeater>
</asp:Content>

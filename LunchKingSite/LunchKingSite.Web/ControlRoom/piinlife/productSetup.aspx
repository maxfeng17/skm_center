﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" ValidateRequest="false"  AutoEventWireup="true" CodeBehind="productSetup.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.piinlife.productSetup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
<script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
<script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="../../Tools/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="../../Tools/js/jquery.numeric.js"></script>
<script type="text/javascript" src="../../Tools/js/homecook.js"></script>
<link rel="stylesheet" type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"/> 

<style type="text/css">
fieldset 
{
    margin-bottom:16px;
}
fieldset legend
{
    color: #1C94C4;
    font-size: 100%;
}
.mainLabel
{
    font-size: smaller;
    color: #1C94C4;
    margin-top: 2px;
    margin-bottom: 2px;    
}
.divGroupItem
{
    margin-top:4px;
    margin-bottom: 4px;
}
.horizontalPadding
{
    padding-left: 20px;    
}
ul.formList
{
    list-style-type: none;
    padding-left: 0px;
    margin-left: 0px;
    margin-top: 5px;
    margin-bottom: 5px;
}
ul.formList li
{
    margin-top: 6px;
    margin-bottom: 6px;
}
.tblStoreSettings, .tblFreightIncome, .tblFreightExpense, .tblPurchasePrice
{
    border-width: 1px;
    border-style: solid;
    border-color: #333333;
    border-collapse: collapse;        
    margin-top: 10px;
    margin-left: 4px;
}
.tblStoreSettings th, .tblFreightIncome th, .tblFreightExpense th, .tblPurchasePrice th
{
    background-color: #F6A828;
    border-color: #333333;
    border-style: solid;  
    border-bottom-width: 1px;
    border-left-width: 0px;
    border-right-width: 0px;
    border-top-width: 1px;
    padding: 2px;
    padding-left:6px;
    padding-right:6px;
}
.tblStoreSettings td, .tblFreightIncome td, .tblFreightExpense td, .tblPurchasePrice td
{
    border-bottom-width: 1px;
    border-left-width: 0px;
    border-right-width: 0px;
    border-top-width: 1px;    
    padding: 2px;    
}
.tblStoreSettings tbody tr, .tblFreightIncome tbody tr, .tblFreightExpense tbody tr, .tblPurchasePrice tbody tr
{
    background-color: #F5F5F5;
}
.tblStoreSettings tbody tr:hover td, .tblFreightIncome tbody tr:hover td, .tblFreightExpense tbody tr:hover td, .tblPurchasePrice tbody tr:hover td
{
    background-color: #EDBD3E;
}
.ui-datepicker 
{
    font-size:13px;
}
.inputInfo
{
    background-color: #FFE87C; 
    margin-right:3em;
    font-size:13px;
    padding: 2px 4px 2px 4px;
}
</style>

<script type="text/javascript">               
    $(document).ready(function () {

        $('#tabs').tabs({
            select: function (event, ui) {
                var url = $.data(ui.tab, 'load.tabs');
                if (url) {
                    location.href = url;
                    return false;
                }
                return true;
            }
        });

        setupdatepicker('tbUseStartDate', 'tbUseEndDate');
        setupdatepicker('tbChangedExpireDate', 'tbChangedExpireDate');
        $('.datePicker').keydown(function () { $(this).blur(); });
        $('.tblStoreSettings tbody').sortable();


        //分店勾選時, 可編輯 "數量限制"; 取消勾選時,  "數量限制" 不可編輯且清除內容.
        $('.storeCheck').change(function () {
            if ($(this).is(':checked')) {
                $(this).closest('tr').find('td:eq(0) input', this).removeAttr('disabled');
                $('#chkIsInStore').attr('checked', 'checked');  //分店勾選時, 配送方式的 "到店" 也必須要勾起來
            }
            else {
                $(this).closest('tr').find('td:eq(0) input', this).val('');
                $(this).closest('tr').find('td:eq(0) input', this).attr('disabled', 'disabled');
            }
        });

        //宅配勾選時, 可編輯 "數量設定"; 取消勾選時,  "數量設定" 不可編輯且清除內容.
        $('.chkHomeDelivery input').change(homeDeliveryValidation);

        //初始化宅配的數量設定
        homeDeliveryValidation();

        if (getPid() > 0) {
            var imageupload = '../../Tools/ckeditor/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images&pid=' + getPid();
            CKEDITOR.replace('<%=tbProductDescription.ClientID%>', {
                filebrowserImageUploadUrl: imageupload,
                customConfig: '../../Tools/ckeditor/config_Hi.js'
            });
        }
        else {
            CKEDITOR.replace('<%=tbProductDescription.ClientID%>');
        }

        //把 CKEditor 用到的 textarea HTML decode.
        $('#<%=tbProductDescription.ClientID%>').text($('#<%=tbProductDescription.ClientID%>').html($('#<%=tbProductDescription.ClientID%>').text()).text());

        //簡訊尚餘字數初始化
        setSmsRemainingCount();

        //簡訊相關 textbox 輸入時更新尚餘字數
        $('.classSmsInputs').each(function (index, smsTxtBox) {
            $(smsTxtBox).keyup(setSmsRemainingCount);
            $(smsTxtBox).keypress(noPipelineChar);
        });


        //勾選宅配才顯示無法配送外島選項
        $('.chkHomeDelivery input').change(showNotDeliveryIslands);
        showNotDeliveryIslands();

        var productIsCombo = <%=this.IsCombo ? "true" : "false" %>;
        $('#chkIsCombo').click(function() {
            setComboChecked($(this).is(':checked'));
        });
        function setComboChecked(val) {
            if (val) {
                $('#lbTotalQuantityInfo').text('已勾選成套販售。必須為每單限購數的倍數');
                $('#lbPerBuyerLimitInfo').text('已勾選成套販售。必須為每單限購數的倍數');
            } else {
                $('#lbTotalQuantityInfo').text('若無限制, 請留空白，系統將自動帶入9,999');
                $('#lbPerBuyerLimitInfo').text('若無限制, 請留空白，系統將自動帶入20');
            }
        }
        setComboChecked(productIsCombo);
        bookingSystemCheckBoxLinkBind();
        SetBookingSystemDisplay();
        $('#txtAdvanceReservationDays').numeric();
        $('#txtCouponUsers').numeric();
    });
    
    function noPipelineChar(e) {
        var keynum;
        var keychar;
        var inputCheck;

        if (window.event) // IE
        {
            keynum = e.keyCode;
        }
        else if (e.which) // Netscape/Firefox/Opera
        {
            keynum = e.which;
        }
        keychar = String.fromCharCode(keynum);
        inputCheck = /\|/;
        
        return !inputCheck.test(keychar);
    }
    
    function setSmsRemainingCount() {
        var smsCnt = 136 - countSmsChars($('#tbSmsTitle').val() + $('#tbSmsPreText').val() + $('#tbSmsMessage').val());
        $('#spanSmsWordCount').text(smsCnt);
    }

    function prePostbackEncoding() {
        var str = $('<div/>').text($('#ctl00_ContentPlaceHolder1_tbProductDescription').text()).html();
        $('#<%=tbProductDescription.ClientID%>').text(str);
    }
    
    function updateOrderedStoreValues() {
        //format:  
        //有數量時: "store_guid1, 1, quantity_limit1; store_guid2, 2, quantity_limit2"
        //沒有數量時: "store_guid1, 1; store_guid2, 2"
        var trs = $('.tblStoreSettings tbody tr');
        var str = '';
        var seq = 0;
        trs.each(function (index, tr) {
            if ($(tr).find('input:checked', this).length != 0) {
                seq++;
                str += $(tr).find('td:eq(1) input', this).attr('value') +  ',' + seq;
                if ($(tr).find('td:eq(0) input', this).val().length > 0){
                    str += ',' + $(tr).find('td:eq(0) input', this).val();
                }
                str += ';';
            }
        });
        str = str.substring(0, str.length - 1);
        $('#<%= hidOrderedStoreValues.ClientID %>').attr('value', str);
    }

    //新增選擇類別
    function addOptCatg(strCatgName) {
        var htmlStr = "<fieldset class='catgBlock' style='display: none; background-color: #D8DBE1; color: #1C94C4;'>\
                                        <label>類別名稱：</label>\
                                        <input type='text' class='catgNameClass'/>\
                                        <button style='float:right;' onclick='$(this).parent().parent().slideUp(); return false;'>X</button>\
                                        <br/>\
                                        <div class=catgItem></div>\
                                        <button onclick=\"addOptItem($(this).prev(), '',''); return false;\">+</button>\
                                    </fieldset>";
        var optCatg = $('<div class="optCatg"/>').html(htmlStr);
        if (strCatgName.length > 0) {
            $(optCatg).find('.catgNameClass', this).val(strCatgName);
        }
        optCatg.appendTo('#divOptionalSettings');
        $('.optCatg .catgBlock').last().slideDown();
        $('#divOptionalSettings').sortable();
        return $('.optCatg .catgBlock').last();
    }

    //新增選擇項目
    function addOptItem(appendNode, strItemName, strItemQuantity) {
        var htmlStr = "項目名稱：<input type='text' style='width: 100px;' class='itemNameClass'/>\
                                    數量：<input type='text' style='width: 50px;' class='itemQuantityClass'/>\
                                    <button onclick='$(this).parent().slideUp(); return false;'>-</button>";
        var optItem = $('<div class="optItem" style="margin-left: 20px; display:none; font-size: smaller;"/>').html(htmlStr);
        if (strItemName.length > 0) {
            $(optItem).find('.itemNameClass', this).val(strItemName);
        }
        if (strItemQuantity.length > 0) {
            $(optItem).find('.itemQuantityClass', this).val(strItemQuantity);
        }
        optItem.appendTo($(appendNode));
        $(appendNode).find('.optItem', this).last().slideDown();
        $('.catgItem').sortable();
    }

    //把後端傳到 hidden field 的值填入成 "其他選單" 的內容
    function loadOptionals() {
        var str = $('#hidOptXml').val();
        str = str.replace(/\[/g, '<');
        str = str.replace(/\]/g, '>');
        var xmlDoc = $.parseXML(str);
        var root = xmlDoc.getElementsByTagName('Optionals')[0];
        var catgs = root.getElementsByTagName('OptCatg');
        $(catgs).each(function (index, catg) {
            var parent = addOptCatg(catg.getAttribute('catgName'));
            var items = catg.getElementsByTagName('OptItem');
            $(items).each(function (indexItem, item) {
                var quan = item.getAttribute('quantity');
                if(quan == null)
                    quan = '';
                addOptItem($(parent).find('.catgItem', this), item.childNodes[0].nodeValue, quan);
            });
        });
    }

    //把 "其他選單" 的內容轉成類Xml格式並放到 hidden field 裡傳送給後端用
    function setOptXml() {
        /*
        格式： **element 大寫開頭, attribute 小寫開頭
        [Optionals]
            [OptCatg catgName="顏色"]
                [OptItem]紅[/OptItem]
                [OptItem]白[/OptItem]
            [/OptCatg]
            [OptCatg catgName="大小"]
                [OptItem quantity="100"]大[/OptItem]
                [OptItem quantity="120"]中[/OptItem]
                [OptItem quantity="60"]小[/OptItem]
            [/OptCatg]
        [/Optionals]
        */
        var xml = '<Optionals></Optionals>';
        var xmlDoc = $.parseXML(xml);
        var root = xmlDoc.getElementsByTagName('Optionals')[0];

        $('#divOptionalSettings .optCatg').each(function (index, catg) {
            if ($(catg).is(':visible')) {
                var inputCatgName = $(catg).find('.catgNameClass', this);
                if ($(inputCatgName).val().length > 0) {
                    var catgElem = xmlDoc.createElement('OptCatg');
                    catgElem.setAttribute('catgName', $(inputCatgName).val());

                    var optItemDivs = $(catg).find('.optItem', this);
                    $(optItemDivs).each(function (idx, div) {
                        var inputItemName = $(div).find('.itemNameClass', this);
                        var inputItemQuantity = $(div).find('.itemQuantityClass', this);
                        if ($(div).is(':visible') && $(inputItemName).val().length > 0) {
                            var itemElem = xmlDoc.createElement('OptItem');
                            var itemNameText = xmlDoc.createTextNode($(inputItemName).val());
                            if ($(inputItemQuantity).val().length > 0) {
                                itemElem.setAttribute('quantity', $(inputItemQuantity).val());
                            }
                            itemElem.appendChild(itemNameText);
                            catgElem.appendChild(itemElem);
                        }
                    });

                    root.appendChild(catgElem);
                }
            }
        });

        var str = (new XMLSerializer()).serializeToString(xmlDoc);
        str = str.replace(/</g, '[');
        str = str.replace(/>/g, ']');
        $('#hidOptXml').val(str);
    }

    //宅配勾選時, 可編輯 "數量設定"; 取消勾選時,  "數量設定" 不可編輯且清除內容.
    function homeDeliveryValidation() {
        if ($('.chkHomeDelivery input:checked').attr('checked') == undefined) {
            $('#tbHomeDeliveryQuantity').attr('disabled', 'disabled');
            $('#tbHomeDeliveryQuantity').val('');
        } else {
            $('#tbHomeDeliveryQuantity').removeAttr('disabled');
        }
    }

    //勾選宅配才顯示無法配送外島選項
    function showNotDeliveryIslands() {
        if ($('.chkHomeDelivery input:checked').attr('checked') == undefined) {
            $('#showNotDeliveryIslands').hide();            
        } else {
            $('#showNotDeliveryIslands').show();
        }
    }

    function verifyQuantityLimit() {
        var isAllLimitSet = true;

        //宅配有勾的話, 數量要有值
        if ($('.chkHomeDelivery input:checked').attr('checked') != undefined){
            isAllLimitSet &= $('#tbHomeDeliveryQuantity').val().length > 0;
        }
        
        //分店有勾的話, 數量要有值
        $('.storeCheck:checked').closest('tr', this).find('.branchStoreQuantityLimit', this).each(function (index, tb) {
            isAllLimitSet &= $(tb).val().length > 0;
        });
        
        //宅配&分店都沒有勾的話, 要為false (以總量為準)
        if (($('.chkHomeDelivery input:checked').attr('checked') == undefined) & ($('.storeCheck:checked').length == 0)) {
            isAllLimitSet = false;
        }
            
        
        return isAllLimitSet;
    }
    
    function setCouponBySpecifics() {
        if(verifyQuantityLimit()) {
            $('#hidCouponByTotalOrBySpecifics').val('BySpecifics');
        }
        else {
            $('#hidCouponByTotalOrBySpecifics').val('ByTotal');
        }
    }

    //把sms字串裡特殊意義的地方取代成範例字串
    function replaceSpecialSmsMessage(smsStr) {
        var startDate = $('#tbUseStartDate').val();
        var endDate = $('#tbUseEndDate').val();
        var changedExpireDate = $("#tbChangedExpireDate").val();
        startDate = ReplaceAll(startDate,"/", "");
        endDate = ReplaceAll(endDate,"/", "");
        changedExpireDate = ReplaceAll(changedExpireDate, "/", "");
        var patternTel = /{tel}/g ;
        var pattern0 = /\{0}/g; // {0} = (coupon prefix + sequence)
        var pattern1 = /\{1}/g; //{1} = (coupon code)
        var pattern2 = /\{2}/g; //{2} = (begin date)
        var pattern3 = /\{3}/g; //{3} = (expire date)
        var replacementTel = '，預約專線0987654321';
        var replacement0 = 'A123456-1234';
        var replacement1 = '123456';
        var replacement2 = startDate;// '20120201';
        var replacement3 = endDate;// '20120331';

        smsStr = smsStr.replace(patternTel, replacementTel);
        smsStr = smsStr.replace(pattern0, replacement0);
        smsStr = smsStr.replace(pattern1, replacement1);
        smsStr = smsStr.replace(pattern2, replacement2);
        smsStr = smsStr.replace(pattern3, replacement3);
        
        return smsStr;
    }

    function ReplaceAll(source, stringToFind, stringToReplace) {
        var temp = source;
        var index = temp.indexOf(stringToFind);
        while (index != -1) {
            temp = temp.replace(stringToFind, stringToReplace);
            index = temp.indexOf(stringToFind);
        }
        return temp;
    }
    
    //算sms字串字數.
    function countSmsChars(smsStr) {
        var x0 = 12; //A123456-1234  (coupon prefix + sequence)
        var x1 = 6; //123456 (coupon code)
        var x2 = 8; //20120101 (date)
        var xtel = 15; //，預約專線0987654321

        var pattern0 = /\{0}/g; // {0} = (coupon prefix + sequence)
        var pattern1 = /\{1}/g; //{1} = (coupon code)
        var pattern2 = /{[2-3]}/g; //{2} or {3} = (begin date) or (expire date)
        var patternTel = /{tel}/g;
        var patternAll = /{[0-3]}/g ;

        var charCount = 0;
        if (smsStr.match(pattern0) !== null)
            charCount += smsStr.match(pattern0).length * x0;
        if (smsStr.match(pattern1) !== null)
            charCount += smsStr.match(pattern1).length * x1;
        if (smsStr.match(pattern2) !== null)
            charCount += smsStr.match(pattern2).length * x2;
        if (smsStr.match(patternTel) !== null)
            charCount += smsStr.match(patternTel).length * xtel;

        smsStr = smsStr.replace(patternAll, '');
        smsStr = smsStr.replace(patternTel, '');
        charCount += smsStr.length;

        return charCount;
    }

    //防呆
    function checkInput() {
        var result = true;
        var errMsg = '';

        if ($('#tbUseStartDate').val().length == 0) {
            errMsg += '"有效期間"要設定!\r\n';
            result = false;
        }

        if ($('#tbUseEndDate').val().length == 0) {
            errMsg += '"配送期間"要設定!\r\n';
            result = false;
        }

        if ($(".tblPurchasePrice td:contains('無資料')").size() != 0) {
            errMsg += '"進貨價"要設定!\r\n';
            result = false;
        }

        if($('#tbOriginalPrice').val().length == 0){
            errMsg += '"原價"要設定!\r\n';
            result = false;
        }

        if ($('#tbPrice').val().length == 0) {
            errMsg += '"售價"要設定!\r\n';
            result = false;
        }

        if ($('#chkIsCombo').is(':checked')) {
            var cq = parseInt($('#tbPerOrderLimit').val(), 10);
            var tq = parseInt($('#tbTotalQuantity').val(), 10);
            var pb = parseInt($('#tbPerBuyerLimit').val(), 10);
            if (isNaN(tq) || isNaN(pb) || isNaN(cq)) {
                errMsg += '"總數量"或"每人限購數"或"每單限購數"設定錯誤';
                result = false;
            } else if (cq < 2) {
                errMsg += '當成套售販勾選，每單限購數"必需大於1';
                result = false;
            } else if (tq % cq > 0 || pb % cq > 0) {
                errMsg += '當勾選"成套販售"時，"總數量"與"每人限購數"，必須為"每單限購數"的倍數';
                result = false;
            }
        }

        if (136 - countSmsChars($('#tbSmsTitle').val() + $('#tbSmsPreText').val() + $('#tbSmsMessage').val()) < 0) {
            errMsg += '簡訊字數太多! 請調整.\r\n';
            result = false;
        }

        if (!$(".chkHomeDelivery input:checked").attr("checked") && !$("#chkIsInStore").attr("checked")) {
            errMsg += '請至少選一種配送方式!\r\n';
            result = false;
        } else {
            if ($('#chkIsInStore').attr('checked')) {
               //確認至少選一分店
               var bolChoseStore = false;
               $('.tblStoreSettings .storeCheck').each(
                   function () {
                      if($(this).attr('checked')){bolChoseStore=true;}
                   }
                   );

               if (!bolChoseStore) {
                  errMsg += '到店至少選一分店消費 .\r\n';
                  result = false;
               }
            }
         }

        //預約系統設定檢查
        if ($('input[id*=cb_IsUsingbookingSystem]').is(':checked')) {
            if (!$("#txtAdvanceReservationDays").val() || $("#txtAdvanceReservationDays").val() == "0")
            {
                errMsg += '消費者提早預約天數至少需填1天 .\r\n';
                result = false;
                $("#txtAdvanceReservationDays").focus();
            }
            if ($('input[id*=rdoMultiUsers]').is(':checked'))
            {
                if (!isNumber($("#txtCouponUsers").val()) || !$("#txtCouponUsers").val() || $("#txtCouponUsers").val() == "0") {
                    errMsg += '多人優惠時請設定本次優惠最多幾人使用(不可為0) .\r\n';
                    result = false;
                    $("#txtCouponUsers").focus();
                }
            }
        }

        if (!result)
            alert(errMsg);
        
        return result;
    }

    function showSmsPreview() {
        alert(replaceSpecialSmsMessage($('#tbSmsTitle').val() + $('#tbSmsPreText').val() + $('#tbSmsMessage').val()));
    }
    
    //抓 Url 的 query string Pid 部分. (for ckeditor)
    function getPid() {
        var a;
        $(window.location.search.substring(1).split('&')).each(function (index, queryParam) {
            if (queryParam.split('=')[0] == 'pid') {
                a = queryParam.split('=')[1];
            }            
        });
        if(a == undefined) {
            return 0;
        }
        else {
            return a;
        }
    }
    
    //檢查參數是否為數字
    function validateNumerics(classStr) {
        var pattern = new RegExp('^[0-9]+$', 'i');
        var str = '';
        $('.' + classStr).each(function (index, tb) {
            str += $(tb).val();
        });
        return pattern.test(str);
    }

    //預約預約服務 check box bind
    function bookingSystemCheckBoxLinkBind() {
        $(':checkbox[id*=cb_IsUsingbookingSystem]').live('change', function () {
            if ($(this).is(':checked'))
            {
                $('.CouponUsers').show();
                SetBookingSystemDisplay();
            }
            else
            {
                $('.CouponUsers').hide();
            }
        });
    
        $(':radio[id*=rdoSingleUser]').live('change', function () {
            if ($(this).is(':checked')) {
                $('#txtCouponUsers').val("");
                $('#txtCouponUsers').prop('disabled', true);
            }
        });
        $(':radio[id*=rdoMultiUsers]').live('change', function () {
            if ($(this).is(':checked')) {
                $('#txtCouponUsers').prop('disabled', false);
            }
        });
    }
    function SetBookingSystemDisplay()
    {
        if ($(':checkbox[id*=cb_IsUsingbookingSystem]').is(':checked')) {
            $('.CouponUsers').show();
        }
        if ($(':radio[id*=rdoSingleUser]').is(':checked')) {
            $('#txtCouponUsers').val("");
            $('#txtCouponUsers').prop('disabled', true);
        } else {
            $('#txtCouponUsers').prop('disabled', false);
        }
    }
    function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div style="position: absolute; width:84%; right: 1%;">
<div id="tabs">
    <ul class="tabMenu">
        <li><a href="#tabs-1">商品設定</a></li>
        <li><a href="#tabs-2">選單設定</a></li>
        <li><a href="#tabs-3">其他設定</a></li>              
        <li style="float:right;"><a href='../piinlife/setup.aspx?did=<%=Did%>'>回檔次設定</a></li> 
    </ul>

    <%-- 商品設定 --%>
    <div id="tabs-1">
        <%-- 商品設定 --%>
        <fieldset class="ui-corner-all">
            <legend>商品設定</legend>
            <p>
                <asp:Label ID="Label4" runat="server" CssClass="mainLabel"><%=Resources.Localization.ProductId %>：</asp:Label>
                <asp:Label ID="Label5" runat="server" CssClass="mainLabel"><%=Pid %></asp:Label>
            </p>
            <p>
            <asp:Label runat="server" CssClass="mainLabel">狀態：</asp:Label>
                <asp:DropDownList runat="server" ID="ddlIsShow">
                    <asp:ListItem Value="true" Selected="true">顯示</asp:ListItem>
                    <asp:ListItem Value="false">隱藏</asp:ListItem>
                </asp:DropDownList>
            </p>           
            <p>
                <asp:Label runat="server" AssociatedControlID="tbProductName" CssClass="mainLabel">商品名稱：</asp:Label>
                <asp:TextBox ID="tbProductName" runat="server" />
            </p>
            <p>
                <asp:Label runat="server" AssociatedControlID="tbProductDescription" CssClass="mainLabel">商品簡介：</asp:Label>
                <asp:TextBox ID="tbProductDescription" runat="server" TextMode="MultiLine"/>
            </p>
        </fieldset>
        
        <%-- 預約管理設定 --%>
        <fieldset class="ui-corner-all">
            <legend>預約管理設定</legend>
            <asp:Label runat="server" CssClass="mainLabel">預約管理：</asp:Label>
            <asp:CheckBox ID="cb_IsUsingbookingSystem" Text="配合" CssClass="mainLabel" runat="server" />
            <div class="CouponUsers" style="display:none">
                <label class="mainLabel">類型:</label>
                <label class="mainLabel">
                    <asp:RadioButton ID="rdoBookingSystemSeat" GroupName="bookingSystemType" Checked="true" runat="server" />訂位管理(如：餐廳、SPA)
                </label>
                <label class="mainLabel">
                    <asp:RadioButton ID="rdoBookingSystemRoom" Enabled="false" GroupName="bookingSystemType" runat="server" />訂房管理(如：住宿)
                </label>
                <br />
                <label class="mainLabel">
                    消費者需提早
                    <asp:TextBox ID="txtAdvanceReservationDays" MaxLength="2" ClientIDMode="Static" Width="25px" Text="1" runat="server"></asp:TextBox>
                    天預約(至少需填1天)
                </label>
                <br />
                <label class="mainLabel">
                    <asp:RadioButton ID="rdoSingleUser" GroupName="rdoUsers" Checked="true" runat="server" />
                    單人優惠(1人使用一張，或無使用限制的檔次)
                </label>
                <br />
                <label class="mainLabel">
                    <asp:RadioButton ID="rdoMultiUsers" GroupName="rdoUsers" runat="server" />
                    多人優惠，
                </label>
                <label class="mainLabel">
                    本次優惠為
                    <asp:TextBox ID="txtCouponUsers" ClientIDMode="Static" MaxLength="2" Width="25px" runat="server"></asp:TextBox>
                    人享用一張憑證(以最多人數計算，如3~4人套餐，則填4)
                </label>
            </div>
        </fieldset>

        <%-- 時程設定 --%>
        <fieldset class="ui-corner-all">
            <legend>時程設定</legend>
            <asp:Label runat="server" CssClass="mainLabel">有效期間／配送期間：</asp:Label>
            <asp:TextBox runat="server" ID="tbUseStartDate" CssClass="datePicker" Width="120px" ClientIDMode="static"/>
            <span style="padding-left: 2px; padding-right:2px;">~</span>
            <asp:TextBox runat="server" ID="tbUseEndDate" CssClass="datePicker" Width="120px" ClientIDMode="static"/>
            <br />
            <asp:Label runat="server" CssClass="mainLabel">調整有效期限截止日：</asp:Label>
            <asp:TextBox runat="server" ID="tbChangedExpireDate" CssClass="datePicker" Width="120px" ClientIDMode="static"/>
        </fieldset>

        <%-- 進貨資訊 --%>
        <fieldset class="ui-corner-all">
            <legend>進貨資訊</legend>
                <%----------------------------------------------------------------------進貨價----------------------------------------------------------------------%>
                <asp:Label runat="server" AssociatedControlID="tbPurchasePrice" CssClass="mainLabel">進貨價：</asp:Label>
                <asp:TextBox runat="server" ID="tbPurchasePrice" Width="80px" CssClass="purchasePriceClass" />
                <asp:Label runat="server" AssociatedControlID="tb17LifeBuyAmount" CssClass="mainLabel">份數</asp:Label>
                <asp:TextBox runat="server" ID="tbPurchaseQuantity" Width="80px" CssClass="purchasePriceClass" />
                <asp:Button runat="server" ID="btnNewPurchasePrice" Text="新增進貨價" OnClientClick="prePostbackEncoding(); if(!validateNumerics('purchasePriceClass')){ alert('只可輸入數字!');return false;}" OnClick="btnNewPurchasePrice_Click" />
                <asp:Button runat="server" ID="btnRemovePurchasePrice" Text="刪除進貨價" OnClientClick="prePostbackEncoding();" OnClick="btnDeletePurchasePrice_Click" />
                
                <asp:UpdatePanel runat="server" id="UpdatePanel2">
                    <contenttemplate>
                        <asp:ListView runat="server" ID="lvPurchasePrice">
                            <LayoutTemplate>
                                <table class="tblPurchasePrice">
                                    <thead>
                                        <tr>
                                            <th>序號</th>
                                            <th>進貨價</th>
                                            <th>份數</th>
                                            <th>累積份數</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr runat="server" ID="itemPlaceHolder"></tr>
                                    </tbody>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr style='background-color: <%# Eval("Action").ToString() == "add" ? "yellow" : Eval("Action").ToString() == "delete" ? "pink" : "" %>'>                                
                                    <td style="text-align: center;"><%#Container.DataItemIndex + 1%> </td>
                                    <td style="text-align: right;"><%# Eval("Cost")%></td>
                                    <td style="text-align: right;"><%# Eval("Quantity")%></td>
                                    <td style="text-align: right;"><%# Eval("CumulativeQuantity")%></td>
                                </tr>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                                <table class="tblPurchasePrice">
                                    <thead>
                                        <tr>
                                            <th>序號</th>
                                            <th>進貨價</th>
                                            <th>份數</th>
                                            <th>累積份數</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="text-align: center;" colspan="4">無資料</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </EmptyDataTemplate>
                        </asp:ListView>
                    </contenttemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnNewPurchasePrice" />
                        <asp:AsyncPostBackTrigger ControlID="btnRemovePurchasePrice" />
                    </Triggers>
                </asp:UpdatePanel>
                <br/>
                <%----------------------------------------------------------------------進貨運費----------------------------------------------------------------------%>
                <asp:Label ID="Label1" runat="server" AssociatedControlID="tbFreightExpense" CssClass="mainLabel">進貨運費：</asp:Label>
                <asp:TextBox runat="server" ID="tbFreightExpense" Width="80px" CssClass="freightExpenseClass" />
                <asp:Label ID="Label2" runat="server" AssociatedControlID="tb17LifeBuyAmount" CssClass="mainLabel">購買金額 ≧</asp:Label>
                <asp:TextBox runat="server" ID="tb17LifeBuyAmount" Width="80px" CssClass="freightExpenseClass" />
                <asp:Button runat="server" ID="btnNewFreightExpense" Text="新增進貨運費" OnClientClick="prePostbackEncoding(); if(!validateNumerics('freightExpenseClass')){ alert('只可輸入數字!');return false;}" OnClick="btnNewFreightExpense_Click" />
                <asp:Button runat="server" ID="btnRemoveFreightExpense" Text="刪除進貨運費" OnClientClick="prePostbackEncoding();" OnClick="btnDeleteFreightExpense_Click" />
                
                <asp:UpdatePanel runat="server" id="UpdatePanel1">
                    <contenttemplate>
                        <asp:ListView runat="server" ID="lvFreightExpense">
                            <LayoutTemplate>
                                <table class="tblFreightExpense">
                                    <thead>
                                        <tr>
                                            <th>序號</th>
                                            <th>運費</th>
                                            <th>購買金額≧</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr runat="server" ID="itemPlaceHolder"></tr>
                                    </tbody>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr style='background-color: <%# Eval("Action").ToString() == "add" ? "yellow" : Eval("Action").ToString() == "delete" ? "pink" : "" %>'>
                                    <td style="text-align: center;"><%#Container.DataItemIndex + 1%> </td>
                                    <td style="text-align: right;"><%# Eval("FrieghtValue")%></td>
                                    <td style="text-align: right;"><%# Eval("ThresholdAmount")%></td>
                                </tr>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                                <table class="tblFreightExpense">
                                    <thead>
                                        <tr>
                                            <th>序號</th>
                                            <th>運費</th>
                                            <th>購買金額≧</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="text-align: center;" colspan="3">無資料</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </EmptyDataTemplate>
                        </asp:ListView>
                    </contenttemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnNewFreightExpense" />
                        <asp:AsyncPostBackTrigger ControlID="btnRemoveFreightExpense" />
                    </Triggers>
                </asp:UpdatePanel>
               <div id="showNotDeliveryIslands">
                <asp:CheckBox ID="cbx_NotDeliveryIslands" runat="server" 
                CssClass="mainLabel" Text="無法配送外島"/></div>
                <br/>
                <!-- 20120907_Justin 將下面這個div 隱藏起來 -->
                <div id="divNoInvoiceCreate" style="display: none">
                    <asp:Label ID="Label3" runat="server" AssociatedControlID="tbFreightExpense" CssClass="mainLabel">已含18%營業稅，不開立發票：</asp:Label><br/>
                    <asp:CheckBox ID="cbx_NoInvoiceCreate" runat="server" CssClass="mainLabel" Text="打勾為不開立發票" /></div>
        </fieldset>

        <%-- 價格資訊 --%>
        <fieldset class="ui-corner-all">
            <legend>價格資訊</legend>
            <p>
                <asp:Label runat="server" AssociatedControlID="tbOriginalPrice" CssClass="mainLabel">原價：</asp:Label>
                <asp:TextBox runat="server" ID="tbOriginalPrice" ClientIDMode="Static" /> 
                <asp:CheckBox  runat="server"  ID="cbShowDiscount" />      
                <asp:Label AssociatedControlID="cbShowDiscount" runat="server" CssClass="mainLabel" >前台顯示原價與折數</asp:Label>
            </p>
            <p>
                <asp:Label runat="server" AssociatedControlID="tbPrice" CssClass="mainLabel">售價：</asp:Label>
                <asp:TextBox runat="server" ID="tbPrice" ClientIDMode="Static" />
            </p>            
            
            <%----------------------------------------------------------------------消費者運費----------------------------------------------------------------------%>
            <asp:Label runat="server" AssociatedControlID="tbFreightIncome" CssClass="mainLabel">運費：</asp:Label>
            <asp:TextBox runat="server" ID="tbFreightIncome" Width="80px" CssClass="freightIncomeClass" />
            <asp:Label runat="server" AssociatedControlID="tbUserBuyAmount" CssClass="mainLabel">購買金額 ≧</asp:Label>
            <asp:TextBox runat="server" ID="tbUserBuyAmount" Width="80px" CssClass="freightIncomeClass" />
            <asp:Button runat="server" ID="btnNewFreightIncome" Text="新增運費" OnClientClick="prePostbackEncoding(); if(!validateNumerics('freightIncomeClass')){ alert('只可輸入數字!');return false;} " OnClick="btnNewFreightIncome_Click" />
            <asp:Button runat="server" ID="btnRemoveFreightIncome" Text="刪除運費" OnClientClick="prePostbackEncoding();" OnClick="btnDeleteFreightIncome_Click" />
            
            <asp:UpdatePanel runat="server" id="UpdatePanel">
                <contenttemplate>
                    <asp:ListView runat="server" ID="lvFreightIncome">
                        <LayoutTemplate>
                            <table class="tblFreightIncome">
                                <thead>
                                    <tr>
                                        <th>序號</th>
                                        <th>運費</th>
                                        <th>購買金額≧</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr runat="server" ID="itemPlaceHolder"></tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr style='background-color: <%# Eval("Action").ToString() == "add" ? "yellow" : Eval("Action").ToString() == "delete" ? "pink" : "" %>'>
                                <td style="text-align: center;"><%#Container.DataItemIndex + 1%> </td>
                                <td style="text-align: right;"><%# Eval("FrieghtValue")%></td>
                                <td style="text-align: right;"><%# Eval("ThresholdAmount")%></td>
                            </tr>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <table class="tblFreightIncome">
                                <thead>
                                    <tr>
                                        <th>序號</th>
                                        <th>運費</th>
                                        <th>購買金額≧</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: center;" colspan="3">無資料</td>
                                    </tr>
                                </tbody>
                            </table>
                        </EmptyDataTemplate>
                    </asp:ListView>
                    <%--<asp:Literal runat="server" ID="litMessage" />--%>
                </contenttemplate>
                <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnNewFreightIncome" />
                        <asp:AsyncPostBackTrigger ControlID="btnRemoveFreightIncome" />
                    </Triggers>
            </asp:UpdatePanel>
        </fieldset>

        <%-- 銷售限制 --%>
        <fieldset class="ui-corner-all">
            <legend>銷售限制</legend>
            <ul class="formList">
                <li>
                    <asp:Label runat="server" AssociatedControlID="tbTotalQuantity" CssClass="mainLabel">總數量：</asp:Label>
                    <asp:TextBox runat="server" ID="tbTotalQuantity" ClientIDMode="Static" />
                    <span id="lbTotalQuantityInfo" class="inputInfo" style="margin-left: 10px;">若無限制, 請留空白，系統將自動帶入9,999</span>
                </li>
                <li>
                    <asp:Label runat="server" AssociatedControlID="tbPerBuyerLimit" CssClass="mainLabel">每人限購數：</asp:Label>
                    <asp:TextBox runat="server" ID="tbPerBuyerLimit" ClientIDMode="Static" />
                    <span id="lbPerBuyerLimitInfo" class="inputInfo" style="margin-left: 10px;">若無限制, 請留空白，系統將自動帶入20</span>
                </li>
                <li>
                    <asp:Label runat="server" AssociatedControlID="tbPerOrderLimit" CssClass="mainLabel">每單限購數：</asp:Label>
                    <asp:TextBox runat="server" ID="tbPerOrderLimit" ClientIDMode="Static" />
                    <asp:CheckBox runat="server" ID="chkIsCombo" ClientIDMode="Static" />
                    <asp:Label ID="tbIsCombo" runat="server" AssociatedControlID="chkIsCombo" CssClass="mainLabel">成套販售</asp:Label>
                    <span class="inputInfo">若無限制, 請留空白，系統將自動帶入20</span>
                </li>
                <li>
                    <asp:CheckBox runat="server" ID="chkIsNoRefund" />        
                    <asp:Label runat="server" AssociatedControlID="chkIsNoRefund" CssClass="mainLabel">不接受退貨</asp:Label>         
                </li>
                <li>
                    <asp:CheckBox runat="server" ID="chkIsExpireNoRefund" />        
                    <asp:Label runat="server" AssociatedControlID="chkIsExpireNoRefund" CssClass="mainLabel">過期不能退貨</asp:Label>         
                </li>
                <li>
                    <asp:CheckBox runat="server" ID="chkIsDaysNoRefund" />        
                    <asp:Label runat="server" AssociatedControlID="chkIsDaysNoRefund" CssClass="mainLabel">結檔後七天不能退貨</asp:Label>         
                </li>
            </ul>            
        </fieldset>
        
        <%-- 付款設定 --%>
        <fieldset class="ui-corner-all">
            <legend>核銷與帳務設定</legend>
            <ul class="formList">
                <li>
                    <span class="mainLabel payto">
                        <span>匯款資料：</span>
                        <label><asp:RadioButton ID="rbPayToCompany" GroupName="payto" runat="server" />統一匯至賣家匯款資料</label>
                        <label><asp:RadioButton ID="rbPayToStore" GroupName="payto" runat="server" />匯至各分店匯款資料</label>
                    </span>
                </li>
                <li>
                    <span class="mainLabel payto">
                        <span>對帳方式：</span>
                        <label><asp:RadioButton ID="rbBMNone" GroupName="VendorBilling" runat="server" Enabled="False"/>舊版對帳系統</label>
                        <label><asp:RadioButton ID="rbBMBalanceSheetSystem" GroupName="VendorBilling" runat="server" />新版對帳系統</label>
                        <label><asp:RadioButton ID="rbBMMohistSystem" GroupName="VendorBilling" runat="server" />墨攻對帳系統</label>
                    </span>
                </li>
                <li>
                    <span class="mainLabel payto">
                        <span>出帳方式：</span>
                        <label><asp:RadioButton ID="rbAchWeekly" GroupName="achType" runat="server" />ACH每週出帳</label>
                        <label><asp:RadioButton ID="rbAchMonthly" GroupName="achType" runat="server" />ACH每月出帳</label>
                        <label><asp:RadioButton ID="rbManualWeekly" GroupName="achType" runat="server" />人工每週出帳</label>
                        <label><asp:RadioButton ID="rbManualMonthly" GroupName="achType" runat="server" />人工每月出帳</label>
                        <label><asp:RadioButton ID="rbAchPartially" GroupName="achType" runat="server" />商品(暫付70%)</label>
                        <label><asp:RadioButton ID="rbAchOthers" GroupName="achType" runat="server" />其他付款方式</label>
                        <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <span class="inputInfo">憑證檔選擇舊版對帳系統時，出帳方式只有＂ACH每週出帳＂有效，其他選項僅用於備註。</span>
                    </span>
                </li>
                <li>
                    <span class="mainLabel payto">
                        <span>店家單據開立方式：</span>
                        <label><asp:RadioButton ID="rbRecordInvoice" GroupName="record" runat="server" />統一發票</label>
                        <label><asp:RadioButton ID="rbRecordReceipt" GroupName="record" runat="server" />免用統一發票收據</label>
                        <label><asp:RadioButton ID="rbRecordOthers" GroupName="record" runat="server" />其他</label>
                        <label><asp:TextBox ID="tbAccountingMessage" runat="server" /></label>
                    </span>
                </li>
                <li>
                    <span class="mainLabel payto">
                        <span>免稅設定：</span>
                        <asp:CheckBox runat="server" ID="cbIsInputTaxFree" />        
                        <asp:Label runat="server" AssociatedControlID="cbIsInputTaxFree">店家發票免稅（無法確認，請洽財會）</asp:Label>  
                        <asp:CheckBox runat="server" ID="chkIsTaxFree" />        
                        <asp:Label runat="server" AssociatedControlID="chkIsTaxFree">開立免稅發票給消費者(無法確認，請洽財會)</asp:Label>  
                    </span>
                </li>
            </ul>
        </fieldset>
        <%-- 訂房鎖定 --%>
        <fieldset class="ui-corner-all">
            <legend>訂房鎖定</legend>
            <ul class="formList">
                <li>
                    <span class="mainLabel payto">
                        <asp:CheckBox ID="cbIsReserveLock" Text="住宿類，商家系統提供「訂房鎖定」功能" runat="server" />
                    </span>
                </li>
            </ul>
        </fieldset>
    </div>

    <%-- 選單設定 --%>
    <div id="tabs-2">

        <%-- 配送方式 --%>
        <fieldset class="ui-corner-all">
            <legend>配送方式</legend>
            <ul class="formList">
                <li>
                    <asp:CheckBox runat="server" ID="chkIsHomeDelivery" CssClass="chkHomeDelivery"/>        
                    <asp:Label runat="server" AssociatedControlID="chkIsHomeDelivery" CssClass="mainLabel">宅配</asp:Label>         
                    <asp:TextBox runat="server" ID="tbHomeDeliveryDescription"></asp:TextBox>
                    <br/>
                    <span style="padding-left: 3.5em;"></span>
                    <asp:Label runat="server" AssociatedControlID="tbHomeDeliveryQuantity" CssClass="mainLabel">數量設定：</asp:Label> 
                    <asp:TextBox runat="server" ID="tbHomeDeliveryQuantity" ClientIDMode="Static" Width="4em"></asp:TextBox>
                </li>
                <li>
                    <asp:CheckBox runat="server" ID="chkIsInStore" ClientIDMode="Static"/>        
                    <asp:Label runat="server" AssociatedControlID="chkIsInStore" CssClass="mainLabel">到店</asp:Label>         
                    <asp:TextBox runat="server" ID="tbInStoreDescription"></asp:TextBox>
                </li>
            </ul>
        </fieldset>

        <%-- 分店選單 --%>
        <fieldset class="ui-corner-all">
            <legend>分店選單</legend>
            <asp:HiddenField runat="server" ID="hidOrderedStoreValues" />
            <asp:ListView runat="server" ID="lvStore">
                <LayoutTemplate>
                    <table class="tblStoreSettings">
                        <thead>
                            <tr>
                                <th>數量限制</th>
                                <th>排序</th>
                                <th>分店</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr runat="server" ID="itemPlaceholder" />
                        </tbody>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td> 
                            <asp:TextBox runat="server" ID="tbQuantityLimit" CssClass="branchStoreQuantityLimit" Width="80px" Text='<%# Eval("QuantityLimit")  %>'                              
                                Enabled='<%# string.IsNullOrEmpty(Eval("Sequence").ToString()) ? bool.Parse("false") : bool.Parse("true") %>'></asp:TextBox></td>
                        <td style="text-align: center;">                            
                            <asp:HiddenField runat="server" ID="hidStoreGuid" Value='<%# Eval("StoreGuid")  %>' />
                            <span><asp:Literal runat="server" ID="litSeq" Text='<%# Eval("Sequence")  %>' /></span>
                        </td>
                        <td>                            
                            <input type="checkbox" runat="server" ID="chkStore" checked='<%# string.IsNullOrEmpty(Eval("Sequence").ToString()) ? bool.Parse("false") : bool.Parse("true") %>' class="storeCheck" />
                            <asp:Label runat="server" AssociatedControlID="chkStore"/>
                            <asp:Literal runat="server" Text='<%# Eval("StoreName").ToString() + Eval("StoreAddress").ToString()  %>' />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </fieldset>

        <asp:HiddenField ID = "hidCouponByTotalOrBySpecifics" runat="server" ClientIDMode="Static" />

        <%-- 其他選單 --%>
        <fieldset class="ui-corner-all">
            <legend>其他選單</legend>
            <input id="hidOptXml" runat="server" type="hidden" ClientIDMode="Static"/>
            <p>最多<span style="color:Red;">5</span>種類別</p>            
            <div id="divOptionalSettings">
            </div>            
            <button name="btnNewOptCatg" onclick="if($('.optCatg').length >= 5){ alert('最多五種類別!'); return false;} addOptCatg(''); return false;">新類別</button>
        </fieldset>
    </div>

    <%-- 其他設定 --%>
    <div id="tabs-3">
        <%-- 簡訊憑證 --%>
        <fieldset class="ui-corner-all">
            <legend>簡訊憑證</legend>
            <ul class="formList">
                <li style="display: none">
                    <asp:Label runat="server" AssociatedControlID="tbSmsTitle" CssClass="mainLabel">標題：</asp:Label><br/>
                    <asp:TextBox runat="server" ID="tbSmsTitle" ClientIDMode="Static" CssClass="classSmsInputs"  />
                </li>
                <li>
                    <asp:Label runat="server" AssociatedControlID="tbSmsPreText" CssClass="mainLabel">前置文：</asp:Label><br/>
                    <asp:TextBox runat="server" ID="tbSmsPreText" ClientIDMode="Static" CssClass="classSmsInputs" />
                </li>
                <li style="display: none">
                    <asp:Label runat="server" AssociatedControlID="tbSmsMessage" CssClass="mainLabel">資訊：</asp:Label><br/>
                    <asp:TextBox runat="server" ID="tbSmsMessage" ClientIDMode="Static" CssClass="classSmsInputs" />
                </li>
                <li>
                <asp:CheckBox ID="cbx_DisableSMS" runat="server" CssClass="cbx_DisableSMS" />
                 <asp:Label runat="server" AssociatedControlID="cbx_DisableSMS"  CssClass="mainLabel"> 關閉簡訊發送按鈕</asp:Label>     
                </li>
            </ul>
            <span class="inputInfo">尚餘可輸入字數：<span id="spanSmsWordCount" style='color: red'></span></span>
            <input type="button" onclick="showSmsPreview();" value="簡訊預覽"/>
        </fieldset>

        <%-- 預覽 --%>
        <fieldset class="ui-corner-all">
            <legend>預覽</legend>
            <input type="button" value="商品頁預覽" <%=string.Format("onclick=\"window.open('{0}');\"", VirtualPathUtility.ToAbsolute(string.Format("~/piinlife/HiDeal.aspx?gpid={0}&mode=preview", Gpid))) %> />&nbsp;&nbsp;
            <input id="btnPreviewCouponPrint" type="button" value="憑證預覽"  <%=string.Format("onclick=\"window.open('{0}');\"", VirtualPathUtility.ToAbsolute(string.Format("~/piinlife/member/CouponPrint.aspx?did={0}&pid={1}&cid=000000&mode=preview",Did,Pid))) %> />
        </fieldset>
    </div>
</div>
<asp:Button runat="server" ID="btnSave" Text="Save" OnClientClick="updateOrderedStoreValues(); setCouponBySpecifics(); if(!checkInput()) {return false;} ; $('#tabs ul.tabMenu').find('li:eq(1) a', this).click(); setOptXml();" OnClick="btnSave_Click" />
</div>

<asp:Literal runat="server" ID="litRunOnFirstLoad" />
</asp:Content>

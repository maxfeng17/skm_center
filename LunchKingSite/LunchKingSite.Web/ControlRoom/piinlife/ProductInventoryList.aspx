﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductInventoryList.aspx.cs"
MasterPageFile="~/ControlRoom/backend.master"  Inherits="LunchKingSite.Web.ControlRoom.piinlife.ProductInventoryList" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="aspajax" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .head2
        {
            background-color: #6B696B;
            color: #FFFFFF;
            font-weight: bolder;
            font-size: small;
            
        }
        .row
        {
            background-color: #FFE4B5;
            font-size: small;
        } 
    </style>
    <div>&nbsp;</div>

      <table style="width: 1100px">
          <tr>
           <asp:Button ID="lb_ShopExport" runat="server" Text="匯出憑證Excel" OnClick="CheckShop" />
                      
          </tr>
        <tr>
            <td colspan="5">
                <asp:GridView ID="gv_Shop" runat="server" AutoGenerateColumns="true"  
                 HeaderStyle-CssClass = "head2" RowStyle-CssClass="row" Width="1100"
                 OnRowDataBound="gv_Bound" >
<%--                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <table style="width: 1100px">
                                    <tr>
                                        <td class="head2">訂單編號</td>
                                        <td class="head2">商品名稱</td>
                                        <td class="head2">姓名</td>
                                        <td class="head2">憑證編號</td>
                                        <td class="head2">使用者編號</td>    
                                        <td class="head2"><asp:Label ID="lb_shop1" runat="server"></asp:Label></td>
                                        <td class="head2"><asp:Label ID="lb_shop2" runat="server" ></asp:Label></td>
                                        <td class="head2"><asp:Label ID="lb_shop3" runat="server" ></asp:Label></td>
                                        <td class="head2"><asp:Label ID="lb_shop4" runat="server" ></asp:Label></td>
                                        <td class="head2"><asp:Label ID="lb_shop5" runat="server" ></asp:Label></td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="row"><%# ((ViewHiDealProductShopInfo)(Container.DataItem)).OrderId%></td>
                                    <td class="row"><%# ((ViewHiDealProductShopInfo)(Container.DataItem)).ProductName%></td>
                                    <td class="row"><%# ((ViewHiDealProductShopInfo)(Container.DataItem)).Name%></td>
                                    <td class="row"><%# ((ViewHiDealProductShopInfo)(Container.DataItem)).PrefixSequence %></td>
                                    <td class="row"><%# ((ViewHiDealProductShopInfo)(Container.DataItem)).Code%></td>
                                    <td class="row"><%# ((ViewHiDealProductShopInfo)(Container.DataItem)).OptionName1 %></td>
                                    <td class="row"><%# ((ViewHiDealProductShopInfo)(Container.DataItem)).OptionName2 %></td>
                                    <td class="row"><%# ((ViewHiDealProductShopInfo)(Container.DataItem)).OptionName3 %></td>
                                    <td class="row"><%# ((ViewHiDealProductShopInfo)(Container.DataItem)).OptionName4 %></td>
                                    <td class="row"><%# ((ViewHiDealProductShopInfo)(Container.DataItem)).OptionName5 %></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>--%>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <div>&nbsp;</div>
      <table style="width: 1100px">
        <tr>
            <asp:Button ID="lb_HouseExport" runat="server" Text="匯出宅配Excel" OnClick="CheckHouse" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="lbShippedTime" runat="server" Text="出貨回覆日:"></asp:Label>
            <asp:TextBox ID="tbShippedDate" runat="server"></asp:TextBox>
            <cc1:CalendarExtender ID="ceShippedDate" TargetControlID="tbShippedDate" runat="server"></cc1:CalendarExtender>
            <asp:Button ID="btSaveTime" runat="server" Text="儲存日期" onclick="btSaveTime_Click" />
            <asp:Label ID="lbMessage" runat="server" ForeColor="red"></asp:Label>
        </tr>
        <tr>
            <td colspan="5">               
                <asp:GridView ID="gv_House" runat="server" AutoGenerateColumns="true" 
                 HeaderStyle-CssClass = "head2" RowStyle-CssClass="row" Width="1100"
                 OnRowDataBound="gv_Bound"  >
                   <%-- <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <table style="width: 1100px">
                                    <tr>
                                    <td class="head2">訂單編號</td>
                                        <td class="head2">購買人</td>
                                        <td class="head2">收件人</td>
                                        <td class="head2">收件人電話</td>
                                        <td class="head2">購買數量</td>
                                        <td class="head2">收件地址</td>
                                        <td class="head2">商品名稱</td>
                                        <td class="head2"><asp:Label ID="lb_House1" runat="server" ></asp:Label></td>
                                        <td class="head2"><asp:Label ID="lb_House2" runat="server" ></asp:Label></td>
                                        <td class="head2"><asp:Label ID="lb_House3" runat="server" ></asp:Label></td>
                                        <td class="head2"><asp:Label ID="lb_House4" runat="server" ></asp:Label></td>
                                        <td class="head2"><asp:Label ID="lb_House5" runat="server" ></asp:Label></td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="row"><%# ((ViewHiDealProductHouseInfo)(Container.DataItem)).OrderId%></td>
                                    <td class="row"><%# ((ViewHiDealProductHouseInfo)(Container.DataItem)).Name%></td>
                                    <td class="row"><%# ((ViewHiDealProductHouseInfo)(Container.DataItem)).AddresseeName%></td>
                                    <td class="row"><%# ((ViewHiDealProductHouseInfo)(Container.DataItem)).AddresseePhone%></td>
                                    <td class="row"><%# ((ViewHiDealProductHouseInfo)(Container.DataItem)).ItemQuantity%></td>
                                    <td class="row"><%# ((ViewHiDealProductHouseInfo)(Container.DataItem)).DeliveryAddress%></td>
                                    <td class="row"><%# ((ViewHiDealProductHouseInfo)(Container.DataItem)).ProductName%></td>
                                    <td class="row"><%# ((ViewHiDealProductHouseInfo)(Container.DataItem)).OptionName1%></td>
                                    <td class="row"><%# ((ViewHiDealProductHouseInfo)(Container.DataItem)).OptionName2%></td>
                                    <td class="row"><%# ((ViewHiDealProductHouseInfo)(Container.DataItem)).OptionName3%></td>
                                    <td class="row"><%# ((ViewHiDealProductHouseInfo)(Container.DataItem)).OptionName4%></td>
                                    <td class="row"><%# ((ViewHiDealProductHouseInfo)(Container.DataItem)).OptionName5%></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>--%>
                </asp:GridView>
            </td>
        </tr>
    </table>

</asp:Content>
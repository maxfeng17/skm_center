﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="HidealOrderDetailList.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.piinlife.HidealOrderDetailList" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Src="../Controls/AuditBoard.ascx" TagName="AuditBoard" TagPrefix="uc2" %>
<%@ Register Src="../Controls/EinvoiceControl.ascx" TagName="EinvoiceControl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <style type="text/css">
        .style6
        {
            width: 183px;
        }
        .style9
        {
            width: 173px;
        }
        .style15
        {
            width: 95px;
        }
        .style35
        {
            width: 84px;
        }
        .style36
        {
            width: 105px;
        }
        .style37
        {
            width: 122px;
        }
        .style38
        {
            width: 124px;
        }
        .style41
        {
            width: 118px;
        }
        .style48
        {
            width: 40px;
        }
        .style50
        {
            width: 48px;
        }
        .style51
        {
            width: 39px;
        }
        .style52
        {
            width: 32px;
        }
        .style63
        {
            width: 222px;
        }
        .style64
        {
            width: 127px;
        }
        .style67
        {
            width: 800px;
        }
        .style69
        {
            width: 350px;
        }
        .style70
        {
            width: 120px;
        }
        .style71
        {
            width: 137px;
        }
    </style>
    <script src="//ajax.microsoft.com/ajax/jquery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("jqueryui", "1.8");
    </script>
    <script type="text/javascript">
        $().ready(function () {
            $('.datepick').datepicker({ dateFormat: 'yy/mm/dd', minDate: new Date('<%=ShipStartDate.ToString("yyyy/MM/dd") %>') });
            //維護客服紀錄時 則按下runat server按鈕 都需跳回客服紀錄位置頁面 故鎖定scroll top位置
            window.scrollTo(0, $('input[id*=HiddenUseServiceFlag]').val());
            $('#<%=tbShipMemo.ClientID%>').attr('maxlength', '30');
        });

        function SetReturnCashId(returnId) {
            $("#<%=hidReturnedId.ClientID %>").val(returnId);
        }

        function CheckInvoiceInfo() {
            var ct = true;
            $('#CTError').css('visibility', 'hidden');
            $('#CTitleError').css('visibility', 'hidden');
            $('#CNameError').css('visibility', 'hidden');
            $('#CAddressError').css('visibility', 'hidden');

            if (jQuery.trim($("#txtCtitle").val()) == "") {
                $('#CTitleError').css('visibility', 'visible');
                ct = false;
            }
            if (jQuery.trim($("#txtCName").val()) == "") {
                $('#CNameError').css('visibility', 'visible');
                ct = false;
            }
            if (jQuery.trim($("#txtCAddress").val()) == "") {
                $('#CAddressError').css('visibility', 'visible');
                ct = false;
            }

            if (jQuery.trim($("#txtCT").val()) == "") {
                $('#CTError').css('visibility', 'visible');
                $('#CTError').text('請填入統編');
                ct = false;
            }
            else if (jQuery.trim($("#txtCT").val()).length != 8 || isNaN(jQuery.trim($("txtCT").val()))) {
                $('#CTError').css('visibility', 'visible');
                $('#CTError').text('請填入八碼數字');
                ct = false;
            }

            return ct;
        }

        function CheckData() {
            var Category_value = $('select[id*=ddlHidealCategory]').val();
            var SubCategory_value = $('input[id*=hidSubCategory_SelectValue]').val();

            if (Category_value == '請選擇') {
                alert('請選擇分類!');
                return false;
            } else if (SubCategory_value == '請選擇') {
                alert('請選擇子分類!');
                return false;
            } else if ($('textarea[id*=txtIMessage]').val() == '') {
                alert('請輸入問題說明!');
                return false;
            } else {
                btnSetServiceMsg_Click();
                return true;
            }
        }

        function bAddServiceLog_Click() {
            //按下[新增客服紀錄]按鈕 須保持視窗停留於客服紀錄 紀錄目前scrolltop位置
            $('input[id*=HiddenUseServiceFlag]').val(document.body.scrollTop);
        }

        function btnSetServiceMsg_Click() {
            //按下客服紀錄維護畫面[確認]按鈕 須保持視窗停留於客服紀錄 紀錄目前scrolltop位置
            $('input[id*=HiddenUseServiceFlag]').val(document.body.scrollTop);
        }

        function btnAddServiceMsgClose_Click() {
            //按下客服紀錄維護畫面[退出]按鈕 毋須保持視窗停留於客服紀錄 0:scrolltop位置
            $('input[id*=HiddenUseServiceFlag]').val(0);
        }

        function buildSubCategory() {
            var ddlMain = $("#<%=ddlCategory.ClientID%>");
            var ddlMainSelected = $("#<%=ddlCategory.ClientID%> :selected");
            var ddlSub = $("#<%=ddlSubCategory.ClientID%>");

            $.ajax({
                type: "POST",
                url: "HidealOrderDetailList.aspx/GetServiceMessageCategory",
                data: "{parentId: " + ddlMain.val() + "}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    ddlSub.empty();
                    ddlSub.append($('<option></option>').val('0').html('請選擇'));
                    $.each(cates, function (i, item) {
                        ddlSub.append($('<option></option>').val(item.CategoryId).html(item.CategoryName));
                    })
                },
                error: function () {
                    ddlSub.empty();
                    ddlSub.append($('<option></option>').val('0').html('請選擇'));
                }
            });
            if (ddlMainSelected.text() == "會員(帳號)問題" || ddlMainSelected.text() == "其他問題" || ddlMainSelected.text() == "請選擇") {
                $('.OrderText').css('display', 'none');
            } else {
                $('.OrderText').css('display', '');
            }
        }
        //在Save客服紀錄的分類時，無法抓取到子分類下拉式選單的值，所以存在一個hidden去寫資料.
        function setSubCategory() {
            var ddlSub = $("#<%=ddlSubCategory.ClientID%> :selected");
            var hdSub = $("#<%=hdSubCategory.ClientID%>");
            hdSub.val(ddlSub.val());
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="lbMessage" runat="server" ForeColor="Red"></asp:Label>
    <asp:Panel ID="divOrderDetail" runat="server" Width="900px">
        <table style="border: double 3px black; width: 900px;">
            <tr>
                <th colspan="4" style="background-color: Gray" class="style67">
                    PiinLife 訂單基本資料
                </th>
            </tr>
            <tr>
                <td style="background: #C0C0C0" class="style64">
                    訂單編號
                </td>
                <td class="style63">
                    <asp:HiddenField ID="hidOrderPk" runat="server" />
                    <asp:HiddenField ID="hidOrderGuid" runat="server" />
                    <asp:Label ID="lblOrderId" runat="server"></asp:Label>
                </td>
                <td style="background: #C0C0C0" class="style64">
                    購買時間
                </td>
                <td>
                    <asp:Label ID="lblCreateTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background: #C0C0C0" class="style70">
                    賣家
                </td>
                <td class="style63">
                    <asp:HyperLink ID="hkSellerName" runat="server" Target="_blank"></asp:HyperLink>
                </td>
                <td style="background: #C0C0C0" class="style64">
                    最後修改人
                </td>
                <td>
                    <asp:Label ID="lblModifyId" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background: #C0C0C0" class="style70">
                    買家
                </td>
                <td class="style63">
                    <asp:HiddenField ID="hidMemberEmail" runat="server" />
                    <asp:HyperLink ID="hkUserName" runat="server" Target="_blank"></asp:HyperLink>
                </td>
                <td style="background: #C0C0C0" class="style64">
                    最後修改日期
                </td>
                <td>
                    <asp:Label ID="lblModifyTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background: #C0C0C0" class="style70">
                    買家電話
                </td>
                <td class="style63">
                    <asp:Label ID="lblCompanyTel" runat="server"></asp:Label>
                </td>
                <td colspan="2" rowspan="11">
                    <table style="width: 303px; border: 1px groove black;">
                        <tr>
                            <td class="style52">
                            </td>
                            <td class="style50" style="font-weight: bold; text-align: center;">
                                單價
                            </td>
                            <td class="style51" style="font-weight: bold; text-align: center;">
                                數量
                            </td>
                            <td class="style48" style="font-weight: bold; text-align: right;">
                                小計
                            </td>
                        </tr>
                        <tr>
                            <td class="style52" style="background: #C0C0C0">
                                商品
                            </td>
                            <td class="style50" style="text-align: center;">
                                <asp:Label ID="lblProductUnitPrice" runat="server"></asp:Label>
                            </td>
                            <td class="style51" style="text-align: center;">
                                <asp:Label ID="lblProductItemQuantity" runat="server"></asp:Label>
                            </td>
                            <td class="style48" style="text-align: right;">
                                <asp:Label ID="lblProductDetailTotalAmt" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style52" style="background: #C0C0C0">
                                運費
                            </td>
                            <td colspan="3" style="text-align: right;">
                                <asp:Label ID="lblFreightUnitPrice" runat="server" Text="0"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style52" style="background: #C0C0C0">
                                總額
                            </td>
                            <td colspan="3" style="text-align: right; color: Red; font-weight: bold;">
                                NT$<asp:Label ID="lblOrderTotalAmount" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table style="width: 303px;">
                        <tr>
                            <td colspan="4" style="background: #C0C0C0; text-align: center;">
                                付款明細
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                PEZ購物金
                            </td>
                            <td style="text-align: right;">
                                <asp:Label ID="lblPcash" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                17Life購物金
                            </td>
                            <td style="text-align: right;">
                                <asp:Label ID="lblScash" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                17Life紅利金
                            </td>
                            <td style="text-align: right;">
                                <asp:Label ID="lblBcash" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                折價券
                            </td>
                            <td style="text-align: right;">
                                <asp:Label ID="lblDiscountAmount" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                刷卡
                            </td>
                            <td style="text-align: right;">
                                <asp:Label ID="lblCreditCardAmt" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;">
                                ATM
                            </td>
                            <td style="text-align: right;">
                                <asp:Label ID="lblAtmAmount" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="background: #C0C0C0" class="style70">
                    買家手機
                </td>
                <td class="style9">
                    <asp:Label ID="lblMobile" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background: #C0C0C0" class="style70">
                    商品名稱
                </td>
                <td class="style69">
                    <asp:HiddenField ID="hidProductId" runat="server" />
                    <asp:HyperLink ID="hkProductName" runat="server" Target="_blank"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td style="background: #C0C0C0" class="style64">
                    交貨型態
                </td>
                <td>
                    <asp:HiddenField ID="hidDeliveryType" runat="server" />
                    <asp:Label ID="lblDeliveryType" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background: #C0C0C0" class="style52">
                    品項
                </td>
                <td class="style6" style="vertical-align: middle;">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblCategoryName1" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblCategoryName2" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblCategoryName3" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblCategoryName4" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblCategoryName5" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="background: #C0C0C0" class="style41">
                    收件人
                </td>
                <td>
                    <asp:Label ID="lblAddressName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background: #C0C0C0" class="style41">
                    收件人連絡電話
                </td>
                <td>
                    <asp:Label ID="lblAddressPhone" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background: #C0C0C0" class="style41">
                    收件地址
                </td>
                <td>
                    <asp:Label ID="lblDeliveryAddress" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="background: #C0C0C0" class="style41">
                    商品狀態
                </td>
                <td>
                    <asp:Label ID="lblProductSituation" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="background: #C0C0C0" class="style41">
                    請款狀態
                </td>
                <td>
                    <asp:Label ID="lbChargingSituation" runat="server" />
                </td>
            </tr>
        </table>
        <table style="border: double 3px black; width: 900px;">
            <tr>
                <th style="background: #C0C0C0" class="style70" colspan="2">
                    發票
                </th>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="cbHaveInvoice" runat="server" Text="是否有發票" Enabled="false" Checked="false">
                    </asp:CheckBox>
                    <asp:CheckBox ID="cbNoTax" runat="server" Text="免稅檔次" Enabled="false"></asp:CheckBox>
                    <asp:CheckBox ID="CbNoInvoiceCreate" runat="server" Text="不開立發票" Enabled="false" Visible="False">
                    </asp:CheckBox>
                    <asp:CheckBox ID="cbx_EinvoicePapered" runat="server" Text="已印製紙本" Enabled="false">
                    </asp:CheckBox>
                    <asp:Label ID="lab_EinvoiceRequested" runat="server" ForeColor="Gray"></asp:Label>
                    <asp:Label ID="lab_EinvoicePapered" runat="server" ForeColor="Gray"></asp:Label>
                    <asp:CheckBox ID="cbx_EinvoiceWinner" runat="server" Text="此發票中獎" Enabled="false" Visible="False">
                    </asp:CheckBox>
                    <br />
                    <asp:CheckBox ID="cbAllowance" runat="server" Text="折讓單已寄回" Visible="False"></asp:CheckBox>
                    <asp:CheckBox ID="cbInvoice" runat="server" Text="發票已寄回" Visible="False"></asp:CheckBox>
                    <asp:CheckBox ID="cbRefundFormBack" runat="server" Text="退貨申請書已寄回" Visible="False"></asp:CheckBox>
                    &nbsp;&nbsp;
                    <asp:HyperLink  ID="hl_ShowReturnApplicationForm" runat="server" Visible="False"  >匯出</asp:HyperLink>
                    &nbsp;&nbsp;
                    <asp:LinkButton ID="lb_SendReturnApplicationFormNotification" runat="server"  OnCommand="lb_SendReturnApplicationFormNotification_Click" Visible="False" >寄出</asp:LinkButton>
                    &nbsp;&nbsp;
                    <asp:HiddenField ID="hid_RefundId" runat="server" />                    
                </td>
            </tr>
            <tr>
                <td colspan="2" style="margin-top:20px">
                    <asp:Panel ID="pnInvoice" runat="server">
                        <table>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="lblInvoiceVersion" runat="server" style="font-weight:bold" />
                                </td>
                            </tr>
                            <tr style="height:26px">
                                <td style="width:80px; text-align:right">發票註記：</td>                                    
                                <td>
                                    <asp:TextBox ID="tbInvoiceMesssage" runat="server" Width="150" MaxLength="20" />
                                </td>
                            </tr>
                            <tr style="height:26px">
                                <td style="width:80px; text-align:right">發票號碼：</td>
                                <td><asp:Label ID="lbl_EinvoiceNum" runat="server"></asp:Label>
                                    <asp:HiddenField ID="hif_EinvoiceId" runat="server"></asp:HiddenField></td>
                            </tr>
                        <uc2:EinvoiceControl ID="invCtrl" runat="server" />

                   <%--         <tr style="height:26px">
                                <td style="width:80px; text-align:right">
                                    收件人：
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCName" runat="server" Width="100" MaxLength="50" Enabled="false" ClientIDMode="Static" />
                                    <span id="CNameError" class="Usn_red" style="visibility: hidden; color: Red; font-weight: normal">
                                        請填入收件者</span>
                                </td>
                            </tr>
                            <tr style="height:26px">
                                <td style="text-align:right">
                                    收件地址：
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCAddress" runat="server" Width="250" MaxLength="250" Enabled="false" ClientIDMode="Static" />
                                    <span id="CAddressError" class="Usn_red" style="visibility: hidden; color: Red; font-weight: normal">
                                        請填入收件地址</span>
                                </td>
                            </tr>
                            <tr style="height:26px">
                                <td style="text-align:right">
                                    統編：
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCT" runat="server" Width="100" MaxLength="8" Enabled="false" ClientIDMode="Static" />
                                    <span id="CTError" class="Usn_red" style="visibility: hidden; color: Red; font-weight: normal">
                                        請填入統編</span>
                                </td>
                            </tr>
                            <tr style="height:26px">
                                <td style="text-align:right">
                                    抬頭：
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCtitle" runat="server" Width="100" Enabled="false" ClientIDMode="Static" />
                                    <span id="CTitleError" class="Usn_red" style="visibility: hidden; color: Red; font-weight: normal">
                                        請填入抬頭</span>
                                </td>
                            </tr>--%>
                            <tr>
                                <td>
                                    <asp:Button ID="btnInvUpdate" runat="server" Text="修改發票資料"
                                         OnClientClick="if (CheckInvoiceInfo()==false) {return false;}" OnClick="BtnInvoiceUpdate" />
                                </td>
                            </tr>                            
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnUpdateEInvoice" runat="server" Text="儲存" OnClick="BtnUpdateReturned" Visible="False"/>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />    
    <asp:Panel ID="divInvoiceCouponList" runat="server" Width="900px" Visible="False">
        <table style="border: double 3px black; width: 1000px;">
            <tr>
                <th style="background-color: Gray">
                    憑證資訊
                </th>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvInvoiceCouponList" runat="server" OnRowDataBound="gvInvoiceCouponList_RowDataBound" OnRowCommand="gvInvoiceCouponList_RowCommand"
                        AutoGenerateColumns="false" EnableViewState="true" Width="1000px">
                        <Columns>
                            <asp:TemplateField HeaderText="憑證編號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblSequence" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="確認碼" DataField="CouponCode" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="60px" />
                            <asp:TemplateField HeaderText="狀態" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="90px">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="發票號碼" DataField="InvoiceNumber" ItemStyle-HorizontalAlign="Center" NullDisplayText="未開立發票"
                                ItemStyle-Width="100px" />
                            <asp:TemplateField HeaderText="中獎發票" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="70px">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cb_InvoiceWinning" runat="server" Enabled="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="折讓單" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hl_ShowDiscountSingleForm" runat="server">匯出</asp:HyperLink>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lb_SendDiscountSingleFormNotification" runat="server" CommandName="SendDiscountSingleFormNotification" 
                                         CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">寄出</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="折讓單已回" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="90px">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cb_isReturnDiscountSingleForm" runat="server" Enabled="False" />
                                    &nbsp;&nbsp;
                                    <asp:Button ID="bt_ChangeReturnDiscountSingleFormStatus" runat="server" Text="改變" CommandName="SetInvoiceMailbackAllowance" 
                                         CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="發票已回" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="90px">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cb_isReturnEInvoice" runat="server" Enabled="False" />
                                    &nbsp;&nbsp;
                                    <asp:Button ID="bt_ChangeReturnEInvoiceStatus" runat="server" Text="改變" CommandName="SetInvoiceMailbackPaper" 
                                         CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="發票" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblEInvoiceId" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="預約申請時間">
                                <ItemTemplate>
                                    <asp:Label ID="lblReservationCreateDate" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="預約時間">
                                <ItemTemplate>
                                    <asp:Label ID="lblReservationDateTime" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="預約狀態">
                                <ItemTemplate>
                                    <asp:Label ID="lblReservationStatus" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:HiddenField ID="hfIsHaveLockReservationRecord" Value="false" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    
    <asp:Panel ID="divInvoiceGoodsList" runat="server" runat="server" Width="900px" Visible="False">
        <table style="border: double 3px black; width: 900px;">
            <tr>
                <th style="background-color: Gray">
                    商品資訊
                </th>
            </tr>
            <tr>
                <td>    
                <asp:GridView ID="gvInvoiceGoodsList" runat="server" OnRowDataBound="gvInvoiceGoodsList_RowDataBound" OnRowCommand="gvInvoiceGoodsList_RowCommand"
                        AutoGenerateColumns="false" EnableViewState="true" Width="900px">
                        <Columns>
                            <asp:TemplateField HeaderText="商品名稱" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrderItem" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="商品總數" DataField="OrderItemQuantity" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="100px" />
                            <asp:TemplateField HeaderText="狀態" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:BoundField HeaderText="發票號碼" DataField="InvoiceNumber" ItemStyle-HorizontalAlign="Center" NullDisplayText="未開立發票"
                                ItemStyle-Width="60px" />
                            <asp:TemplateField HeaderText="中獎發票" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cb_InvoiceWinning" runat="server" Enabled="False" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="折讓單" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hl_ShowDiscountSingleForm" runat="server">匯出</asp:HyperLink>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lb_SendDiscountSingleFormNotification" runat="server" CommandName="SendDiscountSingleFormNotification" 
                                         CommandArgument="<%# ((GridViewRow) Container).RowIndex %>">寄出</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="折讓單已回" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cb_isReturnDiscountSingleForm" runat="server" Enabled="False"/>
                                    &nbsp;&nbsp;
                                    <asp:Button ID="bt_ChangeReturnDiscountSingleFormStatus" runat="server" Text="改變" CommandName="SetInvoiceMailbackAllowance" 
                                         CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="發票已回" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:CheckBox ID="cb_isReturnEInvoice" runat="server" Enabled="False" />
                                    &nbsp;&nbsp;
                                    <asp:Button ID="bt_ChangeReturnEInvoiceStatus" runat="server" Text="改變" CommandName="SetInvoiceMailbackPaper" 
                                         CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="發票" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblEInvoiceId" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>                
                    </asp:GridView>
                </td>
            </tr>
        </table>                        
    </asp:Panel>
    <br />
    <asp:Panel ID="divButton" runat="server" Width="900px">
        <asp:Label runat="server" ID="labNoRefund" ForeColor="red"></asp:Label>
        <asp:Button ID="btnMakeReturnForm" runat="server" Text="自動退貨" OnClick="BtnMakeReturnFormOnClick" />
        <asp:CheckBox ID="cbBackSCash" runat="server" Text="只退購物金" />
        <asp:Button ID="btnReturnAllPass" runat="server" Text="產生退貨單並強制退貨" OnClick="BtnReturnAllPassOnClick" />
        <asp:Button ID="btnReturnCancel" runat="server" Text="取消退貨" OnClick="BtnReturnCancelOnClick" />
        <asp:Literal ID="litRefundMsg" runat="server"></asp:Literal>
        <br />
        <asp:Button ID="bAddServiceLog" runat="server" Text="新增客服記錄" CssClass="ab_b" OnClick="bAddServiceLog_Click"
            OnClientClick="bAddServiceLog_Click();" />
        <asp:Button ID="bServiceIntegrate" runat="server" Text="客服整合系統" CssClass="ab_b" OnClick="bServiceIntegrate_Click" />
        <asp:HiddenField ID="HiddenUseServiceFlag" runat="server" />
        <%--維護客服紀錄時 須記錄目前scroll-top位置以保持視窗停留於客服紀錄位置--%>
    </asp:Panel>
    <br />
    <asp:Panel ID="divDeliverManage" runat="server" Width="1000px">
        <table width="100%" style="border: double 3px black; text-align: center;">
            <tr>
                <th style="background-color: Gray" colspan="11">
                    出貨資訊
                </th>
            </tr>
            <tr>
                <td>
                    <table width="100%" id="addDeliver" runat="server">
                        <tr>
                            <th style="background-color: Gray">
                                出貨日期
                            </th>
                            <th style="background-color: Gray">
                                運單編號
                            </th>
                            <th style="background-color: Gray">
                                物流公司
                            </th>
                            <th style="background-color: Gray">
                                出貨備註
                            </th>
                            <th style="background-color: Gray">
                                編輯
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="tbAddShipTime" runat="server" CssClass="datepick" MaxLength="10"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="tbAddShipNo" runat="server" MaxLength="30"></asp:TextBox>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAddShipCompanyId" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:TextBox ID="tbShipMemo" TextMode="MultiLine" runat="server" MaxLength="30" Rows="5"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Button ID="addDeliverData" runat="server" Text="新增" OnClick="addDeliverData_Click" />
                            </td>
                        </tr>
                    </table>
                    <asp:GridView ID="gvDeliver" runat="server" AutoGenerateColumns="False" OnRowEditing="gvDeliver_RowEditing"
                        OnRowCancelingEdit="gvDeliver_RowCancelingEdit" Width="100%" DataKeyNames="OrderShipId"
                        OnRowDataBound="gvDeliver_RowDataBound" OnRowUpdating="gvDeliver_RowUpdating"
                        OnRowDeleting="gvDeliver_RowDeleting">
                        <Columns>
                            <asp:TemplateField HeaderText="出貨日期">
                                <ItemTemplate>
                                    <asp:Label ID="lbShipTime" Text='<%# Eval("ShipTime", "{0:yyyy-MM-dd}") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="tbShipTime" CssClass="datepick" Text='<%# Eval("ShipTime", "{0:yyyy-MM-dd}") %>'
                                        runat="server"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="運單編號">
                                <ItemTemplate>
                                    <asp:Label ID="lbShipNo" Text='<%# Bind("ShipNo") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="tbShipNo" Text='<%# Bind("ShipNo") %>' runat="server"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="物流公司">
                                <ItemTemplate>
                                    <asp:Label ID="lbShipCompanyId" Text='<%# Bind("ShipCompanyId") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlShipCompanyId" runat="server">
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="出貨備註">
                                <ItemTemplate>
                                    <asp:Label ID="lbShipMemo" Text='<%# Bind("ShipMemo") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="tbShipMemo" TextMode="MultiLine" Rows="5" Text='<%# Bind("ShipMemo") %>' runat="server"></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="最後修改日期">
                                <ItemTemplate>
                                    <asp:Label ID="lbModifyTime" Text='<%# Eval("OrderShipModifyTime", "{0:yyyy-MM-dd HH:mm:ss}") %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lbModifyTime" Text='<%# Eval("OrderShipModifyTime", "{0:yyyy-MM-dd HH:mm:ss}") %>'
                                        runat="server"></asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="最後修改人">
                                <ItemTemplate>
                                    <asp:Label ID="lbModifyId" Text='<%# Bind("OrderShipModifyId") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lbModifyId" Text='<%# Bind("OrderShipModifyId") %>' runat="server"></asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="編輯">
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" CausesValidation="True" CommandName="Update"
                                        Text="儲存"></asp:Button>
                                    &nbsp;<asp:Button ID="btnCancel" runat="server" CausesValidation="False" CommandName="Cancel"
                                        Text="取消"></asp:Button>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                        Text="編輯"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                        OnClientClick="return confirm('確定刪除這筆出貨資訊?');" Text="刪除"></asp:Button>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle BackColor="Gray" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <asp:Panel ID="divAddServiceLog" runat="server" Width="658px">
        <asp:Label ID="lblMessage" runat="server" ForeColor="Red" EnableViewState="false" />
        <table width="100%" style="border: double 3px black">
            <tr>
                <td style="background-color: Gray" colspan="4">
                    新增客服紀錄
                </td>
            </tr>
            <tr>
                <td align="right" class="style13">
                    姓名
                </td>
                <td align="left">
                    <asp:Label ID="lblIName" runat="server"></asp:Label>
                </td>
                <td align="right" class="style13">
                    Email
                </td>
                <td align="left">
                    <asp:Label ID="lblIEmail" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right" class="style13">
                    電話
                </td>
                <td align="left">
                    <asp:Label ID="lblIPhone" runat="server"></asp:Label>
                </td>
                <td align="right" class="style13">
                    OrderId
                </td>
                <td align="left">
                    <asp:Label ID="lblIOrder" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right" class="style13">
                    形式
                </td>
                <td>
                    <asp:DropDownList ID="ddlItype" runat="server">
                        <asp:ListItem>來信</asp:ListItem>
                        <asp:ListItem Selected="True">來電</asp:ListItem>
                        <asp:ListItem>外撥</asp:ListItem>
                        <asp:ListItem>去信</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td align="right" class="style13">
                    分類
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddlCategory" runat="server" onchange="buildSubCategory();">
                        <asp:ListItem>請選擇</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlSubCategory" runat="server" onchange="setSubCategory();">
                        <asp:ListItem>請選擇</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="hdSubCategory" runat="server" htmlreadonly="true" Width="30px"
                        Style="display: none;" />
                </td>
            </tr>
            <tr>
                <td align="right" class="style13">
                    狀態
                </td>
                <td>
                    <asp:DropDownList ID="ddlIStatus" runat="server">
                        <asp:ListItem Value="wait">待處理</asp:ListItem>
                        <asp:ListItem Value="process">處理中</asp:ListItem>
                        <asp:ListItem Value="complete">完成</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td colspan="2">
                    <asp:HiddenField ID="HiddenOrderGuid" runat="server" />
                    <asp:HiddenField ID="HiddenAddType" Value="1" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="right">
                    轉件項目
                </td>
                <td align="left" colspan="3">
                    <asp:RadioButtonList ID="rdlWorkType" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="商品" Value="0"></asp:ListItem>
                        <asp:ListItem Text="好康" Value="1"></asp:ListItem>
                        <asp:ListItem Text="退貨" Value="2"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td align="right" class="style13">
                    問題說明
                </td>
                <td align="left" colspan="3">
                    <asp:TextBox ID="txtIMessage" runat="server" TextMode="MultiLine" Height="130px"
                        Width="520px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" style="text-align: center">
                    <asp:Button ID="btnSetServiceMsg" runat="server" Text="確認" OnClick="btnSetServiceMsg_Click"
                        OnClientClick="return CheckData();" />
                    <asp:Button ID="btnAddServiceMsgClose" runat="server" Text="退出" OnClick="btnAddServiceMsgClose_Click"
                        OnClientClick="btnAddServiceMsgClose_Click();" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <table width="800" style="border: double 3px black">
        <tr>
            <th style="background-color: Gray">
                記錄
            </th>
        </tr>
        <tr>
            <td>
                <uc2:AuditBoard ID="abHidealOrderLog" runat="server" Width="100%" EnableAdd="true"
                    ShowAllRecords="true" ReferenceType="Order" />
            </td>
        </tr>
    </table>
    <br />
    <asp:Panel ID="divReturnList" runat="server" Width="900px">
        <table style="border: double 3px black; width: 900px;">
            <tr>
                <th style="background-color: Gray">
                    退貨單
                </th>
            </tr>
            <tr>
                <td>
                    <asp:HiddenField runat="server" ID="hidReturnedId" />
                    <asp:GridView ID="gvReturnLsit" runat="server" OnRowDataBound="gvReturnLsit_RowDataBound"
                        OnRowCommand="gvReturnLsit_RowCommand" AutoGenerateColumns="false" EnableViewState="true"
                        Width="900px" CellPadding="2">
                        <HeaderStyle Font-Size="Smaller" />
                        <Columns>
                            <asp:BoundField HeaderText="單號" DataField="Id" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="50px" />
                            <asp:TemplateField HeaderText="狀態" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                    <asp:Button ID="btnReturnComplete" runat="server" CausesValidation="False" CommandName="ReturnComplete"
                                        Text="核准退貨申請" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="金額" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="200px">
                                <ItemTemplate>
                                    <asp:Label ID="lblCash" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="建立人員" DataField="CreateId" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="100px" />
                            <asp:BoundField HeaderText="建立日期" DataField="CreateTime" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="100px" DataFormatString="{0:yyyy/MM/dd HH:mm}" />
                            <asp:TemplateField HeaderText="刷退" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:Label ID="lblCashBack" runat="server"></asp:Label>
                                    <asp:Button ID="btnReturnCash" runat="server" CausesValidation="False" CommandName="ReturnCash"
                                        Text="購物金轉刷退" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="退貨原因" DataField="ReturnReason" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="220px" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

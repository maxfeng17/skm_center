﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="HidealOrderList.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.piinlife.HidealOrderList" %>

<%@ Register Src="~/ControlRoom/Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="aspajax" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="divSearch" runat="server">
        <table width="920">
            <tr>
                <td style="height: 31px;">
                    
                    <table style="border: double 3px black; width:760px">
                        <tr>
                            <th style="width: 714px;">
                                PiinLife訂單搜尋
                            </th>
                        </tr>
                        <tr>
                            <td style="width: 714px;">
                                <asp:DropDownList ID="ddlFilter" runat="server" Width="103px">
                                    
                                </asp:DropDownList>
                                <asp:TextBox ID="txtFilter" runat="server" Width="145px"></asp:TextBox><br />
                                購買日期:
                                <asp:TextBox ID="txtCreateTimeS" runat="server" Columns="8" />
                                <cc1:CalendarExtender ID="ceDS" TargetControlID="txtCreateTimeS" runat="server">
                                </cc1:CalendarExtender>
                                至
                                <asp:TextBox ID="txtCreateTimeE" runat="server" Columns="8" />
                                <cc1:CalendarExtender ID="ceDE" TargetControlID="txtCreateTimeE" runat="server">
                                </cc1:CalendarExtender>
                                &nbsp;<asp:Button ID="btnSearch" runat="server" Text="送出" OnClick="btnSearch_Click">
                                </asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="divOrder" runat="server">
        <asp:GridView ID="gvHidealOrderList" runat="server" OnRowDataBound="gvHidealOrderList_RowDataBound"
            AllowSorting="True" GridLines="Horizontal" ForeColor="Black" CellPadding="4"
            BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE" BackColor="White"
            EmptyDataText="無符合的資料" AutoGenerateColumns="False" Font-Size="Small" Width="763px"
            EnableViewState="False">
            <FooterStyle BackColor="#CCCC99"></FooterStyle>
            <RowStyle BackColor="#F7F7DE" Height="30px"></RowStyle>
            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
            <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="30px">
            </HeaderStyle>
            <Columns>
                <asp:TemplateField HeaderText="訂單編號">
                    <ItemTemplate>
                        <asp:HyperLink ID="hkOrderId" runat="server" Target="_blank"></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="15%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="檔號">
                    <ItemTemplate>
                        <asp:HyperLink ID="hkHiDealId" runat="server" Target="_blank"></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="15%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="會員">
                    <ItemTemplate>
                        <asp:HyperLink ID="hkMemberName" runat="server" Target="_blank"></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="15%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="商品名稱">
                    <ItemTemplate>
                        <asp:HyperLink ID="hkProductName" runat="server" Target="_blank"></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="35%" />
                </asp:TemplateField>
                <asp:BoundField HeaderText="建立日期" DataField="CreateTime" DataFormatString="{0:yyyy/MM/dd HH:mm}">
                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField ReadOnly="True" DataField="TotalAmount" DataFormatString="{0:N0}" HtmlEncode="False"
                    HeaderText="總額">
                    <ItemStyle HorizontalAlign="Right" Width="5%" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="狀態">
                    <ItemTemplate>
                        <asp:Label ID="lblStatus" runat="server"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <uc1:Pager ID="ucHidealPager" runat="server" PageSize="15" ongetcount="GetHidealOrderCount"
            onupdate="HidealOrderUpdateHandler"></uc1:Pager>
    </asp:Panel>
</asp:Content>

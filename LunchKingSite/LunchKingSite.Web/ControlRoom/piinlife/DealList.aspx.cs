﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.ControlRoom.piinlife
{
    public partial class DealList : RolePage, IHiDealDealListView
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }        

        #region event

        public event EventHandler<DataEventArgs<HiDealDealListViewSearchRequest>> SearchClicked = null;
        #endregion

        #region property
        private HiDealDealListPresenter _presenter;
        public HiDealDealListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        #endregion property

        #region method
        public void ShowMessage(string message)
        {
            
        }
        public void ShowData(ViewHiDealSellerProductCollection dataCollection, int dataCount)
        {
            DataCount = dataCount;
            gvHidealDealList.DataSource = dataCollection;
            gvHidealDealList.DataBind();
        }
        #endregion

        #region class property
        public HiDealDealListViewSearchType SearchType
        {
            get
            {
                if (ddlFilter.SelectedValue == "0")
                {
                    return HiDealDealListViewSearchType.SellerName;
                }
                else if (ddlFilter.SelectedValue == "1")
                {
                    return HiDealDealListViewSearchType.DealName;
                }
                else if (ddlFilter.SelectedValue == "2")
                {
                    return HiDealDealListViewSearchType.DealId;
                }
                else
                {
                    return HiDealDealListViewSearchType.ProductId;
                }
            }
        }
        public HiDealDealListViewDealWorkingType DealWorkingType
        {
            get { return (HiDealDealListViewDealWorkingType)rdoDealWorkType.SelectedIndex; }
        }

        public int DataCount { get; set; }
        #endregion class property


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (Validation(SearchType, txtFilter.Text.Trim()))
            {
                SearchData(1);
                ucHidealPager.ResolvePagerView(1, true);
            }
        }

        protected void UpdateHandler(int pageNumber)
        {
            SearchData(pageNumber);
        }

        protected int GetCount()
        {
            return DataCount;
        }

        private void SearchData(int pageNum)
        {
            if (SearchClicked != null)
            {
                DateTime? startTimeBegin = null;
                DateTime? startTimeEnd = null;
                DateTime? endTimeBegin = null;
                DateTime? endTimeEnd = null;
                DateTime tmp;
                if (DateTime.TryParse(txtStartTimeS.Text, out tmp))
                {
                    startTimeBegin = tmp;
                }
                if (DateTime.TryParse(txtStartTimeE.Text, out tmp))
                {
                    startTimeEnd = tmp;
                }

                if (DateTime.TryParse(txtEndTimeS.Text, out tmp))
                {
                    endTimeBegin = tmp;
                }
                if (DateTime.TryParse(txtEndTimeE.Text, out tmp))
                {
                    endTimeEnd = tmp;
                }

                var data = new HiDealDealListViewSearchRequest();
                data.SearchType = SearchType;
                data.FilterString = txtFilter.Text.Trim();
                data.StartTimeBegin = startTimeBegin;
                data.StartTimeEnd = startTimeEnd;
                data.EndTimeBegin = endTimeBegin;
                data.EndTimeEnd = endTimeEnd;
                data.DealWorkingType = DealWorkingType;
                data.PageNum = pageNum;// ucHidealPager.PageCount;
                data.PageSize = ucHidealPager.PageSize;

                SearchClicked(this, new DataEventArgs<HiDealDealListViewSearchRequest>(data));
            }
        }

        protected void SetDdl()
        {
            //店家名稱 0, 檔次名稱 1, 檔號 2, 商品檔號 3
            ddlFilter.Items.Clear();
            ddlFilter.Items.Add(new ListItem(Resources.Localization.StoreName, "0"));
            ddlFilter.Items.Add(new ListItem(Resources.Localization.DealName, "1"));
            ddlFilter.Items.Add(new ListItem(Resources.Localization.DealName, "2"));
            ddlFilter.Items.Add(new ListItem(Resources.Localization.ProductId, "3"));
        }

        private bool Validation(HiDealDealListViewSearchType column, string value)
        {
            lblErrorMsg.Text = string.Empty;

            if (column.Equals(HiDealDealListViewSearchType.DealId))
            {
                int text;
                if (int.TryParse(value, out text))
                    return true;
                else
                {
                    lblErrorMsg.Text = "檔號輸入錯誤!!";
                    return false;
                }
            }
            else
                return true;
        }

        protected void gvHidealDealList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var data = (ViewHiDealSellerProduct)e.Row.DataItem;
                var hkDealName = (HyperLink)e.Row.FindControl("hkDealName");
                var lbDealWorkingType = (Label)e.Row.FindControl("lbDealWorkingType");
                var lbSellCount = (Label)e.Row.FindControl("lbSellCount");
                lbSellCount.Text = string.Format("{0:d}", data.OrderCount - data.ReturnedCount);

                hkDealName.NavigateUrl = "~/ControlRoom/piinlife/ProductInventory.aspx?did=" + data.DealId;

                hkDealName.Text = data.DealName;
                if (data.DealStartTime > DateTime.Now)
                {
                    lbDealWorkingType.Text = "未上檔";
                }
                else if(data.DealEndTime < DateTime.Now)
                {
                    lbDealWorkingType.Text = "下檔";
                }
                else
                {
                    lbDealWorkingType.Text = "上檔";
                }
            }
        }
    }
}
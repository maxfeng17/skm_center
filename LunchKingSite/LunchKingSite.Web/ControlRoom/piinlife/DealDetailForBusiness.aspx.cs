﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.ControlRoom.piinlife
{
    public partial class DealDetailForBusiness : RolePage, IHiDealDealDetailForBusinessView
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        #region event

        #endregion

        #region property
        private HiDealDealDetailForBusinessPresenter _presenter;
        public HiDealDealDetailForBusinessPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public int? DealId 
        { 
            get
            {
                int pid;
                if (int.TryParse(Request.QueryString["bid"], out pid))
                {
                    return pid;
                }
                else
                {
                    return null;
                }
            }
        }
        #endregion property

        #region method

        #endregion
    }
}
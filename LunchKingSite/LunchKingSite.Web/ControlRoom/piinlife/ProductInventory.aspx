﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductInventory.aspx.cs"
    Inherits="LunchKingSite.Web.ControlRoom.piinlife.ProductInventory" MasterPageFile="~/ControlRoom/backend.master" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Src="~/ControlRoom/Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="aspajax" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>&nbsp;</div>
    <style>
        .head2
        {
            background-color: #6B696B;
            color: #FFFFFF;
            font-weight: bolder;
            font-size: small;
        }
        .row
        {
            background-color: #FFE4B5;
            font-size: small;
        } 
    </style>
    <asp:Button ID="btnSearch" runat="server" Text="回上頁" PostBackUrl="~/ControlRoom/piinlife/DealList.aspx" />
    <table style="width: 1100px">
        <tr>
            <td colspan="5">
                <asp:GridView ID="gv_Deal" runat="server" AutoGenerateColumns="false"
                    OnRowDataBound="gv_Bound" OnRowCommand="gv_DealCommand">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <table style="width: 1100px">
                                    <tr>
                                        <td class="head2">匯出清冊</td>
                                        <td class="head2">店家名稱</td>
                                        <td class="head2">檔次名稱</td>
                                        <td class="head2">狀態</td>
                                        <td class="head2"> 開始時間</td>
                                        <td class="head2">結束時間</td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="row"><asp:LinkButton ID="lb_DealExport" runat="server" CommandName="DealExport" CommandArgument='<%# ((ViewHiDealSellerProduct)(Container.DataItem)).DealId %>'>匯出清冊Excel</asp:LinkButton></td>
                                    <td class="row"><%# ((ViewHiDealSellerProduct)(Container.DataItem)).SellerName%></td>
                                    <td class="row"><%# ((ViewHiDealSellerProduct)(Container.DataItem)).DealName%></td>
                                    <td class="row"><asp:Label ID="lb_Slug" runat="server"></asp:Label></td>
                                    <td class="row"><%# ((ViewHiDealSellerProduct)(Container.DataItem)).DealStartTime%></td>
                                    <td class="row"><%# ((ViewHiDealSellerProduct)(Container.DataItem)).DealEndTime%></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
       <div> &nbsp;</div>
    <table style="width: 1000px"  align="center" >
        <tr>
            <td colspan="5">
                <asp:GridView ID="gv_Product" runat="server" AutoGenerateColumns="false" 
                OnRowDataBound="gv_Bound"  OnRowCommand="gv_ProductCommand">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <table style="width: 1000px">
                                    <tr>
                                        <td class="head2">商品名稱 </td>
                                        <td class="head2">目前銷售數 </td>
                                        <td class="head2">目前銷售金額</td>
                                        <td class="head2">目前退貨數</td>
                                        <td class="head2">目前退貨金額</td>
                                        <td class="head2">銷售數-退貨數</td>
                                        <td class="head2">銷售金額-退貨金額</td>
                                        <td class="head2">個別匯出清冊</td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="row">
                                        <a target="_blank" style="font-size:small" id="linkTitle" runat="server" />
                                   </td>
                                    <td class="row"><%# string.Format("{0}", ((ViewHiDealOrderReturnQuantity)(Container.DataItem)).OrderQty?? 0) %></td>
    
                                    <td class="row"><%# string.Format("{0:$#,####;($#,####);$0}", ((ViewHiDealOrderReturnQuantity)(Container.DataItem)).OrderTotalPrice ?? 0)%></td>
                                    <td class="row"><asp:Literal ID="litReturnQty" runat="server" /></td>
                                    <td class="row"><asp:Literal ID="litReturnTotalPrice" runat="server" /></td>
                                    <td class="row"><%# string.Format("{0}", ((ViewHiDealOrderReturnQuantity)(Container.DataItem)).OrderQty ?? 0 - ((ViewHiDealOrderReturnQuantity)(Container.DataItem)).ReturnQty )%></td>
                                    <td class="row"><%# string.Format("{0:$#,####;($#,####);$0}", ((ViewHiDealOrderReturnQuantity)(Container.DataItem)).OrderTotalPrice ?? 0- ((ViewHiDealOrderReturnQuantity)(Container.DataItem)).ReturnTotalPrice) %></td>
                                    <td class="row">
                                        <asp:LinkButton ID="lb_Export" runat="server" Font-Size="Small" CommandName="lbExport" CommandArgument='<%# ((ViewHiDealOrderReturnQuantity)(Container.DataItem)).ProductId %>' >
                                            匯出清冊Excel</asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>

</asp:Content>

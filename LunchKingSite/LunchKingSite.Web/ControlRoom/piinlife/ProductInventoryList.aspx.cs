﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Expressions;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core.Component;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.HSSF;
using System.IO;


namespace LunchKingSite.Web.ControlRoom.piinlife
{
    public partial class ProductInventoryList : RolePage, IHiDealProductInventoryListView
    {
        protected IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        private HiDealProductInventoryListPresenter _presenter;
        public HiDealProductInventoryListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        #region event
        public event EventHandler<DataEventArgs<int>> GetShopReportDetail = null;
        public event EventHandler<DataEventArgs<int>> GetHouseReportDetail = null;
        public event EventHandler<DataEventArgs<DateTime>> SaveShippedDate = null;
        #endregion

        public string Productlid
        {
            get
            {
                string Productid = Request["pid"] != null ? Request["pid"].ToString() : (string)Session[LkSiteSession.NowUrl.ToString()];
                return Productid;
            }
        }

        public string Message
        {
            get { return lbMessage.Text; }
            set { lbMessage.Text = value; }
        }

        public string ShippedDate
        {
            get { return tbShippedDate.Text; }
            set { tbShippedDate.Text = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["pid"] != null)
                {
                    _presenter.OnViewInitialized();
                }
            }
            _presenter.OnViewLoaded();
        }

        protected DataTable ConvertTableForShop(ViewHiDealProductShopInfoCollection vhidealproductshop)
        {
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn { ColumnName = "訂單編號" });
                dt.Columns.Add(new DataColumn { ColumnName = "商品名稱" });
                dt.Columns.Add(new DataColumn { ColumnName = "分店名稱" });
                dt.Columns.Add(new DataColumn { ColumnName = "姓名" });
                dt.Columns.Add(new DataColumn { ColumnName = "憑證編號" });
                dt.Columns.Add(new DataColumn { ColumnName = "使用者編號" });
                dt.Columns.Add(new DataColumn { ColumnName = "選項1" });
                dt.Columns.Add(new DataColumn { ColumnName = "選項2" });
                dt.Columns.Add(new DataColumn { ColumnName = "選項3" });
                dt.Columns.Add(new DataColumn { ColumnName = "選項4" });
                dt.Columns.Add(new DataColumn { ColumnName = "選項5" });

                
                foreach (var item in vhidealproductshop)
                {
                    //建立表頭，把值塞入DataRow
                    DataRow row = dt.NewRow();
                    row["訂單編號"] = item.OrderId;
                    row["商品名稱"] = item.ProductName;
                    row["分店名稱"] = item.StoreName;
                    row["姓名"] = item.Name;
                    row["憑證編號"] = item.PrefixSequence;
                    row["使用者編號"] = item.Code;
                    row["選項1"] = item.OptionName1;
                    row["選項2"] = item.OptionName2;
                    row["選項3"] = item.OptionName3;
                    row["選項4"] = item.OptionName4;
                    row["選項5"] = item.OptionName5;
                    dt.Rows.Add(row);
                }
                //建立資料欄位，把值寫入ColumnName裡，如沒有值就刪除整列欄位
                if (vhidealproductshop.Count > 0)
                {
                    var item = vhidealproductshop[0];
                    if (item.CatgName1 != null)
                    {
                        dt.Columns["選項1"].ColumnName = item.CatgName1;
                    }
                    else
                    { 
                        dt.Columns.Remove("選項1"); 
                    }

                    if (item.CatgName2 != null)
                    {
                        dt.Columns["選項2"].ColumnName = item.CatgName2;
                    }
                    else
                    { 
                        dt.Columns.Remove("選項2");
                    }

                    if (item.CatgName3 != null)
                    {
                        dt.Columns["選項3"].ColumnName = item.CatgName3;
                    }
                    else
                    { 
                        dt.Columns.Remove("選項3");
                    }

                    if (item.CatgName4 != null)
                    {
                        dt.Columns["選項4"].ColumnName = item.CatgName4;
                    }
                    else
                    { 
                        dt.Columns.Remove("選項4"); 
                    }

                    if (item.CatgName5 != null)
                    {
                        dt.Columns["選項5"].ColumnName = item.CatgName5;
                    }
                    else
                    { 
                        dt.Columns.Remove("選項5");
                    }

                }
            
            return dt;
        }

        protected DataTable ConvertTableForHouse(ViewHiDealProductHouseInfoCollection vhidealproducthouse)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn { ColumnName = "訂單編號" });
            dt.Columns.Add(new DataColumn { ColumnName = "購買人" });
            dt.Columns.Add(new DataColumn { ColumnName = "收件人" });
            dt.Columns.Add(new DataColumn { ColumnName = "收件人電話" });
            dt.Columns.Add(new DataColumn { ColumnName = "購買數量" });
            dt.Columns.Add(new DataColumn { ColumnName = "收件地址" });
            dt.Columns.Add(new DataColumn { ColumnName = "商品名稱" });
            dt.Columns.Add(new DataColumn { ColumnName = "選項1" });
            dt.Columns.Add(new DataColumn { ColumnName = "選項2" });
            dt.Columns.Add(new DataColumn { ColumnName = "選項3" });
            dt.Columns.Add(new DataColumn { ColumnName = "選項4" });
            dt.Columns.Add(new DataColumn { ColumnName = "選項5" });

            //int idx = 0;
            foreach (var item in vhidealproducthouse)
            {
                DataRow row = dt.NewRow();
                row["訂單編號"] = item.OrderId;
                row["購買人"] = item.Name;
                row["收件人"] = item.AddresseeName;
                row["收件人電話"] = item.AddresseePhone;
                row["購買數量"] = item.ItemQuantity;
                row["收件地址"] = item.DeliveryAddress;
                row["商品名稱"] = item.ProductName;

                row["選項1"] = item.OptionName1;
                row["選項2"] = item.OptionName2;
                row["選項3"] = item.OptionName3;
                row["選項4"] = item.OptionName4;
                row["選項5"] = item.OptionName5;
                dt.Rows.Add(row);
            }
            if (vhidealproducthouse.Count > 0)
            {
                var item = vhidealproducthouse[0];

                if (item.CatgName1 != null)
                {
                    dt.Columns["選項1"].ColumnName = item.CatgName1;
                }
                else
                { dt.Columns.Remove("選項1"); }

                if (item.CatgName2 != null)
                {
                    dt.Columns["選項2"].ColumnName = item.CatgName2;
                }
                else
                { dt.Columns.Remove("選項2"); }

                if (item.CatgName3 != null)
                {
                    dt.Columns["選項3"].ColumnName = item.CatgName3;
                }
                else
                { dt.Columns.Remove("選項3"); }

                if (item.CatgName4 != null)
                {
                    dt.Columns["選項4"].ColumnName = item.CatgName4;
                }
                else
                { dt.Columns.Remove("選項4"); }

                if (item.CatgName5 != null)
                {
                    dt.Columns["選項5"].ColumnName = item.CatgName5;
                }
                else
                { dt.Columns.Remove("選項5"); }

            }

            return dt;
        }

        public void showShopData(ViewHiDealProductShopInfoCollection vhidealproductshop)
        {
            gv_Shop.DataSource = ConvertTableForShop(vhidealproductshop);
            gv_Shop.DataBind();
        }

        public void showHouseData(ViewHiDealProductHouseInfoCollection vhidealproducthouse)
        {
            gv_House.DataSource = ConvertTableForHouse(vhidealproducthouse);
            gv_House.DataBind();

        }

        protected void gv_Bound(object sender, GridViewRowEventArgs e)
        {
        }

        protected void CheckShop(object sender, EventArgs e)
        {
            if (this.GetShopReportDetail != null)
            {
                GetShopReportDetail(sender, new DataEventArgs<int>(int.Parse(Productlid)));
            }
        }
        protected void CheckHouse(object sender, EventArgs e)
        {
            if (this.GetHouseReportDetail != null)
            {
                GetHouseReportDetail(sender, new DataEventArgs<int>(int.Parse(Productlid)));
            }
        }

        public void ExportProductShopData(ViewHiDealProductShopInfoCollection productShopInfo)
        {
            MemoryStream ms = new MemoryStream();
            HSSFWorkbook workbook = new HSSFWorkbook();
            NPOI.SS.UserModel.DataFormat format_money = workbook.CreateDataFormat();
           
            #region  到店憑證
            int i = 0;
            NPOI.SS.UserModel.Sheet sheet1 = workbook.CreateSheet("到店明細");
            if (productShopInfo.Count > 0)
            {
                SetRow(sheet1, i, new NpoiRow(0, "訂單編號"), new NpoiRow(1, "商品名稱"), new NpoiRow(2, "分店名稱"), new NpoiRow(3, "姓名"), new NpoiRow(4, "憑證編號"), new NpoiRow(5, "使用者編號"), new NpoiRow(6, productShopInfo[0].CatgName1), new NpoiRow(7, productShopInfo[0].CatgName2), new NpoiRow(8, productShopInfo[0].CatgName3), new NpoiRow(9, productShopInfo[0].CatgName4), new NpoiRow(10, productShopInfo[0].CatgName5));
                i++;
                foreach (var item in productShopInfo)
                {
                    SetRow(sheet1, i, new NpoiRow(0, item.OrderId), new NpoiRow(1, item.ProductName), new NpoiRow(2, item.StoreName), new NpoiRow(3, item.Name), new NpoiRow(4, item.PrefixSequence), new NpoiRow(5, item.Code), new NpoiRow(6, item.OptionName1), new NpoiRow(7, item.OptionName2), new NpoiRow(8, item.OptionName3), new NpoiRow(9, item.OptionName4), new NpoiRow(10, item.OptionName5));
                    i++;
                }
            }

            #endregion sheet1_end

            workbook.Write(ms);
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename=ShopReport.xls"));
            Response.BinaryWrite(ms.ToArray());

            workbook = null;
            ms.Close();
            ms.Dispose();
        }

        public void ExportProductHouseData(ViewHiDealProductHouseInfoCollection productHuoseInfo)
        {
            MemoryStream ms = new MemoryStream();
            HSSFWorkbook workbook = new HSSFWorkbook();
            NPOI.SS.UserModel.DataFormat format_money = workbook.CreateDataFormat();

            #region  到府宅配  
            int i = 0;
            NPOI.SS.UserModel.Sheet sheet2 = workbook.CreateSheet("宅配明細");
            if (productHuoseInfo.Count > 0)
            {
                SetRow(sheet2, i
                    , new NpoiRow(0, "訂單編號")
                    , new NpoiRow(1, "購買人")
                    , new NpoiRow(2, "收件人")
                    , new NpoiRow(3, "收件人電話")
                    , new NpoiRow(4, "購買數量")
                    , new NpoiRow(5, "收件地址")
                    , new NpoiRow(6, "商品名稱")
                    , new NpoiRow(7, productHuoseInfo[0].CatgName1)
                    , new NpoiRow(8, productHuoseInfo[0].CatgName2)
                    , new NpoiRow(9, productHuoseInfo[0].CatgName3)
                    , new NpoiRow(10, productHuoseInfo[0].CatgName4)
                    , new NpoiRow(11, productHuoseInfo[0].CatgName5));
                i++;

                foreach (var item in productHuoseInfo)
                {
                    SetRow(sheet2, i
                        , new NpoiRow(0, item.OrderId)
                        , new NpoiRow(1, item.Name)
                        , new NpoiRow(2, item.AddresseeName)
                        , new NpoiRow(3, item.AddresseePhone)
                        , new NpoiRow(4, item.ItemQuantity.ToString())
                        , new NpoiRow(5, item.DeliveryAddress)
                        , new NpoiRow(6, item.ProductName)
                        , new NpoiRow(7, item.OptionName1)
                        , new NpoiRow(8, item.OptionName2)
                        , new NpoiRow(9, item.OptionName3)
                        , new NpoiRow(10, item.OptionName4)
                        , new NpoiRow(11, item.OptionName5));
                    i++;
                }            
            }
            #endregion sheet2_END

            workbook.Write(ms);
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename=HouseReport.xls"));
            Response.BinaryWrite(ms.ToArray());

            workbook = null;
            ms.Close();
            ms.Dispose();
        }


        public void SetRow(NPOI.SS.UserModel.Sheet sheet, int i, params NpoiRow[] row)
        {
            NPOI.SS.UserModel.Row Row = sheet.CreateRow(i);
            foreach (NpoiRow item in row)
            {
                if (item.Style == null)
                {
                    if (item.Type == 0)
                        SetCellValue(Row, item.ColumnI, item.Amount.Value);
                    else
                        SetCellValue(Row, item.ColumnI, item.Input);
                }
                else
                {
                    if (item.Type == 0)
                        SetCellValue(Row, item.ColumnI, item.Style, item.Amount.Value);
                    else
                        SetCellValue(Row, item.ColumnI, item.Style, item.Input);
                }
            }

        }
        protected void SetCellValue(NPOI.SS.UserModel.Row row, int i, string content)
        {
            NPOI.SS.UserModel.Cell cell = row.CreateCell(i);
            cell.SetCellValue(content);
        }
        protected void SetCellValue(NPOI.SS.UserModel.Row row, int i, int content)
        {
            NPOI.SS.UserModel.Cell cell = row.CreateCell(i);
            cell.SetCellValue(content);
        }


        public void SetRow(NPOI.SS.UserModel.Sheet sheet, int i, int x, int y, short height, params NpoiRow[] row)
        {
            NPOI.SS.UserModel.Row Row = sheet.CreateRow(i);
            Row.Height = height;
            foreach (NpoiRow item in row)
            {
                if (item.Style == null)
                {
                    if (item.Type == 0)
                        SetCellValue(Row, item.ColumnI, item.Amount.Value);
                    else
                        SetCellValue(Row, item.ColumnI, item.Input);
                }
                else
                {
                    if (item.Type == 0)
                        SetCellValue(Row, item.ColumnI, item.Style, item.Amount.Value);
                    else
                        SetCellValue(Row, item.ColumnI, item.Style, item.Input);
                }
            }
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(i, i, x, y));
        }
        public void SetRow(NPOI.SS.UserModel.Sheet sheet, int i, int x, int y, params NpoiRow[] row)
        {
            NPOI.SS.UserModel.Row Row = sheet.CreateRow(i);
            foreach (NpoiRow item in row)
            {

                if (item.Style == null)
                {
                    if (item.Type == 0)
                        SetCellValue(Row, item.ColumnI, item.Amount.Value);
                    else
                        SetCellValue(Row, item.ColumnI, item.Input);
                }
                else
                {
                    if (item.Type == 0)
                        SetCellValue(Row, item.ColumnI, item.Style, item.Amount.Value);
                    else
                        SetCellValue(Row, item.ColumnI, item.Style, item.Input);
                }

            }
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(i, i, x, y));
        }


        protected void SetCellStyle(NPOI.SS.UserModel.CellStyle style)
        {
            style.BorderTop = NPOI.SS.UserModel.CellBorderType.THIN;
            style.BorderBottom = NPOI.SS.UserModel.CellBorderType.THIN;
            style.BorderLeft = NPOI.SS.UserModel.CellBorderType.THIN;
            style.BorderRight = NPOI.SS.UserModel.CellBorderType.THIN;
            style.TopBorderColor = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            style.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            style.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            style.RightBorderColor = NPOI.HSSF.Util.HSSFColor.BLACK.index;
        }
        protected void SetCellValue(NPOI.SS.UserModel.Row row, int i, NPOI.SS.UserModel.CellStyle style, double content)
        {
            NPOI.SS.UserModel.Cell cell = row.CreateCell(i);
            cell.CellStyle = style;
            cell.SetCellValue(content);
        }
        protected void SetCellValue(NPOI.SS.UserModel.Row row, int i, NPOI.SS.UserModel.CellStyle style, string content)
        {
            NPOI.SS.UserModel.Cell cell = row.CreateCell(i);
            cell.CellStyle = style;
            cell.SetCellValue(content);
        }

        public class NpoiRow
        {
            public int Type { get; set; }
            public int ColumnI { get; set; }
            public NPOI.SS.UserModel.CellStyle Style { get; set; }
            public int? Amount { get; set; }
            public string Input { get; set; }
            public NpoiRow(int i, string input)
            {
                Type = 1;
                ColumnI = i;
                Input = input;
                Style = null;
            }

            public NpoiRow(int i, NPOI.SS.UserModel.CellStyle style, string input)
            {
                Type = 1;
                ColumnI = i;
                Style = style;
                Input = input;
            }
        }

        protected void btSaveTime_Click(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;
            
            DateTime selectDate;
            if (!DateTime.TryParse(tbShippedDate.Text, out selectDate))
            {
                Response.Write("<script>window.alert('請輸入出貨回覆日')</script>");
                return;
            }
            else
            {
                if (!(now.DayOfWeek == DayOfWeek.Monday && now.AddDays(-3).Date <= selectDate) && now.AddDays(-1).Date > selectDate)
                {
                    Response.Write("<script>window.alert('選取的日期不可小於昨天;若今天是星期一則選取日期不可小於星期五')</script>");
                    return;
                    
                }
                else
                    SaveShippedDate(sender, new DataEventArgs<DateTime>(selectDate));
            }
        }
    }
}
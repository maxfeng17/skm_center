﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Expressions;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core.Component;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.HSSF;
using System.IO;


namespace LunchKingSite.Web.ControlRoom.piinlife
{
    public partial class ProductInventory : RolePage, IHiDealProductInventoryView
    {

        #region property
        protected IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        private HiDealProductInventoryPresenter _presenter;
        public HiDealProductInventoryPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        #endregion

        #region event
        public event EventHandler<DataEventArgs<int>> GetProductReportDetail = null;
        public event EventHandler<DataEventArgs<int>> GetDealReportDetail;
        #endregion

        #region method

        public string Dealid
        {
            get
            {
                string Dealid = Request["did"] != null ? Request["did"].ToString() : (string)Session[LkSiteSession.NowUrl.ToString()];
                return Dealid;
            }
        }

        #endregion

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["did"] != null)
                {
                    _presenter.OnViewInitialized();
                }
                else
                {
                    Response.Redirect("~/ControlRoom/piinlife/DealList.aspx");
                }
            }
            _presenter.OnViewLoaded();

        }

        /// <summary>
        /// 主檔次
        /// DataBound資料繫結
        /// </summary>
        public void showBusinessData(ViewHiDealSellerProductCollection vhidealseller)
        {
            gv_Deal.DataSource = vhidealseller;
            gv_Deal.DataBind();
        }

        public void showProductData(ViewHiDealOrderReturnQuantityCollection hdpc)
        {
            gv_Product.DataSource = hdpc;
            gv_Product.DataBind();
        }

        protected void gv_Bound(object sender, GridViewRowEventArgs e)
        {
            GridView gv = (GridView)sender;

            if (e.Row.DataItem != null)
            {
                switch (gv.ID)
                {
                    //檔次
                    case "gv_Deal":
                        Label lb_Slug = (Label)e.Row.FindControl("lb_Slug");
                        LinkButton lbDealExport = (LinkButton)e.Row.FindControl("lb_DealExport");
                        ViewHiDealSellerProduct vhidealseller = (ViewHiDealSellerProduct)e.Row.DataItem;
                        if (vhidealseller.DealStartTime > DateTime.Now)
                        {
                            lb_Slug.Text = "未上檔";
                        }
                        else if (vhidealseller.DealEndTime < DateTime.Now)
                        {
                            lb_Slug.Text = "下檔";
                        }
                        else
                        {
                            lb_Slug.Text = "上檔";
                        }
                        break;
                    case "gv_Product":
                        LinkButton lb_Export = (LinkButton)e.Row.FindControl("lb_Export");
                        ViewHiDealOrderReturnQuantity data = (ViewHiDealOrderReturnQuantity)e.Row.DataItem;
                        HtmlAnchor anchor = (HtmlAnchor)e.Row.FindControl("linkTitle");
                        anchor.InnerText = data.ProductName;
                        if (data.OrderQty > 0)
                        {
                            anchor.HRef = "~/Controlroom/piinlife/ProductInventoryList.aspx?pid=" + data.ProductId;
                            ((Literal)e.Row.FindControl("litReturnTotalPrice")).Text =
                                string.Format("{0:$#,####;($#,####);$0}", data.ReturnTotalPrice);
                            ((Literal)e.Row.FindControl("litReturnQty")).Text =
                                string.Format("{0}", data.ReturnQty);
                        }
                        else
                        {
                            lb_Export.Visible = false;
                            ((Literal)e.Row.FindControl("litReturnTotalPrice")).Text =
                                 string.Format("{0:$#,####;($#,####);$0}", data.ReturnTotalPrice);
                            ((Literal)e.Row.FindControl("litReturnQty")).Text =
                                string.Format("{0}", data.ReturnQty);
                        }
                        break;
                }
            }
        }
        /// <summary>
        /// RowCommand:觸發執行
        /// </summary>
        protected void gv_DealCommand(object sender, GridViewCommandEventArgs e)
        {
            ViewHiDealOrderReturnQuantityCollection orderreturnQuantity = hp.ViewHiDealOrderReturnQuantityGetListByDealId(int.Parse(e.CommandArgument.ToString()));
            switch (e.CommandName)
            {
                case "DealExport":
                    if (orderreturnQuantity.Count > 0)
                    {
                        int DealId = (int)orderreturnQuantity[0].HiDealId;
                        GetDealReportDetail(sender, new DataEventArgs<int>(DealId));
                    }
                    Response.Write("<script>window.alert('此檔次尚未有購買紀錄')</script>");
                    break;
            }
        }

        protected void gv_ProductCommand(object sender, GridViewCommandEventArgs e)
        {
            ViewHiDealOrderReturnQuantityCollection orderreturnQuantity = hp.ViewHiDealOrderReturnQuantityGetListByProductId(int.Parse(e.CommandArgument.ToString()));
            switch (e.CommandName)
            {
                //轉值至列表頁
                case "ProductName":
                    break;

                //下載商品檔excel
                case "lbExport":
                    int productId = (int)orderreturnQuantity[0].ProductId;
                    GetProductReportDetail(sender, new DataEventArgs<int>(productId));
                    break;
            }
        }

        public void ExportDealData(List<HiDealProductInventoryViewDealProductInfo> datas)
        {
            MemoryStream ms = new MemoryStream();
            HSSFWorkbook workbook = new HSSFWorkbook();
            NPOI.SS.UserModel.DataFormat format_money = workbook.CreateDataFormat();

            #region style
            NPOI.SS.UserModel.CellStyle style_color = workbook.CreateCellStyle();
            style_color.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.TAN.index;
            style_color.FillPattern = NPOI.SS.UserModel.FillPatternType.SPARSE_DOTS;
            style_color.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.TAN.index;
            SetCellStyle(style_color);
            NPOI.SS.UserModel.CellStyle style_border = workbook.CreateCellStyle();
            SetCellStyle(style_border);
            NPOI.SS.UserModel.CellStyle style_font = workbook.CreateCellStyle();
            SetCellStyle(style_font);
            #endregion

            int i = 0;
            foreach (var items in datas)
            {
                if (items.ProductHouseInfos.Count > 0 || items.ProductShopInfos.Count > 0)
                {
                    NPOI.SS.UserModel.Sheet sheet1 = workbook.CreateSheet(items.Product.Name.Replace("/", "_"));
                    sheet1.SetColumnWidth(0, 5000);
                    sheet1.SetColumnWidth(1, 5000);
                    sheet1.SetColumnWidth(2, 5000);
                    sheet1.SetColumnWidth(3, 5000);
                    sheet1.SetColumnWidth(4, 5000);
                    sheet1.SetColumnWidth(5, 5000);
                    sheet1.SetColumnWidth(6, 5000);
                    sheet1.SetColumnWidth(7, 5000);
                    sheet1.SetColumnWidth(8, 5000);
                    sheet1.SetColumnWidth(9, 5000);
                    sheet1.SetColumnWidth(10, 5000);
                    i = 0;

                    #region 到店憑證
                    if (items.ProductShopInfos.Count > 0)
                    {
                        SetRow(sheet1, i, 0, 10, 400, new NpoiRow(0, style_color, "到店憑證"), new NpoiRow(1, string.Empty), new NpoiRow(2, string.Empty), new NpoiRow(3, string.Empty), new NpoiRow(4, string.Empty), new NpoiRow(5, string.Empty), new NpoiRow(6, string.Empty)
                              , new NpoiRow(7, string.Empty), new NpoiRow(8, string.Empty), new NpoiRow(9, string.Empty));
                        i++;
                        //20120823_新增分店名稱
                        SetRow(sheet1, i, new NpoiRow(0, style_border, "訂單編號"), new NpoiRow(1, style_border, "商品名稱"), new NpoiRow(2, "分店名稱"), new NpoiRow(3, style_border, "姓名"), new NpoiRow(4, style_border, "憑證編號"), new NpoiRow(5, style_border, "使用者編號"), new NpoiRow(6, style_border, items.ProductShopInfos[0].CatgName1), new NpoiRow(7, style_border, items.ProductShopInfos[0].CatgName2), new NpoiRow(8, style_border, items.ProductShopInfos[0].CatgName3), new NpoiRow(9, style_border, items.ProductShopInfos[0].CatgName4), new NpoiRow(10, style_border, items.ProductShopInfos[0].CatgName5));
                        i++;
                        foreach (var item in items.ProductShopInfos)
                        {
                            SetRow(sheet1, i, new NpoiRow(0, item.OrderId), new NpoiRow(1, item.ProductName), new NpoiRow(2, item.StoreName), new NpoiRow(3, (item.Name.Length > 1 ? item.Name.Remove(1, 1).Insert(1, "*") : item.Name)), new NpoiRow(4, item.PrefixSequence), new NpoiRow(5, item.Code), new NpoiRow(6, item.OptionName1), new NpoiRow(7, item.OptionName2), new NpoiRow(8, item.OptionName3), new NpoiRow(9, item.OptionName4), new NpoiRow(10, item.OptionName5));
                            i++;
                        }
                        //空白行
                        SetRow(sheet1, i, 0, 10, 400, new NpoiRow(0, string.Empty), new NpoiRow(1, string.Empty), new NpoiRow(2, string.Empty), new NpoiRow(3, string.Empty), new NpoiRow(4, string.Empty), new NpoiRow(5, string.Empty), new NpoiRow(6, string.Empty), new NpoiRow(7, string.Empty), new NpoiRow(8, string.Empty), new NpoiRow(9, string.Empty));
                        i++;
                    }
                    #endregion End_到店憑證

                    #region 到府宅配
                    if (items.ProductHouseInfos.Count > 0)
                    {
                        SetRow(sheet1, i, 0, 10, 400, new NpoiRow(0, style_color, "到府宅配"), new NpoiRow(1, string.Empty), new NpoiRow(2, string.Empty), new NpoiRow(3, string.Empty), new NpoiRow(4, string.Empty), new NpoiRow(5, string.Empty), new NpoiRow(6, string.Empty), new NpoiRow(7, string.Empty), new NpoiRow(8, string.Empty), new NpoiRow(9, string.Empty), new NpoiRow(10, string.Empty), new NpoiRow(11, string.Empty));
                        i++;
                        SetRow(sheet1, i
                                  , new NpoiRow(0, "訂單編號")
                                  , new NpoiRow(1, "購買人")
                                  , new NpoiRow(2, "收件人")
                                  , new NpoiRow(3, "收件人電話")
                                  , new NpoiRow(4, "購買數量")
                                  , new NpoiRow(5, "收件地址")
                                  , new NpoiRow(6, "商品名稱")
                                  , new NpoiRow(7, items.ProductHouseInfos[0].CatgName1)
                                  , new NpoiRow(8, items.ProductHouseInfos[0].CatgName2)
                                  , new NpoiRow(9, items.ProductHouseInfos[0].CatgName3)
                                  , new NpoiRow(10, items.ProductHouseInfos[0].CatgName4)
                                  , new NpoiRow(11, items.ProductHouseInfos[0].CatgName5));
                        i++;

                        foreach (var item in items.ProductHouseInfos)
                        {
                            SetRow(sheet1, i, new NpoiRow(0, item.OrderId), new NpoiRow(1, item.Name), new NpoiRow(2, item.AddresseeName), new NpoiRow(3, item.AddresseePhone), new NpoiRow(4, item.ItemQuantity.ToString()),
                            new NpoiRow(5, item.DeliveryAddress), new NpoiRow(6, item.ProductName), new NpoiRow(7, item.OptionName1), new NpoiRow(8, item.OptionName2), new NpoiRow(9, item.OptionName3), new NpoiRow(10, item.OptionName4), new NpoiRow(11, item.OptionName5));
                            i++;
                        }
                        SetRow(sheet1, i, 0, 10, 400, new NpoiRow(0, string.Empty), new NpoiRow(1, string.Empty), new NpoiRow(2, string.Empty), new NpoiRow(3, string.Empty), new NpoiRow(4, string.Empty), new NpoiRow(5, string.Empty), new NpoiRow(6, string.Empty), new NpoiRow(7, string.Empty), new NpoiRow(8, string.Empty), new NpoiRow(9, string.Empty), new NpoiRow(10, string.Empty), new NpoiRow(11, string.Empty));
                        i++;
                    }
                    #endregion end_到府宅配
                }

            }
            workbook.Write(ms);
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename=" + datas[0].Deal.Name + ".xls"));
            Response.BinaryWrite(ms.ToArray());

            workbook = null;
            ms.Close();
            ms.Dispose();
        }

        public void ExportProductData(ViewHiDealProductShopInfoCollection productShopInfo, ViewHiDealProductHouseInfoCollection productHuoseInfo, ViewHiDealOrderReturnQuantityCollection dealInfo)
        {
            MemoryStream ms = new MemoryStream();
            HSSFWorkbook workbook = new HSSFWorkbook();
            NPOI.SS.UserModel.DataFormat format_money = workbook.CreateDataFormat();

            #region sheet1 : 到店憑證
            int i = 0;
            NPOI.SS.UserModel.Sheet sheet1 = workbook.CreateSheet("到店明細");
            if (productShopInfo.Count > 0)
            {
                //20120823_新增分店名稱
                SetRow(sheet1, i, new NpoiRow(0, "訂單編號"), new NpoiRow(1, "商品名稱"), new NpoiRow(2, "分店名稱"), new NpoiRow(3, "姓名"), new NpoiRow(4, "憑證編號"), new NpoiRow(5, "使用者編號"), new NpoiRow(6, productShopInfo[0].CatgName1), new NpoiRow(7, productShopInfo[0].CatgName2), new NpoiRow(8, productShopInfo[0].CatgName3), new NpoiRow(9, productShopInfo[0].CatgName4), new NpoiRow(10, productShopInfo[0].CatgName5));
                i++;
                foreach (var item in productShopInfo)
                {
                    SetRow(sheet1, i, new NpoiRow(0, item.OrderId), new NpoiRow(1, item.ProductName), new NpoiRow(2, item.StoreName), new NpoiRow(3, item.Name), new NpoiRow(4, item.PrefixSequence), new NpoiRow(5, item.Code), new NpoiRow(6, item.OptionName1), new NpoiRow(7, item.OptionName2), new NpoiRow(8, item.OptionName3), new NpoiRow(9, item.OptionName4), new NpoiRow(10, item.OptionName5));
                    i++;
                }
            }
            #endregion sheet1_end

            #region sheet2 到府宅配
            i = 0;
            NPOI.SS.UserModel.Sheet sheet2 = workbook.CreateSheet("宅配明細");
            if (productHuoseInfo.Count > 0)
            {
                SetRow(sheet2, i
                , new NpoiRow(0, "訂單編號")
                , new NpoiRow(1, "購買人")
                , new NpoiRow(2, "收件人")
                , new NpoiRow(3, "收件人電話")
                , new NpoiRow(4, "購買數量")
                , new NpoiRow(5, "收件地址")
                , new NpoiRow(6, "商品名稱")
                , new NpoiRow(7, productHuoseInfo[0].CatgName1)
                , new NpoiRow(8, productHuoseInfo[0].CatgName2)
                , new NpoiRow(9, productHuoseInfo[0].CatgName3)
                , new NpoiRow(10, productHuoseInfo[0].CatgName4)
                , new NpoiRow(11, productHuoseInfo[0].CatgName5));
                i++;

                foreach (var item in productHuoseInfo)
                {
                    SetRow(sheet2, i
                        , new NpoiRow(0, item.OrderId)
                        , new NpoiRow(1, item.Name)
                        , new NpoiRow(2, item.AddresseeName)
                        , new NpoiRow(3, item.AddresseePhone)
                        , new NpoiRow(4, item.ItemQuantity.ToString())
                        , new NpoiRow(5, item.DeliveryAddress)
                        , new NpoiRow(6, item.ProductName)
                        , new NpoiRow(7, item.OptionName1)
                        , new NpoiRow(8, item.OptionName2)
                        , new NpoiRow(9, item.OptionName3)
                        , new NpoiRow(10, item.OptionName4)
                        , new NpoiRow(11, item.OptionName5));
                    i++;
                }

            }
            #endregion sheet2_END

            workbook.Write(ms);
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename=" + dealInfo[0].Name + "－" + dealInfo[0].ProductName + ".xls"));
            Response.BinaryWrite(ms.ToArray());
            workbook = null;
            ms.Close();
            ms.Dispose();
        }

        public void SetRow(NPOI.SS.UserModel.Sheet sheet, int i, params NpoiRow[] row)
        {
            NPOI.SS.UserModel.Row Row = sheet.CreateRow(i);
            foreach (NpoiRow item in row)
            {
                if (item.Style == null)
                {
                    if (item.Type == 0)
                        SetCellValue(Row, item.ColumnI, item.Amount.Value);
                    else
                        SetCellValue(Row, item.ColumnI, item.Input);
                }
                else
                {
                    if (item.Type == 0)
                        SetCellValue(Row, item.ColumnI, item.Style, item.Amount.Value);
                    else
                        SetCellValue(Row, item.ColumnI, item.Style, item.Input);
                }
            }

        }
        protected void SetCellValue(NPOI.SS.UserModel.Row row, int i, string content)
        {
            NPOI.SS.UserModel.Cell cell = row.CreateCell(i);
            cell.SetCellValue(content);
        }
        protected void SetCellValue(NPOI.SS.UserModel.Row row, int i, int content)
        {
            NPOI.SS.UserModel.Cell cell = row.CreateCell(i);
            cell.SetCellValue(content);
        }

        //MergeRow_style
        public void SetRow(NPOI.SS.UserModel.Sheet sheet, int i, int x, int y, short height, params NpoiRow[] row)
        {
            NPOI.SS.UserModel.Row Row = sheet.CreateRow(i);
            Row.Height = height;
            foreach (NpoiRow item in row)
            {
                if (item.Style == null)
                {
                    if (item.Type == 0)
                        SetCellValue(Row, item.ColumnI, item.Amount.Value);
                    else
                        SetCellValue(Row, item.ColumnI, item.Input);
                }
                else
                {
                    if (item.Type == 0)
                        SetCellValue(Row, item.ColumnI, item.Style, item.Amount.Value);
                    else
                        SetCellValue(Row, item.ColumnI, item.Style, item.Input);
                }
            }
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(i, i, x, y));
        }
        public void SetRow(NPOI.SS.UserModel.Sheet sheet, int i, int x, int y, params NpoiRow[] row)
        {
            NPOI.SS.UserModel.Row Row = sheet.CreateRow(i);
            foreach (NpoiRow item in row)
            {

                if (item.Style == null)
                {
                    if (item.Type == 0)
                        SetCellValue(Row, item.ColumnI, item.Amount.Value);
                    else
                        SetCellValue(Row, item.ColumnI, item.Input);
                }
                else
                {
                    if (item.Type == 0)
                        SetCellValue(Row, item.ColumnI, item.Style, item.Amount.Value);
                    else
                        SetCellValue(Row, item.ColumnI, item.Style, item.Input);
                }

            }
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(i, i, x, y));
        }

        //CellStyle
        protected void SetCellStyle(NPOI.SS.UserModel.CellStyle style)
        {
            style.BorderTop = NPOI.SS.UserModel.CellBorderType.THIN;
            style.BorderBottom = NPOI.SS.UserModel.CellBorderType.THIN;
            style.BorderLeft = NPOI.SS.UserModel.CellBorderType.THIN;
            style.BorderRight = NPOI.SS.UserModel.CellBorderType.THIN;
            style.TopBorderColor = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            style.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            style.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            style.RightBorderColor = NPOI.HSSF.Util.HSSFColor.BLACK.index;
        }
        protected void SetCellValue(NPOI.SS.UserModel.Row row, int i, NPOI.SS.UserModel.CellStyle style, double content)
        {
            NPOI.SS.UserModel.Cell cell = row.CreateCell(i);
            cell.CellStyle = style;
            cell.SetCellValue(content);
        }
        protected void SetCellValue(NPOI.SS.UserModel.Row row, int i, NPOI.SS.UserModel.CellStyle style, string content)
        {
            NPOI.SS.UserModel.Cell cell = row.CreateCell(i);
            cell.CellStyle = style;
            cell.SetCellValue(content);
        }

        public class NpoiRow
        {
            public int Type { get; set; }
            public int ColumnI { get; set; }
            public NPOI.SS.UserModel.CellStyle Style { get; set; }
            public int? Amount { get; set; }
            public string Input { get; set; }
            public NpoiRow(int i, string input)
            {
                Type = 1;
                ColumnI = i;
                Input = input;
                Style = null;
            }

            public NpoiRow(int i, NPOI.SS.UserModel.CellStyle style, string input)
            {
                Type = 1;
                ColumnI = i;
                Style = style;
                Input = input;
            }
        }

        #endregion
    }
}
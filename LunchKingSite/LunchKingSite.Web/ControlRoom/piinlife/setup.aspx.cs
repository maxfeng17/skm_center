﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI.WebControls;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core.UI;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.WebLib.Component;
using LunchKingSite.Core;

namespace LunchKingSite.Web.ControlRoom.piinlife
{
    public partial class setup : RolePage, IHiDealSetupView
    {
        private HiDealSetupPresenter _presenter;
        public HiDealSetupPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        #region properties

        public string UserName { get { return Page.User.Identity.Name; } }
        public Guid Sid
        {
            get
            {
                Guid id;
                return Guid.TryParse(Request.QueryString["sid"], out id) ? id : Guid.Empty;
            }
        }
        public int Did
        {
            get
            {
                int id;
                return int.TryParse(Request.QueryString["did"], out id) ? id : 0;
            }
            set { lbDealId.Text = value.ToString(); }
        }

        public string DealName
        {
            get { return tbDealName.Text; }
            set { tbDealName.Text = value; }
        }
        public string DealPromoName
        {
            get { return tbPromoName.Text; }
            set { tbPromoName.Text = value; }
        }
        public string DealPromoShortName
        {
            get { return tbPromoShortName.Text; }
            set { tbPromoShortName.Text = value; }
        }
        /*
        public string EdmTitle
        {
            get { return tbEdmTitle.Text; }
            set { tbEdmTitle.Text = value; }
        }
        public string EdmSubject
        {
            get { return tbEdmSubject.Text; }
            set { tbEdmSubject.Text = value; }
        }
         */
        public Dictionary<int, string> DealRegions
        {
            get
            {
                Dictionary<int, string> regions = new Dictionary<int, string>();
                foreach (ListItem item in cblCities.Items)
                {
                    if (item.Selected)
                    {
                        regions.Add(int.Parse(item.Value), item.Text);
                    }
                }
                return regions;
            }
            set
            {
                foreach (KeyValuePair<int, string> pair in value)
                {
                    foreach (ListItem item in cblCities.Items)
                    {
                        if (int.Parse(item.Value) == pair.Key)
                            item.Selected = true;
                    }
                }
            }
        }
        public List<int> DealCategories
        {
            get
            {
                List<int> categories = new List<int>();
                foreach (ListItem item in cblCategories.Items)
                {
                    if (item.Selected)
                    {
                        categories.Add(int.Parse(item.Value));
                    }
                }
                return categories;
            }
            set
            {
                foreach (int i in value)
                {
                    foreach (ListItem item in cblCategories.Items)
                    {
                        if (int.Parse(item.Value) == i)
                            item.Selected = true;
                    }
                }
            }
        }
        public List<int> DealDiscounts
        {
            get
            {
                List<int> discounts = new List<int>();
                foreach (ListItem item in ddlSpecialDiscounts.Items)
                {
                    if (item.Selected)
                    {
                        discounts.Add(int.Parse(item.Value));
                    }
                }
                return discounts;
            }
            set
            {
                foreach (int i in value)
                {
                    foreach (ListItem item in ddlSpecialDiscounts.Items)
                    {
                        if (int.Parse(item.Value) == i)
                            item.Selected = true;
                    }
                }
            }
        }
        public bool IsOpen
        {
            get { return bool.Parse(ddlIsOpen.SelectedValue); }
            set
            {
                if (value)
                {
                    ddlIsOpen.SelectedIndex = 0;
                }
                else
                {
                    ddlIsOpen.SelectedIndex = 1;
                }
            }
        }
        public bool IsAlwaysMain
        {
            get { return cbAlwaysMain.Checked; }
            set { cbAlwaysMain.Checked = value; }
        }

        public bool IsVerifyByList
        {
            get { return rbVerifyByList.Checked; }
            set { rbVerifyByList.Checked = value; }
        }
        public bool IsVerifyByPad
        {
            get { return rbVerifyByPad.Checked; }
            set { rbVerifyByPad.Checked = value; }
        }

        public string DealStartDate
        {
            get { return tbDealStartDate.Text; }
            set { tbDealStartDate.Text = value; }
        }
        public string DealStartTime
        {
            get { return tbDealStartTime.Text; }
            set { tbDealStartTime.Text = value; }
        }
        public string DealEndDate
        {
            get { return tbDealEndDate.Text; }
            set { tbDealEndDate.Text = value; }
        }
        public string DealEndTime
        {
            get { return tbDealEndTime.Text; }
            set { tbDealEndTime.Text = value; }
        }

        public DataTable StoreSettings
        {
            get { return (DataTable)ViewState["StoreSettingCollection"]; }
            set { ViewState["StoreSettingCollection"] = value; }
        }
        public string OrderedStoreSettings
        {
            get { return hidOrderedStoreValues.Value; }
        }

        public string TagXml
        {
            get { return HttpUtility.HtmlDecode(hidDealTags.Value); }
            set { hidDealTags.Value = HttpUtility.HtmlEncode(value); }
        }

        public string BigPictureUrl
        {
            get
            {
                if (ViewState["BigPicUrl"] != null)
                    return (string)ViewState["BigPicUrl"];
                return null;
            }
            set
            {
                ViewState["BigPicUrl"] = value;
            }
        }
        //預備上傳圖(已上傳尚未存檔)
        public string BigCandidatePictureUrl
        {
            get
            {
                if (ViewState["BigCandidatePictureUrl"] != null)
                {
                    return (string)ViewState["BigCandidatePictureUrl"];
                }
                return null;
            }
            set
            {
                ViewState["BigCandidatePictureUrl"] = value;
                Image img = new Image();

                //預設值
                img.ImageUrl = value + "?" + DealModifyTime.Ticks; //顯示圖片

                //更換大圖時，避免Cache住
                if (!string.IsNullOrEmpty(value))
                {
                    string imgPathstr = value;
                    string[] imgPathArray = imgPathstr.Split('.');

                    if (ImageUtility.isFileExit(HttpContext.Current.Server.MapPath(imgPathArray[0] + "_" + DealModifyTime.ToString("yyyyMMddHHmmss") + "." + imgPathArray[1])))
                    {
                        img.ImageUrl = imgPathstr.Replace("HidealBigPic", "HidealBigPic_" + DealModifyTime.ToString("yyyyMMddHHmmss")) + "?" + DealModifyTime.Ticks; //顯示圖片
                    }

                }
                placeHolder_BigPicture.Controls.Add(img);
            }
        }
        public DateTime DealModifyTime { get; set; }

        public List<string> SmallPictureUrls
        {
            get
            {
                //if (ViewState["SmallPictureUrls"] == null)
                //{
                //    ViewState["SmallPictureUrls"] = new List<string>();
                //}
                //return (List<string>)ViewState["SmallPictureUrls"];
                //string urlCsv = Request.Form["hidSmallPicSequence"];
                string urlCsv = hidSmallPicSequence.Value;
                return !string.IsNullOrWhiteSpace(urlCsv) ? urlCsv.Split(',').ToList() : new List<string>();
            }
            set
            {
                //ViewState["SmallPictureUrls"] = value;
                //lvSmallPics.DataSource = SmallPictureUrls.Select(x => new { Url = x }).ToList();

                hidSmallPicSequence.Value = string.Join(",", value);
                lvSmallPics.DataSource = value.Select(x => new { Url = x }).ToList();
                lvSmallPics.DataBind();
            }
        }
        public string SelectedPrimarySmallPictureUrl
        {
            get { return hidSelectedPicUrl.Value; }
            set { hidSelectedPicUrl.Value = value; }
        }

        public FileUpload BigPictureFileUpload
        {
            get { return fileUpload_BigPic; }
        }
        public List<HttpPostedFile> SmallPictureFileUpload
        {
            get
            {
                List<HttpPostedFile> fileList = new List<HttpPostedFile>();
                if (fileUpload_SmallPic.HasFile)
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        if (Request.Files.AllKeys[i].Contains(fileUpload_SmallPic.ID, StringComparison.OrdinalIgnoreCase))
                        {
                            fileList.Add(Request.Files[i]);
                        }
                    }
                }
                return fileList;
            }
        }

        public string SalesName
        {
            get { return tbSalesName.Text; }
            set { tbSalesName.Text = value; }
        }
        public KeyValuePair<string, string> SalesDepartment
        {
            get
            {
                KeyValuePair<string, string> dept;
                foreach (ListItem item in ddlSalesDepartment.Items)
                {
                    if (item.Selected)
                    {
                        dept = new KeyValuePair<string, string>(item.Value, item.Text);
                        return dept;
                    }
                }
                return new KeyValuePair<string, string>("", "");
            }
            set
            {
                foreach (ListItem item in ddlSalesDepartment.Items)
                {
                    if (value.Key == item.Value)
                    {
                        item.Selected = true;
                    }
                }
            }
        }
        public KeyValuePair<string, string> SalesAccountableBU
        {
            get
            {
                KeyValuePair<string, string> bu;
                foreach (ListItem item in ddlSalesAccBU.Items)
                {
                    if (item.Selected)
                    {
                        bu = new KeyValuePair<string, string>(item.Value, item.Text);
                        return bu;
                    }
                }
                return new KeyValuePair<string, string>("", "");
            }
            set
            {
                foreach (ListItem item in ddlSalesAccBU.Items)
                {
                    if (value.Key == item.Value)
                    {
                        item.Selected = true;
                    }
                }
            }
        }
        public KeyValuePair<string, string> SalesBelongCity
        {
            get
            {
                KeyValuePair<string, string> city;
                foreach (ListItem item in ddlSalesAccCity.Items)
                {
                    if (item.Selected)
                    {
                        city = new KeyValuePair<string, string>(item.Value, item.Text);
                        return city;
                    }
                }
                return new KeyValuePair<string, string>("", "");
            }
            set
            {
                foreach (ListItem item in ddlSalesAccCity.Items)
                {
                    if (value.Key == item.Value)
                    {
                        item.Selected = true;
                    }
                }
            }
        }
        public KeyValuePair<string, string> SalesCatg1
        {
            get
            {
                KeyValuePair<string, string> salesCatg;
                foreach (ListItem item in ddlSalesCatg1.Items)
                {
                    if (item.Selected)
                    {
                        salesCatg = new KeyValuePair<string, string>(item.Value, item.Text);
                        return salesCatg;
                    }
                }
                return new KeyValuePair<string, string>("", "");
            }
            set
            {
                foreach (ListItem item in ddlSalesCatg1.Items)
                {
                    if (value.Key == item.Value)
                    {
                        item.Selected = true;
                    }
                }
            }
        }
        public KeyValuePair<string, string> SalesCatg2
        {
            get
            {
                return new KeyValuePair<string, string>(hdSalesCatg2Id.Value, hdSalesCatg2Name.Value);
            }
            set
            {
                foreach (ListItem item in ddlSalesCatg2.Items)
                {
                    if (value.Key == item.Value)
                    {
                        item.Selected = true;
                    }
                }
            }
        }
        public KeyValuePair<string, string> HdSalesCatg2
        {
            get
            {
                return new KeyValuePair<string, string>(this.hdSalesCatg2Id.Value, this.hdSalesCatg2Name.Value);
            }
            set
            {
                this.hdSalesCatg2Id.Value = value.Key;
                this.hdSalesCatg2Name.Value = value.Value;
            }
        }
        public List<Product> Products
        {
            get
            {
                if (ViewState["Products"] == null)
                {
                    ViewState["Products"] = new List<Product>();
                    return (List<Product>)ViewState["Products"];
                }
                return (List<Product>)ViewState["Products"];
            }
            set { ViewState["Products"] = value; }
        }

        public Guid DealGuid { set; get; }

        public string SalesManNameList = string.Empty;
        public string[] SalesmanNameArray
        {
            set
            {
                var rtn = string.Empty;
                foreach (var s in value)
                {
                    if (rtn == string.Empty)
                    {
                        rtn = string.Format("'{0}'", s);
                    }
                    else
                    {
                        rtn = string.Format("{0},'{1}'", rtn, s);
                    }
                }
                SalesManNameList = rtn;
            }
        }

        #endregion

        #region events

        public event EventHandler SaveDeal = delegate { };
        public event EventHandler<DataEventArgs<Product>> AddNewProduct = delegate { };
        public event EventHandler UploadBigPicture = delegate { };
        public event EventHandler UploadSmallPicture = delegate { };
        public event EventHandler GenCoupon = delegate { };

        #endregion

        #region Page Event

        protected void Page_Init(object sender, EventArgs e)
        {
            if (ViewState["RegionCollection"] == null)
            {
                ViewState["RegionCollection"] = Presenter.GetDealRegionsForControl();
                if (ProviderFactory.Instance().GetConfig().IsVisa2013 == false)
                {
                    ViewState["CategoryCollection"] = Presenter.GetDealCatgForControl();
                }
                else
                {
                    ViewState["CategoryCollection"] = Presenter.GetDealCatgForControl()
                        .Where(t => t.CodeName != "Visa御璽卡‧無限卡 專屬").ToList();
                }
                if (ProviderFactory.Instance().GetConfig().IsVisa2013 == false)
                {
                    ViewState["SpecialDiscountCollection"] =
                        Presenter.GetSecialDiscountsForControl().Where(t => t.CodeId != 2).ToList();
                }
                else
                {
                    ViewState["SpecialDiscountCollection"] = Presenter.GetSecialDiscountsForControl();
                }
                ViewState["DepartmentCollection"] = Presenter.GetSalesDepartment();
                ViewState["AccBuCollection"] = Presenter.GetSalesAccBU();
                ViewState["SalesCatgCollection"] = Presenter.GetSalesCatg().Where(x => x.ParentCodeId == null).OrderBy(x => x.Seq);
            }

            //區域
            cblCities.DataSource = ViewState["RegionCollection"];
            cblCities.DataTextField = "CodeName";
            cblCities.DataValueField = "CodeId";
            cblCities.DataBind();
            //總覽預設為選取
            foreach (ListItem listItem in cblCities.Items)
            {
                if (listItem.Value.Equals(HiDealRegionManager.DealOverviewRegion.ToString()))
                {
                    listItem.Enabled = false;
                    listItem.Selected = true;
                }
            }

            //類別
            cblCategories.DataSource = ViewState["CategoryCollection"];
            cblCategories.DataTextField = "CodeName";
            cblCategories.DataValueField = "CodeId";
            cblCategories.DataBind();

            //優惠設定
            ddlSpecialDiscounts.DataSource = ViewState["SpecialDiscountCollection"];
            ddlSpecialDiscounts.DataTextField = "CodeName";
            ddlSpecialDiscounts.DataValueField = "CodeId";
            ddlSpecialDiscounts.DataBind();
            ddlSpecialDiscounts.Items.Insert(0, new ListItem { Text = "無", Value = "0" });


            ddlSalesDepartment.Items.Add(new ListItem("請選擇", "-1"));
            ddlSalesDepartment.AppendDataBoundItems = true;
            ddlSalesDepartment.DataSource = ViewState["DepartmentCollection"];
            ddlSalesDepartment.DataTextField = "DeptName";
            ddlSalesDepartment.DataValueField = "DeptId";
            ddlSalesDepartment.DataBind();

            ddlSalesAccCity.Items.Add(new ListItem("請選擇", "-1"));
            ddlSalesAccCity.AppendDataBoundItems = true;
            ddlSalesAccCity.DataSource = CityManager.Citys.Where(x => x.Code != "SYS");
            ddlSalesAccCity.DataTextField = "CityName";
            ddlSalesAccCity.DataValueField = "Id";
            ddlSalesAccCity.DataBind();

            ddlSalesAccBU.Items.Add(new ListItem("請選擇", "-1"));
            ddlSalesAccBU.AppendDataBoundItems = true;
            ddlSalesAccBU.DataSource = ViewState["AccBuCollection"];
            ddlSalesAccBU.DataTextField = "AccBusinessGroupName";
            ddlSalesAccBU.DataValueField = "AccBusinessGroupId";
            ddlSalesAccBU.DataBind();

            ddlSalesCatg1.Items.Add(new ListItem("請選擇", "-1"));
            ddlSalesCatg1.AppendDataBoundItems = true;
            ddlSalesCatg1.DataSource = ViewState["SalesCatgCollection"];
            ddlSalesCatg1.DataTextField = "CodeName";
            ddlSalesCatg1.DataValueField = "CodeId";
            ddlSalesCatg1.DataBind();

            if (ViewState["Products"] == null)
            {
                ViewState["Products"] = new List<Product>();
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Sid != Guid.Empty || Did != 0)
                {
                    //InitializeViewStateInfo();
                    Presenter.OnViewInitialized();
                    lvStore.DataSource = this.StoreSettings;
                    lvStore.DataBind();
                    if (Did == 0)
                        ddlSalesAccBU.SelectedValue = ((int)DepartmentTypes.HiDeal).ToString();
                }
                if (Sid != Guid.Empty)
                {
                    divProduct.Visible = false;     //有檔次資料後才可以設定商品

                    //ckeditor 相關的設定要有檔次資料後才可設定, 避免圖片上傳到莫名其妙的地方.
                    divTagTabsArea.Visible = false;
                }

            }

            hidBigPicUpload.Value = "false";
            hidSmallPicUpload.Value = "false";

            Presenter.DealSaved += OnDealSaved;
            Products.Sort((prodA, prodB) => prodA.Seq.CompareTo(prodB.Seq));
            lvProducts.DataSource = Products;
            lvProducts.DataBind();

            Presenter.OnViewLoaded();
        }

        public void OnDealSaved(object sender, DataEventArgs<int> did)
        {
            Response.Redirect(string.Format("~/ControlRoom/piinlife/setup.aspx?did={0}", did));
        }

        protected void lvSmallPics_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Del":
                    List<string> newList = this.SmallPictureUrls;
                    newList.Remove((string)e.CommandArgument);
                    this.SmallPictureUrls = newList;
                    hidSmallPicUpload.Value = "true";
                    break;
            }
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            UpdateViewStateProductOrder();
            SaveDeal(this, null);
        }

        protected void BtnBigPicUpload_Click(object sender, EventArgs e)
        {
            UploadBigPicture(sender, e);
            hidBigPicUpload.Value = "true";
        }

        protected void BtnSmallPicUpload_Click(object sender, EventArgs e)
        {
            UploadSmallPicture(sender, e);
            hidSmallPicUpload.Value = "true";
        }

        protected void BtnNewProduct_Click(object sender, EventArgs e)
        {
            UpdateViewStateProductOrder();

            Product p = new Product();
            p.Seq = Products.Count + 1;
            p.Name = tbProductName.Text;
            AddNewProduct(this, new DataEventArgs<Product>(p));

            tbProductName.Text = "";
            lvProducts.DataSource = Products;
            lvProducts.DataBind();
            //重新觸發$('.tblProducts tbody').sortable();
            System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddNewProductSortTable", "$('.tblProducts tbody').sortable();isAddNewProductClick=false;", true);


        }

        private void UpdateViewStateProductOrder()
        {
            List<string> orderList = new List<string>(hidProducts.Value.Split(","));
            List<Product> originalList = Products;

            List<Product> orderedList = new List<Product>();
            if (originalList.Count > 0)
            {
                foreach (string s in orderList)
                {
                    int i = originalList.FindIndex(delegate(Product product) { return product.Name == s; });
                    orderedList.Add(originalList[i]);
                }
            }

            Products = orderedList;
        }

        public void lvProducts_OnItemDataBound(object sender, ListViewItemEventArgs e)
        {
            string pid = ((HiddenField)e.Item.FindControl("hidProdId")).Value;
            if (pid == "0")  //尚未新增至DB, 不可以用 hyperlink
            {
                e.Item.FindControl("linkProdName").Visible = false;
                e.Item.FindControl("litProdName").Visible = true;
            }
            else  //已有商品資訊在DB, 可以用 hyperlink 連到商品設定頁
            {
                e.Item.FindControl("litProdName").Visible = false;
                e.Item.FindControl("linkProdName").Visible = true;
                ((HyperLink)e.Item.FindControl("linkProdName")).NavigateUrl = "productSetup.aspx?pid=" + pid;
            }

        }

        #endregion

        protected string replaceHiDealCouponStoreName(object o)
        {
            string s = o == System.DBNull.Value ? "" : Convert.ToString(o);
            return s == "" ? "宅配" : s;
        }

        #region method
        public void ShowMessage(string msg)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + msg + "');", true);
        }

        public void OnGenCoupon(object sender, object e)
        {
            if (GenCoupon != null)
            {
                GenCoupon(this, new EventArgs());
            }
        }

        public void ShowCouponList(ViewHiDealCouponStoreListCountCollection createcouponlistcollection)
        {
            lvProductCounponCount.DataSource = createcouponlistcollection;
            lvProductCounponCount.DataBind();
        }

        public void ShowCouponNotMatchSettingMessage(HiDealCouponCountType hiDealCouponCountType)
        {
            couponQuantityNotMatchPanel.Visible = false;
            switch (hiDealCouponCountType)
            {
                case HiDealCouponCountType.GenateSettingMattch:
                    break;
                case HiDealCouponCountType.GenateCouponSettingNotMattch:
                    couponQuantityNotMatchPanel.Visible = true;
                    tbCouponQuantityNotMatchErrorMessage.Text = "目前產出的憑證數與設定數比對不一致，請重新產生憑證。";
                    tbCouponQuantityNotMatchErrorMessage.ForeColor = System.Drawing.Color.Red;
                    break;
                case HiDealCouponCountType.HaveRefundFormCoupon:
                    couponQuantityNotMatchPanel.Visible = true;
                    tbCouponQuantityNotMatchErrorMessage.Text = "因退貨造成產出憑證數與設定數不一致，詳細資訊請查看憑證資訊標簽。";
                    tbCouponQuantityNotMatchErrorMessage.ForeColor = System.Drawing.Color.Blue;
                    break;
            }

        }

        public void BuildDdlSalesCatg2(int codeId, SystemCodeCollection scs) 
        {
            ddlSalesCatg2.Items.Add(new ListItem("請選擇", "-1"));
            ddlSalesCatg2.DataSource = scs;
            ddlSalesCatg2.DataTextField = "CodeName";
            ddlSalesCatg2.DataValueField = "CodeId";
            ddlSalesCatg2.DataBind();
            ddlSalesCatg2.SelectedValue = codeId.ToString();
        }

        #endregion method

        #region WebMethod

        [WebMethod]
        public static string GetSalesCatg2(int parentId)
        {
            var scs = PponFacade.DealType2GetList(parentId);
            return new JsonSerializer().Serialize(scs);
        }

        #endregion

    }
}
﻿using System;
using System.Collections.ObjectModel;
using System.Web;
using System.Web.UI;
using System.Data;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.Core;

namespace LunchKingSite.Web.ControlRoom.piinlife
{
    public partial class productSetup : RolePage, IHiDealProductSetupView
    {
        private HiDealProductSetupPresenter _presenter;
        public HiDealProductSetupPresenter Presenter
        {
            get { return _presenter; }
            set { 
                _presenter = value;
                if(value != null)
                {
                    _presenter.View = this;
                }
            }
        }

        #region Properties
        public int Pid
        {
            get
            {
                int pid;
                return int.TryParse(Request.QueryString["pid"], out pid) ? pid : 0;
            }
        }

        public Guid Gpid { get; set; }

        public int Did { get; set; }

        public string ProductName
        {
            get { return tbProductName.Text; }
            set { tbProductName.Text = value; }
        }
        
        public string ProductDescription { 
            get { return HttpUtility.HtmlDecode(tbProductDescription.Text); }
            set { tbProductDescription.Text = HttpUtility.HtmlEncode(value); }
        }

        public bool IsShow
        {
            get { return bool.Parse(ddlIsShow.SelectedValue); }
            set { ddlIsShow.SelectedValue = value ? "true" : "false"; }
        }

        public string UseStartDate
        {
            get { return tbUseStartDate.Text; }
            set { tbUseStartDate.Text = value; }
        }

        public string UseEndDate
        {
            get { return tbUseEndDate.Text; }
            set { tbUseEndDate.Text = value; }
        }

        public string ChangeExpireDate
        {
            get { return tbChangedExpireDate.Text; }
            set { tbChangedExpireDate.Text = value; }
        }

        public VendorBillingModel BillingModel
        {
            get
            {
                if (rbBMBalanceSheetSystem.Checked)
                    return VendorBillingModel.BalanceSheetSystem;
                else if (rbBMMohistSystem.Checked)
                    return VendorBillingModel.MohistSystem;
                else
                    return VendorBillingModel.None;
            }
            set
            {
                rbBMNone.Checked = rbBMBalanceSheetSystem.Checked = rbBMMohistSystem.Checked = false;

                switch (value)
                {
                    case VendorBillingModel.BalanceSheetSystem:
                        rbBMBalanceSheetSystem.Checked = true;
                        break;
                    case VendorBillingModel.MohistSystem:
                        rbBMMohistSystem.Checked = true;
                        break;
                    default:
                        rbBMNone.Checked = true;
                        break;
                }
            }
        }

        public RemittanceType PaidType
        {
            get
            {
                if (rbAchWeekly.Checked)
                    return RemittanceType.AchWeekly;
                else if (rbAchMonthly.Checked)
                    return RemittanceType.AchMonthly;
                else if (rbAchPartially.Checked)
                    return RemittanceType.ManualPartially;
                else if (rbManualWeekly.Checked)
                    return RemittanceType.ManualWeekly;
                else if (rbManualMonthly.Checked)
                    return RemittanceType.ManualMonthly;
                else
                    return RemittanceType.Others;
            }
            set
            {
                rbAchOthers.Checked = rbAchWeekly.Checked = rbAchPartially.Checked = rbManualWeekly.Checked = rbManualMonthly.Checked = rbAchOthers.Checked = false;

                switch (value)
                {
                    case RemittanceType.AchWeekly:
                        rbAchWeekly.Checked = true;
                        break;
                    case RemittanceType.AchMonthly:
                        rbAchMonthly.Checked = true;
                        break;
                    case RemittanceType.ManualPartially:
                        rbAchPartially.Checked = true;
                        break;
                    case RemittanceType.ManualWeekly:
                        rbManualWeekly.Checked = true;
                        break;
                    case RemittanceType.ManualMonthly:
                        rbManualMonthly.Checked = true;
                        break;
                    default:
                        rbAchOthers.Checked = true;
                        break;
                }
            }
        }

        public VendorReceiptType ReceiptType
        {
            get
            {
                if (rbRecordInvoice.Checked)
                    return VendorReceiptType.Invoice;
                else if (rbRecordReceipt.Checked)
                    return VendorReceiptType.Receipt;
                else
                    return VendorReceiptType.Other;
            }
            set
            {
                rbRecordInvoice.Checked = rbRecordReceipt.Checked = rbRecordOthers.Checked = false;

                switch (value)
                {
                    case VendorReceiptType.Invoice:
                        rbRecordInvoice.Checked = true;
                        break;
                    case VendorReceiptType.Receipt:
                        rbRecordReceipt.Checked = true;
                        break;
                    default:
                        rbRecordOthers.Checked = true;
                        break;
                }
            }
        }

        public string AccountingMessage
        {
            get { return tbAccountingMessage.Text; }
            set { tbAccountingMessage.Text = value; }
        }

        public string OriginalPrice
        {
            get { return tbOriginalPrice.Text; }
            set { tbOriginalPrice.Text = value; }
        }

        public string Price
        {
            get { return tbPrice.Text; }
            set { tbPrice.Text = value; }
        }

        public string TotalQuantity
        {
            get { return tbTotalQuantity.Text; }
            set { tbTotalQuantity.Text = value; }
        }

        public string PerOrderLimit
        {
            get { return tbPerOrderLimit.Text; }
            set { tbPerOrderLimit.Text = value; }
        }

        public string PerBuyerLimit
        {
            get { return tbPerBuyerLimit.Text; }
            set { tbPerBuyerLimit.Text = value; }
        }

        public bool IsNoRefund
        {
            get { return chkIsNoRefund.Checked; }
            set { chkIsNoRefund.Checked = value; }
        }

        public bool IsDaysNoRefund
        {
            get { return chkIsDaysNoRefund.Checked; }
            set { chkIsDaysNoRefund.Checked = value; }
        }

        public bool IsExpireNoRefund
        {
            get { return chkIsExpireNoRefund.Checked; }
            set { chkIsExpireNoRefund.Checked = value; }
        }

        public bool IsTaxFree
        {
            get { return chkIsTaxFree.Checked; }
            set { chkIsTaxFree.Checked = value; }
        }

        public bool IsInputTaxFree
        {
            get { return cbIsInputTaxFree.Checked; }
            set { cbIsInputTaxFree.Checked = value; }
        }

        public bool IsHomeDelivery
        {
            get { return chkIsHomeDelivery.Checked; }
            set { chkIsHomeDelivery.Checked = value; }
        }

        public string HomeDeliveryDescription
        {
            get { return tbHomeDeliveryDescription.Text; }
            set { tbHomeDeliveryDescription.Text = value; }
        }

        public int? HomeDeliveryQuantity
        {
            get
            {
                int? quan = null;
                int q;
                if(int.TryParse(tbHomeDeliveryQuantity.Text, out q))
                    quan = q;
                return quan;
            }
            set
            {
                if (value.HasValue)
                {
                    tbHomeDeliveryQuantity.Text = value.Value.ToString();
                }
                else
                {
                    tbHomeDeliveryQuantity.Text = "";
                }
            }
        }

        public bool IsInStore
        {
            get { return chkIsInStore.Checked; }
            set { chkIsInStore.Checked = value; }
        }

        public string InStoreDescription
        {
            get { return tbInStoreDescription.Text; }
            set { tbInStoreDescription.Text = value; }
        }

        public DataTable StoreSettings
        {
            get { return (DataTable) ViewState["StoreSettingCollection"]; }
            set { ViewState["StoreSettingCollection"] = value; }
        }

        public string OrderedStoreSettings
        {
            get { return hidOrderedStoreValues.Value; }
        }

        public bool IsCouponBySpecifics
        {
            get
            {
                if (hidCouponByTotalOrBySpecifics.Value == "BySpecifics")
                    return true;
                return false;
            }
        }

        public string SmsTitle
        {
            get { return tbSmsTitle.Text; }
            set { tbSmsTitle.Text = value; }
        }

        public string SmsPrefix
        {
            get { return tbSmsPreText.Text; }
            set { tbSmsPreText.Text = value; }
        }

        public string SmsInformation
        {
            get { return tbSmsMessage.Text; }
            set { tbSmsMessage.Text = value; }
        }

        public bool DisableSMS
        {
            get { return cbx_DisableSMS.Checked; }
            set { cbx_DisableSMS.Checked = value; }
        }

        public string OptionalSettings
        {
            get { return hidOptXml.Value; }
            set { hidOptXml.Value = value; }
        }

        public Collection<Freight> FreightIncome
        {
            get { return (Collection<Freight>)ViewState["FreightIncome"]; }
            set { ViewState["FreightIncome"] = value; }
        }
        
        public Collection<Freight> FreightExpense
        {
            get { return (Collection<Freight>) ViewState["FreightExpence"]; }
            set { ViewState["FreightExpence"] = value; }
        }

        public Collection<ProductCost> ProductCosts
        {
            get { return (Collection<ProductCost>)ViewState["ProductCosts"]; }
            set { ViewState["ProductCosts"] = value; }
        }

        public bool NotDeliveryIslands
        {
            get { return cbx_NotDeliveryIslands.Checked; }
            set { cbx_NotDeliveryIslands.Checked = value; }
        }
		
        //不開立發票註記
        public bool IsInvoiceCreate
        {
            get { return !cbx_NoInvoiceCreate.Checked; }
            set { cbx_NoInvoiceCreate.Checked = !value; }
        }
		
         public bool IsShowDiscount
        {
            get { return cbShowDiscount.Checked; }
            set { cbShowDiscount.Checked = value ; }
        }

        public bool PayToCompany
        {
            get { return rbPayToCompany.Checked; }
            set
            {
                if (value)
                {
                    rbPayToCompany.Checked = true;
                }
                else
                {
                    rbPayToStore.Checked = true;
                }
            }
        }

        public bool IsCombo
        {
            get
            {
                return chkIsCombo.Checked;
            }
            set
            {
                chkIsCombo.Checked = value;
            }
        }

        public int BookingSystemType
        {
            get
            {
                if (!cb_IsUsingbookingSystem.Checked)
                {
                    return 0;
                }
                else if (rdoBookingSystemSeat.Checked)
                {
                    return 1;
                }
                else if (rdoBookingSystemRoom.Checked)
                {
                    return 2;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                switch (value)
                {
                    case 0:
                        cb_IsUsingbookingSystem.Checked = false;
                        break;
                    case 1:
                    case 2:
                        cb_IsUsingbookingSystem.Checked = true;
                        if (value == 1)
                        {
                            rdoBookingSystemSeat.Checked = true;
                        }
                        else
                        {
                            rdoBookingSystemRoom.Checked = true;
                        }
                        break;
                }
            }
        }

        public int AdvanceReservationDays
        {
            get
            {
                int val = 0;
                if (cb_IsUsingbookingSystem.Checked)
                {
                    int.TryParse(txtAdvanceReservationDays.Text.Trim(), out val);
                }
                return val;
            }
            set
            {
                txtAdvanceReservationDays.Text = value.ToString();
            }
        }

        public int CouponUsers
        {
            get
            {
                if (cb_IsUsingbookingSystem.Checked)
                {
                    if (rdoSingleUser.Checked)
                    {
                        return 1;
                    }
                    else if (rdoMultiUsers.Checked)
                    {
                        int val = 0;
                        int.TryParse(txtCouponUsers.Text.Trim(), out val);
                        return val;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                if (BookingSystemType > 0)
                {
                    txtCouponUsers.Text = "";
                    if (value > 1)
                    {
                        rdoMultiUsers.Checked = true;
                        txtCouponUsers.Text = value.ToString();
                    }
                    else
                    {
                        rdoSingleUser.Checked = true;
                    }
                }
            }
        }

        public bool IsReserveLock
        {
            get
            {
                return cbIsReserveLock.Checked;
            }
            set
            {
                cbIsReserveLock.Checked = value;
            }
        }


        public event EventHandler SaveProduct = delegate { };
        public event EventHandler<DataEventArgs<CostArgs>> NewProductPurchaseCost = delegate { };
        public event EventHandler DeleteProductPurchaseCost = delegate { };
        public event EventHandler<DataEventArgs<FreightArgs>> NewFreightIncome = delegate { };
        public event EventHandler DeleteFreightIncome = delegate { };
        public event EventHandler<DataEventArgs<FreightArgs>> NewFreightExpense = delegate { };
        public event EventHandler DeleteFreightExpense = delegate { };
        #endregion
    


        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Presenter.OnViewInitialized();
                lvStore.DataSource = this.StoreSettings;
                lvStore.DataBind();
                lvFreightIncome.DataSource = this.FreightIncome;
                lvFreightIncome.DataBind();
                lvFreightExpense.DataSource = this.FreightExpense;
                lvFreightExpense.DataBind();
                lvPurchasePrice.DataSource = this.ProductCosts;
                lvPurchasePrice.DataBind();
                string s = "<script type='text/javascript'>loadOptionals();</script>";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "loadDbOptionalData", s, false);
            }
            Presenter.OnViewLoaded();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Presenter.ProductSaved += OnProductSaved;
            SaveProduct(this, null);
        }

        public void OnProductSaved(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/ControlRoom/piinlife/productSetup.aspx?pid={0}", this.Pid));
        }

        protected void btnNewFreightExpense_Click(object sender, EventArgs e)
        {
            FreightArgs freightArgs = new FreightArgs(tbFreightExpense.Text, tb17LifeBuyAmount.Text);
            NewFreightExpense(sender, new DataEventArgs<FreightArgs>(freightArgs));
        }

        protected void btnDeleteFreightExpense_Click(object sender, EventArgs e)
        {
            DeleteFreightExpense(sender, null);
        }

        protected void btnNewFreightIncome_Click(object sender, EventArgs e)
        {
            FreightArgs freightArgs = new FreightArgs(tbFreightIncome.Text, tbUserBuyAmount.Text);
            NewFreightIncome(sender, new DataEventArgs<FreightArgs>(freightArgs));
        }

        protected void btnDeleteFreightIncome_Click(object sender, EventArgs e)
        {
            DeleteFreightIncome(sender, null);
        }

        protected void btnNewPurchasePrice_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbPurchasePrice.Text) && !string.IsNullOrEmpty(tbPurchaseQuantity.Text))
            {
                CostArgs cost = new CostArgs(tbPurchasePrice.Text, tbPurchaseQuantity.Text);
                NewProductPurchaseCost(sender, new DataEventArgs<CostArgs>(cost));
            }
        }

        protected void btnDeletePurchasePrice_Click(object sender, EventArgs e)
        {
            DeleteProductPurchaseCost(sender, null);
        }

        public void RefreshProductCost()
        {
            lvPurchasePrice.DataSource = ProductCosts;
            lvPurchasePrice.DataBind();
        }

        public void RefreshFreightIncome()
        {
            lvFreightIncome.DataSource = this.FreightIncome;
            lvFreightIncome.DataBind();
        }
        public void RefreshFreightExpense()
        {
            lvFreightExpense.DataSource = this.FreightExpense;
            lvFreightExpense.DataBind();
        }
    }
}

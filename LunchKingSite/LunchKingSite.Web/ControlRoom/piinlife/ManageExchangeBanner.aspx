﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" 
    AutoEventWireup="true" CodeBehind="ManageExchangeBanner.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.piinlife.HiDealManageExchangeBanner" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.8.20/themes/base/jquery-ui.css" type="text/css" media="all" />
	<link rel="stylesheet" href="//static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" />    
    <script type="text/javascript" src="<%=ResolveUrl("~/Tools/ckeditor/ckeditor.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Tools/ckeditor/adapters/jquery.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Tools/js/json2.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Tools/js/jquery-1.7.2.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Tools/js/jquery-ui-1.8.16.custom.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Tools/js/jquery.cycle.all.min.js") %>"></script>
        
    <script type="text/javascript">
        $().ready(function() {
            var activedTabIndex = <%=this.ActivedTabIndex %>;
            $("#tabs").tabs({ selected: activedTabIndex });

            $('.btn_delete').click(function() {
                if (confirm('資料刪除後無法恢復，確定刪除嗎？') == false) {
                    return false;
                }
            });

            $('.btn_preview').click(function() {
                var url = '<%=ResolveUrl("~/piinlife/VboBanner.aspx") %>';
                window.open(url, 'preview', 'height=85, width=780, to =0, left=0, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no');
            });

            $('.btn_test').click(function() {
                var editorId = '<%=txt_Content.ClientID %>';
                var content = CKEDITOR.instances[editorId].getData();
            });            
            
            CKEDITOR.on( 'dialogDefinition', function( ev )
            {
               var dialogName = ev.data.name;
               var dialogDefinition = ev.data.definition;
     
               if ( dialogName == 'image' )
               {
                  var targetTab = dialogDefinition.getContents( 'Link' );
                  var targetField = targetTab.get( 'cmbTarget' );
                  targetField['default'] = '_blank';
               }
            });
        });
        
        function editorContenteClientValidator(sender, args) {
            args.IsValid = false;
            var editorId = '<%=txt_Content.ClientID %>';
            var content = CKEDITOR.instances[editorId].getData();
            if (content.length>0) {
                content = content.toLowerCase();
                if (content.indexOf('<img ')!=-1 && content.indexOf('<a ')!=-1 && content.indexOf('_blank')!=-1) {
                    args.IsValid = true;   
                }
                if (content.indexOf('<img ')!=-1 && content.indexOf('<a ')==-1) {
                    sender.errormessage = "請為圖片加上連結"; <%-- $().attr() not work here >_< --%>
                }
                if (content.indexOf('<a ')!=-1 && content.indexOf('_blank')==-1) {
                    sender.errormessage = "連結必需設定為開新頁 target=_blank";
                }                
            }
        }
    </script>
    
    <style>
        #tabs {
         font-size:13px;   
        }
        .info {
            background-color: #fff49e;
            border: 1px solid #e6cb68;
            color: #333333;
        }
        .info_block {
            display:inline-block;
            margin-left: 5px;
            padding: 5px;
            width:200px;
            margin-bottom:10px;
        }
        .banners {
            font-size:11px;
            width:1000px;
        }
        .banners th {
          background-color:Gray;
          color: White;   
        }
        .banners .date-range {
            font-size:10px;
        }
        input.button {
            width:40px;
            margin-left:5px;
            padding: 2px;
            font-size:13px;
            word-spacing:5px;
        }
    </style>
    
    <!--[if IE]>
    <style>
        #tabs-1 {
            width: 900px;
        }
    </style>  
    <![endif]-->

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hf_ContentID" runat="server" />
    <div id="tabs">
        <ul>
            <li><a href="#tabs-1">Banner 上傳</a></li>
            <li><a href="#tabs-2">排程管理</a></li>
        </ul>
        <div id="tabs-1">
            <p>
                <label>
                    輪播區間：</label>
                <asp:TextBox ID="txt_StartDate" runat="server" onfocus="this.blue()" />                
                <cc1:CalendarExtender ID="ce_StartDate" TargetControlID="txt_StartDate" runat="server"
                    Format="yyyy/MM/dd">
                </cc1:CalendarExtender>
                <asp:DropDownList ID="ddl_StartHour" runat="server" />
                <asp:DropDownList ID="ddl_StartMinute" runat="server" />
                <span>～</span>
                <asp:TextBox ID="txt_EndDate" runat="server" />
                <cc1:CalendarExtender ID="ce_EndDate" TargetControlID="txt_EndDate" runat="server"
                    Format="yyyy/MM/dd">
                </cc1:CalendarExtender>
                <asp:DropDownList ID="ddl_EndHour" runat="server" />
                <asp:DropDownList ID="ddl_EndMinute" runat="server" />
            </p>
            <p>
                <div>
                    <span>內容：</span>
                    <span class="info info_block">請直接在編輯器貼入圖片。<br />758 * 87px 72dpi (JPG/GIF)</span>
                </div>
                <div>
                    <asp:TextBox ID="txt_Content" runat="server" TextMode="MultiLine"/>
            <script type="text/javascript">
                CKEDITOR.replace('<%=txt_Content.ClientID %>');
            </script>
                </div>
            </p>
            <p><button id="btn_Submit" runat="server" onserverclick="btn_Submit_onclick">儲存</button>            
            <button id="btn_Cancel" runat="server" style="margin-left:20px" onserverclick="btn_Cnacel_onclick" validationgroup="btn_cancel">取消</button></p>
        </div>
        <div id="tabs-2">
            <p>
                <asp:Repeater ID="rep_Banners" runat="server" OnItemCommand="rep_Banners_ItemCommand">
                    <HeaderTemplate>
                        <table class="banners">
                        <thead>
                            <th style="width:120px">輪播區間</th>
                            <th style="width:760px;">Banner預覽</th>
                            <th style="width:100px">編輯</th>
                        </thead>
                        <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                        <td class="date-range">
                            <div><%#((DateTime)Eval("StartTime")).ToString("yyyy/MM/dd HH:mm") %></div>
                            <div style="text-align:center;margin: 5px 0px 5px 0px">至</div>
                            <div><%#((DateTime)Eval("EndTime")).ToString("yyyy/MM/dd HH:mm")%></div>
                        </td>
                        <td style="width:760px; overflow:hidden; max-width:760px"><%#((ViewCmsRandom)Container.DataItem).Body%></td>
                        <td><asp:Button ID="btnEdit" runat="server" Text="修改" CssClass="button" ValidationGroup="ForEdit" 
                            CommandName="_EDIT" CommandArgument='<%#Eval("pid") %>' />
                            <asp:Button ID="Button1" runat="server" Text="刪除" 
                                CommandName="_DELETE" CommandArgument='<%#Eval("pid") %>'
                                CssClass="button btn_delete" style="margin-left:10px" ValidationGroup="ForDelete"/></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </p>
            <p><button class="btn_preview" type="button">預覽</button></p>
        </div>
    </div>  
    <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_StartDate" ErrorMessage="輪播區間-起始時間設定不完全" Display="None" />
    <asp:RequiredFieldValidator runat="server" ControlToValidate="txt_EndDate" ErrorMessage="輪播區間-結束時間設定不完全" Display="None" />
    <asp:CustomValidator runat="server" ClientValidationFunction="editorContenteClientValidator" ErrorMessage="內容需要有圖片與連結" Display="None" />
    <asp:ValidationSummary runat="server" ShowMessageBox="true" ShowSummary="false" />
</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.ControlRoom.piinlife
{
    public partial class HiDealManageExchangeBanner : RolePage, IHiDealManageExchangeBannerView
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Presenter.OnViewInitialized();
            }
            Presenter.OnViewLoaded();
        }

        protected void rep_Banners_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "_EDIT")
            {
                if (BannerEditing != null)
                {
                    int pid = int.Parse((string)e.CommandArgument);
                    BannerEditing(source, new DataEventArgs<int>(pid));
                }
            }
            else if (e.CommandName == "_DELETE")
            {
                if (BannerDeleting != null)
                {
                    int pid = int.Parse((string)e.CommandArgument);
                    BannerDeleting(source, new DataEventArgs<int>(pid));
                }
                this.ActivedTabIndex = 1;
            }
        }

        protected void btn_Cnacel_onclick(object sender, EventArgs e)
        {
            if (BannerCancel!=null)
            {
                BannerCancel(sender, e);
            }
        }
        protected void btn_Submit_onclick(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(ContentID))
            {
                if (BannerSaving != null)
                {
                    BannerSaving(sender, e);
                }
            }
            else
            {
                if (BannerUpdating!= null)
                {
                    BannerUpdating(sender, e);
                }
            }            
            this.NewBanner();
            this.ActivedTabIndex = 1;
        }

        #region class properties

        public int ActivedTabIndex { get; set; }

        protected DateTime? StartDate
        {
            get
            {
                DateTime result;
                if (DateTime.TryParse(txt_StartDate.Text, out result))
                {
                    return result;
                }
                return null;
            }
            set { txt_StartDate.Text = value == null ? "" : value.Value.ToString("yyyy/MM/dd"); }
        }

        protected int StartHour
        {
            get { return int.Parse(ddl_StartHour.SelectedValue); }
            set { ddl_StartHour.SelectedValue = value.ToString("00"); }
        }

        protected int StartMinute
        {
            get { return int.Parse(ddl_StartMinute.SelectedValue); }
            set { ddl_StartMinute.SelectedValue = value.ToString("00"); }
        }

        protected DateTime? EndDate
        {
            get
            {
                DateTime result;
                if (DateTime.TryParse(txt_EndDate.Text, out result))
                {
                    return result;
                }
                return null;
            }
            set { txt_EndDate.Text = value == null ? "" : value.Value.ToString("yyyy/MM/dd"); }
        }

        protected int EndHour
        {
            get { return int.Parse(ddl_EndHour.SelectedValue); }
            set { ddl_EndHour.SelectedValue = value.ToString("00"); }
        }

        protected int EndMinute
        {
            get { return int.Parse(ddl_EndMinute.SelectedValue); }
            set { ddl_EndMinute.SelectedValue = value.ToString("00"); }
        }
        #endregion

        #region interface properties

        private HiDealManageExchangeBannerPresenter _presenter;
        public HiDealManageExchangeBannerPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }


        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }
        /// <summary>
        /// 修改時才會有值，其值為CmsRandomContent的Id
        /// </summary>
        public string ContentID
        {
            get { return hf_ContentID.Value; }
            set { hf_ContentID.Value = value; }
        }

        public DateTime? StartTime
        {
            get
            {
                if (StartDate == null) return null;
                return StartDate.Value.AddHours(StartHour).AddMinutes(StartMinute);
            }
            set
            {
                if (value == null)
                {
                    StartHour = 0;
                    StartMinute = 0;
                    StartDate = null;
                }
                else
                {
                    StartHour = value.Value.Hour;
                    StartMinute = value.Value.Minute;
                    StartDate = value.Value.Date;
                }
            }
        }

        public DateTime? EndTime
        {
            get
            {
                if (EndDate == null) return null;
                return EndDate.Value.AddHours(EndHour).AddMinutes(EndMinute);
            }
            set
            {
                if (value == null)
                {
                    EndHour = 0;
                    EndMinute = 0;
                    EndDate = null;
                }
                else
                {
                    EndHour = value.Value.Hour;
                    EndMinute = value.Value.Minute;
                    EndDate = value.Value.Date;
                }
            }
        }

        public string Content
        {
            get { return HttpUtility.HtmlDecode(txt_Content.Text); }
            set { txt_Content.Text = value; }
        }
        #endregion

        #region interface methods
        public void InitTimeControl()
        {
            ddl_StartHour.Items.Clear();
            foreach (var item in new[] { 
                    "00","01","02","03","04","05","06","07","08","09","10","11",
                    "12","13","14","15","16","17","18","19","20","21","22","23"})
            {
                ddl_StartHour.Items.Add(new ListItem { Text = item, Value = item });
                ddl_EndHour.Items.Add(new ListItem { Text = item, Value = item });
            }
            foreach (var item in new[] { "00", "10", "20", "30", "40", "50" })
            {
                ddl_StartMinute.Items.Add(new ListItem { Text = item, Value = item });
                ddl_EndMinute.Items.Add(new ListItem { Text = item, Value = item });
            }
        }

        public void FillBannerList(IList<ViewCmsRandom> banners)
        {
            rep_Banners.DataSource = banners;
            rep_Banners.DataBind();
        }


        public void NewBanner()
        {
            ContentID = "";
            Content = "";
            StartTime = null;
            EndTime = null;
        }
        #endregion

        #region events

        public event EventHandler<DataEventArgs<int>> BannerEditing;
        public event EventHandler<DataEventArgs<int>> BannerDeleting;
        public event EventHandler BannerSaving;
        public event EventHandler BannerUpdating;
        public event EventHandler BannerCancel;

        #endregion
    }
}
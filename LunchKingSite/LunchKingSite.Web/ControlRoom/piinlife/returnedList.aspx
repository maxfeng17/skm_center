﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="returnedList.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.piinlife.returnedList" %>
<%@ Register TagPrefix="cc1" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=3.0.30930.28736, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="uc1" TagName="Pager" Src="~/ControlRoom/Controls/Pager.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="divSearch" runat="server">
        <a href="../Ppon/PponReturnList.aspx">好康換貨訂單列表</a>&nbsp;&nbsp;
        <a href="../Order/ReturnFormList">退貨訂單列表</a>
        <table width="920">
            <tr>
                <td style="height: 31px;">
                    <table style="border: double 3px black; width:760px">
                        <tr>
                            <th style="width: 714px;" colspan="2">
                                PiinLife退貨列表
                            </th>
                        </tr>
                         <tr>
                            <td>
                            <asp:DropDownList ID="ddlST" runat="server" DataTextField="Value" DataValueField="Key" Width="103px" />
                            </td>
                            <td>
                                <asp:textbox ID="tbSearch" runat="server"></asp:textbox>
                            </td>
                        </tr>
<%--                        <tr>
                            <td style="width: 100px;">
                                檔次名稱：
                            </td>
                            <td>--%>
                                <asp:textbox ID="tbDealName" runat="server" Visible="False" Enabled="False"></asp:textbox>
    <%--                        </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">
                                訂單編號：
                            </td>
                            <td>--%>
                                <asp:textbox ID="tbOrderId" runat="server" Enabled="False" Visible="False"></asp:textbox>
          <%--                  </td>
                        </tr>--%>
                        <tr>
                            <td style="width: 100px;">
                                申請時間：
                            </td>
                            <td>
                                <asp:TextBox ID="tbApplicationTimeS" runat="server" Columns="8" />
                                <cc1:CalendarExtender ID="ceAS" TargetControlID="tbApplicationTimeS" runat="server">
                                </cc1:CalendarExtender>
                                ～
                                <asp:TextBox ID="tbApplicationTimeE" runat="server" Columns="8" />
                                <cc1:CalendarExtender ID="ceAE" TargetControlID="tbApplicationTimeE" runat="server">
                                </cc1:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">
                                退貨單狀態：
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rblRtnStatus" runat="server" RepeatColumns="4" DataValueField="Value" DataTextField="Text"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" Text="搜尋" OnClick="btnSearch_Click"></asp:Button>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel ID="divReturned" runat="server">
        <asp:GridView ID="gvHidealReturnedList" runat="server" OnRowDataBound="gvHidealReturnedList_RowDataBound"
            AllowSorting="True" GridLines="Horizontal" ForeColor="Black" CellPadding="4"
            BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE" BackColor="White"
            EmptyDataText="無符合的資料" AutoGenerateColumns="False" Font-Size="Small" Width="1000px"
            EnableViewState="False">
            <FooterStyle BackColor="#CCCC99"></FooterStyle>
            <RowStyle BackColor="#F7F7DE" Height="30px"></RowStyle>
            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
            <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="30px">
            </HeaderStyle>
            <Columns>
                <asp:TemplateField HeaderText="DID(檔號)">
                    <ItemTemplate>
                        <asp:HyperLink ID="hlId" runat="server" Target="_blank"></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                </asp:TemplateField>                
                <asp:TemplateField HeaderText="訂單編號">
                    <ItemTemplate>
                        <asp:HyperLink ID="hlOrderId" runat="server" Target="_blank"></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="10%" />
                </asp:TemplateField>
                <asp:BoundField HeaderText="檔次名稱" DataField="DealName">
                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField HeaderText="商品編號" DataField="ProductId">
                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField HeaderText="商品名稱" DataField="ProductName">
                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="消費者名稱">
                    <ItemTemplate>
                        <asp:HyperLink ID="hlUserName" runat="server" Target="_blank"></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="10%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="退貨狀態">
                    <ItemTemplate>
                        <asp:label ID="lbReturnedStatus" runat="server"></asp:label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="10%" />
                </asp:TemplateField>
                <asp:BoundField HeaderText="申請日期" DataField="ApplicationTime" DataFormatString="{0:yyyy/MM/dd HH:mm}">
                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField HeaderText="退貨原因" DataField="ReturnReason" >
                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                </asp:BoundField>
            </Columns>
        </asp:GridView>
        <uc1:Pager ID="ucHidealPager" runat="server" PageSize="15" PageCount="1" ongetcount="GetCount"
            onupdate="UpdateHandler"></uc1:Pager>
        <asp:Button ID="btnExport" runat="server" Text="匯出" onclick="btnExport_Click" />
    </asp:Panel>
</asp:Content>

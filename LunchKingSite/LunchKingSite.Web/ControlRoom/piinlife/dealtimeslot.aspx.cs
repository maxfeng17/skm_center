﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.piinlife
{
    public partial class dealtimeslot : RolePage, IHiDealTimeSlotView
    {
        #region property
        private HiDealTimeSlotPresenter _presenter;
        public HiDealTimeSlotPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public DateTime Now
        {
            get { return DateTime.Now; }
        }
        public DateTime StartDate
        {
            get { return Now.Date.AddHours(-12); }
        }
        public DateTime EndDate
        {
            get { return Now.Date.AddDays(7); }
        }
        #endregion
        #region event
        public event EventHandler<DataEventArgs<HiDealTimeSlotViewUpdateSlotEntity>> UpdateTimeSlotSequence = null;
        public event EventHandler<DataEventArgs<int>> SelectRegionIdChange = null;
        #endregion
        #region method
        public void SetRegionData(SystemCodeCollection data, int? defRegion)
        {
            ddlRegion.DataSource = data;
            ddlRegion.DataValueField = "CodeId";
            ddlRegion.DataTextField = "CodeName";
            ddlRegion.DataBind();
            ddlRegion.Items.Insert(0, new ListItem("--請選擇--", "-1"));
            if (defRegion != null)
            {
                ddlRegion.SelectedValue = defRegion.ToString();
            }
        }



        public void GetHiDealTimeSlot(IList<ViewHiDealCityseq> mainData, IList<ViewHiDealCityseq> subData,
            ViewHiDealCollection freshVisaPriorityDeals,
            ViewHiDealCollection staleVisaPriorityDeals)
        {

            string visaFreshIcon = string.Format("<img src='{0}' title='不會顯示在EDM中。不會顯示在各分類與 \"全部\" 分類中' />",
                ResolveUrl("~/Themes/HighDeal/images/index/VISA_f.png"));
            string visaStaleIcon = string.Format("<img src='{0}' title='原Visa優先檔。會顯示在EDM中，會顯示在各分類與 \"全部\" 分類中。' />",
                ResolveUrl("~/Themes/HighDeal/images/index/VISA_f2.png"));

            int days = (EndDate - StartDate).Days;
            string html = "<table style=\"border:thin solid gray;\" border=1 ><tr><td></td>";
            for (int i = 0; i < days; i++)
            {
                html += "<td style=\"border:thin solid gray;text-align:center\">" + StartDate.AddDays(i).ToString("yyyy/MM/dd") + "</td>";
            }
            html += "</tr>";
            //foreach (var item in mainData.GroupBy(x => x.City))
            //{
            html += "<tr><td style=\"border:thin solid gray\">主打檔次</td>";
            for (int i = 0; i < days; i++)
            {
                html += "<td style=\"vertical-align:top;\"><table onclick=\"copytoblockui(this);\" style=\"font-size:smaller\">";
                var innerData =
                    mainData.Where(y => y.StartTime >= StartDate.AddDays(i) && y.StartTime < StartDate.AddDays(i + 1))
                    .OrderBy(y => y.Seq).ToList();
                var freshItems = freshVisaPriorityDeals
                    .Where(y => y.CityStartTime >= StartDate.AddDays(i) && y.CityStartTime < StartDate.AddDays(i + 1))
                    .ToList();
                var staleItems = staleVisaPriorityDeals
                    .Where(y => y.CityStartTime >= StartDate.AddDays(i) && y.CityStartTime < StartDate.AddDays(i + 1))
                    .ToList();
                foreach (var item in innerData)
                {
                    string visaIcon = "";
                    string visaClass = "";
                    if (freshItems.Any(t => t.DealId == item.HiDealId))
                    {
                        visaIcon = visaFreshIcon;
                        visaClass = "freshVisa";
                    }
                    else if (staleItems.Any(t => t.DealId == item.HiDealId))
                    {
                        visaIcon = visaStaleIcon;
                        visaClass = "staleVisa";
                    }
                    string dealUrl = string.Format("{0}/ControlRoom/piinlife/setup.aspx?did={1}",
                        WebUtility.GetSiteRoot(), item.HiDealId);
                    html += string.Format(@"
                            <tr>
                                <td style='border:thin dotted gray'>
                                    <span class='ID'>{0}</span> 
                                    <span class='SEQ'>{1}</span> 
                                    <span class='ExtAttrs' visaClass='{8}'></span>
                                    {7}<a class='NAME' href='{2}' >{3}({4})</a>
                                    <div class='TIME'>起:{5:MM/dd hh:mm}<br/>迄:{6:MM/dd hh:mm}
                                    </div>
                                </td>
                            </tr>",
                        item.Id, item.Seq, dealUrl, item.Name, item.ProductName, item.StartTime, item.EndTime,
                        visaIcon, visaClass);
                }
                html += "</table></td>";
            }
            html += "</tr><tr><td colspan=\"8\" style=\"background-color:gray\"></td></tr>";


            html += "<tr><td style=\"border:thin solid gray\">即將售完</td>";
            for (int i = 0; i < days; i++)
            {
                html += "<td style=\"vertical-align:top;\"><table onclick=\"copytoblockui(this);\" style=\"font-size:smaller\">";
                var innerData =
                    subData.Where(y => y.StartTime >= StartDate.AddDays(i) && y.StartTime < StartDate.AddDays(i + 1)).OrderBy(y => y.Seq).ToList();
                var freshItems = freshVisaPriorityDeals
                    .Where(y => y.CityStartTime >= StartDate.AddDays(i) && y.CityStartTime < StartDate.AddDays(i + 1))
                    .ToList();
                var staleItems = staleVisaPriorityDeals
                    .Where(y => y.CityStartTime >= StartDate.AddDays(i) && y.CityStartTime < StartDate.AddDays(i + 1))
                    .ToList();
                foreach (var item in innerData)
                {
                    string visaIcon = "";
                    string visaClass = "";
                    if (freshItems.Any(t => t.DealId == item.HiDealId))
                    {
                        visaIcon = visaFreshIcon;
                        visaClass = "freshVisa";
                    }
                    else if (staleItems.Any(t => t.DealId == item.HiDealId))
                    {
                        visaIcon = visaStaleIcon;
                        visaClass = "staleVisa";
                    }
                    string dealUrl = WebUtility.GetSiteRoot() + "/ControlRoom/piinlife/setup.aspx?did=" + item.HiDealId;

                    html += string.Format(@"
                            <tr>
                                <td style=""border:thin dotted gray"">
                                    <span class=""ID"">{0}</span> 
                                    <span class=""SEQ"">{1}</span> 
                                    <span class='ExtAttrs' visaClass='{8}'></span>
                                    {7}<a class='NAME' href='{2}' >{3}({4})</a>
                                    <div class=""TIME"">起:{5:MM/dd hh:mm}<br/>迄:{6:MM/dd hh:mm}</div>
                                </td>
                            </tr>",
                        item.Id, item.Seq, dealUrl, item.Name, item.ProductName, item.StartTime, item.EndTime,
                        visaIcon, visaClass);

                    //var dealUrl = WebUtility.GetSiteRoot() + "/ControlRoom/piinlife/setup.aspx?did=" + innitem.HiDealId;
                    //html += "<tr><td style=\"border:thin dotted gray\">" + "<span class=\"ID\">" + innitem.Id + "</span> <span class=\"SEQ\">" + innitem.Seq +
                    //    "</span><a class='NAME' href='" + dealUrl + "' >" + innitem.Name +
                    //    "</a><div class=\"TIME\">起:" + innitem.StartTime.ToString("MM/dd hh:mm") + "<br/>迄:" + innitem.EndTime.Value.ToString("MM/dd hh:mm") + "</div></td></tr>";
                }
                html += "</table></td>";
            }
            html += "</tr><tr><td colspan=\"8\" style=\"background-color:gray\"></td></tr>";
            //}
            html += "</table>";
            lit_html.Text = html;
        }

        #endregion
        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();

        }
        protected void ChangeSequence(object sender, EventArgs e)
        {
            string[] seqpair = hif_Seq.Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            List<KeyValuePair<int, int>> list_seq = new List<KeyValuePair<int, int>>();
            foreach (string item in seqpair)
            {
                string[] id_seq = item.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                if (id_seq.Length == 2)
                {
                    int id, seq;
                    if (int.TryParse(id_seq[0], out id) && int.TryParse(id_seq[1], out seq))
                        list_seq.Add(new KeyValuePair<int, int>(id, seq));
                }
            }
            int selectRegion = 0;
            if (list_seq.Count > 0 && UpdateTimeSlotSequence != null && int.TryParse(ddlRegion.SelectedValue, out selectRegion))
            {
                var data = new HiDealTimeSlotViewUpdateSlotEntity() { NewSlotList = list_seq, SelectRegionId = selectRegion };
                this.UpdateTimeSlotSequence(sender, new DataEventArgs<HiDealTimeSlotViewUpdateSlotEntity>(data));
            }
        }

        protected void SelectRegionChanged(object sender, EventArgs e)
        {
            int selectRegion = 0;
            if (int.TryParse(ddlRegion.SelectedValue, out selectRegion))
            {
                if (SelectRegionIdChange != null)
                {
                    SelectRegionIdChange(this, new DataEventArgs<int>(selectRegion));
                }
            }
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Expressions;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace LunchKingSite.Web.ControlRoom.piinlife
{
    public partial class returnedList : RolePage, IHiDealReturnedListView
    {
        #region event

        public event EventHandler<DataEventArgs<HiDealReturnedLightListViewSearchRequest>> SearchClicked = null;
        public event EventHandler<DataEventArgs<HiDealReturnedLightListViewSearchRequest>> ExportClicked = null;

        #endregion

        #region property

        private HiDealReturnedListPresenter _presenter;
        public HiDealReturnedListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        protected static IHiDealProvider hp;
        protected IMemberProvider mp;

        #endregion property

        #region method

        public void ShowMessage(string message)
        {

        }

        public void ShowData(ViewHiDealReturnedLightCollection dataCollection, int dataCount)
        {
            DataCount = dataCount;
            gvHidealReturnedList.DataSource = dataCollection;
            gvHidealReturnedList.DataBind();
        }

        /// <summary>
        /// 分頁查詢資料
        /// </summary>
        /// <param name="pageNumber"></param>
        protected void UpdateHandler(int pageNumber)
        {
            SearchData(pageNumber);
        }

        /// <summary>
        /// 取得資料筆數
        /// </summary>
        /// <returns></returns>
        protected int GetCount()
        {
            return DataCount;
        }

        private void SearchData(int pageNum)
        {
            if(SearchClicked!=null)
            {
                HiDealReturnedLightListViewSearchRequest data = this.ViewSearchRequestGet(pageNum);
                SearchClicked(this, new DataEventArgs<HiDealReturnedLightListViewSearchRequest>(data));
            }
        }

        /// <summary>
        /// 依據使用者輸入的條件, 產出SearchRequest物件
        /// </summary>
        /// <param name="pageNum"></param>
        /// <returns></returns>
        public HiDealReturnedLightListViewSearchRequest ViewSearchRequestGet(int pageNum) 
        {
            DateTime? applicationTimeS = null;
            DateTime? applicationTimeE = null;
            HiDealReturnedStatus? returnedStatus = null;
            DateTime tmp;
            if (DateTime.TryParse(tbApplicationTimeS.Text, out tmp))
            {
                applicationTimeS = tmp;
            }
            if (DateTime.TryParse(tbApplicationTimeE.Text, out tmp))
            {
                applicationTimeE = tmp;
            }
            if (!rblRtnStatus.SelectedValue.Equals("-1"))
            {
                returnedStatus = (HiDealReturnedStatus)(int.Parse(rblRtnStatus.SelectedValue));
            }

            var data = new HiDealReturnedLightListViewSearchRequest();
            data.ApplicationTimeStart = applicationTimeS;
            data.ApplicationTimeEnd = applicationTimeE;
            data.OrderId = ddlST.SelectedValue == ViewHiDealReturnedLight.Columns.OrderId ? ddlST.SelectedValue : string.Empty;
            data.DealName = ddlST.SelectedValue == ViewHiDealReturnedLight.Columns.DealName ? ddlST.SelectedValue : string.Empty;
            data.HiDealId = ddlST.SelectedValue == ViewHiDealReturnedLight.Columns.Id ? ddlST.SelectedValue : string.Empty;
            data.ReturnedStatus = returnedStatus;

            data.PageNum = pageNum;// ucHidealPager.PageCount;
            data.PageSize = ucHidealPager.PageSize;

            return data;
        }

        /// <summary>
        /// 傳入品生活退貨集合物件, 將其匯出成Excel
        /// </summary>
        /// <param name="rtnCols">ViewHiDealReturnedLightCollection</param>
        public void Export(ViewHiDealReturnedLightCollection rtnCols) 
        {
            if (rtnCols.Count > 0) 
            {
                DataTable dt = new DataTable();
                mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

                #region 建構表頭

                DataRow drHeader = dt.NewRow();
                int counter = 0;
                string[] headers = new string[] { "退貨/換貨狀態", "DID(檔號)", "檔次名稱", "商品編號", "商品名稱", "申請日期", "結檔時間", "收件者"
                ,"連絡電話", "地址", "數量", "退貨原因", "廠商回覆"};

                foreach (string s in headers)
                {
                    DataColumn dc = new DataColumn(s);
                    dt.Columns.Add(dc);
                    drHeader[counter] = dc.ColumnName;
                    counter++;
                }
                dt.Rows.Add(drHeader);

                #endregion

                #region 建構表體

                foreach (ViewHiDealReturnedLight rtn in rtnCols) 
                {
                    DataRow dr = dt.NewRow();

                    //退換貨狀態
                    switch (rtn.ReturnedStatus) {
                        case (int)HiDealReturnedStatus.Cancel:
                            dr[0] = "取消退貨";
                            break;
                        case (int)HiDealReturnedStatus.Completed:
                            dr[0] = "完成退貨";
                            break;
                        case (int)HiDealReturnedStatus.Create:
                            dr[0] = "新成立退貨單";
                            break;
                        case (int)HiDealReturnedStatus.Fail:
                            dr[0] = "退貨失敗";
                            break;
                        default:
                            dr[0] = string.Empty;
                            break;
                    }

                    //DID檔號
                    dr[1] = rtn.Id;

                    //檔次名稱
                    dr[2] = rtn.DealName;

                    //商品編號
                    dr[3] = rtn.ProductId;

                    //商品名稱
                    dr[4] = rtn.ProductName;

                    //申請日期
                    dr[5] = ((DateTime)rtn.ApplicationTime).ToString("yyyy-MM-dd");

                    //結檔時間
                    dr[6] = ((DateTime)rtn.DealEndTime).ToString("yyyy-MM-dd HH:mm:ss");

                    //收件者
                    dr[7] = rtn.MemberName;

                    //聯絡電話
                    dr[8] = rtn.Mobile;

                    //地址 (這邊為了不要影響HiDealReturnedLight的結構, 不直接去Join MemberDelivery, 而依迴圈抓出地址)
                    MemberDeliveryCollection mdc = mp.MemberDeliveryGetCollection(rtn.UniqueId, true);
                    foreach (MemberDelivery md in mdc) {
                        dr[9] += md.Address + "  ";    
                    }

                    //數量
                    dr[10] = rtn.ItemQuantity;

                    //退貨原因
                    dr[11] = rtn.ReturnReason;

                    //廠商回覆 (放空值給廠商填)
                    dr[12] = string.Empty;

                    dt.Rows.Add(dr);
                }

                #endregion

                #region 產生Excel試算表

                Workbook workbook = new HSSFWorkbook();

                // 新增試算表。
                Sheet sheet = workbook.CreateSheet("退貨報表");

                Cell c = null;
                int rowCount = 0;
                int rowIndex = 0;
                foreach (DataRow row in dt.Rows)
                {
                    Row dataRow = sheet.CreateRow(rowIndex);
                    foreach (DataColumn column in dt.Columns)
                    {
                        c = dataRow.CreateCell(column.Ordinal);
                        c.SetCellValue(row[column].ToString());
                    }
                    rowIndex++;
                    rowCount++;
                }

                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode("RefundReport.xls")));
                workbook.Write(HttpContext.Current.Response.OutputStream);

                #endregion
            }
        }

        #endregion

        #region class property
        public int DataCount { get; set; }
        public Dictionary<string, string> FilterTypes
        {
            get
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add(ViewHiDealReturnedLight.Columns.OrderId, Resources.Localization.OrderId);
                data.Add(ViewHiDealReturnedLight.Columns.DealName, Resources.Localization.DealName);
                data.Add(ViewHiDealReturnedLight.Columns.Id, Resources.Localization.HiDealId);
                data.Add(ViewHiDealReturnedLight.Columns.ProductId, Resources.Localization.ProductId);
                return data;
            }
        }

        public void SetFilterTypeDropDown(Dictionary<string, string> types)
        {
            ddlST.DataSource = types;
            ddlST.DataBind();
        }

        public string FilterType
        {
            get { return ddlST.SelectedValue; }
            set { ddlST.SelectedValue = value; }
        }

        public string FilterUser
        {
            get { return tbSearch.Text; }
            set { tbSearch.Text = value; }
        }
        #endregion class property

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
                List<ListItem> rtnStatus =  WebUtility.GetListItemFromEnumWithSystemCode(HiDealReturnedStatus.Create);
                rtnStatus.Insert(0, new ListItem("忽略", "-1"));
                rblRtnStatus.DataSource = rtnStatus;
                rblRtnStatus.SelectedIndex = 0;
                rblRtnStatus.DataBind();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SearchData(1);
            ucHidealPager.ResolvePagerView(1, true);
        }

        /// <summary>
        /// 匯出Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (ExportClicked != null) 
            {
                HiDealReturnedLightListViewSearchRequest data = this.ViewSearchRequestGet(0);
                ExportClicked(this, new DataEventArgs<HiDealReturnedLightListViewSearchRequest>(data));
            }
        }

        protected void gvHidealReturnedList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var data = (ViewHiDealReturnedLight)e.Row.DataItem;
                var hlUserName = (HyperLink)e.Row.FindControl("hlUserName");
                var lbReturnedStatus = (Label)e.Row.FindControl("lbReturnedStatus");
                var hlOrderId = (HyperLink)e.Row.FindControl("hlOrderId");
                var hlId = (HyperLink)e.Row.FindControl("hlId");


                lbReturnedStatus.Text = SystemCodeManager.GetSystemCodeNameByEnumValue((HiDealReturnedStatus) data.ReturnedStatus);
                hlUserName.Text = data.LastName + data.FirstName;
                //hlUserName.NavigateUrl = ResolveUrl("~/controlroom/piinlife/hidealorderdetaillist.aspx?oid=" + data.OrderGuid);
                hlOrderId.Text = data.OrderId;
                hlOrderId.NavigateUrl = ResolveUrl("~/controlroom/piinlife/hidealorderdetaillist.aspx?oid=" + data.OrderGuid);
                hlId.Text = data.Id.ToString();
            }
        }

        #endregion page

    }
}
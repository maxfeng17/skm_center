﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.piinlife
{
    public partial class deal : RolePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            IHiDealProvider hp= ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();

            try
            {
                #region CreateSeller
                Seller s = new Seller();
                s.CityId = 2;
                s.Guid = Guid.NewGuid();
                s.CreateId = Page.User.Identity.Name;
                s.CreateTime = DateTime.Now;
                s.SellerName = "HidealTest" + DateTime.Now.ToString("yyyyMMdd");
                sp.SellerSet(s);
                #endregion

                #region CreateDeal
                HiDealDeal deal = new HiDealDeal();
                deal.HiDealGuid = Guid.NewGuid();
                deal.SellerGuid = s.Guid;
                deal.DealStartTime = DateTime.Now.AddDays(-1);
                deal.DealEndTime = DateTime.Now.AddMonths(1);
                deal.DealType = 0;
                deal.Name = "HidealTest" + DateTime.Now.ToString("yyyyMMdd");
                deal.PromoLongDesc = "this is desc";
                deal.IsOpen = true;
                deal.IsShow = true;
                deal.IsSoldout = false;
                deal.Sales = "jacky";
                deal.SalesDept = "IT";
                deal.CreateId = Page.User.Identity.Name;
                deal.CreateTime = DateTime.Now;
                deal.Cities = "TP,TY";
                hp.HiDealDealSet(deal);
                #endregion

                #region AddCity
                HiDealCity city = new HiDealCity();
                city.City = "TP";
                city.HiDealId = deal.Id;
                city.CityCodeGroup = "HiDealRegion";
                city.CityCodeId = 1;
                city.StartTime = DateTime.Now.AddDays(-1);
                city.Seq = 0;
                hp.HiDealCitySet(city);
                city = new HiDealCity();
                city.City = "TP";
                city.CityCodeGroup = "HiDealRegion";
                city.CityCodeId = 1;
                city.HiDealId = deal.Id;
                city.StartTime = DateTime.Now;
                city.Seq = 0;
                hp.HiDealCitySet(city);
                city = new HiDealCity();
                city.City = "TP";
                city.CityCodeGroup = "HiDealRegion";
                city.CityCodeId = 1;
                city.HiDealId = deal.Id;
                city.StartTime = DateTime.Now.AddDays(1);
                city.Seq = 0;
                hp.HiDealCitySet(city);
                city = new HiDealCity();
                city.HiDealId = deal.Id;
                city.City = "TY";
                city.CityCodeGroup = "HiDealRegion";
                city.CityCodeId = 2;
                city.StartTime = DateTime.Now.AddDays(-1);
                city.Seq = 0;
                hp.HiDealCitySet(city);
                city = new HiDealCity();
                city.HiDealId = deal.Id;
                city.City = "TY";
                city.CityCodeGroup = "HiDealRegion";
                city.CityCodeId = 2;
                city.StartTime = DateTime.Now;
                city.Seq = 0;
                hp.HiDealCitySet(city);
                city = new HiDealCity();
                city.HiDealId = deal.Id;
                city.City = "TY";
                city.CityCodeGroup = "HiDealRegion";
                city.CityCodeId = 2;
                city.StartTime = DateTime.Now.AddDays(1);
                city.Seq = 0;
                hp.HiDealCitySet(city);
                #endregion

                #region AddCategory
                HiDealCategory category = new HiDealCategory();
                category.CategoryCodeName = "Food";
                category.HiDealId = deal.Id;
                category.CategoryCodeGroup = "HiDealCatg";
                category.CategoryCodeId = 3;
                hp.HiDealCategorySet(category);
                category = new HiDealCategory();
                category.CategoryCodeName = "Restaurant";
                category.HiDealId = deal.Id;
                category.CategoryCodeGroup = "HiDealCatg";
                category.CategoryCodeId = 1;
                hp.HiDealCategorySet(category);

                #endregion

                #region AddProduct
                HiDealProduct product = new HiDealProduct();
                product.DealId = deal.Id;
                product.SellerGuid = s.Guid;
                product.Description = "123";
                product.Guid = Guid.NewGuid();
                product.Name = "產品一";
                product.Seq = 0;
                product.CreateId = Page.User.Identity.Name;
                product.CreateTime = DateTime.Now;
                product.OriginalPrice = 20;
                product.Price = 10;
                hp.HiDealProductSet(product);

                product = new HiDealProduct();
                product.DealId = deal.Id;
                product.SellerGuid = s.Guid;
                product.Description = "123";
                product.Guid = Guid.NewGuid();
                product.Name = "產品二";
                product.Seq = 1;
                product.Price = 20;
                product.OriginalPrice = 20;
                product.CreateId = Page.User.Identity.Name;
                product.CreateTime = DateTime.Now;
                hp.HiDealProductSet(product);
                #endregion

                #region AddItem
                HiDealProductOptionCategory optionCategory = new HiDealProductOptionCategory();
                

               

                #endregion
                lblResult.Text = "完成";
            }
            catch (Exception ex)
            {
                lblResult.Text = "Error:" + ex.Message;
            }
        }
    }
}
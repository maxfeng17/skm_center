﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="DealList.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.piinlife.DealList" %>

<%@ Register TagPrefix="cc1" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=3.0.30930.28736, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register Src="~/ControlRoom/Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="divSearch" runat="server">
        <table width="920">
            <tr>
                <td style="height: 31px;">
                    <table style="border: double 3px black; width: 760px">
                        <tr>
                            <th style="width: 714px;">
                                PiinLife檔次列表
                            </th>
                        </tr>
                        <tr>
                            <td style="width: 714px;">
                                <asp:DropDownList ID="ddlFilter" runat="server" Width="103px">
                                    <asp:ListItem Text="店家名稱" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="檔次名稱" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="DID(檔號)" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="PID(商品檔號)" Value="3"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="txtFilter" runat="server" Width="145px"></asp:TextBox>
                                <br />
                                開始時間：
                                <asp:TextBox ID="txtStartTimeS" runat="server" Columns="8" />
                                <cc1:CalendarExtender ID="ceSS" TargetControlID="txtStartTimeS" runat="server">
                                </cc1:CalendarExtender>
                                至
                                <asp:TextBox ID="txtStartTimeE" runat="server" Columns="8" />
                                <cc1:CalendarExtender ID="csSE" TargetControlID="txtStartTimeE" runat="server">
                                </cc1:CalendarExtender>
                                <br />
                                截止時間：
                                <asp:TextBox ID="txtEndTimeS" runat="server" Columns="8" />
                                <cc1:CalendarExtender ID="ceES" TargetControlID="txtEndTimeS" runat="server">
                                </cc1:CalendarExtender>
                                至
                                <asp:TextBox ID="txtEndTimeE" runat="server" Columns="8" />
                                <cc1:CalendarExtender ID="ceEE" TargetControlID="txtEndTimeE" runat="server">
                                </cc1:CalendarExtender>
                                <br />
                                篩選狀態：
                                <asp:RadioButtonList runat="server" ID="rdoDealWorkType" RepeatColumns="4">
                                    <asp:ListItem>忽略</asp:ListItem>
                                    <asp:ListItem>尚未開始</asp:ListItem>
                                    <asp:ListItem>進行中</asp:ListItem>
                                    <asp:ListItem>已結束</asp:ListItem>
                                </asp:RadioButtonList>
                                <br />
                                &nbsp;<asp:Button ID="btnSearch" runat="server" Text="搜尋" OnClick="btnSearch_Click">
                                </asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="divOrder" runat="server">
        <asp:Label ID="lblErrorMsg" runat="server" ForeColor="Red"></asp:Label>
        <asp:GridView ID="gvHidealDealList" runat="server" OnRowDataBound="gvHidealDealList_RowDataBound"
            AllowSorting="True" GridLines="Horizontal" ForeColor="Black" CellPadding="4"
            BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE" BackColor="White"
            EmptyDataText="無符合的資料" AutoGenerateColumns="False" Font-Size="Small" Width="1000px"
            EnableViewState="False">
            <FooterStyle BackColor="#CCCC99"></FooterStyle>
            <RowStyle BackColor="#F7F7DE" Height="30px"></RowStyle>
            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
            <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="30px">
            </HeaderStyle>
            <Columns>
                <asp:BoundField HeaderText="店家名稱" DataField="SellerName">
                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField HeaderText="DID(檔號)" DataField="DealId">
                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="檔次名稱">
                    <ItemTemplate>
                        <asp:HyperLink ID="hkDealName" runat="server" Target="_blank"></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="30%" />
                </asp:TemplateField>
                <asp:BoundField HeaderText="PID(商品檔號)" DataField="ProductId">
                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField HeaderText="商品名稱" DataField="ProductName">
                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="狀態">
                    <ItemTemplate>
                        <asp:Label ID="lbDealWorkingType" runat="server"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="10%" />
                </asp:TemplateField>
                <asp:BoundField HeaderText="開始日期" DataField="DealStartTime" DataFormatString="{0:yyyy/MM/dd HH:mm}">
                    <ItemStyle Width="8%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField HeaderText="截止日期" DataField="DealEndTime" DataFormatString="{0:yyyy/MM/dd HH:mm}">
                    <ItemStyle Width="8%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField HeaderText="銷售數字" DataField="OrderCount" DataFormatString="{0:d}">
                    <ItemStyle Width="8%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField HeaderText="退貨數字" DataField="ReturnedCount" DataFormatString="{0:d}">
                    <ItemStyle Width="8%" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="實際銷售">
                    <ItemTemplate>
                        <asp:Label ID="lbSellCount" runat="server"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="8%" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <uc1:Pager ID="ucHidealPager" runat="server" PageSize="15" PageCount="1" ongetcount="GetCount"
            onupdate="UpdateHandler"></uc1:Pager>
    </asp:Panel>
</asp:Content>

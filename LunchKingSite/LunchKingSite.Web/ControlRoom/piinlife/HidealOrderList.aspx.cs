﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;

namespace LunchKingSite.Web.ControlRoom.piinlife
{
    public partial class HidealOrderList : RolePage, IHidealOrderListView
    {
        #region property
        private HidealOrderListPresenter _presenter;
        public HidealOrderListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public int FilterTypeValue
        {
            get
            {
                return Convert.ToInt32(ddlFilter.SelectedValue);
            }
        }

        public string FilterText
        {
            get
            {
                return txtFilter.Text.Trim();
            }
        }

        public string FilterInternal
        {
            get
            {
                string filter = string.Empty;
                if (!string.IsNullOrEmpty(txtCreateTimeS.Text) && !string.IsNullOrEmpty(txtCreateTimeE.Text))
                    filter = ViewHiDealOrderMemberOrderShow.Columns.CreateTime + " between " + txtCreateTimeS.Text + " and " + txtCreateTimeE.Text;
                return filter;
            }
        }

        public int PageSize
        {
            get { return ucHidealPager.PageSize; }
            set { ucHidealPager.PageSize = value; }
        }
        #endregion

        #region event
        public event EventHandler OnSearchClicked;
        public event EventHandler<DataEventArgs<int>> OnGetHidealOrderCount;
        public event EventHandler<DataEventArgs<int>> HidealOrderPageChanged;
        
        #endregion

        #region method
        public void GetHiDealOrderList(ViewHiDealOrderMemberOrderShowCollection dataList)
        {
            gvHidealOrderList.DataSource = dataList;
            gvHidealOrderList.DataBind();
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
                SetDdl();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.OnSearchClicked != null)
                this.OnSearchClicked(this, e);

            ucHidealPager.ResolvePagerView(1, true);
        }

        protected void HidealOrderUpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.HidealOrderPageChanged != null)
                this.HidealOrderPageChanged(this, e);
        }

        protected int GetHidealOrderCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetHidealOrderCount != null)
                OnGetHidealOrderCount(this, e);
            return e.Data;
        }

        protected void gvHidealOrderList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ViewHiDealOrderMemberOrderShow data = (ViewHiDealOrderMemberOrderShow)e.Row.DataItem;
                HyperLink hkOrderId = (HyperLink)e.Row.FindControl("hkOrderId");
                HyperLink hkHiDealId = (HyperLink)e.Row.FindControl("hkHiDealId");
                HyperLink hkMemberName = (HyperLink)e.Row.FindControl("hkMemberName");
                HyperLink hkProductName = (HyperLink)e.Row.FindControl("hkProductName");
                Label lblStatus = (Label)e.Row.FindControl("lblStatus");

                hkOrderId.Text = data.OrderId;
                hkOrderId.NavigateUrl = ResolveUrl("~/controlroom/piinlife/hidealorderdetaillist.aspx?oid=") + data.Guid;
                hkHiDealId.Text = Convert.ToString(data.HiDealId);
                hkMemberName.Text = data.MemberName;
                hkMemberName.NavigateUrl = ResolveUrl("~/controlroom/user/users_edit.aspx?username=") + data.UserName;
                hkProductName.Text = data.ProductName;
                hkProductName.NavigateUrl = ResolveUrl("~/piinlife/HiDeal.aspx?pid=") + data.ProductId;
                lblStatus.Text = data.OrderShowStatus != null ? ((HiDealOrderShowStatus)data.OrderShowStatus).ToString() : string.Empty;
            }
        }

        protected void SetDdl()
        {
            //訂單編號 0, 會員帳號 1, 會員姓名 2, 手機 3, 商品名稱 4, 檔號 5
            ddlFilter.Items.Clear();
            ddlFilter.Items.Add(new ListItem(Resources.Localization.OrderId, "0"));
            ddlFilter.Items.Add(new ListItem(Resources.Localization.MemberEmail, "1"));
            ddlFilter.Items.Add(new ListItem(Resources.Localization.MemberName, "2"));
            ddlFilter.Items.Add(new ListItem(Resources.Localization.MobileNumber, "3"));
            ddlFilter.Items.Add(new ListItem(Resources.Localization.ItemName, "4"));
            ddlFilter.Items.Add(new ListItem(Resources.Localization.HiDealId, "5"));
            ddlFilter.Items.Add(new ListItem(Resources.Localization.ProductId, "6"));
        }

        #endregion
    }
}
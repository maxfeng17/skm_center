﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.UI;
public partial class accessory_list : RolePage
{
    private const int DEFAULT_PAGE_SIZE = 15;
    private int pageSize;

    IItemProvider ip;
    ISellerProvider sp;
    Boolean isUpdate = false;
    Guid guidItem;
    Guid guidAccessoryGroup;
    Guid guidAccessoryGroupMember;
    Guid guidSeller;
    AccessoryGroup clsAG;
    Seller clsSeller;
    Item clsItem;
    AccessoryGroupMember clsAGM;
    DataTable dtAccessory = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        pageSize = (gvAccessory.PageSize == 0) ? DEFAULT_PAGE_SIZE : gvAccessory.PageSize;

        if (!String.IsNullOrEmpty(Request.QueryString["iid"]) && !String.IsNullOrEmpty(Request.QueryString["mid"]))
        {
            guidItem = new Guid(Request["iid"].ToString());
        }
        else
        {
            Response.Redirect("../Seller/seller_list.aspx");
        }
        if (!String.IsNullOrEmpty(Request.QueryString["agmid"]) && !String.IsNullOrEmpty(Request.QueryString["agid"]))
        {
            guidAccessoryGroup = new Guid(Request["agid"].ToString());
            guidAccessoryGroupMember = new Guid(Request["agmid"].ToString());
            isUpdate = true;
        }
        else
        {
            isUpdate = false;
        }
        guidSeller = findSellerGUID();

        FillLabel();

        if (!Page.IsPostBack)
        {
            LoadAccessoryCategoryDropDownList();
            BindAccessoryGroupList();
            LoadAccessoryGroup();
            if (isUpdate)
            {
                LoadAccessoryGroupMember();
            }
            else
            {
                LoadAccessoryDropDownList();

            }
            if (ddlAccessoryGroup.Items.Count > 0)
            {
                if (isUpdate)
                {
                    ddlAccessoryGroup.SelectedIndex = ddlAccessoryGroup.Items.IndexOf(ddlAccessoryGroup.Items.FindByValue(guidAccessoryGroup.ToString()));
                }
                else
                {
                    ddlAccessoryGroup.SelectedIndex = 0;
                }
                LoadGrid("", 1, "");
                BindData();
            }
        }
        if (isUpdate)
        {
            lblMode.Text = "編輯";
        }
        else
        {
            lblMode.Text = "檢視";
        }
    }

#region aux functions: FillLabel(), findSellerGUID()
    private void FillLabel()
    {
        clsSeller = sp.SellerGet(guidSeller);
        lblSellerName.Text = clsSeller.SellerName;
        clsItem = ip.ItemGet(guidItem);
        lblItemName.Text = clsItem.ItemName;
    }
    private Guid findSellerGUID()
    {
        return Guid.Empty;
    }
#endregion
    private void LoadAccessoryGroup()
    {
        ddlAccessoryGroup.Items.Clear();
        AccessoryGroupCollection agc = ip.AccessoryGroupGetListByItem(guidItem);
        foreach (AccessoryGroup ag in agc)
        {
            ddlAccessoryGroup.Items.Add(new ListItem(ag.AccessoryGroupName, ag.Guid.ToString()));
        }
    }
    private void LoadAccessoryDropDownList()
    {
        ddlAccessory.Items.Clear();
        AccessoryCollection aCol = ip.AccessoryGetList(new Guid( ddlAccessoryCategory.SelectedValue));
        foreach (Accessory a in aCol)
        {
            ddlAccessory.Items.Add(new ListItem(a.AccessoryName, a.Guid.ToString()));
        }
        if (ddlAccessory.Items.Count == 0)
        {
            ddlAccessory.Items.Add(new ListItem("沒有資料"));
            ddlAccessory.Enabled = false;
        }
        else
        {
            ddlAccessory.Enabled = true;
        }
    }
    private void LoadAccessoryCategoryDropDownList()
    {
        ddlAccessoryCategory.Items.Clear();
        AccessoryCategoryCollection acCol = ip.AccessoryCategoryGetList();
        foreach (AccessoryCategory ac in acCol)
        {
            ddlAccessoryCategory.Items.Add(new ListItem(ac.AccessoryCategoryName, ac.Guid.ToString()));
        }
        if (ddlAccessoryCategory.Items.Count == 0)
        {
            ddlAccessoryCategory.Items.Add(new ListItem("沒有資料"));
            ddlAccessoryCategory.Enabled = false;
        }
        else
        {
            ddlAccessoryCategory.Enabled = true;
        }
        ddlAccessoryCategory.SelectedIndex = 0;
    }
    private void BindAccessoryGroupList()
    {
        ddlAccessoryGroupName.Items.Clear();
        AccessoryGroupCollection acCol = ip.AccessoryGroupGetListBySeller(guidSeller);
        if (acCol.Count == 0)
        {
            ddlAccessoryGroupName.Enabled = false;
            ddlAccessoryGroupName.Items.Add(new ListItem("此項目沒有配料"));
        }
        else
        {
            ddlAccessoryGroupName.Enabled = true;
            foreach (AccessoryGroup ag in acCol)
            {
                ddlAccessoryGroupName.Items.Add(new ListItem(ag.AccessoryGroupName, ag.Guid.ToString()));
            }
        }
    }

    private void LoadAccessoryGroupMember()
    {
        ddlAccessoryGroupName.SelectedIndex = ddlAccessoryGroupName.Items.IndexOf(ddlAccessoryGroupName.Items.FindByValue(guidAccessoryGroup.ToString()));
        AccessoryGroupMember agm = ip.AccessoryGroupMemberGet(guidAccessoryGroupMember);
        tbAccessoryGroupMemberNote.Text = agm.AccessoryGroupMemberNote;
        tbAccessoryGroupMemberSequence.Text = agm.Sequence.ToString();
        tbAccessoryGroupMemberPrice.Text = agm.AccessoryGroupMemberPrice.ToString();
        AccessoryCategory clsAccessoryCategory = ip.AccessoryCategoryGetByAccessory(agm.AccessoryGuid);
        ddlAccessoryCategory.SelectedIndex = ddlAccessoryCategory.Items.IndexOf(ddlAccessoryCategory.Items.FindByValue(clsAccessoryCategory.Guid.ToString()));
        LoadAccessoryDropDownList();
        ddlAccessory.SelectedIndex = ddlAccessory.Items.IndexOf(ddlAccessory.Items.FindByValue(agm.AccessoryGuid.ToString()));
    }

    private void BindData()
    {
        gvAccessory.DataSource = dtAccessory;
        gvAccessory.DataBind();
    }

    private void LoadGrid(string filter, int pageNumber, string orderBy)
    {
            guidAccessoryGroup = new Guid(ddlAccessoryGroup.SelectedValue);
            //dtAccessory = ip.AccessoryGroupMemberGetList(filter, pageNumber, pageSize, orderBy, guidAccessoryGroup, guidItem);
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        AccessoryGroup ag = new AccessoryGroup();
        ag.Guid = Helper.GetNewGuid(ag);
        ag.AccessoryGroupName = tbGroupName.Text;
        ag.CreateTime = DateTime.Now;
        ag.SellerGuid = guidSeller;
        ip.AccessoryGroupSet(ag);
        ItemAccessoryGroupList iagl = new ItemAccessoryGroupList();
        iagl.Guid = Helper.GetNewGuid(iagl);
        iagl.ItemGuid = guidItem;
        iagl.AccessoryGroupGuid = ag.Guid;
        iagl.Sequence = 1;
        this.BindAccessoryGroupList();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (isUpdate)
        {
            clsAGM = ip.AccessoryGroupMemberGet(guidAccessoryGroupMember);
            clsAGM.AccessoryGroupGuid = guidAccessoryGroup;
            clsAGM.MarkOld();
            clsAGM.ModifyTime = DateTime.Now;
        }
        else
        {

            clsAGM = new AccessoryGroupMember();
            clsAGM.Guid = Helper.GetNewGuid(clsAGM);
            clsAGM.AccessoryGroupGuid = new Guid(ddlAccessoryGroupName.SelectedValue);
            clsAGM.MarkNew();
            clsAGM.CreateTime = DateTime.Now;
        }
        clsAGM.AccessoryGroupMemberNote = tbAccessoryGroupMemberNote.Text;
        clsAGM.AccessoryGroupMemberPrice = Convert.ToDecimal(tbAccessoryGroupMemberPrice.Text);
        clsAGM.AccessoryGuid = new Guid(ddlAccessory.SelectedValue);
        clsAGM.Sequence = Convert.ToInt32(tbAccessoryGroupMemberSequence.Text);
        ip.AccessoryGroupMemberSet(clsAGM);
        Response.Redirect(Request.Url.ToString());
    }

    protected void ddlAccessoryCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadAccessoryDropDownList();
    }

    protected void ddlAccessoryGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadGrid("", 1, "");
        BindData();
    }

    protected void btnModifyAccessoryGroupName_Click(object sender, EventArgs e)
    {
        clsAG = ip.AccessoryGroupGet(new Guid(ddlAccessoryGroupName.SelectedValue));
        clsAG.MarkOld();
        clsAG.ModifyTime = DateTime.Now;
        clsAG.AccessoryGroupName = tbAccessoryGroupName.Text;
        ip.AccessoryGroupSet(clsAG);
        Response.Redirect(Request.Url.ToString());
    }
}
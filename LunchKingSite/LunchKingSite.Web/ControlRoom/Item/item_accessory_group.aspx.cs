﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.UI;

public partial class item_accessory_group : RolePage
{

    private const int DEFAULT_PAGE_SIZE = 15;
    private int pageSize;

    IItemProvider ip;
    ISellerProvider sp;
    Boolean isUpdate = false;
    Guid guidItem;
    DataTable dtItemAccessoryGroupList = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        pageSize = (this.gvAccessoryGroup.PageSize == 0) ? DEFAULT_PAGE_SIZE : this.gvAccessoryGroup.PageSize;

        if (!String.IsNullOrEmpty(Request.QueryString["iid"]) && !String.IsNullOrEmpty(Request.QueryString["mid"]))
        {
            guidItem = new Guid(Request["iid"].ToString());
        }
        else
        {
            Response.Redirect("../Seller/seller_list.aspx");
        }

        FillLabel();

        if (!Page.IsPostBack)
        {
            LoadGrid("", 1, "");
            BindData();
        }


    }

    private void FillLabel()
    {
        if(isUpdate){
            lblMode.Text = "編輯";
        }
        else{
            lblMode.Text = "檢視";
        }
        Seller s = null; //wei: the whole page needs rewrite so we explicitly fail it for now 
        Item i = ip.ItemGet(guidItem);
        lblSellerName.Text = s.SellerName;
        lblItemName.Text = i.ItemName;
    }

    private void BindData()
    {
        this.gvAccessoryGroup.DataSource = dtItemAccessoryGroupList;
        this.gvAccessoryGroup.DataBind();
    }

    private void LoadGrid(string filter, int pageNumber, string orderBy)
    {
            //dtItemAccessoryGroupList = ip.ItemAccessoryGroupListGetList(filter, pageNumber, pageSize, orderBy, guidItem);
    }

    private void BindItemAccessoryGroupListType()
    {
        foreach (string type in Enum.GetNames(typeof(ItemAccessoryListType)))
        {
            this.rblItemAccessoryGroupListType.Items.Add(new ListItem(Enum.Parse(typeof(ItemAccessoryListType), type).ToString(), type));
        }
    }

}
﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="item_accessory_group.aspx.cs" Inherits="item_accessory_group" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>

<%@ Register Assembly="System.Web.Extensions"
    Namespace="System.Web.UI" TagPrefix="aspajax" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <table>
        <tr>
            <td>
                您正在<asp:Label ID="lblMode" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>商家
            </td>
            <td>
                <asp:Label ID="lblSellerName" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                -&gt;
                <asp:Label ID="lblItemName" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label></td>
            <td>
            </td>
        </tr>
        <tr>
            <td>類別
            </td>
            <td>
                <asp:RadioButtonList ID="rblItemAccessoryGroupListType" runat="server">
                </asp:RadioButtonList>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>順序
            </td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="送出" />
                <asp:Button ID="btnCancel" runat="server" Text="取消" /></td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <br />
    <aspajax:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            搜尋品項:
            <asp:TextBox ID="tbSearch" runat="server"></asp:TextBox><asp:Button ID="bSearchBtn"
                runat="server" Text="送出" /><br />
            <br />
            <asp:GridView ID="gvAccessoryGroup" runat="server" AllowPaging="True" AllowSorting="True"
                AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None"
                BorderWidth="1px" CellPadding="4" EmptyDataText="無符合的資料" ForeColor="Black" GridLines="Vertical"
                PageSize="15">
                <FooterStyle BackColor="#CCCC99" />
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFields="ItemGUID,ItemAccessoryGroupListGUID,MenuGUID"
                        DataNavigateUrlFormatString="item_accessory_group.aspx?iid={0}&amp;iaglid={1}&amp;mid={2}"
                        DataTextField="AccessoryGroupName" HeaderText="配料組名稱">
                        <ItemStyle Width="200px" />
                    </asp:HyperLinkField>
                    <asp:BoundField DataField="ItemAccessoryGroupListSequence" HeaderText="顯示順序" />
                    <asp:BoundField DataField="ItemAccessoryGroupListType" HeaderText="配料類別" />
                    <asp:CommandField ButtonType="Button" DeleteText="刪除" ShowDeleteButton="True" />
                    <asp:HyperLinkField DataNavigateUrlFields="ItemGUID,MenuGUID" DataNavigateUrlFormatString="./accessory_list.aspx?iid={0}&amp;mid={1}"
                        HeaderText="配料細項" Text="配料細項" />
                </Columns>
                <RowStyle BackColor="#F7F7DE" />
                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
            &nbsp;
        </ContentTemplate>
    </aspajax:UpdatePanel>
</asp:Content>

﻿using System;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.UI;


public partial class item_list : RolePage
{
    IItemProvider ip;
    Guid guidItemMenu;
    Guid guidItem;
    Item clsItem;
    bool isUpdate;
    protected void Page_Load(object sender, EventArgs e)
    {
        ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        if (!String.IsNullOrEmpty(Request.QueryString["mid"]))
        {
            guidItemMenu = new Guid(Request["mid"].ToString());
        }
        else
        {
            Response.Redirect("../Seller/seller_list.aspx");
        }

        if (!String.IsNullOrEmpty(Request.QueryString["iid"]))
        {
                guidItem = new Guid(Request["iid"].ToString());
                isUpdate = true;
        }
        else
        {
            guidItem = Helper.GetNewGuid(clsItem);
            isUpdate = false;
        }

        BindItemStatus();

        if (!Page.IsPostBack)
        {
            LoadGrid("", 1, "");
            if (isUpdate)
            {
                LoadItem();
            }
        }
        
    }

    private void BindItemStatus()
    {
        foreach (string status in Enum.GetNames(typeof(ItemStatusFlag)))
        {
            cblItemStatus.Items.Add(new ListItem(Enum.Parse(typeof(ItemStatusFlag), status).ToString(), status));
        }
    }

    private void LoadItem()
    {
        clsItem = ip.ItemGet(guidItem);
        tbItemName.Text = clsItem.ItemName;
        tbItemPrice.Text = clsItem.ItemPrice.ToString();
        tbItemSequence.Text = clsItem.Sequence.ToString();
        tbItemDescription.Text = clsItem.ItemDescription;
        if (clsItem.ItemDefaultDailyAmount.HasValue)
        {
            tbItemDefaultDailyAmount.Text = clsItem.ItemDefaultDailyAmount.Value.ToString();
        }
    }

    private void LoadGrid(string filter, int pageNumber, string orderBy)
    {
        //gvItem.DataSource = ip.ItemGetList(pageNumber, pageSize, orderBy, guidItemMenu);
        gvItem.DataBind();
    }
    protected void UpdateHandler(int pageNumber)
    {
        LoadGrid("", pageNumber, (string)ViewState["SortExp"] + ViewState["SortDirection"]);
    }
    protected void onDelete(object sender, GridViewDeletedEventArgs e)
    {        
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (isUpdate)
        {
            clsItem = ip.ItemGet(guidItem);
            clsItem.ModifyTime = DateTime.Now;
            clsItem.MarkOld();
        }
        else
        {
            clsItem = new Item();
            clsItem.CreateTime = DateTime.Now;
            clsItem.MarkNew();
        }

        clsItem.Guid = guidItem;
        clsItem.MenuGuid = Guid.Empty;
        clsItem.ItemName = tbItemName.Text;
        clsItem.ItemDescription = tbItemDescription.Text;
        clsItem.ItemDefaultDailyAmount = Convert.ToInt32(tbItemDefaultDailyAmount.Text);
//        clsItem.Status = (int)Helper.SetFlag(Convert.ToBoolean(rblItemOnline.SelectedValue), (long)clsItem.Status, (long)ItemStatusFlag.Online);
        ip.ItemSet(clsItem);
        Response.Redirect("./item_list.aspx?mid=" + this.guidItemMenu.ToString());
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }
    protected void bSearchBtn_Click(object sender, EventArgs e)
    {

    }
    protected int GetItemCount()
    {
        if (ip == null)
            ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        return ip.ItemGetCount(guidItemMenu);
    }

    protected void gvItem_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["SortDirection"] = ((string)ViewState["SortDirection"] == " ASC") ? " DESC" : " ASC";
        ViewState["SortExp"] = e.SortExpression;
        LoadGrid("", 1, e.SortExpression + ViewState["SortDirection"]);
        gridPager.ResolvePagerView(1);
    }

}

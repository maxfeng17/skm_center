﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="accessory_category.aspx.cs" Inherits="accessory_category" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>

<%@ Register Assembly="System.Web.Extensions"
    Namespace="System.Web.UI" TagPrefix="aspajax" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <aspajax:UpdatePanel ID="UPanelAccessoryItem" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        配料管理群組&nbsp;
                    </td>
                    <td align="left" colspan="2">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DropDownList ID="ddlAccessoryCategory" runat="server" AutoPostBack="True" DataTextField="AccessoryCategoryName" DataValueField="Guid" OnSelectedIndexChanged="ddlAccessoryCategory_SelectedIndexChanged">
                        </asp:DropDownList></td>
                    <td align="left" colspan="2">
                        <asp:LinkButton ID="lbAddCategory" runat="server" OnClick="lbAddCategory_Click">新增類別</asp:LinkButton>&nbsp;
                        <asp:LinkButton ID="lbEditCategory" runat="server" OnClick="lbEditCategory_Click">修改此類別</asp:LinkButton>
                        <asp:LinkButton ID="lbDeleteCategory" runat="server" OnClick="lbDeleteCategory_Click">刪除此類別</asp:LinkButton>
                        <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" BehaviorID="ConfirmButtonExtender1"
                            ConfirmText="確定刪除?" TargetControlID="lbDeleteCategory">
                        </cc1:ConfirmButtonExtender>
                        <br />
                        <asp:Panel ID="CategoryPanel" runat="server" Visible="False">
                            類別名稱:<br />
                            <asp:TextBox ID="tbCategoryName" runat="server" Width="200px"></asp:TextBox>
                            <asp:Button ID="btnAddCategory" runat="server" Text="新增" CommandName="Add" OnCommand="btnAddCategory_Command" />
                            <asp:Button ID="btnCancelCategory" runat="server" Text="取消" OnClick="btnCancelCategory_Click" />
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="tbCategoryName"
                                WatermarkCssClass="ldnmbg" WatermarkText="類別名稱不得空白">
                            </cc1:TextBoxWatermarkExtender>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td rowspan="2" valign="top">
                        <asp:GridView ID="gvAccessoryIn" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Guid" EmptyDataText="無配料在此類別"
                            ForeColor="#333333" GridLines="None" OnRowDeleting="gvAccessoryIn_RowDeleting" OnRowEditing="gvAccessoryIn_RowEditing">
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="配料名稱">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlAccessory" runat="server" NavigateUrl='<%# Eval("Guid", "accessory_list.aspx?aid={0}") %>'
                                            Text='<%# Eval("AccessoryName") %>'></asp:HyperLink>
                                    </ItemTemplate>
                                    <HeaderStyle Wrap="False" />
                                    <ItemStyle Wrap="False" />
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lkbDeleteAccessory" runat="server" CausesValidation="False" CommandName="Delete"
                                            Text="刪除"></asp:LinkButton>
                                        <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" ConfirmText="確定刪除？"
                                            TargetControlID="lkbDeleteAccessory">
                                        </cc1:ConfirmButtonExtender>
                                    </ItemTemplate>
                                    <HeaderStyle Wrap="False" />
                                    <ItemStyle Wrap="False" />
                                </asp:TemplateField>
                                <asp:CommandField ShowEditButton="True">
                                    <HeaderStyle Wrap="False" />
                                    <ItemStyle Wrap="False" />
                                </asp:CommandField>
                            </Columns>
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                            <RowStyle BackColor="#EFF3FB" />
                            <EditRowStyle BackColor="#2461BF" />
                        </asp:GridView>
                    </td>
                    <td colspan="2" valign="top">
                        <asp:LinkButton ID="lbAddAccessory" runat="server" OnClick="lbAddAccessory_Click">新增配料於此類別</asp:LinkButton><br />
                        <asp:Panel ID="AccessoryPanel" runat="server" Visible="False">
                            配料名稱:<br />
                            <asp:TextBox ID="tbAccessoryName" runat="server" Width="200px"></asp:TextBox>
                            <asp:Button ID="btAddAccessory" runat="server" Text="新增" CommandName="Add" OnCommand="btAddAccessory_Command" />
                            <asp:Button ID="btCancelAccessory" runat="server" Text="取消" OnClick="btCancelAccessory_Click" />
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="tbAccessoryName"
                                WatermarkCssClass="ldnmbg" WatermarkText="配料名稱不得空白">
                            </cc1:TextBoxWatermarkExtender>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" valign="top">
                        &nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </aspajax:UpdatePanel>
    <br />
</asp:Content>
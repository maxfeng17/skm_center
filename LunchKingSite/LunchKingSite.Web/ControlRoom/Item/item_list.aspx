﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="item_list.aspx.cs" Inherits="item_list" %>

<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>

<%@ Register Assembly="System.Web.Extensions"
    Namespace="System.Web.UI" TagPrefix="aspajax" %>


<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    &nbsp;<table>
        <tr>
            <td>品項名稱
            </td>
            <td>
                            <asp:TextBox ID="tbItemName" runat="server"></asp:TextBox>

            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>單價
            </td>
            <td>
                <asp:TextBox ID="tbItemPrice" runat="server"></asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="height: 42px">
                描述</td>
            <td style="height: 42px">
                <asp:TextBox ID="tbItemDescription" runat="server" Height="50px" TextMode="MultiLine"
                    Width="250px"></asp:TextBox></td>
            <td style="height: 42px">
            </td>
        </tr>
        <tr>
            <td>
                預設日供應量</td>
            <td>
                <asp:TextBox ID="tbItemDefaultDailyAmount" runat="server"></asp:TextBox></td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                日供應量</td>
            <td>
                <asp:TextBox ID="tbItemDailyAmount" runat="server"></asp:TextBox></td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                顯示順序</td>
            <td>
                <asp:TextBox ID="tbItemSequence" runat="server"></asp:TextBox></td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                圖片上傳</td>
            <td>
                <asp:FileUpload ID="FileUpload1" runat="server" /></td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                狀態</td>
            <td>
                <asp:CheckBoxList ID="cblItemStatus" runat="server" RepeatDirection="Horizontal">
                </asp:CheckBoxList></td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;<asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="送出" />
                <asp:Button ID="btnCancel" runat="server" Text="取消" /></td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <aspajax:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        搜尋品項:
                        <asp:TextBox ID="tbSearch" runat="server"></asp:TextBox><asp:Button ID="bSearchBtn"
                            runat="server" OnClick="bSearchBtn_Click" Text="送出" />
                        <br />
                        <br />
                        <asp:GridView ID="gvItem" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None"
                            BorderWidth="1px" CellPadding="4" EmptyDataText="無符合的資料" ForeColor="Black" GridLines="Vertical"
                            OnSorting="gvItem_Sorting" PageSize="15" DataKeyNames="MenuGUID" OnRowDeleted="onDelete">
                            <FooterStyle BackColor="#CCCC99" />
                            <Columns>
                                <asp:HyperLinkField DataNavigateUrlFields="ItemGUID,MenuGUID" DataNavigateUrlFormatString="item_list.aspx?iid={0}&amp;mid={1}"
                                    DataTextField="ItemName" HeaderText="品項名稱">
                                    <ItemStyle Width="200px" />
                                </asp:HyperLinkField>
                                <asp:BoundField DataField="ItemPrice" HeaderText="價格" ReadOnly="True" />
                                <asp:BoundField DataField="ItemDescription" HeaderText="描述" ReadOnly="True">
                                    <ItemStyle Width="300px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ItemDailyAmount" HeaderText="供應量" ReadOnly="True" />
                                <asp:BoundField DataField="ItemSequence" HeaderText="顯示順序" />
                                <asp:BoundField DataField="CreateTime" HeaderText="建立時間" ReadOnly="True" />
                                <asp:BoundField DataField="ModifyTime" HeaderText="最後修改時間" ReadOnly="True" />
                                <asp:CommandField ButtonType="Button" DeleteText="刪除" ShowDeleteButton="True" />
                                <asp:HyperLinkField DataNavigateUrlFields="ItemGUID,MenuGUID" DataNavigateUrlFormatString="./item_accessory_group.aspx?iid={0}&amp;mid={1}"
                                    HeaderText="檢視配料" Text="檢視配料" />
                            </Columns>
                            <RowStyle BackColor="#F7F7DE" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                        <uc1:Pager ID="gridPager" runat="server" OnGetCount="GetItemCount" OnUpdate="UpdateHandler" PageSize="15" />
                    </ContentTemplate>
                </aspajax:UpdatePanel>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>


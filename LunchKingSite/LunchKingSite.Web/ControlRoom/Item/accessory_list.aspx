﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="accessory_list.aspx.cs" Inherits="accessory_list" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>

<%@ Register Assembly="System.Web.Extensions"
    Namespace="System.Web.UI" TagPrefix="aspajax" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <table border="2" cellpadding="1" cellspacing="1" style="border-style:solid;border-color:Gray;">
        <tr>
            <td>
                您正在<asp:Label ID="lblMode" runat="server"></asp:Label>商家</td>
            <td>
                <asp:Label ID="lblSellerName" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                -
                <asp:Label ID="lblMenuName" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>&nbsp;
                -
                <asp:Label ID="lblItemName" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label></td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                配料組別</td>
            <td>
                &nbsp;<asp:DropDownList ID="ddlAccessoryGroupName" runat="server">
                </asp:DropDownList>
                <asp:LinkButton ID="lbAddAccessoryGroupName" runat="server">新增類別</asp:LinkButton>
                <cc1:ModalPopupExtender  TargetControlID="lbAddAccessoryGroupName" BackgroundCssClass="modalBackground" PopupControlID="pDrag" DropShadow="true" CancelControlID="btnCancel" PopupDragHandleControlID="pDrag" ID="ModalPopupExtender1" runat="server">
                </cc1:ModalPopupExtender>
                修改類別名稱：
                <asp:TextBox ID="tbAccessoryGroupName" runat="server"></asp:TextBox>
                <asp:Button ID="btnModifyAccessoryGroupName" runat="server" OnClick="btnModifyAccessoryGroupName_Click"
                    Text="確定" /></td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                配料</td>
            <td>
                &nbsp;<asp:DropDownList ID="ddlAccessoryCategory" runat="server" OnSelectedIndexChanged="ddlAccessoryCategory_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList>
                <asp:DropDownList ID="ddlAccessory" runat="server">
                </asp:DropDownList>
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/ControlRoom/Item/accessory_category.aspx">去配料管理</asp:HyperLink></td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                價格</td>
            <td>
                <asp:TextBox ID="tbAccessoryGroupMemberPrice" runat="server"></asp:TextBox></td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="height: 42px">
                附註</td>
            <td style="height: 42px">
                <asp:TextBox ID="tbAccessoryGroupMemberNote" runat="server" Height="50px" TextMode="MultiLine"
                    Width="250px"></asp:TextBox></td>
            <td style="height: 42px">
            </td>
        </tr>
        <tr>
            <td>
                顯示順序</td>
            <td>
                <asp:TextBox ID="tbAccessoryGroupMemberSequence" runat="server"></asp:TextBox></td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;<asp:Button ID="btnSubmit" runat="server"  Text="送出" OnClick="btnSubmit_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="取消" /></td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <aspajax:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        搜尋品項:
                        <asp:TextBox ID="tbSearch" runat="server"></asp:TextBox><asp:Button ID="bSearchBtn"
                            runat="server"  Text="送出" />
                        選擇組別：
                        <asp:DropDownList ID="ddlAccessoryGroup" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAccessoryGroup_SelectedIndexChanged">
                        </asp:DropDownList><br />
                        <br />
                        <asp:GridView ID="gvAccessory" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None"
                            BorderWidth="1px" CellPadding="4" EmptyDataText="無符合的資料" ForeColor="Black" GridLines="Vertical"
                             PageSize="15" Font-Size="Small">
                            <FooterStyle BackColor="#CCCC99" />
                            <Columns>
                                <asp:HyperLinkField DataNavigateUrlFields="ItemGUID,AccessoryGroupGUID,AccessoryGroupMemberGUID,MenuGUID" DataNavigateUrlFormatString="accessory_list.aspx?iid={0}&amp;agid={1}&amp;agmid={2}&amp;mid={3}"
                                    DataTextField="AccessoryName" HeaderText="配料名稱">
                                    <ItemStyle Width="200px" />
                                </asp:HyperLinkField>
                                <asp:BoundField DataField="AccessoryGroupName" HeaderText="配料組名稱" />
                                <asp:BoundField DataField="AccessoryGroupMemberPrice" HeaderText="價格" ReadOnly="True" />
                                <asp:BoundField DataField="AccessoryGroupMemberNote" HeaderText="描述" ReadOnly="True">
                                    <ItemStyle Width="300px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="AccessoryGroupMemberSequence" HeaderText="顯示順序" />
                                <asp:BoundField DataField="ItemAccessoryGroupListType" HeaderText="配料類別" />
                                <asp:CommandField ButtonType="Button" DeleteText="刪除" ShowDeleteButton="True" />
                            </Columns>
                            <RowStyle BackColor="#F7F7DE" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                        &nbsp;
                    </ContentTemplate>
                </aspajax:UpdatePanel>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            
                <asp:Panel ID="pDrag" runat="server" Height="50px" Width="100%" CssClass="modalPopup">
                    <asp:Label ID="Label1" runat="server" Text="類別名稱： "></asp:Label>
                    <asp:TextBox ID="tbGroupName" runat="server"></asp:TextBox>
                    <asp:Button ID="btnOK" runat="server" Text="新增" OnClick="btnOK_Click" />
                    <asp:Button ID="btnDrop" runat="server" Text="放棄" /></asp:Panel>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>

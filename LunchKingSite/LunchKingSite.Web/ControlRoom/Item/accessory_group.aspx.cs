﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.ControlRoom.Item
{
    public partial class accessory_group : RolePage, IAccessoryGroupView
    {
        public event EventHandler<DataEventArgs<int>> GetBuildingCount;
        public event EventHandler<DataEventArgs<int>> PageChanged;
        public event EventHandler SearchClicked;
        public event EventHandler<DataEventArgs<AccessoryGroup>> SetAccessoryGroup;

        #region props
        private AccessoryGroupresenter _presenter;
        public AccessoryGroupresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public Guid SellerGuid 
        {
            get 
            {
                if (Request.QueryString["sid"] != null)
                    return new Guid(Request.QueryString["sid"]);
                else
                    return Guid.Empty;
            }
        }
        public string Filter
        {
            get
            {
                return tbSearch.Text;
            }
            set
            {
                tbSearch.Text = value;
            }
        }
        public int PageSize
        {
            get { return int.Parse(ddlPageSize.SelectedValue); }
            set { value = int.Parse(ddlPageSize.SelectedValue); }
        }
        public int CurrentPage
        {
            get { return gridPager.CurrentPage; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Presenter.OnViewInitialized();
            }
            Presenter.OnViewLoaded();
        }

        protected void UpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.PageChanged != null)
                this.PageChanged(this, e);
        }

        protected int RetrieveBuildingCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (GetBuildingCount != null)
                GetBuildingCount(this, e);
            lbTotalCount.Text = e.Data.ToString();
            return e.Data;
        }

        public void SetAccessoryGroupList(AccessoryGroupCollection AccessoryGroupList)
        {
            gvAccessoryGropu.DataSource = AccessoryGroupList;
            gvAccessoryGropu.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            gvAccessoryGropu.PageSize = int.Parse(ddlPageSize.SelectedValue);
            gridPager.PageSize = int.Parse(ddlPageSize.SelectedValue);

            if (this.SearchClicked != null)
                this.SearchClicked(this, e);
            gridPager.ResolvePagerView(1, true);
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvAccessoryGropu.PageSize = int.Parse(ddlPageSize.SelectedValue);
            gridPager.PageSize = int.Parse(ddlPageSize.SelectedValue);

            if (this.SearchClicked != null)
                this.SearchClicked(this, e);

            gridPager.ResolvePagerView(1, true);
        }

        protected void gvAccessoryGropu_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvAccessoryGropu.EditIndex = e.NewEditIndex;
            if (this.SearchClicked != null)
                this.SearchClicked(this, e);
        }

        protected void gvAccessoryGropu_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvAccessoryGropu.EditIndex = -1;
            if (this.SearchClicked != null)
                this.SearchClicked(this, e);
        }

        protected void gvAccessoryGropu_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            if (SetAccessoryGroup != null)
            {
                AccessoryGroup info = new AccessoryGroup();
                info.Guid = (Guid)gvAccessoryGropu.DataKeys[e.RowIndex].Value;
                info.AccessoryGroupName = ((TextBox)gvAccessoryGropu.Rows[e.RowIndex].FindControl("tbAccessoryGroupName")).Text;
                info.ModifyId = User.Identity.Name;
                info.ModifyTime = DateTime.Now;

                SetAccessoryGroup(this, new DataEventArgs<AccessoryGroup>(info));

                gvAccessoryGropu.EditIndex = -1;
                if (this.SearchClicked != null)
                    this.SearchClicked(this, e);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (SetAccessoryGroup != null)
            {
                AccessoryGroup info = new AccessoryGroup();
                info.Guid = Guid.Empty;
                info.AccessoryGroupName = tbAddAccessoryGroupName.Text;
                info.CreateId = User.Identity.Name;
                info.CreateTime = DateTime.Now;

                SetAccessoryGroup(this, new DataEventArgs<AccessoryGroup>(info));

                gvAccessoryGropu.EditIndex = -1;
                if (this.SearchClicked != null)
                    this.SearchClicked(this, e);
            }
        }
    }
}

﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="accessory_group.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Item.accessory_group" %>

<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <asp:UpdatePanel ID="UPanelAccessoryGroup" runat="server">
        <ContentTemplate>
    配料群組<asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
        <asp:ListItem Value="15">15筆</asp:ListItem>
        <asp:ListItem Value="50">50筆</asp:ListItem>
        <asp:ListItem Value="100">100筆</asp:ListItem>
        <asp:ListItem Value="200">200筆</asp:ListItem>
        <asp:ListItem Value="400">400筆</asp:ListItem>
        <asp:ListItem Value="800">800筆</asp:ListItem>
    </asp:DropDownList><br />
            群組名稱 :
            <asp:TextBox ID="tbSearch" runat="server"></asp:TextBox>&nbsp;
            <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="搜尋" />
            <asp:LinkButton ID="lbkAddAccessoryGroup" runat="server">新增配料群組</asp:LinkButton>
            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="ldnmbg"
                BehaviorID="ModalPopupExtender1" CancelControlID="btnCancel" PopupControlID="PAddAccessoryGroup"
                PopupDragHandleControlID="PAddAccessoryGroup" TargetControlID="lbkAddAccessoryGroup">
            </cc1:ModalPopupExtender>
            <br />
    <asp:GridView ID="gvAccessoryGropu" runat="server" AutoGenerateColumns="False" BackColor="LightGoldenrodYellow"
        BorderColor="Tan" BorderWidth="1px" CellPadding="2" DataKeyNames="Guid" ForeColor="Black"
        GridLines="None" AllowPaging="True" AllowSorting="True" OnRowCancelingEdit="gvAccessoryGropu_RowCancelingEdit" OnRowEditing="gvAccessoryGropu_RowEditing" OnRowUpdating="gvAccessoryGropu_RowUpdating" PageSize="15">
        <FooterStyle BackColor="Tan" />
        <Columns>
            <asp:TemplateField HeaderText="配料群組名稱">
                <EditItemTemplate>
                    <asp:TextBox ID="tbAccessoryGroupName" runat="server" Text='<%# Bind("AccessoryGroupName") %>'
                        Width="200px"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lbAccessoryGroupName" runat="server" Text='<%# Bind("AccessoryGroupName") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Wrap="False" />
                <ItemStyle Wrap="True" />
            </asp:TemplateField>
            <asp:CommandField ShowEditButton="True" />
        </Columns>
        <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
        <HeaderStyle BackColor="Tan" Font-Bold="True" />
        <AlternatingRowStyle BackColor="PaleGoldenrod" />
    </asp:GridView>
            <table>
                <tr>
                    <td>
    <uc1:Pager ID="gridPager" runat="server" OnGetCount="RetrieveBuildingCount" OnUpdate="UpdateHandler" PageSize="15" />
                    </td>
                    <td>
                        總筆數 :
                        <asp:Label ID="lbTotalCount" runat="server"></asp:Label></td>
                </tr>
            </table>
            &nbsp;
            <asp:Panel ID="PAddAccessoryGroup" runat="server" CssClass="modalPopup" Height="70px"
                Width="220px">
                配料群組名稱 :
                <asp:TextBox ID="tbAddAccessoryGroupName" runat="server"></asp:TextBox><br />
                <asp:Button ID="btnAdd" runat="server" Text="新增" OnClick="btnAdd_Click" Enabled="False" />
                <asp:Button ID="btnCancel" runat="server" Text="取消" />
                請至賣家頁面新增</asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

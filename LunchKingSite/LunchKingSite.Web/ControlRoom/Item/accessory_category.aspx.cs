﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

public partial class accessory_category : RolePage, IAccessoryCategoryView
{
    public event EventHandler<DataEventArgs<Guid>> SelectAccessory;
    public event EventHandler<DataEventArgs<AccessoryCategory>> SetAccessoryCategory;
    public event EventHandler<DataEventArgs<Guid>> DeleteAccessoryCategory;
    public event EventHandler<DataEventArgs<Accessory>> SetAccessory;
    public event EventHandler<DataEventArgs<Guid>> DeleteAccessory;

    #region props
    private AccessoryCategoryPresenter _presenter;
    public AccessoryCategoryPresenter Presenter
    {
        set
        {
            this._presenter = value;
            if (value != null)
            {
                this._presenter.View = this;
            }
        }
        get
        {
            return this._presenter;
        }
    }
    public Guid AccessoryCategoryGuid
    {
        get
        {
            return new Guid(ddlAccessoryCategory.SelectedValue);
        }
    }
    public bool UpdateMessage
    {
        get { return Presenter.UpdateMessage; }
        set { Presenter.UpdateMessage = value; }
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Presenter.OnViewInitialized();
        }

        Presenter.OnViewLoaded();
    }

    public void SetAccessoryCategoryList(AccessoryCategoryCollection AccessoryCategoryList)
    {
        ddlAccessoryCategory.DataSource = AccessoryCategoryList;
        ddlAccessoryCategory.DataBind();
    }

    protected void ddlAccessoryCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (SelectAccessory != null)
        {
            gvAccessoryIn.SelectedIndex = -1;
            DataEventArgs<Guid> g = new DataEventArgs<Guid>(new Guid(ddlAccessoryCategory.SelectedValue.ToString()));
            SelectAccessory(this, g);

            CategoryPanel.Visible = false;
            AccessoryPanel.Visible = false;
        }
    }

    public void SetAccessoryList(AccessoryCollection AccessoryList)
    {
        gvAccessoryIn.DataSource = AccessoryList;
        gvAccessoryIn.DataBind();
    }

    protected void lbAddCategory_Click(object sender, EventArgs e)
    {
        tbCategoryName.Text = string.Empty;
        btnAddCategory.Text = "新增";
        btnAddCategory.CommandName = "Add";
        CategoryPanel.Visible = true;
    }

    protected void lbEditCategory_Click(object sender, EventArgs e)
    {
        tbCategoryName.Text = ddlAccessoryCategory.SelectedItem.Text;
        btnAddCategory.Text = "修改";
        btnAddCategory.CommandName = "Edit";
        CategoryPanel.Visible = true;
    }

    protected void btnCancelCategory_Click(object sender, EventArgs e)
    {
        CategoryPanel.Visible = false;
    }

    protected void btnAddCategory_Command(object sender, CommandEventArgs e)
    {
        if (!string.IsNullOrEmpty(tbCategoryName.Text))
        {
            AccessoryCategory info = new AccessoryCategory();
            info.AccessoryCategoryName = tbCategoryName.Text;

            if (e.CommandName == "Add")
            {
                info.Guid = Helper.GetNewGuid(info);
                info.CreateId = User.Identity.Name;
                info.CreateTime = DateTime.Now;
                info.MarkNew();
            }
            else
            {
                info.Guid = new Guid(ddlAccessoryCategory.SelectedValue);
                info.ModifyId = User.Identity.Name;
                info.ModifyTime = DateTime.Now;
                info.MarkOld();
            }
            if (SetAccessoryCategory != null)
            {
                DataEventArgs<AccessoryCategory> g = new DataEventArgs<AccessoryCategory>(info);
                SetAccessoryCategory(this, g);
                ddlAccessoryCategory.Items.FindByValue(info.Guid.ToString()).Selected = true;
                DataEventArgs<Guid> d = new DataEventArgs<Guid>(new Guid(ddlAccessoryCategory.SelectedValue.ToString()));
                SelectAccessory(this, d);
                CategoryPanel.Visible = false;
            }
        }
    }

    protected void lbAddAccessory_Click(object sender, EventArgs e)
    {
        tbAccessoryName.Text = string.Empty;
        btAddAccessory.CommandName = "Add";
        btAddAccessory.Text = "新增";
        AccessoryPanel.Visible = true;
    }

    protected void btCancelAccessory_Click(object sender, EventArgs e)
    {
        AccessoryPanel.Visible = false;
        gvAccessoryIn.SelectedIndex = -1;
    }

    protected void gvAccessoryIn_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvAccessoryIn.SelectedIndex = e.NewEditIndex;
        tbAccessoryName.Text = ((HyperLink)gvAccessoryIn.Rows[e.NewEditIndex].FindControl("hlAccessory")).Text;
        btAddAccessory.CommandName = "Edit";
        btAddAccessory.Text = "修改";
        AccessoryPanel.Visible = true;
    }

    protected void btAddAccessory_Command(object sender, CommandEventArgs e)
    {
        if (!string.IsNullOrEmpty(tbAccessoryName.Text))
        {
            Accessory info = new Accessory();
            info.AccessoryName = tbAccessoryName.Text;
            info.AccessoryCategoryGuid = new Guid(ddlAccessoryCategory.SelectedValue);

            if (e.CommandName == "Add")
            {
                info.Guid = Helper.GetNewGuid(info);
                info.CreateTime = DateTime.Now;
                info.MarkNew();
            }
            else
            {
                info.Guid = new Guid(gvAccessoryIn.DataKeys[gvAccessoryIn.SelectedIndex].Value.ToString());
                info.ModifyTime = DateTime.Now;
                info.MarkOld();
            }
            if (SetAccessoryCategory != null)
            {
                gvAccessoryIn.SelectedIndex = -1;
                DataEventArgs<Accessory> g = new DataEventArgs<Accessory>(info);
                SetAccessory(this, g);
                AccessoryPanel.Visible = false;
            }
        }
    }

    protected void gvAccessoryIn_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (DeleteAccessory != null)
        {
            DataEventArgs<Guid> g = new DataEventArgs<Guid>(new Guid(gvAccessoryIn.DataKeys[e.RowIndex].Value.ToString()));
            DeleteAccessory(this, g);
        }
    }

    protected void lbDeleteCategory_Click(object sender, EventArgs e)
    {
        if (DeleteAccessoryCategory != null)
        {
            DataEventArgs<Guid> g = new DataEventArgs<Guid>(new Guid(ddlAccessoryCategory.SelectedValue));
            DeleteAccessoryCategory(this, g);
        }
    }

    public void SetAccessoryGroupMemberList(bool CanDelete)
    {
        if (CanDelete)
            lbDeleteCategory.Visible = true;
        else
            lbDeleteCategory.Visible = false;
    }
} 

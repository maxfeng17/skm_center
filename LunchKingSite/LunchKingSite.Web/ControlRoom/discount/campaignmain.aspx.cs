﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Data;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using log4net;

namespace LunchKingSite.Web.ControlRoom.discount
{
    public partial class campaignmain : RolePage
    {
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
        private static ILog log = LogManager.GetLogger(typeof(campaignmain));

        protected void UpdateHandler(int pageNumber)
        {
            SetGrid(pageNumber);
        }

        protected int RetrieveTransCount()
        {
            return ProviderFactory.Instance().GetProvider<IOrderProvider>().ViewDiscountMainGetCount(GetFilter()); ;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetGrid(1);
            }
        }

        protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string[] dates = ((Label)e.Row.FindControl("between")).Text.Split('~');
                DateTime sDate = DateTime.Parse(dates[0]);
                DateTime eDate = DateTime.Parse(dates[1]);

                #region 判斷是否已經啟用

                ((LinkButton)e.Row.Cells[1].FindControl("btnApply")).CommandArgument = e.Row.Cells[0].Text;
                ((LinkButton)e.Row.Cells[1].FindControl("btncancel")).CommandArgument = e.Row.Cells[0].Text;
                ((LinkButton)e.Row.Cells[1].FindControl("btnDelete")).CommandArgument = e.Row.Cells[0].Text;

                if (((Label)e.Row.Cells[1].FindControl("lblapplytime")).Text == "")
                {
                    ((LinkButton)e.Row.Cells[1].FindControl("btncancel")).Visible = true;
                    ((LinkButton)e.Row.Cells[1].FindControl("btnApply")).Visible = true;
                }
                else
                {
                    ((LinkButton)e.Row.Cells[1].FindControl("btnApply")).Visible = false;

                    #region 狀態欄位設定

                    if (DateTime.Compare(DateTime.Now, sDate) > 0 && DateTime.Compare(DateTime.Now, eDate) < 0)
                    {
                        ((Label)e.Row.Cells[3].FindControl("lblstatus")).Text = "有效";
                    }
                    else if (DateTime.Compare(DateTime.Now, sDate) < 0)
                    {
                        ((Label)e.Row.Cells[3].FindControl("lblstatus")).Text = "未生效";
                    }
                    else if (DateTime.Compare(DateTime.Now, eDate) > 0)
                    {
                        ((Label)e.Row.Cells[3].FindControl("lblstatus")).Text = "失效";
                    }

                    #endregion
                }

                #region 判斷是否已經作廢
                if (!string.IsNullOrWhiteSpace(e.Row.Cells[14].Text.Replace("&nbsp;", "")))
                {
                    DiscountCodeCollection codes = op.DiscountCodeGetByCampaignIdAndUseTimeIsnotnull(int.Parse(e.Row.Cells[0].Text));

                    ((Label)e.Row.Cells[3].FindControl("lblstatus")).Text = "已作廢";
                    ((LinkButton)e.Row.Cells[1].FindControl("btncancel")).Visible = false;
                    ((LinkButton)e.Row.Cells[1].FindControl("btnApply")).Visible = false;
                    if (codes.Count == 0)
                    {
                        ((LinkButton)e.Row.Cells[1].FindControl("btnDelete")).Visible = true;
                    }
                }
                #endregion

                #endregion

                Label lbbetween = (Label)e.Row.FindControl("lbbetween");

                DateTime betweenSDate = DateTime.Parse(((Label)e.Row.FindControl("lbstarttime")).Text);
                DateTime betweenEDate = DateTime.Parse(((Label)e.Row.FindControl("lbendtime")).Text);

                lbbetween.Text = betweenSDate.ToString("yyyy/MM/dd HH:mm") + "~" + betweenEDate.ToString("yyyy/MM/dd HH:mm");

                #region 時間區隔

                //e.Row.Cells[11].Text = (eDate - sDate).Days.ToString();
                Label lbValidDays = (Label)e.Row.FindControl("lbValidDays");
                lbValidDays.Text = (eDate - sDate).Days.ToString();

                #endregion

            }
        }

        protected void gvMain1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string[] dates = e.Row.Cells[10].Text.Split('~');
                DateTime sDate = DateTime.Parse(dates[0]);
                DateTime eDate = DateTime.Parse(dates[1]);

                #region 判斷是否已經啟用
                ((LinkButton)e.Row.Cells[1].FindControl("btnApply")).CommandArgument = e.Row.Cells[0].Text;
                ((LinkButton)e.Row.Cells[1].FindControl("btncancel")).CommandArgument = e.Row.Cells[0].Text;
                if (((Label)e.Row.Cells[1].FindControl("lblapplytime")).Text == "")
                {
                    ((LinkButton)e.Row.Cells[1].FindControl("btncancel")).Visible = true;
                    if (IsApplyMember())
                    {
                        ((LinkButton)e.Row.Cells[1].FindControl("btnApply")).Visible = true;
                    }
                }
                else
                {
                    ((LinkButton)e.Row.Cells[1].FindControl("btnApply")).Visible = false;

                    #region 狀態欄位設定

                    if (DateTime.Compare(DateTime.Now, sDate) > 0 && DateTime.Compare(DateTime.Now, eDate) < 0)
                    {
                        ((Label)e.Row.Cells[3].FindControl("lblstatus")).Text = "有效";
                    }
                    else if (DateTime.Compare(DateTime.Now, sDate) < 0)
                    {
                        ((Label)e.Row.Cells[3].FindControl("lblstatus")).Text = "未生效";
                    }
                    else if (DateTime.Compare(DateTime.Now, eDate) > 0)
                    {
                        ((Label)e.Row.Cells[3].FindControl("lblstatus")).Text = "失效";
                    }

                    #endregion
                }

                #region 判斷是否已經作廢

                if (!string.IsNullOrWhiteSpace(e.Row.Cells[13].Text.Replace("&nbsp;", "")))
                {
                    DiscountCodeCollection codes = op.DiscountCodeGetByCampaignIdAndUseTimeIsnotnull(int.Parse(e.Row.Cells[0].Text));

                    ((Label)e.Row.Cells[3].FindControl("lblstatus")).Text = "已作廢";
                    ((LinkButton)e.Row.Cells[1].FindControl("btncancel")).Visible = false;
                    if (codes.Count == 0)
                    {
                        ((LinkButton)e.Row.Cells[1].FindControl("btnDelete")).Visible = true;
                    }
                }

                #endregion

                #endregion

                #region 時間區隔
                e.Row.Cells[11].Text = (eDate - sDate).Days.ToString();
                #endregion

            }
        }

        protected void gvMain_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "apply")
            {
                if (IsApplyMember())
                {
                    #region 啟用

                    DiscountCampaign dca = op.DiscountCampaignGet(int.Parse(e.CommandArgument.ToString()));
                    dca.ApplyTime = DateTime.Now;
                    dca.ApplyId = this.Page.User.Identity.Name;
                    op.DiscountCampaignSet(dca);
                    SetGrid(1);

                    #endregion
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('您無此權限');", true);
                }


            }

            if (e.CommandName == "canceled")
            {
                #region 作廢

                DiscountCampaign dca = op.DiscountCampaignGet(int.Parse(e.CommandArgument.ToString()));
                dca.CancelTime = DateTime.Now;
                dca.CancelId = this.Page.User.Identity.Name;
                op.DiscountCampaignSet(dca);
                DiscountCodeCollection codes = op.DiscountCodeGetListByCampaign(dca.Id);
                foreach (DiscountCode code in codes)
                {
                    code.CancelTime = dca.CancelTime;
                    op.DiscountCodeSet(code);
                }
                SetGrid(1);
                #endregion
            }

            if (e.CommandName == "DEL")
            {
                #region 刪除

                int id = int.Parse(e.CommandArgument.ToString());
                op.DiscountCampaignDeleteById(id);
                op.DiscountCodeDeleteByCampaignId(id);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('coupon "+id.ToString()+" 已刪除');", true);
                SetGrid(1);
                #endregion
            }

        }

        private void SetGrid(int pagenumber)
        {
            IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            gvMain.DataSource = op.ViewDiscountMainGetList(pagenumber, 20, "", GetFilter());
            gvMain.DataBind();

            gridPager.ResolvePagerView(pagenumber, true);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("campaign_add.aspx");
        }

        protected void btnQuery_Click(object sender, EventArgs e)
        {
            Response.Redirect("codesearch.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SetGrid(1);
        }

        private string[] GetFilter()
        {
            List<string> ll = new List<string>();
            if (txtEventName.Text != "")
            {
                ll.Add(ViewDiscountMain.Columns.Name + " LIKE %" + txtEventName.Text + "%");
            }

            if (!string.IsNullOrEmpty(txtActivitySDate.Text) && !string.IsNullOrEmpty(txtActivityEDate.Text))
            {
                ll.Add(ViewDiscountMain.Columns.StartTime + " between " + txtActivitySDate.Text + " and " + txtActivityEDate.Text);
            }

            if (!string.IsNullOrEmpty(txtActivitySDate.Text) && !string.IsNullOrEmpty(txtActivityEDate.Text))
            {
                ll.Add(ViewDiscountMain.Columns.EndTime + " between " + txtActivitySDate.Text + " and " + txtActivityEDate.Text);
            }
            ll.Add(ViewDiscountMain.Columns.User + " <> " + "Sys_StaffDiscount");
            return ll.ToArray();
        }


        private bool IsApplyMember()
        {
            if (conf.EnablePageAccessControl)
            {
                return CommonFacade.IsInSystemFunctionPrivilege(this.Page.User.Identity.Name, SystemFunctionType.Approve);
            }
            else
            {
                return true;
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using log4net;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.Web.ControlRoom.discount
{
    public partial class campaign_add : RolePage
    {
        private static ILog log = LogManager.GetLogger(typeof(campaign_add));
        private EventPromoCollection EventPromoList
        {
            get
            {
                if (ViewState["EventPromo"] == null)
                {
                    ViewState["EventPromo"] = new EventPromoCollection();
                }
                return (EventPromoCollection)ViewState["EventPromo"];
            }
            set
            {
                ViewState["EventPromo"] = value;
            }
        }
        private BrandCollection BrandList
        {
            get
            {
                if (ViewState["Brand"] == null)
                {
                    ViewState["Brand"] = new BrandCollection();
                }
                return (BrandCollection)ViewState["Brand"];
            }
            set
            {
                ViewState["Brand"] = value;
            }
        }

        private ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        public ISysConfProvider SystemConfig
        {
            get { return cp; }
        }

        //連續新增(設定日期區間 每天設定一個折價券 限當日用完)
        public bool IsMultipleAdd
        {
            get
            {
                bool ismultipleadd;
                if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "multipleadd")
                    ismultipleadd = true;
                else
                    ismultipleadd = false;
                return ismultipleadd;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            cbx_MultipleAdd.Visible = IsMultipleAdd;
            if (!IsPostBack)
            {
                List<ListItem> hours = new List<ListItem>();
                for (int i = 0; i < 24; i++)
                {
                    string hour = ("00" + i).Substring(("00" + i).Length - 2, 2);
                    hours.Add(new ListItem(hour, hour));
                }

                ddlSTimeHour.DataSource = hours;
                ddlSTimeHour.SelectedValue = "00";
                ddlSTimeHour.DataBind();

                ddlEventDateSHour.DataSource = hours;
                ddlEventDateSHour.SelectedValue = "00";
                ddlEventDateSHour.DataBind();

                ddlETimeHour.DataSource = hours;
                ddlETimeHour.SelectedValue = "23";
                ddlETimeHour.DataBind();

                ddlEventDateEHour.DataSource = hours;
                ddlEventDateEHour.SelectedValue = "00";
                ddlEventDateEHour.DataBind();

                List<ListItem> mintues = new List<ListItem>();
                for (int i = 0; i < 60; i++)
                {
                    string minute = ("00" + i).Substring(("00" + i).Length - 2, 2);
                    mintues.Add(new ListItem(minute, minute));
                }

                ddlSTimeMinute.DataSource = mintues;
                ddlSTimeMinute.SelectedValue = "00";
                ddlSTimeMinute.DataBind();

                ddlEventDateSMinute.DataSource = mintues;
                ddlEventDateSMinute.SelectedValue = "00";
                ddlEventDateSMinute.DataBind();

                ddlETimeMinute.DataSource = mintues;
                ddlETimeMinute.SelectedValue = "59";
                ddlETimeMinute.DataBind();

                ddlEventDateEMinute.DataSource = mintues;
                ddlEventDateEMinute.SelectedValue = "00";
                ddlEventDateEMinute.DataBind();

                rptChannel.DataSource = CategoryManager.PponChannelCategoryTree.CategoryNodes.OrderBy(x => x.Seq);
                rptChannel.DataBind();
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                bool check = true;
                string message = string.Empty;
                int quantity = 0;
                int amount = 0;
                int minimum_amount;
                DateTime start_date, end_date;
                DiscountCampaign campaign = new DiscountCampaign();

                if (string.IsNullOrEmpty(txtName.Text))
                {
                    check = false;
                    message += "請輸入活動名稱;";
                }

                #region 選擇面額

                if (rbten.Checked)
                {
                    if (!int.TryParse(txttexqty.Text, out quantity))
                    {
                        check = false;
                        message += "請輸入數量;";
                    }
                    campaign.Amount = 10;
                }
                else if (rbfifty.Checked)
                {
                    if (!int.TryParse(txtfiftyqty.Text, out quantity))
                    {
                        check = false;
                        message += "請輸入數量;";
                    }
                    campaign.Amount = 50;
                }
                else
                {
                    if (!int.TryParse(txtcustqty.Text, out quantity))
                    {
                        check = false;
                        message += "請輸入數量;";
                    }
                    if (!int.TryParse(txtcust.Text, out amount))
                    {
                        check = false;
                        message += "請輸入自訂面額;";
                    }
                    campaign.Amount = amount;
                }
                campaign.Qty = quantity;

                #endregion

                if (!DateTime.TryParse(txtSTime.Text, out start_date))
                {
                    check = false;
                    message += "請輸入開始時間;";
                }
                else
                {
                    start_date = new DateTime(start_date.Year, start_date.Month, start_date.Day, int.Parse(ddlSTimeHour.SelectedItem.Value),
                                              int.Parse(ddlSTimeMinute.SelectedItem.Value), 0);
                }

                if (!DateTime.TryParse(txtETime.Text, out end_date))
                {
                    check = false;
                    message += "請輸入結束時間;";
                }
                else
                {
                    end_date = new DateTime(end_date.Year, end_date.Month, end_date.Day, int.Parse(ddlETimeHour.SelectedItem.Value), int.Parse(ddlETimeMinute.SelectedItem.Value), 59);
                }

                campaign.StartTime = start_date;
                campaign.EndTime = end_date;
                campaign.Type = (int)DiscountCampaignType.PponDiscountTicket;
                if (cbx_MinimumAmount.Checked)
                {
                    if (!int.TryParse(tbx_MinimumAmount.Text, out minimum_amount))
                    {
                        check = false;
                        message += "請輸入最低訂單金額;";
                    }
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.MinimumAmount;
                    campaign.MinimumAmount = minimum_amount;
                    if (minimum_amount < campaign.Amount)
                    {
                        check = false;
                        message += "限制金額不可小於面額;";
                    }
                }

                int freeGiftDiscount;
                DateTime eventDateS, eventDateE;

                // 活動期間檢查(檢查需搭配活動期間的設定項目，ex:消費贈送等值折價券)
                if (rbFreeGiftWithPurchase.Checked)
                {
                    if (string.IsNullOrWhiteSpace(txtEventDateS.Text) || string.IsNullOrWhiteSpace(txtEventDateE.Text))
                    {
                        message += "請輸入活動期間;";
                        check = false;
                    }
                    else if (!DateTime.TryParse(txtEventDateS.Text, out eventDateS) || !DateTime.TryParse(txtEventDateE.Text, out eventDateE))
                    {
                        message += "活動期間格式有誤;";
                        check = false;
                    }
                    else
                    {
                        campaign.EventDateS = new DateTime(eventDateS.Year, eventDateS.Month, eventDateS.Day, int.Parse(ddlEventDateSHour.SelectedItem.Value), int.Parse(ddlEventDateSMinute.SelectedItem.Value), 0);
                        campaign.EventDateE = new DateTime(eventDateE.Year, eventDateE.Month, eventDateE.Day, int.Parse(ddlEventDateEHour.SelectedItem.Value), int.Parse(ddlEventDateEMinute.SelectedItem.Value), 0);
                    }
                }

                // 消費贈送等值折價券檢查
                if (rbFreeGiftWithPurchase.Checked)
                {
                    if (!int.TryParse(txtFreeGiftDiscount.Text, out freeGiftDiscount))
                    {
                        message += "請輸入贈送折價券門檻;";
                        check = false;
                    }
                    else
                    {
                        campaign.FreeGiftDiscount = freeGiftDiscount;
                        campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.FreeGiftWithPurchase;
                    }
                }

                if (rbAutoSend.Checked)
                {
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.AutoSend;
                }

                if (chkOwnerUseOnly.Checked)
                {
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.OwnerUseOnly;
                }

                if (chkTakeCeiling.Checked)
                {
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.TakeCeiling;
                }

                if (chkVISA.Checked)
                {
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.VISA;
                }

                if (chkAppUseOnly.Checked)
                {
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.AppUseOnly;
                }

                campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.Ppon;

                foreach (ListItem item in chklType.Items)
                {
                    if (item.Selected)
                    {
                        int value;
                        int.TryParse(item.Value, out value);
                        campaign.Flag = campaign.Flag | value;
                    }
                }

                if ((campaign.Flag & (int)DiscountCampaignUsedFlags.InstantGenerate) > 0 && (campaign.Flag & (int)DiscountCampaignUsedFlags.SingleSerialKey) > 0)
                {
                    check = false;
                    message += "即時產生不可搭配統一序號;";
                }

                if (cbx_BindEventPromoId.Checked)
                {
                    if (EventPromoList.Count == 0)
                    {
                        check = false;
                        message += "限制使用檔次需輸入商品主題活動ID";
                    }
                }


                if (cbx_BindBrandId.Checked)
                {
                    if (BrandList.Count == 0)
                    {
                        check = false;
                        message += "限制使用檔次需輸入策展活動ID";
                    }
                }

                if ((campaign.Flag & (int)DiscountCampaignUsedFlags.VISA) > 0 && (campaign.Flag & (int)DiscountCampaignUsedFlags.OwnerUseOnly) > 0)
                {
                    check = false;
                    message += "VISA獨享不可限制本人使用;";
                }


                if (check)
                {
                    var op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

                    if ((campaign.Flag & (int)DiscountCampaignUsedFlags.SingleSerialKey) > 0 &&
                        !string.IsNullOrEmpty(txtCustomCode.Text.Trim()))
                    {
                        var viewDiscountDetail =
                            op.ViewDiscountDetailCheckSingleSerialDuplicate(txtCustomCode.Text.Trim(), start_date, end_date);
                        if (viewDiscountDetail.IsLoaded)
                        {
                            lblError.Text = string.Format("統一序號自訂短碼與活動({0}:{1}~{2})短碼內容重複"
                                , viewDiscountDetail.Name
                                , (viewDiscountDetail.StartTime != null) ? ((DateTime)viewDiscountDetail.StartTime).ToString("yyyy/MM/dd HH:mm:ss") : string.Empty
                                , (viewDiscountDetail.EndTime != null) ? ((DateTime)viewDiscountDetail.EndTime).ToString("yyyy/MM/dd HH:mm:ss") : string.Empty
                                );
                            return;
                        }
                        campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.CustomSerialKey;
                    }

                    campaign.Name = txtName.Text;
                    campaign.CreateTime = DateTime.Now;
                    campaign.CreateId = Page.User.Identity.Name;

                    if (IsMultipleAdd && cbx_MultipleAdd.Checked)
                    {
                        int days = (campaign.EndTime.Value.Date - campaign.StartTime.Value.Date).Days;
                        for (int j = 0; j < days; j++)
                        {
                            DiscountCampaign i_campaign = new DiscountCampaign();
                            i_campaign = campaign.Clone();
                            i_campaign.StartTime = campaign.StartTime.Value.Date.AddDays(j);
                            i_campaign.EndTime = i_campaign.StartTime.Value.Date.AddDays(1);
                            i_campaign.Name += i_campaign.StartTime.Value.Date.ToString("(yyyy/MM/dd)");
                            op.DiscountCampaignSet(i_campaign);
                            if ((i_campaign.Flag & (int)DiscountCampaignUsedFlags.InstantGenerate) == 0)
                            {
                                string code = string.Empty;
                                if ((i_campaign.Flag & (int)DiscountCampaignUsedFlags.SingleSerialKey) > 0)
                                {
                                    code = PromotionFacade.GetDiscountCode(i_campaign.Amount.Value);
                                }
                                for (int i = 0; i < i_campaign.Qty; i++)
                                {
                                    DiscountCode dc = new DiscountCode();
                                    dc.CampaignId = i_campaign.Id;
                                    dc.Amount = i_campaign.Amount;
                                    if ((i_campaign.Flag & (int)DiscountCampaignUsedFlags.SingleSerialKey) == 0)
                                    {
                                        code = PromotionFacade.GetDiscountCode(dc.Amount.Value);
                                    }
                                    dc.Code = code;
                                    op.DiscountCodeSet(dc);
                                }
                            }
                            SetDiscountEventLink(i_campaign.Id);
                        }
                    }
                    else
                    {
                        op.DiscountCampaignSet(campaign);
                        if ((campaign.Flag & (int)DiscountCampaignUsedFlags.InstantGenerate) == 0)
                        {
                            var code = string.Empty;
                            if ((campaign.Flag & (int)DiscountCampaignUsedFlags.SingleSerialKey) > 0)
                            {
                                code = string.IsNullOrEmpty(txtCustomCode.Text.Trim()) ? PromotionFacade.GetDiscountCode(campaign.Amount.Value) : txtCustomCode.Text.Trim();
                            }

                            // 若為限本人使用勢必需要歸戶，因此程式改為歸戶時才產生資料
                            if ((campaign.Flag & (int)DiscountCampaignUsedFlags.OwnerUseOnly) == 0)
                            {
                                DateTime start = DateTime.Now;
                                DiscountCodeCollection dataList = new DiscountCodeCollection();
                                for (var i = 0; i < campaign.Qty; i++)
                                {
                                    DiscountCode dc = new DiscountCode();
                                    dc.CampaignId = campaign.Id;
                                    dc.Amount = campaign.Amount;
                                    if ((campaign.Flag & (int)DiscountCampaignUsedFlags.SingleSerialKey) == 0)
                                    {
                                        code = PromotionFacade.GetDiscountCode(dc.Amount.Value);
                                    }
                                    dc.Code = code;
                                    dataList.Add(dc);
                                }

                                if (dataList.Count > 0)
                                {
                                    op.DiscountCodeCollectionBulkInsert(dataList);
                                }
                                log.Info(campaign + " - BulkInsert 耗時 :" + new TimeSpan(DateTime.Now.Ticks - start.Ticks).TotalSeconds.ToString("0.##") + "秒");
                            }
                        }
                        SetDiscountEventLink(campaign.Id);
                        SetDiscountCategory(campaign.Id);
                    }

                    Response.Write("<script>window.alert('新增完成');location.href = 'campaignmain.aspx';</script>");
                }
                else
                    lblError.Text = message;
            }
            catch (Exception ex)
            {
                lblError.Text = "出現錯誤請將以下文字給技術部<br/>" + ex.Message;
            }

        }

        protected void btnAddEventPromo_Click(object sender, EventArgs e)
        {
            int eventPromoId = 0;
            Guid bid = Guid.Empty;
            if (Guid.TryParse(tbx_EventPromoId.Text.Trim(), out bid))
            {
                #region 如若輸入的是bid，則自動建立一筆商品主題活動

                ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(bid);
                if (vpd.IsLoaded)
                {
                    if (!Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
                    {
                        DateTime start_date;
                        if (!DateTime.TryParse(txtSTime.Text, out start_date))
                        {
                            lbl_BindEventPromoMsg.Text = "請輸入開始時間;";
                            return;
                        }
                        else
                        {
                            start_date = new DateTime(start_date.Year, start_date.Month, start_date.Day, int.Parse(ddlSTimeHour.SelectedItem.Value),
                                                      int.Parse(ddlSTimeMinute.SelectedItem.Value), 0);
                        }

                        DateTime end_date;
                        if (!DateTime.TryParse(txtETime.Text, out end_date))
                        {
                            lbl_BindEventPromoMsg.Text = "請輸入結束時間;";
                            return;
                        }
                        else
                        {
                            end_date = new DateTime(end_date.Year, end_date.Month, end_date.Day, int.Parse(ddlETimeHour.SelectedItem.Value), int.Parse(ddlETimeMinute.SelectedItem.Value), 59);
                        }

                        EventPromo promo = new EventPromo();
                        promo.Title = vpd.ItemName;
                        promo.Url = string.Format("discount_{0}", vpd.UniqueId);
                        promo.StartDate = start_date;
                        promo.EndDate = end_date;
                        promo.BgColor = "FFFFFF";
                        promo.Status = true;
                        promo.Creator = Page.User.Identity.Name;
                        promo.Cdt = DateTime.Now;
                        promo.TemplateType = (int)EventPromoTemplateType.PponOnly;
                        promo.ShowInWeb = true;
                        MarketingFacade.SaveCurationAndResetMemoryCache(promo);

                        EventPromoItem item = new EventPromoItem();
                        item.MainId = promo.Id;
                        item.Title = vpd.ItemName;
                        item.Description = vpd.EventTitle;
                        item.Seq = 1;
                        item.Status = true;
                        item.Creator = Page.User.Identity.Name;
                        item.Cdt = DateTime.Now;
                        item.ItemId = vpd.UniqueId.Value;
                        item.ItemType = (int)EventPromoItemType.Ppon;
                        pp.SaveEventPromoItem(item);

                        tbx_EventPromoId.Text = promo.Id.ToString();
                    }
                    else
                    {
                        lbl_BindEventPromoMsg.Text = "無法綁定子檔次，請輸入母擋bid";
                    }
                }
                else
                {
                    lbl_BindEventPromoMsg.Text = "bid錯誤，無法綁定";
                }

                #endregion
            }

            if (!int.TryParse(tbx_EventPromoId.Text.Trim(), out eventPromoId))
            {
                lbl_BindEventPromoMsg.Text = "請輸入商品主題活動ID";
            }
            else
            {
                lbl_BindEventPromoMsg.Text = string.Empty;
                EventPromo eventPromo = pp.GetEventPromo(eventPromoId);
                if (!eventPromo.IsLoaded)
                {
                    lbl_BindEventPromoMsg.Text = "查無此商品主題活動ID";
                }
                else
                {
                    cbx_BindEventPromoId.Checked = true;
                    lbl_BindEventPromoMsg.Text = string.Empty;
                    tbx_EventPromoId.Text = string.Empty;
                    EventPromoList.Add(eventPromo);
                    gv_BindEventPromo.DataSource = EventPromoList;
                    gv_BindEventPromo.DataBind();
                }
            }
            UpdatePanel1.Update();
        }

        protected void btnAddBrand_Click(object sender, EventArgs e)
        {
            int brandId = 0;

            if (!int.TryParse(tbx_BrandId.Text.Trim(), out brandId))
            {
                lbl_BindEventPromoMsg.Text = "請輸入策展版型活動ID";
            }
            else
            {
                lbl_BindEventPromoMsg.Text = string.Empty;
                DataOrm.Brand brand = pp.GetBrand(brandId);
                if (!brand.IsLoaded)
                {
                    lbl_BindEventPromoMsg.Text = "查無此策展版型活動ID";
                }
                else
                {
                    cbx_BindBrandId.Checked = true;
                    lbl_BindEventPromoMsg.Text = string.Empty;
                    tbx_BrandId.Text = string.Empty;
                    BrandList.Add(brand);
                    gv_BindBrand.DataSource = BrandList;
                    gv_BindBrand.DataBind();
                }
            }
            UpdatePanel1.Update();
        }

        protected void gv_BindEventPromo_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ImageButton ibtn = (ImageButton)e.CommandSource;
            GridViewRow row = (GridViewRow)ibtn.NamingContainer;
            if (e.CommandName == "DeleteEventPromoId")
            {
                EventPromoList.RemoveAt(row.RowIndex);
            }
            gv_BindEventPromo.DataSource = EventPromoList;
            gv_BindEventPromo.DataBind();
            if (EventPromoList.Count == 0)
            {
                cbx_BindEventPromoId.Checked = false;
            }
            UpdatePanel1.Update();
        }

        protected void gv_BindBrand_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ImageButton ibtn = (ImageButton)e.CommandSource;
            GridViewRow row = (GridViewRow)ibtn.NamingContainer;
            if (e.CommandName == "DeleteBrandId")
            {
                BrandList.RemoveAt(row.RowIndex);
            }
            gv_BindBrand.DataSource = BrandList;
            gv_BindBrand.DataBind();

            if (BrandList.Count == 0)
            {
                cbx_BindBrandId.Checked = false;
            }
            UpdatePanel1.Update();
        }

        private void SetDiscountEventLink(int campaignId)
        {
            IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

            //商品主題活動
            if (cbx_BindEventPromoId.Checked)
            {
                foreach (EventPromo promo in EventPromoList)
                {
                    DiscountEventPromoLink promoLink = new DiscountEventPromoLink();
                    promoLink.CampaignId = campaignId;
                    promoLink.EventPromoId = promo.Id;
                    op.DiscountEventPromoLinkSet(promoLink);
                }
            }

            //策展2.0
            if (cbx_BindBrandId.Checked)
            {
                foreach (DataOrm.Brand brand in BrandList)
                {
                    DiscountBrandLink brandLink = new DiscountBrandLink();
                    brandLink.CampaignId = campaignId;
                    brandLink.BrandId = brand.Id;
                    op.DiscountBrandLinkSet(brandLink);
                }
            }
        }

        private void SetDiscountCategory(int campaignId)
        {
            bool result = false;
            foreach (RepeaterItem channel in rptChannel.Items)
            {
                int channelId = 0;
                CheckBox chkChannel = (CheckBox)channel.FindControl("chkChannel");
                if (chkChannel.Checked)
                {
                    result = true;
                    HiddenField hidChannel = (HiddenField)channel.FindControl("hidChannel");
                    if (int.TryParse(hidChannel.Value, out channelId) && channelId != 0)
                    {
                        bool channelOnly = true;
                        Repeater rptArea = (Repeater)channel.FindControl("rptArea");
                        foreach (RepeaterItem area in rptArea.Items)
                        {
                            CheckBox chkArea = (CheckBox)area.FindControl("chkArea");
                            if (chkArea.Checked)
                            {
                                HiddenField hidArea = (HiddenField)area.FindControl("hidArea");
                                int areaId = int.TryParse(hidArea.Value, out areaId) ? areaId : 0;
                                if (areaId != 0)
                                {
                                    DiscountCategorySet(campaignId, channelId, areaId);
                                    channelOnly = false;
                                }
                            }
                        }

                        Repeater rptCategory = (Repeater)channel.FindControl("rptCategory");
                        foreach (RepeaterItem category in rptCategory.Items)
                        {
                            CheckBox chkCategory = (CheckBox)category.FindControl("chkCategory");
                            if (chkCategory.Checked)
                            {
                                HiddenField hidCategory = (HiddenField)category.FindControl("hidCategory");
                                int categoryId = int.TryParse(hidCategory.Value, out categoryId) ? categoryId : 0;
                                if (categoryId != 0)
                                {
                                    DiscountCategorySet(campaignId, channelId, categoryId);
                                    channelOnly = false;
                                }
                            }
                        }

                        if (channelOnly)
                        {
                            DiscountCategorySet(campaignId, channelId, channelId);
                        }
                    }
                }
            }

            //若為商品主題活動或策展也代表有分類限制
            if ((cbx_BindEventPromoId.Checked & EventPromoList.Count > 0) || (cbx_BindBrandId.Checked & BrandList.Count > 0))
            {
                result = true;
            }

            if (result)
            {
                IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
                DiscountCampaign campaign = op.DiscountCampaignGet(campaignId);
                campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.CategoryLimit;
                op.DiscountCampaignSet(campaign);
            }
        }

        private static void DiscountCategorySet(int campaignId, int channelId, int categoryId)
        {
            IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

            DiscountCategory dc = new DiscountCategory();
            dc.CampaignId = campaignId;
            dc.ChannelId = channelId;
            dc.CategoryId = categoryId;
            op.DiscountCategorySet(dc);
        }

        protected void gv_BindEventPromo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is EventPromo)
            {
                EventPromo promo = (EventPromo)e.Row.DataItem;

                string pageName = string.Empty;
                switch ((EventPromoTemplateType)promo.TemplateType)
                {
                    case EventPromoTemplateType.PponOnly:
                        pageName = "exhibition";
                        break;
                    case EventPromoTemplateType.JoinPiinlife:
                        pageName = "exhibitionjoinpiinlife";
                        break;
                    case EventPromoTemplateType.Commercial:
                        pageName = "exhibitioncommercial";
                        break;
                    case EventPromoTemplateType.Kind:
                        pageName = "exhibitionkind";
                        break;
                    case EventPromoTemplateType.Zero:
                        pageName = "exhibitionzero";
                        break;
                    case EventPromoTemplateType.SkmEvent:
                        pageName = "skmexhibitioncommercial";
                        break;

                    default:
                        break;
                }

                HyperLink lkPreview = (HyperLink)e.Row.FindControl("lkPreview");
                lkPreview.NavigateUrl = SystemConfig.SiteUrl + "/event/" + pageName + ".aspx?u=" + promo.Url + "&p=show_me_the_preview";
            }
        }

        protected void gv_BindBrand_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is DataOrm.Brand)
            {
                DataOrm.Brand brand = (DataOrm.Brand)e.Row.DataItem;
                
                HyperLink lkPreview = (HyperLink)e.Row.FindControl("lkPreview");
                lkPreview.NavigateUrl = SystemConfig.SiteUrl + "/event/brandevent/" + brand.Url + "?p=show_me_the_preview";
            }
        }

        protected void rptChannel_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                Repeater rptArea = (Repeater)e.Item.FindControl("rptArea");
                Repeater rptCategory = (Repeater)e.Item.FindControl("rptCategory");
                CheckBox chkChannel = (CheckBox)e.Item.FindControl("chkChannel");
                HiddenField hidChannel = (HiddenField)e.Item.FindControl("hidChannel");

                chkChannel.Text = dataItem.CategoryName;
                hidChannel.Value = dataItem.CategoryId.ToString();

                List<CategoryTypeNode> areaCategories = dataItem.NodeDatas.Where(x => x.NodeType == CategoryType.PponChannelArea).ToList();
                if (areaCategories.Count > 0)
                {
                    CategoryTypeNode areaNode = areaCategories.First();
                    rptArea.DataSource = areaNode.CategoryNodes;
                    rptArea.DataBind();
                }

                List<CategoryTypeNode> categories = dataItem.NodeDatas.Where(x => x.NodeType == CategoryType.DealCategory).ToList();
                if (categories.Count > 0)
                {
                    CategoryTypeNode categoryNode = categories.First();
                    rptCategory.DataSource = categoryNode.CategoryNodes;
                    rptCategory.DataBind();
                }
            }
        }

        protected void rptArea_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                CheckBox chkArea = (CheckBox)e.Item.FindControl("chkArea");
                HiddenField hidArea = (HiddenField)e.Item.FindControl("hidArea");
                chkArea.Text = dataItem.CategoryName;
                hidArea.Value = dataItem.CategoryId.ToString();

            }
        }

        protected void rptCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                CheckBox chkCategory = (CheckBox)e.Item.FindControl("chkCategory");
                HiddenField hidCategory = (HiddenField)e.Item.FindControl("hidCategory");
                chkCategory.Text = dataItem.CategoryName;
                hidCategory.Value = dataItem.CategoryId.ToString();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;


namespace LunchKingSite.Web.ControlRoom.discount
{
    public partial class codesearch : RolePage
    {
        public int campaignId = 0;
        public string m_Status = "";
        public string m_amount="";
        public string m_CouponCode="";
        public string m_EventName="";
        public string minimumAmount = "";
        public string expireDate = "";
        public string m_Date = "";
        public string m_user_id="";
        public string m_Order_id="";
        public string m_real_cost = "";
        public string owner = "";
        public string useLimit = "";
        public string memo = "";
        public string eventPromoId = "";
        public string eventPromoLink = "";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnQuery_Click(object sender, EventArgs e)
        {
            IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            DiscountCode code= op.DiscountCodeGetByCode(txtCode.Text);
            p1.Visible = false;
            p2.Visible = false;
            if (code.Code == null)
            {
                p2.Visible = true;
            }
            else
            {
                DiscountCampaign dc= op.DiscountCampaignGet(code.CampaignId.Value);
                campaignId = dc.Id;
                p1.Visible = true;
                if (code.UseTime != null)
                    m_Status = "已使用";
                else
                    m_Status = "未使用";
                if(code.CancelTime!=null)
                    m_Status="已作廢";

                m_amount = code.Amount.Value.ToString("f0");
                m_CouponCode = code.Code;
                m_EventName = dc.Name;
                minimumAmount = (dc.MinimumAmount ?? 0).ToString();
                expireDate = dc.StartTime.Value.ToString("yyyy/MM/dd HH:mm") + " ~ " + dc.EndTime.Value.ToString("yyyy/MM/dd HH:mm");
                useLimit = Helper.IsFlagSet(dc.Flag, DiscountCampaignUsedFlags.OwnerUseOnly) ? "限本人使用" : string.Empty;
                if (Helper.IsFlagSet(dc.Flag, DiscountCampaignUsedFlags.FreeGiftWithPurchase))
                {
                    if (dc.EventDateS.HasValue && dc.EventDateE.HasValue)
                    {
                        memo += dc.EventDateS.Value.ToString("yyyy/MM/dd HH:mm") + " ~ " + dc.EventDateE.Value.ToString("yyyy/MM/dd HH:mm");
                    }
                    memo += "消費贈等值折價券";
                    if (Helper.IsFlagSet(dc.Flag, DiscountCampaignUsedFlags.TakeCeiling))
                    {
                        memo += "(無條件進位)";
                    }
                }
                m_Date = code.UseTime.ToString();
                m_user_id = code.UseId == null ? "" : MemberFacade.GetUserName(code.UseId.GetValueOrDefault());
                if (code.OrderGuid != null)
                {
                    m_Order_id = op.OrderGet(code.OrderGuid.Value).OrderId;
                    m_real_cost = code.OrderAmount.Value.ToString("f0");
                }
                if (code.Owner != null)
                {
                    owner = MemberFacade.GetUserEmail(code.Owner.Value);
                }
            }
        }
    }
}
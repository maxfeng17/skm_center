﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="campaignmain.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.discount.campaignmain" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Register Src="~/ControlRoom/Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("jqueryui", "1.8");
    </script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui-timepicker-addon.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <script type="text/javascript" language="javascript">
        function $$(id, context) {
            var el = $("#" + id, context);
            if (el.length < 1)
                el = $("*[id$=" + id + "]", context);
            return el;
        }
        $(document).ready(function () {
            $$('txtActivitySDate').datepicker({ dateFormat: 'yy/mm/dd', stepMinute: 1 });
            $$('txtActivityEDate').datepicker({ dateFormat: 'yy/mm/dd', stepMinute: 1 });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        body
        {
            font-size: 10px;
        }
        .ui-timepicker-div .ui-widget-header
        {
            margin-bottom: 8px;
        }
        .ui-timepicker-div dl
        {
            text-align: left;
        }
        .ui-timepicker-div dl dt
        {
            height: 25px;
        }
        .ui-timepicker-div dl dd
        {
            margin: -25px 0 10px 65px;
        }
        .ui-timepicker-div td
        {
            font-size: 90%;
        }
        label.error
        {
            float: none;
            color: red;
            padding-left: .5em;
            vertical-align: top;
        }
        .style1
        {
            width: 414px;
        }
        .hidfield {
            display: none;
        }
    </style>
    <table>
        <tr>
            <td style="position:relative">
                <asp:Button ID="btnAdd" Visible="False" runat="server" Text="申請coupon" OnClick="btnAdd_Click"/>
                <asp:Button ID="btnQuery" runat="server" Text="查詢coupon" OnClick="btnQuery_Click" />
                <input type="button" id="getExport" class="" value="申請新版折價券" onclick="javascript: location.href = '/ControlRoom/DiscountCode/CampaignAdd';" />
                <a target="_blank" style="position:absolute; right:0px" href="/Controlroom/Ppon/OnlineDealDiscountReport">檔次折扣後均價表</a>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <fieldset style="width: 450px">
                                <legend>查詢條件</legend>
                                <table>
                                    <tr>
                                        <td>
                                            活動名稱：<asp:TextBox ID="txtEventName" runat="server" Width="260px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            活動日期：<asp:TextBox ID="txtActivitySDate" runat="server" Width="120px"></asp:TextBox>
                                            ~ <asp:TextBox ID="txtActivityEDate" runat="server" Width="120px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSearch" runat="server" Text="搜尋" OnClick="btnSearch_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="display: none">
                <br />
                營業金額:<asp:Label ID="lblorertotal" runat="server" Text=""></asp:Label>&nbsp;| 折扣後金額:<asp:Label
                    ID="lblordercost" runat="server" Text=""></asp:Label><br />
                <br />
                申請金額:<asp:Label ID="lblamount" runat="server" Text=""></asp:Label>&nbsp;| 申請數:<asp:Label
                    ID="lblcodecount" runat="server" Text=""></asp:Label><br />
                <br />
                已使用金額:<asp:Label ID="lbluseamount" runat="server" Text=""></asp:Label>&nbsp;| 實際折扣金額:<asp:Label
                    ID="lbluserealamount" runat="server" Text=""></asp:Label>&nbsp;| 已使用數:<asp:Label
                        ID="lblusecount" runat="server" Text=""></asp:Label><br />
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvMain" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvMain_RowDataBound"
                    OnRowCommand="gvMain_RowCommand" >
                    <Columns>
                        <asp:BoundField DataField="id" HeaderText="ID" />
                        <asp:TemplateField HeaderText="啟用日期">
                            <ItemTemplate>
                                <asp:Label ID="lblapplytime" runat="server" Text=' <%# ((ViewDiscountMain)(Container.DataItem)).ApplyTime.HasValue?((ViewDiscountMain)(Container.DataItem)).ApplyTime.Value.ToString("yyyy/MM/dd HH:mm:ss"):string.Empty%>'></asp:Label><asp:LinkButton
                                    ID="btnApply" runat="server" CausesValidation="false" CommandName="apply" Text="啟用"
                                    Visible="false"></asp:LinkButton>
                                <div style="color: Red; font-size: smaller">
                                    <%# (((ViewDiscountMain)(Container.DataItem)).Flag & (int)DiscountCampaignUsedFlags.InstantGenerate)>0?"*即時發放":string.Empty%></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="狀態">
                            <ItemTemplate>
                                <asp:Label ID="lblstatus" runat="server"></asp:Label><asp:LinkButton ID="btnCancel"
                                    runat="server" CausesValidation="false" CommandName="canceled" Text="作廢" Visible="false"></asp:LinkButton>
                                <asp:LinkButton ID="btnDelete" runat="server" CausesValidation="false" CommandName="DEL" Text="刪除" Visible="false"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="createtime" HeaderText="申請日期" />
                        <asp:HyperLinkField DataTextField="name" HeaderText="活動名稱" DataNavigateUrlFields="id"
                            DataNavigateUrlFormatString="discount_detail.aspx?cid={0}" />
                        <asp:BoundField DataField="amount" HeaderText="面額" DataFormatString="{0:N0}" />
                        <asp:TemplateField HeaderText="申請數量">
                            <ItemTemplate>
                                <%# (((ViewDiscountMain)(Container.DataItem)).Flag & (int)DiscountCampaignUsedFlags.InstantGenerate)>0?(((ViewDiscountMain)(Container.DataItem)).CurrentCount.ToString()+"/"):string.Empty%>
                                <%# ((ViewDiscountMain)(Container.DataItem)).Qty%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="total" HeaderText="申請總金額" DataFormatString="{0:N0}" />
                        <asp:BoundField DataField="alreadyuse" HeaderText="已使用金額" DataFormatString="{0:N0}" />
                        <asp:BoundField DataField="realuse" HeaderText="實際折抵金額" DataFormatString="{0:N0}" />
                        <asp:TemplateField HeaderText="有效期限">
                            <HeaderStyle CssClass="hidfield" />
                            <ItemStyle CssClass="hidfield" />
                            <FooterStyle CssClass="hidfield" />
                            <ItemTemplate>
                                <asp:Label ID="between" runat="server" Text='<%# Bind("between") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="有效期限">
                            <ItemTemplate>
                                <asp:Label ID="lbbetween" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="區間(日)">
                            <ItemTemplate>
                                <asp:Label ID="lbValidDays" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="user" HeaderText="申請人" />
                        <asp:BoundField DataField="canceltime" HeaderText="作廢時間" />
                        <asp:TemplateField HeaderText="sDate">
                            <HeaderStyle CssClass="hidfield" />
                            <ItemStyle CssClass="hidfield" />
                            <FooterStyle CssClass="hidfield" />
                            <ItemTemplate>
                                <asp:Label ID="lbstarttime" runat="server" Text='<%# Bind("starttime") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="eDate">
                            <HeaderStyle CssClass="hidfield" />
                            <ItemStyle CssClass="hidfield" />
                            <FooterStyle CssClass="hidfield" />
                            <ItemTemplate>
                                <asp:Label ID="lbendtime" runat="server" Text='<%# Bind("endtime") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
                <asp:GridView ID="gvMain1" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvMain1_RowDataBound"
                    Visible="false">
                    <Columns>
                        <asp:BoundField DataField="id" HeaderText="ID" />
                        <asp:TemplateField HeaderText="啟用日期">
                            <ItemTemplate>
                                <asp:Label ID="lblapplytime" runat="server" Text='<%# Bind("applytime") %>'></asp:Label><asp:LinkButton
                                    ID="btnApply" runat="server" CausesValidation="false" CommandName="apply" Text="啟用"
                                    Visible="false"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="狀態">
                            <ItemTemplate>
                                <asp:Label ID="lblstatus" runat="server"></asp:Label><asp:LinkButton ID="btnCancel"
                                    runat="server" CausesValidation="false" CommandName="canceled" Text="作廢" Visible="false"></asp:LinkButton>
                                <asp:LinkButton ID="btnDelete" runat="server" CausesValidation="false" CommandName="DEL" Text="刪除" Visible="false"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="createtime" HeaderText="申請日期" />
                        <asp:HyperLinkField DataTextField="name" HeaderText="活動名稱" DataNavigateUrlFields="id"
                            DataNavigateUrlFormatString="discount_detail.aspx?cid={0}" />
                        <asp:BoundField DataField="amount" HeaderText="面額" />
                        <asp:BoundField DataField="qty" HeaderText="申請數量" />
                        <asp:BoundField DataField="total" HeaderText="申請總金額" />
                        <asp:BoundField DataField="alreadyuse" HeaderText="已使用金額" />
                        <asp:BoundField DataField="realuse" HeaderText="實際折抵金額" />
                        <asp:BoundField DataField="between" HeaderText="有效期限" />
                        <asp:BoundField HeaderText="區間(日)" />
                        <asp:BoundField DataField="user" HeaderText="申請人" />
                        <asp:BoundField DataField="canceltime" HeaderText="作廢時間" />
                        <asp:BoundField DataField="starttime" HeaderText="sDate" Visible="False" />
                        <asp:BoundField DataField="endtime" HeaderText="eDate" Visible="False" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <uc1:Pager ID="gridPager" runat="server" PageSize="20" OnGetCount="RetrieveTransCount"
                    OnUpdate="UpdateHandler"></uc1:Pager>
            </td>
        </tr>
    </table>
</asp:Content>

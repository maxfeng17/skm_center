﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="codesearch.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.discount.codesearch" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<a href="campaignmain.aspx">回總覽</a>
<br />

<table >
    <tr>
        <td><br /></td>
    </tr>
    <tr>
        
        <td>coupon code：<asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
            <asp:Button ID="btnQuery"
            runat="server" Text="查詢" onclick="btnQuery_Click" /></td>
    </tr>
    <asp:Panel runat="server" ID="p1"  Visible="false">
    <tr>
        <td>
            <table border="1">
                <tr>
                    <td>
                        狀態
                    </td>
                    <td>
                        面額
                    </td>
                    <td>
                        coupon code
                    </td>
                    <td>
                        活動名稱
                    </td>
                    <td>
                        有效期限
                    </td>
                    <td>
                        使用日期
                    </td>
                    <td>
                        使用帳號
                    </td>
                    <td>
                        訂單編號
                    </td>
                    <td>
                        實際扣抵
                    </td>
                    <td>
                        擁有者
                    </td>
                    <td>
                        使用門檻
                    </td>
                    <td>
                        使用限制
                    </td>
                    <td>
                        其他
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=m_Status%>
                    </td>
                    <td>
                        <%=m_amount%>
                    </td>
                    <td>
                        <%=m_CouponCode%>
                    </td>
                    <td>
                        <a target="_blank" href="discount_detail.aspx?cid=<%=campaignId%>"><%=m_EventName%></a>
                    </td>
                    <td>
                        <%=expireDate%>
                    </td>
                    <td>
                        <%=m_Date%>
                    </td>
                    <td>
                        <%=m_user_id%>
                    </td>
                    <td>
                        <%=m_Order_id%>
                    </td>
                    <td>
                        <%=m_real_cost%>
                    </td>
                    <td>
                        <%=owner%>
                    </td>
                    <td>
                        <%=minimumAmount%>
                    </td>
                    <td>
                        <%=useLimit%>
                    </td>
                    <td>
                        <%=memo%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </asp:Panel>
</table>

<asp:Panel runat="server" ID="p2" Visible="false">
<br />
<br />
查無資料
</asp:Panel>
</asp:Content>

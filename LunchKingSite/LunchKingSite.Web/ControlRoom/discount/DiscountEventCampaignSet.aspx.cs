﻿using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.discount
{
    public partial class DiscountEventCampaignSet : RolePage, IDiscountEventCampaignSetView
    {
        #region props
        private DiscountEventCampaignSetPresenter _presenter;
        public DiscountEventCampaignSetPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public int EventId
        {
            get
            {
                int id = 0;
                if (Request.QueryString["eid"] != null)
                {
                    int.TryParse(Request.QueryString["eid"].ToString(), out id);
                }
                return id;
            }
        }

        #endregion

        #region event
        public event EventHandler<DataEventArgs<Dictionary<int, int>>> Save;
        #endregion

        #region method
        public void SetDiscountCampaighList(ViewDiscountEventCampaignCollection campaignList)
        {
            gvDiscountCampaign.DataSource = campaignList;
            gvDiscountCampaign.DataBind();
        }

        public void ShowMessage(string msg)
        {
            gvDiscountCampaign.Visible = false;
            btnSave.Visible = false;
            lblMessage.Text = msg;
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Presenter.OnViewInitialized();
                hkBack.NavigateUrl = ResolveUrl("~/ControlRoom/discount/DiscountEventList.aspx?eid=" + EventId);
            }
            _presenter.OnViewLoaded();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Save != null)
            {
                Dictionary<int, int> list = new Dictionary<int, int>();
                foreach (GridViewRow gd in gvDiscountCampaign.Rows)
                {
                    int id = int.TryParse(gvDiscountCampaign.DataKeys[gd.RowIndex].Value.ToString(), out id) ? id : 0;
                    TextBox txtQty = (TextBox)gd.Cells[0].FindControl("txtQty");
                    int qty = int.TryParse(txtQty.Text, out qty) ? qty : 0;
                    if (id != 0 && qty != 0)
                    {
                        list.Add(id, qty);
                    }
                }
                this.Save(this, new DataEventArgs<Dictionary<int, int>>(list));
                Response.Redirect(ResolveUrl("~/controlroom/discount/DiscountEventList.aspx?eid=" + EventId));
            }
        }

        protected void gvDiscountCampaign_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is ViewDiscountEventCampaign)
            {
                ViewDiscountEventCampaign item = (ViewDiscountEventCampaign)e.Row.DataItem;
                TextBox txtQty = (TextBox)e.Row.FindControl("txtQty");
                Label lblId = (Label)e.Row.FindControl("lblId");
                HyperLink hkName = (HyperLink)e.Row.FindControl("hkName");
                Label lblEventDateS = (Label)e.Row.FindControl("lblEventDateS");
                Label lblEventDateE = (Label)e.Row.FindControl("lblEventDateE");
                Label lblAmount = (Label)e.Row.FindControl("lblAmount");
                Label lblQty = (Label)e.Row.FindControl("lblQty");
                txtQty.Text = item.EventQty.HasValue ? item.EventQty.Value.ToString() : string.Empty;
                lblId.Text = item.CampaignId.ToString();
                hkName.Text = item.CampaignName;
                hkName.NavigateUrl = ResolveUrl("~/ControlRoom/discount/discount_detail.aspx?cid=") + item.CampaignId;
                lblEventDateS.Text = item.StartTime.HasValue ? item.StartTime.Value.ToString("yyyy/MM/dd/ HH:mm") : string.Empty;
                lblEventDateE.Text = item.EndTime.HasValue ? item.EndTime.Value.ToString("yyyy/MM/dd/ HH:mm") : string.Empty;
                lblAmount.Text = (item.Amount ?? 0).ToString("N0");
                lblQty.Text = item.CampaignQty.HasValue ? item.CampaignQty.Value.ToString() : string.Empty;
            }
        }

        #endregion

        #region private method
        #endregion
    }
}
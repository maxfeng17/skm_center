﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="discount_detail.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.discount.discount_detail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>

<%@ Register Src="~/ControlRoom/Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ContentPlaceHolderID="SSC" runat="server">
    <style type="text/css">
        .tbEdit tr td, th {
            border: 1px solid;
            padding: 5px;
            border-color: gray;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#btn_Send').click(function () {
                var userNames = $('#tbx_UserName').val();
                if (userNames.indexOf(';') != -1) {
                    alert('不允許有分號，請以逗號或空行分隔');
                    return false;
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <a href="campaignmain.aspx">回總覽</a>
    <table>
        <tr>
            <td>
                <table class="tbEdit" style="border-collapse: collapse;">
                    <tr>
                        <td>狀態</td>
                        <td>
                            <asp:Label ID="lblstatus" runat="server" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>活動名稱</td>
                        <td>
                            <asp:TextBox ID="txtEventName" runat="server"></asp:TextBox>
                            <asp:Label ID="lblEventName" runat="server"></asp:Label>
                            &nbsp;(<asp:Label ID="lblcreatetime" runat="server"></asp:Label>&nbsp;&nbsp;<asp:Label ID="lbluser" runat="server"></asp:Label>)
                        </td>
                    </tr>
                    <tr>
                        <td>使用期限</td>
                        <td>
                            <asp:Label ID="lblExpireDate" runat="server"></asp:Label>
                            <asp:Panel ID="divExpireDate" runat="server">
                                <asp:TextBox ID="txtStartDate" runat="server" Columns="8" />
                                <asp:DropDownList ID="ddlStartTime" runat="server"></asp:DropDownList>&nbsp;:
                                <asp:DropDownList ID="ddlStartMin" runat="server"></asp:DropDownList>
                                <cc1:CalendarExtender ID="ceSD" TargetControlID="txtStartDate" runat="server" Format="yyyy/MM/dd">
                                </cc1:CalendarExtender>
                                ~
                                <asp:TextBox ID="txtEndDate" runat="server" Columns="8" />
                                <asp:DropDownList ID="ddlEndTime" runat="server"></asp:DropDownList>&nbsp;:
                                <asp:DropDownList ID="ddlEndMin" runat="server"></asp:DropDownList>
                                <cc1:CalendarExtender ID="ceED" TargetControlID="txtEndDate" runat="server" Format="yyyy/MM/dd">
                                </cc1:CalendarExtender>
                            </asp:Panel>
                            (<asp:Label ID="lbldays" runat="server"></asp:Label>天)
                        </td>
                    </tr>
                    <tr>
                        <td>面額/數量</td>
                        <td>$<asp:Label ID="lblamount" runat="server"></asp:Label>&nbsp;/&nbsp;<asp:TextBox ID="txtTotalQty" Width="48" runat="server"></asp:TextBox>
                            <asp:Label ID="lblTotalQty" runat="server"></asp:Label>
                            &nbsp;(已發送:<asp:Label ID="lblCurrentQty" Visible="false" runat="server"></asp:Label>)
                        </td>
                    </tr>
                    <tr>
                        <td>強制指定<br />最低均價計算</td>
                        <td><input type="checkbox" id="chkDiscountPrice" runat="server" style="width:18px; height:18px; cursor:pointer" />
                            <span style="display:block; font-size:9px">目前規則為限定頻道,即時發放,且不是統一序號的現金券活動會被計算，但如果想自行指定，此欄打勾。</span>
                        </td>
                    </tr>
                    <tr>
                        <td>發送給指定用戶
                        </td>
                        <td>
                            <asp:TextBox ID="txtInstantGenUserId" runat="server" Visible="false" Width="100px"></asp:TextBox>
                            <asp:Button ID="btn_InstantGen" runat="server" Text="即時產生" Visible="false" OnClick="InstantGenerateDiscountCode" />
                            <asp:Label ID="lab_InstantGenDiscountCode" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>批次匯入名單
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUpload1" runat="server" /><asp:Button ID="btn_Send" runat="server" Text="匯入" OnClick="SendDiscountCode" ClientIDMode="Static" />
                            <asp:Label ID="lab_Message" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top" style="padding-left: 50px;">
                <table class="tbEdit" style="border-collapse: collapse;">
                    <tr>
                        <td>領取條件</td>
                        <td>
                            <asp:Label ID="lblDiscountGetLimit" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>序號類型</td>
                        <td>
                            <asp:Label ID="lblDiscountType" runat="server"></asp:Label>
                            <asp:Label ID="lblUseLimit" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>適用範圍</td>
                        <td>
                            <asp:Label ID="lblFlag" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>使用限制</td>
                        <td>
                            <asp:Repeater ID="rptContions" runat="server">
                                <HeaderTemplate>
                                    <ul style="list-style-type: decimal; padding-left: 20px; margin-bottom: 0px;">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <%# Container.DataItem %>
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="更新" OnClick="btnSave_Click" />
                <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
            </td>
            <td align="right">
                <asp:Button ID="btnExport" runat="server" Text="匯出Excel" OnClick="btnExport_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound" CssClass="tbEdit">
                    <Columns>
                        <asp:BoundField HeaderText="狀態" />
                        <asp:BoundField DataField="amount" HeaderText="面額" DataFormatString="{0:N0}" />
                        <asp:BoundField DataField="code" HeaderText="coupon code" />
                        <asp:BoundField DataField="usetime" HeaderText="使用日期" />
                        <asp:TemplateField HeaderText="使用帳號">
                            <ItemTemplate>
                                <%#GetUserName(Eval("useid"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="實際扣抵" />
                        <asp:BoundField DataField="orderamount" HeaderText="訂單總價" DataFormatString="{0:N0}" />
                        <asp:BoundField DataField="ordercost" HeaderText="折扣後金額" DataFormatString="{0:N0}" />
                        <asp:BoundField DataField="orderguid" HeaderText="訂單編號" />
                        <asp:TemplateField HeaderText="擁有者">
                            <ItemTemplate>
                                <%#GetUserName(Eval("owner")) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="sender" HeaderText="發送者" />
                        <asp:BoundField DataField="senddate" HeaderText="發送日期" DataFormatString="{0:d}" />
                    </Columns>
                </asp:GridView>
                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" OnRowDataBound="GridView2_RowDataBound"
                    Visible="false">
                    <Columns>
                        <asp:BoundField HeaderText="狀態" />
                        <asp:BoundField DataField="amount" HeaderText="面額" DataFormatString="{0:N0}" />
                        <asp:BoundField DataField="code" HeaderText="coupon code" />
                        <asp:BoundField DataField="usetime" HeaderText="使用日期" />
                        <asp:TemplateField HeaderText="使用帳號">
                            <ItemTemplate>
                                <%#GetUserName(Eval("useid"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="實際扣抵" />
                        <asp:BoundField DataField="orderamount" HeaderText="訂單總價" DataFormatString="{0:N0}" />
                        <asp:BoundField DataField="ordercost" HeaderText="折扣後金額" DataFormatString="{0:N0}" />
                        <asp:BoundField DataField="orderguid" HeaderText="訂單編號" />
                        <asp:TemplateField HeaderText="擁有者">
                            <ItemTemplate>
                                <%#GetUserName(Eval("owner")) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="sender" HeaderText="發送者" />
                        <asp:BoundField DataField="senddate" HeaderText="發送日期" DataFormatString="{0:d}" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:Pager ID="gridPager" runat="server" PageSize="20" OnGetCount="RetrieveTransCount"
                    OnUpdate="UpdateHandler"></uc1:Pager>
            </td>
        </tr>
    </table>
</asp:Content>

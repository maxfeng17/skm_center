﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="DiscountEventCampaignSet.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.discount.DiscountEventCampaignSet" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <span style="font-size: small">折價券綁定限制
        <span style="color: blue; cursor: pointer;" onmouseover="$('#note').show();" onmouseout="$('#note').hide();">[?]
            <ul id="note" style="font-size: small; list-style-type: decimal; display: none;">
                <li>未綁定其他活動</li>
                <li>折價券尚未開始兌換</li>
                <li>折價券使用起日必須小於活動起日</li>
                <li>折價券使用截止日必須大於活動截止日</li>
                <li>不能綁定已作廢的折價券</li>
                <li>折價券必須設定為即時產生</li>
                <li>折價券必須設定為限本人使用</li>
                <li>邀請親友及首購的折價券會被排除</li>
                <li>無法綁定統一序號的折價券</li>
                <li>折價券必須核准</li>
            </ul>
        </span>
    </span>
    <asp:GridView ID="gvDiscountCampaign" runat="server" OnRowDataBound="gvDiscountCampaign_RowDataBound"
        GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None"
        BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="false"
        Font-Size="Small" Width="1024px" DataKeyNames="CampaignId">
        <FooterStyle BackColor="#CCCC99"></FooterStyle>
        <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
        <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
        <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
            HorizontalAlign="Center"></HeaderStyle>
        <Columns>
            <asp:TemplateField HeaderText="張數" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="2%">
                <ItemTemplate>
                    <asp:TextBox ID="txtQty" runat="server" Width="30px"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="2%">
                <ItemTemplate>
                    <asp:Label ID="lblId" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="折價券活動名稱" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                <ItemTemplate>
                    <asp:HyperLink ID="hkName" runat="server" Target="_blank"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="使用期限" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                <ItemTemplate>
                    <asp:Label ID="lblEventDateS" runat="server"></asp:Label>
                    &nbsp; ~ &nbsp;
                    <asp:Label ID="lblEventDateE" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="面額" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                <ItemTemplate>
                    $<asp:Label ID="lblAmount" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="總數" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                <ItemTemplate>
                    <asp:Label ID="lblQty" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    <asp:Button ID="btnSave" runat="server" Text="儲存" OnClick="btnSave_Click" />&nbsp;
    <asp:HyperLink ID="hkBack" runat="server">返回</asp:HyperLink>
    <asp:HyperLink ID="hkCampaignAdd" runat="server" NavigateUrl="~/ControlRoom/discount/campaign_add.aspx" Target="_blank">申請折價券</asp:HyperLink>
</asp:Content>

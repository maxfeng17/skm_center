﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Security;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using log4net;
using System.IO;
using System.Data;
using System.Text;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.Web.ControlRoom.discount
{
    public partial class discount_detail : RolePage
    {
        private static ILog log = LogManager.GetLogger(typeof(discount_detail));
        IOrderProvider op;
        ISysConfProvider cp;
        IPponProvider pp;
        IMemberProvider mp;

        public int Cid
        {
            get
            {
                int cid;
                return int.TryParse(Request["cid"] ?? string.Empty, out cid) ? cid : 0;
            }
        }

        protected void UpdateHandler(int pageNumber)
        {
            LoadData(pageNumber);
        }

        protected int RetrieveTransCount()
        {

            return Cid > 0 ? op.DiscountCodeGetCount(DiscountCode.Columns.CampaignId + "=" + Cid) : Cid;
        }

        private void LoadData(int pagenumber)
        {
            ViewDiscountMainCollection dcs = op.ViewDiscountMainGetList(1, 1, "", ViewDiscountMain.Columns.Id + "=" + Cid);
            if (dcs.Count > 0)
            {
                ViewDiscountMain discount_main = dcs.First();
                DiscountCampaign campaign = op.DiscountCampaignGet(Cid);
                String[] dates = discount_main.Between.Split('~');
                DateTime sDate = DateTime.Parse(dates[0]);
                DateTime eDate = DateTime.Parse(dates[1]);
                lblcreatetime.Text = discount_main.CreateTime.Value.ToString("yyyy/MM/dd");
                txtEventName.Text = lblEventName.Text = discount_main.Name;
                lblamount.Text = discount_main.Amount.Value.ToString("f0");

                if (discount_main.StartTime.HasValue && discount_main.EndTime.HasValue)
                {
                    txtStartDate.Text = discount_main.StartTime.Value.ToString("yyyy/MM/dd");
                    ddlStartTime.SelectedValue = string.Format("{0:00}", discount_main.StartTime.Value.Hour);
                    ddlStartMin.SelectedValue = string.Format("{0:00}", discount_main.StartTime.Value.Minute);
                    txtEndDate.Text = discount_main.EndTime.Value.ToString("yyyy/MM/dd");
                    ddlEndTime.SelectedValue = string.Format("{0:00}", discount_main.EndTime.Value.Hour);
                    ddlEndMin.SelectedValue = string.Format("{0:00}", discount_main.EndTime.Value.Minute);
                    lblExpireDate.Text = string.Format("{0} ~ {1}", discount_main.StartTime.Value.ToString("yyyy/MM/dd HH:mm"), discount_main.EndTime.Value.ToString("yyyy/MM/dd HH:mm"));
                }

                if (discount_main.StartTime < DateTime.Now)
                {
                    txtStartDate.Enabled = ddlStartTime.Enabled = ddlStartMin.Enabled = false;
                }

                lbluser.Text = discount_main.User;
                lbldays.Text = (eDate - sDate).Days.ToString();

                if (DateTime.Compare(DateTime.Now, sDate) > 0 && DateTime.Compare(DateTime.Now, eDate) < 0)
                {
                    lblstatus.Text = "有效";
                    lblstatus.ForeColor = System.Drawing.Color.Blue;
                }
                else if (DateTime.Compare(DateTime.Now, sDate) < 0)
                {
                    lblstatus.Text = "未生效";
                    lblstatus.ForeColor = System.Drawing.Color.Green;
                }
                else if (DateTime.Compare(DateTime.Now, eDate) > 0)
                {
                    lblstatus.Text = "失效";
                    lblstatus.ForeColor = System.Drawing.Color.Gray;
                }

                if (discount_main.ApplyTime == null)
                {
                    lblstatus.Text = "未啟用";
                    lblstatus.ForeColor = System.Drawing.Color.Red;
                }

                if (discount_main.CancelTime != null)
                {
                    lblstatus.Text = "已作廢";
                    lblstatus.ForeColor = System.Drawing.Color.Gray;
                }

                if (Helper.IsFlagSet(discount_main.Flag, DiscountCampaignUsedFlags.FreeGiftWithPurchase))
                {
                    lblDiscountGetLimit.Text = string.Format("於{0}至{1}消費滿{2}元贈等值折價券{3}",
                                                    discount_main.EventDateS.Value.ToString("yyyy/MM/dd HH:mm"),
                                                    discount_main.EventDateE.Value.ToString("yyyy/MM/dd HH:mm"),
                                                    discount_main.FreeGiftDiscount.Value.ToString("N0"),
                                                    (Helper.IsFlagSet(discount_main.Flag, DiscountCampaignUsedFlags.TakeCeiling) ? "(無條件進位)" : string.Empty));
                }

                if (Helper.IsFlagSet(discount_main.Flag, DiscountCampaignUsedFlags.AutoSend))
                {
                    lblDiscountGetLimit.Text = "使用自動再送一張";
                }

                lblCurrentQty.Text = discount_main.CurrentCount.ToString();
                //是即時產生
                if ((discount_main.Flag & (int)DiscountCampaignUsedFlags.InstantGenerate) > 0)
                {
                    btn_InstantGen.Visible = txtInstantGenUserId.Visible = lab_InstantGenDiscountCode.Visible = true;
                    lblCurrentQty.Visible = true;
                    lblDiscountType.Text = "即時產生折價券";
                }
                else if ((discount_main.Flag & (int)DiscountCampaignUsedFlags.SingleSerialKey) > 0)
                {
                    lblDiscountType.Text = "統一序號";
                    if ((discount_main.Flag & (int)DiscountCampaignUsedFlags.CustomSerialKey) > 0)
                    {
                        lblDiscountType.Text += "(自訂短碼)";
                    }
                }
                else
                {
                    lblDiscountType.Text = "預產";
                }

                if ((discount_main.Flag & (int)DiscountCampaignUsedFlags.VISA) > 0)
                {
                    lblDiscountType.Text += "(VISA卡獨享)";
                }

                if (Helper.IsFlagSet(discount_main.Flag, DiscountCampaignUsedFlags.OwnerUseOnly))
                {
                    lblUseLimit.Text = "(限本人使用)";
                }

                if (Helper.IsFlagSet(discount_main.Flag, DiscountCampaignUsedFlags.AppUseOnly))
                {
                    lblUseLimit.Text += "(限app用)";
                }

                txtTotalQty.Enabled = true;
                txtTotalQty.Text = lblTotalQty.Text = discount_main.Qty.ToString();
                //已作廢或限本人使用時，不可修改申請數量
                if (discount_main.CancelTime != null || (discount_main.Flag & (int)DiscountCampaignUsedFlags.OwnerUseOnly) > 0)
                {
                    txtTotalQty.Enabled = false;
                }
                chkDiscountPrice.Checked = campaign.IsDiscountPriceForDeal;
                //已作廢時，將發送給指定用戶的瀏覽和確定按鈕隱藏
                if (discount_main.CancelTime != null || DateTime.Compare(DateTime.Now, eDate) > 0)
                {
                    FileUpload1.Visible = false;
                    btn_Send.Visible = false;
                    chkDiscountPrice.Disabled = true;
                }

                //無更新權限時，將更新按鈕隱藏
                if (!IsUpdateOk(discount_main))
                {
                    btnSave.Visible = false;

                    lblEventName.Visible = true;
                    txtEventName.Visible = false;

                    lblExpireDate.Visible = true;
                    divExpireDate.Visible = false;

                    lblTotalQty.Visible = true;
                    txtTotalQty.Visible = false;
                }
                else
                {
                    btnSave.Visible = true;

                    lblEventName.Visible = false;
                    txtEventName.Visible = true;

                    lblExpireDate.Visible = false;
                    divExpireDate.Visible = true;

                    lblTotalQty.Visible = false;
                    txtTotalQty.Visible = true;
                }

                if (Helper.IsFlagSet(discount_main.Flag, DiscountCampaignUsedFlags.CategoryLimit))
                {
                    lblFlag.Text = PromotionFacade.GetDiscountCategoryString(discount_main.Id, true, false, false);
                }

                List<string> conditions = new List<string>();
                if ((discount_main.Flag & (int)DiscountCampaignUsedFlags.MinimumAmount) > 0)
                {
                    conditions.Add("限制最低訂單金額" + discount_main.MinimumAmount.Value.ToString("F0"));
                }
                
                if (discount_main.MinGrossMargin != null && discount_main.MinGrossMargin != 0 )
                {
                    conditions.Add("限制最低毛利率" + discount_main.MinGrossMargin + "%");
                }

                EventPromoCollection epc = pp.GetEventPromoListByDiscountCampaignId(Cid);
                if (epc.Count > 0)
                {
                    conditions.Add(string.Format("限制使用檔次/商品主題活動: <br /><ul style='list-style-type: circle; padding-left: 20px;'><li>{0}</li></ul>", string.Join("</li><li>", epc.Select(x => string.Format("{0} <a target='_blank' href='{1}'>{2}</a>", x.Id, GetEventPromoLink(x), x.Title)))));
                }

                BrandCollection bc = pp.GetBrandListByDiscountCampaignId(Cid);
                if (bc.Count > 0)
                {
                    conditions.Add(string.Format("限制使用策展活動: <br /><ul style='list-style-type: circle; padding-left: 20px;'><li>{0}</li></ul>", string.Join("</li><li>", bc.Select(x => string.Format("{0} <a target='_blank' href='{1}'>{2}</a>", x.Id, cp.SiteUrl + "/event/brandevent/" + x.Url + "?p=show_me_the_preview", x.BrandName)))));
                }

                rptContions.DataSource = conditions;
                rptContions.DataBind();
            }

            DiscountCodeCollection smc = ProviderFactory.Instance().GetProvider<IOrderProvider>()
                .DiscountCodeGetList(pagenumber, 20, "", (DiscountCode.Columns.CampaignId + "=" + Cid));
            GridView1.DataSource = smc;
            GridView1.DataBind();

            gridPager.ResolvePagerView(pagenumber, true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cid == 0)
                return;
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            cp = ProviderFactory.Instance().GetConfig();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

            if (!Page.IsPostBack)
            {
                IntialControls();
                LoadData(1);
            }
        }

        private void IntialControls()
        {
            List<ListItem> hours = new List<ListItem>();
            for (int i = 0; i < 24; i++)
            {
                string hour = ("00" + i).Substring(("00" + i).Length - 2, 2);
                hours.Add(new ListItem(hour, hour));
            }

            ddlStartTime.DataSource = hours;
            ddlStartTime.DataBind();
            ddlStartTime.SelectedValue = "00";

            ddlEndTime.DataSource = hours;
            ddlEndTime.DataBind();
            ddlEndTime.SelectedValue = "00";

            List<ListItem> mins = new List<ListItem>();
            for (int i = 0; i < 60; i++)
            {
                string min = ("00" + i).Substring(("00" + i).Length - 2, 2);
                mins.Add(new ListItem(min, min));
            }
            ddlStartMin.DataSource = mins;
            ddlStartMin.DataBind();
            ddlStartMin.SelectedValue = "00";

            ddlEndMin.DataSource = mins;
            ddlEndMin.DataBind();
            ddlEndMin.SelectedValue = "00";
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[3].Text.Replace("&nbsp;", "") == "")
                    e.Row.Cells[0].Text = "未使用";
                else
                {
                    e.Row.Cells[0].Text = "已使用";
                    decimal amount = decimal.Parse(e.Row.Cells[1].Text);
                    decimal orderAmount = decimal.Parse(e.Row.Cells[6].Text);
                    if (amount > orderAmount)
                        e.Row.Cells[5].Text = orderAmount.ToString();
                    else
                        e.Row.Cells[5].Text = amount.ToString();
                }

            }
        }

        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[3].Text.Replace("&nbsp;", "") == "")
                    e.Row.Cells[0].Text = "未使用";
                else
                {
                    e.Row.Cells[0].Text = "已使用";
                    decimal amount = decimal.Parse(e.Row.Cells[1].Text);
                    decimal orderAmount = decimal.Parse(e.Row.Cells[6].Text);
                    if (amount > orderAmount)
                        e.Row.Cells[5].Text = orderAmount.ToString();
                    else
                        e.Row.Cells[5].Text = amount.ToString();
                }

            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            DiscountCodeCollection smc = op.DiscountCodeGetList(-1, -1, "", (DiscountCode.Columns.CampaignId + "=" + Cid));
            GridView2.DataSource = smc;
            GridView2.DataBind();
            GridViewExportUtil.xlsxExport("report.xlsx", GridView2);
        }

        protected void SendDiscountCode(object sender, EventArgs e)
        {
            lab_Message.Text = string.Empty;
            int total = 0;
            string userName = Page.User.Identity.Name;
            List<int> users = new List<int>();
            if (FileUpload1.HasFile)
            {
                using (StreamReader reader = new StreamReader(FileUpload1.FileContent))
                {
                    do
                    {
                        string textLine = reader.ReadLine();
                        int userId = int.TryParse(textLine, out userId) ? userId : 0;
                        users.Add(userId);
                    } while (reader.Peek() != -1);
                    reader.Close();
                }
            }

            DiscountCampaign campaign = op.DiscountCampaignGet(Cid);
            if ((campaign.Flag & (int)DiscountCampaignUsedFlags.OwnerUseOnly) > 0)
            {
                List<int> errorUsers = new List<int>();
                int userCount = users.Count;
                DiscountCodeCollection dataList = new DiscountCodeCollection();
                if (users.Count > 0)
                {
                    const int batchLimit = 300000;
                    try
                    {
                        do
                        {
                            DataTable table = dataList.ToDataTable();

                            int addCount = 0;
                            DateTime logStart = DateTime.Now;
                            for (var i = 0; i < batchLimit && i < users.Count; i++)
                            {
                                Member m = mp.MemberGet(users[i]);
                                if (m.IsLoaded && m.UniqueId != 0)
                                {
                                    DataRow row = table.NewRow();
                                    row[DiscountCode.IdColumn.ColumnName] = i;
                                    row[DiscountCode.CampaignIdColumn.ColumnName] = campaign.Id;
                                    row[DiscountCode.CampaignIdColumn.ColumnName] = campaign.Id;
                                    row[DiscountCode.AmountColumn.ColumnName] = campaign.Amount;
                                    row[DiscountCode.CodeColumn.ColumnName] = PromotionFacade.GetDiscountCode(campaign.Amount.Value);
                                    row[DiscountCode.OwnerColumn.ColumnName] = users[i];
                                    row[DiscountCode.SenderColumn.ColumnName] = userName;
                                    row[DiscountCode.SendDateColumn.ColumnName] = DateTime.Now;
                                    table.Rows.Add(row);

                                    addCount++;
                                }
                                else
                                {
                                    errorUsers.Add(users[i]);
                                }
                            }
                            log.Info(campaign + " - users toDataTable 耗時 :" + new TimeSpan(DateTime.Now.Ticks - logStart.Ticks).TotalSeconds.ToString("0.##") + "秒");

                            if (op.DiscountCodeCellectionBulkInsertFromDataTable(table))
                            {
                                users.RemoveRange(0, (users.Count < batchLimit ? users.Count : batchLimit));
                                total += addCount;
                            }
                            else
                            {
                                break;
                            }
                        } while (users.Count > 0);
                    }
                    catch (Exception ex)
                    {
                        log.InfoFormat("折價券{0}{1}匯入失敗: {2}", campaign.Id, campaign.Name, ex);
                        ExportFailUserList(users, errorUsers, string.Format("[{0}]折價券匯入失敗剩餘名單_Exception", campaign.Name));
                    }
                }

                var sendCount = op.DiscountCodeGetCount(DiscountCode.Columns.CampaignId + "=" + campaign.Id);
                if (sendCount > campaign.Qty) //若發匯入後筆數大於上限則更新上限值
                {
                    campaign.Qty = sendCount;
                }
                op.DiscountCampaignSet(campaign);

                if (total < userCount)
                {
                    ExportFailUserList(users, errorUsers, string.Format("[{0}]折價券匯入失敗剩餘名單", campaign.Name));
                }
                else
                {
                    lab_Message.Text = "發給" + total + "筆" + (total != userCount ? (";折價券不足" + (userCount - total) + "!!") : string.Empty);
                    LoadData(1);
                }
            }
        }

        private void ExportFailUserList(List<int> users, List<int> errorUsers, string fileName)
        {
            StringBuilder sb = new StringBuilder();
            int count = 0;
            for (int i = 0; i < errorUsers.Count; i++)
            {
                sb.AppendLine(errorUsers[i].ToString() + "(錯誤的使用者id)");
                count++;
            }
            for (int i = 0; i < users.Count; i++)
            {
                if (errorUsers.Contains(users[i]))
                {
                    continue;
                }
                sb.AppendLine(users[i].ToString());
                count++;
            }
            
            Context.Response.Clear();
            Context.Response.ContentEncoding = Encoding.GetEncoding(0);
            Context.Response.ContentType = "text/octet-stream";
            Context.Response.AddHeader("Content-Disposition", "attachment; filename=" + string.Format("{0}{1}筆_{2}.txt", fileName, count, DateTime.Now.ToString("yyyyMMddHHmm")));
            Response.BinaryWrite(Encoding.UTF8.GetBytes(sb.ToString()));
            Response.End();
        }


        protected void InstantGenerateDiscountCode(object sender, EventArgs e)
        {
            int userId;
            string code;
            InstantGenerateDiscountCampaignResult result;
            if (!string.IsNullOrWhiteSpace(txtInstantGenUserId.Text) && int.TryParse(txtInstantGenUserId.Text, out userId))
            {
                int amount, discountCodeId, minimumAmount;
                DateTime? endTime;
                result = PromotionFacade.InstantGenerateDiscountCode(Cid, userId, false, out code, out amount, out endTime, out discountCodeId, out minimumAmount);
            }
            else
            {
                DiscountCode discount;
                result = PromotionFacade.InstantGenerateDiscountCode(Cid, out discount);
                code = discount.IsLoaded ? discount.Code : string.Empty;
            }

            if (!string.IsNullOrEmpty(code))
            {
                lab_InstantGenDiscountCode.Text += code + ";";
            }
            LoadData(1);
            Response.Write("<script type=\"text/javascript\">alert('" + Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, result) + (string.IsNullOrEmpty(code) ? string.Empty : (":" + code)) + "')</script>");
        }

        protected string GetUserName(object owner)
        {
            //一頁最多會 query 40 次，有時間再調整
            if (owner == null)
            {
                return "";
            }
            int ownerId = ((int?)owner).Value;
            return HttpUtility.HtmlEncode(MemberFacade.GetUserName(ownerId));
        }

        private string GetEventPromoLink(EventPromo promo)
        {
            string pageName = string.Empty;
            switch ((EventPromoTemplateType)promo.TemplateType)
            {
                case EventPromoTemplateType.PponOnly:
                    pageName = "Exhibition";
                    break;
                case EventPromoTemplateType.JoinPiinlife:
                    pageName = "ExhibitionJoinPiinlife";
                    break;
                case EventPromoTemplateType.Commercial:
                    pageName = "ExhibitionCommercial";
                    break;
                case EventPromoTemplateType.Kind:
                    pageName = "ExhibitionKind";
                    break;
                case EventPromoTemplateType.Zero:
                    pageName = "ExhibitionZero";
                    break;
                case EventPromoTemplateType.SkmEvent:
                    pageName = "SkmExhibitionCommercial";
                    break;

                default:
                    break;
            }

            return cp.SiteUrl + "/Event/" + pageName + ".aspx?u=" + promo.Url + "&p=show_me_the_preview";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            lblMessage.Text = string.Empty;

            DateTime dateStart;
            DateTime.TryParse(txtStartDate.Text, out dateStart);
            int hourS = int.TryParse(ddlStartTime.SelectedValue, out hourS) ? hourS : 0;
            int minS = int.TryParse(ddlStartMin.SelectedValue, out minS) ? minS : 0;
            dateStart = new DateTime(dateStart.Year, dateStart.Month, dateStart.Day, hourS, minS, 0);

            DateTime dateEnd;
            DateTime.TryParse(txtEndDate.Text, out dateEnd);
            int hourE = int.TryParse(ddlEndTime.SelectedValue, out hourE) ? hourE : 0;
            int minE = int.TryParse(ddlEndMin.SelectedValue, out minE) ? minE : 0;
            dateEnd = new DateTime(dateEnd.Year, dateEnd.Month, dateEnd.Day, hourE, minE, 0);

            bool change = false;
            DiscountCampaign campaign = op.DiscountCampaignGet(Cid);
            if (campaign.IsLoaded && campaign.StartTime.HasValue && campaign.EndTime.HasValue)
            {
                DateTime now = DateTime.Now;
                if (string.IsNullOrWhiteSpace(txtEventName.Text))
                {
                    lblMessage.Text = "請輸入活動名稱";
                }
                else
                {
                    if (campaign.Name != txtEventName.Text.Trim())
                    {
                        campaign.Name = txtEventName.Text.Trim();
                        change = true;
                    }
                }

                if (campaign.StartTime != dateStart)
                {
                    if (campaign.StartTime > now && dateStart < now)
                    {
                        lblMessage.Text = "活動起日不可小於今日";
                    }
                    else
                    {
                        campaign.StartTime = dateStart;
                        change = true;
                    }
                }

                if (campaign.EndTime.Value.Date != dateEnd.Date || campaign.EndTime.Value.Hour != dateEnd.Hour || campaign.EndTime.Value.Minute != dateEnd.Minute)
                {
                    if (dateEnd < dateStart)
                    {
                        lblMessage.Text = "活動迄日不可小於起日";
                    }
                    else if (dateEnd < now)
                    {
                        lblMessage.Text = "活動迄日不可小於今日";
                    }
                    else
                    {
                        campaign.EndTime = dateEnd;
                        change = true;
                    }
                }
                if (chkDiscountPrice.Disabled == false)
                {
                    if (campaign.IsDiscountPriceForDeal != chkDiscountPrice.Checked)
                    {
                        change = true;
                        campaign.IsDiscountPriceForDeal = chkDiscountPrice.Checked;
                    }
                }

                //已申請數量
                int currentQty = string.IsNullOrEmpty(lblCurrentQty.Text) ? 0 : int.Parse(lblCurrentQty.Text);
                //輸入的總申請數量
                int totalQty = 0;
                //輸入欄位異動前的總申請數量，暫存以供更新後比對
                int originQty = campaign.Qty == null ? 0 : (int)campaign.Qty;
                //申請數量有異動
                if (campaign.Qty.ToString() != txtTotalQty.Text.Trim())
                {
                    if (!int.TryParse(txtTotalQty.Text, out totalQty))
                    {
                        lblMessage.Text = "總申請數量需為整數";
                    }
                    else if (totalQty < 0)
                    {
                        lblMessage.Text = "總申請數量不可小於0";
                    }
                    else if (totalQty < currentQty)
                    {
                        lblMessage.Text = "總申請數量不可小於已申請數量";
                    }
                    else
                    {
                        campaign.Qty = totalQty;
                        change = true;
                    }
                }

                if (change)
                {
                    campaign.ModifyId = MemberFacade.GetUniqueId(Page.User.Identity.Name);
                    campaign.ModifyTime = DateTime.Now;
                    op.DiscountCampaignSet(campaign);

                    //當總申請數量有異動，且非即時產生時
                    if (originQty != campaign.Qty && (campaign.Flag & (int)DiscountCampaignUsedFlags.InstantGenerate) == 0)
                    {
                        string code = string.Empty;
                        DiscountCodeCollection dataList = new DiscountCodeCollection();
                        //需增加的coupon code數量
                        int appendQty = totalQty - currentQty;
                        //true=>統一序號，false=>非統一序號
                        bool isSingleSerialKey = (campaign.Flag & (int)DiscountCampaignUsedFlags.SingleSerialKey) > 0;
                        if (isSingleSerialKey)
                        {
                            //因統一序號每筆都一樣，所以取已存的統一序號第一筆來新增
                            DiscountCodeCollection smc = ProviderFactory.Instance().GetProvider<IOrderProvider>().DiscountCodeGetList(1, 1, "", (DiscountCode.Columns.CampaignId + "=" + Cid));
                            if (smc != null && smc.Count > 0)
                            {
                                code = smc[0].Code;
                            }
                        }

                        //批次新增統一序號
                        for (int i = 0; i < appendQty; i++)
                        {
                            DiscountCode dc = new DiscountCode();
                            dc.CampaignId = campaign.Id;
                            dc.Amount = campaign.Amount;
                            //非統一序號時，每筆新增需重新產生coupon code
                            if (!isSingleSerialKey)
                            {
                                code = PromotionFacade.GetDiscountCode(dc.Amount.Value);
                            }
                            dc.Code = code;
                            dataList.Add(dc);
                        }
                        if (dataList.Count > 0)
                        {
                            op.DiscountCodeCollectionBulkInsert(dataList);
                        }
                    }

                    LoadData(1);
                    lblMessage.Text = string.Empty;
                }
            }
            else
            {
                lblMessage.Text = "查無活動資料";
            }
        }

        private bool IsUpdateOk(ViewDiscountMain discount_main)
        {
            if (CommonFacade.IsInSystemFunctionPrivilege(this.Page.User.Identity.Name, SystemFunctionType.Update)
                && discount_main.EndTime > DateTime.Now && discount_main.CancelTime == null)
            {
                return true;
            }
            return false;
        }
    }
}
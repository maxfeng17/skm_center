﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="DiscountEventList.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.discount.DiscountEventList" %>
<%@ Import Namespace="LunchKingSite.Core" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {
            CheckType();
        });

        function CheckData(obj) {
            if ($('[id*=txtName]').val() == '') {
                alert('請輸入主題活動名稱!!');
                return false;
            }
            if ($('[id*=txtStartDate]').val() == '' || $('[id*=txtEndDate]').val() == '') {
                alert('請輸入主題活動時間!!');
                return false;
            }
            return true;
        }

        function CheckType() {
            var type = $('[id*=rdlEventType]:checked').val();
            $('#trDiscountUser').hide();
            $('#trDiscountEventCode').hide();
            $('#trResult').hide();

            if ($('[id*=hiId]').val() != '' && $('[id*=hiId]').val() != '0') {
                if (type == '<%= (int)DiscountEventType.EventCode %>') {
                    $('#trDiscountEventCode').show();
                } else if (type == '<%= (int)DiscountEventType.ImportUsers %>') {
                    $('#trDiscountUser').show();
                }
                $('#trResult').show();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="divDiscountEventSearch" runat="server">
        <fieldset>
            <legend>折價券主題搜尋</legend>
            <table>
                <tr>
                    <td>名稱
                    </td>
                    <td>
                        <asp:TextBox ID="txtSearchName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>代碼
                    </td>
                    <td>
                        <asp:TextBox ID="txtSearchEventCode" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>結束時間
                    </td>
                    <td>
                        <asp:TextBox ID="txtSearchEndDateS" runat="server" Columns="8" />
                        <cc1:CalendarExtender ID="ceSDS" TargetControlID="txtSearchEndDateS" runat="server" Format="yyyy/MM/dd">
                        </cc1:CalendarExtender>
                        至
                        <asp:TextBox ID="txtSearchEndDateE" runat="server" Columns="8" />
                        <cc1:CalendarExtender ID="ceSDE" TargetControlID="txtSearchEndDateE" runat="server" Format="yyyy/MM/dd">
                        </cc1:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnSearch" runat="server" Text="搜尋" OnClick="btnSearch_Click" />&nbsp;
                        <asp:Button ID="btnAdd" runat="server" Text="新增" OnClick="btnAdd_Click" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <br />
    <asp:Panel ID="divDiscountEventEdit" runat="server" Visible="false">
        <fieldset>
            <legend>折價券主題編輯</legend>
            <table>
                <tr>
                    <td style="width: 100px">名稱
                    </td>
                    <td>
                        <asp:HiddenField ID="hiId" runat="server" />
                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>活動時間
                    </td>
                    <td>
                        <asp:TextBox ID="txtStartDate" runat="server" Columns="8" />
                        <asp:DropDownList ID="ddlStartTime" runat="server"></asp:DropDownList>&nbsp;:
                        <asp:DropDownList ID="ddlStartMin" runat="server"></asp:DropDownList>
                        <cc1:CalendarExtender ID="ceSD" TargetControlID="txtStartDate" runat="server" Format="yyyy/MM/dd">
                        </cc1:CalendarExtender>
                        至
                    <asp:TextBox ID="txtEndDate" runat="server" Columns="8" />
                        <asp:DropDownList ID="ddlEndTime" runat="server"></asp:DropDownList>&nbsp;:
                        <asp:DropDownList ID="ddlEndMin" runat="server"></asp:DropDownList>
                        <cc1:CalendarExtender ID="ceED" TargetControlID="txtEndDate" runat="server" Format="yyyy/MM/dd">
                        </cc1:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td>類型
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rdlEventType" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" onchange="return CheckType();"></asp:RadioButtonList>
                        
                    </td>
                </tr>
                <tr>
                    <td>狀態
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rblStatus" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal"></asp:RadioButtonList>
                    </td>
                </tr>
                <tr id="trDiscountUser">
                    <td>折價券名單
                    </td>
                    <td>
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                        <asp:Button ID="btnImport" runat="server" Text="匯入" OnClick="btnImport_Click" />
                        <asp:Button ID="btnImportAllMembers" runat="server" Text="匯入全會員" OnClick="btnImportAllMembers_Click" />
                        &nbsp;&nbsp;&nbsp;<br />
                    </td>
                </tr>
                <tr id="trDiscountEventCode">
                    <td>
                        代碼:
                    </td>
                    <td>
                        <asp:Label ID="lblEventCode" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr id="trResult">
                    <td colspan="2">
                        已匯入:
                            <asp:Label ID="lblDiscountUsersCount" runat="server" ForeColor="Green" Font-Bold="true"></asp:Label>
                        筆；
                            已領取:
                            <asp:Label ID="lblDiscountCodeCount" runat="server" ForeColor="Blue" Font-Bold="true"></asp:Label>
                        筆；
                            不足:
                            <asp:Label ID="lblDiscountShortageCount" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                        筆
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnSave" runat="server" Text="儲存" OnClick="btnSave_Click" OnClientClick="return CheckData();" />&nbsp;
                        <asp:Button ID="btnClose" runat="server" Text="取消" OnClick="btnClose_Click" />
                        <asp:Button ID="btnUpdateCampaignExpireTime" runat="server" Text="更新折價券使用期限" Visible="false" ForeColor="Red" OnClick="btnUpdateCampaignExpireTime_Click" OnClientClick="return confirm('確定要更新折價券使用期限?');" />
                    </td>
                </tr>
            </table>
            <br />
            <asp:HyperLink ID="hkDiscountCampaignAdd" runat="server" Text="新增折價券"></asp:HyperLink>
            <asp:GridView ID="gvDiscountCampaign" runat="server" OnRowDataBound="gvDiscountCampaign_RowDataBound"
                OnRowCommand="gvDiscountCampaign_RowCommand" GridLines="Horizontal" ForeColor="Black" CellPadding="4"
                BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料"
                AutoGenerateColumns="false" Font-Size="Small" Width="1024px">
                <FooterStyle BackColor="#CCCC99"></FooterStyle>
                <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
                <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
                <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                    HorizontalAlign="Center"></HeaderStyle>
                <Columns>
                    <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                        <ItemTemplate>
                            <asp:Label ID="lblId" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="折價券活動名稱" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                        <ItemTemplate>
                            <asp:HyperLink ID="hkName" runat="server" Target="_blank"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="使用期限" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                        <ItemTemplate>
                            <asp:Label ID="lblEventDateS" runat="server"></asp:Label>
                            &nbsp;~&nbsp;
                            <asp:Label ID="lblEventDateE" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="面額" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                        <ItemTemplate>
                            $<asp:Label ID="lblAmount" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="每人張數" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7%">
                        <ItemTemplate>
                            <asp:Label ID="lblQty" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="總數" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7%">
                        <ItemTemplate>
                            <asp:Label ID="lblCampaignQty" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="使用門檻" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7%">
                        <ItemTemplate>
                            <asp:Label ID="lblMinimumAmount" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="刪除" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                        <ItemTemplate>
                            <asp:Button ID="btnDelete" runat="server" Text="刪除" CommandName="DEL" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Label ID="lblTotalAmount" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
        </fieldset>
    </asp:Panel>
    <asp:Panel ID="divDiscountEventList" runat="server" Visible="false">
        <asp:GridView ID="gvDiscountEvent" runat="server" OnRowCommand="gvDiscountEvent_RowCommand" OnRowDataBound="gvDiscountEvent_RowDataBound"
            GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None"
            BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="false"
            Font-Size="Small" Width="1029px">
            <FooterStyle BackColor="#CCCC99"></FooterStyle>
            <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
            <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                HorizontalAlign="Center"></HeaderStyle>
            <Columns>
                <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="2%">
                    <ItemTemplate>
                        <asp:Label ID="lblEventId" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="主題名稱" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                    <ItemTemplate>
                        <asp:LinkButton ID="lkEventName" runat="server" CommandName="UPD"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="主題活動時間" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                    <ItemTemplate>
                        <asp:Label ID="lblEventDateS" runat="server"></asp:Label>
                        &nbsp;~&nbsp;
                        <asp:Label ID="lblEventDateE" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="狀態" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="Red"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="異動日期" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                    <ItemTemplate>
                        <asp:Label ID="lblModifyTime" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="異動人員" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblModifyId" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <uc1:Pager ID="ucPager" runat="server" PageSize="10" ongetcount="GetEventCount" onupdate="EventUpdateHandler"></uc1:Pager>
    </asp:Panel>
</asp:Content>

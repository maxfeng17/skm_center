﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="campaign_add.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.discount.campaign_add" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
        //google.load("jquery", "1.7.2");
        google.load("jqueryui", "1.8");
    </script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui-timepicker-addon.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <script type="text/javascript" language="javascript">
        function $$(id, context) {
            var el = $("#" + id, context);
            if (el.length < 1)
                el = $("*[id$=" + id + "]", context);
            return el;
        }

        function checkData() {
            if ($("#txtCustomCode").val()) {
                var regex = new RegExp(/[\W_]+/);
                if (regex.test($("#txtCustomCode").val())) {
                    $("#errorCustomCode").show();
                    $("#errorCustomCode").attr("style", "color:red;");
                    return false;
                } else {
                    $("#errorCustomCode").hide();
                    return true;
                }
            }
            return true;
        }

        function CheckFreeGiftWithPurchase() {
            if ($('[id*=rbNoFreeGift]').is(':checked')) {
                // 無
                $('[id*=txtFreeGiftDiscount]').val('');
                $('[id*=txtFreeGiftDiscount]').attr('disabled', 'disabled');
                $('[id*=chkTakeCeiling]').attr('checked', '');
                $('[id*=chkTakeCeiling]').attr('disabled', 'disabled');
                $('[id*=txtEventDateS]').val('');
                $('[id*=txtEventDateE]').val('');
                $('#limitConditions').hide();
                $('#eventDate').hide();
            } else if ($('[id*=rbFreeGiftWithPurchase]').is(":checked")) {
                // 贈送等值折價券
                $('#limitConditions').show();
                $('#eventDate').show();
                $('#TakeCeiling').show();
                $('[id*=txtFreeGiftDiscount]').removeAttr('disabled');
                $('[id*=chkTakeCeiling]').removeAttr('disabled');
            }
        }

        $(document).ready(function () {
            $$('txtSTime').datepicker({ dateFormat: 'yy/mm/dd', stepMinute: 1 });
            $$('txtETime').datepicker({ dateFormat: 'yy/mm/dd', stepMinute: 1 });

            $$('txtEventDateS').datepicker({ dateFormat: 'yy/mm/dd', stepMinute: 1 });
            $$('txtEventDateE').datepicker({ dateFormat: 'yy/mm/dd', stepMinute: 1 });

            if ($(".singleKey").find('input[type=checkbox]').is(':checked')) {
                if (!$("#<%=cbx_MultipleAdd.ClientID%>").is(':checked')) {
                    $("#customCode").show();
                }
                $('[id*=chkOwnerUseOnly]').attr('checked', false);
                $('[id*=chkOwnerUseOnly]').attr('disabled', 'disabled');
            } else {
                $('[id*=chkOwnerUseOnly]').removeAttr('disabled');
            }

            if ($('[id*=chkOwnerUseOnly]').is(':checked')) {
                $('.singleKey').find('input[type=checkbox]').attr('checked', false);
                $('.singleKey').find('input[type=checkbox]').attr('disabled', 'disabled');
            } else {
                $('.singleKey').find('input[type=checkbox]').removeAttr('disabled');
            }

            CheckFreeGiftWithPurchase();

            $(".singleKey").find('input[type=checkbox]').live('change', function () {
                if ($(this).is(':checked') && !$("#<%=cbx_MultipleAdd.ClientID%>").is(':checked')) {
                    $("#customCode").show();
                } else {
                    $("#txtCustomCode").val("");
                    $("#errorCustomCode").hide();
                    $("#customCode").hide();
                }

                if ($(this).is(':checked')) {
                    $('[id*=chkOwnerUseOnly]').attr('checked', false);
                    $('[id*=chkOwnerUseOnly]').attr('disabled', 'disabled');
                } else {
                    $('[id*=chkOwnerUseOnly]').removeAttr('disabled');
                }
            });

            $('[id*=chkOwnerUseOnly]').live('change', function () {
                if ($(this).is(':checked')) {
                    $('.singleKey').find('input[type=checkbox]').attr('checked', false);
                    $('.singleKey').find('input[type=checkbox]').attr('disabled', 'disabled');
                } else {
                    $('.singleKey').find('input[type=checkbox]').removeAttr('disabled');
                }
            });

            $("#<%=cbx_MultipleAdd.ClientID%>").live('change', function () {
                if ($(this).is(':checked')) {
                    $("#txtCustomCode").val("");
                    $("#errorCustomCode").hide();
                    $("#customCode").hide();
                } else if (!$(this).is(':checked') && $(".singleKey").find('input[type=checkbox]').is(':checked')) {
                    $("#customCode").show();
                }
            });

            $("#txtCustomCode").live("keyup", function () {
                if ($(this).val()) {
                    var regex = new RegExp(/[\W_]+/);
                    if (regex.test($(this).val())) {
                        $("#errorCustomCode").show();
                    } else {
                        $("#errorCustomCode").hide();
                    }
                } else {
                    $("#errorCustomCode").hide();
                }

            });

            $('[id*=chkVISA]').live('change', function () {
                if ($(this).is(':checked')) {
                    $('[id*=chkOwnerUseOnly]').attr('checked', false);
                    $('[id*=chkOwnerUseOnly]').attr('disabled', 'disabled');
                } else {
                    $('[id*=chkOwnerUseOnly]').removeAttr('disabled');
                }
            });

            $('[name*=DiscountGift]').bind("click", function () {
                CheckFreeGiftWithPurchase();
            });
        });

        function selectChannel(obj) {

            if ($(obj).is(':checked')) {
                $(obj).parent().next('td').find('.ulArea').show();
                $(obj).parent().next('td').find('.ulCategory').show();
                $('[id*=cbx_BindEventPromoId]').attr('disabled', 'disabled');
            } else {
                $(obj).parent().next('td').find('.ulArea').hide();
                $(obj).parent().next('td').find('.ulCategory').hide();

                $(obj).parent().next('td').find('.ulArea input[type=checkbox]').each(function () {
                    $(this).next('label').css('color', 'black');
                    $(this).removeAttr('disabled');
                    $(this).attr('checked', false);
                });

                $(obj).parent().next('td').find('.ulCategory input[type=checkbox]').each(function () {
                    $(this).next('label').css('color', 'black');
                    $(this).removeAttr('disabled');
                    $(this).attr('checked', false);
                });
                $('[id*=cbx_BindEventPromoId]').removeAttr('disabled');
            }
        }

        function selectArea(obj) {

            if ($(obj).parent().parent('.ulArea').find('input[type=checkbox]:checked').length > 0) {
                $(obj).parent().parent().parent().find('.ulCategory input[type=checkbox]').each(function () {
                    $(this).attr('checked', false);
                    $(this).attr('disabled', 'disabled');
                    $(this).next('label').css('color', 'gray');
                });
            } else {
                $(obj).parent().parent().parent().find('.ulCategory input[type=checkbox]').each(function () {
                    $(this).removeAttr('disabled');
                    $(this).next('label').css('color', 'black');
                });
            }
        }

        function selectCategory(obj) {

            if ($(obj).parent().parent('.ulCategory').find('input[type=checkbox]:checked').length > 0) {
                $(obj).parent().parent().parent().find('.ulArea input[type=checkbox]').each(function () {
                    $(this).attr('checked', false);
                    $(this).attr('disabled', 'disabled');
                    $(this).next('label').css('color', 'gray');
                });
            } else {
                $(obj).parent().parent().parent().find('.ulArea input[type=checkbox]').each(function () {
                    $(this).removeAttr('disabled');
                    $(this).next('label').css('color', 'black');
                });
            }
        }

        function checkBindEvent() {
            if($('#<%= cbx_BindBrandId.ClientID%>').is(':checked') || $('#<%= cbx_BindEventPromoId.ClientID%>').is(':checked')) {
                $('#category').hide();
            } else {
                $('#category').show();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        body {
            font-size: 10px;
        }

        .ui-timepicker-div .ui-widget-header {
            margin-bottom: 8px;
        }

        .ui-timepicker-div dl {
            text-align: left;
        }

            .ui-timepicker-div dl dt {
                height: 25px;
            }

            .ui-timepicker-div dl dd {
                margin: -25px 0 10px 65px;
            }

        .ui-timepicker-div td {
            font-size: 90%;
        }

        label.error {
            float: none;
            color: red;
            padding-left: .5em;
            vertical-align: top;
        }

        .style1 {
            width: 414px;
        }

        .auto-style1 {
            height: 88px;
        }
    </style>
    <a href="campaignmain.aspx">回總覽</a>
    <table>
        <tr>
            <td>活動名稱（申請原因）：<asp:TextBox ID="txtName" runat="server" Width="267px" MaxLength="20"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>有效期限：
                <asp:TextBox ID="txtSTime" runat="server"></asp:TextBox>&nbsp;&nbsp;
                <asp:DropDownList ID="ddlSTimeHour" runat="server" DataTextField="Text" DataValueField="Value" />:<asp:DropDownList ID="ddlSTimeMinute" runat="server" DataTextField="Text" DataValueField="Value" />
                ～
                <asp:TextBox ID="txtETime" runat="server"></asp:TextBox>&nbsp;&nbsp;
                <asp:DropDownList ID="ddlETimeHour" runat="server" DataTextField="Text" DataValueField="Value" />:<asp:DropDownList ID="ddlETimeMinute" runat="server" DataTextField="Text" DataValueField="Value" />
                <br />
                <asp:CheckBox ID="cbx_MultipleAdd" runat="server" Visible="false" Text="時間區間內，每日產生一個折價券活動，使用期限為當日"
                    ForeColor="Red" />
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>面額
                        </td>
                        <td>數量
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbten" runat="server" GroupName="amount" Checked="true" />10
                        </td>
                        <td>
                            <asp:TextBox ID="txttexqty" runat="server"></asp:TextBox>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbfifty" runat="server" GroupName="amount" />50
                        </td>
                        <td>
                            <asp:TextBox ID="txtfiftyqty" runat="server"></asp:TextBox>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbcust" runat="server" GroupName="amount" /><asp:TextBox ID="txtcust"
                                runat="server" Width="38px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="txtcustqty" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">
                <table>
                    <tr>
                        <td>序號類型：
                        </td>
                        <td>
                            <asp:CheckBoxList ID="chklType" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                <asp:ListItem Text="即時產生折價券" Value="16"></asp:ListItem>
                                <asp:ListItem Text="統一序號" Value="64" class="singleKey"></asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr id="customCode" style="display: none">
                        <td valign="top">自定序號：</td>
                        <td>
                            <asp:TextBox ID="txtCustomCode" ClientIDMode="static" runat="server" placeholder="請輸入活動短碼"></asp:TextBox>
                            <span id="errorCustomCode" class="ui-state-highlight ActivityUrlError" style="display: none">短碼格式有誤，不支援符號、空格及全型字母</span>
                            <br />
                            <span id="Span1" class="ui-state-highlight ActivityUrlError">若無輸入，則由系統自動產生統一序號。</span>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">領取條件：</td>
                        <td>
                            <asp:RadioButton ID="rbNoFreeGift" runat="server" Text="無" GroupName="DiscountGift" Checked="true" />
                            <br />
                            <asp:RadioButton ID="rbFreeGiftWithPurchase" runat="server" Text="每消費" GroupName="DiscountGift" />
                            <asp:TextBox ID="txtFreeGiftDiscount" runat="server" Enabled="false" Width="60px"></asp:TextBox>元，即贈送等值折價券
                            <br />
                            <asp:RadioButton ID="rbAutoSend" runat="server" Text="使用自動再送一張" GroupName="DiscountGift"  />
                            <br />
                            <table id="limitConditions" style="display: none; padding-left: 20px;">
                                <tr>
                                    <td style="padding: 10px 10px 10px 10px; background-color: lightyellow;">
                                        <span id="TakeCeiling" style="display: none;">
                                            <asp:CheckBox ID="chkTakeCeiling" runat="server" Text="無條件進位" />
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="eventDate" style="display: none;">
                        <td>活動期間：
                        </td>
                        <td>
                            <asp:TextBox ID="txtEventDateS" runat="server"></asp:TextBox>&nbsp;&nbsp;
                            <asp:DropDownList ID="ddlEventDateSHour" runat="server" DataTextField="Text" DataValueField="Value" />:<asp:DropDownList ID="ddlEventDateSMinute" runat="server" DataTextField="Text" DataValueField="Value" />
                            ～
                            <asp:TextBox ID="txtEventDateE" runat="server"></asp:TextBox>&nbsp;&nbsp;
                            <asp:DropDownList ID="ddlEventDateEHour" runat="server" DataTextField="Text" DataValueField="Value" />:<asp:DropDownList ID="ddlEventDateEMinute" runat="server" DataTextField="Text" DataValueField="Value" />
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="4" valign="top">使用限制：
                        </td>
                        <td valign="top">
                            <asp:CheckBox ID="cbx_MinimumAmount" runat="server" Text="限制最低訂單金額" />
                            :<asp:TextBox ID="tbx_MinimumAmount" runat="server"></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkOwnerUseOnly" runat="server" Text="限本人使用" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkVISA" runat="server" Text="VISA獨享" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="cbx_BindEventPromoId" runat="server" Text="限制使用商品主題活動檔次" onclick="checkBindEvent();" />
                                                <asp:TextBox ID="tbx_EventPromoId" runat="server"></asp:TextBox>
                                                <asp:Button ID="btnAddEventPromo" OnClick="btnAddEventPromo_Click" Text="新增" runat="server" />
                                                &nbsp;<span class="ui-state-highlight ActivityUrlError">請輸入商品主題頁ID或檔次BID</span>                                    
                                                <asp:GridView ID="gv_BindEventPromo" runat="server" AutoGenerateColumns="False" OnRowDataBound="gv_BindEventPromo_RowDataBound" OnRowCommand="gv_BindEventPromo_RowCommand">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="刪除">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:ImageButton ImageAlign="AbsBottom" CommandName="DeleteEventPromoId" ID="imgBtnDelete" ImageUrl="~/Images/icons/delete.gif" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="ID" DataField="Id" />
                                                        <asp:TemplateField HeaderText="商品主題活動名稱">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="lkPreview" runat="server" Target="_blank" Text='<%# Eval("Title") %>'></asp:HyperLink>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="cbx_BindBrandId" runat="server" Text="限制使用策展檔次" onclick="checkBindEvent();" />
                                                <asp:TextBox ID="tbx_BrandId" runat="server"></asp:TextBox>
                                                <asp:Button ID="btnAddBrand" OnClick="btnAddBrand_Click" Text="新增" runat="server" />
                                                &nbsp;<span class="ui-state-highlight ActivityUrlError">請輸入策展BrandID</span>
                                                <br /><br />
                                                <asp:GridView ID="gv_BindBrand" runat="server" AutoGenerateColumns="False" OnRowDataBound="gv_BindBrand_RowDataBound" OnRowCommand="gv_BindBrand_RowCommand">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="刪除">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:ImageButton ImageAlign="AbsBottom" CommandName="DeleteBrandId" ID="imgBtnDelete" ImageUrl="~/Images/icons/delete.gif" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="ID" DataField="Id" />
                                                        <asp:TemplateField HeaderText="策展活動名稱">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="lkPreview" runat="server" Target="_blank" Text='<%# Eval("BrandName") %>'></asp:HyperLink>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_BindEventPromoMsg" runat="server" ForeColor="Red" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">裝置限制：
                        </td>
                        <td>
                            <asp:CheckBox ID="chkAppUseOnly" runat="server" Text="限APP可使用" />
                        </td>
                    </tr>
                    <tr id="category">
                        <td valign="top">館別(分類)限制：
                        </td>
                        <td>
                            <asp:Repeater ID="rptChannel" runat="server" OnItemDataBound="rptChannel_ItemDataBound">
                                <HeaderTemplate>
                                    <table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td valign="top">
                                            <asp:HiddenField ID="hidChannel" runat="server" />
                                            <asp:CheckBox ID="chkChannel" runat="server" onclick="selectChannel(this);" />
                                        </td>
                                        <td>
                                            <asp:Repeater ID="rptArea" runat="server" OnItemDataBound="rptArea_ItemDataBound">
                                                <HeaderTemplate>
                                                    <ul class="ulArea" style="display: none;">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <li style="display: inline">
                                                        <asp:HiddenField ID="hidArea" runat="server" />
                                                        <asp:CheckBox ID="chkArea" runat="server" onclick="selectArea(this);" />
                                                    </li>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </ul>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            <asp:Repeater ID="rptCategory" runat="server" OnItemDataBound="rptCategory_ItemDataBound">
                                                <HeaderTemplate>
                                                    <ul class="ulCategory" style="display: none;">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <li style="display: inline">
                                                        <asp:HiddenField ID="hidCategory" runat="server" />
                                                        <asp:CheckBox ID="chkCategory" runat="server" onclick="selectCategory(this);" />
                                                    </li>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </ul>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label><br />
                <asp:Button ID="btnSend" OnClientClick="return checkData();" runat="server" Text="送出申請" OnClick="btnSend_Click" />
            </td>
        </tr>
    </table>
</asp:Content>

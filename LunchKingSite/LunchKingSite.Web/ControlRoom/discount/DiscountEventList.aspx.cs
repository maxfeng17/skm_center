﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.discount
{
    public partial class DiscountEventList : RolePage, IDiscountEventListView
    {
        private ISysConfProvider _config = ProviderFactory.Instance().GetConfig();

        #region props
        private DiscountEventListPresenter _presenter;
        public DiscountEventListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public string SearchName
        {
            get
            {
                return txtSearchName.Text;
            }
        }

        public string SearchEventCode
        {
            get
            {
                return txtSearchEventCode.Text;
            }
        }

        public DateTime SearchEndDateS
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtSearchEndDateS.Text, out date);
                return date;
            }
        }

        public DateTime SearchEndDateE
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtSearchEndDateE.Text, out date);
                return date;
            }
        }

        public int PageSize
        {
            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }
        }

        public int CurrentPage
        {
            get
            {
                return ucPager.CurrentPage;
            }
        }

        public int EventId
        {
            get
            {
                int id = int.TryParse(hiId.Value, out id) ? id : 0;
                return id;
            }
        }

        public int Eid
        {
            get
            {
                int eid = 0;
                if (Request.QueryString["eid"] != null)
                {
                    int.TryParse(Request.QueryString["eid"].ToString(), out eid);
                }
                return eid;
            }
        }

        public string EventName
        {
            get
            {
                return txtName.Text;
            }
        }

        public DateTime StartTime
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtStartDate.Text, out date);
                int hour = int.TryParse(ddlStartTime.SelectedValue, out hour) ? hour : 0;
                int min = int.TryParse(ddlStartMin.SelectedValue, out min) ? min : 0;
                return new DateTime(date.Year, date.Month, date.Day, hour, min, 0);
            }
        }

        public DateTime EndTime
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtEndDate.Text, out date);
                int hour = int.TryParse(ddlEndTime.SelectedValue, out hour) ? hour : 0;
                int min = int.TryParse(ddlEndMin.SelectedValue, out min) ? min : 0;
                return new DateTime(date.Year, date.Month, date.Day, hour, min, 0);
            }
        }

        public DiscountEventType Type
        {
            get
            {
                DiscountEventType type = DiscountEventType.ImportUsers;
                DiscountEventType.TryParse(rdlEventType.SelectedValue, out type);
                return type;
            }
        }

        public DiscountEventStatus Status
        {
            get
            {
                DiscountEventStatus status = DiscountEventStatus.Initial;
                DiscountEventStatus.TryParse(rblStatus.SelectedValue, out status);
                return status;
            }
        }

        #endregion

        #region event
        public event EventHandler Search;
        public event EventHandler<DataEventArgs<int>> Save;
        public event EventHandler<DataEventArgs<int>> DiscountEventGet;
        public event EventHandler<DataEventArgs<int>> DiscountEventGetCount;
        public event EventHandler<DataEventArgs<int>> DiscountEventPageChanged;
        public event EventHandler<DataEventArgs<int>> DiscountEventCampaignDelete;
        public event EventHandler<DataEventArgs<List<int>>> DiscountUsersImport;
        public event EventHandler ImportAllMembers;
        public event EventHandler UpdateCampaignExpireTime;
        public event EventHandler GetDiscountForImportedUsers;
        #endregion

        #region method
        public void SetEventList(DiscountEventCollection eventList)
        {
            divDiscountEventList.Visible = true;
            divDiscountEventEdit.Visible = false;
            gvDiscountEvent.DataSource = eventList;
            gvDiscountEvent.DataBind();
        }

        public void SetEventEdit(DiscountEvent item, ViewDiscountEventCampaignCollection campaignList, int usersCount, int sentQty, int shortageQty)
        {
            divDiscountEventList.Visible = false;
            divDiscountEventEdit.Visible = true;
            if (item.StartTime < DateTime.Now)
            {
                rdlEventType.Enabled = false;
            }
            else
            {
                rdlEventType.Enabled = true;
            }
            if (item.Type == (int)DiscountEventType.ImportUsers)
            {
                lblEventCode.Text = string.Empty;
            }
            else if (item.Type == (int)DiscountEventType.EventCode)
            {
                lblEventCode.Text = item.EventCode + "-" + item.Code;
            }
            rdlEventType.SelectedValue = item.Type.ToString();
            hiId.Value = item.Id.ToString();
            txtName.Text = item.Name;

            txtStartDate.Text = item.StartTime.ToShortDateString();
            ddlStartTime.SelectedValue = string.Format("{0:00}", item.StartTime.Hour);
            ddlEndMin.SelectedValue = string.Format("{0:00}", item.StartTime.Minute);
            txtStartDate.Enabled = ddlStartTime.Enabled = ddlEndMin.Enabled = item.StartTime > DateTime.Now;
            txtEndDate.Text = item.EndTime.ToShortDateString();
            ddlEndTime.SelectedValue = string.Format("{0:00}", item.EndTime.Hour);
            ddlEndMin.SelectedValue = string.Format("{0:00}", item.EndTime.Minute);
            rblStatus.SelectedValue = item.Status.ToString();
            if (item.StartTime > DateTime.Now)
            {
                hkDiscountCampaignAdd.Visible = true;
                hkDiscountCampaignAdd.NavigateUrl = ResolveUrl("~/ControlRoom/discount/DiscountEventCampaignSet.aspx?eid=" + item.Id);
            }
            else
            {
                hkDiscountCampaignAdd.Visible = false;
            }

            lblDiscountUsersCount.Text = usersCount.ToString();
            lblDiscountCodeCount.Text = sentQty.ToString();
            lblDiscountShortageCount.Text = shortageQty.ToString();

            gvDiscountCampaign.Visible = true;
            gvDiscountCampaign.DataSource = campaignList;
            gvDiscountCampaign.DataBind();
            lblTotalAmount.Text = string.Format("每人發送 ${0} 元", campaignList.Sum(x => x.Amount * (x.EventQty ?? 0)).Value.ToString("N0"));

            if (CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.UpdateExpireDate) &&
                campaignList.Any(x => x.EndTime < item.EndTime || x.StartTime > item.StartTime))
            {
                btnUpdateCampaignExpireTime.Visible = true;
            }
            else
            {
                btnUpdateCampaignExpireTime.Visible = false;
            }
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IntialControls();
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.Search != null)
            {
                this.Search(this, e);
            }
            ucPager.ResolvePagerView(CurrentPage, true);
        }

        protected void gvDiscountEvent_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is DiscountEvent)
            {
                DiscountEvent item = (DiscountEvent)e.Row.DataItem;
                Label lblEventId = ((Label)e.Row.FindControl("lblEventId"));
                LinkButton lkEventName = ((LinkButton)e.Row.FindControl("lkEventName"));
                Label lblEventDateS = ((Label)e.Row.FindControl("lblEventDateS"));
                Label lblEventDateE = ((Label)e.Row.FindControl("lblEventDateE"));
                Label lblStatus = ((Label)e.Row.FindControl("lblStatus"));
                Label lblModifyTime = ((Label)e.Row.FindControl("lblModifyTime"));
                Label lblModifyId = ((Label)e.Row.FindControl("lblModifyId"));
                lblEventId.Text = item.Id.ToString();
                lkEventName.Text = item.Name;
                lkEventName.CommandArgument = item.Id.ToString();
                lblEventDateS.Text = item.StartTime.ToString("yyyy/MM/dd HH:mm");
                lblEventDateE.Text = item.EndTime.ToString("yyyy/MM/dd HH:mm");
                DiscountEventStatus status = (DiscountEventStatus)item.Status;
                if (status != DiscountEventStatus.Initial)
                {
                    lblStatus.Text = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, status);
                }
                lblModifyTime.Text = item.ModifyTime != null ? item.ModifyTime.Value.ToString("yyyy/MM/dd HH:mm") : string.Empty;
                lblModifyId.Text = item.ModifyId;
                if (item.StartTime < DateTime.Now && item.EndTime > DateTime.Now && item.Status != (int)DiscountEventStatus.Disabled)
                {
                    e.Row.BackColor = System.Drawing.Color.Pink;
                }
            }
        }

        protected void gvDiscountEvent_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "UPD":
                    if (this.DiscountEventGet != null)
                    {
                        int id = int.TryParse(e.CommandArgument.ToString(), out id) ? id : 0;
                        this.DiscountEventGet(this, new DataEventArgs<int>(id));
                    }
                    break;
                default:
                    break;
            }
        }

        protected void gvDiscountCampaign_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is ViewDiscountEventCampaign)
            {
                ViewDiscountEventCampaign item = (ViewDiscountEventCampaign)e.Row.DataItem;
                Label lblId = (Label)e.Row.FindControl("lblId");
                HyperLink hkName = (HyperLink)e.Row.FindControl("hkName");
                Label lblEventDateS = (Label)e.Row.FindControl("lblEventDateS");
                Label lblEventDateE = (Label)e.Row.FindControl("lblEventDateE");
                Label lblAmount = (Label)e.Row.FindControl("lblAmount");
                Label lblQty = (Label)e.Row.FindControl("lblQty");
                Label lblCampaignQty = (Label)e.Row.FindControl("lblCampaignQty");
                Label lblMinimumAmount = (Label)e.Row.FindControl("lblMinimumAmount");
                Button btnDelete = (Button)e.Row.FindControl("btnDelete");

                lblId.Text = item.CampaignId.ToString();
                hkName.Text = item.CampaignName;
                hkName.NavigateUrl = ResolveUrl("~/ControlRoom/discount/discount_detail.aspx?cid=" + item.CampaignId);
                lblEventDateS.Text = item.StartTime != null ? item.StartTime.Value.ToString("yyyy/MM/dd/ HH:mm") : string.Empty;
                if (item.StartTime > StartTime)
                {
                    lblEventDateS.ForeColor = System.Drawing.Color.Red;
                }
                lblEventDateE.Text = item.EndTime != null ? item.EndTime.Value.ToString("yyyy/MM/dd/ HH:mm") : string.Empty;
                if (item.EndTime < EndTime)
                {
                    lblEventDateE.ForeColor = System.Drawing.Color.Red;
                }
                lblAmount.Text = (item.Amount ?? 0).ToString("N0");
                lblQty.Text = (item.EventQty ?? 0).ToString();
                lblCampaignQty.Text = (item.CampaignQty ?? 0).ToString();
                lblMinimumAmount.Text = item.MinimumAmount != null ? "滿$" + item.MinimumAmount.Value.ToString() : "無";
                if (item.StartTime > DateTime.Now)
                {
                    btnDelete.CommandArgument = (item.Id ?? 0).ToString();
                }
                else
                {
                    btnDelete.Text = "活動已開始";
                    btnDelete.Enabled = false;
                }
            }
        }

        protected void gvDiscountCampaign_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "DEL":
                    if (this.DiscountEventGet != null)
                    {
                        int id = int.TryParse(e.CommandArgument.ToString(), out id) ? id : 0;
                        this.DiscountEventCampaignDelete(this, new DataEventArgs<int>(id));
                    }
                    break;
                default:
                    break;
            }
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                List<int> users = new List<int>();
                using (StreamReader reader = new StreamReader(FileUpload1.FileContent))
                {
                    do
                    {
                        string textLine = reader.ReadLine();
                        int userId = int.TryParse(textLine, out userId) ? userId : 0;
                        users.Add(userId);
                    } while (reader.Peek() != -1);
                    reader.Close();
                }
                if (this.DiscountUsersImport != null)
                {
                    this.DiscountUsersImport(this, new DataEventArgs<List<int>>(users));
                }
            }
        }

        protected void btnImportAllMembers_Click(object sender, EventArgs e)
        {
            if (this.ImportAllMembers != null)
            {
                this.ImportAllMembers(this, e);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Save != null)
            {
                int id = int.TryParse(hiId.Value, out id) ? id : 0;
                this.Save(this, new DataEventArgs<int>(id));
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            divDiscountEventEdit.Visible = true;
            divDiscountEventList.Visible = false;
            hiId.Value = string.Empty;
            txtName.Text = string.Empty;
            txtStartDate.Text = string.Empty;
            txtEndDate.Text = string.Empty;
            rblStatus.SelectedIndex = 0;
            rdlEventType.SelectedIndex = 0;
            lblDiscountUsersCount.Text = "0";
            gvDiscountCampaign.Visible = false;
            lblTotalAmount.Text = string.Empty;
            hkDiscountCampaignAdd.Visible = false;
            rdlEventType.Enabled = txtStartDate.Enabled = ddlStartTime.Enabled = ddlStartMin.Enabled = true;
            lblEventCode.Text = string.Empty;
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            divDiscountEventEdit.Visible = false;
            divDiscountEventList.Visible = true;
        }

        protected void btnUpdateCampaignExpireTime_Click(object sender, EventArgs e)
        {
            if (this.UpdateCampaignExpireTime != null)
            {
                this.UpdateCampaignExpireTime(this, e);
            }
        }

        protected void EventUpdateHandler(int pageNumber)
        {
            if (this.DiscountEventPageChanged != null)
            {
                this.DiscountEventPageChanged(this, new DataEventArgs<int>(pageNumber));
            }
        }

        protected int GetEventCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (DiscountEventGetCount != null)
            {
                DiscountEventGetCount(this, e);
            }
            return e.Data;
        }
        #endregion

        #region private method
        private void IntialControls()
        {
            txtSearchEndDateS.Text = DateTime.Now.Date.ToShortDateString();
            txtSearchEndDateE.Text = DateTime.Now.Date.AddMonths(3).ToShortDateString();

            Dictionary<int, string> status = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(DiscountEventStatus)))
            {
                status[(int)item] = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (DiscountEventStatus)item);
            }

            rblStatus.DataSource = status;
            rblStatus.DataTextField = "Value";
            rblStatus.DataValueField = "Key";
            rblStatus.DataBind();
            rblStatus.SelectedIndex = 0;

            Dictionary<int, string> type = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(DiscountEventType)))
            {
                type[(int)item] = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (DiscountEventType)item);
            }

            rdlEventType.DataSource = type;
            rdlEventType.DataTextField = "Value";
            rdlEventType.DataValueField = "Key";
            rdlEventType.DataBind();
            rdlEventType.SelectedIndex = 0;

            List<ListItem> hours = new List<ListItem>();
            for (int i = 0; i < 24; i++)
            {
                string hour = ("00" + i).Substring(("00" + i).Length - 2, 2);
                hours.Add(new ListItem(hour, hour));
            }

            ddlStartTime.DataSource = hours;
            ddlStartTime.DataBind();
            ddlStartTime.SelectedValue = "00";

            ddlEndTime.DataSource = hours;
            ddlEndTime.DataBind();
            ddlEndTime.SelectedValue = "00";

            List<ListItem> mins = new List<ListItem>();
            for (int i = 0; i < 60; i++)
            {
                string min = ("00" + i).Substring(("00" + i).Length - 2, 2);
                mins.Add(new ListItem(min, min));
            }
            ddlStartMin.DataSource = mins;
            ddlStartMin.DataBind();
            ddlStartMin.SelectedValue = "00";

            ddlEndMin.DataSource = mins;
            ddlEndMin.DataBind();
            ddlEndMin.SelectedValue = "00";
        }
        #endregion
    }
}
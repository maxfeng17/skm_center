﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="BrandSetup.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Brand.BrandSetUp" ValidateRequest="false" %>

<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/adapters/jquery.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.numeric.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.tablesorter-2.24.5.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
     <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>
    
    <style type="text/css">
        #container #tabs .ui-widget-content {
            background-color: #B3D5F0;
        }
        .bgOrange {
            background-color: #FFC690;
            text-align: center;
        }

        .bgWhite {
            background-color: White;
        }

        .bgBlue {
            background-color: #B3D5F0;
        }

        .header {
            font-size: large;
            font-weight: bolder;
            background-color: Blue;
            color: White;
        }

        .title {
            text-align: center;
            background-color: #688E45;
            font-weight: bolder;
            color: White;
        }

        .virtualPic {
            border: 1px solid black;
            background-color: white;
        }

        #tabs {
            font-size: smaller;
        }

        #multiTabs ul li a {
            padding-left: 0px;
            padding-right: 0px;
        }

        .TextboxWatermark {
            color: #ACA899;
            filter: alpha(opacity=60);
            opacity: 0.60;
        }

        .tablesorter-header {
            background-image: url("data:image/gif;base64,R0lGODlhFQAJAIAAACMtMP///yH5BAEAAAEALAAAAAAVAAkAAAIXjI+AywnaYnhUMoqt3gZXPmVg94yJVQAAOw==");
            background-position: right center;
            background-repeat: no-repeat;
            cursor: pointer;
            padding: 0px 20px 0px 4px;
            white-space: normal;
        }

        .ui-dialog-buttonset {
            text-align: center;
        }

        .itemSeqTd {
            text-align: center;
        }

        .ui-dialog .ui-dialog-buttonpane button {
            float: none;
            padding: 0;
        }
    </style>
    
    <script type="text/javascript">
        $(document).ready(function () {

            if ($('#<%=txtActBid1.ClientID%>').val() != "") {
                if ($('#<%=txtActBid2.ClientID%>').val() == "") {
                    $("#checkboxAct2").attr("checked", true);
                }
                if ($('#<%=txtActBid3.ClientID%>').val() == "") {
                    $("#checkboxAct3").attr("checked", true);
                }
                if ($('#<%=txtActBid4.ClientID%>').val() == "") {
                    $("#checkboxAct4").attr("checked", true);
                }
                if ($('#<%=txtActBid5.ClientID%>').val() == "") {
                    $("#checkboxAct5").attr("checked", true);
                }
            }

            $('#multiTabs').tabs();
            DelegateRearrange();
                        
            var selectTab = $('#<%=hif_tab.ClientID%>').val();
            if (selectTab == null) selectTab = 0;
            $('#tabs').tabs({ selected: selectTab });
            localStorage.setItem("tabsIndex", selectTab);

            $('#cblCities').find(':checkbox').each(function () {
                if ($(this).is(':checked') == false) {
                    $(this).siblings('label').css('color', 'Gray');
                }
            });
            $('#cblCities').find(':checkbox').click(function () {
                if ($(this).is(':checked')) {
                    $(this).siblings('label').css('color', 'Black');
                } else {
                    $(this).siblings('label').css('color', 'Gray');
                }
            });

            $('.itemSeqTd').find(':checkbox').click(function () {
                if ($(this).is(':checked')) {
                    $(this).siblings('label').css('color', 'Black');
                } else {
                    $(this).siblings('label').css('color', 'Gray');
                }
            });

            $('.tabs1').click(function () {
                $('#<%=hif_tab.ClientID%>').val(0);
            });

            $('.tabs2').click(function () {
                $('#<%=hif_tab.ClientID%>').val(1);
            });

            $("#divChangeCategory").dialog({
                autoOpen: false
            });
            
            var picList = $('img');
            $.each(picList, function (i, pic) {
                var delBtn = $(this).next();
                if (pic.currentSrc != '') {
                    delBtn.show(); //若有圖則顯示刪除圖片button
                } else {
                    delBtn.hide();
                }

                var hidImg = $(delBtn).next();
                hidImg.val(''); //恢復預設值
            });
        });
        
        function previewBid(obj) {
            if (obj) {
                var bid = $('#' + obj).val();
                var bidregx = new RegExp(/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i);
                if (bid.match(bidregx)) {
                    var url = '<%= SystemConfig.SiteUrl %>/' + bid.toString();
                    window.open(url, 'previewBid');
                } else {
                    alert('請輸入正確Bid');
                }
            } else {
                alert('請輸入正確Bid');
            }
        }

        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width());
            });
            return $helper;
        },
        updateIndex = function (e, ui) {
            $('td.itemSeqTd .itemSeqDiv', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);//前端顯示用
                $(this).siblings("input[id$='HidTempSeq']").val(i + 1);//後端儲存用
            });
        };

        updateIndex2 = function (e, ui) {
            $('td.itemSeqTd2 .itemSeqDiv2', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);//前端顯示用
                $(this).siblings("input[id$='HidTempSeq']").val(i + 1);//後端儲存用
            });
        };
        
        //啟動手動排序
        function EnableManuallySort() {
            //<tr>手動排序
            $(".manualSort tbody").sortable({
                helper: fixHelperModified,
                stop: updateIndex
            }).disableSelection();
            $( ".manualSort2 tbody").sortable({
                helper: fixHelperModified,
                stop: updateIndex2
            }).disableSelection();

            //表格欄位排序
            $('.manualSort').tablesorter({
                headers: {
                    0: {
                        sorter: false
                    }
                }
            });

            //為了不要吃到css，移除tablesorter-header的class(否則會輸線排序的黑色箭頭)
            $('.headerNoSort').removeClass('tablesorter-header');


            $('.manualSort').bind("sortEnd", function () {
                DelegateRearrange("");
            });
            $('.manualSort2').bind("sortEnd", function () {
                DelegateRearrange(2);
            });
        }

        //取消手動排序
        function DisableManuallySort() {
            $('.manualSort tbody').sortable('destroy');
            $('.manualSort2 tbody').sortable('destroy');
        }

        function DelegateRearrange(type) {
            var tdArray = $('td.itemSeqTd' + type);
            $.each(tdArray, function (index, obj) {
                $(obj).children('.itemSeqDiv' + type).html(index + 1);//前端顯示用
                $(obj).children("input[id$='HidTempSeq']").val(index + 1);//後端儲存用
            });
        }

        function AlertImportMsg(msg) {
            $("<div id='alertDialog'>").append(msg).dialog({
                modal: true,
                title: "執行結果",
                minWidth: 800,
                buttons: [{
                    text: "確定",
                    click: function () {
                        $(this).dialog("close");
                    }
                }],
                close: function () {
                    $(this).remove();
                }
            }).dialog("open");
        }
        
        function OnBrandItemSelected(type) {
            var itemList = $(".itemSeqTd input[type='checkbox']");
            var selectArray = [];
            $.each(itemList, function (i, item) {
                var itemId = parseInt(item.id.replace('cbx_EditCategory_', ''));

                if (type == 'all') {
                    if ($("#cbx_EditAll").is(':checked')) {
                        item.checked = true;
                        selectArray.push(itemId);
                    } else {
                        item.checked = false;
                    }
                } else {
                    if (item.checked) {
                        selectArray.push(itemId);
                    }
                    $("input#cbx_EditAll").attr('checked', false);
                }
            });

            $('#<%=hif_CheckItemList.ClientID%>').val(selectArray.toString());
        }

        function showCheckbox(obj) {
            if (obj[0].id == 'divEditCategory' || obj[0].id == 'divEditDeleteItemList') {
                $('.hide').show();
                $('.itemSeqTd').css('text-align', 'left');
            } else {
                $('.hide').hide();
                $('.itemSeqTd').css('text-align', 'center');
            }
        }

        function showDiv(objshow) {
            $('#divDefault').hide();
            $('#divAddCategory').hide();
            $('#divEditCategory').hide();
            $('#divEditDeleteItemList').hide();
            $('#divItemSort').hide();
            $('#divCategoryName').hide();
            $('#divCategorySort').hide();
            $('#tbCategoryName').hide();
            $('#divDeleteCategory').hide();
            $('#tbDeleteCategory').hide();
            $('#tbCategory').hide();
            $('#tbItem').show();
            $('#grossMarginmsg').show();            

            if (objshow[0].id != 'divItemSort') {
                DisableManuallySort();
            }
            objshow.show();
            showCheckbox(objshow);
        }

        function showCategorySort() {
            $('#divDefault').hide();
            $('#divAddCategory').hide();
            $('#divEditCategory').hide();
            $('#divEditDeleteItemList').hide();
            $('#divItemSort').hide();
            $('#divCategoryName').hide();
            $('#divDeleteCategory').hide();
            $('#tbDeleteCategory').hide();
            $('#tbItem').hide();
            $('#grossMarginmsg').hide();
            $('#tbCategoryName').hide();
            $('#divCategorySort').show();
            $('#tbCategory').show();
            EnableManuallySort();
        }

        function showCategoryName() {
            $('#divDefault').hide();
            $('#divAddCategory').hide();
            $('#divEditCategory').hide();
            $('#divEditDeleteItemList').hide();
            $('#divItemSort').hide();      
            $('#divDeleteCategory').hide();           
            $('#tbDeleteCategory').hide();
            $('#tbItem').hide();
            $('#grossMarginmsg').hide();
            $('#tbCategory').hide();
            $('#divCategoryName').show();
            $('#tbCategoryName').show();
        }

        function showDeleteCategory() {
            $('#divDefault').hide();
            $('#divAddCategory').hide();
            $('#divEditCategory').hide();
            $('#divEditDeleteItemList').hide();
            $('#divItemSort').hide();
            $('#divCategoryName').hide();
            $('#tbItem').hide();
            $('#grossMarginmsg').hide();
            $('#tbCategory').hide();
            $('#tbCategoryName').hide();
            $('#divDeleteCategory').show();
            $('#tbDeleteCategory').show();
        }

        function checkSelect(obj,mode) {
            var isSelect = false;
            var itemList = $(".itemSeqTd input[type='checkbox']");
            $.each(itemList, function (i, item) {
                if (item.checked) {
                    isSelect = true;
                }                    
            });
            
            if (isSelect) {
                var categoryName = $('#ctl00_ContentPlaceHolder1_' + obj + ' option:selected').text();
                if (mode == "R") {
                    return confirm('確定要把勾選的商品,類別標籤全部取代為"' + categoryName + '"?');
                } else if (mode == "DelItem") {
                    return confirm('確定要刪除勾選的商品清單?');
                }
            } else {
                alert('請至少勾選一商品！');
                return false;
            }
        }

        function ShowChangeCategory(bid, biId, biName, selectList) {
            var list = selectList.split("/");

            var itemList = $("#<%= cbBrandItemCategory.ClientID %> input[type='checkbox']");
            $.each(itemList, function (i, item) {
                var categoryName = $(this).next('label').text();
                item.checked = false;
                $.each(list, function (j, originalCategory) {
                    if (originalCategory == categoryName) {
                        item.checked = true;
                    }
                });
            });

            $("#divChangeCategory").dialog(
                {
                modal: true,
                title: '商品名稱：' + biName,
                buttons: [{
                    text: "取消",
                    click: function () {
                        $(this).dialog("close");
                    }
                }, 
                {
                    text: "確定",
                    click: function () {
                        var changeCategoryList = $("#<%= cbBrandItemCategory.ClientID %> input[type='checkbox']:checked").map(function () { return $(this).parent().attr('someValue'); }).get().toString();

                        $.ajax(
                            {
                                type: "POST",
                                url: "BrandSetup.aspx/ChangeItemCategory",
                                data: " { 'bid': '" + bid + "','biid': '" + biId + "','clist': '" + changeCategoryList + "'}",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",                         
                                success: function () {
                                    $('#<%=hif_tab.ClientID%>').val(1);
                                    $('#<%= btnSearchItem.ClientID %>').click();
                                    alert('商品類別更新完成');
                                }, error: function () {
                                    alert('更新失敗');
                                }
                            });
                        $(this).dialog('destroy').remove();
                    }
                }],
            }).dialog("open");
        }
        function delPic(id) {
            var objFileUpload = $('#fu' + id);
            var objImage = $('#' + id);
            var objHidden = $('#hid' + id);
            var objBtnDel = $('#btnDel' + id);

            objFileUpload.html('');
            objImage.attr('src', '');
            objImage.hide();
            objHidden.val('true');
            objBtnDel.hide();
        }

        function autoAddBid() {
            var brandI = $('#<%=hif_BrandId.ClientID %>').val();
            $.ajax(
                {
                    type: "POST",
                    url: "BrandSetup.aspx/GetBid",
                    data: " { 'brandI': '" + brandI + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var jsonObj = JSON.parse(data.d);
                        var count = 0;
                        if (jsonObj.Message.length >= 10) {
                            $("input[id*='txtActBid']").each(function() {
                                var type = $(this).attr('disabled') != "disabled";
                                if (count == 0 || type) {
                                    $(this).val(jsonObj.Message[count]);
                                    $(this).removeClass("TextboxWatermark");
                                    count++;
                                }
                            });
                            $("input[id*='txtHotItem']").each(function() {
                                $(this).val(jsonObj.Message[count]);
                                $(this).removeClass("TextboxWatermark");
                                count++;
                            });
                        } else {
                            alert('目前尚未匯入檔次或是商品數不到10個，請先確認商品');
                        }
                    }, error: function () {
                        alert('目前尚未匯入檔次或是商品數不到10個，請先確認商品');
                    }
                });
        }

        function disableAct(obj) {
            var index = obj.value;
            if (obj.checked) {
                $("input[id*='txtActBid" + index + "']").val("");
                $("input[id*='txtActBid" + index + "']").attr('disabled', true);
            } else {
                $("input[id*='txtActBid" + index + "']").removeAttr('disabled');
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hif_Mode" runat="server" />
    <asp:HiddenField ID="hif_BrandId" runat="server" />
    <asp:HiddenField ID="hif_ItemId" runat="server" />
    <asp:HiddenField ID="hif_ItemCount" runat="server" />
    <asp:HiddenField ID="hif_tab" runat="server" />
    <asp:Panel ID="pan_BrandList" runat="server">
        <asp:CheckBox ID="cbx_LimitInThreeMonth" runat="server" Text="只顯示3個月內的" Checked="true" /><br />
        <asp:TextBox ID="tbSearch" runat="server"></asp:TextBox>
        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="tbSearch"
            WatermarkCssClass="TextboxWatermark" WatermarkText="可輸入ID/名稱">
        </cc1:TextBoxWatermarkExtender>
        <asp:DropDownList ID="ddlSearch" runat="server" DataTextField="Value" DataValueField="Key" Height="25">
        </asp:DropDownList>
        <asp:Button ID="btnSearch" runat="server" Text="搜尋" OnClick="btnSearch_Click" />
        <br />
        <br />
        <asp:Button ID="btnAdd" runat="server" Text="新增" CommandArgument="1" OnCommand="AddBrand" />
        <br />
        <br />
        <asp:GridView ID="gv_Brand" runat="server" AutoGenerateColumns="false" OnRowCommand="gvBrandCommand" OnRowDataBound="gv_Brand_RowDataBound" Width="100%">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <table style="background-color: gray;">
                            <tr>
                                <td class="bgOrange" style="width:  3%" rowspan="2">ID</td>
                                <td class="bgOrange" style="width: 20%" rowspan="2">名稱</td>
                                <td class="bgOrange" style="width: 20%" rowspan="2">曝光頻道</td>
                                <td class="bgOrange" style="width: 20%" colspan="2">輪播期間</td>
                                <td class="bgOrange" style="width: 15%" rowspan="2">連結</td>
                                <td class="bgOrange" style="width: 12%" rowspan="2" colspan="2">狀態</td>
                                <td class="bgOrange" style="width:  4%" colspan="2">顯示</td>
                                <td class="bgOrange" style="width:  3%" rowspan="2">編輯</td>
                                <td class="bgOrange" style="width:  3%" rowspan="2">預覽</td>
                            </tr>
                            <tr>
                                <td class="bgOrange">開始</td>
                                <td class="bgOrange">結束</td>
                                <td class="bgOrange">Web</td>
                                <td class="bgOrange">App</td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="bgWhite" style="text-align: center;">
                                <%# ((Brand)(Container.DataItem)).Id %>
                            </td>
                            <td class="bgWhite" style="word-break: break-all;">
                                <%# ((Brand)(Container.DataItem)).BrandName %>
                            </td>
                            <td class="bgWhite" style="word-break: break-all;">
                                <asp:Literal ID="litChannel" runat="server" Mode="PassThrough" />
                            </td>
                            <td class="bgWhite" style="text-align: center;">
                                <%# ((Brand)(Container.DataItem)).StartTime.ToString("yyyy/MM/dd HH:mm")%>
                            </td>
                            <td class="bgWhite" style="text-align: center;">
                                <%# ((Brand)(Container.DataItem)).EndTime.ToString("yyyy/MM/dd HH:mm")%>
                            </td>
                            <td class="bgWhite" style="word-break: break-all;">
                                <asp:Label ID="lblLink" runat="server" Font-Size="Small"></asp:Label>
                            </td>
                            <td class="bgWhite" style="text-align:center;">
                                <asp:Label ID="discountMsg" runat="server"></asp:Label>
                            </td>
                            <td class="bgWhite"  style="text-align:center;">                                
                                <asp:ImageButton ID="ibt_ChangeStatus" runat="server" CommandName="ChangeStatus"
                                    Width="30" CommandArgument="<%# ((Brand)(Container.DataItem)).Id %>" ImageUrl='<%# ((Brand)(Container.DataItem)).Status ? "~/Themes/default/images/17Life/G3/CheckoutPass.png"  : "~/Themes/default/images/17Life/G3/CheckoutError.png"%>' />
                            </td>
                            <td class="bgWhite" style="text-align: center;">
                                <asp:CheckBox runat="server" ID="ShowInWeb" Checked="<%# ((Brand)(Container.DataItem)).ShowInWeb %>" OnCheckedChanged="ShowInWebChange" AutoPostBack="True" />
                            </td>
                            <td class="bgWhite" style="text-align: center;">
                                <asp:CheckBox runat="server" ID="ShowInApp" Checked="<%# ((Brand)(Container.DataItem)).ShowInApp %>" OnCheckedChanged="ShowInAppChange" AutoPostBack="True" />
                            </td>
                            <td class="bgWhite" style="text-align: center;">
                                <asp:LinkButton ID="lbt_Edit" runat="server" CommandName="EditBrand" CommandArgument="<%# ((Brand)(Container.DataItem)).Id %>">編輯</asp:LinkButton>
                            </td>
                            <td class="bgWhite" style="text-align: center;">
                                <asp:Label ID="lblPreview" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pan_BrandEdit" runat="server" Visible="false">
    <div id="container">
        &nbsp;&nbsp;<asp:Button ID="btnBackList" runat="server" Text="回到列表" CommandArgument="0" OnCommand="AddBrand" /><br />
        <br />
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1" class="tabs1">版型設定</a></li>
                <li><a href="#tabs-2" class="tabs2">商品與分類管理</a></li>
            </ul>
            <div id="tabs-1">    
                <table style="width: 1000px">
                    <tr>
                        <td colspan="2" class="header">名稱    
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            名稱:<asp:TextBox ID="tbx_BrandName" runat="server" Width="300"></asp:TextBox><span style="color: Gray">請以中文命名！將帶入此活動網頁title </span>
                            <asp:RequiredFieldValidator ID="rev_EventName" runat="server" ErrorMessage="*" ControlToValidate="tbx_BrandName"
                                Display="Dynamic" ForeColor="Red" ValidationGroup="A"></asp:RequiredFieldValidator>
                            <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input id="btnShowPopup" type="button" value="匯入商品" onclick="if($('#divImport').css('display') == 'none'){$('#divImport').show();}else{$('#divImport').hide();}" />
                                <br/>
                                <br/>
                            <div id="divImport" style="display:none;background-color:white; border:solid;padding:5px 10px 5px 10px;" align="left">
                                <span style="font-weight:bold;">匯入bid檔次</span><br /><br />
                                <asp:TextBox ID="txtImportBid" runat="server" Width="300px" />&nbsp;&nbsp;<asp:Button ID="btnSingalImport" runat="server" Text="單檔匯入" OnClick="btnSingalImport_Click" />
                                <hr>
                                <asp:FileUpload ID="FileUpload1" runat="server" /><asp:Button ID="btnImport" runat="server" Text="確定匯入" OnClick="btnImport_Click" />
                                <br />
                                <br />
                                *註: 請上傳txt檔案，並以換行符號分隔。
                                <br/>
                                <br/>
                                若無類別，範例如下:<br/>
                                2EB9234B-7C0C-45B7-877A-0000B45F0657<br/>
                                7FEAFC2F-FABC-4381-8A1F-0002786FCBDC<br/>
                                28D61EEA-9B02-4412-9727-00036CE71DE9
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="header">網址設定    
                        </td>
                    </tr>
                    <tr>
                        <td class="title">CPA:
                        </td>
                        <td>
                           <asp:TextBox ID="txt_Cpa" runat="server" Width="300"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="title">自訂網址:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_Url" runat="server" Width="300"></asp:TextBox> <span style="color: Gray">請以英文命名，將帶入此活動網網址 </span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="header">SEO設定區塊
                        </td>
                    </tr>
                    <tr>
                        <td class="title">meta description:
                        </td>
                        <td>
                            <asp:TextBox ID="txt_MetaDescription" runat="server" Width="300"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="header">輪播區間    
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:CheckBoxList ID="cblCities" runat="server" RepeatDirection="Horizontal"  RepeatColumns="7"  
                                 CssClass="tdwidth" ClientIDMode="Static"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="header">輪播期間    
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:TextBox ID="tbx_Start" runat="server" Width="100"></asp:TextBox>
                            <asp:DropDownList ID="ddl_StartH" runat="server"></asp:DropDownList>:
                            <asp:DropDownList ID="ddl_Startm" runat="server"></asp:DropDownList>
                            <cc1:CalendarExtender ID="ce_Start" TargetControlID="tbx_Start" runat="server">
                            </cc1:CalendarExtender>
                            ~
                            <asp:TextBox ID="tbx_End" runat="server" Width="100"></asp:TextBox>
                            <asp:DropDownList ID="ddl_EndH" runat="server"></asp:DropDownList>:
                            <asp:DropDownList ID="ddl_Endm" runat="server"></asp:DropDownList>
                            <cc1:CalendarExtender ID="ce_End" TargetControlID="tbx_End" runat="server">
                            </cc1:CalendarExtender>
                            
                            <asp:RequiredFieldValidator ID="rev_Start" runat="server" ErrorMessage="*起" ControlToValidate="tbx_Start" Display="Dynamic" ForeColor="Red" ValidationGroup="A"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rev_End" runat="server" ErrorMessage="*迄" ControlToValidate="tbx_End" Display="Dynamic" ForeColor="Red" ValidationGroup="A"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="header">示意圖    
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="400px" style="border-collapse:collapse">
                                <tr style="height:60px" align="center">
                                    <td colspan="3" class="virtualPic">廠商形象Banner</td>
                                </tr>
                                <tr style="height:40px" align="center">
                                    <td rowspan="2" class="virtualPic">行銷活動(輪播呈現,最多5個)</td>
                                    <td class="virtualPic">商品1<br />(小圖)</td>
                                    <td class="virtualPic">商品2<br />(小圖)</td>
                                </tr>
                                <tr style="height:40px" align="center">
                                    <td class="virtualPic">商品3<br />(小圖)</td>
                                    <td class="virtualPic">商品4<br />(小圖)</td>
                                </tr>
                                <tr style="height:20px" align="center">
                                    <td class="virtualPic" colspan="3">分類標籤</td>
                                </tr>
                                <tr style="height:60px" align="center">
                                    <td class="virtualPic" colspan="3">商品列表</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="header">編輯    
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span style="color: Blue;font-weight:bold;">形象圖Banner</span>
                        </td>
                    </tr>  
                    <tr>
                        <td class="title" rowspan="2">主圖:
                        </td>
                        <td>
                            網站主圖  <br />
                            (業務用)圖片大小(960x250)  <br />
                            (行銷用)圖片大小(960x不限)  <br />
                            <asp:Image runat="server" ID="MainPic" ClientIDMode="Static" />
                            <input id="btnDelMainPic" type="button" value="刪除原圖片" onclick="delPic('MainPic');" />
                            <asp:HiddenField ID="hidMainPic" runat="server" ClientIDMode="Static" />
                            <br />
                            上傳圖片:
                            <asp:FileUpload ID="fuMainPic" runat="server" ClientIDMode="Static" />                            
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td style="border-top-color: black; border-top: solid;">
                            <br />
                            網站主圖背景圖片大小(1920x不限(但須和主圖一致))(行銷用)
                            <br />
                            <asp:Image runat="server" ID="MainBackgroundPic" ClientIDMode="Static" />
                            <input id="btnDelMainBackgroundPic" type="button" value="刪除原圖片" onclick="delPic('MainBackgroundPic');" />
                            <asp:HiddenField ID="hidMainBackgroundPic" runat="server" ClientIDMode="Static" />
                            <br />
                            上傳圖片:
                            <asp:FileUpload ID="fuMainBackgroundPic" runat="server" ClientIDMode="Static" /> 
                        </td>
                    </tr>
                    <tr>
                        <td class="title">行動版主圖:
                        </td>
                        <td>
                            <br />
                            行動版主圖圖片大小(640x320)
                            <br />
                            <asp:Image runat="server" ID="MobileMainPic" ClientIDMode="Static" />
                            <input id="btnDelMobileMainPic" type="button" value="刪除原圖片" onclick="delPic('MobileMainPic');" />
                            <asp:HiddenField ID="hidMobileMainPic" runat="server" ClientIDMode="Static" />
                            <br />
                            上傳圖片:
                            <asp:FileUpload ID="fuMobileMainPic" runat="server" ClientIDMode="Static" />                            
                        </td>
                    </tr>                    
                    <tr>
                        <td class="title">背景:
                        </td>
                        <td>
                            <asp:TextBox ID="tbx_BgPic" runat="server" Width="300"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="tbw_BgPic" runat="server" TargetControlID="tbx_BgPic"
                                WatermarkCssClass="ldnmbg" WatermarkText="http://">
                            </cc1:TextBoxWatermarkExtender>
                            &nbsp;
                            <asp:TextBox ID="tbx_BgColor" runat="server"></asp:TextBox>
                            <cc1:ColorPickerExtender runat="server" TargetControlID="tbx_BgColor" ID="cpe_BgColor" />
                        </td>
                    </tr>
                    <tr>
                        <td class="title" rowspan="2">APP相關素材
                        </td>
                        <td>圖片大小(640x320)，下方顯示位置可以複選:
                            <br />
                            <br />
                            顯示位置：<asp:CheckBoxList ID="chbBannerLocation" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static"></asp:CheckBoxList>
                            <asp:Image runat="server" ID="AppBannerImage" ClientIDMode="Static" />
                            <input id="btnDelAppBannerImage" type="button" value="刪除原圖片" onclick="delPic('AppBannerImage');" />
                            <asp:HiddenField ID="hidAppBannerImage" runat="server" ClientIDMode="Static" />
                            <br />
                            上傳圖片:
                            <asp:FileUpload ID="fuAppBannerImage" runat="server" ClientIDMode="Static" />
                        </td>
                    </tr>
                    <tr>
                        <td style="border-top-color:black;border-top:solid;">
                            <br />
                            App首頁圖片大小(640x356)
                            <br />
                            <asp:Image runat="server" ID="AppPromoImage" ClientIDMode="Static" />
                            <input id="btnDelAppPromoImage" type="button" value="刪除原圖片" onclick="delPic('AppPromoImage');" />
                            <asp:HiddenField ID="hidAppPromoImage" runat="server" ClientIDMode="Static" />
                            <br />
                            上傳圖片:
                            <asp:FileUpload ID="fuAppPromoImage" runat="server" ClientIDMode="Static" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span style="color: Blue;font-weight:bold;">行銷活動:</span>
                            &nbsp;&nbsp;<input id="btnAutoAdd" type="button" value="產生商品" onclick="autoAddBid('<%=txtActBid3.ClientID %>')" />
                        </td>
                    </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;&nbsp;&nbsp;1&nbsp;<asp:TextBox ID="txtActBid1" runat="server" placeholder="輸入Bid"></asp:TextBox>
                        &nbsp;&nbsp;<input id="btnCheckAct1" type="button" value="檢視商品" onclick="previewBid('<%=txtActBid1.ClientID %>')" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;&nbsp;&nbsp;2&nbsp;<asp:TextBox ID="txtActBid2" runat="server" placeholder="輸入Bid"></asp:TextBox>
                        &nbsp;&nbsp;<input id="btnCheckAct2" type="button" value="檢視商品" onclick="previewBid('<%=txtActBid2.ClientID %>')" />
                        &nbsp;&nbsp;<input id="checkboxAct2" type="checkbox" value="2" onchange="disableAct(this)" /> 前台不顯示
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;&nbsp;&nbsp;3&nbsp;<asp:TextBox ID="txtActBid3" runat="server" placeholder="輸入Bid"></asp:TextBox>
                        &nbsp;&nbsp;<input id="btnCheckAct3" type="button" value="檢視商品" onclick="previewBid('<%=txtActBid3.ClientID %>')" />
                        &nbsp;&nbsp;<input id="checkboxAct3" type="checkbox" value="3" onchange="disableAct(this)" /> 前台不顯示
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;&nbsp;&nbsp;4&nbsp;<asp:TextBox ID="txtActBid4" runat="server" placeholder="輸入Bid"></asp:TextBox>
                        &nbsp;&nbsp;<input id="btnCheckAct4" type="button" value="檢視商品" onclick="previewBid('<%=txtActBid4.ClientID %>')" />
                        &nbsp;&nbsp;<input id="checkboxAct4" type="checkbox" value="4" onchange="disableAct(this)" /> 前台不顯示
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;&nbsp;&nbsp;5&nbsp;<asp:TextBox ID="txtActBid5" runat="server" placeholder="輸入Bid"></asp:TextBox>
                        &nbsp;&nbsp;<input id="btnCheckAct5" type="button" value="檢視商品" onclick="previewBid('<%=txtActBid5.ClientID %>')" />
                        &nbsp;&nbsp;<input id="checkboxAct5" type="checkbox" value="5" onchange="disableAct(this)" /> 前台不顯示
                    </td>
                </tr>
                    <tr>
                        <td colspan="2"><span style="color: Blue;font-weight:bold;">主打商品(僅適用於Web):</span></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;&nbsp;&nbsp;1&nbsp;<asp:TextBox ID="txtHotItem1" runat="server" placeholder="輸入Bid"></asp:TextBox>
                            &nbsp;&nbsp;<input id="btnCheckBid1" type="button" value="檢視商品" onclick="previewBid('ctl00_ContentPlaceHolder1_txtHotItem1')" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;&nbsp;&nbsp;2&nbsp;<asp:TextBox ID="txtHotItem2" runat="server" placeholder="輸入Bid"></asp:TextBox>
                            &nbsp;&nbsp;<input id="btnCheckBid2" type="button" value="檢視商品" onclick="previewBid('ctl00_ContentPlaceHolder1_txtHotItem2')" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;&nbsp;&nbsp;3&nbsp;<asp:TextBox ID="txtHotItem3" runat="server" placeholder="輸入Bid"></asp:TextBox>
                            &nbsp;&nbsp;<input id="btnCheckBid3" type="button" value="檢視商品" onclick="previewBid('ctl00_ContentPlaceHolder1_txtHotItem3')" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;&nbsp;&nbsp;4&nbsp;<asp:TextBox ID="txtHotItem4" runat="server" placeholder="輸入Bid"></asp:TextBox>
                            &nbsp;&nbsp;<input id="btnCheckBid4" type="button" value="檢視商品" onclick="previewBid('ctl00_ContentPlaceHolder1_txtHotItem4')" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2"><span style="color: Blue;font-weight:bold;">分類標籤顏色:</span></td>
                    </tr>
                    <tr>
                        <td class="title">按鈕圖片:
                        </td>
                        <td>
                            <table style="width: 100%">
                                <tr>
                                    <td></td>
                                    <td>Original</td>
                                    <td>Hover</td>
                                    <td>Active</td>
                                </tr>
                                <tr>
                                    <td>圖片連結:</td>
                                    <td>
                                        <asp:TextBox ID="tbx_BtnOriginalUrl" runat="server"></asp:TextBox>
                                        <cc1:TextBoxWatermarkExtender ID="tbw_BtnOriginal" runat="server" TargetControlID="tbx_BtnOriginalUrl"
                                            WatermarkCssClass="ldnmbg" WatermarkText="http://">
                                        </cc1:TextBoxWatermarkExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbx_BtnHoverUrl" runat="server"></asp:TextBox>
                                        <cc1:TextBoxWatermarkExtender ID="tbw_BtnHover" runat="server" TargetControlID="tbx_BtnHoverUrl"
                                            WatermarkCssClass="ldnmbg" WatermarkText="http://">
                                        </cc1:TextBoxWatermarkExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbx_BtnActiveUrl" runat="server"></asp:TextBox>
                                        <cc1:TextBoxWatermarkExtender ID="tbw_BtnActive" runat="server" TargetControlID="tbx_BtnActiveUrl"
                                            WatermarkCssClass="ldnmbg" WatermarkText="http://">
                                        </cc1:TextBoxWatermarkExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>字體顏色:</td>
                                    <td>
                                        <asp:TextBox ID="tbx_BtnOriginalFontColor" runat="server"></asp:TextBox>
                                        <cc1:ColorPickerExtender ID="cpe_BrnFontColor" runat="server" TargetControlID="tbx_BtnOriginalFontColor" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ValidationExpression="^(#|)(?:[0-9a-fA-F]{3}){1,2}$" ControlToValidate="tbx_BtnOriginalFontColor" Display="Dynamic" ErrorMessage="必需輸入Hex網頁色碼"></asp:RegularExpressionValidator>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbx_BtnHoverFontColor" MaxLength="7" runat="server"></asp:TextBox>
                                        <cc1:ColorPickerExtender ID="ColorPickerExtender9" runat="server" TargetControlID="tbx_BtnHoverFontColor" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ValidationExpression="^(#|)(?:[0-9a-fA-F]{3}){1,2}$" ControlToValidate="tbx_BtnHoverFontColor" Display="Dynamic" ErrorMessage="必需輸入Hex網頁色碼"></asp:RegularExpressionValidator>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbx_BtnActiveFontColor" MaxLength="7" runat="server"></asp:TextBox>
                                        <cc1:ColorPickerExtender ID="ColorPickerExtender10" runat="server" TargetControlID="tbx_BtnActiveFontColor" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server" ValidationExpression="^(#|)(?:[0-9a-fA-F]{3}){1,2}$" ControlToValidate="tbx_BtnActiveFontColor" Display="Dynamic" ErrorMessage="必需輸入Hex網頁色碼"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="header">活動辦法(若空白則不會顯現)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span style="color: Gray">無法變更字形、字元大小與顏色 </span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:TextBox ID="txtDescription" runat="server" Width="750" Height="300" TextMode="MultiLine" ValidateRequestMode="Disabled"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="header">相關檔次設定
                        </td>
                    </tr>
                    <tr>
                        <td class="title">黑標文案:
                        </td>
                        <td>
                            <asp:TextBox ID="txtDealPromoTitle" runat="server" MaxLength="50"></asp:TextBox>
                            字數請勿超過50！訊息將接續檔次黑標之後
                        </td>
                    </tr>
                    <tr>
                        <td class="title">活動LOGO:
                        </td>
                        <td>
                            檔次大圖將壓上活動LOGO
                            <br />
                            <asp:Image runat="server" ID="imgDealPromoImage" ClientIDMode="Static" />
                            <input id="btnDelimgDealPromoImage" type="button" value="刪除原圖片" onclick="delPic('imgDealPromoImage');" />
                            <asp:HiddenField ID="hidimgDealPromoImage" runat="server" ClientIDMode="Static" />
                            <br />
                            上傳圖片:
                            <asp:FileUpload ID="fuimgDealPromoImage" runat="server" ClientIDMode="Static" />
                        </td>
                    </tr>
                    <tr>
                        <td class="title">詳細介紹文案:
                        </td>
                        <td>
                            放置檔次詳細介紹最下方(560X不限)
                            <asp:TextBox ID="txtDealPromoDescription" runat="server" Width="750" Height="300" TextMode="MultiLine" ValidateRequestMode="Disabled"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="header">立刻歸戶折價券活動ID(List)   
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:TextBox ID="txtDiscountList" runat="server" placeholder="ex: 812,813,814" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="header">立刻歸戶折價券活動Banner(圖片大小：960x134)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Image runat="server" ID="imgDiscountBanner" ClientIDMode="Static" />
                            <input id="btnDelimgDiscountBanner" type="button" value="刪除原圖片" onclick="delPic('imgDiscountBanner');" />
                            <asp:HiddenField ID="hidimgDiscountBanner" runat="server" ClientIDMode="Static" />
                            <br />
                            上傳圖片:
                            <asp:FileUpload ID="fuimgDiscountBanner" runat="server" ClientIDMode="Static" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="header">策展列表頁圖片(圖片大小：600x260)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Image runat="server" ID="imgEventList" ClientIDMode="Static" />
                            <input id="btnDelimgEventList" type="button" value="刪除原圖片" onclick="delPic('imgEventList');" />
                            <asp:HiddenField ID="hidimgEventList" runat="server" ClientIDMode="Static" />
                            <br />
                            上傳圖片:
                            <asp:FileUpload ID="fuimgEventList" runat="server" ClientIDMode="Static" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="header">策展頻道圖片(圖片大小：640x320)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Image runat="server" ID="imgChannelList" ClientIDMode="Static" />
                            <input id="btnDelimgChannelList" type="button" value="刪除原圖片" onclick="delPic('imgChannelList');" />
                            <asp:HiddenField ID="hidimgChannelList" runat="server" ClientIDMode="Static" />
                            <br />
                            上傳圖片:
                            <asp:FileUpload ID="fuimgChannelList" runat="server" ClientIDMode="Static" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="header">策展網站商品的策展折價券圖片(圖片大小：700x70)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Image runat="server" ID="imgWebRelay" ClientIDMode="Static" />
                            <input id="btnDelimgWebRelay" type="button" value="刪除原圖片" onclick="delPic('imgWebRelay');" />
                            <asp:HiddenField ID="hidimgWebRelay" runat="server" ClientIDMode="Static" />
                            <br />
                            上傳圖片:
                            <asp:FileUpload ID="fuimgWebRelay" runat="server" ClientIDMode="Static" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="header">策展行動版網頁及APP商品的策展折價券圖片(圖片大小：640x140)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Image runat="server" ID="imgMobileRelay" ClientIDMode="Static" />
                            <input id="btnDelimgMobileRelay" type="button" value="刪除原圖片" onclick="delPic('imgMobileRelay');" />
                            <asp:HiddenField ID="hidimgMobileRelay" runat="server" ClientIDMode="Static" />
                            <br />
                            上傳圖片:
                            <asp:FileUpload ID="fuimgMobileRelay" runat="server" ClientIDMode="Static" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="header">FB分享圖片(圖片大小：1200x630)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Image runat="server" ID="imgFbShare" ClientIDMode="Static" />
                            <input id="btnDelimgFbShare" type="button" value="刪除原圖片" onclick="delPic('imgFbShare');" />
                            <asp:HiddenField ID="hidimgFbShare" runat="server" ClientIDMode="Static" />
                            <br />
                            上傳圖片:
                            <asp:FileUpload ID="fuimgFbShare" runat="server" ClientIDMode="Static" />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <asp:Button ID="btn_Save" runat="server" Text="新增" ForeColor="Blue" ValidationGroup="A"
                                OnClick="SaveBrandMain" />&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btn_Cancel" runat="server" Text="取消" ForeColor="Red" CommandArgument="0"
                                OnCommand="AddBrand" />
                        </td>
                    </tr>
                </table>                
                <asp:Label ID="lab_Message" runat="server" ForeColor="White" BackColor="Red"></asp:Label>   
            </div>
            <div id="tabs-2" align="left"> 
                <asp:Label ID="lab_emptyItem" runat="server" ForeColor="Red" BackColor="White"></asp:Label> <br />
                <asp:PlaceHolder ID="ph_BrandItemInfo" Visible="false" runat="server">
                    <input id="btnShowAddBrandItemCategory" type="button" value="新增商品類別" onclick="showDiv($('#divAddCategory')); " />&nbsp;
                    <input id="btnBatchEditBrandItemCategory" type="button" value="批次修改類別" onclick="showDiv($('#divEditCategory'));" />
                    <asp:Button ID="btnShowSort" runat="server" Text="手動排序" OnClick="btnShowSort_Click" />&nbsp;
                    <input id="btnShowCategorySort" type="button" value="分類排序" onclick="showCategorySort();" />&nbsp;
                    <input id="btnShowCategoryName" type="button" value="商品類別名稱修改" onclick="showCategoryName();" />&nbsp;
                    <input id="btnShowCategoryDelete" type="button" value="商品類別刪除" onclick="showDeleteCategory();" />&nbsp;
                    <input id="btnBatchDeleteItemList" type="button" value="批次刪除商品" onclick="showDiv($('#divEditDeleteItemList'));" />
                    <asp:Button ID="btnExportBrandItemBid" runat="server" Text="匯出商品檔號" OnClick="btnExportBrandItemBid_Click" />&nbsp;
                    <br />
                    <br />                
                    <div id="divDefault" style=" padding: 5px 10px 5px 10px;" align="left">
                        商品名稱：&nbsp;<asp:TextBox ID="txtSearchName" runat="server"></asp:TextBox><br />
                        商品類別：&nbsp;<asp:DropDownList ID="ddlSearchCategory" runat="server"></asp:DropDownList> &nbsp;<asp:Button ID="btnSearchItem" runat="server" Text="搜尋" OnClick="btnSearchItem_Click" />
                        <br />&nbsp;
                    </div>
                    <div id="divAddCategory" style="display: none; padding: 5px 10px 5px 10px;" align="left">
                        商品類別：&nbsp;<asp:TextBox ID="txtBrandItemCategory" runat="server"></asp:TextBox><br />
                        <asp:Button ID="btnAddBrandItemCategory" runat="server" Text="更新" OnClick="btnAddBrandItemCategory_Click" />&nbsp;
                        <input id="btnHide1" type="button" value="取消" onclick="showDiv($('#divDefault'));" />
                        <br />&nbsp;
                    </div>
                    <div id="divEditCategory" style="display: none; padding: 5px 10px 5px 10px;" align="left">
                        取代商品類別：&nbsp;<asp:DropDownList ID="ddlReplaceCategory" runat="server"></asp:DropDownList>&nbsp;<asp:Button ID="btnReplaceCategory" runat="server" Text="取代" OnClientClick="return checkSelect('ddlReplaceCategory','R');" OnClick="btnReplaceCategory_Click" /><br />
                        新增商品類別：&nbsp;<asp:DropDownList ID="ddlAddCategory" runat="server"></asp:DropDownList>&nbsp;<asp:Button ID="btnAddCategory" runat="server" Text="新增" OnClientClick="return checkSelect('ddlAddCategory','A');" OnClick="btnAddCategory_Click" />&nbsp;&nbsp;
                        <input id="btnHide2" type="button" value="取消" onclick="showDiv($('#divDefault'));" /><br />&nbsp;
                    </div>
                    <div id="divEditDeleteItemList" style="display: none; padding: 5px 10px 5px 10px;" align="left">
                        <asp:Button ID="btnDeleteItemList" runat="server" Text="確認刪除" OnClientClick="return checkSelect('btnDeleteItemList','DelItem');" OnClick="btnDeleteItemList_Click" />
                        <input id="btnHide3" type="button" value="取消" onclick="showDiv($('#divDefault'));" />
                        <br />&nbsp;
                    </div>
                    <div id="divItemSort" style="display: none; padding: 5px 10px 5px 10px;" align="left">
                        <span style="color:red">請直接於下方列表拖曳項目以調整順序,調整完畢請儲存!</span><br /><br />
                        <asp:Button ID="btnSaveSeq" runat="server" Text="儲存排序" OnClientClick="return confirm('會覆蓋掉原有排序，確定要儲存？');" OnClick="btnSaveSeq_Click" />
                        <input id="btnHide4" type="button" value="取消" onclick="showDiv($('#divDefault'));" /><br />&nbsp;
                    </div>
                    <div id="divCategorySort" style="display: none; padding: 5px 10px 5px 10px;" align="left">
                        <span style="color:red">請直接於下方列表拖曳項目以調整分類順序,調整完畢請儲存!</span><br /><br />
                        <asp:Button ID="btnSaveCategorySeq" runat="server" Text="儲存分類排序" OnClientClick="return confirm('會覆蓋掉原有排序，確定要儲存？');" OnClick="btnSaveCategorySeq_Click" />
                        <input id="btnHide5" type="button" value="取消" onclick="showDiv($('#divDefault'));" /><br />&nbsp;
                    </div>
                    <div id="divCategoryName" style="display: none; padding: 5px 10px 5px 10px;" align="left">
                        <span style="color:red">請直接於下方列表調整分類名稱,調整完畢請儲存!</span><br /><br />
                        <asp:Button ID="btnSaveCategoryName" runat="server" Text="儲存分類名稱" OnClientClick="return confirm('會改變分類名稱，確定要儲存？');" OnClick="btnSaveCategoryName_Click" />
                        <input id="btnHide6" type="button" value="取消" onclick="showDiv($('#divDefault'));" /><br />&nbsp;
                    </div>
                <div id="divDeleteCategory" style="display: none; padding: 5px 10px 5px 10px;" align="left">
                    <span style="color:red">請直接於下方列表勾選刪除,完畢請按刪除!</span><br /><br />
                    <asp:Button ID="btnDeleteCategory" runat="server" Text="刪除" OnClientClick="return confirm('會刪除分類，確定？');" OnClick="btnDeleteCategory_Click" />
                    <input id="btnHide6" type="button" value="取消" onclick="showDiv($('#divDefault'));" /><br />&nbsp;
                </div>
                    <div id="divChangeCategory" style="display: none; padding: 5px 10px 5px 10px;">
                        商品類別頁籤:<br />
                        <asp:CheckBoxList ID="cbBrandItemCategory" runat="server" RepeatDirection="Horizontal"  RepeatColumns="2" CssClass="tdwidth" />
                    </div>

                    <asp:HiddenField ID="hif_CheckItemList" runat="server" />
                    <asp:Repeater ID="rpt_BrandItem" runat="server" OnItemDataBound="rpt_BrandItem_ItemDataBound" OnItemCommand="rptBrandItemCommand">
                        <HeaderTemplate>
                            <table id="tbItem" class="manualSort" style="background-color: gray; width: 1240px; word-break: break-all;">
                                <thead>
                                    <tr>
                                        <td class="bgOrange headerNoSort" style="min-width: 60px">
                                            <input id="cbx_EditAll" class="hide" type="checkbox" onchange="OnBrandItemSelected('all');" style="display: none;" />排序
                                        </td>
                                        <td class="bgOrange" style="min-width: 75px">檔號</td>
                                        <td class="bgOrange" style="min-width: 200px">商品名稱</td>
                                        <td class="bgOrange" style="min-width: 80px">營業額</td>
                                        <td class="bgOrange" style="min-width: 80px">毛利額</td>
                                        <td class="bgOrange grossMarginTd" style="min-width: 100px;">毛利率</td>
                                        <td class="bgOrange" style="min-width: 60px">狀態</td>
                                        <td class="bgOrange" style="min-width: 100px">商品類別頁籤</td>
                                        <td class="bgOrange headerNoSort" style="min-width: 80px">商品顯示</td>
                                        <td class="bgOrange headerNoSort" style="min-width: 80px">排序鎖定</td>
                                    </tr>
                                </thead>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="bgBlue itemSeqTd">
                                    <asp:HiddenField runat="server" ID="HidId" Value="<%# ((ViewBrandItem)(Container.DataItem)).Id %>" />
                                    <asp:HiddenField runat="server" ID="HidTempSeq" Value="<%# ((ViewBrandItem)(Container.DataItem)).Seq %>" />
                                    <asp:HiddenField runat="server" ID="HidBid" Value="<%# ((ViewBrandItem)(Container.DataItem)).BusinessHourGuid %>" />
                                    <input id="cbx_EditCategory_<%# ((ViewBrandItem)(Container.DataItem)).Id%>" class="hide" type="checkbox" onchange="OnBrandItemSelected();" style="display: none;" />
                                    <span class="itemSeqDiv">
                                        <%# ((ViewBrandItem)(Container.DataItem)).Seq%>
                                    </span>
                                </td>
                                <td class="bgBlue" align="center">
                                    <%# ((ViewBrandItem)(Container.DataItem)).ItemId%>
                                </td>
                                <td class="bgBlue">
                                    <%# ((ViewBrandItem)(Container.DataItem)).ItemName%>
                                </td>
                                <td class="bgBlue" style="text-align: right;">
                                    <%# string.Format("{0:#,0}", ((ViewBrandItem)(Container.DataItem)).OrderedTotal) %>
                                </td>
                                <td class="bgBlue" style="text-align: right;">
                                    <%# string.Format("{0:#,0}", ((ViewBrandItem)(Container.DataItem)).OrderedTotal * ((ViewBrandItem)(Container.DataItem)).GrossMargin / 100) %>
                                </td>
                                <td class="bgBlue grossMarginTd" style="text-align: right; <%# (((ViewBrandItem)(Container.DataItem)).GrossMarginStatus & 1) > 0 ? "color:red": "" %>; <%# (((ViewBrandItem)(Container.DataItem)).GrossMarginStatus > 0) ? "" : "display:none" %>;"> 
                                    <%# string.Format("{0:0.##}%", ((ViewBrandItem)(Container.DataItem)).GrossMargin) %> <%# (((ViewBrandItem)(Container.DataItem)).GrossMarginStatus & 2) > 0 ? "(開放)" : "" %>
                                    <asp:Literal ID="litDiscountStatus" runat="server" />
                                </td>
                                <td class="bgBlue" style="text-align: center;">
                                    <%--狀態--%>
                                    <a href='<%# "../../Ppon/default.aspx?bid="+((ViewBrandItem)(Container.DataItem)).BusinessHourGuid%>'
                                        target="_blank">
                                        <%# GetItemStatus((ViewBrandItem)Container.DataItem)%>
                                    </a>
                                </td>
                                <td class="bgBlue">
                                    <asp:Literal ID="litBrandItemCategory" runat="server"></asp:Literal>
                                    <a id="aChangeCategory" runat="server"><span style="color:blue;text-decoration:underline;cursor:pointer;">修改</span></a>
                                </td>
                                <td class="bgBlue" style="text-align: center;">
                                    <asp:ImageButton ID="ibt_ChangeStatus" runat="server" CommandName="ChangeItemStatus" Width="30" CommandArgument="<%# ((ViewBrandItem)(Container.DataItem)).Id %>"
                                        ImageUrl='<%# ((ViewBrandItem)(Container.DataItem)).Status ? "~/Themes/default/images/17Life/G3/CheckoutPass.png"  : "~/Themes/default/images/17Life/G3/CheckoutError.png"%>' />
                                </td>
                                <td class="bgBlue" style="text-align: center;">
                                    <asp:ImageButton ID="ibt_SeqStatus" runat="server" CommandName="ChangeSeqStatus" Width="30" CommandArgument="<%# ((ViewBrandItem)(Container.DataItem)).Id %>"
                                                     ImageUrl='<%# ((ViewBrandItem)(Container.DataItem)).SeqStatus ? "~/Themes/default/images/17Life/G3/CheckoutPass.png"  : "~/Themes/default/images/17Life/G3/CheckoutError.png"%>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <br />
                    <asp:Repeater ID="rpCategory" runat="server">
                         <HeaderTemplate>
                            <table id="tbCategory" class="manualSort2" style="background-color: gray;display:none;">
                                <thead>
                                    <tr>
                                        <td class="bgOrange">
                                            排序
                                        </td>
                                        <td class="bgOrange" style="min-width: 300px">分類名稱</td>
                                    </tr>
                                </thead>
                        </HeaderTemplate>
                        <ItemTemplate>
                                    <tr>
                                <td class="bgBlue itemSeqTd2">
                                    <asp:HiddenField runat="server" ID="HidId" Value="<%# ((BrandItemCategory)(Container.DataItem)).Id %>" />
                                    <asp:HiddenField runat="server" ID="HidTempSeq" Value="<%# ((BrandItemCategory)(Container.DataItem)).Seq %>" />
                                    <span class="itemSeqDiv2">
                                        <%# ((BrandItemCategory)(Container.DataItem)).Seq%>
                                    </span>
                                </td>
                                <td class="bgBlue">
                                    <%# ((BrandItemCategory)(Container.DataItem)).Name%>
                                </td>
                                    </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    
                    <asp:Repeater ID="rpCategoryName" runat="server">
                        <HeaderTemplate>
                            <table id="tbCategoryName" class="manualSort2" style="background-color: gray;display:none;">
                            <thead>
                            <tr>
                                <td class="bgOrange">
                                    分類名稱
                                </td>
                                <td class="bgOrange" style="min-width: 300px">修改分類名稱</td>
                            </tr>
                            </thead>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="bgBlue itemSeqTd2">
                                    <asp:HiddenField runat="server" ID="HidId" Value="<%# ((BrandItemCategory)(Container.DataItem)).Id %>" />
                                    <span class="itemSeqDiv2">
                                        <%# ((BrandItemCategory)(Container.DataItem)).Name%>
                                    </span>
                                </td>
                                <td class="bgBlue">
                                    <asp:TextBox ID="txtCategoryName" runat="server" Text="<%# ((BrandItemCategory)(Container.DataItem)).Name%>"></asp:TextBox>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    
                <asp:Repeater ID="rpDeleteCategory" runat="server">
                    <HeaderTemplate>
                        <table id="tbDeleteCategory" class="manualSort2" style="background-color: gray;display:none;">
                        <thead>
                        <tr>
                            <td class="bgOrange">
                                刪除
                            </td>
                            <td class="bgOrange" style="min-width: 300px">分類名稱</td>
                        </tr>
                        </thead>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="bgBlue itemSeqTd2">
                                <asp:HiddenField runat="server" ID="HidId" Value="<%# ((BrandItemCategory)(Container.DataItem)).Id %>" />
                                <asp:CheckBox runat="server" ID="DeleteType"/>
                            </td>
                            <td class="bgBlue">
                                <%# ((BrandItemCategory)(Container.DataItem)).Name%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>

                </asp:PlaceHolder>
            </div>
        </div>
    </div>
    </asp:Panel>
</asp:Content>

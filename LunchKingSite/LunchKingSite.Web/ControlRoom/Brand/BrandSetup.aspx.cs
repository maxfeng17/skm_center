﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using System.Web;
using System.Linq;
using System.IO;
using System.Web.Services;
using LunchKingSite.BizLogic.Model.API;
using System.Text;

namespace LunchKingSite.Web.ControlRoom.Brand
{
    public partial class BrandSetUp : RolePage, IBrandSetUp
    {

        #region event
        public event EventHandler<DataEventArgs<BrandUpdateModel>> SaveBrand;
        public event EventHandler<DataEventArgs<bool>> SetBrandShowInApp;
        public event EventHandler<DataEventArgs<bool>> SetBrandShowInWeb;
        public event EventHandler ChangeMode;
        public event EventHandler Search;
        public event EventHandler UpdateBrandStatus;
        public event EventHandler GetBrand;
        public event EventHandler<DataEventArgs<List<string>>> ImportBidList;
        public event EventHandler<DataEventArgs<KeyValuePair<int, Dictionary<int, int>>>> UpdateBrandItemSeq;
        public event EventHandler<DataEventArgs<KeyValuePair<int, Dictionary<int, int>>>> UpdateBrandItemCategorySeq;
        public event EventHandler<DataEventArgs<KeyValuePair<int, Dictionary<int, string>>>> UpdateBrandItemCategoryName;
        public event EventHandler<DataEventArgs<KeyValuePair<int, List<int>>>> DeleteBrandItemCategory;
        public event EventHandler UpdateBrandItemStatus;
        public event EventHandler UpdateSeqStatus;
        public event GetBrandItemStatusHandler GetBrandItemStatus;
        public event GetBrandItemCategoryHandler GetBrandItemCategory;
        public event EventHandler SearchBrandItem;
        public event EventHandler<DataEventArgs<KeyValuePair<int, string>>> SaveBrandItemCategory;
        public event EventHandler<DataEventArgs<KeyValuePair<string, string>>> BatchEditBrandItemCategory;
        public event EventHandler DeleteBrandItemList;
        public event GetStringBrandDIscountStatusHandler GetStringBrandDIscountStatus;
        #endregion

        #region property
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public ISysConfProvider SystemConfig
        {
            get { return cp; }
        }
        private BrandSetUpPresenter _presenter;
        public BrandSetUpPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public int BrandId
        {
            get
            {
                int brandid;
                return int.TryParse(hif_BrandId.Value, out brandid) ? brandid : 0;
            }
            set
            {
                hif_BrandId.Value = value.ToString();
            }
        }
        public int ItemId
        {
            get
            {
                int itemid;
                return int.TryParse(hif_ItemId.Value, out itemid) ? itemid : 0;
            }
            set
            {
                hif_ItemId.Value = value.ToString();
            }
        }        
        public int ItemCount
        {
            get
            {
                int itemcount;
                return int.TryParse(hif_ItemCount.Value, out itemcount) ? itemcount : 0;
            }
            set
            {
                hif_ItemCount.Value = value.ToString();
            }
        }
        public bool IsLimitInThreeMonth
        {
            get
            {
                return cbx_LimitInThreeMonth.Checked;
            }
        }
        public string SearchWord
        {
            get { return tbSearch.Text; }
        }
        public string SearchCityId
        {
            get { return ddlSearch.SelectedValue; }
        }
        //0 品牌館列表，1 主檔新增, 2 主檔修改
        public int Mode
        {
            get
            {
                int mode;
                return int.TryParse(hif_Mode.Value, out mode) ? mode : 0;
            }
            set
            {
                hif_Mode.Value = value.ToString();
                if (ChangeMode != null)
                {
                    EventArgs e = new EventArgs();
                    ChangeMode(this, e);
                }
            }
        }
        public string UserName
        {
            get { return User.Identity.Name; }
        }
        public List<int> SelectedBrandItem {
            get {
                List<int> selected = new List<int>();
                foreach (var brandItemId in hif_CheckItemList.Value.Split(",")) {
                    int itemId = int.TryParse(brandItemId, out itemId) ? itemId : 0;
                    if (itemId > 0)
                    {
                        selected.Add(itemId);
                    }
                }
                return selected;
            }
        }
        public string SearchItemName
        {
            get { return txtSearchName.Text; }
        }
        public int SearchItemCid
        {
            get {
                int itemCid = int.TryParse(ddlSearchCategory.SelectedValue, out itemCid) ? itemCid : 0;
                return itemCid;
            }
        }
        public string IsDelMainPic { get { return hidMainPic.Value; } }
        public string IsDelMainBackgroundPic { get { return hidMainBackgroundPic.Value; } }
        public string IsDelMobileMainPic { get { return hidMobileMainPic.Value; } }
        public string IsDelAppBannerImage { get { return hidAppBannerImage.Value; } }
        public string IsDelAppPromoImage { get { return hidAppPromoImage.Value; } }
        public string IsDelImgWeb1 { get { return txtActBid1.Text; } }
        public string IsDelImgWeb2 { get { return txtActBid2.Text; } }
        public string IsDelImgWeb3 { get { return txtActBid3.Text; } }
        public string IsDelImgWeb4 { get { return txtActBid4.Text; } }
        public string IsDelImgWeb5 { get { return txtActBid5.Text; } }
        public string IsDelDealPromoImage { get { return hidimgDealPromoImage.Value; } }
        public string IsDelDiscountBannerImage { get { return hidimgDiscountBanner.Value; } }
        public string IsDelEventListImage { get { return hidimgEventList.Value; } }
        public string IsDelChannelListImage { get { return hidimgChannelList.Value; } }
        public string IsDelWebRelayImage { get { return hidimgWebRelay.Value; } }
        public string IsDelMobileRelayImage { get { return hidimgMobileRelay.Value; } }
        public string IsDelFbShareImage { get { return hidimgFbShare.Value; } }
        #endregion  property

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
                InitialControls();
            }
            RenderCKEditor();
            this.Presenter.OnViewLoaded();
        }

        protected void AddBrand(object sender, CommandEventArgs e)
        {
            int mode;
            if (int.TryParse(e.CommandArgument.ToString(), out mode))
            {
                Mode = mode;
            }
        }

        public void InitBrandCities(List<PponCity> pponCities)
        {
            ddlSearch.DataSource = pponCities;
            ddlSearch.DataTextField = "CityName";
            ddlSearch.DataValueField = "CityId";
            ddlSearch.DataBind();
            ddlSearch.Items.Insert(0, new ListItem("全頻道", ""));
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.Search != null)
            {
                this.Search(this, e);
            }
        }

        protected void ShowInWebChange(object sender, EventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;
            //取出品牌館ID
            int id;
            if (int.TryParse(checkBox.InputAttributes["value"], out id))
            {
                //先將品牌館ID儲存於BrandId參數中，之後供Presenter使用
                BrandId = id;
                if (SetBrandShowInWeb != null)
                {
                    //異動資料
                    SetBrandShowInWeb(sender, new DataEventArgs<bool>(checkBox.Checked));
                }
            }

        }

        protected void ShowInAppChange(object sender, EventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;
            //取出品牌館ID
            int id;
            if (int.TryParse(checkBox.InputAttributes["value"], out id))
            {
                //先將品牌館ID儲存於BrandId參數中，之後供Presenter使用
                BrandId = id;
                if (SetBrandShowInApp != null)
                {
                    //異動資料
                    SetBrandShowInApp(sender, new DataEventArgs<bool>(checkBox.Checked));
                }
            }
        }
        
        protected void gv_Brand_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is DataOrm.Brand)
            {
                DataOrm.Brand brand = (DataOrm.Brand)e.Row.DataItem;
                Label lblLink = (Label)e.Row.FindControl("lblLink");
                Literal litChannel = (Literal)e.Row.FindControl("litChannel");
                Label lbdiscountMsg = (Label)e.Row.FindControl("discountMsg");
                CheckBox checkBoxShowInApp = (CheckBox)e.Row.FindControl("ShowInApp");
                CheckBox checkBoxShowInWeb = (CheckBox)e.Row.FindControl("ShowInWeb");
                Label lblPreview = (Label)e.Row.FindControl("lblPreview");

                checkBoxShowInApp.InputAttributes.Add("value", brand.Id.ToString());
                checkBoxShowInWeb.InputAttributes.Add("value", brand.Id.ToString());

                var url = PromotionFacade.GetCurationTwoLink(brand);
                lblLink.Text = string.Format("<a href='{0}' target='_blank'>{0}</a>",url);
                lblPreview.Text = string.Format("<a href='{0}&p=show_me_the_preview' target='_blank'>預覽</a>", url);

                if (!string.IsNullOrEmpty(brand.DiscountList))
                {
                    string brandDIscountStatus = "";
                    if (GetStringBrandDIscountStatus != null)
                    {
                        brandDIscountStatus = GetStringBrandDIscountStatus(brand.Id);
                    }
                    lbdiscountMsg.Text = "綁折價券" + brandDIscountStatus;
                }

                litChannel.Text = string.Join("、", CmsRandomFacade.GetBrandCityNames(brand.Id));
            }
        }

        protected void gvBrandCommand(object sender, GridViewCommandEventArgs e)
        {
            int id;
            if (int.TryParse(e.CommandArgument.ToString(), out id))
            {
                BrandId = id;
                switch (e.CommandName)
                {
                    case "EditBrand":
                        if (GetBrand != null)
                            GetBrand(sender, e);
                        break;
                    case "ChangeStatus":
                        if (UpdateBrandStatus != null)
                            UpdateBrandStatus(sender, e);
                        break;
                }
            }
        }
        
        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (BrandId > 0)
            {
                if (FileUpload1.HasFile)
                {
                    List<string> result = new List<string>();
                    using (StreamReader reader = new StreamReader(FileUpload1.FileContent, System.Text.Encoding.Default))
                    {
                        do
                        {
                            string bid = reader.ReadLine();
                            result.Add(bid);
                        } while (reader.Peek() != -1);
                        reader.Close();
                    }

                    if (this.ImportBidList != null)
                    {
                        this.ImportBidList(this, new DataEventArgs<List<string>>(result));
                    }
                }
            }
            else
            {
                AlertMsg("請先儲存主檔，再進行商品匯入!");
            }
        }

        protected void btnSingalImport_Click(object sender, EventArgs e)
        {
            if (BrandId > 0)
            {
                if (!string.IsNullOrEmpty(txtImportBid.Text))
                {
                    List<string> result = new List<string>();
                    Guid bid = new Guid();
                    if (Guid.TryParse(txtImportBid.Text, out bid))
                    {
                        result.Add(bid.ToString());
                        if (this.ImportBidList != null)
                        {
                            this.ImportBidList(this, new DataEventArgs<List<string>>(result));
                        }
                    }
                    else
                    {
                        AlertMsg("單檔匯入BID格式錯誤!");
                        return;
                    }
                }
            }
            else
            {
                AlertMsg("請先儲存主檔，再進行商品匯入!");
            }
        }

        protected void rpt_BrandItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var dataItem = (ViewBrandItem)e.Item.DataItem;
                Literal litBic = (Literal)e.Item.FindControl("litBrandItemCategory");
                litBic.Text = GetItemCategory(dataItem.Id);
                HtmlAnchor anchor = (HtmlAnchor) e.Item.FindControl("aChangeCategory");
                anchor.Attributes.Add("onclick", string.Format("ShowChangeCategory('{3}','{0}','{1}','{2}');", dataItem.Id, dataItem.ItemName.Replace("'", "’"), litBic.Text.Replace("'", "’"), dataItem.MainId)); //若有"'" 符號會造成js錯誤
                
                Literal litDiscountStatus = (Literal)e.Item.FindControl("litDiscountStatus");

                if (dataItem.DiscountLimtType)
                {
                    litDiscountStatus.Text = "<br /><span  style='color:red;font-weight:bold'>(禁用折價券檔次)</span>";
                }
            }
        }

        protected void rptBrandItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            lab_Message.Text = string.Empty;
            int id;
            if (int.TryParse(e.CommandArgument.ToString(), out id))
            {
                ItemId = id;
                switch (e.CommandName)
                {
                    case "ChangeItemStatus":
                            UpdateBrandItemStatus(sender, e);
                        break;
                    case "ChangeSeqStatus":
                            UpdateSeqStatus(sender, e);
                        break;
                }
            }
            else
                lab_Message.Text = "Id錯誤";
        }
        /// <summary>
        /// 取得商品上檔狀態
        /// </summary>
        /// <param name="promoItem">主題活動商品</param>
        /// <returns></returns>
        protected string GetItemStatus(ViewBrandItem brand)
        {
            if (GetBrandItemStatus != null)
            {
                string itemStatus = GetBrandItemStatus(brand);
                return itemStatus;
            }
            return string.Empty;
        }

        protected string GetItemCategory(int brandItemId)
        {
            if (GetBrandItemCategory != null)
            {
                string itemCategory = GetBrandItemCategory(brandItemId);
                return itemCategory;
            }
            return string.Empty;
        }

        protected void btnSearchItem_Click(object sender, EventArgs e)
        {
            if (SearchBrandItem != null)
            {
                SearchBrandItem(sender, e);
            }
        }

        protected void btnAddBrandItemCategory_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtBrandItemCategory.Text))
            {
                if (SaveBrandItemCategory != null)
                {
                    SaveBrandItemCategory(sender, new DataEventArgs<KeyValuePair<int, string>>(new KeyValuePair<int, string>(BrandId, txtBrandItemCategory.Text)));
                }
            }
        }

        protected void btnReplaceCategory_Click(object sender, EventArgs e)
        {
            if (ddlReplaceCategory.SelectedIndex > -1)
            {
                if (BatchEditBrandItemCategory != null)
                {
                    txtSearchName.Text = "";
                    ddlSearchCategory.SelectedValue = "0";
                    BatchEditBrandItemCategory(sender, new DataEventArgs<KeyValuePair<string, string>>(new KeyValuePair<string, string>("Replace", ddlReplaceCategory.SelectedValue)));
                }
            }
        }

        protected void btnAddCategory_Click(object sender, EventArgs e)
        {
            if (ddlAddCategory.SelectedIndex > -1)
            {
                if (BatchEditBrandItemCategory != null)
                {
                    txtSearchName.Text = "";
                    ddlSearchCategory.SelectedValue = "0";
                    BatchEditBrandItemCategory(sender, new DataEventArgs<KeyValuePair<string, string>>(new KeyValuePair<string, string>("Add", ddlAddCategory.SelectedValue)));
                }
            }
        }
        
        protected void btnShowSort_Click(object sender, EventArgs e)
        {
            txtSearchName.Text = "";
            ddlSearchCategory.SelectedValue = "0";
            SearchBrandItem(sender, e);
            EnableManuallySort();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowSort", "showDiv($('#divItemSort'));", true);
        }

        protected void btnSaveSeq_Click(object sender, EventArgs e)
        {
            int mainId = Convert.ToInt32(hif_BrandId.Value);

            Dictionary<int, int> dictionaryIdAndSeq = new Dictionary<int, int>();//brand_item.Id, brand_item.Seq

            foreach (RepeaterItem item in rpt_BrandItem.Items)
            {
                HiddenField hidIdField = (HiddenField)item.FindControl("HidId");
                int id = hidIdField == null ? 0 : Convert.ToInt32(hidIdField.Value);
                if (id != 0)
                {
                    HiddenField hidSeqField = (HiddenField)item.FindControl("HidTempSeq");
                    int newSeq = hidSeqField == null ? 0 : Convert.ToInt32(hidSeqField.Value);

                    dictionaryIdAndSeq.Add(id, newSeq);
                }

            }
            if (UpdateBrandItemSeq != null)
            {
                UpdateBrandItemSeq(this, new DataEventArgs<KeyValuePair<int, Dictionary<int, int>>>
                    (new KeyValuePair<int, Dictionary<int, int>>(mainId, dictionaryIdAndSeq)));
            }
        }

        protected void btnSaveCategorySeq_Click(object sender, EventArgs e)
        {
            int mainId = Convert.ToInt32(hif_BrandId.Value);

            Dictionary<int, int> dictionaryIdAndSeq = new Dictionary<int, int>();//brand_item.Id, brand_item.Seq

            foreach (RepeaterItem item in rpCategory.Items)
            {
                HiddenField hidIdField = (HiddenField)item.FindControl("HidId");
                int id = hidIdField == null ? 0 : Convert.ToInt32(hidIdField.Value);
                if (id != 0)
                {
                    HiddenField hidSeqField = (HiddenField)item.FindControl("HidTempSeq");
                    int newSeq = hidSeqField == null ? 0 : Convert.ToInt32(hidSeqField.Value);

                    dictionaryIdAndSeq.Add(id, newSeq);
                }

            }
            if (UpdateBrandItemCategorySeq != null)
            {
                UpdateBrandItemCategorySeq(this, new DataEventArgs<KeyValuePair<int, Dictionary<int, int>>>
                    (new KeyValuePair<int, Dictionary<int, int>>(mainId, dictionaryIdAndSeq)));
            }
        }

        protected void btnSaveCategoryName_Click(object sender, EventArgs e)
        {
            int mainId = Convert.ToInt32(hif_BrandId.Value);

            Dictionary<int, string> dictionaryIdAndName = new Dictionary<int, string>();//brand_item.Id, brand_item.Name

            foreach (RepeaterItem item in rpCategoryName.Items)
            {
                HiddenField hidIdField = (HiddenField)item.FindControl("HidId");
                TextBox categoryName = (TextBox)item.FindControl("txtCategoryName");
                int id = hidIdField == null ? 0 : Convert.ToInt32(hidIdField.Value);
                if (id != 0)
                {
                    dictionaryIdAndName.Add(id, categoryName.Text);
                }

            }
            if (dictionaryIdAndName.Count > 0)
            {
                UpdateBrandItemCategoryName(this, new DataEventArgs<KeyValuePair<int, Dictionary<int, string>>>
                    (new KeyValuePair<int, Dictionary<int, string>>(mainId, dictionaryIdAndName)));
            }
        }

        protected void btnDeleteCategory_Click(object sender, EventArgs e)
        {
            int mainId = Convert.ToInt32(hif_BrandId.Value);

            List<int> idList = new List<int>();

            foreach (RepeaterItem item in rpDeleteCategory.Items)
            {
                HiddenField hidIdField = (HiddenField)item.FindControl("HidId");
                CheckBox deleteType = item.FindControl("DeleteType") as CheckBox;
                if (deleteType != null)
                {
                    int id = hidIdField == null ? 0 : Convert.ToInt32(hidIdField.Value);
                    if (deleteType.Checked && id != 0)
                    {
                        idList.Add(id);
                    }
                }

            }
            if (idList.Count > 0)
            {
                DeleteBrandItemCategory(this, new DataEventArgs<KeyValuePair<int, List<int>>>
                    (new KeyValuePair<int, List<int>>(mainId, idList)));
            }
        }

        protected void btnDeleteItemList_Click(object sender, EventArgs e)
        {
            if (DeleteBrandItemList != null)
            {
                DeleteBrandItemList(sender, e);
            }
        }
        
        protected void btnExportBrandItemBid_Click(object sender, EventArgs e)
        {
            ExportTxt();
        }
        #endregion page

        #region method

        protected void SaveBrandMain(object sender, EventArgs e)
        {
            string errMsg = CheckBrandMain();
            if (!string.IsNullOrWhiteSpace(errMsg)) {
                AlertMsg(errMsg);
                return;
            }

            DateTime d_start, d_end;
            if (DateTime.TryParse(tbx_Start.Text + " " + ddl_StartH.SelectedValue + ":" + ddl_Startm.SelectedValue, out d_start) && DateTime.TryParse(tbx_End.Text + " " + ddl_EndH.SelectedValue + ":" + ddl_Endm.SelectedValue, out d_end))
            {
                if (d_start > d_end)
                {
                    AlertMsg("起訖時間有誤!");
                    return;
                }

                lab_Message.Text = string.Empty;
                if (string.IsNullOrEmpty(tbx_BgColor.Text) && string.IsNullOrEmpty(tbx_BgPic.Text))
                {
                    AlertMsg("請填入背景圖或背景色");
                }
                else
                {
                    BrandUpdateModel model = new BrandUpdateModel();

                    DataOrm.Brand brand = new DataOrm.Brand();
                    if (Mode == 2)
                    {
                        brand.Id = BrandId;
                    }
                    else
                    {
                        //新增 設定預設值
                        brand.ShowInWeb = true;
                        brand.ShowInApp = false;
                    }


                    //為求快速使用原欄位名稱不更改
                    brand.Mk1ActName = txtActBid1.Text;
                    brand.Mk2ActName = txtActBid2.Text;
                    brand.Mk3ActName = txtActBid3.Text;
                    brand.Mk4ActName = txtActBid4.Text;
                    brand.Mk5ActName = txtActBid5.Text;

                    
                    brand.BrandName = tbx_BrandName.Text;
                    brand.Cpa = txt_Cpa.Text;
                    brand.Url = txt_Url.Text;
                    brand.StartTime = d_start;
                    brand.EndTime = d_end;
                    brand.Bgpic = tbx_BgPic.Text;
                    brand.Bgcolor = tbx_BgColor.Text.Replace("#", string.Empty);
                    brand.Status = false;
                    brand.CreateTime = DateTime.Now;
                    brand.Creator = UserName;
                    brand.Description = txtDescription.Text;
                    brand.DealPromoTitle = txtDealPromoTitle.Text;
                    brand.DealPromoDescription = txtDealPromoDescription.Text;
                    brand.DiscountList = txtDiscountList.Text;
                    brand.MetaDescription = txt_MetaDescription.Text.Trim();

                    Guid bid1, bid2, bid3, bid4;
                    if (!string.IsNullOrEmpty(txtHotItem1.Text))
                    {
                        if (Guid.TryParse(txtHotItem1.Text, out bid1))
                        {
                            brand.HotItemBid1 = bid1;
                        }
                    }
                    if (!string.IsNullOrEmpty(txtHotItem2.Text))
                    {
                        if (Guid.TryParse(txtHotItem2.Text, out bid2))
                        {
                            brand.HotItemBid2 = bid2;
                        }
                    }
                    if (!string.IsNullOrEmpty(txtHotItem3.Text))
                    {
                        if (Guid.TryParse(txtHotItem3.Text, out bid3))
                        {
                            brand.HotItemBid3 = bid3;
                        }
                    }
                    if (!string.IsNullOrEmpty(txtHotItem4.Text))
                    {
                        if (Guid.TryParse(txtHotItem4.Text, out bid4))
                        {
                            brand.HotItemBid4 = bid4;
                        }
                    }
                    brand.BtnOriginalUrl = tbx_BtnOriginalUrl.Text;
                    brand.BtnHoverUrl = tbx_BtnHoverUrl.Text;
                    brand.BtnActiveUrl = tbx_BtnActiveUrl.Text;
                    brand.BtnOriginalFontColor = tbx_BtnOriginalFontColor.Text.Replace("#", string.Empty);
                    brand.BtnHoverFontColor = tbx_BtnHoverFontColor.Text.Replace("#", string.Empty);
                    brand.BtnActiveFontColor = tbx_BtnActiveFontColor.Text.Replace("#", string.Empty);
                    
                    List<int> banners = new List<int>();
                    foreach (ListItem item in chbBannerLocation.Items)
                    {
                        if (item.Selected)
                        {
                            banners.Add(int.Parse(item.Value));
                        }
                    }
                    model.BannerType = banners;

                    model.UpdateCmsRandomCities = new UpdateCmsRandomCitiesModel
                    {
                        Update = true,
                        CityIds = new List<int>()
                    };
                    foreach (ListItem item in cblCities.Items)
                    {
                        if (item.Selected)
                        {
                            model.UpdateCmsRandomCities.CityIds.Add(int.Parse(item.Value));
                        }
                    }

                    if (SaveBrand != null)
                    {
                        model.MainBrand = brand;
                        model.MainPicPostedFile = fuMainPic.PostedFile.ContentLength > 0
                            ? fuMainPic.PostedFile
                            : null;
                        model.MainBackgroundPicPostedFile = fuMainBackgroundPic.PostedFile.ContentLength > 0
                            ? fuMainBackgroundPic.PostedFile
                            : null;
                        model.MobileMainPicPostedFile = fuMobileMainPic.PostedFile.ContentLength > 0
                            ? fuMobileMainPic.PostedFile
                            : null;
                        model.AppBannerPostedFile = fuAppBannerImage.PostedFile.ContentLength > 0
                            ? fuAppBannerImage.PostedFile
                            : null;
                        model.AppPromoPostedFile = fuAppPromoImage.PostedFile.ContentLength > 0
                            ? fuAppPromoImage.PostedFile
                            : null;

                        model.MarketingWebPostedFile1 = txtActBid1.Text;
                        model.MarketingWebPostedFile2 = txtActBid2.Text;
                        model.MarketingWebPostedFile3 = txtActBid3.Text;
                        model.MarketingWebPostedFile4 = txtActBid4.Text;
                        model.MarketingWebPostedFile5 = txtActBid5.Text;

                        model.DealPromoImagePostedFile = fuimgDealPromoImage.PostedFile.ContentLength > 0
                            ? fuimgDealPromoImage.PostedFile
                            : null;
                        model.DiscountBannerImagePostedFile = fuimgDiscountBanner.PostedFile.ContentLength > 0
                            ? fuimgDiscountBanner.PostedFile
                            : null;
                        model.EventListImagePostedFile = fuimgEventList.PostedFile.ContentLength > 0
                            ? fuimgEventList.PostedFile
                            : null;
                        model.ChannelListImagePostedFile = fuimgChannelList.PostedFile.ContentLength > 0
                            ? fuimgChannelList.PostedFile
                            : null;
                        model.WebRelayImagePostedFile = fuimgWebRelay.PostedFile.ContentLength > 0
                            ? fuimgWebRelay.PostedFile
                            : null;
                        model.MobileRelayImagePostedFile = fuimgMobileRelay.PostedFile.ContentLength > 0
                            ? fuimgMobileRelay.PostedFile
                            : null;
                        model.FbShareImagePostedFile = fuimgFbShare.PostedFile.ContentLength > 0
                            ? fuimgFbShare.PostedFile
                            : null;
                        SaveBrand(sender, new DataEventArgs<BrandUpdateModel>(model));

                        //若在正式有更新FB分享預覽圖，自動刷新FB分享網址預覽圖片
                        if (fuimgFbShare.PostedFile.ContentLength > 0 && Request.Url.Host.ToString().Contains("www.17life.com"))
                        {
                            FacebookUtility.CleanFacebookPictureCache(PromotionFacade.GetCurationTwoLink(brand),"");
                        }
                    }
                }
            }
            else
                AlertMsg("日期格式錯誤!");
        }

        public void SetBrand(DataOrm.Brand brand)
        {
            Mode = 2;
            tbx_BrandName.Text = brand.BrandName;
            txt_Cpa.Text = brand.Cpa;
            txt_Url.Text = brand.Url;
            tbx_Start.Text = brand.StartTime.ToString("MM/dd/yyyy");
            ddl_StartH.SelectedValue = brand.StartTime.ToString("HH");
            ddl_Startm.SelectedValue = brand.StartTime.ToString("mm");
            tbx_End.Text = brand.EndTime.ToString("MM/dd/yyyy");
            ddl_EndH.SelectedValue = brand.EndTime.ToString("HH");
            ddl_Endm.SelectedValue = brand.EndTime.ToString("mm");

            //過渡期程式，之後需移除
            string tempMainPic = string.IsNullOrEmpty(brand.MainPic) ? string.Empty : ImageFacade.GetHtmlFirstSrc(brand.MainPic);
            if (tempMainPic == string.Empty) tempMainPic = brand.MainPic;
            MainPic.ImageUrl = ImageFacade.GetMediaPath(tempMainPic, MediaType.DealPromoImage);

            MainBackgroundPic.ImageUrl = ImageFacade.GetMediaPath(brand.MainBackgroundPic, MediaType.DealPromoImage);

            //過渡期程式，之後需移除
            string tempMobileMainPic = string.IsNullOrEmpty(brand.MobileMainPic) ? string.Empty : ImageFacade.GetHtmlFirstSrc(brand.MobileMainPic);
            if (tempMobileMainPic == string.Empty) tempMobileMainPic = brand.MobileMainPic;
            MobileMainPic.ImageUrl = ImageFacade.GetMediaPath(tempMobileMainPic, MediaType.DealPromoImage);

            tbx_BgPic.Text = brand.Bgpic;
            tbx_BgColor.Text = brand.Bgcolor;
            AppBannerImage.ImageUrl = ImageFacade.GetMediaPath(brand.AppBannerImage, MediaType.EventPromoAppBanner);
            AppPromoImage.ImageUrl = ImageFacade.GetMediaPath(brand.AppPromoImage, MediaType.EventPromoAppMainImage);
            txtActBid1.Text = brand.Mk1ActName;
            txtActBid2.Text = brand.Mk2ActName;
            txtActBid3.Text = brand.Mk3ActName;
            txtActBid4.Text = brand.Mk4ActName;
            txtActBid5.Text = brand.Mk5ActName;
            txtHotItem1.Text = brand.HotItemBid1.ToString();
            txtHotItem2.Text = brand.HotItemBid2.ToString();
            txtHotItem3.Text = brand.HotItemBid3.ToString();
            txtHotItem4.Text = brand.HotItemBid4.ToString();
            tbx_BtnOriginalUrl.Text = brand.BtnOriginalUrl;
            tbx_BtnHoverUrl.Text = brand.BtnHoverUrl;
            tbx_BtnActiveUrl.Text = brand.BtnActiveUrl;
            tbx_BtnOriginalFontColor.Text = brand.BtnOriginalFontColor;
            tbx_BtnHoverFontColor.Text = brand.BtnHoverFontColor;
            tbx_BtnActiveFontColor.Text = brand.BtnActiveFontColor;
            txtDescription.Text = brand.Description;
            txtDealPromoTitle.Text = brand.DealPromoTitle;
            imgDealPromoImage.ImageUrl = ImageFacade.GetMediaPath(brand.DealPromoImage, MediaType.DealPromoImage);
            txtDealPromoDescription.Text = brand.DealPromoDescription;
            txtDiscountList.Text = brand.DiscountList;
            imgDiscountBanner.ImageUrl = ImageFacade.GetMediaPath(brand.DiscountBannerImage, MediaType.DealPromoImage);
            imgEventList.ImageUrl = ImageFacade.GetMediaPath(brand.EventListImage, MediaType.DealPromoImage);
            imgChannelList.ImageUrl = ImageFacade.GetMediaPath(brand.ChannelListImage, MediaType.DealPromoImage);
            imgWebRelay.ImageUrl = ImageFacade.GetMediaPath(brand.WebRelayImage, MediaType.DealPromoImage);
            imgMobileRelay.ImageUrl = ImageFacade.GetMediaPath(brand.MobileRelayImage, MediaType.DealPromoImage);
            imgFbShare.ImageUrl = ImageFacade.GetMediaPath(brand.FbShareImage, MediaType.DealPromoImage);

            if ((brand.BannerType & (int)EventBannerType.MainDown) > 0)
            {
                chbBannerLocation.Items.FindByValue(((int)EventBannerType.MainDown).ToString()).Selected = true;
            }

            if ((brand.BannerType & (int)EventBannerType.ChannelUp) > 0)
            {
                chbBannerLocation.Items.FindByValue(((int)EventBannerType.ChannelUp).ToString()).Selected = true;
            }

            if ((brand.BannerType & (int)EventBannerType.ChannelDown) > 0)
            {
                chbBannerLocation.Items.FindByValue(((int)EventBannerType.ChannelDown).ToString()).Selected = true;
            }

            txt_MetaDescription.Text = brand.MetaDescription;
        }

        public void SetBrandCities(List<PponCity> pponCities, ViewCmsRandomCollection cmsRandoms)
        {
            cblCities.DataSource = pponCities;
            cblCities.DataTextField = "CityName";
            cblCities.DataValueField = "CityId";
            cblCities.DataBind();
            foreach (var cmsRandom in cmsRandoms)
            {
                cblCities.Items.FindByValue(cmsRandom.CityId.ToString()).Selected = true;
            }
        }

        public void SetBrandItemCategory(BrandItemCategoryCollection bicList)
        {
            var excludeAllList = bicList.Where(x => x.Name != "全部").OrderBy(x => x.Seq);
            
            ddlSearchCategory.DataSource = excludeAllList;
            ddlSearchCategory.DataTextField = "Name";
            ddlSearchCategory.DataValueField = "Id";
            ddlSearchCategory.DataBind();
            ddlSearchCategory.Items.Insert(0, new ListItem("全部商品", "0"));
            
            ddlReplaceCategory.DataSource = excludeAllList;
            ddlReplaceCategory.DataTextField = "Name";
            ddlReplaceCategory.DataValueField = "Id";
            ddlReplaceCategory.DataBind();
            
            ddlAddCategory.DataSource = excludeAllList;
            ddlAddCategory.DataTextField = "Name";
            ddlAddCategory.DataValueField = "Id";
            ddlAddCategory.DataBind();

            rpCategory.DataSource = rpCategoryName.DataSource = rpDeleteCategory.DataSource = excludeAllList;
            rpCategory.DataBind();
            rpCategoryName.DataBind();
            rpDeleteCategory.DataBind();


            SetBrandItemCategoryForDialog(bicList);
            txtBrandItemCategory.Text = "";
        }

        public void SetBrandItemCategoryForDialog(BrandItemCategoryCollection bicList)
        {
            var excludeAllList = bicList.Where(x => x.Name != "全部").OrderBy(x => x.Seq);
            cbBrandItemCategory.Items.Clear();
            foreach (var a in excludeAllList)
            {
                ListItem item = new ListItem(a.Name, a.Id.ToString());
                item.Attributes.Add("someValue", a.Id.ToString()); //處理 framework 4.0以下 checkbox 沒有value欄位的問題
                cbBrandItemCategory.Items.Add(item);
            }
        }

        public void SetBrandList(List<DataOrm.Brand> brands)
        {
            if (brands != null)
            {
                gv_Brand.DataSource = brands.OrderByDescending(x => x.Id);
                gv_Brand.DataBind();
            }
            else
            {
                gv_Brand.DataSource = null;
                gv_Brand.DataBind();
                AlertMsg("抱歉，查無任何資料!");
            }
        }

        public void SetBrandItemList(ViewBrandItemCollection items)
        {
            ItemCount = items.Count;
            if (ItemCount > 0)
            {
                rpt_BrandItem.DataSource = items.OrderBy(x => x.Seq);
                rpt_BrandItem.DataBind();
                txtBrandItemCategory.Text = "";
                ph_BrandItemInfo.Visible = true;
                lab_emptyItem.Text = "";
            }
            else
            {
                rpt_BrandItem.DataSource = null;
                rpt_BrandItem.DataBind();

                if (BrandId == 0)
                {
                    ph_BrandItemInfo.Visible = false;
                }
                lab_emptyItem.Text = "抱歉，查無任何資料!";
            }
        }

        public void ChangePanel(int mode)
        {
            pan_BrandList.Visible = pan_BrandEdit.Visible = false;
            switch (mode)
            {
                case 0:
                    pan_BrandList.Visible = true;
                    this.Presenter.OnViewInitialized();
                    break;
                case 1: //add brand
                    btn_Save.Text = "新增";
                    tbx_BrandName.Text = txt_Cpa.Text = txt_Url.Text = tbx_Start.Text = tbx_End.Text = tbx_BgPic.Text = tbx_BgColor.Text = txtHotItem1.Text = txtHotItem2.Text = txtHotItem3.Text = txtHotItem4.Text = tbx_BtnOriginalUrl.Text = tbx_BtnHoverUrl.Text = tbx_BtnActiveUrl.Text =
                        tbx_BtnOriginalFontColor.Text = tbx_BtnHoverFontColor.Text = tbx_BtnActiveFontColor.Text = txtDescription.Text = txtDealPromoTitle.Text = txtDealPromoDescription.Text = txtDiscountList.Text = txt_MetaDescription.Text = string.Empty;
                    ddl_EndH.SelectedIndex = ddl_Endm.SelectedIndex = ddl_StartH.SelectedIndex = ddl_Startm.SelectedIndex = -1;
                    MainPic.ImageUrl = MainBackgroundPic.ImageUrl = MobileMainPic.ImageUrl = AppBannerImage.ImageUrl = AppPromoImage.ImageUrl = string.Empty;
                    pan_BrandEdit.Visible = true;

                    //網址預設值
                    long ticks = DateTime.Now.Ticks;
                    string id = Helper.GetNewGuid(ticks).ToString();
                    txt_Url.Text = DateTime.Now.Year + "_" + id.Substring(0, id.IndexOf('-'));
                    tbx_BgColor.Text = "#F2F2F0";
                    txt_Cpa.Text = "17_Curation";

                    ph_BrandItemInfo.Visible = false;
                    lab_emptyItem.Text = "抱歉，查無任何資料!";
                    break;
                case 2: //edit brand
                    btn_Save.Text = "更新";
                    InitialControls();
                    pan_BrandEdit.Visible = true;
                    break;
            }
        }

        public void EnableManuallySort()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "EnableManuallySort", "EnableManuallySort();", true);
        }

        public void AlertMsg(string Msg)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertImportMsg", string.Format("AlertImportMsg(\"{0}\");", Msg), true);
        }

        [WebMethod]
        public static string ChangeItemCategory(int bid,int biid,string clist)
        {
            string msg = PponFacade.ChangeBrandItemCategory(bid, biid, clist);

            if (string.IsNullOrEmpty(msg))
            {
                var cacheKey = "B" + bid;
                SystemFacade.ClearCacheByObjectNameAndCacheKey("ApiEventPromo", cacheKey);
                return new JsonSerializer().Serialize((new { Success = true, Message = "分類修改完成" }));
            }
            else
                return new JsonSerializer().Serialize((new { Success = false, Message = msg }));
        }

        [WebMethod]
        public static string GetBid(int brandI)
        {
            var msg = PponFacade.GetBidByBrandId(brandI).Take(10);
            return new JsonSerializer().Serialize((new { Success = false, Message = msg }));
        }

        #endregion method

        #region private method
        private void RenderCKEditor()
        {
            ScriptManager.RegisterStartupScript(this, typeof(Button), "Description", "$('#" + txtDescription.ClientID + "').ckeditor();", true);
            ScriptManager.RegisterStartupScript(this, typeof(Button), "DealPromoDescription", "$('#" + txtDealPromoDescription.ClientID + "').ckeditor();", true);
            txtDescription.Text = HttpUtility.HtmlDecode(txtDescription.Text);
            txtDealPromoDescription.Text = HttpUtility.HtmlDecode(txtDealPromoDescription.Text);
        }
        private void InitialControls()
        {
            for (int i = 0; i <= 23; i++)
            {
                ddl_StartH.Items.Add(new ListItem()
                {
                    Text = i.ToString("D2"),
                    Value = i.ToString("D2")
                });
                ddl_EndH.Items.Add(new ListItem()
                {
                    Text = i.ToString("D2"),
                    Value = i.ToString("D2")
                });
            }
            for (int i = 0; i <= 59; i++)
            {
                ddl_Startm.Items.Add(new ListItem()
                {
                    Text = i.ToString("D2"),
                    Value = i.ToString("D2")
                });
                ddl_Endm.Items.Add(new ListItem()
                {
                    Text = i.ToString("D2"),
                    Value = i.ToString("D2")
                });
            }
            ddl_StartH.DataBind();
            ddl_EndH.DataBind();
            ddl_Startm.DataBind();
            ddl_Endm.DataBind();
            
            chbBannerLocation.Items.Clear();
            chbBannerLocation.Items.Add(new ListItem("APP首頁下方產品列表輪播", ((int) EventBannerType.MainDown).ToString()));
            chbBannerLocation.Items.Add(new ListItem("APP指定頻道上方列表Banner", ((int)EventBannerType.ChannelUp).ToString()));
            chbBannerLocation.Items.Add(new ListItem("APP指定頻道產品列表輪播", ((int)EventBannerType.ChannelDown).ToString()));
            chbBannerLocation.DataBind();            
        }
        private string CheckBrandMain()
        {
            string errMsg = "";
            bool haveAct = false;

            if (!string.IsNullOrEmpty(txtDiscountList.Text))
            {
                string[] DiscountList = txtDiscountList.Text.Trim().Split(",");
                foreach (var dId in DiscountList) {
                    int i = 0;
                    int.TryParse(dId, out i);
                    if (i == 0) {
                        errMsg += "折價券活動ID: " + dId + " 設定錯誤<br>";
                    }
                }
            }

            //fuAppBannerImage 640x320
            if (fuAppBannerImage.PostedFile.ContentLength > 0) {
                Stream stream = fuAppBannerImage.PostedFile.InputStream;
                System.Drawing.Image image = System.Drawing.Image.FromStream(stream);

                if (image.Width != 640 || image.Height != 320)
                {
                    errMsg += "頻道列表banner 尺寸應為 640x320 <br>";
                }
            }

            //fuAppPromoImage 640x356
            if (fuAppPromoImage.PostedFile.ContentLength > 0)
            {
                Stream stream = fuAppPromoImage.PostedFile.InputStream;
                System.Drawing.Image image = System.Drawing.Image.FromStream(stream);

                if (image.Width != 640 || image.Height != 356)
                {
                    errMsg += "App首頁banner 尺寸應為 640x356 <br>";
                }
            }

            if (!string.IsNullOrEmpty(txtActBid1.Text) || !string.IsNullOrEmpty(txtActBid2.Text) ||
                !string.IsNullOrEmpty(txtActBid3.Text) || !string.IsNullOrEmpty(txtActBid4.Text))
            {
                haveAct = true;
            }

            if (!string.IsNullOrEmpty(txtHotItem1.Text) || !string.IsNullOrEmpty(txtHotItem2.Text) ||
                !string.IsNullOrEmpty(txtHotItem3.Text) || !string.IsNullOrEmpty(txtHotItem4.Text))
            {
                if (haveAct)
                {
                    if (string.IsNullOrEmpty(txtHotItem1.Text) ||
                    string.IsNullOrEmpty(txtHotItem2.Text) ||
                    string.IsNullOrEmpty(txtHotItem3.Text) ||
                    string.IsNullOrEmpty(txtHotItem4.Text))
                    {
                        errMsg += "主打商品必須設定滿四個商品! <br>";
                    }
                }
                else
                {
                    errMsg += "行銷活動未設定,無法設定主打商品! <br>";
                }
            }

            if (!string.IsNullOrEmpty(txtDiscountList.Text) || fuimgDiscountBanner.PostedFile.ContentLength > 0 || (!string.IsNullOrEmpty(imgDiscountBanner.ImageUrl) && hidimgDiscountBanner.Value == ""))
            {
                if (string.IsNullOrEmpty(txtDiscountList.Text) || (fuimgDiscountBanner.PostedFile.ContentLength == 0 && imgDiscountBanner.ImageUrl == "") || (fuimgDiscountBanner.PostedFile.ContentLength == 0 && hidimgDiscountBanner.Value == "true" && imgDiscountBanner.ImageUrl != ""))
                {
                    errMsg += "立刻歸戶折價券活動未設定完全! <br>";
                }
            }

            return errMsg;
        }        
        public void ExportTxt()
        {
            try
            {
                HttpContext.Current.Response.Clear(); //清除buffer
                HttpContext.Current.Response.ClearHeaders(); //清除 buffer 表頭
                HttpContext.Current.Response.Buffer = false;
                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=BrandItem.txt");
                                
                foreach (RepeaterItem item in rpt_BrandItem.Items)
                {
                    HiddenField hidIdField = (HiddenField)item.FindControl("HidBid");

                    if (!string.IsNullOrEmpty(hidIdField.Value))
                    {
                        HttpContext.Current.Response.Write(hidIdField.Value + Environment.NewLine);
                    }
                }

                // 將檔案輸出
                HttpContext.Current.Response.Flush();
                // 強制 Flush buffer 內容
                HttpContext.Current.Response.End();
            }
            catch (Exception ex)
            {
                AlertMsg(ex.ToString());
            }
        }
        #endregion
    }
}
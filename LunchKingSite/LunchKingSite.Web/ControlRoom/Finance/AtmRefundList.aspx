﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="AtmRefundList.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Finance.AtmRefundList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
<link type="text/css" href="../../Tools/js/css/jquery.tooltip.css" rel="stylesheet" />	
<link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8.13.custom.css" rel="stylesheet" />
<link href="../../Tools/js/css/jqGrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
<style type="text/css" >
.ui-jqgrid-sortable{
font-size:11px;
}
</style>
<script type="text/javascript" src="//www.google.com/jsapi"></script>  
<script type="text/javascript">
    google.load("jquery", "1.7.0");
    google.load("jqueryui", "1.8.16"); 
</script>

<script src="../../Tools/js/grid.locale-en.js" type="text/javascript"></script>
<script src="../../Tools/js/jquery.jqGrid.min.js" type="text/javascript"></script>
    
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        jQuery("#list2").jqGrid({
            datatype: "local",
            colNames: [/*'Si',*/'訂單時間', '使用者', '訂單序號'/*, '交易序號'*/, '退款金額', '退款日', '帳戶名稱', '身分證/統一編號', '銀行', '分行', '銀行代碼', '帳號', '電話', '退貨結果'],
            colModel: [
            //{ name: 'Si', index: 'Si', width: 40 },
        { name: 'CreateTime', index: 'CreateTime', width: 210 },
   		{ name: 'UserName', index: 'UserName' },
        { name: 'OrderGuid', index: 'OrderGuid', width: 100 },
        //{ name: 'PaymentId', index: 'PaymentId', width: 100 },
        { name: 'Amount', index: 'Amount', width: 80, align: "right" },
        { name: 'TargetTime', index: 'TargetTime', width: 210 },
        { name: 'AccountName', index: 'AccountName', align: "right" },
        { name: 'ID', index: 'ID', width: 100, align: "right" },
        { name: 'BankName', index: 'BankName', sortable: false },
        { name: 'BranchName', index: 'BranchName', sortable: false },
        { name: 'BankCode', index: 'BankCode', width: 80, sortable: false },
        { name: 'AccountNumber', index: 'AccountNumber',  align: "right" },
        { name: 'Phone', index: 'Phone', width: 105 },
        { name: 'FailReason', index: 'FailReason', sortable: false }
   	],
            rowNum: 10,
            rowList: [10, 25, 50],
            pager: '#pager2',
            sortname: 'CreateTime',
            height: "auto",
            autowidth: true,
            viewrecords: true,
            sortorder: "desc",
            caption: "退款資料",
            loadonce: true,
            rownumbers: true,
            emptyrecords: "查無資料"
        });
        jQuery("#list2").jqGrid('navGrid', '#pager2', { edit: false, add: false, del: false });
        $.getJSON('atmdatacenter.ashx',
        function (data) {
            for (var i = 0; i <= data.length+1; i++) {
                jQuery("#list2").jqGrid('addRowData', i + 1, data[i]);
            }
            $('#list2').trigger("reloadGrid");
        });
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
僅列出30天內資料

<table id="list2" style="font-size:11px;width:1020px"></table>
<div id="pager2"></div>

</asp:Content>

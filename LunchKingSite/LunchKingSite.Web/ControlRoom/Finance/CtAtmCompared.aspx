﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/BackendBootstrap.master" AutoEventWireup="true" CodeBehind="CtAtmCompared.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Finance.CtAtmCompared" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:FileUpload ID="fileImport" runat="server" /><asp:Button ID="btnImport" runat="server" Text="確定匯入" OnClick="btnImport_Click" />
    <hr />
    <asp:TextBox ID="txtDate" runat="server"></asp:TextBox><asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" />
    <asp:Repeater ID="rptCtAtm" runat="server">
        <HeaderTemplate>
            <ul>
        </HeaderTemplate>
        <ItemTemplate>
            <li>
                <label class="unit-label rd-unit-label">
                    <%# ((string)Container.DataItem) %>&nbsp;&nbsp;:
                </label>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
</asp:Content>



﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="EntrustSellReceiptList.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Finance.EntrustSellReceiptList" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="SSC">
    <script src="<%=ResolveClientUrl("~/Tools/js/jquery-ui.1.8.min.js") %>" type="text/javascript"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="../../Tools/js/homecook.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />

    <style type="text/css">
        #search_table td
        {
            padding: 5px;
        }
        .list_table
        {
            border-collapse: collapse;
            width: 1900px;
        }
        .list_table thead tr
        {
            background-color: #6b696b;
            height: 30px;
            color: white;
            font-size: 14px;
            font-weight: bold;
        }
        .list_table td
        {
            padding: 5px;
        }
        .list_table .row0
        {
            background-color: #f7f7de;
            height: 30px;
        }
        .list_table .row1
        {
            background-color: white;
            height: 30px;
        }
        .list_table .cell_text
        {
            text-align: left;
        }
        .list_table .cell_num
        {
            text-align: right;
        }
        .btn_search
        {
            letter-spacing: 3px;
            padding: 3px;
            margin-right:20px;
            cursor: pointer;
            width:80px;
        }
        .btn_export
        {
            margin-right:5px;
            padding: 3px;
            cursor: pointer;
            width:80px;
        }
        .btn_book
        {
            letter-spacing: 3px;
            padding: 3px 3px 3px 10px;
            cursor: pointer;
            margin-left: 20px;
        }
        td.booked_cell, td.center
        {
            text-align: center;
        }
        .ui-datepicker 
        {
            font-size:10px;
        }
        input.date
        {
            width: 80px;
        }
        .message 
        {
            font-size:13px;
            color: Blue;
            display: block;
        }
    </style>
    <script type="text/javascript">
        $().ready(function () {
            setupdatepicker('<%=tbSTime.ClientID%>', '<%=tbETime.ClientID%>', 3);
            $('.btn_export').click(function (evt) {
                $.blockUI({ message: "<h2 style='padding:20px; letter-spacing:2px'>資料傳輸中...</h2>", css: { left: ($(window).width() - 200) / 2 + 'px', width: '200px'} });
            });
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <h2>
        代收轉付收據查詢</h2>
    <fieldset style="border: double 3px black; width: 650px;">
        <table style="width: 650px;" id="search_table">
            <tr>
                <td colspan="2">
                    <asp:DropDownList ID="ddlQueryItem" runat="server">
                        <asp:ListItem Text="訂單編號" Value="OrderId" />
                        <asp:ListItem Text="會員帳號" Value="MemberId" />
                        <asp:ListItem Text="會員姓名" Value="MemberName" />
                        <asp:ListItem Text="廠商別" Value="SellerName" />
                        <asp:ListItem Text="收據編號" Value="ReceiptCode" />                        
                        <asp:ListItem Text="BID/商品QC" Value="ProductQc" Selected="true" />                        
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearchInput" runat="server" style="width:265px" />
                </td>
            </tr>
            <tr>
                <td class="style2" valign="middle" colspan="2">
                    核銷日期：<asp:TextBox ID="tbSTime" runat="server" CssClass="date" />
                    ～
                    <asp:TextBox ID="tbETime" runat="server" CssClass="date" />
                </td>
            </tr>
            <tr>
                <td class="style1" nowrap="nowrap" colspan="2">
                    <asp:CheckBox ID="chkIsPhysical" runat="server" Text="僅列出需寄送" />
                    <asp:CheckBox ID="chkIsExported" runat="server" Text="已匯出給科威" style="margin-left:10px" />
                    <asp:CheckBox ID="chkHasReceiptCode" runat="server" Text="已匯入代轉單號" style="margin-left:10px" />
                </td>
            </tr>
            <tr>
                <td valign="middle" nowrap="nowrap">
                </td>
                <td style="text-align: right; margin-right: 40px">
                    <asp:Button ID="btnSearch" CssClass="btn_search" runat="server" Text="搜尋" OnClick="btnSearch_Click" />
                </td>
            </tr>
        </table>        
    </fieldset>
    <br />
    <asp:Button ID="btnOutput" CssClass="btn_export" runat="server" Text="匯出資料" OnClick="btnExport_Click" />
    <span style="font-size:15px; color:Navy">匯出所有未匯出的資料</span>
    <br />
    <asp:Label ID="lbMessage" runat="server" CssClass="message" EnableViewState="false" />
    <div style="margin-top: 15px;">
        <asp:Repeater ID="repReceipts" runat="server" EnableViewState="false">
            <HeaderTemplate>
                <table class="list_table" border="1" cellspacing="0">
                    <thead>
                        <tr>
                            <th scope="col">
                                已匯出
                            </th>
                            <th scope="col">
                                收據編號
                            </th>                                                        
                            <th scope="col">
                                核銷日期
                            </th>
                            <th scope="col">
                                訂單編號
                            </th>
                            <th scope="col">
                                BID/商品QC
                            </th>
                            <th scope="col">
                                買受人抬頭
                            </th>
                            <th scope="col">
                                摘要
                            </th>
                            <th scope="col">
                                統一編號
                            </th>
                            <th scope="col">
                                買受人地址
                            </th>
                            <th scope="col">
                                帳款科目
                            </th>
                            <th scope="col">
                                數量
                            </th>
                            <th scope="col">
                                單價
                            </th>
                            <th scope="col">
                                銷項金額(刷卡金額)
                            </th>
                            <th scope="col">
                                進項金額
                            </th>
                            <th scope="col">
                                寄送狀態
                            </th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="row<%#Container.ItemIndex % 2 %>">
                    <td class="center">
                        <%#(bool)Eval("IsExported") ? "<input type=checkbox checked disabled onclick='return false' />" : "<input type=checkbox disabled onclick='return false' />"%>
                    </td>                    
                    <td class="cell_text">
                        <%#Eval("ReceiptCode")%>
                    </td>
                    <td class="cell_text">
                        <%#Eval("VerifiedTime","{0:yyyy/MM/dd}")%>
                    </td>
                    <td class="cell_text">
                        <%#Eval("OrderId")%>
                    </td>
                    <td class="cell_text">
                        <%#Eval("ProductQc")%>
                    </td>
                    <td class="cell_text">
                        <%#Eval("BuyerName")%>
                    </td>
                    <td class="cell_text">
                        <%#Eval("ItemName")%>
                    </td>
                    <td class="cell_text">
                        <%#Eval("ComId")%>
                    </td>
                    <td class="cell_text">
                        <%#Eval("BuyerAddress")%>
                    </td>
                    <td class="cell_text">
                        <%#Eval("AccountCode")%>
                    </td>
                    <td class="cell_text">
                        <%#Eval("ItemQuantity")%>
                    </td>
                    <td class="cell_text">
                        <%#Eval("UnitPrice", "{0:0}")%>
                    </td>
                    <td class="cell_text">
                        <%#Eval("AmountReceived", "{0:0}")%>
                    </td>
                    <td class="cell_text">
                        <%#Eval("AmountPaid", "{0:0}")%>
                    </td>
                    <td class="cell_text">
                        <%#(bool)Eval("IsPhysical") ? "需寄送" : "不用寄"%>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="AntiCreditcardFraud.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Finance.AntiCreditcardFraud" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link type="text/css" href="../../Tools/js/css/jquery.tooltip.css" rel="stylesheet" />	
<link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8.13.custom.css" rel="stylesheet" />
<link href="../../Tools/js/css/jqGrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
<style type="text/css" >
.ui-jqgrid-sortable{
font-size:11px;
}
.ui-pg-input{
height:20px;
}
.waterMarkText
{
    color:#CCCCCC;
}
.black-title{
    text-align:center;
    height: 35px;
    background: #e59e2c;
    color: #fff;
    line-height:32px;
}
.black-content{
    border: 1px solid #bbb;
    padding: 15px 10px;
}

</style>
<script src="../../Tools/js/json2.js" type="text/javascript"></script>
<script type="text/javascript" src="../../Tools/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
<script src="../../Tools/js/grid.locale-en.js" type="text/javascript"></script>
<script src="../../Tools/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../../Tools/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#divShowAllOrders").hide();
    })
    $(function () {
        $("#div_moc_list").hide();

        $("#ddlMain").change(function () {
            if ($("#ddlMain").val() == 'memberid' && IsShowAllOrders.value == 'True') {
                $("#divShowAllOrders").show();
            } else {
                $("#divShowAllOrders").hide();
                $("#chkShowAllOrders").attr("checked", false);
            }
        });

        if ($('#forceShowGrid').val() == '1') {
            runGrid();
        }

        //setDdlResult();
        setDdlTime();
        setinputTimeDefault();
        $("#btnSearch").click(function () {
            $("#div_moc_list").show();
            $("#btnExcel").show();
            var data = getQueryData();
            if (data == "") {
                return false;
            } else {
                setGrid(data);
            }
        });

        $('#date_start, #date_end').datepicker({
            changeMonth: true,
            numberOfMonths: 3,
            dateFormat: 'yy/mm/dd',
            onSelect: function (selectedDate) {
                var option = this.id == from ? "minDate" : "maxDate";
                var instance = $(this).data("datepicker");
                var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                dates.not(this).datepicker("option", option, date);
            }
        });

        $('#relate_date_start, #relate_date_end').datepicker({
            changeMonth: true,
            numberOfMonths: 3,
            dateFormat: 'yy/mm/dd',
            onSelect: function (selectedDate) {
                var option = this.id == from ? "minDate" : "maxDate";
                var instance = $(this).data("datepicker");
                var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                dates.not(this).datepicker("option", option, date);
            }
        });

        $("#btnExcel").click(function () {
            var data = getQueryData();
            var list = ajaxReturn("AntiCreditcardFraud.aspx", "GetCdInfo", data);     
            getExcel(JSON.stringify(list));
        });

        $("#btnRefer").live("click", function () {
            ajaxReturn("AntiCreditcardFraud.aspx", "UpdateRefer", "{strIds:'" + getReferId("tbl_creditcard_order_list") + "'}");
            $("#btnSearch").click();
        });

        $("#btnCheckAccountSafe").live("click", function () {
            ajaxReturn("AntiCreditcardFraud.aspx", "UpdateCheckAccountSafe", "{strIds:'" + getReferId("tbl_check_account") + "',userName:'" + "<%=UserName %>" + "'}");
            $("#btnSearchCheckAccount").click();
        });

        $("#btnCheckAccountError").live("click", function () {
            ajaxReturn("AntiCreditcardFraud.aspx", "UpdateCheckAccountError", "{strIds:'" + getReferId("tbl_check_account") + "',userName:'" + "<%=UserName %>" + "'}");
            $("#btnSearchCheckAccount").click();
        });

        $("#<%=txtCreditcardID.ClientID %>").focus(function () {
            var $this = $(this)
            if ($this.val() == '請輸入16碼信用卡號') {
                $this.val("");
                $this.removeClass('waterMarkText');
            }
        });

        $("#<%=txtCreditcardID.ClientID %>").blur(function () {
            var $this = $(this)
            if ($this.val() == '') {
                $this.val("請輸入16碼信用卡號");
                $this.addClass('waterMarkText');
            }
        });

        $("#<%=btnAddBlackList.ClientID %>").click(function () {
            var selectAddType = $("#<%=ddlAddResult.ClientID %>").val();
            if (selectAddType == "def") {
                alert("請選擇一個狀態");
                return false;
            }
        });

        $('td[aria-describedby=tbl_creditcard_order_list_IsLocked] :checkbox').live("change", function () {
            var $this = $(this),
                checked, msg,
                id = $this.parents('tr').find('td[aria-describedby=tbl_creditcard_order_list_CreditcardInfo]').attr('title');
            if($this.attr('checked') == 'checked') {
                checked = 'true';
                msg = '設定為黑名單?';
            } else {
                checked = 'false';
                msg = '設定回白名單?';
            }
            if (checked=='true') {
                var result = compareCreditcard(id);
                if (result) {
                    $this.attr('checked', false);
                    return;
                }
            }
            if (confirm("是否將本卡號：" + id + msg)) {
                ajaxReturn("AntiCreditcardFraud.aspx", "UpdateBlacklist", "{strId:'" + id + "', isLock:'" + checked + "'}");
                $("#btnSearch").click();
            } else {
                $this.attr('checked', false);
                return false;
            }

        });

        $("#btnSearchCreditcardFraudData").click(function () {

            var searchStartData = ($("#relate_date_start").val() != "") ? $("#relate_date_start").val() : "";
            var searchEndData = ($("#relate_date_end").val() != "") ? $("#relate_date_end").val() : "";
            if (searchStartData == "" || searchEndData == "") {
                alert("請選擇篩選時間");
                return "";
            } else {
                var searchData =
                    "{strSDate:'" + searchStartData + "', strEDate:'" + searchEndData + "'}";
            }

            setCreditcardFraudDataGrid(searchData);
        })

        $("#btnSearchCheckAccount").click(function () {
            setCheckAccountGrid();
        })


        function getReferId(table) {
            var ids = $("#" + table).jqGrid('getGridParam', 'selarrrow');
            var rtnIds = "";
            $.each(ids, function (i) {
                var row = $("#" + table).jqGrid('getRowData', ids[i]);
                if (i == 0) {
                    rtnIds = row.Id;
                } else {
                    rtnIds += "," + row.Id;
                }
            });
            return rtnIds;
        }

        function getExcel(strData) {
            ajaxReturn("AntiCreditcardFraud.aspx", "GetExcel", ("{strJson: '" + strData + "'}"));
        }

        function compareCreditcard(strCreditcard) {
            var result = ajaxReturn("AntiCreditcardFraud.aspx", "CompareTestCreditcard", ("{strCreditcard: '" + strCreditcard + "'}"));
            alert('測試卡號不可加入黑名單!');
            return result;
        }

        function getQueryData() {
            var searchDdlMain = $("#ddlMain option:selected").val();
            var searchTxtMain = $("#txtMain").val();
            var searchStartData = ($("#date_start").val() != "") ? $("#date_start").val() : "";
            var searchEndData = ($("#date_end").val() != "") ? $("#date_end").val() : "";
            var searchStartTime = $("#ddl_shtime").val() + ":" + $("#ddl_smtime").val();
            var searchEndTime = $("#ddl_ehtime").val() + ":" + $("#ddl_emtime").val();
            var searchRefer = ($("#chkIsRefer").attr("checked")) ? "refer" : "";
            var searchBl = ($("#chkIsBlacklist").attr("checked")) ? "blacklist" : "";
            var searchAllOrder = ($("#chkShowAllOrders").attr("checked")) ? "allorders" : "";
            if (searchDdlMain == "def" && searchTxtMain != "") {
                alert("請選擇主要選項");
                return "";
            } else {
                var searchResult = $("#ddlResult option:selected").val();
                var searchData =
                    "{strType:'" + searchDdlMain + "', strMain:'" + searchTxtMain + "', strRes:'" + searchResult +
                        "', strRefer:'" + searchRefer + "', strBl:'" + searchBl + "', strAllOrder:'" + searchAllOrder +
                        "', strSDate:'" + searchStartData + "', strEDate:'" + searchEndData +
                        "', strSTime:'" + searchStartTime + "', strETime:'" + searchEndTime + "'}";
            }
            return searchData;
        }
//        function setDdlResult() {
        //            var list = ajaxReturn("AntiCreditcardFraud.aspx", "GetResultList", "");
//            var ddlResult = setDdlOpts(list);
//            $("#ddlResult").find("option").remove().end().append(ddlResult);
//        }
        function setDdlTime() {
            var tempSelectOption = "<option value='{val}'>{opt}</option>";
            var tempValue = "";
            var value = "";
            var opts = "";
            for (var i = 0; i < 24; i++) {
                var temph = tempSelectOption;
                tempValue = "" + i;
                value = (tempValue.length < 2) ? "0" + "" + tempValue : tempValue;
                opts += temph.replace("{val}", value).replace("{opt}", value);
            }
            $("#ddl_shtime, #ddl_ehtime").find("option").remove().end().append(opts);
            opts = "";
            for (i = 0; i < 60; i++) {
                var tempm = tempSelectOption;
                tempValue = "" + i;
                value = (tempValue.length < 2) ? "0" + "" + tempValue : tempValue;
                opts += tempm.replace("{val}", value).replace("{opt}", value);
            }
            $("#ddl_smtime, #ddl_emtime").find("option").remove().end().append(opts);
        }

        function setinputTimeDefault() {
            var endDate = new Date();

            var endMonth = endDate.getMonth() + 1;
            var endDay = endDate.getDate() + 1;
            var startDate = new Date(endDate);
            startDate.setDate(startDate.getDate() - 7);
            var startMonth = startDate.getMonth() + 1;
            var startDay = startDate.getDate();

            var endOutput = endDate.getFullYear() + '/' +
            (endMonth < 10 ? '0' : '') + endMonth + '/' +
            (endDay < 10 ? '0' : '') + endDay;
            var startOutput = startDate.getFullYear() + '/' +
                (startMonth < 10 ? '0' : '') + startMonth + '/' +
                (startDay < 10 ? '0' : '') + startDay;

            $("#date_start").val(startOutput);
            $("#date_end").val(endOutput);
        }

        function setDdlOpts(list) {
            var tempSelectOption = "<option title='{title}' value='{val}'>{opt}</option>";
            var rtnOpts = "<option value='def'>請選擇</option>";
            $.each(list, function (i) {
                var temp = tempSelectOption;
                if (list[i].id != -1) {
                    rtnOpts += temp.replace("{val}", list[i].Id)
                        .replace("{opt}", list[i].Name)
                        .replace("{title}", list[i].Title);
                }
            });
            return rtnOpts;
        }

        function runGrid() {
            $("#div_moc_list").show();
            $("#btnExcel").show();
            var data = "{strType:'creditcard', strMain:'" + $("#<%=txtCreditcardID.ClientID %>").val() + "', strRes:'def', strRefer:'', strBl:'blacklist', strAllOrder:'allorders', strSDate:'', strEDate:'', strSTime:'', strETime:''}";
            if (data == "") {
                return false;
            } else {
                setGrid(data);
            }
        }

        function setGrid(strData) {
            $("#tbl_creditcard_order_list").jqGrid({
                datatype: "local",
                colNames: ["序號", "照會中", "訂單編號", "卡號", "訂單時間", "姓名", "手機", "地址", "金額", "檔名", "狀態", "IP", "最後修改帳號", "最後修改時間", "黑名單"],
                colModel: [
                    { name: "Id", index: "Id", width: 50, align: "center"},
                    { name: "IsReference", index: "IsReference", width: 40, align: "center", editable: true, edittype: "checkbox", formatter: 'checkbox' },
                    { name: "OrderGuid", index: "OrderGuid", width: 120 },
                    { name: "CreditcardInfo", index: "CreditcardInfo", width: 100 },
                    { name: "OrderTime", index: "OrderTime", width: 100},
                    { name: "MemberName", index: "MemberName", width: 40 },
                    { name: "MemberMobile", index: "MemberMobile", width: 80, align: "center" },
                    { name: "DeliveryAddress", index: "DeliveryAddress", width: 120 },
                    { name: "Amount", index: "Amount", width: 50, align: "right" },
                    { name: "ItemName", index: "ItemName", width: 205 },
                    { name: "Result", index: "Result" ,width: 100 },
                    { name: "IP", index: "IP", width: 90 },
                    { name: "LastID", index: "LastID", width: 100 },
                    { name: "LastTime", index: "LastTime", width: 120 },
                    { name: "IsLocked", index: "IsLocked", width: 50, align: "center", editable: true, edittype: "checkbox", formatter: 'checkbox', editoptions: { value: "1:0" }, formatoptions: { disabled: false} }
                ],
                rowNum: 15,
                pager: "#tbl_creditcard_order_pager",
                height: "auto",
                width: 1470,
                forceFit: false,
                shrinkToFit:false,
                scrollrows:true,
                multiselect: true,
                caption: "防止盜刷與否認消費管理<input id='btnRefer' type='button' value='照會完成' /> ",
                emptyrecords: "查無資料"
            });
            $("#tbl_creditcard_order_list").jqGrid('navGrid', '#tbl_creditcard_order_pager', { edit: false, add: false, del: false });

            var list = ajaxReturn("AntiCreditcardFraud.aspx", "GetCdInfo", strData);
            $("#<%=txtHideJson.ClientID %>").val(JSON.stringify(list));
            $("#tbl_creditcard_order_list").jqGrid("clearGridData", true);
            if (list != null && list.length > 0) {
                for (var i = 0; i <= list.length + 1; i++) {
                    jQuery("#tbl_creditcard_order_list").jqGrid('addRowData', i + 1, list[i]);
                }
            }           
            $('#tbl_creditcard_order_list').trigger("reloadGrid");
        }

        function setCreditcardFraudDataGrid(strData) {
            $("#tbl_creditcard_fraud_data").jqGrid({
                datatype: "local",
                colNames: ["序號", "UserId", "IP", "地址", "卡號", "類型", "匯入帳號", "匯入時間", "修改帳號", "修改時間", "風險時間"],
                colModel: [
                    { name: "Id", index: "Id", width: 50, align: "center"},
                    { name: "UserId", index: "UserId", width: 100, align: "center" },
                    { name: "Ip", index: "Ip", width: 100 },
                    { name: "Address", index: "Address", width: 250 },
                    { name: "CardNumber", index: "CardNumber", width: 100 },
                    { name: "Type", index: "Type", width: 50 },
                    { name: "CreateId", index: "CreateId", width: 170, align: "center" },
                    { name: "CreateTime", index: "CreateTime", width: 150 },
                    { name: "ModifyId", index: "ModifyId", width: 170, align: "right" },
                    { name: "ModifyTime", index: "ModifyTime", width: 150 },
                    { name: "RelateTime", index: "RelateTime", width: 150 }
                ],
                rowNum: 10,
                pager: "#tbl_creditcard_fraud_data_pager",
                height: "auto",
                width: 1500,
                forceFit: false,
                shrinkToFit:false,
                scrollrows:true,
                caption: "盜刷資料顯示管理",
                emptyrecords: "查無資料"
            });
            $("#tbl_creditcard_fraud_data").jqGrid('navGrid', '#tbl_creditcard_fraud_data_pager', { edit: false, add: false, del: false });

            var list = ajaxReturn("AntiCreditcardFraud.aspx", "GetCreditcardFraudDataInfo", strData);
            $("#<%=txtCreditcardFraudDataHideJson.ClientID %>").val(JSON.stringify(list));
            $("#tbl_creditcard_fraud_data").jqGrid("clearGridData", true);
            if (list != null && list.length > 0) {
                for (var i = 0; i <= list.length + 1; i++) {
                    jQuery("#tbl_creditcard_fraud_data").jqGrid('addRowData', i + 1, list[i]);
                }
            }           
            $('#tbl_creditcard_fraud_data').trigger("reloadGrid");
        }
        function setCheckAccountGrid() {
            $("#tbl_check_account").jqGrid({
                datatype: "local",
                colNames: ["序號", "UserId", "匯入帳號", "匯入時間"],
                colModel: [
                    { name: "Id", index: "Id", width: 50, align: "center"},
                    { name: "UserId", index: "UserId", width: 100, align: "center" },
                    { name: "CreateId", index: "CreateId", width: 170, align: "center" },
                    { name: "CreateTime", index: "CreateTime", width: 150 },
                ],
                rowNum: 10,
                pager: "#tbl_check_account_pager",
                height: "auto",
                width: 520,
                forceFit: false,
                shrinkToFit:false,
                scrollrows: true,
                multiselect: true,
                caption: "需確認風險帳號查詢<input id='btnCheckAccountSafe' type='button' value='帳號正常' /> <input id='btnCheckAccountError' type='button' value='風險帳號' /> ",
                emptyrecords: "查無資料"
            });
            $("#tbl_check_account").jqGrid('navGrid', '#tbl_check_account_pager', { edit: false, add: false, del: false });

            var list = ajaxReturn("AntiCreditcardFraud.aspx", "GetCheckAccountInfo");
            $("#<%=txtCheckAccountHideJson.ClientID %>").val(JSON.stringify(list));
            $("#tbl_check_account").jqGrid("clearGridData", true);
            if (list != null && list.length > 0) {
                for (var i = 0; i <= list.length + 1; i++) {
                    jQuery("#tbl_check_account").jqGrid('addRowData', i + 1, list[i]);
                }
            }           
            $('#tbl_check_account').trigger("reloadGrid");
        }

        

        /* TODO: Ajax */
        function ajaxReturn(strUrlPage, strMethod, strData) {
            var jsonReturn;
            var strUrl = strUrlPage + "/" + strMethod;
            $.ajax({
                type: "POST",
                url: strUrl,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: strData,
                async: false,
                success: function (data) {
                    jsonReturn = $.parseJSON(data.d);
                }
            });
            return jsonReturn;
        }
    });

    function numcheck(id, creditcard) {
        var re = /^[0-9]+$/;
        if (!re.test(creditcard.value)) {
            $("#lblMessage").val("格式不正確，請輸入16碼卡號");
            document.getElementById(id).value = "";
            $("#<%=btnAddBlackList.ClientID %>").attr("disabled", true);
        } else {
            if (creditcard.value.length<16) {
                $("#lblMessage").val("格式不正確，請輸入16碼卡號");
                document.getElementById(id).value = "";
                $("#<%=btnAddBlackList.ClientID %>").attr("disabled", true);
            } else {
                $("#lblMessage").val("");
                $("#<%=btnAddBlackList.ClientID %>").attr("disabled", false);
            }
            
        }
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="width: 1500px">
        <div class="black-title">照會管理</div>

        <div class="black-content">
            <div>
                <asp:DropDownList ID="ddlAddResult" runat="server">
                    <asp:ListItem Value="def" Selected="True">請選擇</asp:ListItem>
                    <asp:ListItem Value="10000001">單筆消費超出上限</asp:ListItem>
                    <asp:ListItem Value="10000005">單卡24h內累積消費超出上限</asp:ListItem>
                    <asp:ListItem Value="10000002">短期間連續消費</asp:ListItem>
                    <asp:ListItem Value="99999999">卡片異常</asp:ListItem>
                    <asp:ListItem Value="10000006">否認交易</asp:ListItem>
                    <asp:ListItem Value="10000007">特殊客戶</asp:ListItem>
                    <asp:ListItem Value="5">(卡片異常)超額/管制/效期錯</asp:ListItem>
                    <asp:ListItem Value="7">(卡片異常)掛失卡</asp:ListItem>
                    <asp:ListItem Value="12">(卡片異常)3D 驗證失敗</asp:ListItem>
                    <asp:ListItem Value="41">(卡片異常)遺失卡</asp:ListItem>
                    <asp:ListItem Value="43">(卡片異常)被竊卡</asp:ListItem>
                    <asp:ListItem Value="51">(卡片異常)超額婉拒交易</asp:ListItem>
                    <asp:ListItem Value="61">(卡片異常)超額</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtCreditcardID" runat="server" Text="請輸入16碼信用卡號" onchange="javascript:numcheck(this.id,this)" class="waterMarkText" MaxLength="16"></asp:TextBox>
                &nbsp;<asp:Button ID="btnAddBlackList" runat="server" Text="新增黑名單" OnClick="btnAddBlackList_Click" />
                <br />
                <input id="lblMessage" type="text" value="" style="color: red; background-color: transparent; border: none; width: 300px;" />
                <br />
            </div>
            <br />
            <div>
                <select id="ddlMain">
                    <option value="def">請選擇</option>
                    <option value="creditcard">信用卡號</option>
                    <option value="order">訂單編號</option>
                    <option value="memberid">會員帳號</option>
                    <%--            <option value="membername">會員姓名</option>--%>
                </select><input id="txtMain" type="text" /><br />
                訂單日期：<input type='text' id='date_start' />
                <select id="ddl_shtime"></select>:<select id="ddl_smtime"></select>
                ～ 
                <input type='text' id='date_end' />
                <select id="ddl_ehtime"></select>:<select id="ddl_emtime"></select>
                <br />
                狀態：<select id="ddlResult">
                    <option value="def">請選擇</option>
                    <option value="10000001">單筆消費超出上限</option>
                    <option value="10000005">單卡24h內累積消費超出上限</option>
                    <option value="10000002">短期間連續消費</option>
                    <option value="10000008">風險IP</option>
                    <option value="10000009">風險地址</option>
                    <option value="10000010">風險帳號</option>
                    <option value="10000011">風險檔次</option>
                    <option value="99999999">卡片異常</option>
                    <option value="5">(卡片異常)超額/管制/效期錯</option>
                    <option value="7">(卡片異常)掛失卡</option>
                    <option value="12">(卡片異常)3D 驗證失敗</option>
                    <option value="41">(卡片異常)遺失卡</option>
                    <option value="43">(卡片異常)被竊卡</option>
                    <option value="51">(卡片異常)超額婉拒交易</option>
                    <option value="61">(卡片異常)超額</option>
                </select>
                <br />
                <br />
                <input id="chkIsRefer" type="checkbox" />照會中
        <input id="chkIsBlacklist" type="checkbox" />黑名單
        <div id="divShowAllOrders" runat="server" clientidmode="static">
            <input id="chkShowAllOrders" type="checkbox" />包含正常交易訂單</div>
                <br />
                <input id="btnSearch" type="button" value="搜尋" />
            </div>
            <br />
            <div id="div_moc_list">
                <asp:Button ID="btnExcel" runat="server" Text="匯出資料" OnClick="btnExcel_Click" />
                <br />
                <br />
                <table id="tbl_creditcard_order_list" style="font-size: 11px; width: 1000"></table>
                <div id="tbl_creditcard_order_pager"></div>
            </div>
            <asp:HiddenField ID="txtHideJson" runat="server" ClientIDMode="Static" Value="0" EnableViewState="false" />
            <asp:HiddenField ID="forceShowGrid" runat="server" ClientIDMode="Static" Value="0" EnableViewState="false" />
            <asp:HiddenField ID="IsShowAllOrders" runat="server" ClientIDMode="Static" EnableViewState="false" />
        </div>
    </div>

    
    <br />
    <br />
    
    <div style="width:1550px">
        <div class="black-title">盜刷資料查詢</div>

        <div class="black-content">

            風險建立日期：
            <input type='text' id='relate_date_start' />
            ～ 
            <input type='text' id='relate_date_end' />
            <br />
            <asp:Button ID="btnCreditcardFraudDataExcel" runat="server" Text="匯出資料" OnClick="btnCreditcardFraudDataExcel_Click" />
            <input id="btnSearchCreditcardFraudData" type="button" value="搜尋" /> 
            <br />
            <br />
            <table id="tbl_creditcard_fraud_data" style="font-size: 11px; width: 1450px"></table>
             <div id="tbl_creditcard_fraud_data_pager"></div>
        </div>

        <asp:HiddenField ID="txtCreditcardFraudDataHideJson" runat="server" ClientIDMode="Static" Value="0" EnableViewState="false" />
    </div>

    <br />
    <br />

    <div style="width:600px">
        <div class="black-title">需確認風險帳號查詢</div>

        <div class="black-content">

            <asp:Button ID="btnCheckAccountExcel" runat="server" Text="匯出資料" OnClick="btnCheckAccountExcel_Click" />
            <input id="btnSearchCheckAccount" type="button" value="搜尋" /> 
            <br />
            <br />
            <table id="tbl_check_account" style="font-size: 11px; width: 520px"></table>
             <div id="tbl_check_account_pager"></div>
        </div>

        <asp:HiddenField ID="txtCheckAccountHideJson" runat="server" ClientIDMode="Static" Value="0" EnableViewState="false" />
    </div>

    

</asp:Content>

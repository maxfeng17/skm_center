﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.ControlRoom.Finance
{
    public partial class PponDealReports : RolePage, IPponDealReportsView
    {
        #region Event
        public event EventHandler<DataEventArgs<DateTime[]>> SearchByDepartment;
        #endregion

        #region props
        private PponDealReportsPresenter _presenter;
        public PponDealReportsPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                _presenter.OnViewInitialized();

                tbBeginDate.Text = DateTime.Now.Month.ToString() + "/1/" + DateTime.Now.Year.ToString();
                tbEndDate.Text = (DateTime.Now.AddMonths(1)).Month.ToString() + "/1/" + (DateTime.Now.AddMonths(1)).Year.ToString();
            }
            _presenter.OnViewLoaded();
        }

        public void SetGvDepartment(DataTable dt)
        {
            gvDepartment.DataSource = dt;
            gvDepartment.DataBind();
        }

        protected string GetDepartment(string dCode)
        {
            if (!string.IsNullOrEmpty(dCode))
            {
                switch ((DepartmentTypes) int.Parse(dCode))
                {
                    case DepartmentTypes.Meal:
                        dCode = Resources.Localization.SDT_lunchking;
                        break;
                    case DepartmentTypes.Cosmetics:
                        dCode = Resources.Localization.SDT_cosmetics;
                        break;
                    case DepartmentTypes.Delicacies:
                        dCode = Resources.Localization.SDT_delicacies;
                        break;
                    case DepartmentTypes.Ppon:
                        dCode = Resources.Localization.SDT_Ppon;
                        break;
                    case DepartmentTypes.PponItem:
                        dCode = Resources.Localization.SDT_Ppon_Item;
                        break;
                    case DepartmentTypes.Peauty:
                        dCode = Resources.Localization.SDT_Peauty;
                        break;
                    default:
                        break;
                }
            }
            return dCode;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DateTime[] arg = new DateTime[2];
            arg[0] = DateTime.Parse(tbBeginDate.Text);
            arg[1] = DateTime.Parse(tbEndDate.Text);
            if (SearchByDepartment != null)
                SearchByDepartment(this, new DataEventArgs<DateTime[]>(arg));
        }
    }
}
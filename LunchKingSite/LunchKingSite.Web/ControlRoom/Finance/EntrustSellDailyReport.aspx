﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="EntrustSellDailyReport.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Finance.EntrustSellDailyReport" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="SSC">
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />    
    <script src="<%=ResolveClientUrl("~/Tools/js/jquery-ui.1.8.min.js") %>" type="text/javascript"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/homecook.js"></script>
    
    <style type="text/css">
        #search_table td
        {
            padding: 5px;
        }
        .list_table
        {
            border-collapse: collapse;
            width: 800px;
        }
        .list_table thead tr
        {
            background-color: #6b696b;
            height: 30px;
            color: white;
            font-size: 14px;
            font-weight: bold;
        }
        .list_table td
        {
            padding: 5px;
        }
        .list_table .row0
        {
            background-color: #f7f7de;
            height: 30px;
        }
        .list_table .row1
        {
            background-color: white;
            height: 30px;
        }
        .list_table .cell_text
        {
            text-align: left;
        }
        .list_table .cell_num
        {
            text-align: right;
        }
        .btn_search
        {
            letter-spacing: 3px;
            padding: 3px 3px 3px 10px;
            cursor: pointer;
        }
        .btn_export
        {
            padding: 3px 3px 3px 10px;
            cursor: pointer;
        }
        .btn_book
        {
            letter-spacing: 3px;
            padding: 3px 3px 3px 10px;
            cursor: pointer;
            margin-left: 20px;
        }
        td.booked_cell
        {
            text-align: center;
        }
        .ui-datepicker 
        {
            font-size:10px;
        }
        input.date
        {
            width: 80px;
        }
    </style>
    <script type="text/javascript">
        $().ready(function () {
            setupdatepicker('<%=tbSTime.ClientID%>', '<%=tbETime.ClientID%>', 3);         
        })
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <h2>
        代收轉付每日請款報表</h2>
    <fieldset style="border: double 3px black; width: 650px;">
        <table class="list_table" style="width: 650px;" id="list-table">
            <tr>
                <td class="style2" valign="middle" colspan="2">
                    請款檔組檔日：<asp:TextBox ID="tbSTime" runat="server" CssClass="date" />
                    ～
                    <asp:TextBox ID="tbETime" runat="server" CssClass="date" />
                </td>
            </tr>
            <tr>
                <td valign="middle" nowrap="nowrap">
                </td>
                <td style="text-align: right; margin-right: 40px">
                    <asp:Button ID="btnSearch" CssClass="btn_search" runat="server" Text="搜尋" OnClick="btnSearch_Click" />
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <asp:Button ID="btnOutput" CssClass="btn_export" runat="server" Text="匯出資料" OnClick="btnExport_Click" />
    <div style="margin-top: 20px;">
        <asp:Repeater ID="repList" runat="server">
            <HeaderTemplate>
                <table class="list_table" border="1" cellspacing="0">
                    <thead>
                        <tr>
                            <th scope="col">
                                請款檔組檔日
                            </th>
                            <th scope="col">
                                訂單編號
                            </th>
                            <th scope="col">
                                請/退款金額
                            </th>
                            <th scope="col">
                                動作別
                            </th>
                            <th scope="col">
                                購買人
                            </th>
                            <th scope="col">
                                商品名稱
                            </th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="row<%#Container.ItemIndex % 2 %>">
                    <td class="cell_text">
                        <%#Eval("TransTime", "{0:yyyy/MM/dd}")%>
                    </td>
                    <td class="cell_text">
                        <%#Eval("OrderId")%>
                    </td>
                    <td class="cell_num">
                        <%#GetAmount(Eval("TransType"), Eval("Amount"))%>
                    </td>
                    <td class="cell_text">
                        <%#GetTransType(Eval("TransType"))%>
                    </td>
                    <td class="cell_text">
                        <%#Eval("MemberName")%>
                    </td>
                    <td class="cell_text">
                        <%#Eval("CouponUsage")%>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</asp:Content>

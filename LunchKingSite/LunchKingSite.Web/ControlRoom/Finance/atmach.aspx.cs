﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core.UI;
using log4net;


namespace LunchKingSite.Web.ControlRoom.Finance
{
    public partial class atmach : BasePage, IAtmAchView
    {
        #region Props
        protected static ILog logger = LogManager.GetLogger(typeof(atmach));
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public int P1Count;
        public int P1Total;
        public string pRefundList;
        public string ImportResult { get; set; }
        public string FundTransferType {
            get {
                return rblFundTransferType.SelectedValue;
            }
        }

        public string UserName {
            get { return Page.User.Identity.Name; }
        }


        private AtmAchPresenter _presenter;
        public AtmAchPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        #endregion

        #region Events

        public event EventHandler<DataEventArgs<StreamReader>> BatchImportClicked;
        public event EventHandler<DataEventArgs<StreamReader>> BatchImportManualClicked;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Presenter.OnViewInitialized();
            }

            Presenter.OnViewLoaded();
        }

        #region 產生ACHP1資料

        private string AchP1Head(DateTime time)
        {
            return "BOFACHP01" + (DateTime.Now.Year - 1911).ToString().PadLeft(4, '0')  //13
                   + time.Month.ToString().PadLeft(2, '0')                              //2
                   + time.Day.ToString().PadLeft(2, '0')                                //2
                   + time.Hour.ToString().PadLeft(2, '0')                               //2
                   + time.Minute.ToString().PadLeft(2, '0')                             //2 
                   + time.Second.ToString().PadLeft(2, '0')                             //2
                   + string.Format("{0}{1}", config.SendUnitNo, config.ReceiveUnitNo)   //14
                   + new string(' ', 123) + "\r\n";
        }

        private string AchP1Foot(DateTime time, int count, int total)
        {
            return "EOFACHP01" + (DateTime.Now.Year - 1911).ToString().PadLeft(4, '0')  //13
                   + time.Month.ToString().PadLeft(2, '0')                              //2
                   + time.Day.ToString().PadLeft(2, '0')                                //2
                   + string.Format("{0}{1}", config.SendUnitNo, config.ReceiveUnitNo)   //14
                   + count.ToString().PadLeft(8, '0')                                   //8
                   + total.ToString().PadLeft(16, '0')                                  //16
                   + new string(' ', 105) + "\r\n";
        }

        private string AchP1Body(DateTime date, DateTime process)
        {
            string result = "";
            P1Count = 0;
            P1Total = 0;
            pRefundList = "";
            CtAtmRefundCollection refundlist = PaymentFacade.CtAtmRefundGetList();

            ///依照財會目前的流程為1週1次，完成上一批才會匯出下一批
            ///如之後改成連續匯出則要注意將已產出ACH但未匯入結果的退貨單算入
            ///購物金檢查才會正確
            #region 匯出ACH前檢查購物金餘額是否足夠

            Dictionary<int, decimal> userScahList = new Dictionary<int, decimal>();
            var tmpRefundlist = refundlist.Clone();
            foreach (var item in tmpRefundlist)
            {
                LunchKingSite.BizLogic.Model.Refund.ReturnFormEntity form = LunchKingSite.BizLogic.Model.Refund.ReturnFormRepository.FindById(item.ReturnFormId.GetValueOrDefault(0));
                decimal userScashTotal = 0;
                if (form != null)
                {
                    try
                    {
                        if (item.ReturnFormId != null)
                        {
                            if (userScahList.Any(x => x.Key == item.UserId))
                            {
                                userScashTotal = userScahList[item.UserId];
                            }
                            else
                            {
                                userScashTotal = OrderFacade.GetSCashSum(item.UserId);
                                userScahList.Add(item.UserId, userScashTotal);
                            }
                            RefundedAmount refundedAmount = form.GetRefundedSCashToCashAmount();

                            if (userScashTotal < refundedAmount.Atm + refundedAmount.CreditCard + refundedAmount.TCash + refundedAmount.IspPay)
                            {
                                //"購物金不足, 無法轉退現金"
                                refundlist.Remove(item);
                                CommonFacade.AddAudit(item.OrderGuid, AuditType.Refund, "購物金餘額不足，ACH匯出失敗。", "sys", false);
                            }
                            else
                            {
                                userScahList[item.UserId] = userScashTotal - (refundedAmount.Atm + refundedAmount.CreditCard + refundedAmount.TCash + refundedAmount.IspPay);
                            }
                        }
                        else
                        {
                            //log一下觀察資料
                            refundlist.Remove(item);
                            logger.Error("order_guid:" + form.OrderGuid.ToString() + "   si:" + item.Si.ToString() + " ATM退款退貨單ID不存在");
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        logger.Error("order_guid:" + form.OrderGuid.ToString() + "   si:" + item.Si.ToString() + " ATM退款有誤", ex);
                        throw;
                    }
                }
            }

            #endregion 匯出ACH前檢查購物金餘額是否足夠

            if (refundlist.Count > 0)
            {
                for (int i = 0; i < refundlist.Count; i++)
                {
                    result += "NSC405" + (i + 1).ToString().PadLeft(6, '0') + string.Format("{0}{1}", config.TriggerBankAndBranchNo, config.TriggerAccountNo) +
                              refundlist[i].BankCode.PadLeft(7, '0') + refundlist[i].AccountNumber.PadLeft(14, '0') +
                              refundlist[i].Amount.ToString().PadLeft(10, '0') + string.Format("00B{0}  ", config.TriggerCompanyId) +
                              refundlist[i].Id.ToUpper().PadRight(10, ' ') + "      00000000000000 " +
                              (refundlist[i].Si.ToString().Length > 20 ? refundlist[i].Si.ToString().Substring(0, 20) : refundlist[i].Si.ToString().PadRight(20, ' ')) +
                              "".PadRight(32, ' ') + "\r\n";
                    P1Count++;
                    P1Total += refundlist[i].Amount;
                    pRefundList += refundlist[i].Si.ToString() + ",";
                    refundlist[i].Status = Convert.ToInt32(AtmRefundStatus.AchSend);
                    refundlist[i].RefundSendTime = DateTime.Now;
                    refundlist[i].TargetDate = date;
                    refundlist[i].ProcessDate = process;
                    using (TransactionScope ts = new TransactionScope())
                    {
                        PaymentFacade.UpdateAtmStatus(AtmStatus.RefundSend, refundlist[i].OrderGuid);
                        PaymentFacade.CtAtmRefundSet(refundlist[i]);
                        ts.Complete();
                    }
                }
                pRefundList = pRefundList.Substring(0, pRefundList.Length - 1);
            }
            else
            {
                result = null;
            }
            return result;
        }

        /// <summary>
        /// 產生並存入ACHP1資料
        /// </summary>
        /// <param name="name">產生者</param>
        /// <param name="target">目標時間</param>
        /// <returns></returns>
        private string AchP1(string name, DateTime target)
        {
            DateTime t = new DateTime();
            if (target.DayOfWeek == DayOfWeek.Monday && DayDiff(DateTime.Now, target) > 3)
                t = target.AddDays(-3);
            else
                t = target.AddDays(-1);
            string body = AchP1Body(target, t);
            string result;
            if (body != null)
            {
                result = AchP1Head(t) + body + AchP1Foot(t, P1Count, P1Total);
                PaymentFacade.CtAtmAchLogSave(result, name, target, P1Count, P1Total, pRefundList);
            }
            else
                result = null;
            return result;
        }

        #endregion

        /// <summary>
        /// 將須退貨資料塞進表格
        /// </summary>
        protected void SetRefundList()
        {
            Gv.DataSource = PaymentFacade.ViewCtAtmRefundGetList();
            Gv.DataBind();
        }

        /// <summary>
        /// 將退貨資料塞進表格
        /// </summary>
        protected void GetRefundList(string list)
        {
            Gv.DataSource = PaymentFacade.ViewCtAtmRefundGetListIn(StringToInts(list));
            Gv.DataBind();
        }

        /// <summary>
        /// 將須ACHP1資料塞進表格
        /// </summary>
        protected void SetP1LogList()
        {
            Gv2.DataSource = PaymentFacade.CtAtmGetP1LogList();
            Gv2.DataBind();
        }

        protected void Gv2Paging(object sender, GridViewPageEventArgs e)
        {
            Gv2.PageIndex = e.NewPageIndex;
            Gv2.DataSource = PaymentFacade.CtAtmGetP1LogList();
            Gv2.DataBind();
        }

        protected void Gv2RowCreate(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[5].CssClass = "tdhide";
                e.Row.Cells[6].CssClass = "tdhide";
            }
            else if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[5].CssClass = "tdhide";
                e.Row.Cells[6].CssClass = "tdhide";
            }
        }

        protected void Gv2DataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string tempstring = e.Row.Cells[1].Text;
                e.Row.Cells[1].Text = tempstring.Substring(0, tempstring.IndexOf(' '));
            }
        }

        #region 按鈕
        /// <summary>
        /// 產出ACHP1資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            string result;
            DateTime t = new DateTime();
            if (DateTime.TryParse(tbOS.Text, out t))
            {
                if (t.DayOfWeek == DayOfWeek.Sunday || t.DayOfWeek == DayOfWeek.Saturday)
                    Label1.Text = @"退款日不得為假日";
                else if (DayDiff(DateTime.Now, t) < 2)
                    Label1.Text = @"退款日需比發動日大兩天" + DayDiff(DateTime.Now, t);
                else if (t.DayOfWeek == DayOfWeek.Monday && DayDiff(DateTime.Now, t) < 4)
                    Label1.Text = @"處理日不得為假日";
                else
                {
                    result = AchP1(User.Identity.Name, t);
                    if (result != null)
                    {
                        TB.Text = result;
                        Label1.Text = @"<pre>" + result + @"</pre>";
                        Panel1.Visible = false;
                        Panel2.Visible = true;
                        Gv.Visible = false;
                    }
                    else
                    {
                        Label1.Text = @"無須退貨資料或該使用者剩餘購物金不足";
                    }
                }
            }
            else
            {
                Label1.Text = @"時間格式錯誤";
            }
        }

        /// <summary>
        /// 顯示需退貨列表
        /// </summary>
        protected void Button2_Click(object sender, EventArgs e)
        {
            SetRefundList();
            PanelMain.Visible = false;
            Gv.Visible = true;
            Panel1.Visible = true;
        }

        /// <summary>
        /// 顯示Log列表
        /// </summary>
        protected void Button3_Click(object sender, EventArgs e)
        {
            SetP1LogList();
            PanelMain.Visible = false;
            Gv2.Visible = true;
        }

        /// <summary>
        /// 處理ACHR01匯入
        /// </summary>
        protected void btnImport_Click(object sender, EventArgs e)
        {
            Label1.Text = @"<pre>" + PaymentFacade.CtAtmAchR1LogSave(TB2.Text, User.Identity.Name) + @"</pre>";
        }

        /// <summary>
        /// 處理ACHR01批次匯入
        /// </summary>
        protected void btnBatchImport_Click(object sender, EventArgs e)
        {
            if (rblFundTransferType.SelectedValue == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('請先選擇「ACH匯款」或者「人工匯款」')", true);
                return;
            }
            if (fuBatchImport.PostedFile.FileName.Trim() != string.Empty)
            {
                if (rblFundTransferType.SelectedValue == "1")
                {
                    //asc匯款
                    StreamReader sr = new StreamReader(this.fuBatchImport.PostedFile.InputStream, System.Text.Encoding.ASCII);
                    this.BatchImportClicked(this, new DataEventArgs<StreamReader>(sr));
                }
                else
                {
                    //人工匯款
                    StreamReader sr = new StreamReader(this.fuBatchImport.PostedFile.InputStream, System.Text.Encoding.Default);
                    this.BatchImportManualClicked(this, new DataEventArgs<StreamReader>(sr));
                }

            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('" + this.ImportResult + "')", true);
        }


        /// <summary>
        /// 顯示匯入ACH介面
        /// </summary>
        protected void Button5_Click(object sender, EventArgs e)
        {
            Panel3.Visible = true;
            PanelMain.Visible = false;
        }

        protected void btnToBatchImport_Click(object sender, EventArgs e)
        {
            PanelBatchImport.Visible = true;
            PanelMain.Visible = false;
        }

        protected void ButtonTest_Click(object sender, EventArgs e)
        {
            for (int i = 1; i <= 50; i++)
            {
                Label1.Text += PaymentFacade.GetAtmAccount(i.ToString().PadLeft(5, '0'), MemberFacade.GetUniqueId("doomdied@gmail.com"),
                    50, Guid.Parse("b958e502-21eb-428f-935e-e67b5b3f334d"), i.ToString().PadLeft(4, '0'), Guid.NewGuid());
                Label1.Text += @"<br />";
            }
        }

        /// <summary>
        /// 塞入要產生的ACHP01資料
        /// </summary>
        protected void Gv2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "AddToTextBox")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = Gv2.Rows[index];
                TB.Text = row.Cells[5].Text;
                Label1.Text = @"<pre>" + row.Cells[5].Text + @"</pre>";
                Panel2.Visible = true;
            }

            if (e.CommandName == "ViewList")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = Gv2.Rows[index];
                GetRefundList(row.Cells[6].Text);
                Gv.Visible = true;
                Gv2.Visible = false;
            }
        }
        #endregion

        #region 工具

        protected int DayDiff(DateTime dfrom, DateTime dto)
        {
            int day;
            day = DateTime.IsLeapYear(dfrom.Year) ? 366 : 365;

            if (dto.Year == dfrom.Year)
                return dto.DayOfYear - dfrom.DayOfYear;
            else if (dto.Year > dfrom.Year)
                return dto.DayOfYear - dfrom.DayOfYear + day;
            else
                return dto.DayOfYear - dfrom.DayOfYear - day;
        }

        protected int[] StringToInts(string myString)
        {
            List<int> ints = new List<int>();
            string[] strings = myString.Split(',');

            foreach (string s in strings)
            {
                int i;
                if (int.TryParse(s.Trim(), out i))
                {
                    ints.Add(i);
                }
            }
            return ints.ToArray();
        }

        /// <summary>
        /// 取得對應的退貨發票
        /// </summary>
        /// <param name="si">CtAtmRefund 的 Si</param>
        protected string GetReturnedInvoice(int si)
        {
            string invString = string.Empty;

            EinvoiceMainCollection invs = new EinvoiceMainCollection();
            CtAtmRefund atmRefund = OrderFacade.CtAtmRefundGetBySi(si);
            string trustIds = this.GetTrustIds(atmRefund.CouponList);
            ViewPponOrder o = OrderFacade.ViewPponOrderGetByOrderGuid(atmRefund.OrderGuid);
            if (o != null) 
            {
                DealProperty dp = PponFacade.DealPropertyGetByBid(o.BusinessHourGuid);

                switch ((DeliveryType)dp.DeliveryType)
                {
                    case DeliveryType.ToHouse:
                        invs = EinvoiceFacade.EinvoiceGetListByToHouseTrustIds(trustIds);
                        break;
                    case DeliveryType.ToShop:
                        if (o.CreateTime < config.NewInvoiceDate)
                        {
                            invs = EinvoiceFacade.EinvoiceGetListByToHouseTrustIds(trustIds);
                        }
                        else
                        {
                            invs = EinvoiceFacade.EinvoiceGetListByToShopTrustIds(trustIds);
                        }
                        break;
                }

                foreach (var inv in invs)
                {
                    if (!string.IsNullOrEmpty(inv.InvoiceNumber) && inv.InvoiceNumberTime != null)
                    {
                        invString += inv.InvoiceNumber + "(" + ((DateTime)inv.InvoiceNumberTime).ToShortDateString() + "),";
                    }
                }
            }
            
            return invString;
        }

        #endregion

        private string GetTrustIds(string trustList) 
        {
            string result = string.Empty;
            if (trustList != null) 
            { 
                if (trustList.Contains(I18N.Phrase.SemiColon))
                {
                    if (trustList.EndsWith(I18N.Phrase.SemiColon))
                    {
                        trustList = trustList.Substring(0, trustList.Length - 1);
                    }
                    if (trustList.Contains(I18N.Phrase.SemiColon))
                    {
                        var temp = trustList.Split(I18N.Phrase.SemiColon);
                        foreach (string t in temp)
                        {
                            Guid guid;
                            if (Guid.TryParse(t, out guid))
                            {
                                result += "'" + t + "',";
                            }
                        }
                        result = result.Substring(0, result.Length - 1);
                    }
                    else 
                    {
                        Guid guid;
                        if (Guid.TryParse(trustList, out guid))
                        {
                            result = "'" + trustList + "'";
                        }
                    }
                }
                else 
                {
                    Guid guid;
                    if (Guid.TryParse(trustList, out guid))
                    {
                        result = "'" + trustList + "'";
                    }
                }
            }

            return result;
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="referrer.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Finance.referrer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8.13.custom.css" rel="stylesheet" />
    <style type="text/css">
        body {
            font-size: 10px;
            background-color: antiquewhite
        }
        .ui-timepicker-div .ui-widget-header {
            margin-bottom: 8px;
        }
        .ui-timepicker-div dl {
            text-align: left;
        }
        .ui-timepicker-div dl dt {
            height: 25px;
        }
        .ui-timepicker-div dl dd {
            margin: -25px 0 10px 65px;
        }
        .ui-timepicker-div td {
            font-size: 90%;
        }
        label.error {
            float: none;
            color: red;
            padding-left: .5em;
            vertical-align: top;
        }

        /*

RESPONSTABLE 2.0 by jordyvanraaij
  Designed mobile first!

If you like this solution, you might also want to check out the 1.0 version:
  https://gist.github.com/jordyvanraaij/9069194

*/
        .responstable {
            margin: 1em 0;
            width: 100%;
            overflow: hidden;
            background: #FFF;
            color: #024457;
            border-radius: 10px;
            border: 1px solid #167F92;
        }
        .responstable tr {
            border: 1px solid #D9E4E6;
        }
        .responstable tr:nth-child(odd) {
            background-color: #EAF3F3;
        }
        .responstable th {
            display: none;
            border: 1px solid #FFF;
            background-color: #167F92;
            color: #FFF;
            padding: 1em;
        }
        .responstable th:first-child {
            display: table-cell;
            text-align: center;
        }
        .responstable th:nth-child(2) {
            display: table-cell;
        }
        .responstable th:nth-child(2) span {
            display: none;
        }
        .responstable th:nth-child(2):after {
            content: attr(data-th);
        }
        @media (min-width: 480px) {
            .responstable th:nth-child(2) span {
                display: block;
            }
            .responstable th:nth-child(2):after {
                display: none;
            }
        }
        .responstable td {
            display: block;
            word-wrap: break-word;
            max-width: 7em;
        }
        .responstable td:first-child {
            display: table-cell;
            text-align: center;
            border-right: 1px solid #D9E4E6;
        }
        @media (min-width: 480px) {
            .responstable td {
                border: 1px solid #D9E4E6;
            }
        }
        .responstable th, .responstable td {
            text-align: left;
            margin: .5em 1em;
        }
        @media (min-width: 480px) {
            .responstable th, .responstable td {
                display: table-cell;
                padding: 1em;
            }
        }
        .fm-input {
            margin: 5px;
            padding: 5px 10px 5px 10px;
        }
        h1.header-title {
            font-family: Verdana;
            font-weight: normal;
            color: #024457;
        }
        .responstable .del-cell {
        }
        .cmdbtn {
            position: absolute;
            padding: 5px 10px 5px 10px;
        }
        .control-label {
            font-weight:normal;
        }
        #myContainer {
            min-width:600px;
        }
        .campaign-records-table {
            margin-top:12px;
        }
        .campaign-records-table td {
            padding:1px 0px 1px 5px;
        }

    </style>
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("jqueryui", "1.8.13");
    </script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript">
        function $$(id, context) {
            var el = $("#" + id, context);
            if (el.length < 1)
                el = $("*[id$=" + id + "]", context);
            return el;
        }

        $(document).ready(function () {
            $$('tbOS').datetimepicker({ dateFormat: 'yy-mm-dd', stepMinute: 10 });
            $$('tbOE').datetimepicker({ dateFormat: 'yy-mm-dd', stepMinute: 10 });

            $('#btnEdit').click(function () {
                $('.del-cell', '#referrers-table').show();
                $('.name-cell', '#referrers-table').find(':text').show();
                $('.name-cell', '#referrers-table').find('a').hide();
                window.setTimeout(function () {
                    $('#btnEditSubmit').show();
                    $('#btnEditCancel').show();
                    $('#btnEdit').hide();
                }, 10);
            });

            $('#btnEditSubmit').click(function () {
                var delItems = [];
                var targets = $('.sel-id:checked', '#referrers-table');
                targets.each(function () {
                    delItems.push({ "id": $(this).val() });
                });
                var editItems = [];
                $('.name-cell', '#referrers-table').each(function () {
                    var id = $(this).closest('tr').find('.sel-id').val();
                    var oldText = $(this).find('a').text();
                    var newText = $(this).find(':text').val();
                    if (oldText !== newText) {
                        editItems.push({ "id": id, name: newText });
                    }
                });
                if (delItems.length === 0 && editItems.length=== 0) {
                    alert('請先選擇要刪除的項目，或是編輯名稱。');
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "Referrer.aspx/MarkDeleted",
                    data: JSON.stringify({ delData: JSON.stringify(delItems), editData: JSON.stringify(editItems) }),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        location.href = "Referrer.aspx";
                    }
                });
            });

            $('#btnEditCancel').click(function () {
                $('#btnEditSubmit').hide();
                $('#btnEditCancel').hide();
                $('#btnEdit').show();
                $('.sel-id:checked', '#referrers-table').removeAttr('checked');
                $('.del-cell', '#referrers-table').hide();
                $('.name-cell', '#referrers-table').each(function () {
                    var text = $(this).find('a').text();
                    $(this).find(':text').val(text).hide();
                    $(this).find('a').show();
                });
            });

            // referrer 
            $('#btnEditCampaign').click(function () {
                $('.del-cell', '#campaigns-table').show().find('.sel-id').removeAttr('checked');
                $('#campaigns-table tbody').find('tr').each(function () {
                    $(this).find('td').each(function () {
                        $(this).find('.view-elem').hide();
                        $(this).find('.edit-elem').show();
                        var name = $(this).find('.name-view-elem').text();                        
                        var duration = parseInt($(this).find('.duration-view-elem').text(), 10);
                        var oneShot = $.trim($(this).find('.oneShot-view-elem').text()) === "是";
                        var isExternal = $.trim($(this).find('.isExternal-view-elem').text()) === "是";
                        $(this).find('.name-edit-elem').val(name);
                        $(this).find('.duration-edit-elem').val(new String(duration));
                        if (oneShot) {
                            $(this).find('.oneShot-edit-elem').attr('checked', true);
                        } else {
                            $(this).find('.oneShot-edit-elem').removeAttr('checked');
                        }
                        if (isExternal) {
                            $(this).find('.isExternal-edit-elem').attr('checked', true);
                        } else {
                            $(this).find('.isExternal-edit-elem').removeAttr('checked');
                        }
                    });
                });
                $('#btnEditCampaignSubmit,#btnEditCampaignCancel').show();
                $('#btnEditCampaign').hide();
            });

            $('#btnEditCampaignSubmit').click(function () {
                var delItems = [];
                var targets = $('.sel-id:checked', '#campaigns-table');
                targets.each(function () {
                    delItems.push({ "id": $(this).val() });
                });
                var editItems = [];
                $('#campaigns-table tbody').find('tr').each(function () {                                       
                    var code = $(this).find('.code-elem').text();
                    var oldName = $(this).find('.name-view-elem').text();
                    var newName = $(this).find('.name-edit-elem').val();
                    var oldDuration = parseInt($(this).find('.duration-view-elem').text(), 10);
                    var newDuration = parseInt($(this).find('.duration-edit-elem').val(), 10);
                    var oldOneShot = $.trim($(this).find('.oneShot-view-elem').text()) === "是";
                    var newOneShot = $(this).find('.oneShot-edit-elem').is(':checked');
                    var oldIsExternal = $.trim($(this).find('.isExternal-view-elem').text()) === "是";
                    var newIsExternal = $(this).find('.isExternal-edit-elem').is(':checked');

                    if (oldName !== newName || oldDuration !== newDuration || oldOneShot !== newOneShot || oldIsExternal !== newIsExternal) {
                        editItems.push({"id": "<%=ReferrerId%>", "code": code, "name": newName, "duration": newDuration, "oneShot": newOneShot, "isExternal": newIsExternal });
                    }
                });
                if (delItems.length === 0 && editItems.length === 0) {
                    alert('請選擇要刪除的項目，或請編輯資料。');
                    return;
                }

                $.ajax({
                    type: "POST",
                    url: "Referrer.aspx/UpdateCampaign",
                    data: JSON.stringify({ delData: JSON.stringify(delItems), editData: JSON.stringify(editItems) }),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        location.href = "?id=<%=ReferrerId%>";
                    }
                });
            });

            $('#btnEditCampaignCancel').click(function () {
                $('.del-cell', '#campaigns-table').hide();
                $(this).find('.view-elem').show();
                $(this).find('.edit-elem').hide();
                $('#campaigns-table tbody').find('tr').each(function () {
                    $(this).find('.view-elem').show();
                    $(this).find('.edit-elem').hide();
                });
                $('#btnEditCampaignSubmit,#btnEditCampaignCancel').hide();
                $('#btnEditCampaign').show();
            });


        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:PlaceHolder ID="phReferences" runat="server" Visible="false">
        <div id="myContainer" style="position: relative">
            <button id="btnEdit" type="button" class="cmdbtn" style="right: 10px; top: 10px;">
                編輯模式
            </button>
            <button type="button" id="btnEditSubmit" class="cmdbtn" style="right: 10px; top: 10px; display: none">
                確定
            </button>
            <button type="button" id="btnEditCancel" class="cmdbtn" style="right: 80px; top: 10px; display: none">
                取消
            </button>
            <h1 class="header-title">Referrers Listing</h1>
            <asp:Repeater ID="repReferences" runat="server">
                <HeaderTemplate>
                    <table class="responstable" id="referrers-table">
                        <tbody>
                            <tr>
                                <th class="del-cell" style="display: none">刪除</th>
                                <th>名稱</th>
                                <th>編碼</th>
                                <th>建立日期</th>
                            </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="del-cell" style="display: none">
                            <input type="checkbox" class="sel-id" value="<%#Eval("Guid") %>" style="width: 60px; height: 20px; cursor: pointer;" />
                        </td>
                        <td class="name-cell"><a href="?id=<%#Eval("Guid") %>"><%#Eval("Name") %></a>
                            <input type="text" value="<%#Eval("Name") %>" style="display:none; width:100%; font-size:11px" />
                        </td>
                        <td><%#Eval("Code") %></td>
                        <td><%#Eval("CreateTime", "{0:yyyy/MM/dd}") %></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                </table>
                </FooterTemplate>
            </asp:Repeater>
            <div>
                名稱:
                <asp:TextBox ID="tAN" runat="server" CssClass="fm-input" />
                編碼:
                <asp:TextBox ID="tAC" runat="server" CssClass="fm-input" />
                <asp:Button ID="bAdd" runat="server" Text="新增" OnClick="bR_Click" CssClass="btn btn-primary" />
            </div>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="pd" runat="server" Visible="false">
        <div id="myContainer" style="position: relative; padding-right:20px">
        <button id="btnHome" type="button" class="cmdbtn" style="right: 10px; top: 60px;" onclick="location.href='referrer.aspx'">
            回到列表
        </button>
        <button id="btnEditCampaign" type="button" class="cmdbtn" style="right: 120px; top: 60px;">
            編輯模式
        </button>
        <button type="button" id="btnEditCampaignSubmit" class="cmdbtn" style="right: 120px; top: 60px; display: none">
            確定
        </button>
        <button type="button" id="btnEditCampaignCancel" class="cmdbtn" style="right: 190px; top: 60px; display: none">
            取消
        </button>
        <h1 class="header-title">Referrer: <%=Referral.Name %></h1>        
            <h3>Campaign:</h3>
            <table class="responstable" id="campaigns-table" style="width:720px">
                <thead>
                <tr>
                    <th class="del-cell col-md-1" style="display: none">刪除</th>
                    <th class="col-md-5">名稱</th><th class="col-md-2">編碼</th><th class="col-md-2">時效</th><th class="col-md-1" nowrap="nowrap">單次</th><th class="col-md-1" nowrap="nowrap">外部導購商</th>
                </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="repCampaigns" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td class="del-cell" style="display: none">
                                    <input type="checkbox" class="sel-id" value="<%#Eval("Guid") %>" style="width: 60px; height: 20px; cursor: pointer;" />
                                </td>
                                <td class="name-cell">
                                    <a class="view-elem name-view-elem" href="?id=<%#Eval("ReferrerGuid") %>&cid=<%#Eval("Code") %>" target="_blank"><%#Eval("Name") %></a>
                                    <input class="edit-elem name-edit-elem" type="text" name="campaignName" value="<%#Eval("Name") %>" style="display:none; width:90%" />
                                </td>
                                <td class="code-cell">
                                    <span class="code-elem"><%#Eval("Code") %></span>
                                </td>
                                <td class="sessionDuration-cell">
                                    <span class="view-elem duration-view-elem"><%#Eval("SessionDuration") %></span>
                                    <input class="edit-elem duration-edit-elem" type="text" name="campaignSessionDuration" value="<%#Eval("SessionDuration") %>" style="display:none; width:90%"/>
                                </td>
                                <td class="oneShotAction-cell">
                                    <span class="view-elem oneShot-view-elem"><%#(bool)Eval("OneShotAction") ? " 是 " : "" %></span>
                                    <input class="edit-elem oneShot-edit-elem" name="campaignOneShotAction" type="checkbox" 
                                        style="display:none; width:60px;height:20px; cursor:pointer" <%#(bool)Eval("OneShotAction") ? "checked" : "" %> />
                                </td>
                                <td class="IsExternal-cell">
                                    <span class="view-elem isExternal-view-elem"><%#(bool)Eval("IsExternal") ? " 是 " : "" %></span>
                                    <input class="edit-elem isExternal-edit-elem" name="campaignIsExternal" type="checkbox" 
                                        style="display:none; width:60px;height:20px; cursor:pointer" <%#(bool)Eval("IsExternal") ? "checked" : "" %> />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            
            <h2>新增Campaign</h2>
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="tACN">名稱:</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="tACN" runat="server" ClientIDMode="static" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="tACC">編碼:</label>
                    <div class="col-sm-10">          
                        <asp:TextBox ID="tACC" runat="server" ClientIDMode="static"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="tACT">時效:</label>
                    <div class="col-sm-10">          
                        <asp:TextBox ID="tACT" runat="server" Columns="4" ClientIDMode="static" />秒
                    </div>
                </div>
                <div class="form-group">        
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                            <label>
                                <asp:CheckBox ID="cbS" runat="server" ClientIDMode="static" /> 單次性
                            </label>
                            <label>
                                <asp:CheckBox ID="cbExternal" runat="server" ClientIDMode="static" /> 外部導購商
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">        
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:Button ID="bAC" runat="server" Text="新增" CssClass="btn btn-primary" OnClick="bC_Click" />
                    </div>
                </div>                
            </div>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="pc" Visible="false" runat="server">
        <div id="myContainer" style="position: relative; padding-right:20px">
            <h1 class="header-title">Campaign:<%=CampaignCode %></h1>
        <fieldset>
            <br />
            篩選  
            <asp:DropDownList ID="aTypeList" runat="server">
                <asp:ListItem Text=""></asp:ListItem>
                <asp:ListItem Text="購買" Value="0"></asp:ListItem>
                <asp:ListItem Text="註冊" Value="1"></asp:ListItem>
                <asp:ListItem Text="訂閱" Value="2"></asp:ListItem>
            </asp:DropDownList>

            <div>從
                <asp:TextBox ID="tbOS" runat="server" style="width:140px" class="required"></asp:TextBox>
                至
                <asp:TextBox ID="tbOE" runat="server" style="width:140px" class="required"></asp:TextBox>
                <asp:Button ID="btnQueryRecord" runat="server" Text="查詢" OnClick="btnQueryRecord_Click" CssClass="btn btn-primary" style="margin-left:12px"/>
                <asp:Button ID="btnExportExcel" runat="server" Text="匯出" OnClick="btnExportExcel_Click" style="margin-left:12px" CssClass="btn btn-primary" />
            </div>
        </fieldset>

        <asp:DataGrid ID="dCD" runat="server" AutoGenerateColumns="false" OnItemDataBound="dCD_ItemDataBound" EnableViewState="false" CssClass="campaign-records-table">
            <Columns>
                <asp:TemplateColumn HeaderText="Type">
                    <ItemTemplate>
                        <asp:Literal ID="lat" runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="ReferenceId" HeaderText="ReferenceId" />
                <asp:TemplateColumn HeaderText="時間">
                    <ItemTemplate>
                        <asp:Literal ID="lct" runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="Department" HeaderText="館別" />
                <asp:BoundColumn DataField="City" HeaderText="商家城市" />
                <asp:BoundColumn DataField="ItemName" HeaderText="名稱" />
                <asp:BoundColumn DataField="Price" HeaderText="單價" />
                <asp:BoundColumn DataField="SubTotal" HeaderText="總價" />
                <asp:BoundColumn DataField="Total" HeaderText="折扣後金額" />
                <asp:BoundColumn DataField="Remark" HeaderText="相關資訊" />
                <asp:TemplateColumn HeaderText="業績歸屬館別">
                    <ItemTemplate>
                        <asp:Literal ID="lAccBus" runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="上檔分類">
                    <ItemTemplate>
                        <asp:Literal ID="lDealType" runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>

        </div>
    </asp:PlaceHolder>
</asp:Content>

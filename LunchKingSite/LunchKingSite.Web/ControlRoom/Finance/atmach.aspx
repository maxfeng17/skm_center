﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="atmach.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Finance.atmach" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("jquery", "1.5.2");
        google.load("jqueryui", "1.8.13");
    </script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="../../Tools/js/downloadify.min.js"></script>
    <script type="text/javascript" src="../../Tools/js/swfobject.js"></script>
    <script type="text/javascript">
        function $$(id, context) {
            var el = $("#" + id, context);
            if (el.length < 1)
                el = $("*[id$=" + id + "]", context);
            return el;
        }
        $(document).ready(function () {
            $$('TB').hide();
            $$('tbOS').datepicker({ dateFormat: 'yy-mm-dd' });
        });
        function Download() {
            alert($('#filePath').val());
        }
    </script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8.13.custom.css"
        rel="stylesheet" />
    <style type="text/css">
        .tdhide
        {
            display: none;
        }
    </style>
    <style type="text/css">
        body
        {
            font-size: 10px;
        }
        .ui-timepicker-div .ui-widget-header
        {
            margin-bottom: 8px;
        }
        .ui-timepicker-div dl
        {
            text-align: left;
        }
        .ui-timepicker-div dl dt
        {
            height: 25px;
        }
        .ui-timepicker-div dl dd
        {
            margin: -25px 0 10px 65px;
        }
        .ui-timepicker-div td
        {
            font-size: 90%;
        }
        .toFixTheWidth
        {
            word-wrap: break-word; word-break:break-all;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:Panel ID="PanelMain" runat="server">
            <asp:Button ID="Button2" runat="server" Text="取得退貨列表" OnClick="Button2_Click" />
            <asp:Button ID="Button3" runat="server" Text="瀏覽已產生資料" OnClick="Button3_Click" />
            <asp:Button ID="Button5" runat="server" Text="匯入ACH ATM退款結果" OnClick="Button5_Click" />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br/>
            <asp:Button ID="btnToBatchImport" runat="server" Text="匯入ACH商家匯款結果" OnClick="btnToBatchImport_Click" />
        </asp:Panel>
        <asp:Panel ID="Panel1" runat="server" Visible="false">
            <div>
                選擇欲退款日，需比今日晚兩天且不為假日。<asp:TextBox ID="tbOS" runat="server" class="required"></asp:TextBox>
                <asp:Button ID="Button1" runat="server" Text="確認" OnClick="Button1_Click" /></div>
        </asp:Panel>
        <div>
            <asp:Panel ID="Panel2" runat="server" Visible="false">
                <p id="downloadify">
                    需要安裝 Flash 10 才能下載檔案</p>
            </asp:Panel>
            <asp:TextBox ID="TB" runat="server" TextMode="MultiLine"></asp:TextBox>
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        </div>
        <asp:Panel ID="Panel3" runat="server" Visible="false">
            <asp:TextBox ID="TB2" runat="server" TextMode="MultiLine" Width="800px" Height="400px"></asp:TextBox><br />
            <asp:Button ID="btnImport" runat="server" Text="確定匯入" OnClick="btnImport_Click" />
        </asp:Panel>
        <asp:Panel ID="PanelBatchImport" runat="server" Visible="false">
            <asp:RadioButtonList ID="rblFundTransferType" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Value="1">ACH匯款</asp:ListItem>
                <asp:ListItem Value="2">人工匯款</asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <asp:FileUpload ID="fuBatchImport" runat="server" />
            <br />
            <asp:Button ID="btnBatchImport" runat="server" Text="確定匯入" OnClick="btnBatchImport_Click" />
        </asp:Panel>
        <asp:GridView ID="Gv" runat="server" Visible="False" EmptyDataText="無資料" AutoGenerateColumns="False">
            <Columns>
				<asp:TemplateField HeaderText="使用者" AccessibleHeaderText="使用者">
            		<ItemTemplate>
            			<%#MemberFacade.GetUserName(((ViewCtAtmRefund)(Container.DataItem)).UserId) %>
					</ItemTemplate>					
				</asp:TemplateField>
                <asp:TemplateField AccessibleHeaderText="訂單編號" HeaderText="訂單編號">
                    <ItemTemplate>
                        <asp:HyperLink ID="hlOrder" runat="server" Target="_blank" NavigateUrl='<%# "~/ControlRoom/Order/order_detail.aspx?oid=" + Eval("OrderGuid") %>' Text='<%# Eval("OrderId") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField AccessibleHeaderText="付款方式" DataField="PaymentType" HeaderText="付款方式" />
                <asp:BoundField AccessibleHeaderText="退款金額" DataField="Amount" HeaderText="退款金額" />
                <asp:BoundField AccessibleHeaderText="帳號名" DataField="AccountName" HeaderText="帳號名" />
                <asp:BoundField AccessibleHeaderText="銀行代碼" DataField="BankCode" HeaderText="銀行代碼" />
                <asp:BoundField AccessibleHeaderText="帳戶號碼" DataField="AccountNumber" HeaderText="帳戶號碼" />
                <asp:BoundField AccessibleHeaderText="申請時間" DataField="CreateTime" HeaderText="申請時間" />
                <asp:BoundField AccessibleHeaderText="退款時間" DataField="TargetDate" HeaderText="退款時間" />
                <asp:BoundField AccessibleHeaderText="退貨結果" DataField="FailReason" HeaderText="退貨結果" />
                <asp:TemplateField AccessibleHeaderText="退貨發票" HeaderText="退貨發票">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" CssClass="toFixTheWidth" Text='<%# GetReturnedInvoice(((ViewCtAtmRefund)(Container.DataItem)).Si) %>'></asp:Label>
                        <asp:HiddenField ID="hdSi" runat="server" Value='<%# Eval("Si")%>' />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="15%" Wrap="true" />
                </asp:TemplateField>
               <asp:BoundField AccessibleHeaderText="身分證字號" DataField="ID" HeaderText="身分證字號" />
            </Columns>
        </asp:GridView>
        <asp:GridView ID="Gv2" runat="server" Visible="false" EmptyDataText="無資料" AutoGenerateColumns="False"
            PageSize="50" OnRowCommand="Gv2_RowCommand" OnRowCreated="Gv2RowCreate" AllowPaging="True"
            OnPageIndexChanging="Gv2Paging" OnRowDataBound="Gv2DataBound">
            <Columns>
                <asp:BoundField AccessibleHeaderText="產生時間" DataField="CreateTime" HeaderText="產生時間" />
                <asp:BoundField AccessibleHeaderText="目標退款日" DataField="TargetDate" HeaderText="目標退款日" />
                <asp:BoundField AccessibleHeaderText="金額" DataField="Amount" HeaderText="金額" />
                <asp:BoundField AccessibleHeaderText="總數" DataField="Count" HeaderText="總數" />
                <asp:BoundField AccessibleHeaderText="產生者" DataField="UserName" HeaderText="產生者" />
                <asp:BoundField AccessibleHeaderText="body" DataField="body" HeaderText="body" ControlStyle-CssClass="tdhide" />
                <asp:BoundField AccessibleHeaderText="refundlist" DataField="refundlist" HeaderText="refundlist"
                    ControlStyle-CssClass="tdhide" />
                <asp:TemplateField HeaderText="功能">
                    <ItemTemplate>
                        <asp:Button ID="GvBtn" runat="server" Text="瀏覽ACH文件" CommandName="AddToTextBox" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />
                        <asp:Button ID="BtnList" runat="server" Text="瀏覽清單" CommandName="ViewList" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <br />
        <script type="text/javascript">
            Downloadify.create('downloadify', {
                filename: 'CTATMACHP01.txt',
                data: $$('TB').val(),
                onComplete: function () { alert('檔案已儲存!'); },
                onCancel: function () { alert('已取消'); },
                onError: function () { alert('無資料可儲存!'); },
                transparent: false,
                swf: '../images/downloadify.swf',
                downloadImage: '../images/download.png',
                width: 100,
                height: 30,
                transparent: true,
                append: false
            });
        </script>
    </div>
    <asp:Button ID="ButtonTest" runat="server" Text="test" Visible="false" OnClick="ButtonTest_Click">
    </asp:Button>
</asp:Content>

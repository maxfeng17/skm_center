﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="EntrustSellOrderList.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Finance.EntrustSellOrderList" %>

<asp:Content runat="server" ContentPlaceHolderID="SSC">
    <script src="<%=ResolveClientUrl("~/Tools/js/jquery-ui.1.8.min.js") %>" type="text/javascript"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/homecook.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <style type="text/css">
        #search_table td
        {
            padding: 5px;
        }
        .btn_search
        {
            letter-spacing: 3px;
            padding: 3px 3px 3px 10px;
            cursor: pointer;
        }
        .btn_export
        {
            padding: 3px 3px 3px 10px;
            cursor: pointer;
        }
        .btn_book
        {
            letter-spacing: 3px;
            padding: 3px 3px 3px 10px;
            cursor: pointer;
            margin-left: 20px;
        }

        .list_table
        {
            border-collapse: collapse;
        }
        .list_table td
        {
            padding: 5px;
        }
        .list_table .row0
        {
            background-color: #f7f7de;
            height: 30px;
        }
        .list_table .row1
        {
            background-color: white;
            height: 30px;
        }
        .list_table .cell_text
        {
            text-align: left;
        }
        .list_table .cell_num
        {
            text-align: right;
        }
        .ui-datepicker 
        {
            font-size:10px;
        }
        input.date
        {
            width: 80px;
        }
    </style>
    <script type="text/javascript">
        $().ready(function () {
            setupdatepicker('<%=tbSTime.ClientID%>', '<%=tbETime.ClientID%>', 3);
        })
    </script>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <h2>
        代收轉付訂單</h2>
    <fieldset style="border: double 3px black; width: 650px;">
        <table style="width: 650px;" id="search_table">
            <tr>
                <td colspan="2">
                    <asp:DropDownList ID="ddlQueryItem" runat="server">
                        <asp:ListItem Text="訂單編號" Value="OrderId" />
                        <asp:ListItem Text="會員帳號" Value="MemberId" />
                        <asp:ListItem Text="會員姓名" Value="MemberName" />
                        <asp:ListItem Text="廠商別" Value="SellerName" />
                        <asp:ListItem Text="檔次BID/商品QC" Value="ProductQc" Selected="true" />
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearchInput" runat="server" style="width:265px" />
                </td>
            </tr>
            <tr>
                <td class="style2" valign="middle" colspan="2">
                    核銷日期：<asp:TextBox ID="tbSTime" runat="server" CssClass="date" />
                    ～
                    <asp:TextBox ID="tbETime" runat="server" CssClass="date" />
                </td>
            </tr>
            <tr>
                <td valign="middle" nowrap="nowrap">
                    訂單狀態：<asp:DropDownList ID="ddlOrderStatus" runat="server">
                             <asp:ListItem Text="全部" Value="all" />
                             <asp:ListItem Text="核銷" Value="verified" Selected="true" />
                             <asp:ListItem Text="反核銷" Value="undoVerified" />
                             <asp:ListItem Text="強制退貨" Value="refundedForced" />
                         </asp:DropDownList>
                </td>
                <td style="text-align: right; margin-right: 40px">
                    <asp:Button ID="btnSearch" CssClass="btn_search" runat="server" Text="搜尋" OnClick="btnSearch_Click" />
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <asp:Button ID="btnOutput" CssClass="btn_export" runat="server" Text="匯出資料" OnClick="btnExport_Click" />
    <br />
    <br />
    <div style="margin-top: 5px;">
        <asp:Repeater ID="repOrders" runat="server" EnableViewState="false">
            <HeaderTemplate>
                <table class="list_table" border="1" style="width: 2000px;
                    border-collapse: collapse">
                    <thead>
                        <tr style="background-color: #6b696b; height: 30px; color: white; font-size: 14px;
                            font-weight: bold">                      
                            <th scope="col" style="width:60px">
                                <%=GetMixedDateTitle() %>
                            </th>
                            <th scope="col" style="width:80px">
                                訂單編號
                            </th>
                            <th scope="col" style="width:60px">
                                訂購人
                            </th>
                            <th scope="col" style="width:100px">
                                檔次BID/商品QC
                            </th>
                            <th scope="col" style="width:100px">
                                商品名稱
                            </th>
                            <th scope="col" style="width:30px">
                                單價
                            </th>
                            <th scope="col" style="width:30px">
                                訂購數量
                            </th>
                            <th scope="col">
                                付款方式
                            </th>
                            <th scope="col" style="width:50px">
                                Payeasy購物金
                            </th>
                            <th scope="col" style="width:50px">
                                17Life購物金
                            </th>
                            <th scope="col" style="width:40px">
                                紅利
                            </th>
                            <th scope="col" style="width:40px">
                                信用卡
                            </th>
                            <th scope="col" style="width:40px">
                                折價券
                            </th>
                            <th scope="col">
                                訂單總金額
                            </th>
                            <th scope="col" style="width:40px">
                                成本
                            </th>
                            <th scope="col" style="width:40px">
                                利潤
                            </th>
                            <th scope="col">
                                訂單狀態
                            </th>
                            <th scope="col" style="width:100px">
                                廠商別
                            </th>
                            <th scope="col" style="width:80px">
                                付款日
                            </th>
                        </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="row<%#Container.ItemIndex % 2 %>">
                    <td class="cell_text">
                        <%#Eval("VerifiedTime","{0:yyyy/MM/dd}")%>
                    </td>
                    <td class="cell_text">
                        <%#Eval("OrderId") %>
                    </td>
                    <td class="cell_text">
                        <%#Eval("MemberName") %>
                    </td>
                    <td class="cell_text">
                        <%#Eval("ProductQc") %>
                    </td>
                    <td class="cell_text">
                        <%#Eval("ItemName") %>
                    </td>
                    <td class="cell_num">
                        <%#Eval("ItemPrice", "{0:0}")%>
                    </td>
                    <td class="cell_num">
                        <%#Eval("ItemQuantity") %>
                    </td>
                    <td class="cell_num">
                        <%#GetPaymentWay(Eval("Pcash"), Eval("Scash"), Eval("Bcash"), Eval("CreditCard"), Eval("DiscountAmount"))%>
                    </td>
                    <td class="cell_num">
                        <%#Eval("Pcash")%>
                    </td>
                    <td class="cell_num">
                        <%#Eval("Scash")%>
                    </td>
                    <td class="cell_num">
                        <%#Eval("Bcash")%>
                    </td>
                    <td class="cell_num">
                        <%#Eval("CreditCard")%>
                    </td>
                    <td class="cell_num">
                        <%#Eval("DiscountAmount")%>
                    </td>
                    <td class="cell_num">
                        <%#Eval("Amount")%>
                    </td>
                    <td class="cell_num">
                        <%#Eval("Cost", "{0:0}")%>
                    </td>
                    <td class="cell_num">
                        <%#Eval("Profit")%>
                    </td>
                    <td class="cell_num">
                        <%#GetOrderStatus(Eval("VerifiedStatus"), Eval("VerifiedSpecialStatus"), Eval("UndoFlag"))%>
                    </td>
                    <td class="cell_num">
                        <%#Eval("SellerName") %>
                    </td>
                    <td class="cell_num">
                        <%#Eval("CreateTime", "{0:yyyy/MM/dd}")%>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </tbody> </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</asp:Content>

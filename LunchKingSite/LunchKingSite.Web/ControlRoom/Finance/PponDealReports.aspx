﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="PponDealReports.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Finance.PponDealReports" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <p>
            <asp:TextBox ID="tbBeginDate" runat="server"></asp:TextBox>
            <cc1:CalendarExtender ID="ceBeginDate" TargetControlID="tbBeginDate" runat="server"></cc1:CalendarExtender>&nbsp;～&nbsp;
            <asp:TextBox ID="tbEndDate" runat="server"></asp:TextBox>
            <cc1:CalendarExtender ID="ceEndDate" TargetControlID="tbEndDate" runat="server"></cc1:CalendarExtender>&nbsp;
            <asp:Button ID="btnSearch" OnClick="btnSearch_Click" runat="server" Text="開始查詢"></asp:Button>
        </p>

        <asp:GridView ID="gvDepartment" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateField HeaderText="館別">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetDepartment(DataBinder.Eval(Container,"DataItem.Department").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="檔數" />
                <asp:BoundField HeaderText="訂單數" DataField="Orders" />
                <asp:BoundField HeaderText="銷售份數" />
                <asp:BoundField HeaderText="商品金額" DataField="Amount" />
                <asp:BoundField HeaderText="運費收入" />
                <asp:BoundField HeaderText="紅利折抵" />
                <asp:BoundField HeaderText="應收總額" />
                <asp:BoundField HeaderText="毛利金額" />
                <asp:BoundField HeaderText="毛利率" />
                <asp:BoundField HeaderText="平均每檔份數" />
                <asp:BoundField HeaderText="平均每檔金額" />
                <asp:BoundField HeaderText="平均每單均價" />
                <asp:BoundField HeaderText="平均每份售價" />
            </Columns>
        </asp:GridView>
</asp:Content>
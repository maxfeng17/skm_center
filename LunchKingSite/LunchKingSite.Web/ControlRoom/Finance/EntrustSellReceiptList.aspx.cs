﻿using System;
using System.Text;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System.Collections.Generic;
using LunchKingSite.DataOrm;
using log4net;

namespace LunchKingSite.Web.ControlRoom.Finance
{
    public partial class EntrustSellReceiptList : RolePage, IEntrustSellReceiptListView
    {
        public string FtpHost { get; set; }
        public string FtpUsername { get; set; }
        public string FtpPassword { get; set; }
        public string FtpRemoteUploadFolder { get; set; }
        public string FtpRemoteCompletedFolder { get; set; }

        private static ILog logger = LogManager.GetLogger(typeof(EntrustSellReceiptList));

        #region props
        private EntrustSellReceiptListPresenter _presenter;
        public EntrustSellReceiptListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string FilterColumn
        {
            get
            {
                return ddlQueryItem.SelectedValue;
            }
            set
            {
                ddlQueryItem.SelectedValue = "";
            }
        }

        public string FilterValue
        {
            get
            {
                return txtSearchInput.Text;
            }
            set
            {
                txtSearchInput.Text = value;
            }
        }

        public DateTime? FilterVerifiedStartTime
        {
            get
            {
                DateTime dt;
                if (DateTime.TryParse(tbSTime.Text, out dt))
                {
                    return dt;
                }
                return null;
            }
            set
            {
                tbSTime.Text = value == null ? "" : value.Value.ToString("'yyyy/MM/dd");
            }
        }

        public DateTime? FilterVerifiedEndTime
        {
            get
            {
                DateTime dt;
                if (DateTime.TryParse(tbETime.Text, out dt))
                {
                    return dt;
                }
                return null;
            }
            set
            {
                tbETime.Text = value == null ? "" : value.Value.ToString("'yyyy/MM/dd");
            }
        }

        public bool? FilterIsPhysical
        {
            get
            {
                return chkIsPhysical.Checked ? true : (bool?)null;
            }
        }

        public bool? FilterIsExported
        {
            get
            {
                return chkIsExported.Checked ? true : (bool?)null;
            }
        }

        public bool? FilterHasReceiptCode
        {
            get
            {
                return chkHasReceiptCode.Checked ? true : (bool?)null;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                Presenter.OnViewInitialized();
            }
            Presenter.OnViewLoaded();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.SearchClicked != null)
            {
                this.SearchClicked(this, e);
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (ExportClicked != null)
            {
                ExportClicked(this, e);
            }
        }

        #region events
        public event EventHandler SearchClicked;
        public event EventHandler ExportClicked;
        #endregion

        #region IEntrustSellReceiptListView Members

        public void SetContent(List<ViewEntrustSellReceipt> receipts)
        {
            repReceipts.DataSource = receipts;
            repReceipts.DataBind();
        }

        #endregion


        public void SetMessage(string msg)
        {
            lbMessage.Text = msg;
        }
    }

}
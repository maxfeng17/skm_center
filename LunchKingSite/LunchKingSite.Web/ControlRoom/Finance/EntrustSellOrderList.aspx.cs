﻿using System;
using System.Collections.Generic;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace LunchKingSite.Web.ControlRoom.Finance
{
    public partial class EntrustSellOrderList : RolePage, IEntrustSellOrderListView
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                Presenter.OnViewInitialized();
            }
            Presenter.OnViewLoaded();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (SearchClicked != null)
            {
                SearchClicked(this, e);
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (ExportToExcel != null)
            {
                ExportToExcel(this, e);
            }
        }

        #region properties

        private EntrustSellOrderListPresenter _presenter;
        public EntrustSellOrderListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string FilterOrderStatus
        {
            get
            {
                return ddlOrderStatus.SelectedValue;
            }
        }

        public string FilterColumn
        {
            get
            {
                return ddlQueryItem.SelectedValue;
            }
        }

        public string FilterValue
        {
            get
            {
                return txtSearchInput.Text;
            }
        }

        public DateTime? FilterStartTime
        {
            get
            {
                DateTime dt;
                if (DateTime.TryParse(tbSTime.Text, out dt))
                {
                    return dt;
                }
                return null;
            }
        }

        public DateTime? FilterEndTime
        {
            get
            {
                DateTime dt;
                if (DateTime.TryParse(tbETime.Text, out dt))
                {
                    return dt;
                }
                return null;
            }
        }

        #endregion

        #region events
        public event EventHandler SearchClicked;
        public event EventHandler ExportToExcel;
        #endregion

        public void Export(ViewEntrustsellOrderCollection orders)
        {
            Workbook workbook = new HSSFWorkbook();
            // 新增試算表。
            Sheet sheet = workbook.CreateSheet("代收轉付訂單");
            Row cols = sheet.CreateRow(0);
            cols.CreateCell(0).SetCellValue(GetMixedDateTitle());
            cols.CreateCell(1).SetCellValue("訂單編號");
            cols.CreateCell(2).SetCellValue("訂購人");
            cols.CreateCell(3).SetCellValue("商品QC");
            cols.CreateCell(4).SetCellValue("商品名稱");
            cols.CreateCell(5).SetCellValue("單價");
            cols.CreateCell(6).SetCellValue("訂購數量");
            cols.CreateCell(7).SetCellValue("付款方式");
            cols.CreateCell(8).SetCellValue("Payeasy購物金");
            cols.CreateCell(9).SetCellValue("17Life購物金");
            cols.CreateCell(10).SetCellValue("紅利");
            cols.CreateCell(11).SetCellValue("信用卡");
            cols.CreateCell(12).SetCellValue("折價券");
            cols.CreateCell(13).SetCellValue("訂單總金額");
            cols.CreateCell(14).SetCellValue("成本");
            cols.CreateCell(15).SetCellValue("利潤");
            cols.CreateCell(16).SetCellValue("訂單狀態");
            cols.CreateCell(17).SetCellValue("廠商別");
            cols.CreateCell(18).SetCellValue("付款日");

            int rowIdx = 1;
            foreach (ViewEntrustsellOrder order in orders)
            {
                Row row = sheet.CreateRow(rowIdx);
                row.CreateCell(0).SetCellValue(order.VerifiedTime == null ? "" : order.VerifiedTime.Value.ToString("yyyy/MM/dd"));
                row.CreateCell(1).SetCellValue(order.OrderId);
                row.CreateCell(2).SetCellValue(order.MemberName);
                row.CreateCell(3).SetCellValue(order.ProductQc);
                row.CreateCell(4).SetCellValue(order.ItemName);
                row.CreateCell(5).SetCellValue((int)order.ItemPrice);
                row.CreateCell(6).SetCellValue(order.ItemQuantity);
                row.CreateCell(7).SetCellValue(GetPaymentWay(order.Pcash, order.Scash, order.Bcash, order.CreditCard, order.DiscountAmount));
                row.CreateCell(8).SetCellValue(order.Pcash);
                row.CreateCell(9).SetCellValue(order.Scash);
                row.CreateCell(10).SetCellValue(order.Bcash);
                row.CreateCell(11).SetCellValue(order.CreditCard);
                row.CreateCell(12).SetCellValue(order.DiscountAmount);
                row.CreateCell(13).SetCellValue(order.Amount);
                row.CreateCell(14).SetCellValue((int)order.Cost);
                row.CreateCell(15).SetCellValue(order.Profit == null ? "" : order.Profit.Value.ToString());
                row.CreateCell(16).SetCellValue(GetOrderStatus(order.VerifiedStatus, order.VerifiedSpecialStatus, order.UndoFlag));
                row.CreateCell(17).SetCellValue(order.SellerName);
                row.CreateCell(18).SetCellValue(order.CreateTime.ToString("yyyy/MM/dd"));
                rowIdx++;
            }
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename=代收轉付訂單_{0}.xls", DateTime.Now.ToString("yyyy-MM-dd")));
            workbook.Write(Response.OutputStream);
        }

        public void SetOrderList(ViewEntrustsellOrderCollection orders)
        {
            repOrders.DataSource = orders;
            repOrders.DataBind();
        }

        protected string GetOrderStatus(object verifiedStatus, object verifiedSpecialStatus, object verifiedUndoFlag)
        {
            //資料本來就過瀘只取 verifiedStatus == 2(已核銷) 或 verifiedStatus == 3 且 specialStatus & 1 == 1(因強制退貨 狀態改成退貨)
            //所以這邊判斷 oStatus 是2或3就夠了
            TrustStatus status = (TrustStatus)((int)verifiedStatus);
            int undoFlag = (int)verifiedUndoFlag;
            int specialStatus = (int)verifiedSpecialStatus;
            if (status == TrustStatus.Verified && specialStatus == 0 && undoFlag == 0)
            {
                return "核銷";
            }
            if ((status == TrustStatus.Initial || status == TrustStatus.Trusted) && undoFlag == 1)
            {
                return "反核銷";
            }
            if (status == TrustStatus.Refunded && specialStatus == 1 && undoFlag == 1)
            {
                return "強制退貨";
            }
            return "…";
        }

        protected string GetPaymentWay(object pcash, object scash, object bcash, object creditCard, object discountAmount)
        {
            StringBuilder sb = new StringBuilder();
            if (pcash != null && (int)pcash > 0)
            {
                if (sb.Length > 0) sb.Append(',');
                sb.Append("Payeasy購物金");
            }
            if (scash != null && (int)scash > 0)
            {
                if (sb.Length > 0) sb.Append(',');
                sb.Append("17Life購物金");
            }
            if (bcash != null && (int)bcash > 0)
            {
                if (sb.Length > 0) sb.Append(',');
                sb.Append("紅利");
            }
            if (creditCard != null && (int)creditCard > 0)
            {
                if (sb.Length > 0) sb.Append(',');
                sb.Append("信用卡");
            }
            if (discountAmount != null && (int)discountAmount > 0)
            {
                if (sb.Length > 0) sb.Append(',');
                sb.Append("折價券");
            }
            return sb.ToString();
        }

        /// <summary>
        /// 這個顯示用的標題，會因為過瀘訂單狀態不同而不同
        /// </summary>
        /// <returns></returns>
        protected string GetMixedDateTitle()
        {
            if (FilterOrderStatus == "undoVerified")
            {
                return "反核銷日期";
            }
            if (FilterOrderStatus == "refundedForced")
            {
                return "強制退貨日期";
            }
            return "核銷日期";
        }
    }
}
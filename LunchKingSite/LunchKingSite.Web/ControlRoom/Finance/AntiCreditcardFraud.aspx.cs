﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;

namespace LunchKingSite.Web.ControlRoom.Finance
{
    public partial class AntiCreditcardFraud : RolePage
    {
        protected static IOrderProvider op = ProviderFactory.Instance().GetDefaultProvider<IOrderProvider>();
        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            bool controlAllOrders=CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name,SystemFunctionType.SetCreditcardOrderGetAllOrders);
            if (controlAllOrders)
            {
                IsShowAllOrders.Value = "True";
            }
            else 
            {
                IsShowAllOrders.Value = "False";
            }
        }

        [WebMethod]
        public static string GetCdInfo(string strType, string strMain, string strRes, string strRefer, string strBl, string strAllOrder
            , string strSDate, string strEDate, string strSTime, string strETime)
        {
            try
            {
                List<Creditcard> creditcardOrderList = PaymentFacade.GetCreditcardOrderInfo(strType, strMain, strRes, strRefer, strBl, strAllOrder, strSDate, strEDate, strSTime, strETime);
                string json = GetJson(creditcardOrderList);
                return json;
            }
            catch
            {
                return null;
            }
        }

        [WebMethod]
        public static string GetCreditcardFraudDataInfo(string strSDate, string strEDate)
        {
            try
            {
                DateTime sDate = Convert.ToDateTime(strSDate);
                DateTime eDate = Convert.ToDateTime(strEDate);
                CreditcardFraudDatumCollection data = op.CreditcardFraudDataGet(sDate, eDate);
                string json = GetCreditcardFraudDataJson(data);
                return json;
            }
            catch
            {
                return null;
            }
        }

        [WebMethod]
        public static string GetCheckAccountInfo()
        {
            try
            {
                CreditcardFraudDatumCollection data = op.CreditcardFraudDataGet(CreditcardFraudDataType.CheckAccount);
                string json = GetCheckAccountJson(data);
                return json;
            }
            catch
            {
                return null;
            }
        }

        [WebMethod]
        public static string UpdateRefer(string strIds)
        {
            try
            {
                string[] ids = strIds.Split(",");

                AntiCreditcardFraud thisPage = new AntiCreditcardFraud();
                PaymentFacade.SetCreditcardOrderRefer(ids, thisPage.UserName);
                return "true";
            }
            catch
            {
                return "false";
            }
        }

        [WebMethod]
        public static string GetResultList()
        {
            CreditcardProcessor cp = new CreditcardProcessor();
            List<ObjectCreditcardResult> rList = new List<ObjectCreditcardResult>();
            ObjectCreditcardResult r = new ObjectCreditcardResult();
            r.Id = 999999;
            r.Name = "Exceptional";
            r.Title = "所有卡片異常";
            rList.Add(r);
            rList.AddRange(cp.GetResultList());
            var jsonCol = from x in rList
                          select new { Id = x.Id, Name = x.Title, Title = x.Name };
            return new JsonSerializer().Serialize(jsonCol);
        }

        [WebMethod]
        public static string UpdateCheckAccountSafe(string strIds, string userName)
        {
            try
            {
                string[] ids = strIds.Split(",");

                foreach (string id in ids)
                {
                    CreditcardFraudDatum cfd = op.CreditcardFraudDataGetById(id);
                    cfd.Type = (int)CreditcardFraudDataType.SafeAccount;
                    cfd.ModifyId = userName;
                    cfd.ModifyTime = DateTime.Now;
                    op.CreditcardFraudDataSet(cfd);
                }
               
                return "true";
            }
            catch
            {
                return "false";
            }
        }

        [WebMethod]
        public static string UpdateCheckAccountError(string strIds, string userName)
        {
            try
            {
                string[] ids = strIds.Split(",");

                foreach (string id in ids)
                {
                    CreditcardFraudDatum cfd = op.CreditcardFraudDataGetById(id);
                    cfd.Type = (int)CreditcardFraudDataType.Account;
                    cfd.ModifyId = userName;
                    cfd.ModifyTime = DateTime.Now;
                    op.CreditcardFraudDataSet(cfd);
                }

                return "true";
            }
            catch
            {
                return "false";
            }
        }

        [WebMethod]
        public static string UpdateBlacklist(string strId, string isLock)
        {
            bool isLocked = false;
            if (bool.TryParse(isLock, out isLocked))
            {
                AntiCreditcardFraud thisPage = new AntiCreditcardFraud();
                PaymentFacade.SetCreditcardOrderLocked(strId, thisPage.UserName, isLocked);
                return "true";
            }
            else
            {
                return "false";
            }
        }

        private static string GetJson(List<Creditcard> creditcardOrderList)
        {
            var jsonCol = from x in creditcardOrderList
                          select new
                          {
                              Id = x.Id,
                              IsReference = x.IsReference,
                              OrderGuid = x.OrderGuid,
                              CreditcardInfo = x.CreditcardInfo,
                              ExpDate = x.ExpDate,
                              OrderTime = x.OrderTime,
                              MemberName = x.MemberName,
                              MemberMobile = x.MemberMobile,
                              DeliveryAddress=x.DeliveryAddress,
                              Amount = x.Amount,
                              ItemName = x.ItemName,
                              Result = x.Result,
                              IP = x.IP,
                              IsLocked = x.IsLocked,
                              LastID=x.LastID,
                              LastTime=x.LastTime,
                              CreditCardInfo = x.CreditcardInfo
                          };
            return new JsonSerializer().Serialize(jsonCol);
        }

        private static string GetCreditcardFraudDataJson(CreditcardFraudDatumCollection data)
        {
            Security sec = new Security(SymmetricCryptoServiceProvider.AES);
            var jsonCol = from x in data
                          select new
                          {
                              Id = x.Id,
                              UserId = x.UserId != null ? x.UserId.Value.ToString() : string.Empty,
                              Address = x.Address,
                              Ip = x.Ip,
                              CardNumber = string.IsNullOrEmpty(x.CardNumber) ? string.Empty : sec.Decrypt(x.CardNumber),
                              Type = Helper.GetEnumDescription(((CreditcardFraudDataType)x.Type)),
                              CreateId = x.CreateId,
                              CreateTime = x.CreateTime.ToString("yyyy/MM/dd tt hh:mm:ss"),
                              ModifyTime = x.ModifyTime != null ? x.ModifyTime.Value.ToString("yyyy/MM/dd tt hh:mm:ss") : string.Empty,
                              ModifyId = x.ModifyId,
                              RelateTime = x.RelateTime != null ? x.RelateTime.Value.ToString("yyyy/MM/dd tt hh:mm:ss") : string.Empty,
                          };
            return new JsonSerializer().Serialize(jsonCol);
        }
        private static string GetCheckAccountJson(CreditcardFraudDatumCollection data)
        {
            var jsonCol = from x in data
                          select new
                          {
                              Id = x.Id,
                              UserId = x.UserId != null ? x.UserId.Value.ToString() : string.Empty,
                              CreateId = x.CreateId,
                              CreateTime = x.CreateTime.ToString("yyyy/MM/dd tt hh:mm:ss")
                          };
            return new JsonSerializer().Serialize(jsonCol);
        }

        

        private static void Export(string fileName, string jsonString)
        {
            Creditcard[] creditcardOrderList = ProviderFactory.Instance().GetSerializer().Deserialize<Creditcard[]>(jsonString);
            if (creditcardOrderList.Any())
            {
                Workbook workbook = new HSSFWorkbook();

                // 新增試算表。
                Sheet sheet = workbook.CreateSheet("訂單信用卡清冊");
                Row HeaderRow = sheet.CreateRow(0);

                HeaderRow.CreateCell(0).SetCellValue("序號");
                HeaderRow.CreateCell(1).SetCellValue("訂單編號");
                HeaderRow.CreateCell(2).SetCellValue("卡號");
                HeaderRow.CreateCell(3).SetCellValue("有效期限");
                HeaderRow.CreateCell(4).SetCellValue("訂單時間");
                HeaderRow.CreateCell(5).SetCellValue("姓名");
                HeaderRow.CreateCell(6).SetCellValue("手機");
                HeaderRow.CreateCell(7).SetCellValue("地址");
                HeaderRow.CreateCell(8).SetCellValue("金額");
                HeaderRow.CreateCell(9).SetCellValue("檔名");
                HeaderRow.CreateCell(10).SetCellValue("狀態");
                HeaderRow.CreateCell(11).SetCellValue("IP");
                HeaderRow.CreateCell(12).SetCellValue("最後修改帳號");
                HeaderRow.CreateCell(13).SetCellValue("最後修改時間");
                Cell c = null;
                int rowCount = 0;
                int rowIndex = 1;
                foreach (Creditcard creditcard in creditcardOrderList)
                {
                    Row dataRow = sheet.CreateRow(rowIndex);

                    c = dataRow.CreateCell(0);
                    c.SetCellValue(creditcard.Id);
                    c = dataRow.CreateCell(1);
                    c.SetCellValue(creditcard.OrderGuid);
                    c = dataRow.CreateCell(2);
                    c.SetCellValue(creditcard.CreditcardInfo);
                    c = dataRow.CreateCell(3);
                    c.SetCellValue(creditcard.ExpDate);
                    c = dataRow.CreateCell(4);
                    c.SetCellValue(creditcard.OrderTime);
                    c = dataRow.CreateCell(5);
                    c.SetCellValue(creditcard.MemberName);
                    c = dataRow.CreateCell(6);
                    c.SetCellValue(creditcard.MemberMobile);
                    c = dataRow.CreateCell(7);
                    c.SetCellValue(creditcard.DeliveryAddress);
                    c = dataRow.CreateCell(8);
                    c.SetCellValue(creditcard.Amount);
                    c = dataRow.CreateCell(9);
                    c.SetCellValue(creditcard.ItemName);
                    c = dataRow.CreateCell(10);
                    c.SetCellValue(creditcard.Result);
                    c = dataRow.CreateCell(11);
                    c.SetCellValue(creditcard.IP);
                    c = dataRow.CreateCell(12);
                    c.SetCellValue(creditcard.LastID);
                    c = dataRow.CreateCell(13);
                    c.SetCellValue(creditcard.LastTime);
                    rowIndex++;
                    rowCount++;
                }
                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode(fileName)));
                workbook.Write(HttpContext.Current.Response.OutputStream);
            }
        }

        private static void CreditcardFraudDataExport(string fileName, string jsonString)
        {
            CreditcardFraudData[] creditcardFraudDataList = ProviderFactory.Instance().GetDefaultSerializer().Deserialize<CreditcardFraudData[]>(jsonString);
            if (creditcardFraudDataList.Any())
            {
                Workbook workbook = new HSSFWorkbook();

                // 新增試算表。
                Sheet sheet = workbook.CreateSheet("風險資料清冊");
                Row HeaderRow = sheet.CreateRow(0);

                HeaderRow.CreateCell(0).SetCellValue("序號");
                HeaderRow.CreateCell(1).SetCellValue("UserId");
                HeaderRow.CreateCell(2).SetCellValue("IP");
                HeaderRow.CreateCell(3).SetCellValue("地址");
                HeaderRow.CreateCell(4).SetCellValue("卡號");
                HeaderRow.CreateCell(5).SetCellValue("資料類型");
                HeaderRow.CreateCell(6).SetCellValue("匯入帳號");
                HeaderRow.CreateCell(7).SetCellValue("匯入時間");
                HeaderRow.CreateCell(8).SetCellValue("修改帳號");
                HeaderRow.CreateCell(9).SetCellValue("修改時間");
                HeaderRow.CreateCell(10).SetCellValue("風險建立時間");
                Cell c = null;
                int rowCount = 0;
                int rowIndex = 1;
                foreach (CreditcardFraudData creditcard in creditcardFraudDataList)
                {
                    Row dataRow = sheet.CreateRow(rowIndex);

                    c = dataRow.CreateCell(0);
                    c.SetCellValue(creditcard.Id);
                    c = dataRow.CreateCell(1);
                    c.SetCellValue(creditcard.UserId.ToString());
                    c = dataRow.CreateCell(2);
                    c.SetCellValue(creditcard.Ip);
                    c = dataRow.CreateCell(3);
                    c.SetCellValue(creditcard.Address);
                    c = dataRow.CreateCell(4);
                    c.SetCellValue(creditcard.CardNumber);
                    c = dataRow.CreateCell(5);
                    c.SetCellValue(creditcard.Type);
                    c = dataRow.CreateCell(6);
                    c.SetCellValue(creditcard.CreateId);
                    c = dataRow.CreateCell(7);
                    c.SetCellValue(creditcard.CreateTime);
                    c = dataRow.CreateCell(8);
                    c.SetCellValue(creditcard.ModifyId);
                    c = dataRow.CreateCell(9);
                    c.SetCellValue(creditcard.ModifyTime);
                    c = dataRow.CreateCell(10);
                    c.SetCellValue(creditcard.RelateTime);
                    rowIndex++;
                    rowCount++;
                }
                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode(fileName)));
                workbook.Write(HttpContext.Current.Response.OutputStream);
            }
        }

        private static void CheckAccountExport(string fileName, string jsonString)
        {
            CheckAccount[] CheckAccountList = ProviderFactory.Instance().GetDefaultSerializer().Deserialize<CheckAccount[]>(jsonString);
            if (CheckAccountList.Any())
            {
                Workbook workbook = new HSSFWorkbook();

                // 新增試算表。
                Sheet sheet = workbook.CreateSheet("需確認風險帳號查詢清冊");
                Row HeaderRow = sheet.CreateRow(0);

                HeaderRow.CreateCell(0).SetCellValue("序號");
                HeaderRow.CreateCell(1).SetCellValue("UserId");
                HeaderRow.CreateCell(2).SetCellValue("匯入帳號");
                HeaderRow.CreateCell(3).SetCellValue("匯入時間");
                Cell c = null;
                int rowCount = 0;
                int rowIndex = 1;
                foreach (CheckAccount CheckAccount in CheckAccountList)
                {
                    Row dataRow = sheet.CreateRow(rowIndex);

                    c = dataRow.CreateCell(0);
                    c.SetCellValue(CheckAccount.Id);
                    c = dataRow.CreateCell(1);
                    c.SetCellValue(CheckAccount.UserId.ToString());
                    c = dataRow.CreateCell(2);
                    c.SetCellValue(CheckAccount.CreateId);
                    c = dataRow.CreateCell(3);
                    c.SetCellValue(CheckAccount.CreateTime);
                    rowIndex++;
                    rowCount++;
                }
                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode(fileName)));
                workbook.Write(HttpContext.Current.Response.OutputStream);
            }
        }
        protected void btnExcel_Click(object sender, EventArgs e)
        {
            string json = txtHideJson.Value;
            Export("訂單信用卡清冊.xls", json);
        }

        protected void btnCreditcardFraudDataExcel_Click(object sender, EventArgs e)
        {
            string json = txtCreditcardFraudDataHideJson.Value;
            CreditcardFraudDataExport("風險資料清冊.xls", json);
        }

        protected void btnCheckAccountExcel_Click(object sender, EventArgs e)
        {
            string json = txtCheckAccountHideJson.Value;
            CheckAccountExport("需確認風險帳號資料清冊.xls", json);
        }



        protected void btnAddBlackList_Click(object sender, EventArgs e)
        {
            AntiCreditcardFraud thisPage = new AntiCreditcardFraud();
            //若不是測試卡號才給予加入黑名單
            if (PaymentFacade.CompareDefaultTestCreditcard(txtCreditcardID.Text.Trim()))
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "alert", "alert('測試卡號不可加入黑名單!')", true);
                return;
            }
            PaymentFacade.AddCreditcardOrder(txtCreditcardID.Text.Trim(),ddlAddResult.SelectedValue, thisPage.UserName);
            forceShowGrid.Value = "1";
            
        }

        [WebMethod]
        public static string CompareTestCreditcard(string strCreditcard)
        {
            try
            {
                //若不是測試卡號才給予加入黑名單
                if (PaymentFacade.CompareDefaultTestCreditcard(strCreditcard))
                {
                    return "true";
                }
                else 
                {
                    return "false";
                }
            }
            catch
            {
                return "false";
            }
        }

    }
}
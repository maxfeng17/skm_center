﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.Finance
{
    /// <summary>
    /// Summary description for AtmDataCenter
    /// </summary>
    public class AtmDataCenter : RolePage, IHttpHandler
    {

        new public void ProcessRequest(HttpContext context)
        {
            int mode;
            context.Response.ContentType = "application/json";// "text/plain";
            context.Response.ContentEncoding = Encoding.UTF8;
            int.TryParse(context.Request["m"],out mode);
            if (mode==2)
            {
                CtAtmCollection list = PaymentFacade.CtAtmGetList(DateTime.Now.AddDays(-30));
                context.Response.Write(new JsonSerializer().Serialize(ToAtm(list)));
            }
            else
            {
                CtAtmRefundCollection list = PaymentFacade.CtAtmGetRefundList(DateTime.Now.AddDays(-30));
                context.Response.Write(new JsonSerializer().Serialize(ToAtmRefund(list)));
            }

        }

        new public bool IsReusable
        {
            get
            {
                return false;
            }
        }



        public ModelAtmRefund[] ToAtmRefund(CtAtmRefundCollection list)
        {
            int i = 0;
            ModelAtmRefund[] r= new ModelAtmRefund[list.Count];
            foreach (var v in list)
            {
                r[i]= new ModelAtmRefund { 
                    Si=v.Si,
                    UserName = MemberFacade.GetUserName(v.UserId),
                    OrderGuid = v.OrderGuid,
                    PaymentId = v.PaymentId,
                    Amount = v.Amount,
                    AccountName = v.AccountName,
                    ID = v.Id,
                    BankName = v.BankName,
                    BankCode = v.BankCode,
                    BranchName = v.BranchName,
                    AccountNumber = v.AccountNumber,
                    Phone = v.Phone,
                    Status = v.Status,
                    FailReason = v.FailReason,
                    CouponList = v.CouponList,
                    Flag = v.Flag,
                    CreateTime = v.CreateTime.ToString("yyyy/MM/dd HH:mm:ss"),
                    RefundProcessTime = v.RefundProcessTime ?? DateTime.Parse("1900-01-01"),
                    RefundResponseTime = v.RefundResponseTime ?? DateTime.Parse("1900-01-01"),
                    RefundSendTime = v.RefundSendTime ?? DateTime.Parse("1900-01-01"),
                    TargetTime = v.TargetDate.ToString() ?? null,
                    ProcessTime = v.ProcessDate ?? DateTime.Parse("1900-01-01")
                };
                i++;
            }

            return r;
        }

        public ModelAtm[] ToAtm(CtAtmCollection list)
        {
            int i = 0;
            ModelAtm[] r = new ModelAtm[list.Count];
            foreach (var v in list)
            {
                r[i] = new ModelAtm
                {
                    Si = v.Si,
                    UserEmail = MemberFacade.GetUserEmail(v.UserId),
                    OrderGuid = v.OrderGuid,
                    PaymentId = v.PaymentId,
                    VirtualAccount = v.VirtualAccount,
                    BusinessGuid = v.BusinessGuid,
                    OrderId = v.OrderId,
                    PayTime = v.PayTime.ToString(),
                    Senq = v.Senq,
                    Amount = v.Amount,
                    Status = AtmStatusConvert((AtmStatus) v.Status),
                    CreateTime = v.CreateTime.ToString("yyyy/MM/dd HH:mm:ss"),
                };
                i++;
            }
            return r;
        }

        public string AtmStatusConvert(AtmStatus atm)
        {
            string result = "";
            switch(atm)
            {
                case AtmStatus.Initial:
                    result = "未付款";
                    break;
                case AtmStatus.Paid:
                    result = "已付款";
                    break;
                case AtmStatus.Refund:
                    result = "已退貨";
                    break;
                case AtmStatus.Expired:
                    result = "已逾期";
                    break;
                case AtmStatus.RefundRequest:
                    result = "退貨提出";
                    break;
                case AtmStatus.RefundSend:
                    result = "退貨中";
                    break;
            }
            return result;
        }
    }


    #region Model
    public class ModelAtmRefund
    {
        public int Si;
        public string UserName;
        public Guid OrderGuid;
        public string PaymentId;
        public int Amount;
        public string AccountName;
        public string ID;
        public string BankName;
        public string BranchName;
        public string BankCode;
        public string AccountNumber;
        public string Phone;
        //public DateTime CreateTime;
        public string CreateTime;
        public DateTime RefundProcessTime;
        public DateTime RefundSendTime;
        public DateTime RefundResponseTime;
        //public DateTime TargetTime;
        public string TargetTime;
        public DateTime ProcessTime;
        public int Status;
        public string FailReason;
        public string CouponList;
        public int Flag;
    }

    public class ModelAtm
    {
        public int Si;
        public string PaymentId;
        public string UserEmail;
        public int Amount;
        public string VirtualAccount;
        public string CreateTime;
        public string PayTime;
        public Guid BusinessGuid;
        public string OrderId;
        public Guid OrderGuid;
        public string Senq;
        public string Status;
    }
    #endregion

}
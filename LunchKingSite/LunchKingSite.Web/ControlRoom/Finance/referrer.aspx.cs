﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using Newtonsoft.Json;
using LunchKingSite.Core;
using NPOI.HSSF.UserModel;
using System.Web.UI;

namespace LunchKingSite.Web.ControlRoom.Finance
{
    public partial class referrer : RolePage, IReferrerManageView
    {
        public event EventHandler<DataEventArgs<KeyValuePair<string, string>>> AddReferrer;        
        public event EventHandler<DataEventArgs<ReferralCampaign>> AddCampaign;
        public event EventHandler<DataEventArgs<ReferralCampaign>> EditCampaign;
        public event EventHandler<DataEventArgs<Tuple<Guid,string>>> ExportToExcel;
        public event EventHandler<DataEventArgs<Tuple<Guid, string>>> QueryCampaignRecord;

        #region props
        protected MemberReferral _referral;
        protected MemberReferral Referral
        {
            get
            {
                if (_referral != null)
                {
                    return _referral;
                }
                return new MemberReferral();
            }
        }
        private ReferrerManagePresenter _presenter;
        public ReferrerManagePresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public Guid ReferrerId
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Request["id"]))
                    return Guid.Empty;
                return new Guid(Request["id"]);
            }
        }

        public string UserName
        {
            get { return User.Identity.Name; }
        }

        public string CampaignCode
        {
            get
            {
                //if (string.IsNullOrWhiteSpace(Request["cid"]))
                //    return Guid.Empty;
                //return new Guid(Request["cid"]);
                return Request["cid"];
            }
        }

        public Guid CampaignId {
            get
            {
                if (string.IsNullOrWhiteSpace(Request["cid"]))
                    return Guid.Empty;
                return new Guid(Request["cid"]);
            }
        }

        public string ActionType
        {
            get
            {
                return aTypeList.SelectedValue;
            }
        }

        public string QueryStartTime
        {
            get
            {
                return tbOS.Text;
            }
        }

        public string QueryEndTime
        {
            get
            {
                return tbOE.Text;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                _presenter.OnViewInitialized();
            _presenter.OnViewLoaded();
        }

        public void SetReferrerList(List<MemberReferral> referrers)
        {
            phReferences.Visible = true;
            repReferences.DataSource = referrers;
            repReferences.DataBind();
        }


        public void SetReferrerDetail(MemberReferral data, List<ReferralCampaign> campaigns)
        {
            pd.Visible = true;
            _referral = data;
            repCampaigns.DataSource = campaigns;
            repCampaigns.DataBind();
        }

        public void SetCampaignDetail(MemberReferral referrer,ReferralCampaign campaign, List<ReferralAction> actions, 
            bool exportToExcel = false)
        {
            // exportToExcel == false
            //
            //
            
            if (exportToExcel == false)
            {
                pc.Visible = true;
                dCD.DataSource = actions;
                dCD.DataBind();
            }
            else
            {

                MemoryStream ms = new MemoryStream();
                HSSFWorkbook workbook = new HSSFWorkbook();
   
                NPOI.SS.UserModel.Sheet sheet = workbook.CreateSheet(campaign.Code);
                var headerRow = sheet.CreateRow(0);
                headerRow.CreateCell(0).SetCellValue("Type");
                headerRow.CreateCell(1).SetCellValue("ReferenceId");
                headerRow.CreateCell(2).SetCellValue("時間");
                headerRow.CreateCell(3).SetCellValue("館別");
                headerRow.CreateCell(4).SetCellValue("商家城市");
                headerRow.CreateCell(5).SetCellValue("名稱");
                headerRow.CreateCell(6).SetCellValue("單價");
                headerRow.CreateCell(7).SetCellValue("總價");
                headerRow.CreateCell(8).SetCellValue("折扣後金額");
                headerRow.CreateCell(9).SetCellValue("相關資訊");
                headerRow.CreateCell(10).SetCellValue("業績歸屬館別");
                headerRow.CreateCell(11).SetCellValue("上檔分類");
                for (int i=0;i<actions.Count;i++)
                {
                    var item = actions[i];
                    var daraRow = sheet.CreateRow(i + 1);
                    daraRow.CreateCell(0).SetCellValue(item.ActionType);
                    daraRow.CreateCell(1).SetCellValue(item.ReferenceId);
                    daraRow.CreateCell(2).SetCellValue(item.CreateTime.ToString("yyyy-MM-dd"));
                    daraRow.CreateCell(3).SetCellValue(item.Department);
                    daraRow.CreateCell(4).SetCellValue(item.City);
                    daraRow.CreateCell(5).SetCellValue(item.ItemName);
                    decimal price;
                    if (decimal.TryParse(item.Price, out price))
                    {
                        item.Price = ((int)price).ToString();
                    }
                    decimal subTotal;
                    if (decimal.TryParse(item.SubTotal, out subTotal))
                    {
                        item.SubTotal = ((int)subTotal).ToString();
                    }
                    decimal total;
                    if (decimal.TryParse(item.Total, out total))
                    {
                        item.Total = ((int)total).ToString();
                    }
                    daraRow.CreateCell(6).SetCellValue(item.Price);
                    daraRow.CreateCell(7).SetCellValue(item.SubTotal);
                    daraRow.CreateCell(8).SetCellValue(item.Total);
                    daraRow.CreateCell(9).SetCellValue(item.Remark);                    
                    daraRow.CreateCell(10).SetCellValue(AccBusinessGroupManager.GetAccBusinessGroupName(item.AccBusinessGroupId));
                    daraRow.CreateCell(11).SetCellValue(SystemCodeManager.GetDealTypeName(item.DealType));
                }
                for(int i = 0; i < 12; i++)
                {
                    sheet.AutoSizeColumn(i);
                    sheet.SetColumnWidth(i, sheet.GetColumnWidth(i) + 10);
                }
                
                Response.Clear();
                Response.ContentType = "application/ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}_{1}_{2}.xls",
                    referrer.Name, campaign.Code, DateTime.Now.ToJavaScriptMilliseconds()));
                //referrer.Name, campaign.Code, DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss")));
                workbook.Write(ms);
                Response.BinaryWrite(ms.ToArray());

                ms.Close();
                ms.Dispose();

            }
        }

        public void ShowReferrerListing()
        {
            Response.Redirect("referrer.aspx");
        }

        public void ShowReferrerDetail(Guid referrerId)
        {
            Response.Redirect("referrer.aspx?id=" + referrerId.ToString());
        }

        public void ShowCampaign(Guid referrerId, string campaignCode)
        {
            Response.Redirect("referrer.aspx?id=" + referrerId.ToString() + "&cid=" + campaignCode);
        }

        protected void bR_Click(object sender, EventArgs e)
        {
            if (AddReferrer != null)
            {
                AddReferrer(this, new DataEventArgs<KeyValuePair<string, string>>(new KeyValuePair<string, string>(tAC.Text.Trim(), tAN.Text.Trim())));
            }
        }

        protected void bC_Click(object sender, EventArgs e)
        {
            if (AddCampaign != null)
            {
                var c = new ReferralCampaign() { Code = tACC.Text.Trim(), Name = tACN.Text.Trim(), SessionDuration = int.Parse(tACT.Text.Trim()), OneShotAction = cbS.Checked, IsExternal = cbExternal.Checked };
                AddCampaign(this, new DataEventArgs<ReferralCampaign>(c));
            }
        }

        protected void btnQueryRecord_Click(object sender, EventArgs e)
        {
            var args = new Tuple<Guid, string>(this.ReferrerId, CampaignCode);
            QueryCampaignRecord(sender, new DataEventArgs<Tuple<Guid, string>>(args));
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            var args = new Tuple<Guid, string>(this.ReferrerId, CampaignCode);
            ExportToExcel(sender, new DataEventArgs<Tuple<Guid, string>>(args));
        }

        protected void dCD_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var data = e.Item.DataItem as ReferralAction;
                (e.Item.Cells[0].FindControl("lat") as Literal).Text = data.ActionType.ToString();
                (e.Item.Cells[2].FindControl("lct") as Literal).Text = data.CreateTime.ToString();
                var lAccBus = (Literal)e.Item.Cells[10].FindControl("lAccBus");
                var lDealType = (Literal)e.Item.Cells[11].FindControl("lDealType");
                lAccBus.Text = AccBusinessGroupManager.GetAccBusinessGroupName(data.AccBusinessGroupId);
                lDealType.Text = SystemCodeManager.GetDealTypeName(data.DealType);
            }
        }

        [WebMethod]
        public static string MarkDeleted(string delData, string editData)
        {
            if (string.IsNullOrEmpty(delData) && string.IsNullOrEmpty(editData))
            {
                return "no data";
            }
            if (string.IsNullOrEmpty(delData) == false)
            {
                List<ReferrerDeleteItem> deleteItems = JsonConvert.DeserializeObject<List<ReferrerDeleteItem>>(delData);
                MarketingFacade.MarkMemberReferencesDeleted(deleteItems);
            }
            if (string.IsNullOrEmpty(editData) == false)
            {
                List<ReferrerUpdateItem> updateModels = JsonConvert.DeserializeObject<List<ReferrerUpdateItem>>(editData);
                MarketingFacade.UpdateMemberReferences(updateModels);
            }
            return string.Empty;
        }

        [WebMethod]
        public static string UpdateCampaign(string delData, string editData)
        {
            if (string.IsNullOrEmpty(delData) && string.IsNullOrEmpty(editData))
            {
                return "no data";
            }
            if (string.IsNullOrEmpty(delData) == false)
            {
                List<CampaignDeleteItem> deleteItems = JsonConvert.DeserializeObject<List<CampaignDeleteItem>>(delData);
                MarketingFacade.MarkReferralCampaignsDeleted(deleteItems);
            }
            if (string.IsNullOrEmpty(editData) == false)
            {
                List<CampaignUpdateItem> updateModels = JsonConvert.DeserializeObject<List<CampaignUpdateItem>>(editData);
                MarketingFacade.UpdateCampaigns(updateModels, HttpContext.Current.User.Identity.Name);
            }

            return string.Empty;
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="AtmList.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Finance.AtmList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
<link type="text/css" href="../../Tools/js/css/jquery.tooltip.css" rel="stylesheet" />	
<link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8.13.custom.css" rel="stylesheet" />
<link href="../../Tools/js/css/jqGrid/ui.jqgrid.css" rel="stylesheet" type="text/css" />
<style type="text/css" >
.ui-jqgrid-sortable{
font-size:11px;
}
.ui-pg-input{
height:20px;
}
</style>
<script type="text/javascript" src="//www.google.com/jsapi"></script>  
<script type="text/javascript">
    google.load("jquery", "1.7.0");
    google.load("jqueryui", "1.8.16"); 
</script>

<script src="../../Tools/js/grid.locale-en.js" type="text/javascript"></script>
<script src="../../Tools/js/jquery.jqGrid.min.js" type="text/javascript"></script>
    
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        jQuery("#list2").jqGrid({
            datatype: "local",
            colNames: ['訂單時間', '使用者', '訂單編號', '檔次Bid', '金額', '付款時間', '虛擬帳戶','狀態', '傳輸序號'],
            colModel: [
        { name: 'CreateTime', index: 'CreateTime', width: 210 },
   		{ name: 'UserEmail', index: 'UserEmail' },
        { name: 'OrderId', index: 'OrderId', width: 100 },
        { name: 'BusinessGuid', index: 'BusinessGuid', width: 100 },
        { name: 'Amount', index: 'Amount', width: 80, align: "right" },
        { name: 'PayTime', index: 'PayTime', width: 210 },
        { name: 'VirtualAccount', index: 'VirtualAccount', align: "right" },
        { name: 'Status', index: 'Status' },
        { name: 'Senq', index: 'Senq' }
   	],
            rowNum: 10,
            rowList: [10, 25, 50],
            pager: '#pager2',
            sortname: 'CreateTime',
            height: "auto",
            autowidth: true,
            viewrecords: true,
            sortorder: "desc",
            caption: "退款資料",
            loadonce: true,
            rownumbers: true,
            emptyrecords: "查無資料"
        });
        jQuery("#list2").jqGrid('navGrid', '#pager2', { edit: false, add: false, del: false });
        $.getJSON('atmdatacenter.ashx?m=2',
        function (data) {
            for (var i = 0; i <= data.length + 1; i++) {
                jQuery("#list2").jqGrid('addRowData', i + 1, data[i]);
            }
            $('#list2').trigger("reloadGrid");
        });
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
僅列出30天內資料

<table id="list2" style="font-size:11px;width:1020px"></table>
<div id="pager2"></div>
</asp:Content>

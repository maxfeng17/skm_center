﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace LunchKingSite.Web.ControlRoom.Finance
{

    public partial class EntrustSellDailyReport : RolePage, IEntrustSellDailyReportView
    {
        private EntrustSellDailyReportPresenter _presenter;
        public EntrustSellDailyReportPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public DateTime? FilterStartTime
        {
            get
            {
                DateTime dt;
                if (DateTime.TryParse(tbSTime.Text, out dt))
                {
                    return dt;
                }
                return null;
            }
            set
            {
                tbSTime.Text = value == null ? "" : value.Value.ToString("'yyyy/MM/dd");
            }
        }

        public DateTime? FilterEndTime
        {
            get
            {
                DateTime dt;
                if (DateTime.TryParse(tbETime.Text, out dt))
                {
                    return dt;
                }
                return null;
            }
            set
            {
                tbETime.Text = value == null ? "" : value.Value.ToString("'yyyy/MM/dd");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                Presenter.OnViewInitialized();
            }
            Presenter.OnViewLoaded();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.SearchClicked != null)
            {
                this.SearchClicked(this, e);
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (ExportToExcel != null)
            {
                ExportToExcel(this, e);
            }
        }

        #region events
        public event EventHandler SearchClicked;
        public event EventHandler ExportToExcel;
        #endregion


        public void SetContent(List<ViewEntrustSellDailyPayment> list)
        {
            repList.DataSource = list;
            repList.DataBind();
        }


        public void Export(List<ViewEntrustSellDailyPayment> list)
        {
            Workbook workbook = new HSSFWorkbook();

            Sheet sheet = workbook.CreateSheet("收轉付每日請款報表");
            Row cols = sheet.CreateRow(0);
            cols.CreateCell(0).SetCellValue("請款檔組檔日");
            cols.CreateCell(1).SetCellValue("訂單編號");
            cols.CreateCell(2).SetCellValue("請/退款金額");
            cols.CreateCell(3).SetCellValue("動作別");
            cols.CreateCell(4).SetCellValue("購買人");
            cols.CreateCell(5).SetCellValue("商品名稱");

            int rowIdx = 1;
            foreach (ViewEntrustSellDailyPayment item in list)
            {
                Row row = sheet.CreateRow(rowIdx);
                row.CreateCell(0).SetCellValue(item.TransTime.Value.ToString("yyyy/MM/dd"));
                row.CreateCell(1).SetCellValue(item.OrderId);
                row.CreateCell(2).SetCellType(CellType.NUMERIC);
                row.GetCell(2).SetCellValue(GetAmount(item.TransType, item.Amount));
                row.CreateCell(3).SetCellValue(GetTransType(item.TransType));
                row.CreateCell(4).SetCellValue(item.MemberName);
                row.CreateCell(5).SetCellValue(item.CouponUsage);
                rowIdx++;
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            sheet.AutoSizeColumn(5);
            Response.AddHeader("Content-Disposition", string.Format("attachment; filename=收轉付每日請款報表_{0}.xls", DateTime.Now.ToString("yyyy-MM-dd")));
            workbook.Write(Response.OutputStream);  
        }

        protected string GetAmount(object oTansType, object oAmount)
        {
            int tansType = (int) oTansType;
            decimal amount = (decimal)oAmount;

            if (tansType == 2)
            {
                return string.Format("-{0:#}", amount);
            }
            return string.Format("{0:#}", amount);
        }

        protected string GetTransType(object oTansType)
        {
            int tansType = (int)oTansType;
            if (tansType == 0 )
            {
                return "購買";
            } else if (tansType == 2)
            {
                return "退貨";
            }
            return "未定義";
        }
    }
}
﻿using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.Finance
{
    public partial class CtAtmCompared : RolePage, ICtAtmComparedView
    {
        #region property
        private CtAtmComparedPresenter _presenter;
        public CtAtmComparedPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public string AtmDate
        {
            get
            {
                return txtDate.Text;
            }
        }
        #endregion property

        #region event
        public event EventHandler<DataEventArgs<StreamReader>> ImportClicked;
        public event EventHandler Search;
        #endregion event

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Presenter.OnViewInitialized();
                txtDate.Text = DateTime.Now.ToString("yyyy/MM/dd");
            }

            Presenter.OnViewLoaded();
        }

        public void SetContentList(List<string> cols)
        {
            rptCtAtm.DataSource = cols;
            rptCtAtm.DataBind();
        }

        /// <summary>
        /// 匯入
        /// </summary>
        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (fileImport.PostedFile.FileName.Trim() != string.Empty)
            {
                StreamReader sr = new StreamReader(this.fileImport.PostedFile.InputStream, System.Text.Encoding.ASCII);
                this.ImportClicked(this, new DataEventArgs<StreamReader>(sr));
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.Search != null)
            {
                this.Search(this, e);
            }
        }
    }
}
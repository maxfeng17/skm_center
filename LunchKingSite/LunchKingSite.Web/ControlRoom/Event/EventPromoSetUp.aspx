﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="EventPromoSetUp.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Event.EventPromoSetUp" %>

<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link type="text/css" href="<%=ResolveUrl("~/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css") %>" rel="stylesheet" />
    <script src="../../Tools/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/adapters/jquery.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.numeric.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.tablesorter-2.24.5.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .bgOrange {
            background-color: #FFC690;
            text-align: center;
        }

        .bgBlue {
            background-color: #B3D5F0;
        }

        .title {
            text-align: center;
            background-color: #688E45;
            font-weight: bolder;
            color: White;
        }

        .header {
            text-align: center;
            font-size: large;
            font-weight: bolder;
            background-color: #F0AF13;
            color: Blue;
        }
        .tablesorter-header {
            background-image: url("data:image/gif;base64,R0lGODlhFQAJAIAAACMtMP///yH5BAEAAAEALAAAAAAVAAkAAAIXjI+AywnaYnhUMoqt3gZXPmVg94yJVQAAOw==");
            background-position: right center;
            background-repeat: no-repeat;
            cursor: pointer;
            padding: 0px 20px 0px 4px;
            white-space: normal;
        }
        .ui-dialog-buttonset {
            text-align: center;
        }
            .ui-dialog .ui-dialog-buttonpane button {
                float: none;
                padding: 0;
            }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            AutoHideOrDisplayGrossMargin();
            DelegateRearrange();

            $('input.number').numeric();
            $("input[id*=rdlItemType][value=1]").attr("disabled", true);
            if ($("#[id*=ddlTemplateType] option:selected").val() == "7") {
                $(".voteArea").show();
            } else {
                $(".voteArea").hide();
            }
            if ($("#[id*=ddlTemplateType] option:selected").val() == "1") {
                $("#[id*=btn_Save]").hide();
            } else {
                $("#[id*=btn_Save]").show();
            }

            $('#[id*=ddlTemplateType]').change(function () {
                if ($("#[id*=ddlTemplateType] option:selected").val() == "7") {
                    $(".voteArea").show();
                } else {
                    $(".voteArea").hide();
                }
                if ($("#[id*=ddlTemplateType] option:selected").val() == "1") {
                    $("#[id*=btn_Save]").hide();
                } else {
                    $("#[id*=btn_Save]").show();
                }
            });

            var itemType = $("input[id*=rdlItemType]:checked").next().text();
            if (itemType == "圖片投票") {
                $(".picVoteArea").show();
                $("#linkVourcherArea").hide();
            } else {
                $(".picVoteArea").hide();
                if (itemType == "團購券" || itemType == "品生活") {
                    $("#linkVourcherArea").show();
                } else {
                    $("#linkVourcherArea").hide();
                }
            }

            $("input[id*=rdlItemType]").bind('click', function () {
                var cItemType = $(this).next().text();
                if (cItemType == "圖片投票") {
                    $(".picVoteArea").show();
                    $("#linkVourcherArea").hide();
                } else {
                    $(".picVoteArea").hide();
                    if (cItemType == "團購券" || cItemType == "品生活") {
                        $("#linkVourcherArea").show();
                    } else {
                        $("#linkVourcherArea").hide();
                    }
                }
            });
            
            $('#<%= ddlEventType.ClientID %>').change(function () {
                if ($("#<%= ddlEventType.ClientID %>").val() == '<%= (int)EventPromoEventType.Curation %>' || $("#<%= ddlEventType.ClientID %>").val() == '<%= (int)EventPromoEventType.CurationTwo %>') {
                    $('#<%= chbBannerLocation.ClientID %> [value=<%= (int) EventBannerType.ChannelUp %>]').removeAttr("disabled");
                    $('#<%= chbBannerLocation.ClientID %> [value=<%= (int) EventBannerType.ChannelDown %>]').removeAttr("disabled");
                } else {
                    $('#<%= chbBannerLocation.ClientID %> [value=<%= (int) EventBannerType.ChannelUp %>]').attr("disabled", "disabled").attr("checked", false);;
                    $('#<%= chbBannerLocation.ClientID %> [value=<%= (int) EventBannerType.ChannelDown %>]').attr("disabled", "disabled").attr("checked", false);;
                }
            });
        });

        function CheckDeleteEventPromoItem()
        {
            return confirm('確定要刪除？');
        }
        
        var fixHelperModified = function(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function(index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        },
        updateIndex = function (e, ui) {
            $('td.itemSeqTd .itemSeqDiv', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);//前端顯示用
                $(this).siblings("input[id$='HidTempSeq']").val(i + 1);//後端儲存用
            });
        };


        //啟動手動排序
        function EnableManuallySort()
        {
            //<tr>手動排序
            $(".manualSort tbody").sortable({
                helper: fixHelperModified,
                stop: updateIndex
            }).disableSelection();

            //表格欄位排序
            $('.manualSort').tablesorter({
                headers: {
                    0: {
                        sorter: false
                    },
                    8: {// disabled by index (class=headerDelBtn)
                        sorter: false
                    }
                }
            });
            //為了不要吃到css，移除tablesorter-header的class(否則會輸線排序的黑色箭頭)
            $('.headerSeq').removeClass('tablesorter-header');
            $('.headerDelBtn').removeClass('tablesorter-header');

            
            $('.manualSort').bind("sortEnd",function() { 
                DelegateRearrange();
            }); 
        }

        function AutoHideOrDisplayGrossMargin() {
            <%--綁定折價券才顯示毛利率--%>
            if ("<%= EventUseDiscount %>" == "True") {
                $('.grossMarginTd').show();
                $("#grossMarginmsg").show();
            }
            else {
                $('.grossMarginTd').hide();
                $("#grossMarginmsg").hide();
            }
        }

        function DelegateRearrange() {
            var tdArray = $('td.itemSeqTd');
            $.each(tdArray, function (index, obj) {
                $(obj).children('.itemSeqDiv').html(index + 1);//前端顯示用
                $(obj).children("input[id$='HidTempSeq']").val(index + 1);//後端儲存用
            });
        }

        function AlertImportErrorMsg(errorMsg) {
            $("<div id='alertDialog'>").append(errorMsg).dialog({
                modal: true,
                title: "匯入失敗",
                minWidth: 800,
                buttons: [{
                    text: "確定",
                    click: function() {
                        $(this).dialog("close");
                    }
                }],
                close: function () {
                    $(this).remove();
                }
            }).dialog("open");
        }
    </script>
    <asp:HiddenField ID="hif_Mode" runat="server" />
    <asp:HiddenField ID="hif_EventId" runat="server" />
    <asp:HiddenField ID="hif_ItemId" runat="server" />
    <asp:HiddenField ID="hif_ItemCount" runat="server" />
    <asp:HiddenField ID="hif_VourcherItemCount" runat="server" />
    <asp:HiddenField ID="hif_VotePicItemCount" runat="server" />
    <asp:HiddenField ID="hif_TemplateType" runat="server" />
    <asp:HiddenField ID="hif_SeoId" runat="server" />
    <asp:Panel ID="pan_EventList" runat="server">
        <asp:CheckBox ID="cbx_LimitInThreeMonth" runat="server" Text="只顯示3個月內的" Checked="true" OnCheckedChanged="LimitInThreeMonth_CheckedChanged" AutoPostBack="true" />
        <br />
        <asp:Button ID="btn_AddEventPromo" runat="server" Text="新增商品主題活動" CommandArgument="1"
            OnCommand="AddEventPromo" />

        <a href="../System/randomcms.aspx" target="_blank">活動Banner設定</a>

        <asp:GridView ID="gv_EventPromo" runat="server" AutoGenerateColumns="false" OnRowCommand="gvEventPromoCommand" OnRowDataBound="gv_EventPromo_RowDataBound" Width="95%">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <table style="background-color: gray;">
                            <tr>
                                <td class="bgOrange" style="width: 3%" rowspan="2">ID</td>
                                <td class="bgOrange" style="width: 10%" rowspan="2">開始時間</td>
                                <td class="bgOrange" style="width: 10%" rowspan="2">結束時間</td>
                                <td class="bgOrange" style="width: 10%" rowspan="2">版型</td>
                                <td class="bgOrange" style="width: 6%" rowspan="2">類型</td>
                                <td class="bgOrange" style="width: 23%" rowspan="2">商品主題活動名稱</td>
                                <td class="bgOrange" style="width: 21%" rowspan="2">連結</td>
                                <td class="bgOrange" style="width: 12%" rowspan="2" colspan="4">狀態</td>
                                <td class="bgOrange" style="width: 5%" colspan="2">顯示於</td>
                            </tr>
                            <tr>
                                <td class="bgOrange">Web</td>
                                <td class="bgOrange">App</td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="bgBlue">
                                <%# ((EventPromo)(Container.DataItem)).Id %>
                            </td>
                            <td class="bgBlue">
                                <%# ((EventPromo)(Container.DataItem)).StartDate.ToString("yyyy/MM/dd HH:mm")%>
                            </td>
                            <td class="bgBlue">
                                <%# ((EventPromo)(Container.DataItem)).EndDate.ToString("yyyy/MM/dd HH:mm")%>
                            </td>
                            <td class="bgBlue">
                                <%# Helper.GetLocalizedEnum((EventPromoTemplateType)(((EventPromo)(Container.DataItem)).TemplateType))%>
                                <asp:HiddenField ID="hidEventPromoTemplateType" runat="server" Value="<%# ((EventPromo)(Container.DataItem)).TemplateType%>" />
                            </td>
                            <td class="bgBlue" style="text-align: center;">
                                <%# Helper.GetLocalizedEnum((EventPromoEventType)(((EventPromo)(Container.DataItem)).EventType))%>
                            </td>
                            <td class="bgBlue" style="word-break: break-all;">
                                <asp:LinkButton ID="lbt_Edit" runat="server" CommandName="EditEvent" CommandArgument="<%# ((EventPromo)(Container.DataItem)).Id %>"><%# ((EventPromo)(Container.DataItem)).Title%></asp:LinkButton>
                            </td>
                            <td class="bgBlue" style="word-break: break-all;">
                                <asp:Label ID="lblLink" runat="server"></asp:Label>
                            </td>
                            <td class="bgBlue" style="text-align:center;">
                                <asp:Label ID="discountMsg" runat="server" Width="40px"></asp:Label>
                            </td>
                            <td class="bgBlue" style="text-align:center;">
                                <asp:LinkButton ID="lbt_ShowItemList" runat="server" CommandName="ShowItemList" CommandArgument="<%# ((EventPromo)(Container.DataItem)).Id %>" Font-Size="Small" Width="30px">設定商品</asp:LinkButton>
                            </td>
                            <td class="bgBlue" style="text-align:center;">
                                <asp:HyperLink ID="lkPreview" runat="server" Target="_blank" Font-Size="Small">預覽</asp:HyperLink>
                            </td>
                            <td class="bgBlue" style="text-align:center;">
                                <asp:ImageButton ID="ibt_ChangeStatus" runat="server" CommandName="ChangeStatus"
                                    Width="28" CommandArgument="<%# ((EventPromo)(Container.DataItem)).Id %>" ImageUrl='<%# ((EventPromo)(Container.DataItem)).Status ? "~/Themes/default/images/17Life/G3/CheckoutPass.png"  : "~/Themes/default/images/17Life/G3/CheckoutError.png"%>' />
                            </td>
                            <td class="bgBlue" style="text-align:center;">
                                <asp:CheckBox runat="server" ID="ShowInWeb" Checked="<%# ((EventPromo)(Container.DataItem)).ShowInWeb %>" OnCheckedChanged="ShowInWebChange" AutoPostBack="True" />
                            </td>
                            <td class="bgBlue" style="text-align:center;">
                                <asp:CheckBox runat="server" ID="ShowInApp" Checked="<%# ((EventPromo)(Container.DataItem)).ShowInApp %>" OnCheckedChanged="ShowInAppChange" AutoPostBack="True" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pan_EventEdit" runat="server" Visible="false">
        <table style="width: 1000px">
            <tr>
                <td colspan="2" class="header">一、商品主題活動頁面設定
                </td>
            </tr>
            <tr>
                <td class="title">活動名稱:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_EventName" runat="server" Width="300"></asp:TextBox>
                    <span style="color: Gray">請以中文命名！將帶入此活動網頁title</span>
                    <asp:RequiredFieldValidator ID="rev_EventName" runat="server" ErrorMessage="*" ControlToValidate="tbx_EventName"
                        Display="Dynamic" ForeColor="Red" ValidationGroup="A"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="title">自訂網址:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_Url" runat="server" Width="300"></asp:TextBox>
                    <span style="color: Gray">請以英文命名，將帶入此活動網網址 </span>
                    <asp:RequiredFieldValidator ID="rev_Url" runat="server" ErrorMessage="*" ControlToValidate="tbx_Url"
                        Display="Dynamic" ForeColor="Red" ValidationGroup="A"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="title">CPA:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_Cpa" runat="server" Width="300"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="title">活動期間:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_Start" runat="server" Width="100"></asp:TextBox>
                    <asp:DropDownList ID="ddl_StartH" runat="server"></asp:DropDownList>:
                    <asp:DropDownList ID="ddl_Startm" runat="server"></asp:DropDownList>
                    <cc1:CalendarExtender ID="ce_Start" TargetControlID="tbx_Start" runat="server">
                    </cc1:CalendarExtender>
                    ~
                    <asp:TextBox ID="tbx_End" runat="server" Width="100"></asp:TextBox>
                    <asp:DropDownList ID="ddl_EndH" runat="server"></asp:DropDownList>:
                    <asp:DropDownList ID="ddl_Endm" runat="server"></asp:DropDownList>
                    <cc1:CalendarExtender ID="ce_End" TargetControlID="tbx_End" runat="server">
                    </cc1:CalendarExtender>
                    <asp:RequiredFieldValidator ID="rev_Start" runat="server" ErrorMessage="*起" ControlToValidate="tbx_Start"
                        Display="Dynamic" ForeColor="Red" ValidationGroup="A"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rev_End" runat="server" ErrorMessage="*迄" ControlToValidate="tbx_End"
                        Display="Dynamic" ForeColor="Red" ValidationGroup="A"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="title">版型設定:
                </td>
                <td class="bgBlue">
                    <asp:DropDownList ID="ddlTemplateType" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="title">活動類型:
                </td>
                <td class="bgBlue">
                    <asp:DropDownList ID="ddlEventType" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr class="voteArea">
                <td class="title">投票周期:
                </td>
                <td class="bgBlue">
                    <asp:RadioButton ID="rdo_VoteTypeEveryday" GroupName="VoteType" Text="每日(每日凌晨0點可再投)" Checked="true" runat="server" />
                    <asp:RadioButton ID="rdo_VoteTypeAllCycle" GroupName="VoteType" Text="整個活動期間(至活動截止不可再投)" runat="server" />
                </td>
            </tr>
            <tr class="voteArea">
                <td class="title">周期票數:
                </td>
                <td class="bgBlue">每人於單一周期可投
                    <asp:TextBox ID="tbx_CycleMaxVotes" Width="30" MaxLength="2" CssClass="number" runat="server"></asp:TextBox>
                    票
                </td>
            </tr>
            <tr class="voteArea">
                <td class="title">行銷標題:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_EventPromoTitle" Width="500" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr class="voteArea">
                <td class="title">活動簡介:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_EventIntroduction" runat="server" Width="750" Height="300" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr class="voteArea">
                <td class="title">參加獎(折價券):
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_CampaignId" placeholder="折價券ID" Width="65" MaxLength="4" CssClass="number" runat="server"></asp:TextBox>
                    <span style="color: Gray">首次參與本活動(限團購券投票)贈送一筆折價券，如不需此功能可填0或不填。</span>
                </td>
            </tr>
            <tr>
                <td class="title">團購綁定:</td>
                <td class="bgBlue">
                    <asp:CheckBoxList ID="cbl_CategoryBind" runat="server" Font-Size="Small" RepeatLayout="Table"
                        RepeatDirection="Horizontal" RepeatColumns="7">
                    </asp:CheckBoxList>
                </td>
            </tr>

            <tr>
                <td class="title">SEO description:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_SEOd" runat="server" Width="600"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="title">SEO KeyWords:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_SEOk" runat="server" Width="600"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td colspan="2" class="header">二、圖片編輯
                </td>
            </tr>
            <tr>
                <td class="title">主圖:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="txtMainPic" runat="server" Width="750" Height="300" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="title">行動版主圖:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="txtMobileMainPic" runat="server" Width="750" Height="300" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="title">背景:
                </td>
                <td class="bgBlue">
                    <cc1:TextBoxWatermarkExtender ID="tbw_BgPic" runat="server" TargetControlID="tbx_BgPic"
                        WatermarkCssClass="ldnmbg" WatermarkText="http://">
                    </cc1:TextBoxWatermarkExtender>
                    <asp:TextBox ID="tbx_BgPic" runat="server" Width="300"></asp:TextBox>
                    &nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="tbx_BgColor" runat="server"></asp:TextBox>
                    <cc1:ColorPickerExtender runat="server" TargetControlID="tbx_BgColor" ID="cpe_BgColor" />
                </td>
            </tr>
            <tr>
                <td class="title" rowspan="2">APP相關素材
                </td>
                <td class="bgBlue">活動頻道banner(294X51):
                    <br />
                    顯示位置：<span style="color:red;font-size:small;">(行銷活動不可勾選 "APP指定頻道上方列表Banner"、"APP指定頻道產品列表輪播")</span>
                    <asp:CheckBoxList ID="chbBannerLocation" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static"></asp:CheckBoxList>
                    <asp:Image runat="server" ID="appBannerImage" />
                    <br />
                    上傳圖片:
                    <asp:FileUpload ID="fuAppBannerImage" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="bgBlue">列表banner(594X194)
                    <br />
                    <asp:Image runat="server" ID="appPromoImage" />
                    <br />
                    上傳圖片:
                    <asp:FileUpload ID="fuAppPromoImage" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" class="header">三、商品類別按鈕
                </td>
            </tr>
            <tr>
                <td class="title">按鈕圖片:
                </td>
                <td class="bgBlue">
                    <table style="width: 100%">
                        <tr>
                            <td></td>
                            <td>Original
                            </td>
                            <td>Hover
                            </td>
                            <td>Active
                            </td>
                        </tr>
                        <tr>
                            <td>圖片連結:</td>
                            <td>
                                <asp:TextBox ID="tbx_BtnOriginal" runat="server"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="tbw_BtnOriginal" runat="server" TargetControlID="tbx_BtnOriginal"
                                    WatermarkCssClass="ldnmbg" WatermarkText="http://">
                                </cc1:TextBoxWatermarkExtender>
                            </td>
                            <td>
                                <asp:TextBox ID="tbx_BtnHover" runat="server"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="tbw_BtnHover" runat="server" TargetControlID="tbx_BtnHover"
                                    WatermarkCssClass="ldnmbg" WatermarkText="http://">
                                </cc1:TextBoxWatermarkExtender>
                            </td>
                            <td>
                                <asp:TextBox ID="tbx_BtnAvtive" runat="server"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="tbw_BtnActive" runat="server" TargetControlID="tbx_BtnAvtive"
                                    WatermarkCssClass="ldnmbg" WatermarkText="http://">
                                </cc1:TextBoxWatermarkExtender>
                            </td>
                        </tr>
                        <tr>
                            <td>字體顏色:</td>
                            <td>
                                <asp:TextBox ID="tbx_BtnFontColor" runat="server"></asp:TextBox>
                                <cc1:ColorPickerExtender ID="cpe_BrnFontColor" runat="server" TargetControlID="tbx_BtnFontColor" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ValidationExpression="^(#|)(?:[0-9a-fA-F]{3}){1,2}$" ControlToValidate="tbx_BtnFontColor" Display="Dynamic" ErrorMessage="必需輸入Hex網頁色碼"></asp:RegularExpressionValidator>
                            </td>
                            <td class="bgBlue">
                                <asp:TextBox ID="tbx_BtnFontColorHover" MaxLength="7" runat="server"></asp:TextBox>
                                <cc1:ColorPickerExtender ID="ColorPickerExtender9" runat="server" TargetControlID="tbx_BtnFontColorHover" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ValidationExpression="^(#|)(?:[0-9a-fA-F]{3}){1,2}$" ControlToValidate="tbx_BtnFontColorHover" Display="Dynamic" ErrorMessage="必需輸入Hex網頁色碼"></asp:RegularExpressionValidator>
                            </td>
                            <td class="bgBlue">
                                <asp:TextBox ID="tbx_BtnFontColorActive" MaxLength="7" runat="server"></asp:TextBox>
                                <cc1:ColorPickerExtender ID="ColorPickerExtender10" runat="server" TargetControlID="tbx_BtnFontColorActive" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server" ValidationExpression="^(#|)(?:[0-9a-fA-F]{3}){1,2}$" ControlToValidate="tbx_BtnFontColorActive" Display="Dynamic" ErrorMessage="必需輸入Hex網頁色碼"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="header">四、子頁籤按鈕及顏色
                </td>
            </tr>
            <tr>
                <td class="title">項目:
                </td>
                <td class="bgBlue">背景色
                    <asp:TextBox ID="txtSubCagegoryBackgroundColor" runat="server"></asp:TextBox>
                    <cc1:ColorPickerExtender ID="ColorPickerExtender1" runat="server" TargetControlID="txtSubCagegoryBackgroundColor" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^(#|)(?:[0-9a-fA-F]{3}){1,2}$" ControlToValidate="txtSubCagegoryBackgroundColor" Display="Dynamic" ErrorMessage="必需輸入Hex網頁色碼"></asp:RegularExpressionValidator>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    文字顏色
                    <asp:TextBox ID="txtSubCategoryFontColor" runat="server"></asp:TextBox>
                    <cc1:ColorPickerExtender ID="ColorPickerExtender2" runat="server" TargetControlID="txtSubCategoryFontColor" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="^(#|)(?:[0-9a-fA-F]{3}){1,2}$" ControlToValidate="txtSubCategoryFontColor" Display="Dynamic" ErrorMessage="必需輸入Hex網頁色碼"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td rowspan="1" class="title">快速功能鍵:
                </td>
                <td class="bgBlue">
                    <table style="width: 100%">
                        <tr>
                            <td>呈現方式</td>
                            <td colspan="3">
                                <asp:RadioButton ID="rdoShowColor" Text="底色" Checked="true" GroupName="showType" runat="server" />
                                <asp:RadioButton ID="rdoShowImage" Text="底圖" GroupName="showType" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Original
                            </td>
                            <td>Hover
                            </td>
                            <td>Active
                            </td>
                        </tr>
                        <tr>
                            <td>底色</td>
                            <td>
                                <asp:TextBox ID="txtSubCategoryBtnColorOriginal" runat="server"></asp:TextBox>
                                <cc1:ColorPickerExtender ID="ColorPickerExtender3" runat="server" TargetControlID="txtSubCategoryBtnColorOriginal" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationExpression="^(#|)(?:[0-9a-fA-F]{3}){1,2}$" ControlToValidate="txtSubCategoryBtnColorOriginal" Display="Dynamic" ErrorMessage="必需輸入Hex網頁色碼"></asp:RegularExpressionValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtSubCategoryBtnColorHover" runat="server"></asp:TextBox>
                                <cc1:ColorPickerExtender ID="ColorPickerExtender5" runat="server" TargetControlID="txtSubCategoryBtnColorHover" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationExpression="^(#|)(?:[0-9a-fA-F]{3}){1,2}$" ControlToValidate="txtSubCategoryBtnColorHover" Display="Dynamic" ErrorMessage="必需輸入Hex網頁色碼"></asp:RegularExpressionValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtSubCategoryBtnColorActive" runat="server"></asp:TextBox>
                                <cc1:ColorPickerExtender ID="ColorPickerExtender6" runat="server" TargetControlID="txtSubCategoryBtnColorActive" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ValidationExpression="^(#|)(?:[0-9a-fA-F]{3}){1,2}$" ControlToValidate="txtSubCategoryBtnColorActive" Display="Dynamic" ErrorMessage="必需輸入Hex網頁色碼"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>底圖</td>
                            <td>
                                <asp:TextBox ID="txtSubCategoryBtnImageOriginal" runat="server"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSubCategoryBtnImageOriginal"
                                    WatermarkCssClass="ldnmbg" WatermarkText="http://">
                                </cc1:TextBoxWatermarkExtender>
                            </td>
                            <td>
                                <asp:TextBox ID="txtSubCategoryBtnImageHover" runat="server"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtSubCategoryBtnImageHover"
                                    WatermarkCssClass="ldnmbg" WatermarkText="http://">
                                </cc1:TextBoxWatermarkExtender>
                            </td>
                            <td>
                                <asp:TextBox ID="txtSubCategoryBtnImageActive" runat="server"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtSubCategoryBtnImageActive"
                                    WatermarkCssClass="ldnmbg" WatermarkText="http://">
                                </cc1:TextBoxWatermarkExtender>
                            </td>
                        </tr>
                        <tr>
                            <td>文字顏色</td>
                            <td>
                                <asp:TextBox ID="txtSubCategoryBtnFontOriginal" MaxLength="7" runat="server"></asp:TextBox>
                                <cc1:ColorPickerExtender ID="ColorPickerExtender4" PopupPosition="BottomRight" runat="server" TargetControlID="txtSubCategoryBtnFontOriginal" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^(#|)(?:[0-9a-fA-F]{3}){1,2}$" ControlToValidate="txtSubCategoryBtnFontOriginal" Display="Dynamic" ErrorMessage="必需輸入Hex網頁色碼"></asp:RegularExpressionValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtSubCategoryBtnFontHover" MaxLength="7" runat="server"></asp:TextBox>
                                <cc1:ColorPickerExtender ID="ColorPickerExtender7" PopupPosition="BottomRight" runat="server" TargetControlID="txtSubCategoryBtnFontHover" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ValidationExpression="^(#|)(?:[0-9a-fA-F]{3}){1,2}$" ControlToValidate="txtSubCategoryBtnFontHover" Display="Dynamic" ErrorMessage="必需輸入Hex網頁色碼"></asp:RegularExpressionValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtSubCategoryBtnFontActive" MaxLength="7" runat="server"></asp:TextBox>
                                <cc1:ColorPickerExtender ID="ColorPickerExtender8" PopupPosition="BottomRight" runat="server" TargetControlID="txtSubCategoryBtnFontActive" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ValidationExpression="^(#|)(?:[0-9a-fA-F]{3}){1,2}$" ControlToValidate="txtSubCategoryBtnFontActive" Display="Dynamic" ErrorMessage="必需輸入Hex網頁色碼"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="header">五、活動辦法 (若空白則不會顯現)
                </td>
            </tr>
            <tr>
                <td colspan="2" class="bgBlue">
                    <span style="color: Gray">無法變更字形、字元大小與顏色 </span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="tbx_Html" runat="server" Width="900" Height="300" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="header">六、相關檔次設定
                </td>
            </tr>
            <tr>
                <td class="title">黑標文案:</td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_DealPromoTitle" runat="server" MaxLength="50"></asp:TextBox>
                    字數請勿太多！訊息將接續檔次黑標之後
                </td>
            </tr>
            <tr>
                <td class="title">活動LOGO:</td>
                <td class="bgBlue">檔次大圖將壓上活動LOGO
                    <br />
                    <asp:Image runat="server" ID="imgDealPromoImage" />
                    <br />
                    上傳圖片:
                    <asp:FileUpload ID="fuDealPromoImage" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="title">詳細介紹文案：</td>
                <td class="bgBlue">放置檔次詳細介紹最下方(560X不限)
                    <asp:TextBox ID="tbx_DealPromoDescription" runat="server" Width="900" Height="300" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="bgBlue" style="text-align: center">
                    <asp:Button ID="btn_Save" runat="server" Text="新增" ForeColor="Blue" ValidationGroup="A"
                        OnClick="SavePromo" />&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_Cancel" runat="server" Text="取消" ForeColor="Red" CommandArgument="0"
                        OnCommand="AddEventPromo" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pan_ItemList" runat="server" Visible="false">
        <asp:Button ID="btn_AddEventPromoItem" runat="server" Text="新增商品主題商品" CommandArgument="4"
            OnCommand="AddEventPromo" />
        <asp:Button ID="btn_BackEventPromoList" runat="server" Text="回主題活動列表" CommandArgument="0"
            OnCommand="AddEventPromo" />
        <asp:FileUpload ID="FileUpload1" runat="server" />
        <asp:Button ID="btnImport" runat="server" Text="匯入bid" OnClick="btnImport_Click" />
        &nbsp;<span onclick="if($('#importExample').css('display') == 'none'){$('#importExample').show();}else{$('#importExample').hide();}" style="cursor: pointer; color: blue;">[?]</span>
        <asp:Button ID="btn_SaveSort" runat="server" Text="儲存排序" OnClientClick="return confirm('會覆蓋掉原有排序，確定要儲存？');" OnClick="btn_SaveSort_Click" /><br />
        <span id="importExample" style="line-height: 24px; display: none;">
            <br />
            <br />
            *註: 請上傳txt檔案，並以換行符號分隔
            <ul>
                <li>若無類別，範例如下:
                <ul style="list-style-type: none;">
                    <li>2EB9234B-7C0C-45B7-877A-0000B45F0657</li>
                    <li>7FEAFC2F-FABC-4381-8A1F-0002786FCBDC</li>
                    <li>28D61EEA-9B02-4412-9727-00036CE71DE9</li>
                </ul>
                </li>
                <li>若有類別，範例如下:
                <ul style="list-style-type: none;">
                    <li>2EB9234B-7C0C-45B7-877A-0000B45F0657,本週推薦</li>
                    <li>7FEAFC2F-FABC-4381-8A1F-0002786FCBDC,本週推薦</li>
                    <li>28D61EEA-9B02-4412-9727-00036CE71DE9,特別企劃</li>
                </ul>
                </li>
                <li>若有類別及子類別，範例如下:
                <ul style="list-style-type: none;">
                    <li>2EB9234B-7C0C-45B7-877A-0000B45F0657,本週推薦,低卡輕食</li>
                    <li>7FEAFC2F-FABC-4381-8A1F-0002786FCBDC,本週推薦,低卡輕食</li>
                    <li>28D61EEA-9B02-4412-9727-00036CE71DE9,本週推薦,超值套餐</li>
                    <li>297FF972-EC0E-445A-A91B-000779A91337,特別企劃,貴婦下午茶</li>
                </ul>
                </li>
            </ul>
        </span>
        <br />
        
        <b>團購券:</b>
        <div id="xoxox"></div>
        <p id="grossMarginmsg" style="color: red">本檔活動已綁折價券，低毛利商品若未標示「開放」者，一律不顯示於前台:</p>
        <asp:Repeater ID="rpt_EventPromoItem" runat="server" OnItemCommand="rptEventPromoItemCommand">
            <HeaderTemplate>
                <table class="manualSort" style="background-color: gray; width: 1240px;word-break: break-all;">
                    <thead>
                        <tr>
                            <td class="bgOrange headerSeq" style="min-width: 40px">排序</td>
                            <td class="bgOrange" style="max-width: 500px">商品名稱</td>
                            <td class="bgOrange" style="min-width: 100px">營業額</td>
                            <td class="bgOrange" style="min-width: 100px">毛利額</td>
                            <td class="bgOrange grossMarginTd" style="min-width: 50px;">毛利率</td>
                            <td id="statusTd" colspan="3" class="bgOrange" style="min-width: 130px">狀態</td>
                            <td class="bgOrange" style="min-width: 100px">商品類別頁籤</td>
                            <td class="bgOrange" style="min-width: 100px">商品子類別</td>
                            <td class="bgOrange headerDelBtn" style="min-width: 80px;">刪除檔次</td>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="bgBlue itemSeqTd">
                        <asp:HiddenField runat="server" ID="HidId" Value="<%# ((ViewEventPromoItem)(Container.DataItem)).Id %>" />
                        <asp:HiddenField runat="server" ID="HidTempSeq" Value="<%# ((ViewEventPromoItem)(Container.DataItem)).Seq %>" />
                        <div class="itemSeqDiv">
                            <%# ((ViewEventPromoItem)(Container.DataItem)).Seq%>
                        </div>
                    </td>
                    <td class="bgBlue">
                        <%# ((ViewEventPromoItem)(Container.DataItem)).Title%>
                    </td>
                    <%--<td class="bgBlue grossMarginTd" style="<%# (((ViewEventPromoItem)(Container.DataItem)).GrossMarginStatus & (int)EventPromoSetUpDiscountType.RedWord ) > 0 ? "color:red": ""%>
                                                            <%# (((ViewEventPromoItem)(Container.DataItem)).GrossMarginStatus & (int)EventPromoSetUpDiscountType.DisplayGrossMargin ) > 0 ?"":"display:none" %>">--%>
                    <%--<%# ((ViewEventPromoItem)(Container.DataItem)).GrossMargin + "%" %><%# (((ViewEventPromoItem)(Container.DataItem)).GrossMarginStatus & (int)EventPromoSetUpDiscountType.UseDiscount ) > 0 ?"(開放)":"" %>--%>
                    <%--<td class="bgBlue" style="text-align:right;">
                        <%# GetGrossRelated(((ViewEventPromoItem)(Container.DataItem)).OrderedTotal, ((ViewEventPromoItem)(Container.DataItem)).GrossMargin, "OrderedTotal") %>
                    </td>
                    <td class="bgBlue" style="text-align:right;">
                        <%# GetGrossRelated(((ViewEventPromoItem)(Container.DataItem)).OrderedTotal, ((ViewEventPromoItem)(Container.DataItem)).GrossMargin, "GrossAmount") %>
                    </td>
                    <td class="bgBlue" style="text-align:right;">
                        <%# GetGrossRelated(((ViewEventPromoItem)(Container.DataItem)).OrderedTotal, ((ViewEventPromoItem)(Container.DataItem)).GrossMargin, "GrossMargin") %>
                    </td>--%>
                    <td class="bgBlue" style="text-align:right;">
                        <%# string.Format("{0:#,0}", ((ViewEventPromoItem)(Container.DataItem)).OrderedTotal) %>
                    </td>
                    <td class="bgBlue" style="text-align:right;">
                        <%# string.Format("{0:#,0}", ((ViewEventPromoItem)(Container.DataItem)).OrderedTotal * ((ViewEventPromoItem)(Container.DataItem)).GrossMargin / 100) %>
                    </td>
                    <td class="bgBlue grossMarginTd" style="text-align:right;
                                                            <%# (((ViewEventPromoItem)(Container.DataItem)).GrossMarginStatus & 1) > 0 ? "color:red": "" %>
                                                            <%# (((ViewEventPromoItem)(Container.DataItem)).GrossMarginStatus > 0) ? "" : "display:none" %>">
                        <%# string.Format("{0:0.##}%", ((ViewEventPromoItem)(Container.DataItem)).GrossMargin) %>
                        <%# (((ViewEventPromoItem)(Container.DataItem)).GrossMarginStatus & 2) > 0 ? "(開放)" : "" %>
                    </td>
                    <td class="bgBlue" style="text-align:center;">
                        <%--狀態--%>
                        <a href='<%# "../../Ppon/default.aspx?bid="+((ViewEventPromoItem)(Container.DataItem)).BusinessHourGuid%>'
                            target="_blank">
                            <%# GetItemStatus((ViewEventPromoItem)Container.DataItem)%>
                        </a>
                    </td>
                    <td class="bgBlue" style="text-align:center;">
                        <asp:LinkButton ID="lbt_ShowItemList" runat="server" CommandName="EditItem" CommandArgument="<%# ((ViewEventPromoItem)(Container.DataItem)).Id %>">編輯</asp:LinkButton>
                    </td>
                    <td class="bgBlue" style="text-align:center;">
                        <asp:ImageButton ID="ibt_ChangeStatus" runat="server" CommandName="ChangeItemStatus"
                            Width="30" CommandArgument="<%# ((ViewEventPromoItem)(Container.DataItem)).Id %>"
                            ImageUrl='<%# ((ViewEventPromoItem)(Container.DataItem)).Status ? "~/Themes/default/images/17Life/G3/CheckoutPass.png"  : "~/Themes/default/images/17Life/G3/CheckoutError.png"%>' />
                    </td>
                    <td class="bgBlue">
                        <%# ((ViewEventPromoItem)(Container.DataItem)).Category%>
                    </td>
                    <td class="bgBlue">
                        <%# ((ViewEventPromoItem)(Container.DataItem)).SubCategory%>
                    </td>
                    <td class="bgBlue" style="text-align:center;">
                        <asp:Button ID="btn_DeleteEventPromoItem" runat="server" CommandArgument="<%# ((ViewEventPromoItem)(Container.DataItem)).Id %>" OnClientClick='return CheckDeleteEventPromoItem();' CommandName="DeleteEventPromoItem" Text="刪除" />
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <br />
        <b>優惠券:</b><br />
        <asp:Repeater ID="rptEventPromoItemVourcher" runat="server" OnItemCommand="rptEventPromoItemCommand">
            <HeaderTemplate>
                <table style="background-color: gray; width: 1000px">
                    <tr>
                        <td class="bgOrange" style="width: 40px">排序
                        </td>
                        <td class="bgOrange" style="width: 100px">活動商品編號</td>
                        <td class="bgOrange" style="width: 100px">優惠券編號</td>
                        <td class="bgOrange" style="width: 500px">店家名稱 / 優惠內容
                        </td>
                        <td colspan="3" class="bgOrange">狀態
                        </td>
                        <td class="bgOrange" style="width: 100px">商品類別頁籤
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="bgBlue">
                        <%# ((KeyValuePair<EventPromoItem, DateTime>)(Container.DataItem)).Key.Seq%>
                    </td>
                    <td class="bgBlue">
                        <%# ((KeyValuePair<EventPromoItem, DateTime>)(Container.DataItem)).Key.Id%>
                    </td>
                    <td class="bgBlue">
                        <%# ((KeyValuePair<EventPromoItem, DateTime>)(Container.DataItem)).Key.ItemId%>
                    </td>
                    <td class="bgBlue">
                        <%# ((KeyValuePair<EventPromoItem, DateTime>)(Container.DataItem)).Key.Title%><br />
                        <%# ((KeyValuePair<EventPromoItem, DateTime>)(Container.DataItem)).Key.Description%>
                    </td>
                    <td class="bgBlue">
                        <a href='<%# "../../vourcher/vourcher_preview.aspx?eventid="+((KeyValuePair<EventPromoItem, DateTime>)(Container.DataItem)).Key.ItemId%>'
                            target="_blank">
                            <%# (((KeyValuePair<EventPromoItem, DateTime>)(Container.DataItem)).Value>=DateTime.Now)?"未上檔":"已上檔"%>
                        </a>
                    </td>
                    <td class="bgBlue">
                        <asp:LinkButton ID="lbt_ShowItemList" runat="server" CommandName="EditItem" CommandArgument="<%# ((KeyValuePair<EventPromoItem, DateTime>)(Container.DataItem)).Key.Id %>">編輯</asp:LinkButton>
                    </td>
                    <td class="bgBlue">
                        <asp:ImageButton ID="ibt_ChangeStatus" runat="server" CommandName="ChangeItemStatus"
                            Width="30" CommandArgument="<%# ((KeyValuePair<EventPromoItem, DateTime>)(Container.DataItem)).Key.Id %>"
                            ImageUrl='<%# ((KeyValuePair<EventPromoItem, DateTime>)(Container.DataItem)).Key.Status ? "~/Themes/default/images/17Life/G3/CheckoutPass.png"  : "~/Themes/default/images/17Life/G3/CheckoutError.png"%>' />
                    </td>
                    <td class="bgBlue">
                        <%# ((KeyValuePair<EventPromoItem, DateTime>)(Container.DataItem)).Key.Category%>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <br />
        <b>圖片投票:</b><br />
        <asp:Repeater ID="rptVotePic" runat="server" OnItemCommand="rptEventPromoItemCommand">
            <HeaderTemplate>
                <table style="background-color: gray; width: 1000px">
                    <tr>
                        <td class="bgOrange" style="width: 40px">排序
                        </td>
                        <td class="bgOrange">圖片
                        </td>
                        <td class="bgOrange" style="width: 400px">商品名稱
                        </td>
                        <td colspan="3" class="bgOrange">狀態
                        </td>
                        <td class="bgOrange" style="width: 100px">商品類別頁籤
                        </td>
                        <td class="bgOrange" style="width: 100px">檔號
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="bgBlue">
                        <%# ((ViewEventPromoItemPicVote)(Container.DataItem)).Seq%>
                    </td>
                    <td class="bgBlue">
                        <img alt="" src='<%# ImageFacade.GetMediaPath(((ViewEventPromoItemPicVote)(Container.DataItem)).ItemPicUrl, MediaType.DealPromoImage)%>' />
                    </td>
                    <td class="bgBlue">
                        <%# ((ViewEventPromoItemPicVote)(Container.DataItem)).Title%>&nbsp;
                        <%# ((ViewEventPromoItemPicVote)(Container.DataItem)).Description%>
                    </td>
                    <td class="bgBlue">
                        <a href='<%# ((ViewEventPromoItemPicVote)(Container.DataItem)).LinkItemType == (int)EventPromoItemType.Ppon ? "../../Ppon/default.aspx?bid="+((ViewEventPromoItemPicVote)(Container.DataItem)).BusinessHourGuid : "../../vourcher/vourcher_preview.aspx?eventid="+((ViewEventPromoItemPicVote)(Container.DataItem)).ItemId%>'
                            target="_blank">
                            <%# ((ViewEventPromoItemPicVote)(Container.DataItem)).LinkItemType == (int)EventPromoItemType.Ppon ?
                            (((ViewEventPromoItemPicVote)(Container.DataItem)).BusinessHourOrderTimeS>=DateTime.Now) ? "未上檔" : "已上檔" 
                            : (((ViewEventPromoItemPicVote)(Container.DataItem)).StartDate>=DateTime.Now)?"未上檔":"已上檔"%>
                        </a>
                    </td>
                    <td class="bgBlue">
                        <asp:LinkButton ID="lbt_ShowItemList" runat="server" CommandName="EditItem" CommandArgument="<%# ((ViewEventPromoItemPicVote)(Container.DataItem)).Id %>">編輯</asp:LinkButton>
                    </td>
                    <td class="bgBlue">
                        <asp:ImageButton ID="ibt_ChangeStatus" runat="server" CommandName="ChangeItemStatus"
                            Width="30" CommandArgument="<%# ((ViewEventPromoItemPicVote)(Container.DataItem)).Id %>"
                            ImageUrl='<%# ((ViewEventPromoItemPicVote)(Container.DataItem)).Status ? "~/Themes/default/images/17Life/G3/CheckoutPass.png"  : "~/Themes/default/images/17Life/G3/CheckoutError.png"%>' />
                    </td>
                    <td class="bgBlue">
                        <%# ((ViewEventPromoItemPicVote)(Container.DataItem)).Category%>
                    </td>
                    <td class="bgBlue">
                        <%# ((ViewEventPromoItemPicVote)(Container.DataItem)).ItemId %>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <asp:Panel ID="pan_ItemEdit" runat="server" Visible="false">
        <asp:Button ID="btn_BackEventPromoItemList" runat="server" Text="回活動商品列表" CommandArgument="3"
            OnCommand="AddEventPromo" />
        <table style="width: 700px">
            <tr>
                <td colspan="2" class="header">一、主題活動頁面商品設定
                </td>
            </tr>
            <tr>
                <td class="title">商品類別
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_Category" runat="server"></asp:TextBox><span style="color: Gray">數字.類別(如:
                        1.旅遊)</span>
                </td>
            </tr>
            <tr>
                <td class="title">子類別
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_SubCategory" runat="server"></asp:TextBox><span style="color: Gray">新增子分類(如1.1旅遊)</span>
                </td>
            </tr>
            <tr>
                <td class="title">類型:
                </td>
                <td class="bgBlue">
                    <asp:RadioButtonList ID="rdlItemType" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal"></asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="title">商品編號:
                </td>
                <td class="bgBlue">
                    <asp:RadioButton ID="rd_PponLink" runat="server" GroupName="voteLinkType" CssClass="picVoteArea" Text="團購券" Checked="true" />
                    <asp:RadioButton ID="rd_VourcherLink" runat="server" GroupName="voteLinkType" CssClass="picVoteArea" Text="優惠券" />
                    <asp:TextBox ID="tbx_ItemId" Width="240" runat="server"></asp:TextBox>&nbsp;&nbsp;
                    <asp:Button ID="btn_CheckItemId" runat="server" Text="檢視" OnClick="CheckItemId" /><br />
                    <span style="color: Gray; font-size: 10px">團購券填入檔號或BID，優惠券請填入優惠券編號</span>
                </td>
            </tr>
            <tr class="picVoteArea" style="display: none">
                <td class="title">上傳圖片</td>
                <td class="bgBlue">
                    <asp:FileUpload ID="fuUploadVotePic" runat="server" />
                    <div>
                        <asp:Image ID="img_vote" runat="server" Visible="false" />
                        <asp:HiddenField ID="hif_itemPicUrl" Value="" runat="server" />
                    </div>
                </td>
            </tr>
            <tr>
                <td class="title">排序:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_Seq" runat="server" Width="100"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_Seq" runat="server" ControlToValidate="tbx_Seq"
                        ErrorMessage="*" Display="Dynamic" ForeColor="Red" ValidationGroup="B"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rv_Deq" runat="server" ErrorMessage="數字" MinimumValue="0"
                        MaximumValue="9999" Type="Integer" ControlToValidate="tbx_Seq" Display="Dynamic"
                        ForeColor="Red" ValidationGroup="B"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="title">商品名稱:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_ItemTitle" runat="server" Width="300"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_ItemTitle" runat="server" ControlToValidate="tbx_ItemTitle"
                        ErrorMessage="*" Display="Dynamic" ForeColor="Red" ValidationGroup="B"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="title">商品行銷文案:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_ItemDescription" runat="server" Width="300"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_ItemDescription" runat="server" ControlToValidate="tbx_ItemDescription"
                        ErrorMessage="*" Display="Dynamic" ForeColor="Red" ValidationGroup="B"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="title">商品狀態:
                </td>
                <td class="bgBlue">
                    <asp:Label ID="lab_ItemStatus" runat="server" Width="300"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="bgBlue" style="text-align: center">
                    <asp:Button ID="btn_ItemSave" runat="server" Text="新增" ForeColor="Blue" ValidationGroup="B"
                        OnClick="SavePromoItem" />&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_ItemCancel" runat="server" Text="取消" ForeColor="Red" CommandArgument="3"
                        OnCommand="AddEventPromo" />
                </td>
            </tr>
        </table>
        <div id="linkVourcherArea" style="display: none">
            <br />
            <table style="width: 700px">
                <tr>
                    <td colspan="2" class="header">二、連結優惠券設定
                    </td>
                </tr>
                <tr>
                    <td class="title">編號:
                    </td>
                    <td class="bgBlue">
                        <asp:RadioButton ID="rdoVourcher" GroupName="linkType" Text="優惠券" runat="server" Checked="true" />
                        <asp:RadioButton ID="rdoPromoItem" GroupName="linkType" Text="活動商品編號" runat="server" />
                        <asp:TextBox ID="txt_VourcherId" class="number" runat="server"></asp:TextBox>
                        <asp:Button ID="btn_LinkVourcherId" runat="server" Text="連結" OnClick="CheckVourcherId" />
                        <asp:Button ID="btn_RemoveLinkVourcherId" runat="server" Text="移除連結" Enabled="false" OnClick="RemoveLinkVourcher" />
                        <asp:HiddenField ID="hidVourcherEventPromoItemId" runat="server" Value="0" />
                        <asp:HiddenField ID="hidVourcherEventPromoItemItemId" runat="server" Value="0" />
                    </td>
                </tr>
            </table>
            <asp:PlaceHolder ID="ph_VourcherInfo" Visible="false" runat="server">
                <table style="width: 700px">
                    <tr>
                        <td class="title">商品類別
                        </td>
                        <td class="bgBlue">
                            <asp:TextBox ID="tbx_VourcherCategory" runat="server"></asp:TextBox><span style="color: Gray">數字.類別(如:
                                1.旅遊)</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="title">子類別
                        </td>
                        <td class="bgBlue">
                            <asp:TextBox ID="tbx_VourcherSubCategory" runat="server"></asp:TextBox><span style="color: Gray">新增子分類(如1.1旅遊)</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="title">排序:
                        </td>
                        <td class="bgBlue">
                            <asp:TextBox ID="tbx_VourcherSeq" runat="server" Width="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tbx_VourcherSeq"
                                ErrorMessage="*" Display="Dynamic" ForeColor="Red" ValidationGroup="B"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="數字" MinimumValue="0"
                                MaximumValue="9999" Type="Integer" ControlToValidate="tbx_VourcherSeq" Display="Dynamic"
                                ForeColor="Red" ValidationGroup="B"></asp:RangeValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="title">商品名稱:
                        </td>
                        <td class="bgBlue">
                            <asp:TextBox ID="tbx_VourcherTitle" runat="server" Width="300"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="tbx_VourcherTitle"
                                ErrorMessage="*" Display="Dynamic" ForeColor="Red" ValidationGroup="B"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="title">商品行銷文案:
                        </td>
                        <td class="bgBlue">
                            <asp:TextBox ID="tbx_VourcherDescription" runat="server" Width="300"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="tbx_VourcherDescription"
                                ErrorMessage="*" Display="Dynamic" ForeColor="Red" ValidationGroup="B"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="title">商品狀態:
                        </td>
                        <td class="bgBlue">
                            <asp:Label ID="lab_VourcherItemStatus" runat="server" Width="300"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:PlaceHolder>
        </div>
    </asp:Panel>
    <asp:Label ID="lab_Message" runat="server" ForeColor="White" BackColor="Red"></asp:Label>
</asp:Content>

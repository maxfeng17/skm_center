﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="VoucherYahooPromoSetup.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Event.VoucherYahooPromoSetup" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/adapters/jquery.js"></script>
    <link type="text/css" href="../../Themes/default/images/17Life/Gactivities/vourcher.css"
        rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(
        function () {
            if ($('#<%=lab_Message.ClientID%>').html() != '') {
                $('#<%=lab_Message.ClientID%>').fadeOut(5000);
            }
            // CKEDITOR.replace('<%=tbx_Content.ClientID%>');
        }
        );
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pan_List" runat="server">
        <table class="tablecontent" style="min-width: 1000px">
            <tr>
                <td class="tablehead" colspan="4">Yahoo優惠家列表</td>
            </tr>
            <tr>
                <td class="tablehead" style="width: 10%;">類別
                </td>
                <td>
                    <asp:DropDownList ID="ddl_ListType" runat="server" OnSelectedIndexChanged="DDL_ListChange" AutoPostBack="true">
                    </asp:DropDownList>
                     <asp:CheckBox ID="cbx_ExcludeExpired" runat="server" Text="排除已過期" Checked="true" OnCheckedChanged="DDL_ListChange" AutoPostBack="true" />
                </td>
                <td>
                    <asp:Button ID="btn_New" runat="server" Text="新增" OnClick="AddPromo" />
                </td>
                <td>
                    <asp:Button ID="btn_Refresh" runat="server" Text="更新優惠+" OnClick="RefreshPromo" />
                    <asp:Label ID="lab_RefreshMessage" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <asp:Repeater ID="rpt_List" runat="server" OnItemCommand="RptPromoCommand">
            <HeaderTemplate>
                <table class="tablecontent" style="min-width: 1000px">
                    <tr>
                        <td class="tablehead">Id</td>
                        <td class="tablehead">優惠券編號</td>
                        <td class="tablehead">優惠券內容</td>
                        <td class="tablehead">賣家</td>
                        <td class="tablehead">名稱</td>
                        <td class="tablehead">排序</td>
                        <td class="tablehead">優惠券期間</td>
                        <td class="tablehead">行銷期間</td>
                        <td class="tablehead">點擊數</td>
                        <td class="tablehead">刪除</td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:LinkButton ID="lit_Edit" runat="server" CommandArgument="<%# ((ViewYahooVoucherPromo)(Container.DataItem)).Id %>" CommandName="EditPromoById"> <%# ((ViewYahooVoucherPromo)(Container.DataItem)).Id %></asp:LinkButton>
                    </td>
                    <td>
                        <%# ((ViewYahooVoucherPromo)(Container.DataItem)).EventId %>
                    </td>
                    <td>
                        <%# ((ViewYahooVoucherPromo)(Container.DataItem)).Contents.TrimToMaxLength(30,"...") %>
                    </td>
                    <td>
                        <%# ((ViewYahooVoucherPromo)(Container.DataItem)).SellerName %>
                    </td>
                    <td>
                        <%# ((ViewYahooVoucherPromo)(Container.DataItem)).Tag %>
                    </td>
                    <td>
                        <%# ((ViewYahooVoucherPromo)(Container.DataItem)).Rank %>
                    </td>
                    <td>
                        <%# ((ViewYahooVoucherPromo)(Container.DataItem)).StartDate.HasValue? ((ViewYahooVoucherPromo)(Container.DataItem)).StartDate.Value.ToString("yyyy/MM/dd HH:mm"):string.Empty%>~
                        <%# ((ViewYahooVoucherPromo)(Container.DataItem)).EndDate.HasValue? ((ViewYahooVoucherPromo)(Container.DataItem)).EndDate.Value.ToString("yyyy/MM/dd HH:mm"):string.Empty%>
                    </td>
                    <td style='color: <%# (((ViewYahooVoucherPromo)(Container.DataItem)).PromoStartDate.HasValue&&((ViewYahooVoucherPromo)(Container.DataItem)).PromoEndDate.HasValue&&((ViewYahooVoucherPromo)(Container.DataItem)).PromoStartDate.Value<=DateTime.Now&&((ViewYahooVoucherPromo)(Container.DataItem)).PromoEndDate.Value>=DateTime.Now)?"red":"black"%>'>
                        <%# ((ViewYahooVoucherPromo)(Container.DataItem)).PromoStartDate.HasValue? ((ViewYahooVoucherPromo)(Container.DataItem)).PromoStartDate.Value.ToString("yyyy/MM/dd HH:mm"):string.Empty%>~
                        <%# ((ViewYahooVoucherPromo)(Container.DataItem)).PromoEndDate.HasValue? ((ViewYahooVoucherPromo)(Container.DataItem)).PromoEndDate.Value.ToString("yyyy/MM/dd HH:mm"):string.Empty%>
                    </td>
                    <td>
                        <%# ((ViewYahooVoucherPromo)(Container.DataItem)).PageCount %>
                    </td>
                    <td>
                        <asp:Button ID="btn_Delete" runat="server" Text="刪除" ForeColor="Red"
                            OnClientClick="return confirm('確定刪除?');" CommandArgument="<%# ((ViewYahooVoucherPromo)(Container.DataItem)).Id %>" CommandName="DeletePromoById" />
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>

    <asp:Panel ID="pan_Detail" runat="server" Visible="false">
        <table class="tablecontent" style="min-width: 1000px">
            <tr>
                <td colspan="3" class="tablehead" style="text-align: center">Yahoo優惠家區塊設定
                </td>
            </tr>
            <tr>
                <td class="tablehead" style="width: 10%;">Id
                </td>
                <td>
                    <asp:Label ID="lab_Id" runat="server"></asp:Label>
                </td>
                <td rowspan="3">
                    <asp:Image ID="img_EventImage" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="tablehead" style="width: 10%;">類別
                </td>
                <td>
                    <asp:DropDownList ID="ddl_PromoType" runat="server" OnSelectedIndexChanged="DDL_Change" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="tablehead">優惠券編號
                </td>
                <td>
                    <asp:TextBox ID="tbx_VoucherId" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rv_VoucherId" runat="server" ErrorMessage="*" Display="Dynamic" ControlToValidate="tbx_VoucherId" ValidationGroup="A"></asp:RequiredFieldValidator>
                    &nbsp;
                    <asp:Label ID="lab_VoucherEventDescription" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tablehead">名稱
                </td>
                <td colspan="2">
                    <asp:TextBox ID="tbx_Tag" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rv_Tag" runat="server" ErrorMessage="*" Display="Dynamic" ControlToValidate="tbx_Tag" ValidationGroup="A"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="tablehead">排序
                </td>
                <td colspan="2">
                    <asp:TextBox ID="tbx_Rank" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="tablehead">圖片網址
                </td>
                <td colspan="2">
                    <asp:TextBox ID="tbx_Content" runat="server" Visible="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="tablehead">分類
                </td>
                <td colspan="2">
                    <asp:CheckBoxList ID="cbx_Category" runat="server" Visible="false" RepeatDirection="Horizontal" RepeatColumns="6" RepeatLayout="Table"></asp:CheckBoxList>
                    <br />
                    <asp:CheckBoxList ID="cbx_City" runat="server" Visible="false" RepeatDirection="Horizontal" RepeatColumns="6" RepeatLayout="Table"></asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td class="tablehead">連結
                </td>
                <td colspan="2">
                    <asp:TextBox ID="tbx_link" runat="server" Visible="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="tablehead">活動期間
                </td>
                <td colspan="2">
                    <asp:TextBox ID="tbx_StartDate" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="ce_StartDate" TargetControlID="tbx_StartDate" runat="server">
                    </cc1:CalendarExtender>
                    <asp:DropDownList ID="ddl_StartDate" runat="server"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rv_StartDate" runat="server" ErrorMessage="*" Display="Dynamic" ControlToValidate="tbx_StartDate" ValidationGroup="A"></asp:RequiredFieldValidator>
                    ~<asp:TextBox ID="tbx_EndDate" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="ce_ndDate" TargetControlID="tbx_EndDate" runat="server">
                    </cc1:CalendarExtender>
                    <asp:DropDownList ID="ddl_EndDate" runat="server"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rv_EndDate" runat="server" ErrorMessage="*" Display="Dynamic" ControlToValidate="tbx_EndDate" ValidationGroup="A"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Button ID="btn_Confirm" runat="server" Text="新增" OnClick="SaveYahooVoucherPromo" ValidationGroup="A" />
                    &nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_Cancel" runat="server" Text="取消 / 回列表" ForeColor="Red" OnClick="ReturnList" />
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center">
                    <asp:Label ID="lab_Message" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hid_Id" runat="server" />
</asp:Content>

﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.ControlRoom.Event
{
    public partial class EventEdmPreview : RolePage, IEventEdmView
    {
        public event EventHandler AreaChange;

        #region props
        private EventEdmPresenter _presenter;
        public EventEdmPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string EventId
        {
            set { eid.Value = value; }
            get
            {
                if (string.IsNullOrEmpty(Request.QueryString["eid"]))
                    return "";
                else
                    return Request.QueryString["eid"];
            }
        }

        public int CityID { get; set; }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!_presenter.OnViewInitialized())
                    return;
            }
            _presenter.OnViewLoaded();
        }

        public void Default_SetUp()
        {
            ListItemCollection source = new ListItemCollection();
            source.Add(new ListItem() { Text = "請選擇", Value = "0", Selected = true });
            string[] city_unwant = { "com", "PBU", "TA" };
            foreach (PponCity city in PponCityGroup.DefaultPponCityGroup.Where(x => !city_unwant.Any(y => y.Equals(x.CityCode, StringComparison.OrdinalIgnoreCase))))
            {
                source.Add(new ListItem() { Text = city.CityName, Value = city.CityId.ToString() });
            }
            ddl_EdmCity.DataSource = source;
            ddl_EdmCity.DataTextField = "Text";
            ddl_EdmCity.DataValueField = "Value";
            ddl_EdmCity.DataBind();
        }

        protected void Index_Changed(Object sender, EventArgs e)
        {
            CityID = Convert.ToInt32(ddl_EdmCity.SelectedValue);
            if (AreaChange != null)
                AreaChange(this, null);
        }

        public void FillDeal(ViewPponDealTimeSlotCollection dtsc)
        {
            pDeal.Visible = true;
            ddlDeal1.DataSource = ddlDeal2.DataSource = ddlDeal3.DataSource = dtsc;
            ddlDeal1.DataTextField = ddlDeal2.DataTextField = ddlDeal3.DataTextField = "EventName";
            ddlDeal1.DataValueField = ddlDeal2.DataValueField = ddlDeal3.DataValueField = "BusinessHourGuid";
            ddlDeal1.DataBind();
            ddlDeal2.DataBind();
            ddlDeal3.DataBind();
        }

        protected void btn_pv_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + ResolveClientUrl("~/service/eventdm.ashx") + "?eid=" + EventId + "&d1=" + ddlDeal1.SelectedValue + "&d2=" + ddlDeal2.SelectedValue + "&d3=" + ddlDeal3.SelectedValue + "','_blank');", true);
        }
    }
}
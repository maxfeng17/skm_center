﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace LunchKingSite.Web.ControlRoom.Event
{
    public partial class WeChatSetUp : RolePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void DeleteMenu(object sender, EventArgs e)
        {
            PromotionFacade.RemoveWeChatMenu();
            lab_Message.Text = "已刪除";
        }

        protected void RecoverDefaultMenu(object sender, EventArgs e)
        {
            PromotionFacade.RemoveWeChatMenu();
            var json_serializer = new JsonSerializer();
            var inner_menu_today = new[]{new{type="click",name="美食",key="todaydeals_local"},
            new{type="click",name="完美休閒",key="todaydeals_peauty"},
            new{type="click",name="旅遊",key="todaydeals_travel"},
            new{type="click",name="宅配",key="todaydeals_delivery"},
            new{type="click",name="全家專區",key="todaydeals_family"}};
            var outer_menu_today = new { name = "今日好康", sub_button = inner_menu_today };
            var inner_menu_enjoy = new[] { new { type = "click", name = "我的17Life", key = "enjoy_mine" } ,
            new{type="click",name="好康活動",key="enjoy_promo"}};
            var outer_menu_enjoy = new { name = "17享樂", sub_button = inner_menu_enjoy };
            var outer_menu_vourcher = new { type = "click", name = "優惠券", key = "vourcher" };
            var all_menu = new List<object> { outer_menu_today, outer_menu_vourcher, outer_menu_enjoy };
            string json = json_serializer.Serialize(new { button = all_menu });
            lab_Message.Text = PromotionFacade.CreateWeChatMenu(json);
        }

        protected void CreateMenu(object sender, EventArgs e)
        {
            PromotionFacade.RemoveWeChatMenu();
            lab_Message.Text = PromotionFacade.CreateWeChatMenu(tbx_MenuJson.Text);
        }

        protected void SendWeChatMessage(object sender, EventArgs e)
        {
            lab_Message.Text = PromotionFacade.PostWeChatTextMessage(tbx_ReceiverId.Text, tbx_Content.Text);
        }

        protected void UploadImage(object sender, EventArgs e)
        {
            String[] allowedExtensions = { ".gif", ".png", ".jpeg", ".jpg" };
            if (fu_UploadImage.HasFile)
            {
                String fileExtension = Path.GetExtension(fu_UploadImage.FileName).ToLower();
                if (allowedExtensions.Any(x => x.Equals(fileExtension, StringComparison.CurrentCultureIgnoreCase)))
                {

                }
            }
        }

        protected void SendImage(object sender, EventArgs e)
        {

        }
    }
}
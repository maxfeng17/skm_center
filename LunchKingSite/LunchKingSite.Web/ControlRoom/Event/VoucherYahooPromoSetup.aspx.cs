﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.Web.ControlRoom.Event
{
    public partial class VoucherYahooPromoSetup : System.Web.UI.Page
    {
        public int PromoId
        {
            get
            {
                int id;
                if (int.TryParse(hid_Id.Value, out id))
                {
                    return id;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                hid_Id.Value = value.ToString();
            }
        }

        public VourcherPromoType PromoTypeSet
        {
            get
            {
                VourcherPromoType type;
                if (Enum.TryParse<VourcherPromoType>(ddl_PromoType.SelectedValue, out type))
                {
                    return type;
                }
                else
                {
                    return VourcherPromoType.VoucherTag;
                }
            }
        }

        public VourcherPromoType PromoTypeList
        {
            get
            {
                VourcherPromoType type;
                if (Enum.TryParse<VourcherPromoType>(ddl_ListType.SelectedValue, out type))
                {
                    return type;
                }
                else
                {
                    return VourcherPromoType.VoucherTag;
                }
            }
        }

        public List<int> SelectedCategory
        {
            get
            {
                return cbx_Category.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => int.Parse(x.Value)).ToList();
            }
            set
            {
                foreach (var item in cbx_Category.Items.Cast<ListItem>())
                {
                    item.Selected = value.Any(x => x.ToString() == item.Value);
                }
            }
        }

        public List<int> SelectedCity
        {
            get
            {
                return cbx_City.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => int.Parse(x.Value)).ToList();
            }
            set
            {
                foreach (var item in cbx_City.Items.Cast<ListItem>())
                {
                    item.Selected = value.Any(x => x.ToString() == item.Value);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ListItemCollection li = new ListItemCollection();
                li.Add(new ListItem("主題特輯", ((int)VourcherPromoType.VoucherTag).ToString()));
                li.Add(new ListItem("輪播大圖", ((int)VourcherPromoType.VoucherBlock).ToString()));
                li.Add(new ListItem("熱門排行榜", ((int)VourcherPromoType.VoucherRanking).ToString()));
                li.Add(new ListItem("分類關聯", ((int)VourcherPromoType.TagCategory).ToString()));
                li.Add(new ListItem("廣告區塊", ((int)VourcherPromoType.Ad).ToString()));
                ddl_ListType.DataSource = ddl_PromoType.DataSource = li;
                ddl_ListType.DataTextField = ddl_PromoType.DataTextField = "Text";
                ddl_ListType.DataValueField = ddl_PromoType.DataValueField = "Value";
                ddl_PromoType.DataBind();
                ddl_ListType.DataBind();

                Dictionary<int, string> category_list = VourcherFacade.GetVoucherCategoryList();
                cbx_Category.DataSource = category_list;
                cbx_Category.DataTextField = "Value";
                cbx_Category.DataValueField = "Key";
                cbx_Category.DataBind();

                cbx_City.DataSource = CityManager.Citys.Where(x => !x.Code.Equals("sys", StringComparison.OrdinalIgnoreCase)).OrderBy(x => x.Rank).ToDictionary(x => x.Id, x => x.CityName); ;
                cbx_City.DataTextField = "Value";
                cbx_City.DataValueField = "Key";
                cbx_City.DataBind();

                ListItemCollection li_hour = new ListItemCollection();
                for (int i = 0; i < 24; i++)
                {
                    li_hour.Add(new ListItem(string.Format("{0}:00", i), i.ToString()));
                }
                ddl_StartDate.DataSource = ddl_EndDate.DataSource = li_hour;
                ddl_StartDate.DataTextField = ddl_EndDate.DataTextField = "Text";
                ddl_StartDate.DataValueField = ddl_EndDate.DataValueField = "Value";
                ddl_StartDate.DataBind();
                ddl_EndDate.DataBind();
                DDL_ListChange(sender, e);

            }
        }

        protected void AddPromo(object sender, EventArgs e)
        {
            pan_Detail.Visible = true;
            pan_List.Visible = false;
            hid_Id.Value = "0";
            tbx_Content.Text = tbx_Rank.Text = tbx_Tag.Text = tbx_VoucherId.Text = lab_Id.Text = lab_VoucherEventDescription.Text = img_EventImage.ImageUrl = string.Empty;
            btn_Confirm.Text = "新增";
        }


        protected void SaveYahooVoucherPromo(object sender, EventArgs e)
        {
            lab_Message.Text = string.Empty;
            VourcherPromo promo = (PromoId != 0) ? VourcherFacade.VourcherPromoGet(PromoId) : new VourcherPromo();
            int rank, id;

            promo.Rank = int.TryParse(tbx_Rank.Text, out rank) ? rank : 0;
            promo.Type = (int)PromoTypeSet;
            promo.EventId = int.TryParse(tbx_VoucherId.Text, out id) ? id : 0;
            promo.Tag = tbx_Tag.Text;

            DateTime startdate, enddate;
            int starthour, endhour;
            if (!(DateTime.TryParse(tbx_StartDate.Text, out startdate) && DateTime.TryParse(tbx_EndDate.Text, out enddate) && int.TryParse(ddl_StartDate.SelectedValue, out starthour) && int.TryParse(ddl_EndDate.SelectedValue, out endhour)))
            {
                lab_Message.Text = "請輸入正確的活動期間";
                return;
            }
            else
            {
                promo.StartDate = startdate.AddHours(starthour);
                promo.EndDate = enddate.AddHours(endhour);
            }
            VourcherEvent v_event;
            bool check = false;


            if (PromoTypeSet == VourcherPromoType.VoucherBlock || PromoTypeSet == VourcherPromoType.TagCategory || PromoTypeSet == VourcherPromoType.Ad)
            {
                promo.SellerId = string.Empty;
                promo.SellerGuid = Guid.Empty;
                promo.EventId = 0;
                tbx_VoucherId.Text = string.Empty;
                if (PromoTypeSet == VourcherPromoType.TagCategory)
                {
                    CityCategoryModel city_category = new CityCategoryModel() { CityList = SelectedCity, CategoryList = SelectedCategory };
                    promo.Content = JsonConvert.SerializeObject(city_category);
                }
                else
                {
                    //promo.Content = HttpUtility.HtmlDecode(tbx_Content.Text);
                    promo.Content = tbx_Content.Text;
                    promo.Link = tbx_link.Text;
                }
                check = true;
            }
            else
            {
                if ((v_event = VourcherFacade.VourcherEventGetById(id)).Id != 0)
                {
                    if (v_event.EndDate.HasValue && v_event.EndDate.Value < DateTime.Now)
                    {
                        lab_Message.Text = "優惠券已過期，結束時間:" + v_event.EndDate.Value.ToString("yyyy/MM/dd");
                        return;
                    }
                    else if (!v_event.EndDate.HasValue || (v_event.Status != (int)VourcherEventStatus.EventChecked))
                    {
                        lab_Message.Text = "優惠券尚未通過審核";
                        return;
                    }
                    else
                    {
                        promo.SellerGuid = v_event.SellerGuid;
                        Seller seller = VourcherFacade.SellerGetByGuid(v_event.SellerGuid);
                        if (seller.Guid != Guid.Empty)
                        {
                            promo.SellerId = seller.SellerId;
                            promo.Content = promo.Link = string.Empty;
                            check = true;
                        }
                    }
                }
            }

            if (check)
            {
                VourcherFacade.VourcherPromoSet(promo);
                if (PromoId != 0)
                {
                    tbx_Content.Text = promo.Content;
                    lab_Message.Text = "更新成功";
                }
                else
                {
                    tbx_Content.Text = tbx_VoucherId.Text = string.Empty;
                    lab_Message.Text = "新增成功";
                    SelectedCity = SelectedCategory = new List<int>();
                }
                DDL_ListChange(sender, e);
            }
            else
            {
                lab_Message.Text = "請輸入正確的優惠券編號";
            }
        }

        protected void DDL_Change(object sender, EventArgs e)
        {
            lab_Message.Text = string.Empty;
            rv_Tag.Enabled = tbx_Tag.Visible = rv_VoucherId.Enabled = tbx_VoucherId.Visible = true;
            cbx_City.Visible = cbx_Category.Visible = tbx_Content.Visible = tbx_link.Visible = false;
            switch (PromoTypeSet)
            {
                case VourcherPromoType.VoucherBlock:
                case VourcherPromoType.Ad:
                    rv_VoucherId.Enabled = tbx_VoucherId.Visible = false;
                    tbx_Content.Visible = tbx_link.Visible = true;
                    break;
                case VourcherPromoType.TagCategory:
                    rv_VoucherId.Enabled = tbx_VoucherId.Visible = false;
                    cbx_City.Visible = cbx_Category.Visible = true;
                    break;
                case VourcherPromoType.VoucherRanking:
                    rv_Tag.Enabled = tbx_Tag.Visible = false;
                    break;
                default:
                case VourcherPromoType.VoucherTag:

                    break;
            }
        }

        protected void DDL_ListChange(object sender, EventArgs e)
        {
            ViewYahooVoucherPromoCollection vp = VourcherFacade.ViewYahooVoucherPromoGetList((int)PromoTypeList, cbx_ExcludeExpired.Checked);
            rpt_List.DataSource = vp.OrderByDescending(x => x.Id);
            rpt_List.DataBind();
        }

        protected void RptPromoCommand(object sender, RepeaterCommandEventArgs e)
        {
            int id;
            if (int.TryParse(e.CommandArgument.ToString(), out id))
            {
                if (e.CommandName == "DeletePromoById")
                {
                    VourcherFacade.VourcherPromoDelete(id);
                    DDL_ListChange(sender, new EventArgs());
                }
                else
                {
                    pan_Detail.Visible = true;
                    pan_List.Visible = false;
                    PromoId = id;
                    lab_VoucherEventDescription.Text = img_EventImage.ImageUrl = string.Empty;
                    VourcherPromo promo = VourcherFacade.VourcherPromoGet(PromoId);
                    if (promo.Id != 0)
                    {
                        lab_Id.Text = promo.Id.ToString();
                        ddl_PromoType.SelectedValue = promo.Type.ToString();
                        if (promo.Type == (int)VourcherPromoType.TagCategory)
                        {
                            CityCategoryModel city_category = JsonConvert.DeserializeObject<CityCategoryModel>(promo.Content);
                            SelectedCategory = city_category.CategoryList;
                            SelectedCity = city_category.CityList;
                        }
                        else
                        {
                            tbx_Content.Text = promo.Content;
                            tbx_link.Text = promo.Link;
                        }
                        tbx_Rank.Text = promo.Rank.ToString();
                        tbx_Tag.Text = promo.Tag;
                        tbx_VoucherId.Text = promo.EventId.ToString();

                        ViewVourcherSeller voucher = VourcherFacade.ViewVourcherSellerGetById(promo.EventId ?? 0);
                        if (voucher.Id != 0)
                        {
                            img_EventImage.ImageUrl = ImageFacade.GetMediaPathsFromRawData(voucher.PicUrl, MediaType.PponDealPhoto).DefaultIfEmpty(string.Empty).First();
                            lab_VoucherEventDescription.Text = voucher.SellerName + " : " + voucher.Contents;
                        }

                        tbx_StartDate.Text = promo.StartDate.HasValue ? promo.StartDate.Value.ToString("MM/dd/yyyy") : string.Empty;
                        ddl_StartDate.Text = promo.StartDate.HasValue ? promo.StartDate.Value.Hour.ToString() : "0";
                        tbx_EndDate.Text = promo.EndDate.HasValue ? promo.EndDate.Value.ToString("MM/dd/yyyy") : string.Empty;
                        ddl_EndDate.Text = promo.EndDate.HasValue ? promo.EndDate.Value.Hour.ToString() : "0";
                        btn_Confirm.Text = "更新";
                        DDL_Change(sender, e);
                        DDL_ListChange(sender, e);
                    }
                    else
                    {
                        lab_Message.Text = "查無此優惠券，請輸入正確的優惠券編號";
                    }
                }
            }
        }

        protected void ReturnList(object sender, EventArgs e)
        {
            pan_Detail.Visible = false;
            pan_List.Visible = true;
        }

        protected void RefreshPromo(object sender, EventArgs e)
        {
            WebRequest request = WebRequest.Create("http://discount.17life.com/api/refreshpromo/70553216/6");
            request.Method = "POST";
            request.ContentLength = 0;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (Stream dataStream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(dataStream))
                    {
                        lab_RefreshMessage.Text = reader.ReadToEnd();
                    }
                }
            }
        }
    }
}

public class CityCategoryModel
{
    public List<int> CityList { get; set; }
    public List<int> CategoryList { get; set; }
}
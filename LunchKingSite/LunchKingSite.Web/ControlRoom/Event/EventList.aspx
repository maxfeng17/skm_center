﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="EventList.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Event.EventList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div><asp:HyperLink ID="NewEvent" runat="server" NavigateUrl="eventeditor.aspx">新增活動</asp:HyperLink></div>
        <asp:GridView ID="Gv" runat="server" AutoGenerateColumns="False" 
        AllowPaging="True" EmptyDataText="無符合的資料" 
        OnRowDataBound="Gv_RowDataBound" PageSize="20" OnDataBound="Gv_DataBound">
            <Columns>
                <asp:BoundField AccessibleHeaderText="狀態" DataField="Enable" HeaderText="狀態" />
                <asp:BoundField AccessibleHeaderText="類別" DataField="Type" HeaderText="類別" />
                <asp:BoundField AccessibleHeaderText="活動網址" DataField="Url" HeaderText="活動網址" />
                <asp:HyperLinkField AccessibleHeaderText="活動名稱" DataNavigateUrlFields="Guid" 
                    DataNavigateUrlFormatString="eventeditor.aspx?eid={0}" DataTextField="Title" 
                    HeaderText="活動名稱" Target="_self" Text="活動名稱" />
                <asp:BoundField AccessibleHeaderText="起始時間" DataField="StartTime" 
                    HeaderText="起始時間" />
                <asp:BoundField AccessibleHeaderText="結束時間" DataField="EndTime" 
                    HeaderText="結束時間" />
                <asp:HyperLinkField AccessibleHeaderText="預覽" Text="預覽" DataNavigateUrlFields="Url"  
                    DataNavigateUrlFormatString="~/ppon/event/{0}"  HeaderText="預覽" Target="_blank"  />
                <asp:HyperLinkField AccessibleHeaderText="預覽EDM" Text="預覽EDM" DataNavigateUrlFields="Guid"  
                    DataNavigateUrlFormatString="eventedmpreview.aspx?eid={0}"  HeaderText="預覽EDM" Target="_blank"  />
            </Columns>
                    <pagerstyle forecolor="Blue"
          backcolor="LightBlue"/>
              
        <pagertemplate>
          <table width="100%">                    
            <tr>                        
              <td width="70%">
                <asp:label id="MessageLabel" forecolor="Blue" text="換頁:" runat="server"/>
                <asp:dropdownlist id="PageDropDownList" autopostback="true" onselectedindexchanged="Gv_SelectedIndexChanged" runat="server"/>
              </td>   
              <td width="70%" align="right">
                <asp:label id="CurrentPageLabel" forecolor="Blue" runat="server"/>
              </td>
                                            
            </tr>                    
          </table>
          
        </pagertemplate> 
        </asp:GridView>
</asp:Content>

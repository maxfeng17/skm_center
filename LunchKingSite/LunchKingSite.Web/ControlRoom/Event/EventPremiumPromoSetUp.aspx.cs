﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using LunchKingSite.Web.Piinlife;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
namespace LunchKingSite.Web.ControlRoom.Event
{
    public partial class EventPremiumPromoSetUp : RolePage, IEventPremiumPromoSetUp
    {

        public event EventHandler ChangeMode;
        public event EventHandler<DataEventArgs<EventPremiumPromo>> SaveEventPremiumPromo;
        public event EventHandler GetEventPremiumPromo;
        public event EventHandler UpdateEventPremiumPromoStatus;

        private EventPremiumPromoSetUpPresenter _presenter;
        public EventPremiumPromoSetUpPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        //0 主檔列表，1 主檔新增, 2 主檔修改, --3 子檔列表, 4 子檔新增, 5 子檔修改--
        public int Mode
        {
            get
            {
                int mode;
                return int.TryParse(hif_Mode.Value, out mode) ? mode : 0;
            }
            set
            {
                hif_Mode.Value = value.ToString();
                if (ChangeMode != null)
                {
                    EventArgs e = new EventArgs();
                    ChangeMode(this, e);
                }
            }
        }

        public string UserName
        {
            get { return User.Identity.Name; }
        }

        public int EventPremiumId
        {
            get
            {
                int eventpremiumid;
                return int.TryParse(hif_EventPremiumId.Value, out eventpremiumid) ? eventpremiumid : 0;
            }
            set
            {
                hif_EventPremiumId.Value = value.ToString();
            }
        }

        public bool IsLimitInThreeMonth
        {
            get
            {
                return cbx_LimitInThreeMonth.Checked;
            }
        }

        public string SiteUrl
        {
            get { return ProviderFactory.Instance().GetConfig().SiteUrl; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
                InitialControls();
            }
            this.Presenter.OnViewLoaded();

        }

        private void InitialControls()
        {
            ddlTemplateType.DataSource = Enum.GetValues(typeof(EventPremiumPromoType)).Cast<EventPremiumPromoType>().Select(x => new ListItem { Value = ((int)x).ToString(), Text = Helper.GetLocalizedEnum(x) });
            ddlTemplateType.DataValueField = "Value";
            ddlTemplateType.DataTextField = "Text";
            ddlTemplateType.DataBind();

        }

        protected void AddEventPremiumPromo(object sender, CommandEventArgs e)
        {
            int mode;
            if (int.TryParse(e.CommandArgument.ToString(), out mode))
            {
                Mode = mode;
            }
        }

        protected void SavePremiumPromo(object sender, EventArgs e)
        {
            lab_Message.Text = string.Empty;
            DateTime d_start, d_end;
            if (DateTime.TryParse(tbx_Start.Text, out d_start) && DateTime.TryParse(tbx_End.Text, out d_end))
            {
                if (string.IsNullOrEmpty(tbx_BgColor.Text) && string.IsNullOrEmpty(tbx_BgPic.Text))
                {
                    ScriptManager.RegisterStartupScript(this, typeof (Button), "Html",
                                                        "$('#" + tbx_Html.ClientID + "').ckeditor();", true);
                    ScriptManager.RegisterStartupScript(this, typeof (Button), "MainPic",
                                                        "$('#" + txtMainPic.ClientID + "').ckeditor();", true);
                    ScriptManager.RegisterStartupScript(this, typeof(Button), "ApplySuccessContent", 
                                                        "$('#" + tbx_ApplySuccessContent.ClientID + "').ckeditor();", true);


                    tbx_Html.Text = HttpUtility.HtmlDecode(tbx_Html.Text);
                    txtMainPic.Text = HttpUtility.HtmlDecode(txtMainPic.Text);

                    tbx_ApplySuccessContent.Text = HttpUtility.HtmlDecode(tbx_ApplySuccessContent.Text); ;
                    

                    lab_Message.Text = "請填入背景圖或背景色";
                }
                else
                {
                    EventPremiumPromo premiumPromo = new EventPremiumPromo();
                    if (Mode == 2)
                    {
                        premiumPromo.Id = EventPremiumId;
                    }

                    premiumPromo.Title = tbx_EventName.Text;
                    premiumPromo.Url = tbx_Url.Text;
                    premiumPromo.Cpa = tbx_Cpa.Text;
                    premiumPromo.PremiumName = tbx_PremiumName.Text;
                    premiumPromo.StartDate = d_start;
                    premiumPromo.EndDate = d_end;
                    
                    int type = int.TryParse(ddlTemplateType.SelectedValue, out type)? type : (int)EventPremiumPromoType.Basic;
                    premiumPromo.TemplateType = type;
                    
                    int premiumamount = int.TryParse(tbx_PremiumsAmount.Text, out premiumamount)?premiumamount:default(int);
                    premiumPromo.PremiumAmount = premiumamount;
                    
                    premiumPromo.MainBanner = HttpUtility.HtmlDecode(txtMainPic.Text);
                    premiumPromo.BackgroundPicture = tbx_BgPic.Text;
                    premiumPromo.BackgroundColor = tbx_BgColor.Text.Replace("#", string.Empty);
                    premiumPromo.Description = HttpUtility.HtmlDecode(tbx_Html.Text);
                    premiumPromo.Status = false;
                    premiumPromo.CreateUniqueId = MemberFacade.GetUniqueId(UserName);

                    premiumPromo.BannerFontColor = (string.IsNullOrEmpty(tbx_BannerFontColor.Text))? null :tbx_BannerFontColor.Text.Replace("#", string.Empty);
                    premiumPromo.ButtonBackgroundColor = (string.IsNullOrEmpty(tbx_BtnBackgroundColor.Text)) ? null : tbx_BtnBackgroundColor.Text.Replace("#", string.Empty);
                    premiumPromo.ButtonFontColor = (string.IsNullOrEmpty(tbx_BtnFontColor.Text)) ? null : tbx_BtnFontColor.Text.Replace("#", string.Empty);
                    premiumPromo.ButtonBackgroundHoverColor = (string.IsNullOrEmpty(tbx_BtnBackgroundHoverColor.Text)) ? null : tbx_BtnBackgroundHoverColor.Text.Replace("#", string.Empty);

                    premiumPromo.ApplySuccessContent = HttpUtility.HtmlDecode(tbx_ApplySuccessContent.Text);

                    if (SaveEventPremiumPromo != null)
                    {
                        SaveEventPremiumPromo(sender, new DataEventArgs<EventPremiumPromo>(premiumPromo));
                    }
                }
            }
            else
            {
                lab_Message.Text = "日期格式錯誤";
            }
        }
        public void ChangePanel(int mode, string return_message = "")
        {
            //lab_Message.Text = string.Empty;
            pl_PremiumEventEdit.Visible = pl_EventPremiumList.Visible  = false;
            switch (mode)
            {
                case 0:
                    pl_EventPremiumList.Visible = true;
                    btn_Save.Text = "新增";
                    lab_Message.Text = return_message;
                    break;
                case 1:
                    pl_PremiumEventEdit.Visible = true;
                    
                    ScriptManager.RegisterStartupScript(this, typeof(Button), "Html", "$('#" + tbx_Html.ClientID + "').ckeditor();", true);
                    ScriptManager.RegisterStartupScript(this, typeof(Button), "MainPic", "$('#" + txtMainPic.ClientID + "').ckeditor();", true);
                    ScriptManager.RegisterStartupScript(this, typeof(Button), "ApplySuccessContent", "$('#" + tbx_ApplySuccessContent.ClientID + "').ckeditor();", true);

                    tbx_EventName.Text = tbx_Url.Text = tbx_Cpa.Text = tbx_Start.Text
                    = tbx_End.Text = txtMainPic.Text  = tbx_BgPic.Text = tbx_BgColor.Text 
                    = tbx_PremiumsAmount.Text=tbx_ApplySuccessContent.Text 
                    //= tbx_BtnFontColor.Text=tbx_loginBtnOriginal.Text=tbx_loginBtnAvtive.Text
                    //= tbx_submitBtnOriginal.Text=tbx_submitBtnAvtive.Text
                    = tbx_Html.Text = string.Empty;
                    break;
                case 2:
                    ScriptManager.RegisterStartupScript(this, typeof(Button), "Html", "$('#" + tbx_Html.ClientID + "').ckeditor();", true);
                    ScriptManager.RegisterStartupScript(this, typeof(Button), "MainPic", "$('#" + txtMainPic.ClientID + "').ckeditor();", true);
                    ScriptManager.RegisterStartupScript(this, typeof(Button), "ApplySuccessContent", "$('#" + tbx_ApplySuccessContent.ClientID + "').ckeditor();", true);

                    pl_PremiumEventEdit.Visible = true;
                    btn_Save.Text = "更新";
                    break;
                case 3:
                    //pan_ItemList.Visible = true;
                    break;
                case 4:
                    //pan_ItemEdit.Visible = true;
                    //btn_ItemSave.Text = "新增";
                    //tbx_Seq.Text = tbx_ItemTitle.Text = tbx_ItemDescription.Text = lab_ItemStatus.Text = tbx_ItemId.Text = string.Empty;
                    break;
                case 5:
                    //pan_ItemEdit.Visible = true;
                    //btn_ItemSave.Text = "更新";
                    break;
            }
        }

        public void SetEventPremiumPromoList(EventPremiumPromoCollection premiumPromos)
        {
            gv_EventPremiumPromo.DataSource = premiumPromos.OrderByDescending(x => x.Id);
            gv_EventPremiumPromo.DataBind();
        }

        protected void gvEventPremiumPromoCommand(object sender, GridViewCommandEventArgs e)
        {
            lab_Message.Text = string.Empty;
            int id;
            if (int.TryParse(e.CommandArgument.ToString(), out id))
            {
                EventPremiumId = id;
                switch (e.CommandName)
                {
                    case "EditEvent":
                        if (GetEventPremiumPromo != null)
                            GetEventPremiumPromo(sender, e);
                        break;
                    case "ChangeStatus":
                        if (UpdateEventPremiumPromoStatus != null)
                        {
                            UpdateEventPremiumPromoStatus(sender, e);
                        }
                        break;
                    case "ShowItemList":
                        //if (GetEventPromoItemList != null)
                        //    GetEventPromoItemList(sender, e);
                        break;
                }
            }
            else
                lab_Message.Text = "Id錯誤";
        }

        protected void gv_EventPremiumPromo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is EventPremiumPromo)
            {
                EventPremiumPromo premiumPromo = (EventPremiumPromo)e.Row.DataItem;
                Label lblLink = (Label)e.Row.FindControl("lblLink");
                HyperLink lkPreview = (HyperLink)e.Row.FindControl("lkPreview");
                //CheckBox checkBoxShowInApp = (CheckBox)e.Row.FindControl("ShowInApp");
                //CheckBox checkBoxShowInWeb = (CheckBox)e.Row.FindControl("ShowInWeb");

                //checkBoxShowInApp.InputAttributes.Add("value", premiumsPromo.Id.ToString());
                //checkBoxShowInWeb.InputAttributes.Add("value", premiumsPromo.Id.ToString());

                string pageName = string.Empty;
                switch ((EventPremiumPromoType)premiumPromo.TemplateType)
                {
                    case EventPremiumPromoType.Basic:
                        pageName = "PremiumPromo";
                        break;
                    default:
                        pageName = "PremiumPromo";
                        break;
                }
                lblLink.Text = SiteUrl + "/Event/" + pageName + ".aspx?u=" + premiumPromo.Url + (!string.IsNullOrEmpty(premiumPromo.Cpa) ? ("&rsrc=" + premiumPromo.Cpa) : string.Empty);
                lkPreview.NavigateUrl = SiteUrl + "/Event/" + pageName + ".aspx?u=" + premiumPromo.Url + "&p=show_me_the_preview";

            }
        }

        public void SetEventPremiumPromo(EventPremiumPromo premiumPromo)
        {
            Mode = 2;
            tbx_EventName.Text = premiumPromo.Title;
            tbx_Url.Text = premiumPromo.Url;
            tbx_Cpa.Text = premiumPromo.Cpa;
            tbx_PremiumName.Text = premiumPromo.PremiumName;
            tbx_Start.Text = premiumPromo.StartDate.ToString("MM/dd/yyyy");
            tbx_End.Text = premiumPromo.EndDate.ToString("MM/dd/yyyy");

            tbx_PremiumsAmount.Text = premiumPromo.PremiumAmount.ToString();
            ddlTemplateType.SelectedValue = premiumPromo.TemplateType.ToString();
            txtMainPic.Text = premiumPromo.MainBanner;

            tbx_BgPic.Text = premiumPromo.BackgroundPicture;
            tbx_BgColor.Text = premiumPromo.BackgroundColor;
            tbx_Html.Text = premiumPromo.Description;

            tbx_BannerFontColor.Text = premiumPromo.BannerFontColor;
            tbx_BtnBackgroundColor.Text = premiumPromo.ButtonBackgroundColor;
            tbx_BtnBackgroundHoverColor.Text = premiumPromo.ButtonBackgroundHoverColor;
            tbx_BtnFontColor.Text = premiumPromo.ButtonFontColor;

            tbx_ApplySuccessContent.Text = premiumPromo.ApplySuccessContent;

        }


    }
}

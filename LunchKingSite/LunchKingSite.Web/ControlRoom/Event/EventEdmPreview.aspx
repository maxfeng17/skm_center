﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="EventEdmPreview.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Event.EventEdmPreview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
<script type="text/javascript">
    google.load("jquery", "1.5.2");
    google.load("jqueryui", "1.8.13"); 
</script>
<script type="text/javascript">
    function view() {
        
        window.open('../../service/eventdm.ashx?eid=09e06c80-6ac6-4ef2-b1b4-85359a203939&d1=6b613af1-00d0-48dc-9c00-58d6e1f06854&d2=d230b925-b1be-4f30-8c6e-3322f052ae95&d3=6b613af1-00d0-48dc-9c00-58d6e1f06854', '_blank');
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <CompositeScript ScriptMode="Release">
            <Scripts>
            </Scripts>
        </CompositeScript>
    </asp:ScriptManagerProxy>
<asp:HiddenField ID="eid" runat="server" />
<fieldset id="Fieldset1">
<legend><b>轉成EDM</b></legend>
<div>選擇區域 <asp:DropDownList ID="ddl_EdmCity" runat="server" Style="font-size: 12px" name="City" size="1" OnSelectedIndexChanged="Index_Changed" AutoPostBack="true"></asp:DropDownList></div>
<asp:panel id="pDeal" runat="server" Visible="false">
Deal 1<asp:DropDownList ID="ddlDeal1" runat="server"></asp:DropDownList><br />
Deal 2<asp:DropDownList ID="ddlDeal2" runat="server"></asp:DropDownList><br />
Deal 3<asp:DropDownList ID="ddlDeal3" runat="server"></asp:DropDownList><br />
<asp:Button ID="btn_pv" runat="server" Text="預覽EDM" onclick="btn_pv_Click" /> </asp:panel>
</fieldset>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="EventEditor.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Event.EventEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
<script type="text/javascript" src="//www.google.com/jsapi"></script>
<script type="text/javascript" src="../../Tools/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="../../Tools/js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="../../Tools/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="../../Tools/js/jquery.maxlength-min.js"></script>
<script type="text/javascript" src="../../Tools/js/jquery.tooltip.min.js"></script>
<script type="text/javascript" src="../../Tools/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="../../Tools/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../../Tools/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
    function $$(id, context) {
        var el = $("#" + id, context);
        if (el.length < 1)
            el = $("*[id$=" + id + "]", context);
        return el; 
    }

    function Check_RB() {
        if ($("input:radio:checked").val() == 'RB2') {
            $('#EmailEnter').show();
        } else {
            $('#EmailEnter').hide();
        }
    }

    function updateFB() {
        $('.MaName').text($$('fbShareCaption').val());
        $('.Madescription').text($$('fbShareDesc').val());
        $('#FBPic').attr('src', $$('fbShareImg').val()); 
    }

    $(document).ready(function () {
        Check_RB();
        updateFB();
        $$('tbOS').datetimepicker({ dateFormat: 'yy-mm-dd', stepMinute: 10 });
        $$('tbOE').datetimepicker({ dateFormat: 'yy-mm-dd', stepMinute: 10 });
        $('#set1 *').tooltip();
        $('#set2 *').tooltip();
        $$('EventTitle').maxlength({ maxCharacters: 50, slider: true });
        $$('Lnk').maxlength({ maxCharacters: 50, slider: true });
        $$('EvnetTitle').maxlength({ maxCharacters: 50, slider: true });
        $$('TBtxtImg').maxlength({ maxCharacters: 150, slider: true });
        $$('TBtxtLink').maxlength({ maxCharacters: 150, slider: true });
        $$('TBviewImg').maxlength({ maxCharacters: 150, slider: true });
        $$('TBViewLink').maxlength({ maxCharacters: 150, slider: true });
        $$('TBbtnImg').maxlength({ maxCharacters: 150, slider: true });
        $$('TBbtnLink').maxlength({ maxCharacters: 150, slider: true });
        $$('TBmailImg').maxlength({ maxCharacters: 150, slider: true });
        $$('TBmailLink').maxlength({ maxCharacters: 150, slider: true });
        $$('TBlike').maxlength({ maxCharacters: 12, slider: true });
        $$('TBshare').maxlength({ maxCharacters: 12, slider: true });
        $$('TBsend').maxlength({ maxCharacters: 12, slider: true });
        $$('fbShareCaption').maxlength({ maxCharacters: 50, slider: true });
        $$('fbShareDesc').maxlength({ maxCharacters: 300, slider: true });
        $$('fbShareImg').maxlength({ maxCharacters: 150, slider: true });
        $$('FBurl').maxlength({ maxCharacters: 200, slider: true });
        $$('EventRule').ckeditor();
        $$('aspnetForm').validate();
    });
</script>
<link type="text/css" href="../../Tools/js/css/jquery.tooltip.css" rel="stylesheet" />	
<link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8.13.custom.css" rel="stylesheet" />
<style type="text/css">
body { font-size: 10px; }
.ui-timepicker-div .ui-widget-header{ margin-bottom: 8px; }
.ui-timepicker-div dl{ text-align: left; }
.ui-timepicker-div dl dt{ height: 25px; }
.ui-timepicker-div dl dd{ margin: -25px 0 10px 65px; }
.ui-timepicker-div td { font-size: 90%; }
label.error { float: none; color: red; padding-left: .5em; vertical-align: top; }

.MarketingTable{
	background-color:#F9F9FB;
	border: 1px solid #6A76AE;
	height: 190px;
	width: 500px;
}
.MarketingMugShot{
	width:50px;
	height:50px;
	margin:7px auto;
}
.MarketingInfo {
	height: 100px;
	clear: both;
	margin-top: 7px;
}
.MarketingPic {
	float: left;
	height: 100px;
	width: 100px;
}
.MaName {
	font-family: "微軟正黑體" Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #414B74;
	float: left;
	margin-left: 10px;
	width: 290px;
	font-weight: bold;
	letter-spacing: 0.1em;
	text-align: left;
	overflow: hidden;
}
.MaWebsite {
	font-family: "微軟正黑體" Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #6C6C6C;
	margin-left: 10px;
	float: left;
	margin-top: 23px;
	height: 16px;
	letter-spacing: 0.1em;
	width: 290px;
	text-align: left;
	overflow: hidden;
}
.Madescription {
	font-family: "微軟正黑體" Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #6C6C6C;
	height: 16px;
	width: 290px;
	margin-top: 7px;
	margin-left: 10px;
	float: left;
	letter-spacing: 0.1em;
	text-align: left;
	overflow: hidden;
}
.MaNotice {
	font-family: "微軟正黑體" Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #6C6C6C;
	margin-top: 7px;
	letter-spacing: 0.1em;
	padding-right: 5px;
	padding-left: 5px;
}
.MaText {
	width:400px;
	height:40px;
	text-align: left;
	overflow: hidden;
	font-family: "微軟正黑體";
	font-size: 14px;
	color: #666;
	letter-spacing: 0.1em;
	background-color: #FFF;
	line-height: 18px;
	border: 1px solid #9A9A9A;
	padding-top: 2px;
	padding-right: 5px;
	padding-bottom: 2px;
	padding-left: 5px;
}


</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="eid" runat="server" />
    <asp:HiddenField ID="rid" runat="server" />
    <asp:HiddenField ID="hdActivityId" runat="server" />
<asp:RadioButton ID="RB_On" runat="server" Text="啟動" GroupName="Enable" /> 
<asp:RadioButton ID="RB_Off" runat="server" Text="停止" GroupName="Enable" />
<div style="float:right"><a href="EventList.aspx">回上頁</a></div>
<fieldset id="set1" >
<legend><b>一、活動設定(必填)</b></legend>
<div id="RB_Group" onclick="Check_RB()">活動類別：
<asp:RadioButton ID="RB1" runat="server" Text="單純活動" title="無任何功能的單純活動頁。" GroupName="RB" checked="true"/> 
<asp:RadioButton ID="RB2" runat="server" Text="活動+填寫email" title="使用者必須填寫個人e-mail才能參加活動。" GroupName="RB"/> 
<asp:RadioButton ID="RB3" runat="server" Text="活動+邀請連結" title="使用者參加活動需登入，可拿到個人專屬連結分享給朋友。" GroupName="RB"/>
</div>
<div>活動名稱：<asp:TextBox ID="EventTitle" runat="server" MaxLength="50" Width="300px" class="required"></asp:TextBox></div>
<div>自訂網址：<%=SystemConfig.SiteUrl %>/event/ <asp:TextBox ID="Lnk" runat="server" MaxLength="30" Width="200px" class="required"></asp:TextBox></div>
<div>從 <asp:TextBox ID="tbOS" runat="server" class="required"></asp:TextBox> 至 <asp:TextBox ID="tbOE" runat="server" class="required"></asp:TextBox></div>
</fieldset>
&nbsp;
<fieldset id="set2">
<legend><b>二、圖片編輯</b></legend>
請先上傳圖素，再將網址複製貼於下方圖素位置<br />
<u>主題文字</u><br />
<div>圖素位置：<asp:TextBox ID="TBtxtImg" runat="server" MaxLength="150" Width="400px" title="請記得打http://"></asp:TextBox></div>
<div>連結設定：<asp:TextBox ID="TBtxtLink" runat="server" MaxLength="150" Width="400px" title="請記得打http://，無連結可空白"></asp:TextBox></div>
<u>主視覺</u><br />
<div>圖素位置：<asp:TextBox ID="TBviewImg" runat="server" MaxLength="150" Width="400px" title="請記得打http://"></asp:TextBox></div>
<div>連結設定：<asp:TextBox ID="TBViewLink" runat="server" MaxLength="150" Width="400px" title="請記得打http://，無連結可空白"></asp:TextBox></div>
<u>活動button</u><br />
<div>圖素位置：<asp:TextBox ID="TBbtnImg" runat="server" MaxLength="150" Width="400px" title="請記得打http://"></asp:TextBox></div>
<div>連結設定：<asp:TextBox ID="TBbtnLink" runat="server" MaxLength="150" Width="400px" title="請記得打http://"></asp:TextBox></div>
<div id="EmailEnter"><u>email輸入</u><br />
<div>圖素位置：<asp:TextBox ID="TBmailImg" runat="server" MaxLength="150" Width="400px" title="請記得打http://" value="https://www.17life.com/Themes/default/images/17Life/Gactivities/MarketingPicEmailHint.png"></asp:TextBox></div>
<div style="display:none">連結設定：<asp:TextBox ID="TBmailLink" runat="server" MaxLength="150" Width="400px" title="設這個沒用啊啊啊"></asp:TextBox></div></div>
</fieldset>
&nbsp;
<fieldset id="set3">
<legend><b>三、Facebook工具列編輯</b></legend>
<div> <asp:CheckBox ID="ChkLike" runat="server" Text="顯示" /> - Facebook 讚：<asp:TextBox ID="TBlike" runat="server" MaxLength="12" Width="150px" ></asp:TextBox> </div>
<div> <asp:CheckBox ID="ChkShare" runat="server" Text="顯示" /> - Facebook 分享：<asp:TextBox ID="TBshare" runat="server" MaxLength="12" Width="150px" ></asp:TextBox> </div>
<div> <asp:CheckBox ID="ChkSend" runat="server" Text="顯示" /> - Facebook 傳送：<asp:TextBox ID="TBsend" runat="server" MaxLength="12" Width="150px" ></asp:TextBox> </div>
 <b>編輯分享內容</b><br />
 <table>
    <tr>
        <td>
<div>Name：<asp:TextBox ID="fbShareCaption" runat="server" MaxLength="50" Width="320px" onFocusOut="updateFB()" ></asp:TextBox></div>
<div>Description：<asp:TextBox ID="fbShareDesc" runat="server" MaxLength="300" Width="300px" Rows="4" Height="100px" TextMode="MultiLine" onFocusOut="updateFB()"></asp:TextBox></div>
<div>Picture：<asp:TextBox ID="fbShareImg" runat="server" MaxLength="150" Width="325px" onFocusOut="updateFB()"></asp:TextBox></div>
<div>Url：<asp:TextBox ID="FBurl" runat="server" MaxLength="200" Width="325px" title="僅影響share與send"></asp:TextBox></div>
        </td>
        <td>
<table  border="0" cellspacing="0" cellpadding="0" class="MarketingTable">
  <tr>
    <td width="80" ><div class="MarketingMugShot"><img src="../../Themes/default/images/17Life/Gactivities/MarketingPic2.jpg" width="50" height="50" alt=""/></div></td>
    <td width="430"><div class="MaText"></div></td>
  </tr>
    <tr>
    <td width="80"></td>
    <td width="430">
      <div class="MarketingInfo">
        <div class="MarketingPic">
          <img id="FBPic" src="../../Themes/default/images/17Life/Gactivities/MarketingPic.jpg" width="100" height="100" alt="" />
        </div>
        <div class="MaName">Name</div>
        <div class="MaWebsite">www.17life.com</div>
        <div class="Madescription"> description</div>
      </div>
      <div class="MaNotice"><p><span style="font-size:9px; margin-right:3px;">》</span>傳自17life最近活動公告</p></div></td>
  </tr>
</table>        
        </td>
    </tr>
 </table>





</fieldset>
&nbsp;
<fieldset id="set4">
<legend><b>四、活動辦法</b></legend>
<asp:TextBox ID="EventRule" runat="server" Width="800px" TextMode="MultiLine" Rows="10"></asp:TextBox>
</fieldset>
&nbsp;
<fieldset id="set5">
<legend><b>五、得獎名單</b></legend>
<div><asp:CheckBox ID="ChkReward" runat="server" Text="顯示" Checked="true"/>&nbsp;&nbsp;&nbsp;
<asp:Button ID="btnExport" runat="server" Text="匯出參加名單" onclick="btnExport_Click" />
    </div>
<div><asp:TextBox ID="TBReward" runat="server" Width="800px" TextMode="MultiLine" Rows="10"></asp:TextBox></div>
</fieldset>
&nbsp;
<fieldset id="Fieldset1">
<legend><b>雜七雜八</b></legend>
CPA : <asp:TextBox ID="CPAurl" runat="server" MaxLength="50" Width="100px" Title="不用打&rsrc="></asp:TextBox><br>
SEO description : <asp:TextBox ID="tbSeoD" runat="server" MaxLength="50" Width="600px" ></asp:TextBox><br>
SEO KeyWords : <asp:TextBox ID="tbSeoK" runat="server" MaxLength="50" Width="600px" ></asp:TextBox><br>
</fieldset>
 &nbsp; <div style="float:right">    <asp:Button ID="btn_save" runat="server" Text="儲存" onclick="BtnSaveClick" /></div>
</asp:Content>

﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.Web.ControlRoom.Event
{
    public partial class WeChatPromoSetUp : RolePage, IEventPromoSetUp
    {
        public bool IsCurationFunction
        {
            get { return false; }
        }
        #region event
        public event EventHandler<DataEventArgs<EventPromoUpdateModel>> SaveEventPromo;
        public event EventHandler ChangeMode;
        public event EventHandler GetEventPromo;
        public event EventHandler UpdateEventPromoStatus;
        public event EventHandler GetEventPromoItemList;
        public event EventHandler UpdateEventPromoItemStatus;
        public event EventHandler<DataEventArgs<KeyValuePair<int, EventPromoItemType>>> GetPponDeal;
        public event EventHandler<DataEventArgs<EventPromoItemUpdateMode>> SaveEventPromoItem;     
        public event EventHandler GetEventPromoItem;
        public event EventHandler<DataEventArgs<string>> WeChatSendTestMessage;
        public event EventHandler GetEventPromoList;
        public event EventHandler<DataEventArgs<bool>> SetEventPromoShowInApp;
        public event EventHandler<DataEventArgs<bool>> SetEventPromoShowInWeb;        
        public event EventHandler<DataEventArgs<List<Tuple<string, string, string>>>> ImportBidList;        
        #pragma warning disable
        public event EventHandler<DataEventArgs<KeyValuePair<int, Dictionary<int, int>>>> UpdateEventPromoItemSeq;
        public event EventHandler<DataEventArgs<int>> DeleteEventPromoItem;
        public event GetEventPromoItemStatusHandler GetEventPromoItemStatus;
        #pragma warning restore
        #endregion
        #region property

        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public ISysConfProvider SystemConfig
        {
            get { return cp; }
        }


        private EventPromoSetUpPresenter _presenter;
        public EventPromoSetUpPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        //0 主檔列表，1 主檔新增, 2 主檔修改, 3 子檔列表, 4 子檔新增, 5 子檔修改
        public int Mode
        {
            get
            {
                int mode;
                return int.TryParse(hif_Mode.Value, out mode) ? mode : 0;
            }
            set
            {
                hif_Mode.Value = value.ToString();
                if (ChangeMode != null)
                {
                    EventArgs e = new EventArgs();
                    ChangeMode(this, e);
                }
            }
        }
        public int EventId
        {
            get
            {
                int eventid;
                return int.TryParse(hif_EventId.Value, out eventid) ? eventid : 0;
            }
            set
            {
                hif_EventId.Value = value.ToString();
            }
        }
        public int ItemId
        {
            get
            {
                int itemid;
                return int.TryParse(hif_ItemId.Value, out itemid) ? itemid : 0;
            }
            set
            {
                hif_ItemId.Value = value.ToString();
            }
        }
        public int ItemCount
        {
            get
            {
                int itemcount;
                return int.TryParse(hif_ItemCount.Value, out itemcount) ? itemcount : 0;
            }
            set
            {
                hif_ItemCount.Value = value.ToString();
            }
        }
        public int VourcherItemCount
        {
            get
            {
                int itemcount;
                return int.TryParse(hif_VourcherItemCount.Value, out itemcount) ? itemcount : 0;
            }
            set
            {
                hif_VourcherItemCount.Value = value.ToString();
            }
        }
        public int PiinlifeItemCount
        {
            get
            {
                int itemcount;
                return int.TryParse(hif_PiinlifeItemCount.Value, out itemcount) ? itemcount : 0;
            }
            set
            {
                hif_PiinlifeItemCount.Value = value.ToString();
            }
        }
        public string ItemTitle
        {
            set { tbx_ItemTitle.Text = value; }
        }
        public string ItemDescription
        {
            set { tbx_ItemDescription.Text = value; }
        }
        public string VourcherTitle { set; get; }
        public string VourcherDescription { set; get; }
        public string VourcherCategory { set; get; }
        public string VourcherSubCategory { set; get; }
        public string VourcherSeq { set; get; }
        public int VourcherEventPromoItemId { get; set; }
        public int VourcherEventPromoItemItemId { get; set; }
        public int VourcherPromoItemId { set; get; }
        public PromoSeoKeyword SeoKeyword { set; get; }

        public string UserName
        {
            get { return User.Identity.Name; }
        }

        public EventPromoType EventType
        {
            get
            {
                EventPromoType type = EventPromoType.WeChatPromoMessage;
                return type;
            }
        }
        public bool IsLimitInThreeMonth
        {
            get
            {
                return cbx_LimitInThreeMonth.Checked;
            }
        }
        public string ItemStatus { get; set; }
        public bool EventUseDiscount { get; set; }
        #endregion
        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            LogManager.GetLogger(LineAppender._LINE).Info("WeChatPromoSetUp");
            if (!Page.IsPostBack)
            {
                if (ImportBidList != null)
                { 
                //avoid building warning
                }
                this.Presenter.OnViewInitialized();
                InitialControls();
            }
            this.Presenter.OnViewLoaded();

        }

        protected void AddEventPromo(object sender, CommandEventArgs e)
        {
            int mode;
            if (int.TryParse(e.CommandArgument.ToString(), out mode))
                Mode = mode;
        }
        protected void SavePromo(object sender, EventArgs e)
        {
            lab_Message.Text = string.Empty;
            DateTime d_start, d_end;
            int eventpromotype;
            if (DateTime.TryParse(tbx_Start.Text, out d_start) && DateTime.TryParse(tbx_End.Text, out d_end) && int.TryParse(ddl_EventPromoType.SelectedValue, out eventpromotype))
            {
                ScriptManager.RegisterStartupScript(this, typeof(Button), "Html", "$('#" + tbx_Html.ClientID + "').ckeditor();", true);
                tbx_Html.Text = HttpUtility.HtmlDecode(tbx_Html.Text);
                EventPromo promo = new EventPromo();
                if (Mode == 2)
                    promo.Id = EventId;
                promo.Title = promo.Url = tbx_Url.Text;
                promo.StartDate = d_start;
                promo.EndDate = d_end;
                promo.MainPic = string.Empty;
                promo.Description = HttpUtility.HtmlDecode(tbx_Html.Text).Replace("<br />", Environment.NewLine);
                promo.Creator = UserName;
                promo.Cdt = DateTime.Now;
                promo.Status = false;
                promo.TemplateType = (int)EventPromoTemplateType.PponOnly;
                promo.Type = eventpromotype;
                if (SaveEventPromo != null)
                {
                    EventPromoUpdateModel model = new EventPromoUpdateModel();
                    model.MainEvent = promo;
                    SaveEventPromo(sender, new DataEventArgs<EventPromoUpdateModel>(model));
                }
            }
            else
                lab_Message.Text = "日期格式錯誤";
        }

        protected void gvEventPromoCommand(object sender, GridViewCommandEventArgs e)
        {
            lab_Message.Text = string.Empty;
            int id;
            if (int.TryParse(e.CommandArgument.ToString(), out id))
            {
                EventId = id;
                switch (e.CommandName)
                {
                    case "EditEvent":
                        if (GetEventPromo != null)
                            GetEventPromo(sender, e);
                        break;
                    case "ChangeStatus":
                        if (UpdateEventPromoStatus != null)
                            UpdateEventPromoStatus(sender, e);
                        break;
                    case "ShowItemList":
                        if (GetEventPromoItemList != null)
                            GetEventPromoItemList(sender, e);
                        break;
                    case "SendTestMessage":
                        if (string.IsNullOrEmpty(tbx_TestAccount.Text))
                        {
                            lab_Message.Text = "請輸入測試帳號";
                        }
                        else if (WeChatSendTestMessage != null)
                        {
                            WeChatSendTestMessage(sender, new DataEventArgs<string>(tbx_TestAccount.Text));
                        }
                        break;
                }
            }
            else
                lab_Message.Text = "Id錯誤";
        }
        protected void rptEventPromoItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            lab_Message.Text = string.Empty;
            int id;
            if (int.TryParse(e.CommandArgument.ToString(), out id))
            {
                ItemId = id;
                switch (e.CommandName)
                {
                    case "ChangeItemStatus":
                        if (UpdateEventPromoItemStatus != null)
                            UpdateEventPromoItemStatus(sender, e);
                        break;
                    case "EditItem":
                        if (GetEventPromoItem != null)
                            GetEventPromoItem(sender, e);
                        break;
                }
            }
            else
                lab_Message.Text = "Id錯誤";
        }

        protected void SavePromoItem(object sender, EventArgs e)
        {
            lab_Message.Text = string.Empty;
            int seq;

            if (int.TryParse(tbx_Seq.Text, out seq))
            {
                List<EventPromoItem> items = new List<EventPromoItem>();
                var item = new EventPromoItem();
                if (Mode == 5)
                    item.Id = ItemId;
                item.ItemId = 0;
                item.Title = tbx_ItemTitle.Text;
                item.Description = tbx_ItemDescription.Text;
                item.Seq = seq;
                item.Creator = UserName;
                item.Cdt = DateTime.Now;
                item.Status = true;
                item.ItemType = (int)EventPromoItemType.Ppon;
                item.ItemUrl = tbx_ItemUrl.Text;
                item.ItemPicUrl = tbx_ItemPicUrl.Text;
                items.Add(item);

                var mode = new EventPromoItemUpdateMode();
                mode.PromoItems = items;

                if (SaveEventPromoItem != null)
                    SaveEventPromoItem(sender, new DataEventArgs<EventPromoItemUpdateMode>(mode));
            }
            else
                lab_Message.Text = "ItemId格式或排序錯誤";
        }

        protected void LimitInThreeMonth_CheckedChanged(object sender, EventArgs e)
        {
            if (GetEventPromoList != null)
            {
                GetEventPromoList(sender, e);
            }
        }
        #endregion
        #region method
        public void ChangePanel(int mode, string return_message = "")
        {
            lab_Message.Text = string.Empty;
            pan_EventEdit.Visible = pan_EventList.Visible = pan_ItemList.Visible = pan_ItemEdit.Visible = false;
            switch (mode)
            {
                case 0:
                    pan_EventList.Visible = true;
                    btn_Save.Text = "新增";
                    lab_Message.Text = return_message;
                    break;
                case 1:
                    ScriptManager.RegisterStartupScript(this, typeof(Button), "Html", "$('#" + tbx_Html.ClientID + "').ckeditor();", true);
                    pan_EventEdit.Visible = true;
                    tbx_Url.Text = tbx_Start.Text
                    = tbx_End.Text
                    = tbx_Html.Text = string.Empty;
                    break;
                case 2:
                    ScriptManager.RegisterStartupScript(this, typeof(Button), "Html", "$('#" + tbx_Html.ClientID + "').ckeditor();", true);
                    pan_EventEdit.Visible = true;
                    btn_Save.Text = "更新";
                    break;
                case 3:
                    pan_ItemList.Visible = true;
                    break;
                case 4:
                    pan_ItemEdit.Visible = true;
                    btn_ItemSave.Text = "新增";
                    tbx_Seq.Text = tbx_ItemTitle.Text = tbx_ItemDescription.Text
                        = tbx_ItemPicUrl.Text = tbx_ItemUrl.Text = string.Empty;
                    break;
                case 5:
                    pan_ItemEdit.Visible = true;
                    btn_ItemSave.Text = "更新";
                    break;
            }
        }
        public void SetEventPromo(PromoSeoKeyword seo , EventPromo promo, bool enableBindingCategory = false)
        {
            
        }
        public void SetEventPromo(EventPromo promo, bool enableBindingCategory = false)
        {
            Mode = 2;
            tbx_Url.Text = promo.Url;
            tbx_Start.Text = promo.StartDate.ToString("MM/dd/yyyy");
            tbx_End.Text = promo.EndDate.ToString("MM/dd/yyyy");
            tbx_Html.Text = promo.Description;
            ddl_EventPromoType.SelectedValue = promo.Type.ToString();
        }
        public void SetEventPromoList(EventPromoCollection promos)
        {
            gv_EventPromo.DataSource = promos.OrderByDescending(x => x.Id);
            gv_EventPromo.DataBind();
        }
        public void SetEventPromoItemList(ViewEventPromoItemCollection items)
        {
            if (GetPponDeal != null && SetEventPromoShowInWeb != null && SetEventPromoShowInApp != null)
            { }
        }
        public void SeTEventPromoItemList(EventPromoItemCollection items)
        {
            ItemCount = items.Count;
            rpt_EventPromoItem.DataSource = items.OrderBy(x => x.Category).ThenBy(x => x.Seq);
            rpt_EventPromoItem.DataBind();
        }

        public void SetVotePicEventPromoItemList(ViewEventPromoItemPicVoteCollection items)
        {

        }

        public void SetVourcherEventPromoItemList(Dictionary<EventPromoItem, DateTime> items)
        {
        }
        public void SetPiinlifeEventPromoItemList(Dictionary<EventPromoItem, DateTime> items)
        {
        }
        public void SetEventPromoItem(DateTime dateStart)
        {
            tbx_Seq.Text = (ItemCount + 1).ToString();
        }
        public void SetEventPromoItem(EventPromoItem item, EventPromoItem linkItem, DateTime dateStart, DateTime linkItemDateStart, EventPromoTemplateType tempLateType)
        {
            Mode = 5;
            tbx_Seq.Text = item.Seq.ToString();
            tbx_ItemTitle.Text = item.Title;
            tbx_ItemDescription.Text = item.Description;
            tbx_ItemPicUrl.Text = item.ItemPicUrl;
            tbx_ItemUrl.Text = item.ItemUrl;

        }

        public void SetBindingCategory(CategoryCollection categories) { }
        public void SetVourcherEventPromoItem(DateTime dateStart) { }
        public void SetEventPromoCities(List<PponCity> getPponCities, ViewCmsRandomCollection viewCmsRandomGetCollectionByEventPromoId)
        {
            
        }
        public void AlertImportErrorMsg(string errorMsg) {
            //不實作
        }
        #endregion
        #region pricate method
        private void InitialControls()
        {
            ddl_EventPromoType.DataSource = Enum.GetValues(typeof(EventPromoType)).Cast<EventPromoType>().Where(x => x != EventPromoType.Ppon).Select(x => new ListItem { Value = ((int)x).ToString(), Text = Helper.GetLocalizedEnum(x) });
            ddl_EventPromoType.DataValueField = "Value";
            ddl_EventPromoType.DataTextField = "Text";
            ddl_EventPromoType.DataBind();
        }
        #endregion
    }
}
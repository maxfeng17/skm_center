﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;

namespace LunchKingSite.Web.ControlRoom.Event
{
    public partial class EventEditor : RolePage, IEventEditorView
    {
        public event EventHandler<DataEventArgs<EventContentSave>> SaveEvent;

        #region props

        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public ISysConfProvider SystemConfig
        {
            get { return cp; }
        }

        private EventEditorPresenter _presenter;
        public EventEditorPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string EventId
        {
            set { eid.Value = value; }
            get
            {
                if (string.IsNullOrEmpty(Request.QueryString["eid"]))
                    return "";
                else
                    return Request.QueryString["eid"];
            }
        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!_presenter.OnViewInitialized())
                    return;
            }
            _presenter.OnViewLoaded();
        }

        public EventContent GetEventFromInterface()
        {
            EventContent ec = new EventContent();
            if (eid.Value.Length > 1)
            {
                ec.Guid = Guid.Parse((eid.Value));
                ec.MarkOld();
            }
            else
            {
                ec.Guid = Guid.NewGuid();
            }
            int activityId;
            ec.Id = int.TryParse(hdActivityId.Value, out activityId) ? activityId : 0;
            ec.ButtomLink = TBbtnLink.Text;
            ec.ButtonImg = TBbtnImg.Text;
            ec.EmailImg = TBmailImg.Text;
            ec.EmailLink = TBmailLink.Text;

            ec.EndTime = DateTime.Parse(tbOE.Text);
            ec.StartTime = DateTime.Parse(tbOS.Text);
            ec.EventRule = HttpUtility.HtmlDecode(EventRule.Text);
            ec.FbLikeText = TBlike.Text;
            ec.FbSendText = TBsend.Text;
            ec.FbShareText = TBshare.Text;
            ec.FbShareCaption = fbShareCaption.Text;
            ec.FbShareDesc = fbShareDesc.Text;
            ec.FbShareImg = fbShareImg.Text;
            ec.FbEventUrl = FBurl.Text;
            ec.CPAurl = CPAurl.Text;
            ec.TextImg = TBtxtImg.Text;
            ec.TextLink = TBtxtLink.Text;
            ec.RewardList = TBReward.Text;
            ec.Title = EventTitle.Text;
            ec.Url = Lnk.Text;
            ec.ViewImg = TBviewImg.Text;
            ec.ViewLink = TBViewLink.Text;
            if(ChkLike.Checked) 
                ec.FbLikeEnable = 1;
            else
                ec.FbLikeEnable = 0;
            if(ChkShare.Checked)
                ec.FbShareEnable = 1;
            else
                ec.FbShareEnable = 0;
            if(ChkSend.Checked)
                ec.FbSendEnable = 1;
            else
                ec.FbSendEnable = 0;
            if (RB_On.Checked)
                ec.Enable = 1;
            else
                ec.Enable = 0;
            if (ChkReward.Checked)
                ec.RewardEnable = 1;
            else
                ec.RewardEnable = 0;
            if (RB2.Checked)
                ec.Type = 2;
            else if (RB3.Checked)
                ec.Type = 3;
            else
                ec.Type = 1;

            return ec;
        }

        public void LoadEvent(EventContent ec, PromoSeoKeyword seo)
        {
            tbSeoD.Text = seo.Description;
            tbSeoK.Text = seo.Keyword;
            hdActivityId.Value = ec.Id.ToString();

            eid.Value = ec.Guid.ToString();
            TBbtnLink.Text = ec.ButtomLink;
            TBbtnImg.Text = ec.ButtonImg;
            TBmailImg.Text = ec.EmailImg;
            TBmailLink.Text = ec.EmailLink;

            tbOE.Text = ec.EndTime.ToString();
            tbOS.Text = ec.StartTime.ToString();
            EventRule.Text = ec.EventRule;
            TBlike.Text = ec.FbLikeText;
            TBsend.Text = ec.FbSendText;
            TBshare.Text = ec.FbShareText;
            fbShareCaption.Text = ec.FbShareCaption;
            fbShareDesc.Text = ec.FbShareDesc;
            fbShareImg.Text = ec.FbShareImg;
            FBurl.Text = ec.FbEventUrl;
            CPAurl.Text = ec.CPAurl;
            TBtxtImg.Text = ec.TextImg;
            TBtxtLink.Text = ec.TextLink;
            TBReward.Text = ec.RewardList;
            EventTitle.Text = ec.Title;
            Lnk.Text = ec.Url;
            TBviewImg.Text = ec.ViewImg;
            TBViewLink.Text = ec.ViewLink;
            if (ec.FbLikeEnable == 1)
                ChkLike.Checked=true;
            else
                ChkLike.Checked=false;
            if (ec.FbShareEnable == 1)
                ChkShare.Checked=true;
            else
                ChkShare.Checked=false;
            if (ec.FbSendEnable == 1)
                ChkSend.Checked = true;
            else
                ChkSend.Checked = false;
            if (ec.Enable == 1)
            {
                RB_On.Checked = true;
                RB_Off.Checked = false;
            }
            else
            {
                RB_Off.Checked = true;
                RB_On.Checked = false;
            }
            if (ec.RewardEnable==1)
                ChkReward.Checked = true;
            else
                ChkReward.Checked = false;

            switch(ec.Type)
            {
                case 1:
                    RB1.Checked = true;
                    break;
                case 2:
                    RB2.Checked = true;
                    break;
                case 3:
                    RB3.Checked = true;
                    break;
            }

        }

        protected void BtnSaveClick(object sender, EventArgs e)
        {
            if (SaveEvent != null)
            {
                EventContentSave EventContentSave = new EventContentSave();
                EventContentSave.EventContent = GetEventFromInterface();
                EventContentSave.SeoDescription = tbSeoD.Text;
                EventContentSave.SeoKeyWords = tbSeoK.Text;
                EventContentSave.UserId = Page.User.Identity.Name;
                SaveEvent(this, new DataEventArgs<EventContentSave>(EventContentSave)); 
            }
                
        }

        protected void BackClick(object sender, EventArgs e)
        {
            Response.Redirect("EventList.aspx");
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = ProviderFactory.Instance().GetProvider<IEventProvider>().EmailGetListFromEventEmailList(EventId);
                if (dt.Rows.Count > 0)
                {
                    Export(EventTitle.Text+"參加名單.xls", dt, 20);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, typeof(Button), "alert", "alert('無資料資料。');", true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //ScriptManager.RegisterStartupScript(Page, typeof(Button), "alert", "alert('產出錯誤，請洽技術部');", true);
            }
        }

        public static void Export(string fileName, DataTable sdt, int rowPerPage)
        {
            if (sdt.Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("email"));
                dt.Columns.Add(new DataColumn("時間"));

                for (int i = 0; i < sdt.Rows.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = sdt.Rows[i]["email"].ToString();
                    dr[1] = sdt.Rows[i]["creattime"].ToString();
                    dt.Rows.Add(dr);
                }

                Workbook workbook = new HSSFWorkbook();

                // 新增試算表。
                Sheet sheet = workbook.CreateSheet("參加名單");
                sheet.SetColumnWidth(0, 15 * 256);
                sheet.SetColumnWidth(1, 25 * 256);//15 * 256);
                sheet.Footer.Center = "&P";


                Cell c = null;
                int rowCount = 0;
                int rowIndex = 0;
                foreach (DataRow row in dt.Rows)
                {
                    Row dataRow = sheet.CreateRow(rowIndex);
                    foreach (DataColumn column in dt.Columns)
                    {
                        c = dataRow.CreateCell(column.Ordinal);
                        c.SetCellValue(row[column].ToString());
                    }

                    rowIndex++;
                    rowCount++;
                }

                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode(fileName)));
                workbook.Write(HttpContext.Current.Response.OutputStream);
            }
        }

    }
}
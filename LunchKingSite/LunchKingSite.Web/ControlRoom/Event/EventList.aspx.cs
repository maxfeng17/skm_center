﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.ControlRoom.Event
{
    public partial class EventList : RolePage, IEventListView
    {
        public event EventHandler<DataEventArgs<int>> PageChange;

        #region props
        private EventListPresenter _presenter;
        public EventListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public int PageSize
        {
            get { return Gv.PageSize; }
            set { Gv.PageSize = value; }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                this.Presenter.OnViewInitialized();
            this.Presenter.OnViewLoaded();
        }

        public void SetEventList(EventContentCollection ecc)
        {
            Gv.DataSource = ecc;
            Gv.DataBind();
        }

        public void OnPageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            if (PageChange != null) PageChange(this, new DataEventArgs<int>(e.NewPageIndex));
        }

        public void Gv_RowDataBound(Object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[1].Text == "1")
                    e.Row.Cells[1].Text = "單純活動";
                if (e.Row.Cells[1].Text == "2")
                    e.Row.Cells[1].Text = "填寫信箱";
                if (e.Row.Cells[1].Text == "3")
                    e.Row.Cells[1].Text = "邀請連結";
                if (e.Row.Cells[0].Text=="1")
                    e.Row.Cells[0].Text = "On";
                else
                    e.Row.Cells[0].Text = "<span style='color:gray'>Off</span>";
            }

        }

        public void Gv_SelectedIndexChanged(Object sender, EventArgs e)
        {
            GridViewRow pagerRow = Gv.BottomPagerRow;
            DropDownList pageList = (DropDownList)pagerRow.Cells[0].FindControl("PageDropDownList");
            Gv.PageIndex = pageList.SelectedIndex;
            if (PageChange != null) PageChange(this, new DataEventArgs<int>(Gv.PageIndex));
        }

        public void Gv_DataBound(Object sender, EventArgs e)
        {

            GridViewRow pagerRow = Gv.BottomPagerRow;
            DropDownList pageList;
            Label pageLabel;
            try
            {
                pageList = (DropDownList)pagerRow.Cells[0].FindControl("PageDropDownList");
                pageLabel = (Label)pagerRow.Cells[0].FindControl("CurrentPageLabel");
            }
            catch (Exception)
            {
                //throw;
                pageList = null;
                pageLabel = null;
            }

            if (pageList != null)
            {
                for (int i = 0; i < Gv.PageCount; i++)
                {
                    int pageNumber = i + 1;
                    ListItem item = new ListItem(pageNumber.ToString());
                    if (i == Gv.PageIndex)
                    {
                        item.Selected = true;
                    }
                    pageList.Items.Add(item);
                }
            }

            if (pageLabel != null)
            {
                int currentPage = Gv.PageIndex + 1;
                pageLabel.Text = "第" + currentPage.ToString() +"頁，共" + Gv.PageCount.ToString()+"頁";
            }

        }
    }
}



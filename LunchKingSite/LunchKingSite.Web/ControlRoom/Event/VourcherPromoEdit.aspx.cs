﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.Event
{
    public partial class VourcherPromoEdit : RolePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                GetViewVourcherPromoCollection();
        }
        /// <summary>
        /// 抓取優惠券行銷資料
        /// </summary>
        protected void GetViewVourcherPromoCollection()
        {
            var promos = VourcherFacade.ViewVourcherPromoCollectionGetAll((int)VourcherPromoType.Seller);
            promos.AddRange(VourcherFacade.ViewVourcherPromoCollectionGetAll((int)VourcherPromoType.Vourcher));
            gv_Promo.DataSource = promos;
            gv_Promo.DataBind();
            pan_List.Visible = btn_Add.Visible = true;
            pan_Edit.Visible = false;
            tbx_Rank.Text = tbx_Ratio.Text = tbx_SellerIdEventId.Text = string.Empty;
        }
        /// <summary>
        /// 對活動期間和狀態做處理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is ViewVourcherPromo)
            {
                ViewVourcherPromo item = (ViewVourcherPromo)e.Row.DataItem;
                Label clab_Date = (Label)e.Row.FindControl("lab_Date");
                Label clab_Status = (Label)e.Row.FindControl("lab_Status");
                Guid seller_guid = item.SellerGuid;
                if (item.Type == (int)VourcherPromoType.Vourcher)
                {
                    VourcherEvent vourcher = VourcherFacade.VourcherEventGetById(item.EventId ?? 0);
                    GetPeriodAndStatus(clab_Date, clab_Status, vourcher);
                }
                else
                {
                    //依賣家搜尋所有優惠券，並取一筆有效期限最大的
                    VourcherEventCollection vourcher_events = VourcherFacade.VourcherEventGetBySellerGuid(seller_guid);
                    if (vourcher_events.Count > 0)
                        GetPeriodAndStatus(clab_Date, clab_Status, vourcher_events.OrderByDescending(x => x.EndDate).First());
                }
            }
        }
        /// <summary>
        /// 編輯選定項目
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvRowEditing(object sender, GridViewEditEventArgs e)
        {
            gv_Promo.EditIndex = e.NewEditIndex;
            GetViewVourcherPromoCollection();
        }
        /// <summary>
        /// 儲存優惠券行銷
        /// </summary>
        /// <param name="hid_id">原id</param>
        /// <param name="input_rank">排序</param>
        /// <param name="input_ratio">權重</param>
        /// <param name="input_id">輸入的賣家或優惠券id</param>
        private void SaveItem(string hid_id, string input_rank, string input_ratio, string input_id)
        {
            bool check = true;
            string message = string.Empty;
            int id, event_id, rank, ratio;
            VourcherPromoType type = VourcherPromoType.Seller;
            Seller seller = new Seller();
            VourcherEvent vourcher = new VourcherEvent();
            //判斷是優惠券id或是賣家id
            if (int.TryParse(input_id, out event_id))
            {
                type = VourcherPromoType.Vourcher;
                vourcher = VourcherFacade.VourcherEventGetById(event_id);
                //判斷是否為有效優惠券id
                if (vourcher.Id == 0)
                {
                    check = false;
                    message += "查詢不到優惠券Id。";
                }
                else
                    seller = VourcherFacade.SellerGetByGuid(vourcher.SellerGuid);
            }
            else if ((seller = VourcherFacade.SellerGetById(input_id)).Guid != Guid.Empty)
                type = VourcherPromoType.Seller;
            else
            {
                check = false;
                message += "請輸入優惠券Id或賣家Id。";
            }
            if (!int.TryParse(input_rank, out rank))
            {
                check = false;
                message += "請輸入排序。";
            }
            if (!int.TryParse(input_ratio, out ratio))
            {
                check = false;
                message += "請輸入權重。";
            }
            if (!check)
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
            else
            {
                VourcherPromo promo = new VourcherPromo();
                //使否為更新或新增
                if (int.TryParse(hid_id, out id))
                    promo = VourcherFacade.VourcherPromoGet(id);
                promo.Type = (int)type;
                if (type == VourcherPromoType.Vourcher)
                    promo.EventId = vourcher.Id;
                else
                    promo.EventId = null;
                promo.SellerId = seller.SellerId;
                promo.SellerGuid = seller.Guid;
                promo.Ratio = ratio;
                promo.Rank = rank;
                VourcherFacade.VourcherPromoSet(promo);
                gv_Promo.EditIndex = -1;
                GetViewVourcherPromoCollection();
            }
        }
        /// <summary>
        /// 更新選定資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = gv_Promo.Rows[e.RowIndex];
            HiddenField hif_gv_Id = (HiddenField)row.FindControl("hif_gv_id");
            TextBox tbx_gv_Rank = (TextBox)row.FindControl("tbx_gv_Rank");
            TextBox tbx_gv_Ratio = (TextBox)row.FindControl("tbx_gv_Ratio");
            TextBox tbx_gv_SellerIdEventId = (TextBox)row.FindControl("tbx_gv_SellerIdEventId");
            SaveItem(hif_gv_Id.Value, tbx_gv_Rank.Text, tbx_gv_Ratio.Text, tbx_gv_SellerIdEventId.Text);
        }
        /// <summary>
        /// 取消動作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvRowCancelEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gv_Promo.EditIndex = -1;
            GetViewVourcherPromoCollection();
        }
        /// <summary>
        /// 刪除選定資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvRowDeleteing(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = gv_Promo.Rows[e.RowIndex];
            HiddenField hif_gv_Id = (HiddenField)row.FindControl("hif_gv_id");
            int id;
            if (int.TryParse(hif_gv_Id.Value, out id))
            {
                VourcherFacade.VourcherPromoDelete(id);
                GetViewVourcherPromoCollection();
            }
        }
        /// <summary>
        /// 確定新增或更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ConfirmSave(object sender, EventArgs e)
        {
            SaveItem(string.Empty, tbx_Rank.Text, tbx_Ratio.Text, tbx_SellerIdEventId.Text);
        }
        /// <summary>
        /// 新增資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddPromo(object sender, EventArgs e)
        {
            //判斷是否超過10筆
            var promos = VourcherFacade.ViewVourcherPromoCollectionGetAll((int)VourcherPromoType.Seller);
            promos.AddRange(VourcherFacade.ViewVourcherPromoCollectionGetAll((int)VourcherPromoType.Vourcher));
            if (promos.Count <= 10)
            {
                btn_Add.Visible = false;
                pan_Edit.Visible = true;
            }
            else
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert('已達上限');", true);
        }
        /// <summary>
        /// 取消編輯
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CancelAction(object sender, EventArgs e)
        {
            pan_Edit.Visible = false;
            btn_Add.Visible = true;
            tbx_Rank.Text = tbx_Ratio.Text = tbx_SellerIdEventId.Text = string.Empty;
        }
        /// <summary>
        /// 顯示活動期間和狀態字串
        /// </summary>
        /// <param name="lab_date">活動期間label</param>
        /// <param name="lab_status">狀態label</param>
        /// <param name="vourcher">優惠券內容</param>
        protected void GetPeriodAndStatus(Label lab_date, Label lab_status, VourcherEvent vourcher)
        {
            DateTime now = DateTime.Now;
            lab_date.Text = (vourcher.StartDate.HasValue ? vourcher.StartDate.Value.ToString("yyyy/MM/dd") : string.Empty)
                + "~" + (vourcher.EndDate.HasValue ? vourcher.EndDate.Value.ToString("yyyy/MM/dd") : string.Empty);
            if (vourcher.StartDate.HasValue && vourcher.EndDate.HasValue)
            {
                if (now > vourcher.StartDate.Value && now <= vourcher.EndDate.Value)
                {
                    lab_status.Text = "正常";
                    lab_status.ForeColor = System.Drawing.Color.Black;
                }
                else if (now < vourcher.StartDate.Value)
                {
                    lab_status.Text = "尚未開始";
                    lab_status.ForeColor = System.Drawing.Color.Blue;
                }
                else
                {
                    lab_status.Text = "已過期";
                    lab_status.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                lab_status.Text = "日期未定";
                lab_status.ForeColor = System.Drawing.Color.Red;
            }
        }
    }
}
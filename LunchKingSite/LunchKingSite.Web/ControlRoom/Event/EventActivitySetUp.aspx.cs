﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.Web.ControlRoom.Event
{
    public partial class EventActivitySetUp : RolePage
    {

        public string UserName
        {
            get { return User.Identity.Name; }
        }
        private DateTime dateStart
        {
            get
            {
                DateTime dateStart;
                DateTime.TryParse(txtCreateTimeS.Text, out dateStart);
                return dateStart;

            }
        }

        private DateTime dateEnd
        {
            get
            {
                DateTime dateEnd;
                DateTime.TryParse(txtCreateTimeE.Text, out dateEnd);
                return dateEnd;
            }
        }

        IEventProvider ep;
        ICmsProvider cp;
        protected void Page_Load(object sender, EventArgs e)
        {
            ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
            cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
            if (!IsPostBack)
            {
                int edmActivityId = Request["id"] == null ? 0 : int.Parse(Request["id"]);
                if (edmActivityId == 0)
                {
                    if (Request["action"] != null && Request["action"] == "new")
                    {
                        InitList();
                        ClearText(false);
                        SetControlSetUpPrpperty();
                        phVersion0Content.Visible = false;   
                        rdoWebOpenTargetSelf.Checked = true;
                        rdoMobileOpenTargetSelf.Checked = true;
                    }
                    else
                    {
                        InitList();
                        GetEventActivityList();
                    }
                }
                else
                {
                    InitList();
                    EditEdmActivity(edmActivityId);
                }
            }
        }

        private void InitList()
        {
            List<ListItem> list_type = new List<ListItem>() {
                new ListItem("跳窗",((int)EventActivityType.PopUp).ToString()),
                new ListItem("跳窗EDM",((int)EventActivityType.PopUpEdm).ToString()),
                new ListItem("點點名",((int)EventActivityType.Ticket).ToString()),
                //new ListItem("贈紅利",((int)EventActivityType.Bouns).ToString()),
                new ListItem("折價券",((int)EventActivityType.DiscountCode).ToString()),
                new ListItem("邀請分享",((int)EventActivityType.InviteFriends).ToString())
                };
            List<ListItem> list_mode = new List<ListItem>() {
                new ListItem("無限制",((int)EventActivityMode.NoLimit).ToString()),
                new ListItem("僅一次",((int)EventActivityMode.ByPeriod).ToString()),
                new ListItem("每日一次",((int)EventActivityMode.ByDay).ToString()),
                new ListItem("依訂單",((int)EventActivityMode.ByOrder).ToString()),
                new ListItem("依序號",((int)EventActivityMode.ByCode).ToString()),
                new ListItem("拼圖",((int)EventActivityMode.ByCollection).ToString()),
                new ListItem("新註冊會員",((int)EventActivityMode.ByNewMember).ToString())
                };

            ddl_Type.DataSource = list_type;
            ddl_Type.DataBind();
            foreach (ListItem item in ddl_Type.Items)
            {
                if (item.Text != "跳窗")
                {
                    item.Attributes.Add("disabled", "disabled");
                }
            }

            txtCreateTimeS.Text = DateTime.Now.AddDays(-7).ToString("yyyy/MM/dd");
            txtCreateTimeE.Text = DateTime.Now.AddMonths(1).ToString("yyyy/MM/dd");
        }

        private void SetControlSetUpPrpperty()
        {            
            ddlCookieExpire.SelectedValue = "1";
            rdoAutoCloseOff.Checked = true;
            ddlAutoCloseSeconds.SelectedValue = "";

            ddl_Type.Enabled = true;

            for (int i = 0; i <= 23; i++)
            {
                ddl_StartH.Items.Add(new ListItem()
                {
                    Text = i.ToString("D2"),
                    Value = i.ToString("D2")
                });
                ddl_EndH.Items.Add(new ListItem()
                {
                    Text = i.ToString("D2"),
                    Value = i.ToString("D2")
                });
            }
            for (int i = 0; i <= 60; i++)
            {
                ddl_Startm.Items.Add(new ListItem()
                {
                    Text = i.ToString("D2"),
                    Value = i.ToString("D2")
                });
                ddl_Endm.Items.Add(new ListItem()
                {
                    Text = i.ToString("D2"),
                    Value = i.ToString("D2")
                });
            }
            ddl_StartH.DataBind();
            ddl_EndH.DataBind();
            ddl_Startm.DataBind();
            ddl_Endm.DataBind();
            ScriptManager.RegisterStartupScript(this, typeof(Button), "refund", "$('#" + tbx_Html_1.ClientID + "').ckeditor();", true);
        }

        protected void ClearText(bool isshow)
        {
            ddlCookieExpire.SelectedIndex = 0;
            ddlAutoCloseSeconds.SelectedIndex = 4;
            cbx_Status.Checked = true;
            tbx_EventName.Text = tbx_StartDate.Text = tbx_EndDate.Text =
               lab_Message.Text =  tbx_Html_1.Text =
               hif_Id.Value = string.Empty;
            pan_Add.Visible = !isshow;
            pan_List.Visible = isshow;
            btn_Save.Text = "儲存";
        }

        protected void SaveActivity(object sender, EventArgs e)
        {
            int id;
            DateTime d_start, d_end;
            if (DateTime.TryParse(tbx_StartDate.Text + " " + ddl_StartH.SelectedValue + ":" + ddl_Startm.SelectedValue, out d_start) &&
                DateTime.TryParse(tbx_EndDate.Text + " " + ddl_EndH.SelectedValue + ":" + ddl_Endm.SelectedValue, out d_end))
            {
                EventActivity ea;
                if (!string.IsNullOrEmpty(hif_Id.Value) && int.TryParse(hif_Id.Value, out id))
                {
                    ea = ep.EventActivityGet(id);
                    ea.Message += UserName + " :" + DateTime.Now.ToString("yyyy/MM/dd");
                }
                else
                {
                    id = -1;
                    ea = new EventActivity();
                    ea.Creator = UserName;
                    ea.Cdt = DateTime.Now;
                }
                ea.Type = (int)EventActivityType.PopUp;
                bool check = true;
                int checkEdmId = 0;
                string checkEdmName = string.Empty;
                if (ea.Type == (int)EventActivityType.PopUpEdm || ea.Type == (int)EventActivityType.PopUp)
                {
                    EventActivityCollection other_activities = ep.EventActivityEdmGetList(id);
                    foreach (var item in other_activities)
                    {
                        List<DateTime> event_dates = new List<DateTime>() { d_start, d_end, item.StartDate, item.EndDate };
                        var ordered_event_dates = event_dates.OrderBy(x => x);
                        if ((ordered_event_dates.Last() - ordered_event_dates.First()).TotalMinutes < ((d_end - d_start) + (item.EndDate - item.StartDate)).TotalMinutes)
                        {
                            check = false;
                            checkEdmId = item.Id;
                            checkEdmName = item.Eventname;
                            break;
                        }
                    }
                }
                if (!check)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Button), "refund", "$('#" + tbx_Html_1.ClientID + "').ckeditor();", true);
                    tbx_Html_1.Text = HttpUtility.HtmlDecode(tbx_Html_1.Text);
                    lab_Message.Text = string.Format("跳窗活動時間與 <a href='?id={0}' target='_blank'>『{1}』</a> 重複", checkEdmId, checkEdmName);
                }
                else
                {
                    ea.Eventname = tbx_EventName.Text;
                    ea.PageName = string.Empty;
                    ea.Description1 = tbx_Html_1.Text;
                    ea.Description2 = string.Empty;
                    ea.Mode = 0;
                    ea.TotalMax = 0;
                    ea.WinningMax = 0;
                    ea.StartDate = d_start;
                    ea.EndDate = d_end;
                    ea.FbPicture = string.Empty;
                    ea.FbDesc = string.Empty;
                    ea.Cpa = string.Empty;
                    ea.Url = string.Empty;
                    ea.Status = cbx_Status.Checked;
                    ea.EdmDefaultCity = PponCityGroup.DefaultPponCityGroup.AllCountry.CityId;
                    ea.Cookieexpire1 = int.Parse(ddlCookieExpire.Text);
                    ea.Cookieexpire2 = 0;
                    ea.Cookieexpire3 = 0;
                    ea.ExcludeCpaList = string.Empty;
                    ea.EdmBg1 = string.Empty;
                    ea.EdmBg2 = string.Empty;
                    ea.EdmCityColor = string.Empty;
                    ea.DiscountCampaignId = 0;
                    ea.DiscountList = string.Empty;
                    bool isnew = ea.IsNew;
                    if (ea.IsNew)
                    {
                        ea.Version = 1;
                        //先存1次是為了取得id
                        ep.EventActivitySet(ea);
                    }

                    ea.WebLinkUrl = txtWebLinkUrl.Text;
                    if (rdoWebOpenTargetSelf.Checked)
                    {
                        ea.WebOpenTarget = 0;
                    }
                    else if (rdoWebOpenTargetBlank.Checked)
                    {
                        ea.WebOpenTarget = 1;
                    }
                    else
                    {
                        throw new Exception("rdoWebOpenTarget 未設定");
                    }
                    if (string.IsNullOrEmpty(hidWebImage.Value))
                    {
                        //刪圖檔
                        if (string.IsNullOrEmpty(ea.WebImage) == false)
                        {
                            ImageFacade.EventActivityImageProcessor.Delete(ea.WebImage);
                            ea.WebImage = string.Empty;
                        }
                    }
                    if (Request.Files[fileUploadWebActivityPic.UniqueID].ContentLength > 0)
                    {
                        string webCacheBustingFileName;
                        ImageFacade.EventActivityImageProcessor.Upload(
                            new AspNetHttpPostedFileAdapter(Request.Files[fileUploadWebActivityPic.UniqueID]), ea.Id.ToString() + "-web",
                            out webCacheBustingFileName);
                        ea.WebImage = webCacheBustingFileName;
                    }

                    ea.MobileLinkUrl = txtMobileLinkUrl.Text;
                    if (rdoMobileOpenTargetSelf.Checked)
                    {
                        ea.MobileOpenTarget = 0;
                    }
                    else if (rdoMobileOpenTargetBlank.Checked)
                    {
                        ea.MobileOpenTarget = 1;
                    }
                    else
                    {
                        throw new Exception("rdoMobileOpenTarget 未設定");
                    }
                    if (string.IsNullOrEmpty(hidMobileImage.Value))
                    {
                        //刪圖檔
                        if (string.IsNullOrEmpty(ea.MobileImage) == false)
                        {
                            ImageFacade.EventActivityImageProcessor.Delete(ea.MobileImage);
                            ea.MobileImage = string.Empty;
                        }
                    }
                    if (Request.Files[fileUploadMobileActivityPic.UniqueID].ContentLength > 0)
                    {
                        string mobileCacheBustingFileName;
                        ImageFacade.EventActivityImageProcessor.Upload(
                            new AspNetHttpPostedFileAdapter(Request.Files[fileUploadMobileActivityPic.UniqueID]), ea.Id.ToString() + "-mobile",
                            out mobileCacheBustingFileName);
                        ea.MobileImage = mobileCacheBustingFileName;
                    }

                    ea.AppLinkUrl = txtAppLinkUrl.Text;
                    ea.AppText = txtAppText.Text;
                    if (string.IsNullOrEmpty(hidAppImage.Value))
                    {
                        //刪圖檔
                        if (string.IsNullOrEmpty(ea.AppImage) == false)
                        {
                            ImageFacade.EventActivityImageProcessor.Delete(ea.AppImage);
                            ea.AppImage = string.Empty;
                        }
                    }
                    if (Request.Files[fileUploadAppActivityPic.UniqueID].ContentLength > 0)
                    {
                        string appCacheBustingFileName;
                        ImageFacade.EventActivityImageProcessor.Upload(
                            new AspNetHttpPostedFileAdapter(Request.Files[fileUploadAppActivityPic.UniqueID]), ea.Id.ToString() + "-app",
                            out appCacheBustingFileName);
                        ea.AppImage = appCacheBustingFileName;
                    }

                    if (rdoAutoCloseOff.Checked || ddlAutoCloseSeconds.SelectedValue == string.Empty)
                    {
                        ea.AutoCloseSeconds = null;
                    }
                    else
                    {
                        ea.AutoCloseSeconds = int.Parse(ddlAutoCloseSeconds.SelectedValue);
                    }

                    ep.EventActivitySet(ea);

                    //InitList();
                    //EditEdmActivity(ea.Id);
                    //清本機暫存
                    HttpContext.Current.Cache.Remove(ProviderFactory.Instance().GetConfig().EdmPopUpCacheName);
                    Response.Redirect("EventActivitySetUp.aspx?id=" + ea.Id);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Button), "refund", "$('#" + tbx_Html_1.ClientID + "').ckeditor();", true);
                lab_Message.Text = "日期格式錯誤";
                tbx_Html_1.Text = HttpUtility.HtmlDecode(tbx_Html_1.Text);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            GetEventActivityList();
        }

        protected void GetEventActivityList()
        {
            pan_List.Visible = true;
            pan_Add.Visible = false;
            gv_EventActivity.DataSource = ep.EventActivityGetList(dateStart, dateEnd);
            gv_EventActivity.DataBind();
        }

        protected void GvRowCommand(object sender, GridViewCommandEventArgs e)
        {
            int id;
            if (int.TryParse(e.CommandArgument.ToString(), out id))
            {
                EventActivity ea = ep.EventActivityGet(id);
                if (e.CommandName == "EditActivity")
                {
                    EditEdmActivity(id);
                }
                else if (e.CommandName == "ChangeStatus")
                {
                    ea.Status = !(ea.Status);
                    ep.EventActivitySet(ea);
                    gv_EventActivity.DataSource = ep.EventActivityGetList(dateStart, dateEnd);
                    gv_EventActivity.DataBind();
                }
            }
        }

        private void EditEdmActivity(int id)
        {
            EventActivity ea = ep.EventActivityGet(id);

            if (ea.Version.GetValueOrDefault() == 0)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Button), "refund", "$('#" + tbx_Html_1.ClientID + "').ckeditor();", true);
                phVersion0Content.Visible = true;
                phVersion1Content.Visible = false;
            } else
            {
                phVersion0Content.Visible = false;
                phVersion1Content.Visible = true;
            }
            if (ea.Id != 0)
            {
                ClearText(false);
                ddl_Type.Enabled = false;
                hif_Id.Value = id.ToString();
                tbx_EventName.Text = ea.Eventname;
                tbx_Html_1.Text = HttpUtility.HtmlDecode(ea.Description1);
                ddl_Type.SelectedValue = ea.Type.ToString();
                tbx_StartDate.Text = ea.StartDate.ToString("MM/dd/yyyy");
                ddl_StartH.SelectedValue = ea.StartDate.ToString("HH");
                ddl_Startm.SelectedValue = ea.StartDate.ToString("mm");
                tbx_EndDate.Text = ea.EndDate.ToString("MM/dd/yyyy");
                ddl_EndH.SelectedValue = ea.EndDate.ToString("HH");
                ddl_Endm.SelectedValue = ea.EndDate.ToString("mm");
                ddlCookieExpire.SelectedValue = ea.Cookieexpire1.GetValueOrDefault().ToString();
                cbx_Status.Checked = ea.Status;
                SetControlSetUpPrpperty();
                ddl_Type.Enabled = false;
                btn_Save.Text = "更新";

                txtWebLinkUrl.Text = ea.WebLinkUrl;
                if (ea.WebOpenTarget == 0)
                {
                    rdoWebOpenTargetSelf.Checked = true;
                }
                else
                {
                    rdoWebOpenTargetBlank.Checked = true;
                }
                hidWebImage.Value = ea.WebImage;
                imgWebPreview.Src = Helper.CombineUrl(
                    ImageFacade.EventActivityImageProcessor.EventActivityImageUrl, ea.WebImage);

                txtMobileLinkUrl.Text = ea.MobileLinkUrl;
                if (ea.MobileOpenTarget == 0)
                {
                    rdoMobileOpenTargetSelf.Checked = true;
                }
                else
                {
                    rdoMobileOpenTargetBlank.Checked = true;
                }
                hidMobileImage.Value = ea.MobileImage;
                imgMobilePreview.Src = Helper.CombineUrl(
                    ImageFacade.EventActivityImageProcessor.EventActivityImageUrl, ea.MobileImage);

                txtAppLinkUrl.Text = ea.AppLinkUrl;
                txtAppText.Text = ea.AppText;
                hidAppImage.Value = ea.AppImage;
                if (ea.AutoCloseSeconds == null)
                {
                    rdoAutoCloseOff.Checked = true;
                }
                else
                {
                    rdoAutoCloseOn.Checked = true;
                    ddlAutoCloseSeconds.SelectedValue = ea.AutoCloseSeconds.ToString();
                }
                hidAppImage.Value = ea.AppImage;
                imgAppPreview.Src = Helper.CombineUrl(
                    ImageFacade.EventActivityImageProcessor.EventActivityImageUrl, ea.AppImage);
            }
        }

        protected string GetAutoCloseSecondsString(int? seconds)
        {
            if (seconds == null)
            {
                return "無設定";
            }
            return seconds.Value.ToString() + "秒";
        }

        protected string GetCookieExpireString(int? cookieExpire)
        {
            if (cookieExpire == null)
            {
                return "每次";
            }
            return cookieExpire.Value.ToString() + "天";
        }

        protected string GetTypeString(int typeId)
        {
            string result = string.Empty;

            switch (typeId)
            {
                case 0:
                    result = "點點名";
                    break;
                case 1:
                    result = "贈紅利";
                    break;
                case 2:
                    result = "贈折價券";
                    break;
                case 3:
                    result = "跳窗";
                    break;
                case 4:
                    result = "跳窗EDM";
                    break;
            }

            return result;
        }

        protected void ShowEdit(object sender, EventArgs e)
        {
            ClearText(false);
            SetControlSetUpPrpperty();
        }
    }

}

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="WeChatPromoSetUp.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Event.WeChatPromoSetUp" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script src="../../Tools/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/adapters/jquery.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .bgOrange {
            background-color: #FFC690;
            text-align: center;
        }

        .bgBlue {
            background-color: #B3D5F0;
        }

        .title {
            text-align: center;
            background-color: #688E45;
            font-weight: bolder;
            color: White;
        }

        .header {
            text-align: center;
            font-size: large;
            font-weight: bolder;
            background-color: #F0AF13;
            color: Blue;
        }
    </style>
    <asp:HiddenField ID="hif_Mode" runat="server" />
    <asp:HiddenField ID="hif_EventId" runat="server" />
    <asp:HiddenField ID="hif_ItemId" runat="server" />
    <asp:HiddenField ID="hif_ItemCount" runat="server" />
    <asp:HiddenField ID="hif_VourcherItemCount" runat="server" />
    <asp:HiddenField ID="hif_PiinlifeItemCount" runat="server" />

    <asp:Panel ID="pan_EventList" runat="server">
        <asp:CheckBox ID="cbx_LimitInThreeMonth" runat="server" Text="只顯示3個月內的" Checked="true" OnCheckedChanged="LimitInThreeMonth_CheckedChanged" AutoPostBack="true" />
        <br />
        <asp:Button ID="btn_AddEventPromo" runat="server" Text="新增WeChat活動訊息" CommandArgument="1"
            OnCommand="AddEventPromo" />
        <br />
        測試帳號:<asp:TextBox ID="tbx_TestAccount" runat="server" Width="250"></asp:TextBox>
        <asp:GridView ID="gv_EventPromo" runat="server" AutoGenerateColumns="false" OnRowCommand="gvEventPromoCommand">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <table style="background-color: gray; width: 800px;">
                            <tr>
                                <td class="bgOrange">活動期間
                                </td>

                                <td class="bgOrange">類型
                                </td>
                                <td class="bgOrange">活動名稱
                                </td>
                                <td class="bgOrange">狀態
                                </td>
                                <td class="bgOrange">圖文設定
                                </td>
                                <td class="bgOrange">測試發送
                                </td>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="bgBlue">
                                <%# ((EventPromo)(Container.DataItem)).StartDate.ToString("yyyy/MM/dd")%>
                           ~
                                <%# ((EventPromo)(Container.DataItem)).EndDate.ToString("yyyy/MM/dd")%>
                            </td>
                            <td class="bgBlue">
                                <%# Helper.GetLocalizedEnum((EventPromoType)(((EventPromo)(Container.DataItem)).Type))%>
                            </td>
                            <td class="bgBlue">
                                <asp:LinkButton ID="lbt_Edit" runat="server" CommandName="EditEvent" CommandArgument="<%# ((EventPromo)(Container.DataItem)).Id %>"><%# ((EventPromo)(Container.DataItem)).Title%></asp:LinkButton>
                            </td>
                            <td class="bgBlue">
                                <asp:ImageButton ID="ibt_ChangeStatus" runat="server" CommandName="ChangeStatus"
                                    Width="30" CommandArgument="<%# ((EventPromo)(Container.DataItem)).Id %>" ImageUrl='<%# ((EventPromo)(Container.DataItem)).Status ? "~/Themes/default/images/17Life/G3/CheckoutPass.png"  : "~/Themes/default/images/17Life/G3/CheckoutError.png"%>' />
                            </td>
                            <td class="bgBlue">
                                <asp:LinkButton ID="lbt_ShowItemList" runat="server" CommandName="ShowItemList" CommandArgument="<%# ((EventPromo)(Container.DataItem)).Id %>" Visible="<%# ((EventPromo)(Container.DataItem)).Type==(int)EventPromoType.WeChatPromoArticle %>">設定圖文</asp:LinkButton>
                            </td>
                            <td class="bgBlue">
                                <asp:LinkButton ID="lbt_SendTestMessage" runat="server" CommandName="SendTestMessage" CommandArgument="<%# ((EventPromo)(Container.DataItem)).Id %>">發送</asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tr> </table>
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pan_EventEdit" runat="server" Visible="false">
        <table style="width: 1000px">
            <tr>
                <td colspan="2" class="header">一、WeChat自訂訊息設定
                </td>
            </tr>
            <tr>
                <td class="title">活動名稱:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_Url" runat="server" Width="300"></asp:TextBox>
                    <span style="color: Gray">請以英文命名</span>
                    <asp:RequiredFieldValidator ID="rev_Url" runat="server" ErrorMessage="*" ControlToValidate="tbx_Url"
                        Display="Dynamic" ForeColor="Red" ValidationGroup="A"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="title">活動期間:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_Start" runat="server" Width="100"></asp:TextBox>
                    <cc1:CalendarExtender ID="ce_Start" TargetControlID="tbx_Start" runat="server">
                    </cc1:CalendarExtender>
                    ~
                    <asp:TextBox ID="tbx_End" runat="server" Width="100"></asp:TextBox>
                    <cc1:CalendarExtender ID="ce_End" TargetControlID="tbx_End" runat="server">
                    </cc1:CalendarExtender>
                    <asp:RequiredFieldValidator ID="rev_Start" runat="server" ErrorMessage="*起" ControlToValidate="tbx_Start"
                        Display="Dynamic" ForeColor="Red" ValidationGroup="A"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rev_End" runat="server" ErrorMessage="*迄" ControlToValidate="tbx_End"
                        Display="Dynamic" ForeColor="Red" ValidationGroup="A"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="title">訊息類別 
                </td>
                <td class="bgBlue">
                    <asp:DropDownList ID="ddl_EventPromoType" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="header">二、訊息內容 (圖文訊息類別不用填入)
                </td>
            </tr>
            <tr>
                <td colspan="2">文字訊息請用原始碼編輯，
                    <br />
                    內容可換行，僅可帶連結(例: &lt;a href="https://www.17life.com"&gt;17Life&lt;/a&gt;)，無法接受其他HTML
                    <asp:TextBox ID="tbx_Html" runat="server" Width="900" Height="200" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="bgBlue" style="text-align: center">
                    <asp:Button ID="btn_Save" runat="server" Text="新增" ForeColor="Blue" ValidationGroup="A"
                        OnClick="SavePromo" />&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_Cancel" runat="server" Text="取消" ForeColor="Red" CommandArgument="0"
                        OnCommand="AddEventPromo" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pan_ItemList" runat="server" Visible="false">
        <asp:Button ID="btn_AddEventPromoItem" runat="server" Text="新增圖文訊息" CommandArgument="4"
            OnCommand="AddEventPromo" />
        <asp:Button ID="btn_BackEventPromoList" runat="server" Text="回活動列表" CommandArgument="0"
            OnCommand="AddEventPromo" />
        <br />
        <asp:Repeater ID="rpt_EventPromoItem" runat="server" OnItemCommand="rptEventPromoItemCommand">
            <HeaderTemplate>
                <table style="background-color: gray; width: 800px;">
                    <tr>
                        <td class="bgOrange">排序
                        </td>
                        <td class="bgOrange">名稱
                        </td>
                        <td class="bgOrange">描述
                        </td>
                        <td class="bgOrange">連結
                        </td>
                        <td class="bgOrange">圖片連結
                        </td>
                        <td class="bgOrange">狀態
                        </td>
                        <td class="bgOrange">編輯
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="bgBlue">
                        <%# ((EventPromoItem)(Container.DataItem)).Seq%>
                    </td>
                    <td class="bgBlue">
                        <%# ((EventPromoItem)(Container.DataItem)).Title%>
                    </td>
                    <td class="bgBlue">
                        <%# ((EventPromoItem)(Container.DataItem)).Description%>
                    </td>
                    <td class="bgBlue">
                        <%# ((EventPromoItem)(Container.DataItem)).ItemUrl%>
                    </td>
                    <td class="bgBlue">
                        <%# ((EventPromoItem)(Container.DataItem)).ItemPicUrl%>
                    </td>
                    <td class="bgBlue">
                        <asp:LinkButton ID="lbt_ShowItemList" runat="server" CommandName="EditItem" CommandArgument="<%# ((EventPromoItem)(Container.DataItem)).Id %>">編輯</asp:LinkButton>
                    </td>
                    <td class="bgBlue">
                        <asp:ImageButton ID="ibt_ChangeStatus" runat="server" CommandName="ChangeItemStatus"
                            Width="30" CommandArgument="<%# ((EventPromoItem)(Container.DataItem)).Id %>"
                            ImageUrl='<%# ((EventPromoItem)(Container.DataItem)).Status ? "~/Themes/default/images/17Life/G3/CheckoutPass.png"  : "~/Themes/default/images/17Life/G3/CheckoutError.png"%>' />
                    </td>

                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <asp:Panel ID="pan_ItemEdit" runat="server" Visible="false">
        <asp:Button ID="btn_BackEventPromoItemList" runat="server" Text="回活動列表" CommandArgument="3"
            OnCommand="AddEventPromo" />
        <table style="width: 700px">
            <tr>
                <td colspan="2" class="header">一、WeChat圖文訊息設定
                </td>
            </tr>
            <tr>
                <td class="title">排序:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_Seq" runat="server" Width="100"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_Seq" runat="server" ControlToValidate="tbx_Seq"
                        ErrorMessage="*" Display="Dynamic" ForeColor="Red" ValidationGroup="B"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rv_Deq" runat="server" ErrorMessage="數字" MinimumValue="0"
                        MaximumValue="9999" Type="Integer" ControlToValidate="tbx_Seq" Display="Dynamic"
                        ForeColor="Red" ValidationGroup="B"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="title">名稱:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_ItemTitle" runat="server" Width="300"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_ItemTitle" runat="server" ControlToValidate="tbx_ItemTitle"
                        ErrorMessage="*" Display="Dynamic" ForeColor="Red" ValidationGroup="B"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="title">描述:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_ItemDescription" runat="server" Width="300"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_ItemDescription" runat="server" ControlToValidate="tbx_ItemDescription"
                        ErrorMessage="*" Display="Dynamic" ForeColor="Red" ValidationGroup="B"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="title">連結:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_ItemUrl" runat="server" Width="300"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_ItemUrl" runat="server" ControlToValidate="tbx_ItemUrl"
                        ErrorMessage="*" Display="Dynamic" ForeColor="Red" ValidationGroup="B"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="title">圖片連結:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_ItemPicUrl" runat="server" Width="300"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_ItemPicUrl" runat="server" ControlToValidate="tbx_ItemPicUrl"
                        ErrorMessage="*" Display="Dynamic" ForeColor="Red" ValidationGroup="B"></asp:RequiredFieldValidator>
                </td>
            </tr>


            <tr>
                <td colspan="2" class="bgBlue" style="text-align: center">
                    <asp:Button ID="btn_ItemSave" runat="server" Text="新增" ForeColor="Blue" ValidationGroup="B"
                        OnClick="SavePromoItem" />&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_ItemCancel" runat="server" Text="取消" ForeColor="Red" CommandArgument="3"
                        OnCommand="AddEventPromo" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Label ID="lab_Message" runat="server" ForeColor="White" BackColor="Red"></asp:Label>
</asp:Content>

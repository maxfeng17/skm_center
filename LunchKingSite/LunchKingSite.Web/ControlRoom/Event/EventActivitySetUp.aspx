﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="EventActivitySetUp.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Event.EventActivitySetUp" ClientIDMode="Static" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link type="text/css" href="/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
    <script type="text/javascript" src="/Tools/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="/Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript" src="/Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="/Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="/Tools/ckeditor/adapters/jquery.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {           

            $('#ddlAutoCloseSeconds').change(function () {
                if ($(this).val() == '') {
                    $('#rdoAutoCloseOff').attr('checked', true);
                } else {
                    $('#rdoAutoCloseOn').attr('checked', true);
                }
            });
            $('#rdoAutoCloseOff').click(function () {
                $('#ddlAutoCloseSeconds option').eq(0).attr('selected', true);
            });

            $('#rdoAutoCloseOn').click(function () {
                if ($('#ddlAutoCloseSeconds').val() == '') {
                    $('#ddlAutoCloseSeconds option').eq(5).attr('selected', true);
                }                
            });

            $('.btnPreview').click(function () {
                var imageValue = $(this).parents('.uploadContainer').find('.imgPreview').attr('src');
                window.open(imageValue, '_blank');
            });

            //$('form').on('submit', function (e) {
            window.validateForm = function () {
                //e.preventDefault();
                var allValidated = true;
                var messages = [];

                if ($.trim($('#tbx_EventName').val()) == '') {
                    allValidated = false;
                    messages.push('請填寫活動名稱');
                }
                if ($.trim($('#tbx_StartDate').val()) == '') {
                    allValidated = false;
                    messages.push('請填寫活動期間');
                }
                if ($.trim($('#tbx_EndDate').val()) == '') {
                    allValidated = false;
                    messages.push('請填寫活動期間');
                }
                if ($('#fileUploadWebActivityPic').val() != '' &&
                    $('#fileUploadWebActivityPic').data('validated') == false) {
                    allValidated = false;
                    messages.push('Web版蓋版圖，尺寸不符合');
                }
                if ($('#fileUploadMobileActivityPic').val() != '' &&
                    $('#fileUploadMobileActivityPic').data('validated') == false) {
                    allValidated = false;
                    messages.push('Mobile版蓋版圖，尺寸不符合');
                }
                if ($('#fileUploadAppActivityPic').val() != '' &&
                    $('#fileUploadAppActivityPic').data('validated') == false) {
                    allValidated = false;
                    messages.push('App版蓋版圖，尺寸不符合');
                }
                if ($('#fileUploadWebActivityPic').val() === '' &&
                    $('#fileUploadMobileActivityPic').val() === '' &&
                    $('#fileUploadAppActivityPic').val() === '' &&
                    $('#hidWebImage').val() === '' &&
                    $('#hidMobileImage').val() === '' &&
                    $('#hidAppImage').val() === '') {
                    allValidated = false;
                    messages.push('至少要設定一種裝置的蓋版圖');
                }


                $('#alert').empty();
                if (allValidated) {
                    return true;
                } else {
                    if (messages.length > 0) {
                        $('#alert').show();
                    } else {
                        $('#alert').hide();
                    }
                    for (var key in messages) {
                        $('#alert').append($('<div>＊' + messages[key] + '</div>'));
                    }
                    return false;
                }
            };
            //});

            window.checkFileUploadImageSize = function () {
                $('#fileUploadWebActivityPic').each(function () {
                    window.checkImageSize(this, 640, 440, function (res, width, height) {
                        $(this).data('validated', res);
                    });
                });
                $('#fileUploadMobileActivityPic').each(function () {
                    window.checkImageSize(this, 414, 660, function (res, width, height) {
                        $(this).data('validated', res);
                    });
                });
                $('#fileUploadAppActivityPic').each(function () {
                    window.checkImageSize(this, 414, 660, function (res, width, height) {
                        $(this).data('validated', res);
                    });
                });
            };

            window.checkImageSize = function (fileInput, matchWidth, matchHeight, callbackFunc) {
                file = fileInput.files && fileInput.files[0];
                if( file ) {
                    var img = new Image();

                    img.src = window.URL.createObjectURL( file );

                    img.onload = function() {
                        var width = img.naturalWidth;
                        var height = img.naturalHeight;

                        window.URL.revokeObjectURL( img.src );

                        if (width != matchWidth || height != matchHeight) {
                            if ($.isFunction(callbackFunc)) {
                                callbackFunc.call(fileInput, false, width, height);
                            }
                        } else {
                            if ($.isFunction(callbackFunc)) {
                                callbackFunc.call(fileInput, true, width, height);
                            }
                        }
                    };
                }
            };

            $("input[type=file]").change(function () {
                window.checkFileUploadImageSize();
            });
            window.checkFileUploadImageSize();

            $('.datepicker').datepicker({
                changeMonth: false,
                numberOfMonths: 1
            });

            window.refreshUploadContainers = function () {
                $('.uploadContainer').each(function () {
                    var fileVal = $(this).find('.valueField').val();
                    if (fileVal == '') {
                        $(this).find('.addPanel').show();
                        $(this).find('.editPanel').hide();
                    } else {
                        $(this).find('.addPanel').hide();
                        $(this).find('.editPanel').show();
                    }
                });
            }
            window.refreshUploadContainers();

            $('.uploadContainer').each(function () {
                var container = this;
                $(this).find('.btnDelete').click(function () {
                    if (confirm('確定要標記刪除?')) {
                        $(container).find('.valueField').val('');
                        window.refreshUploadContainers();
                    }
                });
            });

        });
    </script>
    <style type="text/css">
        #pan_Add {
            padding-top:5px;
        }
        .bgOrange
        {
            background-color: #FFC690;
            text-align: center;
        }
        .bgBlue
        {
            background-color: #B3D5F0;
        }
        .title
        {
            text-align: center;
            background-color: #688E45;
            font-weight: bolder;
            color: White;
        }
        .header
        {
            text-align: center;
            font-size: large;
            font-weight: bolder;
            background-color: #F0AF13;
            color: Blue;
        }
        .mainTable td {
            padding:3px 5px 3px 5px;
        }
        .btn-sm {
            padding:3px 5px 3px 5px;
            font-size:15px;
        }
        .mr-6 {
            margin-right:6px
        }
        .form-inline {
            padding:5px 0px 5px 10px;
            display:table; 
            width:100%;
        }
        .alert {
            padding: .75rem 1.25rem;
            margin-bottom: 1rem;
            border: 1px solid transparent;
            border-radius: .25rem;
        }
        .alert-danger {
            background-color: #f2dede;
            border-color: #ebcccc;
            color: #a94442;
        }
        #alert {
            display:none;
        }
        .btnPreview {
            position:absolute; bottom:0px;
        }
        .btnDelete {
            position:absolute; bottom:0px;
        }
        .imgPreview {
            position:absolute; bottom:0px; 
        }
        .editPanel {
            display:inline-block; position:relative;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pan_List" runat="server" BorderWidth="0">
        <fieldset>
            <legend>活動查詢</legend>
            <table>
                <tr>
                    <td>
                        活動截止日：
                    </td>
                    <td>
                        <asp:TextBox ID="txtCreateTimeS" runat="server" Columns="10" CssClass="datepicker" />
                        ～
                        <asp:TextBox ID="txtCreateTimeE" runat="server" Columns="10" CssClass="datepicker"/>
                        <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <input type="button" value="新增活動" onclick="location.href='?action=new'" style="padding:3px 10px 3px 10px" />
        <asp:GridView ID="gv_EventActivity" runat="server" AutoGenerateColumns="false" OnRowCommand="GvRowCommand">
            <Columns>                
                <asp:TemplateField>
                    <HeaderTemplate>
                        <table style="background-color: gray; width: 900px">
                            <tr>
                                <td class="bgOrange">
                                    ID
                                </td>
                                <td class="bgOrange">
                                    活動名稱
                                </td>
                                <td class="bgOrange">
                                    活動種類
                                </td>
                                <td class="bgOrange">
                                    跳窗自動關閉
                                </td>
                                <td class="bgOrange">
                                    活動期間
                                </td>
                                <td class="bgOrange">
                                    顯示頻率
                                </td>
                                <td class="bgOrange">
                                    狀態
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="bgBlue">
                                <%# ((EventActivity)Container.DataItem).Id%>
                            </td>
                            <td class="bgBlue">
                                <asp:LinkButton ID="lbt_Edit" runat="server" Visible="false" CommandArgument='<%# ((EventActivity)Container.DataItem).Id%>'
                                    CommandName="EditActivity"> <%# ((EventActivity)Container.DataItem).Eventname%></asp:LinkButton>
                                <a href="?id=<%# ((EventActivity)Container.DataItem).Id%>"><%# ((EventActivity)Container.DataItem).Eventname%></a>
                            </td>
                            <td class="bgBlue">
                                <%#GetTypeString(((EventActivity)Container.DataItem).Type)%>
                            </td>
                            <td class="bgBlue">
                                <%#GetAutoCloseSecondsString(((EventActivity)Container.DataItem).AutoCloseSeconds)%>
                            </td>
                            <td class="bgBlue">
                                <%# ((EventActivity)Container.DataItem).StartDate.ToString("yyyy/MM/dd HH:mm") +" ~ "+ ((EventActivity)Container.DataItem).EndDate.ToString("yyyy/MM/dd HH:mm")%>
                            </td>
                            <td class="bgBlue" style="text-align:center">
                                <%# GetCookieExpireString(((EventActivity)Container.DataItem).Cookieexpire1)%>
                            </td>
                            <td class="bgBlue">
                                <asp:ImageButton ID="ibt_ChangeStatus" runat="server" CommandName="ChangeStatus"
                                    Width="30" CommandArgument="<%# ((EventActivity)(Container.DataItem)).Id %>"
                                    ImageUrl='<%# ((EventActivity)(Container.DataItem)).Status ? "~/Themes/default/images/17Life/G3/CheckoutPass.png"  : "~/Themes/default/images/17Life/G3/CheckoutError.png"%>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>                
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pan_Add" runat="server" Visible="false">
        <div id="alert" class="alert alert-danger">

        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table class="mainTable" style="background-color: gray; width: 950px">
                    <tr>
                        <td class="title">
                            活動名稱
                        </td>
                        <td colspan="3" class="bgBlue">
                            <asp:TextBox ID="tbx_EventName" runat="server" Width="300"></asp:TextBox>                            
                            <asp:HiddenField ID="hif_Id" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="title">
                            活動種類
                        </td>
                        <td colspan="3" class="bgBlue">
                            <asp:DropDownList ID="ddl_Type" runat="server">
                            </asp:DropDownList>
                        </td>                    
                    </tr>
                    <tr>
                        <td class="title">
                            活動期間
                        </td>
                        <td colspan="3" class="bgBlue">
                            <asp:TextBox ID="tbx_StartDate" runat="server" Width="100" CssClass="datepicker" />
                            <asp:DropDownList ID="ddl_StartH" runat="server"></asp:DropDownList>:
                            <asp:DropDownList ID="ddl_Startm" runat="server"></asp:DropDownList>
                            ~
                            <asp:TextBox ID="tbx_EndDate" runat="server" Width="100" CssClass="datepicker" />
                            <asp:DropDownList ID="ddl_EndH" runat="server"></asp:DropDownList>:
                            <asp:DropDownList ID="ddl_Endm" runat="server"></asp:DropDownList>
                            &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="cbx_Status" runat="server" Text="啟用" Checked="true" />
                        </td>
                    </tr>
                    <tr>
                        <td class="title">
                            顯示頻率
                        </td>
                        <td class="bgBlue" colspan="3">                                                        
                            <asp:DropDownList id="ddlCookieExpire" runat="server">
                                <asp:ListItem Text="請選擇" Value="" />
                                <asp:ListItem Text="1天" Value="1" />
                                <asp:ListItem Text="2天" Value="2" />
                                <asp:ListItem Text="3天" Value="3" />
                                <asp:ListItem Text="4天" Value="4" />
                                <asp:ListItem Text="5天" Value="5" />
                                <asp:ListItem Text="6天" Value="6" />
                                <asp:ListItem Text="7天" Value="7" />
                                <asp:ListItem Text="8天" Value="8" />
                                <asp:ListItem Text="9天" Value="9" />
                                <asp:ListItem Text="10天" Value="10" />
                                <asp:ListItem Text="11天" Value="11" />
                                <asp:ListItem Text="12天" Value="12" />
                                <asp:ListItem Text="13天" Value="13" />
                                <asp:ListItem Text="14天" Value="14" />
                                <asp:ListItem Text="15天" Value="15" />
                            </asp:DropDownList>
                            <span>內不顯示</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="title">
                            跳窗自動關閉
                        </td>
                        <td class="bgBlue" colspan="3">
                            <label><input type="radio" id="rdoAutoCloseOff" runat="server" name="rdoAutoClose" />關閉</label>
                            <label><input type="radio" id="rdoAutoCloseOn" runat="server" name="rdoAutoClose" style="margin-left:40px" />開啟</label>
                            <span style="margin-left:40px">顯示</span>
                            <asp:DropDownList id="ddlAutoCloseSeconds" runat="server">
                                <asp:ListItem Text="請選擇" Value="" />
                                <asp:ListItem Text="1秒" Value="1" />
                                <asp:ListItem Text="2秒" Value="2" />
                                <asp:ListItem Text="3秒" Value="3" />
                                <asp:ListItem Text="4秒" Value="4" />
                                <asp:ListItem Text="5秒" Value="5" />
                                <asp:ListItem Text="6秒" Value="6" />
                                <asp:ListItem Text="7秒" Value="7" />
                                <asp:ListItem Text="8秒" Value="8" />
                                <asp:ListItem Text="9秒" Value="9" />
                                <asp:ListItem Text="10秒" Value="10" />
                                <asp:ListItem Text="11秒" Value="11" />
                                <asp:ListItem Text="12秒" Value="12" />
                                <asp:ListItem Text="13秒" Value="13" />
                                <asp:ListItem Text="14秒" Value="14" />
                                <asp:ListItem Text="15秒" Value="15" />
                            </asp:DropDownList>
                            <span>後關閉(僅app支援)</span>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:PlaceHolder ID="phVersion0Content" runat="server">
        <table style="background-color: gray; width: 950px">
            <tr>
                <td colspan="4" class="title">
                    Html 編輯 1
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:TextBox ID="tbx_Html_1" runat="server" TextMode="MultiLine" Width="900" Height="250"></asp:TextBox>
                </td>
            </tr>
        </table>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phVersion1Content" runat="server">

        
        <fieldset style="background-color:whitesmoke; margin-top:10px">            
            <legend>Web版蓋版圖(640px*440px)</legend>
            <div class="form-inline uploadContainer">
                <input type="hidden" id="hidWebImage" runat="server" class="valueField" />
                <div style="display:inline-block; width:140px">
                    圖片上傳: 
                </div>
                <div style="display:inline-block; width:200px" class="addPanel">
                    <asp:FileUpload ID="fileUploadWebActivityPic" runat="server" CssClass="btn-sm" />
                </div>                                   
                <div class="editPanel" style="width:400px; height:50px">
                    <img runat="server" id="imgWebPreview" style="bottom:0px; left:0px; width:80px; height:55px" class="imgPreview" alt="" />
                    <button type="button" style="bottom:0px; left:100px;" class="btn-sm mr-6 btnPreview">預覽</button>
                    <button type="button" style="bottom:0px; left:150px;" class="btn-sm mr-6 btnDelete">刪除</button>                    
                </div>
            </div>
            <div class="form-inline">
                <div style="display:table-cell; width:140px">
                    圖片連結網址: 
                </div>
                <div style="display:table-cell; width:auto">
                    <asp:TextBox ID="txtWebLinkUrl" runat="server"  Width="100%" Text="" />
                </div>                                   
            </div>
            <div class="form-inline">
                <div style="display:table-cell; width:140px">
                    視窗開啟方式: 
                </div>
                <div style="display: table-cell; width: auto">
                    <asp:RadioButton ID="rdoWebOpenTargetSelf" runat="server" GroupName="rdoWebOpenTarget" Text="本頁開啟" />
                    <asp:RadioButton ID="rdoWebOpenTargetBlank" runat="server" GroupName="rdoWebOpenTarget" Text="另開視窗" />
                </div>
            </div>
        </fieldset>
        <fieldset style="background-color:whitesmoke; margin-top:10px">
            <legend>M版蓋版圖(414px*660px)</legend>
            <div class="form-inline uploadContainer">
                <input type="hidden" id="hidMobileImage" runat="server" class="valueField" />
                <input type="hidden" id="hidMobilePreviewImageUrl" runat="server" class="previewImageUrl" />
                <div style="display:inline-block; width:140px">
                    圖片上傳: 
                </div>
                <div style="display:inline-block; width:200px" class="addPanel">
                    <asp:FileUpload ID="fileUploadMobileActivityPic" runat="server" CssClass="btn-sm" />
                </div>                                   
                <div style="width:400px; height:80px" class="editPanel">
                    <img runat="server" id="imgMobilePreview" style="bottom:0px; left:0px; width:52px; height:82px" 
                        class="imgPreview" alt="" />
                    <button type="button" style="bottom:0px; left:70px;" class="btn-sm mr-6 btnPreview">預覽</button>
                    <button type="button" style="bottom:0px; left:120px;" class="btn-sm mr-6 btnDelete">刪除</button>
                </div>
            </div>
            <div class="form-inline">
                <div style="display:table-cell; width:140px">
                    圖片連結網址: 
                </div>
                <div style="display:table-cell; width:auto">
                    <asp:TextBox ID="txtMobileLinkUrl" runat="server"  Width="100%" Text="" />
                </div>                                   
            </div>
            <div class="form-inline">
                <div style="display:table-cell; width:140px">
                    視窗開啟方式: 
                </div>
                <div style="display:table-cell; width:auto">
                    <asp:RadioButton ID="rdoMobileOpenTargetSelf" runat="server" GroupName="rdoMobileOpenTarget" Text="本頁開啟" />
                    <asp:RadioButton ID="rdoMobileOpenTargetBlank" runat="server" GroupName="rdoMobileOpenTarget" Text="另開視窗" />
                </div>
            </div>
        </fieldset>
        <fieldset style="background-color:whitesmoke; margin-top:10px">
            <legend>APP蓋版圖(414px*600px)</legend>
            <div class="form-inline uploadContainer">
                <input type="hidden" id="hidAppImage" runat="server" class="valueField" />
                <div style="display:inline-block; width:140px">
                    圖片上傳: 
                </div>
                <div style="display:inline-block; width:200px" class="addPanel">
                    <asp:FileUpload ID="fileUploadAppActivityPic" runat="server" CssClass="btn-sm" />
                </div>                                   
                <div style="width:400px; height:80px" class="editPanel">
                    <img runat="server" id="imgAppPreview" style="bottom:0px; left:0px; width:52px; height:82px" 
                        class="imgPreview" alt="" />
                    <button type="button" style="bottom:0px; left:70px;" class="btn-sm mr-6 btnPreview">預覽</button>
                    <button type="button" style="bottom:0px; left:120px;" class="btn-sm mr-6 btnDelete">刪除</button>
                </div>
            </div>
            <div class="form-inline">
                <div style="display:table-cell; width:140px">
                    圖片連結網址: 
                </div>
                <div style="display:table-cell; width:auto">
                    <asp:TextBox ID="txtAppLinkUrl" runat="server"  Width="100%" Text="" />
                </div>                                   
            </div>
            <div class="form-inline">
                <div style="display:table-cell; width:140px">
                    內容: (40字)
                </div>
                <div style="display:table-cell; width:auto">
                    <asp:TextBox ID="txtAppText" runat="server"  Width="100%" Text="" MaxLength="40" />
                </div>                                   
            </div>
        </fieldset>
        </asp:PlaceHolder>
        <div class="title" style=" margin-top:10px; padding:5px">
            <asp:Button ID="btn_Save" runat="server" Text="存檔" OnClientClick="return validateForm()" OnClick="SaveActivity" style="padding:3px 10px 3px 10px; font-size:15px" />
            <input type="button" value="返回" id="btnCancel" style="color:red; margin-left:30px; padding:3px 10px 3px 10px; 
                    font-size:15px" onclick="location.href='/ControlRoom/Event/EventActivitySetUp.aspx'" />
            <span style="color:white; background-color:red"><asp:Literal ID="lab_Message" runat="server" Mode="PassThrough" /></span>
        </div>
    </asp:Panel>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="VourcherPromoEdit.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Event.VourcherPromoEdit" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script src="../../Tools/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Tools/js/effects/jquery.flexipage.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('fieldset li').hover(function () { $(this).addClass('highlight_tr'); }, function () { $(this).removeClass('highlight_tr'); });
        });
    </script>
      <link type="text/css" href="../../Themes/default/images/17Life/Gactivities/vourcher.css"
        rel="stylesheet" />
    <style type="text/css">
        ul
        {
            margin-left: -50px;
        }
        fieldset
        {
            width: 900px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pan_List" runat="server">
        <fieldset>
            <legend>Top 賣家優惠券管理</legend>
            <asp:GridView ID="gv_Promo" runat="server" OnRowDataBound="gvDataBound" AutoGenerateColumns="false"
                OnRowEditing="gvRowEditing" OnRowUpdating="gvRowUpdating" OnRowCancelingEdit="gvRowCancelEdit"
                OnRowDeleting="gvRowDeleteing" Width="100%" BorderWidth="0">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <table class="tablecontent">
                                <tr>
                                    <td class="tablehead" style="width: 6%">
                                        排序
                                    </td>
                                    <td class="tablehead" style="width: 6%">
                                        權重
                                    </td>
                                    <td class="tablehead" style="width: 13%">
                                        賣家/優惠券編號
                                    </td>
                                    <td class="tablehead" style="width: 23%">
                                        品牌名稱
                                    </td>
                                    <td class="tablehead" style="width: 18%">
                                        活動期間
                                    </td>
                                    <td class="tablehead" style="width: 10%">
                                        狀態
                                    </td>
                                    <td class="tablehead" style="width: 14%">
                                        編輯
                                    </td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="hif_gv_Id" runat="server" Value="<%# ((ViewVourcherPromo)(Container.DataItem)).Id %>" />
                                    <%# ((ViewVourcherPromo)(Container.DataItem)).Rank %>
                                </td>
                                <td>
                                    <%# ((ViewVourcherPromo)(Container.DataItem)).Ratio %>
                                </td>
                                <td>
                                    <%# ((ViewVourcherPromo)(Container.DataItem)).Type == (int)VourcherPromoType.Seller ? ((ViewVourcherPromo)(Container.DataItem)).SellerId : ((ViewVourcherPromo)(Container.DataItem)).EventId.ToString()%>
                                </td>
                                <td>
                                    <%# ((ViewVourcherPromo)(Container.DataItem)).SellerName %>
                                </td>
                                <td>
                                    <asp:Label ID="lab_Date" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lab_Status" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Button ID="btn_Edit" runat="server" Text="編輯" CommandArgument='<%# Container.DataItemIndex %>'
                                        CommandName="Edit" />&nbsp;
                                    <asp:Button ID="btn_Delete" runat="server" Text="刪除" ForeColor="Red" CommandArgument='<%# ((ViewVourcherPromo)(Container.DataItem)).Id %>'
                                        CommandName="Delete" OnClientClick="return confirm('確定刪除');" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr>
                                <td class="oddtr">
                                    <asp:HiddenField ID="hif_gv_Id" runat="server" Value="<%# ((ViewVourcherPromo)(Container.DataItem)).Id %>" />
                                    <%# ((ViewVourcherPromo)(Container.DataItem)).Rank %>
                                </td>
                                <td class="oddtr">
                                    <%# ((ViewVourcherPromo)(Container.DataItem)).Ratio %>
                                </td>
                                <td class="oddtr">
                                    <%# ((ViewVourcherPromo)(Container.DataItem)).Type == (int)VourcherPromoType.Seller ? ((ViewVourcherPromo)(Container.DataItem)).SellerId : ((ViewVourcherPromo)(Container.DataItem)).EventId.ToString()%>
                                </td>
                                <td class="oddtr">
                                    <%# ((ViewVourcherPromo)(Container.DataItem)).SellerName %>
                                </td>
                                <td class="oddtr">
                                    <asp:Label ID="lab_Date" runat="server"></asp:Label>
                                </td>
                                <td class="oddtr">
                                    <asp:Label ID="lab_Status" runat="server"></asp:Label>
                                </td>
                                <td class="oddtr">
                                    <asp:Button ID="btn_Edit" runat="server" Text="編輯" CommandArgument='<%# Container.DataItemIndex %>'
                                        CommandName="Edit" />&nbsp;
                                    <asp:Button ID="btn_Delete" runat="server" Text="刪除" ForeColor="Red" CommandArgument='<%# ((ViewVourcherPromo)(Container.DataItem)).Id %>'
                                        CommandName="Delete" OnClientClick="return confirm('確定刪除');" />
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                        <EditItemTemplate>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="hif_gv_Id" runat="server" Value="<%# ((ViewVourcherPromo)(Container.DataItem)).Id %>" />
                                    <asp:TextBox ID="tbx_gv_Rank" runat="server" Width="50" Text="<%# ((ViewVourcherPromo)(Container.DataItem)).Rank %>"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_gv_Rank" runat="server" ErrorMessage="*" Display="Dynamic"
                                        ValidationGroup="EditRow" ControlToValidate="tbx_gv_Rank"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rev_gv_Rank" runat="server" ErrorMessage="*"
                                        Display="Dynamic" ValidationGroup="EditRow" ControlToValidate="tbx_gv_Rank" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                                </td>
                                <td>
                                    <asp:TextBox ID="tbx_gv_Ratio" runat="server" Width="50" Text="<%# ((ViewVourcherPromo)(Container.DataItem)).Ratio %>"> </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_gv_Ratio" runat="server" ErrorMessage="*" Display="Dynamic"
                                        ValidationGroup="EditRow" ControlToValidate="tbx_gv_Ratio"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rev_gv_Ratio" runat="server" ErrorMessage="*"
                                        Display="Dynamic" ValidationGroup="EditRow" ControlToValidate="tbx_gv_Ratio"
                                        ValidationExpression="\d*"></asp:RegularExpressionValidator>
                                </td>
                                <td>
                                    <asp:TextBox ID="tbx_gv_SellerIdEventId" runat="server" Width="100" Text=" <%# ((ViewVourcherPromo)(Container.DataItem)).Type == (int)VourcherPromoType.Seller ? ((ViewVourcherPromo)(Container.DataItem)).SellerId : ((ViewVourcherPromo)(Container.DataItem)).EventId.ToString()%>"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_gv_Id" runat="server" ErrorMessage="*" Display="Dynamic"
                                        ValidationGroup="EditRow" ControlToValidate="tbx_gv_SellerIdEventId"></asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <%# ((ViewVourcherPromo)(Container.DataItem)).SellerName %>
                                </td>
                                <td>
                                    <asp:Label ID="lab_Date" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lab_Status" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Button ID="btn_gv_Confirm" runat="server" Text="更新" CommandName="Update" ValidationGroup="EditRow" />&nbsp;
                                    <asp:Button ID="btn_gv_Cancel" runat="server" Text="取消" ForeColor="Red" CommandName="Cancel" />
                                </td>
                            </tr>
                        </EditItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <span style="float: left">
                <asp:Button ID="btn_Add" runat="server" Text="新增" OnClick="AddPromo" ForeColor="Blue" /></span>
        </fieldset>
    </asp:Panel>
    <asp:Panel ID="pan_Edit" runat="server" Visible="false">
        <fieldset>
            <legend>新增</legend>
            <table class="tablecontent">
                <tr>
                    <td class="tablehead" style="width: 10%">
                        排序
                    </td>
                    <td class="tablehead" style="width: 10%">
                        權重
                    </td>
                    <td class="tablehead" style="width: 20%">
                        賣家/優惠券編號
                    </td>
                    <td class="tablehead">
                        編輯
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="tbx_Rank" runat="server" Width="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv_Rank" runat="server" ErrorMessage="*" Display="Dynamic"
                            ValidationGroup="Edit" ControlToValidate="tbx_Rank"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="rev_Rank" runat="server" ErrorMessage="*" Display="Dynamic"
                            ValidationGroup="Edit" ControlToValidate="tbx_Rank" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="tbx_Ratio" runat="server" Width="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv_Ratio" runat="server" ErrorMessage="*" Display="Dynamic"
                            ValidationGroup="Edit" ControlToValidate="tbx_Ratio"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="rev_Ratio" runat="server" ErrorMessage="*" Display="Dynamic"
                            ValidationGroup="Edit" ControlToValidate="tbx_Ratio" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="tbx_SellerIdEventId" runat="server" Width="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv_Id" runat="server" ErrorMessage="*" Display="Dynamic"
                            ValidationGroup="Edit" ControlToValidate="tbx_SellerIdEventId"></asp:RequiredFieldValidator>
                    </td>
                    <td style="text-align: center">
                        <asp:Button ID="btn_Confirm" runat="server" Text="新增" OnClick="ConfirmSave" ValidationGroup="Edit" />
                        &nbsp;
                        <asp:Button ID="btn_Cancel" runat="server" Text="取消" ForeColor="Red" OnClick="CancelAction" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
</asp:Content>

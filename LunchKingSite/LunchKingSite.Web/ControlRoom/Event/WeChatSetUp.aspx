﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="WeChatSetUp.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Event.WeChatSetUp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <a href="../../Service/einvoiceaccount.ashx?username=shihling@17life.com.tw">einvoice</a>
    <table>
        <tr>
            <td colspan="4">WeChat基本設定
            </td>
        </tr>
        <tr>
            <td>刪除所有Menu</td>
            <td colspan="2"></td>
            <td>
                <asp:Button ID="btn_DeleteMenu" runat="server" Text="刪除" OnClientClick="return confirm('確定刪除');" OnClick="DeleteMenu" /></td>

        </tr>
        <tr>
            <td>建立Menu</td>
            <td colspan="2">Menu Json格式 :<asp:TextBox ID="tbx_MenuJson" runat="server"></asp:TextBox></td>
            <td>
                <asp:Button ID="btn_CreateMenu" runat="server" Text="建立" OnClick="CreateMenu" />
                &nbsp;
                <asp:Button ID="btn_RecoverDefaultMenu" runat="server" Text="回復預設" OnClick="RecoverDefaultMenu" />
            </td>
        </tr>
        <tr>
            <td>傳送訊息</td>
            <td>接收者OpenID :<asp:TextBox ID="tbx_ReceiverId" runat="server"></asp:TextBox></td>
            <td>內容 :<asp:TextBox ID="tbx_Content" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btn_Send" runat="server" Text="發送" OnClick="SendWeChatMessage" /></td>
        </tr>
        <tr>
            <td>上傳圖片</td>
            <td colspan="2">
                <asp:FileUpload ID="fu_UploadImage" runat="server" /></td>
            <td>
                <asp:Button ID="btn_UploadImage" runat="server" Text="上傳" OnClick="UploadImage" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lab_Message" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>

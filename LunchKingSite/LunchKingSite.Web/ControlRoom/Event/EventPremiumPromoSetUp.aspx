﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="EventPremiumPromoSetUp.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Event.EventPremiumPromoSetUp" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script src="../../Tools/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/adapters/jquery.js"></script>
    
    <style type="text/css">
        .bgOrange {
            background-color: #FFC690;
            text-align: center;
        }

        .bgBlue {
            background-color: #B3D5F0;
        }

        .title {
            text-align: center;
            background-color: #688E45;
            font-weight: bolder;
            color: White;
        }

        .header {
            text-align: center;
            font-size: large;
            font-weight: bolder;
            background-color: #F0AF13;
            color: Blue;
        }
    </style>    

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <asp:HiddenField ID="hif_Mode" runat="server" />
        <asp:HiddenField ID="hif_EventPremiumId" runat="server" />
        <asp:Panel ID="pl_EventPremiumList" runat="server">
        <asp:CheckBox ID="cbx_LimitInThreeMonth" runat="server" Text="只顯示3個月內的" Checked="true" AutoPostBack="true" />
        <br />
        <asp:Button ID="btn_AddEventPremiumPromo" runat="server" Text="新增免費索取活動" CommandArgument="1"
            OnCommand="AddEventPremiumPromo" />    
            
        <asp:GridView ID="gv_EventPremiumPromo" runat="server" AutoGenerateColumns="false" OnRowCommand="gvEventPremiumPromoCommand" OnRowDataBound="gv_EventPremiumPromo_RowDataBound">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <table style="background-color: gray; width: 1000px">
                            <tr>
                                <td class="bgOrange" style="width: 8%" rowspan="2">ID</td>
                                <td class="bgOrange" style="width: 8%" rowspan="2">開始時間
                                </td>
                                <td class="bgOrange" style="width: 8%" rowspan="2">結束時間
                                </td>
                                <td class="bgOrange" style="width: 15%" rowspan="2">類型
                                </td>
                                <td class="bgOrange" style="width: 20%" rowspan="2">免費索取活動名稱
                                </td>
                                <td class="bgOrange" style="width: 25%" rowspan="2">連結
                                </td>
                                <td colspan="3" class="bgOrange" style="width: 15%" rowspan="2">狀態
                                </td>
<%--                                <td class="bgOrange" style="width: 10%" colspan="2">顯示於
                                </td>--%>
                            </tr>
<%--                            <tr>
                                <td class="bgOrange">Web</td>
                                <td class="bgOrange">App</td>
                            </tr>--%>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="bgBlue">
                                <%# ((EventPremiumPromo)(Container.DataItem)).Id %>
                            </td>
                            <td class="bgBlue">
                                <%# ((EventPremiumPromo)(Container.DataItem)).StartDate.ToString("yyyy/MM/dd")%>
                            </td>
                            <td class="bgBlue">
                                <%# ((EventPremiumPromo)(Container.DataItem)).EndDate.ToString("yyyy/MM/dd")%>
                            </td>
                            <td class="bgBlue">
                                <%# Helper.GetLocalizedEnum((EventPremiumPromoType)(((EventPremiumPromo)(Container.DataItem)).TemplateType))%>
                            </td>
                            <td class="bgBlue">
                                <asp:LinkButton ID="lbt_Edit" runat="server" CommandName="EditEvent" CommandArgument="<%# ((EventPremiumPromo)(Container.DataItem)).Id %>"><%# ((EventPremiumPromo)(Container.DataItem)).Title%></asp:LinkButton>
                            </td>
                            <td class="bgBlue">
                                <asp:Label ID="lblLink" runat="server"></asp:Label>
                            </td>
                            <td class="bgBlue" style="display: none;">
                                <asp:LinkButton ID="lbt_ShowItemList" runat="server" CommandName="ShowItemList" CommandArgument="<%# ((EventPremiumPromo)(Container.DataItem)).Id %>">檢視明細</asp:LinkButton>
                            </td>
                            <td class="bgBlue">
                                <asp:HyperLink ID="lkPreview" runat="server" Target="_blank">預覽</asp:HyperLink>
                            </td>
                            <td class="bgBlue">
                                <asp:ImageButton ID="ibt_ChangeStatus" runat="server" CommandName="ChangeStatus"
                                    Width="30" CommandArgument="<%# ((EventPremiumPromo)(Container.DataItem)).Id %>" ImageUrl='<%# ((EventPremiumPromo)(Container.DataItem)).Status ? "~/Themes/default/images/17Life/G3/CheckoutPass.png"  : "~/Themes/default/images/17Life/G3/CheckoutError.png"%>' />
                            </td>
<%--                            <td class="bgBlue">
                                <asp:CheckBox runat="server" ID="ShowInWeb" Checked="<%# ((EventPromo)(Container.DataItem)).ShowInWeb %>" OnCheckedChanged="ShowInWebChange" AutoPostBack="True" />
                            </td>
                            <td class="bgBlue">
                                <asp:CheckBox runat="server" ID="ShowInApp" Checked="<%# ((EventPromo)(Container.DataItem)).ShowInApp %>" OnCheckedChanged="ShowInAppChange" AutoPostBack="True" />
                            </td>--%>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>            

        </asp:Panel>  
    
        <%-- EditPanel --%>
    
<asp:Panel ID="pl_PremiumEventEdit" runat="server" Visible="false">
        <table style="width: 1000px">
            <tr>
                <td colspan="2" class="header">一、免費索取活動頁面設定
                </td>
            </tr>
            <tr>
                <td class="title">活動名稱:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_EventName" runat="server" Width="300"></asp:TextBox>
                    <span style="color: Gray">請以中文命名！將帶入此活動網頁title</span>
                    <asp:RequiredFieldValidator ID="rev_EventName" runat="server" ErrorMessage="*" ControlToValidate="tbx_EventName"
                        Display="Dynamic" ForeColor="Red" ValidationGroup="A"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="title">自訂網址:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_Url" runat="server" Width="300"></asp:TextBox>
                    <span style="color: Gray">請以英文命名，將帶入此活動網網址 </span>
                    <asp:RequiredFieldValidator ID="rev_Url" runat="server" ErrorMessage="*" ControlToValidate="tbx_Url"
                        Display="Dynamic" ForeColor="Red" ValidationGroup="A"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="title">贈品名稱:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_PremiumName" runat="server" Width="300"></asp:TextBox>
                    
                    <asp:RequiredFieldValidator ID="rev_PremiumName" runat="server" ErrorMessage="*" ControlToValidate="tbx_PremiumName"
                        Display="Dynamic" ForeColor="Red" ValidationGroup="A"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="title">CPA:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_Cpa" runat="server" Width="300"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="title">活動期間:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_Start" runat="server" Width="100"></asp:TextBox>
                    <cc1:CalendarExtender ID="ce_Start" TargetControlID="tbx_Start" runat="server">
                    </cc1:CalendarExtender>
                    ~
                    <asp:TextBox ID="tbx_End" runat="server" Width="100"></asp:TextBox>
                    <cc1:CalendarExtender ID="ce_End" TargetControlID="tbx_End" runat="server">
                    </cc1:CalendarExtender>
                    <asp:RequiredFieldValidator ID="rev_Start" runat="server" ErrorMessage="*起" ControlToValidate="tbx_Start"
                        Display="Dynamic" ForeColor="Red" ValidationGroup="A"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rev_End" runat="server" ErrorMessage="*迄" ControlToValidate="tbx_End"
                        Display="Dynamic" ForeColor="Red" ValidationGroup="A"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="title">版型設定:
                </td>
                <td class="bgBlue">
                    <asp:DropDownList ID="ddlTemplateType" runat="server"></asp:DropDownList>
                </td>
            </tr>

            <tr>
                <td class="title">索取贈品份數:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_PremiumsAmount" runat="server" Width="300"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rev_PremiumsAmount" runat="server" ErrorMessage="*" ControlToValidate="tbx_PremiumsAmount"
                    Display="Dynamic" ForeColor="Red" ValidationGroup="A"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="header">二、按鈕&文字顏色
                </td>
            </tr>
            <tr>
                <td class="title">Banner字體顏色:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_BannerFontColor" runat="server"></asp:TextBox>
                    <cc1:ColorPickerExtender ID="cpe_BannerFontColor" runat="server" TargetControlID="tbx_BannerFontColor" />
                </td>
            </tr>
            <tr>
                <td class="title">按鈕背景顏色:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_BtnBackgroundColor" runat="server"></asp:TextBox>
                    <cc1:ColorPickerExtender ID="cpe_BtnBackgroundColor" runat="server" TargetControlID="tbx_BtnBackgroundColor" />                    
                </td>
            </tr>
            <tr>
                <td class="title">按鈕背景顏色:hover:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_BtnBackgroundHoverColor" runat="server"></asp:TextBox>
                    <cc1:ColorPickerExtender ID="cpe_BtnBackgroundHoverColor" runat="server" TargetControlID="tbx_BtnBackgroundHoverColor" />                    
                </td>
            </tr>

            <tr>
                <td class="title">按鈕字體顏色:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_BtnFontColor" runat="server"></asp:TextBox>
                    <cc1:ColorPickerExtender ID="cpe_BrnFontColor" runat="server" TargetControlID="tbx_BtnFontColor" />
                </td>
            </tr>
            <tr>
                <td colspan="2" class="header">三、圖片編輯
                </td>
            </tr>
            <tr>
                <td class="title">背景:
                </td>
                <td class="bgBlue">
                    <cc1:TextBoxWatermarkExtender ID="tbw_BgPic" runat="server" TargetControlID="tbx_BgPic"
                        WatermarkCssClass="ldnmbg" WatermarkText="http://">
                    </cc1:TextBoxWatermarkExtender>
                    <asp:TextBox ID="tbx_BgPic" runat="server" Width="300"></asp:TextBox>
                    &nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="tbx_BgColor" runat="server"></asp:TextBox>
                    <cc1:ColorPickerExtender runat="server" TargetControlID="tbx_BgColor" ID="cpe_BgColor" />
                </td>
            </tr>
            <tr>
                <td class="title">主圖:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="txtMainPic" runat="server" Width="750" Height="300" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="header">四、活動辦法 (若空白則不會顯現)
                </td>
            </tr>
            <tr>
                <td colspan="2" class="bgBlue">
                    <span style="color: Gray">無法變更字形、字元大小與顏色 </span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="tbx_Html" runat="server" Width="900" Height="300" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="header">五、索取成功頁面
                </td>
            </tr>
            <tr>
                <td class="title">(Web)索取成功訊息:
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_ApplySuccessContent" runat="server" Width="750" Height="300" TextMode="MultiLine"></asp:TextBox>
<%--                    <asp:RequiredFieldValidator ID="rev_ApplySuccessContent" runat="server" ErrorMessage="*" ControlToValidate="tbx_ApplySuccessContent"
                    Display="Dynamic" ForeColor="Red" ValidationGroup="A"></asp:RequiredFieldValidator>--%>

                </td>
            </tr>
   
<%--            <tr>
                <td colspan="2" class="header">六、FB分享設定
                </td>
            </tr>--%>
            <tr>
                <td colspan="2" class="bgBlue" style="text-align: center">
                    <asp:Button ID="btn_Save" runat="server" Text="新增" ForeColor="Blue" ValidationGroup="A"
                        OnClick="SavePremiumPromo" />&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_Cancel" runat="server" Text="取消" ForeColor="Red" CommandArgument="0"
                        OnCommand="AddEventPremiumPromo" />
                                   
                </td>
            </tr>
        </table>
    </asp:Panel>
    

    <asp:Label ID="lab_Message" runat="server" ForeColor="White" BackColor="Red"></asp:Label>
</asp:Content>

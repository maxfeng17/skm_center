﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.IO;
namespace LunchKingSite.Web.ControlRoom.Event
{
    public partial class CurationSetup : Page, IEventPromoSetUp
    {
        public bool IsCurationFunction
        {
            get { return true; }
        }

        #region event
        public event EventHandler<DataEventArgs<EventPromoUpdateModel>> SaveEventPromo;
        public event EventHandler ChangeMode;
        public event EventHandler GetEventPromo;
        public event EventHandler UpdateEventPromoStatus;
        public event EventHandler GetEventPromoItemList;
        public event EventHandler UpdateEventPromoItemStatus;
        public event EventHandler<DataEventArgs<KeyValuePair<int, EventPromoItemType>>> GetPponDeal;
        public event EventHandler<DataEventArgs<KeyValuePair<int, bool>>> GetVourcher;
        public event EventHandler<DataEventArgs<EventPromoItemUpdateMode>> SaveEventPromoItem;
        public event EventHandler<DataEventArgs<EventPromoItemUpdateMode>> SaveEventPromoItemWithVourcher;
        public event EventHandler GetEventPromoItem;
        public event EventHandler<DataEventArgs<string>> WeChatSendTestMessage;
        public event EventHandler GetEventPromoList;
        public event EventHandler<DataEventArgs<bool>> SetEventPromoShowInApp;
        public event EventHandler<DataEventArgs<bool>> SetEventPromoShowInWeb;
        public event EventHandler<DataEventArgs<int>> DeleteEventPromoItem;
        public event EventHandler<DataEventArgs<List<Tuple<string, string, string>>>> ImportBidList;
        public event EventHandler<DataEventArgs<KeyValuePair<int, Dictionary<int, int>>>> UpdateEventPromoItemSeq;        
        public event GetEventPromoItemStatusHandler GetEventPromoItemStatus;
        #endregion
        #region property

        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public ISysConfProvider SystemConfig
        {
            get { return cp; }
        }


        private EventPromoSetUpPresenter _presenter;
        public EventPromoSetUpPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        //0 主檔列表，1 主檔新增, 2 主檔修改, 3 子檔列表, 4 子檔新增, 5 子檔修改
        public int Mode
        {
            get
            {
                int mode;
                return int.TryParse(hif_Mode.Value, out mode) ? mode : 0;
            }
            set
            {
                hif_Mode.Value = value.ToString();
                if (ChangeMode != null)
                {
                    EventArgs e = new EventArgs();
                    ChangeMode(this, e);
                }
            }
        }
        public int EventId
        {
            get
            {
                int eventid;
                return int.TryParse(hif_EventId.Value, out eventid) ? eventid : 0;
            }
            set
            {
                hif_EventId.Value = value.ToString();
            }
        }

        public EventPromoTemplateType TemplateType
        {
            get
            {
                int typeId;
                return int.TryParse(hif_TemplateType.Value, out typeId)
                    ? (EventPromoTemplateType)typeId
                    : EventPromoTemplateType.PponOnly;
            }
            set
            {
                hif_TemplateType.Value = Convert.ToString((int)value);
            }
        }

        public int ItemId
        {
            get
            {
                int itemid;
                return int.TryParse(hif_ItemId.Value, out itemid) ? itemid : 0;
            }
            set
            {
                hif_ItemId.Value = value.ToString();
            }
        }


        public int ItemCount
        {
            get
            {
                int itemcount;
                return int.TryParse(hif_ItemCount.Value, out itemcount) ? itemcount : 0;
            }
            set
            {
                hif_ItemCount.Value = value.ToString();
            }
        }
        public int VourcherItemCount
        {
            get
            {
                int itemcount;
                return int.TryParse(hif_VourcherItemCount.Value, out itemcount) ? itemcount : 0;
            }
            set
            {
                hif_VourcherItemCount.Value = value.ToString();
            }
        }
        public int VotePicItemCount
        {
            get
            {
                int itemcount;
                return int.TryParse(hif_VotePicItemCount.Value, out itemcount) ? itemcount : 0;
            }
            set
            {
                hif_VotePicItemCount.Value = value.ToString();
            }
        }
        public string ItemTitle
        {
            set { tbx_ItemTitle.Text = value; }
        }
        public string ItemDescription
        {
            set { tbx_ItemDescription.Text = value; }
        }

        public int VourcherEventPromoItemId
        {
            set
            {
                hidVourcherEventPromoItemId.Value = value.ToString(CultureInfo.InvariantCulture);
            }
            get
            {
                int vId;
                return int.TryParse(hidVourcherEventPromoItemId.Value, out vId) ? vId : 0;
            }
        }

        public int VourcherEventPromoItemItemId
        {
            set
            {
                hidVourcherEventPromoItemItemId.Value = value.ToString(CultureInfo.InvariantCulture);
            }
            get
            {
                int vId;
                return int.TryParse(hidVourcherEventPromoItemItemId.Value, out vId) ? vId : 0;
            }
        }

        public string VourcherCategory
        {
            set { tbx_VourcherCategory.Text = value; }
        }

        public string VourcherSubCategory
        {
            set { tbx_VourcherSubCategory.Text = value; }
        }

        public string VourcherSeq
        {
            set { tbx_VourcherSeq.Text = value; }
        }

        public string VourcherTitle
        {
            set { tbx_VourcherTitle.Text = value; }
        }

        public string VourcherDescription
        {
            set { tbx_VourcherDescription.Text = value; }
        }

        public string UserName
        {
            get { return User.Identity.Name; }
        }
        public EventPromoItemType ItemType
        {
            get
            {
                EventPromoItemType type = EventPromoItemType.TryParse(rdlItemType.SelectedValue, out type) ? type : EventPromoItemType.Ppon;
                return type;
            }
            set
            {
                rdlItemType.SelectedValue = ((int)value).ToString();
            }
        }

        public EventPromoType EventType
        {
            get { return EventPromoType.Ppon; }
        }

        public bool IsLimitInThreeMonth
        {
            get
            {
                return cbx_LimitInThreeMonth.Checked;
            }
        }

        public int[] BindingCategoryId
        {
            get
            {
                List<int> binding = new List<int>();
                for (int i = 0; i < cbl_CategoryBind.Items.Count; i++)
                {
                    if (cbl_CategoryBind.Items[i].Selected)
                    {
                        binding.Add(int.Parse(cbl_CategoryBind.Items[i].Value));
                    }
                }

                return binding.ToArray();
            }

            set
            {
                if (value == null)
                {
                    cbl_CategoryBind.ClearSelection();
                }
                else if (value.Length > 0)
                {
                    for (int i = 0; i < cbl_CategoryBind.Items.Count; i++)
                    {
                        cbl_CategoryBind.Items[i].Selected = value != null && value.Any(x => x == int.Parse(cbl_CategoryBind.Items[i].Value));
                    }
                }
            }
        }
        #region 設定商品頁
        /// <summary>
        /// 活動商品狀態 (未上檔/已上檔/已下檔/已售完/已結檔)
        /// </summary>
        /// <remarks>判斷式in EventPromoSetUpPresenter</remarks>
        public string ItemStatus { get; set; }

        public bool EventUseDiscount { get; set; }
        #endregion
        
        #endregion
        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
                InitialControls();
            }
            this.Presenter.OnViewLoaded();
        }
        protected void AddEventPromo(object sender, CommandEventArgs e)
        {
            int mode;
            if (int.TryParse(e.CommandArgument.ToString(), out mode))
            {
                Mode = mode;
            }
        }

        protected void btnClearCache_Click(object sender, EventArgs e)
        {
            SortedDictionary<string, string> serverIPs =
                ProviderFactory.Instance().GetSerializer().Deserialize<SortedDictionary<string, string>>(cp.ServerIPs);
            string result = SystemFacade.SyncCommand(serverIPs, SystemFacade._CURATION_CLEAR_CACHE, string.Empty, string.Empty);
            litMessage.Text = result.Replace("\r\n", "<br />");
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                List<Tuple<string, string, string>> result = new List<Tuple<string, string, string>>();
                Dictionary<string, int> cateList = new Dictionary<string, int>();
                Dictionary<KeyValuePair<string, string>, int> subCateList = new Dictionary<KeyValuePair<string, string>, int>();
                using (StreamReader reader = new StreamReader(FileUpload1.FileContent, System.Text.Encoding.Default))
                {
                    do
                    {
                        string bid;
                        string textLine = reader.ReadLine();
                        if (textLine.IndexOf(',') > 0)
                        {
                            string[] line = textLine.Split(',');
                            bid = line[0];
                            string cat = line[1].Trim();
                            if (!cateList.ContainsKey(cat))
                            {
                                int c = cateList.Count() + 1;
                                cateList.Add(cat, c);
                            }
                            cat = cateList[cat] + "." + cat;

                            if (line.Length > 2)
                            {
                                string subcat = line[2].Trim();

                                KeyValuePair<string, string> subKey = new KeyValuePair<string, string>(cat, subcat);
                                if (subCateList.Any(x => x.Key.Key == cat))
                                {
                                    if (!subCateList.ContainsKey(subKey))
                                    {
                                        int c = subCateList.Where(x => x.Key.Key == cat).Count() + 1;
                                        subCateList.Add(subKey, c);
                                    }
                                }
                                else
                                {
                                    subCateList.Add(subKey, 1);
                                }
                                subcat = cat.Substring(0, cat.IndexOf('.')) + "." + subCateList[subKey] + subcat;

                                result.Add(new Tuple<string, string, string>(bid, cat, subcat));
                            }
                            else
                            {
                                result.Add(new Tuple<string, string, string>(bid, cat, string.Empty));
                            }
                        }
                        else
                        {
                            bid = textLine;
                            result.Add(new Tuple<string, string, string>(bid, string.Empty, string.Empty));
                        }
                    } while (reader.Peek() != -1);
                    reader.Close();
                }
                if (this.ImportBidList != null)
                {
                    this.ImportBidList(this, new DataEventArgs<List<Tuple<string, string, string>>>(result));
                }
            }
        }
        protected void SavePromo(object sender, EventArgs e)
        {

            lab_Message.Text = string.Empty;
            DateTime d_start, d_end;
            if (DateTime.TryParse(tbx_Start.Text + " " + ddl_StartH.SelectedValue + ":" + ddl_Startm.SelectedValue, out d_start) && DateTime.TryParse(tbx_End.Text + " " + ddl_EndH.SelectedValue + ":" + ddl_Endm.SelectedValue, out d_end))
            {
                if (string.IsNullOrEmpty(tbx_BgColor.Text) && string.IsNullOrEmpty(tbx_BgPic.Text))
                {
                    RenderCKEditor();
                    tbx_Html.Text = HttpUtility.HtmlDecode(tbx_Html.Text);
                    txtMainPic.Text = HttpUtility.HtmlDecode(txtMainPic.Text);
                    txtMobileMainPic.Text = HttpUtility.HtmlDecode(txtMobileMainPic.Text);
                    tbx_DealPromoDescription.Text = HttpUtility.HtmlDecode(tbx_DealPromoDescription.Text);
                    tbx_EventIntroduction.Text = HttpUtility.HtmlDecode(tbx_EventIntroduction.Text);
                    lab_Message.Text = "請填入背景圖或背景色";
                }
                else if (ddlTemplateType.SelectedValue == ((int)EventPromoTemplateType.PponOnly).ToString()
                    && BindingCategoryId.Length > 0)
                {
                    lab_Message.Text = "投票版型無法綁定團購類別";
                }
                else
                {


                    EventPromoUpdateModel model = new EventPromoUpdateModel();

                    EventPromo promo = new EventPromo();
                    if (Mode == 2)
                    {
                        promo.Id = EventId;
                    }
                    else
                    {
                        //新增 設定預設值
                        promo.ShowInWeb = true;
                        promo.ShowInApp = false;
                    }



                    promo.Title = tbx_EventName.Text;
                    promo.EventPromoTitle = tbx_EventPromoTitle.Text;
                    promo.Url = tbx_Url.Text;
                    promo.Cpa = tbx_Cpa.Text;
                    promo.StartDate = d_start;
                    promo.EndDate = d_end;
                    promo.MainPic = HttpUtility.HtmlDecode(txtMainPic.Text);
                    promo.MobileMainPic = HttpUtility.HtmlDecode(txtMobileMainPic.Text);
                    promo.EventIntroduction = HttpUtility.HtmlDecode(tbx_EventIntroduction.Text);
                    promo.BgPic = tbx_BgPic.Text;
                    promo.BgColor = tbx_BgColor.Text.Replace("#", string.Empty);
                    promo.Description = HttpUtility.HtmlDecode(tbx_Html.Text);
                    promo.Creator = UserName;
                    promo.Cdt = DateTime.Now;
                    promo.Status = false;
                    promo.BtnActive = tbx_BtnAvtive.Text;
                    promo.BtnHover = tbx_BtnHover.Text;
                    promo.BtnOriginal = tbx_BtnOriginal.Text;
                    promo.BtnFontColor = tbx_BtnFontColor.Text.Replace("#", string.Empty);
                    promo.BtnFontColorHover = tbx_BtnFontColorHover.Text.Replace("#", string.Empty);
                    promo.BtnFontColorActive = tbx_BtnFontColorActive.Text.Replace("#", string.Empty);
                    promo.Type = (int)EventPromoType.Ppon;
                    int type = int.TryParse(ddlTemplateType.SelectedValue, out type)
                        ? type
                        : (int)EventPromoTemplateType.PponOnly;
                    promo.TemplateType = type;
                    promo.BindCategoryList = string.Join(",", BindingCategoryId);
                    promo.DealPromoTitle = tbx_DealPromoTitle.Text;
                    promo.DealPromoDescription = HttpUtility.HtmlDecode(tbx_DealPromoDescription.Text);
                    promo.SubCategoryBgColor = txtSubCagegoryBackgroundColor.Text.Replace("#", string.Empty);
                    promo.SubCategoryFontColor = txtSubCategoryFontColor.Text.Replace("#", string.Empty);
                    promo.SubCategoryShowImage = rdoShowImage.Checked;
                    //按鈕底色
                    promo.SubCategoryBtnColorOriginal = txtSubCategoryBtnColorOriginal.Text.Replace("#", string.Empty);
                    promo.SubCategoryBtnColorHover = txtSubCategoryBtnColorHover.Text.Replace("#", string.Empty);
                    promo.SubCategoryBtnColorActive = txtSubCategoryBtnColorActive.Text.Replace("#", string.Empty);
                    //圖片連結
                    promo.SubCategoryBtnImageOriginal = txtSubCategoryBtnImageOriginal.Text;
                    promo.SubCategoryBtnImageHover = txtSubCategoryBtnImageHover.Text;
                    promo.SubCategoryBtnImageActive = txtSubCategoryBtnImageActive.Text;
                    //文字顏色
                    promo.SubCategoryBtnFontOriginal = txtSubCategoryBtnFontOriginal.Text.Replace("#", string.Empty);
                    promo.SubCategoryBtnFontHover = txtSubCategoryBtnFontHover.Text.Replace("#", string.Empty);
                    promo.SubCategoryBtnFontActive = txtSubCategoryBtnFontActive.Text.Replace("#", string.Empty);
                    promo.EventType = Convert.ToInt32(ddlEventType.SelectedValue);

                    model.UpdateCmsRandomCities = new UpdateCmsRandomCitiesModel
                    {
                        Update = true,
                        CityIds = new List<int>()
                    };
                    foreach (ListItem item in cblCities.Items)
                    {
                        if (item.Selected)
                        {
                            model.UpdateCmsRandomCities.CityIds.Add(int.Parse(item.Value));
                        }
                    }

                    List<int> banners = new List<int>();
                    foreach (ListItem item in chbBannerLocation.Items)
                    {
                        if (item.Selected)
                        {
                            banners.Add(int.Parse(item.Value));
                        }
                    }
                    model.BannerType = banners;

                    if (type == (int)EventPromoTemplateType.VoteMix)
                    {
                        promo.VoteCycleType = rdo_VoteTypeEveryday.Checked
                            ? (byte)EventPromoVoteCycleType.Everyday
                            : (byte)EventPromoVoteCycleType.AllEventCycle;
                        int votes = int.TryParse(tbx_CycleMaxVotes.Text, out votes) ? votes : 0;
                        if (votes <= 0)
                        {
                            lab_Message.Text = "請設定每周期票數";
                            return;
                        }
                        promo.MaxVotes = votes;

                        int campaignId = int.TryParse(tbx_CampaignId.Text, out campaignId) ? campaignId : 0;
                        if (campaignId > 0)
                        {
                            var result = PromotionFacade.IsExistDiscountCampaignId(campaignId);
                            switch (result)
                            {
                                case InstantGenerateDiscountCampaignResult.Success:
                                    promo.CampaignId = campaignId;
                                    break;
                                case InstantGenerateDiscountCampaignResult.NotExist:
                                    lab_Message.Text = "折價券ID不存在";
                                    return;
                                case InstantGenerateDiscountCampaignResult.Expire:
                                    lab_Message.Text = "折價券已過期";
                                    return;
                                case InstantGenerateDiscountCampaignResult.TypeMistake:
                                    lab_Message.Text = "折價券必需為即時產生類型";
                                    return;
                                case InstantGenerateDiscountCampaignResult.Unaudited:
                                    lab_Message.Text = "折價券未審核通過";
                                    return;
                                case InstantGenerateDiscountCampaignResult.Cancel:
                                    lab_Message.Text = "折價券已作廢";
                                    return;
                            }
                        }
                    }
                    else
                    {
                        promo.VoteCycleType = (byte)EventPromoVoteCycleType.None;
                        promo.MaxVotes = 0;
                        promo.CampaignId = 0;
                    }

                    if (SaveEventPromo != null)
                    {

                        PromoSeoKeyword seo = new PromoSeoKeyword();
                        seo.PromoType = (int)PromoSEOType.EventPromo;
                        seo.ActivityId = promo.Id;
                        seo.ModifyId = Page.User.Identity.Name;
                        seo.CreateId = Page.User.Identity.Name;
                        seo.Description = tbx_SEOd.Text;
                        seo.Keyword = tbx_SEOk.Text;

                        model.SeoKeyword = seo;

                        model.MainEvent = promo;
                        model.AppBannerPostedFile = fuAppBannerImage.PostedFile.ContentLength > 0
                            ? fuAppBannerImage.PostedFile
                            : null;
                        model.AppPromoPostedFile = fuAppPromoImage.PostedFile.ContentLength > 0
                            ? fuAppPromoImage.PostedFile
                            : null;
                        model.DealPromoImagePostedFile = fuDealPromoImage.PostedFile.ContentLength > 0
                            ? fuDealPromoImage.PostedFile
                            : null;
                        SaveEventPromo(sender, new DataEventArgs<EventPromoUpdateModel>(model));

                    }
                }
            }
            else
                lab_Message.Text = "日期格式錯誤";
        }
        protected void gv_EventPromo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is EventPromo)
            {
                EventPromo promo = (EventPromo)e.Row.DataItem;
                Label lblLink = (Label)e.Row.FindControl("lblLink");
                Literal litCity = (Literal)e.Row.FindControl("litCity");
                HyperLink lkPreview = (HyperLink)e.Row.FindControl("lkPreview");
                CheckBox checkBoxShowInApp = (CheckBox)e.Row.FindControl("ShowInApp");
                CheckBox checkBoxShowInWeb = (CheckBox)e.Row.FindControl("ShowInWeb");

                checkBoxShowInApp.InputAttributes.Add("value", promo.Id.ToString());
                checkBoxShowInWeb.InputAttributes.Add("value", promo.Id.ToString());
                var url = PromotionFacade.GetCurationLink(promo);
                lblLink.Text = string.Format("<a href='{0}' target='_blank'>{0}</a>", url);
                lkPreview.NavigateUrl = PromotionFacade.GetCurationPreviewLink(promo);

                litCity.Text = string.Join("、", CmsRandomFacade.GetEventPromoCityNames(promo.Id));
                if (!string.IsNullOrEmpty(promo.BindCategoryList))
                {
                    ((LinkButton)e.Row.FindControl("lbt_ShowItemList")).Enabled = false;
                }
            }
        }
        protected void gvEventPromoCommand(object sender, GridViewCommandEventArgs e)
        {
            lab_Message.Text = string.Empty;
            int id;
            if (int.TryParse(e.CommandArgument.ToString(), out id))
            {
                EventId = id;

                switch (e.CommandName)
                {
                    case "EditEvent":
                        if (GetEventPromo != null)
                            GetEventPromo(sender, e);
                        break;
                    case "ChangeStatus":
                        if (UpdateEventPromoStatus != null)
                            UpdateEventPromoStatus(sender, e);
                        break;
                    case "ShowItemList":
                        hidVourcherEventPromoItemId.Value = hidVourcherEventPromoItemItemId.Value = "0";
                        var row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                        TemplateType = (EventPromoTemplateType)int.Parse(((HiddenField)row.FindControl("hidEventPromoTemplateType")).Value);
                        if (GetEventPromoItemList != null)
                            GetEventPromoItemList(sender, e);
                        break;
                }
            }
            else
                lab_Message.Text = "Id錯誤";
        }

        protected void ShowInWebChange(object sender, EventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;
            //取出活動ID
            int id;
            if (int.TryParse(checkBox.InputAttributes["value"], out id))
            {
                //先將活動ID儲存於EventId參數中，之後供Presenter使用
                EventId = id;
                if (SetEventPromoShowInWeb != null)
                {
                    //異動資料
                    SetEventPromoShowInWeb(sender, new DataEventArgs<bool>(checkBox.Checked));
                }
            }

        }
        protected void ShowInAppChange(object sender, EventArgs e)
        {
            CheckBox checkBox = (CheckBox)sender;
            //取出活動ID
            int id;
            if (int.TryParse(checkBox.InputAttributes["value"], out id))
            {
                //先將活動ID儲存於EventId參數中，之後供Presenter使用
                EventId = id;
                if (SetEventPromoShowInApp != null)
                {
                    //異動資料
                    SetEventPromoShowInApp(sender, new DataEventArgs<bool>(checkBox.Checked));
                }
            }
        }


        protected void rptEventPromoItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            lab_Message.Text = string.Empty;
            int id;
            if (int.TryParse(e.CommandArgument.ToString(), out id))
            {
                ItemId = id;
                switch (e.CommandName)
                {
                    case "ChangeItemStatus":
                        if (UpdateEventPromoItemStatus != null)
                            UpdateEventPromoItemStatus(sender, e);
                        break;
                    case "EditItem":
                        if (GetEventPromoItem != null)
                            GetEventPromoItem(sender, e);
                        break;
                    case "DeleteEventPromoItem":
                        if (DeleteEventPromoItem != null)
                            DeleteEventPromoItem(sender, new DataEventArgs<int>(id));
                        break;
                }
            }
            else
                lab_Message.Text = "Id錯誤";
        }
        protected void CheckItemId(object sender, EventArgs e)
        {
            lab_Message.Text = string.Empty;

            int itemId = 0;
            Guid guidId;
            if (Guid.TryParse(tbx_ItemId.Text, out guidId))
            {
                IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(guidId);
                if (vpd.IsLoaded)
                {
                    itemId = vpd.UniqueId.GetValueOrDefault();
                }
            }
            else
            {
                int.TryParse(tbx_ItemId.Text, out itemId);
            }

            if (itemId != 0)
            {
                EventPromoItemType type;
                if (ItemType == EventPromoItemType.PicVote)
                {
                    type = rd_PponLink.Checked ? EventPromoItemType.Ppon : EventPromoItemType.Vourcher;
                }
                else
                {
                    type = ItemType;
                }
                if (GetPponDeal != null)
                {
                    GetPponDeal(sender,
                        new DataEventArgs<KeyValuePair<int, EventPromoItemType>>(new KeyValuePair<int, EventPromoItemType>(itemId, type)));
                }
            }
            else
            {
                lab_Message.Text = "ItemId格式錯誤";
            }
        }
        protected void SavePromoItem(object sender, EventArgs e)
        {
            lab_Message.Text = string.Empty;
            int seq;
            int itemId = 0;
            if (ItemType == EventPromoItemType.PicVote && fuUploadVotePic.PostedFile.ContentLength <= 0 && img_vote.ImageUrl == string.Empty)
            {
                lab_Message.Text = "必需上傳圖片";
                return;
            }

            Guid guidId;
            if (Guid.TryParse(tbx_ItemId.Text, out guidId))
            {
                IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(guidId);
                if (vpd.IsLoaded)
                {
                    itemId = vpd.UniqueId.GetValueOrDefault();
                }
            }
            else
            {
                int.TryParse(tbx_ItemId.Text, out itemId);
            }

            if (itemId != 0 && int.TryParse(tbx_Seq.Text, out seq))
            {
                var itemMode = new EventPromoItemUpdateMode();
                var epitems = new List<EventPromoItem>();

                EventPromoItem item = new EventPromoItem();
                if (Mode == 5)
                    item.Id = ItemId;
                item.ItemId = itemId;
                item.Title = tbx_ItemTitle.Text;
                item.Description = tbx_ItemDescription.Text;
                item.Seq = seq;
                item.Creator = UserName;
                item.Cdt = DateTime.Now;
                item.Status = true;
                item.Category = tbx_Category.Text.Trim();
                item.SubCategory = tbx_SubCategory.Text.Trim();
                item.ItemType = (int)ItemType;
                if (ItemType == EventPromoItemType.PicVote)
                {
                    item.ItemPicUrl = hif_itemPicUrl.Value;
                    item.LinkItemType = rd_PponLink.Checked ? (int)EventPromoItemType.Ppon : (int)EventPromoItemType.Vourcher;
                }

                if (VourcherEventPromoItemItemId != 0)
                {
                    int vourcherSeq, vourcherItemId;
                    if (int.TryParse(txt_VourcherId.Text, out vourcherItemId) &&
                        int.TryParse(tbx_VourcherSeq.Text, out vourcherSeq))
                    {
                        var vourcherItem = new EventPromoItem();
                        vourcherItem.Id = VourcherEventPromoItemId;
                        vourcherItem.ItemId = VourcherEventPromoItemItemId;
                        vourcherItem.Title = tbx_VourcherTitle.Text;
                        vourcherItem.Description = tbx_VourcherDescription.Text;
                        vourcherItem.Seq = vourcherSeq;
                        vourcherItem.Creator = UserName;
                        vourcherItem.Category = tbx_VourcherCategory.Text;
                        vourcherItem.SubCategory = tbx_VourcherSubCategory.Text;
                        vourcherItem.Cdt = DateTime.Now;
                        vourcherItem.Status = true;
                        vourcherItem.ItemType = (int)EventPromoItemType.Vourcher;

                        epitems.Add(vourcherItem);
                        epitems.Add(item);

                        itemMode.PromoItems = epitems;

                        if (SaveEventPromoItemWithVourcher != null)
                            SaveEventPromoItemWithVourcher(sender, new DataEventArgs<EventPromoItemUpdateMode>(itemMode));
                    }
                    else
                    {
                        lab_Message.Text = "優惠券編號或排序格式錯誤";
                    }
                }
                else
                {
                    if (SaveEventPromoItem != null)
                    {
                        epitems.Add(item);
                        itemMode.PromoItems = epitems;
                        if (item.ItemType == (int)EventPromoItemType.PicVote && fuUploadVotePic.PostedFile.ContentLength > 0)
                        {
                            itemMode.VotePic = fuUploadVotePic.PostedFile.ContentLength > 0 ? fuUploadVotePic.PostedFile : null;
                        }
                        SaveEventPromoItem(sender, new DataEventArgs<EventPromoItemUpdateMode>(itemMode));
                    }
                }
            }
            else
                lab_Message.Text = "ItemId格式或排序錯誤";
        }
        protected void LimitInThreeMonth_CheckedChanged(object sender, EventArgs e)
        {
            if (GetEventPromoList != null)
            {
                GetEventPromoList(sender, e);
            }
        }

        protected void btn_SaveSort_Click(object sender, EventArgs e)
        {
            int mainId = Convert.ToInt32(hif_EventId.Value);

            Dictionary<int, int> dictionaryIdAndSeq = new Dictionary<int, int>();//event_promo_item.Id, event_promo_item.Seq

            foreach (RepeaterItem item in rpt_EventPromoItem.Items)
            {
                HiddenField hidIdField = (HiddenField)item.FindControl("HidId");
                int id = hidIdField == null ? 0 : Convert.ToInt32(hidIdField.Value);
                if (id != 0)
                {
                    HiddenField hidSeqField = (HiddenField)item.FindControl("HidTempSeq");
                    int newSeq = hidSeqField == null ? 0 : Convert.ToInt32(hidSeqField.Value);

                    dictionaryIdAndSeq.Add(id, newSeq);
                }

            }
            if (UpdateEventPromoItemSeq != null)
            {
                UpdateEventPromoItemSeq(this, new DataEventArgs<KeyValuePair<int, Dictionary<int, int>>>
                    (new KeyValuePair<int, Dictionary<int, int>>(mainId, dictionaryIdAndSeq)));
            }
        }

        /// <summary>
        /// 取得商品上檔狀態
        /// </summary>
        /// <param name="promoItem">主題活動商品</param>
        /// <returns></returns>
        protected string GetItemStatus(ViewEventPromoItem promoItem)
        {
            if (GetEventPromoItemStatus != null)
            {
                string itemStatus = GetEventPromoItemStatus(promoItem);
                return itemStatus;
            }
            return string.Empty;
        }
        
        #endregion
        #region method

        private void RenderCKEditor()
        {
            ScriptManager.RegisterStartupScript(this, typeof(Button), "Html", "$('#" + tbx_Html.ClientID + "').ckeditor();", true);
            ScriptManager.RegisterStartupScript(this, typeof(Button), "MainPic", "$('#" + txtMainPic.ClientID + "').ckeditor();", true);
            ScriptManager.RegisterStartupScript(this, typeof(Button), "DealDesc", "$('#" + tbx_DealPromoDescription.ClientID + "').ckeditor();", true);
            ScriptManager.RegisterStartupScript(this, typeof(Button), "MobileMainPic", "$('#" + txtMobileMainPic.ClientID + "').ckeditor();", true);
            ScriptManager.RegisterStartupScript(this, typeof(Button), "EventIntroduction", "$('#" + tbx_EventIntroduction.ClientID + "').ckeditor();", true);
        }

        public void SeTEventPromoItemList(EventPromoItemCollection items)
        {
            if (WeChatSendTestMessage != null)
            { }
        }

        public void ChangePanel(int mode, string return_message = "")
        {
            lab_Message.Text = string.Empty;
            pan_EventEdit.Visible = pan_EventList.Visible = pan_ItemList.Visible = pan_ItemEdit.Visible = false;
            switch (mode)
            {
                case 0:
                    pan_EventList.Visible = true;
                    btn_Save.Text = "新增";
                    lab_Message.Text = return_message;
                    break;
                case 1:
                    RenderCKEditor();
                    pan_EventEdit.Visible = true;
                    tbx_CampaignId.Text = tbx_CycleMaxVotes.Text = tbx_EventName.Text = tbx_Url.Text = tbx_Cpa.Text = tbx_Start.Text = tbx_SEOd.Text = tbx_SEOk.Text
                    = tbx_EventIntroduction.Text = tbx_End.Text = txtMainPic.Text = txtMobileMainPic.Text = tbx_BgPic.Text = tbx_BgColor.Text
                    = tbx_EventPromoTitle.Text = tbx_Html.Text = tbx_DealPromoDescription.Text = tbx_DealPromoTitle.Text = string.Empty;
                    rdo_VoteTypeEveryday.Checked = true;
                    ddlTemplateType.SelectedIndex = 0;

                    if (IsCurationFunction)
                    {
                        #region 策展預設值

                        long ticks = DateTime.Now.Ticks;
                        string id = Helper.GetNewGuid(ticks).ToString();
                        tbx_Url.Text = DateTime.Now.Year + "_" + id.Substring(0, id.IndexOf('-'));
                        tbx_BgColor.Text = "#F2F2F0";
                        tbx_Cpa.Text = "17_Curation";
                        #endregion
                    }

                    break;
                case 2:
                    RenderCKEditor();
                    pan_EventEdit.Visible = true;
                    btn_Save.Text = "更新";
                    break;
                case 3:
                    pan_ItemList.Visible = true;
                    EnableManuallySort();
                    break;
                case 4:
                    pan_ItemEdit.Visible = true;
                    btn_ItemSave.Text = "新增";
                    tbx_VourcherCategory.Text = tbx_VourcherSubCategory.Text = lab_VourcherItemStatus.Text = tbx_VourcherDescription.Text = tbx_VourcherTitle.Text = tbx_VourcherSeq.Text = txt_VourcherId.Text = tbx_Seq.Text = tbx_ItemTitle.Text = tbx_ItemDescription.Text = lab_ItemStatus.Text = tbx_ItemId.Text = tbx_Category.Text = tbx_SubCategory.Text = string.Empty;
                    VourcherEventPromoItemId = VourcherEventPromoItemItemId = 0;
                    img_vote.Visible = btn_RemoveLinkVourcherId.Enabled = rdoPromoItem.Checked = ph_VourcherInfo.Visible = false;
                    rdoVourcher.Enabled = rdoPromoItem.Enabled = rdoVourcher.Checked = txt_VourcherId.Enabled = btn_LinkVourcherId.Enabled = true;
                    rdlItemType.SelectedIndex = 0;
                    img_vote.ImageUrl = "";
                    break;
                case 5:
                    pan_ItemEdit.Visible = true;
                    btn_ItemSave.Text = "更新";
                    break;
            }
        }

        public void SetEventPromo(PromoSeoKeyword seo, EventPromo promo, bool enableBindingCategory = false)
        {

            Mode = 2;
            tbx_EventName.Text = promo.Title;
            tbx_EventPromoTitle.Text = promo.EventPromoTitle;
            tbx_Url.Text = promo.Url;
            tbx_Cpa.Text = promo.Cpa;
            tbx_Start.Text = promo.StartDate.ToString("MM/dd/yyyy");
            ddl_StartH.SelectedValue = promo.StartDate.ToString("HH");
            ddl_Startm.SelectedValue = promo.StartDate.ToString("mm");
            tbx_End.Text = promo.EndDate.ToString("MM/dd/yyyy");
            ddl_EndH.SelectedValue = promo.EndDate.ToString("HH");
            ddl_Endm.SelectedValue = promo.EndDate.ToString("mm");
            txtMainPic.Text = promo.MainPic;
            txtMobileMainPic.Text = promo.MobileMainPic;
            tbx_EventIntroduction.Text = promo.EventIntroduction;
            tbx_BgPic.Text = promo.BgPic;
            tbx_BgColor.Text = promo.BgColor;
            tbx_Html.Text = promo.Description;
            tbx_BtnAvtive.Text = promo.BtnActive;
            tbx_BtnHover.Text = promo.BtnHover;
            tbx_BtnOriginal.Text = promo.BtnOriginal;
            tbx_BtnFontColor.Text = promo.BtnFontColor;
            tbx_BtnFontColorHover.Text = promo.BtnFontColorHover;
            tbx_BtnFontColorActive.Text = promo.BtnFontColorActive;
            appBannerImage.ImageUrl = ImageFacade.GetMediaPath(promo.AppBannerImage, MediaType.EventPromoAppBanner);
            appPromoImage.ImageUrl = ImageFacade.GetMediaPath(promo.AppPromoImage, MediaType.EventPromoAppMainImage);
            imgDealPromoImage.ImageUrl = ImageFacade.GetMediaPath(promo.DealPromoImage, MediaType.DealPromoImage);
            ddlTemplateType.SelectedValue = promo.TemplateType.ToString();
            tbx_DealPromoDescription.Text = promo.DealPromoDescription;
            tbx_DealPromoTitle.Text = promo.DealPromoTitle;
            txtSubCagegoryBackgroundColor.Text = promo.SubCategoryBgColor;
            txtSubCategoryFontColor.Text = promo.SubCategoryFontColor;
            rdoShowImage.Checked = promo.SubCategoryShowImage;
            rdoShowColor.Checked = promo.SubCategoryShowImage ? false : true;
            txtSubCategoryBtnColorOriginal.Text = promo.SubCategoryBtnColorOriginal;
            txtSubCategoryBtnColorHover.Text = promo.SubCategoryBtnColorHover;
            txtSubCategoryBtnColorActive.Text = promo.SubCategoryBtnColorActive;
            txtSubCategoryBtnImageOriginal.Text = promo.SubCategoryBtnImageOriginal;
            txtSubCategoryBtnImageHover.Text = promo.SubCategoryBtnImageHover;
            txtSubCategoryBtnImageActive.Text = promo.SubCategoryBtnImageActive;
            txtSubCategoryBtnFontOriginal.Text = promo.SubCategoryBtnFontOriginal;
            txtSubCategoryBtnFontHover.Text = promo.SubCategoryBtnFontHover;
            txtSubCategoryBtnFontActive.Text = promo.SubCategoryBtnFontActive;
            rdo_VoteTypeEveryday.Checked = promo.VoteCycleType == (int)EventPromoVoteCycleType.Everyday;
            rdo_VoteTypeAllCycle.Checked = promo.VoteCycleType == (int)EventPromoVoteCycleType.AllEventCycle;
            tbx_CycleMaxVotes.Text = promo.MaxVotes.ToString(CultureInfo.InvariantCulture);
            tbx_CampaignId.Text = promo.CampaignId.ToString(CultureInfo.InvariantCulture);
            ddlEventType.SelectedValue = promo.EventType.ToString();


            tbx_SEOd.Text = seo.Description;
            tbx_SEOk.Text = seo.Keyword;

            BindingCategoryId = null;
            if (enableBindingCategory)
            {
                if (!string.IsNullOrEmpty(promo.BindCategoryList))
                {
                    BindingCategoryId = promo.BindCategoryList.Split(',').Select(int.Parse).ToArray();
                }
            }
            else
            {
                cbl_CategoryBind.Enabled = false;
            }


            if ((promo.BannerType & (int)EventBannerType.MainDown) > 0)
            {
                chbBannerLocation.Items.FindByValue(((int)EventBannerType.MainDown).ToString()).Selected = true;
            }

            if ((promo.BannerType & (int)EventBannerType.ChannelUp) > 0)
            {
                chbBannerLocation.Items.FindByValue(((int)EventBannerType.ChannelUp).ToString()).Selected = true;
            }

            if ((promo.BannerType & (int)EventBannerType.ChannelDown) > 0)
            {
                chbBannerLocation.Items.FindByValue(((int)EventBannerType.ChannelDown).ToString()).Selected = true;
            }
        }

        public void SetEventPromoList(EventPromoCollection promos)
        {
            gv_EventPromo.DataSource = promos.OrderByDescending(x => x.Id);
            gv_EventPromo.DataBind();
        }

        public void SetEventPromoItemList(ViewEventPromoItemCollection items)
        {
            ItemCount = items.Count;
            rpt_EventPromoItem.DataSource = items.OrderBy(x => x.Seq).ThenBy(x => x.Category).ThenBy(x => x.SubCategory).ToList();
            rpt_EventPromoItem.DataBind();
        }

        public void SetVotePicEventPromoItemList(ViewEventPromoItemPicVoteCollection items)
        {
            VotePicItemCount = items.Count;
            rptVotePic.DataSource = items.OrderBy(x => x.Category).ThenBy(x => x.Seq);
            rptVotePic.DataBind();
        }

        public void SetVourcherEventPromoItemList(Dictionary<EventPromoItem, DateTime> items)
        {
            VourcherItemCount = items.Count;
            rptEventPromoItemVourcher.DataSource = items.OrderBy(x => x.Key.Seq);
            rptEventPromoItemVourcher.DataBind();
        }

        public void SetEventPromoItem(DateTime dateStart)
        {
            switch (ItemType)
            {
                case EventPromoItemType.Ppon:
                    tbx_Seq.Text = (ItemCount + 1).ToString();
                    break;
                case EventPromoItemType.Vourcher:
                    tbx_Seq.Text = (VourcherItemCount + 1).ToString();
                    break;
                case EventPromoItemType.PicVote:
                    tbx_Seq.Text = (VotePicItemCount + 1).ToString();
                    break;
                default:
                    break;
            }
            lab_ItemStatus.Text = dateStart <= DateTime.Now ? "已開賣" : ("未開賣 (開賣時間:" + dateStart.ToString("yyyy/MM/dd") + ")");
        }

        public void SetVourcherEventPromoItem(DateTime dateStart)
        {
            ph_VourcherInfo.Visible = true;
            tbx_VourcherSeq.Text = (VourcherItemCount + 1).ToString();
            lab_VourcherItemStatus.Text = dateStart <= DateTime.Now ? "已開賣" : ("未開賣 (開賣時間:" + dateStart.ToString("yyyy/MM/dd") + ")");
            txt_VourcherId.Enabled = false;
            btn_LinkVourcherId.Enabled = false;
            btn_RemoveLinkVourcherId.Enabled = true;
        }

        public void SetEventPromoItem(EventPromoItem item, EventPromoItem linkItem, DateTime dateStart, DateTime linkItemDateStart, EventPromoTemplateType tempLateType)
        {
            Mode = 5;
            tbx_ItemId.Text = item.ItemId.ToString();
            tbx_Seq.Text = item.Seq.ToString();
            tbx_ItemTitle.Text = item.Title;
            tbx_ItemDescription.Text = item.Description;
            lab_ItemStatus.Text = dateStart <= DateTime.Now ? "已開賣" : ("未開賣 (開賣時間:" + dateStart.ToString("yyyy/MM/dd") + ")");
            tbx_Category.Text = item.Category;
            tbx_SubCategory.Text = item.SubCategory;
            ItemType = (EventPromoItemType)item.ItemType;
            tbx_Category.Enabled = tbx_SubCategory.Enabled = tempLateType != EventPromoTemplateType.Commercial;
            img_vote.Visible = false;
            img_vote.ImageUrl = string.Empty;

            if (item.ItemType == (int)EventPromoItemType.PicVote)
            {
                rd_PponLink.Checked = item.LinkItemType == (int)EventPromoItemType.Ppon;
                rd_VourcherLink.Checked = item.LinkItemType == (int)EventPromoItemType.Vourcher;

                var imgUrl = ImageFacade.GetMediaPath(item.ItemPicUrl, MediaType.DealPromoImage);
                if (!string.IsNullOrEmpty(imgUrl))
                {
                    img_vote.Visible = true;
                    img_vote.ImageUrl = imgUrl;
                    hif_itemPicUrl.Value = item.ItemPicUrl;
                }
            }

            if (tempLateType == EventPromoTemplateType.VoteMix)
            {
                if (linkItem.IsLoaded)
                {
                    rdoPromoItem.Checked = true;
                    txt_VourcherId.Text = linkItem.Id.ToString(CultureInfo.InvariantCulture);
                    rdoPromoItem.Enabled = rdoVourcher.Enabled = txt_VourcherId.Enabled = btn_LinkVourcherId.Enabled = false;
                    btn_RemoveLinkVourcherId.Enabled = true;
                    hidVourcherEventPromoItemId.Value = linkItem.Id.ToString(CultureInfo.InvariantCulture);
                    hidVourcherEventPromoItemItemId.Value = linkItem.ItemId.ToString(CultureInfo.InvariantCulture);

                    ph_VourcherInfo.Visible = true;
                    tbx_VourcherCategory.Text = linkItem.Category;
                    tbx_VourcherSubCategory.Text = linkItem.SubCategory;
                    tbx_VourcherSeq.Text = linkItem.Seq.ToString(CultureInfo.InvariantCulture);
                    tbx_VourcherTitle.Text = linkItem.Title;
                    tbx_VourcherDescription.Text = linkItem.Description;
                    lab_VourcherItemStatus.Text = linkItemDateStart <= DateTime.Now
                        ? "已開賣"
                        : ("未開賣 (開賣時間:" + linkItemDateStart.ToString("yyyy/MM/dd") + ")");
                }
                else
                {
                    rdoPromoItem.Enabled = rdoVourcher.Enabled = txt_VourcherId.Enabled = rdoVourcher.Checked = btn_LinkVourcherId.Enabled = true;
                    rdoPromoItem.Checked = ph_VourcherInfo.Visible = btn_RemoveLinkVourcherId.Enabled = false;
                    hidVourcherEventPromoItemId.Value = hidVourcherEventPromoItemItemId.Value = "0";
                    txt_VourcherId.Text = tbx_VourcherCategory.Text = tbx_VourcherSubCategory.Text
                        = tbx_VourcherSeq.Text = tbx_VourcherTitle.Text = tbx_VourcherDescription.Text
                        = lab_VourcherItemStatus.Text = string.Empty;
                }
            }

            foreach (ListItem listItem in rdlItemType.Items)
            {
                EventPromoItemType type = EventPromoItemType.Ppon;
                if (EventPromoItemType.TryParse(listItem.Value, out type))
                {
                    switch (tempLateType)
                    {
                        case EventPromoTemplateType.JoinPiinlife:
                            listItem.Enabled = !type.Equals(EventPromoItemType.Vourcher);
                            break;
                        case EventPromoTemplateType.PponOnly:
                        case EventPromoTemplateType.Kind:
                        case EventPromoTemplateType.Zero:
                            listItem.Enabled = !type.EqualsAny(EventPromoItemType.Vourcher);
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        public void SetBindingCategory(CategoryCollection categories)
        {
            cbl_CategoryBind.Items.AddRange(categories.Select(x => new ListItem(x.Name, x.Id.ToString())).ToArray());
        }

        public void EnableManuallySort() {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "EnableManuallySort", "EnableManuallySort();", true);
        }

        public void AlertImportErrorMsg(string errorMsg)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "alertImportErrorMsg", string.Format("AlertImportErrorMsg(\"{0}\");", errorMsg), true);
        }
        #endregion
        #region pricate method
        private void InitialControls()
        {
            //ddlTemplateType.DataSource = Enum.GetValues(typeof(EventPromoTemplateType)).Cast<EventPromoTemplateType>().Select(x => new ListItem { Value = ((int)x).ToString(), Text = Helper.GetLocalizedEnum(x) }).Where(x => x.Value != Convert.ToString((int)EventPromoTemplateType.JoinPiinlife));
            ddlTemplateType.DataSource = Enum.GetValues(typeof(EventPromoTemplateType)).Cast<EventPromoTemplateType>().Select(x => new ListItem { Value = ((int)x).ToString(), Text = Helper.GetLocalizedEnum(x) });
            ddlTemplateType.DataValueField = "Value";
            ddlTemplateType.DataTextField = "Text";
            ddlTemplateType.DataBind();

            ddlEventType.DataSource = Enum.GetValues(typeof(EventPromoEventType)).Cast<EventPromoEventType>().Select(x => new ListItem { Value = ((int)x).ToString(), Text = Helper.GetLocalizedEnum(x) });
            ddlEventType.DataValueField = "Value";
            ddlEventType.DataTextField = "Text";
            ddlEventType.DataBind();

            if (IsCurationFunction)
            {
                ddlEventType.SelectedValue = ((int)EventPromoEventType.Curation).ToString();
                ddlEventType.Enabled = false;
            }
            else
            {
                ddlEventType.Enabled = true;
            }

            rdlItemType.DataSource = Enum.GetValues(typeof(EventPromoItemType)).Cast<EventPromoItemType>().Select(x => new ListItem { Value = ((int)x).ToString(), Text = Helper.GetLocalizedEnum(x) });
            rdlItemType.DataValueField = "Value";
            rdlItemType.DataTextField = "Text";
            rdlItemType.DataBind();
            rdlItemType.SelectedIndex = 0;

            for (int i = 0; i <= 23; i++)
            {
                ddl_StartH.Items.Add(new ListItem()
                {
                    Text = i.ToString("D2"),
                    Value = i.ToString("D2")
                });
                ddl_EndH.Items.Add(new ListItem()
                {
                    Text = i.ToString("D2"),
                    Value = i.ToString("D2")
                });
            }
            for (int i = 0; i <= 60; i++)
            {
                ddl_Startm.Items.Add(new ListItem()
                {
                    Text = i.ToString("D2"),
                    Value = i.ToString("D2")
                });
                ddl_Endm.Items.Add(new ListItem()
                {
                    Text = i.ToString("D2"),
                    Value = i.ToString("D2")
                });
            }
            ddl_StartH.DataBind();
            ddl_EndH.DataBind();
            ddl_Startm.DataBind();
            ddl_Endm.DataBind();
            
            chbBannerLocation.Items.Clear();
            chbBannerLocation.Items.Add(new ListItem("APP首頁下方產品列表輪播", ((int)EventBannerType.MainDown).ToString()));
            chbBannerLocation.Items.Add(new ListItem("APP指定頻道上方列表Banner", ((int)EventBannerType.ChannelUp).ToString()));
            chbBannerLocation.Items.Add(new ListItem("APP指定頻道產品列表輪播", ((int)EventBannerType.ChannelDown).ToString()));
            chbBannerLocation.DataBind();
        }
        #endregion

        protected void CheckVourcherId(object sender, EventArgs e)
        {
            int itemId;
            if (int.TryParse(txt_VourcherId.Text, out itemId))
            {
                if (GetVourcher != null)
                    GetVourcher(sender, new DataEventArgs<KeyValuePair<int, bool>>(new KeyValuePair<int, bool>(itemId, rdoVourcher.Checked)));
            }
        }

        protected void RemoveLinkVourcher(object sender, EventArgs e)
        {
            rdoVourcher.Enabled = rdoPromoItem.Enabled = txt_VourcherId.Enabled = btn_LinkVourcherId.Enabled = true;
            VourcherTitle = VourcherDescription = txt_VourcherId.Text = lab_VourcherItemStatus.Text = tbx_VourcherSeq.Text
                = tbx_VourcherSubCategory.Text = tbx_VourcherCategory.Text = string.Empty;
            btn_RemoveLinkVourcherId.Enabled = ph_VourcherInfo.Visible = false;
            hidVourcherEventPromoItemId.Value = hidVourcherEventPromoItemItemId.Value = "0";
        }

        public void SetEventPromoCities(List<PponCity> pponCities, ViewCmsRandomCollection cmsRandoms)
        {            
            cblCities.DataSource = pponCities;
            cblCities.DataTextField = "CityName";
            cblCities.DataValueField = "CityId";
            cblCities.DataBind();
            foreach (var cmsRandom in cmsRandoms)
            {
                cblCities.Items.FindByValue(cmsRandom.CityId.ToString()).Selected = true;
            }
        }
    }
}
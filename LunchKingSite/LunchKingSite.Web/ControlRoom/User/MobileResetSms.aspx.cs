﻿using System;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.User
{
    public partial class MobileResetSms : RolePage
    {
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private ILog logger = LogManager.GetLogger(typeof(MobileResetSms));
        protected void Page_Load(object sender, EventArgs e)
        {
            ResetMessage();
        }

        protected void btnQuery_Click(object sender, EventArgs e)
        {
            string mobile = txtMobile.Text;
            if (RegExRules.CheckMobile(mobile))
            {
                try
                {
                    var mais = mp.MobileAuthInfoGetList(mobile);
                    foreach (var mai in mais)
                    {
                        ShowMessage(String.Format("手機號碼 {0} 發送次數 {1} ", mobile, mai.QueryTimes));
                    }
                }
                catch (Exception ex)
                {
                    string msg = string.Format("手機號碼 {0} 查詢失敗", mobile);
                    logger.Error(msg, ex);
                    ShowMessage(msg);
                }
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            string mobile = txtMobile.Text;
            if (RegExRules.CheckMobile(mobile))
            {
                try
                {
                    var mais = mp.MobileAuthInfoGetList(mobile);
                    foreach (var mai in mais)
                    {
                        mai.QueryTimes = 0;
                        MemberFacade.SetMobileAuthInfo(mai);
                    }
                    ShowMessage(String.Format("手機號碼 {0} 發送次數己重置", mobile));
                    txtMobile.Text = "";
                }
                catch (Exception ex)
                {
                    string msg = string.Format("手機號碼 {0} 重置失敗", mobile);
                    logger.Error(msg, ex);
                    ShowMessage(msg);
                }
            }
        }

        private void ResetMessage()
        {
            divMessage.InnerHtml = "";
        }

        private void ShowMessage(string s)
        {
            divMessage.InnerHtml += s + "<br />";
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="ServiceOutgrate.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.User.ServiceOutgrate" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/ControlRoom/Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
    <style type="text/css">
        .style2 {
            width: 53px;
        }

        .style3 {
            width: 308px;
        }

        .style4 {
            width: 501px;
        }

        .style5 {
            width: 76px;
        }

        .style6 {
            width: 136px;
        }

        .style7 {
            width: 143px;
        }

        .style8 {
            width: 81px;
        }

        .ajax__myTab .ajax__tab_header {
            height: 40px;
            width: 1250px;
            font-family: verdana,tahoma,helvetica;
            font-size: 12px;
            border-bottom: solid 1px #999999;
        }

        .ajax__myTab .ajax__tab_outer {
            padding-right: 4px;
            height: 40px;
            background-color: #C0C0C0;
            margin-right: 2px;
            border-right: solid 1px #666666;
            border-top: solid 1px #aaaaaa;
        }

        .ajax__myTab .ajax__tab_inner {
            padding-left: 3px;
            background-color: #C0C0C0;
        }

        .ajax__myTab .ajax__tab_tab {
            height: 20px;
            padding-top: 10px;
            padding-bottom: 10px;
            padding-left: 10px;
            padding-right: 10px;
            margin: 0;
            white-space:normal;
        }

        .ajax__myTab .ajax__tab_hover .ajax__tab_outer {
            background-color: #cccccc;
        }

        .ajax__myTab .ajax__tab_hover .ajax__tab_inner {
            background-color: #cccccc;
        }

        .ajax__myTab .ajax__tab_active .ajax__tab_outer {
            background-color: #fff;
            border-left: solid 1px #999999;
        }

        .ajax__myTab .ajax__tab_active .ajax__tab_inner {
            background-color: #fff;
            font-weight: bold;
        }

        .ajax__myTab .ajax__tab_active .ajax__tab_tab {
        }

        .ajax__myTab .ajax__tab_body {
            font-family: verdana,tahoma,helvetica;
            font-size: 10pt;
            border: 1px solid #999999;
            border-top: 0;
            padding: 8px;
            width: 1200px;
            background-color: #ffffff;
        }

        .ajax__myTab {
            text-align: right;
        }

        .ajax__myTab {
            text-align: center;
        }

        .ajax__myTab {
            text-align: center;
        }

        .ajax__myTab {
            text-align: left;
        }

        .style10 {
            width: 10px;
        }

        .style11 {
            width: 10px;
        }

        .style12 {
            width: 219px;
        }

        .style13 {
            width: 134px;
            text-align: right;
        }

        .style15 {
            width: 69px;
        }

        .tmall_cell {
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
    <script type="text/javascript">
        function SetSubmintClickDisable(obj) {
            if (($.trim($('[id*=txtPromotionValue]').val()).length != 0) && ($.trim($('[id*=txtReason]').val()).length != 0) && ($.trim($('[id*=txtBonusStartTime]').val()).length != 0) && ($.trim($('[id*=txtBonusExpireTime]').val()).length != 0)) {
                $('#' + obj).css('display', 'none');
                Sys.Application.remove_init(handler);
            } else {
                alert('請輸入紅利金資訊!!');
                return false;
            }
        }

        function SetUserData(user) {
            var value = $('input[id*=rdlUser]:checked').val();
            $('#' + user).val($('input[id*=rdlUser]:checked').val());
            $('select[id*=ddlType]').val(0);
        }

        function ClearServiceMessageData(order, message) {
            $('#' + order).val('');
            $('#' + message).val('');
        }

        function SetSelectValue() {
            var SubCategory = $('select[id*=ddlSubCategory]');
            var SubCategory_Value = $('input[id*=hdSubCategory]');
            SubCategory_Value.val(SubCategory.val());
        }

        function CheckData() {
            //var Category_value = $('select[id*=ddlCategory]').val();
            //var SubCategory_value = $('input[id*=hidSubCategory_SelectValue]').val();
            var Category_value = $('#<%=ddlCategory.ClientID%>').val();
            var SubCategory_value = $('#<%=ddlSubCategory.ClientID%>').val();
            var rdlWorkType_value = $("input:radio[name='<%=rdlWorkType.ClientID.Replace("_","$")%>']:checked").val();
            var memberEmail = $('#ctl00_ContentPlaceHolder1_lblIEmail').html();

            if (memberEmail == null || memberEmail == "")
            {
                alert("Email可能有誤，請檢查!");
                return false;
            }
            if (Category_value == 0) {
                alert('請選擇客服紀錄的分類!');
                return false;
            }
            if (SubCategory_value == 0) {
                alert('請選擇客服紀錄的子分類!');
                return false;
            }
            if (rdlWorkType_value == undefined) {
                alert('嘿！尚未點選轉件項目!');
                return false;
            }
            if ($('textarea[id*=txtIMessage]').val() == '') {
                alert('請輸入客服紀錄的問題說明!');
                return false;
            }

        }

        function buildSubCategory() {
            var ddlMain = $("#<%=ddlCategory.ClientID%>");
            var ddlMainSelected = $("#<%=ddlCategory.ClientID%> :selected");
            var ddlSub = $("#<%=ddlSubCategory.ClientID%>");

            $.ajax({
                type: "POST",
                url: "serviceIntegrate.aspx/GetServiceMessageCategory",
                data: "{parentId: " + ddlMain.val() + "}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    ddlSub.empty();
                    ddlSub.append($('<option></option>').val('0').html('請選擇'));
                    $.each(cates, function (i, item) {
                        ddlSub.append($('<option></option>').val(item.CategoryId).html(item.CategoryName));
                    });
                },
                error: function () {
                    alert("error");
                    ddlSub.empty();
                    ddlSub.append($('<option></option>').val('0').html('請選擇'));
                }
            });
            if (ddlMainSelected.text() == "會員(帳號)問題" || ddlMainSelected.text() == "其他問題" || ddlMainSelected.text() == "請選擇") {
                $('.OrderText').css('display', 'none');
            } else {
                $('.OrderText').css('display', '');
            }
        }

        //在Save客服紀錄的分類時，無法抓取到子分類下拉式選單的值，所以存在一個hidden去寫資料.
        function setSubCategory() {
            var ddlSub = $("#<%=ddlSubCategory.ClientID%> :selected");
            var hdSub = $("#<%=hdSubCategory.ClientID%>");
            hdSub.val(ddlSub.val());
        }

        function txtPromotionValue_keyup() {
            if ((/^-?\d*$/.test($('#txtPromotionValue').val())) === false) {
                $('#txtPromotionValue').val('');
            }

            if (parseInt($('#txtPromotionValue').val()) < 0) {
                $('#txtOrderId').val('');
                $('#txtOrderId').attr("disabled", true);
                $('#chkBcashPayByVendor').attr("checked", false);
                $('#chkBcashPayByVendor').attr("disabled", true);
            } else {
                $('#txtOrderId').attr("disabled", false);
                $('#chkBcashPayByVendor').attr("disabled", false);
            }
        }

        function txtOrderId_keyup() {
            $('#chkBcashPayByVendor').attr("checked", true);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="divSearch" runat="server">
        <table>
            <tr style="display:none">
                <td valign="top" class="style3">
                    <h3>會員資訊快速搜尋</h3>
                    <table style="width: 800px">
                        <tr>
                            <td align="right" class="style2">依:
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlType" CssClass="adminitem">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtUserData" runat="server" ValidationGroup="Query" />
                                <asp:HiddenField ID="hidUserData" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="style2"></td>
                            <td align="right">
                                <asp:Button ID="btnSearch" runat="server" Text="搜尋" OnClick="btnSearch_Click" ValidationGroup="Query" />
                                <asp:Label ID="lblErrorMsg" runat="server" ForeColor="Red"></asp:Label>
                                <asp:RequiredFieldValidator ID="rfvUserData" ControlToValidate="txtUserData" ValidationGroup="Query"
                                    Display="Dynamic" runat="server" ErrorMessage="請輸入會員資料!!"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Panel ID="divMultiUser" runat="server" Visible="false">
                                    <table>
                                        <tr>
                                            <td style="vertical-align: top; text-align: right;" class="style15">請選擇:
                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="rdlUser" runat="server">
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table id="tbUserData" runat="server" visible="false">
                        <tr>
                            <td class="style4">
                                <fieldset>
                                    <legend>會員基本資料</legend>
                                    <table style="width: 467px">
                                        <tr>
                                            <td class="style5" colspan="2">帳號:&nbsp;&nbsp;
                                                <asp:Label ID="lblEmail" runat="server"></asp:Label> <asp:Literal id="litGuestMemberMark" runat="server"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style7">姓名:&nbsp;&nbsp;
                                                <asp:HyperLink ID="hkUserName" runat="server" Target="_blank"></asp:HyperLink>
                                            </td>
                                            <td class="style6">連絡電話:&nbsp;&nbsp;
                                                <asp:Label ID="lblMobile" runat="server"></asp:Label>
                                            </td>                                            
                                        </tr>
                                        <tr>
                                            <td class="style5" colspan="2">連絡Email:&nbsp;&nbsp;
                                                <asp:Literal ID="lblUserEmail" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style5" colspan="2">地址:&nbsp;&nbsp;
                                                <asp:Label ID="lblAddress" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style5" colspan="2">
												<div style="width: 640px; display: flex; flex-direction: row">
													<div style="padding-right: 20px">
														登入方式:
													</div>
													<div style="flex: 1">
														<asp:GridView ID="gvLoginType" runat="server" GridLines="Horizontal" BorderWidth="1"
															ShowFooter="false" BorderColor="Silver"
															ShowHeader="true" AutoGenerateColumns="false" Width="80%" CellPadding="1" CellSpacing="1"
															OnRowDataBound="gvLoginType_RowDataBound">
															<Columns>
																<asp:TemplateField HeaderText="登入類型" HeaderStyle-Width="35%" ItemStyle-HorizontalAlign="Center">
																	<ItemTemplate>
																		<asp:Label ID="lblExternalOrg" runat="server"></asp:Label>
																	</ItemTemplate>
																</asp:TemplateField>
																<asp:BoundField HeaderText="編號" DataField="ExternalUserId" HeaderStyle-Width="65%"
																	HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
															</Columns>
															<EmptyDataTemplate>
																無串接資訊
															</EmptyDataTemplate>
														</asp:GridView>
													</div>
												</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style5" colspan="2">認證手機:&nbsp;&nbsp;
                                                <asp:Label ID="lbRegisteredMobile" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style7">紅利:&nbsp;&nbsp;
                                                <asp:Label ID="lblBCash" runat="server" Text="0"></asp:Label>
                                            </td>
                                            <td class="style6" style="display: none;">會員卡別:&nbsp;&nbsp;
                                                <asp:Label ID="lblMemberCard" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>                                        
                                        <tr>
                                            <td class="style7">17Life購物金:&nbsp;&nbsp;
                                                <asp:HiddenField ID="hidExternalUserId" runat="server" />
                                                <asp:Label ID="lblSCash" runat="server" Text="0"></asp:Label>
                                            </td>                                            
                                            <td class="style6">
                                                
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <h3>天貓檔次訂單查詢</h3>
                    <table>
                        <tr>
                            <td align="right" class="style2">依:
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddl_Tmall" CssClass="adminitem">
                                    <asp:ListItem Text="天貓訂單編號" Value="0" />
                                    <asp:ListItem Text="手機號碼" Value="1" />
                                    <asp:ListItem Text="憑證號碼" Value="2" />
                                </asp:DropDownList>
                                <asp:TextBox ID="tbx_Tmall" runat="server" ValidationGroup="Query" />
                                <asp:Button ID="btn_TmallSearch" runat="server" Text="搜尋" OnClick="btn_TmallSearch_Click" ValidationGroup="Tmall" />
                                <br />
                                <asp:RequiredFieldValidator ID="rfv_Tmall" runat="server" ErrorMessage="請填入天貓資訊" Display="Dynamic" ControlToValidate="tbx_Tmall" ValidationGroup="Tmall"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Panel ID="pan_Tmall" runat="server" Visible="false">
                                    <asp:GridView ID="gv_Tmall" runat="server" AutoGenerateColumns="false" EmptyDataText="查無資料" EmptyDataRowStyle-BackColor="White">
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <table style="width: 100%; background-color: white; text-align: center;">
                                                        <tr style="background-color: lightblue;">
                                                            <td>P好康訂單編號</td>
                                                            <td>天貓訂單編號</td>
                                                            <td>手機號碼</td>
                                                            <td>憑證號碼</td>
                                                            <td>憑證狀態</td>
                                                            <td>發送時間</td>
                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td class="tmall_cell">
                                                            <a href="../Order/order_detail.aspx?oid=<%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).OrderGuid %>" target="_blank">
                                                                <%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).OrderId %></a></td>
                                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).RelatedOrderId %></td>
                                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).Mobile %></td>
                                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SequenceNumber %></td>
                                                        <td class="tmall_cell"><%# SmsStatusString(((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SmsStatus)  %></td>
                                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SmsCreateTime.HasValue?((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SmsCreateTime.Value.ToString("yyyy/MM/dd HH:mm:ss"):string.Empty %></td>
                                                    </tr>
                                                </ItemTemplate>
                                                <AlternatingItemTemplate>
                                                    <tr style="background-color: lightgray;">
                                                        <td class="tmall_cell"><a href="../Order/order_detail.aspx?oid=<%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).OrderGuid %>" target="_blank">
                                                            <%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).OrderId %></a></td>
                                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).RelatedOrderId %></td>
                                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).Mobile %></td>
                                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SequenceNumber %></td>
                                                        <td class="tmall_cell"><%# SmsStatusString(((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SmsStatus)  %></td>
                                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SmsCreateTime.HasValue?((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SmsCreateTime.Value.ToString("yyyy/MM/dd HH:mm:ss"):string.Empty %></td>
                                                    </tr>
                                                </AlternatingItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <uc1:Pager ID="page_Tmall" runat="server" PageSize="10" ongetcount="GetTmallCount"
                                        onupdate="TmallUpdateHandler"></uc1:Pager>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>

            </tr>
            <tr>
                <td colspan="2">
                    <h3>資策會雙北APP訂單查詢</h3>
                    <table>
                        <tr>
                            <td align="right" class="style2">依:
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlIDEASType" CssClass="adminitem">
                                    <asp:ListItem Text="訂單編號" Value="0" />
                                    <asp:ListItem Text="EMAIL" Value="1" />
                                    <asp:ListItem Text="手機" Value="2" />
                                    <asp:ListItem Text="購買人姓名" Value="3" />
                                </asp:DropDownList>
                                <asp:TextBox ID="txtIDEASSearch" runat="server" ValidationGroup="Query" />
                            </td>
                        </tr>
                        <tr>
                            <td class="style2"></td>
                            <td>
                                <asp:Button ID="IDEASSearch" runat="server" Text="搜尋" OnClick="IDEASSearch_Click" ValidationGroup="IDEASQuery" />
                                <asp:Label ID="IDEASSearchError" runat="server" ForeColor="Red"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtIDEASSearch" ValidationGroup="IDEASQuery"
                                    Display="Dynamic" runat="server" ErrorMessage="請輸入會員資料!!"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Panel ID="divIDEASOrder" runat="server" Visible="false">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="IDEASOrderGrid" runat="server" OnRowDataBound="IDEASOrderGrid_RowDataBound"
                                                    AllowSorting="True" GridLines="Horizontal" ForeColor="Black" CellPadding="4"
                                                    BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE" BackColor="White"
                                                    EmptyDataText="無符合的資料" AutoGenerateColumns="False" Font-Size="Small">
                                                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="訂單編號">
                                                            <ItemTemplate>
                                                                <asp:HyperLink
                                                                    ID="hkOrderId" runat="server" Target="_blank"></asp:HyperLink>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="200px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="PurchaserName" HeaderText="購買人" HtmlEncode="False"
                                                            ReadOnly="True">
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="PurchaserEmail" HeaderText="購買人EMAIL" HtmlEncode="False"
                                                            ReadOnly="True">
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="PurchaserPhone" HeaderText="購買人手機" HtmlEncode="False"
                                                            ReadOnly="True">
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="賣家">
                                                            <ItemTemplate>
                                                                <asp:HyperLink
                                                                    ID="hkSellerName" runat="server" Target="_blank"></asp:HyperLink>
                                                                <div>
                                                                    <asp:Literal ID="hkSellerMemo" runat="server" />
                                                                </div>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" Width="200px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="CreateTime" DataFormatString="{0:yyyy/MM/dd HH:mm}"
                                                            HeaderText="購買日期" HtmlEncode="False" ReadOnly="True">
                                                            <ItemStyle HorizontalAlign="Center" Width="150px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="subtotal" DataFormatString="{0:N0}" HeaderText="總額"
                                                            HtmlEncode="False" ReadOnly="True">
                                                            <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="檔期">
                                                            <ItemTemplate>
                                                                <asp:Label
                                                                    ID="lblBusinessTime" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="狀態">
                                                            <ItemTemplate>
                                                                [<asp:HyperLink
                                                                    ID="hkPaymentStatus" runat="server" Target="_blank"></asp:HyperLink>]<br/>
                                                            <asp:Label
                                                                ID="lblOrderStatus" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" Height="30px"></HeaderStyle>
                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
                                                    <RowStyle BackColor="#F7F7DE" Height="50px"></RowStyle>
                                                    <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                                                </asp:GridView>
                                                <uc1:Pager ID="IDEASOrderGridPager" runat="server" PageSize="10" ongetcount="GetIDEASOrderCount"
                                                    onupdate="IDEASOrderUpdateHandler"></uc1:Pager>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="panDisplay" runat="server" Visible="false">
        <asp:Panel ID="divAddServiceMsg" runat="server">
            <div id="PopAddServiceMsg" class="modalPopup" style="display: none; width: 658px;">
                <asp:Panel ID="Panel3" runat="server" Width="658px" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Gray; color: Black">
                    <fieldset>
                        <legend>自行新增客服</legend>
                        <table width="100%">
                            <tr>
                                <td align="center">
                                    <table>
                                        <tr>
                                            <td align="right" class="style13">姓名
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblIName" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" class="style13">Email
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblIEmail" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="style13">電話
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblIPhone" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" class="style13">OrderId
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtIOrder" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="style13">形式
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlItype" runat="server">
                                                    <asp:ListItem Selected="True">來電</asp:ListItem>
                                                    <asp:ListItem>外撥</asp:ListItem>
                                                    <asp:ListItem>去信</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td align="right" class="style13">分類
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlCategory" runat="server" onchange="buildSubCategory();">
                                                    <asp:ListItem>請選擇</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="ddlSubCategory" runat="server" onchange="SetSelectValue();">
                                                    <asp:ListItem>請選擇</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="hdSubCategory" runat="server" htmlreadonly="true" Width="30px"
                                                    Style="display: none;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style13">狀態
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlIStatus" runat="server">
                                                    <asp:ListItem Value="wait">待處理</asp:ListItem>
                                                    <asp:ListItem Value="process">處理中</asp:ListItem>
                                                    <asp:ListItem Value="complete">完成</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td align="right">轉件項目
                                            </td>
                                            <td align="left" colspan="3">
                                                <asp:RadioButtonList ID="rdlWorkType" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="商品" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="好康" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="退貨" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">緊急程度
                                            </td>
                                            <td align="left" colspan="3">
                                                <asp:RadioButtonList ID="rblPriority" runat="server" RepeatDirection="Horizontal"
                                                    RepeatLayout="Table">
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="style13">問題說明
                                            </td>
                                            <td align="left" colspan="3">
                                                <asp:TextBox ID="txtIMessage" runat="server" TextMode="MultiLine" Height="130px"
                                                    Width="520px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="style13">回報廠商備註
                                            </td>
                                            <td align="left" colspan="3">
                                                <asp:TextBox ID="txtRemarkSeller" runat="server" TextMode="MultiLine" Height="60px"
                                                    Width="520px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center" style="text-align: center">
                                                <asp:Button ID="btnSetServiceMsg" runat="server" Text="確認" OnClick="btnSetServiceMsg_Click"
                                                    OnClientClick="return CheckData();" />
                                                <asp:Button ID="btnAddServiceMsgClose" runat="server" Text="退出" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </asp:Panel>
            </div>
        </asp:Panel>
        <br />
        <asp:Panel ID="divMainData" runat="server" Style="padding-left: 20px;">
            <asp:Label ID="lblMessage" runat="server" ForeColor="Red" />
            <asp:UpdatePanel ID="upMainData" runat="server">
                <ContentTemplate>
                    <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__myTab" AutoPostBack="True" OnActiveTabChanged="TabContainer1_ActiveTabChanged">
                        <cc1:TabPanel ID="tabOrder" runat="server" TabIndex="0" Width="30px" HeaderText="123">
                            <HeaderTemplate>
                                團購訂單紀錄
                            </HeaderTemplate>
                            <ContentTemplate>
                                <div align="right">
                                    <asp:CheckBox runat="server" ID="cbIsFunds" AutoPostBack="True" OnCheckedChanged="cbIsFunds_CheckedChanged" Text="不顯示全家及0元檔次" Checked="True"/>
                                    篩選：<asp:DropDownList runat="server" ID="ddlStatus" DataTextField="Value" DataValueField="Key" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" />                                </div>

                                <asp:PlaceHolder ID="divOrderInfo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="style10"></td>
                                            <td>
                                                <asp:GridView ID="gvOrderList" runat="server" OnRowDataBound="gvOrderList_RowDataBound"
                                                    AllowSorting="True" GridLines="Horizontal" ForeColor="Black" CellPadding="4"
                                                    BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE" BackColor="White"
                                                    EmptyDataText="無符合的資料" AutoGenerateColumns="False" Font-Size="Small">
                                                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="訂單編號">
                                                            <ItemTemplate>
                                                                <asp:HyperLink
                                                                    ID="hkOrderId" runat="server" Target="_blank"></asp:HyperLink>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="200px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="member_name" HeaderText="購買人" HtmlEncode="False"
                                                            ReadOnly="True">
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="檔名">
                                                            <ItemTemplate>
                                                                <asp:HyperLink
                                                                    ID="goItemName" runat="server" Target="_blank"></asp:HyperLink>
                                                                <div>
                                                                    <asp:Literal ID="hkSellerMemo" runat="server" />
                                                                </div>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" Width="200px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="create_time" DataFormatString="{0:yyyy/MM/dd HH:mm}"
                                                            HeaderText="購買日期" HtmlEncode="False" ReadOnly="True">
                                                            <ItemStyle HorizontalAlign="Center" Width="150px" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="subtotal" DataFormatString="{0:N0}" HeaderText="總額"
                                                            HtmlEncode="False" ReadOnly="True">
                                                            <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="檔期">
                                                            <ItemTemplate>
                                                                <asp:Label
                                                                    ID="lblBusinessTime" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="狀態">
                                                            <ItemTemplate>
                                                                [<asp:HyperLink
                                                                    ID="hkPaymentStatus" runat="server" Target="_blank"></asp:HyperLink>]<br/>
                                                            <asp:Label
                                                                ID="lblRefundStatus" runat="server"></asp:Label><br/>
                                                            <asp:Label
                                                                ID="lblExchangeStatus" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" Height="30px"></HeaderStyle>
                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
                                                    <RowStyle BackColor="#F7F7DE" Height="50px"></RowStyle>
                                                    <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                                                </asp:GridView>
                                                <uc1:Pager ID="ucOrderPager" runat="server" PageSize="10" ongetcount="GetOrderCount"
                                                    onupdate="OrderUpdateHandler"></uc1:Pager>
                                            </td>
                                            <td class="style11"></td>
                                        </tr>
                                    </table>
                                </asp:PlaceHolder>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="tabService" runat="server">
                            <HeaderTemplate>
                                客服紀錄
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:PlaceHolder ID="divServiceMessageInfo" runat="server">
                                    <table>
                                        <tr>
                                            <td colspan="3">
                                                <asp:Button ID="btnAddServiceMsg" runat="server" Text="新增客服紀錄" OnClientClick="$('#PopAddServiceMsg').show(); return false;" />
                                                <cc1:ModalPopupExtender ID="MPE" runat="server" TargetControlID="btnAddServiceMsg"
                                                    PopupControlID="divAddServiceMsg" BackgroundCssClass="modalBackground" DropShadow="True"
                                                    CancelControlID="btnAddServiceMsgClose" PopupDragHandleControlID="Panel3" DynamicServicePath=""
                                                    Enabled="True" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style10"></td>
                                            <td>
                                                <asp:GridView ID="gvServiceRecord" runat="server" EnableViewState="true" OnRowDataBound="gvServiceRecord_RowDataBound"
                                                    GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None"
                                                    BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="False"
                                                    Font-Size="Small">
                                                    <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                                    <RowStyle BackColor="#F7F7DE" Height="50px"></RowStyle>
                                                    <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
                                                    <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="30px"></HeaderStyle>
                                                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="分類">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCategory" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="狀態">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hkStatus" runat="server" Target="_blank" />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="型式">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMessageType" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="問題">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMessage" runat="server" />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" Width="260px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="訂單編號">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hkOrderId" runat="server" Target="_blank" />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="180px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="來信時間">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCreateTime" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="90px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="出件人員">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblModifyId" runat="server" />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="最後處理時間">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblModifyTime" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="90px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="館別">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblType" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="5%" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                <uc1:Pager ID="ucServicePager" runat="server" PageSize="10" ongetcount="GetServiceCount"
                                                    onupdate="ServiceUpdateHandler"></uc1:Pager>
                                            </td>
                                            <td class="style11"></td>
                                        </tr>
                                    </table>
                                </asp:PlaceHolder>
                            </ContentTemplate>
                        </cc1:TabPanel>

                        <cc1:TabPanel ID="tabBonus" runat="server">
                            <HeaderTemplate>
                                購物金紅利使用紀錄
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:PlaceHolder ID="divBonusInfo" runat="server">
                                    <table>
                                        <tr>
                                            <td>
                                                <br />
                                                <asp:Label ID="lblTabBonusHint" runat="server" Font-Size="X-Large" ForeColor="#FF3300" Text="讀取中請稍候，這頁資料量比較大要等比較久，請勿切換上排頁籤。" />
                                                <table width="990" style="border: double 3px black">
                                                    <tr>
                                                        <th style="background-color: Gray">記錄
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Panel ID="pe" runat="server">
                                                                <table>
                                                                    <tr>
                                                                        <td>紅利金補入:
                                                                        </td>
                                                                        <td class="style12">
                                                                            <asp:TextBox ID="txtPromotionValue" Columns="4" MaxLength="6" runat="server" AutoPostBack="True" ClientIDMode="Static" onkeyup="txtPromotionValue_keyup();"/><br />
                                                                        </td>
                                                                        <td>摘要:
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtReason" runat="server" Columns="4" MaxLength="180" Width="325px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>紅利生效日:
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtBonusStartTime" runat="server"></asp:TextBox>
                                                                            <cc1:CalendarExtender ID="ucBonusStartDate" TargetControlID="txtBonusStartTime" runat="server"
                                                                                Format="yyyy/MM/dd" Enabled="True">
                                                                            </cc1:CalendarExtender>
                                                                        </td>
                                                                        <td>訂單編號</td>
                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtOrderId" MaxLength="25" AutoPostBack="True" ClientIDMode="Static" onkeyup="txtOrderId_keyup();"></asp:TextBox>
                                                                            <asp:CheckBox runat="server" ID="chkBcashPayByVendor" ClientIDMode="Static"/>由商家扣款給消費者
                                                                            <asp:HiddenField ID="hidDealGuid" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>紅利到期日:
                                                                        </td>
                                                                        <td class="style12">
                                                                            <asp:TextBox ID="txtBonusExpireTime" runat="server"></asp:TextBox>
                                                                            <cc1:CalendarExtender ID="ucBonusExpireDate" TargetControlID="txtBonusExpireTime"
                                                                                runat="server" Format="yyyy/MM/dd" Enabled="True">
                                                                            </cc1:CalendarExtender>
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:Button ID="btnBcashAdd" runat="server" OnClick="btnBcashAdd_Click" Text="確認" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <asp:GridView ID="gvBonusList" runat="server" DataKeyNames="Id" Width="80%" AutoGenerateColumns="False"
                                                                Font-Size="Small" EnableViewState="true">
                                                                <Columns>
                                                                    <asp:BoundField DataField="CreateTime" HeaderText="日期" ReadOnly="True" DataFormatString="{0:yyyy/MM/dd HH:mm}">
                                                                        <ItemStyle Width="20%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Message" HeaderText="信息" ReadOnly="True" HtmlEncode="False">
                                                                        <ItemStyle Width="60%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="CreateId" HeaderText="建立者" ReadOnly="True">
                                                                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="990" style="border: double 3px black">
                                                    <tr>
                                                        <th style="background-color: Gray">紅利累積及兌換記錄
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvBonusConvert" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                EnableViewState="true" Font-Size="Small" OnRowDataBound="gvBonusConvert_RowDataBound">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="日期">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCreateTime" runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="15%" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="摘要">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAction" runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="40%" />
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="紅利新增兌換">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPromotionValue" runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="10%" />
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="餘額">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblRunSum" runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="10%" />
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="到期日">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblExpireTime" runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="15%" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="建立者">
                                                                        <ItemTemplate>
                                                                            <asp:HyperLink ID="hkCreateId" runat="server" Target="_blank"></asp:HyperLink>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="10%" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="990" style="border: double 3px black">
                                                    <tr>
                                                        <th style="background-color: Gray">17Life購物金收支紀錄
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvScash" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                EnableViewState="true" Font-Size="Small">
                                                                <Columns>
                                                                    <asp:BoundField DataField="CreateTime" HeaderText="日期">
                                                                        <HeaderStyle Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField HeaderText="來源">
                                                                        <ItemTemplate>
                                                                            <a target="_blank" href='<%# (Eval("OrderGuid") == null) ? "" : "../../ControlRoom/Order/order_detail.aspx?oid=" + Eval("OrderGuid")  %>'>
                                                                                <%# Eval("Message")%></a>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="訂單編號">
                                                                        <ItemTemplate>
                                                                            <a target="_blank" href='<%# (Eval("OrderGuid") == null) ? "" : "../../ControlRoom/Order/order_detail.aspx?oid=" + Eval("OrderGuid")  %>'>
                                                                                <%# Eval("OrderId")%></a>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="15%" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="店家名稱">
                                                                        <ItemTemplate>
                                                                            <a target="_blank" href='../../ppon/default.aspx?bid=<%#GetBusinessHourGuid(Eval("OrderGuid") == null ? string.Empty : Eval("OrderGuid").ToString())%>'>
                                                                                <%# Eval("SellerName")%></a>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="15%" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="存入">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label1" runat="server" Text='<%# int.Equals(int.Parse(Eval("WithdrawalId").ToString()), -1) ? Eval("Amount") : ""  %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="5%" />
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="支出">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label2" runat="server" Text='<%# int.Equals(int.Parse(Eval("WithdrawalId").ToString()), -1) ? "" : Eval("Amount")  %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="5%" />
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="餘額">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("Subtotal") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="5%" />
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="990" style="border: double 3px black">
                                                    <tr>
                                                        <th style="background-color: Gray">17Life購物金收支紀錄(舊)
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvScashOld" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                EnableViewState="true" Font-Size="Small">
                                                                <Columns>
                                                                    <asp:BoundField DataField="CreateTime" HeaderText="日期">
                                                                        <HeaderStyle Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField HeaderText="來源">
                                                                        <ItemTemplate>
                                                                            <a target="_blank" href='../../ControlRoom/Order/order_detail.aspx?oid=<%#GetOrderGuid(DataBinder.Eval(Container, "DataItem.Reference").ToString())%>'>
                                                                                <%# GetScashSummary(DataBinder.Eval(Container, "DataItem.Id").ToString(), DataBinder.Eval(Container, "DataItem.Reference").ToString(), DataBinder.Eval(Container, "DataItem.Type").ToString())%></a>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="25%" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="訂單編號">
                                                                        <ItemTemplate>
                                                                            <a target="_blank" href='../../ControlRoom/Order/order_detail.aspx?oid=<%#GetOrderGuid(DataBinder.Eval(Container, "DataItem.Reference").ToString())%>'>
                                                                                <%# GetOrderSummary(DataBinder.Eval(Container, "DataItem.Reference").ToString())%></a>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="15%" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="店家名稱">
                                                                        <ItemTemplate>
                                                                            <a target="_blank" href='../../ppon/default.aspx?bid=<%#GetBusinessHourIdSummary(DataBinder.Eval(Container, "DataItem.Reference").ToString())%>'>
                                                                                <%# GetSellerSummary(DataBinder.Eval(Container, "DataItem.Reference").ToString())%></a>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="15%" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="存入">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label1" runat="server" Text='<%# GetScashIncomeAmount(DataBinder.Eval(Container, "DataItem.Amount").ToString()) %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="5%" />
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="支出">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label2" runat="server" Text='<%# GetScashExpendAmount(DataBinder.Eval(Container, "DataItem.Amount").ToString()) %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="5%" />
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="餘額">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label3" runat="server" Text='<%# GetScashBalanceAmount(DataBinder.Eval(Container, "DataItem.UserName").ToString(),
                                                                                                                      DataBinder.Eval(Container, "DataItem.Id").ToString()) %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="5%" />
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="Message" HeaderText="備註">
                                                                        <HeaderStyle Width="10%" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="990" style="border: double 3px black">
                                                    <tr>
                                                        <th style="background-color: Gray">Payeasy購物金消費紀錄(僅列出於17Life中消費的)
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvPcash" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                EnableViewState="true" Font-Size="Small">
                                                                <Columns>
                                                                    <asp:BoundField DataField="TransTime" HeaderText="日期">
                                                                        <HeaderStyle Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField HeaderText="來源">
                                                                        <ItemTemplate>
                                                                            <a target="_blank" href='../../ControlRoom/Order/order_detail.aspx?oid=<%#Eval("OrderGuid")%>'>
                                                                                <%#GetSummary(
                                                                            ((DataBinder.Eval(Container, "DataItem.ParentOrderId")!=null)
                                                                            ? DataBinder.Eval(Container, "DataItem.ParentOrderId")
                                                                            : Guid.NewGuid()).ToString(),
                                                                  DataBinder.Eval(Container, "DataItem.PaymentType").ToString(),
                                                                  DataBinder.Eval(Container, "DataItem.TransType").ToString())%></a>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="44%" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="訂單編號">
                                                                        <ItemTemplate>
                                                                            <a target="_blank" href='../../ControlRoom/Order/order_detail.aspx?oid=<%#Eval("OrderGuid")%>'>
                                                                                <%#DataBinder.Eval(Container, "DataItem.OrderId")%></a>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="15%" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="店家名稱">
                                                                        <ItemTemplate>
                                                                            <a target="_blank" href='../../ppon/default.aspx?bid=<%#GetBusinessHourIdByPid(
                                                                        ((DataBinder.Eval(Container, "DataItem.ParentOrderId")!=null)
                                                                            ? DataBinder.Eval(Container, "DataItem.ParentOrderId")
                                                                            : Guid.NewGuid()).ToString())%>'>
                                                                                <%#DataBinder.Eval(Container, "DataItem.SellerName")%></a>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="15%" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="存入">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label1" runat="server" Text='<%# GetChargingAmount(DataBinder.Eval(Container,"DataItem.TransType").ToString(),
                                                                                                                  DataBinder.Eval(Container, "DataItem.Amount").ToString()) %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="8%" />
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="支出">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label2" runat="server" Text='<%# GetRefundAmount(DataBinder.Eval(Container,"DataItem.TransType").ToString(),
                                                                                                                DataBinder.Eval(Container, "DataItem.Amount").ToString()) %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="8%" />
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="990" style="border: double 3px black">
                                                    <tr>
                                                        <th style="background-color: Gray">刷卡記錄
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvCreditcard" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                EnableViewState="true" Font-Size="Small">
                                                                <Columns>
                                                                    <asp:BoundField DataField="TransTime" HeaderText="日期">
                                                                        <HeaderStyle Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField HeaderText="來源">
                                                                        <ItemTemplate>
                                                                            <a target="_blank" href='../../ControlRoom/Order/order_detail.aspx?oid=<%#Eval("OrderGuid")%>'>
                                                                                <%#GetSummary(
                                                                            ((DataBinder.Eval(Container, "DataItem.ParentOrderId")!=null)
                                                                            ? DataBinder.Eval(Container, "DataItem.ParentOrderId")
                                                                            : Guid.NewGuid()).ToString(),
                                                                  DataBinder.Eval(Container, "DataItem.PaymentType").ToString(),
                                                                  DataBinder.Eval(Container, "DataItem.TransType").ToString())%></a>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="44%" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="訂單編號">
                                                                        <ItemTemplate>
                                                                            <a target="_blank" href='../../ControlRoom/Order/order_detail.aspx?oid=<%#Eval("OrderGuid")%>'>
                                                                                <%#DataBinder.Eval(Container, "DataItem.OrderId")%></a>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="15%" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="店家名稱">
                                                                        <ItemTemplate>
                                                                            <a target="_blank" href='../../ppon/default.aspx?bid=<%#GetBusinessHourIdByPid(
                                                                            ((DataBinder.Eval(Container, "DataItem.ParentOrderId")!=null)
                                                                            ? DataBinder.Eval(Container, "DataItem.ParentOrderId")
                                                                            : Guid.NewGuid()).ToString())%>'>
                                                                                <%#DataBinder.Eval(Container, "DataItem.SellerName")%></a>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="15%" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="刷退">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label1" runat="server" Text='<%# GetChargingAmount(DataBinder.Eval(Container,"DataItem.TransType").ToString(),
                                                                                                                  DataBinder.Eval(Container, "DataItem.Amount").ToString()) %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="8%" />
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="扣款">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label2" runat="server" Text='<%# GetRefundAmount(DataBinder.Eval(Container,"DataItem.TransType").ToString(),
                                                                                                                DataBinder.Eval(Container, "DataItem.Amount").ToString()) %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="8%" />
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="990" style="border: double 3px black">
                                                    <tr>
                                                        <th style="background-color: Gray">折價券抵用紀錄
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvDiscountCode" runat="server" AutoGenerateColumns="false" Width="100%"
                                                                EnableViewState="true" Font-Size="Small">
                                                                <Columns>
                                                                    <asp:BoundField DataField="UseTime" HeaderText="兌換日期">
                                                                        <HeaderStyle Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="EndTime" HeaderText="截止日期">
                                                                        <HeaderStyle Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Code" HeaderText="序號">
                                                                        <HeaderStyle Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField HeaderText="來源">
                                                                        <ItemTemplate>
                                                                            <a target="_blank" href='<%#GetOrderLink(Eval("OrderGuid").ToString(), Eval("OrderClassification")) %>'>
                                                                                <%#GetDiscountSummary(Eval("OrderGuid").ToString(), Eval("OrderClassification"))%></a>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="24%" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="訂單編號">
                                                                        <ItemTemplate>
                                                                            <a target="_blank" href='<%#GetOrderLink(Eval("OrderGuid").ToString(), Eval("OrderClassification")) %>'>
                                                                                <%#Eval("OrderId")%></a>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="15%" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="Name" HeaderText="折價券名稱" ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderStyle Width="15%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Amount" HeaderText="面額" DataFormatString="{0:N0}" ItemStyle-HorizontalAlign="Right">
                                                                        <HeaderStyle Width="8%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="UseAmount" HeaderText="扣款金額" DataFormatString="{0:N0}"
                                                                        ItemStyle-HorizontalAlign="Right">
                                                                        <HeaderStyle Width="8%" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="990" style="border: double 3px black">
                                                    <tr>
                                                        <th style="background-color: Gray">回收購物金
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Panel ID="pnlRefundScash" runat="server">
                                                                <table>
                                                                    <tr>
                                                                        <td>回收金額:
                                                                        </td>
                                                                        <td class="style12">
                                                                            <asp:TextBox ID="tbScashAmount" Columns="4" MaxLength="6" runat="server" /><br />
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <asp:Button ID="btConform" runat="server" OnClick="btRefundScash_Click" Text="確認" />
                                                                        </td>
                                                                    </tr>                                                                
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                                
                                            </td>
                                        </tr>
                                    </table>
                                </asp:PlaceHolder>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="tabSubscription" runat="server">
                            <HeaderTemplate>
                                訂閱管理
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:PlaceHolder ID="divSubscriptionInfo" runat="server">
                                    <table>
                                        <tr>
                                            <td class="style10"></td>
                                            <td style="font-size: small;">優惠活動訊息通知：
                                                <asp:CheckBox ID="chkIfReceiveEDM" runat="server" Text="P好康" />
                                                <asp:CheckBox ID="chkIfReceivePiinlifeEDM" runat="server" Text="品生活" />
                                                <br />
                                                精選好康開賣通知：
                                                <asp:CheckBox ID="chkIfReceiveStartedSell" runat="server" Text="P好康" />
                                                <asp:CheckBox ID="chkIfReceivePiinlifeStartedSell" runat="server" Text="品生活" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style10"></td>
                                            <td>
                                                <br />
                                                <b>P好康</b><br />
                                                <asp:GridView ID="gvSubscription" runat="server" AutoGenerateColumns="False" GridLines="Horizontal"
                                                    ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE"
                                                    EnableViewState="true" BackColor="White" EmptyDataText="無符合的資料" Font-Size="Small"
                                                    Width="620px" OnRowDataBound="gvSubscription_RowDataBound">
                                                    <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                                    <RowStyle BackColor="#F7F7DE" Height="50px"></RowStyle>
                                                    <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
                                                    <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="30px"></HeaderStyle>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="城市">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCityName" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="90px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="createtime" HeaderText="訂閱日期" DataFormatString="{0:yyyy/MM/dd HH:mm}">
                                                            <ItemStyle Width="150px" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="取消訂閱">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblUnsubscriptionTime" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="150px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="重新訂閱">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblResubscriptionTime" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="150px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="取消訂閱">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkCheck" runat="server" />
                                                                <asp:HiddenField ID="hidId" runat="server" />
                                                                <asp:HiddenField ID="hidStatus" runat="server" />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="80px" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                <br />
                                                <b>品生活</b><br />
                                                <asp:GridView ID="gvHidealSubcription" runat="server" AutoGenerateColumns="False" GridLines="Horizontal"
                                                    ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE"
                                                    EnableViewState="true" BackColor="White" EmptyDataText="無符合的資料" Font-Size="Small"
                                                    Width="620px" OnRowDataBound="gvHidealSubcription_RowDataBound">
                                                    <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                                    <RowStyle BackColor="#F7F7DE" Height="50px"></RowStyle>
                                                    <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
                                                    <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="30px"></HeaderStyle>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="城市">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCityName" runat="server"></asp:Label>
                                                                <asp:HiddenField ID="hidCityId" runat="server" />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="70px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="區域">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRegion" runat="server"></asp:Label>
                                                                <asp:HiddenField ID="hidRegion" runat="server" />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="70px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="createtime" HeaderText="訂閱日期" DataFormatString="{0:yyyy/MM/dd HH:mm}">
                                                            <ItemStyle Width="150px" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="取消訂閱">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblUnsubscriptionTime" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="150px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="狀態">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFlag" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="取消訂閱">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkCheck" runat="server" />
                                                                <asp:HiddenField ID="hidId" runat="server" />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="80px" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                            <td class="style11"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">&nbsp;&nbsp;<asp:Button ID="btnSubscribeUpdate" runat="server" OnClick="btnSubscribeUpdate_Click"
                                                Text="修改狀態" />
                                                <asp:Label ID="lblSubscriptionMsg" runat="server" ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:PlaceHolder>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="tabMemberCardDetail" runat="server">
                            <HeaderTemplate>
                                會員卡明細
                            </HeaderTemplate>                            
                            <ContentTemplate>
                                <h3>會員卡明細</h3>
                                    <table>
                                        <tr>
                                            <td>
                                                <table width="990" style="border: double 3px black">
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="gvMembershipCardConvert" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                EnableViewState="true" Font-Size="Small" OnRowDataBound="gvMembershipCardConvert_RowDataBound">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="異動日期">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCreateTime" runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="10%" BackColor="gray" BorderColor="black"/>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="會員卡名稱">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblUserMembershipCardName" runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="15%" BackColor="gray" BorderColor="black"/>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="卡別">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCardType" runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="5%" BackColor="gray" BorderColor="black"/>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="卡號">
                                                                        <ItemTemplate>
                                                                            <asp:HyperLink ID="hkUserMembershipCardId" runat="server" Target="_blank"></asp:HyperLink>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="10%" BackColor="gray" BorderColor="black"/>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="摘要">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblMemo" runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="15%" BackColor="gray" BorderColor="black"/>
                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="優惠有效期限" ControlStyle-BorderColor="Black">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblExpireTime" runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="10%" BackColor="gray" BorderColor="black"/>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="備註">
                                                                        <ItemTemplate>
                                                                            <asp:HyperLink ID="hkDiscountCode" runat="server" Target="_blank"></asp:HyperLink>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle Width="15%" BackColor="gray" BorderColor="black"/>
                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>                                
                                                            <uc1:Pager ID="ucMembershipCardPager" runat="server" PageSize="10" ongetcount="GetMembershipCardCount" onupdate="MembershipCardUpdateHandler" Visible="False"></uc1:Pager>
                                                        </td>
                                                    </tr>
                                                </table>
                                             </td>
                                         </tr>
                                     </table>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="tabHidealOrder" runat="server" TabIndex="1">
                            <HeaderTemplate>
                                PiinLife 訂單紀錄
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:PlaceHolder ID="divHidealOrderInfo" runat="server">
                                    <br />
                                    <span style="color: red">訂單狀態為取消單：表示消費者付款失敗，或於付款頁停頓(發呆)過久，連線中斷。</span>
                                    <table>
                                        <tr>
                                            <td class="style10"></td>
                                            <td>
                                                <asp:GridView ID="gvHidealOrderList" runat="server" OnRowDataBound="gvHidealOrderList_RowDataBound"
                                                    AllowSorting="True" GridLines="Horizontal" ForeColor="Black" CellPadding="4"
                                                    BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE" BackColor="White"
                                                    EmptyDataText="無符合的資料" AutoGenerateColumns="False" Font-Size="Small" Width="960px">
                                                    <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                                    <RowStyle BackColor="#F7F7DE" Height="50px"></RowStyle>
                                                    <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
                                                    <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="30px"></HeaderStyle>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="訂單編號">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hkOrderId" runat="server" Target="_blank"></asp:HyperLink>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="購買人">
                                                            <ItemTemplate>
                                                                <asp:Label ID="hkOrderName" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="賣家">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hkSellerName" runat="server" Target="_blank"></asp:HyperLink>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" Width="20%" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField ReadOnly="True" DataField="CreateTime" DataFormatString="{0:yyyy/MM/dd HH:mm}"
                                                            HtmlEncode="False" HeaderText="購買日期">
                                                            <ItemStyle Width="17%" HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="品名">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hkProductName" runat="server" Target="_blank"></asp:HyperLink>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="總額">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAmount" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Right" Width="2%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="檔期">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOrderTime" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="狀態">
                                                            <ItemTemplate>
                                                                [<asp:Label ID="lblStatus" runat="server"></asp:Label>]
                                                            <asp:Label ID="lblHidealOrderReturnStatus" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                <uc1:Pager ID="ucHidealPager" runat="server" PageSize="10" ongetcount="GetHidealOrderCount"
                                                    onupdate="HidealOrderUpdateHandler"></uc1:Pager>
                                            </td>
                                            <td class="style11"></td>
                                        </tr>
                                    </table>
                                </asp:PlaceHolder>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel ID="TabLunchKing" runat="server" TabIndex="0">
                            <HeaderTemplate>
                                午餐王訂單紀錄
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:PlaceHolder ID="divLunchkingOrderInfo" runat="server">
                                    <br />
                                    <table>
                                        <tr>
                                            <td class="style10"></td>
                                            <td>
                                                <asp:GridView ID="gvLunchKingList" runat="server" EnableViewState="true" OnRowDataBound="gvLunchKingList_RowDataBound"
                                                    AllowSorting="True" GridLines="Horizontal" ForeColor="Black" CellPadding="4"
                                                    BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE" BackColor="White"
                                                    EmptyDataText="無符合的資料" AutoGenerateColumns="False" Font-Size="Small">
                                                    <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                                    <RowStyle BackColor="#F7F7DE" Height="50px"></RowStyle>
                                                    <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
                                                    <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="30px"></HeaderStyle>
                                                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="訂單編號">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hkOrderId" runat="server" Target="_blank"></asp:HyperLink>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" Width="200px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField ReadOnly="True" DataField="member_name" HtmlEncode="False" HeaderText="購買人"
                                                            ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:TemplateField HeaderText="賣家">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hkSellerName" runat="server" Target="_blank"></asp:HyperLink>
                                                                <div>
                                                                </div>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField ReadOnly="True" DataField="create_time" DataFormatString="{0:yyyy/MM/dd HH:mm}"
                                                            HtmlEncode="False" HeaderText="購買日期">
                                                            <ItemStyle Width="250px" HorizontalAlign="Center" />
                                                        </asp:BoundField>
                                                        <asp:BoundField ReadOnly="True" DataField="subtotal" DataFormatString="{0:N0}" HtmlEncode="False"
                                                            HeaderText="總額">
                                                            <ItemStyle HorizontalAlign="Right" Width="100px" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                                <uc1:Pager ID="ucOrderLunchKingPager" runat="server" PageSize="10" ongetcount="GetLunchKingCount"
                                                    onupdate="LunchKingOrderUpdateHandler"></uc1:Pager>
                                            </td>
                                            <td class="style11"></td>
                                        </tr>
                                    </table>
                                </asp:PlaceHolder>
                            </ContentTemplate>
                        </cc1:TabPanel>
                    </cc1:TabContainer>
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </asp:Panel>
    <asp:Label ID="lblErrMsg" Visible="false" Text="查無會員資料" CssClass="emtext" runat="server"></asp:Label>
</asp:Content>

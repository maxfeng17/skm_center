﻿
using System;
using System.Data;
using System.Collections;
using System.Web.Services;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.UI;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;




namespace LunchKingSite.Web.ControlRoom.User
{
    public partial class BitrafficsImport : RolePage
    {
        IMarketingProvider op = ProviderFactory.Instance().GetProvider<IMarketingProvider>();
        //IPponProvider pp;

        protected void Page_Load(object sender, EventArgs e)
        { }

        protected void Import_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(FileUpload1.FileName))
            {
                ClientScript.RegisterStartupScript(GetType(), "lockedOut", "alert('請選擇檔案！');", true);
            }
            else
            {
                if (null != FileUpload1.PostedFile && FileUpload1.PostedFile.ContentLength > 0)
                {
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(FileUpload1.PostedFile.InputStream);
                    Sheet sheet = hssfworkbook.GetSheetAt(0);
                    Hashtable bi = new Hashtable();
                    BiTrafficCollection bic = new BiTrafficCollection();
                    Row row;

                    for (int k = 1; k < sheet.LastRowNum; k++)
                    {
                        row = sheet.GetRow(k);
                        //int g = row.LastCellNum;
                        //int Q = 0;  
                        if (!string.IsNullOrWhiteSpace(row.GetCell(0).ToString()))
                        {
                            //DateTime TrafficesDate = row.GetCell(0).DateCellValue;


                            DateTime TrafficesDate = DateTime.ParseExact(row.GetCell(0).ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);


                            Guid BusinessHourGuid;
                            if (!Guid.TryParse(row.GetCell(1).ToString(), out BusinessHourGuid))
                            {
                                continue;
                            }

                            Int32 Views;
                            if (!Int32.TryParse(row.GetCell(7).ToString(), out Views))
                            {
                                continue;
                            }

                            Int32 Viewercount;
                            if (!Int32.TryParse(row.GetCell(8).ToString(), out Viewercount))
                            {
                                continue;
                            }

                            Int32 Buyercount;
                            if (!Int32.TryParse(row.GetCell(11).ToString(), out Buyercount))
                            {
                                continue;
                            }

                            BiTraffic bt = new BiTraffic();

                            if (bt != null)
                            {
                                bt.TrafficesDate = TrafficesDate;
                                bt.BusinessHourGuid = BusinessHourGuid;
                                bt.Views = Views;
                                bt.Viewercount = Viewercount;
                                bt.Buyercount = Buyercount;

                                bic.Add(bt);

                            }

                            //Q++;

                        }
                        else
                        {
                            continue;
                        }

                    }
                    op.BiTrafficCollectionSet(bic);
                    ClientScript.RegisterStartupScript(GetType(), "lockedOut", "alert('匯入成功！');", true);
                }
            }
        }

        protected void Import_Click_csv(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(FileUpload2.FileName))
            {
                ClientScript.RegisterStartupScript(GetType(), "lockedOut", "alert('請選擇檔案！');", true);
            }
            else
            {
                if (null != FileUpload2.PostedFile && FileUpload2.PostedFile.ContentLength > 0)
                {
                    HttpPostedFile file = FileUpload2.PostedFile;
                    using (Stream filestream = file.InputStream)
                    {
                        using (StreamReader reader = new StreamReader(filestream, Encoding.GetEncoding(950)))
                        {
                            String line;
                            String[] split = null;
                            var result = new BiTrafficCollection();
                            while ((line = reader.ReadLine()) != null)
                            {
                                split = line.Split('\t');
                                BiTraffic row = new BiTraffic();

                                if (!string.IsNullOrWhiteSpace(split[0].ToString()))
                                {
                                    Int32 a;
                                    if (Int32.TryParse(split[0].ToString().Substring(1, 1),out a))
                                    {
                                        var c= a;
                                        var b = split.Length;

                                        if (c == 2 && b == 12)
                                        {

                                            DateTime tep;
                                            if (DateTime.TryParse(split[0].Replace(@"""", "").Replace(@"\", "").ToString(), out tep))
                                            {
                                                //row.TrafficesDate = DateTime.ParseExact(tep.ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                                                row.TrafficesDate = tep;
                                            }
                                            else
                                            {
                                                continue;
                                            }

                                            Guid Guid_Split;
                                            if (Guid.TryParse(split[1].ToString().Replace(@"""", "").Replace(@"\", ""), out Guid_Split))
                                            {
                                                row.BusinessHourGuid = Guid_Split;
                                            }
                                            else
                                            {
                                                continue;
                                            }

                                            Int32 Views_Split;
                                            if (Int32.TryParse(split[7].ToString().Replace(@"""", "").Replace(@"\", ""), out Views_Split))
                                            {
                                                row.Views = Views_Split;
                                            }
                                            else
                                            {
                                                continue;
                                            }

                                            Int32 Viewercount_Split;
                                            if (Int32.TryParse(split[8].ToString().Replace(@"""", "").Replace(@"\", ""), out Viewercount_Split))
                                            {
                                                row.Viewercount = Viewercount_Split;
                                            }
                                            else
                                            {
                                                continue;
                                            }

                                            Int32 Buyercount_Split;
                                            if (Int32.TryParse(split[11].ToString().Replace(@"""", "").Replace(@"\", ""), out Buyercount_Split))
                                            {
                                                row.Buyercount = Buyercount_Split;
                                            }
                                            else
                                            {
                                                continue;
                                            }


                                            result.Add(row);

                                        }

                                    }
                                    //string a = split[0].ToString().Substring(1, 1);                                
                                                          
                                   

                                    else
                                    {
                                        continue;
                                    }

                                }

                                DB.SaveAll(result);

                                op.BiTrafficCollectionSet(result);
                                ClientScript.RegisterStartupScript(GetType(), "lockedOut", "alert('匯入成功！');", true);

                            }
                        }
                    }
                }
            }
        }
    }
}
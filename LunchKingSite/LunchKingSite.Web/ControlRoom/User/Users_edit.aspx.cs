﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.WebLib.Component.MemberActions;
using PaymentType = LunchKingSite.Core.PaymentType;

public partial class admin_user_edit : RolePage
{
    private bool isEditMode = true;
    private string _userName = HttpContext.Current.Request["username"];
    private ILocationProvider locProv;
    private IMemberProvider memProv;
    private IOrderProvider odrProv;
    private IPponProvider pponProv;
    private IHiDealProvider hp;
    protected static ISysConfProvider config;

    protected void Page_Load(object sender, EventArgs e)
    {
        locProv = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        memProv = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        odrProv = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
        config = ProviderFactory.Instance().GetConfig();
        hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        // Is in add mode?
        if (string.IsNullOrEmpty(_userName))
        {
            Page.Title = ActionTitle.Text = "新增使用者";
            isEditMode = false;
            PasswordRow.Visible = true;
            SecretQuestionRow.Visible = Membership.RequiresQuestionAndAnswer ? true : false;
            SecretAnswerRow.Visible = Membership.RequiresQuestionAndAnswer ? true : false;
            pS.Visible = false;
        }
        else
        {
            // We are in edit mode
            PasswordRow.Visible = false; // in future we can show this by role
            NewPasswordRow.Visible = true;

            SecretQuestionRow.Visible = (Membership.RequiresQuestionAndAnswer && Membership.EnablePasswordRetrieval) ? true : false;
            SecretAnswerRow.Visible = (Membership.RequiresQuestionAndAnswer && Membership.EnablePasswordRetrieval) ? true : false;
        }

        if (!IsPostBack)
        {
            ddlCity.DataSource = locProv.CityGetListFromAvailBuilding(AppendMode.DistrictOnly);
            ddlCity.DataBind();

            // Is it in edit mode?
            if (isEditMode)
            {
                MembershipUser mu = null;
                try
                {
                    mu = Membership.GetUser(Membership.GetUserNameByEmail(_userName));
                }
                catch { }

                if (mu == null)
                {
                    SetResultMessage("Membership 資料不完整，請在工具選項修正.");
                    panFixMembership.Visible = true;
                }

                if (Membership.EnablePasswordRetrieval)
                {
                    // Load old password
                    Password.Text = mu.GetPassword();
                    Password.Enabled = false;

                    // Load old secret question
                    SecretQuestion.Text = mu.PasswordQuestion;
                }

                if (mu != null)
                {
                    btnUnlockUser.Enabled = mu.IsLockedOut;
                }

                Member user = memProv.MemberGet(_userName);
                if (user.BuildingGuid != null)
                {
                    ViewBuildingCity bld = locProv.ViewBuildingCityGet(user.BuildingGuid.Value);
                    ddlCity.SelectedValue = bld.CityId.ToString();
                    ddlBuilding.DataSource = locProv.BuildingGetListInZoneAndPrefix(bld.CityId, null);
                    ddlBuilding.DataBind();
                    ddlBuilding.SelectedValue = bld.Guid.ToString();
                    aib.BuildingAddress = bld.BuildingStreetName;
                    aib.BuildingZoneType = Helper.GetZoneType(bld.ParentId, bld.CityStatus != null ? (CityStatus)bld.CityStatus.Value : CityStatus.Nothing);
                    aib.ValidationGroup = "validAddressEvent";  //避免 AddressInputBox.ascx 和 this page Submit 互動
                }
                Email.Text = _userName;
                if (user.IsGuest)
                {
                    litGuestMemberMark.Text = string.Format("[{0}]", Member._GUEST_MEMBER);
                }
                else
                {
                    //Email.ReadOnly = true;
                    //Email.Style["border"] = "0px";
                    //Email.Style["background-color"] = "transparent";      
                    litGuestMemberMark.Text = "";
                }

                hfUserId.Value = ltUserId.Text = user.UniqueId.ToString();
                tbBirthDay.Text = (user.Birthday.HasValue) ? user.Birthday.Value.ToShortDateString() : "";
                tbFirstName.Text = user.FirstName;
                tbLastName.Text = user.LastName;
                if (user.Gender.HasValue)
                {
                    rbGender.SelectedValue = user.Gender.Value.ToString();
                }
                tbMobile.Text = user.Mobile;
                tbUserEmail.Text = user.UserEmail;
                tbExtension.Text = user.CompanyTelExt;

                string[] comPhone = string.IsNullOrEmpty(user.CompanyTel) ? new string[] { "" } : user.CompanyTel.Split(new char[] { ' ' });
                tbAreaCode.Text = comPhone[0];
                if (comPhone.Length > 1)
                {
                    tbCompanyPhone.Text = comPhone[1];
                }
                aib.PresetAddress = user.CompanyAddress;
                aib.CityName = ddlCity.SelectedItem.Text;
                aib.DataBind();
                
                litRegisterFrom.Text = Helper.GetSmartTimeSpanString(DateTime.Now - user.CreateTime);

                var memProp = memProv.MemberPropertyGetByUserId(user.UniqueId);
                if (memProp.IsLoaded)
                {
                    litRiskScore.Text = memProp.RiskScore.ToString();
                }

                tbC.Text = user.Comment;
                string picUrl = MemberFacade.GetMemberPicUrl(user);
                if (string.IsNullOrEmpty(picUrl) == false)
                {
                    phPic.Visible = true;
                    imgPic.Src = picUrl;
                }


                ListItemCollection sCol = new ListItemCollection();
                foreach (int val in Enum.GetValues(typeof(MemberStatusFlag)))
                {
                    if (val != (int)MemberStatusFlag.None)
                    {
                        sCol.Add(new ListItem(Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, (MemberStatusFlag)val), val.ToString()));
                    }
                }

                cbS.DataSource = sCol;
                cbS.DataBind();

                chkIsApproved.Checked = user.IsApproved;

                if (mu != null)
                {
                    foreach (ListItem li in cbS.Items)
                    {
                        int val = int.Parse(li.Value);
                        li.Selected = Helper.IsFlagSet(user.Status, val) ||
                                      (val == (int)MemberStatusFlag.Locked && mu.IsLockedOut) ||
                                      (val == (int)MemberStatusFlag.Disabled && !mu.IsApproved);
                        li.Enabled =
                            !(val == (int)MemberStatusFlag.HasOrdered ||
                              (val == (int)MemberStatusFlag.Locked && !mu.IsLockedOut));
                    }
                }

                int userId = MemberFacade.GetUniqueId(_userName);
                int orderCount = odrProv.OrderGetCount(Order.Columns.UserId + "=" + userId);
                int failOrderCount = odrProv.FailOrderGetCountByUser(userId);
                litOrderCount.Text = orderCount.ToString();
                if (failOrderCount > 0)
                {
                    litSuccessOrderCount.Text = "(" + (orderCount - failOrderCount).ToString() + ")";
                }
                try
                {
                    litTurnover.Text = odrProv.OrderGetSuccessTurnover(userId).ToString();
                }
                catch
                {
                    litTurnover.Text = "0";
                }
                
                if (orderCount > 0)
                {
                    decimal failOrderRate = failOrderCount / (decimal)orderCount;
                    lbFailOrderRate.Text = failOrderRate.ToString("P2");
                    if (failOrderRate >= 0.6m)
                    {
                        rowSuccessfulOrderRate.Visible = true;
                    }
                    else
                    {
                        rowSuccessfulOrderRate.Visible = false;
                    }
                }

                NewPassword.Visible = Password.Enabled = false;
                RequiredFieldValidator6.Enabled = false;

                ab.ReferenceId = _userName;

                MemberLinkCollection memberLink = memProv.MemberLinkGetList(userId);
                gML.DataSource = memberLink;
                gML.DataBind();

                lbPcash.Text = "0";
                if (memberLink.Count > 0)
                {
                    foreach (MemberLink ml in memberLink)
                    {
                        if (ml.ExternalOrg == 0)
                        {
                            if (string.IsNullOrEmpty(ml.ExternalUserId))
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(Button), "alert", "alert('PEZId空值');", true);
                            }
                            else
                            {
                                lbPcash.Text = PCashWorker.CheckUserRemaining(ml.ExternalUserId).ToString();
                            }
                            break;
                        }
                    }
                }

                BindOAuthUserClients(user.UniqueId);

                if (Session[this.ClientID + "resultMsg"] != null)
                {
                    SetResultMessage(Session[this.ClientID + "resultMsg"].ToString());
                    Session.Remove(this.ClientID + "resultMsg");
                }

            }
            else
            {
                ddlBuilding.DataSource = locProv.BuildingGetListInZoneAndPrefix(int.Parse(ddlCity.SelectedValue), "");
                ddlBuilding.DataBind();
                cbChangePassword.Visible = cbChangePassword.Enabled = false;
                ab.Visible = false;
                aib.ValidationGroup = "validAddressEvent"; //避免 AddressInputBox.ascx 和 this page Submit 互動
            }
        }
    }

    protected void gML_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            MemberLink ml = (MemberLink)e.Row.DataItem;
            ITextControl litExternalOrg = (ITextControl)e.Row.FindControl("litExternalOrg");
            if ((SingleSignOnSource)ml.ExternalOrg == SingleSignOnSource.ContactDigitalIntegration)
            {
                litExternalOrg.Text = "Email";
            }
            else if ((SingleSignOnSource)ml.ExternalOrg == SingleSignOnSource.Mobile17Life)
            {
                litExternalOrg.Text = "手機";
            }
            else
            {
                litExternalOrg.Text = ((SingleSignOnSource)ml.ExternalOrg).ToString();
            }
        }
    }

    private void BindOAuthUserClients(int userId)
    {
        IOAuthProvider oap = ProviderFactory.Instance().GetProvider<IOAuthProvider>();
        OauthClientCollection clientCol = oap.ClientGetListByUser(userId);
        repOAuthUserClients.DataSource = clientCol.Count == 0 ? null : clientCol;
        repOAuthUserClients.DataBind();
    }

    protected string GetSummary(string oid, string paymentType, string transType)
    {
        string summary = string.Empty;

        switch ((PayTransType)int.Parse(transType))
        {
            case PayTransType.Authorization:
                if ((PaymentType)int.Parse(paymentType) == PaymentType.Creditcard)
                {
                    summary = Resources.Localization.PayTransAuthorization + "：";
                }
                else
                {
                    summary = Resources.Localization.PayTransCharging + "：";
                }
                break;

            case PayTransType.Charging:
                summary = Resources.Localization.PayTransCharging + "：";
                break;

            case PayTransType.Refund:
                summary = Resources.Localization.PayTransRefund + "：";
                break;

            default:
                break;
        }

        Order o = odrProv.OrderGet(new Guid(oid));
        Guid pid = (o != null) ? (Guid)o.ParentOrderId : new Guid();
        ViewPponDeal d = pponProv.ViewPponDealGet(pid);
        summary += (d != null) ? d.EventName : string.Empty;

        return summary;
    }

    protected string GetOrderGuid(string cashpointId)
    {
        string oid = cashpointId;
        string msg = odrProv.PaymentTransactionGet(new Guid(cashpointId)).Message;
        if (!string.IsNullOrEmpty(msg) && msg.Split('|').Length > 1)
        {
            oid = msg.Split('|')[1];
        }
        return oid;
    }

    protected string GetOrderLink(string oid, object classification)
    {
        OrderClassification orderClassification = OrderClassification.LkSite;
        if ((int?)classification == 2)
        {
            orderClassification = OrderClassification.HiDeal;
        }

        if (orderClassification == OrderClassification.LkSite)
        {
            //~controlroom/piinlife/hidealorderdetaillist.aspx?oid=cd231abf-8a12-41ae-bb11-11ba68f53587
            return ResolveUrl("~/ControlRoom/Order/order_detail.aspx?oid=") + oid;
        }
        else
        {
            return ResolveUrl("~/controlroom/piinlife/hidealorderdetaillist.aspx?oid=") + oid;
        }
    }

    protected string GetScashSummary(string id, string cashpointId, string type)
    {
        string summary = string.Empty;
        Order o = null;
        string soid = GetOrderGuid(cashpointId);

        switch ((CashPointListType)int.Parse(type))
        {
            case CashPointListType.Income:
                if (odrProv.PaymentTransactionGet(new Guid(cashpointId)).IsNew)
                {
                    summary = "補購物金";
                }
                else if (string.IsNullOrEmpty(soid))
                {
                    summary = "刷卡儲值";
                }
                else
                {
                    summary = "刷卡訂購轉購物金：";
                }
                break;

            case CashPointListType.Expense:
                summary = "支付：";
                break;

            case CashPointListType.Reject:
                summary = "取消訂單：";
                break;

            default:
                break;
        }

        if (!string.IsNullOrEmpty(soid))
        {
            o = odrProv.OrderGet(new Guid(soid));
            ViewPponDeal d = (o.IsNew) ? null : pponProv.ViewPponDealGet(o.ParentOrderId.Value);
            summary += (d != null) ? d.EventName : string.Empty;
        }

        return summary;
    }

    protected string GetChargingAmount(string transType, string amount)
    {
        decimal a = decimal.Parse(amount);
        return ((PayTransType)int.Parse(transType) != PayTransType.Refund) ? a.ToString("f0") : string.Empty;
    }

    protected string GetRefundAmount(string transType, string amount)
    {
        decimal a = decimal.Parse(amount);
        return ((PayTransType)int.Parse(transType) == PayTransType.Refund) ? a.ToString("f0") : string.Empty;
    }

    protected string GetScashIncomeAmount(string amount)
    {
        decimal a = decimal.Parse(amount);
        return (a >= 0) ? a.ToString("f0") : string.Empty;
    }

    protected string GetScashExpendAmount(string amount)
    {
        decimal a = decimal.Parse(amount);
        return (a < 0) ? a.ToString("f0") : string.Empty;
    }

    protected string GetScashBalanceAmount(string user, string id)
    {
        return odrProv.CashPointListSum(user, int.Parse(id)).ToString("f0");
    }

    protected string GetName(string email)
    {
        Member m = MemberFacade.GetMember(email);
        return m.DisplayName;
    }

    protected string GetDiscountSummary(string oid, object classification)
    {
        if (String.IsNullOrWhiteSpace(oid))
        {
            return string.Empty;
        }
        OrderClassification orderClassification = OrderClassification.LkSite;
        if ((int?)classification == 2)
        {
            orderClassification = OrderClassification.HiDeal;
        }

        if (orderClassification == OrderClassification.LkSite)
        {
            LunchKingSite.DataOrm.Order o = odrProv.OrderGet(new Guid(oid));
            ViewPponDeal d = pponProv.ViewPponDealGet(o.ParentOrderId.GetValueOrDefault());
            return d.ItemName;
        }
        else if (orderClassification == OrderClassification.HiDeal)
        {
            HiDealOrderDetail od = hp.HiDealOrderDetailGetListByOrderGuid(new Guid(oid))[0];
            return od.ProductName;
        }
        return string.Empty;
    }

    public void SaveClick(object sender, EventArgs e)
    {
        if (isEditMode)
        {
            UpdateUser(sender, e);
        }
        else
        {
            AddUser(sender, e);
        }
    }

    public void UpdateUser(object sender, EventArgs e)
    {
        if (!Page.IsValid)
        {
            return;
        }

        string resultMsg = string.Empty;

        string password = null;
        string newPassword = null;
        string question = null;
        string answer = null;

        password = Password.Text.Trim();
        newPassword = NewPassword.Text.Trim();

        if (Membership.RequiresQuestionAndAnswer)
        {
            question = SecretQuestion.Text;
            answer = SecretAnswer.Text;
        }

        try
        {
            if (tbMobile.Text != "" && RegExRules.CheckMobile(this.tbMobile.Text.Trim()) == false)
            {
                resultMsg = "請輸入台灣手機號碼, ex. 0912345678";
            }
            else if (tbUserEmail.Text != "" && RegExRules.CheckEmail(tbUserEmail.Text.Trim()) == false)
            {
                resultMsg = "請輸入正確信箱格式";
            }
            else
            {
                // Are we allowed to change secret question & answer?
                // We will need old password for this.
                if (Membership.EnablePasswordRetrieval &&
                    Membership.RequiresQuestionAndAnswer &&
                    question != null && answer != null)
                {
					MembershipUser mu = Membership.GetUser(Membership.GetUserNameByEmail(_userName));
                    mu.ChangePasswordQuestionAndAnswer(password, question, answer);
                }

                if (!string.IsNullOrEmpty(newPassword))
                {
                    MemberFacade.ResetMemberPassword(_userName, newPassword, ResetPasswordReason.AdminChange);
                }

                Member m = GetMemberFromUI(_userName);
                string newEmail = Email.Text.Trim();

                if (_userName.Equals(newEmail, StringComparison.OrdinalIgnoreCase) == false)
                {
                    if (RegExRules.CheckEmail(newEmail) == false)
                    {
                        resultMsg = string.Format("更新失敗. Email格式不合.", newEmail);
                    }
                    else if (memProv.MemberGet(newEmail).IsLoaded)
                    {
                        resultMsg = string.Format("更新失敗. {0} 被使用.", newEmail);
                    }
                    else
                    {
                        MemberUtility.UpdateMemberInfo(m, newEmail);
                        if (_userName.Equals(newEmail, StringComparison.OrdinalIgnoreCase) == false)
                        {
                            Session[this.ClientID + "resultMsg"] = string.Format("使用者更改Email, 原:{0} ,新:{1}", _userName, newEmail);
                            Response.Redirect(Request.Url.AbsolutePath + "?username=" + newEmail);
                        }
                    }
                }
                else
                {
                    MemberUtility.UpdateMemberInfo(m, null);
                    resultMsg = "使用者資料更新成功.";
                }
            }
        }
        catch (Exception ex)
        {
            resultMsg = "使用者資料更新失敗. 訊息: " + ex.Message;
        }
        SetResultMessage(resultMsg);
    }

    public void AddUser(object sender, EventArgs e)
    {
        if (!Page.IsValid)
        {
            return;
        }
        string resultMsg;
        string emailText = Email.Text;
        string password = Password.Text;
        IMemberRegister reg = new Life17MemberRegister(emailText, password, 0);
        MemberRegisterReplyType reply = reg.Process(false);

        if (!RegExRules.CheckMobile(this.tbMobile.Text.Trim()))
        {
            resultMsg = "請輸入台灣手機號碼, ex. 0912345678";
            return;
        }

        if (reply == MemberRegisterReplyType.RegisterSuccess)
        {
            Member m = GetMemberFromUI(emailText);
            m.Status = m.Status | (int)MemberStatusFlag.Active17Life;
            memProv.MemberSet(m);
            resultMsg = "使用者已被建立.";
        }
        else if (reply == MemberRegisterReplyType.MailOldMember)
        {
            resultMsg = string.Format("使用者已存在, <a href='users_edit.aspx?username={0}'>編輯 {1}</a>",
                HttpUtility.UrlEncode(emailText), emailText);
        }
        else
        {
            resultMsg = "建立使用者失敗. 錯誤: " + reg.Message;
        }
        SetResultMessage(resultMsg);
    }

    private void SetResultMessage(string resultMsg)
    {
        trResultRow.Visible = !String.IsNullOrEmpty(resultMsg);
        lblMessage.Text = resultMsg;
    }

    public bool IsUserInRole(string roleName)
    {
        if (string.IsNullOrEmpty(_userName) ||
            string.IsNullOrEmpty(roleName))
        {
            return false;
        }

        return Roles.IsUserInRole(_userName, roleName);
    }

    protected void ResetPassword_Click(object sender, EventArgs e)
    {
        string resultMsg;
        try
        {
            MembershipUser mu = Membership.GetUser(Membership.GetUserNameByEmail(Email.Text));
            mu.ResetPassword();
            resultMsg = "User password has been reset. User will receive an email with the new password";
        }
        catch (Exception ex)
        {
            resultMsg = "Could not reset user password. " + ex.Message;
        }
        SetResultMessage(resultMsg);
    }

    protected void unlockAccount_Click(object sender, EventArgs e)
    {
        try
        {
            MembershipUser mu = Membership.GetUser(Membership.GetUserNameByEmail(Email.Text));
            if (mu.UnlockUser())
            {
                Membership.UpdateUser(mu);
                mu = Membership.GetUser(Membership.GetUserNameByEmail(Email.Text));
                btnUnlockUser.Enabled = mu.IsLockedOut;
                SetResultMessage("User account unlocked");
            }
            else
            {
                SetResultMessage("Could not unlock user account.");
            }
        }
        catch (Exception ex)
        {
            SetResultMessage("Could not unlock user account. " + ex.Message);
        }
    }

    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        ListItemCollection buildings = locProv.BuildingGetListInZoneAndPrefix(int.Parse(ddlCity.SelectedValue), "");
        ddlBuilding.DataSource = buildings;
        ddlBuilding.DataBind();
        ddlBuilding.SelectedIndex = 0;
        ddlBuilding_SelectedIndexChanged(sender, e);
    }

    private Member GetMemberFromUI(string userName)
    {
        Member user;

        if (String.IsNullOrEmpty(userName) == false)
        {
            user = memProv.MemberGet(userName);
        }
        else
        {
            user = new Member();
            user.UserName = Email.Text;
        }

        if (String.IsNullOrEmpty(ddlBuilding.SelectedValue))
        {
            user.BuildingGuid = NewMemberUtility.DefaultBuildingGuid;
        }
        else
        {
            user.BuildingGuid = new System.Guid(ddlBuilding.SelectedValue);
        }
        try
        {
            user.Birthday = Convert.ToDateTime(tbBirthDay.Text);
        }
        catch { }
        user.LastName = tbLastName.Text;
        user.FirstName = tbFirstName.Text;
        if (rbGender.SelectedIndex > -1)
        {
            user.Gender = Convert.ToInt32(rbGender.SelectedValue);
        }
        user.Mobile = tbMobile.Text;
        user.UserEmail = tbUserEmail.Text.Trim();
        user.CompanyTel = tbAreaCode.Text + " " + tbCompanyPhone.Text;
        user.CompanyTelExt = tbExtension.Text;
        user.CompanyAddress = aib.FullAddress;
        user.Comment = tbC.Text.Trim();

        return user;
    }

    protected void cbChangePassword_CheckedChanged(object sender, EventArgs e)
    {
        if (cbChangePassword.Checked)
        {
            NewPassword.Visible = NewPassword.Enabled = true;
            RequiredFieldValidator6.Enabled = true;
        }
        else
        {
            NewPassword.Visible = NewPassword.Enabled = false;
            RequiredFieldValidator6.Enabled = false;
        }
    }

    protected void ddlBuilding_SelectedIndexChanged(object sender, EventArgs e)
    {
        aib.BuildingAddress = ddlBuilding.SelectedItem.Text;
        aib.BuildingZoneType = Helper.GetZoneType(int.Parse(ddlCity.SelectedValue));
        if (aib.BuildingZoneType == ZoneType.Company)
        {
            Building b = locProv.BuildingGet(new Guid(ddlBuilding.SelectedValue));
            if (!string.IsNullOrEmpty(b.BuildingStreetName) && b.BuildingStreetName != b.BuildingName)
            {
                aib.BuildingAddress = b.BuildingStreetName;
            }
        }
        aib.CityName = ddlCity.SelectedItem.Text;

        aib.PresetAddress = null;
        aib.DataBind();
    }

    

    protected void bS_Click(object sender, EventArgs e)
    {
        int val = (int)MemberStatusFlag.None;
        foreach (ListItem li in cbS.Items)
        {
            int v;
            if (int.TryParse(li.Value, out v))
            {
                if (li.Selected)
                {
                    val = val | v;
                }
            }
        }
        Member m = memProv.MemberGet(_userName);
        if (m.IsLoaded)
        {
            MembershipUser mu = Membership.GetUser(Membership.GetUserNameByEmail(_userName));
            if (mu.IsApproved ^ !Helper.IsFlagSet(val, (long)MemberStatusFlag.Disabled))
            {
                mu.IsApproved = !mu.IsApproved;
                Membership.UpdateUser(mu);
            }

            if (mu.IsLockedOut && !Helper.IsFlagSet(val, (long)MemberStatusFlag.Locked))
            {
                if (mu.UnlockUser())
                {
                    Membership.UpdateUser(mu);
                    SetResultMessage("User account unlocked");
                }
                else
                {
                    SetResultMessage("Could not unlock user account.");
                }
            }

            m.Status = val;
            
            if (Helper.IsFlagSet(m.Status, MemberStatusFlag.Active17Life) && m.IsGuest)
            {
                MemberFacade.UpgradeGuestMember(m);
            }

            m.IsApproved = chkIsApproved.Checked;

            if (m.IsApproved == false && IsMemberOnly(m.UserName) == false)
            {
                chkIsApproved.Checked = true;
                SetResultMessage("這個使用者非一般會員(擁有多個角色)，不允許這個操作");
                return;
            }

            if (m.IsDirty)
            {
                memProv.MemberSet(m);
            }

            SetResultMessage("狀態更新完成");
        }
    }

    private bool IsMemberOnly(string userName)
    {
        string[] userRoles = Roles.GetRolesForUser(userName);
        return userRoles.Length == 1 && userRoles[0] == "RegisteredUser";
    }

    protected void Impersonate_Click(object sender, EventArgs e)
    {
        // 被檢視者(權限只有RegisteredUser，才可被模擬)
        string[] editUserRole = Roles.GetRolesForUser(_userName);
        bool isAllowImpersonate = editUserRole.Length == 1 && editUserRole[0].Equals(MemberRoles.RegisteredUser.ToString("g"));
        // 登入者(權限有Administrator或MemberAdmin，才可模擬)
        bool isAdministrator = Roles.IsUserInRole(MemberRoles.Administrator.ToString("g"));
        bool isMemberAdmin = Roles.IsUserInRole(MemberRoles.MemberAdmin.ToString("g"));

        if (isAllowImpersonate || isAdministrator || isMemberAdmin)
        {
            MemberUtility.UserForceSignIn(_userName);
            Response.Redirect("~/");
        }
        else
        {
            SetResultMessage("您無法模擬為此使用者!!");
        }
    }

    protected void btnFixMembership_Click(object sender, EventArgs e)
    {
        try
        {
            NewMemberUtility.FixMembership(this._userName, txtPassword.Text);
            SetResultMessage(String.Format("會員 {0} 資料已修正.", this._userName));
            panFixMembership.Visible = false;
        }
        catch (Exception ex)
        {
            log4net.LogManager.GetLogger(typeof(admin_user_edit)).Warn(ex);
            SetResultMessage(ex.Message);
        }
    }

    protected void btnUpdateOAuthUserClients(object sender, EventArgs e)
    {
        int userId = MemberFacade.GetUniqueId(_userName);
        foreach (RepeaterItem item in repOAuthUserClients.Items)
        {
            int clientId = int.Parse(((HiddenField)item.FindControl("hidId")).Value);
            bool isDelete = ((CheckBox)item.FindControl("chkDelete")).Checked;
            if (isDelete)
            {
                OAuthFacade.ClientRemoveUser(clientId, userId);
            }
        }
        BindOAuthUserClients(userId);
        SetResultMessage("應用程式設定已更新");
    }

    private string GetDefaultOrderBonusStartDateString()
    {
        return DateTime.Today.ToString("yyyy/MM/dd");
    }

    private string GetDefaultOrderBonusExpireDateString()
    {
        DateTime rtnDate = new DateTime(DateTime.Today.AddYears(3).Year, 1, 1, 0, 0, 0, 0);
        return rtnDate.ToString("yyyy/MM/dd");
    }

    protected void btnInvalidMember_Click(object sender, EventArgs e)
    {
        if (CommonFacade.IsInSystemFunctionPrivilege(Page.User.Identity.Name, SystemFunctionType.Delete))
        {
            string replaceId = MemberUtility.DeleteMember(_userName, Page.User.Identity.Name, txtInvalidMemberMemo.Text);
            Response.Redirect(Request.Url.AbsolutePath + "?username=" + replaceId);
        }
        else
        {
            SetResultMessage("您不具有刪除會員資料的權限。");
        }
    }
}
<%@ Page Language="C#" MasterPageFile="../backend.master" AutoEventWireup="true"
    Inherits="admin_user_edit" Title="修改使用者資料" CodeBehind="Users_edit.aspx.cs" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Register Src="../Controls/AuditBoard.ascx" TagName="AuditBoard" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Controls/AddressInputBox.ascx" TagName="AddressInputBox" TagPrefix="uc1" %>

<asp:Content ContentPlaceHolderID="SSC" runat="Server">
    <script type="text/javascript">
        window.sslSiteUrl = '<%=config.SSLSiteUrl%>';
        $(document).ready(function() {
            var userId = parseInt($('#hfUserId').val(), 10);
            reloadMobileMemberBindInfo(userId);
           
            function reloadMobileMemberBindInfo(userId) {
                var model = JSON.stringify({
                    'UserId': userId
                });
                $.ajax({
                    type: "POST",
                    url: window.sslSiteUrl + "/api/controlroom/GetdMobileMemberBindInfo",
                    data: model,
                    cache: false,
                    dataType: "json",
                    xhrFields: {
                        withCredentials: true
                    },
                    contentType: 'application/json; charset=utf-8',
                    success: function(result) {
                        if (result.Code == 1000) { //1000 is ApiResultCode.Success
                            if (result.Data.BindEnabled) {
                                $('#btnBindMobile').removeAttr('disabled');
                            } else {
                                $('#btnBindMobile').attr('disabled', true);
                            }
                            if (result.Data.UnbindEnabled) {
                                $('#btnUnbindMobile').removeAttr('disabled');
                            } else {
                                $('#btnUnbindMobile').attr('disabled', true);
                            }
                            $('#mobileNumber').text(result.Data.MobileNumber);
                            $('#mobileMemberStatusDesc').text(result.Data.StatusDesc);
                            $('#userSmsSentCount').text(result.Data.SmsSentCount);
                            if (result.Data.MobileNumber != '') {
                                $('#mobileMemberContainer').show();
                            } else {
                                $('#mobileMemberContainer').hide();
                            }
                        } else if (result.Code != 1140){
                            alert(result.Message);
                        }
                    }, 
                    error: function(res) {
                        alert(res.statusText);
                    }
                });
            }

            $('#btnResetSmsSentCount').click(function() {
                var mobile = $('#mobileNumber').text();
                if (mobile == '') {
                    return;
                }
                var model = JSON.stringify({
                    'Mobile': mobile
                });
                $(this).attr('disabled', true);
                var trigger = this;
                $.ajax({
                    type: "POST",
                    url: window.sslSiteUrl + "/api/controlroom/ResetMobileSmsSentCount",
                    data: model,
                    dataType: "json",
                    xhrFields: {
                        withCredentials: true
                    },
                    contentType: 'application/json; charset=utf-8',
                    success: function(result) {
                        $(trigger).removeAttr('disabled');
                        if (result.Code == 1000) { //1000 is ApiResultCode.Success
                            $('#userSmsSentCount').text(result.Data);
                        } else {
                            alert(result.Message);
                        }
                    }, 
                    error: function(res) {
                        alert(res.statusText);
                        $(trigger).removeAttr('disabled');
                    }
                });
            });

            $('#btnBindMobile').click(function() {
                var mobile = $('#mobileNumber').text();
                if (mobile == '') {
                    return;
                }
                if (confirm('確定對手機帳號直接認證嗎?') == false) {
                    return;
                }
                var model = JSON.stringify({
                    'Mobile': mobile
                });
                $(this).attr('disabled', true);
                var trigger = this;
                $.ajax({
                    type: "POST",
                    url: window.sslSiteUrl + "/api/controlroom/BindMobile",
                    data: model,
                    dataType: "json",                    
                    xhrFields: {
                        withCredentials: true
                    },
                    contentType: 'application/json; charset=utf-8',
                    success: function(result) {
                        $(trigger).removeAttr('disabled');
                        if (result.Code == 1000) { //1000 is ApiResultCode.Success
                            reloadMobileMemberBindInfo(userId);
                        } else {
                            alert(result.Message);
                        }
                    }, 
                    error: function(res) {
                        alert(res.statusText);
                        $(trigger).removeAttr('disabled');
                    }
                });
            });
            $('#btnUnbindMobile').click(function() {
                if (confirm('確定解除手機帳號綁定嗎?') == false) {
                    return;
                }
                var mobile = $('#mobileNumber').text();
                if (mobile == '') {
                    return;
                }
                var model = JSON.stringify({
                    'Mobile': mobile
                });
                $(this).attr('disabled', true);
                var trigger = this;
                $.ajax({
                    type: "POST",
                    url: window.sslSiteUrl + "/api/controlroom/UnbindMobile",
                    data: model,
                    dataType: "json",                    
                    xhrFields: {
                        withCredentials: true
                    },
                    contentType: 'application/json; charset=utf-8',
                    success: function(result) {
                        $(trigger).removeAttr('disabled');
                        if (result.Code == 1000) { //1000 is ApiResultCode.Success
                            reloadMobileMemberBindInfo(userId);
                        } else {
                            alert(result.Message);
                        }
                    }, 
                    error: function() {
                        alert(res.statusText);
                        $(trigger).removeAttr('disabled');
                    }
                });
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="panDisplay" runat="server">
        <table cellspacing="0" cellpadding="0" border="0" width="900">
            <tr align="left" valign="top">
                <td width="70%" height="100%" class="lbBorders">
                    <table class="bodyText" cellspacing="0" width="100%" cellpadding="0" style="border: double 3px">
                        <tr class="header">
                            <td colspan="4">
                                <asp:Literal ID="ActionTitle" runat="server" Text="更新使用者資料" />
                            </td>
                        </tr>
                        <tr id="trResultRow" runat="server" visible="false">
                            <td colspan="4">
                                <asp:Label ID="lblMessage" runat="server" ForeColor="Red" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="bodyText" bordercolor="#ccddef">
                                    <tr>
                                        <td width="2">
                                        </td>
                                        <td>
                                            <asp:Literal runat="server" Text="User ID: " />
                                        </td>
                                        <td>
                                            <asp:HiddenField ID="hfUserId" runat="server" ClientIDMode="Static" />
	                                        <asp:Literal ID="ltUserId" runat="server" /> <asp:Literal id="litGuestMemberMark" runat="server"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="2">
                                        </td>
                                        <td>
                                            <asp:Label ID="Label2" runat="server" AssociatedControlID="Email" Text="電子信箱: " CssClass="adminlabel" />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="Email" MaxLength="128" TabIndex="102" Columns="50"
                                                CssClass="adminlabel" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Email"
                                                Display="Dynamic" EnableClientScript="true" ValidationGroup="changeUserEvent" >必填</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr id="PasswordRow" runat="server" visible="false">
                                        <td width="2">
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPassword" runat="server" AssociatedControlID="Password" Text="舊密碼: " />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="Password" MaxLength="50" TabIndex="103" Columns="20"
                                                TextMode="Password" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="Password"
                                                Display="Dynamic" EnableClientScript="true" ValidationGroup="changeUserEvent" >必填</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr id="NewPasswordRow" runat="server" visible="false">
                                        <td width="2">
                                        </td>
                                        <td>
                                            <asp:Label ID="lblNewPassword" runat="server" AssociatedControlID="NewPassword" Text="新密碼: " />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="cbChangePassword" Text="改密碼?" runat="server" OnCheckedChanged="cbChangePassword_CheckedChanged"
                                                AutoPostBack="true" />
                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" RenderMode="Inline">
                                                <contenttemplate>
                                                    <asp:TextBox runat="server" ID="NewPassword" MaxLength="50" TabIndex="103" Columns="20"
                                                        TextMode="Password" Text="" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="NewPassword"
                                                        Display="Dynamic" EnableClientScript="true" ValidationGroup="changeUserEvent">必填</asp:RequiredFieldValidator>
                                                </contenttemplate>
                                                <triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="cbChangePassword" EventName="CheckedChanged" />
                                                </triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr id="SecretQuestionRow" runat="server" visible="false">
                                        <td width="2">
                                        </td>
                                        <td>
                                            <asp:Label ID="Label5" runat="server" AssociatedControlID="SecretQuestion" Text="登入提問: " />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="SecretQuestion" MaxLength="128" TabIndex="104" Columns="30" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="SecretQuestion"
                                                Display="Dynamic" EnableClientScript="true" ValidationGroup="changeUserEvent">必填</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr id="SecretAnswerRow" runat="server" visible="false">
                                        <td width="2">
                                        </td>
                                        <td>
                                            <asp:Label ID="Label6" runat="server" AssociatedControlID="SecretAnswer" Text="題問答案: " />
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="SecretAnswer" MaxLength="128" TabIndex="105" Columns="30" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="SecretAnswer"
                                                Display="Dynamic" EnableClientScript="true" ValidationGroup="changeUserEvent">必填</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="2">
                                        </td>
                                        <td>
                                            姓名
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbLastName" Columns="2" MaxLength="6" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="tbFirstName" Columns="4" MaxLength="6" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="2">
                                        </td>
                                        <td>
                                            生日
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                <contenttemplate>
                                                    <asp:TextBox ID="tbBirthDay" runat="server" autocomplete="off" Columns="8"></asp:TextBox>
                                                    <asp:ImageButton ID="btnDate" runat="server" ImageUrl="~/Themes/default/images/cc0000_calendar.gif" />
                                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="tbBirthday"
                                                        Format="yyyy/MM/dd" PopupButtonID="btnDate">
                                                    </cc1:CalendarExtender>
                                                </contenttemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="2">
                                        </td>
                                        <td>
                                            性別
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rbGender" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="男" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="女" Value="2"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="2">
                                        </td>
                                        <td>
                                            通訊手機
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbMobile" Columns="15" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="2">
                                        </td>
                                        <td>
                                            通訊Email
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbUserEmail" Columns="30" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="2">
                                        </td>
                                        <td>
                                            公司電話
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbAreaCode" Columns="3" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="tbCompanyPhone" Columns="10" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="2">
                                        </td>
                                        <td>
                                            分機
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbExtension" Columns="5" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="2">
                                        </td>
                                        <td valign="top">
                                            住址
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlCity" runat="server" DataTextField="CityName" DataValueField="id"
                                                OnSelectedIndexChanged="ddlCity_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
                                                <contenttemplate>
                                                    <asp:DropDownList ID="ddlBuilding" runat="server" DataTextField="text" DataValueField="value" Visible="false"
                                                        OnSelectedIndexChanged="ddlBuilding_SelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </contenttemplate>
                                                <triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="ddlCity" EventName="SelectedIndexChanged" />
                                                </triggers>
                                            </asp:UpdatePanel>
                                            <br />                                            
                                            <asp:UpdatePanel ID="up4" RenderMode="Inline" ChildrenAsTriggers="false" UpdateMode="Conditional"
                                                runat="server">
                                                <contenttemplate>
                                                    <uc1:AddressInputBox ID="aib" runat="server" ShowStreetName="false" />
                                                </contenttemplate>
                                                <triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="ddlBuilding" EventName="SelectedIndexChanged" />
                                                    <asp:AsyncPostBackTrigger ControlID="ddlCity" EventName="SelectedIndexChanged" />
                                                </triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>                                        
                                        <td width="2">
                                        </td>
                                        <td>
                                            會員註冊於
                                        </td>
                                        <td>
                                            <asp:Literal ID="litRegisterFrom" runat="server" />
                                            , 風險 <asp:Literal ID="litRiskScore" runat="server" />
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td width="2">
                                        </td>
                                        <td>
                                            備註
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbC" Columns="50" MaxLength="100" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="2">
                                        </td>
                                        <td colspan="2">
                                            <asp:Button runat="server" ID="btnUnlockUser" Text="解除封鎖" TabIndex="107" OnClick="unlockAccount_Click"
                                                CssClass="frmbutton" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <br />
                                <asp:Button runat="server" ID="SaveButton" OnClick="SaveClick" Text="更新" Width="100" ValidationGroup="changeUserEvent"
                                    CssClass="frmbutton" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <br />
                                <br />
                                <a href="users.aspx" class="frmbutton">回到使用者列表</a>
                                <a href="ServiceIntegrate.aspx?Name=<%=Email.Text%>" class="frmbutton">回到客服整合系統</a>
                            </td>
                        </tr>
                    </table>
                    
                    <div id="mobileMemberContainer" style="margin-top:12px; display:none">
                        <fieldset>
                            <legend>
                                手機會員資訊
                            </legend>
                            <div>
                                <div style="margin: 4px;">認證手機: <span id="mobileNumber">...</span></div>
                                <hr />
                                <div style="margin: 4px 4px 4px 16px;">
                                    <div style="height:26px">
                                        會員狀態:
                                    </div>
                                    <div style="height:26px; padding-left:14px">
                                        <span style="display: inline-block; padding-right: 10px;" id="mobileMemberStatusDesc">
                                        </span>
                                        <button id="btnBindMobile" disabled="disabled" style="margin-right:8px"><span style="padding:4px">直接認證</span></button>
                                        <button id="btnUnbindMobile" disabled="disabled" style="margin-right:8px" title="如果只有一種登入方式，是不能解除手機綁定"><span style="padding:4px">解除綁定</span></button>
                                    </div>
                                    
                                    <div style="height:26px; margin-top:4px">
                                        簡訊發送次數歸零:
                                    </div>
                                    <div style="height:26px; padding-left:14px">
                                        <span style="display: inline-block; padding-right: 10px;">目前發送簡訊數: <span id="userSmsSentCount">...</span></span>
                                        <button id="btnResetSmsSentCount"><span style="padding:4px">重設</span></button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Panel ID="pS" runat="server">
						<table class="bodyText" cellspacing="0" width="100%" cellpadding="0" style="border: double 3px;">
                            <tr>
                                <td class="header" height="100%" colspan="4">
                                    會員狀態
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:CheckBoxList ID="cbS" runat="server" DataTextField="Text" DataValueField="Value"
                                        RepeatColumns="1" RepeatDirection="Horizontal" />
                                    <div style="padding-left:3px">
                                    <asp:CheckBox ID="chkIsApproved" runat="server" Text="允許登入"/>
                                    </div>
                                    <br />
                                    <asp:Button ID="bS" runat="server" OnClick="bS_Click" Text="修改狀態" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="header" height="100%" colspan="4">
                                    統計
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="color:black; font-size:18px;">
                                    訂單數: <asp:Literal ID="litOrderCount" runat="server" /> <asp:Literal ID="litSuccessOrderCount" runat="server" />
                                </td>
                            </tr>
                            <tr id="rowSuccessfulOrderRate" runat="server">
                                <td colspan="4" style="color:orangered; font-size:18px;">
                                    失敗率: <asp:Literal ID="lbFailOrderRate" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="color:black; font-size:18px;">
                                    營業額: <asp:Literal ID="litTurnover" runat="server" />
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table class="bodyText" cellspacing="5" width="100%" cellpadding="3" style="border: double 3px">
                            <tr>
                                <td class="header" height="100%" colspan="4">
                                    工具
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="bIm" runat="server" Text="模擬為此使用者" OnClick="Impersonate_Click" /><br />
                                    <span class="emtext">注意: 使用此功能, 在未重新登入之前, 將會無法進入後台</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                	<asp:Panel ID="panFixMembership" runat="server" Visible ="false">
                                	新密碼: <asp:TextBox ID="txtPassword" runat="server" Text="123456" />(FB或PZ登入的用不到,但請留預設值)
                                    <asp:Button ID="btnFixMembership" runat="server" Text="修復Membership" OnClick="btnFixMembership_Click" />
									</asp:Panel>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table class="bodyText" cellspacing="5" width="100%" cellpadding="3" style="border: double 3px">
                            <tr>
                                <td class="header" height="100%">
                                    串接資訊
                                </td>
                            </tr>
                            <tr runat="server" visible="false">
                                <td>
                                    PEZ紅利點數:(<asp:Label ID="lbPcash" runat="server" Text=""></asp:Label>)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gML" runat="server" GridLines="None" BorderWidth="1" ShowFooter="false"
                                        ShowHeader="true" AutoGenerateColumns="false" Width="100%" CellPadding="1" CellSpacing="1"
										 OnRowDataBound="gML_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="機構" HeaderStyle-Width="35%">
                                                <ItemTemplate>
                                                    <asp:Literal ID="litExternalOrg" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="編號" DataField="ExternalUserId" HeaderStyle-Width="65%" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            無串接資訊
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <asp:PlaceHolder ID="phPic" runat="server" Visible="false">
                            <tr>
                                <td colspan="4" style="color: black; font-size: 18px;">
                                    <img style="width:100px" runat="server" id="imgPic" />
                                </td>
                            </tr>
                            </asp:PlaceHolder>
                        </table>
                        <br />
                        <table class="bodyText" cellspacing="5" width="100%" cellpadding="3" style="border: double 3px">
                            <tr>
                                <td class="header" height="100%">
                                    會員資料刪除
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    備註:&nbsp;<span style="font-size: small">(可註記客服記錄連結、原因等等)</span><br />
                                    <asp:TextBox ID="txtInvalidMemberMemo" runat="server" TextMode="MultiLine" Rows="3" Width="300px"></asp:TextBox><br />
                                    <asp:Button ID="btnInvalidMember" runat="server" Text="刪除" OnClick="btnInvalidMember_Click" OnClientClick="return confirm('確定要刪除這個會員嗎?');" />
                                </td>
                            </tr>
                        </table>
                        <asp:Repeater ID="repOAuthUserClients" runat="server">
                            <HeaderTemplate>
                                <table class="bodyText" cellspacing="5" width="100%" cellpadding="3" style="border: double 3px; margin-top:20px">
                                    <tr>
                                        <td class="header" height="100%" colspan="2">
                                            應用程式設定
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            刪除
                                        </td>
                                        <td>
                                            名稱
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkDelete" runat="server" />
                                            <asp:HiddenField ID="hidId" runat="server" Value='<%#Eval("Id") %>' />
                                        </td>
                                        <td>
                                            <%#Eval("Name") %>
                                        </td>
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="btnUpdateOAuthUserClients" runat="server" OnClick="btnUpdateOAuthUserClients" Text="更新" />
                                    </td>
                                </tr>
                                </table>                                
                            </FooterTemplate>                        
                        </asp:Repeater>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br />
                    <table width="800" style="border: double 3px black">
                        <tr>
                            <th style="background-color: Gray">
                                記錄
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <uc2:AuditBoard ID="ab" EnableAdd="true" ShowAllRecords="true" ReferenceType="Member"
                                    runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
	<asp:Label ID="lblErrMsg" Visible="false" Text="無使用者" CssClass="emtext" runat="server"></asp:Label>
</asp:Content>

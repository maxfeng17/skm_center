using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.ControlRoom.User
{
    public partial class bonus_back : RolePage, IBonusExchangeBackView
    {
        public event EventHandler<DataEventArgs<Guid>> RedeemOrderDetailClicked;
        public event EventHandler<DataEventArgs<UpdatableRedeemOrderInfo>> RedeemOrderUpdated;
        public event EventHandler<DataEventArgs<int>> GetOrderCount;
        public event EventHandler<DataEventArgs<int>> PageChanged;

        #region props
        private BonusExchangeBackPresenter _presenter;
        public BonusExchangeBackPresenter Presenter 
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public string RedeemOrderId { get { return Request.QueryString["roid"]; } }

        public int PageSize { get { return pg.PageSize; } }
        #endregion

        #region interface members
        public void SetOrderListView(RedeemOrderCollection records)
        {
            p1.Visible = true;
            p2.Visible = false;
            gvo.DataSource = records;
            gvo.DataBind();
        }

        public void SetOrderDetailView(RedeemOrder theOrder, RedeemOrderDetailCollection theDetail)
        {
            p1.Visible = false;
            p2.Visible = true;

            RedeemOrderCollection col = new RedeemOrderCollection();
            col.Add(theOrder);
            dv.DataSource = col;
            gvod.DataSource = theDetail;
            this.Page.DataBind();
        }
        #endregion

        #region event handlers
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                _presenter.OnViewInitialized();
            _presenter.OnViewLoaded();
        }

        protected void gvo_RowCommand(object sender, CommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName == "d")
            {
                if (this.RedeemOrderDetailClicked != null)
                    this.RedeemOrderDetailClicked(sender, new DataEventArgs<Guid>((Guid)gvo.DataKeys[index].Value));
            }
        }

        private int rowCount;
        protected void gvOD_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                rowCount = 0;
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                RedeemOrderDetail data = (RedeemOrderDetail)e.Row.DataItem;
                e.Row.Cells[0].Text = (++rowCount).ToString();
                ((Literal)e.Row.Cells[4].FindControl("l")).Text = (data.ItemUnitPrice * data.ItemQuantity).ToString("N0") + Resources.Localization.Point;
            }
        }

        protected void dv_ModeChanging(object sender, DetailsViewModeEventArgs e)
        {
            dv.ChangeMode(e.NewMode);
            if (this.RedeemOrderDetailClicked != null)
                this.RedeemOrderDetailClicked(sender, new DataEventArgs<Guid>((Guid)dv.DataKey.Value));
        }

        protected void dv_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            if (this.RedeemOrderUpdated != null)
            {
                for (int i = 0; i < dv.Fields.Count; i++)
                    dv.Fields[i].ExtractValuesFromCell(e.NewValues, (DataControlFieldCell)dv.Rows[i].Cells[1], dv.Rows[i].RowState, true);
                UpdatableRedeemOrderInfo dtl = new UpdatableRedeemOrderInfo();
                dtl.Id = (Guid)dv.DataKey.Value;
                dtl.CustomerName = (string)e.NewValues[1];
                dtl.CustomerPhone = (string)e.NewValues[2];
                dtl.CustomerMobile = (string)e.NewValues[3];
                dtl.DeliveryAddress = (string)e.NewValues[4];
                dtl.OrderMemo = (string)e.NewValues[7];
                TextBox b = dv.Rows[6].Cells[0].FindControl("tbD") as TextBox;
                if (b != null)
                {
                    DateTime d;
                    if (DateTime.TryParse(b.Text, out d))
                        dtl.DeliveryTime = d;
                    else
                        dtl.DeliveryTime = null;
                }
                if (this.RedeemOrderUpdated != null)
                    this.RedeemOrderUpdated(this, new DataEventArgs<UpdatableRedeemOrderInfo>(dtl));

                dv.ChangeMode(DetailsViewMode.ReadOnly);
                if (this.RedeemOrderDetailClicked != null)
                    this.RedeemOrderDetailClicked(sender, new DataEventArgs<Guid>((Guid)dv.DataKey.Value));
            }
        }

        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChanged != null)
                this.PageChanged(this, new DataEventArgs<int>(pageNumber));
        }

        protected int RetrieveOrderCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (GetOrderCount != null)
            {
                GetOrderCount(this, e);
            }
            return e.Data;
        }
        #endregion

        protected void btnRedeem_Click(object sender, EventArgs e)
        {
            Response.Redirect("redeem_checkout.aspx");
        }
    }
}

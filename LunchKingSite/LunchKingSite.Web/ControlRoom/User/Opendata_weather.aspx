﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Opendata_weather.aspx.cs" 
    Inherits="LunchKingSite.Web.ControlRoom.User.Weather_opendata" MasterPageFile="~/ControlRoom/backend.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <style type="text/css">
        .bgOrange
        {
            background-color: #FFC690;
            text-align: center;
        }
        .bgBlue
        {
            background-color: #B3D5F0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table style="background-color: gray; width: 700px">
            <tr>
                <td colspan="2" style="background-color: Green; color: White; font-weight: bolder;
                    text-align: center">
                    天氣資料下載
                </td>
            </tr>
               <tr>
                <td colspan="2" class="bgBlue" style="text-align: center">
                    <asp:Button ID="Import" runat="server" Text="手動下載" OnClick="Import_Click" ValidationGroup="A" />
                </td>
                     </tr>
       
        </table>
    </div>
</asp:Content>
﻿<%@ Page Language="C#" MasterPageFile="../backend.master" AutoEventWireup="true" CodeBehind="MobileResetSms.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.User.MobileResetSms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <div style="margin: 20px" id="divMessage" runat="server">
        </div>
        <fieldset style="border-color: #ccddef">
            <legend>手機認證: 發送次數重置</legend>
            <div style="border-color: #ccddef">
                <table>
                    <tr>
                        <td width="2"></td>
                        <td>
                            <span class="adminlabel">查詢手機號碼: </span>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtMobile" MaxLength="128" TabIndex="102" Columns="30"
                                CssClass="adminlabel" />

                        </td>
                    </tr>
                </table>
            </div>
            <div style="text-align: center; margin-top: 20px">
                <asp:Button runat="server" ID="btnQuery" Text="查詢"
                    CssClass="frmbutton" OnClick="btnQuery_Click" />
                <asp:Button runat="server" ID="btnReset" Text="重置" CssClass="frmbutton" Style="margin-left: 20px"
                    OnClick="btnReset_Click" />
            </div>
        </fieldset>
    </div>
</asp:Content>

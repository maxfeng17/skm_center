<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    Codebehind="bonus_back.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.User.bonus_back"
    Title="紅利兌換管理" %>

<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="width: 800px">
        <asp:Panel ID="p1" runat="server" Visible="false">
            <asp:Button ID="btnRedeem" runat="server" Text="PEZ請款" 
                onclick="btnRedeem_Click" /><br />
            <asp:Literal ID="l4" runat="server" Text="<%$Resources:Localization, RedeemRecords%>" />
            <asp:GridView ID="gvo" runat="server" GridLines="None" Width="100%" CellPadding="4"
                OnRowCommand="gvo_RowCommand" DataKeyNames="GUID" AutoGenerateColumns="false">
                <Columns>
                    <asp:ButtonField HeaderText="<%$ Resources:Localization,OrderNo %>" DataTextField="OrderId"
                        CommandName="d"></asp:ButtonField>
                    <asp:BoundField HeaderText="<%$Resources:Localization,MemberName%>" DataField="MemberName" />
                    <asp:BoundField HeaderText="<%$ Resources:Localization,Total %>" DataField="Total"
                        DataFormatString="{0:N0}" HtmlEncode="False"></asp:BoundField>
                    <asp:BoundField HeaderText="<%$ Resources:Localization,RedeemTime %>" DataField="CreateTime"
                        DataFormatString="{0:d}"></asp:BoundField>
                    <asp:BoundField HeaderText="<%$ Resources:Localization,DeliveryDate %>" DataField="DeliveryTime"
                        DataFormatString="{0:d}"></asp:BoundField>
                </Columns>
                <EmptyDataTemplate>
                    Oops! 您還未有記錄
                </EmptyDataTemplate>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="Gainsboro" HorizontalAlign="left" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" HorizontalAlign="left" />
                <EditRowStyle BackColor="#999999" />
                <AlternatingRowStyle BackColor="White" HorizontalAlign="left" />
            </asp:GridView>
            <uc1:Pager ID="pg" runat="server" PageSize="30" OnGetCount="RetrieveOrderCount" OnUpdate="UpdateHandler" />
        </asp:Panel>
        <asp:Panel ID="p2" runat="server">
            <asp:DetailsView ID="dv" runat="server" AutoGenerateRows="False" Width="100%" CellPadding="4"
                AutoGenerateEditButton="true" ForeColor="#333333" GridLines="None" DataKeyNames="GUID"
                OnModeChanging="dv_ModeChanging" OnItemUpdating="dv_ItemUpdating">
                <Fields>
                    <asp:BoundField HeaderText="<%$Resources:Localization,Email%>" DataField="MemberEmail"
                        ReadOnly="true" />
                    <asp:BoundField HeaderText="<%$Resources:Localization,MemberName%>" DataField="MemberName" />
                    <asp:BoundField HeaderText="<%$Resources:Localization,CompanyTel%>" DataField="PhoneNumber" />
                    <asp:BoundField HeaderText="<%$Resources:Localization,MobileNumber%>" DataField="MobileNumber" />
                    <asp:BoundField HeaderText="<%$Resources:Localization,CompanyDetailAddress%>" DataField="DeliveryAddress" />
                    <asp:BoundField HeaderText="<%$Resources:Localization,RedeemTime%>" DataField="CreateTime"
                        ReadOnly="true" />
                    <asp:TemplateField ConvertEmptyStringToNull="true">
                        <HeaderTemplate>
                            <asp:Literal ID="l1" runat="server" Text="<%$Resources:Localization,DeliveryDate%>" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Literal ID="lD" runat="server" Text='<%#Eval("DeliveryTime")%>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="tbD" runat="server" Text='<%#Eval("DeliveryTime")%>' />
                            <cc1:CalendarExtender ID="ce" runat="server" TargetControlID="tbD" Format="yyyy/MM/dd" />
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="<%$Resources:Localization,BonusDeduct%>" DataField="Total"
                        ReadOnly="true" DataFormatString="{0:N0}" />
                    <asp:BoundField HeaderText="備註" DataField="OrderMemo" ConvertEmptyStringToNull="true" ControlStyle-Width="400" />
                </Fields>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#999999" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:DetailsView>
            <br />
            <asp:GridView ID="gvod" runat="server" Width="100%" CellPadding="2" CellSpacing="1"
                AutoGenerateColumns="false" BorderWidth="0" GridLines="none" OnRowDataBound="gvOD_RowDataBound"
                DataKeyNames="GUID">
                <RowStyle BackColor="#FFFFFF" CssClass="style14" Height="25" />
                <AlternatingRowStyle BackColor="#EAEAEA" CssClass="style14" Height="25" />
                <HeaderStyle BackColor="#EAEAEA" CssClass="style14" Height="20" />
                <Columns>
                    <asp:TemplateField HeaderText="<%$Resources:Localization, Stance%>" ItemStyle-HorizontalAlign="center" />
                    <asp:BoundField HeaderText="<%$Resources:Localization, ItemName%>" ItemStyle-HorizontalAlign="Left"
                        DataField="ItemName" HtmlEncode="false" />
                    <asp:BoundField HeaderText="<%$Resources:Localization, Quantity%>" ItemStyle-HorizontalAlign="center"
                        DataField="ItemQuantity" />
                    <asp:BoundField HeaderText="<%$Resources:Localization, UnitPrice%>" ItemStyle-HorizontalAlign="right"
                        DataField="ItemUnitPrice" DataFormatString="{0:N0}點" HtmlEncode="False" />
                    <asp:TemplateField HeaderText="<%$Resources:Localization, BonusExchange%>" ItemStyle-HorizontalAlign="right">
                        <ItemTemplate>
                            <asp:Literal ID="l" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </div>
</asp:Content>

<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../backend.master" Inherits="Admin_admin_users" Title="會員列表" 
    Trace="false" CodeBehind="Users.aspx.cs" ClientIDMode="Static"  %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>

<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>

<asp:Content ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript">
        $(document).ready(function() {
            $('#txtKeyword').keydown(function(evt) {
                if (evt.keyCode == 13) {
                    $('#btnSearch').trigger('click');
                    return false;
                }
            });
            $('#SearchByDropDown').change(function() {
                $('#txtKeyword').focus();
            });
            $('#txtKeyword').focus();
        });
    </script>
    <style type="text/css">
        .frmbutton {
            padding:4px 16px 4px 16px;
            margin-left:6px;
            cursor: pointer;
        }
        .frmtext {
            width:200px;
            padding:3px;
        }
        .frmselect {
            padding:3px;
        }
    </style>    
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="centercontent">
        <br />
        <script type="text/javascript">function ConfDel() { return confirm('確定要刪除此使用者?'); } </script>

        <table cellspacing="0" cellpadding="0" class="lrbBorders" width="100%" style="margin-bottom:10px">
            <tr>
                <td class="bodyTextLowTopPadding">
                    <asp:Label ID="Label1" runat="server" AssociatedControlID="SearchByDropDown" Text="依: " Font-Bold="true" CssClass="adminlabel" />
                    <asp:DropDownList runat="server" ID="SearchByDropDown" CssClass="frmselect">
                        <asp:ListItem Text="帳號" Value="Username" />
                        <asp:ListItem Text="姓名" Value="MemberName" />
                        <asp:ListItem Text="地區" Value="BuildingId" />
                        <asp:ListItem Text="大樓名稱" Value="BuildingName" />
                        <asp:ListItem Text="公司" Value="CompanyName" />
                        <asp:ListItem Text="手機" Value="Mobile" />
                        <asp:ListItem Text="串接編號" Value="ExternalId" />
                        <asp:ListItem Text="UserId" Value="UserId" />
                        <asp:ListItem Text="認證手機" Value="RegistedMobile" />
                    </asp:DropDownList>&nbsp;&nbsp;
                    <asp:TextBox runat="server" ID="txtKeyword" CssClass="frmtext" />

                    <asp:Button ID="btnSearch" runat="server" Text="搜尋" OnClick="SearchForUsers" CssClass="frmbutton" />
                </td>
            </tr>
        </table>

        <asp:GridView ID="GridView1" runat="server"
            AutoGenerateColumns="False"
            EmptyDataText="無符合的使用者, 請重新搜尋"
            Font-Italic="False" CellPadding="10" ForeColor="#333333" GridLines="None" AllowSorting="true" AllowPaging="true" PageSize="10" PagerSettings-Visible="false" OnSorting="GridView1_Sorting">
            <Columns>
                <asp:TemplateField HeaderText="帳號" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <a target="_blank" href='users_edit.aspx?username=<%#HttpUtility.UrlEncode((string)Eval("UserName"))%>'>
                            <%#ShowUserName((ViewMemberBuildingCity)Container.DataItem) %>
                        </a>
                    </ItemTemplate>
                    <ItemStyle />
                </asp:TemplateField>
                <asp:BoundField HeaderText="姓名" DataField="MemberName" ItemStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="地區">
                    <ItemTemplate>
                        <%# (String.IsNullOrEmpty((string)(DataBinder.Eval(Container.DataItem, "BuildingId"))) ? "" : ((string)DataBinder.Eval(Container.DataItem, "BuildingId")).Substring(0, 11)) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="BuildingName" HeaderText="大樓" />
                <asp:BoundField DataField="CompanyName" HeaderText="公司" ItemStyle-Wrap="false" />
                <asp:BoundField DataField="CreateTime" HeaderText="加入時間" DataFormatString="{0:yyyy/MM/dd HH:mm}" ItemStyle-Wrap="false" />
                <asp:TemplateField HeaderText="會員類別" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <%# ShowMemberLevel((ViewMemberBuildingCity)Container.DataItem) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="狀態">
                    <ItemTemplate>
                        <%# ShowStatus((ViewMemberBuildingCity)Container.DataItem) %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataRowStyle Font-Italic="True" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#EFF3FB" />
            <EditRowStyle BackColor="#2461BF" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        <uc1:Pager ID="Pager1" runat="server" PageSize="10" OnGetCount="GetMemberCount" OnUpdate="UpdateHandler" Visible="true" />

        <br />
        <br />

        <div style="margin-top: 5px">
            <a href="Users_edit.aspx">新增使用者</a>
        </div>
    </div>
</asp:Content>

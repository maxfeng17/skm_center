﻿using System.Diagnostics;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.BizLogic.Facade;
using PaymentType = LunchKingSite.Core.PaymentType;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web;


namespace LunchKingSite.Web.ControlRoom.User
{
    public partial class ServiceOutgrate : RolePage, IUserServiceOutgrateView
    {
        protected static IAccountingProvider ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        protected static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected static IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        protected static IOrderProvider odrProv = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected static IPponProvider pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected static LunchKingSite.Core.IServiceProvider sp = ProviderFactory.Instance().GetProvider<LunchKingSite.Core.IServiceProvider>();

        IDictionary<Guid, Guid> BidLookupByOid = new Dictionary<Guid, Guid>();

        #region property

        private UserServiceOutgratePresenter _presenter;
        public UserServiceOutgratePresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public Dictionary<Subscription, bool> UnsubscribeList
        {
            get
            {
                return GetUnsubscribeList();
            }
        }

        public Dictionary<HiDealSubscription, bool> UnHidealsubscribeList
        {
            get
            {
                return GetHidealUnsubscribeList();
            }
        }

        public int FilterTypeValue
        {
            get
            {
                return Convert.ToInt32(ddlType.SelectedValue);
            }
        }

        public UserServiceIntergrateIDEASFilterType IDEASFilterType
        {
            get
            {
                return (UserServiceIntergrateIDEASFilterType)Convert.ToInt32(ddlIDEASType.SelectedValue);
            }
        }

        public UserServiceIntergrateTmallFilterType TmallFilterType
        {
            get
            {
                return (UserServiceIntergrateTmallFilterType)Convert.ToInt32(ddl_Tmall.SelectedValue);
            }
        }

        public string IDEASSearchData
        {
            get { return txtIDEASSearch.Text; }
        }

        public string TmallSearchData
        {
            get { return tbx_Tmall.Text; }
        }

        public int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(UserData);
            }
        }

        public bool CBIsFunds
        {
            get { return cbIsFunds.Checked; }
        }

        public string UserData
        {
            get
            {
                return hidUserData.Value;
            }
            set
            {
                hidUserData.Value = value;
            }
        }

        public string UserDataView
        {
            get
            {
                return txtUserData.Text;
            }
            set
            {
                txtUserData.Text = value;
            }
        }

        public string ExternalUserId
        {
            get
            {
                return hidExternalUserId.Value;
            }
            set
            {
                hidExternalUserId.Value = value.ToString();
            }
        }

        public int PageSize
        {
            get { return ucOrderPager.PageSize; }
            set { ucOrderPager.PageSize = value; }
        }

        public string SortExpression
        {
            get { return LunchKingSite.DataOrm.Order.Columns.CreateTime; }
        }

        public string HidealSortExpression
        {
            get { return ViewHiDealOrderDealOrderReturned.Columns.OrderPk + " desc, " + ViewHiDealOrderDealOrderReturned.Columns.CreateTime; }
        }

        public MemberPromotionDeposit MemberPromotionDeposit
        {
            get { return GetMemberPromotionDepositData(); }
        }

        public bool IfReceiveEDM
        {
            get { return chkIfReceiveEDM.Checked; }
            set { chkIfReceiveEDM.Checked = value; }
        }

        public bool IfReceiveStartedSell
        {
            get { return chkIfReceiveStartedSell.Checked; }
            set { chkIfReceiveStartedSell.Checked = value; }
        }

        public bool IfReceivePiinlifeEDM
        {
            get { return chkIfReceivePiinlifeEDM.Checked; }
            set { chkIfReceivePiinlifeEDM.Checked = value; }
        }

        public bool IfReceivePiinlifeStartedSell
        {
            get { return chkIfReceivePiinlifeStartedSell.Checked; }
            set { chkIfReceivePiinlifeStartedSell.Checked = value; }
        }

        public int RefundScashAmount
        {
            get
            {
                int value;
                return int.TryParse(tbScashAmount.Text, out value) ? value : 0;
            }
        }

        public string CreateId
        {
            get
            {
                return User.Identity.Name;
            }
        }

        public bool IsBcashPayByVendor
        {
            get
            {
                return chkBcashPayByVendor.Checked;
            }
        }

        public Guid? DealGuid
        {
            get
            {
                if (!string.IsNullOrEmpty(hidDealGuid.Value))
                {
                    return Guid.Parse(hidDealGuid.Value);
                }

                return null;
            }
        }

        public string GetStatusTypeDropDown
        {
            get { return ddlStatus.SelectedValue; }
        }

        private enum TabPageName
        {
            OrderPager,
            HidealOrderPager,
            OrderLunchKingPager,
            BonusList,
            ServicePager,
            SubscriptionPaper,
            SuperBonusPaper,
            SuperDiscountTicketPaper,
            SuperDiscountPointPaper,
            MembershipCardPaper,
            NotClosed,
            Closed
        }

        #endregion property

        #region event

        public event EventHandler OnSearchClicked;
        public event EventHandler OnStatusSelected;
        public event EventHandler OnIDEASSearchClicked;
        public event EventHandler OnTmallSearchClicked;
        public event EventHandler OnBcashAddClicked;
        public event EventHandler OnRefundScashClicked;

        public event EventHandler<DataEventArgs<ServiceMessage>> OnSetServiceMsgClicked;
        public event EventHandler<EventArgs> OnSubscribeUpdateClick;
        public event EventHandler<DataEventArgs<int>> OnGetOrderCount;
        public event EventHandler<DataEventArgs<int>> OrderPageChanged;
        public event EventHandler<DataEventArgs<int>> OnGetHidealOrderCount;
        public event EventHandler<DataEventArgs<int>> HidealOrderPageChanged;
        public event EventHandler<DataEventArgs<int>> OnGetServiceCount;
        public event EventHandler<DataEventArgs<int>> ServicePageChanged;
        public event EventHandler<DataEventArgs<int>> OnGetNotClosedCount;
        public event EventHandler<DataEventArgs<int>> NotClosedPageChanged;
        public event EventHandler<DataEventArgs<int>> OnGetClosedCount;
        public event EventHandler<DataEventArgs<int>> ClosedPageChanged;
        public event EventHandler<DataEventArgs<int>> OnGetLunchKingCount;
        public event EventHandler<DataEventArgs<int>> OrderLunchKingPageChanged;
        public event EventHandler<DataEventArgs<string>> GetBonusListInfo;
        public event EventHandler<DataEventArgs<int>> OnGetIDEASOrderCount;
        public event EventHandler<DataEventArgs<int>> OnIDEASOrderPageChanged;
        public event EventHandler<DataEventArgs<int>> OnGetTmallCount;
        public event EventHandler<DataEventArgs<int>> OnTmallPageChanged;

        public event EventHandler<DataEventArgs<string>> GetSuperBonusListInfo;
        public event EventHandler<DataEventArgs<int>> OnGetSuperBonusCount;
        public event EventHandler<DataEventArgs<int>> SuperBonusPageChanged;

        public event EventHandler<DataEventArgs<string>> GetSuperDiscountTicketListInfo;
        public event EventHandler<DataEventArgs<int>> OnGetSuperDiscountTicketCount;
        public event EventHandler<DataEventArgs<int>> SuperDiscountTicketPageChanged;

        public event EventHandler<DataEventArgs<string>> GetSuperDiscountPointListInfo;
        public event EventHandler<DataEventArgs<int>> OnGetSuperDiscountPointCount;
        public event EventHandler<DataEventArgs<int>> SuperDiscountPointPageChanged;

        public event EventHandler<DataEventArgs<string>> GetMembershipCardListInfo;
        public event EventHandler<DataEventArgs<int>> OnGetMembershipCardCount;
        public event EventHandler<DataEventArgs<int>> MembershipCardPageChanged;


        #endregion event

        #region method

        public void SetUserName(MemberCollection mc)
        {
            if (mc.Count > 1)
            {
                divMultiUser.Visible = true;
                Dictionary<string, string> item = new Dictionary<string, string>();
                foreach (Member m in mc)
                {
                    string displayName = string.Format("[{0}] {1}", m.DisplayName, m.UserName);
                    if (m.IsGuest)
                    {
                        displayName = string.Format("{0} [{1}]", displayName, Member._GUEST_MEMBER);
                    }
                    item.Add(m.UserName, displayName);
                }
                rdlUser.DataTextField = "Value";
                rdlUser.DataValueField = "Key";
                rdlUser.DataSource = item;
                rdlUser.DataBind();

                panDisplay.Visible = false;
                tbUserData.Visible = false;
                lblErrMsg.Visible = false;
            }
            else
            {
                panDisplay.Visible = false;
                tbUserData.Visible = false;
                txtUserData.Text = string.Empty;
                lblErrMsg.Visible = true;
            }
        }

        public void SetMemberData(ViewMemberBuildingCityParentCity m, MobileMember mm)
        {
            lblErrMsg.Visible = false;
            divMultiUser.Visible = false;
            tbUserData.Visible = true;
            panDisplay.Visible = true;
            lblMessage.Text = string.Empty;
            lblSubscriptionMsg.Text = string.Empty;
            txtIMessage.Text = string.Empty;
            txtIOrder.Text = string.Empty;
            lbRegisteredMobile.Text = string.Empty;

            TabContainer1.ActiveTabIndex = 0;


            Member mem = mp.MemberGet(m.UniqueId);

            hkUserName.Text = lblIName.Text = mem.DisplayName;
            string account = m.UserName;
            if (RegExRules.CheckEmail(account))
            {
                lblEmail.Text = lblIEmail.Text = m.UserName;
            }
            else if (mm.IsLoaded)
            {
                lblEmail.Text = lblIEmail.Text = mm.MobileNumber;
            }
            else
            {
                lblEmail.Text = lblIEmail.Text = "";
            }

            if (mem.IsGuest)
            {
                litGuestMemberMark.Text = string.Format("[{0}]", Member._GUEST_MEMBER);
            }
            else
            {
                litGuestMemberMark.Text = "";
            }
            hkUserName.NavigateUrl = ResolveUrl("~/controlroom/User/users_edit.aspx?username=") + System.Web.HttpUtility.UrlEncode(m.UserName);
            lblMobile.Text = lblIPhone.Text = m.Mobile;
            lblUserEmail.Text = mem.UserEmail;
            if (mm.IsLoaded)
            {
                lbRegisteredMobile.Text = mm.MobileNumber;
            }
        }

        public void SetMemberAddress(string address)
        {
            lblAddress.Text = address;
        }

        public void SetMemberLinkList(MemberLinkCollection mlc)
        {
            MemberLink ml = mlc.Where(x => x.ExternalOrg.Equals((int)SingleSignOnSource.PayEasy)).FirstOrDefault();
            if (ml != null)
                this.ExternalUserId = ml.ExternalUserId;

            gvLoginType.DataSource = mlc;
            gvLoginType.DataBind();
        }

        public void SetMemberBonus(ViewMemberPromotionTransactionCollection vmptc)
        {
            lblBCash.Text = (vmptc.Count > 0) ? vmptc[vmptc.Count - 1].RunSum.Value.ToString("N0") : "0";
        }

        public void SetMemberScash(decimal scash)
        {
            lblSCash.Text = scash.ToString("N0");
        }

        public void SetOrderData(DataTable dt)
        {
            if (checkFirstTabClick(TabPageName.OrderPager)) { ucOrderPager.ResolvePagerView(1, true); }

            gvOrderList.DataSource = dt;
            gvOrderList.DataBind();
        }

        public void SetHidealOrderList(ViewHiDealOrderDealOrderReturnedCollection vhodor)
        {
            if (checkFirstTabClick(TabPageName.HidealOrderPager)) { ucHidealPager.ResolvePagerView(1, true); }
            gvHidealOrderList.DataSource = vhodor;
            gvHidealOrderList.DataBind();
        }

        public void SetLunchKingOrderData(DataTable dt)
        {
            //ucOrderLunchKingPager.ResolvePagerView(1, true);
            if (checkFirstTabClick(TabPageName.OrderLunchKingPager)) { ucOrderLunchKingPager.ResolvePagerView(1, true); }

            gvLunchKingList.DataSource = dt;
            gvLunchKingList.DataBind();
        }

        public void SetBonusData(AuditCollection data, Dictionary<ViewMemberPromotionTransaction, string> dataList, ViewMemberCashpointListCollection vmclc, List<UserScashTransactionInfo> vstc, ViewOrderMemberPaymentCollection vompcp, ViewOrderMemberPaymentCollection vompcc, ViewDiscountOrderCollection vdoc)
        {
            SetBonusList(data);

            gvBonusConvert.DataSource = dataList;
            gvBonusConvert.DataBind();

            gvScashOld.DataSource = vmclc.OrderByDescending(x => x.CreateTime);
            gvScashOld.DataBind();

            gvScash.DataSource = vstc.OrderByDescending(x => x.CreateTime).ThenByDescending(c => c.WithdrawalId);
            gvScash.DataBind();

            gvPcash.DataSource = vompcp;
            gvPcash.DataBind();

            gvCreditcard.DataSource = vompcc;
            gvCreditcard.DataBind();

            gvDiscountCode.DataSource = vdoc;
            gvDiscountCode.DataBind();
        }

        public void SetMembershipCardData(List<ViewMembershipCardLog> membbershipCardList)
        {
            if (checkFirstTabClick(TabPageName.MembershipCardPaper)) { ucMembershipCardPager.ResolvePagerView(1, true); }

            ucMembershipCardPager.Visible = membbershipCardList.Count > 0;
            gvMembershipCardConvert.DataSource = membbershipCardList;
            gvMembershipCardConvert.DataBind();

        }


        public void SetBonusList(AuditCollection data)
        {
            gvBonusList.DataSource = data;
            gvBonusList.DataBind();
        }

        public void SetServiceMessageList(Dictionary<ServiceMessage, Guid> dataList)
        {
            if (checkFirstTabClick(TabPageName.ServicePager)) { ucServicePager.ResolvePagerView(1, true); }
            gvServiceRecord.DataSource = dataList;
            gvServiceRecord.DataBind();
        }


        public void SetSubscriptionList(SubscriptionCollection subscriptionList, HiDealSubscriptionCollection hidealSubscriptionList)
        {
            gvSubscription.DataSource = subscriptionList;
            gvSubscription.DataBind();

            gvHidealSubcription.DataSource = hidealSubscriptionList;
            gvHidealSubcription.DataBind();
        }

        public void SetIDEASOrderList(ViewCompanyUserOrderCollection orderCollection)
        {
            //隱藏客服資料查詢會顯示的會員資料
            panDisplay.Visible = false;
            //顯示IDEAS訂單列表
            divIDEASOrder.Visible = true;
            IDEASOrderGrid.DataSource = orderCollection;
            IDEASOrderGrid.DataBind();
        }

        public void SetTmallList(ViewOrderCorrespondingSmsLogCollection logCollection)
        {
            //隱藏客服資料查詢會顯示的會員資料
            panDisplay.Visible = false;
            //顯示IDEAS訂單列表
            pan_Tmall.Visible = true;
            gv_Tmall.DataSource = logCollection;
            gv_Tmall.DataBind();
        }

        public void SetStatusList(Dictionary<string, string> list)
        {
            ddlStatus.DataSource = list;
            ddlStatus.DataBind();
        }

        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlType.DataSource = Enum.GetValues(typeof(FilterType)).Cast<FilterType>()
                    .Select(x => new { Text = Helper.GetEnumDescription((FilterType)x), Value = (int)x });
                ddlType.DataTextField = "Text";
                ddlType.DataValueField = "Value";
                ddlType.DataBind();

                if (!String.IsNullOrEmpty(Request["name"]))
                {
                    txtUserData.Text = Request["name"];
                }
                _presenter.OnViewInitialized();
                this.buildPriorityRbl(ref rblPriority);
            }
            _presenter.OnViewLoaded();
            lblMessage.Text = string.Empty;
            lblSubscriptionMsg.Text = string.Empty;
            AddJavascript();
            divIDEASOrder.Visible = false;

            if (IsPostBack == false)
            {
                if (!String.IsNullOrEmpty(Request["userId"]))
                {
                    ddlType.SelectedValue = ((int)FilterType.UserId).ToString();
                    txtUserData.Text = Request["userId"];
                    this.OnSearchClicked(this, e);
                }
                else if (!String.IsNullOrEmpty(Request["mobile"]))
                {
                    ddlType.SelectedValue = ((int)FilterType.Mobile).ToString();
                    txtUserData.Text = Request.QueryString["mobile"];
                    this.OnSearchClicked(this, e);
                }
                else if (!String.IsNullOrEmpty(Request["name"]))
                {
                    ddlType.SelectedValue = ((int)FilterType.UserName).ToString();
                    txtUserData.Text = Request["name"];
                    this.OnSearchClicked(this, e);
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Session[LkSiteSession.ServiceIntegrate.ToString()] = new Dictionary<string, int>();
            Session[LkSiteSession.Search.ToString()] = ddlStatus.SelectedValue.ToString();
            this.lblTabBonusHint.Text = "讀取中請稍候，這頁資料量比較大要等比較久，請勿切換上排頁籤。";

            //clear grid view when search click

            var gridViewReset = new GridView[] { gvHidealOrderList, gvLunchKingList, gvBonusList
                , gvBonusConvert, gvScash, gvScashOld, gvPcash, gvCreditcard, gvDiscountCode
                , gvServiceRecord, gvSubscription, gvHidealSubcription};

            foreach (var item in gridViewReset)
            {
                item.DataSource = null;
                item.DataBind();
            }

            if (!string.IsNullOrWhiteSpace(txtUserData.Text) && this.OnSearchClicked != null)
            {
                this.OnSearchClicked(this, e);
            }
        }

        protected void IDEASSearch_Click(object sender, EventArgs e)
        {
            IDEASOrderGridPager.ResolvePagerView(1, true);
            if (OnIDEASSearchClicked != null)
            {
                OnIDEASSearchClicked(this, e);
            }
        }

        protected void btn_TmallSearch_Click(object sender, EventArgs e)
        {
            page_Tmall.ResolvePagerView(1, true);
            if (OnTmallSearchClicked != null)
            {
                OnTmallSearchClicked(this, e);
            }
        }

        protected void btnBcashAdd_Click(object sender, EventArgs e)
        {
            if (!BcashAddCheck())
            {
                return;
            }

            if (this.OnBcashAddClicked != null)
            {
                this.OnBcashAddClicked(this, e);
            }

            ScriptManager.RegisterStartupScript(Page, typeof(Button), "alert", "alert('補紅利完成!!');", true);
        }

        protected void btRefundScash_Click(object sender, EventArgs e)
        {
            if (this.OnRefundScashClicked != null)
            {
                this.OnRefundScashClicked(this, e);
            }

            ScriptManager.RegisterStartupScript(Page, typeof(Button), "alert", "alert('購物金回收完成!!');", true);
        }

        protected void btnSetServiceMsg_Click(object sender, EventArgs e)
        {
            bool isOrderValid = false;
            if (txtIOrder.Text.Trim() != string.Empty)
            {
                LunchKingSite.DataOrm.Order order = ProviderFactory.Instance().GetProvider<IOrderProvider>().OrderGet(LunchKingSite.DataOrm.Order.Columns.OrderId, txtIOrder.Text.Trim());
                isOrderValid = order.OrderId != null;
            }

            if (string.IsNullOrWhiteSpace(txtIOrder.Text.Trim()) || isOrderValid)
            {
                if (this.OnSetServiceMsgClicked != null)
                {
                    this.OnSetServiceMsgClicked(sender, new DataEventArgs<ServiceMessage>(GetServiceMessageData()));
                }

                lblMessage.Text = "客服資料新增成功";
                ddlCategory.SelectedIndex = 0;
                rdlWorkType.SelectedIndex = -1;
                txtIMessage.Text = "";
            }
            else
            {
                lblMessage.Text = "無此訂單編號!!";
            }
        }

        protected void btnSubscribeUpdate_Click(object sender, EventArgs e)
        {
            if (this.OnSubscribeUpdateClick != null)
            {
                this.OnSubscribeUpdateClick(null, e);
            }

            lblSubscriptionMsg.Text = "狀態修改成功!!";
            lblMessage.Text = string.Empty;
        }

        protected void OrderUpdateHandler(int pageNumber)
        {
            AddTabPagerSession(TabPageName.OrderPager, pageNumber);
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.OrderPageChanged != null)
            {
                this.OrderPageChanged(this, e);
            }
        }

        protected void LunchKingOrderUpdateHandler(int pageNumber)
        {
            AddTabPagerSession(TabPageName.OrderLunchKingPager, pageNumber);
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.OrderLunchKingPageChanged != null)
            {
                this.OrderLunchKingPageChanged(this, e);
            }
        }

        protected int GetOrderCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetOrderCount != null)
            {
                OnGetOrderCount(this, e);
            }
            return e.Data;
        }

        #region SuperBonus

        protected int GetSuperBonusCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetSuperBonusCount != null)
            {
                OnGetSuperBonusCount(this, e);
            }
            return e.Data;
        }

        protected void SuperBonusUpdateHandler(int pageNumber)
        {
            AddTabPagerSession(TabPageName.SuperBonusPaper, pageNumber);
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.SuperBonusPageChanged != null)
            {
                this.SuperBonusPageChanged(this, e);
            }
        }


        #endregion SuperBonus

        #region  SuperDiscountTicket

        protected int GetSuperDiscountTicketCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetSuperDiscountTicketCount != null)
            {
                OnGetSuperDiscountTicketCount(this, e);
            }
            return e.Data;
        }

        protected void SuperDiscountTicketUpdateHandler(int pageNumber)
        {
            AddTabPagerSession(TabPageName.SuperDiscountTicketPaper, pageNumber);
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.SuperDiscountTicketPageChanged != null)
            {
                this.SuperDiscountTicketPageChanged(this, e);
            }
        }

        #endregion SuperDiscountTicket

        #region SuperDiscountPoint
        protected int GetSuperDiscountPointCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetSuperDiscountPointCount != null)
            {
                OnGetSuperDiscountPointCount(this, e);
            }
            return e.Data;
        }

        protected void SuperDiscountPointUpdateHandler(int pageNumber)
        {
            AddTabPagerSession(TabPageName.SuperDiscountPointPaper, pageNumber);
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.SuperDiscountPointPageChanged != null)
            {
                this.SuperDiscountPointPageChanged(this, e);
            }
        }

        #endregion SuperDiscountPoint

        #region MembershipCard

        protected int GetMembershipCardCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetMembershipCardCount != null)
            {
                OnGetMembershipCardCount(this, e);
            }
            return e.Data;
        }

        protected void MembershipCardUpdateHandler(int pageNumber)
        {
            AddTabPagerSession(TabPageName.MembershipCardPaper, pageNumber);
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.MembershipCardPageChanged != null)
            {
                this.MembershipCardPageChanged(this, e);
            }
        }

        #endregion MembershipCard

        protected void HidealOrderUpdateHandler(int pageNumber)
        {
            AddTabPagerSession(TabPageName.HidealOrderPager, pageNumber);

            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.HidealOrderPageChanged != null)
            {
                this.HidealOrderPageChanged(this, e);
            }
        }

        protected int GetHidealOrderCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetHidealOrderCount != null)
            {
                OnGetHidealOrderCount(this, e);
            }
            return e.Data;
        }

        protected int GetLunchKingCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetLunchKingCount != null)
            {
                OnGetLunchKingCount(this, e);
            }
            return e.Data;
        }

        protected void ServiceUpdateHandler(int pageNumber)
        {
            AddTabPagerSession(TabPageName.ServicePager, pageNumber);

            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.ServicePageChanged != null)
            {
                this.ServicePageChanged(this, e);
            }
        }

        protected int GetServiceCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetServiceCount != null)
            {
                OnGetServiceCount(this, e);
            }
            return e.Data;
        }

        protected void NotClosedUpdateHandler(int pageNumber)
        {
            AddTabPagerSession(TabPageName.ServicePager, pageNumber);

            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.NotClosedPageChanged != null)
            {
                this.NotClosedPageChanged(this, e);
            }
        }

        protected int GetNotClosedCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetNotClosedCount != null)
            {
                OnGetNotClosedCount(this, e);
            }
            return e.Data;
        }

        protected void ClosedUpdateHandler(int pageNumber)
        {
            AddTabPagerSession(TabPageName.ServicePager, pageNumber);

            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.ClosedPageChanged != null)
            {
                this.ClosedPageChanged(this, e);
            }
        }

        protected int GetClosedCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetClosedCount != null)
            {
                OnGetClosedCount(this, e);
            }
            return e.Data;
        }

        protected void gvLoginType_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                MemberLink ml = (MemberLink)e.Row.DataItem;
                Label lblExternalOrg = (Label)e.Row.FindControl("lblExternalOrg");
                if ((SingleSignOnSource)ml.ExternalOrg == SingleSignOnSource.ContactDigitalIntegration)
                {
                    lblExternalOrg.Text = "Email";
                }
                else if ((SingleSignOnSource)ml.ExternalOrg == SingleSignOnSource.Mobile17Life)
                {
                    lblExternalOrg.Text = "手機";
                }
                else
                {
                    lblExternalOrg.Text = ((SingleSignOnSource)ml.ExternalOrg).ToString();
                }
            }
        }

        protected void IDEASOrderUpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (OnIDEASOrderPageChanged != null)
            {
                OnIDEASOrderPageChanged(this, e);
            }
        }

        protected void TmallUpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (OnTmallPageChanged != null)
            {
                OnTmallPageChanged(this, e);
            }
        }

        protected int GetIDEASOrderCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetIDEASOrderCount != null)
            {
                OnGetIDEASOrderCount(this, e);
            }
            return e.Data;
        }

        protected int GetTmallCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetTmallCount != null)
            {
                OnGetTmallCount(this, e);
            }
            return e.Data;
        }

        protected void IDEASOrderGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ViewCompanyUserOrder data = (ViewCompanyUserOrder)e.Row.DataItem;
                HyperLink hkOrderId = (HyperLink)e.Row.FindControl("hkOrderId");
                HyperLink hkSellerName = (HyperLink)e.Row.FindControl("hkSellerName");
                Literal hkSellerMemo = (Literal)e.Row.FindControl("hkSellerMemo");
                Label lblBusinessTime = (Label)e.Row.FindControl("lblBusinessTime");
                HyperLink hkPaymentStatus = (HyperLink)e.Row.FindControl("hkPaymentStatus");
                Label lblOrderStatus = (Label)e.Row.FindControl("lblOrderStatus");

                hkOrderId.Text = data.OrderId;
                hkOrderId.NavigateUrl = ResolveUrl("~/controlroom/Order/order_detail.aspx?oid=") + data.OrderGuid;

                hkSellerName.Text = data.SellerName;
                hkSellerName.NavigateUrl = ResolveUrl("~/controlroom/Seller/seller_add.aspx?sid=") + data.SellerGuid + "&Tc=" + (int)SellerTabPage.PponSetup;

                hkSellerMemo.Text = GetMemo(data);

                string startTime = data.BusinessHourOrderTimeS == null
                                       ? string.Empty
                                       : data.BusinessHourOrderTimeS.Value.ToShortDateString();
                string endTime = data.BusinessHourOrderTimeE == null
                                       ? string.Empty
                                       : data.BusinessHourOrderTimeE.Value.ToShortDateString();
                lblBusinessTime.Text = startTime + "<br />|<br />" + endTime;

                OrderStatus orderstatus = data.OrderStatus == null ? 0 : (OrderStatus)data.OrderStatus;
                if (Helper.IsFlagSet(orderstatus, OrderStatus.Complete))
                {
                    hkPaymentStatus.Text = "已付款";
                }
                else
                {
                    if (!string.IsNullOrEmpty(data.TransId))
                    {
                        hkPaymentStatus.Text = "付款失敗";
                        hkPaymentStatus.NavigateUrl = ResolveUrl("~/controlroom/Ppon/QueryCreditcardTransaction.aspx?tid=") + data.TransId;
                    }
                    else
                    {
                        hkPaymentStatus.Text = "未付款";
                    }
                }

                //抓取訂單退貨狀態
                IList<ReturnFormEntity> forms = ReturnFormRepository.FindAllByOrder(data.OrderGuid);
                ReturnFormEntity form = forms.Where(x => x.ProgressStatus.EqualsAny(ProgressStatus.Processing, ProgressStatus.AtmQueueing, ProgressStatus.AtmQueueSucceeded))
                    .OrderByDescending(x => x.VendorProcessTime).FirstOrDefault();
                if (form == null)
                {
                    form = forms.OrderByDescending(x => x.VendorProcessTime).FirstOrDefault();
                }
                lblOrderStatus.Text = OrderFacade.GetRefundType(form) + " " + OrderFacade.GetRefundStatus(form);

                //抓取訂單換貨狀態
                if (data.LogId != null)
                {
                    int logStatus = data.LogStatus == null ? 0 : data.LogStatus.Value;
                    if (!string.IsNullOrEmpty(lblOrderStatus.Text))
                    {
                        lblOrderStatus.Text += "<br/>";
                    }
                    lblOrderStatus.Text += GetExchangeStatus((OrderReturnStatus)logStatus);
                }
            }
        }

        protected void gvOrderList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                HyperLink hkOrderId = (HyperLink)e.Row.FindControl("hkOrderId");
                HyperLink goItemName = (HyperLink)e.Row.FindControl("goItemName");
                Literal hkSellerMemo = (Literal)e.Row.FindControl("hkSellerMemo");
                Label lblBusinessTime = (Label)e.Row.FindControl("lblBusinessTime");
                HyperLink hkPaymentStatus = (HyperLink)e.Row.FindControl("hkPaymentStatus");
                Label lblRefundStatus = (Label)e.Row.FindControl("lblRefundStatus");
                Label lblExchangeStatus = (Label)e.Row.FindControl("lblExchangeStatus");

                hkOrderId.Text = rowView["order_id"].ToString();
                hkOrderId.NavigateUrl = ResolveUrl("~/controlroom/Order/order_detail.aspx?oid=") + rowView["GUID"].ToString();

                goItemName.Text = rowView["item_name"].ToString();
                goItemName.NavigateUrl = ResolveUrl("~/controlroom/ppon/setup.aspx?bid=") + rowView["business_hour_guid"].ToString();

                hkSellerMemo.Text = GetMemo(rowView);

                DateTime time = DateTime.Now;
                string startTime = DateTime.TryParse(rowView["business_hour_order_time_s"].ToString(), out time) ? Convert.ToDateTime(rowView["business_hour_order_time_s"].ToString()).ToShortDateString() : string.Empty;
                string endTime = DateTime.TryParse(rowView["business_hour_order_time_e"].ToString(), out time) ? Convert.ToDateTime(rowView["business_hour_order_time_e"].ToString()).ToShortDateString() : string.Empty;
                lblBusinessTime.Text = startTime + "<br />|<br />" + endTime;

                int status;
                OrderStatus orderstatus = (OrderStatus)(int.TryParse(rowView["order_status"].ToString(), out status) ? Convert.ToInt32(rowView["order_status"].ToString()) : status);
                if (Helper.IsFlagSet(orderstatus, OrderStatus.Complete))
                {
                    hkPaymentStatus.Text = "已付款";
                }
                else
                {
                    if (!string.IsNullOrEmpty(rowView["trans_id"].ToString()))
                    {
                        hkPaymentStatus.Text = "付款失敗";
                        hkPaymentStatus.NavigateUrl = ResolveUrl("~/controlroom/Ppon/QueryCreditcardTransaction.aspx?tid=") + rowView["trans_id"].ToString();
                    }
                    else
                    {
                        hkPaymentStatus.Text = "未付款";
                    }
                }

                //抓取訂單退貨狀態
                //判斷憑證或宅配
                IList<ReturnFormEntity> forms = string.IsNullOrEmpty(rowView["trust_id"].ToString()) ? ReturnFormRepository.FindAllByOrder((Guid)rowView["GUID"]) : ReturnFormRepository.FindAllByTrustId((Guid)rowView["trust_id"]);
                ReturnFormEntity form = forms.Where(x => x.ProgressStatus.EqualsAny(ProgressStatus.Processing, ProgressStatus.AtmQueueing, ProgressStatus.AtmQueueSucceeded))
                    .OrderByDescending(x => x.VendorProcessTime).FirstOrDefault();
                if (form == null)
                {
                    form = forms.OrderByDescending(x => x.VendorProcessTime).FirstOrDefault();
                }
                lblRefundStatus.Text = OrderFacade.GetRefundType(form) + " " + OrderFacade.GetRefundStatus(form);

                //抓取訂單換貨狀態
                if (!string.IsNullOrEmpty(rowView["log_id"].ToString()))
                {
                    int tmp;
                    int logStatus = int.TryParse(rowView["log_status"].ToString(), out tmp) ? Convert.ToInt32(rowView["log_status"].ToString()) : tmp;

                    lblExchangeStatus.Text = GetExchangeStatus((OrderReturnStatus)logStatus);
                }
            }
        }

        private string GetMemo(DataRowView rowView)
        {
            DateTime bhDeliverTimeEnd;

            if (!DateTime.TryParse(rowView["business_hour_deliver_time_e"].ToString(), out bhDeliverTimeEnd))
            {
                return string.Empty;
            }

            DateTime? storeCloseDownDate = null;
            DateTime temp;
            if (DateTime.TryParse(rowView["store_close_down_date"].ToString(), out temp))
            {
                storeCloseDownDate = temp;
            }
            DateTime? sellerCloseDownDate = null;
            if (DateTime.TryParse(rowView["seller_close_down_date"].ToString(), out temp))
            {
                sellerCloseDownDate = temp;
            }
            DateTime? storeChangedExpireDate = null;
            if (DateTime.TryParse(rowView["store_changed_expire_date"].ToString(), out temp))
            {
                storeChangedExpireDate = temp;
            }
            DateTime? bhChangedExpireDate = null;
            if (DateTime.TryParse(rowView["bh_changed_expire_date"].ToString(), out temp))
            {
                bhChangedExpireDate = temp;
            }


            return GetMemo(bhDeliverTimeEnd, storeCloseDownDate, rowView["store_name"].ToString(),
               sellerCloseDownDate, storeChangedExpireDate, bhChangedExpireDate);
        }

        public string GetMemo(ViewCompanyUserOrder order)
        {
            if (order.BusinessHourDeliverTimeE == null)
            {
                return string.Empty;
            }
            return GetMemo(order.BusinessHourDeliverTimeE.Value, order.StoreCloseDownDate, order.StoreName,
                           order.SellerCloseDownDate, order.StoreChangedExpireDate
                           , order.BhChangedExpireDate);
        }

        public string GetMemo(DateTime businessHourDeliverTimeE, DateTime? storeCloseDownDate, string storeName, DateTime? sellerColseDownDate
            , DateTime? storeChangedExpireDate, DateTime? dealChangedExpireDate
            )
        {

            if (storeCloseDownDate != null)
            {
                if (storeCloseDownDate < businessHourDeliverTimeE)
                {
                    return string.Format("({0}結束營業{1})", storeName, storeCloseDownDate.Value.ToString("yyyy/MM/dd"));
                }
            }
            if (sellerColseDownDate != null)
            {
                if (sellerColseDownDate < businessHourDeliverTimeE)
                {
                    return string.Format("(結束營業{0})", sellerColseDownDate.Value.ToString("yyyy/MM/dd"));
                }
            }
            if (storeChangedExpireDate != null)
            {
                if (storeChangedExpireDate < businessHourDeliverTimeE)
                {
                    return string.Format("({0}停止使用{1})", storeName, storeChangedExpireDate.Value.ToString("yyyy/MM/dd"));
                }
            }
            if (dealChangedExpireDate != null)
            {
                if (dealChangedExpireDate < businessHourDeliverTimeE)
                {
                    return string.Format("(停止使用{0})", dealChangedExpireDate.Value.ToString("yyyy/MM/dd"));
                }
            }

            return string.Empty;
        }


        protected void gvHidealOrderList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ViewHiDealOrderDealOrderReturned RowView = (ViewHiDealOrderDealOrderReturned)e.Row.DataItem;
                HyperLink hkOrderId = (HyperLink)e.Row.FindControl("hkOrderId");
                Label hkOrderName = (Label)e.Row.FindControl("hkOrderName");
                HyperLink hkSellerName = (HyperLink)e.Row.FindControl("hkSellerName");
                HyperLink hkProductName = (HyperLink)e.Row.FindControl("hkProductName");
                Label lblAmount = (Label)e.Row.FindControl("lblAmount");
                Label lblOrderTime = (Label)e.Row.FindControl("lblOrderTime");
                Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                Label lblHidealOrderReturnStatus = (Label)e.Row.FindControl("lblHidealOrderReturnStatus");

                hkOrderId.Text = RowView.OrderId;
                hkOrderId.NavigateUrl = ResolveUrl("~/controlroom/piinlife/hidealorderdetaillist.aspx?oid=") + RowView.OrderGuid;
                hkOrderName.Text = RowView.OrderName;
                hkSellerName.Text = RowView.SellerName;
                hkSellerName.NavigateUrl = ResolveUrl("~/controlroom/Seller/seller_add.aspx?sid=") + RowView.SellerGuid + "&Tc=" + (int)SellerTabPage.PiinLifeSetUp;
                hkProductName.Text = RowView.ProductName;
                lblAmount.Text = RowView.Amount.ToString("N0");
                lblOrderTime.Text = RowView.DealStartTime.Value.ToShortDateString() + "<br />|<br />" + RowView.DealEndTime.Value.ToShortDateString();

                if (RowView.OrderStatus == (int)HiDealOrderStatus.Completed)
                {
                    lblStatus.Text = "已付款";
                    if (RowView.ReturnId != null)
                    {
                        if (RowView.ReturnStatus == (int)HiDealReturnedStatus.Create)
                        {
                            lblHidealOrderReturnStatus.Text = "成立退貨單";
                            e.Row.BackColor = System.Drawing.Color.FromName("#FFCCFF");
                        }
                        else if (RowView.ReturnStatus == (int)HiDealReturnedStatus.Completed)
                        {
                            lblHidealOrderReturnStatus.Text = "退貨完成";
                            e.Row.BackColor = System.Drawing.Color.FromName("#FFCCFF");
                        }
                    }
                }
                else
                {
                    lblStatus.Text = "此訂單狀況異常";
                }
            }
        }

        protected void gvLunchKingList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView rowView = (DataRowView)e.Row.DataItem;
                HyperLink hkOrderId = (HyperLink)e.Row.FindControl("hkOrderId");
                HyperLink hkSellerName = (HyperLink)e.Row.FindControl("hkSellerName");
                hkOrderId.Text = rowView["order_id"].ToString();
                hkSellerName.Text = rowView["seller_name"].ToString();
                hkSellerName.NavigateUrl = ResolveUrl("~/controlroom/Seller/seller_add.aspx?sid=") + rowView["seller_GUID"].ToString() + " &Tc=" + (int)SellerTabPage.FoodIngredientsList;
            }
        }

        protected void gvBonusConvert_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                KeyValuePair<ViewMemberPromotionTransaction, string> dateItem = (KeyValuePair<ViewMemberPromotionTransaction, string>)e.Row.DataItem;

                Label lblCreateTime = (Label)e.Row.FindControl("lblCreateTime");
                Label lblAction = (Label)e.Row.FindControl("lblAction");
                Label lblPromotionValue = (Label)e.Row.FindControl("lblPromotionValue");
                Label lblRunSum = (Label)e.Row.FindControl("lblRunSum");
                Label lblExpireTime = (Label)e.Row.FindControl("lblExpireTime");
                HyperLink hkCreateId = (HyperLink)e.Row.FindControl("hkCreateId");

                lblCreateTime.Text = dateItem.Key.CreateTime.ToString("yyyy/MM/dd HH:mm");
                lblAction.Text = dateItem.Key.Action;
                lblPromotionValue.Text = (dateItem.Key.PromotionValue ?? 0).ToString();
                lblRunSum.Text = dateItem.Key.RunSum.ToString();
                lblExpireTime.Text = dateItem.Key.ExpireTime.ToString("yyyy/MM/dd HH:mm");
                if (dateItem.Value != null)
                {
                    hkCreateId.Text = dateItem.Value;
                    hkCreateId.NavigateUrl = ResolveUrl("~/ControlRoom/user/users_edit.aspx?username=") + System.Web.HttpUtility.UrlEncode(dateItem.Value);
                }
            }
        }

        protected void gvSuperBonusConvert_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                KeyValuePair<ViewMemberPromotionTransaction, string> dateItem = (KeyValuePair<ViewMemberPromotionTransaction, string>)e.Row.DataItem;

                Label lblCreateTime = (Label)e.Row.FindControl("lblCreateTime");
                Label lblAction = (Label)e.Row.FindControl("lblAction");
                HyperLink hpAction = (HyperLink)e.Row.FindControl("hyperAction");
                Label lblDepositPromotionValue = (Label)e.Row.FindControl("lblDepositPromotionValue");
                Label lblWithDrawalPromotionValue = (Label)e.Row.FindControl("lblWithDrawalPromotionValue");
                Label lblRunSum = (Label)e.Row.FindControl("lblRunSum");
                Label lblExpireTime = (Label)e.Row.FindControl("lblExpireTime");
                HyperLink hkCreateId = (HyperLink)e.Row.FindControl("hkCreateId");

                lblCreateTime.Text = dateItem.Key.CreateTime.ToString("yyyy/MM/dd HH:mm");
                lblAction.Text = hpAction.Text = dateItem.Key.Action;

                double promotionValue = dateItem.Key.PromotionValue ?? 0;
                if (promotionValue >= 0)
                {
                    hpAction.Visible = false;
                    lblAction.Visible = true;
                    lblDepositPromotionValue.Text = promotionValue.ToString();
                    lblWithDrawalPromotionValue.Text = string.Empty;
                }
                else
                {
                    hpAction.Visible = true;
                    lblAction.Visible = false;
                    hpAction.NavigateUrl = ResolveUrl("~/controlroom/Order/order_detail.aspx?oid=") + dateItem.Key.WithdrawalOrderGuid;
                    lblDepositPromotionValue.Text = string.Empty;
                    lblWithDrawalPromotionValue.Text = (dateItem.Key.PromotionValue ?? 0).ToString();
                }

                lblRunSum.Text = dateItem.Key.RunSum.ToString();
                lblExpireTime.Text = dateItem.Key.ExpireTime.ToString("yyyy/MM/dd HH:mm");
                if (dateItem.Value != null)
                {
                    hkCreateId.Text = dateItem.Value;
                    hkCreateId.NavigateUrl = ResolveUrl("~/ControlRoom/user/users_edit.aspx?username=") + System.Web.HttpUtility.UrlEncode(dateItem.Value);
                }
            }
        }

        protected void gvSuperDiscountTicketConvert_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ViewDiscountOrder dataItem = (ViewDiscountOrder)e.Row.DataItem;

                Label lblUseTime = (Label)e.Row.FindControl("lblUseTime");
                Label lblCreateTime = (Label)e.Row.FindControl("lblCreateTime");
                Label lblExpireTime = (Label)e.Row.FindControl("lblExpireTime");
                Label lblSuperDiscountCode = (Label)e.Row.FindControl("lblSuperDiscountCode");
                Label lblSuperDiscountStatus = (Label)e.Row.FindControl("lblSuperDiscountStatus");
                Label lblAction = (Label)e.Row.FindControl("lblAction");
                HyperLink hpAction = (HyperLink)e.Row.FindControl("hyperAction");
                HyperLink hkCreateId = (HyperLink)e.Row.FindControl("hkCreateId");

                lblUseTime.Text = dataItem.UseTime.HasValue ? dataItem.UseTime.Value.ToString("yyyy/MM/dd HH:mm") : string.Empty;
                lblCreateTime.Text = dataItem.CreateTime.HasValue ? dataItem.CreateTime.Value.ToString("yyyy/MM/dd HH:mm") : string.Empty;
                lblExpireTime.Text = dataItem.EndTime.HasValue ? dataItem.EndTime.Value.ToString("yyyy/MM/dd HH:mm") : string.Empty;

                lblSuperDiscountCode.Text = dataItem.Code;
                if (dataItem.UseTime.HasValue)
                {
                    lblSuperDiscountStatus.Text = "已使用";
                    hpAction.Visible = true;
                    lblAction.Visible = false;
                    hpAction.NavigateUrl = GetOrderLink(dataItem.OrderGuid.ToString(), dataItem.OrderClassification);
                    hpAction.Text = GetDiscountSummary(dataItem.OrderGuid.ToString(), dataItem.OrderClassification);
                }
                else if (DateTime.Now > dataItem.EndTime)
                {
                    lblSuperDiscountStatus.Text = "過期";
                    hpAction.Visible = false;
                    lblAction.Visible = true;
                }
                else
                {
                    lblSuperDiscountStatus.Text = "未使用";
                    hpAction.Visible = false;
                    lblAction.Visible = true;
                }

                if (!string.IsNullOrEmpty(dataItem.OwnerName))
                {
                    hkCreateId.Text = dataItem.OwnerName;
                    hkCreateId.NavigateUrl = ResolveUrl("~/ControlRoom/user/users_edit.aspx?username=") + System.Web.HttpUtility.UrlEncode(dataItem.OwnerName);
                }
            }
        }

        protected void gvMembershipCardConvert_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ViewMembershipCardLog dataItem = (ViewMembershipCardLog)e.Row.DataItem;

                Label lblCreateTime = (Label)e.Row.FindControl("lblCreateTime");
                Label lblUserMembershipCardName = (Label)e.Row.FindControl("lblUserMembershipCardName");
                Label lblCardType = (Label)e.Row.FindControl("lblCardType");
                HyperLink hkUserMembershipCardId = (HyperLink)e.Row.FindControl("hkUserMembershipCardId");
                Label lblMemo = (Label)e.Row.FindControl("lblMemo");
                Label lblExpireTime = (Label)e.Row.FindControl("lblExpireTime");
                HyperLink hkDiscountCode = (HyperLink)e.Row.FindControl("hkDiscountCode");

                lblCreateTime.Text = dataItem.CreateTime.ToString("yyyy/MM/dd HH:mm");
                lblUserMembershipCardName.Text = dataItem.SellerName;

                switch (dataItem.Level)
                {
                    case 1:
                        lblCardType.Text = "普卡";
                        break;
                    case 2:
                        lblCardType.Text = "金卡";
                        break;
                    case 3:
                        lblCardType.Text = "白金卡";
                        break;
                    case 4:
                        lblCardType.Text = "VIP卡";
                        break;

                    default:
                        lblCardType.Text = "";
                        break;
                }

                hkUserMembershipCardId.Text = dataItem.UserMembershipCardId.ToString();
                hkUserMembershipCardId.NavigateUrl = ResolveUrl("~/ControlRoom/user/users_edit.aspx?username=") + System.Web.HttpUtility.UrlEncode(UserData);

                int memoLimit = 20;
                lblMemo.Text = (dataItem.Memo.Length > memoLimit ? dataItem.Memo.Substring(0, memoLimit) + "..." : dataItem.Memo);
                lblExpireTime.Text = dataItem.EndTime.HasValue ? dataItem.EndTime.Value.ToString("yyyy/MM/dd HH:mm") : string.Empty;

                if (!string.IsNullOrEmpty(dataItem.Code) && dataItem.DiscountType.HasValue)
                {
                    int discounttype = dataItem.DiscountType.Value;
                    switch (discounttype)
                    {
                        case 1:
                            hkDiscountCode.Text = "17Life折價券: ";
                            break;
                        case 2:
                            hkDiscountCode.Text = "熟客券: ";
                            break;
                        case 3:
                            hkDiscountCode.Text = "公關券: ";
                            break;
                        case 4:
                            hkDiscountCode.Text = "超級折價券: ";
                            break;

                        default:
                            hkDiscountCode.Text = "";
                            break;
                    }

                    hkDiscountCode.Text += dataItem.Code;
                    hkDiscountCode.NavigateUrl = "#";//待熟客券一覽頁面
                }
            }
        }

        protected void gvServiceRecord_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                KeyValuePair<ServiceMessage, Guid> dataItem = (KeyValuePair<ServiceMessage, Guid>)e.Row.DataItem;

                Label lblCategory = (Label)e.Row.FindControl("lblCategory");
                HyperLink hkStatus = (HyperLink)e.Row.FindControl("hkStatus");
                Label lblMessageType = (Label)e.Row.FindControl("lblMessageType");
                Label lblMessage = (Label)e.Row.FindControl("lblMessage");
                HyperLink hkOrderId = (HyperLink)e.Row.FindControl("hkOrderId");
                Label lblCreateTime = (Label)e.Row.FindControl("lblCreateTime");
                Label lblModifyId = (Label)e.Row.FindControl("lblModifyId");
                Label lblModifyTime = (Label)e.Row.FindControl("lblModifyTime");
                Label lblType = (Label)e.Row.FindControl("lblType");
                lblCategory.Text = mp.ServiceMessageCategoryGetByCategoryId(Convert.ToInt32(dataItem.Key.Category)).CategoryName;
                switch (dataItem.Key.Status)
                {
                    case "wait":
                        hkStatus.Text = "待處理";
                        break;
                    case "process":
                        hkStatus.Text = "處理中";
                        break;
                    case "complete":
                        hkStatus.Text = "完成";
                        break;
                }
                hkStatus.NavigateUrl = ResolveUrl("~/controlroom/servicedetail.aspx?id=") + dataItem.Key.Id;
                lblMessageType.Text = dataItem.Key.MessageType;
                int limit = 100000;
                string message = System.Web.HttpUtility.HtmlDecode(dataItem.Key.Message.Replace("\n", "<br>"));
                lblMessage.Text = (message.Length > limit ? message.Substring(0, limit) + "..." : message);
                hkOrderId.Text = dataItem.Key.OrderId;
                switch ((ServiceMessageType)dataItem.Key.Type)
                {
                    case ServiceMessageType.Ppon:
                        hkOrderId.NavigateUrl = ResolveUrl("~/controlroom/Order/order_detail.aspx?oid=") + dataItem.Value.ToString();
                        break;
                    case ServiceMessageType.Hideal:
                        hkOrderId.NavigateUrl = ResolveUrl("~/controlroom/piinlife/hidealorderdetaillist.aspx?oid=") + dataItem.Value.ToString();
                        break;
                    default:
                        break;
                }
                lblCreateTime.Text = dataItem.Key.CreateTime.Value.ToString("yyyy/MM/dd HH:mm");

                string user = dataItem.Key.WorkUser;
                if (!string.IsNullOrEmpty(user))
                {
                    lblModifyId.Text = user.Substring(0, user.IndexOf('@'));
                }

                lblModifyTime.Text = dataItem.Key.ModifyTime != null ? dataItem.Key.ModifyTime.Value.ToString("yyyy/MM/dd HH:mm") : string.Empty;
                lblType.Text = ((ServiceMessageType)dataItem.Key.Type).ToString();
            }
        }

 
        protected void gvSubscription_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Subscription subs = (Subscription)e.Row.DataItem;
                Label lblCityName = (Label)e.Row.FindControl("lblCityName");
                CheckBox chkCheck = (CheckBox)e.Row.FindControl("chkCheck");
                HiddenField hidId = (HiddenField)e.Row.FindControl("hidId");
                HiddenField hidStatus = (HiddenField)e.Row.FindControl("hidStatus");
                Label lblUnsubscriptionTime = (Label)e.Row.FindControl("lblUnsubscriptionTime");
                Label lblResubscriptionTime = (Label)e.Row.FindControl("lblResubscriptionTime");

                chkCheck.Checked = Helper.IsFlagSet((SubscribeType)subs.Status, SubscribeType.Unsubscribe);
                hidId.Value = subs.Id.ToString();
                hidStatus.Value = subs.Status.ToString();
                lblUnsubscriptionTime.Text = subs.UnsubscriptionTime != null ? subs.UnsubscriptionTime.Value.ToString("yyyy/MM/dd HH:mm") : string.Empty;
                lblResubscriptionTime.Text = subs.ResubscriptionTime != null ? subs.ResubscriptionTime.Value.ToString("yyyy/MM/dd HH:mm") : string.Empty;

                CategoryNode category = CategoryManager.Default.Find(subs.CategoryId);
                if (category != null)
                {
                    lblCityName.Text = category.CategoryName;
                }
            }
        }

        protected void gvHidealSubcription_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiDealSubscription subs = (HiDealSubscription)e.Row.DataItem;
                Label lblCityName = (Label)e.Row.FindControl("lblCityName");
                Label lblRegion = (Label)e.Row.FindControl("lblRegion");
                CheckBox chkCheck = (CheckBox)e.Row.FindControl("chkCheck");
                HiddenField hidCityId = (HiddenField)e.Row.FindControl("hidCityId");
                HiddenField hidRegion = (HiddenField)e.Row.FindControl("hidRegion");
                Label lblUnsubscriptionTime = (Label)e.Row.FindControl("lblUnsubscriptionTime");
                Label lblFlag = (Label)e.Row.FindControl("lblFlag");

                if (subs.City != null)
                {
                    lblCityName.Text = CityManager.CityGetById(subs.City.Value).CityName;
                    hidCityId.Value = subs.City.ToString();
                }
                if (subs.RegionCodeId != null)
                {
                    SystemCode code = HiDealRegionManager.GetRegionOfSet().Where(x => x.CodeId.Equals(subs.RegionCodeId.Value)).FirstOrDefault();
                    if (code != null)
                        lblRegion.Text = code.CodeName;
                    hidRegion.Value = subs.RegionCodeId.ToString();
                    chkCheck.Enabled = subs.RegionCodeId.Value.Equals((int)HiDealRegionManager.DealOverviewRegion);
                }
                chkCheck.Checked = Helper.IsFlagSet((PiinlifeSubscriptionFlag)subs.Flag, PiinlifeSubscriptionFlag.Cancel);
                lblUnsubscriptionTime.Text = subs.CancelTime != null ? subs.CancelTime.Value.ToString("yyyy/MM/dd HH:mm") : string.Empty;
                switch ((PiinlifeSubscriptionFlag)subs.Flag)
                {
                    case PiinlifeSubscriptionFlag.General:
                        lblFlag.Text = "訂閱";
                        break;
                    case PiinlifeSubscriptionFlag.Cancel:
                        lblFlag.Text = "取消訂閱";
                        break;
                    case PiinlifeSubscriptionFlag.Resubscribe:
                        lblFlag.Text = "重新訂閱";
                        break;
                    default:
                        break;
                }
            }
        }

        protected string GetOrderGuid(string cashpointId)
        {
            string oid = cashpointId;
            string msg = odrProv.PaymentTransactionGet(new Guid(cashpointId)).Message;
            if (!string.IsNullOrEmpty(msg) && msg.Split('|').Length > 1)
                oid = msg.Split('|')[1];
            return oid;
        }

        protected string GetOrderSummary(string cashpointId)
        {
            string summary = string.Empty;
            LunchKingSite.DataOrm.Order o = null;
            string soid = GetOrderGuid(cashpointId);

            if (!string.IsNullOrEmpty(soid))
            {
                o = odrProv.OrderGet(new Guid(soid));
                summary += (o != null) ? o.OrderId : string.Empty;
            }
            return summary;
        }

        protected string GetBusinessHourIdByPid(string pid)
        {
            ViewPponDeal deal = pponProv.ViewPponDealGet(ViewPponDeal.Columns.OrderGuid + "=" + new Guid(pid));
            return (deal != null) ? deal.BusinessHourGuid.ToString() : string.Empty;
        }

        protected string GetBusinessHourIdSummary(string cashpointId)
        {
            string summary = string.Empty;
            LunchKingSite.DataOrm.Order o = null;
            string soid = GetOrderGuid(cashpointId);

            if (!string.IsNullOrEmpty(soid))
            {
                Guid oid;
                if (Guid.TryParse(soid, out oid))
                {
                    o = odrProv.OrderGet(oid);
                    ViewPponDeal d = (o.IsNew) ? null : pponProv.ViewPponDealGet(o.ParentOrderId.Value);
                    summary += (d != null) ? d.BusinessHourGuid.ToString() : string.Empty;
                }
            }
            return summary;
        }

        protected string GetBusinessHourGuid(string order_guid)
        {
            Guid oid;
            if (Guid.TryParse(order_guid, out oid))
            {
                if (BidLookupByOid.Keys.Contains(oid))
                {
                    return BidLookupByOid[oid].ToString();
                }

                LunchKingSite.DataOrm.Order o = odrProv.OrderGet(oid);
                if (o != null && o.ParentOrderId.HasValue)
                {
                    ViewPponDeal d = pponProv.ViewPponDealGet(o.ParentOrderId.Value);
                    if (d != null)
                    {
                        BidLookupByOid.Add(oid, d.BusinessHourGuid);
                    }
                    return d.BusinessHourGuid.ToString();
                }
            }

            return string.Empty;
        }

        protected string GetSellerSummary(string cashpointId)
        {
            string summary = string.Empty;
            LunchKingSite.DataOrm.Order o = null;
            string soid = GetOrderGuid(cashpointId);

            if (!string.IsNullOrEmpty(soid))
            {
                o = odrProv.OrderGet(new Guid(soid));
                summary += (o != null) ? o.SellerName : string.Empty;
            }
            return summary;
        }

        protected string GetScashSummary(string id, string cashpointId, string type)
        {
            string summary = string.Empty;
            LunchKingSite.DataOrm.Order o = null;
            string soid = GetOrderGuid(cashpointId);

            switch ((CashPointListType)int.Parse(type))
            {
                case CashPointListType.Income:
                    if (odrProv.PaymentTransactionGet(new Guid(cashpointId)).IsNew)
                        summary = "補購物金";
                    else if (string.IsNullOrEmpty(soid))
                        summary = "刷卡儲值";
                    else
                        summary = "刷卡訂購轉購物金：";
                    break;
                case CashPointListType.Expense:
                    summary = "支付：";
                    break;
                case CashPointListType.Reject:
                    summary = "取消訂單：";
                    break;
                default:
                    break;
            }

            if (!string.IsNullOrEmpty(soid))
            {
                o = odrProv.OrderGet(new Guid(soid));
                ViewPponDeal d = (o.IsNew) ? null : pponProv.ViewPponDealGet(o.ParentOrderId.Value);
                summary += (d != null) ? d.ItemName : string.Empty;
            }

            return summary;
        }



        protected string GetScashIncomeAmount(string amount)
        {
            decimal a = decimal.Parse(amount);
            return (a >= 0) ? a.ToString("f0") : string.Empty;
        }

        protected string GetScashExpendAmount(string amount)
        {
            decimal a = decimal.Parse(amount);
            return (a < 0) ? a.ToString("f0") : string.Empty;
        }

        protected string GetScashBalanceAmount(string user, string id)
        {
            return odrProv.CashPointListSum(user, int.Parse(id)).ToString("f0");
        }

        protected string GetSummary(string pid, string paymentType, string transType)
        {
            string summary = string.Empty;

            switch ((PayTransType)int.Parse(transType))
            {
                case PayTransType.Authorization:
                    if ((PaymentType)int.Parse(paymentType) == PaymentType.Creditcard)
                        summary = Resources.Localization.PayTransAuthorization + "：";
                    else
                        summary = Resources.Localization.PayTransCharging + "：";
                    break;
                case PayTransType.Charging:
                    summary = Resources.Localization.PayTransCharging + "：";
                    break;
                case PayTransType.Refund:
                    summary = Resources.Localization.PayTransRefund + "：";
                    break;
                default:
                    break;
            }
            ViewPponDeal d = pponProv.ViewPponDealGet(new Guid(pid));
            summary += (d != null) ? d.ItemName : string.Empty;

            return summary;
        }

        protected string GetChargingAmount(string transType, string amount)
        {
            decimal a = decimal.Parse(amount);
            return ((PayTransType)int.Parse(transType) != PayTransType.Refund) ? a.ToString("f0") : string.Empty;
        }

        protected string GetRefundAmount(string transType, string amount)
        {
            decimal a = decimal.Parse(amount);
            return ((PayTransType)int.Parse(transType) == PayTransType.Refund) ? a.ToString("f0") : string.Empty;
        }

        protected string GetOrderLink(string oid, object classification)
        {
            OrderClassification orderClassification = OrderClassification.LkSite;
            if ((int?)classification == 2)
            {
                orderClassification = OrderClassification.HiDeal;
            }
            if (orderClassification == OrderClassification.LkSite)
            {
                //~controlroom/piinlife/hidealorderdetaillist.aspx?oid=cd231abf-8a12-41ae-bb11-11ba68f53587
                return ResolveUrl("~/ControlRoom/Order/order_detail.aspx?oid=") + oid;
            }
            else
            {
                return ResolveUrl("~/controlroom/piinlife/hidealorderdetaillist.aspx?oid=") + oid;
            }
        }

        protected string GetDiscountSummary(string oid, object classification)
        {
            if (oid == "")
            {
                return "";
            }
            OrderClassification orderClassification = OrderClassification.LkSite;
            if ((int?)classification == 2)
            {
                orderClassification = OrderClassification.HiDeal;
            }

            if (orderClassification == OrderClassification.LkSite)
            {
                DataOrm.Order o = odrProv.OrderGet(new Guid(oid));
                ViewPponDeal d = pponProv.ViewPponDealGet(o.ParentOrderId.GetValueOrDefault());
                return d.ItemName;
            }
            else if (orderClassification == OrderClassification.HiDeal)
            {
                HiDealOrderDetail od = hp.HiDealOrderDetailGetListByOrderGuid(new Guid(oid))[0];
                return od.ProductName;
            }
            return "";
        }

        protected ViewPponOrder GetDealOrderInfoByOrderId(string orderId, string userName)
        {
            var order = pponProv.ViewPponOrderGetList(1, 1, string.Empty,
                                                        ViewPponOrder.Columns.OrderId + " = " + orderId,
                                                        ViewPponOrder.Columns.MemberEmail + " = " + userName).FirstOrDefault();
            return order;
        }

        protected ViewPponDeal GetDealInfo(Guid bid)
        {
            return pponProv.ViewPponDealGetByBusinessHourGuid(bid);
        }

        protected bool HasBalanceSheet(Guid bid)
        {
            return ap.ViewBalanceSheetListGetList(BusinessModel.Ppon, bid, null)
                        .Count(x => x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.LumpSumBalanceSheet) > 0;
        }

        #endregion

        #region private method
        private Dictionary<Subscription, bool> GetUnsubscribeList()
        {
            Dictionary<Subscription, bool> list = new Dictionary<Subscription, bool>();
            for (int i = 0; i < gvSubscription.Rows.Count; i++)
            {
                CheckBox objCheckBox = (CheckBox)gvSubscription.Rows[i].FindControl("chkCheck");
                HiddenField hidId = (HiddenField)gvSubscription.Rows[i].FindControl("hidId");
                HiddenField hidStatus = (HiddenField)gvSubscription.Rows[i].FindControl("hidStatus");

                int dd;
                Subscription subs = new Subscription();
                subs.Id = int.TryParse(hidId.Value.ToString(), out dd) ? Convert.ToInt32(hidId.Value.ToString()) : 0;
                subs.Status = int.TryParse(hidStatus.Value.ToString(), out dd) ? Convert.ToInt32(hidStatus.Value.ToString()) : 0;

                int value;
                if (int.TryParse(hidId.Value, out value) && !value.Equals(0))
                    list.Add(subs, objCheckBox.Checked);
            }
            return list;
        }

        private Dictionary<HiDealSubscription, bool> GetHidealUnsubscribeList()
        {
            Dictionary<HiDealSubscription, bool> list = new Dictionary<HiDealSubscription, bool>();
            for (int i = 0; i < gvHidealSubcription.Rows.Count; i++)
            {
                CheckBox objCheckBox = (CheckBox)gvHidealSubcription.Rows[i].FindControl("chkCheck");
                HiddenField hidCityId = (HiddenField)gvHidealSubcription.Rows[i].FindControl("hidCityId");
                HiddenField hidRegion = (HiddenField)gvHidealSubcription.Rows[i].FindControl("hidRegion");

                HiDealSubscription subs = new HiDealSubscription();
                subs.Id = i;
                subs.Email = UserData;
                if (!string.IsNullOrEmpty(hidRegion.Value))
                    subs.RegionCodeId = int.Parse(hidRegion.Value);
                else if (!string.IsNullOrEmpty(hidCityId.Value))
                    subs.City = int.Parse(hidCityId.Value);
                list.Add(subs, !objCheckBox.Checked);
            }
            return list;
        }

        private ServiceMessage GetServiceMessageData()
        {
            ServiceMessage sm = new ServiceMessage();
            sm.Email = lblIEmail.Text;
            sm.Phone = lblIPhone.Text;
            sm.Name = lblIName.Text;
            sm.OrderId = txtIOrder.Text;
            if (!string.IsNullOrEmpty(rdlWorkType.SelectedValue))
            {
                int workType;
                int.TryParse(rdlWorkType.SelectedValue, out workType);
                sm.WorkType = workType;
            }
            sm.Message = txtIMessage.Text;
            sm.MessageType = ddlItype.SelectedValue;
            //原本 Type=0 是好康, 1是品生活; 整併完以後要設什麼? 可能得討論..
            sm.Type = 0;

            int temp;
            sm.Category = int.TryParse(ddlCategory.SelectedValue, out temp) ? temp : 0;
            sm.SubCategory = int.TryParse(hdSubCategory.Text, out temp) ? temp : 0;

            sm.Status = ddlIStatus.SelectedValue;
            sm.WorkUser = Page.User.Identity.Name;
            if (!string.IsNullOrEmpty(rblPriority.SelectedValue))
            {
                sm.Priority = Convert.ToInt32(rblPriority.SelectedValue);
            }
            sm.RemarkSeller = txtRemarkSeller.Text;
            sm.CreateId = Page.User.Identity.Name;
            sm.CreateTime = DateTime.Now;
            sm.ModifyId = Page.User.Identity.Name;
            sm.ModifyTime = DateTime.Now;

            return sm;
        }

        private MemberPromotionDeposit GetMemberPromotionDepositData()
        {
            MemberPromotionDeposit mpd = new MemberPromotionDeposit();
            mpd.StartTime = DateTime.ParseExact(txtBonusStartTime.Text, "yyyy/MM/dd", null);
            mpd.ExpireTime = DateTime.ParseExact(txtBonusExpireTime.Text, "yyyy/MM/dd", null);
            double value;
            mpd.PromotionValue = double.TryParse(txtPromotionValue.Text, out value) ? value : 0;
            mpd.CreateTime = DateTime.Now;
            mpd.UserId = this.UserId;
            mpd.Action = txtReason.Text;
            mpd.CreateId = User.Identity.Name;

            if (!string.IsNullOrEmpty(txtOrderId.Text))
            {
                var orderInfo = GetDealOrderInfoByOrderId(txtOrderId.Text, UserData);
                if (orderInfo != null)
                {
                    mpd.OrderGuid = orderInfo.Guid;
                }
            }

            return mpd;
        }

        private void AddJavascript()
        {
            btnBcashAdd.Attributes.Add("onclick", "return SetSubmintClickDisable('" + btnBcashAdd.ClientID + "');");
            btnAddServiceMsg.Attributes.Add("onclick", "ClearServiceMessageData('" + txtIOrder.ClientID + "','" + txtIMessage.ClientID + "');");
            rdlUser.Attributes.Add("onclick", "SetUserData('" + txtUserData.ClientID + "');");
        }

        private void buildPriorityRbl(ref RadioButtonList rbl)
        {
            rbl.Items.Clear();
            rbl.Items.Add(new ListItem(ServiceConfig.replyPriorityString.Normal, ((int)ServiceConfig.replyPriority.Normal).ToString()));
            rbl.Items.Add(new ListItem(ServiceConfig.replyPriorityString.High, ((int)ServiceConfig.replyPriority.High).ToString()));
            rbl.Items.Add(new ListItem("<font color='red'>" + ServiceConfig.replyPriorityString.Extreme + "</font>", ((int)ServiceConfig.replyPriority.Extreme).ToString()));
            rbl.SelectedValue = ((int)ServiceConfig.replyPriority.Normal).ToString();
        }

        private string GetExchangeStatus(OrderReturnStatus status)
        {
            switch ((OrderReturnStatus)status)
            {
                case OrderReturnStatus.ExchangeProcessing:
                    return Resources.Localization.ExchangeProcessing;
                case OrderReturnStatus.ExchangeSuccess:
                    return Resources.Localization.ExchangeSuccess;
                case OrderReturnStatus.ExchangeFailure:
                    return Resources.Localization.ExchangeFailure;
                case OrderReturnStatus.ExchangeCancel:
                    return Resources.Localization.ExchangeCancel;
                case OrderReturnStatus.SendToSeller:
                    return Resources.Localization.SendToSeller;
                default:
                    return string.Empty;
            }
        }

        private bool BcashAddCheck()
        {
            if (!string.IsNullOrEmpty(txtOrderId.Text))
            {
                var orderInfo = GetDealOrderInfoByOrderId(txtOrderId.Text, UserData);
                if (orderInfo == null)
                {
                    ScriptManager.RegisterStartupScript(Page, typeof(Button), "alert", "alert('查無此訂單，請重新確認');", true);
                    return false;
                }

                if (!Helper.IsFlagSet(orderInfo.OrderStatus, OrderStatus.Complete))
                {
                    ScriptManager.RegisterStartupScript(Page, typeof(Button), "alert", "alert('此訂單非成立訂單，請重新確認');", true);
                    return false;
                }

                var dealGuid = orderInfo.BusinessHourGuid;
                hidDealGuid.Value = dealGuid.ToString();
                var dealInfo = GetDealInfo(dealGuid);
                if (dealInfo.DeliveryType != (int)DeliveryType.ToHouse)
                {
                    ScriptManager.RegisterStartupScript(Page, typeof(Button), "alert", "alert('只限輸入宅配檔次訂單');", true);
                    return false;
                }

                if (PponOrderManager.CheckIsOverTrialPeriod(orderInfo.Guid))
                {
                    ScriptManager.RegisterStartupScript(Page, typeof(Button), "alert", "alert('此訂單已過鑑賞期，無法新增商家扣款資料');", true);
                    return false;
                }

                double value;
                var promotionValue = double.TryParse(txtPromotionValue.Text, out value) ? value : 0;
                if (promotionValue < 0)
                {
                    ScriptManager.RegisterStartupScript(Page, typeof(Button), "alert", "alert('只限輸入商家扣款資料');", true);
                    return false;
                }

                if (!chkBcashPayByVendor.Checked)
                {
                    ScriptManager.RegisterStartupScript(Page, typeof(Button), "alert", "alert('請勾選由商家扣款給消費者');", true);
                    return false;
                }

                if (HasBalanceSheet(dealGuid))
                {
                    ScriptManager.RegisterStartupScript(Page, typeof(Button), "alert", "alert('檔次對帳單已開立，無法輸入商家扣款資料');", true);
                    return false;
                }
            }
            else
            {
                hidDealGuid.Value = string.Empty;
                if (chkBcashPayByVendor.Checked)
                {
                    ScriptManager.RegisterStartupScript(Page, typeof(Button), "alert", "alert('請輸入訂單編號，方能進行商家扣款作業');", true);
                    return false;
                }
            }

            return true;
        }

        #endregion

        protected string SmsStatusString(int? status)
        {
            if (!status.HasValue)
            {
                return "無紀錄";
            }
            else
            {
                switch (status)
                {
                    case (int)SmsStatus.Success:
                        return Resources.Localization.SmsSuccess;
                    case (int)SmsStatus.Fail:
                        return Resources.Localization.SmsFail;
                    case (int)SmsStatus.Queue:
                        return Resources.Localization.SmsQueue;
                    case (int)SmsStatus.Reset:
                        return Resources.Localization.SmsReset;
                    case (int)SmsStatus.Ppon:
                        return Resources.Localization.SmsPpon;
                    case (int)SmsStatus.Cancel:
                        return Resources.Localization.Cancel;
                    default:
                        return "error";
                }
            }
        }

        public void BuildServiceCategory(ServiceMessageCategoryCollection serviceCates)
        {
            ddlCategory.Items.Clear();
            ddlCategory.Items.Add(new ListItem("請選擇", "0"));
            foreach (var cate in serviceCates)
            {
                ddlCategory.Items.Add(new ListItem(cate.CategoryName, cate.CategoryId.ToString()));
            }
        }

        [WebMethod]
        public static string GetServiceMessageCategory(int parentId)
        {
            var serviceCates = mp.ServiceMessageCategoryCollectionGetListByParentId(parentId).ToList();
            return new JsonSerializer().Serialize(serviceCates);
        }

        protected void cbIsFunds_CheckedChanged(object sender, EventArgs e)
        {
            if (OnSearchClicked != null)
                OnSearchClicked(this, e);
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (OnStatusSelected != null)
                OnStatusSelected(this, e);
            Session[LkSiteSession.Search.ToString()] = ddlStatus.SelectedValue.ToString();
        }

        private bool checkFirstTabClick(TabPageName name)
        {
            int page;
            Dictionary<string, int> tmp = new Dictionary<string, int>();
            if (Session[LkSiteSession.ServiceIntegrate.ToString()] != null)
            {
                tmp = Session[LkSiteSession.ServiceIntegrate.ToString()] as Dictionary<string, int>;
            }


            if (!tmp.TryGetValue(name.ToString(), out page))
            {
                return true;
            }

            if ((string)Session[LkSiteSession.Search.ToString()] != ddlStatus.SelectedValue.ToString())
            {
                return true;
            }
            return false;
        }

        private void AddTabPagerSession(TabPageName tabPageName, int pageNumber)
        {
            int page;
            Dictionary<string, int> tmp = new Dictionary<string, int>();
            if (Session[LkSiteSession.ServiceIntegrate.ToString()] != null)
            {
                tmp = Session[LkSiteSession.ServiceIntegrate.ToString()] as Dictionary<string, int>;
            }

            if (!tmp.TryGetValue(tabPageName.ToString(), out page))
            {
                page = pageNumber;
            }
            tmp[tabPageName.ToString()] = page;
            Session[LkSiteSession.ServiceIntegrate.ToString()] = tmp;
        }

        protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
        {
            var tabContain = (AjaxControlToolkit.TabContainer)sender;
            DataEventArgs<int> d = new DataEventArgs<int>(1);

            //請從網頁那邊開始數過來   （汗）
            switch (tabContain.ActiveTabIndex)
            {
                case 0:
                    if (checkFirstTabClick(TabPageName.OrderPager)) { this.OrderPageChanged(this, d); }
                    break;
                case 1:
                    if (checkFirstTabClick(TabPageName.ServicePager)) { this.ServicePageChanged(this, d); }
                    break;
                case 2:
                    if (checkFirstTabClick(TabPageName.NotClosed)){this.NotClosedPageChanged(this, d);}
                    break;
                case 3:
                    if (checkFirstTabClick(TabPageName.Closed)) { this.ClosedPageChanged(this, d); }
                    break;
                case 4:
                    if (checkFirstTabClick(TabPageName.BonusList))
                    {
                        this.GetBonusListInfo(this, new DataEventArgs<string>("Bonus"));
                        AddTabPagerSession(TabPageName.BonusList, 1);
                    }
                    this.lblTabBonusHint.Text = "";
                    break;
                case 5:
                    //超級紅利金使用紀錄
                    if (checkFirstTabClick(TabPageName.SuperBonusPaper))
                    {
                        this.GetSuperBonusListInfo(this, new DataEventArgs<string>("SuperBonus"));
                        AddTabPagerSession(TabPageName.SuperBonusPaper, 1);
                    }
                    break;
                case 6:
                    //超級折價券使用紀錄
                    this.GetSuperDiscountTicketListInfo(this, new DataEventArgs<string>("SuperDiscountTrickPaper"));
                    AddTabPagerSession(TabPageName.SuperDiscountTicketPaper, 1);
                    //超級折價券點數紀錄
                    this.GetSuperDiscountPointListInfo(this, new DataEventArgs<string>("SuperDiscountPointPaper"));
                    AddTabPagerSession(TabPageName.SuperDiscountPointPaper, 1);
                    break;
                case 7:
                    if (checkFirstTabClick(TabPageName.SubscriptionPaper))
                    {
                        this.GetBonusListInfo(this, new DataEventArgs<string>("Subscription"));
                        AddTabPagerSession(TabPageName.SubscriptionPaper, 1);
                    }
                    break;
                case 8:
                    //會員卡明細
                    this.GetMembershipCardListInfo(this, new DataEventArgs<string>("MembershipCardPaper"));
                    AddTabPagerSession(TabPageName.MembershipCardPaper, 1);

                    break;
                case 9:
                    if (checkFirstTabClick(TabPageName.HidealOrderPager)) { this.HidealOrderPageChanged(this, d); }
                    break;
                case 10:
                    if (checkFirstTabClick(TabPageName.OrderLunchKingPager))
                    {
                        this.GetBonusListInfo(this, new DataEventArgs<string>("LunchKing"));
                        AddTabPagerSession(TabPageName.OrderLunchKingPager, 1);
                    }
                    break;

            }
        }
    }
}

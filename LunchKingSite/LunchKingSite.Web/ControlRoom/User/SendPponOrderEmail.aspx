﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="SendPponOrderEmail.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.User.SendPponOrderEmail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="-1">
<script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../../Tools/js/json2.js"></script>
<script type="text/javascript" src="//www.google.com/jsapi"></script>  
<script type="text/javascript">
    google.load("jquery", "1.4.2");
    google.load("jqueryui", "1.8");
     google.setOnLoadCallback(function () {
        $(document).ready(function () {
           
            CKEDITOR.replace('<%=txtbody.ClientID%>');
        });
    });
</script>
<script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
<link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />	
<style type="text/css">
body { font-size: 10px; }
#tabs { font-size: smaller; }
label { display: block; }

fieldset { margin: 10px 0px; border: solid 1px gray; }
fieldset legend { color: #4F8AFF; font-weight: bolder; }
ul { list-style-type: none; margin: 0 0 0 0; }
li {
    color:#4D4D4D;
    font-weight:bold;
}

ul.status li { float: left; }
ul.target input { width: 80px; }

input { border: solid 1px grey; }
textarea { border: solid 1px grey; }

input.dealName {
    width: 300px;
}
input.date {
    width: 80px;
}

.price { color: red; font-weight: bolder; }

.intro { float:left; margin: 5px 0px 5px 0px; border: none; }
.intro legend { color: #4F8AFF; font-weight: bolder; }
.intro textarea { width: 500px; }

.watermarkOn {
    color: #AAAAAA;
    font-style: italic;
}

#avpane ul
{
    margin-bottom: 3px;
    border: dashed 1px gray;
    padding: 2px 2px 2px 5px;
}

#avpane ul.hover
{
    background-color: #E5ECF9;
}

#avpane li 
{
    font-weight: normal;
    font-size: smaller;
    margin: 0px;
}

#avpane li.eb, #avpane li.db, #avpane li.cb
{
    display: none;
    float: left;
    padding-right: 15px;
    text-decoration: underline;
    color: blue;
    cursor: pointer;
}

.btn_left 
{
    float: left;
}
.btn_mid
{
    float: left;
    margin-left: 100px;
}
.btn_right
{
    float: right;
}
.btn_right a
{
    margin: 0px 10px 0px 10px;
}


</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<fieldset>
    <legend>第一步(選擇要發送的方式)</legend>
    <table>
    <tr>
        <td>送好康全部會員</td>
        <td width="600px">
            <asp:CheckBox ID="cbPpon" runat="server" />
        </td>
    </tr>
    <tr>
        <td>送名單Email</td>
        <td width="600px">
            <asp:CheckBox ID="cbList" runat="server" 
                /><br />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            好康的Bid:<asp:TextBox ID="txtbid" runat="server" Columns="60"></asp:TextBox><br />
            Email清單(範例:jacky@17life.com,anna@17life.com,aaa@17life.com):<br /> <asp:TextBox ID="txtList" runat="server" TextMode="MultiLine" Rows="10" Columns="50" ></asp:TextBox>
        </td>

    </tr>
    </table>
</fieldset>
<fieldset>
    <legend>第二步(檢查發送筆數)</legend>
    <asp:Label ID="lblresult" runat="server" Text="" ForeColor="Red"></asp:Label>
    <asp:Button ID="btn" runat="server" Text="檢核" onclick="btn_Click" />
</fieldset>
<fieldset>
    <legend>第三步(填入信件標題)</legend>
    信件標題<asp:TextBox ID="txttitle" runat="server" Columns="60"></asp:TextBox>
</fieldset>
<fieldset>
    <legend>第四步(填入信件內容)</legend>
    信件內容:<asp:TextBox ID="txtbody" runat="server" TextMode="MultiLine" Rows="10"></asp:TextBox>
</fieldset>
<fieldset>
    <legend>第五步(發送,發送完成後內容看起來會有點亂碼，先不用理他)</legend>
    <asp:Button ID="btnSend" runat="server" Text="發送" onclick="btnSend_Click" />
    <asp:Label ID="lblfeeback" runat="server" Text="" ForeColor="Red"></asp:Label>
</fieldset>

</asp:Content>

<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="MassEmailer.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.User.MassEmailer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../../Tools/js/jquery.js"></script>
    <style type="text/css">
        .style1
        {
            height: 23px;
        }
        .style2
        {
            height: 23px;
            width: 176px;
        }
        .style3
        {
            width: 176px;
        }
        
        .msb
        {
            width: 200px;
        }
        
        .msa
        {
            width: 300px;
        }
        
        .warn
        {
            color: red;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("input[name='filt']").click(function () {
                switch ($(this).val()) {
                    case '1':
                        $('#cf').show();
                        $('#sf').hide();
                        break;
                    case '2':
                        $('#cf').hide();
                        $('#sf').show();
                        break;
                    default:
                        $('#cf').hide();
                        $('#sf').hide();
                        break;
                }
            });
            $('#bcl').click(function () {
                val = $('#<%=cl.ClientID%>').val();
                $.ajax({
                    type: "POST",
                    url: "massemailer.aspx/ChangeCity",
                    data: '{"sc":"' + val + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != '') {
                            data = eval(msg.d);
                            $('#cfav').text('');
                            $.each(data, function () {
                                if ($('#cfs option[value="' + this.val + '"]').length == 0)
                                    $('#cfav').append($("<option />").val(this.val).text(this.text));
                            });
                        }
                    }
                });
            });
            $('#ssb').click(function () {
                if ($('#ss').val().length == 0)
                    return;
                $.ajax({
                    type: "POST",
                    url: "massemailer.aspx/SearchSeller",
                    data: '{"sn":"' + $('#ss').val() + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != '') {
                            data = eval(msg.d);
                            $('#sfav').text('');
                            $.each(data, function () {
                                if ($('#sfs option[value="' + this.val + '"]').length == 0)
                                    $('#sfav').append($("<option />").val(this.val).text(this.text));
                            });
                        }
                    }
                });
            });
            $('#cf_as').click(function () { movesel('cfav', 'cfs', false); });
            $('#cf_rs').click(function () { movesel('cfs', 'cfav', false); });
            $('#cf_aa').click(function () { movesel('cfav', 'cfs', true); });
            $('#cf_ra').click(function () { movesel('cfs', 'cfav', true); });
            $('#sf_as').click(function () { movesel('sfav', 'sfs', false); });
            $('#sf_rs').click(function () { movesel('sfs', 'sfav', false); });
            $('#sf_aa').click(function () { movesel('sfav', 'sfs', true); });
            $('#sf_ra').click(function () { movesel('sfs', 'sfav', true); });
            $('#<%=btnOK.ClientID%>').click(function () {
                if ($('#<%=FileUpload1.ClientID%>').val().length == 0) {
                    alert('請選信件內容!');
                    return false;
                }
                if ($('#<%=txtbxTitle.ClientID%>').val().length == 0) {
                    alert('請填信件標題');
                    return false;
                }
                $('#cfs > option').attr('selected', 'selected');
                $('#sfs > option').attr('selected', 'selected');
                return true;
            });
        });

        function movesel(src, dst, all) {
            $('#' + src + ' option' + (all ? '' : ':selected')).remove().appendTo('#' + dst);
        }
    </script>
    <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Size="Large" Text="大量寄會員信"></asp:Label>&nbsp;&nbsp;&nbsp;<a
        href="../system/jobs.aspx">(排程列表)</a>
    <table style="width: 100%;">
        <tr>
            <td class="style2" align="right">
                說明&nbsp;
            </td>
            <td class="style1">
                &nbsp;<asp:TextBox ID="tbD" runat="server" Width="384px" />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style2" align="right">
                <asp:Label ID="Label1" runat="server" Text="信件標題"></asp:Label>
                &nbsp;
            </td>
            <td class="style1">
                &nbsp;<asp:TextBox ID="txtbxTitle" runat="server" Width="384px" />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style2" align="right">
                <asp:Label ID="lblFileUpload" runat="server" Text="選擇要上傳的HTML檔案"></asp:Label>
                &nbsp;
            </td>
            <td class="style1">
                &nbsp;<asp:FileUpload ID="FileUpload1" runat="server" Width="384px" />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" class="style3">
                <asp:Label ID="lblEmailType" runat="server" Text="選擇寄發會員信類型"></asp:Label>
            </td>
            <td align="left">
                <asp:RadioButtonList ID="rdbtnlstType" runat="server" RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="包含訂閱EDM的會員"></asp:Label>
            </td>
            <td>
                <asp:RadioButtonList ID="rdEdm" runat="server" RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="style2" align="right" valign="top">
                其他條件
            </td>
            <td>
                <input type="radio" name="filt" value="0" id="filt0" checked /><label for="filt0">無</label>
                <input type="radio" name="filt" value="1" id="filt1" /><label for="filt1">區域</label>
                <input type="radio" name="filt" value="2" id="filt2" /><label for="filt2">商家</label>
                <br />
                <div id="cf" style="display: none">
                    區域:
                    <asp:DropDownList ID="cl" runat="server" DataTextField="CityName" DataValueField="Id" />
                    <input type="button" id="bcl" value="顯示" />
                    <table>
                        <tr>
                            <td>
                                現有區域
                                <select multiple="multiple" id="cfav" size="20" class="msb" />
                            </td>
                            <td>
                                <input type="button" id="cf_as" value=" > " /><br />
                                <input type="button" id="cf_rs" value=" < " /><br />
                                <br />
                                <input type="button" id="cf_aa" value=">>" /><br />
                                <input type="button" id="cf_ra" value="<<" /><br />
                            </td>
                            <td>
                                已選區域
                                <select multiple="multiple" name="cfs" id="cfs" size="20" class="msb" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="sf" style="display: none">
                    商家名稱:
                    <input type="text" id="ss" maxlength="50" size="30" /><input type="button" id="ssb"
                        value="搜尋" />
                    <table>
                        <tr>
                            <td>
                                <select multiple="multiple" id="sfav" size="20" class="msa" />
                            </td>
                            <td>
                                <input type="button" id="sf_as" value=" > " /><br />
                                <input type="button" id="sf_rs" value=" < " /><br />
                                <br />
                                <input type="button" id="sf_aa" value=">>" /><br />
                                <input type="button" id="sf_ra" value="<<" /><br />
                            </td>
                            <td>
                                <select multiple="multiple" name="sfs" id="sfs" size="20" class="msa" />
                            </td>
                        </tr>
                    </table>
                    <span class="warn">注意: 全區配送商家不適用.</span>
                </div>
            </td>
        </tr>
        <tr>
            <td class="style2" align="right">
                發送時間&nbsp;
            </td>
            <td class="style1">
                &nbsp;<asp:TextBox ID="tbT" runat="server" Width="384px" />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Button ID="btnOK" runat="server" Text="確認" OnClick="btnOK_Click" />
            </td>
        </tr>
    </table>
    <br />
    <table>
        <tr>
            <td class="style1" colspan="2" align="center">
                &nbsp;<asp:Label ID="lblMessage" runat="server" ForeColor="Red" Visible="False" />
                &nbsp;
            </td>
        </tr>
    </table>
    <br />
    <asp:Label ID="lblTitle2" runat="server" Text="匯入發送EDM無效清單" Font-Bold="True" Font-Size="Large"></asp:Label>
    <table>
        <tr>
            <td>
                <asp:FileUpload ID="FileUpload2" runat="server" />
                <asp:Button ID="btnImport" runat="server" Text="Import" OnClick="btnImport_Click" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td style="font-size: small">
                注意：
            </td>
        </tr>
        <tr>
            <td style="font-size: small">
                1.匯入txt檔，email以換行方式分隔<br />
                2.請於撈取報表前一日匯入排除清單<br />
            </td>
        </tr>
    </table>
</asp:Content>

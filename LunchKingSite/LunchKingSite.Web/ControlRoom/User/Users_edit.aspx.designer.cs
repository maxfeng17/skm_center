//------------------------------------------------------------------------------
// <自動產生的>
//     這段程式碼是由工具產生的。
//
//     變更這個檔案可能會導致不正確的行為，而且如果已重新產生
//     程式碼，則會遺失變更。
// </自動產生的>
//------------------------------------------------------------------------------



public partial class admin_user_edit {
    
    /// <summary>
    /// panDisplay 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Panel panDisplay;
    
    /// <summary>
    /// ActionTitle 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Literal ActionTitle;
    
    /// <summary>
    /// trResultRow 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlTableRow trResultRow;
    
    /// <summary>
    /// lblMessage 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lblMessage;
    
    /// <summary>
    /// hfUserId 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.HiddenField hfUserId;
    
    /// <summary>
    /// ltUserId 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Literal ltUserId;
    
    /// <summary>
    /// litGuestMemberMark 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Literal litGuestMemberMark;
    
    /// <summary>
    /// Label2 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label Label2;
    
    /// <summary>
    /// Email 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox Email;
    
    /// <summary>
    /// RequiredFieldValidator2 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator2;
    
    /// <summary>
    /// PasswordRow 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlTableRow PasswordRow;
    
    /// <summary>
    /// lblPassword 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lblPassword;
    
    /// <summary>
    /// Password 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox Password;
    
    /// <summary>
    /// RequiredFieldValidator3 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator3;
    
    /// <summary>
    /// NewPasswordRow 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlTableRow NewPasswordRow;
    
    /// <summary>
    /// lblNewPassword 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lblNewPassword;
    
    /// <summary>
    /// cbChangePassword 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.CheckBox cbChangePassword;
    
    /// <summary>
    /// UpdatePanel3 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.UpdatePanel UpdatePanel3;
    
    /// <summary>
    /// NewPassword 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox NewPassword;
    
    /// <summary>
    /// RequiredFieldValidator6 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator6;
    
    /// <summary>
    /// SecretQuestionRow 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlTableRow SecretQuestionRow;
    
    /// <summary>
    /// Label5 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label Label5;
    
    /// <summary>
    /// SecretQuestion 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox SecretQuestion;
    
    /// <summary>
    /// RequiredFieldValidator4 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator4;
    
    /// <summary>
    /// SecretAnswerRow 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlTableRow SecretAnswerRow;
    
    /// <summary>
    /// Label6 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label Label6;
    
    /// <summary>
    /// SecretAnswer 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox SecretAnswer;
    
    /// <summary>
    /// RequiredFieldValidator5 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator5;
    
    /// <summary>
    /// tbLastName 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox tbLastName;
    
    /// <summary>
    /// tbFirstName 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox tbFirstName;
    
    /// <summary>
    /// UpdatePanel2 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.UpdatePanel UpdatePanel2;
    
    /// <summary>
    /// tbBirthDay 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox tbBirthDay;
    
    /// <summary>
    /// btnDate 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.ImageButton btnDate;
    
    /// <summary>
    /// CalendarExtender1 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::AjaxControlToolkit.CalendarExtender CalendarExtender1;
    
    /// <summary>
    /// rbGender 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.RadioButtonList rbGender;
    
    /// <summary>
    /// tbMobile 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox tbMobile;
    
    /// <summary>
    /// tbUserEmail 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox tbUserEmail;
    
    /// <summary>
    /// tbAreaCode 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox tbAreaCode;
    
    /// <summary>
    /// tbCompanyPhone 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox tbCompanyPhone;
    
    /// <summary>
    /// tbExtension 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox tbExtension;
    
    /// <summary>
    /// ddlCity 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.DropDownList ddlCity;
    
    /// <summary>
    /// UpdatePanel1 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.UpdatePanel UpdatePanel1;
    
    /// <summary>
    /// ddlBuilding 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.DropDownList ddlBuilding;
    
    /// <summary>
    /// up4 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.UpdatePanel up4;
    
    /// <summary>
    /// aib 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::LunchKingSite.Web.ControlRoom.Controls.AddressInputBox aib;
    
    /// <summary>
    /// litRegisterFrom 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Literal litRegisterFrom;
    
    /// <summary>
    /// litRiskScore 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Literal litRiskScore;
    
    /// <summary>
    /// tbC 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox tbC;
    
    /// <summary>
    /// btnUnlockUser 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Button btnUnlockUser;
    
    /// <summary>
    /// SaveButton 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Button SaveButton;
    
    /// <summary>
    /// pS 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Panel pS;
    
    /// <summary>
    /// cbS 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.CheckBoxList cbS;
    
    /// <summary>
    /// chkIsApproved 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.CheckBox chkIsApproved;
    
    /// <summary>
    /// bS 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Button bS;
    
    /// <summary>
    /// litOrderCount 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Literal litOrderCount;
    
    /// <summary>
    /// litSuccessOrderCount 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Literal litSuccessOrderCount;
    
    /// <summary>
    /// rowSuccessfulOrderRate 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlTableRow rowSuccessfulOrderRate;
    
    /// <summary>
    /// lbFailOrderRate 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Literal lbFailOrderRate;
    
    /// <summary>
    /// litTurnover 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Literal litTurnover;
    
    /// <summary>
    /// bIm 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Button bIm;
    
    /// <summary>
    /// panFixMembership 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Panel panFixMembership;
    
    /// <summary>
    /// txtPassword 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox txtPassword;
    
    /// <summary>
    /// btnFixMembership 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Button btnFixMembership;
    
    /// <summary>
    /// lbPcash 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lbPcash;
    
    /// <summary>
    /// gML 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.GridView gML;
    
    /// <summary>
    /// phPic 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.PlaceHolder phPic;
    
    /// <summary>
    /// imgPic 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.HtmlControls.HtmlImage imgPic;
    
    /// <summary>
    /// txtInvalidMemberMemo 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox txtInvalidMemberMemo;
    
    /// <summary>
    /// btnInvalidMember 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Button btnInvalidMember;
    
    /// <summary>
    /// repOAuthUserClients 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Repeater repOAuthUserClients;
    
    /// <summary>
    /// ab 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::LunchKingSite.Web.ControlRoom.Controls.AuditBoard ab;
    
    /// <summary>
    /// lblErrMsg 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lblErrMsg;
}

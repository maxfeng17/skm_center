﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BitrafficsImport.aspx.cs" 
    Inherits="LunchKingSite.Web.ControlRoom.User.BitrafficsImport" MasterPageFile="~/ControlRoom/backend.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <style type="text/css">
        .bgOrange
        {
            background-color: #FFC690;
            text-align: center;
        }
        .bgBlue
        {
            background-color: #B3D5F0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <table style="background-color: gray; width: 700px">
            <tr>
                <td colspan="2" style="background-color: Green; color: White; font-weight: bolder;
                    text-align: center">
                    BItraffics資料匯入
                </td>
            </tr>
            <tr>
                <td class="bgOrange" style="width: 25%">
                    上傳XLS檔案
                </td>
                <td class="bgBlue">
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                    <br />
                  (請確認匯入的excel檔為 97-2003共用格式*.xls)
                </td>
            </tr>
                        <tr>
                <td colspan="2" class="bgBlue" style="text-align: center">
                    <asp:Button ID="Import" runat="server" Text="確認上傳" OnClick="Import_Click" ValidationGroup="A" />
                </td>
                     </tr>
            <tr>
                <td class="bgOrange" style="width: 25%">
                    上傳CSV檔案
                </td>
                <td class="bgBlue">
                    <asp:FileUpload ID="FileUpload2" runat="server" />
                    <br />
                  (請確認匯入的檔案為CSV檔)
                </td>
            </tr>
                        <tr>
                <td colspan="2" class="bgBlue" style="text-align: center">
                    <asp:Button ID="Import_csv" runat="server" Text="確認上傳" OnClick="Import_Click_csv" ValidationGroup="A" />
                </td>
                     </tr>
        </table>
    </div>
</asp:Content>
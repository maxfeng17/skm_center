﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Web.Services;
using System.Text;
using System.IO;
using System.Net;
using System.Xml;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.UI;


namespace LunchKingSite.Web.ControlRoom.User
{
    public partial class Weather_opendata : RolePage
    {
        IMarketingProvider op = ProviderFactory.Instance().GetProvider<IMarketingProvider>();
        //IPponProvider pp;

        protected void Page_Load(object sender, EventArgs e)
        { }

        protected void Import_Click(object sender, EventArgs e)
        {
            XmlDocument xml = new XmlDocument();

            xml.Load("http://opendata.cwb.gov.tw/member/opendataapi?dataid=F-C0032-001&authorizationkey=CWB-A01FD046-AA6B-4C27-A307-616C33DB89B7");

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xml.NameTable);
            nsmgr.AddNamespace("xs", "urn:cwb:gov:tw:cwbcommon:0.1");

            XmlNodeList xnList;
            XmlElement root = xml.DocumentElement;
            //xnList = root.SelectNodes("/xs:cwbopendata/xs:dataset/xs:location", nsmgr);
            xnList = root.SelectNodes("/xs:cwbopendata/xs:dataset/xs:location/xs:weatherElement/xs:time", nsmgr);

            //XmlNodeList xyList;
            //xyList = root.SelectNodes("/xs:cwbopendata/xs:dataset/xs:location/xs:weatherElement/xs:time", nsmgr);

            var result = new OpendataWeatherCollection();

            foreach (XmlNode xn in xnList)
            {
                OpendataWeather row = new OpendataWeather();

                if (xn.ParentNode.PreviousSibling.Name == "locationName")
                {
                    row.City = xn.ParentNode.PreviousSibling.ParentNode.FirstChild.InnerText;
                    row.Createtime = DateTime.Now;

                    string st = xn.FirstChild.InnerText;
                    string st2 = st.Substring(0, 19);
                    string st3 = st2.Replace("T", " ");

                    DateTime tep;
                    if (DateTime.TryParse(st3.ToString(), out tep))
                    {
                        row.Stime = tep;
                    }

                    //string stime = st.Substring(0, 10);  

                    string se = xn.FirstChild.NextSibling.InnerText;
                    string et2 = se.Substring(0, 19);
                    string et3 = et2.Replace("T", " ");

                    DateTime etp;
                    if (DateTime.TryParse(et3.ToString(), out etp))
                    {
                        row.Etime = etp;
                    }

                    foreach (XmlNode xy in xnList)
                    {
                        string sCity = xn.ParentNode.PreviousSibling.ParentNode.FirstChild.InnerText;
                        if (xy.ParentNode.PreviousSibling.Name == "locationName" & sCity == row.City & xy.FirstChild.InnerText == st)
                        {

                            row.Wx = xy.LastChild.FirstChild.InnerText;

                        }
                        else
                            if (xy.ParentNode.FirstChild.InnerText == "MaxT")
                            {
                                row.MaxT = Convert.ToInt32(xy.LastChild.FirstChild.InnerText);
                            }
                            else
                                if (xy.ParentNode.FirstChild.InnerText == "MinT")
                                {
                                    //string MinT = xn.LastChild.FirstChild.InnerText;
                                    row.MinT = Convert.ToInt32(xy.LastChild.FirstChild.InnerText);
                                }
                                else
                                    if (xy.ParentNode.FirstChild.InnerText == "CI")
                                    {
                                        //string CI = xn.LastChild.FirstChild.InnerText;
                                        row.Ci = xy.LastChild.FirstChild.InnerText;
                                    }
                                    else
                                        if (xy.ParentNode.FirstChild.InnerText == "PoP")
                                        {
                                            //string PoP = xn.LastChild.FirstChild.InnerText;
                                            row.PoP = xy.LastChild.FirstChild.InnerText;
                                        }
                                        else continue;
                    }
                    result.Add(row);
                }
            }
            DB.SaveAll(result);

        }
    }
}

   
  






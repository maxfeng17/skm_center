﻿using System;
using System.Web;
using System.Web.Security;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System.Net.Mail;
using LunchKingSite.Core.UI;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.Text;
using log4net;

namespace LunchKingSite.Web.ControlRoom.User
{
    public partial class SendPponOrderEmail : RolePage
    {
        protected static ISysConfProvider config;
        protected void Page_Load(object sender, EventArgs e)
        {
            config = ProviderFactory.Instance().GetConfig();
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            int SendCount = 0;
            string strError = "";
            if (cbPpon.Checked)
            {
                OrderCollection orders = ProviderFactory.Instance().GetProvider<IPponProvider>().PponOrderGetByBid(new Guid(txtbid.Text));
                foreach (DataOrm.Order o in orders)
                {
                    string userEmail = MemberFacade.GetUserEmail(o.UserId);
                    try
                    {
                        SendMail(userEmail);
                        SendCount++;
                    }
                    catch
                    {
                        strError = strError + userEmail + "<br/>";
                    }
                }

                // 呼叫雄獅的 api 通知檔次異動
                NotifyLionTravel();
            }

            if (cbList.Checked)
            {
                string[] sTemp = txtList.Text.Split(',');
                foreach (string s in sTemp)
                {
                    try
                    {
                        SendMail(s);
                        SendCount++;
                    }
                    catch { strError = strError + s + "<br/>"; }
                }
            }

            lblfeeback.Text = "發送完畢，共計發送了" + SendCount.ToString() + "筆資料<br/>未發送名單<br/>";
        }

        protected void btn_Click(object sender, EventArgs e)
        {
            if (cbPpon.Checked)
            {
                OrderCollection vpdc = ProviderFactory.Instance().GetProvider<IPponProvider>().PponOrderGetByBid(new Guid(txtbid.Text));
                lblresult.Text = "將會執行" + vpdc.Count.ToString() + "筆好康訂購人資料，第一筆訂單編號為" + vpdc[0].OrderId + "<br/>";
            }

            if (cbList.Checked)
            {
                string[] sTemp = txtList.Text.Split(',');
                lblresult.Text += "將會執行" + (sTemp.Length).ToString() + "筆清單中的資料";
            }
        }

        private void SendMail(string addr)
        {
            // 3. Send mail
            MailMessage msg = new MailMessage();

            msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
            msg.To.Add(addr);
            msg.Subject = txttitle.Text;
            msg.Body = HttpUtility.HtmlDecode(txtbody.Text.Trim());
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }

        protected void cbList_CheckedChanged(object sender, EventArgs e)
        {
            txtList.Visible = cbList.Checked;

        }

        private void NotifyLionTravel()
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", config.LionTravelNofityAPIToken);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //LF06_DEAL		    Guid	檔次Guid
                //LF06_STORES		Guid	分店Guid
                //LF06_TITLE		string	主旨
                //LF06_DESC		    string	內容
                //LF06_TEXT		    text	檔次資料
                var content = JObject.FromObject(new { LF06_DEAL = txtbid.Text, LF06_STORES = Guid.Empty, LF06_TITLE = txttitle.Text, LF06_DESC = HttpUtility.HtmlDecode(txtbody.Text.Trim()), LF06_TEXT = string.Empty }).ToString();
                try
                {
                    var response = client.PostAsync(config.LionTravelNotifyAPI, new StringContent(content, Encoding.UTF8, "application/json")).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        //{
                        // "IsSuccess": true,
                        // "Msg": "",
                        // "Data": "true"
                        //}
                        dynamic message = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                        if (message.Value<bool>("IsSuccess"))
                        {
                            ILog logger = LogManager.GetLogger(typeof(SendPponOrderEmail).Name);
                            logger.Error("檔次異動通知雄獅成功!!" + " content：" + content);
                        }
                        else
                        {
                            ILog logger = LogManager.GetLogger(typeof(SendPponOrderEmail).Name);
                            logger.Error("檔次異動通知雄獅失敗!!, MSG：" + message.Value<string>("Msg") + ", content：" + content);
                        }
                    }
                    else
                    {
                        ILog logger = LogManager.GetLogger(typeof(SendPponOrderEmail).Name);
                        logger.Error("檔次異動通知雄獅失敗!!，StatusCode：" + response.StatusCode.ToString() + ", content：" + content);
                    }
                }
                catch (Exception ex)
                {
                    ILog logger = LogManager.GetLogger(typeof(SendPponOrderEmail).Name);
                    logger.Error("檔次異動通知雄獅失敗!!, content：" + content + ", message：" + ex.Message);
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using System.Web.Security;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.Web.ControlRoom.hr
{
    public partial class MemberRoleManagement : RolePage, IMemberRoleManagementView
    {
        #region props
        private static ISysConfProvider cp;
        protected int? CurrentTabPage;
        const int _TAB_RENEW = 3;
        public MemberRoleManagement()
        {
            cp = ProviderFactory.Instance().GetConfig();
        }

        private MemberRoleManagementPresenter _presenter;
        public MemberRoleManagementPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public string LoginUser
        {
            get
            {
                return User.Identity.Name;
            }
        }
        public int PageSize
        {
            get { return ucMemberPager.PageSize; }
            set { ucMemberPager.PageSize = value; }
        }
        public string MemberEmail
        {
            get { return txtMemberEmail.Text.Trim(); }
        }
        public string OrgName
        {
            get { return txtRoleNameSearch.Text.Trim(); }
            set { txtRoleNameSearch.Text = value; }
        }
        public string OrgDisplayName
        {
            set
            {
                lblRoleDisplayName.Text = value;
            }
        }
        public string FuncNameSearch
        {
            get { return txtFuncNameSearch.Text.Trim(); }
        }
        public string SiteUrl
        {
            get { return ProviderFactory.Instance().GetConfig().SiteUrl; }
        }

        #endregion

        #region event
        public event EventHandler<EventArgs> MemberSearchClicked;
        public event EventHandler<DataEventArgs<int>> OnGetMemberCount;
        public event EventHandler<DataEventArgs<int>> MemberPageChanged;
        public event EventHandler<DataEventArgs<string>> GetUserInRolesData;
        public event EventHandler<DataEventArgs<KeyValuePair<string, List<string>>>> MemberLeftSearch;
        public event EventHandler<DataEventArgs<KeyValuePair<string, List<string>>>> MemberRightSave;
        public event EventHandler<DataEventArgs<string>> GetMemberPrivilege;
        public event EventHandler<DataEventArgs<KeyValuePair<string, Dictionary<int, string>>>> MemberPrivilegeRightSave;

        public event EventHandler<EventArgs> RoleSearchClicked;
        public event EventHandler<DataEventArgs<string>> GetMemberRolesData;
        public event EventHandler<DataEventArgs<string>> DeleteMemberRoles;
        public event EventHandler<DataEventArgs<string>> AddMemberRoles;
        public event EventHandler<DataEventArgs<Organization>> RoleAddClicked;
        public event EventHandler<DataEventArgs<KeyValuePair<string, string>>> RoleNameEdit;
        public event EventHandler<DataEventArgs<KeyValuePair<string, List<string>>>> OrgLeftSearch;
        public event EventHandler<DataEventArgs<KeyValuePair<string, List<string>>>> OrgRightSave;
        public event EventHandler<DataEventArgs<string>> GetOrgPrivilege;
        public event EventHandler<DataEventArgs<KeyValuePair<string, Dictionary<int, string>>>> OrgPrivilegeRightSave;

        public event EventHandler<EventArgs> FuncSearchClicked;
        public event EventHandler<DataEventArgs<int>> OnGetFuncCount;
        public event EventHandler<DataEventArgs<int>> FuncPageChanged;
        public event EventHandler<DataEventArgs<int>> GetPrivilegeData;
        public event EventHandler<DataEventArgs<KeyValuePair<string, List<string>>>> SysOrgPrivilegeLeftSearch;
        public event EventHandler<DataEventArgs<KeyValuePair<KeyValuePair<int, string>, List<string>>>> SysOrgPrivilegeRightSave;
        public event EventHandler<DataEventArgs<KeyValuePair<string, List<string>>>> SysPrivilegeLeftSearch;
        public event EventHandler<DataEventArgs<KeyValuePair<KeyValuePair<int, string>, List<string>>>> SysPrivilegeRightSave;
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Presenter.OnViewInitialized();
            }
            lblRoleErrorMsg.Text = string.Empty;
            _presenter.OnViewLoaded();
        }

        #region 員工管理
        protected void lkMember_Click(object sender, EventArgs e)
        {
            divMemberSearch.Visible = true;
            divMemberSubTitle.Visible = false;
            divMemberDetail.Visible = false;
            divMemberPrivilege.Visible = false;
        }
        protected void btnMemberSearch_Click(object sender, EventArgs e)
        {
            if (this.MemberSearchClicked != null)
                this.MemberSearchClicked(this, e);
        }
        protected void gvMember_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName.ToString())
            {
                case "ROLE":
                    if (this.GetUserInRolesData != null)
                        this.GetUserInRolesData(this, new DataEventArgs<string>(e.CommandArgument.ToString()));
                    break;
                case "PRI":
                    if (this.GetMemberPrivilege != null)
                        this.GetMemberPrivilege(this, new DataEventArgs<string>(e.CommandArgument.ToString()));
                    break;
                default:
                    break;
            }
        }
        protected void MemberUpdateHandler(int pageNumber)
        {
            if (this.MemberPageChanged != null)
                this.MemberPageChanged(this, new DataEventArgs<int>(pageNumber));

            ucMemberPager.ResolvePagerView(pageNumber, true);
        }
        protected int GetMemberCount()
        {
            if (IsPostBack)
            {
                DataEventArgs<int> e = new DataEventArgs<int>(0);
                if (OnGetMemberCount != null)
                    OnGetMemberCount(this, e);
                return e.Data;
            }
            else
                return 0;
        }
        protected void btnMemberRight_Click(object sender, EventArgs e)
        {
            if (libMemberLeft.SelectedItem != null)
            {
                foreach (ListItem item in libMemberLeft.Items)
                {
                    if (item.Selected == true)
                    {
                        libMemberRight.Items.Add(item);
                    }
                }

                foreach (ListItem item in libMemberRight.Items)
                {
                    if (item.Selected == true)
                    {
                        libMemberLeft.Items.Remove(item);
                    }
                }
                libMemberRight.ClearSelection();
            }
        }
        protected void btnMemberLeft_Click(object sender, EventArgs e)
        {
            if (libMemberRight.SelectedItem != null)
            {
                foreach (ListItem item in libMemberRight.Items)
                {
                    if (item.Selected == true)
                    {
                        libMemberLeft.Items.Add(item);
                    }
                }

                foreach (ListItem item in libMemberLeft.Items)
                {
                    if (item.Selected == true)
                    {
                        libMemberRight.Items.Remove(item);
                    }
                }
                libMemberLeft.ClearSelection();
            }
        }
        protected void btnMemberLeftSearch_Click(object sender, EventArgs e)
        {
            if (this.MemberLeftSearch != null)
                this.MemberLeftSearch(this, new DataEventArgs<KeyValuePair<string, List<string>>>(new KeyValuePair<string, List<string>>(txtMemberLeftSearch.Text.Trim(), libMemberRight.Items.Cast<ListItem>().Select(item => item.Value).ToList())));
        }
        protected void btnMemberRightSave_Click(object sender, EventArgs e)
        {
            if (this.MemberRightSave != null)
                this.MemberRightSave(this, new DataEventArgs<KeyValuePair<string, List<string>>>(new KeyValuePair<string, List<string>>(hkMemberName.Text, libMemberRight.Items.Cast<ListItem>().Select(item => item.Value).Where(x => !x.Equals(MemberRoles.RegisteredUser.ToString())).ToList())));
        }
        protected void btnMemberPrivilegeRight_Click(object sender, EventArgs e)
        {
            if (libMemberPrivilegeLeft.SelectedItem != null)
            {
                foreach (ListItem item in libMemberPrivilegeLeft.Items)
                {
                    if (item.Selected == true)
                    {
                        libMemberPrivilegeRight.Items.Add(item);
                    }
                }

                foreach (ListItem item in libMemberPrivilegeRight.Items)
                {
                    if (item.Selected == true)
                    {
                        libMemberPrivilegeLeft.Items.Remove(item);
                    }
                }
                libMemberPrivilegeRight.ClearSelection();
            }
        }
        protected void btnMemberPrivilegeLeft_Click(object sender, EventArgs e)
        {
            if (libMemberPrivilegeRight.SelectedItem != null)
            {
                foreach (ListItem item in libMemberPrivilegeRight.Items)
                {
                    if (item.Selected == true)
                    {
                        libMemberPrivilegeLeft.Items.Add(item);
                    }
                }

                foreach (ListItem item in libMemberPrivilegeLeft.Items)
                {
                    if (item.Selected == true)
                    {
                        libMemberPrivilegeRight.Items.Remove(item);
                    }
                }
                libMemberPrivilegeLeft.ClearSelection();
            }
        }
        protected void btnMemberPrivilegeRightSave_Click(object sender, EventArgs e)
        {
            if (this.MemberPrivilegeRightSave != null)
                this.MemberPrivilegeRightSave(this, new DataEventArgs<KeyValuePair<string, Dictionary<int, string>>>(new KeyValuePair<string, Dictionary<int, string>>(hkMemberName.Text, libMemberPrivilegeRight.Items.Cast<ListItem>().ToDictionary(x => int.Parse(x.Value), x => x.Text))));
        }
        #endregion

        #region 群組管理
        protected void lkRole_Click(object sender, EventArgs e)
        {
            divRoleSearch.Visible = true;
            divRoleSubTitle.Visible = false;
            divRoleDetail.Visible = false;
            lkRoleAdd.Visible = true;
            divRolePrivilege.Visible = false;

            if (this.RoleSearchClicked != null)
                this.RoleSearchClicked(this, e);
        }
        protected void lkRoleAdd_Click(object sender, EventArgs e)
        {
            lblParentOrgDisPlayName.Text = string.Empty;
            txtRoleAddEnglish.Text = string.Empty;
            hidParentOrgName.Value = string.Empty;
            txtRoleAddChiness.Text = string.Empty;
            txtRoleAddEnglish.Text = string.Empty;
            hidOrgLevel.Value = string.Empty;

            divRoleAdd.Visible = true;
        }
        protected void btnRoleAddClose_Click(object sender, EventArgs e)
        {
            divRoleAdd.Visible = false;
        }
        protected void btnRoleSearch_Click(object sender, EventArgs e)
        {
            if (this.RoleSearchClicked != null)
                this.RoleSearchClicked(this, e);
        }
        protected void gvRole_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName.ToString())
            {
                case "USR":
                    if (this.GetMemberRolesData != null)
                        this.GetMemberRolesData(this, new DataEventArgs<string>(e.CommandArgument.ToString()));
                    break;
                case "PRI":
                    if (this.GetOrgPrivilege != null)
                        this.GetOrgPrivilege(this, new DataEventArgs<string>(e.CommandArgument.ToString()));
                    break;
                case "DEL":
                    if (this.DeleteMemberRoles != null)
                        this.DeleteMemberRoles(this, new DataEventArgs<string>(e.CommandArgument.ToString()));
                    break;
                case "ADD":
                    if (this.AddMemberRoles != null)
                        this.AddMemberRoles(this, new DataEventArgs<string>(e.CommandArgument.ToString()));
                    break;
                default:
                    break;
            }
        }
        protected void gvRole_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                KeyValuePair<Organization, string> dataItem = (KeyValuePair<Organization, string>)e.Row.DataItem;
                Label lblParentDisplayName = e.Row.FindControl("lblParentDisplayName") as Label;
                lblParentDisplayName.Text = string.Join(">", dataItem.Value);
            }
        }
        protected void btnRoleAdd_Click(object sender, EventArgs e)
        {
            if (this.RoleAddClicked != null)
            {
                Organization org = new Organization();
                org.Name = txtRoleAddEnglish.Text.Trim();
                org.ParentOrgName = hidParentOrgName.Value;
                org.DisplayName = string.Format("{0}({1})", txtRoleAddChiness.Text.Trim(), txtRoleAddEnglish.Text.Trim());
                int level = 0;
                int.TryParse(hidOrgLevel.Value, out level);
                org.OrgLevel = level;
                this.RoleAddClicked(this, new DataEventArgs<Organization>(org));
            }
            divRoleAdd.Visible = false;
            divRoleSearch.Visible = true;
            divRoleSubTitle.Visible = false;
            divRoleDetail.Visible = false;
            divRolePrivilege.Visible = false;
        }
        protected void btnRoleNameEdit_Click(object sender, EventArgs e)
        {
            if (this.RoleNameEdit != null)
                this.RoleNameEdit(this, new DataEventArgs<KeyValuePair<string, string>>(new KeyValuePair<string, string>(hidRoleName.Value, txtRoleNameEdit.Text.Trim())));
        }
        protected void btnOrgRight_Click(object sender, EventArgs e)
        {
            if (libOrgLeft.SelectedItem != null)
            {
                foreach (ListItem item in libOrgLeft.Items)
                {
                    if (item.Selected == true)
                    {
                        libOrgRight.Items.Add(item);
                    }
                }

                foreach (ListItem item in libOrgRight.Items)
                {
                    if (item.Selected == true)
                    {
                        libOrgLeft.Items.Remove(item);
                    }
                }
                libOrgRight.ClearSelection();
            }
        }
        protected void btnOrgLeft_Click(object sender, EventArgs e)
        {
            if (libOrgRight.SelectedItem != null)
            {
                foreach (ListItem item in libOrgRight.Items)
                {
                    if (item.Selected == true)
                    {
                        libOrgLeft.Items.Add(item);
                    }
                }

                foreach (ListItem item in libOrgLeft.Items)
                {
                    if (item.Selected == true)
                    {
                        libOrgRight.Items.Remove(item);
                    }
                }
                libOrgLeft.ClearSelection();
            }
        }
        protected void btnOrgLeftSearch_Click(object sender, EventArgs e)
        {
            if (this.OrgLeftSearch != null)
                this.OrgLeftSearch(this, new DataEventArgs<KeyValuePair<string, List<string>>>(new KeyValuePair<string, List<string>>(txtOrgLeftSearch.Text.Trim(), libOrgRight.Items.Cast<ListItem>().Select(item => item.Value).ToList())));
        }
        protected void btnOrgRightSave_Click(object sender, EventArgs e)
        {
            if (this.OrgRightSave != null)
                this.OrgRightSave(this, new DataEventArgs<KeyValuePair<string, List<string>>>(new KeyValuePair<string, List<string>>(hidRoleName.Value, libOrgRight.Items.Cast<ListItem>().Select(item => item.Value).ToList())));
        }
        protected void btnOrgPrivilegeRight_Click(object sender, EventArgs e)
        {
            if (libOrgPrivilegeLeft.SelectedItem != null)
            {
                foreach (ListItem item in libOrgPrivilegeLeft.Items)
                {
                    if (item.Selected == true)
                    {
                        libOrgPrivilegeRight.Items.Add(item);
                    }
                }

                foreach (ListItem item in libOrgPrivilegeRight.Items)
                {
                    if (item.Selected == true)
                    {
                        libOrgPrivilegeLeft.Items.Remove(item);
                    }
                }
                libOrgPrivilegeRight.ClearSelection();
            }
        }
        protected void btnOrgPrivilegeLeft_Click(object sender, EventArgs e)
        {
            if (libOrgPrivilegeRight.SelectedItem != null)
            {
                foreach (ListItem item in libOrgPrivilegeRight.Items)
                {
                    if (item.Selected == true)
                    {
                        libOrgPrivilegeLeft.Items.Add(item);
                    }
                }

                foreach (ListItem item in libOrgPrivilegeLeft.Items)
                {
                    if (item.Selected == true)
                    {
                        libOrgPrivilegeRight.Items.Remove(item);
                    }
                }
                libOrgPrivilegeLeft.ClearSelection();
            }
        }
        protected void btnOrgPrivilegeRightSave_Click(object sender, EventArgs e)
        {
            if (this.OrgPrivilegeRightSave != null)
                this.OrgPrivilegeRightSave(this, new DataEventArgs<KeyValuePair<string, Dictionary<int, string>>>(new KeyValuePair<string, Dictionary<int, string>>(hidRoleName.Value, libOrgPrivilegeRight.Items.Cast<ListItem>().ToDictionary(x => int.Parse(x.Value), x => x.Text))));
        }
        #endregion

        #region 功能權限管理
        protected void lkSystemFunc_Click(object sender, EventArgs e)
        {
            hlSystemFunc.Text = string.Empty;
            divFuncSearch.Visible = true;
            divSystemFuncSubTitle.Visible = false;
            divSystemFuncDetail.Visible = false;
        }
        protected void btnFuncSearch_Click(object sender, EventArgs e)
        {
            if (this.FuncSearchClicked != null)
                this.FuncSearchClicked(this, e);
        }
        protected void gvFunctionList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                SystemFunction dataItem = (SystemFunction)e.Row.DataItem;
                HyperLink hlFuncName = e.Row.FindControl("hlFuncName") as HyperLink;
                hlFuncName.Text = string.Format("{0}-{1}({2})", dataItem.ParentFunc, dataItem.DisplayName, dataItem.TypeDesc);
                hlFuncName.NavigateUrl = SiteUrl + dataItem.Link;
            }
        }
        protected void gvFunctionList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int id = 0;
            int.TryParse(e.CommandArgument.ToString(), out id);
            if (e.CommandName.Equals("UPD"))
            {
                if (this.GetPrivilegeData != null)
                    this.GetPrivilegeData(this, new DataEventArgs<int>(id));
            }
        }
        protected void FuncUpdateHandler(int pageNumber)
        {
            if (this.FuncPageChanged != null)
                this.FuncPageChanged(this, new DataEventArgs<int>(pageNumber));

            ucFuncPager.ResolvePagerView(pageNumber, true);
        }
        protected int GetFuncCount()
        {
            if (IsPostBack)
            {
                DataEventArgs<int> e = new DataEventArgs<int>(0);
                if (OnGetFuncCount != null)
                    OnGetFuncCount(this, e);
                return e.Data;
            }
            else
                return 0;
        }
        protected void btnSysOrgPrivilegeRight_Click(object sender, EventArgs e)
        {
            if (libSysOrgPrivilegeLeft.SelectedItem != null)
            {
                foreach (ListItem item in libSysOrgPrivilegeLeft.Items)
                {
                    if (item.Selected == true)
                    {
                        libSysOrgPrivilegeRight.Items.Add(item);
                    }
                }

                foreach (ListItem item in libSysOrgPrivilegeRight.Items)
                {
                    if (item.Selected == true)
                    {
                        libSysOrgPrivilegeLeft.Items.Remove(item);
                    }
                }
                libSysOrgPrivilegeRight.ClearSelection();
            }
        }
        protected void btnSysOrgPrivilegeLeft_Click(object sender, EventArgs e)
        {
            if (libSysOrgPrivilegeRight.SelectedItem != null)
            {
                foreach (ListItem item in libSysOrgPrivilegeRight.Items)
                {
                    if (item.Selected == true)
                    {
                        libSysOrgPrivilegeLeft.Items.Add(item);
                    }
                }

                foreach (ListItem item in libSysOrgPrivilegeLeft.Items)
                {
                    if (item.Selected == true)
                    {
                        libSysOrgPrivilegeRight.Items.Remove(item);
                    }
                }
                libSysOrgPrivilegeLeft.ClearSelection();
            }
        }
        protected void btnSysOrgPrivilegeLeftSearch_Click(object sender, EventArgs e)
        {
            if (this.SysOrgPrivilegeLeftSearch != null)
                this.SysOrgPrivilegeLeftSearch(this, new DataEventArgs<KeyValuePair<string, List<string>>>(new KeyValuePair<string, List<string>>(txtSysOrgPrivilegeLeftSearch.Text.Trim(), libSysOrgPrivilegeRight.Items.Cast<ListItem>().Select(item => item.Value).ToList())));
        }
        protected void btnSysOrgPrivilegeRightSave_Click(object sender, EventArgs e)
        {
            if (this.SysOrgPrivilegeRightSave != null)
                this.SysOrgPrivilegeRightSave(this, new DataEventArgs<KeyValuePair<KeyValuePair<int, string>, List<string>>>(new KeyValuePair<KeyValuePair<int, string>, List<string>>(new KeyValuePair<int, string>(int.Parse(hidFuncId.Value), hidFuncDisplayName.Value), libSysOrgPrivilegeRight.Items.Cast<ListItem>().Select(x => x.Value).ToList())));
        }
        protected void btnSysPrivilegeRight_Click(object sender, EventArgs e)
        {
            if (libSysPrivilegeLeft.SelectedItem != null)
            {
                foreach (ListItem item in libSysPrivilegeLeft.Items)
                {
                    if (item.Selected == true)
                    {
                        libSysPrivilegeRight.Items.Add(item);
                    }
                }

                foreach (ListItem item in libSysPrivilegeRight.Items)
                {
                    if (item.Selected == true)
                    {
                        libSysPrivilegeLeft.Items.Remove(item);
                    }
                }
                libSysPrivilegeRight.ClearSelection();
            }
        }
        protected void btnSysPrivilegeLeft_Click(object sender, EventArgs e)
        {
            if (libSysPrivilegeRight.SelectedItem != null)
            {
                foreach (ListItem item in libSysPrivilegeRight.Items)
                {
                    if (item.Selected == true)
                    {
                        libSysPrivilegeLeft.Items.Add(item);
                    }
                }

                foreach (ListItem item in libSysPrivilegeLeft.Items)
                {
                    if (item.Selected == true)
                    {
                        libSysPrivilegeRight.Items.Remove(item);
                    }
                }
                libSysPrivilegeLeft.ClearSelection();
            }
        }
        protected void btnSysPrivilegeLeftSearch_Click(object sender, EventArgs e)
        {
            if (this.SysPrivilegeLeftSearch != null)
                this.SysPrivilegeLeftSearch(this, new DataEventArgs<KeyValuePair<string, List<string>>>(new KeyValuePair<string, List<string>>(txtSysPrivilegeLeftSearch.Text.Trim(), libSysPrivilegeRight.Items.Cast<ListItem>().Select(item => item.Value).ToList())));
        }
        protected void btnSysPrivilegeRightSave_Click(object sender, EventArgs e)
        {
            if (this.SysPrivilegeRightSave != null)
                this.SysPrivilegeRightSave(this, new DataEventArgs<KeyValuePair<KeyValuePair<int, string>, List<string>>>(new KeyValuePair<KeyValuePair<int, string>, List<string>>(new KeyValuePair<int, string>(int.Parse(hidFuncId.Value), hidFuncDisplayName.Value), libSysPrivilegeRight.Items.Cast<ListItem>().Select(x => x.Value).ToList())));
        }
        #endregion

        /// <summary>
        /// 更新權限
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Renew(object sender, EventArgs e)
        {
            this.CurrentTabPage = _TAB_RENEW;
            
            if (chkSyncAllServers.Checked)
            {
                SortedDictionary<string, string> serverIPs =
                    ProviderFactory.Instance().GetSerializer().Deserialize<SortedDictionary<string, string>>(cp.ServerIPs);
                string result = SystemFacade.SyncCommand(serverIPs, SystemFacade._MEMBER_ROLE_MANAGEMENT, string.Empty, string.Empty);
                litTabRenewMessage.Text = result.Replace("\r\n", "<br />");
            }
            else
            {
                CommonFacade.CreateSystemFunctionPrivilege();
                litTabRenewMessage.Text = "更新完成";
            }
        }
        #endregion

        #region method
        public void GetMemberList(MemberCollection dataList)
        {
            gvMember.DataSource = dataList;
            gvMember.DataBind();

            ucMemberPager.ResolvePagerView(1, true);

            divMemberSearch.Visible = true;
            divMemberSubTitle.Visible = false;
            divMemberDetail.Visible = false;
            divMemberPrivilege.Visible = false;
        }
        public void SetUserInRolesData(Dictionary<string, string> roles, string userName)
        {
            hkMemberName.Text = userName;
            hkMemberName.NavigateUrl = ResolveUrl("~/controlroom/User/users_edit.aspx?username=" + userName);

            libMemberRight.DataSource = roles;
            libMemberRight.DataValueField = "Key";
            libMemberRight.DataTextField = "Value";
            libMemberRight.DataBind();

            libMemberLeft.Items.Clear();
            libMemberLeft.DataBind();
            txtMemberLeftSearch.Text = string.Empty;

            divMemberSearch.Visible = false;
            divMemberSubTitle.Visible = true;
            divMemberDetail.Visible = true;
            divMemberPrivilege.Visible = false;
        }
        public void SetMemberLeftSearchResult(Dictionary<string, string> resultList)
        {
            libMemberLeft.DataSource = resultList;
            libMemberLeft.DataValueField = "Key";
            libMemberLeft.DataTextField = "Value";
            libMemberLeft.DataBind();
        }
        public void SetMemberPrivilegeLeftSearchResult(Dictionary<string, string> resultList)
        {
            libMemberPrivilegeLeft.DataSource = resultList;
            libMemberPrivilegeLeft.DataValueField = "Key";
            libMemberPrivilegeLeft.DataTextField = "Value";
            libMemberPrivilegeLeft.DataBind();
        }
        public void SetMemberPrivilegeData(IOrderedEnumerable<KeyValuePair<int, string>> privileges, string userName, IOrderedEnumerable<KeyValuePair<int, string>> allFuncs)
        {
            hkMemberName.Text = userName;
            hkMemberName.NavigateUrl = ResolveUrl("~/controlroom/User/users_edit.aspx?username=" + userName);

            libMemberPrivilegeRight.DataSource = privileges;
            libMemberPrivilegeRight.DataValueField = "Key";
            libMemberPrivilegeRight.DataTextField = "Value";
            libMemberPrivilegeRight.DataBind();

            libMemberPrivilegeLeft.DataSource = allFuncs;
            libMemberPrivilegeLeft.DataValueField = "Key";
            libMemberPrivilegeLeft.DataTextField = "Value";
            libMemberPrivilegeLeft.DataBind();

            divMemberSearch.Visible = false;
            divMemberSubTitle.Visible = true;
            divMemberDetail.Visible = false;
            divMemberPrivilege.Visible = true;
        }

        public void GetRoleList(IOrderedEnumerable<KeyValuePair<Organization, string>> dataList)
        {
            gvRole.DataSource = dataList;
            gvRole.DataBind();

            divRoleSearch.Visible = true;
            divRoleSubTitle.Visible = false;
            divRoleDetail.Visible = false;
            divRolePrivilege.Visible = false;
        }
        public void SetOrganizationData(List<string> users, Organization mainOrg)
        {
            OrgDisplayName = txtRoleNameEdit.Text = mainOrg.DisplayName;
            hidRoleName.Value = mainOrg.Name;

            libOrgRight.DataSource = users;
            libOrgRight.DataBind();

            libOrgLeft.Items.Clear();
            libOrgRight.DataBind();
            txtOrgLeftSearch.Text = string.Empty;

            divRoleSearch.Visible = false;
            divRoleSubTitle.Visible = true;
            divRoleDetail.Visible = true;
            lkRoleAdd.Visible = false;
            divRolePrivilege.Visible = false;
        }
        public void SetParentOrganizationInfo(Organization org)
        {
            txtRoleAddChiness.Text = string.Empty;
            txtRoleAddEnglish.Text = string.Empty;
            lblParentOrgDisPlayName.Text = org.DisplayName;
            hidParentOrgName.Value = org.Name;
            hidOrgLevel.Value = org.OrgLevel.ToString();

            divRoleAdd.Visible = true;
        }
        public void SetOrgLeftSearchResult(List<Member> resultList)
        {
            libOrgLeft.DataSource = resultList.Select(x => x.UserName);
            libOrgLeft.DataBind();
        }
        public void SetOrgPrivilegeLeftSearchResult(List<OrgPrivilege> resultList)
        {
            libOrgPrivilegeLeft.DataSource = resultList;
            libOrgPrivilegeLeft.DataBind();
        }
        public void SetOrgPrivilegeData(IOrderedEnumerable<KeyValuePair<int, string>> orgPrivileges, Organization org, IOrderedEnumerable<KeyValuePair<int, string>> allFuncs)
        {
            OrgDisplayName = txtRoleNameEdit.Text = org.DisplayName;
            hidRoleName.Value = org.Name;

            libOrgPrivilegeRight.DataSource = orgPrivileges;
            libOrgPrivilegeRight.DataValueField = "Key";
            libOrgPrivilegeRight.DataTextField = "Value";
            libOrgPrivilegeRight.DataBind();

            libOrgPrivilegeLeft.DataSource = allFuncs;
            libOrgPrivilegeLeft.DataValueField = "Key";
            libOrgPrivilegeLeft.DataTextField = "Value";
            libOrgPrivilegeLeft.DataBind();

            divRoleSearch.Visible = false;
            divRoleSubTitle.Visible = true;
            divRoleDetail.Visible = false;
            lkRoleAdd.Visible = false;
            divRolePrivilege.Visible = true;
        }

        public void GetSystemFuncList(SystemFunctionCollection dataList)
        {
            gvFunctionList.DataSource = dataList;
            gvFunctionList.DataBind();

            ucFuncPager.ResolvePagerView(1, true);

            divFuncSearch.Visible = true;
            divSystemFuncSubTitle.Visible = false;
            divSystemFuncDetail.Visible = false;
        }
        public void SetSystemFunctionInfo(ViewPrivilegeCollection privileges, IOrderedEnumerable<KeyValuePair<Organization, string>> orgPrivileges, SystemFunction func)
        {
            hidFuncId.Value = func.Id.ToString();
            hidFuncDisplayName.Value = func.DisplayName;
            hlSystemFunc.Text = string.Format("{0}-{1}({2})", func.ParentFunc, func.DisplayName, func.TypeDesc);
            hlSystemFunc.NavigateUrl = SiteUrl + func.Link;

            libSysPrivilegeLeft.Items.Clear();
            libSysPrivilegeLeft.DataBind();
            txtSysPrivilegeLeftSearch.Text = string.Empty;

            libSysPrivilegeRight.DataSource = privileges.Select(x => x.UserName);
            libSysPrivilegeRight.DataBind();

            libSysOrgPrivilegeLeft.Items.Clear();
            libSysOrgPrivilegeLeft.DataBind();
            txtSysOrgPrivilegeLeftSearch.Text = string.Empty;

            libSysOrgPrivilegeRight.DataSource = orgPrivileges.ToDictionary(x => x.Key.Name, x => x.Value);
            libSysOrgPrivilegeRight.DataValueField = "Key";
            libSysOrgPrivilegeRight.DataTextField = "Value";
            libSysOrgPrivilegeRight.DataBind();

            divFuncSearch.Visible = false;
            divSystemFuncSubTitle.Visible = true;
            divSystemFuncDetail.Visible = true;
        }
        public void SetSysOrgPrivilegeLeftSearchResult(Dictionary<string, string> resultList)
        {
            libSysOrgPrivilegeLeft.DataSource = resultList;
            libSysOrgPrivilegeLeft.DataValueField = "Key";
            libSysOrgPrivilegeLeft.DataTextField = "Value";
            libSysOrgPrivilegeLeft.DataBind();
        }
        public void SetSysPrivilegeLeftSearchResult(List<Member> resultList)
        {
            libSysPrivilegeLeft.DataSource = resultList.Select(x => x.UserName);
            libSysPrivilegeLeft.DataBind();
        }

        public void ShowMessage(string msg)
        {
            string scriptMsg = "alert('" + msg + "');";
            ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "alert", scriptMsg, true);
        }


        #endregion

        #region private method
        #endregion
    }
}
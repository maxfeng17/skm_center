using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;

public partial class Admin_admin_users : RolePage
{
    private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
    private const int DEFAULT_PAGE_SIZE = 20;
    private int pageSize;

    protected void Page_Load(object sender, EventArgs e)
    {
        pageSize = (Pager1.PageSize == 0) ? DEFAULT_PAGE_SIZE : Pager1.PageSize;

        if (!Page.IsPostBack)
        {
            ViewState["SortDirection"] = " DESC";
            ViewState["SortExp"] = ViewMemberBuildingCity.Columns.CreateTime;

            GridView1.Columns[0].SortExpression = ViewMemberBuildingCity.Columns.UserName;
            GridView1.Columns[1].SortExpression = ViewMemberBuildingCity.Columns.MemberName;
            GridView1.Columns[2].SortExpression = ViewMemberBuildingCity.Columns.BuildingId;
            GridView1.Columns[3].SortExpression = ViewMemberBuildingCity.Columns.BuildingName;
            GridView1.Columns[4].SortExpression = ViewMemberBuildingCity.Columns.CompanyName;
            GridView1.Columns[5].SortExpression = ViewMemberBuildingCity.Columns.CreateTime;
            GridView1.Columns[6].SortExpression = ViewMemberBuildingCity.Columns.MemberStatus;

            //LoadGrid(1);
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["SortDirection"] = ((string)ViewState["SortDirection"] == " ASC") ? " DESC" : " ASC";
        ViewState["SortExp"] = e.SortExpression;
        LoadGrid(1);
        Pager1.ResolvePagerView(1);
    }

    public void SearchForUsers(object sender, EventArgs e)
    {
        SearchForUsers(sender, e, GridView1, SearchByDropDown, txtKeyword);
    }

    protected void SearchForUsers(object sender, EventArgs e, GridView dataGrid, DropDownList dropDown, TextBox textBox)
    {
        bool ifShow = true;
        string text = txtKeyword.Text;
        text = text.Replace("_", "[_]");
        text = text.Replace("*", "%");
        text = text.Replace("?", "_");

        if (text.Trim().Length != 0)
        {
            switch (dropDown.SelectedIndex)
            {
                case 0:
                case 8:
                    if (RegExRules.CheckMobile(text))
                    {
                        //認證手機
                        MobileMember mm = MemberFacade.GetMobileMember(text);
                        if (mm.IsLoaded)
                        {
                            ViewState["filter"] = ViewMemberBuildingCity.Columns.UniqueId + " = " + mm.UserId;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('找不到會員!')", true);
                            ifShow = false;
                        }
                    }
                    else
                    {
                        //帳號
                        ViewState["filter"] = ViewMemberBuildingCity.Columns.UserName + " like " + text;
                    }
                    break;
                case 1:
                    //姓名
                    ViewState["filter"] = ViewMemberBuildingCity.Columns.MemberName + " like " + text;
                    break;
                case 2:
                    //地區
                    ViewState["filter"] = ViewMemberBuildingCity.Columns.BuildingId + " like " + text;
                    break;
                case 3:
                    //大樓名稱
                    ViewState["filter"] = ViewMemberBuildingCity.Columns.BuildingName + " like " + text;
                    break;
                case 4:
                    //公司
                    ViewState["filter"] = ViewMemberBuildingCity.Columns.CompanyName + " like " + text;
                    break;
                case 5:
                    //手機
                    ViewState["filter"] = ViewMemberBuildingCity.Columns.Mobile + " like " + text;
                    break;
                case 6:
                    //串接編號
                    MemberLink ml = mp.MemberLinkGetByExternalId(text);
                    Member mem = mp.MemberGet(ml.UserId);
                    if (ml.IsLoaded)
                    {
                        ViewState["filter"] = ViewMemberBuildingCity.Columns.UserName + " like " + mem.UserName;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('找不到會員!')", true);
                        ifShow = false;
                    }
                    break;
                case 7:
                    //UserId
                    ViewState["filter"] = ViewMemberBuildingCity.Columns.UniqueId + " like " + text;
                    break;
                default:
                    ViewState["filter"] = null;
                    break;
            }
        }
        else
        {
            ViewState["filter"] = null;
        }
        if (ifShow)
        {
            LoadGrid(1);
            Pager1.ResolvePagerView(1, true);
        }
    }

    void LoadGrid(int pageNumber)
    {
        GridView1.DataSource = mp.ViewMemberBuildingCityGetListPaging(pageNumber, pageSize, (string)ViewState["SortExp"] + ViewState["SortDirection"], (string)ViewState["filter"]);
        GridView1.DataBind();
    }

    protected void UpdateHandler(int pageNumber)
    {
        LoadGrid(pageNumber);
    }

    protected int GetMemberCount()
    {
        if (IsPostBack)
        {
            return mp.MemberGetCount((string)ViewState["filter"]);
        }
        else
            return 0;
    }

    protected string ShowUserName(ViewMemberBuildingCity mem)
    {
        Member m = mp.MemberGet(mem.UniqueId);
        MemberProperty mpp = mp.MemberPropertyGetByUserId(m.UniqueId);
        if (mpp.IsLoaded && mpp.RegisterSourceType == (int)SingleSignOnSource.Mobile17Life)
        {
            MobileMember mm = MemberFacade.GetMobileMember(mem.UniqueId);
            if (mm.IsLoaded)
            {
                return mm.MobileNumber;
            }
        }
        return m.UserName;
    }

    protected string ShowStatus(ViewMemberBuildingCity mem)
    {
        Member m = mp.MemberGet(mem.UniqueId);
        MemberLinkCollection mlCol = mp.MemberLinkGetList(mem.UniqueId);
        MobileMember mm = MemberFacade.GetMobileMember(mem.UniqueId);

        if (mlCol.Any(t => t.ExternalOrg == (int)SingleSignOnSource.ContactDigitalIntegration))
        {
            if (Helper.IsFlagSet(m.Status, MemberStatusFlag.Active17Life) == false)
            {
                return "未認證、不能登入";
            }
            else if (m.IsApproved && m.IsLockedOut == false)
            {
                return "認證、可登入";
            }
            else
            {
                return "認證、限制登入";
            }
        }
        else if (mlCol.Any(t => t.ExternalOrg == (int)SingleSignOnSource.VbsMember))
        {
            if (m.IsApproved && m.IsLockedOut == false)
            {
                return "可登入";
            }
            else
            {
                return "限制登入";
            }
        }
        else if (mlCol.Count > 0)
        {
            if (m.IsApproved && m.IsLockedOut == false)
            {
                return "串接會員、可登入";
            }
            else
            {
                return "串接會員、限制登入";
            }
        }
        else if (mm.IsLoaded)
        {
            if (mm.Status == (int)MobileMemberStatusType.Activated)
            {
                return "已認證";
            }
            else
            {
                return "未認證";
            }
        }
        else
        {
            return "資料異常";
        }
    }

    protected string ShowMemberLevel(ViewMemberBuildingCity mem)
    {
        MemberProperty mpp = mp.MemberPropertyGetByUserId(mem.UniqueId);
        
        MemberLinkCollection mlCol = mp.MemberLinkGetList(mem.UniqueId);
        if (mlCol.Any(t => t.ExternalOrg == (int)SingleSignOnSource.VbsMember))
        {
            return "商家會員";
        }
        if (mpp.IsLoaded && mpp.RegisterSourceType == (int)SingleSignOnSource.Mobile17Life)
        {
            MobileMember mm = MemberFacade.GetMobileMember(mem.UniqueId);
            if (mm.IsLoaded)
            {
                return "手機會員";
            }
        }
        return mem.IsGuest ? Member._GUEST_MEMBER : "1級會員";
    }
}

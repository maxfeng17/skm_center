﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Services;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.User
{
    public partial class MassEmailer : RolePage, IMassEmailerView
    {
        public event EventHandler<DataEventArgs<string>> Send;
        public event EventHandler<DataEventArgs<string>> EdmSend;

        #region Properties
        private MassEmailerPresenter _presenter;
        public MassEmailerPresenter Presenter
        {
            set
            {
                _presenter = value;
                if (value != null)
                {
                    _presenter.View = this;
                }
            }
            get
            {
                return _presenter;
            }
        }

        public MassMailType MailType
        {
            get { return (MassMailType)int.Parse(this.rdbtnlstType.SelectedValue); }
        }

        public string Subject { get { return txtbxTitle.Text; } }

        public DateTime? SendTime
        {
            get
            {
                try
                {
                    return Helper.PowerExtractDateTime(tbT.Text);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                tbT.Text = value != null ? value.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty;
            }
        }

        public string Description
        {
            get { return tbD.Text; }
        }

        public List<string> CityBuildingFilter
        {
            get
            {
                return !string.IsNullOrEmpty(Request.Form["cfs"]) ? Request.Form["cfs"].Split(',').ToList() : null;
            }
        }

        public List<Guid> SellerFilter
        {
            get
            {
                return !string.IsNullOrEmpty(Request.Form["sfs"]) ? (from x in Request.Form["sfs"].Split(',') select new Guid(x)).ToList() : null;
            }
        }

        public MassMailFilterType FilterType
        {
            get
            {
                return !string.IsNullOrEmpty(Request.Form["filt"])
                       ? (MassMailFilterType)(int.Parse(Request.Form["filt"]))
                       : MassMailFilterType.None;
            }
        }

        public bool IsContainSubscription
        {
            get
            {
                if (rdEdm.SelectedValue == "Y")
                {
                    return true;
                }
                return false;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Presenter.OnViewInitialized();
                rdbtnlstType.Items.AddRange(WebUtility.GetListItemFromEnum(MassMailType.Advertisement));
                rdEdm.Items.Add(new ListItem("是", "Y"));
                rdEdm.Items.Add(new ListItem("否", "N"));
                rdEdm.SelectedValue = "N";
            }
            this.Presenter.OnViewLoaded();
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            if (Send != null && FileUpload1.HasFile)
            {
                string strContent = null;
                if (FileUpload1.HasFile)
                {
                    StreamReader rdr = new StreamReader(FileUpload1.PostedFile.InputStream, Encoding.Default);
                    strContent = rdr.ReadToEnd();
                }
                Send(this, new DataEventArgs<string>(strContent));
            }
            else
                SetResultMessage(I18N.Message.MassMailEmptyContent);
        }

        public void SetResultMessage(string resultMsg)
        {
            lblMessage.Text = resultMsg;
            lblMessage.Visible = !String.IsNullOrEmpty(resultMsg);
        }

        public void SetTopCityList(List<City> cities)
        {
            cl.DataSource = cities;
            cl.DataBind();
        }

        [WebMethod]
        public static string ChangeCity(string sc)
        {
            return new MassEmailerPresenter().GetChildrenCityJson(int.Parse(sc));
        }

        [WebMethod]
        public static string SearchSeller(string sn)
        {
            return new MassEmailerPresenter().GetSellerJson(sn);
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (EdmSend != null && FileUpload2.HasFile)
            {
                string strContent = null;
                if (FileUpload2.HasFile)
                {
                    StreamReader rdr = new StreamReader(FileUpload2.PostedFile.InputStream, Encoding.Default);
                    strContent = rdr.ReadToEnd();
                }
                EdmSend(this, new DataEventArgs<string>(strContent));
                SetResultMessage("排除名單更新完成");
            }
            else
            {
                SetResultMessage(I18N.Message.AgentArgumentInvalid);
            }
        }
    }
}

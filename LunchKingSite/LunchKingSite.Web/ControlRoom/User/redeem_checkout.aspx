﻿<%@ Page Title="申請購物金" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="redeem_checkout.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.User.redeem_checkout" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/javascript" src="//www.google.com/jsapi"></script>  
<script type="text/javascript">
    google.load("jquery", "1.4.2");
    google.load("jqueryui", "1.8");
</script>
<script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
<link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />	
<script type="text/javascript">
 function setupdatepicker(from, to) {
        var dates = $('#' + from + ', #' + to).datepicker({
            changeMonth: true,
            numberOfMonths: 3,
            onSelect: function(selectedDate) {
                var option = this.id == from ? "minDate" : "maxDate";
                var instance = $(this).data("datepicker");
                var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                dates.not(this).datepicker("option", option, date);
            }
        });
    }

    google.setOnLoadCallback(function () {
        $(document).ready(function () {
            setupdatepicker('<%=tsDate.ClientID%>', '<%=teDate.ClientID%>');
        })
    });
</script>

<table>
    <tr>
        <td>紅利申請時間：</td>
        <td>
            起<asp:TextBox ID="tsDate" runat="server"></asp:TextBox>日期<asp:DropDownList ID="tsH"
                runat="server">
                <asp:ListItem>00</asp:ListItem>
                <asp:ListItem>01</asp:ListItem>
                <asp:ListItem>02</asp:ListItem>
                <asp:ListItem>03</asp:ListItem>
                <asp:ListItem>04</asp:ListItem>
                <asp:ListItem>05</asp:ListItem>
                <asp:ListItem>06</asp:ListItem>
                <asp:ListItem>07</asp:ListItem>
                <asp:ListItem>08</asp:ListItem>
                <asp:ListItem>09</asp:ListItem>
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>11</asp:ListItem>
                <asp:ListItem>12</asp:ListItem>
                <asp:ListItem>13</asp:ListItem>
                <asp:ListItem>14</asp:ListItem>
                <asp:ListItem>15</asp:ListItem>
                <asp:ListItem>16</asp:ListItem>
                <asp:ListItem>17</asp:ListItem>
                <asp:ListItem>18</asp:ListItem>
                <asp:ListItem>19</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>21</asp:ListItem>
                <asp:ListItem>22</asp:ListItem>
                <asp:ListItem>23</asp:ListItem>
                <asp:ListItem>24</asp:ListItem>
            </asp:DropDownList>
            時<asp:TextBox 
                ID="tsM" runat="server" Width="30px" Text="00"></asp:TextBox>分<br />訖<asp:TextBox ID="teDate" runat="server"></asp:TextBox>日期<asp:DropDownList ID="teH"
                runat="server">
                <asp:ListItem>00</asp:ListItem>
                <asp:ListItem>01</asp:ListItem>
                <asp:ListItem>02</asp:ListItem>
                <asp:ListItem>03</asp:ListItem>
                <asp:ListItem>04</asp:ListItem>
                <asp:ListItem>05</asp:ListItem>
                <asp:ListItem>06</asp:ListItem>
                <asp:ListItem>07</asp:ListItem>
                <asp:ListItem>08</asp:ListItem>
                <asp:ListItem>09</asp:ListItem>
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>11</asp:ListItem>
                <asp:ListItem>12</asp:ListItem>
                <asp:ListItem>13</asp:ListItem>
                <asp:ListItem>14</asp:ListItem>
                <asp:ListItem>15</asp:ListItem>
                <asp:ListItem>16</asp:ListItem>
                <asp:ListItem>17</asp:ListItem>
                <asp:ListItem>18</asp:ListItem>
                <asp:ListItem>19</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>21</asp:ListItem>
                <asp:ListItem>22</asp:ListItem>
                <asp:ListItem>23</asp:ListItem>
                <asp:ListItem>24</asp:ListItem>
            </asp:DropDownList>時<asp:TextBox 
                ID="teM" runat="server" Width="30px" Text="00"></asp:TextBox>分</td>
                <td><asp:Button ID="btnQuery" runat="server" Text="查詢" onclick="btnQuery_Click" /><br />
                    <asp:Button ID="btnExport" runat="server" Text="產生報表" 
                        onclick="btnExport_Click" /><br />
                        <asp:Button ID="btnExport1" runat="server" Text="產生報表1" 
                        onclick="btnExport_Click" />
               
                <br /><asp:Button ID="btnCheckOut" runat="server" Text="確認請款" 
                        onclick="btnCheckOut_Click" OnClientClick="return confirm('確認後將會更新請款時間為現在');" />
                </td>
        
    </tr>
    <tr>
        <td colspan="3">
            <asp:GridView ID="gv" runat="server">
            </asp:GridView>
            <br />
            <asp:GridView ID="gv1" runat="server">
            </asp:GridView>
        </td>
    </tr>
</table>
</asp:Content>

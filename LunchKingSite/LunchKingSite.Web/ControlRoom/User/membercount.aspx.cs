﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using System.Web.Security;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.User
{
    public partial class membercount : RolePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }
            loaddata();
        }

        private void loaddata()
        {
            char[] num = ProviderFactory.Instance().GetProvider<IMemberProvider>().MemberGetCount("").ToString().ToCharArray();
            
            string output = "";
            foreach (char m in num)
            {
                output = output + " <img alt=\"\" src=\"../../Themes/default/images/number/" + m.ToString() + ".jpg\" />";
            }
            lnum.Text = output;
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            loaddata();
        }
    }
}
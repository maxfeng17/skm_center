﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.User
{
    public partial class redeem_checkout : RolePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnQuery_Click(object sender, EventArgs e)
        {
            DateTime sDate = DateTime.Parse(tsDate.Text+" "+tsH.Text+":"+tsM.Text);
            DateTime eDate = DateTime.Parse(teDate.Text + " " + teH.Text + ":" + teM.Text);
            DataTable dt = ProviderFactory.Instance().GetProvider<IOrderProvider>().RedeemGetForCheckOut(sDate, eDate);
            gv.DataSource = dt;
            gv.DataBind();
            DataTable dt1 = ProviderFactory.Instance().GetProvider<IOrderProvider>().RedeemGetForCheckOutCSV(sDate, eDate);
            gv1.DataSource = dt1;
            gv1.DataBind();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            GridViewExportUtil.Export("P-CASH兌換送簽.xls", gv);
        }
        protected void btnExport1_Click(object sender, EventArgs e)
        {
            GridViewExportUtil.Export("P-CASH兌換送簽.csv", gv1);
        }

        protected void btnCheckOut_Click(object sender, EventArgs e)
        {
            DateTime sDate = DateTime.Parse(tsDate.Text+" "+tsH.Text+":"+tsM.Text);
            DateTime eDate = DateTime.Parse(teDate.Text + " " + teH.Text + ":" + teM.Text);
            ProviderFactory.Instance().GetProvider<IOrderProvider>().RedeemUpdateForCheckOut(sDate, eDate, DateTime.Now);
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="MemberRoleManagement.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.hr.MemberRoleManagement" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Src="~/ControlRoom/Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("jqueryui", "1.8");
    </script>
    <style type="text/css">
        #tabs
        {
            font-size: smaller;
        }
        fieldset
        {
            margin: 10px 0px;
            border: solid 1px gray;
        }
        fieldset legend
        {
            color: #4F8AFF;
            font-weight: bolder;
        }
        .LinkButton
        {
            font-size: Small;
            color: #00F;
            text-decoration: underline;
            border: 0;
            margin: 0;
            background: none;
        }
        .LinkButton :hover
        {
            font-size: Small;
            color: #00F;
            text-decoration: underline;
            border: 0;
            margin: 0;
            background: none;
        }
        .HoverRow:hover
        {
            background-color: LightGoldenrodYellow;
        }
        a:hover
        {
            font-weight: bold;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            var $tabs = $('#tabs').tabs();

            <% if (this.CurrentTabPage != null){
                 Response.Write("changetab(" + this.CurrentTabPage + ");");
             }
             %>

            $('fieldset').addClass('ui-corner-all');
            $("button, input:submit, a, input:button", "#container").button();

            $('[id*=btnMemberSearch]').click(function () {
                $tabs.tabs('select', 0);
            });

            $('[id*=btnRoleSearch]').click(function () {
                $tabs.tabs('select', 1);
            });

            $('[id*=tabs-] a').removeClass().addClass('LinkButton');
            $('[id*=tabs-] input[type=button]').removeClass().addClass('LinkButton');
        });

        function changetab(i) {
            $('#tabs').tabs({ selected: i });
            return false;
        }

        function RoleAddCheckData() {
            if ($('[id*=txtRoleAddChiness]').val() == '') {
                alert('沒有輸入群組中文名稱!!');
                return false;
            }
            if ($('[id*=txtRoleAddEnglish]').val() == '') {
                alert('沒有輸入群組英文名稱!!');
                return false;
            }
            return true;
        }

        function CheckSearchData(obj) {
            if ($(obj).prev('input[type=text]').val() == '') {
                alert('請輸入查詢字串!!');
                return false;
            } else {
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="container">
        <div id="tabs" style="width: 1200px">
            <ul>
                <li><a href="#tabs-1">員工管理</a></li>
                <li><a href="#tabs-2">群組管理</a></li>
                <li><a href="#tabs-3">功能權限管理</a></li>
                <li><a href="#tabs-4">更新權限資料</a></li>
            </ul>
            <div id="tabs-1">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:LinkButton ID="lkMember" runat="server" OnClick="lkMember_Click">員工管理</asp:LinkButton>
                        <span id="divMemberSubTitle" runat="server" visible="false">&nbsp;>&nbsp;
                            <asp:HyperLink ID="hkMemberName" runat="server" Target="_blank"></asp:HyperLink>
                        </span>
                        <asp:Panel ID="divMemberSearch" runat="server">
                            <asp:TextBox ID="txtMemberEmail" runat="server" Width="200px"></asp:TextBox>&nbsp;
                            <asp:Button ID="btnMemberSearch" runat="server" Text="搜尋" OnClick="btnMemberSearch_Click" />
                            <br />
                            <asp:GridView ID="gvMember" runat="server" OnRowCommand="gvMember_RowCommand" ShowHeader="false"
                                Width="500px" AutoGenerateColumns="false" GridLines="Horizontal" ForeColor="Black"
                                CellPadding="4" BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE" Font-Size="Small"
                                CssClass="GridHover">
                                <RowStyle CssClass="HoverRow" />
                                <Columns>
                                    <asp:BoundField ReadOnly="True" DataField="UserName">
                                        <ItemStyle HorizontalAlign="Left" Width="70%" />
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkEdit" runat="server" Text="編輯群組" CommandName="ROLE" CommandArgument="<%# ((Member)Container.DataItem).UserName %>"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkPrivilegeEdit" runat="server" Text="權限設定" CommandName="PRI"
                                                CommandArgument="<%# ((Member)Container.DataItem).UserName %>"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="15%" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <uc1:Pager ID="ucMemberPager" runat="server" PageSize="20" ongetcount="GetMemberCount"
                                onupdate="MemberUpdateHandler" style="width: 400px"></uc1:Pager>
                        </asp:Panel>
                        <asp:Panel ID="divMemberDetail" runat="server" Visible="false">
                            <table>
                                <tr>
                                    <td align="left" style="font-weight: bold; font-size: 13px; font-family: Arial">
                                        可增加名單
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td align="left" style="font-weight: bold; font-size: 13px; font-family: Arial">
                                        已增加名單
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtMemberLeftSearch" runat="server" Width="480px"></asp:TextBox>
                                        <asp:ImageButton ID="btnMemberLeftSearch" runat="server" ImageUrl="~/Themes/default/images/17Life/B5/b_search.jpg"
                                            OnClick="btnMemberLeftSearch_Click" />
                                    </td>
                                    <td>
                                    </td>
                                    <td rowspan="2" style="font-size: small">
                                        <asp:ListBox ID="libMemberRight" runat="server" Height="300px" ondblclick="__doPostBack('libMemberLeft','');"
                                            Rows="10" Width="500px" SelectionMode="Multiple"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-size: small">
                                        <asp:ListBox ID="libMemberLeft" runat="server" Rows="10" Width="500px" Height="265px" SelectionMode="Multiple">
                                        </asp:ListBox>
                                    </td>
                                    <td align="center">
                                        <asp:Button ID="btnMemberRight" runat="server" OnClick="btnMemberRight_Click" Text="＞" />
                                        <br />
                                        <asp:Button ID="ButtonbtnMemberLeft" runat="server" OnClick="btnMemberLeft_Click"
                                            Text="＜" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: center;">
                                        <asp:Button ID="btnMemberRightSave" runat="server" Text="儲存" Width="100px" OnClick="btnMemberRightSave_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="divMemberPrivilege" runat="server" Visible="false">
                            <table>
                                <tr>
                                    <td align="left" style="font-weight: bold; font-size: 13px; font-family: Arial">
                                        可增加名單
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td align="left" style="font-weight: bold; font-size: 13px; font-family: Arial">
                                        已增加名單
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-size: small">
                                        <asp:ListBox ID="libMemberPrivilegeLeft" runat="server" Rows="10" Width="300px" Height="300px" SelectionMode="Multiple">
                                        </asp:ListBox>
                                    </td>
                                    <td align="center">
                                        <asp:Button ID="btnMemberPrivilegeRight" runat="server" OnClick="btnMemberPrivilegeRight_Click"
                                            Text="＞" />
                                        <br />
                                        <asp:Button ID="btnMemberPrivilegeLeft" runat="server" OnClick="btnMemberPrivilegeLeft_Click"
                                            Text="＜" />
                                    </td>
                                    <td style="font-size: small">
                                        <asp:ListBox ID="libMemberPrivilegeRight" runat="server" Height="300px" ondblclick="__doPostBack('libMemberPrivilegeLeft','');"
                                            Rows="10" Width="300px" SelectionMode="Multiple"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: center;">
                                        <asp:Button ID="btnMemberPrivilegeRightSave" runat="server" Text="儲存" Width="100px"
                                            OnClick="btnMemberPrivilegeRightSave_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="tabs-2">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:LinkButton ID="lkRole" runat="server" OnClick="lkRole_Click">群組管理</asp:LinkButton>
                        <span id="divRoleSubTitle" runat="server" visible="false">&nbsp;>&nbsp;
                            <asp:Label ID="lblRoleDisplayName" runat="server"></asp:Label>
                        </span>
                        <asp:LinkButton ID="lkRoleAdd" runat="server" OnClick="lkRoleAdd_Click">新增群組</asp:LinkButton>
                        <asp:Panel ID="divRoleAdd" runat="server" Visible="false">
                            <fieldset>
                                <legend>新增群組</legend>父群組：<asp:Label ID="lblParentOrgDisPlayName" runat="server"></asp:Label>
                                <asp:HiddenField ID="hidParentOrgName" runat="server" />
                                <asp:HiddenField ID="hidOrgLevel" runat="server" />
                                <br />
                                中文：<asp:TextBox ID="txtRoleAddChiness" runat="server"></asp:TextBox><br />
                                英文：<asp:TextBox ID="txtRoleAddEnglish" runat="server"></asp:TextBox><br />
                                <asp:Button ID="btnRoleAdd" runat="server" Text="確定" OnClick="btnRoleAdd_Click" OnClientClick="return RoleAddCheckData();" />
                                <asp:Button ID="btnRoleAddClose" runat="server" Text="取消" OnClick="btnRoleAddClose_Click" />
                            </fieldset>
                        </asp:Panel>
                        <asp:Panel ID="divRoleSearch" runat="server">
                            <asp:TextBox ID="txtRoleNameSearch" runat="server" Width="200px"></asp:TextBox>&nbsp;
                            <asp:Button ID="btnRoleSearch" runat="server" Text="搜尋" OnClick="btnRoleSearch_Click" />
                            <asp:Label ID="lblRoleErrorMsg" runat="server" ForeColor="Red"></asp:Label>
                            <br />
                            <asp:GridView ID="gvRole" runat="server" OnRowCommand="gvRole_RowCommand" OnRowDataBound="gvRole_RowDataBound"
                                ShowHeader="false" Width="850px" AutoGenerateColumns="false" GridLines="Horizontal"
                                ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE"
                                Font-Size="Small" CssClass="GridHover">
                                <RowStyle CssClass="HoverRow" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="lblParentDisplayName" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="65%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkEdit" runat="server" Text="編輯成員" CommandName="USR" CommandArgument="<%# ((KeyValuePair<Organization, string>)Container.DataItem).Key.Name %>"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkPrivilegeEdit" runat="server" Text="權限設定" CommandName="PRI"
                                                CommandArgument="<%# ((KeyValuePair<Organization, string>)Container.DataItem).Key.Name %>"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkAdd" runat="server" Text="新增子群組" CommandArgument="<%# ((KeyValuePair<Organization, string>)Container.DataItem).Key.Name %>"
                                                CommandName="ADD"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkDelete" runat="server" Text="刪除" CommandName="DEL" CommandArgument="<%# ((KeyValuePair<Organization, string>)Container.DataItem).Key.Name %>"
                                                OnClientClick="return confirm('確定刪除此群組?');"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="5%" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:HiddenField ID="hidRolePagerCount" runat="server" />
                        </asp:Panel>
                        <asp:HiddenField ID="hidRoleName" runat="server" />
                        <asp:Panel ID="divRoleDetail" runat="server" Visible="false">
                            <asp:TextBox ID="txtRoleNameEdit" runat="server" Width="250px"></asp:TextBox>&nbsp;
                            <asp:Button ID="btnRoleNameEdit" runat="server" Text="名稱更改" OnClick="btnRoleNameEdit_Click" /><br />
                            <table>
                                <tr>
                                    <td align="left" style="font-weight: bold; font-size: 13px; font-family: Arial">
                                        可增加名單
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td align="left" style="font-weight: bold; font-size: 13px; font-family: Arial">
                                        已增加名單
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtOrgLeftSearch" runat="server" Width="180px"></asp:TextBox>
                                        <asp:ImageButton ID="btnOrgLeftSearch" runat="server" ImageUrl="~/Themes/default/images/17Life/B5/b_search.jpg"
                                            OnClick="btnOrgLeftSearch_Click" OnClientClick="return CheckSearchData(this);" />
                                    </td>
                                    <td>
                                    </td>
                                    <td rowspan="2" style="font-size: small">
                                        <asp:ListBox ID="libOrgRight" runat="server" Height="300px" ondblclick="__doPostBack('libOrgLeft','');"
                                            Rows="10" Width="200px" SelectionMode="Multiple"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-size: small">
                                        <asp:ListBox ID="libOrgLeft" runat="server" Rows="10" Width="200px" Height="265px" SelectionMode="Multiple">
                                        </asp:ListBox>
                                    </td>
                                    <td align="center">
                                        <asp:Button ID="btnOrgRight" runat="server" OnClick="btnOrgRight_Click" Text="＞" />
                                        <br />
                                        <asp:Button ID="ButtonbtnOrgLeft" runat="server" OnClick="btnOrgLeft_Click" Text="＜" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: center;">
                                        <asp:Button ID="btnOrgRightSave" runat="server" Text="儲存" Width="100px" OnClick="btnOrgRightSave_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="divRolePrivilege" runat="server" Visible="false">
                            <table>
                                <tr>
                                    <td align="left" style="font-weight: bold; font-size: 13px; font-family: Arial">
                                        可增加名單
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td align="left" style="font-weight: bold; font-size: 13px; font-family: Arial">
                                        已增加名單
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-size: small">
                                        <asp:ListBox ID="libOrgPrivilegeLeft" runat="server" Rows="10" Width="300px" Height="300px" SelectionMode="Multiple">
                                        </asp:ListBox>
                                    </td>
                                    <td align="center">
                                        <asp:Button ID="btnOrgPrivilegeRight" runat="server" OnClick="btnOrgPrivilegeRight_Click"
                                            Text="＞" />
                                        <br />
                                        <asp:Button ID="btnOrgPrivilegeLeft" runat="server" OnClick="btnOrgPrivilegeLeft_Click"
                                            Text="＜" />
                                    </td>
                                    <td style="font-size: small">
                                        <asp:ListBox ID="libOrgPrivilegeRight" runat="server" Height="300px" ondblclick="__doPostBack('libOrgPrivilegeLeft','');"
                                            Rows="10" Width="300px" SelectionMode="Multiple"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: center;">
                                        <asp:Button ID="btnOrgPrivilegeRightSave" runat="server" Text="儲存" Width="100px"
                                            OnClick="btnOrgPrivilegeRightSave_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="tabs-3">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:LinkButton ID="lkSystemFunc" runat="server" OnClick="lkSystemFunc_Click">功能權限管理</asp:LinkButton>
                        <span id="divSystemFuncSubTitle" runat="server" visible="false">&nbsp;>&nbsp;<asp:HyperLink
                            ID="hlSystemFunc" runat="server" Target="_blank"></asp:HyperLink>
                            <asp:HiddenField ID="hidFuncId" runat="server" />
                            <asp:HiddenField ID="hidFuncDisplayName" runat="server" />
                        </span>
                        <asp:Panel ID="divFuncSearch" runat="server">
                            <asp:TextBox ID="txtFuncNameSearch" runat="server" Width="200px"></asp:TextBox>&nbsp;
                            <asp:Button ID="btnFuncSearch" runat="server" Text="搜尋" OnClick="btnFuncSearch_Click" />
                            <br />
                            <asp:GridView ID="gvFunctionList" runat="server" OnRowDataBound="gvFunctionList_RowDataBound"
                                OnRowCommand="gvFunctionList_RowCommand" ShowHeader="false" Width="600px" AutoGenerateColumns="false"
                                GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None"
                                BorderColor="#DEDFDE" Font-Size="Small" CssClass="GridHover">
                                <RowStyle CssClass="HoverRow" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlFuncName" runat="server" Target="_blank"></asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="70%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lkEdit" runat="server" Text="編輯" CommandName="UPD" CommandArgument="<%# ((SystemFunction)Container.DataItem).Id %>"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="30%" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <uc1:Pager ID="ucFuncPager" runat="server" PageSize="20" ongetcount="GetFuncCount"
                                onupdate="FuncUpdateHandler" style="width: 400px"></uc1:Pager>
                        </asp:Panel>
                        <asp:Panel ID="divSystemFuncDetail" runat="server" Visible="false">
                            <table>
                                <tr>
                                    <td align="left" style="font-weight: bold; font-size: 13px; font-family: Arial">
                                        [群組]可增加名單
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td align="left" style="font-weight: bold; font-size: 13px; font-family: Arial">
                                        [群組]已增加名單
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtSysOrgPrivilegeLeftSearch" runat="server" Width="480px"></asp:TextBox>
                                        <asp:ImageButton ID="btnSysOrgPrivilegeLeftSearch" runat="server" ImageUrl="~/Themes/default/images/17Life/B5/b_search.jpg"
                                            OnClick="btnSysOrgPrivilegeLeftSearch_Click" />
                                    </td>
                                    <td>
                                    </td>
                                    <td rowspan="2" style="font-size: small">
                                        <asp:ListBox ID="libSysOrgPrivilegeRight" runat="server" Height="300px" ondblclick="__doPostBack('libSysOrgPrivilegeLeft','');"
                                            Rows="10" Width="500px" SelectionMode="Multiple"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-size: small">
                                        <asp:ListBox ID="libSysOrgPrivilegeLeft" runat="server" Rows="10" Width="500px" Height="265px" SelectionMode="Multiple">
                                        </asp:ListBox>
                                    </td>
                                    <td align="center">
                                        <asp:Button ID="btnSysOrgPrivilegeRight" runat="server" OnClick="btnSysOrgPrivilegeRight_Click"
                                            Text="＞" />
                                        <br />
                                        <asp:Button ID="ButtonbtnSysOrgPrivilegeLeft" runat="server" OnClick="btnSysOrgPrivilegeLeft_Click"
                                            Text="＜" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: center;">
                                        <asp:Button ID="btnSysOrgPrivilegeRightSave" runat="server" Text="儲存" Width="100px"
                                            OnClick="btnSysOrgPrivilegeRightSave_Click" />
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table>
                                <tr>
                                    <td align="left" style="font-weight: bold; font-size: 13px; font-family: Arial">
                                        [員工]可增加名單
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td align="left" style="font-weight: bold; font-size: 13px; font-family: Arial">
                                        [員工]已增加名單
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtSysPrivilegeLeftSearch" runat="server" Width="480px"></asp:TextBox>
                                        <asp:ImageButton ID="btnSysPrivilegeLeftSearch" runat="server" ImageUrl="~/Themes/default/images/17Life/B5/b_search.jpg"
                                            OnClick="btnSysPrivilegeLeftSearch_Click" OnClientClick="return CheckSearchData(this);" />
                                    </td>
                                    <td>
                                    </td>
                                    <td rowspan="2" style="font-size: small">
                                        <asp:ListBox ID="libSysPrivilegeRight" runat="server" Height="300px" ondblclick="__doPostBack('libSysPrivilegeLeft','');"
                                            Rows="10" Width="500px" SelectionMode="Multiple"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="font-size: small">
                                        <asp:ListBox ID="libSysPrivilegeLeft" runat="server" Rows="10" Width="500px" Height="265px" SelectionMode="Multiple">
                                        </asp:ListBox>
                                    </td>
                                    <td align="center">
                                        <asp:Button ID="btnSysPrivilegeRight" runat="server" OnClick="btnSysPrivilegeRight_Click"
                                            Text="＞" />
                                        <br />
                                        <asp:Button ID="ButtonbtnSysPrivilegeLeft" runat="server" OnClick="btnSysPrivilegeLeft_Click"
                                            Text="＜" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: center;">
                                        <asp:Button ID="btnSysPrivilegeRightSave" runat="server" Text="儲存" Width="100px"
                                            OnClick="btnSysPrivilegeRightSave_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="tabs-4">
                <asp:Literal ID="litTabRenewMessage" runat="server" EnableViewState="false" Mode="PassThrough" />
                <asp:Button ID="btn_Renew" runat="server" Text="更新" OnClick="Renew" />
                <asp:CheckBox ID="chkSyncAllServers" runat="server" Text="嘗試同步所有伺服器" style="margin-left:8px" />
            </div>
        </div>
    </div>
</asp:Content>

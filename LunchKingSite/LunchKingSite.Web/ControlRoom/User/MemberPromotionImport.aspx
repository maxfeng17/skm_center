﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="MemberPromotionImport.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.User.MemberPromotionImport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript">
        function CheckData(obj) {
            if ($('[id*=txtName]').val() == '') {
                alert('請輸入紅利名稱!!');
                return false;
            }
            if ($('[id*=txtAmount]').val() == '') {
                alert('請輸入紅利金額!!');
                return false;
            }
            if ($('[id*=txtStartDate]').val() == '' || $('[id*=txtEndDate]').val() == '') {
                alert('請輸入紅利使用期限!!');
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <fieldset>
        <legend>紅利批次匯入</legend>
        <table>
            <tr>
                <td>有效期限:</td>
                <td>
                    <asp:TextBox ID="txtStartDate" runat="server" Columns="8" />
                    <asp:DropDownList ID="ddlStartTime" runat="server"></asp:DropDownList>&nbsp;:
                    <asp:DropDownList ID="ddlStartMin" runat="server"></asp:DropDownList>
                    <cc1:CalendarExtender ID="ceSD" TargetControlID="txtStartDate" runat="server" Format="yyyy/MM/dd">
                    </cc1:CalendarExtender>
                    至
                    <asp:TextBox ID="txtEndDate" runat="server" Columns="8" />
                    <asp:DropDownList ID="ddlEndTime" runat="server"></asp:DropDownList>&nbsp;:
                    <asp:DropDownList ID="ddlEndMin" runat="server"></asp:DropDownList>
                    <cc1:CalendarExtender ID="ceED" TargetControlID="txtEndDate" runat="server" Format="yyyy/MM/dd">
                    </cc1:CalendarExtender>
                </td>
            </tr>
            <tr>
                <td>金額:</td>
                <td>
                    <asp:TextBox ID="txtAmount" runat="server" MaxLength="5" Width="60px"></asp:TextBox>元&nbsp;&nbsp;<span style="font-size: small">(系統會自動換算成點數為1:10)</span>
                </td>
            </tr>
            <tr>
                <td>名稱:</td>
                <td>
                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnImport" runat="server" Text="匯入" OnClick="btnImport_Click" />
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </fieldset>
</asp:Content>

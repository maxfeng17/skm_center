﻿using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.User
{
    public partial class MemberPromotionImport : RolePage, IMemberPormotionImportView
    {
        #region prop

        private MemberPormotionImportPresenter _presenter;
        public MemberPormotionImportPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }

        public string ActionName
        {
            get
            {
                return txtName.Text;
            }
        }

        public double Amount
        {
            get
            {
                double amt = double.TryParse(txtAmount.Text, out amt) ? amt : 0;
                return amt;
            }
        }

        public DateTime StartTime
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtStartDate.Text, out date);
                int hour = int.TryParse(ddlStartTime.SelectedValue, out hour) ? hour : 0;
                int min = int.TryParse(ddlStartMin.SelectedValue, out min) ? min : 0;
                return new DateTime(date.Year, date.Month, date.Day, hour, min, 0);
            }
        }

        public DateTime EndTime
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtEndDate.Text, out date);
                int hour = int.TryParse(ddlEndTime.SelectedValue, out hour) ? hour : 0;
                int min = int.TryParse(ddlEndMin.SelectedValue, out min) ? min : 0;
                return new DateTime(date.Year, date.Month, date.Day, hour, min, 0);
            }
        }

        #endregion

        #region event

        public event EventHandler<DataEventArgs<List<int>>> Import;

        #endregion


        #region method

        public void ShowMessage(string msg)
        {
            lblMessage.Text = msg;
        }

        #endregion

        #region page event

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IntialControls();
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                List<int> users = new List<int>();
                using (StreamReader reader = new StreamReader(FileUpload1.FileContent))
                {
                    do
                    {
                        string textLine = reader.ReadLine();
                        int userId = int.TryParse(textLine, out userId) ? userId : 0;
                        users.Add(userId);
                    } while (reader.Peek() != -1);
                    reader.Close();
                }

                if (this.Import != null)
                {
                    this.Import(this, new DataEventArgs<List<int>>(users));
                }
            }
        }

        #endregion

        #region private method

        private void IntialControls()
        {
            List<ListItem> hours = new List<ListItem>();
            for (int i = 0; i < 24; i++)
            {
                string hour = ("00" + i).Substring(("00" + i).Length - 2, 2);
                hours.Add(new ListItem(hour, hour));
            }

            ddlStartTime.DataSource = hours;
            ddlStartTime.DataBind();
            ddlStartTime.SelectedValue = "00";

            ddlEndTime.DataSource = hours;
            ddlEndTime.DataBind();
            ddlEndTime.SelectedValue = "00";

            List<ListItem> mins = new List<ListItem>();
            for (int i = 0; i < 60; i++)
            {
                string min = ("00" + i).Substring(("00" + i).Length - 2, 2);
                mins.Add(new ListItem(min, min));
            }
            ddlStartMin.DataSource = mins;
            ddlStartMin.DataBind();
            ddlStartMin.SelectedValue = "00";

            ddlEndMin.DataSource = mins;
            ddlEndMin.DataBind();
            ddlEndMin.SelectedValue = "00";
        }

        #endregion
    }
}
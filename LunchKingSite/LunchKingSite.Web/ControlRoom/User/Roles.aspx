<%@ Page Language="C#"  AutoEventWireup="true" MasterPageFile="../backend.master" Inherits="AdminUsers_admin_roles" Title="群組管理" Codebehind="Roles.aspx.cs" %>
<%@ Register Assembly="System.Web.Extensions"
    Namespace="System.Web.UI" TagPrefix="aspajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
function CheckDeletable(numElement)
{
    if (numElement > 0)
    {
        alert('尚有會員在此群組, 不可刪除');
        return false;
    }
    else
        return true;
}
</script>
<table>
    <tr><td>
    <aspajax:UpdatePanel ID="UpdatePanelManageRoles" runat="server">
    <ContentTemplate>
<asp:GridView id="GridViewRole" runat="server" GridLines="None" ForeColor="#333333" Width="100%" HorizontalAlign="Center" AllowPaging="True" CellPadding="4" DataKeyNames="RoleName" DataSourceID="ObjectDataSourceRoleObject" AutoGenerateColumns="False" OnRowDeleting="GridViewRole_OnRowDeleting">
            <Columns>
                <asp:BoundField DataField="RoleName" ReadOnly="True" HeaderText="群組名稱"
                    SortExpression="RoleName" />           
                <asp:BoundField DataField="NumberOfUsersInRole" HeaderText="此群組會員數"
                    SortExpression="NumberOfUsersInRole" />
                <asp:CheckBoxField DataField="UserInRole" HeaderText="UserInRole" Visible="False" SortExpression="UserInRole" />
                <asp:TemplateField><ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Delete" Text="刪除" OnClientClick='<%#Eval("NumberOfUsersInRole", "return CheckDeletable({0});") %>'></asp:LinkButton>
                </ItemTemplate></asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView> <asp:CheckBox id="CheckBoxShowRolesAssigned" runat="server" Text="只顯示相關群組" AutoPostBack="True"></asp:CheckBox> 
</ContentTemplate>
    <Triggers>
    <aspajax:AsyncPostBackTrigger ControlID="bAddRole" />
</Triggers>
    </aspajax:UpdatePanel>
    
    <asp:ObjectDataSource ID="ObjectDataSourceRoleObject" runat="server" SelectMethod="GetRoles"
                TypeName="MembershipUtilities.RoleDataObject" InsertMethod="Insert" DeleteMethod="Delete">
                <SelectParameters>
                    <asp:Parameter DefaultValue="" Name="UserName" Type="String" ConvertEmptyStringToNull="true" />
                    <asp:ControlParameter ControlID="CheckBoxShowRolesAssigned" Name="ShowOnlyAssignedRolls"
                        PropertyName="Checked" Type="Boolean" />
                </SelectParameters>
                <InsertParameters>
                    <asp:Parameter Name="RoleName" Type="String" />
                </InsertParameters>
                <DeleteParameters>
                    <asp:Parameter Name="RoleName" Type="String" />
                </DeleteParameters>
            </asp:ObjectDataSource>
    </td></tr>            
    <tr><td>
    <br /><br /><br />
        <asp:Label ID="lAddRole" runat="server" Text="增加新群組: "></asp:Label>
        <asp:TextBox ID="tbAddRole" runat="server"></asp:TextBox>
        <asp:Button ID="bAddRole" runat="server" Text="增加" OnClick="bAddRole_Click" />
    </td></tr>
</table>    
</asp:Content>

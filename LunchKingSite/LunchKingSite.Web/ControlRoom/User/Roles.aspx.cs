using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LunchKingSite.Core.UI;

public partial class AdminUsers_admin_roles : RolePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void bAddRole_Click(object sender, EventArgs e)
    {
        if (tbAddRole.Text.Length > 0)
        {
            ObjectDataSourceRoleObject.InsertParameters["RoleName"].DefaultValue = tbAddRole.Text; ;
            ObjectDataSourceRoleObject.Insert();
            GridViewRole.DataBind();
            tbAddRole.Text = "";
        }
    }

    protected void GridViewRole_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        e.Cancel = true;
    }

}

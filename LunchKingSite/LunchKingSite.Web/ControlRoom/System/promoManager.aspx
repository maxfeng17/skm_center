﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="promoManager.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.promoManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript">
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>
        行銷Promo頁管理後台</h3>
    P好康&nbsp;&nbsp;www.17life.com/ppon/promo.aspx?cn=
    <asp:TextBox ID="txtCn" runat="server"></asp:TextBox>
    <asp:Button ID="btnPromo" Text="產生promo頁" runat="server" OnClick="btnPromo_Click"/>
    <br />
    品生活&nbsp;&nbsp;www.17life.com/piinlife/promo.aspx?cn=
    <asp:TextBox ID="txtPiinlifeCn" runat="server"></asp:TextBox>
    <asp:Button ID="btnPiinlifePromo" Text="產生promo頁" runat="server" OnClick="btnPiinlifePromo_Click"/>
    <br />
    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Visible="False" />
    <br />
    <asp:GridView ID="gvPromoList" runat="server" Width="800px" AutoGenerateColumns="False"
        OnRowDataBound="gvPromoList_RowDataBound" OnRowCommand="gvPromoList_RowCommand">
        <Columns>
            <asp:BoundField DataField="ContentId" HeaderText="id" ReadOnly="True">
                <ItemStyle Width="10%" HorizontalAlign="Center"/>
            </asp:BoundField>
            <asp:BoundField DataField="CreatedOn" HeaderText="建立日期" ReadOnly="True" DataFormatString="{0:yyyy/MM/dd HH:mm}">
                <ItemStyle Width="15%" HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="網址參數">
                <ItemTemplate>
                    <asp:Label ID="lblCn" runat="server"></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="15%" HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="編輯器連結">
                <ItemTemplate>
                    <asp:HyperLink ID="hkConman" runat="server" Target="_blank"></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle Width="25%" HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HyperLink ID="hkPromo" runat="server" Target="_blank"></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle Width="10%" HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:BoundField DataField="CreatedBy" HeaderText="建立人員" ReadOnly="True" Visible="false">
                <ItemStyle Width="15%" HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="btnDelete" Text="刪除" runat="server" CommandName="d" OnClientClick="return confirm('確定刪除?');" CommandArgument='<%#Eval("ContentId")%>'></asp:LinkButton>
                </ItemTemplate>
                <ItemStyle Width="5%" HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>

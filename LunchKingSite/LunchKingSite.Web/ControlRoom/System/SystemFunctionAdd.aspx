﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="SystemFunctionAdd.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.SystemFunctionAdd" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Src="~/ControlRoom/Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script src="../../Tools/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            SetCustomeType();
            CheckFuncType();
        });
        function DataCheck() {
            var count = $('[id*=chklFuncType]').find(':checkbox').filter(function () { return $(this).attr('checked') }).length;
            if (count == 0 && $('[id*=txtFuncType]').val() == '') {
                alert('未設定功能類型!!');
                return false;
            } else if ($('[id*=chkIsVisible]').is(':checked')) {
                return confirm('是否確定顯示於左方功能列表?');
            }
        }
        function SetCustomeType() {
            if ($.trim($('[id*=txtFuncType]').val()) != '') {
                $('#CustomeTypeDesc').show();
            } else {
                $('#CustomeTypeDesc').hide();
            }
        }
        function CheckFuncType() {
            $('input[id*=chklFuncType]').each(function () {
                if ($(this).attr('disabled') != 'disabled' && $(this).next().text() == '瀏覽') {
                    if ($(this).is(':checked')) {
                        $('[id*=chkIsVisible]').attr('checked', 'checked');
                    } else {
                        $('[id*=chkIsVisible]').attr('checked', false);
                    }
                } 
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>
        功能權限管理</h3>
    <fieldset style="width: 410px; font-size: small">
        <legend>功能/權限查詢</legend>
        <asp:Panel ID="divSearch" runat="server" DefaultButton="btnSearch">
            顯示名稱：<asp:TextBox ID="txtDisplayNameSearch" runat="server"></asp:TextBox><br />
            頁面名稱：<asp:TextBox ID="txtFuncNameSearch" runat="server"></asp:TextBox><br />
            群組名稱：<asp:TextBox ID="txtParentFuncSearch" runat="server"></asp:TextBox>
            &nbsp;&nbsp;<asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="搜尋" />
        </asp:Panel>
    </fieldset>
    <asp:Button ID="btnFunctionAdd" runat="server" OnClick="btnFunctionAdd_Click" Text="新增功能/權限" />
    <asp:Panel ID="divFunctionAdd" runat="server" Visible="false">
        <fieldset style="width: 740px; font-size: small">
            <legend>新增權限</legend>
            <table style="font-size: small">
                <tr id="FuncName" runat="server">
                    <td style="text-align: right; width: 80px">
                        頁面名稱：
                    </td>
                    <td>
                        <asp:HiddenField ID="hidId" runat="server" />
                        <asp:TextBox ID="txtFuncName" runat="server" Width="250px"></asp:TextBox>&nbsp;sort
                        <asp:TextBox ID="txtSortOrder" runat="server" Width="50px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        功能類型：
                    </td>
                    <td>
                        <asp:CheckBoxList ID="chklFuncType" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" onclick="CheckFuncType();">
                            <asp:ListItem Text="瀏覽" Value="Read"></asp:ListItem>
                            <asp:ListItem Text="建立" Value="Create"></asp:ListItem>
                            <asp:ListItem Text="更新" Value="Update"></asp:ListItem>
                            <asp:ListItem Text="刪除" Value="Delete"></asp:ListItem>
                        </asp:CheckBoxList>
                        &nbsp;&nbsp;&nbsp;&nbsp; 自訂：<asp:TextBox ID="txtFuncType" runat="server" onkeyup="SetCustomeType();"
                            Width="150px"></asp:TextBox>
                        &nbsp; <span id="CustomeTypeDesc" style="display: none;">描述：<asp:TextBox ID="txtTypeDesc"
                            runat="server"></asp:TextBox></span>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        頁面群組名稱：
                    </td>
                    <td>
                        <asp:TextBox ID="txtParentFunc" runat="server" Width="250px"></asp:TextBox>
                        <span id="ParentFuncName" runat="server">&nbsp;sort
                            <asp:TextBox ID="txtParentSortOrder" runat="server" Width="50px"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        Link：
                    </td>
                    <td>
                        <asp:TextBox ID="txtLink" runat="server" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        顯示名稱：
                    </td>
                    <td>
                        <asp:TextBox ID="txtDisplayName" runat="server" Width="250px"></asp:TextBox>&nbsp;
                        <asp:CheckBox ID="chkIsVisible" runat="server" Text="顯示於功能列表" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        EIP功能表：
                    </td>
                    <td>
                        <asp:CheckBox ID="chkEIPUsed" runat="server" Text="EIP使用" />&nbsp;<asp:TextBox ID="txtMenuIcon" runat="server" Width="250px" placeholder="icon"></asp:TextBox><a href="http://fortawesome.github.io/Font-Awesome/3.2.1/icons/" target="_blank">參考連結</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="儲存" OnClientClick="return DataCheck();" />
                        <asp:Button ID="btnDelete" runat="server" OnClick="btnDelete_Click" Text="刪除" OnClientClick="return confirm('確定要刪除此筆資料??')" />
                        <asp:Button ID="btnClose" runat="server" OnClick="btnClose_Click" Text="關閉" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <asp:GridView ID="gvFunctionList" runat="server" OnRowCommand="gvFunctionList_RowCommand"
        GridLines="Horizontal" ForeColor="Black" CellPadding="1" BorderWidth="1px" BorderStyle="None"
        BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="False"
        Font-Size="Small" Width="1200px">
        <FooterStyle BackColor="#CCCC99"></FooterStyle>
        <RowStyle BackColor="#F7F7DE" Height="50px"></RowStyle>
        <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
        <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="30px">
        </HeaderStyle>
        <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
        <Columns>
            <asp:BoundField DataField="Id" HtmlEncode="False" HeaderText="Id">
                <ItemStyle HorizontalAlign="Center" Width="3%" />
            </asp:BoundField>
            <asp:BoundField DataField="FuncName" HeaderText="頁面名稱">
                <ItemStyle HorizontalAlign="Center" Width="7%" />
            </asp:BoundField>
            <asp:BoundField DataField="SortOrder" HeaderText="排序">
                <ItemStyle HorizontalAlign="Center" Width="3%" />
            </asp:BoundField>
            <asp:BoundField DataField="Link" HeaderText="Link">
                <ItemStyle HorizontalAlign="Left" Width="20%" />
            </asp:BoundField>
            <asp:BoundField ReadOnly="True" DataField="DisplayName" HeaderText="顯示名稱">
                <ItemStyle HorizontalAlign="Center" Width="10%" />
            </asp:BoundField>
            <asp:BoundField ReadOnly="True" DataField="FuncType" HeaderText="功能名稱">
                <ItemStyle HorizontalAlign="Center" Width="5%" />
            </asp:BoundField>
            <asp:BoundField ReadOnly="True" DataField="TypeDesc" HeaderText="功能描述">
                <ItemStyle Width="12%" HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField ReadOnly="True" DataField="ParentFunc" HeaderText="頁面群組名稱">
                <ItemStyle HorizontalAlign="Center" Width="8%" />
            </asp:BoundField>
            <asp:BoundField DataField="ParentSortOrder" HeaderText="群組排序">
                <ItemStyle HorizontalAlign="Center" Width="4%" />
            </asp:BoundField>
            <asp:BoundField ReadOnly="True" DataField="ModifyId" HeaderText="修改人員">
                <ItemStyle HorizontalAlign="Center" Width="8%" />
            </asp:BoundField>
            <asp:BoundField ReadOnly="True" DataField="ModifyTime" DataFormatString="{0:yyyy/MM/dd HH:mm}"
                HtmlEncode="False" HeaderText="修改日期">
                <ItemStyle Width="9%" HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="lkEdit" runat="server" Text="修改" CommandName="UPD" CommandArgument="<%# ((SystemFunction)Container.DataItem).Id %>"></asp:LinkButton>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" Width="4%" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <uc1:Pager ID="ucPager" runat="server" PageSize="20" ongetcount="GetCount" onupdate="UpdateHandler">
    </uc1:Pager>
</asp:Content>

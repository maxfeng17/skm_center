﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="conman.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.conman" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
<script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="../../Tools/js/json2.js"></script>
<script src="//www.google.com/jsapi" type="text/javascript"></script>  
<script type="text/javascript">
    google.load("jquery", "1.4.2");
    google.load("jqueryui", "1.8");
</script>
<link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />	

<style type="text/css">
img 
{
    border: 0px;
}

table.content {
  width:100%;
}
table.content .hd0, table.content .col0 {
  width:120px;
  border-bottom: solid 1px orange;
}
table.content .hd1, table.content .col1 {
  width:auto;
  border-bottom: solid 1px orange;
  word-break: break-all;
}
table.content .hd2, table.content .col2 {
  width:80px;
  border-bottom: solid 1px orange;
}
table.content .hd3, table.content .col3 {
  width:80px;
  border-bottom: solid 1px orange;
}
table.content .hd4, table.content .col4 {
  width:80px;
  border-bottom: solid 1px orange;
}
table.content .col2 {
    font-size:11px;
}
table.content .col3 {
    font-size:11px;
}
</style>

<script type="text/javascript">
    google.setOnLoadCallback(function () {
        $(document).ready(function () {
            CKEDITOR.replace('<%=tc.ClientID%>');
        });
    });
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div><asp:Literal ID="litMessage" runat="server" EnableViewState="false" /></div>
    <asp:PlaceHolder ID="pl" runat="server" Visible="false">
        <asp:Repeater ID="rs" runat="server" OnItemCommand="rs_OnItemCommand">
            <HeaderTemplate>
                <table class="content">
                    <thead>
                    <tr>
                        <th class="hd0">標題</th>
                        <th class="hd1">標的位址</th>
                        <th class="hd2">修改時間</th>
                        <th class="hd3">建立時間</th>
                        <th class="hd4">功能</th>
                    </tr>
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>                
                <tr>
                    <td class="col0"><a href="?id=<%#Eval("ContentId")%>"><%#Eval("Title")%></a></td>
                    <td class="col1"><a href=""><img src="../../images/icons/image.gif" alt="preview" /></a> <a href="?id=<%#Eval("ContentId")%>"><%#Eval("ContentName")%></a></td>
                    <td class="col2"><%#Eval("ModifiedOn", "{0:yyyy/MM/dd HH:mm:ss}")%></td>
                    <td class="col3"><%#Eval("CreatedOn", "{0:yyyy/MM/dd HH:mm:ss}")%></td>
                    <td class="col4"><asp:Button ID="clearButton" runat="server" Text="清空暫存" CommandName="ClearCache" CommandArgument="<%# ((CmsContent)(Container.DataItem)).ContentName %>"/></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="ps" runat="server" Visible="false">
        <dl>
            <dt>標題</dt>
                <dd><asp:TextBox ID="tt" runat="server" /></dd>
            <dt>SEO description</dt>
                <dd><asp:TextBox ID="tSeoD" runat="server" Width="600" /></dd>
            <dt>SEO KeyWords</dt>
                <dd><asp:TextBox ID="tSeoK" runat="server" Width="600" /></dd>
            <dt>標的</dt>
                <dd><asp:Literal ID="lc" runat="server" /></dd>
            <dt>內容</dt>
                <dd><asp:TextBox ID="tc" runat="server" TextMode="MultiLine" /></dd>
            <dt>建立者</dt>
                <dd><asp:Literal ID="lcb" runat="server" /></dd>
            <dt>建立時間</dt>
                <dd><asp:Literal ID="ct" runat="server" /></dd>
            <dt>修改者</dt>
                <dd><asp:Literal ID="lm" runat="server" /></dd>
            <dt>修改時間</dt>
                <dd><asp:Literal ID="lmt" runat="server" /></dd>
        </dl>
        <fieldset>
            <a href="conman.aspx">返回</a>
            <asp:Button ID="bs" runat="server" Text="Save" OnClick="bs_Click" />
        </fieldset>
    </asp:PlaceHolder>
</asp:Content>

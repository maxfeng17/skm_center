﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using log4net.Core;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Facade;
using Newtonsoft.Json;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class conman : RolePage, IContentManageView
    {
        protected static ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(conman));
        public event EventHandler<DataEventArgs<ContentManageSave>> SaveContent;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                _presenter.OnViewInitialized();
            _presenter.OnViewLoaded();
        }

        private ContentManagePresenter _presenter;
        public ContentManagePresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public int? ContentId
        {
            get
            {
                int id;
                if (string.IsNullOrEmpty(Request["id"]) || !int.TryParse(Request["id"], out id))
                    return null;
                return id;
            }
        }

        public string UserName { get { return User.Identity.Name; } }

        public void SetContentList(CmsContentCollection contents)
        {
            pl.Visible = true;
            rs.DataSource = contents;
            rs.DataBind();
        }

        public void SetContent(CmsContent content, PromoSeoKeyword seo)
        {
            ps.Visible = true;
            tt.Text = content.Title;
            lc.Text = content.ContentName;
            tc.Text = content.Body;
            lcb.Text = content.CreatedBy;
            ct.Text = content.CreatedOn.HasValue ? content.CreatedOn.Value.ToString("yyyy/MM/dd HH:mm:ss") : "";
            lm.Text = content.ModifiedBy;
            lmt.Text = content.ModifiedOn.HasValue ? content.ModifiedOn.Value.ToString("yyyy/MM/dd HH:mm:ss") : "";

            tSeoD.Text = seo.Description;
            tSeoK.Text = seo.Keyword;
        }

        public string PreviewContent(int id)
        {
            throw new NotImplementedException();
        }

        public void ClearCacheDependency(string key)
        {
            //Cache.Insert(key, DateTime.Now.Ticks, null, DateTime.MaxValue, TimeSpan.Zero, System.Web.Caching.CacheItemPriority.NotRemovable, null);
            //int pageIdx = key.IndexOf(".aspx");
            //if (pageIdx > 0)
            //    HttpResponse.RemoveOutputCacheItem(key.Substring(0, pageIdx + 5));
        }

        protected void bs_Click(object sender, EventArgs e)
        {
            ContentManageSave savedata = new ContentManageSave();
            savedata.Title = tt.Text;
            savedata.SeoDescription = tSeoD.Text;
            savedata.SeoKeyWords = tSeoK.Text;
            savedata.UserID = Page.User.Identity.Name;
            savedata.PromoContent = HttpUtility.HtmlDecode(tc.Text);
            if (SaveContent != null)
            {
                SaveContent(this, new DataEventArgs<ContentManageSave>(savedata));
            }
                
        }

        protected void rs_OnItemCommand(object source, RepeaterCommandEventArgs e)
        {
                if (e.CommandName == "ClearCache")
                {
                    var key = e.CommandArgument.ToString().ToLower();                    
                   

                    if (Cache[key] != null)
                    {
                        Cache.Remove(key);
                    }

                using (var client = new WebClient())
                {
                    var hostList = CommonFacade.GetWebServerHostList();
                    List<ApiResult> apiResults = new List<ApiResult>();
                    StringBuilder sb = new StringBuilder();
                    foreach (var siteUrl in hostList)
                    {
                        string apiUrl = "";
                        try
                        {
                            apiUrl = string.Format("http://{0}/api/promo/RemoveParagraphCache?AccessToken={1}&userName={2}&contentName={3}"
                                , siteUrl, cp.Web17LifeAccessToken, User.Identity.Name, key);
                            client.Encoding = Encoding.UTF8;
                            var result = JsonConvert.DeserializeObject<ApiResult>(client.DownloadString(apiUrl));
                            apiResults.Add(result);
                            if (result.Code == ApiResultCode.Success)
                            {
                                if (sb.Length > 0)
                                {
                                    sb.Append(", ");
                                }
                                sb.Append(siteUrl);
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Info("get " + apiUrl + " error, ex=" + ex);
                        }
                    }
                    if (sb.Length > 0)
                    {
                        sb.Insert(0, "更新: ");
                    }
                    litMessage.Text = sb.ToString();
                }
            }
        }
    }
}
﻿using LunchKingSite.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class ElmahLogList : RolePage, IElmahLogListView
    {
        #region property
        private ElmahLogListPresenter _presenter;
        public ElmahLogListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public int PageCount
        {
            get
            {
                int pagecount;
                return int.TryParse((ViewState["pagecount"] == null ? string.Empty : ViewState["pagecount"].ToString()), out pagecount) ? pagecount : 0;
            }
            set
            {
                ViewState["pagecount"] = value;
            }
        }

        public string Source
        {
            get
            {
                return tbx_Source.Text;
            }
        }

        public DateTime? StartDate
        {
            get
            {
                DateTime startdate;
                if (DateTime.TryParse(tbx_StartDate.Text, out startdate))
                {
                    return startdate.ToUniversalTime();
                }
                else
                {
                    return null;
                }
            }
        }

        public DateTime? EndDate
        {
            get
            {
                DateTime enddate;
                if (DateTime.TryParse(tbx_EndDate.Text, out enddate))
                {
                    return enddate.ToUniversalTime();
                }
                else
                {
                    return null;
                }
            }
        }
        #endregion

        #region event
        public event EventHandler<DataEventArgs<int>> PageChange = null;
        public event EventHandler Search = null;
        #endregion

        #region method
        public void GetElmahLogList(ElmahErrorCollection data)
        {
            lab_NoData.Visible = !(data.Count > 0);
            pan_ElmahLog.Visible = !lab_NoData.Visible;
            rpt_ElmahLog.DataSource = data;
            rpt_ElmahLog.DataBind();
        }

        public void SetUpPageCount()
        {
            ucPager.ResolvePagerView(PageCount > 0 ? 1 : 0, true);
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void ElmahLogSearch(object sender, EventArgs e)
        {
            if (this.Search != null)
            {
                Search(sender, e);
            }
        }


        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChange != null)
            {
                this.PageChange(this, new DataEventArgs<int>(pageNumber));
            }
        }

        protected int RetrieveElmahLogCount()
        {
            return PageCount;
        }
        #endregion
    }
}
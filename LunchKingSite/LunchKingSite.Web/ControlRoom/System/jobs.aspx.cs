using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core.UI;
using System.Net;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class jobs : RolePage, IJobsView
    {
        public event EventHandler PlayPauseAll;
        public event EventHandler<DataEventArgs<string>> TimerTick;
        public event EventHandler<DataEventArgs<string>> Fire;
        public event EventHandler<DataEventArgs<string>> PlayPause;
        public event EventHandler<DataEventArgs<string>> Remove;

        #region Properties
        private JobsPresenter _presenter;
        public JobsPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string JobName { get { return Request.QueryString["n"]; } }

        public bool IsJobLoadBalanceProceed { get; set; }

        public int Interval { get { return 30000; } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) this.Presenter.OnViewInitialized();
            this.Presenter.OnViewLoaded();
             lab_SingleJob.Text = string.Format("本機Server Name:{0}; SingleJob {1}", Dns.GetHostName(), (IsJobLoadBalanceProceed ? "於本機執行" : "非本機執行"));
        }


        protected void ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is JobContext)
            {
                JobContext jc = (JobContext)e.Item.DataItem;
                int job_balance_mode;
                //判斷是否有設定單一執行或多台server執行
                if (!string.IsNullOrEmpty(jc.Setting.Parameters[SingleOperator.SingleJob.ToString()]) && int.TryParse(jc.Setting.Parameters[SingleOperator.SingleJob.ToString()], out job_balance_mode) && job_balance_mode == (int)JobLoadBalanceMode.Multiple)
                {
                    Label clab_JobLoadBalance = (Label)e.Item.FindControl("lab_JobLoadBalance");
                    clab_JobLoadBalance.Text = "MultipleJobs同步執行";
                }
            }
        }
        #region IJobsView Members
        public void SetListMode(List<JobContext> jobs)
        {
            p1.Visible = true;
            p2.Visible = false;
            rJ.DataSource = jobs;
            rJ.DataBind();
            tm1.Interval = Interval;
        }

        public void SetDetailMode(JobContext job)
        {
            p2.Visible = true;
            p1.Visible = false;
            lN.Text = job.Setting.Name;
            lD.Text = job.Setting.Description;

            int job_balance_mode;
            //判斷是否有設定單一執行或多台server執行
            if (!string.IsNullOrEmpty(job.Setting.Parameters[SingleOperator.SingleJob.ToString()]) && int.TryParse(job.Setting.Parameters[SingleOperator.SingleJob.ToString()], out job_balance_mode) && job_balance_mode == (int)JobLoadBalanceMode.Single)
                lD.Text += "SingleJob: " + (IsJobLoadBalanceProceed ? "本機執行" : "非本機執行");

            lW.Text = job.Setting.Worker != null ? job.Setting.Worker.FullName : "N/A";
            lS.Text = job.Schedule;
            lSt.Text = job.Status.ToString("g");
            lNR.Text = job.NextRun != null ? job.NextRun.Value.ToString() : "Never";
            lLR.Text = job.LastExecution.RunTime != null ? job.LastExecution.RunTime.Value.ToString() : "Never";
            lRD.Text = job.LastExecution.Duration != null ? job.LastExecution.Duration.Value.ToString() : "Never Ran";
            bP.Text = job.Status == JobStatus.Paused ? "Resume" : "Pause";
            tm1.Interval = Interval;
        }
        #endregion

        protected void bP_Click(object sender, EventArgs e)
        {
            if (PlayPause != null)
                PlayPause(null, new DataEventArgs<string>(lN.Text));
        }

        protected void bF_Click(object sender, EventArgs e)
        {
            if (Fire != null)
                Fire(null, new DataEventArgs<string>(lN.Text));
        }

        protected void bR_Click(object sender, EventArgs e)
        {
            if (Remove != null)
                Remove(null, new DataEventArgs<string>(lN.Text));
        }

        protected void tm1_Tick(object sender, EventArgs e)
        {
            lab_SingleJob.Text = string.Format("本機Server Name:{0}; SingleJob {1}", Dns.GetHostName(), (IsJobLoadBalanceProceed ? "於本機執行" : "非本機執行"));
            if (TimerTick != null)
                TimerTick(null, new DataEventArgs<string>(p1.Visible ? "" : lN.Text));
        }

        protected void bPA_Click(object sender, EventArgs e)
        {
            if (PlayPauseAll != null)
                PlayPauseAll(null, null);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class PushTestForMGM : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Random seed = new Random();
            txtMsgId.Text = seed.Next(1, 1000).ToString();
            if (User.Identity.IsAuthenticated == false)
            {
                throw new Exception("請先登入");
            }
            txtUserName.Text = MemberFacade.GetUniqueId(User.Identity.Name).ToString();
        }

        protected void btnTest_Click(object sender, EventArgs e)
        {
            int uniqueId = MemberFacade.GetUniqueId(User.Identity.Name);
            try
            {
                NotificationFacade.PushMemberMessageForMessageCenter(
                    uniqueId, txtMessage.Text, int.Parse(txtCarType.Text), txtMsgId.Text, pushOnlyForTest: true);

                var userDeviceInfo = NotificationFacade.GetDefaultUserDeviceInfoModel(uniqueId);
                litMessage.Text = "測試完成 推到 " + 
                    (userDeviceInfo == null ? string.Empty : userDeviceInfo.Model + "-" + userDeviceInfo.DeviceId);
                litMessage.Text += " " + DateTime.Now.ToString("mm:ss");
            }
            catch (Exception ex)
            {
                litMessage.Text = "測試失敗, ex=" + ex.ToString();
                litMessage.Text += " " + DateTime.Now.ToString("mm:ss");
            }
            
        }
    }    
}
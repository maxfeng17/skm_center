﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="PEZHtmlCombiner.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.PEZHtmlCombiner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript">
        function CombineData() {
            //            
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3>
        Pez行銷用Html編輯器</h3>
    <asp:HyperLink ID="hkEditorLink" runat="server" NavigateUrl="~/PEZHtmlEditer.aspx"
        Target="_blank" Text="Html製作連結"></asp:HyperLink>
    <asp:Panel ID="divEditor" runat="server">
        <table>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                1:
                            </td>
                            <td>
                                <asp:TextBox ID="txt1" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                2:
                            </td>
                            <td>
                                <asp:TextBox ID="txt2" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                3:
                            </td>
                            <td>
                                <asp:TextBox ID="txt3" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                4:
                            </td>
                            <td>
                                <asp:TextBox ID="txt4" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td>
                                5:
                            </td>
                            <td>
                                <asp:TextBox ID="txt5" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                6:
                            </td>
                            <td>
                                <asp:TextBox ID="txt6" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button ID="btnSubmit" runat="server" Text="產生" OnClick="btnSubmit_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                預覽(看太多會長針眼)
                            </td>
                        </tr>
                        <tr>
                            <asp:Label ID="lblResult" runat="server" Text=""></asp:Label>
                        </tr>
                        <tr>
                            <td>
                                <span id="lblOutput"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                請複製以下html原始碼：
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtOutput" runat="server" Height="166px" Width="815px" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="Repeater1_ItemCommand">
                        <HeaderTemplate>
                            <table width="1224px">
                                <tr>
                                    <td>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <span>
                                <table border="1" style="display: inline">
                                    <tr>
                                        <td>
                                            id:<%# Eval("id") %>
                                        </td>
                                        <td>
                                            版位編號:<%# Eval("code") %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            PM:<%# Eval("PM") %>
                                        </td>
                                        <td>
                                            單位:<%# Eval("part") %>--部門:<%# Eval("dept") %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:Button ID="btnDelete" runat="server" Text="刪除" CommandArgument='<%# Eval("id") %>'
                                                OnClientClick="return confirm('確定刪除?');" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:Label ID="lblContent" runat="server" Text='<%# Eval("edmcontent") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </span>
                        </ItemTemplate>
                        <FooterTemplate>
                            </td> </tr> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="divOutput" runat="server">
    </asp:Panel>
</asp:Content>

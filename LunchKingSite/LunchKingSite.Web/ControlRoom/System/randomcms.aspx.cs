﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class randomcms : RolePage, IContentManageRandomView
    {
        #region property

        private ContentManageRandomPresenter _presenter;

        public ContentManageRandomPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        public string ContentName
        {
            get
            {
                return ddl_TargetView.SelectedValue;
            }
        }

        public string ContentName2
        {
            get
            {
                return ddl_Target2.SelectedValue;
            }
        }

        public int Id
        {
            get
            {
                int id;
                if (int.TryParse(hif_ID.Value, out id))
                {
                    return id;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                hif_ID.Value = value.ToString();
            }
        }

        public RandomCmsType Type
        {
            get
            {
                RandomCmsType type;
                if (Enum.TryParse<RandomCmsType>(rbl_RandomCmsType.SelectedValue, out type))
                {
                    return type;
                }
                else
                {
                    return RandomCmsType.PponRandomCms;
                }
            }
        }

        public int ContentId
        {
            get
            {
                int id;
                if (int.TryParse(hif_ContentId.Value, out id))
                {
                    return id;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                hif_ContentId.Value = value.ToString();
            }
        }

        public string Mode
        {
            get
            {
                lbl_City.Visible =  cbx_CityStatus.Visible = btn_EditCity.Visible = btn_Delete.Visible = false;
                ddl_Target2.Visible = ddl_Banners.Visible = ddl_Target2.Visible = cbx_City.Visible = btn_Save2.Visible = true;
                string mode = string.IsNullOrEmpty(hif_Mode.Value) ? "1" : hif_Mode.Value;
                if (mode == "1")
                {
                    btn_Save.Text = "新增";
                }
                else if (mode == "2")
                {
                    btn_Save.Text = "更新";
                    btn_Delete.Visible = true;
                }
                else if (mode == "3")
                {
                    lbl_City.Visible = cbx_CityStatus.Visible = btn_EditCity.Visible = true;
                    ddl_Target2.Visible = ddl_Banners.Visible = ddl_Target2.Visible = cbx_City.Visible = btn_Save2.Visible = false;
                }
                return mode;
            }
            set
            {
                lbl_City.Visible = cbx_CityStatus.Visible = btn_EditCity.Visible = btn_Delete.Visible = false;
                ddl_Target2.Visible = ddl_Banners.Visible = ddl_Target2.Visible = cbx_City.Visible = btn_Save2.Visible = true;
                if (value == "1")
                {
                    btn_Save.Text = "新增";
                }
                else if (value == "2")
                {
                    btn_Save.Text = "更新";
                    btn_Delete.Visible = true;
                }
                else if (value == "3")
                {
                    lbl_City.Visible = cbx_CityStatus.Visible = btn_EditCity.Visible = true;
                    ddl_Target2.Visible = ddl_Banners.Visible = ddl_Target2.Visible = cbx_City.Visible = btn_Save2.Visible = false;
                }
                hif_Mode.Value = value;
            }
        }

        public string UploadType
        {
            get
            {
                return ddl_UploadType.SelectedValue;
            }
        }

        public bool IsCurationUser
        {
            get
            {
                if (!User.IsInRole("Administrator") && CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Curation))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #endregion property

        #region method

        public void SetFileList(List<ImageFile> filelist)
        {
            gv_UploadFile.DataSource = filelist;
            gv_UploadFile.DataBind();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "tab", "changetab(1);", true);
        }

        public void SetUploadTypeDDL()
        {
            List<ListItem> uploadTypeList = new List<ListItem>();
            uploadTypeList.Add(new ListItem("臉書分享小圖", "17P/100X100/"));
            uploadTypeList.Add(new ListItem("網站 Top banner", "17P/17sidebn/"));
            uploadTypeList.Add(new ListItem("網站 Side banner", "17P/17topbn/"));
            uploadTypeList.Add(new ListItem("全家美麗年中慶all", "17P/20130430-FamilyMothersDay/"));
            uploadTypeList.Add(new ListItem("辦活動圖片", "17P/active/"));
            uploadTypeList.Add(new ListItem("福利網網頁1", "easyfree/"));
            uploadTypeList.Add(new ListItem("福利網網頁2", "easyfree/NEWversion/"));
            uploadTypeList.Add(new ListItem("福利網圖片+網頁", "easyfree/NEWversion/images/"));
            uploadTypeList.Add(new ListItem("全家頁面底圖", "fami_mkt/"));
            uploadTypeList.Add(new ListItem("全家浮水印(直式)", "Outside_AD/"));
            uploadTypeList.Add(new ListItem("全家浮水印(橫式)", "Inside_AD/"));
            uploadTypeList.Add(new ListItem("大團購EDM HTML", "promo_new/"));
            DDLBinding(uploadTypeList, "Text", "Value", ddl_SearchUploadType, ddl_UploadType);
        }

        public void GetCmsCollection(CmsRandomContentCollection data)
        {
            gv_ViewData.DataSource = data;
            gv_ViewData.DataBind();
        }

        public void ReturnMessage(string message)
        {
            ClearContent(tbx_Title, tbx_editor_Html, tbx_StartTime, tbx_EndTime, cbx_City);
            int id;
            if (this.ChangeCmsRandom != null)
            {
                this.ChangeCmsRandom(null, new DataEventArgs<string>(ddl_TargetView.ID));
                this.ChangeCmsRandom(null, new DataEventArgs<string>(ddl_Target2.ID));
            }

            if (this.GetCmsRandomContentById != null && int.TryParse(ddl_Banners.SelectedValue, out id))
            {
                this.GetCmsRandomContentById(null, new DataEventArgs<KeyValuePair<int, string>>(new KeyValuePair<int, string>(id, ddl_Banners.ID)));
            }

            if (this.GetCmsRandomContentById != null && int.TryParse(ddl_BannersView.SelectedValue, out id))
            {
                this.GetCmsRandomContentById(null, new DataEventArgs<KeyValuePair<int, string>>(new KeyValuePair<int, string>(id, ddl_BannersView.ID)));
            }

            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + message + "!!');", true);
        }

        public void GetCmsCityById(CmsRandomCity city)
        {
            tbx_EndTime.Text = city.EndTime.ToString("yyyy/MM/dd");
            ddl_EndHour.SelectedValue = city.EndTime.ToString("HH");
            ddl_EndMinute.SelectedValue = city.EndTime.ToString("mm");
            tbx_StartTime.Text = city.StartTime.ToString("yyyy/MM/dd");
            ddl_StartHour.SelectedValue = city.StartTime.ToString("HH");
            ddl_StartMinute.SelectedValue = city.StartTime.ToString("mm");
            lbl_City.Text = city.CityName;
            cbx_CityStatus.Checked = city.Status;
        }

        public void GetCmsById(CmsRandomContent cms)
        {
            tbx_Title.Text = cms.Title;
            tbx_editor_Html.Text = HttpUtility.HtmlDecode(cms.Body);
            ddl_Target.SelectedValue = cms.ContentName;
            cbx_ContentStatus.Checked = cms.Status;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "tab", "changetab(3);", true);
        }

        public void GetCmsContentById(string content)
        {
            lab_Content.Text = content;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ckeditor", "changetab(4);", true);
        }

        public void GetCmsRandomTitleNId(DataTable dt, string ddl)
        {
            if (string.IsNullOrEmpty(ddl))
            {
                DDLBinding(dt, "Title", "Id", ddl_Banners, ddl_BannersView);
            }

            if (ddl == ddl_Target2.ID)
            {
                DDLBinding(dt, "Title", "Id", ddl_Banners);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "tab", "changetab(4);", true);
            }
            else if (ddl == ddl_TargetView.ID)
            {
                DDLBinding(dt, "Title", "Id", ddl_BannersView);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "tab", "changetab(5);", true);
            }
        }

        public void GetCmsRandom(CmsRandomContent content, CmsRandomCityCollection cities)
        {
            if (content.Body == null)
                btn_EditCmsContent.Enabled = false;
            else
                btn_EditCmsContent.Enabled = true;

            lbl_Body.Text = content.Body;
            cbx_Status.Checked = content.Status;
            hif_ContentId.Value = content.Id.ToString();
            btn_EditCmsContent.CommandArgument = content.Id.ToString();
            gv_ViewData.DataSource = cities;
            gv_ViewData.DataBind();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "tab", "changetab(5);", true);


        }

        public void ClearChaheByName(string name)
        {
            //先清本機
            Cache.Remove(name);
            //清其它機器
            string message = SystemFacade.ClearCacheByObjectNameAndCacheKey("Banner", name);

            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "alert", "alert('清除成功!!')", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "tab", "changetab(5);", true);
        }

        public void GetViewCmsRandom(ViewCmsRandomCollection data)
        {
            if (data.Count > 0)
            {
                string[] colors = { "Red", "Blue", "Green", "Brown", "DarkMagenta", "Teal" };
                DateTime start = data.Min(x => x.StartTime);
                DateTime end = data.Max(x => x.EndTime);
                int ratio = 5;
                int width = TimsDiff(start, end, ratio);
                string content = "<div style='width:970px;overflow:scroll;height:400px;position:relative'>";
                int top = 30;
                int left = 70;
                int i = 0;

                foreach (var item in data.GroupBy(x => x.CityName))
                {
                    string color = colors[i % 4];
                    content += "<div style='position:absolute;margin-top:" + top + "px;color:" + color + "'>" + item.Key + "</div>";
                    foreach (var inneritem in item)
                    {
                        int innerwidth = TimsDiff(inneritem.StartTime, inneritem.EndTime, ratio);
                        int innerleft = TimsDiff(start, inneritem.StartTime, ratio);
                        content += "<div style='position:absolute;margin-top:" + top + "px;margin-left:" + (left + innerleft + ((int)innerwidth / 2)) + "px;width:350px;'>" + inneritem.Title + "(" + inneritem.CityName + ":" + inneritem.ContentTitle + ")" + "</div>";
                        content += "<div style='position:absolute;margin-top:" + top + "px;margin-left:" + (left + innerleft) + "px;width:100px;'>" + inneritem.StartTime.ToString("MM/dd HH:mm") + "</div>";
                        content += "<div style='position:absolute;margin-top:" + top + "px;margin-left:" + (left + innerleft + innerwidth) + "px;width:100px;'>" + inneritem.EndTime.ToString("MM/dd HH:mm") + "</div>";
                        top += 17;
                        content += "<hr style='position:absolute;margin-top:" + top + "px;margin-left:" + (left + innerleft) + "px;width:" + innerwidth + "px;border: 2px solid " + color + ";'/>";
                        top += 5;
                    }
                    i++;
                    top += 20;
                }
                content += "<div style='position:absolute;margin-left:" + (left + TimsDiff(start, DateTime.Now, ratio) + 10) + "px;width:100px;'>Now " + DateTime.Now.ToString("MM/dd HH:mm") + "</div> ";
                content += "<div style='position:absolute;margin-left:" + (left + TimsDiff(start, DateTime.Now, ratio)) + "px;width:1px;height:" + (top + 400) + "px;border-right:1px dotted blue'></div> ";
                lab_timeslot.Text = content + "</div>";
            }
            else
            {
                lab_timeslot.Text = string.Empty;
            }
        }

        public int TimsDiff(DateTime d1, DateTime d2, int ratio)
        {
            return ((int)(new TimeSpan(d2.Ticks - d1.Ticks).TotalHours)) * ratio;
        }

        public void SetUpCityDDL(List<PponCity> citygroup, SystemCodeCollection codecollection)
        {
            //BuyAdBanner不分區域
            if (Type == RandomCmsType.PponBuyAd)
            {
                citygroup = new List<PponCity>();
            }

            citygroup.Add(new PponCity(-2, "策展頻道", "", 998, (int)CmsSpecialCityType.CurationPage));
            citygroup.Add(new PponCity(-1, "不分區", "", 999, (int)CmsSpecialCityType.All));
            if (Type == RandomCmsType.Home2018)
            {
                citygroup.Clear();
                citygroup.Add(new PponCity(-1, "不分區", "", 999, (int)CmsSpecialCityType.All));
            }

            List<ListItem> targets = new List<ListItem>();
            cbx_City.DataSource = citygroup;
            cbx_City.DataTextField = "CityName";
            cbx_City.DataValueField = "CityId";
            if (Type == RandomCmsType.PponRandomCms)
            {
                targets.Add(new ListItem("Top Banner", "/ppon/default.aspx_pponnews"));
                targets.Add(new ListItem("Side Banner", "/ppon/default.aspx_news"));
                targets.Add(new ListItem("PreviewAll Banner", "/ppon/todaydeals.aspx_news"));
                targets.Add(new ListItem("Main Banner", "/default.aspx_news"));
                targets.Add(new ListItem("MainActive Banner", "/default.aspx_mainactive"));
                targets.Add(new ListItem("Visa Banner", "/default.aspx_visa"));
                targets.Add(new ListItem("Active Banner", "/ppon/default.aspx_active"));
                targets.Add(new ListItem("OutsideAD Banner", "/ppon/default.aspx_outsideAD"));
                targets.Add(new ListItem("InsideAD Banner", "/ppon/default.aspx_insideAD"));
                targets.Add(new ListItem("OutsideADPreviewAll Banner", "/ppon/todaydeals.aspx_outsideAD"));
                targets.Add(new ListItem("Piinlife Top Banner", "/piinlife/default.aspx_pponnews"));
                targets.Add(new ListItem("Sale Banner", "/ppon/default.aspx_saleB"));
                targets.Add(new ListItem("Classify Banner1", "/ppon/default.classify_banner1"));
                targets.Add(new ListItem("Classify Banner2", "/ppon/default.classify_banner2"));
            }
            else if (Type == RandomCmsType.BBH)
            {
                targets.Add(new ListItem("Top Banner", "/ppon/bbhtodaydeals.aspx_pponnews"));
                targets.Add(new ListItem("Side Banner", "/ppon/bbhtodaydeals.aspx_news"));
            }
            else if (Type == RandomCmsType.SuperTaste)
            {
                targets.Add(new ListItem("Top Banner", "/ppon/supertastetodaydeals.aspx_pponnews"));
                targets.Add(new ListItem("Side Banner", "/ppon/supertastetodaydeals.aspx_news"));
            }
            else if (Type == RandomCmsType.PiinLifeRandomCms)
            {
                targets.Add(new ListItem("Top Banner", "/piinlife/default.aspx_pponnews"));
                targets.Add(new ListItem("Side Banner", "/piinlife/default.aspx_news"));
                targets.Add(new ListItem("Visa Banner", "/piinlife/default.aspx_visa"));
            }
            else if (Type == RandomCmsType.PponMasterPage)
            {
                targets.Add(new ListItem("MasterBanner", "masterbanner"));
                targets.Add(new ListItem("MasterSpecialBanner", "special"));
                targets.Add(new ListItem("Curation Banner", "curation"));
            }
            else if (Type == RandomCmsType.PponBuyAd)
            {
                targets.Add(new ListItem("BuyAdBanner", "buyadbanner"));
            }
            else if (Type == RandomCmsType.Home2018)
            {
                targets.Add(new ListItem("Top Banner", "topbanner"));
            }

            DDLBinding(targets, "Text", "Value", ddl_Target, ddl_TargetView, ddl_Target2);
            cbx_City.DataBind();
            List<ListItem> hours = new List<ListItem>();
            for (int i = 0; i < 24; i++)
            {
                hours.Add(new ListItem(i.ToString().PadLeft(2, '0'), i.ToString().PadLeft(2, '0')));
            }
            DDLBinding(hours, "Text", "Value", ddl_StartHour, ddl_EndHour);
            List<ListItem> minutes = new List<ListItem>();
            for (int i = 0; i <= 50; i = i + 10)
            {
                minutes.Add(new ListItem(i.ToString().PadLeft(2, '0'), i.ToString().PadLeft(2, '0')));
            }
            DDLBinding(minutes, "Text", "Value", ddl_StartMinute, ddl_EndMinute);
        }

        #endregion method

        #region event

        public event EventHandler<DataEventArgs<CmsRandomContent>> SaveCmsRandomContent = null;

        public event EventHandler<DataEventArgs<KeyValuePair<CmsRandomCity, List<RandomSelectedCity>>>> SaveCmsRandomCity = null;

        public event EventHandler<DataEventArgs<CmsRandomCity>> UpdateCmsRandomCity = null;

        public event EventHandler GetCmsRandomById = null;

        public event EventHandler GetCmsRandomCityById = null;

        public event EventHandler<DataEventArgs<KeyValuePair<int, string>>> GetCmsRandomContentById = null;

        public event EventHandler<DataEventArgs<string>> ChangeCmsRandom = null;

        public event EventHandler GetCacheName = null;

        public event EventHandler ChangeRandomCmsType = null;

        public event EventHandler<DataEventArgs<KeyValuePair<int, int>>> DeleteCmsRandomCity = null;

        public event EventHandler<DataEventArgs<string>> GetFileList = null;

        public event EventHandler<DataEventArgs<HttpPostedFile>> UploadFile = null;

        public event EventHandler<DataEventArgs<string>> DelFile = null;

        #endregion event

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                rbl_RandomCmsType.Items.Add(new ListItem("P好康", RandomCmsType.PponRandomCms.ToString()));
                rbl_RandomCmsType.Items.Add(new ListItem("首頁2018", RandomCmsType.Home2018.ToString()));
                //rbl_RandomCmsType.Items.Add(new ListItem("BabyHome", RandomCmsType.BBH.ToString()));
                //rbl_RandomCmsType.Items.Add(new ListItem("舊品生活", RandomCmsType.PiinLifeRandomCms.ToString(), false));
                //rbl_RandomCmsType.Items.Add(new ListItem("時尚玩家", RandomCmsType.PponRandomCms.ToString()));
                rbl_RandomCmsType.Items.Add(new ListItem("P好康MasterPage", RandomCmsType.PponMasterPage.ToString()));
                rbl_RandomCmsType.Items.Add(new ListItem("P好康付款頁AD", RandomCmsType.PponBuyAd.ToString()));
                rbl_RandomCmsType.SelectedIndex = 0;

                _presenter.OnViewInitialized();
                ScriptManager.RegisterStartupScript(this, typeof(Button), "tab", "changetab(0);", true);
            }
            _presenter.OnViewLoaded();

            if (!Page.IsPostBack)
            {
                SetCurationEnabled();
            }
        }

        protected void ClickSaveCmsRandomContent(object sender, EventArgs e)
        {
            if (this.SaveCmsRandomContent != null)
            {
                if (!string.IsNullOrEmpty(tbx_Title.Text) && !string.IsNullOrEmpty(tbx_editor_Html.Text))
                {
                    CmsRandomContent cms = new CmsRandomContent();
                    cms.Title = tbx_Title.Text;
                    cms.ContentName = ddl_Target.SelectedValue;
                    cms.ContentTitle = ddl_Target.SelectedItem.Text;
                    cms.Body = HttpUtility.HtmlDecode(tbx_editor_Html.Text);
                    cms.Status = (((Button)sender).ID == "btn_Delete" ? false : cbx_ContentStatus.Checked);
                    cms.Locale = "zh_TW";
                    cms.CreatedOn = DateTime.Now;
                    cms.CreatedBy = UserName;
                    cms.ModifiedOn = DateTime.Now;
                    cms.ModifiedBy = (((Button)sender).ID == "btn_Delete" ? "KILLER" : UserName);
                    cms.Type = (int)Type;
                    this.SaveCmsRandomContent(sender, new DataEventArgs<CmsRandomContent>(cms));
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "alert", "alert('請填入標題和內容!!');", true);
                }
                tbx_editor_Html.Text = HttpUtility.HtmlDecode(tbx_editor_Html.Text);
                //避免chrome出現ERR_BLOCKED_BY_XSS_AUDITOR
                Response.AddHeader("X-XSS-Protection", "0");
            }
        }

        protected void ClearContent(params object[] objs)
        {
            foreach (object item in objs)
            {
                if (item is TextBox)
                {
                    ((TextBox)item).Text = string.Empty;
                }
                else if (item is CheckBox)
                {
                    ((CheckBox)item).Checked = false;
                }
                else if (item is AjaxControlToolkit.HTMLEditor.Editor)
                {
                    ((AjaxControlToolkit.HTMLEditor.Editor)item).Content = string.Empty;
                }
                else if (item is CheckBoxList)
                {
                    CheckBoxList list = (CheckBoxList)item;
                    for (int i = 0; i < list.Items.Count; i++)
                    {
                        list.Items[i].Selected = false;
                    }
                }
            }
        }

        protected void gv_Command(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditId")
            {
                hif_Mode.Value = "2";
                hif_ID.Value = e.CommandArgument.ToString();
                if (this.GetCmsRandomCityById != null)
                {
                    this.GetCmsRandomCityById(sender, e);
                }
            }
            else if (e.CommandName == "DeleteId")
            {
                int id, content_id;
                if (int.TryParse(e.CommandArgument.ToString(), out id) && int.TryParse(ddl_BannersView.SelectedValue, out content_id) && DeleteCmsRandomCity != null)
                {
                    KeyValuePair<int, int> pair = new KeyValuePair<int, int>(id, content_id);
                    DeleteCmsRandomCity(sender, new DataEventArgs<KeyValuePair<int, int>>(pair));
                }
            }
        }

        protected void CancelAction(object sender, EventArgs e)
        {
            ClearContent(tbx_EndTime, tbx_editor_Html, tbx_StartTime, tbx_Title);
            hif_Mode.Value = "1";
            btn_Save.Text = "新增";
        }

        protected void BannerChangeGetContent(object sender, EventArgs e)
        {
            int id;
            DropDownList ddl = ((DropDownList)sender);
            if (this.GetCmsRandomContentById != null && int.TryParse(ddl.SelectedValue, out id))
            {
                this.GetCmsRandomContentById(sender, new DataEventArgs<KeyValuePair<int, string>>(new KeyValuePair<int, string>(id, ddl.ID)));
            }
        }

        protected void TargetChange(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            if (this.ChangeCmsRandom != null)
            {
                this.ChangeCmsRandom(sender, new DataEventArgs<string>(((DropDownList)sender).ID));
            }
            BannerChangeGetContent(ddl == ddl_Target2 ? ddl_Banners : ddl_BannersView, e);
        }

        protected void ClickSaveCmsRandomCity(object sender, EventArgs e)
        {
            if (this.SaveCmsRandomCity != null)
            {
                int pid;
                DateTime starttime, endtime;
                if (!string.IsNullOrEmpty(tbx_StartTime.Text) && !string.IsNullOrEmpty(tbx_EndTime.Text)
                    && int.TryParse(ddl_Banners.SelectedValue, out pid)
                    && DateTime.TryParse(tbx_StartTime.Text + " " + ddl_StartHour.SelectedValue + ":" + ddl_StartMinute.SelectedValue, out starttime)
                    && DateTime.TryParse(tbx_EndTime.Text + " " + ddl_EndHour.SelectedValue + ":" + ddl_EndMinute.SelectedValue, out endtime))
                {
                    List<RandomSelectedCity> cities = new List<RandomSelectedCity>();
                    for (int i = 0; i < cbx_City.Items.Count; i++)
                    {
                        int cityid;
                        if (cbx_City.Items[i].Selected && int.TryParse(cbx_City.Items[i].Value, out cityid))
                        {
                            int ratio = 1;
                            cities.Add(new RandomSelectedCity(cityid, cbx_City.Items[i].Text, ratio));
                        }
                    }

                    if (DateTime.Compare(starttime, endtime) < 0)
                    {
                        if (cities.Count != 0)
                        {
                            CmsRandomCity city = new CmsRandomCity();
                            city.Pid = pid;
                            city.StartTime = starttime;
                            city.EndTime = endtime;
                            city.Status = true;
                            city.CreatedBy = UserName;
                            city.CreatedOn = DateTime.Now;
                            city.Type = (int)Type;
                            this.SaveCmsRandomCity(sender, new DataEventArgs<KeyValuePair<CmsRandomCity, List<RandomSelectedCity>>>(new KeyValuePair<CmsRandomCity, List<RandomSelectedCity>>(city, cities)));
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Button), "alert", "alert('請選擇城市!!');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Button), "alert", "alert('起訖日期錯誤!!');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Button), "alert", "alert('日期格式錯誤!!');", true);
                }

                ScriptManager.RegisterStartupScript(this, typeof(Button), "tab", "changetab(3);", true);
            }
        }

        protected void EditCmsContent(object sender, EventArgs e)
        {
            if (this.GetCmsRandomById != null)
            {
                this.GetCmsRandomById(sender, e);
            }
        }

        protected void UpdateCity(object sender, EventArgs e)
        {
            DateTime starttime, endtime;
            if (this.UpdateCmsRandomCity != null)
            {
                if (!string.IsNullOrEmpty(tbx_StartTime.Text) && !string.IsNullOrEmpty(tbx_EndTime.Text)
                   && DateTime.TryParse(tbx_StartTime.Text + " " + ddl_StartHour.SelectedValue + ":" + ddl_StartMinute.SelectedValue, out starttime)
                   && DateTime.TryParse(tbx_EndTime.Text + " " + ddl_EndHour.SelectedValue + ":" + ddl_EndMinute.SelectedValue, out endtime))
                {
                    CmsRandomCity city = new CmsRandomCity();
                    city.StartTime = starttime;
                    city.EndTime = endtime;
                    city.Ratio = 1;
                    city.Status = cbx_CityStatus.Checked;
                    this.UpdateCmsRandomCity(sender, new DataEventArgs<CmsRandomCity>(city));
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Button), "alert", "alert('日期格式錯誤!!');", true);
                }
                ScriptManager.RegisterStartupScript(this, typeof(Button), "tab", "changetab(0);", true);
            }
        }

        protected void DDLBinding(DataTable dt, string text, string value, params DropDownList[] objects)
        {
            foreach (DropDownList item in objects)
            {
                item.DataSource = dt;
                item.DataTextField = text;
                item.DataValueField = value;
                item.DataBind();
            }
        }

        protected void DDLBinding(List<ListItem> dt, string text, string value, params DropDownList[] objects)
        {
            foreach (DropDownList item in objects)
            {
                item.DataSource = dt;
                item.DataTextField = text;
                item.DataValueField = value;
                item.DataBind();
            }
        }

        protected void ClearCache(object sender, EventArgs e)
        {
            if (this.GetCacheName != null)
            {
                GetCacheName(sender, e);
            }
        }

        protected void CancelAction2(object sender, EventArgs e)
        {
            lbl_City.Visible = cbx_CityStatus.Visible = btn_EditCity.Visible = false;
            ddl_Target2.Visible = ddl_Banners.Visible = ddl_Target2.Visible = cbx_City.Visible = btn_Save2.Visible = true;
            hif_Mode.Value = "1";
            btn_Save.Text = "新增";
            ClearContent(tbx_StartTime, tbx_EndTime);
        }

        protected void RandomCmsTypeChange(object sender, EventArgs e)
        {
            if (ChangeRandomCmsType != null)
            {
                ChangeRandomCmsType(sender, e);
            }
        }

        #endregion page

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            ddl_UploadType.SelectedValue = ddl_SearchUploadType.SelectedValue;//搜尋要把uploadtype設定一致
            GetFileList(sender, new DataEventArgs<string>(tbx_Search.Text));
            string selectedText = ddl_SearchUploadType.SelectedItem.Text;

            switch (ddl_SearchUploadType.SelectedValue)
            {
                case "17P/100X100/":
                    lbl_UploadType.Text = selectedText + "(100px*100px)";
                    break;

                case "17P/17sidebn/":
                    lbl_UploadType.Text = selectedText + "(960px*110px、740px*80px)";
                    break;

                case "17P/17topbn/":
                    lbl_UploadType.Text = selectedText + "(220px*220px)";
                    break;

                case "17P/20130430-FamilyMothersDay/":
                    lbl_UploadType.Text = selectedText;
                    break;

                case "17P/active/":
                    lbl_UploadType.Text = selectedText;
                    break;

                case "easyfree/":
                    lbl_UploadType.Text = selectedText + "(HTML)";
                    break;

                case "easyfree/NEWversion/":
                    lbl_UploadType.Text = selectedText + "(HTML)";
                    break;

                case "easyfree/NEWversion/images/":
                    lbl_UploadType.Text = selectedText + "(138px*2430px、80px*75px、HTML)";
                    break;

                case "fami_mkt/":
                    lbl_UploadType.Text = selectedText + "(960px*351px)";
                    break;

                case "Outside_AD/":
                    lbl_UploadType.Text = selectedText + "(100px*270px)";
                    break;

                case "Inside_AD/":
                    lbl_UploadType.Text = selectedText + "(100px*270px)";
                    break;

                case "promo_new/":
                    lbl_UploadType.Text = selectedText + "(HTML)";
                    break;
            }
            lbl_UploadType.Visible = true;
        }

        protected void gv_UploadFile_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "del")
            {
                DelFile(sender, new DataEventArgs<string>((string)e.CommandArgument));
                GetFileList(sender, new DataEventArgs<string>(tbx_Search.Text));
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "tab", "changetab(1);", true);
            }
        }

        protected void btn_Commit_Click(object sender, EventArgs e)
        {
            UploadFile(sender, new DataEventArgs<HttpPostedFile>(fd_UploadImage.PostedFile));
            GetFileList(sender, new DataEventArgs<string>(tbx_Search.Text));
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "tab", "changetab(1);", true);
        }

        private void SetCurationEnabled()
        {
            if (this.IsCurationUser)
            {
                rbl_RandomCmsType.SelectedValue = RandomCmsType.PponMasterPage.ToString();
                RandomCmsTypeChange(rbl_RandomCmsType, new EventArgs());
                rbl_RandomCmsType.Enabled = false;
                if (ddl_TargetView.Items.FindByValue("curation") != null)
                {
                    ddl_TargetView.SelectedValue = "curation";
                    TargetChange(ddl_TargetView, new EventArgs());
                    ddl_TargetView.Enabled = false;

                    ddl_Target.SelectedValue = "curation";
                    ddl_Target.Enabled = false;

                    ddl_Target2.SelectedValue = "curation";
                    TargetChange(ddl_Target2, new EventArgs());
                    ddl_Target2.Enabled = false;
                }
            }
        }
    }
}
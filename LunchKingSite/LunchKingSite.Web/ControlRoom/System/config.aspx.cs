﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Net = System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using log4net;

namespace LunchKingSite.Web.ControlRoom
{
    /// <summary>
    /// TODO: better layout
    /// TODO: Localization support, make label to be the description
    /// </summary>
    public partial class config : RolePage
    {
        private ISysConfProvider conf;        
        private ILog log = LogManager.GetLogger("config");
        protected void page_load() 
        { 
        
        }

        protected override void CreateChildControls()
        {
            conf = ProviderFactory.Instance().GetConfig();
            SetupPage();
            base.CreateChildControls();
        }

        /// <summary>
        /// Setups the page. It'll dynamically generates controls according to the parameter
        /// </summary>
        private void SetupPage()
        {
            lConf.Text = conf.GetType().FullName;

            tabContents.Controls.Clear();

            ConfigParameterAttribute.ConfigCategories[] confCategories = Enum.GetValues(typeof(ConfigParameterAttribute.ConfigCategories))
                    as ConfigParameterAttribute.ConfigCategories[];

            for (int i = 0; i < confCategories.Length; i++)
            {
                var confCategory = confCategories[i];
                PlaceHolder phOptions = WebUtility.ClassToControl(conf, confCategory);
                if (phOptions.Controls.Count > 0)
                {
                    if (i == 0)
                    {
                        myTab.Controls.Add(
                            new LiteralControl(
                                string.Format("<li class='active'><a href='#{0}' data-toggle='tab'>{0}</a></li>",
                                    confCategory)));
                    }
                    else
                    {
                        myTab.Controls.Add(
                            new LiteralControl(String.Format("<li><a href='#{0}' data-toggle='tab'>{0}</a></li>",
                                confCategory)));
                    }

                    HtmlGenericControl divOption = new HtmlGenericControl("div");
                    divOption.ID = confCategory.ToString();
                    if (i == 0)
                    {
                        divOption.Attributes["class"] = "tab-pane active";
                    }
                    else
                    {
                        divOption.Attributes["class"] = "tab-pane";
                    }

                    divOption.ClientIDMode = ClientIDMode.Static;
                    divOption.Controls.Add(phOptions);
                    tabContents.Controls.Add(divOption);
                }
            }
            //ConfigParameterAttribute.ConfigTypes[] confTypes = Enum.GetValues(typeof(ConfigParameterAttribute.ConfigTypes))
            //        as ConfigParameterAttribute.ConfigTypes[];

            //for (int i = 0; i < confTypes.Length; i++)
            //{
            //    var confType = confTypes[i];
            //    PlaceHolder phOptions = WebUtility.ClassToControl(conf, confType);
            //    if (phOptions.Controls.Count > 0)
            //    {
            //        if (i == 0)
            //        {
            //            myTab.Controls.Add(
            //                new LiteralControl(
            //                    string.Format("<li class='active'><a href='#{0}' data-toggle='tab'>{0}</a></li>",
            //                        confType)));
            //        }
            //        else
            //        {
            //            myTab.Controls.Add(
            //                new LiteralControl(String.Format("<li><a href='#{0}' data-toggle='tab'>{0}</a></li>",
            //                    confType)));
            //        }

            //        HtmlGenericControl divOption = new HtmlGenericControl("div");
            //        divOption.ID = confType.ToString();
            //        if (i == 0)
            //        {
            //            divOption.Attributes["class"] = "tab-pane active";
            //        }
            //        else
            //        {
            //            divOption.Attributes["class"] = "tab-pane";
            //        }

            //        divOption.ClientIDMode = ClientIDMode.Static;
            //        divOption.Controls.Add(phOptions);
            //        tabContents.Controls.Add(divOption);
            //    }

            //}
        }

        /// <summary>
        /// Sets the config.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        /// <remarks>TODO: error catching, input checking</remarks>
        protected void SetConfig(object sender, EventArgs e)
        {
            var changeCol = WebUtility.ControlToClass(tabContents, conf);
            if (changeCol.Count > 0 && CommonFacade.GetWebServerHostList().Any())
            {
                rpt_syncList.Visible = true;
                ViewState["changeCol"] = changeCol;
                pan_sync.Visible = true;
                rpt_syncList.DataSource = changeCol;
                rpt_syncList.DataBind();
            }
        }

        protected void SetAllConfig(object sender, EventArgs e)
        {
            //var serverList = CommonFacade.GetWebServerHostList();
            var serverList = CommonFacade.GetWebServerIpList();
            if (!serverList.Any()) return;

            if (ViewState["changeCol"] == null) return;

            var changeCol = (ConcurrentDictionary<string, string>)ViewState["changeCol"];
            if (!changeCol.Any()) return;


            const string service = "/webservice/ApplicationService.asmx/RefreshConfig";
            var result = new ConcurrentDictionary<string, string>();

            var authCookies = Context.Request.Cookies[".ASPXAUTH"];
            foreach (var changeItem in changeCol)
            {
                var param = string.Format("key={0}&value={1}", changeItem.Key.Trim(), changeItem.Value.Trim());
                Parallel.ForEach(serverList, item =>
                {
                    try
                    {
                        var url = string.Format(@"https://{0}{1}", item, service);
                        byte[] bs = Encoding.UTF8.GetBytes(param);
                        Net.HttpWebRequest req = Net.WebRequest.Create(url) as Net.HttpWebRequest;
                        if (req == null)
                        {
                            result.AddOrUpdate(item, string.Format("HttpWebRequest Create {0} is null.", url), (k, v) => string.Empty);
                            return;
                        }

                        req.Method = "POST";
                        req.ContentType = "application/x-www-form-urlencoded";
                        req.ContentLength = bs.Length;
                        Net.CookieContainer gaCookies = new Net.CookieContainer();
                        if (authCookies != null)
                        {
                            var domain = item;
                            gaCookies.Add(new Net.Cookie(authCookies.Name, authCookies.Value)
                            {
                                Domain = domain,
                                Expires = DateTime.Now.AddMinutes(5)
                            });
                            log.Info("Server:" + item + ";domain:" + domain + ";authCookies:" + authCookies.Value);
                            req.CookieContainer = gaCookies;

                            using (Stream reqStream = req.GetRequestStream())
                            {
                                reqStream.Write(bs, 0, bs.Length);
                            }

                            using (var response = req.GetResponse() as Net.HttpWebResponse)
                            {
                                if (response == null)
                                {
                                    result.AddOrUpdate(item, "request GetResponse() is null.", (k, v) => string.Empty);
                                    return;
                                }
                                var stream = response.GetResponseStream();
                                if (stream == null)
                                {
                                    result.AddOrUpdate(item, "response GetResponseStream() is null.", (k, v) => string.Empty);
                                    return;
                                }
                                StreamReader reader = new StreamReader(stream);
                                string retVal = reader.ReadToEnd() ?? "";
                                result.AddOrUpdate(item, retVal, (k, v) => retVal);
                            }
                        }
                        else
                        {
                            result.AddOrUpdate(item, "Cookie not found(.ASPXAUTH).", (k, v) => string.Empty);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Info("Server:" + item + ";authCookies:" + authCookies.Value + " err:" + ex.Message + ex.StackTrace);

                        var jsonS = new JsonSerializer();
                        var err = new { Code = (int)ApiReturnCode.Error, Message = ex.Message + ex.StackTrace };
                        result.AddOrUpdate(item, jsonS.Serialize(err), (k, v) => jsonS.Serialize(err));
                    }
                });
            }

            rpt_syncList.Visible = false;
            ViewState["changeCol"] = null;

            Result.Text = @"Update Result : <br />";
            Result.Text += @"<table><thead><td>Server</td><td style='width:100px;' >Result</td><td>Message</td></thead>";
            Result.Text += @"<tbody>";
            foreach (var item in serverList)
            {
                var tmpValue = "";
                if (result.TryGetValue(item, out tmpValue))
                {
                    JObject jo = JObject.Parse(tmpValue);
                    dynamic dyna = jo as dynamic;
                    Result.Text += string.Format(@"<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>",
                        item, dyna.Code == ApiReturnCode.Success ? "更新成功" : "更新失敗", dyna.Message);
                }
                else
                {
                    Result.Text += string.Format(@"<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>",
                        item, "更新失敗", "超過等待時間或伺服器沒有回應");
                }
            }
            Result.Text += @"</tbody>";
            Result.Text += @"</table>";
            pan_sync.Visible = true;
        }
    }
}
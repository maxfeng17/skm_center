﻿using System;
using System.Collections.Generic;
using System.Xml;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Jobs;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Web.Service;
using LunchKingSite.Web.WebService;
using LunchKingSite.Mongo.Facade;

using LunchKingSite.DataOrm;
using LunchKingSite.SsBLL;
using System.Web;
using LunchKingSite.WebLib;
using LunchKingSite.Core.UI;
using SMS = LunchKingSite.BizLogic.Component.SMS;
using System.Linq;
using System.Data;
using log4net;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.SsBLL.Provider;
using LunchKingSite.BizLogic.Models.Wms;
using LunchKingSite.BizLogic.Jobs.Wms;
using LunchKingSite.Core.Interface;
using LunchKingSite.BizLogic.Models.FamiPort;
using LunchKingSite.Core.ModelCustom;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class HamiTestPage : RolePage
    {
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        protected IHiDealProvider hp = new SSHiDealProvider();
        protected IMemberProvider mp = new SSMemberProvider();
        protected IOrderProvider op = new SSOrderProvider();
        protected IPponProvider pp = new SSPponProvider();
        protected ISellerProvider sp = new SSSellerProvider();
        protected INotificationProvider np = new SSNotificationProvider();
        private static IWmsProvider _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        private static ILog logger = LogManager.GetLogger(typeof(HamiTestPage));

        protected void Page_Load(object sender, EventArgs e)
        {
            frontDealsStatStart.Text = DateTime.Now.ToString("yyyy/MM/dd");
            frontDealsStatEnd.Text = DateTime.Now.ToString("yyyy/MM/dd");
        }

        protected void btnEventPageCount_Click(object sender, EventArgs e)
        {
            VourcherFacade.UpdateVourcherEventPageCountAddCount(1, 1);

            //XmlDocument xml = PponFacade.PponDealGetByDateHamiXml(DateTime.Now, DateTime.Now);
            //tbXml.Text = xml.InnerXml;
        }

        protected void btnMac_Click(object sender, EventArgs e)
        {
            tbMAC.Text = HamiDataModel.GetRqDataMac(tbQueryDate.Text.Trim(), tbResponseTime.Text.Trim());
        }

        protected void btnHiDealCoupon_Click(object sender, EventArgs e)
        {
            HiDealCouponManager.GenerateHiDealCoupon(int.Parse(tbPid.Text.Trim()), cp.SystemEmail);
        }
        protected void btnPiinlifeSend_Click(object sender, EventArgs e)
        {
            throw new NotSupportedException();
        }

        protected void testPponPaymentSuccessful_Click(object sender, EventArgs e)
        {
            /*for 測試站*/
            OrderFacade.SendPponMail(MailContentType.Authorized, Guid.Parse("4669DA6C-488A-4DD8-ABAC-4F49600A869A"));
            /*for 本機*/
            //OrderFacade.SendPponMail(MailContentType.Authorized, Guid.Parse("38790045-AB1A-4B7B-AF27-6495551370CE"));

            ClientScript.RegisterClientScriptBlock(Page.GetType(), "Startup", "<script type='text/javascript'>alert('" + tbMailAddress.Text.ToString() + "')</script>");
            // tbMailAddress.Text;
        }

        protected void sendSms_Click(object sender, EventArgs e)
        {
            tbResult.Text = string.Empty;
            string result = string.Empty;

            string hidealid = Sms_Hi_deal_id.Text;
            Guid hidealstoreGuid = Guid.Parse(Sms_Hi_deal_store_guid.Text);


            //1.取得該hi_deal_deal_中某一分店已發出未使用的的所有coupon
            ViewHiDealCouponCollection storeAllCoupon = DB.SelectAllColumnsFrom<ViewHiDealCoupon>().Where(ViewHiDealCoupon.Columns.DealId).IsEqualTo(hidealid)
                .And(ViewHiDealCoupon.Columns.StoreGuid).IsEqualTo(hidealstoreGuid.ToString())
                .And(ViewHiDealCoupon.Columns.Status).IsEqualTo("1")
                .ExecuteAsCollection<ViewHiDealCouponCollection>();
            int i = 0;

            SMS sms = new SMS();

            foreach (ViewHiDealCoupon vhdc in storeAllCoupon)
            {

                #region content
                HiDealCouponSmsFormatter smsFormatter = new HiDealCouponSmsFormatter(vhdc.Sms);
                smsFormatter.CouponPrefix = vhdc.Prefix;
                smsFormatter.CouponSequence = vhdc.Sequence;
                smsFormatter.CouponCode = vhdc.Code;
                smsFormatter.BeginDate = vhdc.UseStartTime;
                smsFormatter.ExpireDate = vhdc.UseEndTime;
                smsFormatter.TelephoneInfo = OrderFacade.HiDealSmsGetPhone(vhdc);

                Member member = mp.MemberGet(vhdc.UserName);
                sms.SendMessage("", smsFormatter.ToSms(), "", member.Mobile, vhdc.UserName, Core.SmsType.HiDeal, Guid.Empty, vhdc.CouponId.GetValueOrDefault().ToString());

                #endregion
                i++;
                result += "筆數:{" + i + "}、{" + smsFormatter.TelephoneInfo + "}、編號:{" + smsFormatter.CouponPrefix + " " + smsFormatter.CouponSequence + "}、驗證碼{"
                 + smsFormatter.CouponCode + "}、期限:{" + smsFormatter.BeginDate.GetValueOrDefault().ToString("yyyyMMdd") + "~" + smsFormatter.ExpireDate.GetValueOrDefault().ToString("yyyyMMdd") + "}\n";

            }
            tbResult.Text = result + "共" + i + "筆簡訊發送";


        }

        protected void msgText_Click(object sender, EventArgs e)
        {
            SMS sms = new SMS();

            string showMessage = string.Format("親愛的會員 {0} 您好，這是一封測試簡訊", "SamLee");

            sms.SendMessage("", showMessage, "", "0975547970", "SamLee", Core.SmsType.System, Guid.Empty, "123456789");

        }

        protected void PushMemberCollectDeal_Click(object sender, EventArgs e)
        {
            DateTime due_date;
            if (DateTime.TryParse(this.txtDue_Date.Text, out due_date))
            {
                NotificationFacade.PushMemberCollectDeal(due_date);
            }
        }

        protected void TestIDEAS_Click(object sender, EventArgs e)
        {
            ApiLocationDealManager.UpdateIDEASOrder();
        }
        protected void TestIDEASDeal_Click(object sender, EventArgs e)
        {
            int dealId;
            if (int.TryParse(tbIDEASDealId.Text, out dealId))
            {
                ApiLocationDealManager.UpdateIDEASDeal(dealId);
            }
        }
        protected void TestIDEASVourcher_Click(object sender, EventArgs e)
        {
            int vourcherId;
            if (int.TryParse(tbIDEASVourcherId.Text, out vourcherId))
            {
                ApiLocationDealManager.UpdateIDEASVourcher(vourcherId);
            }
        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
        }

        protected void DataManagerResetJobClick(object sender, EventArgs e)
        {
            DataManagerResetJob.DataManagerReset();
        }

        protected void CacheDataResetJobClick(object sender, EventArgs e)
        {
            CacheDataResetJob.CacheDataReset();
        }

        protected void SendMgmGiftNoticeToReceiverClick(object sender, EventArgs e)
        {
            Core.Configuration.JobSetting js = new Core.Configuration.JobSetting("SendMgmGiftNoticeToReceiverClick", GetType(), "SendMgmGiftNoticeToReceiverClick");

            SendMgmGiftNoticeToReceiver.Instance().Execute(js);
        }

        protected void VbsCustomerServiceNoticeMailClick(object sender, EventArgs e)
        {
            Core.Configuration.JobSetting js = new Core.Configuration.JobSetting("VbsCustomerServiceNoticeMailClick", GetType(), "VbsCustomerServiceNoticeMailClick");

            VbsCustomerServiceNoticeMail.Instance().Execute(js);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            string eventCode = "2013_SKM"; // 新光商圈專案
            DateTime dateStart = (DateTime.Parse(JobBeginDate.Text)).Date;
            DateTime dateEnd = (DateTime.Parse(JobEndDate.Text)).Date;
            PromotionFacade.GenerateEventPromoItemByPeriod(eventCode, dateStart, dateEnd);
        }

       

        protected void BookingButton_Click(object sender, EventArgs e)
        {
            string toMail = bookingToMail.Text;
            var couponSequences = new List<string>();
            couponSequences.Add("1111-1111");
            couponSequences.Add("2222-2222");

            //SendReservationSuccessMail
            var reservationSuccessMailInfo = new ReservationSuccessMailInfomation
            {
                MemberName = "XXX",
                ReservationDate = DateTime.Now,
                ReservationTimeSlot = DateTime.Now.SetTime(12, 30, 0),
                NumberOfPeople = 100,
                MemberEmail = toMail,
                ContactName = "訂位第一人",
                ContactNumber = "09XX-XXX-XXX",
                Remark = "這是備註欄",
                CouponsSequence = couponSequences,
                CouponInfo = "這是優惠內容欄",
                CouponLink = "https://www.17life.com",
                SellerName = "XXX店家",
                StoreName = "分店XXX號",
                StoreAddress = "XXX市XXX區XXX路XX號XX樓"
            };

            BookingSystemFacade.SendReservationSuccessMail(toMail, reservationSuccessMailInfo);

            //SendTravelSuccessMail
            var travelSuccessMailInfo = new TravelSuccessMailInfomation
            {
                MemberName = "XXX",
                ReservationDate = DateTime.Now,
                NumberOfPeople = 100,
                MemberEmail = toMail,
                ContactName = "訂房第一人",
                ContactNumber = "09XX-XXX-XXX",
                Remark = "這是備註欄",
                CouponsSequence = couponSequences,
                CouponInfo = "這是優惠內容欄",
                CouponLink = "https://www.17life.com",
                SellerName = "XXX店家",
                StoreName = "分店XXX號",
                StoreAddress = "XXX市XXX區XXX路XX號XX樓"
            };

            BookingSystemFacade.SendTravelSuccessMail(toMail, travelSuccessMailInfo);

            //ReservationCancelMail
            var reservationCancelMailInfo = new ReservationCancelMailInfomation
            {
                MemberName = "XXX",
                SellerName = "XXX店家",
                StoreName = "分店XXX號",
            };
            BookingSystemFacade.SendReservationCancelMail(toMail, reservationCancelMailInfo);

            //ReservationRemindMail
            var reservationRemindInfo = new ReservationRemindMailInfomation
            {
                MemberName = "XXX",
                ReservationDate = DateTime.Now,
                ReservationTimeSlot = DateTime.Now.SetTime(12, 30, 0),
                NumberOfPeople = 100,
                MemberEmail = toMail,
                ContactName = "訂位第一人",
                ContactNumber = "09XX-XXX-XXX",
                Remark = "這是備註欄",
                CouponsSequence = couponSequences,
                CouponInfo = "這是優惠內容欄",
                CouponLink = "https://www.17life.com",
                SellerName = "XXX店家",
                StoreName = "分店XXX號",
                StoreAddress = "XXX市XXX區XXX路XX號XX樓"
            };

            BookingSystemFacade.SendReservationRemindMail(toMail, reservationRemindInfo);

            //TravelRemindMail
            var travelRemindMailInfo = new TravelRemindMailInfomation
            {
                MemberName = "XXX",
                ReservationDate = DateTime.Now,
                NumberOfPeople = 100,
                MemberEmail = toMail,
                ContactName = "訂房第一人",
                ContactNumber = "09XX-XXX-XXX",
                CouponsSequence = couponSequences,
                Remark = "這是備註欄",
                CouponInfo = "這是優惠內容欄",
                CouponLink = "https://www.17life.com",
                SellerName = "XXX店家",
                StoreName = "分店XXX號",
                StoreAddress = "XXX市XXX區XXX路XX號XX樓"
            };

            BookingSystemFacade.SendTravelRemindMail(toMail, travelRemindMailInfo);

            //SettingChangeLogMail

            var testList = new List<KeyValuePair<string, string>>();
            testList.Add(new KeyValuePair<string, string>("日期", "2013/11/12"));
            testList.Add(new KeyValuePair<string, string>("暫不開放預約", "ON-->OFF"));
            testList.Add(new KeyValuePair<string, string>("12:00", "0-->10"));
            testList.Add(new KeyValuePair<string, string>("18:00", "0-->10"));


            var settingChangelogMailInfo = new BookingSettingChangeLogMailInfomation
            {
                VbsAccount = "XXX Account",
                SellerName = "XXXX",
                StoreName = "《XX店》",
                BookingSystemStoreId = 0,
                BookingSystemDateId = 0,
                ModifyTime = DateTime.Now,
                ChangeSettingLogs = testList
            };

            BookingSystemFacade.SendSettingChangeLogMail(toMail, settingChangelogMailInfo);

        }

        protected void IDEASMain_Click(object sender, EventArgs e)
        {
            var o = op.OrderGet(new Guid("CD6219D4-A5CB-48BD-875F-FEB7BF187A40"));
            OrderFacade.SendIDEASMail(IDEASMailContentType.PaymentCompleted, o, null);

            //OrderFacade.IDEASSendRefundCompleteNoticeMail(returnForm, deal, allCtlogs, order, companyUserOrder);
        }


        protected void PcpMailButton_Click(object sender, EventArgs e)
        {
            string toMail = pcpPointToMail.Text;
            if (!string.IsNullOrEmpty(toMail))
            {
                #region  點數儲值

                //熟客點與公關點
                var orderMailInfo = new PcpPointTransactionOrderMailInformation
                {
                    SellerName = "山姆小舖",
                    ShowRegularsContent = true,
                    RegularsPoint = 3000,
                    FavorPoint = 1000
                };

                BonusFacade.SendPcpTransactionOrderMail(toMail, orderMailInfo);

                var favorOrderMailInfo = new PcpPointTransactionOrderMailInformation
                {
                    SellerName = "山姆小舖",
                    ShowRegularsContent = false,
                    RegularsPoint = 0,
                    FavorPoint = 1000
                };

                BonusFacade.SendPcpTransactionOrderMail(toMail, favorOrderMailInfo);


                #endregion 點數儲值


                #region  點數退款

                var refundMailInfo = new PcpPointTransactionRefundMailInformation
                {
                    SellerName = "山姆小舖",
                    RefundAmount = 333
                };

                BonusFacade.SendPcpTransactionRefundMail(toMail, refundMailInfo);

                #endregion 點數退款

                #region  超級紅利金請款

                var exchangeMailInfo = new PcpPointTransactionExchangeOrderMailInformation()
                {
                    SellerName = "山姆小舖",
                    ExchangeAmount = 1500
                };

                BonusFacade.SendPcpTransactionExchangeOrderMail(toMail, exchangeMailInfo);


                #endregion 超級紅利金請款

            }
        }

        protected void Button7_OnClick(object sender, EventArgs e)
        {
            LogManager.GetLogger(LineAppender._LINE).Info("Button7_OnClick");            
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('此功能異常，請洽IT處理');", true);
        }
        protected void RemoveCachebtn_OnClick(object sender, EventArgs e)
        {
            Cache.Remove(tbRemoveCache.Text);
        }

        protected void btnSkmBeaconLowPowerNotify_OnClick(object sender, EventArgs e)
        {
            SkmFacade.SkmBeaconLowPowerNotify();
        }

        protected void btnOneTimeEvent_Click(object sender, EventArgs e)
        {
            DataTable sendList = mp.GetOneTimeEventMailList();
            int count = 0;
            foreach (DataRow data in sendList.AsEnumerable())
            {
                if (data["member_mail"] != null && data["member_name"] != null)
                {
                    EventFacade.SendOneTimeEventdMail(data["member_mail"].ToString(), data["member_name"].ToString());
                    count++;
                }
            }
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('已發送" + count.ToString() + "封EDM');", true);
        }

        protected void btnTestOneTimeEvent_Click(object sender, EventArgs e)
        {
            EventFacade.SendOneTimeEventdMail(txtTestEvent.Text.Trim(), "測試測試");
        }

        protected void btnRefreshDistcountedPrice_Click(object sender, EventArgs e)
        {
            var deals = ViewPponDealManager.DefaultManager.ViewPponDealGetList(true, true);
            DiscountManager.RefreshDistcountedPrice(deals);
        }

        protected void frontDealsStat_Click(object sender, EventArgs e)
        {
            DateTime dateStart = (DateTime.Parse(frontDealsStatStart.Text)).Date;
            DateTime dateEnd = (DateTime.Parse(frontDealsStatEnd.Text)).Date.AddDays(1);

            List<FrontDealsDailyStat> frontDealsDailyStats = MarketingFacade.GetFrontDealsDailyStateByRange(dateStart, dateEnd);
            MarketingFacade.ImportFrontDealsDailyState(dateStart, dateEnd, frontDealsDailyStats);

            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('已完成');", true);
        }

        protected void runIChannelJob_Click(object sender, EventArgs e)
        {
            DateTime dateStart = (DateTime.Parse(IChannelStart.Text)).Date;
            DateTime dateEnd = (DateTime.Parse(IChannelEnd.Text)).Date.AddDays(1);

            OrderFacade.IChannelAPI(dateStart, dateEnd);

            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('已完成');", true);
        }
        protected void TestShopBackOrderAPI_Click(object sender, EventArgs e)
        {
            DateTime dateStart = (DateTime.Parse(ShopBackOrderStart.Text)).Date;
            DateTime dateEnd = (DateTime.Parse(ShopBackOrderEnd.Text)).Date.AddDays(1);

            var result = OrderFacade.ShopBackOrderAPI(dateStart, dateEnd);

            logger.Info(string.Format("ShopBackOrderAPI =>{0}", result));

            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('已完成');", true);
        }
        protected void TestShopBackValidationAPI_Click(object sender, EventArgs e)
        {
            DateTime dateStart = (DateTime.Parse(ShopBackValidationStart.Text)).Date;
            DateTime dateEnd = (DateTime.Parse(ShopBackValidationEnd.Text)).Date.AddDays(1);

            var result = OrderFacade.ShopBackValidationAPI(dateStart, dateEnd);

            logger.Info(string.Format("ShopBackValidationAPI =>{0}", result));

            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('已完成');", true);
        }

        protected void btnWmsOrder_Click(object sender, EventArgs e)
        {
            var readyTransferWmsOrders = WmsFacade.GetReadyTransferOfWmsOrders();
            foreach (var order in readyTransferWmsOrders)
            {
                try
                {
                    string content;

                    if (PchomeWmsAPI.AddOrder(order, out content))
                    {
                        var wo = _wp.WmsOrderGet(Guid.Parse(order.VendorOrderId));
                        wo.SyncTime = DateTime.Now;
                        wo.Status = (int)WmsOrderSendStatus.Success;
                        wo.WmsOrderId = order.Id;
                        _wp.WmsOrderSet(wo);
                    }
                    else
                    {
                        var wo = _wp.WmsOrderGet(Guid.Parse(order.VendorOrderId));
                        wo.SyncTime = DateTime.Now;
                        wo.Status = (int)WmsOrderSendStatus.failure;
                        wo.Message = content;
                        _wp.WmsOrderSet(wo);
                    }
                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("btnWmsOrder_Click error. order_guid={0}, ex={1}", order.VendorOrderId, ex);
                }
            }
        }

        protected void btnWmsShipStatus_Click(object sender, EventArgs e)
        {
            var readySyncShipStatusOfWmsOrders = WmsFacade.GetReadySyncShipStatusOfWmsOrders();
            foreach (var item in readySyncShipStatusOfWmsOrders)
            {
                try
                {
                    //查詢訂單
                    var wmsOrdersInfo = PchomeWmsAPI.GetOrder(item.WmsOrderId);

                    if (wmsOrdersInfo.ShipStatus == WmsOrderShipStatus.Init.ToString() || wmsOrdersInfo.ShipStatus == WmsOrderShipStatus.Progressing.ToString())
                    {
                        WmsFacade.UpdateWmsOrderByWmsOrderInfo(item.OrderGuid, (int)WmsDeliveryStatus.Processing, null, false);
                    }
                    else if (wmsOrdersInfo.ShipStatus == WmsOrderShipStatus.Shipped.ToString())
                    {
                        //查詢出貨進度:用多個訂單編號(限筆數)
                        List<WmsOrderProgressStatus> orderProgressStatus = PchomeWmsAPI.GetOrderProgressStatus(item.WmsOrderId); //一張訂單pchome可能分多次揀貨

                        if (WmsFacade.ProgressStatusNotChanged(item.OrderGuid ,orderProgressStatus) 
                            && (item.SendNotification || !WmsFacade.NeedNotifiedByProgressStatus(orderProgressStatus.First())))
                        {
                            continue;
                        }

                        //查詢出貨物流資訊
                        List<WmsLogistic> wmsLogistics = PchomeWmsAPI.GetLogistics(item.WmsOrderId);
                                                
                        List<CurrentWmsOrderProgress> currentWmsOrderProgresses
                            = WmsFacade.GetWmsOrderProgresses(orderProgressStatus, wmsLogistics, item.OrderGuid);

                        //取第一筆出貨單作為訂單狀態
                        var firstCurrentWmsOrderProgresses = currentWmsOrderProgresses.FirstOrDefault();

                        //要傳通知
                        bool wantToSend = firstCurrentWmsOrderProgresses != null 
                            && (!item.SendNotification
                            && (firstCurrentWmsOrderProgresses.CurrentProgressStatus == (int)WmsDeliveryStatus.Shipping
                            || firstCurrentWmsOrderProgresses.CurrentProgressStatus == (int)WmsDeliveryStatus.Missed));

                        if (wantToSend)
                        {
                            WmsFacade.SendShipmentNotification(item.OrderGuid, firstCurrentWmsOrderProgresses);
                        }

                        int wmsShipStatus = (int)WmsDeliveryStatus.Processing;
                        DateTime? wmsShipDate = null;
                        if (firstCurrentWmsOrderProgresses != null)
                        {
                            wmsShipStatus = firstCurrentWmsOrderProgresses.CurrentProgressStatus;
                            wmsShipDate = firstCurrentWmsOrderProgresses.ShipDate;
                        }

                        //更新出貨單
                        WmsFacade.UpdateWmsOrderShipByWmsOrderShipInfo(currentWmsOrderProgresses);
                        //更新訂單出貨狀態
                        WmsFacade.UpdateWmsOrderByWmsOrderInfo(item.OrderGuid, wmsShipStatus, wmsShipDate, wantToSend);
                    }
                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("btnWmsShipStatus_Click error. order_guid={0}, ex={1}", item.OrderGuid, ex);
                }
            }
        }

        protected void btnPchomeOrderInfoQuery_Click(object sender, EventArgs e)
        {
            var wmsOrdersInfo = PchomeWmsAPI.GetOrder(pchomeOrderId.Text);
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(wmsOrdersInfo);

            Response.Write(json);
        }

        protected void btnPchomeOrderShipInfoQuery_Click(object sender, EventArgs e)
        {
            List<WmsOrderProgressStatus> orderProgressStatus = PchomeWmsAPI.GetOrderProgressStatus(pchomeOrderId.Text);
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(orderProgressStatus);

            Response.Write(json);
        }

        protected void runPezChannelOrderJob_Click(object sender, EventArgs e)
        {
            DateTime dateStart = (DateTime.Parse(PayeasyStart.Text)).Date;
            DateTime dateEnd = (DateTime.Parse(PayeasyEnd.Text)).Date.AddDays(1);

            OrderFacade.PezChannelOrderAPI(dateStart, dateEnd);

            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('已完成');", true);
        }

        protected void btnPchomeSyncReturn_Click(object sender, EventArgs e)
        {
            DateTime sDate = default(DateTime);
            DateTime.TryParse(syncSDate.Text, out sDate);
            if (sDate == default(DateTime))
            {
                Response.Write("<script>alert('日期隔日錯誤')</script>");
            }
            else
            {
                SyncWmsReturnOrderFromPchomeJob.SyncWmsReturnOrderFromPchome(sDate);
            }
        }

        private static IFamiportProvider fami = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
        protected void btnExecuteOrderExg_Click(object sender, EventArgs e)
        {
            try
            {
                var orderGuid = Guid.Parse(OrderGuid.Text);
                List<FamiOrderExgParam> param =  fami.GetFamiOrderExgParams(orderGuid);
                foreach (var item in param)
                {
                    var pez = fami.GetPeztemp(item.PezCode);
                    FamiGroupCoupon.FamiportPincodeOrderExg(pez, item.OrderGuid, item.CouponId);
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("btnExecuteOrderExg_Click error. ex={0}", ex);
                Response.Write(ex.Message);
            }
        }
    }
}
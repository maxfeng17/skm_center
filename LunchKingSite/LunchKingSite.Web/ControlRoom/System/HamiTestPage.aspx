﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="HamiTestPage.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.HamiTestPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript">
        $(document).ready(function() {
            //傳送值及接數json回傳
            $("#bt4").click(function() {
                alert(2);
                $.ajax({
                    type: "POST",
                    url: "http://localhost:7479/WebService/PponDealService.asmx/PponDealDataJSon",
                    data: "UserId=40885",
                    dataType: "json",
                    success: function(response) {
                        alert(2);
                        $("#data").html(JSON.stringify(response));
                    }
                });
            }
            );

            JSON.stringify = JSON.stringify || function(obj) {
                var t = typeof(obj);
                if (t != "object" || obj === null) {
                    // simple data type
                    if (t == "string") obj = '"' + obj + '"';
                    return String(obj);
                } else {
                    // recurse array or object
                    var n, v, json = [], arr = (obj && obj.constructor == Array);
                    for (n in obj) {
                        v = obj[n];
                        t = typeof(v);
                        if (t == "string") v = '"' + v + '"';
                        else if (t == "object" && v !== null) v = JSON.stringify(v);
                        json.push((arr ? "" : '"' + n + '":') + String(v));
                    }
                    return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
                }
            };
        });        
    </script>
    <style type="text/css">
        fieldset div {
            padding:5px;
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td>
                優惠券關注+1
            </td>
            <td>
                <asp:Button ID="btnEventPageCount" Text="開始" runat="server" OnClick="btnEventPageCount_Click" />
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                測試HamiPpon
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="tbQueryDate" runat="server"></asp:TextBox>
                <asp:TextBox ID="tbResponseTime" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnMac" runat="server" Text="產生MAC" OnClick="btnMac_Click" />
                <asp:TextBox ID="tbMAC" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" id="bt4" value="傳送值及回傳json" />
                <div id="data">
                    123
                </div>
            </td>
        </tr>
        <tr>
            <td>
                測試產生HiDealCoupon
                <asp:TextBox ID="tbPid" runat="server"></asp:TextBox>
                <asp:Button ID="btnHiDealCoupon" runat="server" Text="HiDealCoupon" OnClick="btnHiDealCoupon_Click" />
            </td>
        </tr>
        <tr>
            <td>
                測試piinlife相關信件格式
                <asp:TextBox ID="tbPiinlifeSendTo" runat="server"></asp:TextBox>
                <asp:Button ID="btnPiinlifeSend" runat="server" Text="寄發" OnClick="btnPiinlifeSend_Click" />
            </td>
        </tr>
        <tr>
            <td>
                測試「付款成功通知信」發信功能
                <asp:TextBox ID="tbMailAddress" runat="server"></asp:TextBox>
                <asp:Button ID="testPponPaymentSuccessful" runat="server" Text="寄信" OnClick="testPponPaymentSuccessful_Click" />
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            檔次Id
                        </td>
                        <td>
                            <asp:TextBox ID="Sms_Hi_deal_id" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            分店GUID
                        </td>
                        <td>
                            <asp:TextBox ID="Sms_Hi_deal_store_guid" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="sendSms" runat="server" Text="重發品生活分店憑證簡訊" OnClick="sendSms_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="tbResult" runat="server" TextMode="MultiLine" Width="800" Height="500"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="msgText" runat="server" Text="Button" OnClick="msgText_Click" />
            </td>
        </tr>   
        <tr>
            <td>
                @收藏推播測試：<br />
                檔次到期日 ：<asp:TextBox ID="txtDue_Date" 
                    runat="server" 
                    Text="" 
                    Width="437px"></asp:TextBox><br />
                <asp:Button ID="btnPushMemberCollectDeal" runat="server" Text="收藏推播測試" OnClick="PushMemberCollectDeal_Click" />
            </td>
        </tr>
        <tr>
            <td>
                測試更新IDEAS Order資料：
                <asp:Button ID="Button1" runat="server" Text="更新" OnClick="TestIDEAS_Click" />
            </td>
        </tr>
        <tr>
            <td>
                測試更新IDEAS Deal資料：
                <asp:TextBox ID="tbIDEASDealId" runat="server" 
                    Text="" 
                    Width="389px"></asp:TextBox>
                <asp:Button ID="Button4" runat="server" Text="更新" OnClick="TestIDEASDeal_Click" />
            </td>
        </tr>
        <tr>
            <td>
                測試更新IDEAS Vourcher資料：
                <asp:TextBox ID="tbIDEASVourcherId" runat="server" 
                    Text="" 
                    Width="389px"></asp:TextBox>
                <asp:Button ID="Button5" runat="server" Text="更新" OnClick="TestIDEASVourcher_Click" />
            </td>
        </tr>

        <tr>
            <td>
                DataManagerResetJob：
                <asp:Button ID="Button2" runat="server" Text="執行" OnClick="DataManagerResetJobClick" />
            </td>
        </tr>  
        <tr>
            <td>
                CacheDataResetJob：
                <asp:Button ID="Button6" runat="server" Text="執行" OnClick="CacheDataResetJobClick" />
            </td>
        </tr> 
        <tr>
            <td>
                SendMgmGiftNoticeToReceiver：
                <asp:Button ID="Button8" runat="server" Text="執行" OnClick="SendMgmGiftNoticeToReceiverClick" />
            </td>
        </tr>    
        <tr>
            <td>
                商家轉單提醒信：
                <asp:Button ID="Button9" runat="server" Text="執行" OnClick="VbsCustomerServiceNoticeMailClick" />
            </td>
        </tr>                                
        <tr>
            <td>
                EventPromoJob：<br />
                <br />
                <asp:TextBox ID="JobBeginDate" ClientIDMode="Static" runat="server" Text="2013-09-26 00:00:00"></asp:TextBox>
                <br />
                <asp:TextBox ID="JobEndDate" ClientIDMode="Static" runat="server" Text="2013-09-27 00:00:00"></asp:TextBox>
                <br />
&nbsp;<asp:Button ID="Button3" runat="server" Text="執行" OnClick="Button3_Click" />
            </td>
        </tr>          
        <tr>
            <td>
            預約系統通知信功能測試：<asp:TextBox ID="bookingToMail" runat="server" Text=""></asp:TextBox>  <asp:Button ID="BookingButton" runat="server" Text="發信" OnClick="BookingButton_Click" />
            </td>
        </tr>
        <tr>
            <td>
            測試寄發資策會通知信：<asp:Button ID="IDEASMain" runat="server" Text="發信" OnClick="IDEASMain_Click" />
            </td>
        </tr>
        <tr>
            <td>
            點數交易通知信功能測試：<asp:TextBox ID="pcpPointToMail" runat="server" Text=""></asp:TextBox>  <asp:Button ID="PcpMailButton" runat="server" Text="發信" OnClick="PcpMailButton_Click" />
            </td>
        </tr>
        <tr>
            <td>
                測試更新商家資訊：
                <asp:Button ID="Button7" runat="server" Text="更新" OnClick="Button7_OnClick" />
            </td>
        </tr>
        <tr>
            <td>
                刪除特定Cache設定{Cache.Remove(Config.EdmPopUpCacheName)}：
                <asp:TextBox ID="tbRemoveCache" runat="server" Text="EdmPopUp"></asp:TextBox>
                <asp:Button ID="RemoveCachebtn" runat="server" Text="刪除" OnClick="RemoveCachebtn_OnClick" />
            </td>
        </tr>
        <tr>
            <td>
                SKMTestBeaconNotification：
                <asp:Button ID="btnSkmBeaconLowPowerNotify" runat="server" Text="發送" OnClick="btnSkmBeaconLowPowerNotify_OnClick" />
            </td>
        </tr>
        <tr>
            <td>
                一次性行銷EDM發送：
                <asp:Button ID="btnOneTimeEvent" runat="server" Text="發送" OnClick="btnOneTimeEvent_Click"/>&nbsp;&nbsp<asp:TextBox ID="txtTestEvent" runat="server" /><asp:Button ID="btnTestOneTimeEvent" runat="server" Text="測試發送" OnClick="btnTestOneTimeEvent_Click" />
            </td>
        </tr>
        <tr>
            <td>
                更新折後價：
                <asp:Button ID="btnRefreshDistcountedPrice" runat="server" Text="執行" OnClick="btnRefreshDistcountedPrice_Click"/>
            </td>
        </tr> 
       <tr>
            <td>
                測試對 Wms 下訂單(wms_order)：
                <asp:Button ID="btnWmsOrder" runat="server" Text="執行" OnClick="btnWmsOrder_Click"/>
            </td>
        </tr> 
        <tr>
            <td>
                測試同步Wms出貨狀態：
                <asp:Button ID="btnWmsShipStatus" runat="server" Text="執行" OnClick="btnWmsShipStatus_Click"/>
            </td>
        </tr>  
    </table>

    <a name="appabtest"></a>
    <fieldset style="margin-top:40px">
        <legend>APP首頁檔次列表-成效報表資料(填入該日期區間成效報表的資料內容，跟Job作的事相同)
        </legend>
        <div>
            <div>
                起始日期：<asp:TextBox ID="frontDealsStatStart" ClientIDMode="Static" runat="server" Text="2018/04/13"></asp:TextBox>
            </div>
            <div>
                終止日期： <asp:TextBox ID="frontDealsStatEnd" ClientIDMode="Static" runat="server" Text="2018/04/14"></asp:TextBox>
            </div>
            <div>
                <asp:Button ID="btnFrontDealsStat" runat="server" Text="執行" OnClick="frontDealsStat_Click" />
            </div>
        </div>
    </fieldset>

<fieldset style="margin-top:40px">
    <legend>測試IChannel Job(跟Job作的事相同)
    </legend>
    <div>
        <div>
            起始日期：<asp:TextBox ID="IChannelStart" ClientIDMode="Static" runat="server" Text="2018/08/01"></asp:TextBox>
        </div>
        <div>
            終止日期： <asp:TextBox ID="IChannelEnd" ClientIDMode="Static" runat="server" Text="2018/08/02"></asp:TextBox>
        </div>
        <div>
            <asp:Button ID="btnRunIChannelJob" runat="server" Text="執行" OnClick="runIChannelJob_Click" />
        </div>
    </div>
</fieldset>
<fieldset style="margin-top:40px">
    <legend>測試ShopBack Order API(跟Job作的事相同)
    </legend>
    <div>
        <div>
            起始日期：<asp:TextBox ID="ShopBackOrderStart" ClientIDMode="Static" runat="server" Text="2018/09/13"></asp:TextBox>
        </div>
        <div>
            終止日期： <asp:TextBox ID="ShopBackOrderEnd" ClientIDMode="Static" runat="server" Text="2018/09/14"></asp:TextBox>
        </div>
        <div>
            <asp:Button ID="btnCallShopBackOrderAPI" runat="server" Text="執行" OnClick="TestShopBackOrderAPI_Click" />
        </div>
    </div>
</fieldset>
<fieldset style="margin-top:40px">
    <legend>測試ShopBack Validation API(跟Job作的事相同)
    </legend>
    <div>
        <div>
            起始日期：<asp:TextBox ID="ShopBackValidationStart" ClientIDMode="Static" runat="server" Text="2018/09/13"></asp:TextBox>
        </div>
        <div>
            終止日期： <asp:TextBox ID="ShopBackValidationEnd" ClientIDMode="Static" runat="server" Text="2018/09/14"></asp:TextBox>
        </div>
        <div>
            <asp:Button ID="Button10" runat="server" Text="執行" OnClick="TestShopBackValidationAPI_Click" />
        </div>
    </div>
</fieldset>
<fieldset style="margin-top:40px">
    <legend>測試Payeasy Order Job(跟Job作的事相同)
    </legend>
    <div>
        <div>
            起始日期：<asp:TextBox ID="PayeasyStart" ClientIDMode="Static" runat="server" Text="2019/02/20"></asp:TextBox>
        </div>
        <div>
            終止日期： <asp:TextBox ID="PayeasyEnd" ClientIDMode="Static" runat="server" Text="2019/02/20"></asp:TextBox>
        </div>
        <div>
            <asp:Button ID="Button11" runat="server" Text="執行" OnClick="runPezChannelOrderJob_Click" />
        </div>
    </div>
</fieldset>
<fieldset style="margin-top:40px">
    <legend>PChome API查詢
    </legend>
    <div>
        <div>
            訂單編號：<asp:TextBox ID="pchomeOrderId" runat="server" Text=""></asp:TextBox>
        </div>
        <div>
            <asp:Button ID="btnPchomeOrderInfoQuery" runat="server" Text="3.2.5 查詢訂單資訊： 用訂單編號" OnClick="btnPchomeOrderInfoQuery_Click" />
            <br />
            <asp:Button ID="btnPchomeOrderShipInfoQuery" runat="server" Text="3.3.3 查詢出貨進度：用多個訂單編號(限筆數)" OnClick="btnPchomeOrderShipInfoQuery_Click" />
        </div>
    </div>
</fieldset>
<fieldset style="margin-top:40px">
    <legend>PChome24小，一次性同步訂單
    </legend>
    <div>
        <div>
            查詢時間：<asp:TextBox ID="syncSDate" runat="server" Text=""></asp:TextBox> ex:yyyy/MM/dd
        </div>
        <div>
            <asp:Button ID="btnPchomeSyncReturn" runat="server" Text="5.2.6 查詢還貨清單： 放入查詢起始時間" OnClick="btnPchomeSyncReturn_Click" />
        </div>
    </div>
</fieldset>
<fieldset style="margin-top:40px">
    <legend>測試全家(全網)Pincode換Barcode
    </legend>
    <div>
        <div>
            OrderGuid： <asp:TextBox ID="OrderGuid" ClientIDMode="Static" runat="server" Text=""></asp:TextBox>
        </div>
        <div>
            <asp:Button ID="btnExecuteOrderExg" runat="server" Text="執行" OnClick="btnExecuteOrderExg_Click" />
        </div>
    </div>
</fieldset>
</asp:Content>

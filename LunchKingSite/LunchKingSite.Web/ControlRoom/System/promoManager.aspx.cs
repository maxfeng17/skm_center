﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Web.Security;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class promoManager : RolePage
    {
        protected ICmsProvider cp;
        protected ISysConfProvider sp;

        protected void Page_Load(object sender, EventArgs e)
        {
            cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
            sp = ProviderFactory.Instance().GetConfig();
            LoadData();
        }

        protected void btnPromo_Click(object sender, EventArgs e)
        {
            // 驗證參數
            string webRoot = VirtualPathUtility.AppendTrailingSlash(HttpContext.Current.Request.ApplicationPath);
            string ContentName = webRoot + "ppon/promo.aspx_pMain_" + txtCn.Text.Trim();
            CmsContent article = cp.CmsContentGet(ContentName);
            if (article.ContentId != 0)
            {
                SetResultMessage("參數已存在!!");
            }
            else
            {
                article = new CmsContent();
                article.ContentName = ContentName;
                article.CreatedBy = Page.User.Identity.Name;
                article.CreatedOn = DateTime.Now;
                article.Locale = "zh_TW";
                cp.CmsContentSet(article);

                LoadData();
            }
        }

        protected void btnPiinlifePromo_Click(object sender, EventArgs e)
        {
            // 驗證參數
            string webRoot = VirtualPathUtility.AppendTrailingSlash(HttpContext.Current.Request.ApplicationPath);
            string ContentName = webRoot + "piinlife/promo.aspx_pMain_" + txtPiinlifeCn.Text.Trim();
            CmsContent article = cp.CmsContentGet(ContentName);
            if (article.ContentId != 0)
            {
                SetResultMessage("參數已存在!!");
            }
            else
            {
                article = new CmsContent();
                article.ContentName = ContentName;
                article.CreatedBy = Page.User.Identity.Name;
                article.CreatedOn = DateTime.Now;
                article.Locale = "zh_TW";
                cp.CmsContentSet(article);

                LoadData();
            }
        }

        public void LoadData()
        {
            lblMessage.Visible = false;
            gvPromoList.DataSource = cp.CmsContentGetList(-1, 0, CmsContent.Columns.CreatedOn + " desc", GetFilter(CmsContent.Columns.ContentName));
            gvPromoList.DataBind();
        }

        protected void gvPromoList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CmsContent RowView = (CmsContent)e.Row.DataItem;

                // 網址參數
                Label lblCn = (Label)e.Row.FindControl("lblCn");
                string cn = RowView.ContentName;
                lblCn.Text = cn.Substring(cn.IndexOf("pMain_") + 6);

                // 編輯器連結
                HyperLink hkConman = (HyperLink)e.Row.FindControl("hkConman");
                hkConman.Text = RowView.ContentName;
                hkConman.NavigateUrl = "../../ControlRoom/system/conman.aspx?id=" + RowView.ContentId;

                // 預覽頁面
                HyperLink hkPromo = (HyperLink)e.Row.FindControl("hkPromo");
                hkPromo.Text = "預覽";
                hkPromo.NavigateUrl = sp.SiteUrl.ToString() + RowView.ContentName.Substring(0, RowView.ContentName.IndexOf('_')).Replace("pMain_", string.Empty) + "?cn=" + lblCn.Text;
            }
        }

        protected void gvPromoList_RowCommand(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "d")
            {
                cp.CmsContentDelete(Convert.ToInt32(e.CommandArgument));
                LoadData();
            }
        }

        protected string[] GetFilter(object obj)
        {
            string[] filter = new string[1];
            filter[0] = "promo.aspx_pMain_";
            filter[0] = obj + " like %" + filter[0] + "%";
            return filter;
        }

        private void SetResultMessage(string resultMsg)
        {
            lblMessage.Visible = !String.IsNullOrEmpty(resultMsg);
            lblMessage.Text = resultMsg;
        }
    }
}
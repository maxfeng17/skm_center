﻿using System.Web.UI.WebControls;
using Autofac;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.Mongo.Facade;
using LunchKingSite.Mongo.Services;
using LunchKingSite.Web.Ppon;
using LunchKingSite.WebLib.WebModules;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using LunchKingSite.SsBLL;
using log4net;
using System.Text.RegularExpressions;
using System.Web.Services;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class DataManagerReset : RolePage
    {
        private static ISystemProvider ss;
        private static IPponProvider pp;
        private static IHumanProvider hp;
        private static ISysConfProvider cp;
        private static ILog logger = LogManager.GetLogger(typeof(DataManagerReset));

        protected int? CurrentTabPage;
        const int _TAB_SYSTEM = 8;
        const int _TAB_PPON_CACHE = 0;

        public DataManagerReset()
        {
            ss = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            cp = ProviderFactory.Instance().GetConfig();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var sysCode = ss.SystemCodeGetByCodeGroupId(SingleOperator.SingleJob.ToString(), 1);
                tbx_JobLoadBalance.Text = sysCode.CodeName;
                lab_JobModifyTime.Text = ((DateTime)sysCode.ModifyTime).ToString("yyyy/MM/dd HH:mm:ss");
                lab_JobModifyName.Text = sysCode.ModifyId;
                lab_JobLoadBalance.Text = Dns.GetHostName();
                SystemCodeCollection system_codes = ss.SystemCodeGetListByCodeGroup(SingleOperator.ImageUNC.ToString());
                if (system_codes.Count > 0)
                {
                    tbx_ImageLoadBalanceHost_1.Text = system_codes[0].ShortName;
                    tbx_ImageLoadBalance_1.Text = system_codes[0].CodeName;
                }
                if (system_codes.Count > 1)
                {
                    tbx_ImageLoadBalanceHost_2.Text = system_codes[1].ShortName;
                    tbx_ImageLoadBalance_2.Text = system_codes[1].CodeName;
                }

                SystemCode system_code = ss.SystemCodeGetByCodeGroupId(SingleOperator.ImpersonateAccount.ToString(), 1);
                tbx_ImpersonateUserName.Text = system_code.CodeName;
                tbx_ImpersonatePass.Text = system_code.ShortName;

                //cache init
                ICacheProvider cache = ProviderFactory.Instance().GetProvider<ICacheProvider>();
                txtCacheServer.Text = cache.GetServer();
                chkCacheServerEnabled.Checked = cp.CacheServerMode != 0;
                if (String.IsNullOrEmpty(txtCacheServer.Text))
                {
                    btnCacheStatus.Enabled = false;
                    btnCacheTest.Enabled = false;
                }

                //appversion
                rpt_AppVersion.DataSource = ApiSystemManager.GetAppVersionCollection();
                rpt_AppVersion.DataBind();

                btnMohistRedoProduct.CommandArgument = MohistRedoFlag.Product.ToString();
                btnMohistRedoDeal.CommandArgument = MohistRedoFlag.OrderDeal.ToString();
                btnMohistRedoVerify.CommandArgument = MohistRedoFlag.Confirm.ToString();
            }
        }

        protected void ClearServerCache(object sender, EventArgs e)
        {
            foreach (DictionaryEntry entry in System.Web.HttpRuntime.Cache)
            {
                System.Web.HttpRuntime.Cache.Remove((string)entry.Key);
            }
        }

        /// <summary>
        /// 好康檔次暫存資料(ViewPponDealManage)更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnViewPponDeal_Click(object sender, EventArgs e)
        {
            string output = JsonConvert.SerializeObject(new DataManagerResetViewModel
            {
                NameSpace = "LunchKingSite.BizLogic.Component",
                ClassName = "ViewPponDealManager",
                MethodName = "ReloadDefaultManager",
                Parameters = new object[] { true }
            });

            ss.DataManagerResettimeSetData(new DataManagerResettime
            {
                ManagerName = output,
                LastRunningTime = null,
                NextRunningTime = DateTime.Now,
                IntervalMinute = 0,
                Enabled = true,
                JobType = (int)DataManagerResettimeJobType.RunOnce,
                ServerName = null
            });

            //ViewPponDealManager.ReloadDefaultManager(true);
            //商品主題活動的資料重整與檔次資料的重整需一致
            //ApiPromoEventManager.ReloadDataManager();

            string output2 = JsonConvert.SerializeObject(new DataManagerResetViewModel
            {
                NameSpace = "LunchKingSite.BizLogic.Component.API",
                ClassName = "ApiPromoEventManager",
                MethodName = "ReloadDataManager",
                Parameters = null
            });

            ss.DataManagerResettimeSetData(new DataManagerResettime
            {
                ManagerName = output2,
                LastRunningTime = null,
                NextRunningTime = DateTime.Now,
                IntervalMinute = 0,
                Enabled = true,
                JobType = (int)DataManagerResettimeJobType.RunOnce,
                ServerName = null
            });

            if (cp.DataManagerResetImmediately)
            {
                ViewPponDealManager.ReloadDefaultManager(true);
                //商品主題活動的資料重整與檔次資料的重整需一致
                ApiPromoEventManager.ReloadDataManager();
            }

        }

        /// <summary>
        /// 好康檔次暫存資料(ViewPponDealManage)清空
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnViewPponDealClear_Click(object sender, EventArgs e)
        {
            string output = JsonConvert.SerializeObject(new DataManagerResetViewModel
            {
                NameSpace = "LunchKingSite.BizLogic.Component",
                ClassName = "ViewPponDealManager",
                MethodName = "ClearDefaultManager",
                Parameters = null
            });

            ss.DataManagerResettimeSetData(new DataManagerResettime
            {
                ManagerName = output,
                LastRunningTime = null,
                NextRunningTime = DateTime.Now,
                IntervalMinute = 0,
                Enabled = true,
                JobType = (int)DataManagerResettimeJobType.RunOnce,
                ServerName = null
            });

            if (cp.DataManagerResetImmediately)
            {
                ViewPponDealManager.ClearDefaultManager();
            }
        }

        protected void btnSellerActivity_Click(object sender, EventArgs e)
        {
            logger.Warn("btnSellerActivity_Click 功能暫停，請洽IT");
        }

        protected void btnActivityBase_Click(object sender, EventArgs e)
        {
            logger.Warn("btnActivityBase_Click 功能暫停，請洽IT");
        }

        protected void btnMongoSellerActivityListReset_Click(object sender, EventArgs e)
        {
            logger.Warn("btnMongoSellerActivityListReset_Click 功能暫停，請洽IT");
        }

        protected void btnHiDealProduct_Click(object sender, EventArgs e)
        {
            HiDealProductManager.ReloadDefaultManager();
        }
        /// <summary>
        /// 代碼檔資料(SystemCodeManager)更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCode_Click(object sender, EventArgs e)
        {

            string output = JsonConvert.SerializeObject(new DataManagerResetViewModel
            {
                NameSpace = "LunchKingSite.BizLogic.Component",
                ClassName = "SystemCodeManager",
                MethodName = "ReloadAllSystemCodeCollection",
                Parameters = null
            });

            ss.DataManagerResettimeSetData(new DataManagerResettime
            {
                ManagerName = output,
                LastRunningTime = null,
                NextRunningTime = DateTime.Now,
                IntervalMinute = 0,
                Enabled = true,
                JobType = (int)DataManagerResettimeJobType.RunOnce,
                ServerName = null
            });

            if (cp.DataManagerResetImmediately)
            {
                SystemCodeManager.ReloadAllSystemCodeCollection();
            }
        }
        /// <summary>
        /// 城市與鄉鎮市區(CityManage)更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCity_Click(object sender, EventArgs e)
        {
            string output = JsonConvert.SerializeObject(new DataManagerResetViewModel
            {
                NameSpace = "LunchKingSite.BizLogic.Component",
                ClassName = "CityManager",
                MethodName = "ReloadDefaultManager",
                Parameters = null
            });

            ss.DataManagerResettimeSetData(new DataManagerResettime
            {
                ManagerName = output,
                LastRunningTime = null,
                NextRunningTime = DateTime.Now,
                IntervalMinute = 0,
                Enabled = true,
                JobType = (int)DataManagerResettimeJobType.RunOnce,
                ServerName = null
            });

            if (cp.DataManagerResetImmediately)
            {
                CityManager.ReloadDefaultManager();
            }

        }
        /// <summary>
        /// 好康分店資料(ViewPponStoreManage)更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnViewPponStore_Click(object sender, EventArgs e)
        {
            string output = JsonConvert.SerializeObject(new DataManagerResetViewModel
            {
                NameSpace = "LunchKingSite.BizLogic.Component",
                ClassName = "ViewPponStoreManager",
                MethodName = "ReloadDefaultManager",
                Parameters = null
            });

            ss.DataManagerResettimeSetData(new DataManagerResettime
            {
                ManagerName = output,
                LastRunningTime = null,
                NextRunningTime = DateTime.Now,
                IntervalMinute = 0,
                Enabled = true,
                JobType = (int)DataManagerResettimeJobType.RunOnce,
                ServerName = null
            });

            if (cp.DataManagerResetImmediately)
            {
                ViewPponStoreManager.ReloadDefaultManager();
            }
        }

        /// <summary>
        /// 好康分店資料(ViewPponStoreManage)清空
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnViewPponStoreC_Click(object sender, EventArgs e)
        {
            string output = JsonConvert.SerializeObject(new DataManagerResetViewModel
            {
                NameSpace = "LunchKingSite.BizLogic.Component",
                ClassName = "ViewPponStoreManager",
                MethodName = "ClearDefaultManager",
                Parameters = null
            });

            ss.DataManagerResettimeSetData(new DataManagerResettime
            {
                ManagerName = output,
                LastRunningTime = null,
                NextRunningTime = DateTime.Now,
                IntervalMinute = 0,
                Enabled = true,
                JobType = (int)DataManagerResettimeJobType.RunOnce,
                ServerName = null
            });

            if (cp.DataManagerResetImmediately)
            {
                ViewPponStoreManager.ClearDefaultManager();
            }
        }
        /// <summary>
        /// API的使用者(ApiUserManage)更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnApiUser_Click(object sender, EventArgs e)
        {
            string output = JsonConvert.SerializeObject(new DataManagerResetViewModel
            {
                NameSpace = "LunchKingSite.BizLogic.Component",
                ClassName = "ApiUserManager",
                MethodName = "ReloadDefaultManager",
                Parameters = null
            });

            ss.DataManagerResettimeSetData(new DataManagerResettime
            {
                ManagerName = output,
                LastRunningTime = null,
                NextRunningTime = DateTime.Now,
                IntervalMinute = 0,
                Enabled = true,
                JobType = (int)DataManagerResettimeJobType.RunOnce,
                ServerName = null
            });

            if (cp.DataManagerResetImmediately)
            {
                ApiUserManager.ReloadDefaultManager();
            }
        }
        /// <summary>
        /// Category(CategoryManage)更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCategory_Click(object sender, EventArgs e)
        {
            CategoryManager.UpdateCategoryDbExtendedProperty();

            string output = JsonConvert.SerializeObject(new DataManagerResetViewModel
            {
                NameSpace = "LunchKingSite.BizLogic.Component",
                ClassName = "CategoryManager",
                MethodName = "ReloadManager",
                Parameters = null
            });

            ss.DataManagerResettimeSetData(new DataManagerResettime
            {
                ManagerName = output,
                LastRunningTime = null,
                NextRunningTime = DateTime.Now,
                IntervalMinute = 0,
                Enabled = true,
                JobType = (int)DataManagerResettimeJobType.RunOnce,
                ServerName = null
            });

            if (cp.DataManagerResetImmediately)
            {
                logger.InfoFormat("只是提醒:{0} 即時觸發 CategoryManager.ReloadManager", User.Identity.Name);
                CategoryManager.ReloadManager();
            }
            else
            {
                logger.InfoFormat("只是提醒:{0} 排程觸發 CategoryManager.ReloadManager", User.Identity.Name);
            }
        }
        /// <summary>
        /// 檔次分類相關資料(PponDealPreviewManager)更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPponDealPreview_Click(object sender, EventArgs e)
        {

            string output = JsonConvert.SerializeObject(new DataManagerResetViewModel
            {
                NameSpace = "LunchKingSite.BizLogic.Component",
                ClassName = "PponDealPreviewManager",
                MethodName = "ReloadManager",
                Parameters = null
            });

            ss.DataManagerResettimeSetData(new DataManagerResettime
            {
                ManagerName = output,
                LastRunningTime = null,
                NextRunningTime = DateTime.Now,
                IntervalMinute = 0,
                Enabled = true,
                JobType = (int)DataManagerResettimeJobType.RunOnce,
                ServerName = null
            });

            if (cp.DataManagerResetImmediately)
            {
                PponDealPreviewManager.ReloadManager();
            }
        }

        protected void btnHiDealCoupon_Click(object sender, EventArgs e)
        {
            int productId;
            if (int.TryParse(tbhdcProductId.Text, out productId))
            {
                HiDealCouponManager.ClearProductCouponData(productId);
            }
        }

        protected void btnHiDealDeal_Click(object sender, EventArgs e)
        {
            HiDealDealManager.ClearManagerData();
        }

        protected void btnCancelPiinlifeOrder_Click(object sender, EventArgs e)
        {
            HiDealOrderManager.CancelConfirmHiDealOrderListBeforeDatetime(DateTime.Now.AddMinutes(-30));
        }

        protected void GetViewPponDealManagerSummary(object sender, EventArgs e)
        {
            Dictionary<string, List<ViewPponDealManagerSummary>> summary = ViewPponDealManager.DefaultManager.GetViewPponDealManagerSummary();
            StringBuilder builder = new StringBuilder();
            builder.Append("<table><tr><td>種類</td><td>數量</td><td>Bid</td><td>CityId</td><td>CategoryId</td></tr>");
            foreach (var item in summary)
            {
                builder.Append("<tr><td>" + item.Key + "</td><td>" + item.Value.Count + "</td><td></td><td></td><td></td></tr>");
                foreach (var innitem in item.Value)
                {
                    builder.Append("<tr><td></td><td></td><td>" + innitem.Bid + "</td><td>" + innitem.CityId + "</td><td>" + innitem.CategoryId + "</td></tr>");
                }
            }
            builder.Append("</table>");
            lit_Summary.Text = builder.ToString();
        }
        protected void GetApiEventData(object sender, EventArgs e)
        {
            this.CurrentTabPage = _TAB_PPON_CACHE;
            if (chkSyncAllServersForEvent.Checked)
            {
                SortedDictionary<string, string> serverIPs =
                    ProviderFactory.Instance().GetSerializer().Deserialize<SortedDictionary<string, string>>(cp.ServerIPs);
                string result = SystemFacade.SyncCommand(serverIPs, SystemFacade._API_PROMO_EVENT_MANAGER, string.Empty, string.Empty);
                litTabPponCacheMessage.Text = result.Replace("\r\n", "<br />");
            }
            else
            {
                ApiPromoEventManager.LoadDataManager(true);
                litTabPponCacheMessage.Text = "更新完成";
            }
        }

        protected void btnCategoryList_Click(object sender, EventArgs e)
        {
            pp.UpdateCategoryDealsWithCategoryList();
        }

        protected void btnMohistVerifyMemo_Click(object sender, EventArgs e)
        {
            OrderFacade.UpdateMohistVerifyInfoStatus(txtCouponId.Text.Trim(), txtSequenceNumber.Text.Trim(), txtMemo.Text.Trim());
        }

        protected void btnUploadMohistProduct_Click(object sender, EventArgs e)
        {
            DateTime dateStart;
            DateTime dateEnd;
            DateTime.TryParse(txtMohistDateStart.Text, out dateStart);
            DateTime.TryParse(txtMohistDateEnd.Text, out dateEnd);

            for (int i = 0; i < (dateEnd - dateStart).Days; i++)
            {
                OrderFacade.UploadMohistProduct(dateStart.AddDays(i), dateStart.AddDays(i + 1));
            }
        }

        protected void btnSaveMohistVerifyInfo_Click(object sender, EventArgs e)
        {
            DateTime dateStart;
            DateTime dateEnd;
            DateTime.TryParse(txtMohistDateStart.Text, out dateStart);
            DateTime.TryParse(txtMohistDateEnd.Text, out dateEnd);

            OrderFacade.SaveMohistVerifyInfo(dateStart, dateEnd);
        }

        protected void btnUpdateMohistVerifyInfoStatusByPeriod_Click(object sender, EventArgs e)
        {
            DateTime dateStart;
            DateTime dateEnd;
            DateTime.TryParse(txtMohistDateStart.Text, out dateStart);
            DateTime.TryParse(txtMohistDateEnd.Text, out dateEnd);

            OrderFacade.UpdateMohistVerifyInfoStatusByPeriod(dateStart, dateEnd);
        }

        protected void btnUploadMohistDeals_Click(object sender, EventArgs e)
        {
            DateTime dateStart;
            DateTime dateEnd;
            DateTime.TryParse(txtMohistDateStart.Text, out dateStart);
            DateTime.TryParse(txtMohistDateEnd.Text, out dateEnd);

            for (int i = 0; i < (dateEnd - dateStart).Days; i++)
            {
                OrderFacade.UploadMohistDeals(dateStart.AddDays(i), dateStart.AddDays(i + 1));
            }
        }

        protected void btnUploadMohistConfirms_Click(object sender, EventArgs e)
        {
            DateTime dateStart;
            DateTime dateEnd;
            DateTime.TryParse(txtMohistDateStart.Text, out dateStart);
            DateTime.TryParse(txtMohistDateEnd.Text, out dateEnd);

            for (int i = 0; i < (dateEnd - dateStart).Days; i++)
            {
                OrderFacade.UploadMohistConfirms(dateStart.AddDays(i), dateStart.AddDays(i + 1));
            }
        }

        protected void btnQueryBusinessOrder_Click(object sender, EventArgs e)
        {
            SalesService _salServ = ServiceLocator.Container.Resolve<SalesService>();
            List<IMongoQuery> queryList = new List<IMongoQuery>();
            if (!string.IsNullOrEmpty(txtStatus.Text))
            {
                int status = int.TryParse(txtStatus.Text, out status) ? status : 0;
                queryList.Add(Query.GTE("Status", new BsonInt32(Convert.ToInt32(status))));
            }
            if (!string.IsNullOrEmpty(txtCreateId.Text))
            {
                queryList.Add(Query.EQ("CreateId", new BsonString(txtCreateId.Text)));
            }
            if (!string.IsNullOrEmpty(txtBusinessOrderSequenceNumber.Text))
            {
                int sequenceNumber = int.TryParse(txtBusinessOrderSequenceNumber.Text, out sequenceNumber) ? sequenceNumber : 0;
                queryList.Add(Query.EQ("SequenceNumber", new BsonInt32(sequenceNumber)));
            }
            if (!string.IsNullOrWhiteSpace(txtBusinessOrderModifyTimeS.Text) && !string.IsNullOrWhiteSpace(txtBusinessOrderModifyTimeE.Text))
            {
                DateTime dateS = DateTime.TryParse(txtBusinessOrderModifyTimeS.Text, out dateS) ? dateS : DateTime.MaxValue;
                DateTime dateE = DateTime.TryParse(txtBusinessOrderModifyTimeE.Text, out dateE) ? dateE : DateTime.MaxValue;
                queryList.Add(Query.And(Query.GTE("ModifyTime", new BsonDateTime(dateS)), Query.LT("ModifyTime", new BsonDateTime(dateE))));
            }
            IEnumerable<BusinessOrder> businessOrderList = _salServ.BusinessOrderGetList(Query.And(queryList.ToArray()));

            StringBuilder sb = new StringBuilder();
            foreach (BusinessOrder item in businessOrderList)
            {
                sb.AppendLine(item.ToJson());
            }
            Context.Response.Clear();
            Context.Response.ContentEncoding = Encoding.UTF8;
            Context.Response.ContentType = "text/octet-stream";
            Context.Response.AddHeader("Content-Disposition", "attachment; filename=businessOrderList.txt");
            Response.BinaryWrite(Encoding.UTF8.GetBytes(sb.ToString()));
            Response.End();
        }

        protected void BusinessOrderReferred(object sender, EventArgs e)
        {
            ViewEmployee emp = hp.ViewEmployeeGet(ViewEmployee.Columns.Email, txtBusinessOrderCreateId.Text.Trim());
            ViewEmployee ref_emp = hp.ViewEmployeeGet(ViewEmployee.Columns.Email, txtBusinessOrderReferredEmail.Text.Trim());
            if (!emp.IsLoaded)
            {
                AlertMessage("原業務帳號錯誤!!");
            }
            else if (!ref_emp.IsLoaded)
            {
                AlertMessage("轉業務帳號錯誤!!");
            }
            else
            {
                int referred = SalesFacade.ReferredAllBusinessOrderByEmail(txtBusinessOrderCreateId.Text, txtBusinessOrderReferredEmail.Text);
                AlertMessage(string.Format("轉件筆數 :{0}筆", referred));
            }
        }

        protected void SalesDeptIdReferred(object sender, EventArgs e)
        {
            SalesService _salServ = ServiceLocator.Container.Resolve<SalesService>();

            int referred = 0;
            List<IMongoQuery> queryList = new List<IMongoQuery>();
            if (!string.IsNullOrEmpty(txtSalesDeptId.Text) && !string.IsNullOrEmpty(txtReferredSalesDeptId.Text))
            {
                EmployeeChildDept dept = EmployeeChildDept.S001;
                if (EmployeeChildDept.TryParse(txtSalesDeptId.Text.Trim(), out dept))
                {
                    Department dep = hp.DepartmentGet(dept.ToString());
                    if (dep.IsLoaded)
                    {
                        queryList.Add(Query.EQ("SalesDeptId", new BsonString(dep.DeptId)));
                        IEnumerable<BusinessOrder> businessOrderList = _salServ.BusinessOrderGetList(Query.And(queryList.ToArray()));
                        foreach (BusinessOrder order in businessOrderList)
                        {
                            EmployeeChildDept refDept = EmployeeChildDept.S001;
                            if (EmployeeChildDept.TryParse(txtReferredSalesDeptId.Text.Trim(), out refDept))
                            {
                                Department refDep = hp.DepartmentGet(refDept.ToString());
                                if (refDep.IsLoaded)
                                {
                                    order.AuthorizationGroup = refDep.DeptId;
                                    order.SalesDeptId = refDep.DeptId;
                                    order.HistoryList.Add(new History() { Id = ObjectId.GenerateNewId(), Changeset = string.Format("由{0}轉區至{1}", dep.DeptName, refDep.DeptName), ModifyId = "sys", ModifyTime = DateTime.Now });
                                    _salServ.SaveBusinessOrder(order);
                                    referred++;
                                }
                            }
                        }
                        AlertMessage(string.Format("轉區筆數 :{0}筆", referred));
                    }
                }
            }
        }

        protected void btnDownLoadContractDataList_Click(object sender, EventArgs e)
        {
            int month = int.TryParse(txtBusinessOrderMonth.Text, out month) ? month : -2;
            List<string> dataList = SalesFacade.GetFilterDataList(month);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("工單編號,賣家名稱,業務,業務部門,上檔日期,建立日期,送審天數,核准天數,複查天數,排檔天數,建檔天數, 草稿到建檔總計天數");
            foreach (string item in dataList)
            {
                sb.AppendLine(item);
            }
            Context.Response.Clear();
            Context.Response.ContentEncoding = Encoding.UTF8;
            Context.Response.ContentType = "text/octet-stream";
            Context.Response.AddHeader("Content-Disposition", "attachment; filename=BusinessOrderCourse.txt");
            Response.BinaryWrite(Encoding.UTF8.GetBytes(sb.ToString()));
            Response.End();
        }

        private void SendEmail(List<string> mailToUser, string subject, string body)
        {
            foreach (string user in mailToUser)
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(cp.SystemEmail);
                msg.To.Add(user);
                msg.Subject = subject;
                msg.Body = body;
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }

        public void UpdateFamilyPrint(object sender, EventArgs e)
        {
            if (FileUpload_Family.HasFile)
            {
                DateTime now = DateTime.Now;
                HttpPostedFile file = FileUpload_Family.PostedFile;
                using (Stream filestream = file.InputStream)
                {
                    using (StreamReader reader = new StreamReader(filestream, Encoding.GetEncoding(950)))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            if (line.Length == 60)
                            {
                                DateTime print_time;
                                string code = line.Substring(0, 10);
                                string print = line.Substring(10, 14);
                                int cvsid = PponFacade.CvsIdGet(line.Substring(24, 6));
                                if (cvsid == 0)
                                {
                                    PponFacade.PeztempErrorLogInsert(code, line.Substring(24, 6), 0);
                                }
                                else if (DateTime.TryParseExact(print, "yyyyMMddHHmmss", new CultureInfo("zh-TW", true), DateTimeStyles.None, out print_time))
                                {
                                    if (PponFacade.PeztempValidator(code))
                                    {
                                        PponFacade.PeztempUpdatePrintTime(code, print_time, cvsid);
                                    }
                                    else
                                    {
                                        PponFacade.PeztempErrorLogInsert(code, line.Substring(24, 6), 1);
                                    }
                                }
                            }
                        }
                    }
                }
                AlertMessage("更新完成!!");
            }
            else
            {
                AlertMessage("檔案不存在!!");
            }
        }

        public void UpdateFamilyPrintJob(object sender, EventArgs e)
        {
            DateTime print_date;
            if (DateTime.TryParse(tbx_FamilyPrintDate.Text, out print_date))
            {
                int total = OrderFacade.FamiUpdatePrintDate(print_date);
                AlertMessage("已更新" + total + "!!");
            }
            else
                AlertMessage("請輸入日期!!");
        }

        private void AlertMessage(string message, int? tab_index = null)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", (tab_index.HasValue ? "$(document).ready(function() { changetab(" + tab_index + "); });" : string.Empty) + "alert('" + message + "');"
                , true);
        }

        protected void UpdateImageLoadBalance(object sender, EventArgs e)
        {
            SystemCodeCollection system_codes = ss.SystemCodeGetListByCodeGroup(SingleOperator.ImageUNC.ToString());
            SystemCode system_code;
            if (system_codes.Count > 0)
            {
                system_code = system_codes[0];
                system_code.ShortName = tbx_ImageLoadBalanceHost_1.Text;
                system_code.CodeName = tbx_ImageLoadBalance_1.Text;
                system_code.ModifyId = User.Identity.Name;
                system_code.ModifyTime = DateTime.Now;
            }
            else
            {
                system_code = new SystemCode();
                system_code.CodeGroup = SingleOperator.ImageUNC.ToString();
                system_code.CodeGroupName = "圖片上傳LoadBalance";
                system_code.CodeId = 1;
                system_code.CodeName = tbx_ImageLoadBalance_1.Text;
                system_code.Enabled = true;
                system_code.CreateId = User.Identity.Name;
                system_code.CreateTime = DateTime.Now;
                system_code.ShortName = tbx_ImageLoadBalanceHost_1.Text;
            }
            ss.SystemCodeSet(system_code);

            if (system_codes.Count > 1)
            {
                system_code = system_codes[1];
                system_code.ShortName = tbx_ImageLoadBalanceHost_2.Text;
                system_code.CodeName = tbx_ImageLoadBalance_2.Text;
                system_code.ModifyId = User.Identity.Name;
                system_code.ModifyTime = DateTime.Now;
            }
            else
            {
                system_code = new SystemCode();
                system_code.CodeGroup = SingleOperator.ImageUNC.ToString();
                system_code.CodeGroupName = "圖片上傳LoadBalance";
                system_code.CodeId = 2;
                system_code.CodeName = tbx_ImageLoadBalance_2.Text;
                system_code.Enabled = true;
                system_code.CreateId = User.Identity.Name;
                system_code.CreateTime = DateTime.Now;
                system_code.ShortName = tbx_ImageLoadBalanceHost_2.Text;
            }
            ss.SystemCodeSet(system_code);
        }

        protected void UpdateJobLoadBalance(object sender, EventArgs e)
        {
            SystemCode system_code = ss.SystemCodeGetByCodeGroupId(SingleOperator.SingleJob.ToString(), 1);
            string original = system_code.CodeName;
            if (string.IsNullOrEmpty(system_code.CodeGroup))
            {
                system_code.CodeGroup = SingleOperator.SingleJob.ToString();
                system_code.CodeGroupName = "單一JobLoadBalance HostName";
                system_code.CodeId = 1;
                system_code.Enabled = true;
                system_code.CreateId = User.Identity.Name;
                system_code.CreateTime = DateTime.Now;
                system_code.ShortName = "單一JobLoadBalance";
            }
            else
            {
                system_code.ModifyId = User.Identity.Name;
                system_code.ModifyTime = DateTime.Now;
            }
            system_code.CodeName = tbx_JobLoadBalance.Text;
            ss.SystemCodeSet(system_code);
            lab_JobModifyTime.Text = ((DateTime)system_code.ModifyTime).ToString("yyyy/MM/dd HH:mm:ss");
            lab_JobModifyName.Text = system_code.ModifyId;
            CommonFacade.AddAudit(Guid.Empty, AuditType.Controlroom, string.Format("job load balance has been changed from {0} to {1}", original, tbx_JobLoadBalance.Text), User.Identity.Name, false);
        }

        protected void UpdateImpersonateAccount(object sender, EventArgs e)
        {
            SystemCode system_code = ss.SystemCodeGetByCodeGroupId(SingleOperator.ImpersonateAccount.ToString(), 1);
            if (string.IsNullOrEmpty(system_code.CodeGroup))
            {
                system_code.CodeGroup = SingleOperator.ImpersonateAccount.ToString();
                system_code.CodeGroupName = "網路芳鄰登入帳密";
                system_code.CodeId = 1;
                system_code.Enabled = true;
                system_code.CreateId = User.Identity.Name;
                system_code.CreateTime = DateTime.Now;
            }
            else
            {
                system_code.ModifyId = User.Identity.Name;
                system_code.ModifyTime = DateTime.Now;
            }
            system_code.CodeName = tbx_ImpersonateUserName.Text;
            system_code.ShortName = tbx_ImpersonatePass.Text;
            ss.SystemCodeSet(system_code);
        }

        protected void UpdateVourcherCityBit(object sender, EventArgs e)
        {
            ViewVourcherSellerCollection vourchers = VourcherFacade.VourcherEventCollectionGetBySearch(-1, -1, new KeyValuePair<string, string>(string.Empty, string.Empty), string.Empty, -1);
            foreach (var vourcher in vourchers)
            {
                int city_bit = 0;
                ViewVourcherStoreCollection stores = VourcherFacade.ViewVourcherStoreCollectionGetByEventId(vourcher.Id);
                foreach (var store in stores)
                {
                    City city = CityManager.CityGetById(store.CityId ?? 0);
                    if (city != null && (city.BitNumber & city_bit) == 0)
                        city_bit += city.BitNumber;
                }
                VourcherEvent vourcher_event = VourcherFacade.VourcherEventGetById(vourcher.Id);
                vourcher_event.CityBit = city_bit;
                VourcherFacade.VourcherEventSet(vourcher_event);
            }
        }

        protected void GetSerialBid(object sender, EventArgs e)
        {
            lab_SerialKeyGet.Text = string.Empty;
            string error = string.Empty;
            Guid bid;
            if (Guid.TryParse(tbx_SerialBid.Text, out bid))
            {
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                if ((deal.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0)
                {
                    Peztemp pez = pp.PeztempGet(bid);
                    if (pez.Id != 0)
                    {
                        lab_SerialKeyGet.Text = pez.PezCode;
                        pez.OrderDetailGuid = new Guid("00000000-0000-0000-0000-000000000001");
                        pp.PeztempSet(pez);
                    }
                    else
                        error = "已無剩餘序號";
                }
                else
                    error = "此檔非廠商提供序號檔次";
            }
            else
                error = "Bid錯誤";

            if (!string.IsNullOrEmpty(error))
                AlertMessage(error);
        }

        protected void ApiSellerSampleCategoryClear(object sender, EventArgs e)
        {
            string output = JsonConvert.SerializeObject(new DataManagerResetViewModel
            {
                NameSpace = "LunchKingSite.BizLogic.Component.API",
                ClassName = "ApiVourcherManager",
                MethodName = "ApiSellerSampleCategoryClear",
                Parameters = null
            });

            ss.DataManagerResettimeSetData(new DataManagerResettime
            {
                ManagerName = output,
                LastRunningTime = null,
                NextRunningTime = DateTime.Now,
                IntervalMinute = 0,
                Enabled = true,
                JobType = (int)DataManagerResettimeJobType.RunOnce,
                ServerName = null
            });

            if (cp.DataManagerResetImmediately)
            {
                ApiVourcherManager.ApiSellerSampleCategoryClear();
            }

        }

        protected void GenPponCoupon(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            int max_count;
            if (int.TryParse(tbx_GenPponCouponCount.Text, out max_count))
            {
                Guid orderId = Guid.NewGuid();
                string userName = cp.AdminEmail;
                for (int i = 1; i <= max_count; i++)
                {
                    MersenneTwister mt = new MersenneTwister((uint)(orderId + userName).GetHashCode());
                    uint hash = mt.NextUIntAtOrderN(1000, 9999, (uint)i);
                    string sequence_number = string.Format("{0}{1:0000}-{2:####}", string.Empty, i, hash);
                    mt = new MersenneTwister((uint)(sequence_number + orderId).GetHashCode());
                    string code = mt.Next(100000, 999999).ToString(); // 6 digits
                    sb.AppendLine(sequence_number + "-" + code);
                }
            }
            Context.Response.Clear();
            Context.Response.ContentEncoding = Encoding.GetEncoding(0);
            Context.Response.ContentType = "text/octet-stream";
            Context.Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("Coupon-yyyyMMdd-HHmmss") + ".txt");
            Response.BinaryWrite(Encoding.UTF8.GetBytes(sb.ToString()));
            Response.End();
        }

        protected void CouponPrint(object sender, EventArgs e)
        {
            Response.Redirect("~/Ppon/coupon_print.aspx" + string.Format("?bid={0}&sequencenumber={1}&couponcode={2}&membername={3}&cid={4}", tbx_CouponBid.Text, tbx_CouponSequenceNumber.Text, tbx_CouponCouponCode.Text, tbx_CouponMemberName.Text, tbx_CouponCouponId.Text));
        }

        protected void CouponDownLoad(object sender, EventArgs e)
        {
            Response.Redirect("~/User/CouponGet.aspx" + string.Format("?bid={0}&sequencenumber={1}&couponcode={2}&membername={3}&cid={4}", tbx_CouponBid.Text, tbx_CouponSequenceNumber.Text, tbx_CouponCouponCode.Text, tbx_CouponMemberName.Text, tbx_CouponCouponId.Text));
        }
        protected void btnCommercialExchangeExchange_Click(object sender, EventArgs e)
        {
            DateTime dateStart;
            DateTime.TryParse(txtCommercialExchangeDateStart.Text, out dateStart);
            DateTime dateEnd;
            DateTime.TryParse(txtCommercialExchangeDateEnd.Text, out dateEnd);
            PromotionFacade.GenerateEventPromoItemByPeriod(txtEventCode.Text.Trim(), dateStart, dateEnd);
        }

        /// <summary>
        /// 重新讀取AppSystemManager的資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReloadAppSystemManager(object sender, EventArgs e)
        {
            this.CurrentTabPage = _TAB_SYSTEM;
            if (chkSyncAllServers.Checked)
            {
                SortedDictionary<string, string> serverIPs =
                    ProviderFactory.Instance().GetSerializer().Deserialize<SortedDictionary<string, string>>(cp.ServerIPs);
                string result = SystemFacade.SyncCommand(serverIPs, SystemFacade._API_SYSTEM_MANAGER, string.Empty, string.Empty);
                litTabCacheMessage.Text = result.Replace("\r\n", "<br />");
            }
            else
            {
                ApiSystemManager.ReloadManagerData();
                litTabCacheMessage.Text = "更新完成";
            }
        }

        protected void btnCacheTest_Click(object sender, EventArgs e)
        {
            ICacheProvider cache = ProviderFactory.Instance().GetProvider<ICacheProvider>();
            //拿全國宅配來測
            var deals = ViewPponDealManager.DefaultManager.ViewPponDealGetListForPponByCityWithDataSlot(
                PponCityGroup.DefaultPponCityGroup.AllCountry.CityId);
            int testSecs = 1;
            {
                DateTime now = DateTime.Now;
                int count = 0;
                while (true)
                {
                    cache.Set("allcountry_one", deals[0], new TimeSpan(0, 5, 0));
                    count++;
                    if ((DateTime.Now - now).TotalSeconds > testSecs)
                    {
                        break;
                    }
                }
                litCacheSpeed.Text = string.Format("測試 one by one: 寫入 {0}", count / testSecs);
            }
            {
                DateTime now = DateTime.Now;
                int count = 0;
                while (true)
                {
                    ViewPponDeal vpd = cache.Get<ViewPponDeal>("allcountry_one");
                    if (vpd != null)
                    {
                        count++;
                    }
                    if ((DateTime.Now - now).TotalSeconds > testSecs)
                    {
                        break;
                    }
                }
                litCacheSpeed.Text += string.Format(" 讀取 {0}", count / testSecs);
            }
            testSecs = 2;
            {
                DateTime now = DateTime.Now;
                int count = 0;
                while (true)
                {
                    bool result = cache.Set("allcountry_all", deals, new TimeSpan(0, 5, 0));
                    if (result)
                    {
                        count = count + deals.Count;
                    }
                    if ((DateTime.Now - now).TotalSeconds > testSecs)
                    {
                        break;
                    }
                }
                litCacheSpeed.Text += string.Format("<br />測試 package: 寫入 {0}", count / testSecs);
            }
            {
                DateTime now = DateTime.Now;
                int count = 0;
                while (true)
                {
                    ViewPponDealCollection vpdCol = cache.Get<ViewPponDealCollection>("allcountry_all");
                    if (vpdCol != null)
                    {
                        count = count + vpdCol.Count;
                    }
                    if ((DateTime.Now - now).TotalSeconds > testSecs)
                    {
                        break;
                    }
                }
                litCacheSpeed.Text += string.Format(" 讀取 {0}", count / testSecs);
            }

            testSecs = 2;
            {
                DateTime now = DateTime.Now;
                int count = 0;
                while (true)
                {
                    bool result = cache.Set("allcountry_all", deals, new TimeSpan(0, 5, 0), true);
                    if (result)
                    {
                        count = count + deals.Count;
                    }
                    if ((DateTime.Now - now).TotalSeconds > testSecs)
                    {
                        break;
                    }
                }
                litCacheSpeed.Text += string.Format("<br />測試 compressed package: 寫入 {0}", count / testSecs);
            }
            {
                DateTime now = DateTime.Now;
                int count = 0;
                while (true)
                {
                    ViewPponDealCollection vpdCol = cache.Get<ViewPponDealCollection>("allcountry_all", true);
                    if (vpdCol != null)
                    {
                        count = count + vpdCol.Count;
                    }
                    if ((DateTime.Now - now).TotalSeconds > testSecs)
                    {
                        break;
                    }
                }
                litCacheSpeed.Text += string.Format(" 讀取 {0}", count / testSecs);
            }


            Random rand = new Random();

            testSecs = 2;
            {
                DateTime now = DateTime.Now;
                int count = 0;

                while (true)
                {
                    byte[] buffer = new byte[20480];
                    rand.NextBytes(buffer);

                    cache.Set("random_test_data", buffer, new TimeSpan(0, 5, 0));
                    count = count + 1;
                    if ((DateTime.Now - now).TotalSeconds > testSecs)
                    {
                        break;
                    }
                }
                litCacheSpeed.Text += string.Format("<br />測試 random 20k bytes: 寫入 {0}", count / testSecs);
            }
            {
                DateTime now = DateTime.Now;
                int count = 0;
                while (true)
                {
                    byte[] buffer = cache.Get<byte[]>("random_test_data");
                    if (buffer == null)
                    {
                        break;
                    }
                    count = count + 1;
                    if ((DateTime.Now - now).TotalSeconds > testSecs)
                    {
                        break;
                    }
                }
                litCacheSpeed.Text += string.Format(" 讀取 {0}", count / testSecs);
            }
            testSecs = 2;
            {
                DateTime now = DateTime.Now;
                int count = 0;

                while (true)
                {
                    byte[] buffer = new byte[1024 * 1024];
                    rand.NextBytes(buffer);

                    cache.Set("random_test_data_1mb", buffer, new TimeSpan(0, 5, 0));
                    count = count + 1;
                    if ((DateTime.Now - now).TotalSeconds > testSecs)
                    {
                        break;
                    }
                }
                litCacheSpeed.Text += string.Format("<br />測試 random 1mb: 寫入 {0}", count / testSecs);
            }
            {
                DateTime now = DateTime.Now;
                int count = 0;
                while (true)
                {
                    byte[] buffer = cache.Get<byte[]>("random_test_data_1mb");
                    if (buffer == null)
                    {
                        break;
                    }
                    count = count + 1;
                    if ((DateTime.Now - now).TotalSeconds > testSecs)
                    {
                        break;
                    }
                }
                litCacheSpeed.Text += string.Format(" 讀取 {0}", count / testSecs);
            }
            this.ClientScript.RegisterStartupScript(this.GetType(), "changetab", "$(document).ready(function() { changetab(7); });", true);
            //cache.Reset();
        }

        protected void btnCacheStatus_Click(object sender, EventArgs e)
        {
            ICacheProvider cache = ProviderFactory.Instance().GetProvider<ICacheProvider>();
            CacheServerStatus status = cache.GetStatus();
            litCacheStatus.Mode = LiteralMode.PassThrough;
            int timeDiffSec = (int)(status.ServerTime - DateTime.Now).TotalSeconds;
            if (status == CacheServerStatus.Offline)
            {
                litCacheStatus.Text = "連線失敗";
            }
            else
            {
                litCacheStatus.Text = string.Format("使用: {0}<br />啟動: {1} 天<br />時間: {2} {3} 秒<br />",
                    status.ToString(),
                    (int)status.Uptime.TotalDays,
                    timeDiffSec > 0 ? "快" : "慢",
                    Math.Abs(timeDiffSec));
            }
            this.ClientScript.RegisterStartupScript(this.GetType(), "changetab",
                "$(document).ready(function() { changetab(7); });", true);
        }

        protected void ResetPponDealIndex(object sender, EventArgs e)
        {
            if (cp.SearchEngine == 0)
            {
                throw new Exception("請洽IT");
            }
            else
            {
                string msg;
                SystemFacade.RefreshExternalSearchEngine(User.Identity.Name, out msg);
            }
        }

        protected void ClearBlackIps(object sender, EventArgs e)
        {
            DefenseModule.ClearBlackIps();
            this.ClientScript.RegisterStartupScript(this.GetType(), "changetab",
                "$(document).ready(function() { changetab(8); });", true);
        }
        protected void GaUpdate(object sender, EventArgs e)
        {
            var success = true;

            foreach (var item in Enum.GetValues(typeof(GaViewIDMap)))
            {
                success &= AnalyticsFacade.GaUpdate(((int)item).ToString(), GaStartDate.Text, GaEndDate.Text
                    , new GaQueryMatrics().GetQueryProperty());
            }

            if (success)
            { AlertMessage("更新成功。", 9); }
            else
            { AlertMessage("更新失敗。", 9); }
        }

        protected void ViewSubCategoryDependencyCollectionClear_Click(object sender, EventArgs e)
        {
            PponFacade.ViewSubCategoryDependencyCollectionClear();
        }

        private string GetImageSize(string size)
        {
            Double s;
            var result = "0 byte";
            if (!Double.TryParse(size, out s)) return result;

            if ((s / 1024 / 1024) > 1)
            {
                s = s / 1024 / 1024;
                result = string.Format("{0} MB", s.ToString("f2"));
            }
            else if ((s / 1024) > 1)
            {
                s = s / 1024;
                result = string.Format("{0} KB", s.ToString("f2"));
            }
            else
            {
                result = string.Format("{0} byte", s.ToString("f2"));
            }

            return result;
        }

        protected void UpdateAppVersionCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "UpdateAppVersion")
            {
                string appName = e.CommandArgument.ToString();

                string latestVersion = ((TextBox)e.Item.FindControl("tbx_latest_version")).Text;
                string minVersion = ((TextBox)e.Item.FindControl("tbx_min_version")).Text;
                string osVersion = ((TextBox)e.Item.FindControl("tbx_os_version")).Text;
                string osMinVersion = ((TextBox)e.Item.FindControl("tbx_os_min_version")).Text;
                string appConfig = ((TextBox)e.Item.FindControl("tbx_app_config")).Text;


                string errMsg = string.Empty;

                //驗證 App 的定義檔，是否符合json格式
                try
                {
                    ProviderFactory.Instance().GetSerializer().DeserializeDynamic(appConfig);
                }
                catch
                {
                    errMsg += "App定義檔Json序列化格式錯誤。\\n";
                }

                if (appName.Contains("TemporaryData"))
                {
                    DateTime upDataTime;
                    if (!DateTime.TryParse(latestVersion, out upDataTime))
                    {
                        if (upDataTime.Minute != 0)
                        {
                            errMsg += "更新時間格式錯誤。\\n";
                        }
                    }
                }
                else
                {
                    Version verLatestVersion;
                    Version verMinVersion;
                    Version verOSVersion;
                    Version verOSMinVersion;
                    if ((Version.TryParse(latestVersion, out verLatestVersion) && Version.TryParse(minVersion, out verMinVersion)
                        && Version.TryParse(osVersion, out verOSVersion) && Version.TryParse(osMinVersion, out verOSMinVersion)) == false)
                    {
                        errMsg += "輸入的版本格式錯誤。\\n";
                    }
                }

                if (string.IsNullOrEmpty(errMsg))
                {
                    AppVersion version = new AppVersion()
                    {
                        AppName = appName,
                        LatestVersion = latestVersion,
                        MinimumVersion = minVersion,
                        OsVersion = osVersion,
                        OsMinimumVersion = osMinVersion,
                        AppConfig = appConfig
                    };
                    ss.UpdateAppVersion(version);
                    AlertMessage("更新成功。", 10);
                }
                else
                {
                    AlertMessage(errMsg, 10);
                }
            }
        }

        protected void OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is AppVersion)
            {
                AppVersion data = (AppVersion)e.Item.DataItem;
                if (data.DeviceType == 9527)
                {
                    var litText = (Literal)e.Item.FindControl("litText");
                    var latestVersion = (TextBox)e.Item.FindControl("tbx_latest_version");
                    var minVersion = (TextBox)e.Item.FindControl("tbx_min_version");
                    var osVersion = (TextBox)e.Item.FindControl("tbx_os_version");
                    var osminVersion = (TextBox)e.Item.FindControl("tbx_os_min_version");
                    var upDataBtn = (Button)e.Item.FindControl("btn_AppVersion");
                    litText.Text = @"預定更新時間: ";
                    if (string.IsNullOrEmpty(latestVersion.Text))
                    {
                        latestVersion.Text = DateTime.Now.ToString("yyyy-MM-dd HH:00");
                    }
                    minVersion.Visible = false;
                    osVersion.Visible = false;
                    osminVersion.Visible = false;
                    upDataBtn.Text = @"暫存";
                }
            }
        }


        protected void RedoUpload(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string CouponId = txtCouponId.Text.Trim();
            string SequenceNumber = txtSequenceNumber.Text.Trim();
            MohistRedoFlag flag = (MohistRedoFlag)Enum.Parse(typeof(MohistRedoFlag), btn.CommandArgument);
            OrderFacade.UpdateMohistVerifyInfoRedo(CouponId, SequenceNumber, flag);
        }

        protected void UpdateCouponEventContent(object sender, EventArgs e)
        {
            logger.WarnFormat("UpdateCouponEventContent 被 {0} 執行", User.Identity.Name);
            throw new Exception("功能不被支援，如果有任何疑問，請洽IT");
        }

        [WebMethod]
        public static string GetSystemDataData(string name)
        {
            ISystemProvider ss = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            var sd = ss.SystemDataGet(name);
            return JsonConvert.SerializeObject(JsonConvert.DeserializeObject(sd.Data), Formatting.Indented);
        }


        [WebMethod]
        public static string UpdateSystemDataData(string name, string data)
        {
            ISystemProvider ss = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            var sd = ss.SystemDataGet(name);
            if (sd.IsLoaded == false)
            {
                throw new Exception(string.Format("資料 {0} 不存在.", name));
            }
            
            try
            {
                data = JsonConvert.SerializeObject(JsonConvert.DeserializeObject(data), Formatting.Indented);
            }
            catch (Exception ex)
            {
                Match match = Regex.Match(ex.Message, @"[\w+ ]line ([\d+]), position ([\d+])");
                if (match.Success)
                {
                    throw new Exception(string.Format("不符合JSON格式. 錯誤於 {0}行, 第{1}字元",
                        match.Groups[1].Value, match.Groups[2].Value));
                }
                throw new Exception(string.Format("不符合JSON格式. ex={0}", ex));
            }

            try
            {
                sd.Data = data;
                ss.SystemDataSet(sd);
            }
            catch
            {
                throw new Exception("資料存入失敗.");
            }

            return string.Empty;
        }
    }
}
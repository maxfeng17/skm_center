﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class SystemFunctionAdd : RolePage, ISystemFunctionAddView
    {
        #region props
        private SystemFunctionAddPresenter _presenter;
        public SystemFunctionAddPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public string LoginUser
        {
            get
            {
                return User.Identity.Name;
            }
        }
        public int PageSize
        {
            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }
        }
        public string DisplayNameSearch
        {
            get { return txtDisplayNameSearch.Text.Trim(); }
        }
        public string FuncNameSearch
        {
            get { return txtFuncNameSearch.Text.Trim(); }
        }
        public string ParentFuncSearch
        {
            get { return txtParentFuncSearch.Text.Trim(); }
        }
        #endregion

        #region event
        public event EventHandler<EventArgs> SearchClicked;
        public event EventHandler<DataEventArgs<int>> OnGetCount;
        public event EventHandler<DataEventArgs<int>> PageChanged;
        public event EventHandler<DataEventArgs<int>> GetSystemFunctionData;
        public event EventHandler<DataEventArgs<int>> SystemFunctionDelete;
        public event EventHandler<DataEventArgs<List<SystemFunction>>> SystemFunctionSave;
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnFunctionAdd_Click(object sender, EventArgs e)
        {
            hidId.Value = string.Empty;
            divFunctionAdd.Visible = true;
            FuncName.Visible = false;
            txtFuncName.Text = string.Empty;
            txtSortOrder.Text = string.Empty;
            txtLink.Text = string.Empty;
            txtDisplayName.Text = string.Empty;
            ParentFuncName.Visible = false;
            txtParentFunc.Text = string.Empty;
            txtParentSortOrder.Text = string.Empty;
            foreach (ListItem item in chklFuncType.Items)
            {
                item.Enabled = true;
            }
            txtFuncType.Text = string.Empty;
            txtFuncType.Enabled = true;
            txtTypeDesc.Text = string.Empty;
            btnDelete.Visible = false;
            chkIsVisible.Checked = true;
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            divFunctionAdd.Visible = false;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.SystemFunctionSave != null)
            {
                this.SystemFunctionSave(this, new DataEventArgs<List<SystemFunction>>(SystemFunctGetInfo()));
            }

            divFunctionAdd.Visible = false;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.SystemFunctionDelete != null)
            {
                int funcId = int.TryParse(hidId.Value, out funcId) ? funcId : 0;
                this.SystemFunctionDelete(this, new DataEventArgs<int>(funcId));
                divFunctionAdd.Visible = false;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.SearchClicked != null)
                this.SearchClicked(this, e);
        }

        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChanged != null)
                this.PageChanged(this, new DataEventArgs<int>(pageNumber));

            ucPager.ResolvePagerView(pageNumber, true);
        }

        protected int GetCount()
        {
            if (IsPostBack)
            {
                DataEventArgs<int> e = new DataEventArgs<int>(0);
                if (OnGetCount != null)
                    OnGetCount(this, e);
                return e.Data;
            }
            else
                return 0;
        }

        protected void gvFunctionList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int id;
            int.TryParse(e.CommandArgument.ToString(), out id);
            switch (e.CommandName.ToString())
            {
                case "UPD":
                    if (this.GetSystemFunctionData != null)
                    {
                        this.GetSystemFunctionData(this, new DataEventArgs<int>(id));
                        btnDelete.Visible = true;
                        FuncName.Visible = true;
                        ParentFuncName.Visible = true;
                    }
                    break;
                default:
                    break;
            }
        }
        #endregion

        #region method
        public void GetSystemFunctionList(SystemFunctionCollection dataList)
        {
            gvFunctionList.DataSource = dataList;
            gvFunctionList.DataBind();

            ucPager.ResolvePagerView(1, true);
        }

        public void SetSystemFunctioData(SystemFunction func)
        {
            divFunctionAdd.Visible = true;
            hidId.Value = func.Id.ToString();
            txtFuncName.Text = func.FuncName;
            txtLink.Text = func.Link;
            txtDisplayName.Text = func.DisplayName;
            foreach (ListItem item in chklFuncType.Items)
            {
                item.Selected = item.Value.Equals(func.FuncType, StringComparison.OrdinalIgnoreCase);
                item.Enabled = false;
            }
            if (chklFuncType.Items.FindByValue(func.FuncType) == null)
            {
                txtFuncType.Text = func.FuncType;
                txtTypeDesc.Text = func.TypeDesc;
            }
            chkIsVisible.Checked = func.Visible;
            txtFuncType.Enabled = false;
            txtParentFunc.Text = func.ParentFunc;
            txtSortOrder.Text = func.SortOrder.ToString();
            txtParentSortOrder.Text = func.ParentSortOrder.ToString();
            chkEIPUsed.Checked = func.EipUsed ?? false;
            txtMenuIcon.Text = func.MenuIcon;
        }

        public void ShowMessage(string msg)
        {
            ScriptManager.RegisterStartupScript(Page, typeof(Button), "alert", "alert('" + msg + "');", true);
        }
        #endregion

        #region private method
        private List<SystemFunction> SystemFunctGetInfo()
        {
            List<SystemFunction> funcList = new List<SystemFunction>();
            Dictionary<string, string> funcTypeList = chklFuncType.Items.Cast<ListItem>().Where(item => item.Selected).ToDictionary(x => x.Value, x => x.Text);
            if (!string.IsNullOrWhiteSpace(txtFuncType.Text))
            {
                funcTypeList.Add(txtFuncType.Text.Trim(), txtTypeDesc.Text.Trim());
            }

            foreach (KeyValuePair<string, string> funcType in funcTypeList)
            {
                SystemFunction func = new SystemFunction();
                int id = 0;
                int.TryParse(hidId.Value, out id);
                func.Id = id;
                func.FuncName = txtFuncName.Text.Trim();
                func.Link = txtLink.Text.Trim();
                func.DisplayName = txtDisplayName.Text.Trim();
                func.Visible = chkIsVisible.Checked;
                func.FuncType = funcType.Key;
                func.TypeDesc = funcType.Value;
                func.ParentFunc = txtParentFunc.Text.Trim();
                int sortOrder = 0;
                int.TryParse(txtSortOrder.Text.Trim(), out sortOrder);
                func.SortOrder = sortOrder;
                int parentSortOrder = 0;
                int.TryParse(txtParentSortOrder.Text.Trim(), out parentSortOrder);
                func.ParentSortOrder = parentSortOrder;
                func.EipUsed = chkEIPUsed.Checked;
                func.MenuIcon = txtMenuIcon.Text;

                funcList.Add(func);
            }
            return funcList;
        }
        #endregion
    }
}
﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using System;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class PEZHtmlCombiner : RolePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            IMarketingProvider mp = ProviderFactory.Instance().GetProvider<IMarketingProvider>();
            Repeater1.DataSource = mp.PezEdmUploadGetListTop50();
            Repeater1.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            #region GetContent

            IMarketingProvider mp = ProviderFactory.Instance().GetProvider<IMarketingProvider>();
            string content1 = mp.PezEdmUploadGet(int.Parse(txt1.Text)).EdmContent;
            string content2 = mp.PezEdmUploadGet(int.Parse(txt2.Text)).EdmContent;
            string content3 = mp.PezEdmUploadGet(int.Parse(txt3.Text)).EdmContent;
            string content4 = mp.PezEdmUploadGet(int.Parse(txt4.Text)).EdmContent;

            #endregion GetContent

            #region 組合Html

            string strResult = @"<table width=""740"" style=""background-color: #ffe9f6"" border=""0"" cellspacing=""0"" cellpadding=""0"">
        <tbody>
            <tr>
                <td align=""center"">
                    <table width=""726"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                        <tbody>
                            <tr>
                                <td style=""background-color:#FFC4D9; vertical-align:top"">
                                    <table width=""363"" style=""background-color: #ffc4d9"" border=""0"" align=""center"" cellpadding=""0""
                                        cellspacing=""0"">
                                        <tbody>
                                            <tr>
                                                <td height=""10"">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align=""center"">
                                                    " + content1 + @"
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height=""10"">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style=""background-color:#FFC4D9; vertical-align:top"">
                                    <table width=""363"" style=""background-color: #ffc4d9"" border=""0"" align=""center"" cellpadding=""0""
                                        cellspacing=""0"">
                                        <tbody>
                                            <tr>
                                                <td height=""10"">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align=""center"">
                                                    " + content2 + @"
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height=""10"">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table width=""726"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                        <tbody>
                            <tr>
                                <td style=""background-color:#FFC4D9; vertical-align:top"">
                                    <table width=""363"" style=""background-color: #ffc4d9"" border=""0"" align=""center"" cellpadding=""0""
                                        cellspacing=""0"">
                                        <tbody>
                                            <tr>
                                                <td align=""center"">
                                                    " + content3 + @"
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height=""10"">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style=""background-color:#FFC4D9; vertical-align:top"">
                                    <table width=""363"" style=""background-color: #ffc4d9"" border=""0"" align=""center"" cellpadding=""0""
                                        cellspacing=""0"">
                                        <tbody>
                                            <tr>
                                                <td align=""center"">
                                                    " + content4 + @"
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height=""10"">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td height=""15"">
                </td>
            </tr>
        </tbody>
    </table>";

            #endregion 組合Html

            lblResult.Text = strResult;
            txtOutput.Text = strResult;
        }

        protected void Repeater1_ItemCommand(object source, System.Web.UI.WebControls.RepeaterCommandEventArgs e)
        {
            IMarketingProvider mp = ProviderFactory.Instance().GetProvider<IMarketingProvider>();
            mp.PezEdmUploadDelete(int.Parse(e.CommandArgument.ToString()));
            LoadData();
        }
    }
}
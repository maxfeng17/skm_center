﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/ControlRoom/backend.master"
    CodeBehind="CreditCardTest.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.CreditCardTest" %>

<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
    <script type="text/javascript">
        $(function () {
            $(".cc").bind("keyup", function () { checkCardNo(this); }).css("text-align", "center").attr("autocomplete", "off");
            $("#<%= SecurityCode.ClientID %>").attr("autocomplete", "off");
        });

        function checkCardNo(tb) {
            var maxLength = $(tb).attr('maxlength');
            var reg = new RegExp("\\D");
            if (reg.test($(tb).val())) {
                $(tb).val($(tb).val().replace(reg, ''));
            }

            if ($(tb).val().length == maxLength) {
                var cc = $(".cc");
                cc.eq((cc.index($(tb))) + 1).focus();
            }
        }
    </script>
    <style media="screen" type="text/css">
        .result, .result tr td {
            text-align: center;
            border: 2px solid black;
            border-collapse: collapse;
            padding: 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div>
        ＊測試元件時直接按下&nbsp;<asp:Button ID="QuickTest" runat="server" Text="測試" OnClick="QuickTest_Click" />&nbsp;即可。<asp:Button ID="BruteForceTest" runat="server" Text="Don't Click" OnClick="BruteForceTest_Click" />
    </div>
    <fieldset>
        <legend>刷卡設定</legend>
        <div>
            <ul style="list-style-type: none;">
                <li>Server：<asp:Label ID="ServerName" runat="server"></asp:Label>
                </li>
                <li>系統廠商：<asp:DropDownList ID="ServiceProvider" runat="server">
                    <asp:ListItem Value="HiTrust">網際威信</asp:ListItem>
                    <asp:ListItem Value="Neweb">藍新</asp:ListItem>
                </asp:DropDownList></li>
                <li>HiTrust 元件：
                    <asp:RadioButton ID="HiTrustComComponent" runat="server" Checked="True" GroupName="HiTrustComponent" Text="Com" />
                    <asp:RadioButton ID="HiTrustDotNetComponent" runat="server" GroupName="HiTrustComponent" Text=".Net" />
                </li>
                <li>特店代號：<asp:DropDownList ID="MerchantID" runat="server">
                    <asp:ListItem Value="Ppon">P 好康</asp:ListItem>
                    <asp:ListItem Value="PiinLife">品生活</asp:ListItem>
                    <asp:ListItem Value="PezTravel">P 旅遊</asp:ListItem>
                    <asp:ListItem Value="UnionPay">銀聯</asp:ListItem>
                </asp:DropDownList>
                </li>
                <li>信用卡號：
                    <asp:TextBox ID="CardNo1" runat="server" CssClass="cc" Style="width: 45px;" MaxLength="4"></asp:TextBox>&nbsp;-&nbsp;<asp:TextBox
                        ID="CardNo2" runat="server" CssClass="cc" Style="width: 45px;" MaxLength="4"></asp:TextBox>&nbsp;-&nbsp;<asp:TextBox
                            ID="CardNo3" runat="server" CssClass="cc" Style="width: 45px;" MaxLength="4"></asp:TextBox>&nbsp;-&nbsp;<asp:TextBox
                                ID="CardNo4" runat="server" CssClass="cc" Style="width: 45px;" MaxLength="4"></asp:TextBox>
                </li>
                <li>有效期限：
                    <label>
                        <asp:DropDownList ID="ExpMonth" runat="server" />
                    </label>
                    月/20
                    <label>
                        <asp:DropDownList ID="ExpYear" runat="server" />
                    </label>
                    年 </li>
                <li>背面三碼：
                    <asp:TextBox ID="SecurityCode" runat="server" Style="width: 45px;" MaxLength="3"></asp:TextBox>
                </li>
                <li>
                    <asp:Button ID="RealCardTest" runat="server" Text="實際卡號測試" OnClick="RealCardTest_Click" />&nbsp;&nbsp;
                    <asp:Button ID="TestCardTest" runat="server" Text="測試卡號測試" OnClick="TestCardTest_Click" />
                    <asp:Button ID="TestCardTest2" runat="server" Text="測試分期卡號測試" OnClick="TestCardTest2_Click" />
                </li>
            </ul>
        </div>
    </fieldset>
    <fieldset>
        <legend>測試結果</legend>
        <table class="result">
            <asp:Literal ID="TestResult" runat="server"></asp:Literal>
        </table>
    </fieldset>
</asp:Content>

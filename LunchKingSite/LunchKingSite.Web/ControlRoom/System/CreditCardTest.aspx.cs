﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class CreditCardTest : RolePage
    {
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static string resultHeader = "<tr><td colspan='6'><strong>#provider</strong></td></tr><tr><td>交易類別</td><td>交易結果</td><td>回傳碼</td><td>結果訊息</td><td>銀行授權碼</td><td>交易時間</td></tr>";
        private static StringBuilder sb = new StringBuilder();

        #region settings

        #region HiTrust

        private readonly string hitrustTestCardMerchantId = "Test";

        private static CreditCardAuthObject hitrustTestAuthObject = new CreditCardAuthObject()
        {
            Amount = 1,
            CardNumber = config.DefaultTestCreditcard.Replace("-", string.Empty),
            ExpireYear = config.DefaultTestCreditcardExpireDate.Split('/')[0],
            ExpireMonth = config.DefaultTestCreditcardExpireDate.Split('/')[1],
            SecurityCode = config.DefaultTestCreditcardSecurityCode,
            Description = "Testing Component"
        };

        private static CreditCardAuthObject hitrustTestAuthObject2 = new CreditCardAuthObject()
        {
            Amount = 3001,
            CardNumber = config.DefaultTestCreditcard.Replace("-",string.Empty),
            ExpireYear = config.DefaultTestCreditcardExpireDate.Split('/')[0],
            ExpireMonth = config.DefaultTestCreditcardExpireDate.Split('/')[1],
            SecurityCode = config.DefaultTestCreditcardSecurityCode,
            InstallmentPeriod = 12,
            Description = "Testing Component"
        };

        #endregion HiTrust

        #region Neweb

        private readonly string newebTestCardMerchantId = "759851";

        private static CreditCardAuthObject newebTestAuthObject = new CreditCardAuthObject()
                {
                    Amount = 1,
                    CardNumber = "4344117339451014",
                    ExpireYear = "20",
                    ExpireMonth = "12",
                    SecurityCode = "000",
                    Description = "Testing Component"
                };

        #endregion Neweb

        private static CreditCardAuthObject bfAuthObject = new CreditCardAuthObject()
         {
             Amount = 1,
             CardNumber = "8888880000000006",
             ExpireYear = "16",
             ExpireMonth = "04",
             SecurityCode = "036",
             Description = "Testing Component"
         };

        #endregion settings

        protected void Page_Load(object sender, EventArgs e)
        {
            ServerName.Text = Environment.MachineName;
            setupDropdown();
        }

        protected void QuickTest_Click(object sender, EventArgs e)
        {
            TestResult.Text = string.Empty;
            TestResult.Text += testHiTrust(hitrustTestAuthObject, hitrustTestCardMerchantId);
            TestResult.Text += testNeweb(newebTestAuthObject, newebTestCardMerchantId);
        }

        protected void TestCardTest_Click(object sender, EventArgs e)
        {
            TestResult.Text = string.Empty;

            switch (ServiceProvider.SelectedValue)
            {
                case "HiTrust":
                    TestResult.Text += testHiTrust(hitrustTestAuthObject, hitrustTestCardMerchantId);
                    break;

                case "Neweb":
                    TestResult.Text += testNeweb(newebTestAuthObject, newebTestCardMerchantId);
                    break;

                default:
                    break;
            }
        }

        protected void TestCardTest2_Click(object sender, EventArgs e)
        {
            TestResult.Text = string.Empty;

            switch (ServiceProvider.SelectedValue)
            {
                case "HiTrust":
                    TestResult.Text += testHiTrust(hitrustTestAuthObject2, hitrustTestCardMerchantId);
                    break;

                case "Neweb":
                    TestResult.Text += testNeweb(newebTestAuthObject, newebTestCardMerchantId);
                    break;

                default:
                    break;
            }
        }

        protected void RealCardTest_Click(object sender, EventArgs e)
        {
            TestResult.Text = string.Empty;
            CreditCardAuthObject authObject = new CreditCardAuthObject()
                    {
                        Amount = 1,
                        CardNumber = CardNo1.Text + CardNo2.Text + CardNo3.Text + CardNo4.Text,
                        ExpireYear = ExpYear.SelectedValue,
                        ExpireMonth = ExpMonth.SelectedValue,
                        SecurityCode = SecurityCode.Text,
                        Description = "Testing Component"
                    };

            switch (ServiceProvider.SelectedValue)
            {
                case "HiTrust":
                    TestResult.Text += testHiTrust(authObject, MerchantID.SelectedValue);
                    break;

                case "Neweb":
                    TestResult.Text += testNeweb(authObject, MerchantID.SelectedValue);
                    break;

                default:
                    break;
            }
        }

        private string testHiTrust(CreditCardAuthObject authObject, string merchantID)
        {
            CreditCardOrderSource source = CreditCardOrderSource.HiTrustComTest;
            switch (merchantID)
            {
                case "PiinLife":
                    source = CreditCardOrderSource.PiinLife;
                    break;

                case "PezTravel":
                    source = CreditCardOrderSource.PayeasyTravel;
                    break;

                case "Ppon":
                case "UnionPay":
                    source = CreditCardOrderSource.Ppon;
                    break;

                case "Test":
                    if (HiTrustDotNetComponent.Checked)
                    {
                        source = CreditCardOrderSource.HiTrustDotNetTest;
                    }
                    break;
                default:
                    break;
            }

            authObject.TransactionId = "htest" + Path.GetRandomFileName();
            CreditCardAuthResult result = CreditCardUtility.Authenticate(authObject, Guid.NewGuid(), config.AdminEmail, OrderClassification.LkSite, source);

            string log = resultHeader.Replace("#provider", result.APIProvider.ToString());

            // auth
            log += "<tr><td>授權</td><td>" + result.TransPhase.ToString() +
                "</td><td>" + result.ReturnCode +
                "</td><td>" + result.TransMessage +
                "</td><td>" + result.AuthenticationCode +
                "</td><td>" + result.OrderDate +
                "</td></tr>";

            return log;
        }

        private string testNeweb(CreditCardAuthObject authObject, string merchantID)
        {
            CreditCardOrderSource source = CreditCardOrderSource.NewebTest;
            switch (merchantID)
            {
                case "PiinLife":
                    source = CreditCardOrderSource.PiinLife;
                    break;

                case "Ppon":
                    source = CreditCardOrderSource.Ppon;
                    break;

                case "Test":
                default:
                    break;
            }

            authObject.TransactionId = "ntest" + Path.GetRandomFileName().Replace(".", string.Empty);
            CreditCardAuthResult result = CreditCardUtility.Authenticate(authObject, Guid.NewGuid(), config.AdminEmail, OrderClassification.LkSite, source);

            string log = resultHeader.Replace("#provider", result.APIProvider.ToString());

            // auth
            log += "<tr><td>授權</td><td>" + result.TransPhase.ToString() +
                "</td><td>" + result.ReturnCode +
                "</td><td>" + result.TransMessage +
                "</td><td>" + result.AuthenticationCode +
                "</td><td>" + result.OrderDate +
                "</td></tr>";

            return log;
        }

        private void bfTest(int count)
        {
            for (int x = 0; x < count; x++)
            {
                Thread t = new Thread(justAuth);
                t.Start();
            }

            TestResult.Text = sb.ToString();
        }

        private static void justAuth()
        {
            for (int y = 0; y < 100; y++)
            {
                bfAuthObject.TransactionId = "ntest" + Path.GetRandomFileName().Replace(".", string.Empty);
                CreditCardAuthResult result = CreditCardUtility.Authenticate(bfAuthObject, Guid.NewGuid(), config.AdminEmail, OrderClassification.LkSite, CreditCardOrderSource.Ppon);

                string log = resultHeader.Replace("#provider", result.APIProvider.ToString());

                // auth
                log += "<tr><td>授權</td><td>" + result.TransPhase.ToString() +
                    "</td><td>" + result.ReturnCode +
                    "</td><td>" + result.TransMessage +
                    "</td><td>" + result.AuthenticationCode +
                    "</td><td>" + result.OrderDate +
                    "</td></tr>";

                sb.Append(log);
            }
        }

        protected void BruteForceTest_Click(object sender, EventArgs e)
        {
            bfTest(100);
        }

        private void setupDropdown()
        {
            for (int m = 0; m < 12; m++)
            {
                ExpMonth.Items.Add((m + 1).ToString("00"));
            }

            for (int y = 0; y < 10; y++)
            {
                ExpYear.Items.Add((DateTime.Today.Year + y).ToString().Remove(0, 2));
            }
        }
    }
}
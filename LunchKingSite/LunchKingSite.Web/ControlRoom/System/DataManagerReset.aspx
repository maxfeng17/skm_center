﻿<%@ Page Title="暫存資料重設" Language="C#" MasterPageFile="~/ControlRoom/backend.master"
    AutoEventWireup="true" CodeBehind="DataManagerReset.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.DataManagerReset" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script src="../../Tools/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/adapters/jquery.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <script src="//www.google.com/jsapi" type="text/javascript"></script>
    <script type="text/javascript">
        google.load("jqueryui", "1.8");
    </script>
    <script type="text/javascript">
        $(window).on("orientationchange", function () {
            if (window.orientation == 0) // Portrait
            {
                $("p").css({ "background-color": "yellow", "font-size": "300%" });
                $("img").css({ "width": "300%", "height": "300%" });
            }
            else // Landscape
            {
                $("p").css({ "background-color": "pink", "font-size": "200%" });
                $("img").css({ "width": "300%; height: 200%" });
            }
        });
    </script>

    <script type="text/javascript">

        $(document).ready(function () {

			$tabs = $('#tabs').tabs();


			$('#selSystemData').bind('change', function () {
				var name = $(this).val();
				if (name == '') {
					return;
				}
				$.ajax({
					type: "POST",
					data: JSON.stringify({'name': name}),
					contentType: "application/json;charset=utf-8",
					dataType: "json",
					async: true,
					url: "/ControlRoom/System/DataManagerReset.aspx/GetSystemDataData",
					success: function (res) {
						var jsonStr = res.d;
						console.log(res.d);
						$('#tbSystemDataContent').val(jsonStr);
					},
					error: function (err) {
						console.log(err);
						alert("Sorry, error!");
					}
				});



			});

			$('#btnSaveSystemData').click(function () {
				var name = $('#selSystemData').val();
				if (name == '') {
					return;
				}
				var data = $('#tbSystemDataContent').val();

				$.ajax({
					type: "POST",
					data: JSON.stringify({ 'name': name, 'data': data }),
					contentType: "application/json;charset=utf-8",
					dataType: "json",
					async: true,
					url: "/ControlRoom/System/DataManagerReset.aspx/UpdateSystemDataData",
					success: function (res) {
						console.log(res.d);
						alert('更新完成');
						$('#selSystemData').val('');
						$('#tbSystemDataContent').val('');
					},
					error: function (err) {
						console.log(err);
						try {
							alert(JSON.parse(err.responseText).Message);
						} catch{
							alert("Sorry, error!");
						}
					}
				});				
			});

            $('tr:odd').each(function () {
                if ($(this).closest('.noAutoColor').size() == 0) {
                    $(this).css('backgroundColor', '#bbbbff');
                }
            });
                
            <% if (this.CurrentTabPage != null){
                 Response.Write("changetab(" + this.CurrentTabPage + ");");
             }
             %>
               //media ImageCacheInfo

            var serverLocation = "/";
            //判斷正式站跟測試站
            if (window.document.location.pathname.toLowerCase().indexOf('releasebranch/') > 0) {
                serverLocation = "/releasebranch/";
            }
        });

        function changetab(i) {
            $('#tabs').tabs({ selected: i });
            return false;
        }

    </script>
    <style type="text/css">
        .head {
            text-align: left;
            font-weight: bolder;
            background-color: #F0AF13;
            color: white;
        }

        .innerhead {
            background-color: #688E45;
            font-weight: bolder;
            color: White;
        }

        .tab {
            font-size: smaller;
        }

        legend {
            font-weight: bolder;
            background-color: navajowhite;
        }

        .row-header {
            background-color: #bbbbff;
        }
        #tableAppConfig textarea {
            font-size:11px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="tabs">
        <ul>
            <li><a href="#tabs-1" class="tab">P好康暫存資料</a></li>
            <li><a href="#tabs-2" class="tab">PiinLife暫存資料</a></li>
            <li><a href="#tabs-3" class="tab">Mongo</a></li>
            <li><a href="#tabs-4" class="tab">墨攻</a></li>
            <li><a href="#tabs-5" class="tab">工單</a></li>
            <li><a href="#tabs-6" class="tab">附載平衡</a></li>
            <li><a href="#tabs-7" class="tab">優惠券相關</a></li>
            <li><a href="#tabs-8" class="tab">Cache</a></li>
            <li><a href="#tabs-9" class="tab">系統相關</a></li>
            <li><a href="#tabs-10" class="tab">GA</a></li>
            <li><a href="#tabs-11" class="tab">APP版本設定</a></li>
            <li><a href="#tabs-12" class="tab">SystemData異動</a></li>
        </ul>
        <div id="tabs-1">
            <asp:Literal ID="litTabPponCacheMessage" runat="server" EnableViewState="false" Mode="PassThrough" />
            <fieldset>
                <legend>P好康系統暫存資料</legend>
                <table style="border-width: 1px; width: 100%">
                    <tr>
                        <td>Category(CategoryManage)：
                        </td>
                        <td>
                            <asp:Button ID="Button1" runat="server" Text="更新" OnClick="btnCategory_Click" />
                            <span style="color: red;">Step_01: 前後台分類更新</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30%">好康檔次暫存資料(ViewPponDealManage)：
                        </td>
                        <td>
                            <asp:Button ID="btnViewPponDeal" runat="server" Text="更新" OnClick="btnViewPponDeal_Click" />
                            <span style="color: red;">Step_02: 好康檔次內容更新</span>
                            <asp:Button ID="btnViewPponDealClear" runat="server" Text="清空" Visible="false" OnClick="btnViewPponDealClear_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>檔次分類相關資料(PponDealPreviewManager)：
                        </td>
                        <td>
                            <asp:Button ID="Button2" runat="server" Text="更新" OnClick="btnPponDealPreview_Click" />
                            <span style="color: red;">Step_03: 前台好康檔次顯示更新</span>
                        </td>
                    </tr>
                    <tr>
                        <td>好康分店資料(ViewPponStoreManage)：
                        </td>
                        <td>
                            <asp:Button ID="btnViewPponStore" runat="server" Text="更新" OnClick="btnViewPponStore_Click" />
                            <asp:Button ID="btnViewPponStoreC" runat="server" Text="清空" OnClick="btnViewPponStoreC_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>代碼檔資料(SystemCodeManager)：
                        </td>
                        <td>
                            <asp:Button ID="btnCode" runat="server" Text="更新" OnClick="btnCode_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>城市與鄉鎮市區(CityManage)：
                        </td>
                        <td>
                            <asp:Button ID="btnCity" runat="server" Text="更新" OnClick="btnCity_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>API的使用者(ApiUserManage)：
                        </td>
                        <td>
                            <asp:Button ID="btnApiUser" runat="server" Text="更新" OnClick="btnApiUser_Click" />
                        </td>
                    </tr>

                    <tr>
                        <td>subDealCategory分類資料(Subdealcategorycount)：
                        </td>
                        <td>
                            <asp:Button ID="Button4" runat="server" Text="更新" OnClick="ViewSubCategoryDependencyCollectionClear_Click" />
                        </td>
                    </tr>

                    <tr style="display: none">
                        <td colspan="2">ViewPponDealManagerSummary
                            <asp:Button ID="btn_Summary" runat="server" Text="檢視" OnClick="GetViewPponDealManagerSummary" />
                            <br />
                            <asp:Literal ID="lit_Summary" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>Api商品主題活動(ApiPromoEventManager)：
                        </td>
                        <td>
                            <asp:Button ID="btn_Event" runat="server" Text="更新" OnClick="GetApiEventData" />
                            <asp:CheckBox ID="chkSyncAllServersForEvent" runat="server" Text="嘗試同步所有伺服器" style="margin-left:8px" />
                        </td>
                    </tr>
                    <tr>
                        <td>CategoryListManage類別清單(CategoryDeals)：
                        </td>
                        <td>
                            <asp:Button ID="btn_CategoryList" runat="server" Text="更新" OnClick="btnCategoryList_Click" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div id="tabs-2">
            <fieldset>
                <legend>PiinLife系統暫存資料 </legend>
                <table style="border-width: 1px;">
                    <tr>
                        <td style="width: 30%">重設HiDealProduct暫存資料(HiDealProductManager)
                        </td>
                        <td>
                            <asp:Button ID="btnHiDealProduct" runat="server" Text="執行" OnClick="btnHiDealProduct_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>於HiDealDeal暫存清空(HiDealDealManager)
                        </td>
                        <td>
                            <asp:Button ID="btnHiDealDeal" runat="server" Text="執行" OnClick="btnHiDealDeal_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>於HiDealCoupon暫存清空(HiDealCouponManager)
                        </td>
                        <td>要清除憑證暫存的Product檔次ID
                            <asp:TextBox ID="tbhdcProductId" runat="server"></asp:TextBox>
                            <asp:Button ID="btnHiDealCoupon" runat="server" Text="執行" OnClick="btnHiDealCoupon_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>清除Piinlife30分鐘前，未完成的訂單
                        </td>
                        <td>
                            <asp:Button ID="btnCancelPiinlifeOrder" runat="server" Text="更新" OnClick="btnCancelPiinlifeOrder_Click" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div id="tabs-3">
            <fieldset>
                <legend>Mongo資料轉移</legend>
                <table style="border-width: 1px;">
                    <tr>
                        <td style="width: 30%">補商家P好康活動項目資料<br />
                            原Mongo端沒有資料的財會補
                        </td>
                        <td>
                            <asp:Button ID="btnSellerActivity" runat="server" Text="執行" OnClick="btnSellerActivity_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>Rum用MongoDB資料ActivityBase建立
                        </td>
                        <td>
                            <asp:Button ID="btnActivityBase" runat="server" Text="執行" OnClick="btnActivityBase_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>Rum用MongoDB資料ActivityBase重設
                        </td>
                        <td>
                            <asp:Button ID="btnMongoSellerActivityListReset" runat="server" Text="更新" OnClick="btnMongoSellerActivityListReset_Click" />
                        </td>
                    </tr>                    
                </table>
            </fieldset>
        </div>
        <div id="tabs-4">
            <fieldset>
                <legend>墨攻相關資料 </legend>
                <table style="border-width: 1px;">
                    <tr>
                        <td style="width: 30%">墨攻核銷特別註記
                        </td>
                        <td>CouponId：<asp:TextBox ID="txtCouponId" runat="server"></asp:TextBox><br />
                            SequenceNumber：<asp:TextBox ID="txtSequenceNumber" runat="server"></asp:TextBox><br />
                            Memo：<asp:TextBox ID="txtMemo" runat="server" Width="300px"></asp:TextBox>
                            <asp:Button ID="btnMohistVerifyMemo" runat="server" Text="更新" OnClick="btnMohistVerifyMemo_Click" />
                        </td>
                    </tr>
                    <tr style="display:none;">
                        <td>墨攻重傳(與隔天合併上傳)
                        </td>
                        <td>
                            <asp:Button ID="btnMohistRedoProduct" runat="server" Text="產品檔重傳" OnClick="RedoUpload" />
                            <asp:Button ID="btnMohistRedoDeal" runat="server" Text="交易檔重傳" OnClick="RedoUpload" />
                            <asp:Button ID="btnMohistRedoVerify" runat="server" Text="核銷檔重傳" OnClick="RedoUpload" />
                        </td>
                    </tr>
                    <tr>
                        <td>墨攻核銷
                        </td>
                        <td>產出日期:
                            <asp:TextBox ID="txtMohistDateStart" runat="server" Columns="8" />00:00
                            <cc1:CalendarExtender ID="ceMohistDateStart" TargetControlID="txtMohistDateStart"
                                runat="server">
                            </cc1:CalendarExtender>
                            至
                            <asp:TextBox ID="txtMohistDateEnd" runat="server" Columns="8" />00:00
                            <cc1:CalendarExtender ID="ceMohistDateEnd" TargetControlID="txtMohistDateEnd" runat="server">
                            </cc1:CalendarExtender>
                            <br />
                            <asp:Button ID="btnUploadMohistProduct" runat="server" Text="上傳產品檔" OnClick="btnUploadMohistProduct_Click" />
                            <asp:Button ID="btnSaveMohistVerifyInfo" runat="server" Text="寫入交易資訊" OnClick="btnSaveMohistVerifyInfo_Click" />
                            <asp:Button ID="btnUpdateMohistVerifyInfoStatusByPeriod" runat="server" Text="更新異動狀態"
                                OnClick="btnUpdateMohistVerifyInfoStatusByPeriod_Click" />
                            <asp:Button ID="btnUploadMohistDeals" runat="server" Text="上傳交易檔" OnClick="btnUploadMohistDeals_Click" />
                            <asp:Button ID="btnUploadMohistConfirms" runat="server" Text="上傳核銷檔" OnClick="btnUploadMohistConfirms_Click" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div id="tabs-5">
            <fieldset>
                <legend>工單相關資料 </legend>
                <table style="border-width: 1px;" width="100%">
                    <tr>
                        <td style="width: 20%">工單資料查詢
                        </td>
                        <td>狀態：<asp:TextBox ID="txtStatus" runat="server" Width="80px"></asp:TextBox>
                            帳號：<asp:TextBox ID="txtCreateId" runat="server"></asp:TextBox>
                            工單序號：<asp:TextBox ID="txtBusinessOrderSequenceNumber" runat="server" Width="80px"></asp:TextBox><br />
                            異動區間：
                            <asp:TextBox ID="txtBusinessOrderModifyTimeS" runat="server" Columns="8" Width="100px" />
                            <cc1:CalendarExtender ID="ceBusinessOrderModifyTimeS" TargetControlID="txtBusinessOrderModifyTimeS"
                                runat="server">
                            </cc1:CalendarExtender>
                            至
                            <asp:TextBox ID="txtBusinessOrderModifyTimeE" runat="server" Columns="8" Width="100px" />
                            <cc1:CalendarExtender ID="ceBusinessOrderModifyTimeE" TargetControlID="txtBusinessOrderModifyTimeE"
                                runat="server">
                            </cc1:CalendarExtender>
                            <br />
                            <asp:Button ID="btnQueryBusinessOrder" runat="server" Text="查詢" OnClick="btnQueryBusinessOrder_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>下載工單歷程報表</td>
                        <td>期間(月份)：<asp:TextBox ID="txtBusinessOrderMonth" runat="server" Width="80px"></asp:TextBox>
                            <asp:Button ID="btnDownLoadContractDataList" runat="server" Text="下載" OnClick="btnDownLoadContractDataList_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>工單轉件</td>
                        <td>原業務:<asp:TextBox ID="txtBusinessOrderCreateId" runat="server" Width="200px"></asp:TextBox><br />
                            轉業務:<asp:TextBox ID="txtBusinessOrderReferredEmail" runat="server" Width="200px"></asp:TextBox><asp:Button
                                ID="btnBusinessOrderReferred" runat="server" Text="轉件" OnClick="BusinessOrderReferred" />
                        </td>
                    </tr>
                    <tr>
                        <td>工單部門轉換</td>
                        <td>原部門:<asp:TextBox ID="txtSalesDeptId" runat="server" Width="200px"></asp:TextBox><br />
                            轉部門:<asp:TextBox ID="txtReferredSalesDeptId" runat="server" Width="200px"></asp:TextBox><asp:Button
                                ID="btnSalesDeptIdReferred" runat="server" Text="轉區" OnClick="SalesDeptIdReferred" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div id="tabs-6">
            <fieldset>
                <legend>附載平衡</legend>
                <table style="border-width: 1px;">
                    <tr>
                        <td>JobLoadBalance
                        </td>
                        <td>
                            <asp:TextBox ID="tbx_JobLoadBalance" runat="server"></asp:TextBox><asp:Button ID="btn_JobLoadBalance"
                                runat="server" Text="更新" OnClick="UpdateJobLoadBalance" />
                            <br />
                            (目前Server HostName:<asp:Label ID="lab_JobLoadBalance" runat="server"></asp:Label>)
                            <br />最後異動時間:<asp:Label ID="lab_JobModifyTime" runat="server"></asp:Label> by <asp:Label ID="lab_JobModifyName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>ImageLoadBalance
                        </td>
                        <td>
                            <ul>
                                <li>
                                    <asp:TextBox ID="tbx_ImageLoadBalanceHost_1" runat="server"></asp:TextBox>:
                                    <asp:TextBox ID="tbx_ImageLoadBalance_1" runat="server"></asp:TextBox></li>
                                <li>
                                    <asp:TextBox ID="tbx_ImageLoadBalanceHost_2" runat="server"></asp:TextBox>:
                                    <asp:TextBox ID="tbx_ImageLoadBalance_2" runat="server"></asp:TextBox></li>
                                <li>
                                    <asp:Button ID="tbx_ImageLoadBalance" runat="server" Text="更新" OnClick="UpdateImageLoadBalance" />
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>ImpersonateAccount
                        </td>
                        <td>帳號:<asp:TextBox ID="tbx_ImpersonateUserName" runat="server"></asp:TextBox>&nbsp;密碼:<asp:TextBox
                            ID="tbx_ImpersonatePass" runat="server"></asp:TextBox>&nbsp;&nbsp;<asp:Button ID="btn_Impersonate"
                                runat="server" Text="更新" OnClick="UpdateImpersonateAccount" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div id="tabs-7">
            <fieldset>
                <legend>優惠券相關</legend>
                <table style="border-width: 1px; width: 50%">
                    <tr>
                        <td style="width: 30%">優惠券商家類型-優惠券數量紀錄清除
                        </td>
                        <td>
                            <asp:Button ID="btnSCClear" runat="server" Text="確定" OnClick="ApiSellerSampleCategoryClear" />
                        </td>
                    </tr>
                    <tr>
                        <td>優惠券更新分店城市city_bit
                        </td>
                        <td>
                            <asp:Button ID="btn_CityBit" runat="server" Text="確定" OnClick="UpdateVourcherCityBit" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div id="tabs-8">
            <fieldset>
                <legend>Cache</legend>
                <table style="border-width: 1px; width: 80%">
                    <tr>
                        <td style="width: 30%">是否啟用
                        </td>
                        <td>
                            <asp:CheckBox ID="chkCacheServerEnabled" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>伺服器
                        </td>
                        <td>
                            <asp:TextBox ID="txtCacheServer" runat="server" onclick="return false" />
                        </td>
                    </tr>
                    <tr>
                        <td>狀態
                        </td>
                        <td>
                            <asp:Literal ID="litCacheStatus" runat="server" />
                            <asp:Button ID="btnCacheStatus" runat="server" Text="取得狀態" OnClick="btnCacheStatus_Click"
                                Style="margin-left: 20px" />
                        </td>
                    </tr>
                    <tr>
                        <td>速度測試
                        </td>
                        <td>
                            <asp:Literal ID="litCacheSpeed" runat="server" />
                            <asp:Button ID="btnCacheTest" runat="server" Text="開始測試" OnClick="btnCacheTest_Click"
                                Style="margin-left: 20px" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div id="tabs-9">
            <asp:Literal ID="litTabCacheMessage" runat="server" EnableViewState="false" Mode="PassThrough" />
            <fieldset>
                <legend>系統相關</legend>
                <table style="border-width: 1px;">
                    <tr>
                        <td style="width: 30%">FamiPort更新列印紀錄
                        </td>
                        <td>
                            <asp:FileUpload ID="FileUpload_Family" runat="server" />
                            &nbsp;<asp:Button ID="btn_Family" runat="server" Text="確定" OnClick="UpdateFamilyPrint" />
                            <br />
                            <asp:TextBox ID="tbx_FamilyPrintDate" runat="server" Columns="8" Width="150" />
                            <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="tbx_FamilyPrintDate"
                                runat="server">
                            </cc1:CalendarExtender>
                            &nbsp;<asp:Button ID="btn_FamilyJob" runat="server" Text="自動更新列印日" OnClick="UpdateFamilyPrintJob" />
                        </td>
                    </tr>
                    <tr>
                        <td>索取廠商提供序號
                        </td>
                        <td>bid:<asp:TextBox ID="tbx_SerialBid" runat="server"></asp:TextBox><asp:Button
                            ID="btn_SerialBid"
                            runat="server" Text="索取" OnClick="GetSerialBid" OnClientClick="return confirm('!!索取會減少廠商提供的序號!!');" />
                            <br />
                            <asp:Label ID="lab_SerialKeyGet" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>產生P好康序號(提供廠商)
                        </td>
                        <td>數量:<asp:TextBox ID="tbx_GenPponCouponCount" runat="server"></asp:TextBox>
                            <asp:Button ID="btn_GenPponCoupon" runat="server" Text="產生" OnClick="GenPponCoupon" />
                        </td>
                    </tr>
                    <tr>
                        <td>產生P好康憑證
                        </td>
                        <td>Bid:<asp:TextBox ID="tbx_CouponBid" runat="server"></asp:TextBox><br />
                            CouponId:<asp:TextBox ID="tbx_CouponCouponId" runat="server"></asp:TextBox><br />
                            SequecnceNumber:<asp:TextBox ID="tbx_CouponSequenceNumber" runat="server"></asp:TextBox><br />
                            CouponCode:<asp:TextBox ID="tbx_CouponCouponCode" runat="server"></asp:TextBox><br />
                            MemberName:<asp:TextBox ID="tbx_CouponMemberName" runat="server"></asp:TextBox><br />
                            <asp:Button ID="btn_CouponPrint" runat="server" Text="列印" OnClick="CouponPrint" />
                            <asp:Button ID="btn_CouponDownLoad" runat="server" Text="下載" OnClick="CouponDownLoad" />
                        </td>
                    </tr>
                    <tr>
                        <td>新光商圈活動專案
                        </td>
                        <td>EventCode:
                            <asp:TextBox ID="txtEventCode" runat="server"></asp:TextBox><br />
                            匯入日期:
                            <asp:TextBox ID="txtCommercialExchangeDateStart" runat="server" Columns="8" />00:00
                            <cc1:CalendarExtender ID="ceCommercialExchangeDateStart" TargetControlID="txtCommercialExchangeDateStart"
                                runat="server">
                            </cc1:CalendarExtender>
                            至
                            <asp:TextBox ID="txtCommercialExchangeDateEnd" runat="server" Columns="8" />00:00
                            <cc1:CalendarExtender ID="ceCommercialExchangeDateEnd" TargetControlID="txtCommercialExchangeDateEnd"
                                runat="server">
                            </cc1:CalendarExtender>
                            <asp:Button ID="btnCommercialExchangeExchange" runat="server" Text="產生主題活動商品" OnClick="btnCommercialExchangeExchange_Click" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td>搜尋資料
                        </td>
                        <td>
                            <asp:Button ID="btn_PponDealIndex" runat="server" Text="重新讀取" OnClick="ResetPponDealIndex" />
                        </td>
                    </tr>
                    <tr>
                        <td>DefenseModule<br />
                            清空黑名單IP
                        </td>
                        <td>
                            <asp:Button ID="btn_ClearBlackIps" runat="server" Text="清空" OnClick="ClearBlackIps" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div id="tabs-10">
            <fieldset>
                <legend>GA</legend>
                <table style="border-width: 1px; width: 100%;">
                    <tr>
                        <td style="width: 30%">抓取資料
                        </td>
                        <td>
                            <div style="display: none">
                                id:<asp:TextBox ID="gaId" runat="server" Text="69858944" /><br />
                                metrics:<asp:TextBox ID="gaMetrics" runat="server" Text="sessions,users" /><br />
                            </div>
                            date:<asp:TextBox ID="GaStartDate" runat="server" Text="2015-03-01" />-<asp:TextBox ID="GaEndDate" runat="server" Text="2015-03-01" />
                            <asp:Button ID="btn_update" runat="server" Text="更新" OnClick="GaUpdate" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div id="tabs-11">
            <fieldset>
                <legend>App版本設定</legend>
                <table style="border-width: 1px; width: 100%;" class="noAutoColor" id="tableAppConfig">
                    <tr class="row-header">
                        <td>App名稱
                        </td>
                        <td>
                            <table style="border-width: 1px; width: 100%;">
                                <tr>
                                    <td>最新版本
                                    </td>
                                    <td>最低版本
                                    </td>
                                    <td>OS建議版本
                                    </td>
                                    <td>OS最低版本
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>更新
                        </td>
                    </tr>
                    <asp:Repeater ID="rpt_AppVersion" runat="server" OnItemCommand="UpdateAppVersionCommand" OnItemDataBound="OnItemDataBound" >
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <%# ((AppVersion)(Container.DataItem)).AppName %>
                                </td>
                                <td>
                                    <table style="border-width: 1px; width: 100%; background-color: #eeeeee">
                                        <tr>
                                            <td>
                                                <asp:Literal ID="litText" runat="server"></asp:Literal><asp:TextBox ID="tbx_latest_version" runat="server" Text="<%# ((AppVersion)(Container.DataItem)).LatestVersion %>"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbx_min_version" runat="server" Text="<%# ((AppVersion)(Container.DataItem)).MinimumVersion %>" ></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbx_os_version" runat="server" Text="<%# ((AppVersion)(Container.DataItem)).OsVersion %>"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbx_os_min_version" runat="server" Text="<%# ((AppVersion)(Container.DataItem)).OsMinimumVersion %>"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <div class="row-header">App定義檔</div>
                                                <asp:TextBox ID="tbx_app_config" Style="width: 100%; height:160px" spellcheck="false"
                                                    runat="server" Text="<%# ((AppVersion)(Container.DataItem)).AppConfig %>" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="background-color: #eeeeee">
                                    <asp:Button ID="btn_AppVersion" runat="server" Text="更新" CommandName="UpdateAppVersion" CommandArgument="<%# ((AppVersion)(Container.DataItem)).AppName %>" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <tr>
                        <td>更新快取: App版本設定
                        </td>
                        <td>
                            <asp:Button ID="btn_appSystem" runat="server" Text="重新讀取" OnClick="ReloadAppSystemManager" />
                            <asp:CheckBox ID="chkSyncAllServers" runat="server" Text="嘗試同步所有伺服器" Style="margin-left: 8px" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div id="tabs-12">
            <fieldset>
                <legend>SystemData異動</legend>
                <p>					
                    <select id="selSystemData">
						<option value="">選擇</option>
						<option value="P24HContentBanner">P24HContentBanner</option>
                    </select>
                </p>
                <table style="border-width: 1px; width: 100%;">
                    <tr>
                        <td>
                            <textarea rows="2" cols="20" wrap="off" id="tbSystemDataContent" style="width:100%; height:320px"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
							<button type="button" class="btn btn-default" id="btnSaveSystemData">確認更新</button>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
    </div>
</asp:Content>

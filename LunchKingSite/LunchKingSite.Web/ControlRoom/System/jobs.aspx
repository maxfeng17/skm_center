<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/ControlRoom/backend.master"
    CodeBehind="jobs.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.jobs" %>

<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ContentPlaceHolderID="SSC" runat="server">
    <style type="text/css">
        .field
        {
            margin: 5px 0px 5px 0px;
            border: dotted 1px gray;
            width: 800px;
        }
        .field legend
        {
            color: blue;
            font-weight: bold;
        }
        ul
        {
            list-style-type: none;
            margin: 0 0 0 0;
        }
        li
        {
            padding: 10 0 0 10;
            color: #4D4D4D;
            font-weight: bold;
        }
        label
        {
            font-weight: bold;
            color: black;
            font-size: larger;
            padding-right: 10;
        }
        
        .jlist
        {
            border: solid 1px #e8eef4;
            border-collapse: collapse;
        }
        
        .jlist td
        {
            padding: 5px;
            border: solid 1px #e8eef4;
        }
        
        .jlist th
        {
            padding: 6px 5px;
            text-align: left;
            background-color: blue;
            border: solid 1px #e8eef4;
        }
        
        th.d
        {
            width: 150px;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function conf() {
            return confirm("Are you sure you want to continue?");
        }
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <asp:PlaceHolder ID="p1" runat="server">
                <asp:Label ID="lab_SingleJob" runat="server" ForeColor="White" BackColor="Red" Font-Bold="true"></asp:Label>
                <asp:Repeater ID="rJ" runat="server" OnItemDataBound="ItemDataBound">
                    <HeaderTemplate>
                        <table class="jlist">
                            <tr>
                                <th>
                                    工作名稱
                                </th>
                                <th class="d">
                                    敘述
                                </th>
                                <th>
                                    狀態
                                </th>
                                <th>
                                    上次執行
                                </th>
                                <th>
                                    執行時間
                                </th>
                                <th>
                                    下次執行
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <a href="?n=<%#Eval("Setting.Name")%>">
                                    <%#Eval("Setting.Name")%></a>
                            </td>
                            <td>
                                <%#Eval("Setting.Description")%>
                                <asp:Label ID="lab_JobLoadBalance" runat="server" Font-Bold="true" ForeColor="Red"
                                    Font-Size="Smaller"></asp:Label>
                            </td>
                            <td>
                                <%#Eval("Status")%>
                            </td>
                            <td>
                                <%#Eval("LastExecution.RunTime")%>
                            </td>
                            <td>
                                <%#Eval("LastExecution.Duration")%>
                            </td>
                            <td>
                                <%#Eval("NextRun")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="p2" runat="server">
                <fieldset class="field">
                    <legend>Job Detail</legend>
                    <ul>
                        <li>
                            <label for="<%=lN.ClientID%>">
                                Name:</label>
                            <asp:Literal ID="lN" runat="server" /></li>
                        <li>
                            <label for="<%=lD.ClientID%>">
                                Description:</label>
                            <asp:Literal ID="lD" runat="server" /></li>
                        <li>
                            <label for="<%=lW.ClientID%>">
                                Worker:</label>
                            <asp:Literal ID="lW" runat="server" /></li>
                        <li>
                            <label for="<%=lS.ClientID%>">
                                Schedule:</label>
                            <asp:Literal ID="lS" runat="server" /></li>
                        <li>
                            <label for="<%=lSt.ClientID%>">
                                Status:</label>
                            <asp:Literal ID="lSt" runat="server" /></li>
                        <li>
                            <label for="<%=lNR.ClientID%>">
                                Next Run:</label>
                            <asp:Literal ID="lNR" runat="server" /></li>
                        <li>
                            <label for="<%=lLR.ClientID%>">
                                Last Run:</label>
                            <asp:Literal ID="lLR" runat="server" /></li>
                        <li>
                            <label for="<%=lRD.ClientID%>">
                                Last Run Duration:</label>
                            <asp:Literal ID="lRD" runat="server" /></li>
                    </ul>
                </fieldset>
                <fieldset class="field">
                    <legend>Job Control</legend>
                    <input type="button" value="Back" onclick="window.location='?n='" />
                    &nbsp;&nbsp;&nbsp;
                    <asp:Button ID="bP" runat="server" Text="Pause" OnClick="bP_Click" />
                    <asp:Button ID="bF" runat="server" Text="Fire" OnClick="bF_Click" OnClientClick="return conf();" />
                    <asp:Button ID="bR" runat="server" Text="Remove" OnClick="bR_Click" OnClientClick="return conf();" />
                </fieldset>
            </asp:PlaceHolder>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="tm1" EventName="Tick" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:Timer ID="tm1" runat="server" Interval="30000" OnTick="tm1_Tick" />
</asp:Content>

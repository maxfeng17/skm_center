﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PushTestForMGM.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.PushTestForMGM" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .row {
            padding: 5px;
        }

        .cell-left {
            width: 70px;
            display: inline-block;
        }

        .cell-right {
            width: 400px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Literal ID="litMessage" runat="server" EnableViewState="false" />
        </div>
        <fieldset>
            <legend>MGM推播測試 - 會員帳號: <asp:Literal ID="txtUserName" runat="server" />
            </legend>
            <div>
                <div class="row">
                    <span class="cell-left">Message 
                    </span>
                    <span class="cell-right">
                        <asp:TextBox ID="txtMessage" runat="server" Text="您的朋友 XYZ 回覆您一張卡片囉" Width="400" />
                    </span>
                </div>
                <div class="row">
                    <span class="cell-left">CardType:
                    </span>
                    <span class="cell-right">
                        <asp:TextBox ID="txtMsgId" runat="server" Text="1" />
                    </span>
                </div>
                <div class="row">
                    <span class="cell-left">Msg ID:
                    </span>
                    <span class="cell-right">
                        <asp:TextBox ID="txtCarType" runat="server" Text="1" />
                    </span>
                </div>
                <br />
                <asp:Button ID="btnTest" runat="server" Text="送出" OnClick="btnTest_Click" />
            </div>
        </fieldset>
    </form>
</body>
</html>

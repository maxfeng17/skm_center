﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/BackendBootstrap.master"
    AutoEventWireup="true" CodeBehind="config.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.config" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script>
        $(document).ready(function () {
            //$('#myTab a[href="#option"]').tab('show');

            var m_tabsIndex = localStorage.getItem("m_tabsIndex");
            if (m_tabsIndex == null) m_tabsIndex = '0';
            $('#myTab li').eq(m_tabsIndex).find('a').tab('show');
            localStorage.setItem("m_tabsIndex", '0');

            $('#bSet').click(function () {
                var m_tabsIndex = $('#myTab li').index($('#myTab .active'));
                localStorage.setItem('m_tabsIndex', m_tabsIndex);
            });

            $('#showAll').change(function () {
                var showAll = $(this).is(':checked');
                localStorage.setItem('m_showAll', showAll ? '1' : '0');
                if (showAll) {
                    $('.tab-pane').addClass('active');
                    $('#myTab a').removeAttr('data-toggle');
                } else {
                    $('.tab-pane').removeClass('active');
                    var idx = $('#myTab li').index($('#myTab .active'));
                    $('.tab-pane').eq(idx).addClass('active');
                    $('#myTab a').attr('data-toggle', 'tab');
                }
            });

            var m_showAll = localStorage.getItem("m_showAll");
            if (m_showAll == null || m_showAll == '0') {
                $('#showAll').removeAttr('checked');
            } else {
                $('#showAll').attr('checked', true);
            }
            $('#showAll').trigger('change');
        });
    </script>
    <style>
        .nav-tabs > li > a, .nav-tabs > li > a:visited, .nav-tabs > li > a:hover {
            padding: 0.25em 0.5em;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">   
    <div class="panel panel-default top-buffer-sm">      
        <div class="panel-heading">
            <asp:Literal ID="lConf" runat="server" />
            <div class="checkbox pull-right" style="margin-top: 0px">
                <label>
                    <input type="checkbox" id="showAll">
                    顯示全部
                </label>
            </div>
        </div>
        <asp:Panel id="pan_sync" runat="server" Visible="false">
            <div style="padding-top: 30px;padding-left:20px">
                本次更新項目
                <asp:Repeater ID="rpt_syncList" runat="server" Visible="false">
                    <HeaderTemplate>
                        <table border="1" cellspacing="0" cellpadding="3">
                            <tr><td>KeyName</td><td>Value</td></tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><%#((KeyValuePair<string, string>)(Container.DataItem)).Key%></td>
                            <td><%#((KeyValuePair<string, string>)(Container.DataItem)).Value%></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <div style="padding:5px">
                    <asp:Button ID="btnSync" runat="server" Text="同步到其他伺服器" OnClick="SetAllConfig" UseSubmitBehavior="False"
                    CssClass="btn btn-primary btn-lg" />
                    <div>
                        Host List:<span class="largeblue"><%=string.Join(", ", CommonFacade.GetWebServerHostList()) %></span><br />
                        IP Address List: <span class="largeblue"><%=string.Join(",", CommonFacade.GetWebServerIpList()) %><br /></span>
                    </div>
                </div>
                <asp:Label ID="Result" runat="server" Text=""></asp:Label>
            </div>
        </asp:Panel>   
        <div class="panel-body">
            <ul class="nav nav-tabs" id="myTab" runat="server" clientidmode="Static">
            </ul>
            <div class="tab-content" id="tabContents" runat="server">
            </div>
            <asp:Button ID="bSet" runat="server" Text="即時更新" OnClick="SetConfig" ClientIDMode="Static" CssClass="btn btn-primary btn-lg top-buffer" />
        </div>
    </div>
</asp:Content>


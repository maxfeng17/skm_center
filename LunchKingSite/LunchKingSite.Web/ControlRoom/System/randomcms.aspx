﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="randomcms.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.randomcms" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script src="../../Tools/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/adapters/jquery.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("jqueryui", "1.8");
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $tabs = $('#tabs').tabs();
            CKEDITOR.replace('<%=tbx_editor_Html.ClientID%>');

            if ('<%= IsCurationUser %>' == 'True') {
                $('.tab').not('.curation').hide();
            }
        });

        function changetab(i) {
            $('#tabs').tabs({ selected: i });
            return false;
        }
        $(function () {
            var moveLeft = 200;
            var moveDown = 20;
            $('.trigger').hover(function (e) {
                moveLeft = $(this).find('.pop-up').width();
                $(this).find('.pop-up').show();
            }, function () {
                $(this).find('.pop-up').hide();
            });
            $('.trigger').mousemove(function (e) {
                $(this).find(".pop-up").css('top', e.pageY - moveDown).css('left', e.pageX - moveLeft);
            });
            $('.copyhref').bind('click', function() {
                var copy = $('.copyf_image')[0].createTextRange();
                copy.execCommand("Copy"); 
                
            });
        });
    </script>
    <style type="text/css">
        .head
        {
            text-align: left;
            font-weight: bolder;
            background-color: #F0AF13;
            color: white;
        }

        .innerhead
        {
            background-color: #688E45;
            font-weight: bolder;
            color: White;
        }

        .tab
        {
            font-size: smaller;
        }

        .tdwidth td
        {
            width: 60px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="background-color: Orange">
        <tr>
            <td>頻道選擇:
            </td>
            <td>
                <asp:RadioButtonList ID="rbl_RandomCmsType" runat="server" OnSelectedIndexChanged="RandomCmsTypeChange"
                    AutoPostBack="true" RepeatDirection="Horizontal">
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
    <div id="tabs">
        <ul>
            <li><a href="#tabs-1" class="tab">時程表</a></li>
            <li><a href="#tabs-5" class="tab">圖片上傳</a></li>			
			<li><a href="#tabs-6" class="tab">其它</a></li>			
            <li><a href="#tabs-2" class="tab">Banner</a></li>
            <li><a href="#tabs-3" class="tab curation">區域設定</a></li>
            <li><a href="#tabs-4" class="tab curation">列表</a></li>
        </ul>
        <div id="tabs-1">
            <fieldset>
                <legend>時程表</legend>
                <table style="width: 970px">
                    <tr>
                        <td>
                            <asp:Label ID="lab_timeslot" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div id="tabs-5">
            <fieldset>
                <legend>圖片上傳</legend>
                <table style="width: 970px">
                    <tr>
                        <td width="10%">查詢欄位     
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_SearchUploadType" runat="server">
                            </asp:DropDownList><asp:TextBox ID="tbx_Search" runat="server"></asp:TextBox>
                            <asp:Button ID="btn_Search" runat="server" Text="查詢"
                                OnClick="btn_Search_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td width="10%">上傳欄位</td>
                        <td>
                            <asp:DropDownList ID="ddl_UploadType" runat="server"></asp:DropDownList>
                            <asp:FileUpload ID="fd_UploadImage" runat="server" /><asp:Button ID="btn_Commit" runat="server" Text="上傳" OnClick="btn_Commit_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lbl_UploadType" runat="server" Visible ="false"></asp:Label>
                            <asp:GridView ID="gv_UploadFile" runat="server" AutoGenerateColumns="False"
                                OnRowCommand="gv_UploadFile_RowCommand" >
                                <Columns>
                                    <asp:BoundField DataField="AcessTime" HeaderText="上傳日期">
                                        <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="檔名">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_FullName" runat="server" Text='<%# ((LunchKingSite.BizLogic.Model.ImageFile)Container.DataItem).FileName %>'></asp:Label>    
                                        </ItemTemplate>
                                        <ItemStyle Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="刪除" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbt_Del" runat="server" OnClientClick="return confirm('確定要刪除?')" CausesValidation="false" CommandName="del" CommandArgument="<%# ((LunchKingSite.BizLogic.Model.ImageFile)Container.DataItem).FileName%>" Text="刪除"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="複製連結">
                                        <ItemTemplate>
                                            
                                            <input type="text" style="width:300px;" class="copyf_image" value="<%# ((LunchKingSite.BizLogic.Model.ImageFile)Container.DataItem).Url%>" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="預覽">
                                        <ItemTemplate>
                                            <div class="trigger">
                                                <div class="pop-up" style="position: absolute; display: none;">
                                                    <img class="f_image" alt="<%# ((LunchKingSite.BizLogic.Model.ImageFile)Container.DataItem).FileName%>" border="0" src="<%# ((LunchKingSite.BizLogic.Model.ImageFile)Container.DataItem).Url%>" vspace="5">
                                                </div>
                                                <a id="preView" href="javascript:void(0)">預覽</a>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div id="tabs-6">
            <fieldset>
                <legend>其它設定</legend>
                <div style="width:970px">
					<a href="/controlroom/WebEdit/PageBlock" target="_blank">設定首頁區塊-新好友設定好康</a>
				</div>
            </fieldset>
        </div>
        <div id="tabs-2">
            <fieldset>
                <legend>Banner </legend>
                <table style="width: 970px">
                    <tr>
                        <td colspan="6">Top :/ppon/default.aspx 上方廣告；Side :/ppon/default.aspx 右邊小廣告；PreviewAll :/ppon/todaydeals.aspx
                            好康總覽；Main :/default.aspx 17Life首頁上方廣告
                        </td>
                    </tr>
                    <tr>
                        <td class="head">標題
                        </td>
                        <td>
                            <asp:TextBox ID="tbx_Title" runat="server"></asp:TextBox>
                        </td>
                        <td class="head">Banner
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_Target" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="head">狀態
                        </td>
                        <td>
                            <asp:CheckBox ID="cbx_ContentStatus" runat="server" Checked="true" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" class="head">內容
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:TextBox ID="tbx_editor_Html" runat="server" Width="970" Height="300" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:Button ID="btn_Save" runat="server" Text="新增" OnClick="ClickSaveCmsRandomContent" />
                            <asp:Button ID="btn_Cancel" runat="server" Text="取消" ForeColor="Red" OnClick="CancelAction" />
                            &nbsp;&nbsp;
                            <asp:Button ID="btn_Delete" runat="server" Text="刪除Banner" ForeColor="Red" OnClick="ClickSaveCmsRandomContent"
                                OnClientClick="return confirm('確定要刪除Banner?')" Visible="false" />
                            <asp:HiddenField ID="hif_ID" runat="server" />
                            <asp:HiddenField ID="hif_Mode" runat="server" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div id="tabs-3">
            <fieldset>
                <legend>區域設定</legend>
                <table style="width: 970px">
                    <tr>
                        <td class="head" style="width: 25%">Banner
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_Target2" runat="server" OnSelectedIndexChanged="TargetChange"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="head" style="width: 25%">標題
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_Banners" runat="server" OnSelectedIndexChanged="BannerChangeGetContent"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="padding-top: 30px;">
                            <asp:Label ID="lab_Content" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr style="height:35px;">
                        <td class="head">城市
                        </td>
                        <td colspan="3">
                            <asp:Label ID="lbl_City" runat="server" Visible="false"></asp:Label>
                            <asp:CheckBox ID="cbx_CityStatus" runat="server" Text="狀態" Visible="false" />
                            <asp:CheckBoxList ID="cbx_City" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" Font-Size="Smaller" 
                                CssClass="tdwidth">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td class="head">輪播區間
                        </td>
                        <td colspan="3">
                            <cc1:CalendarExtender ID="ce_Start" TargetControlID="tbx_StartTime" runat="server"
                                Format="yyyy/MM/dd">
                            </cc1:CalendarExtender>
                            <asp:TextBox ID="tbx_StartTime" runat="server"></asp:TextBox>&nbsp;
                            <asp:DropDownList ID="ddl_StartHour" runat="server">
                            </asp:DropDownList>
                            :
                            <asp:DropDownList ID="ddl_StartMinute" runat="server">
                            </asp:DropDownList>
                            ~
                            <cc1:CalendarExtender ID="ce_End" TargetControlID="tbx_EndTime" runat="server" Format="yyyy/MM/dd">
                            </cc1:CalendarExtender>
                            <asp:TextBox ID="tbx_EndTime" runat="server"></asp:TextBox>&nbsp;
                            <asp:DropDownList ID="ddl_EndHour" runat="server">
                            </asp:DropDownList>
                            :
                            <asp:DropDownList ID="ddl_EndMinute" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Button ID="btn_Save2" runat="server" Text="新增" OnClick="ClickSaveCmsRandomCity" />
                            <asp:Button ID="btn_EditCity" runat="server" Text="更新" OnClick="UpdateCity" Visible="false" />
                            <asp:Button ID="btn_Cancel2" runat="server" Text="取消" ForeColor="Red" OnClick="CancelAction2" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div id="tabs-4">
            <fieldset>
                <legend>列表</legend>
                <table style="width: 970px;">
                    <tr>
                        <td class="head">Banner
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_TargetView" runat="server" OnSelectedIndexChanged="TargetChange"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td class="head">標題
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_BannersView" runat="server" OnSelectedIndexChanged="BannerChangeGetContent"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="head">狀態
                        </td>
                        <td>
                            <asp:HiddenField ID="hif_ContentId" runat="server" />
                            <asp:CheckBox ID="cbx_Status" runat="server" Enabled="false" />
                        </td>
                        <td class="head">編輯
                        </td>
                        <td>
                            <asp:Button ID="btn_EditCmsContent" runat="server" Text="修改Banner" OnClick="EditCmsContent"
                                ForeColor="Blue" />
                            &nbsp;
                            <asp:Button ID="btn_ClearCache" runat="server" Text="清除Cache" OnClick="ClearCache" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="padding-top: 30px;">
                            <asp:Label ID="lbl_Body" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="gv_ViewData" runat="server" AutoGenerateColumns="false" OnRowCommand="gv_Command">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <table style="width: 970px">
                                                <tr>
                                                    <td class="innerhead">修改
                                                    </td>
                                                    <td class="innerhead">狀態
                                                    </td>
                                                    <td class="innerhead">城市
                                                    </td>
                                                    <td class="innerhead">加權
                                                    </td>
                                                    <td class="innerhead">區間
                                                    </td>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:LinkButton ID="lbt_Edit" runat="server" CommandArgument="<%# ((CmsRandomCity)Container.DataItem).Id%>"
                                                        Text="編輯" CommandName="EditId"></asp:LinkButton>
                                                    <asp:LinkButton ID="lbt_Delete" runat="server" CommandArgument="<%# ((CmsRandomCity)Container.DataItem).Id%>"
                                                        Text="刪除" CommandName="DeleteId" ForeColor="Red"></asp:LinkButton>
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="cbx_CityStatus" runat="server" Checked="<%# ((CmsRandomCity)Container.DataItem).Status%>"
                                                        Enabled="false" />
                                                </td>
                                                <td>
                                                    <%# ((CmsRandomCity)Container.DataItem).CityName%>
                                                </td>
                                                <td>
                                                    <%# ((CmsRandomCity)Container.DataItem).Ratio%>
                                                </td>
                                                <td>
                                                    <%# ((CmsRandomCity)Container.DataItem).StartTime.ToString("yyyy/MM/dd HH:mm")%>~
                                                    <%# ((CmsRandomCity)Container.DataItem).EndTime.ToString("yyyy/MM/dd HH:mm")%>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>

    </div>
</asp:Content>

﻿<%@ Page Title="ElmahLog搜尋" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="ElmahLogList.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.ElmahLogList" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            $(".dates").datepicker();
            $(".datee").datepicker();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <fieldset style="display: inline-block; width: 1000px;">
        <legend>ElmahLog搜尋</legend>
        <table>
            <tr>
                <td>NameSpace、Class或Method名稱
                </td>
                <td>
                    <asp:TextBox ID="tbx_Source" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>區間(可不填)</td>
                <td>
                    <asp:TextBox ID="tbx_StartDate" runat="server" CssClass="dates"></asp:TextBox>~<asp:TextBox ID="tbx_EndDate" runat="server" CssClass="datee"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <asp:Button ID="btnSearch" runat="server" Text="搜尋" OnClick="ElmahLogSearch" />&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lab_NoData" runat="server" Text="查無資料" Visible="false" Font-Bold="true" ForeColor="Red"></asp:Label>
                    <asp:Panel ID="pan_ElmahLog" runat="server" Visible="false">
                        <table style="width: 950px">
                            <asp:Repeater ID="rpt_ElmahLog" runat="server">
                                <HeaderTemplate>
                                    <thead>
                                        <tr style="background-color: blue; color: white;">
                                            <th>序號
                                            </th>
                                            <th>主機
                                            </th>
                                            <th>來源
                                            </th>
                                            <th>訊息
                                            </th>
                                            <th>時間
                                            </th>
                                        </tr>
                                    </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr style="background-color: white;">
                                        <td>
                                            <a href="../../oops.axd/detail?id=<%# ((ElmahError)(Container.DataItem)).ErrorId %>" target="_blank"><%# ((ElmahError)(Container.DataItem)).Sequence %>
                                            </a>
                                        </td>
                                        <td>
                                            <%# ((ElmahError)(Container.DataItem)).Host %>
                                        </td>
                                        <td style="font-size: smaller">
                                            <%# ((ElmahError)(Container.DataItem)).Source %>
                                        </td>
                                        <td style="font-size: smaller">
                                            <%# ((ElmahError)(Container.DataItem)).Message.TrimToMaxLength(30,"...") %>
                                        </td>
                                        <td>
                                            <%# ((ElmahError)(Container.DataItem)).TimeUtc.ToLocalTime().ToString("yyyy/MM/dd HH:mm:ss") %>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tr style="background-color: khaki">
                                        <td>
                                            <a href="../../oops.axd/detail?id=<%# ((ElmahError)(Container.DataItem)).ErrorId %>" target="_blank"><%# ((ElmahError)(Container.DataItem)).Sequence %>
                                            </a>
                                        </td>
                                        <td>
                                            <%# ((ElmahError)(Container.DataItem)).Host %>
                                        </td>
                                        <td style="font-size: smaller">
                                            <%# ((ElmahError)(Container.DataItem)).Source %>
                                        </td>
                                        <td style="font-size: smaller">
                                            <%# ((ElmahError)(Container.DataItem)).Message.TrimToMaxLength(30,"...") %>
                                        </td>
                                        <td>
                                            <%# ((ElmahError)(Container.DataItem)).TimeUtc.ToLocalTime().ToString("yyyy/MM/dd HH:mm:ss") %>
                                        </td>
                                    </tr>
                                </AlternatingItemTemplate>
                            </asp:Repeater>
                            <tr>
                                <td colspan="5" style="text-align: center">
                                    <uc1:Pager ID="ucPager" runat="server" PageSize="20" OnGetCount="RetrieveElmahLogCount" OnUpdate="UpdateHandler"></uc1:Pager>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>

                </td>
            </tr>
        </table>
    </fieldset>

</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class TrustFixTool : RolePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //顯示有錯的東西
        protected void Button1_Click(object sender, EventArgs e)
        {
            DataTable toc = TrustFacade.TrustOverCoupon();
            DataTable cot = TrustFacade.CouponOverTrust();
            GridView1.DataSource = toc;
            GridView2.DataSource = cot;
            GridView3.DataSource = TrustFacade.TrustCouponEmpty();
            GridView1.DataBind();
            GridView2.DataBind();
            GridView3.DataBind();
            lbt.Text = toc.Rows.Count.ToString();
            lbi.Text = cot.Rows.Count.ToString();
        }

        //補可以補的coupon上去
        protected void Button2_Click(object sender, EventArgs e)
        {
            Label1.Text = TrustFacade.FillTrustEmptyCoupon(30);
        }
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="EmpSet.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.EmpSet" %>

<asp:Content ID="S" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="<%=ResolveClientUrl("~/Tools/js/jquery-ui.1.8.min.js") %>"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.tablesorter.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <link type="text/css" href="../../Tools/js/css/sorterGrid.css"
        rel="stylesheet" />
    <style type="text/css">
        body
        {
            font-size: 10px;
        }
        fieldset
        {
            margin: 10px 0px;
            border: solid 1px gray;
        }
        fieldset legend
        {
            color: #4F8AFF;
            font-weight: bolder;
        }
        ul
        {
            list-style-type: none;
            margin: 0 0 0 0;
        }
        li
        {
            color: #4D4D4D;
            font-weight: bold;
        }
        ul.status li
        {
            float: left;
        }
        ul.target input
        {
            width: 80px;
        }
        
        input
        {
            border: solid 1px grey;
        }
        textarea
        {
            border: solid 1px grey;
        }
        
        input.date
        {
            width: 80px;
        }
    </style>
    <script type="text/javascript">
        function setupdatepicker(from, to) {
            var dates = $('#' + from + ', #' + to).datepicker({
                changeMonth: true,
                numberOfMonths: 3,
                onSelect: function (selectedDate) {
                    var option = this.id == from ? "minDate" : "maxDate";
                    var instance = $(this).data("datepicker");
                    var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                }
            });
        }
        
        function ClearData() {
            $('[id*=divEdit]').find('input[type=text]').val('');
            $('[id*=divEdit]').find('input[type=checkbox]').prop('checked', false);
            if ($('#<%=ddlParentDept.ClientID%>').length > 0)
                $('#<%=ddlParentDept.ClientID%> option')[0].selected = true;
            
        }

        function ClosePanel() {
            $('[id*=divEdit]').hide();
            ClearData();
            return false;
        }

        $(document).ready(function () {
            setupdatepicker('<%=tbAvailableDate.ClientID%>', '<%=tbDepartureDate.ClientID%>');

            SetDeptVisible();

            $('select[id*=ddlSearch]').bind('change', function () {
                SetDeptVisible();
            });

            $('select[id*=ddlDeptSearch]').bind('change', function () {
                $('[id*=txtFilter]').val($(this).val());
            });



            $('[id*=chkCrossDeptTeam]').change(function () {
                //顯示跨區詳細設定
                if ($(this).is(':checked'))
                    $('.CrossSetting').show();
                else
                    $('.CrossSetting').hide();
            });
            

            


        });

        function BindFunctions() {
            $('#<%=gvEmp.ClientID%>').tablesorter();
            setupdatepicker('<%=tbAvailableDate.ClientID%>', '<%=tbDepartureDate.ClientID%>');
            ShowCross();

            $(document).ready(function () {
                $('#rblMon input').change(function () {
                    var kpi = $('#txtKPI').val();
                    $('#txtKPI').val($('#hdKPI').val());
                    $('#hdKPI').val(kpi);
                    var deales = $('#txtDeals').val();
                    $('#txtDeals').val($('#hdDeals').val());
                    $('#hdDeals').val(deales);
                })
            });
        }

        function SetDeptVisible() {
            if ($('select[id*=ddlSearch]').val() == 'dept_id') {
                $('[id*=txtFilter]').hide();
                $('select[id*=ddlParentDeptSearch]').show();
                $('select[id*=ddlDeptSearch]').show();
                $('[id*=txtFilter]').val($('select[id*=ddlDeptSearch]').val());
            } else {
                $('[id*=txtFilter]').show();
                $('select[id*=ddlParentDeptSearch]').hide();
                $('select[id*=ddlDeptSearch]').hide();
                $('[id*=txtFilter]').val('');
            }
        }

        function SearchClick() {
            //            if ($('select[id*=ddlSearch]').val() == 'mobile') {
            //                var str = '';
            //                $('[id*=txtFilter]').split('-').each(function () {
            //                    str = str + $(this);
            //                })
            //            }
            ClosePanel();
        }

        function SaveCheck() {

            var check = true;
            $('[id*=txtTeam]').each(function () {
                var team = $(this).val();
                if (team != '') {


                    if (team.indexOf(',')) {
                        var split = team.split(',');
                        for (var t in split) {
                            if (isNaN(split[t])) {
                                alert("組別輸入錯誤");
                                check = false;
                            }
                        }
                    }
                    else {
                        if (isNaN(team)) {
                            alert("組別輸入錯誤");
                            check = false;
                        }

                    }
                }

            });

            if (check) {
                if ($('[id*=divKPI]').is(':visible') && $('[id*=txtKPI]').val() == '') {
                    return confirm('您有人員尚未輸入目標KPI(萬)金額，請問是否要儲存');
                }
            }
            else
                return false;

            
        }

        function blag() {
            var gridID = "<%= gvEmp.ClientID %>";
            $.ajax({
                type: "POST",
                data: {},
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                async: true,
                url: "EmpSet.aspx/gvSort",
                success: function (data) {
                    //not sure why this is data.d , but what the hey
                    var theHtml = data.d;
                    $("#" + gridID).html(theHtml);
                },
                failure: function () {
                    alert("Sorry,there is a error!");
                }
            });
        }

        function CheckDisplayTeamSet() {
            var dept = $('#<%=ddlParentDept.ClientID%> option:selected').val();

            if (dept == "S000") {//業務部
                $('.teamSet').show();
            }
            else {
                $('.teamSet').hide();
            }
        }

        function ddlParentDeptOnChange() {
            CheckDisplayTeamSet();
            $('#<%=ddlParentDept.ClientID%>').on('change', function () {
                CheckDisplayTeamSet();
            });

            ShowCross();
        }

        //顯示跨區詳細設定
        function ShowCrossSetting(obj)
        {
            if ($(obj).is(':checked'))
                $('.CrossSetting').show();
            else
                $('.CrossSetting').hide();
        }

        //業務部顯示跨區設定
        function ShowCross()
        {
            var dept = $('#<%=ddlParentDept.ClientID%> option:selected').val()
            if (dept == "S000") {
                $('.Cross').show();
                ShowCrossSetting('#<%=chkCrossDeptTeam.ClientID%>');
            }
            else {
                $('.Cross').hide();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="divSearch" runat="server">
        <asp:DropDownList ID="ddlSearch" runat="server" DataValueField="Key" DataTextField="Value">
        </asp:DropDownList>
        <asp:TextBox ID="txtFilter" runat="server"></asp:TextBox>
        <asp:DropDownList ID="ddlParentDeptSearch" runat="server" DataValueField="DeptId"
            AutoPostBack="true" DataTextField="DeptName" OnSelectedIndexChanged="ddlParentDeptSearch_SelectedIndexChanged">
        </asp:DropDownList>
        <asp:DropDownList ID="ddlDeptSearch" runat="server" DataValueField="DeptId" DataTextField="DeptName">
        </asp:DropDownList>
        <asp:Button ID="btnSearch" runat="server" Text="搜尋" OnClick="btnSearch_Click" OnClientClick="SearchClick();" /><br />
    </asp:Panel>
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    <asp:LinkButton ID="lkAddEmployee" runat="server" Text="新增員工" OnClick="btnAddEmployee_Click"
        OnClientClick="ClearData();"></asp:LinkButton>
    <asp:CheckBox runat="server" ID="cbHideEmployee" Text="隱藏離職員工" />
    <asp:Panel ID="divEdit" runat="server" Visible="false">
        <asp:HiddenField ID="hidWorkType" runat="server" />
        <table>
                <tr>
                <td>
                    員工編號
                </td>
                <td>
                        <asp:TextBox ID="tbEmpNo" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvEmpNo" runat="server" ControlToValidate="tbEmpNo"
                            Display="Dynamic" ErrorMessage="請輸入員工編號" ValidationGroup="Send"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    員工中文姓名
                </td>
                <td>
                    <asp:TextBox ID="tbEmpName" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmpName" runat="server" ControlToValidate="tbEmpName"
                        Display="Dynamic" ErrorMessage="請填寫員工中文姓名" ValidationGroup="Send"></asp:RequiredFieldValidator>
                    <asp:CheckBox ID="chkIsGrpPerformance" runat="server"/>不作團績計算
                    
                </td>
            </tr>
            <tr>
                <td>
                    員工Email
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                        Display="Dynamic" ErrorMessage="請填寫Email" ValidationGroup="Send"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    員工手機
                </td>
                <td>
                    <asp:TextBox ID="txtMobile" runat="server" MaxLength="10"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    員工分機
                </td>
                <td>
                    <asp:TextBox ID="txtExtension" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    員工到職日
                </td>
                <td>
                    <asp:TextBox ID="tbAvailableDate" runat="server"></asp:TextBox>
                    <asp:CheckBox ID="chkIsOfficial" runat="server" />正式員工(試用期結束)
                </td>
            </tr>
            <tr>
                <td>
                    員工所屬部門
                </td>
                <td>
                    <asp:DropDownList ID="ddlParentDept" runat="server" DataValueField="DeptId" DataTextField="DeptName"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlParentDept_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlDept" runat="server" DataValueField="DeptId" DataTextField="DeptName">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvDept" runat="server" ControlToValidate="ddlDept"
                        Display="Dynamic" ErrorMessage="請填寫員工所屬部門" ValidationGroup="Send"></asp:RequiredFieldValidator>
                </td>
            </tr>
                <tr>
                    <td>
                        員工職級
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlEmpLevel" runat="server" />
                    </td>
                </tr>
            <asp:Panel runat ="server" ID="divKPI" Visible="true">
                <tr>
                    <td>
                        期別
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rblMon" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                            <asp:ListItem Selected="True" Value="0" >本月</asp:ListItem>
                            <asp:ListItem Value="-1" >上月</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>
                        目標KPI(萬)
                    </td>
                    <td>
                        <asp:TextBox ID="txtKPI" runat="server" ClientIDMode="Static" ></asp:TextBox>
                        <asp:HiddenField ID="hdKPI" runat="server" ClientIDMode="Static" />
                    </td>
                </tr>
                     <tr>
                        <td>
                            目標上檔數
                        </td>
                        <td>
                            <asp:TextBox ID="txtDeals" runat="server" ClientIDMode="Static" ></asp:TextBox>
                            <asp:HiddenField ID="hdDeals" runat="server" ClientIDMode="Static" />
                        </td>
                    </tr>
            </asp:Panel>
                <tr>
                    <td>
                        員工離職日
                    </td>
                    <td>
                        <asp:TextBox ID="tbDepartureDate" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr class="teamSet" style="display:none;">
                    <td>
                        組別
                    </td>
                    <td>
                        <asp:TextBox ID="tbTeamNo" runat="server"></asp:TextBox>(報表計算獎金用)
                    </td>
                </tr>
                <tr class="teamSet" style="display:none;">
                    <td>
                        組長
                    </td>
                    <td>
                        <asp:CheckBox ID="cbTeamManager" runat="server" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(報表計算獎金用)
                    </td>
                </tr>
                <tr class="teamSet" style="display:none;">
                    <td>
                        是否擔任主管<br />(報表計算獎金用)
                    </td>
                    <td>
                        <asp:CheckBoxList ID="cblDeptManager" runat="server" RepeatColumns="3"></asp:CheckBoxList>
                    </td>
                </tr>
                
                <tr class="Cross"  style="display:none;">
                    <td colspan="2">
                        <asp:CheckBox ID="chkCrossDeptTeam" runat="server" onclick="ShowCrossSetting(this)"/>系統權限跨區/組設定
                        <br />
                        <font color="gray" size="2">設定跨區/組權限用。組別以半形數字呈現，組和組之間已半形,隔開，<br />跨區以本區加跨區勾選，沒填組別，則代表該區全部組別適用</font>
                    </td>
                </tr>
                <tr class="Cross CrossSetting"  style="display:none;">
                    <td colspan="2">
                        <asp:Repeater ID="rptCrossDeptTeam" runat="server" OnItemDataBound="rptCrossDeptTeam_ItemDataBound">
                            <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td width="200px">
                                                <asp:CheckBox ID="chkDept" runat="server" />
                                                <asp:HiddenField ID="hidDept" runat="server" />
                                                
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtTeam" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                            </ItemTemplate>
                        </asp:Repeater>
                        <p id="pCrossSetting" class="if-error" style="display: none;">組別輸入錯誤</p>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
   
                <tr>
                <td colspan="2">
                    <asp:RadioButtonList ID="rdlIsInvisible" runat="server" RepeatDirection="Horizontal"
                        RepeatLayout="Flow">
                        <asp:ListItem Text="顯示" Value="False" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="隱藏" Value="True"></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator ID="rfvIsInvisible" runat="server" ControlToValidate="rdlIsInvisible"
                        Display="Dynamic" ErrorMessage="請填寫員工顯示狀態" ValidationGroup="Send"></asp:RequiredFieldValidator>
                </td>
                </tr>
            <tr>
                <td>
                    <asp:HiddenField ID="hidUserId" runat="server" />
                    <asp:Button ID="btnSave" Text="儲存" runat="server" OnClientClick="SaveCheck();" OnClick="btnSave_Click" ValidationGroup="Send" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" Text="取消" runat="server" OnClientClick="return ClosePanel();" />
                </td>
            </tr>
        </table>
    </asp:Panel>
        <script type="text/javascript">
            Sys.Application.add_load(BindFunctions);
        </script>
    <asp:Panel ID="divEmployeeList" runat="server">
        <asp:GridView ID="gvEmp" runat="server" OnRowDataBound="gvEmp_RowDataBound" OnRowCommand="gvEmp_OnRowCommand"
                GridLines="Vertical" ForeColor="Black" CellPadding="4" BorderWidth="1px" 
                BorderStyle="None" BorderColor="#DEDFDE" BackColor="White" CssClass="tablesorterBlue"
                EmptyDataText="無符合的資料" AutoGenerateColumns="False" Font-Size="13px" >
            <FooterStyle BackColor="#CCCC99"></FooterStyle>
            <RowStyle BackColor="#F7F7DE"></RowStyle>
            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
                <HeaderStyle BackColor="#6B696B" Font-Bold="True">
                </HeaderStyle>
            <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
            <Columns>
                    <asp:BoundField HeaderText="員工編號" DataField="EmpNo" ReadOnly="True">
                        <ItemStyle Width="70px" Wrap="False" HorizontalAlign="Left" />
                </asp:BoundField>
                    <asp:TemplateField HeaderText="員工中文姓名" SortExpression="emp_name">
                    <ItemTemplate>
                        <asp:LinkButton ID="lkEmpName" runat="server" CommandName="UPD"></asp:LinkButton>
                    </ItemTemplate>
                    <ItemStyle Width="100px" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="員工英文姓名">
                    <ItemTemplate>
                        <asp:Label ID="lblEmpEName" runat="server"></asp:Label>
                    </ItemTemplate>
                        <ItemStyle Width="100px" HorizontalAlign="Center" />
                </asp:TemplateField>
                    <asp:TemplateField HeaderText="員工Email">
                        <ItemTemplate>
                            <asp:Label ID="lblEmail" runat="server"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="150px" HorizontalAlign="Center" />
                    </asp:TemplateField>
                <asp:TemplateField HeaderText="員工手機">
                    <ItemTemplate>
                        <asp:Label ID="lblMoblie" runat="server"></asp:Label>
                    </ItemTemplate>
                        <ItemStyle Width="90px" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="員工分機">
                    <ItemTemplate>
                        <asp:Label ID="lblExtension" runat="server"></asp:Label>
                    </ItemTemplate>
                        <ItemStyle Width="90px" HorizontalAlign="Center" />
                </asp:TemplateField>
                    <asp:TemplateField HeaderText="員工到職日">
                    <ItemTemplate>
                            <asp:Label runat="server" ID="lbAvailableDate"></asp:Label>
                    </ItemTemplate>
                        <ItemStyle Width="80px" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="員工所屬部門">
                    <ItemTemplate>
                        <asp:Label ID="lblDeptName" runat="server"></asp:Label>
                    </ItemTemplate>
                        <ItemStyle Width="90px" HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="員工職級" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lbEmpLevel" runat="server"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="70px" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="目標KPI(萬)" Visible="false">
                    <ItemTemplate>
                            <asp:Label ID="lbKPITarget" runat="server"></asp:Label>
                    </ItemTemplate>
                        <ItemStyle Width="90px" HorizontalAlign="Center" />
                </asp:TemplateField>
                    <asp:TemplateField HeaderText="目標上檔數" Visible="false">
                    <ItemTemplate>
                            <asp:Label ID="lbDealTarget" runat="server"></asp:Label>
                    </ItemTemplate>
                        <ItemStyle Width="90px" HorizontalAlign="Center" />
                </asp:TemplateField>
                    <asp:TemplateField HeaderText="員工離職日" >
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lbDepartureDate"></asp:Label>
                    </ItemTemplate>
                        <ItemStyle Width="90px" HorizontalAlign="Center" />
                </asp:TemplateField>
                    <asp:TemplateField HeaderText="顯示/隱藏">
                    <ItemTemplate>
                            <asp:Label ID="lblIsInvisible" runat="server"></asp:Label>
                    </ItemTemplate>
                        <ItemStyle Width="70px" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="是否擔任主管">
                    <ItemTemplate>
                            <asp:Label ID="lblIsManager" runat="server"></asp:Label>
                    </ItemTemplate>
                        <ItemStyle Width="70px" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="組別">
                    <ItemTemplate>
                            <asp:Label ID="lblTeamNo" runat="server"></asp:Label>
                    </ItemTemplate>
                        <ItemStyle Width="70px" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="組長">
                    <ItemTemplate>
                            <asp:Label ID="lblTeamManager" runat="server"></asp:Label>
                    </ItemTemplate>
                        <ItemStyle Width="70px" HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

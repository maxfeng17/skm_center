﻿using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System.Collections.Generic;
using LunchKingSite.Core;
using System.Linq;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class EmpSet : RolePage, IEmpSetView
    {
        #region IDeptSetView屬性
        private EmpSetPresenter _presenter;
        private IHumanProvider hp = null;
        private List<Cross> listcross = new List<Cross>();

        public EmpSetPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public EmpSetViewWorkType ViewWorkType
        {
            get
            {
                if (ViewState["ViewWorkType"] != null)
                    return (EmpSetViewWorkType)ViewState["ViewWorkType"];
                else
                    return EmpSetViewWorkType.List;
            }
            set
            {
                ViewState["ViewWorkType"] = value;
            }
        }

        public string EmpId
        {
            get
            {
                return (ViewState["EmpId"] != null) ? ViewState["EmpId"].ToString() : string.Empty;
            }
            set
            {
                ViewState["EmpId"] = value;
            }
        }

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        public List<KeyValuePair<string, object>> filter
        {
            get
            {
                return new List<KeyValuePair<string, object>> { new KeyValuePair<string, object>(ddlSearch.SelectedValue, txtFilter.Text), new KeyValuePair<string, object>(ViewEmployee.Columns.IsInvisible, cbHideEmployee.Checked) };
            }
        }

        public EmployeeDept parentDeptId
        {
            get
            {
                EmployeeDept dept = EmployeeDept.S000;
                EmployeeDept.TryParse(ddlParentDept.SelectedValue, out dept);
                return dept;
            }
            set
            {
                ddlParentDept.SelectedValue = value.ToString();
            }
        }

        public EmployeeDept parentDeptIdSearch
        {
            get
            {
                EmployeeDept dept = EmployeeDept.S000;
                EmployeeDept.TryParse(ddlParentDeptSearch.SelectedValue, out dept);
                return dept;
            }
        }

        public bool CrossDeptTeam
        {
            get
            {
                return chkCrossDeptTeam.Checked;
            }
        }
        public bool IsGrpPerformance
        {
            get
            {
                return chkIsGrpPerformance.Checked;
            }
            set
            {
                chkIsGrpPerformance.Checked = value;
            }
        }
        public bool IsOfficial
        {
            get
            {
                return chkIsOfficial.Checked;
            }
            set
            {
                chkIsOfficial.Checked = value;
            }
        }

        public bool KPIVisible { get; set; }
        #endregion

        #region IDeptSetView method
        public void SetPageWorkType(EmpSetViewWorkType workType)
        {
            switch (workType)
            {
                case EmpSetViewWorkType.List:
                    divEdit.Visible = false;
                    break;
                case EmpSetViewWorkType.Add:
                case EmpSetViewWorkType.Modify:
                    divEdit.Visible = true;
                    break;
            }
            ViewWorkType = workType;
        }

        public void ShowList(ViewEmployeeCollection viewEmpCol)
        {
            gvEmp.DataSource = viewEmpCol;
            gvEmp.DataBind();

            if (gvEmp.Rows.Count > 0)
            {
                gvEmp.UseAccessibleHeader = true;
                gvEmp.HeaderRow.TableSection = TableRowSection.TableHeader;
                gvEmp.FooterRow.TableSection = TableRowSection.TableFooter;
            }
        }

        public void ShowEmpData(Employee emp)
        {
            tbEmpNo.Text = emp.EmpNo;
            tbEmpName.Text = emp.EmpName;
            txtEmail.Text = MemberFacade.GetUserEmail(emp.UserId);
            txtEmail.ReadOnly = true;
            txtEmail.ForeColor = Color.Gray;
            txtMobile.Text = emp.Mobile ?? string.Empty;
            ddlDept.SelectedValue = emp.DeptId;
            ddlEmpLevel.SelectedValue = emp.EmpLevel != null ? emp.EmpLevel.Value.ToString() : null;
            rdlIsInvisible.SelectedValue = emp.IsInvisible.ToString();
            hidUserId.Value = emp.UserId.ToString();
            rblMon.SelectedIndex = 0;
            if (emp.DeptManager != null)
            {
                string[] depts = emp.DeptManager.Split(",");
                for (int i = 0; i < cblDeptManager.Items.Count; i++)
                {
                    if (depts.Contains(cblDeptManager.Items[i].Value))
                    {
                        cblDeptManager.Items[i].Selected = true;
                    }
                }
            }

            if (KPIVisible)
            {
                divKPI.Visible = true;
                var m1 = HumanFacade.GetSalesVolumeMonth(emp.UserId);
                var m2 = HumanFacade.GetSalesVolumeMonth(emp.UserId, DateTime.Now.AddMonths(-1).ToString("yyyyMM"));
                txtKPI.Text = m1 != null ? m1.KpiTarget.ToString() : string.Empty;
                hdKPI.Value = m2 != null ? m2.KpiTarget.ToString() : string.Empty;
                txtDeals.Text = m1 != null ? m1.DealTarget.ToString() : string.Empty;
                hdDeals.Value = m2 != null ? m2.DealTarget.ToString() : string.Empty;
            }
            else
            {
                divKPI.Visible = false;
            }


            if (emp.AvailableDate != null) tbAvailableDate.Text = emp.AvailableDate.Value.ToString("yyyy/MM/dd");
            if (emp.DepartureDate != null) tbDepartureDate.Text = emp.DepartureDate.Value.ToString("yyyy/MM/dd");
            if (emp.TeamNo != null) tbTeamNo.Text = Convert.ToString(emp.TeamNo);
            cbTeamManager.Checked = emp.TeamManager;



            if (!string.IsNullOrEmpty(emp.CrossDeptTeam))
            {
                chkCrossDeptTeam.Checked = true;
                listcross = ProposalFacade.GetCrossDeptTeam(emp.CrossDeptTeam);
            }

            foreach (RepeaterItem channelItem in rptCrossDeptTeam.Items)
            {
                CheckBox chkDept = (CheckBox)channelItem.FindControl("chkDept");
                TextBox txtTeamNo = (TextBox)channelItem.FindControl("txtTeam");
                HiddenField hidDept = (HiddenField)channelItem.FindControl("hidDept");

                if (listcross.Exists(x => x.Dept == hidDept.Value))
                {
                    Cross c = listcross.Find(x => x.Dept == hidDept.Value);
                    chkDept.Checked = true;
                    txtTeamNo.Text = c.Team;
                }

            }
        }

        public void SetParentDepartment(DepartmentCollection parentDeptList)
        {
            ddlParentDept.DataSource = parentDeptList;
            ddlParentDept.DataBind();

            ddlParentDeptSearch.DataSource = parentDeptList;
            ddlParentDeptSearch.DataBind();
        }

        public void SetDepartment(DepartmentCollection deptList)
        {
            ddlDept.DataSource = deptList;
            ddlDept.DataBind();
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "showTeamSet", "CheckDisplayTeamSet();", true);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "parentDeptOnChange", "ddlParentDeptOnChange();", true);
        }
        public void SetDeptManager(DepartmentCollection deptList)
        {
            cblDeptManager.DataSource = deptList;
            cblDeptManager.DataTextField = "DeptName";
            cblDeptManager.DataValueField = "DeptId";
            cblDeptManager.DataBind();
        }
        public void SetDepartmentSearch(DepartmentCollection deptList)
        {
            ddlDeptSearch.DataSource = deptList;
            ddlDeptSearch.DataBind();
        }

        public void SetEmpLevel()
        {
            var list = Enum.GetNames(typeof(EmployeeLevel)).
                 Select(s => new { Text = s.Replace(EmployeeLevel.None.ToString(), ""), Value = (Int32)(Enum.Parse(typeof(EmployeeLevel), s)) }).ToList();
            ddlEmpLevel.DataSource = list;
            ddlEmpLevel.DataTextField = "Text";
            ddlEmpLevel.DataValueField = "Value";
            ddlEmpLevel.DataBind();
        }

        public void SetCrossDeptTeam(DepartmentCollection deptList)
        {
            rptCrossDeptTeam.DataSource = deptList;
            rptCrossDeptTeam.DataBind();
        }

        public void ShowMessage(string msg)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + msg + "');", true);
        }
        #endregion

        #region Event
        public event EventHandler<DataEventArgs<Employee>> SaveEmp;
        public event GridViewCommandEventHandler ShowEmp;
        public event EventHandler OnSearchClicked;
        public event EventHandler OnParentDeptSearchSelectedIndexChanged;
        public event EventHandler OnParentDeptSelectedIndexChanged;
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();

            if (!Page.IsPostBack)
            {
                if (!_presenter.OnViewInitialized())
                    return;
                InitialControl();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.OnSearchClicked != null)
                this.OnSearchClicked(this, e);
        }

        protected void gvEmp_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is ViewEmployee)
            {
                ViewEmployee item = (ViewEmployee)e.Row.DataItem;
                LinkButton lkEmpName = (LinkButton)e.Row.FindControl("lkEmpName");
                Label lblEmpEName = (Label)e.Row.FindControl("lblEmpEName");
                Label lblEmail = (Label)e.Row.FindControl("lblEmail");
                Label lblMoblie = (Label)e.Row.FindControl("lblMoblie");
                Label lblExtension = (Label)e.Row.FindControl("lblExtension");
                Label lblDeptName = (Label)e.Row.FindControl("lblDeptName");
                Label lbEmpLevel = (Label)e.Row.FindControl("lbEmpLevel");
                Label lblIsInvisible = (Label)e.Row.FindControl("lblIsInvisible");
                Label lbAvailableDate = (Label)e.Row.FindControl("lbAvailableDate");
                Label lbDepartureDate = (Label)e.Row.FindControl("lbDepartureDate");
                Label lbKPITarget = (Label)e.Row.FindControl("lbKPITarget");
                Label lbDealTarget = (Label)e.Row.FindControl("lbDealTarget");
                Label lblIsManager = (Label)e.Row.FindControl("lblIsManager");
                Label lblTeamNo = (Label)e.Row.FindControl("lblTeamNo");
                Label lblTeamManager = (Label)e.Row.FindControl("lblTeamManager");

                if (!string.IsNullOrEmpty(item.EmpName))
                    lkEmpName.Text = item.EmpName;
                lkEmpName.CommandArgument = item.EmpId;
                if (!string.IsNullOrEmpty(item.Email) && item.Email.Contains("@"))
                {
                    lblEmpEName.Text = item.Email.Substring(0, item.Email.IndexOf('@'));
                    lblEmail.Text = item.Email;
                }
                if (!string.IsNullOrEmpty(item.Mobile))
                {
                    int mobile;
                    int.TryParse(item.Mobile, out mobile);
                    if (!mobile.Equals(0))
                        lblMoblie.Text = string.Format("{1}{0:####-###-###}", mobile, 0);
                }
                lblExtension.Text = item.Extension;
                lblDeptName.Text = item.DeptName;
                if (item.IsInvisible)
                {
                    lblIsInvisible.Text = "隱藏";
                    e.Row.ForeColor = System.Drawing.Color.Gray;
                    lkEmpName.ForeColor = System.Drawing.Color.Gray;
                }
                else
                    lblIsInvisible.Text = "顯示";

                int lbLevelIndex = -1;
                int lbKPIIndex = -1;
                int lbDealIndex = -1;
                for (int i = 0; i < gvEmp.Columns.Count; i++)
                {
                    if (e.Row.Cells[i].Controls.Count > 0 && e.Row.Cells[i].Controls[1].ID == "lbEmpLevel")
                    {
                        lbLevelIndex = i;
                    }
                    if (e.Row.Cells[i].Controls.Count > 0 && e.Row.Cells[i].Controls[1].ID == "lbKPITarget")
                    {
                        lbKPIIndex = i;
                    }
                    if (e.Row.Cells[i].Controls.Count > 0 && e.Row.Cells[i].Controls[1].ID == "lbDealTarget")
                    {
                        lbDealIndex = i;
                    }
                }

                string deptManager = item.DeptManager;
                if (!string.IsNullOrEmpty(deptManager))
                {
                    string[] depts = deptManager.Split(",");
                    foreach (string dept in depts)
                    {
                        Department _dept = hp.DepartmentGet(dept);
                        if (_dept != null)
                        {
                            lblIsManager.Text += _dept.DeptName + "<br />";
                        }

                    }
                }

                int? teamNo = item.TeamNo;
                if (teamNo != null)
                {
                    lblTeamNo.Text = Convert.ToString(teamNo);
                }

                bool teamManager = item.TeamManager;
                lblTeamManager.Text = teamManager ? "是" : "";


                if (KPIVisible)
                {
                    gvEmp.Columns[lbLevelIndex].Visible = true;
                    lbEmpLevel.Text = (item.EmpLevel != null) ? ((EmployeeLevel)item.EmpLevel.Value).ToString() : string.Empty;
                    gvEmp.Columns[lbKPIIndex].Visible = true;
                    lbKPITarget.Text = (item.KpiTarget != null) ? item.KpiTarget.Value.ToString() : string.Empty;
                    gvEmp.Columns[lbDealIndex].Visible = true;
                    lbDealTarget.Text = (item.DealTarget != null) ? item.DealTarget.Value.ToString() : string.Empty;
                }
                else
                {
                    gvEmp.Columns[lbLevelIndex].Visible = false;
                    gvEmp.Columns[lbKPIIndex].Visible = false;
                    gvEmp.Columns[lbDealIndex].Visible = false;
                }

                // 到職/離職日
                if (item.AvailableDate != null)
                    lbAvailableDate.Text = item.AvailableDate.Value.ToString("yyyy/MM/dd");
                if (item.DepartureDate != null)
                    lbDepartureDate.Text = item.DepartureDate.Value.ToString("yyyy/MM/dd");
            }
        }

        protected void gvEmp_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (ShowEmp != null)
            {
                SetPageWorkType(EmpSetViewWorkType.Modify);
                EmpId = e.CommandArgument.ToString();
                if (e.CommandName == "UPD")
                    ShowEmp(this, e);
            }

            if (gvEmp.Rows.Count > 0)
            {
                gvEmp.UseAccessibleHeader = true;
                gvEmp.HeaderRow.TableSection = TableRowSection.TableHeader;
                gvEmp.FooterRow.TableSection = TableRowSection.TableFooter;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //string email = txtEmail.Text.Trim();
            //if (!email.Contains(@"@17life.com") && !email.Contains(@"@payeasy.com"))
            //{
            //    ShowMessage("需使用 17life 或 Payeasy Email帳號!!");
            //    return;

            //}
            //else if (txtMobile.Text.Trim().Length < 10)
            //{
            //    ShowMessage("員工電話輸入錯誤!!請輸入10碼");
            //    return;
            //}
            //else
            //{
            if (ddlParentDept.SelectedValue == "S000")
            {
                int teamNo = 0;
                if ((!string.IsNullOrWhiteSpace(tbTeamNo.Text) && int.TryParse(tbTeamNo.Text, out teamNo) == false) ||//填寫錯誤組別(非數字)
                    (string.IsNullOrWhiteSpace(tbTeamNo.Text) && cbTeamManager.Checked == true))//有勾組長沒填組別
                {
                    ShowMessage("請輸入正確的組別!");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "showTeamSet", "CheckDisplayTeamSet();", true);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "parentDeptOnChange", "ddlParentDeptOnChange();", true);
                    return;
                }
            }

            if (SaveEmp != null)
                SaveEmp(this, new DataEventArgs<Employee>(GetEmployeeData()));
            //}
        }

        protected void btnAddEmployee_Click(object sender, EventArgs e)
        {
            tbEmpNo.Text = string.Empty;
            tbEmpName.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtMobile.Text = string.Empty;
            txtExtension.Text = string.Empty;
            hidUserId.Value = string.Empty;
            ddlDept.SelectedIndex = -1;
            ddlEmpLevel.SelectedIndex = -1;
            txtEmail.ReadOnly = false;
            divKPI.Visible = KPIVisible;
            ddlParentDept.SelectedIndex = 0;
            chkCrossDeptTeam.Checked = false;
            SetPageWorkType(EmpSetViewWorkType.Add);
        }

        protected void ddlParentDeptSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.OnParentDeptSearchSelectedIndexChanged != null)
                this.OnParentDeptSearchSelectedIndexChanged(this, e);
        }

        protected void ddlParentDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.OnParentDeptSelectedIndexChanged != null)
                this.OnParentDeptSelectedIndexChanged(this, e);
        }

        protected void rptCrossDeptTeam_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is Department)
            {
                Department dataItem = (Department)e.Item.DataItem;

                CheckBox chkDept = (CheckBox)e.Item.FindControl("chkDept");
                TextBox txtTeam = (TextBox)e.Item.FindControl("txtTeam");
                HiddenField hidDept = (HiddenField)e.Item.FindControl("hidDept");

                chkDept.Text = dataItem.DeptName;
                hidDept.Value = dataItem.DeptId;

            }
        }
        #endregion

        #region private method
        private void InitialControl()
        {
            ddlSearch.Items.Add(new ListItem("員工中文姓名", ViewEmployee.Columns.EmpName));
            ddlSearch.Items.Add(new ListItem("員工Email", ViewEmployee.Columns.Email));
            ddlSearch.Items.Add(new ListItem("員工電話", ViewEmployee.Columns.Mobile));
            ddlSearch.Items.Add(new ListItem("員工所屬部門", ViewEmployee.Columns.DeptId));
        }
        private Employee GetEmployeeData()
        {
            Employee emp = new Employee();
            emp.EmpNo = tbEmpNo.Text.Trim();
            emp.EmpName = tbEmpName.Text.Trim();
            if (!string.IsNullOrEmpty(hidUserId.Value))
            {
                emp.UserId = int.Parse(hidUserId.Value);
            }
            else
            {
                emp.UserId = MemberFacade.GetUniqueId(txtEmail.Text.Trim());
            }
            emp.Mobile = txtMobile.Text.Trim().Replace("-", string.Empty);
            emp.Extension = txtExtension.Text.Trim();
            emp.DeptId = ddlDept.SelectedValue;
            emp.EmpLevel = int.Parse(ddlEmpLevel.SelectedValue);
            bool isInvisible;
            bool.TryParse(rdlIsInvisible.SelectedValue, out isInvisible);
            emp.IsInvisible = isInvisible;
            emp.YearMon = divKPI.Visible ? DateTime.Now.AddMonths(int.Parse(rblMon.SelectedValue)).ToString("yyyyMM") : string.Empty;
            emp.KPI = (divKPI.Visible && !string.IsNullOrEmpty(txtKPI.Text.Trim())) ? Convert.ToDouble(txtKPI.Text.Trim()) : (double?)null;
            emp.DealTarget = (divKPI.Visible && !string.IsNullOrEmpty(txtDeals.Text.Trim())) ? int.Parse(txtDeals.Text.Trim()) : (int?)null;
            if (!string.IsNullOrEmpty(tbAvailableDate.Text))
                emp.AvailableDate = DateTime.ParseExact(tbAvailableDate.Text.Trim(), "yyyy/MM/dd", null);
            if (!string.IsNullOrEmpty(tbDepartureDate.Text))
                emp.DepartureDate = DateTime.ParseExact(tbDepartureDate.Text.Trim(), "yyyy/MM/dd", null);
            IEnumerable<string> allDeptChecked = (from item in cblDeptManager.Items.Cast<ListItem>()
                                                  where item.Selected
                                                  select item.Value).ToList();
            emp.IsGrpPerformance = (IsGrpPerformance ? 1 : 0);
            emp.IsOfficial = (IsOfficial ? 1 : 0);

            if (ddlParentDept.SelectedValue == "S000")
            {
                //業務部門才要存 [組別]、[組長]
                if (!string.IsNullOrWhiteSpace(tbTeamNo.Text))
                    emp.TeamNo = Convert.ToInt32(tbTeamNo.Text);
                else
                    emp.TeamNo = null;

                emp.TeamManager = cbTeamManager.Checked;
            }

            string sdept = "";
            foreach (string d in allDeptChecked)
            {
                sdept += d + ",";
            }

            emp.DeptManager = sdept;


            //跨區設定
            string strCrossDeptTeam = "";
            foreach (RepeaterItem channelItem in rptCrossDeptTeam.Items)
            {
                CheckBox chkDept = (CheckBox)channelItem.FindControl("chkDept");
                TextBox txtTeamNo = (TextBox)channelItem.FindControl("txtTeam");
                HiddenField hidDept = (HiddenField)channelItem.FindControl("hidDept");


                if (chkDept.Checked)
                {
                    strCrossDeptTeam += hidDept.Value;

                    if (!string.IsNullOrEmpty(txtTeamNo.Text))
                        strCrossDeptTeam += "[" + txtTeamNo.Text.TrimEnd(',') + "]";

                    strCrossDeptTeam += "/";
                }
            }

            emp.CrossDeptTeam = strCrossDeptTeam.TrimEnd('/');

            return emp;
        }
        #endregion
    }
}
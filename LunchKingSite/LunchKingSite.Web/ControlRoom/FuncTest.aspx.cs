﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class FuncTest : System.Web.UI.Page
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected void Page_Load(object sender, EventArgs e)
        {
            litSybaseUrl.Text = config.SybaseUrl;
            string address;
            var client = ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection;
            var qasEndpoint = client.Endpoints.Cast<ChannelEndpointElement>()
                .SingleOrDefault(endpoint => endpoint.Name == "fpMsServiceSoap");
            if (qasEndpoint != null)
            {
                litIte2Url.Text = qasEndpoint.Address.ToString();
            }
        }

        protected void btnIte2Test_Click(object sender, EventArgs e)
        {
            if (RegExRules.CheckMobile(txtIte2Mobile.Text) == false)
            {
                lbIte2Message.Text = "手機號碼格式不正確";
                return;
            }
            SmsSendReply smsReply;
            new iTe2Sender().Send("Hello " + txtIte2Mobile.Text , new string[] { txtIte2Mobile.Text }, 
                new SmsExtraInfo { CouponId = 0 }, out smsReply);
            if (smsReply == SmsSendReply.Empty)
            {
                lbIte2Message.Text = "發送失敗";
            } else
            {
                lbIte2Message.Text = "發送完成 " + smsReply.OrderId;
            }
        }

        protected void btnSybaseTest_Click(object sender, EventArgs e)
        {
            if (RegExRules.CheckMobile(txtSybaseMobile.Text) == false)
            {
                lbSybaseMessage.Text = "手機號碼格式不正確";
                return;
            }
            SmsSendReply smsReply;
            new SybaseSender().Send("Hello " + txtSybaseMobile.Text , txtSybaseMobile.Text, new SmsExtraInfo
            {
                CouponId = 0
            }, out smsReply);

            if (smsReply == SmsSendReply.Empty)
            {
                lbSybaseMessage.Text = "發送失敗";
            }
            else
            {
                lbSybaseMessage.Text = "發送完成 " + smsReply.OrderId;
            }
        }
    }
}
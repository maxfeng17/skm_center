﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="servicedetail.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.servicedetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        var editor;
        $(document).ready(function () {
            editor = CKEDITOR.replace('<%=txtMessage.ClientID%>');
        });

        function chkData() {
            var msg = editor.getData();
            msg = $.trim(msg.replace(/&nbsp;/g, ''));
            if (msg == '')
            {
                window.alert('請填寫 [回覆內容]');
                return false;
            }
            return true;
        }
    </script>

    <style type="text/css">
        body
        {
            font-size: 10px;
        }
        #tabs
        {
            font-size: smaller;
        }

        fieldset
        {
            margin: 10px 0px;
            border: solid 1px gray;
        }
        fieldset legend
        {
            color: #4F8AFF;
            font-weight: bolder;
        }
        ul
        {
            list-style-type: none;
            margin: 0 0 0 0;
        }
        li
        {
            color: #4D4D4D;
            font-weight: bold;
        }
        
        ul.status li
        {
            float: left;
        }
        ul.target input
        {
            width: 750px;
        }
        
        input
        {
            border: solid 1px grey;
        }
        textarea
        {
            border: solid 1px grey;
        }
        
        input.dealName
        {
            width: 750px;
        }
        input.date
        {
            width: 750px;
        }
        
        .price
        {
            color: red;
            font-weight: bolder;
        }
        
        .intro
        {
            float: left;
            margin: 5px 0px 5px 0px;
            border: none;
        }
        .intro legend
        {
            color: #4F8AFF;
            font-weight: bolder;
        }
        .intro textarea
        {
            width: 750px;
        }
        
        .watermarkOn
        {
            color: #AAAAAA;
            font-style: italic;
        }
        
        #avpane ul
        {
            margin-bottom: 3px;
            border: dashed 1px gray;
            padding: 2px 2px 2px 5px;
        }
        
        #avpane ul.hover
        {
            background-color: #E5ECF9;
        }
        
        #avpane li
        {
            font-weight: normal;
            font-size: smaller;
            margin: 0px;
        }
        
        #avpane li.eb, #avpane li.db, #avpane li.cb
        {
            display: none;
            float: left;
            padding-right: 15px;
            text-decoration: underline;
            color: blue;
            cursor: pointer;
        }
        
        .btn_left
        {
            float: left;
        }
        .btn_mid
        {
            float: left;
            margin-left: 100px;
        }
        .btn_right
        {
            float: right;
        }
        .btn_right a
        {
            margin: 0px 10px 0px 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td style="text-align: right">
                            消費者姓名:&nbsp;
                        </td>
                        <td>
                            <asp:Label ID="lblname" runat="server" Text=""></asp:Label>
                            &nbsp; (<asp:HyperLink ID="lnhistory" runat="server">查看歷史紀錄</asp:HyperLink>)
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            館別:&nbsp;
                        </td>
                        <td>
                            <table width="500px">
                                <tr>
                                    <td>
                                        <asp:HiddenField ID="hidType" runat="server" />
                                        <asp:Label ID="lblType" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            分類:&nbsp;
                        </td>
                        <td>
                            <table width="500px">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCateGory" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; text-align: right;">
                            問題描述:&nbsp;
                        </td>
                        <td>
                            <table width="500px">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            收件者Email:&nbsp;
                        </td>
                        <td>
                            <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            收件者電話:&nbsp;
                        </td>
                        <td>
                            <asp:Label ID="lblphone" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            來信時間:&nbsp;
                        </td>
                        <td>
                            <asp:Label ID="lblTime" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            處理狀態:&nbsp;
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlstatus" runat="server">
                                <asp:ListItem>請選擇</asp:ListItem>
                                <asp:ListItem Text="待處理" Value="wait"></asp:ListItem>
                                <asp:ListItem Text="處理中" Value="process"></asp:ListItem>
                                <asp:ListItem Text="完成" Value="complete"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            轉件項目
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rdlWorkType" runat="server" RepeatDirection="Horizontal"
                                RepeatLayout="Table">
                                <asp:ListItem Text="商品" Value="0"></asp:ListItem>
                                <asp:ListItem Text="好康" Value="1"></asp:ListItem>
                                <asp:ListItem Text="退貨" Value="2"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            緊急程度
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblPriority" runat="server" RepeatDirection="Horizontal"
                                RepeatLayout="Table">
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            回覆主題:&nbsp;
                        </td>
                        <td>
                            <asp:TextBox ID="txttitle" runat="server" Width="412px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            回覆內容:&nbsp;
                        </td>
                        <td>
                            <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Height="263px" Width="751px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            客服人員備註:&nbsp;
                        </td>
                        <td>
                            <asp:TextBox ID="txtremark" runat="server" TextMode="MultiLine" Height="47px" Width="412px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right">
                            回報廠商備註:&nbsp;
                        </td>
                        <td>
                            <asp:TextBox ID="txtRemark_Seller" runat="server" TextMode="MultiLine" Height="47px" Width="412px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:HiddenField ID="hidTpye" runat="server" />
                            <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="儲存" />
                            <asp:Button ID="btnSend" runat="server" OnClientClick="return chkData()" OnClick="btnSend_Click" Text="儲存並發送" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
                                <ItemTemplate>
                                    <fieldset>
                                        <legend>
                                            <%# Eval("createtime") %>
                                        </legend>
                                        <table>
                                            <tr>
                                                <td>
                                                    處理方式:
                                                </td>
                                                <td>
                                                    <%# Eval("sendType")%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    處理人員:
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCreatdId" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="color: Red">
                                                <td>
                                                    客服備註:
                                                </td>
                                                <td>
                                                    <%# (Eval("remark")== null ? "" : Eval("remark").ToString().Replace("\n","<br>"))%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    回覆內容:
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td style="width: 820px">
                                                    <asp:Label ID="lblm" runat="server" Text='<%# Eval("message")%>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    回報廠商備註:
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td style="width: 820px">
                                                    <asp:Label ID="Label1" runat="server" Text='<%# (Eval("remarkSeller")== null ? "" : Eval("remarkSeller").ToString().Replace("\n","<br>")) %>'></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

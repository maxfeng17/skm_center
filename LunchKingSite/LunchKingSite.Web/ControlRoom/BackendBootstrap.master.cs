using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

public partial class BackendBootstrap : System.Web.UI.MasterPage
{
    private static ISysConfProvider config;
    static BackendBootstrap()
    {
        config = ProviderFactory.Instance().GetConfig();
    }

    protected string SiteUrl
    {
        get
        {
            return config.SiteUrl.TrimEnd('/');
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AtmPostPage.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.AtmPostPage" %>
<%@ Import Namespace="LunchKingSite.WebLib" %>
<%@ Import Namespace="LunchKingSite.Core.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>訊息主動通知客戶端(模擬主機發送交易)</title>
<meta name="Generator" content="Microsoft FrontPage 4.0">
<meta name="Author" content="">
<meta name="Keywords" content="">
<meta name="Description" content="">
</head>

<body bgcolor="#99FFCC">
    <form id="form2" runat="server">
    <div>
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <asp:Button ID="Button2" runat="server" Text="模擬ATM退款成功資料匯入" onclick="Button2_Click" />
        <asp:Button ID="btnRefundFailed" runat="server" Text="模擬ATM退款失敗資料匯入" OnClick="btnRefundFailed_Click" />
        <br/>
        <asp:Label ID="Label2" runat="server"></asp:Label>
    </div>
    <br/>
    <div>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="模擬ATM繳款單號查詢" onclick="Button1_Click" />
        <asp:Label ID="Label1" runat="server"></asp:Label>
    </div>
    </form>
    
<form id=form1 name=form1 method=post action="<%=NotifyUrl%>">
<center><table width=80%>
<tr>
	<td width=15%>訊息通知別</td>
	<td width=85%><input type=text name=MsgID id=MsgID style="width: 20%" value="2"></td>
</tr>
<tr>
	<td>交易序號</td>
	<td><input type=text name=TransactionNo id=TransactionNo style="width: 30%" value="<%=Request.QueryString["seqno"]%>"></td>
</tr>
<tr>
	<td>訊息內容</td>
	<td><textarea name=body rows=10 cols="" style="width: 100%">
6245400952232<%=Request.QueryString["date"]%><%=Request.QueryString["senq"]%>     <%=Request.QueryString["amount"]%>00<%=Request.QueryString["date"]%>1600008225560609C+98909040454822000025554**1668*           95948000000000000000000000560609                                                                                                                                                                   </textarea></td>
</tr>
</table><br>
<input type="submit">&nbsp;&nbsp;<input type="reset"></center>
</form>
</body>
</html>

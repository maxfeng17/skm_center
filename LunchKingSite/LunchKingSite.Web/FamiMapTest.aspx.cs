﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web
{
    public partial class FamiMapTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hidCvstemp.Value = Request["cvstemp"] ?? string.Empty;
            hidCvsId.Value = Request["cvsid"] ?? string.Empty;
            hidCvsName.Value= Request["cvsname"] ?? string.Empty;
            
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            StringBuilder sb = new StringBuilder();
            sb.Append("https://" +hidCvsName.Value +  "/ppon/buy.aspx?");
            sb.Append("cvsid=");
            sb.Append(hidCvsId.Value);

            sb.Append("&cvsspot=");
            sb.Append(txtCvsspot.Text);

            sb.Append("&cvstemp=");
            sb.Append(HttpUtility.UrlEncode(hidCvstemp.Value));

            sb.Append("&name=");
            sb.Append(HttpUtility.UrlEncode(txtName.Text));

            sb.Append("&tel=");
            sb.Append(HttpUtility.UrlEncode(txtTel.Text));

            sb.Append("&addr=");
            sb.Append(HttpUtility.UrlEncode(txtAddr.Text));



            Response.Redirect(sb.ToString());
        }

        protected void btnSubmit2_Click(object sender, EventArgs e)
        {

            StringBuilder sb = new StringBuilder();
            sb.Append("https://" + hidCvsName.Value + "/ppon/buy.aspx?");
            sb.Append("cvsid=");
            sb.Append(hidCvsId.Value);

            sb.Append("&cvsspot=");
            sb.Append(txtCvsspot2.Text);

            sb.Append("&cvstemp=");
            sb.Append(HttpUtility.UrlEncode(hidCvstemp.Value));

            sb.Append("&name=");
            sb.Append(HttpUtility.UrlEncode(txtName2.Text));

            sb.Append("&tel=");
            sb.Append(HttpUtility.UrlEncode(txtTel2.Text));

            sb.Append("&addr=");
            sb.Append(HttpUtility.UrlEncode(txtAddr2.Text));



            Response.Redirect(sb.ToString());
        }
    }
}
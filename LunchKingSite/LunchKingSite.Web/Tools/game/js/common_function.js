//共用函式放置處，記得先載入再引用
//算剩餘時間
function remain_time(mission_time, client_time, serverTime, serverStartTime) {
    
    //console.log(jQuery.type(mission_time));
    //console.log(jQuery.type(client_time));
    if (jQuery.type(mission_time) == 'string') {
        mission_time = Date.parse(mission_time);
    }    
    let remain_time = mission_time - client_time;// - (clientTime - clientStartTime);    
    if (remain_time >= 0) {
        let days = Math.floor(remain_time / (1000 * 60 * 60 * 24))
        let hours = Math.floor(remain_time % (1000 * 60 * 60 * 24) / (1000 * 60 * 60))
        let minutes = Math.floor(remain_time % (1000 * 60 * 60) / (1000 * 60))
        let seconds = Math.floor(remain_time % (1000 * 60) / 1000)
        // console.log(days+"天"+hours+"小時"+minutes+"分鐘"+seconds+"秒")
        if (hours < 10) {
            hours = '0' + hours;
        } else {
            hours = hours.toString();
        }
        if (minutes < 10) {
            minutes = '0' + minutes;
        } else {
            minutes = minutes.toString();
        }
        if (seconds < 10) {
            seconds = '0' + seconds;
        } else {
            seconds = seconds.toString();
        }
        return { days: days, hours: hours, minutes: minutes, seconds: seconds };
    } else {
        console.log("Time's up")
        return { hours: '00', minutes: '00', seconds: '00' }
    }
}

function remain_time2(countdownSecs, clientStartTime) {
    let remain_time = countdownSecs * 1000 - (new Date().getTime() - clientStartTime.getTime());
    if (remain_time >= 0) {
        let days = Math.floor(remain_time / (1000 * 60 * 60 * 24))
        let hours = Math.floor(remain_time % (1000 * 60 * 60 * 24) / (1000 * 60 * 60))
        let minutes = Math.floor(remain_time % (1000 * 60 * 60) / (1000 * 60))
        let seconds = Math.floor(remain_time % (1000 * 60) / 1000)
        // console.log(days+"天"+hours+"小時"+minutes+"分鐘"+seconds+"秒")
        if (hours < 10) {
            hours = '0' + hours;
        } else {
            hours = hours.toString();
        }
        if (minutes < 10) {
            minutes = '0' + minutes;
        } else {
            minutes = minutes.toString();
        }
        if (seconds < 10) {
            seconds = '0' + seconds;
        } else {
            seconds = seconds.toString();
        }
        return { days: days, hours: hours, minutes: minutes, seconds: seconds };
    } else {
        console.log("Time's up")
        return { hours: '00', minutes: '00', seconds: '00' }
    }
}


//Modal
function close_modal(){
    var modal = document.getElementsByClassName("modal")[0];
    modal.style.display = "none";
    return null
}
function open_modal(){
    var modal = document.getElementsByClassName("modal")[0];
    modal.style.display = "block";
    return null
}

//任務的格式
function Mission(){
    let mission_product={
        title: "私募席 - 超好吃蜂蜜蛋糕綿到你心坎",
        img: "https://www.17life.com/media/JL-RA-2094/32950894EDM3a96EDM4b22EDMa25cEDM6e4bc6e57f0e.jpg"
    }
    let mission_content={
        price: 224,
        period: 12,
        people: 5,
        deadline: "8 Dec 2018 11:13:00",
        classList: {
            "mission-tag": true,
            easy: true,
            medium: false,
            hard: false
        }
    }
    let mission_characters= [
        {
            nick_name: "齊齊",
            quote: "Here I am, baby!",
            photo: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxASEBUSEBAPDxAQDxUPDxAQDw8QEBAQFRUWFhURFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OFQ8QFS0dFR0tLSsrKy0tLS0rLS0tLS0rLS0rKy0tKy0rLSstLS03LSstKy0tKy0rKy0rLSsrKysrLf/AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAwQCBQYBB//EAD4QAAICAQEFBQUFBgQHAAAAAAABAgMRBAUSITFBBiJRYXEygZGhsRMjYsHRJEJScnPhBxSCkhUzNFNjovD/xAAYAQEBAQEBAAAAAAAAAAAAAAAAAQIDBP/EAB8RAQEBAQACAgMBAAAAAAAAAAABEQIhMRJRQWGBA//aAAwDAQACEQMRAD8A+4gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaPbnaSrTvcX3l2PYXKPnJ9PTmBu3IpX7XohwdkW/CPefyOLu2jde/vJvHSEcqK93X3k1NKRnVx0ku0Ff7sJy+CMP8Aj3hV8Z/2NRCJPCsauNnHbfjX8J/2LFe163zUo+7KNTGszVQ1Mb6rUQl7Mk/qSnOqrquHoXNPrZx4S7y8eqLpjbAwrsUllPJmVAAAAAAAAAAAAAAAAAAAAAAAK+v1Maq5WS9mEXJ+eOnqBoO2naP/AC0Ps6mvt7Fw/wDHH+J+fgcDoW28yblJvMpPi2+rZFtPUTutlbP2pycn4JdIryS4FjZ0TjetrpI3mlRsKolLTGxpRdTFiqBahAgrZZgzURnGBmoHsSVRKiLcMXAn3TxoqsKLHB5XLqvE2sZZWVyZqmi1orP3fehEq4ACoAAAAAAAAAAAAAAAAAAAcv281OKYVJ/8ye8/5YYf1cTqDh+3E83wj/DVn/dJ/ojPV8LPbk7KyTR8GeWmejjxOMdW600jZ0GpgsGx0thplfiiWLMankm3TcZSVyLdbKMC7SaiMmRskZg0BHI9rlhhmCfEittF5PSLTy4ehKaZAAAAAAAAAAAAAAAAAAAOA7ZS/a35VQX1f5nfnz3ti/2yX9OH0Md+mufbQXMt6CspvjJG206S6nONrqpyjGuTiyzRbHxR5bCLKi9prS19oa2gsOXA1KmLdcy9VPgaWuZfpuLKljYYMJGEdQupkrYvqaRhIwJJIwwBd0T4FkqaHqWyoAAAAAAAAAAAAAAAAAHgEctRBS3XOKk+UXJKT9EcF22X7X61QfzkvyOW2nb/AJm62Vi7++3vdVh4S9y4Hum32s2TnOSyt6cpTeFy4vocb3vh0nP5S3Tw8okrkprDlj3lO+b5YbKUK+93nJ+MK3x9HLoZ1p5qb5V2OLm/JplrT7RtXGNmfJvJPPYatg8aemPDg3fa7Pilg5XUaWzT2buXF9Iz4J+kuT+RPK+Hc6HtFJPFix59DpKdWpxyuOT5vs252Lvc1waO37JV5Tj0i1j0Zrms2NjK1x4lW3a7j0Og1OiWOHE0u0tj9Wt7yXLyyW7EmNVft+TfPC8Wy9o9qx/7ib9TLSwu5Qtpp8lSrMerbX0J7dDbJfe16PVLq4qWmux+GUevrJEmrcbHTa/PU2FFyfDxOa0mzku9VK6CziVWorcZRf4ZYSkvj6m50lclxZuWs2Nvo7Us5eCVauOeT4vGXgoxklny8DTbQr1M7YS38Upp/ZxTTUk2+8+qxj+5q3EkdeDGHL3GRpkAAAAAAAAAAAAADxnoA+NbU06p1epT4JWyS9JPeXyaMdNcpLh0Np/iXpnHUOSXC1Rm/VR3M/8AqjQ7HacZeOVn8jz2ZXaelyNblyMoaayOd2qE8vLzBPL82sP5k2ifeOh00VgsiNTs/UTTw9LFea32vhJstbX2fC6pqdNfGLSbhHK95uYQRX2halF+hR880WmcJY8OHuR2vZBcZ+q+hzVeHNvxZ0/Zj25L+V/UnPsrsImGq07ae5Jxyet8SSMjqw0N1d6fFRsX4oxl9UWdLO3+CMPNQimbSdeTyMCfE1hCjrLi/PieyiSuXAhnI0KOr132c13HNc3h4xw5+ZbqujYk0sccYfPLPPs1JPPl9CXQ0JSSXjve5f3M+dG0R6AbZAAAAAAAAAAAAAAAAcn/AIgaLerhZj2JOEv5Zcs+9fM4TT0KGccnj8z69tHSK2qdb5Ti1nwfR+54Z8n1kXXOVdndnCW60/H9Dl3POunN/DCMsSyb7RX8DnrCXSanBmK6tag1O2tTiD+BFXq8mp27qstRGmINJxeTreyscym/RfDP6nKaKPd9Tr+zN8FBJPj+96jn2V1D5mUGRq2OODMkzrrCxvEU5nmSKZUeOZhOZhJkcHmSXmRV2D4cOfL5FnZlWE5N5bfM09Gqtc3B0XRbfdbg9x+e9yS9To6YYil4ITylZgA0gAAAAAAAAAAAAAAAAVtZpYSjLMIuThKO9hb3FY58yyeMD4xy4MwcS5tKncusj/BbOPwk8P4ECR53Z5G7CNPqLHKWTYakqfZgeaic/svu5bskNlai5NfaWOE5YSdaScn1bWGs+iRb0ujcmd12V2bTDMsRnY1zeMpeQk01zk9BtXU1xel1UKoRn95v4jbNJ8oyUWksdMJ564O02Jpbq68XNOWeGG5YXqZ6ClQss3ViM8Sx0yupeTOkjFrCRhMkmyGbNIrXyGzY5mn5mNpZ2ZHvog3uD0A2yAAAAAAAAAAAAAAAAAAAAAPnnbnQ7mo+0S7t0c/648Gvhh/E5xM+pdo9l/5iiUF7a79bfSa6ej4r3nyySabTTTTw0+aa5pnDuZXTmotQiKLXN9Ce3iiho9JCEpS4tzeW3x+plpJZtKWMQT9Um38iHTbVsjJONs65p5W8rK+Pq0b7Zm1I1PvRW74pcjrNFtumS7tsPTfSfwNSJWh0O0NbYk/srbPxJNxfnlLibrT7U1EV99p7kv4lCUsfBF6G0oN+1H/ciSe1ocoyTf4Xk3n7Z/iutsVSxiceLwk2k8vpgsOZFbKM+M4QlLo3FNr3/AjlMIkbNjseGZN+C+pqt7B0OzKN2teMu8/0LCrYANsgAAAAAAAAAAAAAAAAAAAAAz5P2k/6u7+rI+rs+U9onnV3f1ZL4cDn/p6b49tayvOBYPXHJydGvlp7ZvFayyaHZXXS5Kn/AFN/obPZE92w7LTWrBqc6zbXJbO7D6jGbbKV5Qry/ize7O2G6eGc+46XTzWDC15N/CM/KtW8o8x1LNqSIIV73HoQKouTz0+p1Wm9iPojnoI3+k9hehvlKmABpkAAAAAAAAAAAAAAAAAAAA8bAi1mojXXKcuEYRcn7j5FdY5zlOXtTk5v1k8s6Ttb2gVzdNMs1RffmuVkk+S8Yp9er9DmGcu66czGEjyMzKbK9jObaSy7cakbWvaTccp9DQWXrGJLK+DK9eq3ViM+HhKMuHvjnJZUx22ydqyzhs6COqyj5vs622T7sov0jZ+aR2Ohplhb8svwXBGp0ljYt7z8ixAgrJ4GoyzNzoJ93HVfQ0xapta4roaiVuQV6tVF8+D+RYNMgAAAAAAAAAAAAAAAAI7rowi5TlGEYrMpSaUUvFtnC9oO3yWYaNbz5O+a7q84R6+r4eTJbiyOu2vtmjTR3rpqOfZiuM5vwjHqfO9v9rbtVmEE6aHwcc9+xfja6eS9+Tm7752Tc7JysnL2pSeW/wD7w5GdRy67tbnOLtPIyI4MzyYaYzKtrLM2VrAKlqMtLot5limjLN3odKkMVZ2Xo1BeZuqilTEu1m4zVmtk8CvWWIGozUiRNWRomgVKz3TOvUSj5rwf5BIOJRdo1UZeT8HzJzTygS06uUefeXzLqY2YIqb4y5P3dSUqAAAAAAAYzlhN+CyBkcr2g7bafT5hXjUXLK3YNbkWukp+PkuPocV2h7ZajU5hFuinityEu9Jfin+S4epzWTn139Nzn7bXbO3dRqpZuszFPMa492uHour83lmuTI94ZOdbxMmT1sqKRNXIgvQkTJlSEieEgPbCKMMkky1oq8gR014NlRM8npzGtYKNlTMu1s1tTL9LNIuVlqsq1Fqs1EqaJNAiiTVo1GU0Ees9SMWUYtGDiSGIETj7vMnq1k17XeXzI2YtEG3rllJ+KyZEen9leiJDTIAABhf7Mv5X9DwAfAJc/eYHoPO7MT1ABWUSesAgsRJ6wAjKZe2eegFbTp7itLmAaSLFRepPAWC7UXKwDUSp4lmk8BplMYs9BR4YMADE8AINnR7K9ESAGmQAAf/Z"
        },
        {
            nick_name: "帝帝",
            quote: "Here we are, babe!",
            photo: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxASEBUSEBAPDxAQDxUPDxAQDw8QEBAQFRUWFhURFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OFQ8QFS0dFR0tLSsrKy0tLS0rLS0tLS0rLS0rKy0tKy0rLSstLS03LSstKy0tKy0rKy0rLSsrKysrLf/AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAwQCBQYBB//EAD4QAAICAQEFBQUFBgQHAAAAAAABAgMRBAUSITFBBiJRYXEygZGhsRMjYsHRJEJScnPhBxSCkhUzNFNjovD/xAAYAQEBAQEBAAAAAAAAAAAAAAAAAQIDBP/EAB8RAQEBAQACAgMBAAAAAAAAAAABEQIhMRJRQWGBA//aAAwDAQACEQMRAD8A+4gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaPbnaSrTvcX3l2PYXKPnJ9PTmBu3IpX7XohwdkW/CPefyOLu2jde/vJvHSEcqK93X3k1NKRnVx0ku0Ff7sJy+CMP8Aj3hV8Z/2NRCJPCsauNnHbfjX8J/2LFe163zUo+7KNTGszVQ1Mb6rUQl7Mk/qSnOqrquHoXNPrZx4S7y8eqLpjbAwrsUllPJmVAAAAAAAAAAAAAAAAAAAAAAAK+v1Maq5WS9mEXJ+eOnqBoO2naP/AC0Ps6mvt7Fw/wDHH+J+fgcDoW28yblJvMpPi2+rZFtPUTutlbP2pycn4JdIryS4FjZ0TjetrpI3mlRsKolLTGxpRdTFiqBahAgrZZgzURnGBmoHsSVRKiLcMXAn3TxoqsKLHB5XLqvE2sZZWVyZqmi1orP3fehEq4ACoAAAAAAAAAAAAAAAAAAAcv281OKYVJ/8ye8/5YYf1cTqDh+3E83wj/DVn/dJ/ojPV8LPbk7KyTR8GeWmejjxOMdW600jZ0GpgsGx0thplfiiWLMankm3TcZSVyLdbKMC7SaiMmRskZg0BHI9rlhhmCfEittF5PSLTy4ehKaZAAAAAAAAAAAAAAAAAAAOA7ZS/a35VQX1f5nfnz3ti/2yX9OH0Md+mufbQXMt6CspvjJG206S6nONrqpyjGuTiyzRbHxR5bCLKi9prS19oa2gsOXA1KmLdcy9VPgaWuZfpuLKljYYMJGEdQupkrYvqaRhIwJJIwwBd0T4FkqaHqWyoAAAAAAAAAAAAAAAAAHgEctRBS3XOKk+UXJKT9EcF22X7X61QfzkvyOW2nb/AJm62Vi7++3vdVh4S9y4Hum32s2TnOSyt6cpTeFy4vocb3vh0nP5S3Tw8okrkprDlj3lO+b5YbKUK+93nJ+MK3x9HLoZ1p5qb5V2OLm/JplrT7RtXGNmfJvJPPYatg8aemPDg3fa7Pilg5XUaWzT2buXF9Iz4J+kuT+RPK+Hc6HtFJPFix59DpKdWpxyuOT5vs252Lvc1waO37JV5Tj0i1j0Zrms2NjK1x4lW3a7j0Og1OiWOHE0u0tj9Wt7yXLyyW7EmNVft+TfPC8Wy9o9qx/7ib9TLSwu5Qtpp8lSrMerbX0J7dDbJfe16PVLq4qWmux+GUevrJEmrcbHTa/PU2FFyfDxOa0mzku9VK6CziVWorcZRf4ZYSkvj6m50lclxZuWs2Nvo7Us5eCVauOeT4vGXgoxklny8DTbQr1M7YS38Upp/ZxTTUk2+8+qxj+5q3EkdeDGHL3GRpkAAAAAAAAAAAAADxnoA+NbU06p1epT4JWyS9JPeXyaMdNcpLh0Np/iXpnHUOSXC1Rm/VR3M/8AqjQ7HacZeOVn8jz2ZXaelyNblyMoaayOd2qE8vLzBPL82sP5k2ifeOh00VgsiNTs/UTTw9LFea32vhJstbX2fC6pqdNfGLSbhHK95uYQRX2halF+hR880WmcJY8OHuR2vZBcZ+q+hzVeHNvxZ0/Zj25L+V/UnPsrsImGq07ae5Jxyet8SSMjqw0N1d6fFRsX4oxl9UWdLO3+CMPNQimbSdeTyMCfE1hCjrLi/PieyiSuXAhnI0KOr132c13HNc3h4xw5+ZbqujYk0sccYfPLPPs1JPPl9CXQ0JSSXjve5f3M+dG0R6AbZAAAAAAAAAAAAAAAAcn/AIgaLerhZj2JOEv5Zcs+9fM4TT0KGccnj8z69tHSK2qdb5Ti1nwfR+54Z8n1kXXOVdndnCW60/H9Dl3POunN/DCMsSyb7RX8DnrCXSanBmK6tag1O2tTiD+BFXq8mp27qstRGmINJxeTreyscym/RfDP6nKaKPd9Tr+zN8FBJPj+96jn2V1D5mUGRq2OODMkzrrCxvEU5nmSKZUeOZhOZhJkcHmSXmRV2D4cOfL5FnZlWE5N5bfM09Gqtc3B0XRbfdbg9x+e9yS9To6YYil4ITylZgA0gAAAAAAAAAAAAAAAAVtZpYSjLMIuThKO9hb3FY58yyeMD4xy4MwcS5tKncusj/BbOPwk8P4ECR53Z5G7CNPqLHKWTYakqfZgeaic/svu5bskNlai5NfaWOE5YSdaScn1bWGs+iRb0ujcmd12V2bTDMsRnY1zeMpeQk01zk9BtXU1xel1UKoRn95v4jbNJ8oyUWksdMJ564O02Jpbq68XNOWeGG5YXqZ6ClQss3ViM8Sx0yupeTOkjFrCRhMkmyGbNIrXyGzY5mn5mNpZ2ZHvog3uD0A2yAAAAAAAAAAAAAAAAAAAAAPnnbnQ7mo+0S7t0c/648Gvhh/E5xM+pdo9l/5iiUF7a79bfSa6ej4r3nyySabTTTTw0+aa5pnDuZXTmotQiKLXN9Ce3iiho9JCEpS4tzeW3x+plpJZtKWMQT9Um38iHTbVsjJONs65p5W8rK+Pq0b7Zm1I1PvRW74pcjrNFtumS7tsPTfSfwNSJWh0O0NbYk/srbPxJNxfnlLibrT7U1EV99p7kv4lCUsfBF6G0oN+1H/ciSe1ocoyTf4Xk3n7Z/iutsVSxiceLwk2k8vpgsOZFbKM+M4QlLo3FNr3/AjlMIkbNjseGZN+C+pqt7B0OzKN2teMu8/0LCrYANsgAAAAAAAAAAAAAAAAAAAAAz5P2k/6u7+rI+rs+U9onnV3f1ZL4cDn/p6b49tayvOBYPXHJydGvlp7ZvFayyaHZXXS5Kn/AFN/obPZE92w7LTWrBqc6zbXJbO7D6jGbbKV5Qry/ize7O2G6eGc+46XTzWDC15N/CM/KtW8o8x1LNqSIIV73HoQKouTz0+p1Wm9iPojnoI3+k9hehvlKmABpkAAAAAAAAAAAAAAAAAAAA8bAi1mojXXKcuEYRcn7j5FdY5zlOXtTk5v1k8s6Ttb2gVzdNMs1RffmuVkk+S8Yp9er9DmGcu66czGEjyMzKbK9jObaSy7cakbWvaTccp9DQWXrGJLK+DK9eq3ViM+HhKMuHvjnJZUx22ydqyzhs6COqyj5vs622T7sov0jZ+aR2Ohplhb8svwXBGp0ljYt7z8ixAgrJ4GoyzNzoJ93HVfQ0xapta4roaiVuQV6tVF8+D+RYNMgAAAAAAAAAAAAAAAAI7rowi5TlGEYrMpSaUUvFtnC9oO3yWYaNbz5O+a7q84R6+r4eTJbiyOu2vtmjTR3rpqOfZiuM5vwjHqfO9v9rbtVmEE6aHwcc9+xfja6eS9+Tm7752Tc7JysnL2pSeW/wD7w5GdRy67tbnOLtPIyI4MzyYaYzKtrLM2VrAKlqMtLot5limjLN3odKkMVZ2Xo1BeZuqilTEu1m4zVmtk8CvWWIGozUiRNWRomgVKz3TOvUSj5rwf5BIOJRdo1UZeT8HzJzTygS06uUefeXzLqY2YIqb4y5P3dSUqAAAAAAAYzlhN+CyBkcr2g7bafT5hXjUXLK3YNbkWukp+PkuPocV2h7ZajU5hFuinityEu9Jfin+S4epzWTn139Nzn7bXbO3dRqpZuszFPMa492uHour83lmuTI94ZOdbxMmT1sqKRNXIgvQkTJlSEieEgPbCKMMkky1oq8gR014NlRM8npzGtYKNlTMu1s1tTL9LNIuVlqsq1Fqs1EqaJNAiiTVo1GU0Ees9SMWUYtGDiSGIETj7vMnq1k17XeXzI2YtEG3rllJ+KyZEen9leiJDTIAABhf7Mv5X9DwAfAJc/eYHoPO7MT1ABWUSesAgsRJ6wAjKZe2eegFbTp7itLmAaSLFRepPAWC7UXKwDUSp4lmk8BplMYs9BR4YMADE8AINnR7K9ESAGmQAAf/Z"
        },
        {
            nick_name: "關關",
            quote: "You are running away from me!",
            photo: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxASEBUSEBAPDxAQDxUPDxAQDw8QEBAQFRUWFhURFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OFQ8QFS0dFR0tLSsrKy0tLS0rLS0tLS0rLS0rKy0tKy0rLSstLS03LSstKy0tKy0rKy0rLSsrKysrLf/AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAwQCBQYBB//EAD4QAAICAQEFBQUFBgQHAAAAAAABAgMRBAUSITFBBiJRYXEygZGhsRMjYsHRJEJScnPhBxSCkhUzNFNjovD/xAAYAQEBAQEBAAAAAAAAAAAAAAAAAQIDBP/EAB8RAQEBAQACAgMBAAAAAAAAAAABEQIhMRJRQWGBA//aAAwDAQACEQMRAD8A+4gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaPbnaSrTvcX3l2PYXKPnJ9PTmBu3IpX7XohwdkW/CPefyOLu2jde/vJvHSEcqK93X3k1NKRnVx0ku0Ff7sJy+CMP8Aj3hV8Z/2NRCJPCsauNnHbfjX8J/2LFe163zUo+7KNTGszVQ1Mb6rUQl7Mk/qSnOqrquHoXNPrZx4S7y8eqLpjbAwrsUllPJmVAAAAAAAAAAAAAAAAAAAAAAAK+v1Maq5WS9mEXJ+eOnqBoO2naP/AC0Ps6mvt7Fw/wDHH+J+fgcDoW28yblJvMpPi2+rZFtPUTutlbP2pycn4JdIryS4FjZ0TjetrpI3mlRsKolLTGxpRdTFiqBahAgrZZgzURnGBmoHsSVRKiLcMXAn3TxoqsKLHB5XLqvE2sZZWVyZqmi1orP3fehEq4ACoAAAAAAAAAAAAAAAAAAAcv281OKYVJ/8ye8/5YYf1cTqDh+3E83wj/DVn/dJ/ojPV8LPbk7KyTR8GeWmejjxOMdW600jZ0GpgsGx0thplfiiWLMankm3TcZSVyLdbKMC7SaiMmRskZg0BHI9rlhhmCfEittF5PSLTy4ehKaZAAAAAAAAAAAAAAAAAAAOA7ZS/a35VQX1f5nfnz3ti/2yX9OH0Md+mufbQXMt6CspvjJG206S6nONrqpyjGuTiyzRbHxR5bCLKi9prS19oa2gsOXA1KmLdcy9VPgaWuZfpuLKljYYMJGEdQupkrYvqaRhIwJJIwwBd0T4FkqaHqWyoAAAAAAAAAAAAAAAAAHgEctRBS3XOKk+UXJKT9EcF22X7X61QfzkvyOW2nb/AJm62Vi7++3vdVh4S9y4Hum32s2TnOSyt6cpTeFy4vocb3vh0nP5S3Tw8okrkprDlj3lO+b5YbKUK+93nJ+MK3x9HLoZ1p5qb5V2OLm/JplrT7RtXGNmfJvJPPYatg8aemPDg3fa7Pilg5XUaWzT2buXF9Iz4J+kuT+RPK+Hc6HtFJPFix59DpKdWpxyuOT5vs252Lvc1waO37JV5Tj0i1j0Zrms2NjK1x4lW3a7j0Og1OiWOHE0u0tj9Wt7yXLyyW7EmNVft+TfPC8Wy9o9qx/7ib9TLSwu5Qtpp8lSrMerbX0J7dDbJfe16PVLq4qWmux+GUevrJEmrcbHTa/PU2FFyfDxOa0mzku9VK6CziVWorcZRf4ZYSkvj6m50lclxZuWs2Nvo7Us5eCVauOeT4vGXgoxklny8DTbQr1M7YS38Upp/ZxTTUk2+8+qxj+5q3EkdeDGHL3GRpkAAAAAAAAAAAAADxnoA+NbU06p1epT4JWyS9JPeXyaMdNcpLh0Np/iXpnHUOSXC1Rm/VR3M/8AqjQ7HacZeOVn8jz2ZXaelyNblyMoaayOd2qE8vLzBPL82sP5k2ifeOh00VgsiNTs/UTTw9LFea32vhJstbX2fC6pqdNfGLSbhHK95uYQRX2halF+hR880WmcJY8OHuR2vZBcZ+q+hzVeHNvxZ0/Zj25L+V/UnPsrsImGq07ae5Jxyet8SSMjqw0N1d6fFRsX4oxl9UWdLO3+CMPNQimbSdeTyMCfE1hCjrLi/PieyiSuXAhnI0KOr132c13HNc3h4xw5+ZbqujYk0sccYfPLPPs1JPPl9CXQ0JSSXjve5f3M+dG0R6AbZAAAAAAAAAAAAAAAAcn/AIgaLerhZj2JOEv5Zcs+9fM4TT0KGccnj8z69tHSK2qdb5Ti1nwfR+54Z8n1kXXOVdndnCW60/H9Dl3POunN/DCMsSyb7RX8DnrCXSanBmK6tag1O2tTiD+BFXq8mp27qstRGmINJxeTreyscym/RfDP6nKaKPd9Tr+zN8FBJPj+96jn2V1D5mUGRq2OODMkzrrCxvEU5nmSKZUeOZhOZhJkcHmSXmRV2D4cOfL5FnZlWE5N5bfM09Gqtc3B0XRbfdbg9x+e9yS9To6YYil4ITylZgA0gAAAAAAAAAAAAAAAAVtZpYSjLMIuThKO9hb3FY58yyeMD4xy4MwcS5tKncusj/BbOPwk8P4ECR53Z5G7CNPqLHKWTYakqfZgeaic/svu5bskNlai5NfaWOE5YSdaScn1bWGs+iRb0ujcmd12V2bTDMsRnY1zeMpeQk01zk9BtXU1xel1UKoRn95v4jbNJ8oyUWksdMJ564O02Jpbq68XNOWeGG5YXqZ6ClQss3ViM8Sx0yupeTOkjFrCRhMkmyGbNIrXyGzY5mn5mNpZ2ZHvog3uD0A2yAAAAAAAAAAAAAAAAAAAAAPnnbnQ7mo+0S7t0c/648Gvhh/E5xM+pdo9l/5iiUF7a79bfSa6ej4r3nyySabTTTTw0+aa5pnDuZXTmotQiKLXN9Ce3iiho9JCEpS4tzeW3x+plpJZtKWMQT9Um38iHTbVsjJONs65p5W8rK+Pq0b7Zm1I1PvRW74pcjrNFtumS7tsPTfSfwNSJWh0O0NbYk/srbPxJNxfnlLibrT7U1EV99p7kv4lCUsfBF6G0oN+1H/ciSe1ocoyTf4Xk3n7Z/iutsVSxiceLwk2k8vpgsOZFbKM+M4QlLo3FNr3/AjlMIkbNjseGZN+C+pqt7B0OzKN2teMu8/0LCrYANsgAAAAAAAAAAAAAAAAAAAAAz5P2k/6u7+rI+rs+U9onnV3f1ZL4cDn/p6b49tayvOBYPXHJydGvlp7ZvFayyaHZXXS5Kn/AFN/obPZE92w7LTWrBqc6zbXJbO7D6jGbbKV5Qry/ize7O2G6eGc+46XTzWDC15N/CM/KtW8o8x1LNqSIIV73HoQKouTz0+p1Wm9iPojnoI3+k9hehvlKmABpkAAAAAAAAAAAAAAAAAAAA8bAi1mojXXKcuEYRcn7j5FdY5zlOXtTk5v1k8s6Ttb2gVzdNMs1RffmuVkk+S8Yp9er9DmGcu66czGEjyMzKbK9jObaSy7cakbWvaTccp9DQWXrGJLK+DK9eq3ViM+HhKMuHvjnJZUx22ydqyzhs6COqyj5vs622T7sov0jZ+aR2Ohplhb8svwXBGp0ljYt7z8ixAgrJ4GoyzNzoJ93HVfQ0xapta4roaiVuQV6tVF8+D+RYNMgAAAAAAAAAAAAAAAAI7rowi5TlGEYrMpSaUUvFtnC9oO3yWYaNbz5O+a7q84R6+r4eTJbiyOu2vtmjTR3rpqOfZiuM5vwjHqfO9v9rbtVmEE6aHwcc9+xfja6eS9+Tm7752Tc7JysnL2pSeW/wD7w5GdRy67tbnOLtPIyI4MzyYaYzKtrLM2VrAKlqMtLot5limjLN3odKkMVZ2Xo1BeZuqilTEu1m4zVmtk8CvWWIGozUiRNWRomgVKz3TOvUSj5rwf5BIOJRdo1UZeT8HzJzTygS06uUefeXzLqY2YIqb4y5P3dSUqAAAAAAAYzlhN+CyBkcr2g7bafT5hXjUXLK3YNbkWukp+PkuPocV2h7ZajU5hFuinityEu9Jfin+S4epzWTn139Nzn7bXbO3dRqpZuszFPMa492uHour83lmuTI94ZOdbxMmT1sqKRNXIgvQkTJlSEieEgPbCKMMkky1oq8gR014NlRM8npzGtYKNlTMu1s1tTL9LNIuVlqsq1Fqs1EqaJNAiiTVo1GU0Ees9SMWUYtGDiSGIETj7vMnq1k17XeXzI2YtEG3rllJ+KyZEen9leiJDTIAABhf7Mv5X9DwAfAJc/eYHoPO7MT1ABWUSesAgsRJ6wAjKZe2eegFbTp7itLmAaSLFRepPAWC7UXKwDUSp4lmk8BplMYs9BR4YMADE8AINnR7K9ESAGmQAAf/Z"
        },
    ]
}
//捲上去
function scrollToTop(){
    window.scrollTo({
        top: 0,
        behavior: "smooth"
    });
}

// 回上頁
function goBack() {
    window.history.back();
}

$(document).ready(function () {
    $(window).scroll(function () {
        var offsetTop = window.pageYOffset;
        if (offsetTop <= 100) {
            $('#navbar').css('position', 'absolute');
        } else {
            $('#navbar').css('position', 'fixed');
            //console.log(offsetTop)
        }
    });
});


// RWD 判斷(沒用)
// function rwdEvent(){
//     var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

//     var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;


//     if(w>800){
//         let r = document.getElementsByClassName('recommand-list')[0];
//         let large_button = document.getElementById('see-more');
//         if(r.classList.contains("recommand-list-mb")){
//             r.classList.remove("recommand-list-mb");
//         }        
//         r.classList.add("recommand-list-web");
//         large_button.classList.add("cta-wrapper-large"); 
//         return "web"
//     }else if(w<=800){
//         let r = document.getElementsByClassName('recommand-list')[0];
//         let large_button = document.getElementById('see-more');
//         let web_container = document.getElementById('content-container');
//         if(r.classList.contains("recommand-list-web")){
//             r.classList.remove("recommand-list-web")
//         }
//         r.classList.add("recommand-list-mb")
//         web_container.classList.filters()    
//         return "mobile"
//     }
// }


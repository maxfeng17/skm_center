﻿
CKEDITOR.editorConfig = function (config) {
    config.language = 'zh-tw';
    config.allowedContent = true;
    config.fontSize_sizes = '10pt/10pt;13/13px;16/16px;18/18px;20/20px;22/22px;24/24px;36/36px;48/48px;';
    config.filebrowserBrowseUrl = '../../Tools/ckeditor/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl = '../../Tools/ckeditor/ckfinder/ckfinder.html?type=Images';
    config.enterMode = CKEDITOR.ENTER_BR;
    config.toolbar = [
        ['Source', 'Preview', '-',
            'Undo', 'Redo', '-',
            'Cut', 'Copy', 'Paste', 'PasteFromWord', '-',
            'Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', '-',
            'Maximize', '-', 'Link', 'Unlink'], '/',
        ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'RemoveFormat', '-',
            'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-',
            'NumberedList', 'BulletedList', 'Outdent', 'Indent', 'BidiLtr', 'BidiRtl', '-',
            'TextColor', 'BGColor', 'Format', 'Font', 'FontSize', '-', 'Youtube'
        ]
    ];
};

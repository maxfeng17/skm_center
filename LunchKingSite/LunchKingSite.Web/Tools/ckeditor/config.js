﻿/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/


CKEDITOR.editorConfig = function(config) {
    // Define changes to default configuration here. For example:

    config.htmlEncodeOutput = true;
    config.enterMode = CKEDITOR.ENTER_BR;
    config.font_names = 'Arial;Arial Black;Comic Sans MS;Courier New;Tahoma;Times New Roman;Verdana;新細明體;細明體;標楷體;微軟正黑體';
    config.extraPlugins = 'uploadimage,youtube';

    config.toolbarGroups = [
		{ name: 'document', groups: ['mode', 'document', 'doctools'] },
		{ name: 'clipboard', groups: ['clipboard', 'undo'] },
		{ name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing'] },
		//{ name: 'forms', groups: ['forms'] }, 不顯示的按鈕
		'/',
		{ name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
		{ name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
		{ name: 'links', groups: ['links'] },
		{ name: 'insert', groups: ['insert'] },
		'/',
		{ name: 'styles', groups: ['styles'] },
		{ name: 'colors', groups: ['colors'] },
		{ name: 'tools', groups: ['tools'] },
		{ name: 'others', groups: ['others'] },
		{ name: 'about', groups: ['about'] }
    ];
    
    config.uploadUrl = '/Tools/ckeditor/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files&responseType=json';
    //config.filebrowserBrowseUrl = '/Tools/ckeditor/ckfinder/ckfinder.html';
    config.filebrowserUploadUrl = '/Tools/ckeditor/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files';
    //config.filebrowserImageBrowseUrl = '/Tools/ckeditor/ckfinder/ckfinder.html?type=Images';
    config.filebrowserImageUploadUrl = '/Tools/ckeditor/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images';
    
    //Simplify the dialog windows.
    config.removeDialogTabs = 'image:advanced;link:advanced';

    //PasteFromWord adjust 
    config.pasteFromWordIgnoreFontFace = true;
    config.pasteFromWordKeepsStructure = false;
    config.pasteFromWordRemoveFontStyles = false;
    config.pasteFromWordRemoveStyle = false;

    config.allowedContent = true;
};

//防止以下html tag內如果沒有字元，會被ckeditor移除的bug
CKEDITOR.config.protectedSource.push(/<div[^>]*><\/div>/g);
CKEDITOR.config.protectedSource.push(/<span[^>]*><\/span>/g);
CKEDITOR.config.protectedSource.push(/<p[^>]*><\/p>/g);
CKEDITOR.config.protectedSource.push(/<i[^>]*><\/i>/g);
CKEDITOR.config.protectedSource.push(/<a[^>]*><\/a>/g);

$.each(CKEDITOR.dtd.$removeEmpty, function (i, value) {
    CKEDITOR.dtd.$removeEmpty[i] = false;
});
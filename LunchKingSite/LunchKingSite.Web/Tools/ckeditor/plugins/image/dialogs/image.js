﻿/*
    Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
    For licensing, see LICENSE.md or http://ckeditor.com/license
*/
(function () {
    var w = function (d, k) {
        function w() {
            var a = arguments,
                b = this.getContentElement("advanced", "txtdlgGenStyle");
            b && b.commit.apply(b, a);
            this.foreach(function (b) {
                b.commit && "txtdlgGenStyle" != b.id && b.commit.apply(b, a)
            })
        }

        function l(a) {
            if (!x) {
                x = 1;
                var b = this.getDialog(),
                    c = b.imageElement;
                if (c) {
                    this.commit(1, c);
                    a = [].concat(a);
                    for (var d = a.length, e, f = 0; f < d; f++) (e = b.getContentElement.apply(b, a[f].split(":"))) && e.setup(1, c)
                }
                x = 0
            }
        }
        var r = /^\s*(\d+)((px)|\%)?\s*$/i,
            A = /(^\s*(\d+)((px)|\%)?\s*$)|^$/i,
            t = /^\d+px$/,
            B = function () {
                var a = this.getValue(),
                    b = this.getDialog(),
                    c = a.match(r);
                c && ("%" == c[2] && m(b, !1), a = c[1]);
                b.lockRatio && (c = b.originalElement, "true" == c.getCustomData("isReady") && ("txtHeight" == this.id ? (a && "0" != a && (a = Math.round(a / c.$.height * c.$.width)), isNaN(a) || b.setValueOf("info", "txtWidth", a)) : (a && "0" != a && (a = Math.round(a / c.$.width * c.$.height)), isNaN(a) || b.setValueOf("info", "txtHeight", a))));
                g(b)
            },
            g = function (a) {
                if (!a.originalElement || !a.preview) return 1;
                a.commitContent(4, a.preview);
                return 0
            },
            x, m = function (a,
                b) {
                if (!a.getContentElement("info", "ratioLock")) return null;
                var c = a.originalElement;
                if (!c) return null;
                if ("check" == b) {
                    if (!a.userlockRatio && "true" == c.getCustomData("isReady")) {
                        var d = a.getValueOf("info", "txtWidth"),
                            e = a.getValueOf("info", "txtHeight"),
                            c = 1E3 * c.$.width / c.$.height,
                            f = 1E3 * d / e;
                        a.lockRatio = !1;
                        d || e ? isNaN(c) || isNaN(f) || Math.round(c) != Math.round(f) || (a.lockRatio = !0) : a.lockRatio = !0
                    }
                } else void 0 !== b ? a.lockRatio = b : (a.userlockRatio = 1, a.lockRatio = !a.lockRatio);
                d = CKEDITOR.document.getById(u);
                a.lockRatio ?
                    d.removeClass("cke_btn_unlocked") : d.addClass("cke_btn_unlocked");
                d.setAttribute("aria-checked", a.lockRatio);
                CKEDITOR.env.hc && d.getChild(0).setHtml(a.lockRatio ? CKEDITOR.env.ie ? "\u25a0" : "\u25a3" : CKEDITOR.env.ie ? "\u25a1" : "\u25a2");
                return a.lockRatio
            },
            C = function (a, b) {
                var c = a.originalElement;
                if ("true" == c.getCustomData("isReady")) {
                    var d = a.getContentElement("info", "txtWidth"),
                        e = a.getContentElement("info", "txtHeight"),
                        f;
                    b ? c = f = 0 : (f = c.$.width, c = c.$.height);
                    d && d.setValue(f);
                    e && e.setValue(c)
                }
                g(a)
            },
            D = function (a,
                b) {
                function c(a, b) {
                    var c = a.match(r);
                    return c ? ("%" == c[2] && (c[1] += "%", m(d, !1)), c[1]) : b
                }
                if (1 == a) {
                    var d = this.getDialog(),
                        e = "",
                        f = "txtWidth" == this.id ? "width" : "height",
                        g = b.getAttribute(f);
                    g && (e = c(g, e));
                    e = c(b.getStyle(f), e);
                    this.setValue(e)
                }
            },
            y, v = function () {
                var a = this.originalElement,
                    b = CKEDITOR.document.getById(n);
                a.setCustomData("isReady", "true");
                a.removeListener("load", v);
                a.removeListener("error", h);
                a.removeListener("abort", h);
                b && b.setStyle("display", "none");
                this.dontResetSize || C(this, !1 === d.config.image_prefillDimensions);
                this.firstLoad && CKEDITOR.tools.setTimeout(function () {
                    m(this, "check")
                }, 0, this);
                this.dontResetSize = this.firstLoad = !1;
                g(this)
            },
            h = function () {
                var a = this.originalElement,
                    b = CKEDITOR.document.getById(n);
                a.removeListener("load", v);
                a.removeListener("error", h);
                a.removeListener("abort", h);
                a = CKEDITOR.getUrl(CKEDITOR.plugins.get("image").path + "images/noimage.png");
                this.preview && this.preview.setAttribute("src", a);
                b && b.setStyle("display", "none");
                m(this, !1)
            },
            p = function (a) {
                return CKEDITOR.tools.getNextId() + "_" + a
            },
            u = p("btnLockSizes"),
            z = p("btnResetSize"),
            n = p("ImagePreviewLoader"),
            F = p("previewLink"),
            E = p("previewImage");
        return {
            title: d.lang.image["image" == k ? "title" : "titleButton"],
            minWidth: "moono-lisa" == (CKEDITOR.skinName || d.config.skin) ? 500 : 420,
            minHeight: 360,
            onShow: function () {
                this.linkEditMode = this.imageEditMode = this.linkElement = this.imageElement = !1;
                this.lockRatio = !0;
                this.userlockRatio = 0;
                this.dontResetSize = !1;
                this.firstLoad = !0;
                this.addLink = !1;
                var a = this.getParentEditor(),
                    b = a.getSelection(),
                    c = (b = b && b.getSelectedElement()) &&
                    a.elementPath(b).contains("a", 1),
                    d = CKEDITOR.document.getById(n);
                d && d.setStyle("display", "none");
                y = new CKEDITOR.dom.element("img", a.document);
                this.preview = CKEDITOR.document.getById(E);
                this.originalElement = a.document.createElement("img");
                this.originalElement.setAttribute("alt", "");
                this.originalElement.setCustomData("isReady", "false");
                c && (this.linkElement = c, this.addLink = this.linkEditMode = !0, a = c.getChildren(), 1 == a.count() && (d = a.getItem(0), d.type == CKEDITOR.NODE_ELEMENT && (d.is("img") || d.is("input")) && (this.imageElement =
                    a.getItem(0), this.imageElement.is("img") ? this.imageEditMode = "img" : this.imageElement.is("input") && (this.imageEditMode = "input"))), "image" == k && this.setupContent(2, c));
                if (this.customImageElement) this.imageEditMode = "img", this.imageElement = this.customImageElement, delete this.customImageElement;
                else if (b && "img" == b.getName() && !b.data("cke-realelement") || b && "input" == b.getName() && "image" == b.getAttribute("type")) this.imageEditMode = b.getName(), this.imageElement = b;
                this.imageEditMode && (this.cleanImageElement = this.imageElement,
                    this.imageElement = this.cleanImageElement.clone(!0, !0), this.setupContent(1, this.imageElement));
                m(this, !0);
                CKEDITOR.tools.trim(this.getValueOf("info", "txtUrl")) || (this.preview.removeAttribute("src"), this.preview.setStyle("display", "none"))
            },
            onOk: function () {
                litImages = $("#frameMulti").contents().find("#litImages");
                if ("" != litImages.html()) {
                    var a = new CKEDITOR.htmlWriter;
                    CKEDITOR.htmlParser.fragment.fromHtml(litImages.html().replace(/width="80"/g, "")).writeHtml(a);
                    CKEDITOR.instances[d.name].insertHtml(a.getHtml());
                    CKEDITOR.dialog.getCurrent().hide()
                } else (this.imageEditMode ? (a = this.imageEditMode, "image" == k && "input" == a && confirm(d.lang.image.button2Img) ? (this.imageElement = d.document.createElement("img"), this.imageElement.setAttribute("alt", ""), d.insertElement(this.imageElement)) : "image" != k && "img" == a && confirm(d.lang.image.img2Button) ? (this.imageElement = d.document.createElement("input"), this.imageElement.setAttributes({
                    type: "image",
                    alt: ""
                }), d.insertElement(this.imageElement)) : (this.imageElement = this.cleanImageElement,
                        delete this.cleanImageElement)) : ("image" == k ? this.imageElement = d.document.createElement("img") : (this.imageElement = d.document.createElement("input"), this.imageElement.setAttribute("type", "image")), this.imageElement.setAttribute("alt", "")), this.linkEditMode || (this.linkElement = d.document.createElement("a")), this.commitContent(1, this.imageElement), this.commitContent(2, this.linkElement), this.imageElement.getAttribute("style") || this.imageElement.removeAttribute("style"), this.imageEditMode) ? !this.linkEditMode &&
                    this.addLink ? (d.insertElement(this.linkElement), this.imageElement.appendTo(this.linkElement)) : this.linkEditMode && !this.addLink && (d.getSelection().selectElement(this.linkElement), d.insertElement(this.imageElement)) : this.addLink ? this.linkEditMode ? this.linkElement.equals(d.getSelection().getSelectedElement()) ? (this.linkElement.setHtml(""), this.linkElement.append(this.imageElement, !1)) : d.insertElement(this.imageElement) : (d.insertElement(this.linkElement), this.linkElement.append(this.imageElement, !1)) :
                    d.insertElement(this.imageElement)
            },
            onLoad: function () {
                "image" != k && this.hidePage("Link");
                var a = this._.element.getDocument();
                this.getContentElement("info", "ratioLock") && (this.addFocusable(a.getById(z), 5), this.addFocusable(a.getById(u), 5));
                this.commitContent = w
            },
            onHide: function () {
                this.preview && this.commitContent(8, this.preview);
                this.originalElement && (this.originalElement.removeListener("load", v), this.originalElement.removeListener("error", h), this.originalElement.removeListener("abort", h), this.originalElement.remove(),
                    this.originalElement = !1);
                delete this.imageElement
            },
            contents: [{
                id: "info",
                label: d.lang.image.infoTab,
                accessKey: "I",
                elements: [{
                    type: "vbox",
                    padding: 0,
                    children: [{
                        type: "hbox",
                        widths: ["280px", "110px"],
                        align: "right",
                        className: "cke_dialog_image_url",
                        children: [{
                            id: "txtUrl",
                            type: "text",
                            label: d.lang.common.url,
                            required: !0,
                            onChange: function () {
                                var a = this.getDialog(),
                                    b = this.getValue();
                                if (0 < b.length) {
                                    var a = this.getDialog(),
                                        c = a.originalElement;
                                    a.preview && a.preview.removeStyle("display");
                                    c.setCustomData("isReady",
                                        "false");
                                    var d = CKEDITOR.document.getById(n);
                                    d && d.setStyle("display", "");
                                    c.on("load", v, a);
                                    c.on("error", h, a);
                                    c.on("abort", h, a);
                                    c.setAttribute("src", b);
                                    a.preview && (y.setAttribute("src", b), a.preview.setAttribute("src", y.$.src), g(a))
                                } else a.preview && (a.preview.removeAttribute("src"), a.preview.setStyle("display", "none"))
                            },
                            setup: function (a, b) {
                                if (1 == a) {
                                    var c = b.data("cke-saved-src") || b.getAttribute("src");
                                    this.getDialog().dontResetSize = !0;
                                    this.setValue(c);
                                    this.setInitValue()
                                }
                            },
                            commit: function (a, b) {
                                1 == a && (this.getValue() ||
                                    this.isChanged()) ? (b.data("cke-saved-src", this.getValue()), b.setAttribute("src", this.getValue())) : 8 == a && (b.setAttribute("src", ""), b.removeAttribute("src"))
                            },
                            validate: CKEDITOR.dialog.validate.notEmpty(d.lang.image.urlMissing)
                        }, {
                            type: "button",
                            id: "browse",
                            style: "display:inline-block;margin-top:14px;",
                            align: "center",
                            label: d.lang.common.browseServer,
                            hidden: !0,
                            filebrowser: "info:txtUrl"
                        }]
                    }]
                }, {
                    id: "txtAlt",
                    type: "text",
                    label: d.lang.image.alt,
                    accessKey: "T",
                    "default": "",
                    onChange: function () {
                        g(this.getDialog())
                    },
                    setup: function (a, b) {
                        1 == a && this.setValue(b.getAttribute("alt"))
                    },
                    commit: function (a, b) {
                        1 == a ? (this.getValue() || this.isChanged()) && b.setAttribute("alt", this.getValue()) : 4 == a ? b.setAttribute("alt", this.getValue()) : 8 == a && b.removeAttribute("alt")
                    }
                }, {
                    type: "hbox",
                    children: [{
                        id: "basic",
                        type: "vbox",
                        children: [{
                            type: "hbox",
                            requiredContent: "img{width,height}",
                            widths: ["50%", "50%"],
                            children: [{
                                type: "vbox",
                                padding: 1,
                                children: [{
                                    type: "text",
                                    width: "45px",
                                    id: "txtWidth",
                                    label: d.lang.common.width,
                                    onKeyUp: B,
                                    onChange: function () {
                                        l.call(this,
                                            "advanced:txtdlgGenStyle")
                                    },
                                    validate: function () {
                                        var a = this.getValue().match(A);
                                        (a = !(!a || 0 === parseInt(a[1], 10))) || alert(d.lang.common.invalidWidth);
                                        return a
                                    },
                                    setup: D,
                                    commit: function (a, b) {
                                        var c = this.getValue();
                                        1 == a ? (c && d.activeFilter.check("img{width,height}") ? b.setStyle("width", CKEDITOR.tools.cssLength(c)) : b.removeStyle("width"), b.removeAttribute("width")) : 4 == a ? c.match(r) ? b.setStyle("width", CKEDITOR.tools.cssLength(c)) : (c = this.getDialog().originalElement, "true" == c.getCustomData("isReady") && b.setStyle("width",
                                            c.$.width + "px")) : 8 == a && (b.removeAttribute("width"), b.removeStyle("width"))
                                    }
                                }, {
                                    type: "text",
                                    id: "txtHeight",
                                    width: "45px",
                                    label: d.lang.common.height,
                                    onKeyUp: B,
                                    onChange: function () {
                                        l.call(this, "advanced:txtdlgGenStyle")
                                    },
                                    validate: function () {
                                        var a = this.getValue().match(A);
                                        (a = !(!a || 0 === parseInt(a[1], 10))) || alert(d.lang.common.invalidHeight);
                                        return a
                                    },
                                    setup: D,
                                    commit: function (a, b) {
                                        var c = this.getValue();
                                        1 == a ? (c && d.activeFilter.check("img{width,height}") ? b.setStyle("height", CKEDITOR.tools.cssLength(c)) : b.removeStyle("height"),
                                            b.removeAttribute("height")) : 4 == a ? c.match(r) ? b.setStyle("height", CKEDITOR.tools.cssLength(c)) : (c = this.getDialog().originalElement, "true" == c.getCustomData("isReady") && b.setStyle("height", c.$.height + "px")) : 8 == a && (b.removeAttribute("height"), b.removeStyle("height"))
                                    }
                                }]
                            }, {
                                id: "ratioLock",
                                type: "html",
                                className: "cke_dialog_image_ratiolock",
                                style: "margin-top:30px;width:40px;height:40px;",
                                onLoad: function () {
                                    var a = CKEDITOR.document.getById(z),
                                        b = CKEDITOR.document.getById(u);
                                    a && (a.on("click", function (a) {
                                        C(this);
                                        a.data && a.data.preventDefault()
                                    }, this.getDialog()), a.on("mouseover", function () {
                                        this.addClass("cke_btn_over")
                                    }, a), a.on("mouseout", function () {
                                        this.removeClass("cke_btn_over")
                                    }, a));
                                    b && (b.on("click", function (a) {
                                        m(this);
                                        var b = this.originalElement,
                                            c = this.getValueOf("info", "txtWidth");
                                        "true" == b.getCustomData("isReady") && c && (b = b.$.height / b.$.width * c, isNaN(b) || (this.setValueOf("info", "txtHeight", Math.round(b)), g(this)));
                                        a.data && a.data.preventDefault()
                                    }, this.getDialog()), b.on("mouseover", function () {
                                        this.addClass("cke_btn_over")
                                    },
                                        b), b.on("mouseout", function () {
                                            this.removeClass("cke_btn_over")
                                        }, b))
                                },
                                html: '<div><a href="javascript:void(0)" tabindex="-1" title="' + d.lang.image.lockRatio + '" class="cke_btn_locked" id="' + u + '" role="checkbox"><span class="cke_icon"></span><span class="cke_label">' + d.lang.image.lockRatio + '</span></a><a href="javascript:void(0)" tabindex="-1" title="' + d.lang.image.resetSize + '" class="cke_btn_reset" id="' + z + '" role="button"><span class="cke_label">' + d.lang.image.resetSize + "</span></a></div>"
                            }]
                        }, {
                            type: "vbox",
                            padding: 1,
                            children: [{
                                type: "text",
                                id: "txtBorder",
                                requiredContent: "img{border-width}",
                                width: "60px",
                                label: d.lang.image.border,
                                "default": "",
                                onKeyUp: function () {
                                    g(this.getDialog())
                                },
                                onChange: function () {
                                    l.call(this, "advanced:txtdlgGenStyle")
                                },
                                validate: CKEDITOR.dialog.validate.integer(d.lang.image.validateBorder),
                                setup: function (a, b) {
                                    if (1 == a) {
                                        var c;
                                        c = (c = (c = b.getStyle("border-width")) && c.match(/^(\d+px)(?: \1 \1 \1)?$/)) && parseInt(c[1], 10);
                                        isNaN(parseInt(c, 10)) && (c = b.getAttribute("border"));
                                        this.setValue(c)
                                    }
                                },
                                commit: function (a, b) {
                                    var c = parseInt(this.getValue(), 10);
                                    1 == a || 4 == a ? (isNaN(c) ? !c && this.isChanged() && b.removeStyle("border") : (b.setStyle("border-width", CKEDITOR.tools.cssLength(c)), b.setStyle("border-style", "solid")), 1 == a && b.removeAttribute("border")) : 8 == a && (b.removeAttribute("border"), b.removeStyle("border-width"), b.removeStyle("border-style"), b.removeStyle("border-color"))
                                }
                            },{
                                id: "cmbAlign",
                                requiredContent: "img{float}",
                                type: "select",
                                widths: ["35%", "65%"],
                                style: "width:90px",
                                label: d.lang.common.align,
                                "default": "",
                                items: [
                                    [d.lang.common.notSet, ""],
                                    [d.lang.common.alignLeft, "left"],
                                    [d.lang.common.alignRight, "right"]
                                ],
                                onChange: function () {
                                    g(this.getDialog());
                                    l.call(this,
                                        "advanced:txtdlgGenStyle")
                                },
                                setup: function (a, b) {
                                    if (1 == a) {
                                        var c = b.getStyle("float");
                                        switch (c) {
                                            case "inherit":
                                            case "none":
                                                c = ""
                                        } !c && (c = (b.getAttribute("align") || "").toLowerCase());
                                        this.setValue(c)
                                    }
                                },
                                commit: function (a, b) {
                                    var c = this.getValue();
                                    if (1 == a || 4 == a) {
                                        if (c ? b.setStyle("float", c) : b.removeStyle("float"), 1 == a) switch (c = (b.getAttribute("align") || "").toLowerCase(), c) {
                                            case "left":
                                            case "right":
                                                b.removeAttribute("align")
                                        }
                                    } else 8 == a && b.removeStyle("float")
                                }
                            }]
                        }]
                    }, {
                        type: "vbox",
                        height: "250px",
                        children: [{
                            type: "html",
                            id: "htmlPreview",
                            style: "width:95%;",
                            html: "<div>" + CKEDITOR.tools.htmlEncode(d.lang.common.preview) + '<br><div id="' + n + '" class="ImagePreviewLoader" style="display:none"><div class="loading">&nbsp;</div></div><div class="ImagePreviewBox"><table><tr><td><a href="javascript:void(0)" target="_blank" onclick="return false;" id="' + F + '"><img id="' + E + '" alt="" /></a>' + (d.config.image_previewText || "") +
                                "</td></tr></table></div></div>"
                        }]
                    }]
                }]
            }, {
                id: "Upload",
                hidden: !1,
                filebrowser: "uploadButton",
                label: d.lang.image.upload,
                elements: [{
                    type: "file",
                    id: "upload",
                    label: d.lang.image.btnUpload,
                    style: "height:40px",
                    size: 38
                }, {
                    type: "fileButton",
                    id: "uploadButton",
                    filebrowser: "info:txtUrl",
                    label: d.lang.image.btnUpload,
                    "for": ["Upload",
                        "upload"
                    ]
                }]
            }, {
                id: "multiUpload",
                hidden: !1,
                filebrowser: "uploadButton",
                label: "\u591a\u6a94\u4e0a\u50b3",
                elements: [{
                    type: "html",
                    id: "multiupload",
                    style: "width : 400px;height : 300px ",
                    label: d.lang.image.btnUpload,
                    html: '<iframe id="frameMulti" width="400" height="400" src="/Tools/ckeditor/plugins/image/image.html?param=' + $(CKEDITOR.instances[d.name].element.$)[0].baseURI + '"></iframe>',
                    onLoad: function (a) { }
                }]
            }]
        }
    };
    CKEDITOR.dialog.add("image", function (d) {
        return w(d, "image")
    });
    CKEDITOR.dialog.add("imagebutton",
        function (d) {
            return w(d, "imagebutton")
        })
})();
﻿using LunchKingSite.BizLogic.Facade;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;

namespace LunchKingSite.Web.Tools.ckeditor.plugins.image
{
    /// <summary>
    /// Summary description for FileUploadHandler
    /// </summary>
    public class FileUploadHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Clear();
            if (!(context.User.Identity.IsAuthenticated && (context.User.Identity.Name.Contains("17life.com", StringComparison.CurrentCultureIgnoreCase) 
                                                            || context.User.Identity.Name.Contains("skm.com.tw", StringComparison.CurrentCultureIgnoreCase))))
            {
                if (context.Session[LunchKingSite.Core.VbsSession.UserId.ToString()] != null)
                {
                }
                else
                {
                    return;
                }                
            }

            string bid = "";
            string didPath = "";
            string pidPath = "";
            string param = context.Request["param"].Replace("param=", "");

            Uri myUri = new Uri(param);
            bid = getQuertString(myUri, "bid");
            didPath = getQuertString(myUri, "did");
            pidPath = getQuertString(myUri, "pid");
            if (string.IsNullOrEmpty(bid))
            {
                bid = getQuertString(myUri, "skmId");
            }

            string isPiinlife = getQuertString(myUri, "isPiinlife");
            int maxWidth = (isPiinlife == "true" || myUri.AbsoluteUri.IndexOf("BlogContent.aspx") >=0) ? 650 : 1024; //品生活圖片自動縮至650，一般好康縮為560


            LunchKingSite.Core.ISysConfProvider config = LunchKingSite.Core.Component.ProviderFactory.Instance().GetProvider<LunchKingSite.Core.ISysConfProvider>();
            string BaseDir = string.IsNullOrEmpty(config.CkFinderBaseDir) ? HttpContext.Current.Server.MapPath("~/Images/") : config.CkFinderBaseDir;

            string fold_name = !string.IsNullOrEmpty(bid) ? bid : (!string.IsNullOrEmpty(didPath) ? didPath : pidPath);
            if (!string.IsNullOrEmpty(fold_name))
            {
                fold_name = fold_name.Substring(0, 1) + "/" + fold_name;
            }

            string Dir = BaseDir == "" ? "" : string.Format("{0}imagesU/{1}/", BaseDir, fold_name);

            List<string> arrImages = new List<string>();

            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                if (!Directory.Exists(Dir))
                {
                    Directory.CreateDirectory(Dir);
                }

                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    Image oImage = Image.FromStream(file.InputStream);
                    int iwidth = oImage.Width;
                    int iheight = oImage.Height;
                   
                    FileStorageLocationInfo fileStorageLocationInfo = new FileStorageLocationInfo(Dir, file.FileName);


                    if ((context.Request.UrlReferrer.Query.ToLower().Contains("/controlroom/ppon/setup.aspx") || context.Request.UrlReferrer.Query.ToLower().Contains("/proposal") || context.Request.UrlReferrer.Query.ToLower().Contains("/blogcontent"))
                        && iwidth > maxWidth)
                    {
                        iwidth = maxWidth;
                        iheight = Convert.ToInt32((Convert.ToDouble(maxWidth) / Convert.ToDouble(oImage.Width)) * Convert.ToDouble(oImage.Height));

                        
                        if (fileStorageLocationInfo.Extension.ToLower().IndexOf("gif") == -1)
                        {
                            Image resizeimage = ImageFacade.ResizeImage(oImage, new Size(iwidth, iheight), false);
                            SaveCorrectImageFormatToStorage(file, resizeimage, fileStorageLocationInfo.FullPath);
                        }
                        else
                        {
                            ResizeGif(oImage, maxWidth, fileStorageLocationInfo.FileName);
                        }
                    }
                    else
                    {
                        SaveCorrectImageFormatToStorage(file, oImage, fileStorageLocationInfo.FullPath);
                    }
                    arrImages.Add(fold_name + "/" + fileStorageLocationInfo.FileName);
                }
                int loop = 0;

                foreach (string s in arrImages)
                {
                    loop++;
                }
            }
            context.Response.ContentType = "text/plain";
            context.Response.Write(JsonConvert.SerializeObject(arrImages));
        }

        /// <summary>
        /// 依ContentType存成正確的格式(避免png檔改副檔名為jpg)
        /// </summary>
        /// <param name="file"></param>
        /// <param name="image"></param>
        /// <param name="fullPath"></param>
        private void SaveCorrectImageFormatToStorage(HttpPostedFile file, Image image, string fullPath)
        {
            switch (file.ContentType)
            {
                case "image/jpeg":
                    image.Save(fullPath, ImageFormat.Jpeg);
                    break;
                default:
                    image.Save(fullPath);
                    break;
            }
        }

        /// <summary>
        /// 檔案儲存位置的相關資訊
        /// </summary>
        private class FileStorageLocationInfo
        {
            internal string FullPath { get; private set; }
            internal string Extension { get; private set; }
            internal string FileName { get; private set; }

            public FileStorageLocationInfo(string Dir, string fileName)
            {
                Process(Dir, fileName);
            }

            private void Process(string Dir, string fileName)
            {
                string fname = Path.Combine(Dir, fileName);
                bool flag = false;
                int idxExt = 1;
                string _Extension = fname.Substring(fname.LastIndexOf("."));
                string changeFileName = fileName;
                while (!flag)
                {
                    flag = System.IO.File.Exists(fname);
                    if (flag)
                    {
                        //如果已有檔案，將檔案改名為(N)
                        changeFileName = fileName.Replace(_Extension, "") + "(" + idxExt + ")" + _Extension;
                        fname = Path.Combine(Dir, changeFileName);
                        idxExt++;
                        flag = false;
                    }
                    else
                    {
                        //無檔案則直接上傳
                        break;
                    }
                    if (idxExt > 30)
                    {
                        //至多嘗試30次，免得無限迴圈
                        break;
                    }
                }

                FullPath = fname;
                Extension = _Extension;
                FileName = changeFileName;
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private string getQuertString(Uri input, string compare)
        {
            return HttpUtility.ParseQueryString(input.Query).Get(compare);
        }

        private void ResizeGif(Image res, int width, string fname)
        {
            int height = 0;
            if(res.Width > width)
            {
                double resPercent = (res.Width - width) / Convert.ToDouble(res.Width);
                height = res.Height - Convert.ToInt32(res.Height * resPercent);
            }

            Image gif = new Bitmap(width, height);
            Image frame = new Bitmap(width, height);
            Graphics g = Graphics.FromImage(gif);
            Rectangle rg = new Rectangle(0, 0, width, height);
            Graphics gFrame = Graphics.FromImage(frame);

            foreach (Guid gd in res.FrameDimensionsList)
            {
                FrameDimension fd = new FrameDimension(gd);
                FrameDimension f = FrameDimension.Time;
                int count = res.GetFrameCount(fd);
                ImageCodecInfo codecInfo = GetEncoder(ImageFormat.Gif);
                System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.SaveFlag;
                EncoderParameters eps = null;

                for (int i = 0; i < count; i++)
                {
                    res.SelectActiveFrame(f, i);
                    if (0 == i)
                    {
                        g.DrawImage(res, rg);
                        eps = new EncoderParameters(1);

                        eps.Param[0] = new EncoderParameter(encoder, (long)EncoderValue.MultiFrame);
                        bindProperty(res, gif);
                        gif.Save(fname, codecInfo, eps);
                    }
                    else
                    {

                        gFrame.DrawImage(res, rg);

                        eps = new EncoderParameters(1);

                        eps.Param[0] = new EncoderParameter(encoder, (long)EncoderValue.FrameDimensionTime);

                        bindProperty(res, frame);
                        gif.SaveAdd(frame, eps);
                    }
                }

                eps = new EncoderParameters(1);
                eps.Param[0] = new EncoderParameter(encoder, (long)EncoderValue.Flush);
                gif.SaveAdd(eps);
            }
        }


        private void bindProperty(Image a, Image b)
        {
            for (int i = 0; i < a.PropertyItems.Length; i++)
            {
                b.SetPropertyItem(a.PropertyItems[i]);
            }
        }

        private ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }
    }
}

/*
* Youtube Embed Plugin
*
* @author Jonnas Fonini <jonnasfonini@gmail.com>
* @version 2.1.4
*/
(function () {
    CKEDITOR.plugins.add('youtube', {
        lang: ['en', 'zh'],
        init: function (editor) {
            editor.addCommand('youtube', new CKEDITOR.dialogCommand('youtube', {
                allowedContent: 'div{*}(*); iframe{*}[!width,!height,!src,!frameborder, object param[*]; a[*]; img[*]'
            }));

            editor.ui.addButton('Youtube', {
                label: editor.lang.youtube.button,
                toolbar: 'insert',
                command: 'youtube',
                icon: this.path + 'images/icon.png'
            });

            CKEDITOR.dialog.add('youtube', function (instance) {
                var video;

                return {
                    title: editor.lang.youtube.title,
                    minWidth: 510,
                    minHeight: 200,
                    contents:
						[{
						    id: 'youtubePlugin',
						    expand: true,
						    elements:
								[{
								    id: 'txtEmbed',
								    type: 'textarea',
								    label: editor.lang.youtube.txtEmbed,
								    autofocus: 'autofocus',
								    onChange: function (api) {
								        handleEmbedChange(this, api);
								    },
								    onKeyUp: function (api) {
								        handleEmbedChange(this, api);
								    },
								    validate: function () {
								        if (this.isEnabled()) {
								            if (!this.getValue()) {
								                alert(editor.lang.youtube.noCode);
								                return false;
								            }
								            else
								                if (this.getValue().length === 0 || this.getValue().indexOf('//') === -1) {
								                    alert(editor.lang.youtube.invalidEmbed);
								                    return false;
								                }
								        }
								    }
								},
								{
								    type: 'html',
								    html: editor.lang.youtube.or + '<hr>'
								},
								{
								    type: 'hbox',
								    widths: ['70%', '15%', '15%'],
								    children:
									[
										{
										    id: 'txtUrl',
										    type: 'text',
										    label: editor.lang.youtube.txtUrl,
										    onChange: function (api) {
										        handleLinkChange(this, api);
										    },
										    onKeyUp: function (api) {
										        handleLinkChange(this, api);
										    },
										    validate: function () {
										        if (this.isEnabled()) {
										            if (!this.getValue()) {
										                alert(editor.lang.youtube.noCode);
										                return false;
										            }
										            else {
										                video = ytVidId(this.getValue());

										                if (this.getValue().length === 0 || video === false) {
										                    alert(editor.lang.youtube.invalidUrl);
										                    return false;
										                }
										            }
										        }
										    }
										},
										{
										    type: 'text',
										    id: 'txtWidth',
										    width: '60px',
										    label: editor.lang.youtube.txtWidth,
										    'default': editor.config.youtube_width != null ? editor.config.youtube_width : '650',
										    validate: function () {
										        if (this.getValue()) {
										            var width = parseInt(this.getValue()) || 0;

										            if (width === 0) {
										                alert(editor.lang.youtube.invalidWidth);
										                return false;
										            }
										        }
										        else {
										            alert(editor.lang.youtube.noWidth);
										            return false;
										        }
										    }
										},
										{
										    type: 'text',
										    id: 'txtHeight',
										    width: '60px',
										    label: editor.lang.youtube.txtHeight,
										    'default': editor.config.youtube_height != null ? editor.config.youtube_height : '420',
										    validate: function () {
										        if (this.getValue()) {
										            var height = parseInt(this.getValue()) || 0;

										            if (height === 0) {
										                alert(editor.lang.youtube.invalidHeight);
										                return false;
										            }
										        }
										        else {
										            alert(editor.lang.youtube.noHeight);
										            return false;
										        }
										    }
										}
									]
								}
								]
						}
						],
                    onOk: function () {
                        var content = '';

                        var url = 'https://', params = [];
                        var width = this.getValueOf('youtubePlugin', 'txtWidth');
                        var height = this.getValueOf('youtubePlugin', 'txtHeight');

                        url += 'www.youtube.com/';
                        url += 'embed/' + video;

                        if (params.length > 0) {
                            url = url + '?' + params.join('&');
                        }

                        url = url.replace('embed/', 'v/');
                        url = url.replace(/&/g, '&amp;');

                        content += '<object width="' + width + '" height="' + height + '" >';
                        content += '<param name="movie" value="' + url + '"></param>';
                        content += '<param name="wmode" value="transparent"></param>';
                        content += '<embed src="' + url + '" type="application/x-shockwave-flash" ';
                        content += 'width="' + width + '" height="' + height + '" ';
                        content += ' wmode="transparent"></embed>';
                        content += '</object>';

                        var element = CKEDITOR.dom.element.createFromHtml(content);
                        var instance = this.getParentEditor();
                        instance.insertElement(element);
                    }
                };
            });
        }
    });
})();

function handleLinkChange(el, api) {
	var video = ytVidId(el.getValue());
	var time = ytVidTime(el.getValue());

	if (el.getValue().length > 0) {
		el.getDialog().getContentElement('youtubePlugin', 'txtEmbed').disable();
	}
	else {
		el.getDialog().getContentElement('youtubePlugin', 'txtEmbed').enable();
	}

	if (video && time) {
		var seconds = timeParamToSeconds(time);
		var hms = secondsToHms(seconds);
		el.getDialog().getContentElement('youtubePlugin', 'txtStartAt').setValue(hms);
	}
}

function handleEmbedChange(el, api) {
	if (el.getValue().length > 0) {
		el.getDialog().getContentElement('youtubePlugin', 'txtUrl').disable();
	}
	else {
		el.getDialog().getContentElement('youtubePlugin', 'txtUrl').enable();
	}
}


/**
 * JavaScript function to match (and return) the video Id
 * of any valid Youtube Url, given as input string.
 * @author: Stephan Schmitz <eyecatchup@gmail.com>
 * @url: http://stackoverflow.com/a/10315969/624466
 */
function ytVidId(url) {
	var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
	return (url.match(p)) ? RegExp.$1 : false;
}

/**
 * Matches and returns time param in YouTube Urls.
 */
function ytVidTime(url) {
	var p = /t=([0-9hms]+)/;
	return (url.match(p)) ? RegExp.$1 : false;
}

/**
 * Converts time in hms format to seconds only
 */
function hmsToSeconds(time) {
	var arr = time.split(':'), s = 0, m = 1;

	while (arr.length > 0) {
		s += m * parseInt(arr.pop(), 10);
		m *= 60;
	}

	return s;
}

/**
 * Converts seconds to hms format
 */
function secondsToHms(seconds) {
	var h = Math.floor(seconds / 3600);
	var m = Math.floor((seconds / 60) % 60);
	var s = seconds % 60;

	var pad = function (n) {
		n = String(n);
		return n.length >= 2 ? n : "0" + n;
	};

	if (h > 0) {
		return pad(h) + ':' + pad(m) + ':' + pad(s);
	}
	else {
		return pad(m) + ':' + pad(s);
	}
}

/**
 * Converts time in youtube t-param format to seconds
 */
function timeParamToSeconds(param) {
	var componentValue = function (si) {
		var regex = new RegExp('(\\d+)' + si);
		return param.match(regex) ? parseInt(RegExp.$1, 10) : 0;
	};

	return componentValue('h') * 3600
		+ componentValue('m') * 60
		+ componentValue('s');
}

/**
 * Converts seconds into youtube t-param value, e.g. 1h4m30s
 */
function secondsToTimeParam(seconds) {
	var h = Math.floor(seconds / 3600);
	var m = Math.floor((seconds / 60) % 60);
	var s = seconds % 60;
	var param = '';

	if (h > 0) {
		param += h + 'h';
	}

	if (m > 0) {
		param += m + 'm';
	}

	if (s > 0) {
		param += s + 's';
	}

	return param;
}
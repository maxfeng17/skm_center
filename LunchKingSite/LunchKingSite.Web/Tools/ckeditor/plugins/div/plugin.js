﻿/*
 Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.md or http://ckeditor.com/license
*/
(function () {
    CKEDITOR.plugins.add("div", {
        requires: "dialog", lang: "en,zh", icons: "creatediv", hidpi: !0, init: function (a) {
            if (!a.blockless) {
                var b = a.lang.div, c = "div(*)"; CKEDITOR.dialog.isTabEnabled(a, "editdiv", "advanced") && (c += ";div[dir,id,lang,title]{*}"); a.addCommand("creatediv", new CKEDITOR.dialogCommand("creatediv", {
                    allowedContent: c, requiredContent: "div", contextSensitive: !0, contentTransformations: [["div: alignmentToStyle"]], refresh: function (a, b) {
                        this.setState("div" in (a.config.div_wrapTable ? b.root : b.blockLimit).getDtd() ?
                        CKEDITOR.TRISTATE_OFF : CKEDITOR.TRISTATE_DISABLED)
                    }
                })); a.addCommand("editdiv", new CKEDITOR.dialogCommand("editdiv", { requiredContent: "div" })); a.addCommand("removediv", {
                    requiredContent: "div", exec: function (a) {
                        function b(b) { (b = CKEDITOR.plugins.div.getSurroundDiv(a, b)) && !b.data("cke-div-added") && (g.push(b), b.data("cke-div-added")) } for (var f = a.getSelection(), c = f && f.getRanges(), e, h = f.createBookmarks(), g = [], d = 0; d < c.length; d++) e = c[d], e.collapsed ? b(f.getStartElement()) : (e = new CKEDITOR.dom.walker(e), e.evaluator =
                        b, e.lastForward()); for (d = 0; d < g.length; d++) g[d].remove(!0); f.selectBookmarks(h)
                    }
                }); a.ui.addButton && a.ui.addButton("CreateDiv", { label: b.toolbar, command: "creatediv", toolbar: "blocks,50" }); a.addMenuItems && (a.addMenuItems({ editdiv: { label: b.edit, command: "editdiv", group: "div", order: 1 }, removediv: { label: b.remove, command: "removediv", group: "div", order: 5 } }), a.contextMenu && a.contextMenu.addListener(function (b) {
                    return !b || b.isReadOnly() ? null : CKEDITOR.plugins.div.getSurroundDiv(a) ? { editdiv: CKEDITOR.TRISTATE_OFF, removediv: CKEDITOR.TRISTATE_OFF } :
                    null
                })); CKEDITOR.dialog.add("creatediv", this.path + "dialogs/div.js"); CKEDITOR.dialog.add("editdiv", this.path + "dialogs/div.js")
            }
        }
    }); CKEDITOR.plugins.div = { getSurroundDiv: function (a, b) { var c = a.elementPath(b); return a.elementPath(c.blockLimit).contains(function (a) { return a.is("div") && !a.isReadOnly() }, 1) } }
})();
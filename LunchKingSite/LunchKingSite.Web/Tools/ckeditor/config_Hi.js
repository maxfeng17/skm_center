﻿/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    config.language = 'zh-tw';
    // config.uiColor = '#AADC6E';
    config.htmlEncodeOutput = true;
    config.enterMode = CKEDITOR.ENTER_BR;
    config.skin = 'moono-lisa';
    config.filebrowserBrowseUrl = '../../Tools/ckeditor/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl = '../../Tools/ckeditor/ckfinder/ckfinder.html?type=Images';
    config.toolbar = [
        ['NewPage', 'Preview', '-',
            'Undo', 'Redo', '-',
            'Cut', 'Copy', 'Paste', 'PasteFromWord', '-',
            'Find', 'Replace', 'SelectAll', '-',
            'Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', '-',
            'Maximize', 'Source', '-', 'Link', 'Unlink', 'Anchor'], '/',
        ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'RemoveFormat', '-',
            'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-',
            'NumberedList', 'BulletedList', 'Outdent', 'Indent', 'BidiLtr', 'BidiRtl', '-',
            'TextColor', 'BGColor', 'Format', 'Font', 'FontSize', '-'
        ]
    ];
};

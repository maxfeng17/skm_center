﻿!function () {
    var ue = {};

    function bindingEvent(defaultShow) {

        $(".btn-collapse").trigger('click');

        $('.textBtn').on('click', function () {
            if ($(this).attr("isShow") == undefined) {
                $(this).attr("isShow", "0");
            }

            switch ($(this).attr("isShow")) {
                case "1":
                    $(this).attr("isShow", "0");
                    $(this).text("收起");
                    $(this).parents('tr').next().removeClass("notShow");
                    break;
                case "0":
                    $(this).attr("isShow", "1");
                    $(this).text("展開");
                    $(this).parents('tr').next().addClass("notShow");
                    break;
            }
        });

        $(".textBtn").trigger('click');
    }

    var pageChartData = {};

    ue.initMain = function (data, store) {
        store = (typeof store !== 'undefined') ? store : true;
        var docfrag = document.createDocumentFragment();
        var d = data.DetailData;
        for (var i in d) {
            var key = d[i].PageNo;
            var page = document.createElement("div");
            page.id = key;
            page.className = "fed-box";

            var header = document.createElement("H3");

            var title = document.createElement("span");
            title.className = "title";

            var tt = "[" + d[i].UniqueID + "]" + d[i].ItemName

            if (store) {
                tt += "(" + (d[i].WithSubStore ? "多分店" : "無分店") + ")";
            }
            else {
                tt += " - " + d[i].StoreName;
            }
            title.innerText = tt;

            header.appendChild(title);

            var subTitle = document.createElement("p");
            subTitle.className = "title-small";
            var st =  d[i].EventName;
            subTitle.innerText = st;
           
            var open = document.createElement("div");
            open.className = "fed-small clearfix close";

            var fedLeft = document.createElement("div");
            fedLeft.className = "fed-left";

            var fedDate = document.createElement("div");
            fedDate.className = "fed-date";

            $(fedDate).html("<span class='icon-calendar'></span><h4>兌換期間：" + new parseJsonDate(d[i].DeliverTimeS).getdate() + "～" +
			new parseJsonDate(d[i].DeliverTimeE).getdate() + "</h4>");

            var fedInfor = document.createElement("div");
            fedInfor.className = "fed-infor";

            $(fedInfor).html("" +
            "<p class='bartag fed-count'>評論數：<span>" + d[i].CommentCount + "</span>筆</p>" +
            "<p class='bartag fed-assess'>已評價：<span>" + d[i].EvaluateCount + "</span>筆</p>" +
            "<p class='bartag fed-record'>已核銷：<span>" + d[i].VerifiedCount + "</span>筆</p>" +
            "<p class='bartag fed-total'>總銷售：<span>" + d[i].DealCount + "</span>筆</p>" +
            "<div id='" + key + "_bar' class='fed-infor-bar'></div>");

            fedLeft.appendChild(fedDate);
            fedLeft.appendChild(fedInfor);

            var fedRight = document.createElement("div");
            fedRight.className = "fed-right";

            var rightTopWithoutPie = "<ul class='fed-like'>" +
              "<li><h4 class='necessary'><span class='icon-happy'></span>整體滿意度<strong>" + d[i].QuesCount[0].AvgStar + "</strong></h4></li>" +
              "<li><h4 class='necessary'><span class='icon-thumbs-up'></span>再訪意願<strong>" + d[i].QuesCount[1].AvgStar + "</strong></h4></li>";

            var rightTopWithPie = "<ul class='fed-like'>" +
              "<li><h4 class='necessary'><span class='icon-happy'></span>整體滿意度<strong>" + d[i].QuesCount[0].AvgStar + "</strong></h4></li>" +
              "<li><h4 class='necessary'><span class='icon-thumbs-up'></span>再訪意願<strong>" + d[i].QuesCount[1].AvgStar + "</strong></h4></li>";
            var rightCommon = "<li><h4 class='necessary'><span class='fa fa-trash fa-lg'></span>環境整潔度<strong>" + (d[i].QuesCount[2].AvgStar == 0 ? "- <p class='summery'>自2015/4/7修正選項，因此尚無資料</p>" : d[i].QuesCount[2].AvgStar || 0) + "</strong></h4></li>" +
              "<li><h4><span class='icon-heart'></span>服務親切度<strong>" + (d[i].QuesCount[3] == null ? 0 : d[i].QuesCount[3].AvgStar || 0) + "</strong></h4><p class='summery'>(非必填：作答數 " + (d[i].QuesCount[3] == null ? 0 : d[i].QuesCount[3].Counting || 0) + "/ 總數" + d[i].EvaluateCount + ")</p></li></ul>";

            var rightHref = "";
            if (d[i].WithSubStore && store) {
                rightHref = "<span class = 'cookieR'><a class='btn-branch' href='EvaluateShowProducts?sid=" + d[i].Sid + "&page=' >分店列表</a></span>";
            }
            else {
                rightHref = "<a href='EvaluateDetail?bid=" + d[i].Bid + "&sid=" + d[i].Sid + "' class='btn-assess cookieR'>評價列表</a>";
            }

            $(fedRight).html(rightTopWithPie + rightCommon + rightHref);

            open.appendChild(fedLeft);
            open.appendChild(fedRight);
            var enddiv = document.createElement("div");
            enddiv.className = "endDiv";

            AddDrewRows(key, [d[i].CommentCount, d[i].EvaluateCount, d[i].VerifiedCount, d[i].DealCount], d[i].QuesCount);

            page.appendChild(header);
            page.appendChild(subTitle);
            page.appendChild(open);
            page.appendChild(enddiv);
            docfrag.appendChild(page);
        }

        document.getElementById("mainContent").appendChild(docfrag);
        bindingEvent(store);
        drewChart();
    }

    ue.intiCommentTable = function (data) {
        var tableStr = "", idx = pageCount = 20;
        data.forEach(function (i) {
            var VTime = i.VerifiedTime;
            if (!VTime && typeof VTime != "undefined" && VTime != 0) {
                VTime = "取消核銷";
            } else {
                VTime = new parseJsonDate(VTime).getdatetime();
            }
            tableStr += "<tr class='" + redRow(i.Q1, i.Q2, i.Q3, i.Q4) + " paging" + ((idx / pageCount) | 0) + "'>" +
			"<td>" + new parseJsonDate(i.CreatTime).getdatetime() + "</td>" +
			"<td>" + i.ItemName + "</td>" +
			"<td>" + VTime + "</td>" +
			"<td>" + redHint(i.Q1) + "</td>" +
			"<td>" + redHint(i.Q2) + "</td>" +
			"<td>" + redHint(i.Q3) + "</td>" +
			"<td>" + redHint(i.Q4) + "</td>" +
			"<td>" + i.CouponSequenceNumber + "</td>";

            if (i.Comment) {
                tableStr += "<td><a class='textBtn'>展開</a></td></tr>" +
				"<tr class='notShow'><td colspan='10' class='tleft comPaging" + ((idx / pageCount) | 0) + "'>" + i.Comment + "</td></tr>";
            } else {
                tableStr += "<td></td></tr>";
            }

            idx++;
        });
        $('#detailTable tr:last').after(tableStr);
        bindingEvent();
    }

    ue.initDetail = function (data) {

        var docfrag = document.createDocumentFragment();
        var d = data.DetailData[0];
        var key = d.PageNo;

        var page = document.createElement("div");
        page.id = key;
        page.className = "fed-box";

        var header = document.createElement("H3");

        var title = document.createElement("span");
        title.className = "title";
        title.innerText = "[" + d.UniqueID + "]" + d.ItemName + " - " + d.StoreName;

        header.appendChild(title);

        var subTitle = document.createElement("p");
        subTitle.className = "title-small";
        var st = d.EventName;
        subTitle.innerText = st;

        var open = document.createElement("div");
        open.className = "fed-zone clearfix open";

        var fedLeft = document.createElement("div");
        fedLeft.className = "fed-left";

        var fedDate = document.createElement("div");
        fedDate.className = "fed-date";

        $(fedDate).html("<span class='icon-calendar'></span><h4>兌換期間：" + new parseJsonDate(d.DeliverTimeS).getdate() + "～" + new
        parseJsonDate(d.DeliverTimeE).getdate() + "</h4>");

        var fedInfor = document.createElement("div");
        fedInfor.className = "fed-infor";

        $(fedInfor).html("" +
        "<p class='bartag fed-count'>評論數：<span>" + d.CommentCount + "</span>筆</p>" +
        "<p class='bartag fed-assess'>已評價：<span>" + d.EvaluateCount + "</span>筆</p>" +
        "<p class='bartag fed-record'>已核銷：<span>" + d.VerifiedCount + "</span>筆</p>" +
        "<p class='bartag fed-total'>總銷售：<span>" + d.DealCount + "</span>筆</p>" +
        "<div id='" + key + "_bar' class='fed-infor-bar'></div>");

        fedLeft.appendChild(fedDate);
        fedLeft.appendChild(fedInfor);

        var fedRight = document.createElement("div");
        fedRight.className = "fed-right";

        $(fedRight).html("<ul class='fed-like'>" +
          "<li><h4 class='necessary'><span class='icon-happy'></span>整體滿意度<strong>" + d.QuesCount[0].AvgStar + "</strong></h4><div id='" + key + "_pieL' class='fed-like-peichart'></div></li>" +
          "<li><h4 class='necessary'><span class='icon-thumbs-up'></span>再訪意願<strong>" + d.QuesCount[1].AvgStar + "</strong></h4><div id='" + key + "_pieR' class='fed-like-peichart'></div></li>" +
          "<li><h4 class='necessary'><span class='fa fa-trash fa-lg'></span>環境整潔度<strong>" + (d.QuesCount[2].AvgStar == 0 ? "- <p class='summery'>自2015/4/7修正選項，因此尚無資料</p>" : d.QuesCount[2].AvgStar || 0) + "</strong></h4><div id='" + key + "_pie1' class='fed-like-peichart'></div></li>" +
          "<li><h4><span class='icon-heart'></span>服務親切度<strong>" + (d.QuesCount[3] == null ? 0 : d.QuesCount[3].AvgStar || 0) + "</strong></h4><p class='summery'>(非必填：作答數 " + (d.QuesCount[3] == null ? 0 : d.QuesCount[3].Counting || 0) + "/ 總數" + d.EvaluateCount + ")</p><div id='" + key + "_pie2' class='fed-like-peichart'></div></li>");

        open.appendChild(fedLeft);
        open.appendChild(fedRight);

        var enddiv = document.createElement("div");
        enddiv.className = "endDiv";

        AddDrewRows(key, [d.CommentCount, d.EvaluateCount, d.VerifiedCount, d.DealCount], d.QuesCount);

        page.appendChild(header);
        page.appendChild(subTitle);
        page.appendChild(open);

        docfrag.appendChild(page);


        document.getElementById("mainContent").appendChild(docfrag);

        for (var key in pageChartData) {
            var v = pageChartData[key];
            drewHistogram(key + '_bar', v.histogram);
            drewCycleDetail(key + '_pieL', v.circleDetailgood);
            drewCycleDetail(key + '_pieR', v.circleDetailcome);
            drewCycleDetail(key + '_pie1', v.circleDetailbueaty);
            drewCycleDetail(key + '_pie2', v.circleDetailservice);
        }
    }

    function redRow() {
        var i, max = 0;
        for (var i = arguments.length - 1; i ; --i) {
            if (arguments[i] > 0 && arguments[i] <= 2) {
                return "bgcol";
            }
        }
        return "";
    }

    function redHint(v) {
        var result = "";
        if (v == 0) { result = ""; }
        else if (v >= 1 && v <= 2) {
            result = "<sapn style='color:red'>" + v + "</sapn>";
        } else if (v >= 3 && v <= 5) {
            result = "<sapn>" + v + "</sapn>";
        }
        return result;
    }

    function drewChart() {
        for (var key in pageChartData) {
            var v = pageChartData[key];
            drewHistogram(key + '_bar', v.histogram);
            //drewCycle(key + '_pieL', v.circlegood);
            //drewCycle(key + '_pieR', v.circlecome);
        }
    }

    function drewHistogram(id, data) {
        if ($('#' + id).length <= 0) {
            return;
        }
        var margin = { top: 10, right: 10, bottom: 10, left: 10 },
            width = $('#' + id).parent().width() / 2 - margin.right - margin.left,
            height = 85
        ;

        width = 300;
        height = 165;
        var x = d3.scale.linear()
            .range([0, width]);

        var y = d3.scale.ordinal()
            .rangeRoundBands([0, height], .1);

        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("top")
            .tickSize(-60);

        var yAxis = d3.svg.axis()
            .scale(y)
            .orient("left")
            .tickSize(0);


        var chart = d3.select("#" + id)
            .append("svg")
            .attr("width", width + margin.right + margin.left)
            .attr("height", height)
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        x.domain([0, d3.max(data, function (d) { return d.value; })]);
        y.domain(data.map(function (d) { return d.name; }));

        var bar = chart.selectAll("g")
            .data(data)
            .enter()
            .append("g")
		    .attr("transform", function (d, i) {
		        return "translate(0," + (10 + i * 20) + ")";
		    });

        bar.append("rect")
		    .attr("height", function (d, i) { return 85 - i * 20; })
            .attr("width", function (d, i) { return x(d.value); })
		    .attr("fill", function (c) { return c.color; });

        bar.append("text")
            .attr("x", function (d, i) { return x(d.value); })
            .attr("dx", -5)
            .attr("dy", "0.9em")
            .style("text-anchor", "end")
            .text(function (d) {
                //return d.value;
                if (d.name === "Verified") {
                    //console.log("3:" + data[3].value + "2:" + data[2].value);
                    return d.value + "(" + (((data[2].value / data[3].value) * 100) | 0) + '%)';
                } else {
                    return d.value;
                }
            });
    }

    function AddDrewRows(k, params, QuesCount) {

        var CommentCount = params[0], EvaluateCount = params[1], VerifiedCount = params[2], DealCount = params[3];
        var h = {
            "histogram": [
                    {
                        "name": "a",
                        "value": CommentCount,
                        "color": "#2DAED0"
                    },
                    {
                        "name": "b",
                        "value": EvaluateCount,
                        "color": "#2D39D0"
                    },
                    {
                        "name": "Verified",
                        "value": VerifiedCount,
                        "color": "#94D02D"
                    },
                    {
                        "name": "Deal",
                        "value": DealCount,
                        "color": "#D02D2D"
                    }
            ],
            "circlegood": [
					{
					    "name": "a",
					    "value": QuesCount[0].AvgStar * 20,
					    "color": "#E3665B"
					},
					{
					    "name": "b",
					    "value": 100 - QuesCount[0].AvgStar * 20,
					    "color": "#E3CCCC"
					}
            ],
            "circlecome": [
					{
					    "name": "a",
					    "value": QuesCount[1].AvgStar * 20,
					    "color": "#E3665B"
					},
					{
					    "name": "b",
					    "value": 100 - QuesCount[1].AvgStar * 20,
					    "color": "#E3CCCC"
					}
            ],
            "circleDetailgood": [
                {
                    "name": "star5",
                    "value": QuesCount[0].RateAndCount[5] || 0,
                    "total": QuesCount[0].Counting || 0,
                    "color": "#e3665b"
                },
                {
                    "name": "star4",
                    "value": QuesCount[0].RateAndCount[4] || 0,
                    "total": QuesCount[0].Counting || 0,
                    "color": "#e37b71"
                },
                {
                    "name": "star3",
                    "value": QuesCount[0].RateAndCount[3] || 0,
                    "total": QuesCount[0].Counting || 0,
                    "color": "#e39088"
                },
                {
                    "name": "star2",
                    "value": QuesCount[0].RateAndCount[2] || 0,
                    "total": QuesCount[0].Counting || 0,
                    "color": "#e3b9b6"
                },
                {
                    "name": "star1",
                    "value": QuesCount[0].RateAndCount[1] || 0,
                    "total": QuesCount[0].Counting || 0,
                    "color": "#e3cccc"
                }
            ],
            "circleDetailcome": [
                {
                    "name": "star5",
                    "value": QuesCount[1].RateAndCount[5] || 0,
                    "total": QuesCount[1].Counting || 0,
                    "color": "#e3665b"
                },
                {
                    "name": "star4",
                    "value": QuesCount[1].RateAndCount[4] || 0,
                    "total": QuesCount[1].Counting || 0,
                    "color": "#e37b71"
                },
                {
                    "name": "star3",
                    "value": QuesCount[1].RateAndCount[3] || 0,
                    "total": QuesCount[1].Counting || 0,
                    "color": "#e39088"
                },
                {
                    "name": "star2",
                    "value": QuesCount[1].RateAndCount[2] || 0,
                    "total": QuesCount[1].Counting || 0,
                    "color": "#e3b9b6"
                },
                {
                    "name": "star1",
                    "value": QuesCount[1].RateAndCount[1] || 0,
                    "total": QuesCount[1].Counting || 0,
                    "color": "#e3cccc"
                }
            ],
            "circleDetailbueaty": [
                {
                    "name": "star5",
                    "value": QuesCount[2] == null ? 0 : QuesCount[2].RateAndCount[5] || 0,
                    "total": QuesCount[2] == null ? 0 : QuesCount[2].Counting || 0,
                    "color": "#e3665b"
                },
                {
                    "name": "star4",
                    "value": QuesCount[2] == null ? 0 : QuesCount[2].RateAndCount[4] || 0,
                    "total": QuesCount[2] == null ? 0 : QuesCount[2].Counting || 0,
                    "color": "#e37b71"
                },
                {
                    "name": "star3",
                    "value": QuesCount[2] == null ? 0 : QuesCount[2].RateAndCount[3] || 0,
                    "total": QuesCount[2] == null ? 0 : QuesCount[2].Counting || 0,
                    "color": "#e39088"
                },
                {
                    "name": "star2",
                    "value": QuesCount[2] == null ? 0 : QuesCount[2].RateAndCount[2] || 0,
                    "total": QuesCount[2] == null ? 0 : QuesCount[2].Counting || 0,
                    "color": "#e3b9b6"
                },
                {
                    "name": "star1",
                    "value": QuesCount[2] == null ? 0 : QuesCount[2].RateAndCount[1] || 0,
                    "total": QuesCount[2] == null ? 0 : QuesCount[2].Counting || 0,
                    "color": "#e3cccc"
                }
            ],
            "circleDetailservice": [
                {
                    "name": "star5",
                    "value": QuesCount[3] == null ? 0 : QuesCount[3].RateAndCount[5] || 0,
                    "total": QuesCount[3] == null ? 0 : QuesCount[3].Counting || 0,
                    "color": "#e3665b"
                },
                {
                    "name": "star4",
                    "value": QuesCount[3] == null ? 0 : QuesCount[3].RateAndCount[4] || 0,
                    "total": QuesCount[3] == null ? 0 : QuesCount[3].Counting || 0,
                    "color": "#e37b71"
                },
                {
                    "name": "star3",
                    "value": QuesCount[3] == null ? 0 : QuesCount[3].RateAndCount[3] || 0,
                    "total": QuesCount[3] == null ? 0 : QuesCount[3].Counting || 0,
                    "color": "#e39088"
                },
                {
                    "name": "star2",
                    "value": QuesCount[3] == null ? 0 : QuesCount[3].RateAndCount[2] || 0,
                    "total": QuesCount[3] == null ? 0 : QuesCount[3].Counting || 0,
                    "color": "#e3b9b6"
                },
                {
                    "name": "star1",
                    "value": QuesCount[3] == null ? 0 : QuesCount[3].RateAndCount[1] || 0,
                    "total": QuesCount[3] == null ? 0 : QuesCount[3].Counting || 0,
                    "color": "#e3cccc"
                }
            ]
        };
        pageChartData[k] = h;
    }

    function drewCycleDetail(id, data) {

        if ($('#' + id).length <= 0) {
            return;
        }
        var canvas = d3.select("#" + id).append("svg")
			.attr("width", 200)
            .attr("height", 200);

        var group = canvas.append("g")
        .attr("transform", "translate(100,100)");

        var arc = d3.svg.arc()
        .outerRadius(65);

        var pie = d3.layout.pie()
        .value(function (d, i) { return d.value; }).sort(null);

        var arcs = group.selectAll(".arc")
        .data(pie(data))
        .enter()
        .append("g")
        .attr("class", "arc");

        var tweenPie = function (b) {
            b.innerRadius = 0;
            var i = d3.interpolate({ startAngle: 0, endAngle: 0 }, b);
            return function (t) {
                return arc(i(t));
            };
        }

        arcs.append("path")
        .attr("d", arc)
        .attr("fill", function (d, i) { return data[i].color; })
		.transition()
        .ease("bounce")
		.duration(2000)
        //.delay(function(d, i) { return i * 30; })
        .attrTween("d", tweenPie);

        arcs.append("svg:text")
        .attr("transform", function (d) {
            d.innerRadius = 25;
            //d.outerRadius = 1500;
            return "translate(" + arc.centroid(d) + ")";
        })
        .attr("dy", 5)
        .style('fill', '#FFFFFF')
        .attr("text-anchor", "middle")
        .text(function (d, i) {
            var star = data[i].name.substr(4, 1);
            return data[i].value == 0 ? "" : (((data[i].value / data[i].total) * 100) | 0) + '%';
        });

        arcs.append("svg:text")
        .attr("transform", function (d) {
            d.innerRadius = 105;
            d.outerRadius = 50;
            return "translate(" + arc.centroid(d) + ")";
        })
        .attr("text-anchor", "middle")
        .text(function (d, i) {
            var star = data[i].name.substr(4, 1);
            return data[i].value == 0 ? "" : star + '顆星';
        });
    }

    function drewCycle(id, data) {

        if ($('#' + id).length <= 0) {
            //console.log(id + ' is not found!');
            return;
        }
        var canvas = d3.select("#" + id).append("svg")
			.attr("width", 180)
            .attr("height", 180);

        var group = canvas.append("g")
        .attr("transform", "translate(80,80)");

        var arc = d3.svg.arc()
        .innerRadius(30)
        .outerRadius(55);

        var pie = d3.layout.pie()
        .value(function (d, i) { return d.value; }).sort(null);

        var arcs = group.selectAll(".arc")
        .data(pie(data))
        .enter()
        .append("g")
        .attr("class", "arc");

        arcs.append("path")
        .attr("d", arc)
        .attr("fill", function (d, i) { return data[i].color; });

        arcs.append("text")
        .attr("transform", function (d) { return "translate(0,10)"; })
        .attr("text-anchor", "middle")
        .attr("font-size", "2em")
        .text(data[0].value + '%');
    }

    function parseJsonDate(jsonDateString, withTime) {
        if (jsonDateString != null && jsonDateString != "")
        {
            var d = new Date(parseInt(jsonDateString.replace('/Date(', '')));
            year = d.getFullYear();
            month = pad(d.getMonth() + 1);
            day = pad(d.getDate());
            hour = pad(d.getHours());
            minutes = pad(d.getMinutes());

            var result = year + "/" + month + "/" + day

            this.getdate = function () { return result };
            this.getdatetime = function () { return result += " " + hour + ":" + minutes; };
        }
    }

    function pad(num) {
        num = "0" + num;
        return num.slice(-2);
    }

    ue.block_postback = function (msg) {
        $.blockUI({ message: "<center><img src='../../Themes/PCweb/images/spinner.gif' />" + msg + "</center>", css: { width: '250px' } });
    }

    ue.getQueryDict = function () {
        var result = {}, queryString = location.search.slice(1),
            re = /([^&=]+)=([^&]*)/g, m;

        while (m = re.exec(queryString)) {
            result[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
        }

        return result;
    }

    ue.createPagination = function (totalCount, nowPage) {
        var page = d3.range(1, Math.ceil(totalCount / 20) + 1);
        var options_str = "";

        page.forEach(function (page) {
            options_str += "<option value='" + page + "'>" + page + "</option>";
        });

        $('.totalCount').text(totalCount);
        $('.totalPage').text(Math.ceil(totalCount / 20));
        $('#nowPageSelect').html(options_str);
        $("#nowPageSelect option[value='" + nowPage + "']").attr('selected', 'selected');

    }

    this.ue = ue;
}();
﻿function pupwin(url, title, width, height) {
    var attr = 'height=' + height + ',width=' + width + ',toolbar=no,menubar=no,scrollbars=yes,resizable=yes,status=no,location=no';
    window.open(url, title, attr, '');
}

function BookMark(title, url) {
    if (document.all)
        window.external.AddFavorite(url, title);
    else if (window.sidebar)
        window.sidebar.addPanel(title, url, "");
}

function LogError(msg, url, lineNo) {
    $.dnajax(SiteRoot + '/service/srv_jelog.asmx/Log', "{ msg:'" + msg + "', url:'" + url + "', lineNo:" + lineNo + " }", null);
}

function LogError(ex) {
    // in order to use this "stacktrace.js" has to be included first
    var t = printStackTrace({ e: ex });
    LogError(t.join('\n\n'), ex.Source, ex.Number);
}

$.fn.clearForm = function() {
    return this.each(function() {
        var type = this.type, tag = this.tagName.toLowerCase();
        if (tag == 'form')
            return $(':input', this).clearForm();
        if (type == 'text' || type == 'password' || tag == 'textarea')
            this.value = '';
        else if (type == 'checkbox' || type == 'radio')
            this.checked = false;
        else if (tag == 'select')
            this.selectedIndex = -1;
    });
};

$.fn.watermark = function (className, message) {
    $(this).addClass(className);
    $(this).val(message);
    $(this).focus(function () {
        var str = $(this).val();
        if (str == message) {
            $(this).val("");
            $(this).toggleClass(className);
        }
    });
    $(this).blur(function () {
        var str = $(this).val();
        if (str.length <= 0) {
            $(this).toggleClass(className);
            $(this).val(message);
        }
    });
};

$.extend({
    dnajax: function(url, data, success) {
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: success,
            error: function (xhr, ajaxOption, ex) {
                if ($("#MasterPass_container") == null) {
                    alert('我們發生了一些錯誤喔，請稍後再嘗試.');
                }
            }
        });
    }
});

$.fn.addItems = function (data, getTxt, getVal) {
    return this.each(function () {
        var list = this;
        $.each(data, function (i, t) {
            list.options.add(new Option(getTxt(t), getVal(t)));
        });
    });
};

$.extend({
    logError: function () {
        window.onerror = function (msg, url, lineNo) {
            LogError(msg, url, lineNo);
            return false;
        }
        // override jQuery.fn.bind to wrap every provided function in try/catch
        var jQueryBind = jQuery.fn.bind;
        jQuery.fn.bind = function (type, data, fn) {
            if (!fn && data && typeof data == 'function') {
                fn = data;
                data = null;
            }
            if (fn) {
                var origFn = fn;
                var wrappedFn = function () {
                    var ret;
                    try {
                        ret = origFn.apply(this, arguments);
                    }
                    catch (ex) {
                        LogError(ex);
                        // re-throw ex iff error should propogate
                        throw ex;
                    }
                    return ret;
                };
                fn = wrappedFn;
            }
            return jQueryBind.call(this, type, data, fn);
        };
    }
});

function setupdatepicker(from, to, num) {
	num = num | 1;
	var dates = $('#' + from + ', #' + to).datepicker({
		changeMonth: true,
		numberOfMonths: num,
		onSelect: function (selectedDate) {
			var option = this.id == from ? "minDate" : "maxDate";
			var instance = $(this).data("datepicker");
			var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
			dates.not(this).datepicker("option", option, date);
		}
	});
}

//copy from piinlife
$.fn.queueInterruptibleAction = function (delayMilseconds, actionCallback) {
    //cancel if other action exist
    this.cancelInterruptibleAction();
    // delay execute delayCallback
    var timerId = window.setTimeout(function () {
        $.removeData(this, 'timerId');
        actionCallback.call(this);
    }, delayMilseconds);
    $.data(this[0], 'timerId', timerId);
};

$.fn.cancelInterruptibleAction = function () {
    var timerId = $.data(this[0], 'timerId');
    if (timerId != null) {
        $.removeData(this[0], 'timerId');
        window.clearTimeout(timerId);
    }
};

function checkCreditCardWithLuhnAlgorithm($cardNo) {
    // Luhn algorithm, reference link https://en.wikipedia.org/wiki/Luhn_algorithm
    var value = $cardNo;

    // reverse
    value = value.split("").reverse().join("");
    // set to array
    value = value.split("");

    var s1 = 0, s2 = 0;
    var i, tmp;

    // odd
    for (i = 0; i < value.length; i = i + 2) {
        s1 += parseInt(value[i]);
    }
    // even
    for (i = 1; i < value.length; i = i + 2) {
        tmp = parseInt(value[i]) * 2;
        s2 = s2 + Math.trunc(tmp / 10) + tmp % 10;
    }

    if ((s1 + s2) % 10 == 0)
        return true;
    else
        return false;
}
﻿var Sales = Sales || {};
var SCOPES = ["https://www.googleapis.com/auth/calendar"];

Sales = {
    version: "1.0.0",
    createDate: "2016/03/23",
    dependencies: {
        jQuery: "1.9.1"
    }
}

Sales.Page = {
    Init: (function () {
    }),
};

Sales.Calendar = {
    GetSalesCalenderID: (function () {
        GoogleCalendarListbyName();
    }),
    Create: (function () {
        CreateGoogleCalendar();
    }),
    EventDBSave: (function (SalesCalenderId, Gid, dbData) {
        SalesCalendarEventDBSave(SalesCalenderId, Gid, dbData);
    }),
    EventList: (function (SalesCalenderId) {
        GoogleCalendarEventList(SalesCalenderId);
    }),
};


//*---------------------------calendar method---------------------------*//
/*
* 檢查是否已取得使用者授權
*/
function checkAuth() {
    gapi.auth.authorize(
	  {
	      'client_id': CLIENT_ID,
	      'scope': SCOPES.join(' '),
	      'immediate': true
	  }, handleAuthResult);
}

function handleAuthResult(authResult) {
    var authorizeDiv = document.getElementById('authorize-div');
    if (authResult && !authResult.error) {
        $("#authorize-div").hide();
        $("#cal-content").show();
        GoogleCalendarList();
    } else {
        $("#authorize-div").show();
        $("#cal-content").hide();
    }
}

/*
* 請求認證
*/
function handleAuthClick(event) {
    gapi.auth.authorize(
      { client_id: CLIENT_ID, scope: SCOPES, immediate: false },
      handleAuthResult);
    return false;
}


/**
 * 列出日曆列表
 */
function GoogleCalendarList() {
    var spanCalendarList = $("#spanCalendarList");
    gapi.client.load('calendar', 'v3', function () {
        var request = gapi.client.calendar.calendarList.list();
        request.execute(function (resp) {
            if (resp.items != undefined) {
                var _html = "";
                for (var i = 0; i < resp.items.length; i++) {
                    var calendarID = resp.items[i].id;
                    var calendarSummary = resp.items[i].summary;
                    if (calendarSummary == "業務系統" || calendarID == $("#txtUserName").val() || calendarSummary == $("#txtUserName").val()) {
                        if (i == 0) {
                            _html += "<span><input type='radio' checked='checked' name='rdoCalendarList' value='" + urlencode(calendarID) + "&amp;color=%23" + resp.items[i].backgroundColor.replace("#", "") + "' onclick='changeFrame(this)' />" + calendarSummary + "</span>";
                        } else {
                            _html += "<span><input type='radio' name='rdoCalendarList' value='" + urlencode(calendarID) + "&amp;color=%23" + resp.items[i].backgroundColor.replace("#", "") + "' onclick='changeFrame(this)' />" + calendarSummary + "</span>";
                        }
                        
                    }                    
                }
                if (spanCalendarList != undefined) {
                    spanCalendarList.html(_html);
                }                
            }
        });
    });
}


/*
* 列出日曆事件
*/
function GoogleCalendarEventList(SalesCalenderId) {
    gapi.client.load('calendar', 'v3', function () {
        var request = gapi.client.calendar.events.list({
            'calendarId': SalesCalenderId,
            'timeMin': (new Date()).toISOString(),
            'showDeleted': false,
            'singleEvents': true,
            'maxResults': 10,
            'orderBy': 'startTime'
        });

        request.execute(function (resp) {
            var events = resp.items;
            if (events.length > 0) {
                for (i = 0; i < events.length; i++) {
                    var event = events[i];
                    var when = event.start.dateTime;
                    if (!when) {
                        when = event.start.date;
                    }
                    console.log(event.summary + ' (' + when + ')')
                }
            } else {
                console.log('No upcoming events found.');
            }

        });
    });
}

/*
* 建立日曆
*/
function CreateGoogleCalendar() {
    var CalendarName = $("#SalesCalenderName").val();
    if (CalendarName != undefined) {
        var request = gapi.client.calendar.calendars.insert(
                {
                    "resource":
                    {
                        "summary": CalendarName,
                        "description": "系統建立",
                        "timezone": "Asia/Taipei"
                    }
                });

        request.execute(function (res) {
            $("#CalenderId").val(res.id);
        });
    }
}

/**
 * 依據日曆名稱找出ID
 */
function GoogleCalendarListbyName() {
    var CalendarName = $("#SalesCalenderName").val();
    if (CalendarName != undefined) {
        gapi.client.load('calendar', 'v3', function () {
            var request = gapi.client.calendar.calendarList.list();
            request.execute(function (resp) {
                if (resp.items != undefined) {
                    for (var i = 0; i < resp.items.length; i++) {
                        var calendarID = resp.items[i].id;
                        var calendarSummary = resp.items[i].summary;

                        if (CalendarName == calendarSummary) {
                            $("#CalenderId").val(calendarID);
                            loadFrame(calendarID);
                            return false;
                        }
                    }
                    CreateGoogleCalendar();
                }
            });
        });
    }
}

/*
* 儲存日曆事件
*/
function GoogleCalenderEventInsert(SalesCalenderId, event, dbData, cid) {
    var request = gapi.client.calendar.events.insert({
        'calendarId': SalesCalenderId,
        'resource': event
    });

    request.execute(function (resp) {
        if (resp.code != undefined) {
            console.log(JSON.stringify(resp));
            alert("儲存失敗!");
        } else {
            //var Id = resp.id;
            SalesCalendarEventUpdateGid(cid, resp.id);
        }
        console.log('Event created: ' + resp.htmlLink);
    });
}

function GoogleCalenderEventInsertAfterDelete(SalesCalenderId, eventId, event, dbData, cid) {
    var request = gapi.client.calendar.events.delete({
        'calendarId': SalesCalenderId,
        'eventId': eventId
    });

    request.execute(function (resp) {
        GoogleCalenderEventInsert(SalesCalenderId, event, dbData, cid);
    });
}

/*
* 修改日曆事件
*/
function GoogleCalenderEventUpdate(SalesCalenderId, eventId, paramEvent) {
    var request = gapi.client.calendar.events.update({
        'calendarId': SalesCalenderId,
        'eventId': eventId,
        'resource': paramEvent
    });

    request.execute(function (resp) {
        if (resp.code != undefined) {
            console.log("修改失敗!");
        } else {
            console.log("修改成功!");
        }
        console.log('Event created: ' + resp.htmlLink);
    });
}

/*
* 刪除日曆事件
*/
function GoogleCalenderEventDelete(SalesCalenderId, eventId, paramEvent) {
    var request = gapi.client.calendar.events.delete({
        'calendarId': SalesCalenderId,
        'eventId': eventId
    });

    request.execute(function (resp) {
        console.log('Event deleted: ' + resp);
    });
}

function GoogleCalenderEventSave(SalesCalenderId, eventId, dbData, cid) {
    var request = gapi.client.calendar.events.get({
        'calendarId': SalesCalenderId,
        'eventId': eventId
    });

    request.execute(function (resp) {
        if (resp.code != undefined) {
            //查無資料，儲存一筆日曆資料
            var calData = GoogleEventData(dbData);
            $.ajax({
                type: "POST",
                url: "SalesCalendarContent.aspx/CreateGoogleCalendar",
                data: JSON.stringify(calData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var event = response.d;
                    console.log(event);
                    GoogleCalenderEventInsert(SalesCalenderId, event, dbData, cid);
                }, error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        } else {
            //若有資料，先刪除再儲存
            if (resp.code == undefined) {
                GoogleCalenderEventDelete(SalesCalenderId, eventId);
            }
            var calData = GoogleEventData(dbData);
            $.ajax({
                type: "POST",
                url: "SalesCalendarContent.aspx/CreateGoogleCalendar",
                data: JSON.stringify(calData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var event = response.d;
                    GoogleCalenderEventInsertAfterDelete(SalesCalenderId, eventId, event, dbData, cid)
                }, error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });

        }
    });
}


function GoogleEventData(dbData) {
    var calData = {
        'content': dbData.content,
        'loc': dbData.location,
        'dateS': dbData.dateS,
        'dateE': dbData.dateE,
        'isWakeUp': dbData.isWakeUp,
        'minutes': dbData.wakeupMinutes,
        'cycle': dbData.wakeupCycle,
        'sellerName': dbData.sellerName,
        'contact': dbData.contact,
        'memo': dbData.memo,
        'interval': dbData.interval,
        'freq': dbData.freq,
        'week': dbData.week,
        'endson': dbData.endson,
        'endson_data': dbData.endson_data
    };
    return calData;
}

/*
* 儲存 Local 端資料
*/
function SalesCalendarEventDBSave(SalesCalenderId, Gid, dbData) {
    var nData = {
        'id': dbData.id,
        'gid': Gid,
        'content': dbData.content,
        'dateS': dbData.dateS,
        'dateE': dbData.dateE,
        'isWholeDay': dbData.isWholeDay,
        'isRepeat': dbData.isRepeat,
        'location': dbData.location,
        'sellerName': dbData.sellerName,
        'eventType': dbData.eventType,
        'contact': dbData.contact,
        'isWakeUp': dbData.isWakeUp,
        'wakeupMinutes': dbData.wakeupMinutes,
        'wakeupCycle': dbData.wakeupCycle,
        'memo': dbData.memo,
        'freq': dbData.freq,
        'interval': dbData.interval,
        'week': dbData.week,
        'endson': dbData.endson,
        'endson_data': dbData.endson_data,
        'UserName': dbData.UserName,
        'isSyncSellerNote': dbData.isSyncSellerNote,
        'DevelopStatus': dbData.DevelopStatus,
        'SellerGrade': dbData.SellerGrade,
        'SellerId': dbData.SellerId
    };
    var cid = 0;
    $.ajax({
        type: "POST",
        url: "SalesCalendarContent.aspx/SalesCalendarSave",
        data: JSON.stringify(nData),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            var calendar = response.d;

            cid = calendar.Id;
            if (calendar.EventDateS != undefined && calendar.EventDateE != undefined) {
                //若有填寫日期，將資料寫入Google Calendar
                GoogleCalenderEventSave(SalesCalenderId, calendar.Gid, dbData, cid);
            } else {
                alert("儲存成功!");
                location.href = "SalesCalendar.aspx";
            }
        }, error: function (xhr) {
            console.log(xhr.responseText);
        }
    });
    return cid;
}

function SalesCalendarEventUpdateGid(Id, Gid) {
    $.ajax({
        type: "POST",
        url: "SalesCalendarContent.aspx/SalesCalendarUpdateGid",
        data: "{'Id':'" + Id + "','Gid':'" + Gid + "' }",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            var res = response.d;

            alert("儲存成功!");
            location.href = "SalesCalendar.aspx";

        }, error: function (xhr) {
            console.log(xhr.responseText);
        }
    });
}


function changeFrame(obj) {
    var iframe = $('#gFrame');
    var span = $("#spanCalendarList input[type='radio']:checked");
    var eventCal = "";
    $.each(span, function (idx, val) {
        eventCal += "src=" + $(val).val().replace("&","&amp;") + "&amp;";
    });
    console.log(eventCal);
    var url = 'https://calendar.google.com/calendar/embed?' + eventCal + 'ctz=Asia%2FTaipei';
    iframe.attr('src', url);
}

function loadFrame(cid) {
    var iframe = $('#gFrame');
    var url = 'https://calendar.google.com/calendar/embed?src=' + cid + '&ctz=Asia/Taipei';
    iframe.attr('src', url);
}

function urlencode(txt) {
    var encodetxt = encodeURIComponent(txt);
    return encodetxt;
}



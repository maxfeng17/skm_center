jQuery.fn.startScroll = function () {
    var $strip = jQuery(this);
    var stripWidth = 0;
    stripWidth += jQuery(this, i).width();

    var offset = jQuery(this).offset();

    var residualSpace = offset.left + stripWidth;

    var residualTime = residualSpace / 0.2;
    $strip.stop();
    function scrollnews(spazio, tempo) {
        var containerWidth = $strip.find("li:first").width() * 3;	//a.k.a. 'mask' width 	
        var totalTravel = stripWidth + containerWidth;
        var defTiming = stripWidth / 0.02;
        $strip.animate({ left: '-=' + spazio }, tempo, "linear", function () { $strip.css("left", containerWidth); scrollnews(totalTravel, defTiming); });
    }

    scrollnews(residualSpace, residualTime);
};

jQuery.fn.liScroll = function (settings) {

		settings = jQuery.extend({

		travelocity: 0.03

		}, settings);		

		return this.each(function(){

				var $strip = jQuery(this);

				$strip.addClass("newsticker")

				var stripWidth = 0;

				var $mask = $strip.wrap("<div class='mask'></div>");

				var $tickercontainer = $strip.parent().wrap("<div class='tickercontainer borderbox'></div>");

				var containerWidth = $strip.find("li:first").width() * 3;	//a.k.a. 'mask' width 	

				$strip.find("li").each(function(i){

				stripWidth += jQuery(this, i).width();

				});

				$strip.width(stripWidth);			

				var defTiming = stripWidth / settings.travelocity;

				var totalTravel = stripWidth+containerWidth;								

				function scrollnews(spazio, tempo){

				$strip.animate({left: '-='+ spazio}, tempo, "linear", function(){$strip.css("left", containerWidth); scrollnews(totalTravel, defTiming);});

				}

				scrollnews(totalTravel, defTiming);				

				$strip.hover(function(){

				jQuery(this).stop();

				},

				function(){

				var offset = jQuery(this).offset();

				var residualSpace = offset.left + stripWidth;

				var residualTime = residualSpace/settings.travelocity;

				scrollnews(residualSpace, residualTime);

				});			

		});	

};
﻿function pad(s, l) {
    var data = String(s);
    var result = data;
    for (i = data.length; i < l; i++) { result = '0' + result; }
    return result;
}

function countdown(contentId, hr, mn, sc, hrstr, mnstr, sstr, y, m, d) {

    yy = y; mm = m; ddd = d; hour = hr; minute = mn; sec = sc; hourstr = hrstr; minstr = mnstr; secstr = sstr;
    var el = $('#' + contentId);
    var today = new Date();
    var comparison = new Date(yy, mm, ddd, hour, minute, sec, 0);
    comparison.setHours(hour, minute, sec, 0);

    dd = comparison - today;
    dday = Math.floor((dd / (60 * 60 * 1000 * 24)))
    dhour = Math.floor((dd % (60 * 60 * 1000 * 24)) / (60 * 60 * 1000) * 1) + dday * 24
    dmin = Math.floor(((dd % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) / (60 * 1000) * 1)
    dsec = Math.floor((((dd % (60 * 60 * 1000 * 24)) % (60 * 60 * 1000)) % (60 * 1000)) / 1000 * 1)

    var tt = "";

    if (dsec < 0)
        dsec = 0;
    tt += pad(dsec, 2) + ' ' + secstr;
    if (dmin > 0 || dhour > 0)
        tt = pad(dmin, 2) + ' ' + minstr + ' ' + tt;
    if (dhour > 0)
        tt = pad(dhour, 2) + ' ' + hourstr + ' ' + tt;

    el.html(tt);

    if (dhour >= 0 || dmin >= 0 || dsec >= 0) {
        setTimeout("countdown('" + contentId + "',hour, minute,sec, hourstr, minstr, secstr,yy,mm,ddd)", 1000)
    }
    return;
}

DealCountdown = function () {
    var self = this;
    var target;
    this.exec = function (triggerNext) {
        $(target).each(function () {
            if ($(this).data('end') == 0 || $(this).data('start') == 0 || $(this).data('countdown') == '0') {
                return;
            }


            var curMilsecs = new Date().getTime();
            var EveryDayNewDeal = $(this).data('everydaynewdeal');
            var endMilsecs = null;
            if (EveryDayNewDeal == "True")
            {
                if (parseFloat($(this).data('everydaynewdeal-end')) > parseFloat($(this).data('end')))
                    endMilsecs = parseFloat($(this).data('end'));
                else
                {
                    endMilsecs = parseFloat($(this).data('everydaynewdeal-end'));

                    if (endMilsecs < curMilsecs)
                        endMilsecs = parseFloat($(this).data('end'));
                }  
            }
            else
            {
                endMilsecs = parseFloat($(this).data('end'));
            }

            
            var startMilsecs = parseFloat($(this).data('start'));
            var dd = Math.floor((endMilsecs - curMilsecs) / 86400000);
            var tmp = (endMilsecs - curMilsecs) % 86400000;
            var dh = Math.floor(tmp / 3600000);
            tmp = tmp % 3600000;
            var dm = Math.floor(tmp / 60000);
            tmp = tmp % 60000;
            var ds = Math.floor(tmp / 1000);

            self.render.call(this, dd, dh, dm, ds, endMilsecs, startMilsecs, curMilsecs);
        });
        if (triggerNext == true) {
            this.trigger();
        }
    };

    this.render = function (dd, dh, dm, ds, ed, st, cur) {
        //render
        var icon = $(this).find('.icon-clock-o');
        if (icon.size() == 0) {
            icon = $('<span></span>').addClass('icon-clock-o').appendTo(this);
        }
        var timerField = $(this).find('.TimerField');
        if (timerField.size() == 0) {
            timerField = $('<div></div>').addClass('TimerField').appendTo(this);
        }
        var timeDayCounter = timerField.find('.TimeDayCounter');
        if (timeDayCounter.size() == 0) {
            timeDayCounter = $('<div></div>').addClass('TimeDayCounter').appendTo(timerField);
        }
        var timeCounter = timerField.find('.TimeCounter');
        if (timeCounter.size() == 0) {
            timeCounter = $('<div></div>').addClass('TimeCounter').appendTo(timerField);
        }
        if (st > cur) {
            timerField.find('.TimeDayCounter').text('');
            timerField.find('.TimeCounter').text('即將開賣');
            return;
        }
        if (ed < cur) {
            timerField.find('.TimeDayCounter').text('');
            timerField.find('.TimeCounter').text('已結束販售');
            return;
        }
        if (dd >= 3) {
            timerField.find('.TimeDayCounter').text('');
            timerField.find('.TimeCounter').text('限時優惠中!');
            return;
        }
        if (timeCounter.find('.TimeConDigit').size() == 0) {
            //rebuild
            timeCounter.empty();
            $('<div></div>').addClass('TimeConDigit').addClass('hn').appendTo(timeCounter);
            $('<div></div>').addClass('TimeConUnit').addClass('hl').appendTo(timeCounter);
            $('<div></div>').addClass('TimeConDigit').addClass('mn').appendTo(timeCounter);
            $('<div></div>').addClass('TimeConUnit').addClass('ml').appendTo(timeCounter);
            $('<div></div>').addClass('TimeConDigit').addClass('sn').appendTo(timeCounter);
            $('<div></div>').addClass('TimeConUnit').addClass('sl').appendTo(timeCounter);
        }
        if (timerField.find('.TimeDayCounter').text() != dd.toString() + '天') {
            timerField.find('.TimeDayCounter').text(dd.toString() + '天');
        }
        if (timeCounter.find('.hn').text() != dh.toString()) {
            timeCounter.find('.hn').text(dh.toString());
        }
        if (timeCounter.find('.hl').text() != '時') {
            timeCounter.find('.hl').text('時');
        }
        if (timeCounter.find('.mn').text() != dm.toString()) {
            timeCounter.find('.mn').text(dm.toString());
        }
        if (timeCounter.find('.ml').text() != '分') {
            timeCounter.find('.ml').text('分');
        }
        if (timeCounter.find('.sn').text() != ds.toString()) {
            timeCounter.find('.sn').text(ds.toString());
        }
        if (timeCounter.find('.sl').text() != '秒') {
            timeCounter.find('.sl').text('秒');
        }
    };

    this.refresh = function () {
        if (target != null) {
            this.exec(false);
        }
    };

    this.trigger = function (targetElems, immediatelyRefresh) {
        if (targetElems != null && targetElems != '') {
            target = targetElems;
        }
        if (immediatelyRefresh) {
            //console.log('immediatelyRefresh');
            self.refresh();
        }
        window.setTimeout(function () {
            self.exec(true);
        }, 1000);
    };
};


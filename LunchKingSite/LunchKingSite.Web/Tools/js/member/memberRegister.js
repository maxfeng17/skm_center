﻿/*!
 * MemmberRegisterApi Library

   registerMobileMember:註冊手機會員
   sendMobileRegisterAuthCode:發送認證簡訊
   validateMobileRegisterCode:檢查行動認證碼，如果成功即建立MobileMember
   setMobileMemberPasswordByKey:從code取得key, 用key設定密碼
   loginMember:登入會員
 */

var mobileRegisterUrl = '/api/memberservice/RegisterMobileMember'; 
var mobileAuthCodeUrl = '/api/memberservice/SendMobileRegisterAuthCode';  
var mobileValidCodeUrl = '/api/memberservice/ValidateMobileRegisterCode';  
var mobileSetMemberPassworUrl = '/api/memberservice/SetMobileMemberPasswordByKey'; 
var mobileLoginUrl = '/WebService/sso/MemberService.asmx/ContactLogin';

var MemmberRegisterApi = (function ($) {

    var hostName='';
    function hostNameSet(name) {
        hostName = name;
    }

    var registerMobileMember = function (userId, mobile, userEmail) {
        if (hostName === '') { throw new Error('api 尚未設定host name!'); }
        var postData = {};
        postData.UserId = userId;
        postData.Mobile = mobile;
        postData.UserEmail = userEmail;
        var r = $.Deferred();
        $.ajax({
            type: "POST",
            url: hostName+mobileRegisterUrl,
            data: JSON.stringify(postData),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {
                if (res.Code === 1000) {
                    $.ajax({
                        type: "POST",
                        url: hostName+mobileAuthCodeUrl,
                        data: JSON.stringify({ 'userId': userId, 'mobile': mobile }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (res) {
                            r.resolve(res);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("伺服器忙碌中，請重新操作一次。");
                        }
                    });


                } else {
                    r.resolve(res);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("伺服器忙碌中，請重新操作一次。");
            }
        });
        return r;
    }

    var sendMobileRegisterAuthCode = function (userId, mobile) {
        if (hostName === '') { throw new Error('api 尚未設定host name!'); }
        var r = $.Deferred();
        $.ajax({
            type: "POST",
            url: hostName+mobileAuthCodeUrl,
            data: JSON.stringify({ 'userId': userId, 'mobile': mobile }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {
                r.resolve(res);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("伺服器忙碌中，請重新操作一次。");
            }
        });
        return r;
    }

    var validateMobileRegisterCode = function (userId, mobile, code) {
        if (hostName === '') { throw new Error('api 尚未設定host name!'); }
        var r = $.Deferred();
        $.ajax({
            type: "POST",
            url: hostName+mobileValidCodeUrl,
            data: JSON.stringify({ 'userId': userId, 'mobile': mobile, 'code': code }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {
                r.resolve(res);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("伺服器忙碌中，請重新操作一次。");
            }
        });
        return r;

    }

    var setMobileMemberPasswordByKey = function (userId, mobile, resetPasswordKey, password) {
        if (hostName === '') { throw new Error('api 尚未設定host name!'); }
        var r = $.Deferred();
        $.ajax({
            type: "POST",
            url: hostName+mobileSetMemberPassworUrl,
            data: JSON.stringify({ 'userId': userId, 'mobile': mobile, 'resetPasswordKey': resetPasswordKey, 'password': password }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {
                r.resolve(res);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("伺服器忙碌中，請重新操作一次。");
            }
        });
        return r;
    }

    var loginMember = function (userName, password, userId, driverId) {
        if (hostName === '') { throw new Error('api 尚未設定host name!'); }
        var r = $.Deferred();
        $.ajax({
            url: hostName+mobileLoginUrl,
            type: 'post',
            crossDomain: true,
            data: {'userName': userName, 'password': password, 'userId': userId, 'driverId': driverId },
            contentType: "application/x-www-form-urlencoded",
            dataType: "json",
            success: function (res) {
                r.resolve(res);
            },
            error: function (res) {
                alert("伺服器忙碌中，請重新操作一次。");
            }
        });

        return r;
    }

    return {
        RegisterMobileMember: registerMobileMember,
        SendMobileRegisterAuthCode: sendMobileRegisterAuthCode,
        ValidateMobileRegisterCode: validateMobileRegisterCode,
        SetMobileMemberPasswordByKey: setMobileMemberPasswordByKey,
        LoginMember: loginMember,
        HostNameSet:hostNameSet
    }
}(jQuery));

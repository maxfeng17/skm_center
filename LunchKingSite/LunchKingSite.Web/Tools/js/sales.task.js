﻿var Sales = Sales || {};
var SCOPES = ["https://www.googleapis.com/auth/tasks"];

Sales = {
    version: "1.0.0",
    createDate: "2016/03/23",
    dependencies: {
        jQuery: "1.9.1"
    }
}

Sales.Page = {
    Init: (function () {
    }),
};

Sales.Task = {
    GetList: (function () {
        TaskGet();
    }),
    Insert: (function (TaskListId, Title, Notes, dueDate) {
        TaskInsert(TaskListId, Title, Notes, dueDate);
    })
};


//*---------------------------task method---------------------------*//

/*
* 檢查是否已取得使用者授權
*/
function checkAuth() {
    gapi.auth.authorize(
      {
          'client_id': CLIENT_ID,
          'scope': SCOPES.join(' '),
          'immediate': true
      }, handleAuthResult);
}

function handleAuthResult(authResult) {
    var authorizeDiv = document.getElementById('authorize-div');
    if (authResult && !authResult.error) {
        $("#authorize-div").hide();
        $("#cal-content").show();
        loadTasksApi();
    } else {
        $("#authorize-div").show();
        $("#cal-content").hide();
    }
}
/*
* 請求認證
*/
function handleAuthClick(event) {
    gapi.auth.authorize(
      { client_id: CLIENT_ID, scope: SCOPES, immediate: false },
      handleAuthResult);
    return false;
}

function loadTasksApi() {
    gapi.client.load('tasks', 'v1', TaskListsGet);
}

/**
 * 列出工作列表
 */
function TaskListsGet() {
    var request = gapi.client.tasks.tasklists.list({
        'maxResults': 10
    });

    request.execute(function (resp) {
        var taskLists = resp.items;
        if (taskLists && taskLists.length > 0) {
            for (var i = 0; i < taskLists.length; i++) {
                var taskList = taskLists[i];
                $("#SalesTaskListId").val(taskList.id);
                return false;
                //console.log(taskList.title + ' (' + taskList.id + ')');
            }
        } else {
            console.log('No task lists found.');
        }
    });
}

function TasksGet(TaskListId) {
    var request = gapi.client.tasks.tasks.get({
        'tasklist': TaskListId,
        'task': ''
    });

    request.execute(function (resp) {
        var taskLists = resp.items;
        if (taskLists && taskLists.length > 0) {
            for (var i = 0; i < taskLists.length; i++) {
                var taskList = taskLists[i];
                console.log(taskList.title + ' (' + taskList.id + ')');
            }
        } else {
            console.log('No task lists found.');
        }
    });
}

function TaskInsert(TaskListId, Title, Notes, dueDate) {
    var request;

    if (dueDate != "") {
        request = gapi.client.tasks.tasks.insert({
            'tasklist': TaskListId,
            'title': Title,
            'due': dueDate,
            'notes': Notes
        });
    } else {
        request = gapi.client.tasks.tasks.insert({
            'tasklist': TaskListId,
            'title': Title,
            'notes': Notes
        });
    }
    

    request.execute(function (resp) {
        if (resp.code != undefined) {
            alert("新增失敗!");
        } else {
            var Id = resp.id;
            alert("新增成功!");
        }
    });
}
(function () {

    var keys = [37, 38, 39, 40];

    function preventDefault(e) {
        e = e || window.event;
        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    }

    function keydown(e) {
        for (var i = keys.length; i--;) {
            if (e.keyCode === keys[i]) {
                preventDefault(e);
                return;
            }
        }
    }

    function wheel(e) {
        preventDefault(e);
    }

    function disable_scroll() {
        if (window.addEventListener) {
            window.addEventListener("mousewheel", wheel, false);
            window.addEventListener('DOMMouseScroll', wheel, false);
            if (typeof window.ontouchmove !== 'undefined') {
                window.addEventListener("touchmove", wheel, false);
            }
        } if (window.attachEvent) {
            window.attachEvent("onmousewheel", wheel);
            if (typeof window.ontouchmove !== 'undefined') {
                window.addEventListener("touchmove", wheel, false);
            }
        }

        window.onmousewheel = document.onmousewheel = wheel;
        document.onkeydown = keydown;
    }

    function enable_scroll() {
        if (window.removeEventListener) {
            window.removeEventListener("mousewheel", wheel, false);
            window.removeEventListener('DOMMouseScroll', wheel, false);
            if (typeof window.ontouchmove !== 'undefined') {
                window.removeEventListener("touchmove", wheel, false);
            }
        } else if (window.detachEvent) {
            window.detachEvent("onmousewheel", wheel);
            if (typeof window.ontouchmove !== 'undefined') {
                window.detachEvent("touchmove", wheel);
            }
        }
        window.onmousewheel = document.onmousewheel = document.onkeydown = null;
    }



    var triggerBttn = document.getElementById('trigger-overlay'),
		overlay = document.querySelector('div.overlay'),
		closeBttn = overlay.querySelector('button.overlay-close');
		transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		},
    transEndEventName = transEndEventNames[Modernizr.prefixed('transition')],
    support = { transitions: Modernizr.csstransitions };

	function toggleOverlay() {
        if (classie.has(overlay, 'open')) {
            classie.remove(overlay, 'open');
            classie.add(overlay, 'close');
            var onEndTransitionFn = function (ev) {
                if (support.transitions) {
				    if (ev.propertyName !== 'visibility') return;
				    if (this.removeEventListener) {
                        this.removeEventListener(transEndEventName, onEndTransitionFn);
				    } else if (this.detachEvent) {
				        this.detachEvent(transEndEventName, onEndTransitionFn);
				    }
				    
				}
                classie.remove(overlay, 'close');
			};
			if (support.transitions) {
			    if (overlay.addEventListener) {
                    overlay.addEventListener(transEndEventName, onEndTransitionFn);
			    } else if (overlay.attachEvent) {
			        overlay.attachEvent(transEndEventName, onEndTransitionFn);
			    }
			} else {
				onEndTransitionFn();
			}
			enable_scroll();
            $(window).scrollTop(0);
		}
		else if (!classie.has(overlay, 'close')) {
		    classie.add(overlay, 'open');
            //var scroll_top = $(window).scrollTop();
            var scroll_bottom = $(window).scrollTop() + $(window).height();
            var ul_height = $(overlay).find('ul').height();

            if (scroll_bottom >= (ul_height)) {
		    disable_scroll();
		}
	}
    }
    //ie 8 not addEventListener
    if (triggerBttn.addEventListener) {
        triggerBttn.addEventListener('click', toggleOverlay);
    } else if (triggerBttn.attachEvent) {
        triggerBttn.attachEvent("onclick", toggleOverlay);
    }
    if (closeBttn.addEventListener) {
        closeBttn.addEventListener('click', toggleOverlay);
    } else if (closeBttn.attachEvent) {
        closeBttn.attachEvent("onclick", toggleOverlay);
    }

})();
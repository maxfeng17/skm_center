﻿/*
作者：hsu po-wei
E-Mail：hpw925@hotmail.com
授權：GPL 3

-- 2017/05/11 因table筆數太多會爆版，修正為使用div
*/

(function ($) {
    $.fn.tablepage = function (oObj, dCountOfPage) {
        var dPageIndex = 1;
        var dNowIndex = 1;
        var sPageStr = "";
        var dCount = 0;
        var oSource = $(this);
        var sNoSelColor = "#fff";
        var sNoSelFontColor = "#666";
        var sSelColor = "#428bca";
        var sSelFontColor = "#fff";
        var sBorderColor = "#eee";

        change_page_content();

        function change_page_content() {
            //取得資料筆數
            dCount = oSource.children().children().length - 1;
            dPageIndex = 1;

            //顯示頁碼
            sPageStr = "<table style='max-width:850px;'><tr><td style='height:30px;'><b>第</b></td><td valign='top'>";
            for (var i = 1; i <= dCount; i += dCountOfPage) {
                if (dNowIndex == dPageIndex) {
                    sPageStr += "<div style='width:30px;height:30px;text-align:center;padding-top:5px;cursor:pointer;color:" + sSelFontColor + ";border-collapse:collapse;border-style:solid;border-width:1px;border-color:" + sSelColor + ";background-color:" + sSelColor + ";float:left;'>" + (dPageIndex++) + "</div>";
                }
                else {
                    sPageStr += "<div style='width:30px;height:30px;text-align:center;padding-top:5px;cursor:pointer;color:" + sNoSelFontColor + ";border-collapse:collapse;border-style:solid;border-width:1px;border-color:" + sBorderColor + ";background-color:" + sNoSelColor + ";float:left;'>" + (dPageIndex++) + "</div>";
                }
            }
            sPageStr += "</td><td style='height:30px;'><b>頁</b></td></tr></table>";

            oObj.html(sPageStr);
            dPageIndex = 1;

            //過濾表格內容
            oSource.children().children("tr").each(function () {
                if (dPageIndex <= (((dNowIndex - 1) * dCountOfPage) + 1) || dPageIndex > ((dNowIndex * dCountOfPage) + 1)) {
                    $(this).hide();
                }
                else {
                    $(this).show();
                }
                dPageIndex++;
            });
            oSource.children().children("tr").first().show(); //head一定要顯示

            //加入換頁事件
            oObj.children().children().children().children().each(function () {
                $(this).on('click', 'div', function () {
                    dNowIndex = $(this).text();
                    if (dNowIndex > 0) {
                        change_page_content();
                    }
                });
            });
        }
    };
})(jQuery);
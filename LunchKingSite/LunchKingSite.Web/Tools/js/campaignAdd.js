﻿$(document).ready(function () {

    var value = '';
    $('#hidChannelId').val(value);
    $('.datepicker').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy/mm/dd',
        stepMinute: 1
    });

    $('input[type="radio"]').change(function () {
        var tmpId = $(this).attr('id');
        if (tmpId == 'radioAll' ||
            tmpId == 'radiochannel' ||
            tmpId == 'radioBrand' ||
            tmpId == 'radioDetail' ||
            tmpId == 'radioEvent') {
            $('#channelPart').hide();
            $('#channelBrand').hide();
            $('#channelDetail').hide();
            $('#channelEvent').hide();
            if (tmpId == 'radioAll' || tmpId == 'radiochannel') {
                addRow();
                if (tmpId == 'radiochannel') {
                    $('#channelPart').show();
                }
            } else {
                removeRow();
                if (tmpId == 'radioBrand') {
                    $('#channelBrand').show();
                } else if (tmpId == 'radioDetail') {
                    $('#channelDetail').show();
                } else if (tmpId == 'radioEvent') {
                    $('#channelEvent').show();
                }
            }
        } else {
            if (tmpId == 'radioGiftDetail') {
                $('#discountGiftDetail').show();
            } else if (tmpId == 'radioGiftNone') {
                $('#discountGiftDetail').hide();
                $('#FreeGiftDiscount').val('');
                $('#FreeGiftSDate').val('');
                $('#FreeGiftEDate').val('');
            } else if (tmpId == 'radioGiftAuto') {
                $('#discountGiftDetail').hide();
                $('#discountGiftDetail :input').val("");
            } else if (tmpId == 'discountUnified') {
                $('#discountTypeDetail').show();
                $('#discountGiftDetail').hide();
                $('#reuseRow').show();
                if (($('#radioGiftAuto').is(':checked')) || ($('#radioGiftDetail').is(':checked'))) {
                    $('#radioGiftAuto').prop("checked", false);
                    $('#radioGiftNone').prop("checked", true);
                }
                if ($('#UseOnlySelf').is(':checked')) {
                    $('#UseOnlySelf').prop("checked", false);
                }
                $('#radioGiftAuto').prop('disabled', true);
                $('#radioGiftDetail').prop('disabled', true);
                $('#UseOnlySelf').prop('disabled', true);
            } else if ($(this).attr('id') == 'discountImmediate' || $(this).attr('id') == 'discountPre') {
                $('#discountTypeDetail').hide();
                $('#reuseRow').hide();
                $('#CanReused').prop("checked", false);
                $('#UseOnlySelf').prop('disabled', false);
                $('#radioGiftAuto').prop('disabled', false);
                $('#radioGiftDetail').prop('disabled', false);
            }
        }
        var GiftDetailType = $('#radioGiftDetail').is(':checked');
        $('#FreeGiftSDate').attr('required', GiftDetailType);
        $('#FreeGiftEDate').attr('required', GiftDetailType);

    });

    $('#addForm').submit(function () {
        var value = '';
        if (document.getElementById('radiochannel').checked) {
            $('#channelModal input[type="checkbox"]:checked').each(function () {
                if ($(this).hasClass('subList')) {
                    value = value + $(this).parent().parent().parent().children(0).val() + '|' + $(this).val() + ',';
                }
                else {
                    var checkedInput = $(this).parent().find('input.subList').filter(':checked').length;
                    if (checkedInput == 0) {
                        value = value + $(this).val() + '|' + $(this).val() + ',';
                    }
                }
                $('#hidChannelId').val(value);
            });
        }
        else if (document.getElementById('radioBrand').checked) {
            var rows = document.getElementById('addBrandTable').getElementsByTagName('tr');
            for (var i = 1, length = rows.length; i < length; i++) {
                var rowValue = rows[i].getElementsByTagName('td')[0].innerText;
                value = value + rowValue + ',';
            }
            $('#hidBrandId').val(value);
        }
        else if (document.getElementById('radioDetail').checked) {
            var rows = document.getElementById('addBIDTable').getElementsByTagName('tr');
            for (var i = 1, length = rows.length; i < length; i++) {
                var rowValue = rows[i].getElementsByTagName('td')[0].innerText;
                value = value + rowValue + ',';
            }
            $('#hidEventId').val($('#hidDetailId').val());
        }
        else if (document.getElementById('radioEvent').checked) {
            var rows = document.getElementById('addEventTable').getElementsByTagName('tr');
            for (var i = 1, length = rows.length; i < length; i++) {
                var rowValue = rows[i].getElementsByTagName('td')[0].innerText;
                value = value + rowValue + ',';
            }
            $('#hidEventId').val(value);
        }
        var startDate = $('#txtStartDate').val();
        var endDate = $('#txtEndDate').val();
        startDate = startDate + ' ' + $('#ssh').val() + ':00';
        $('#htxtStartDate').val(startDate)
        endDate = endDate + ' ' + $('#seh').val() + ':00';
        $('#htxtEndDate').val(endDate)
        var startDate2 = $('#FreeGiftSDate').val();
        var endDate2 = $('#FreeGiftEDate').val();
        startDate2 = startDate2 + ' ' + $('#fsh').val() + ':00';
        $('#hFreeGiftSDate').val(startDate2)
        endDate2 = endDate2 + ' ' + $('#feh').val() + ':00';
        $('#hFreeGiftEDate').val(endDate2)
    });
});

function addRow() {
    $('#grossMarginRow').show();
    $("#grossMargin").prop('required', true);
}
function removeRow() {
    $('#grossMarginRow').hide();
    $('#grossMargin').removeAttr('required');
    $('#grossMargin').val('');
}

function selectChannel(obj) {
    if ($(obj).is(':checked')) {
        $(obj).parent().children('.ulArea').show();
        $(obj).parent().children('.ulCategory').show();
        $('[id*=cbx_BindEventPromoId]').attr('disabled', 'disabled');
    } else {
        $(obj).parent().children('.ulArea').hide();
        $(obj).parent().children('.ulCategory').hide();
        $(obj).parent().children('.ulArea input[type=checkbox]').each(function () {
            $(this).next('label').css('color', 'black');
            $(this).removeAttr('disabled');
            $(this).attr('checked', false);
        });
        $(obj).parent().children('.ulCategory input[type=checkbox]').each(function () {
            $(this).next('label').css('color', 'black');
            $(this).removeAttr('disabled');
            $(this).attr('checked', false);
        });
        $('[id*=cbx_BindEventPromoId]').removeAttr('disabled');
    }
}
function selectArea(obj) {
    if ($(obj).parent().parent('.ulArea').find('input[type=checkbox]:checked').length > 0) {
        $(obj).parent().parent().parent().find('.ulCategory input[type=checkbox]').each(function () {
            $(this).attr('checked', false);
            $(this).attr('disabled', 'disabled');
            $(this).next('label').css('color', 'gray');
        });
    } else {
        $(obj).parent().parent().parent().find('.ulCategory input[type=checkbox]').each(function () {
            $(this).removeAttr('disabled');
            $(this).next('label').css('color', 'black');
        });
    }
}
function selectCategory(obj) {
    if ($(obj).parent().parent('.ulCategory').find('input[type=checkbox]:checked').length > 0) {
        $(obj).parent().parent().parent().find('.ulArea input[type=checkbox]').each(function () {
            $(this).attr('checked', false);
            $(this).attr('disabled', 'disabled');
            $(this).next('label').css('color', 'gray');
        });
    } else {
        $(obj).parent().parent().parent().find('.ulArea input[type=checkbox]').each(function () {
            $(this).removeAttr('disabled');
            $(this).next('label').css('color', 'black');
        });
    }
}

function setChannel() {
    var value = '';
    $('#addForm input[type="checkbox"]:checked').each(function () {
        value = value + $(this).next().html() + '　';
    });
    var showChannel = document.getElementById('setChannel');
    showChannel.innerHTML = '已選頻道範圍：<br />' + value;
}

function GetBrandData() {
    var id = $("#brandID").val();
    $.ajax({
        async: false,
        type: "POST",
        url: '/ControlRoom/DiscountCode/GetBrandId',
        data: { id: id },
        dataType: "json",
        success: setBrandID,
        error: function (jqXHR, textStatus, errorThrown) {
            alert("伺服器忙碌中，請重新操作一次。");
        }
    });
}
function setBrandID(data) {
    var rows = document.getElementById('addBrandTable').getElementsByTagName('tr');
    for (var i = 1, length = rows.length; i < length; i++) {
        var rowId = rows[i].getElementsByTagName('td')[0].innerText;
        if (parseInt(data.EventId) == parseInt(rowId)) {
            data.EventId = 'ID重複';
            data.Msg = 'ID重複';
        }
    }
    if (parseInt(data.EventId)) {
        $('#addBrandTable').show();
        var tableRef = document.getElementById('addBrandTable').getElementsByTagName('tbody')[0];
        var insertTr = tableRef.insertRow(tableRef.rows.length);
        var insertTd = insertTr.insertCell(0);
        insertTd.style.textAlign = "center";
        insertTd.innerHTML = data.EventId;
        insertTd = insertTr.insertCell(1);
        insertTd.innerHTML = data.EventName;
        insertTd = insertTr.insertCell(2);
        insertTd.innerHTML = "<a onclick='removeBrandID($(this))' href='javascript:void(0);'>移除</a>";
    }
    else {
        alert(data.Msg);
    }
}
function removeBrandID(id) {
    var row = id.parent().parent().index();
    document.getElementById("addBrandTable").getElementsByTagName('tbody')[0].deleteRow(row);
    var num = document.getElementById("addBrandTable").getElementsByTagName('tbody')[0].rows.length;
    if (num == 0) {
        $('#addBrandTable').hide();
    }
}

function GetEventData() {
    var id = $("#eventID").val();
    $.ajax({
        async: false,
        type: "POST",
        url: '/ControlRoom/DiscountCode/GetEventPromoId',
        data: { id: id },
        dataType: "json",
        success: setEventID,
        error: function (jqXHR, textStatus, errorThrown) {
            alert("伺服器忙碌中，請重新操作一次。");
        }
    });
}
function setEventID(data) {
    var rows = document.getElementById('addEventTable').getElementsByTagName('tr');
    for (var i = 1, length = rows.length; i < length; i++) {
        var rowId = rows[i].getElementsByTagName('td')[0].innerText;
        if (parseInt(data.EventId) == parseInt(rowId)) {
            data.EventId = 'ID重複';
            data.Msg = 'ID重複';
        }
    }
    if (parseInt(data.EventId)) {
        $('#addEventTable').show();
        var tableRef = document.getElementById('addEventTable').getElementsByTagName('tbody')[0];
        var insertTr = tableRef.insertRow(tableRef.rows.length);
        var insertTd = insertTr.insertCell(0);
        insertTd.style.textAlign = "center";
        insertTd.innerHTML = data.EventId;
        insertTd = insertTr.insertCell(1);
        insertTd.innerHTML = data.EventName;
        insertTd = insertTr.insertCell(2);
        insertTd.innerHTML = "<a onclick='removeEventID($(this))' href='javascript:void(0);'>移除</a>";
    }
    else {
        alert(data.Msg);
    }
}
function removeEventID(id) {
    var row = id.parent().parent().index();
    document.getElementById("addEventTable").getElementsByTagName('tbody')[0].deleteRow(row);
    var num = document.getElementById("addEventTable").getElementsByTagName('tbody')[0].rows.length;
    if (num == 0) {
        $('#addEventTable').hide();
    }
}

function GetPromoData() {
    var id = $("#detailBID").val();
    var eventId = $('#hidDetailId').val();
    $.ajax({
        async: false,
        type: "POST",
        url: '/ControlRoom/DiscountCode/GetPromoIdByBid',
        data: { bid: id, evenId: eventId },
        dataType: "json",
        success: setBID,
        error: function (jqXHR, textStatus, errorThrown) {
            alert("伺服器忙碌中，請重新操作一次。");
        }
    });
}
function setBID(data) {
    var rows = document.getElementById('saveBIDTable').getElementsByTagName('tr');
    for (var i = 0, length = rows.length; i < length; i++) {
        var rowId = rows[i].getElementsByTagName('td')[0].innerText;
        if ($('#detailBID').val() == rowId) {
            data.UniqueId = 'ID重複';
            data.Msg = 'ID重複';
        }
    }
    if (parseInt(data.UniqueId)) {
        $('#addBIDTable').show();
        var tableRef = document.getElementById('addBIDTable').getElementsByTagName('tbody')[0];
        var insertTr = tableRef.insertRow(tableRef.rows.length);
        var insertTd = insertTr.insertCell(0);
        insertTd.style.textAlign = "center";
        insertTd.innerHTML = data.UniqueId;
        insertTd = insertTr.insertCell(1);
        insertTd.innerHTML = data.EventName;
        insertTd = insertTr.insertCell(2);
        insertTd.innerHTML = data.GrossMargin * 100 + '%';
        insertTd = insertTr.insertCell(3);
        insertTd.style.textAlign = "center";
        insertTd.innerHTML = "<a onclick='removeBID($(this))' href='javascript:void(0);'>移除</a>";
        insertTd = insertTr.insertCell(4);
        insertTd.style.display = 'none';
        insertTd.innerHTML = data.EventId;

        var tableRef2 = document.getElementById('saveBIDTable');
        var insertTr2 = tableRef2.insertRow(tableRef2.rows.length);
        var insertTd2 = insertTr2.insertCell(0);
        insertTd2.innerHTML = $('#detailBID').val();
        insertTd2 = insertTr2.insertCell(1);
        insertTd2.innerHTML = data.EventId;
        $('#hidDetailId').val(data.EventId);
    }
    else {
        alert(data.Msg);
    }
}

function removeBID(id) {
    var row = id.parent().parent().index();
    var uniqueId = id.parent().parent().find('td').first().html();
    var eventId = id.parent().parent().find('td').last().html();
    document.getElementById("addBIDTable").getElementsByTagName('tbody')[0].deleteRow(row);
    document.getElementById("saveBIDTable").getElementsByTagName('tbody')[0].deleteRow(row);
    var num = document.getElementById("addBIDTable").getElementsByTagName('tbody')[0].rows.length;
    if (num == 0) {
        $('#addBIDTable').hide();
        $('#hidDetailId').val('');
    }
    $.ajax({
        async: false,
        type: "POST",
        url: '/ControlRoom/DiscountCode/DelPromoItem',
        data: { uniqueId: uniqueId, evenId: eventId },
        dataType: "json",
        success: function (ref) {

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("伺服器忙碌中，請重新操作一次。");
        }
    });
}
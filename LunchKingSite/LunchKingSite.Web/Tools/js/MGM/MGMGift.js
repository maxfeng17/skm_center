﻿
$(document).ready(function () {
    $('.giftinfo_btn').hover(function () {
        GetMenuGiftBox(1);
        $('.funtion-menu-block .giftinfo_btn').addClass('giftinfo_btn_on');
    }, function () {
        $('.funtion-menu-block .giftinfo_btn').removeClass('giftinfo_btn_on');
        $('.popup_bg').css('display', 'none');
    });

    $(function () {
        function GOGO() {
            $(".right-menu li.gift").animate({ left: '-1px' }, 100).animate({ left: '-3px' }, 100);

            TT = setTimeout(GOGO, 200);
        }

        if ($(".right-menu li.gift").length == 1) {
            GOGO();
        }

        $(".right-menu li.gift").hover(function () {
            clearTimeout(TT); //清除 TT 計時器
        }, function () {
            TT = setTimeout(GOGO, 200); //重新啟動 TT 計時器
        });
    });

    $(function () {
        $(".right-menu li.gift").hover(
          function () {
              $(".right-menu li a.gift-cash").stop(true, true).fadeTo(400, 1);
          },
        function () {
            $(".right-menu li a.gift-cash").stop(true, true).fadeTo(400, 0);
        }
        );
    });

});

function postData(oid, count, dealName) {

    var form = $("<form method='post'></form>");
    $(document.body).append(form);
    form.attr({ "action": "/User/ChooesFriend" });
    var input = $("<input type='hidden'>");
    input.attr({ "name": "oid" });
    input.val(oid);
    form.append(input);
    input = $("<input type='hidden'>");
    input.attr({ "name": "count" });
    input.val(count);
    form.append(input);
    input = $("<input type='hidden'>");
    input.attr({ "name": "dealName" });
    input.val(dealName);
    form.append(input);
    $(document.body).append(form);
    form.submit();

}

function GiftBoxClick(pageType) {
    GetMenuGiftBox(pageType);
    $("#tabsGift>div").hide();
    $("#menuTabs-" + pageType).css("display", "");
    $('#tabsGift ul li').removeClass('ui-state-active');
    $('#tabsGift ul li').eq(pageType - 1).addClass('ui-state-active');
}

function GetMenuGiftBox(pageType) {

    if ($("#menuTabs-" + pageType + " .gift_itme_wrap .gift_itme").length == 1) {
        $.ajax({
            type: "POST",
            url: "/User/GetMenuGiftList",
            data: "{'pageType': '" + pageType + "' }",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            error: function (jqXHR, textStatus) {
                location.reload();
            },
            success: function (msg) {

                if (msg != null) {

                    if (pageType == 1) {
                        for (var key in msg) {
                            var temp = $('#menuTabs-1 .Template').clone();
                            temp.removeClass('Template').show();
                            temp.find('.sendUrl').attr('href', "javascript:postData('" + msg[key].OrderId + "','" + msg[key].PponCount + "','" + msg[key].PponTitle + "')");
                            temp.find('.dealPic').attr('src', msg[key].PponImg);
                            temp.find('.couponCount').html('可送數 : ' + msg[key].PponCount);
                            temp.find('.dealTitle').html(msg[key].PponTitle);
                            temp.find('.dealContent').html(msg[key].PponContent);
                            $('#menuTabs-1 .gift_itme_wrap').append(temp);
                        }
                    }
                    if (pageType == 2) {
                        for (var key in msg) {
                            var temp = $('#menuTabs-2 .Template').clone();
                            temp.removeClass('Template').show();
                            var SendDate = new Date(parseInt(msg[key].SendTime.replace("/Date(", "").replace(")/", "")));
                            temp.find('.dealPic').attr('src', msg[key].PponImg);
                            temp.find('.dealTitle').html(msg[key].PponTitle);
                            temp.find('.dealContent').html(msg[key].PponContent);
                            temp.find('.giftUrl').attr('href', '/User/GiftList?giftId=' + msg[key].GiftId);
                            temp.find('.info').html(msg[key].SenderName + '&nbsp;於' + SendDate.getDay() + '天前送你送您一份禮物');
                            temp.find('.countdown').html('<i class="fa fa-clock-o fa-1g"></i>&nbsp;' + SendDate.getDay() + '天' + SendDate.getHours() + '時&nbsp;內需領取');
                            $('#menuTabs-2 .gift_itme_wrap').append(temp);
                        }
                    }
                    if (pageType == 3) {
                        for (var key in msg) {
                            var temp = $('#menuTabs-3 .Template').clone();
                            temp.removeClass('Template').show();
                            var SendDate = new Date(msg[key].SendTime);
                            temp.find('.cardImg').attr('src', '/Themes/MGM/images/' + msg[key].ImageType);
                            temp.find('.senderName').html(msg[key].SenderName);
                            temp.find('.sendTime').html(SendDate.getFullYear() + '/' + SendDate.getMonth() + '/' + SendDate.getDate());
                            temp.find('.cardContent').html(msg[key].Message);
                            $('#menuTabs-3 .gift_itme_wrap').append(temp);
                        }
                    }
                }
            }
        });
    }

}

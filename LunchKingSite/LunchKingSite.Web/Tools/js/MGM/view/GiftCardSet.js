﻿; (function ($) {

    $(function () {
        $('input[name=MatchMembers]').attr('value', sessionStorage.matchMembers);
        $('input[name=OrderId]').attr('value', sessionStorage.oid);

        var arrayNames = [];
		var alerdyNames = [];
		var realCount = [];
        if (sessionStorage.getItem('matchMembers') != null && sessionStorage.getItem('matchMembers') != '') {
            var friends = JSON.parse(sessionStorage.matchMembers);
            $(friends).each(function () {
                if (!alerdyNames.includes(this.Name)) {
                    arrayNames.push(this.Name);
                    alerdyNames.push(this.Name);
				}
				realCount++;
            });

            $('#sender_member').text(arrayNames.join('、'));
			$('#member_count').text(realCount);
        }
       

        //跟屁蟲廣告
        $(window).bind('scroll resize', function () {
            var scroll_height = $(window).scrollTop();
            if (scroll_height > 320) {
                $('.outside_AD').addClass('OAD_fixed');
            } else {
                $('.outside_AD').removeClass('OAD_fixed');
            }
        }).scroll();// 觸發一次 scroll()


        /*點縮圖 換圖卡片大圖與背景顏色*/
        $(".card_img_m li").each(function (index) {
            $(this).on("click", function () {
                $(".card_img_m li a").siblings().attr('name', '');
                $('.card_style_left .pic').find('img').attr('src', $(this).find('img').attr('src'));
                $(this).addClass('on').siblings().removeClass('on');
                $(this).find('input[class="cardstyle"]').attr('name', 'CardStyleId');
                $('.card_left').css('background-color', $(this).find('input[class="bgcolor"]').val());
               
            });
        });

        /*點罐頭文字 自動輸入到textarea*/
        $(".card_msg_btn p").each(function (index) {
            $(this).on("click", function () {
                var msgX = document.getElementById("card_msg_text").value; //先前的內容
                document.getElementById("card_msg_text").value = msgX + $(this).children().val();
            });
        });

        /*改署名*/
        $('#nameself').keyup(function () {
            var selfname = document.getElementById("nameself").value;
            $('.member_name').text(selfname); //改署名

        });

        $('#btnSubimt').click(function () {
            if (sessionStorage.getItem('matchMembers') == null || sessionStorage.getItem('oid') == null) {
                alert('sorry,此網頁已失效了。');
                location.href = '/User/SendGift';
            }

            var matchObj = JSON.parse(sessionStorage.matchMembers);

            if (matchObj[0].Type !== 2) { //not fb
                $('#sendForm').submit();
            }
            else {
                var giftCard = {};
                giftCard.Message = $('#card_msg_text').val();
                giftCard.ReceiverId = '';
                giftCard.SenderId = '';
                giftCard.CardStyleId = $('input[name=CardStyleId]').val();
                giftCard.OrderId = $('input[name=OrderId]').val();
                giftCard.SenderName = $('#nameself').val();
                CommunityApi.Facebook.SendAppRequest(matchObj, giftCard, '領取:【' + sessionStorage.dealName + '】禮物!');
            }
        });



    });

}(jQuery))
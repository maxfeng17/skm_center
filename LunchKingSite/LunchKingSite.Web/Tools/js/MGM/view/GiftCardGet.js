﻿function openCard(asCode, msgId, img, title, content, bid, isRead) {

    $("#hidGiftAsCode").val(asCode);
    var temp = $("div#divCard").clone();
        $.ajax({
        type: "POST",
        url: "/User/GiftCardGet",
        data: JSON.stringify(
            {
                'msgId': msgId,
                'isRead': isRead
            }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            temp.find('.cardImg').attr("src", msg.CardData.TemplateCard.ImagePic);
            temp.find('.card_left').css('background-color', msg.CardData.TemplateCard.BackgroundColor);
            temp.find('.friend_name').html(msg.ReceiveName);
            temp.find('.card_word').html(msg.CardData.Message);
            temp.find('.member_name').html(msg.SendUserName);
            temp.find('.giftImg').attr("src", img);
            temp.find('.giftName').text(title);
            temp.find('.giftContent').text(content);
            temp.find('.pic').find('a').attr("href", "/" + bid);
            temp.find('.link').find('a').attr("href", "/" + bid);

            if (msg.CardData.ReadTime != null || asCode == '0' ) {
                temp.find('.action').hide();
            } else {
                temp.find('.action').show();
            }
       
            $.blockUI({
                message: temp,
                css: {
                    top: ($(window).height() - $(window).height() / 2) / 2 + 'px',
                    left: ($(window).width() - $(window).width() / 3) / 2 + 'px',
                    border: 'none',
                    cursor: 'default'
                },
                overlayCSS: { cursor: 'default' }
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("伺服器忙碌中，請重新操作一次。");
            location.reload();
        }
    });


}
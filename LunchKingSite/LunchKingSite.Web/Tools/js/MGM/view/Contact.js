﻿; (function ($) {

    /*popup 新增群組*/
    $(function () {

        $('.popup_new_group').on('click', function (e) {
            e.preventDefault();
            $('.new_group_wrap').bPopup();
            $('#previewGroup').attr('src', 'http://'+location.hostname+'/Images/MGM/contact/group_bg.png');
        });


        $("body").on("change", "#groupPic", function () {
            preview(this);
        });

        function preview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#previewGroup').attr('src', e.target.result);
                    //var KB = format_float(e.total / 1024, 2);
                    //$('.size').text("檔案大小：" + KB + " KB");
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        /*popup 更換群組照片*/
        $('.new_group_wrap .box .pic_gropu').hover(
            function () {
                $(this).find('a').css('display', 'block');
            }, function () {
                $(this).find('a').css('display', 'none');
            });



    });

    //右邊黏住視窗底部
    $(window).bind('scroll resize', function () {
        var scroll_height = $(window).scrollTop();
        var Left_width = $('#Left').width() + 20;
        var Left_height = $('#Left').height();
        var Rightarea_height = $('#Rightarea').height() + 10;
        console.log("Left_width=" + Left_width);
        if (scroll_height > Rightarea_height) {
            $('.LeftRight_wrap').css('position', 'relative');
            $('#Rightarea').css({
                'float': 'none',
                'margin-left': Left_width,
                'display': 'block',
                'position': 'fixed',
                'bottom': '0'
            })
        } else {
            $('#Rightarea').css({
                'position': 'relative',
                'margin-left': 0,
                'float': 'right'
            });
        }
    }).scroll();// 觸發一次 scroll()

    //跟屁蟲廣告
    $(window).bind('scroll resize', function () {
        var scroll_height = $(window).scrollTop();
        if (scroll_height > 320) {
            $('.outside_AD').addClass('OAD_fixed');
        } else {
            $('.outside_AD').removeClass('OAD_fixed');
        }
    }).scroll();// 觸發一次 scroll()



})(jQuery)
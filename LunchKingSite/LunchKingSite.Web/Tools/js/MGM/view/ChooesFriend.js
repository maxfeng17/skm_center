﻿; (function ($, globalAmount, globalOrders) {

    var matchMembers = [];
    var addEmailTemplate;

    $(function () {

        //date 擴充方法
        Date.prototype.yyyymmdd = function () {
            var mm = this.getMonth() + 1; // getMonth() is zero-based
            var dd = this.getDate();

            return [this.getFullYear(), !mm[1] && '0' + mm, !dd[1] && '0' + dd].join('/'); // padding
        };

        var selectOrder = function () {
            var selectCount = 0;
            var $orderList = $('.add_firend_container > table tr');

            $('#js-friend-comfirm > .friend-group').each(function () {
                selectCount += parseInt($(this).find('input[name="amount"]').val());
            });

            $orderList.each(function (e) {

                if (selectCount > $(this).find('input[name=order_amount]').val()) {
                    $(this).find('.not_enough').css('display', '');
                    $(this).find('.radioSN').css('display', 'none');
                } else {
                    $(this).find('.not_enough').css('display', 'none');
                    $(this).find('.radioSN').css('display', '');
                }
            });
            $('.add_friend_giftbox_wrap').bPopup();
        }

        //取訂單
        var getOrder = function (orderId) {
            var orders = JSON.parse(globalOrders);
            var returnOrder;
            $(orders).each(function (e) {
                if (this.OrderId === orderId) {
                    returnOrder = this;
                    return false;
                }
            });

            return returnOrder;
        };

        var updateOrder = function (orderData) {
            var re = /-?\d+/;
            var m = re.exec(orderData.GiftData.DeliverTimeE);
            var date = new Date(parseInt(m[0]));

            var data = {
                Bid: orderData.GiftData.Bid,
                PicUrl: orderData.GiftData.PicUrl,
                amount: orderData.RemainCount,
                ItemName: orderData.GiftData.ItemName,
                Name: orderData.GiftData.Name,
                ItemPrice: '$' + orderData.ItemPrice,
                DeliverTimeE: date.yyyymmdd()
            }

            //更換禮物，塞資料進去
            var container = $('#order_select').empty();
            var template = $('#order-template').html();
            var rendered = Mustache.render(template, data);
            container.append(rendered);
            $('.change_gift').on('click', selectOrder);

            //order modifty
            var currentOrder = {};
            currentOrder.oid = sessionStorage.oid = orderData.OrderId;
            currentOrder.amount = globalAmount = sessionStorage.amount = orderData.RemainCount;
            currentOrder.dealName = sessionStorage.dealName = orderData.GiftData.ItemName;
            sessionStorage.currentOrder = JSON.stringify(currentOrder);
            $('#selectTotal').text(globalAmount); 
        }

        //matchMembers
        if (sessionStorage.getItem('matchMembers') != null && sessionStorage.getItem('oid') != '') {
            var storageMembers = JSON.parse(sessionStorage.matchMembers);
            $('.hint_add_friend').hide();

            var groupAmount = {};
            $(storageMembers).each(function () {
                if (this.Value in groupAmount) {
                    groupAmount[this.Value]++;
                } else {
                    groupAmount[this.Value] = 1;
                }
            });


            var num = 0;
            $(storageMembers).each(function () {
                if ($('#js-friend-comfirm:contains(' + this.Value + ')').length === 0) {
                    var data = {
                        name: this.Name,
                        contact: (IsEmailorPhoneorFB(this.Value) === "Facebook" ? "Facebook" : this.Value),
                        amount: groupAmount[this.Value],
                        type: this.Type,
                        fbdataid: (IsEmailorPhoneorFB(this.Value) === "Facebook" ? this.Value : "")
                    }

                    var template = $('#friend-comfirm-template').html();
                    var rendered = Mustache.render(template, data);
                    $('#js-friend-comfirm').append(rendered);
                    num += data.amount;
                }
            });

            $('#selectCount').text(num);

            if ($('#js-friend-comfirm:contains("Facebook")').length > 0) {
                $('.btn_add_friend_17').addClass('off');
                $('.btn_add_friend_phone').addClass('off');
                $('.btn_add_friend_google').addClass('off');
            } else {
                $('.btn_add_friend_fb').addClass('off');
            }

        }

        //currentOrder
        if (sessionStorage.getItem('currentOrder') != null) {
            var currentOrder = JSON.parse(sessionStorage.currentOrder);
            var orderData = getOrder(currentOrder.oid);
            updateOrder(orderData);
        }


        var oid = sessionStorage.getItem('oid');
        $("input[name=orderList_radio][value=" + oid + "]").prop("checked", true);
        addEmailTemplate = $('#js-friend-group >.friend-group:eq(0)').clone();
        $('#js-friend-group >.friend-group:eq(0)').remove();

        $("#emailForm").validate({
            ignore: ".ignore",
            debug: true,
            rules: {
                contact: {
                    // other rules,
                    eitherEmailPhone: true
                }
            }
        });

        /*google通訊錄*/
        $('.btn_add_friend_google').bind('click', function (e) {
            $('#gmailLoading').css('display', 'block');
            $('#google_choose').text(0);
            $('#google_comfirm').text('');
            var comfirm = 0;
            var amount = globalAmount;
            if ($('#js-friend-comfirm > .friend-group').length > 0) {
                $('#js-friend-comfirm > .friend-group').each(function () {
                    comfirm += parseInt($(this).find('input[name="amount"]').val());
                });
            }
            if (comfirm >= amount) {
                alert('已達張數上限上限囉!');
            } else {
                $('#google_amunt').text(globalAmount - comfirm);
                e.preventDefault();

                $('.add_friend_google_wrap').bPopup();

                CommunityApi.Google.GetContact().done(function (data) {
                    $('#gmailLoading').css('display', 'none');
                    if (data !== "error") {
                        var template = $('#gmail-template').html();
                        var rendered = Mustache.render(template, data);
                        $('#gdata').nextAll().remove();
                        $('#gdata').after(rendered);
                    }
                });

            }
        });

        $('#js-gmail-friend-search').keyup(function () {
            var search = $(this).val();
            if (!search) {
                $(document).find('.js-gmail-selectFriend').show();
                return false;
            }
            $(document).find('.js-gmail-selectFriend').hide();

            $(document).find('.js-gmail-selectFriend input[value*=' + search + ']').parent().parent().show();
            $(document).find('.js-gmail-selectFriend:contains("' + search + '")').show();
        });

        $('body').on('click', '.js-gmail-selectFriend', function (e) {
            var google_amunt = parseInt($('#google_amunt').text());
            var google_choose = parseInt($('#google_choose').text());

            $(this).toggleClass("gmailselected");
            if ($(this).hasClass("gmailselected")) {
                if (google_choose >= google_amunt) {
                    $(this).removeClass("gmailselected");
                    alert('已經達可送數的上限囉!');
                    return;
                }
                $(this).hover(
                    function () {
                        $(this).css('background-color', '#FDFFCA');
                    }, function () {
                        $(this).css('background-color', '#FDFFCA');
                    });
                $(this).css("background-color", "#FDFFCA");
                $('#google_choose').text(google_choose + 1);
                $('#google_comfirm').text('(' + (google_choose + 1) + ')');

            } else {
                $(this).hover(
                    function () {
                        $(this).css('background-color', '#ddd');
                    }, function () {
                        $(this).css('background-color', '#fff');
                    });

                $(this).css("background-color", "#ddd");
                $(this).children('li').find('input[name="name"]').css('border-color', '')
                $('#google_choose').text(google_choose - 1);
                $('#google_comfirm').text('(' + (google_choose - 1) + ')');

            }
        });

        $('#gmailFormBtn').on('click', function () {
            var check = true;
            $('.js-gmail-selectFriend').each(function () {
                if ($(this).hasClass("gmailselected")) {
                    var inputName = $(this).children('li').find('input[name="name"]');
                    var name = inputName.val();
                    if (name.length == 0) {
                        inputName.css('border-color', 'red');
                        //var y = $(this).offset().top;
                        //$('.add_friend_google_container').scrollTop(y);
                        check = false;
                    }
                } else {
                    $(this).removeClass('danger');
                }
            });

            if (check) {
                var num = 0;
                $('.js-gmail-selectFriend').each(function (e) {
                    if ($(this).hasClass("gmailselected")) {
                        var phone = $(this).children('li').find('label[name="phone"]').text();
                        var email = $(this).children('li').find('label[name="email"]').text();
                        var name = $(this).children('li').find('input[name="name"]').val();

                        var data = {
                            name: name,
                            contact: email.length != 0 ? email : phone,
                            amount: 1
                            //type: email.length != 0 ? "email" : "phone"
                        }

                        var template = $('#friend-comfirm-template').html();
                        var rendered = Mustache.render(template, data);
                        $('#js-friend-comfirm').append(rendered);
                        num += 1;
                    }
                });

                var nowAmount = parseInt($('#selectCount').text());
                $('#selectCount').text((nowAmount + num));
                $('.add_friend_google_wrap').bPopup().close();
                $('.btn_add_friend_fb').addClass('off');
                $('.hint_add_friend').hide();
            } else {
                alert("收禮者姓名不可空白!");
            }
        });

        /*Facebook朋友*/
        $('.btn_add_friend_fb').bind('click', function (e) {
            e.preventDefault();
            $('.add_friend_fb_wrap').bPopup();
            $('#fbLoading').toggle();
            //auth
            CommunityApi.Facebook.GetFriends().done(function (data) {
                $('#fbLoading').toggle();
                if (data === "cancel") {
                    $('.add_friend_fb_wrap').bPopup().close();
                    return false;
                }

                var container = $('.add_friend_fb_container').empty();
                var template = $('#fb-template').html();
                Mustache.parse(template); // optional, speeds up future uses
                var rendered = Mustache.render(template, data);
                container.html(rendered);
                CommunityApi.Facebook.GetUsedFriend(container, template);
                return false;
            });

        });

        $('#js-fb-friend-search').keyup(function () {
            var search = $(this).val();
            if (!search) {
                $(document).find('.js-fb-selectFriend').show();
                return false;
            }
            $(document).find('.js-fb-selectFriend').hide();
            $(document).find('.js-fb-selectFriend:contains("' + search + '")').show();
        });

        $('#fbFormBtn').on('click', function () {

            var $select_fb = $('input[name=fb_radio]:checked').parent().parent().parent();

            var data = {
                name: $select_fb.find('.fb_name').text(),
                contact: 'Facebook',
                amount: 1,
                fbdataid: $select_fb.attr('data-id')
                //type: 'fb'
            }

            var template = $('#friend-comfirm-template').html();
            var rendered = Mustache.render(template, data);

            $('#js-friend-comfirm').empty().append(rendered);
            $('.add_friend_fb_wrap').bPopup().close();
            $('.btn_add_friend_17').addClass('off');
            $('.btn_add_friend_phone').addClass('off');
            $('.btn_add_friend_google').addClass('off');
            $('#selectCount').text(1);
            $('.hint_add_friend').hide();

        });


        $('#btnSubimt').click(function () {
            var total = 0;
            var amount = globalAmount;
            var friendGroup = $('#js-friend-comfirm > .friend-group');
            if (friendGroup.length == 0) {
                alert('請至少選擇一位朋友。');
                return false;
            }

            if ($('#js-friend-comfirm > .friend-group').length > 0) {
                $('#js-friend-comfirm > .friend-group').each(function () {
                    total += parseInt($(this).find('input[name="amount"]').val());
                });
            }
            if (total > amount) {
                alert('選擇張數:' + total + '，已超過可送數上限:' + amount + '，請再確認謝謝。');
                return false;
            }

            $('#js-friend-comfirm > .friend-group').each(function (e) {
                for (var i = 0; i < $(this).find('input[name="amount"]').val() ; i++) {
                    var elm = $(this).find('label[name="contact"]');
                    var type = IsEmailorPhoneorFB(elm.text());
                    var data = {};
                    switch (type) {

                        case "Email":
                            data = {
                                Name: $(this).find('label[name="name"]').text(),
                                Type: 0,
                                Value: $(this).find('label[name="contact"]').text()
                            }
                            break;
                        case "Phone":
                            data = {
                                Name: $(this).find('label[name="name"]').text(),
                                Type: 1,
                                Value: $(this).find('label[name="contact"]').text()
                            }
                            break;
                        case "Facebook":
                            data = {
                                Name: $(this).find('label[name="name"]').text(),
                                Type: 2,
                                Value: $(this).find('input[name="data-id"]').val()
                            }
                            break;
                    }
                    matchMembers.push(data);
                }
            });
            var json = JSON.stringify(matchMembers); //選取朋友資訊
            sessionStorage.matchMembers = json;
            location.href = '/User/GiftCardSet';
            return false;
        });

        function IsEmailorPhoneorFB(value) {
            if (/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(value)) {
                return 'Email';
            }
            if (/^09\d{2}-?\d{3}-?\d{3}$/.test(value) && $.trim(value).length == 10) {
                return 'Phone';
            }
            return 'Facebook';
        }


        /*Email和手機*/
        $('.btn_add_friend_phone').bind('click', function (e) {
            var comfirm = 0;
            var amount = globalAmount;
            if ($('#js-friend-comfirm > .friend-group').length > 0) {
                $('#js-friend-comfirm > .friend-group').each(function () {
                    comfirm += parseInt($(this).find('input[name="amount"]').val());
                });
            }
            if (comfirm >= amount) {
                alert('已達張數上限上限囉!');
            } else {
                $('.add_friend_wrap').bPopup();
                $('.add_firend_footer strong').text(globalAmount - comfirm);
                e.preventDefault();
                $('#js-friend-group .friend-group').remove();
                $('#addEmailFriend').trigger('click');

            }
		});

        $('#addEmailFriend').on('click', function () {
            var total = 0;
            var amount = globalAmount;
            var newEmailName = 'contact' + $('#js-friend-group >.friend-group').length + 1;
            var newName = 'name' + $('#js-friend-group >.friend-group').length + 1;
            var addRow = addEmailTemplate.clone();
            $(addRow).find('input[name=contact]').attr("name", newEmailName);
            $(addRow).find('input[name=name]').attr("name", newName);
            $('#js-friend-group').children(':eq(0)').after(addRow);
            $('input[name*=contact').each(function () {
                $(this).rules("add", {
                    required: true,
                    eitherEmailPhone: true
                });
            });
            $('input[name*=name').each(function () {
                $(this).rules("add", {
                    required: true
                });
            });
        });

        $('#emailFormBtn').on('click', function (e) {
            if ($('#emailForm').valid()) {
                var comfirm = 0;
                var amount = globalAmount;
                var select = $('#js-friend-group > .friend-group').length;
                if ($('#js-friend-comfirm > .friend-group').length > 0) {
                    $('#js-friend-comfirm > .friend-group').each(function () {
                        comfirm += parseInt($(this).find('input[name="amount"]').val());
                    });
                }
                if (comfirm + select > amount) {
                    alert('已經達張數上限囉!');
                } else {
                    var num = 0;
                    $('#js-friend-group > .friend-group').each(function (e) {
                        var data = {
                            name: $(this).find('input[name*="name"]').val(),
                            contact: $(this).find('input[name*="contact"]').val(),
                            amount: 1
                        }

                        var template = $('#friend-comfirm-template').html();
                        var rendered = Mustache.render(template, data);
                        $('#js-friend-comfirm').append(rendered);
                        num += 1;
                    });
                    var nowAmount = parseInt($('#selectCount').text());
                    $('#selectCount').text((nowAmount + num));
                    $('.add_friend_wrap').bPopup().close();
                    $('.btn_add_friend_fb').addClass('off');
                    $('.hint_add_friend').hide();
                }
            }
		});

		/*批次新增*/
		$('.btn_batch_add').bind('click', function (e) {
			var comfirm = 0;
			var amount = globalAmount;
			if ($('#js-friend-comfirm > .friend-group').length > 0) {
				$('#js-friend-comfirm > .friend-group').each(function () {
					comfirm += parseInt($(this).find('input[name="amount"]').val());
				});
			}
			if (comfirm >= amount) {
				alert('已達張數上限上限囉!');
			} else {
				$('.batch_add_friend_wrap').bPopup();
				$('.batch_add_friend_footer strong').text(globalAmount - comfirm);
				e.preventDefault();
			}
		});

		$('#btnBatchConfirm').on('click', function (e) {
			var comfirm = 0;
			var amount = globalAmount;
			var select = $('#js-friend-group > .friend-group').length;
			if ($('#js-friend-comfirm > .friend-group').length > 0) {
				$('#js-friend-comfirm > .friend-group').each(function () {
					comfirm += parseInt($(this).find('input[name="amount"]').val());
				});
			}
			if (comfirm + select > amount) {
				alert('已經達張數上限囉!');
			} else {
				var num = 0;
				$.ajax({
					type: "POST",
					url: "/user/GetUserContacts",
					data: JSON.stringify( {userStr: $('#batchNames').val() } ),
					dataType: "json",
					contentType: "application/json; charset=utf-8",
					success: function (res) {
						var errmsgs = [];
						var items = res.items || [];
						for (key in items) {
							if (items[key] == '') {
								continue;
							}
							var exists = false;
							$('.member_info>h5').each(function () {
								if ($(this).text() == items[key]) {
									exists = true;
								}
							});
							if (exists) {
								errmsgs.push(items[key])
								continue;
							}

							var item = {
								name: '會員',
								contact: items[key],
								amount: 1
							}
							var template = $('#friend-comfirm-template').html();
							var rendered = Mustache.render(template, item);
							$('#js-friend-comfirm').append(rendered);
							num += 1;
						}
						if (errmsgs.length > 0) {
							errmsgs.push('重複 ' + errmsgs.length + ' 筆不加入')
							alert(errmsgs.join('\r\n'));
						}
					}, complete: function () {
						var nowAmount = parseInt($('#selectCount').text());
						$('#selectCount').text((nowAmount + num));
						$('.batch_add_friend_wrap').bPopup().close();
						$('.btn_add_friend_fb').addClass('off');
						$('.hint_add_friend').hide();
						$('#batchNames').val('');
					}
				});
			}
		});



        $('body').on('keyup mouseup', 'input[name=amount]', function (e) {
            var amount = globalAmount;
            var total = 0;

            if (e.target.parentNode.parentNode.parentNode.id === "js-friend-comfirm") {
                $('#js-friend-comfirm > .friend-group').each(function () {
                    total += parseInt($(this).find('input[name="amount"]').val());
                });
            }

            if (total > amount) {
                $(this).val($(this).val() - 1);
                alert('已經達張數上限囉!');
            } else {
                $('#selectCount').text(total);
            }
        });



        $('body').on('click', '.js-selectFriend', function (e) {
            $(this).find('i').toggle();
            if ($(this).attr('data-checked') === "false") {
                $(this).attr('data-checked', true);
            } else {
                $(this).attr('data-checked', false);
            }
        });



        $('body').on('click', '.remove-item', function () {
            if ($(this).parent().parent().parent().attr('id') != 'emailForm') {
                var nowAmount = parseInt($('#selectCount').text());
                $('#selectCount').text((nowAmount - $(this).parent().find('input[name=amount]').val()));
            }
            $(this).parent().remove();
            if ($('#js-friend-comfirm > .friend-group').length === 0) {
                $('.btn_add_friend_17').removeClass('off');
                $('.btn_add_friend_phone').removeClass('off');
                $('.btn_add_friend_google').removeClass('off');
                $('.btn_add_friend_fb').removeClass('off');
                $('.hint_add_friend').show();
            }
        });

        /*選擇禮物*/
        $('.change_gift').on('click', selectOrder);

        $('#changeGiftBtn').on('click', function () {
            var orderId = $('input[name=orderList_radio]:checked').val();
            var orderData = getOrder(orderId);
            updateOrder(orderData);
            $('.add_friend_giftbox_wrap').bPopup().close();
        });

       
    });


}(jQuery, sessionStorage.amount, sessionStorage.orderList))
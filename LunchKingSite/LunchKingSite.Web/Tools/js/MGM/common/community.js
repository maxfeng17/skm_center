﻿/*!
 * CommunityAPI Library

   Google:取得通訊錄
   FB:取得朋友、發送邀請
 */

var CommunityApi = (function ($) {

    var _configs = {};

    function init(options) {
        _configs = {
            FBReturnUrl: options.facebookReturnUrl,
            FBScope: options.facebookScope,
            FBAppId: options.facebookAppId,
            FBGiftObjectId: options.facebookObjectId,
            GoogleAppId: options.googleAppId,
            GoogleScopes: options.googleScopes
        }
        facebook.Init();
    }

    // Facebook class
    var facebook = function () {

        var init = function () {
            window.fbAsyncInit = function () {
                FB.init({
                    appId: _configs.FBAppId,
                    xfbml: true,
                    version: 'v2.8'
                });
            };

            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/zh_TW/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        }

        var getFriends = function () {
            var r = $.Deferred();
            FB.login(function (response) {
                if (response.authResponse) {
                    FB.api(
                            '/me/invitable_friends?fields=picture,name&limit=5000',
                            'GET',
                            {},
                            function (response) {
                                r.resolve(response);
                            }
                        );
                } else {
                    console.log('User cancelled login or did not fully authorize.');
                    r.resolve("cancel");
                }

            }, { scope: _configs.FBScope });
            return r;
        }

        var getUsedFriend = function (container, temp) {
            FB.api('me/friends?fields=picture,name,id', 'GET', {},
                function (response) {
                    if (response) {
                        Mustache.parse(temp); // optional, speeds up future uses
                        var rendered = Mustache.render(temp, response);
                        container.append(rendered);
                        container.find('input[type=radio]')[0].checked = true;
                    }
                });
        }

        var sendAppRequest = function (friends, giftCard, message) {

            FB.getLoginStatus(function (response) {
                if (response.status === 'connected') {
                    var toFriends = [];
                    $(friends).each(function () {
                        toFriends.push(this.Value);
                    });

                    FB.ui({
                        method: 'apprequests',
                        title: "準備發送禮物至您所選擇的FB好友",
                        message: message,
                        to: friends[0].Value,//toFriends,
                        display: 'popup', //iframe
                        action_type: 'send',
                        object_id: _configs.FBGiftObjectId,
                        //redirect_uri: "https://www.17life.com/199/9151cc5e-0dbd-465e-9f66-ed4a5812b4b5",
                        //max_recipients: friends.length //account limit
                    },
                    function (res) {
                        if (!res) {/* user canceled */
                            return;
                        } else {
                            //屏蔽產生
                            if (!res.hasOwnProperty('error_code')) {
                                var temp = res.request + '_' + res.to[0];
                                for (i = 0; i < friends.length; i++) {   //可能多筆
                                    friends[i].Value = temp;
                                }
                                giftCard.MatchMembers = friends;

                                $.ajax({
                                    type: "POST",
                                    url: '/User/GiftCardSetByFb',
                                    data: JSON.stringify({ giftCard: giftCard, fbUserId: res.to[0] }),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: function (res) {
                                        //屏蔽消失
                                        if (res.Code === 1000) {
                                            location.href = res.Message;
                                        } else {
                                            alert(res.Message);
                                            location.href = '/User/GiftList';
                                        }
                                    }
                                });
                            }

                        }
                    });
                }

                else if (response.status === 'not_authorized') {
                    alert('FaceBook授權已過期，請重新選擇您要發送的禮物，謝謝。');
                    window.location.href = '/User/SendGift';
                    // the user is logged in to Facebook, but has not authenticated the app
                }
                else {
                    alert('FaceBook授權已過期，請重新選擇您要發送的禮物，謝謝。');
                    window.location.href = '/User/SendGift';
                    //response.status === 'unknown'
                    // the user isn't logged in to Facebook.
                }
            });
        }

        return {
            Init: init,
            GetFriends: getFriends,
            GetUsedFriend: getUsedFriend,
            SendAppRequest: sendAppRequest
        }
    }(); // class end



    // Google class
    var google = function () {

        var getContact = function () {
            var r = $.Deferred();
            gapi.auth.authorize({ client_id: _configs.GoogleAppId, scope: _configs.GoogleScopes, immediate: false },
                function (authResult) {
                    if (authResult && !authResult.error) {
                        $.ajax({
                            url: "https://www.google.com/m8/feeds/contacts/default/full?max-results=99999&access_token=" + authResult.access_token + "&alt=json",
                            dataType: 'jsonp',
                            async: false,
                            data: authResult.access_token,
                            success: function (data) {
                                var contacts = [];
                                if (data.hasOwnProperty("feed")) {
                                    $(data.feed.entry).each(function (index, obj) {
                                        var data = {
                                            name: '',
                                            email: '',
                                            phone: ''
                                        }

                                        if ($(obj['title']).length != 0) {
                                            data.name = obj['title']['$t'];
                                        }
                                        if ($(obj['gd$email']).length != 0) {
                                            data.email = obj['gd$email'][0]['address'];
                                        }

                                        if ($(obj['gd$phoneNumber']).length != 0) {
                                            data.phone = obj['gd$phoneNumber'][0]['$t'];
                                        }

                                        contacts.push(data);
                                    });
                                    var returnData = { data: contacts };
                                    r.resolve(returnData);
                                } else {
                                    r.resolve("error");
                                }

                            },
                            error: function (res) {
                                alert(res);
                            }

                        });
                    } else {
                        r.resolve("error");
                        alert('無法取得Gmail通訊錄，原因尚未授權。');
                    }

                });
            return r;
        };

        return {
            GetContact: getContact
        }

    }(); //class end


    return {
        Init: init,
        Google: google,
        Facebook: facebook
    }
}(jQuery));

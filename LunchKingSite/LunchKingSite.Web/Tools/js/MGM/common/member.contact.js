﻿/*!
 * MemberContact Library

   GetMyContacts:取得通訊錄
   GetMyGroups:取得群組
   MyGroupSet:建立群組
   MyContactSet:建立朋友
 */

var MemberContact = (function ($) {
    // MemberContact class
    var data = [];

    var getMyContacts = function () {

        return r;
    }

    var getMyGroups = function () {
        return r;
    }

    var myGroupSet = function (name, image) {
        $.ajax({
            type: "POST",
            url: '/User/GroupSet',
            data: JSON.stringify({ giftCard: giftCard, matchMembers: JSON.stringify(friends) }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {
                //屏蔽消失
                if (res === 0) {
                    location.href = '/User/GiftSendSuccess';
                } else {
                    alert(res);
                    location.href = '/User/GiftList';
                }
            }
        });
    }

    var myContactSet = function (name, nickName,email,phone,birthday) {
        $.ajax({
            type: "POST",
            url: '/User/ContactSet',
            data: JSON.stringify({ giftCard: giftCard, matchMembers: JSON.stringify(friends) }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {
                //屏蔽消失
                if (res === 0) {
                    location.href = '/User/GiftSendSuccess';
                } else {
                    alert(res);
                    location.href = '/User/GiftList';
                }
            }
        });
    }

    return {
        Data:data,
        GetMyContacts: getMyContacts,
        GetMyGroups: getMyGroups,
        MyGroupSet: myGroupSet,
        MyContactSet: myContactSet
    }
}(jQuery));

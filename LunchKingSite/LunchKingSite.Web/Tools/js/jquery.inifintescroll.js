﻿(
function ($) {
    var TotalTarget = 0;
    var IsActing = false;
    var Index = 0;

    var InifinteScroll = window.InifinteScroll =
        {
            InitialSetting: {
                //Target:scroll觸發載入的物件class
                //Url:ajax取得資料的網址路徑
                //Param:ajax取得資料的網址參數
                //BottomPx:觸發取資料的buffur距離
                //ScrollDelay:觸發後距下次scroll事件的delay時間 
                //FormMethod:Post或Get
                //LoadingImg:顯示loading的物件
                //IsInitialLoad:是否在ready載入第一個target
                //LazyLoadClass:LazyLoad圖片class
            }
        };

    function LoadTargetData(Obj) {
        $.ajax({
            type: InifinteScroll.FormMethod,
            url: InifinteScroll.Url,
            data: Obj.data(InifinteScroll.Param),
            contentType: "application/json; charset=utf-8",
            dataType: "html",
            success: function (msg) {
                $(Obj).html(msg);
                $("img." + InifinteScroll.LazyLoadClass).lazyload({ placeholder: "https://www.17life.com/Themes/default/images/17Life/G2/ppon-M1_pic.jpg", threshold: 300, effect: "fadeIn" }).removeClass(InifinteScroll.LazyLoadClass);;
            },
            error: function (xhr, status, error) {
                $(Obj).hide();
            }
        });
    }

    function ScrollEvent() {
        if (IsActing == false) {
            var remain = InifinteScroll.Target.not('[data-scroll]');
            if (remain.length == 0) {
                $(window).unbind('scroll');
                InifinteScroll.LoadingImg.hide();
                IsActing = true;
            }
            else {
                remain.each(function () {
                    var target_top = $(this).offset().top;
                    if ((target_top - InifinteScroll.BottomPx) <= $(document).scrollTop()) {
                        IsActing = true;
                        InifinteScroll.LoadingImg.show();
                        var obj = $(this);
                        setTimeout(function () {
                            LoadTargetData(obj);
                            obj.attr("data-scroll", true);
                            IsActing = false;
                            InifinteScroll.LoadingImg.hide();
                        }, InifinteScroll.ScrollDelay);

                        return false;
                    }
                });
            }
        }
    }

    jQuery.extend(InifinteScroll, {
        Initialize: function (initialSetting) {
            jQuery.extend(InifinteScroll, initialSetting);
            TotalTarget = InifinteScroll.Target.length;
            if (TotalTarget > 0) {
                $(window).scroll(
                    function () {
                        ScrollEvent();
                    }
                    );

                if (InifinteScroll.IsInitialLoad) {
                    var obj = InifinteScroll.Target.first();
                    LoadTargetData(obj);
                    obj.attr("data-scroll", true);
                }
            }
        }
    });
}
)
(jQuery);
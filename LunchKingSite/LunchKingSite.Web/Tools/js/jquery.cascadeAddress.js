﻿/*                      
 * jquery.cascadeAddress 
 * Version 0.1 (2013.10.6)
 *
 * Written by unicorn <uni2tw@gmail.com>
 * Dual licensed under the MIT and GPL licenses:
 */
$.cascadeAddress = {};
$.cascadeAddress.update = function (selcity, seltown, tbaddr, cityId, townId, addr, island) {
    selcity.val(cityId);
    selcity.trigger('change', { defTownId: townId });
    addr = addr.replace(selcity.find('option:selected').text(), '');
    addr = addr.replace(seltown.find('option:selected').text(), '');
    tbaddr.val(addr);
};
$.cascadeAddress.bind = function (selcity, seltown, tbaddr, cityId, townId, addr, island) {
    var findCity = function (cityid) {
        for (var i in cities) {
            var city = cities[i];
            if (city.id == cityid) {
                return city;
            }
        }
        return null;
    };

    selcity.removeAttr('disabled');
    seltown.removeAttr('disabled');
    tbaddr.removeAttr('disabled');

    selcity.find('option').remove();
    selcity.append('<option value="-1">請選擇</option>');
    var cities = $.cascadeAddress.cities;
    for (var i in cities) {
        if (island || cities[i].vi) {
            if (cities[i].vi) {
                selcity.append("<option value='" + cities[i].id + "'>" + cities[i].name + "</option>");
            } else {
                selcity.append("<option island='island' value='" + cities[i].id + "'>" + cities[i].name + "</option>");
            }
        }
    }
    if (cityId != -1) {
        selcity.val(cityId);
    }
    selcity.change(function (e, data) {
        var cid = $(this).val();
        var city = findCity(cid);
        seltown.find('option').remove();
        seltown.append('<option value="-1">請選擇</option>');
        if (city != null) {
            for (var i in city.towns) {
                var town = city.towns[i];
                if (island || town.vi) {
                    if (town.vi) {
                        seltown.append("<option value='" + town.id + "'>" + town.name + "</option>");
                    } else {
                        seltown.append("<option island='island' value='" + town.id + "'>" + town.name + "</option>");
                    }
                }
            }
        }
        if (data != null && data.defTownId != null) {
            seltown.val(data.defTownId);
        }
    });
    selcity.trigger('change', { defTownId: townId });
    tbaddr.val(addr);
};
$.cascadeAddress.copy = function (srcCity, srcTown, srcAddr, tarCity, tarTown, tarAddr, island) {
    var copySelect = function (srcSel, tarSel) {
        tarSel.find('option').remove();
        srcSel.find('option').each(function () {
            if (island || $(this).attr('island') == null) {
                if ($(this).attr('island') == null) {
                    tarSel.append("<option value='" + $(this).val() + "'>" + $(this).text() + "</option>");
                } else {
                    tarSel.append("<option island='island' value='" + $(this).val() + "'>" + $(this).text() + "</option>");
                }
            }
        });
    };
    $(tarAddr).val($(srcAddr).val());
    copySelect(srcCity, tarCity, island);
    tarCity.val(srcCity.val());
    if (tarCity.val() != '-1') {
        copySelect(srcTown, tarTown, island);
        tarTown.val(srcTown.val());
    } else {
        //it's stupid fix
        tarTown.find('option').remove();
        tarTown.append('<option value="-1">請選擇</option>');
    }
};

$.cascadeAddress.setup = function (rooturl, callback) {
    $.cascadeAddress.setReady(false);
    if (rooturl == null || rooturl == '' || rooturl.substr(rooturl.length - 1, 1) != '/') {
        rooturl += '/';
    }
    $.getJSON(rooturl + 'api/location/getcitytownships', function (res) {
        $.cascadeAddress.cities = res;
        callback.call();
        $.cascadeAddress.setReady(true);
    });
};
$.cascadeAddress._isReady = false;
$.cascadeAddress.setReady = function (isReady) {
    $.cascadeAddress._isReady = isReady;
}
$.cascadeAddress.ready = function () {
    return $.cascadeAddress._isReady;
};
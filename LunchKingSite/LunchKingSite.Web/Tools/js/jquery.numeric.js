/*
 *
 * Copyright (c) 2006 Sam Collett (http://www.texotela.co.uk)
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 * 
 */
 
/*
 * Allows only valid numeric characters to be entered into input boxes.
 * @name     numeric  
 * @author   Sam Collett (http://www.texotela.co.uk)
 * @modifier unicorn liu @ learningdigital.com
 * @example  $("input.numeric").numeric(); // validate integer
 * @example  $("input.numeric").numeric(true); // validate float
 * @example  $("input.numeric").numeric(false,0,9999); // validate integer between 0-9999
 * @example  $("input.numeric").numeric(true,0,9999, callback(res,val) { // res is Boolean, val is number
 *           });
 */
jQuery.fn.numeric = function(isDecimal,min,max,callback) {
	callback = typeof callback == "function" ? callback : function(){};
	var validate = function(s,isDecimal, min, max) {
		var n = parseFloat(s);
		if (isNaN(n)){
			return false;
		}
		if (!isDecimal && s.indexOf('.')!=-1 ){
			return false;
		}
		if (min!=null && n<min) {
			return false;
		}
		if (max!=null && n>max) {
			return false;
		}
		return true;
	}
    //deal with double-byte character.
    this.keyup(function(e) {
        var str = $(this).val();
        var res = '';
        for (i = 0; i < str.length; i++) {
            if ((str.charCodeAt(i) >= 48 && str.charCodeAt(i) <= 57)
                || (isDecimal && str.charCodeAt(i) == 46)
                ) {
                res += str[i];
            }
        }
        if (str != res) {
            $(this).val(res);
        }
    });

    this.keypress(function (e) {
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
		// allow enter/return key (only when in an input box)
		if(key == 13 && this.nodeName.toLowerCase() == "input") {
			return true;
		}
		else if(key == 13) {
			return false;
		}
		var allow = false;
		// allow Ctrl+A
		if((e.ctrlKey && key == 97 /* firefox */) || (e.ctrlKey && key == 65) /* opera */) return true;
		// allow Ctrl+X (cut)
		if((e.ctrlKey && key == 120 /* firefox */) || (e.ctrlKey && key == 88) /* opera */) return true;
		// allow Ctrl+C (copy)
		if((e.ctrlKey && key == 99 /* firefox */) || (e.ctrlKey && key == 67) /* opera */) return true;
		// allow Ctrl+Z (undo)
		if((e.ctrlKey && key == 122 /* firefox */) || (e.ctrlKey && key == 90) /* opera */) return true;
		// allow or deny Ctrl+V (paste), Shift+Ins
		if((e.ctrlKey && key == 118 /* firefox */) || (e.ctrlKey && key == 86) /* opera */
		|| (e.shiftKey && key == 45)) return true;

		if((key < 48 || key > 57) && key!=46) {
			allow = false;				
			// check for other keys that have special purposes for (ie don't need it)
			if(jQuery.browser.mozilla) {
				if(
						key == 8 /* backspace */ ||
						key == 9 /* tab */ ||								
						key == 35 /* end */ ||
						key == 36 /* home */ ||
						key == 37 /* left */ ||
						key == 39 /* right */ ||
						(key == 46 && e.whcih==0) /* del */)	{
					allow = true;
				}
			}
		} else {
			allow = true;
		}
		var val = $(this).val() + String.fromCharCode(key);
		if (allow) {
			allow = validate(val,isDecimal, min, max);
		}
		return allow;
	});
	this.blur(function() {
		var val = jQuery(this).val();
		if(validate(val,isDecimal, min, max)){
			//$(this).val(new String(parseFloat(val)));
			callback.call(this, true, parseFloat(val));
		} else {
			$(this).val("");
			callback.call(this, false, null);
		}
	});	
}
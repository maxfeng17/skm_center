﻿var allowList = []; 
var denyList = []; 
var noDenyList = [];  

$(document).ready(function () {
    $('.denychecked').click(denyUncheck);
    $('.denyunchecked').click(denyCheck);
    $('.denypartialchecked').click(denyUncheck);
    
    $('.allowchecked').click(allowUncheck);
    $('.allowunchecked').click(allowCheck);
});

var aclAllowPageManager = {
    setPermissionLists: function() {
        var that = this;
        $('.permission_group').each(function(idx, ul) {
            var rootListItem = $(ul).find('li').not('[parentid]');
            that.setPermission(rootListItem, undefined, undefined);
        });
    },
    setPermission: function(resourceListItem, parentId, parentState) {
        var that = this;
        var currentState = getCheckState(resourceListItem);
        var resourceValue = $(resourceListItem).find('input').val();
        
        //rule: 
        //1. upper checkboxes affect lower checkboxes.
        //2. lower checkboxes does not affect upper checkboxes.
        //3. when upper checkbox is checked, lower checkboxes cannot be unchecked.
        //implementation:
        //1. allow the topmost checked resource 
        //2. make sure that no deny exists anywhere else in the hierarchy

        if (currentState === 'checked' && parentState !== 'checked') {
            allowList.push(resourceValue);
            noDenyAncestors(resourceListItem);
            noDenyDecendents(resourceListItem);
            return;
        }else {
            var resourceId = $(resourceListItem).attr('id');
            var children = getChilden(resourceListItem);
            if(children.length===0) {
                return;
            } else {
                children.each(function (idx, li) {
                    that.setPermission(li, resourceId, currentState);
                });
            }
        }
    }
};


var aclDenyPageManager = {
    setPermissionLists: function() {
        var that = this;
        $('.permission_group').find('li[id]').not('[parentid]').each(function(idx, li) {
            that.setPermission(li);
        });
    },
    setPermission: function(resourceListItem) {
        var that = this;
        var currentState = getCheckState(resourceListItem);
        
        if(currentState === 'unchecked') {
            var resourceValue = $(resourceListItem).find('input').val();
            denyList.push(resourceValue);
            return;
        }else {
            var resourceId = $(resourceListItem).attr('id');
            var children = getChilden(resourceListItem);
            if(children.length===0) {
                return;
            }else {
                children.each(function(idx, li){
                    that.setPermission(li);
                });
            }
        }
    }
};

function denyUncheck() {
    var resourceListItem = $(this).closest('li');
    uncheckDecendents(resourceListItem);
    partialCheckAncestors(resourceListItem);
    changePartialCheckAncestors(resourceListItem);
}

function denyCheck() {
    var resourceListItem = $(this).closest('li');
    checkDecendents(resourceListItem);
    partialCheckAncestors(resourceListItem);
    changePartialCheckAncestors(resourceListItem);  
}

function allowUncheck() {
    var resourceListItem = $(this).closest('li');
    var parentId = resourceListItem.attr('parentid');
    if(parentId !== undefined) {
        var parentResourceListItem = $('li[id="' + parentId + '"]');
        var parentState = getCheckState(parentResourceListItem);
        if(parentState === 'unchecked') {
            uncheckDecendents(resourceListItem);
        }
    }else {
        uncheckDecendents(resourceListItem);
    }
}

function allowCheck() {
    var resourceListItem = $(this).closest('li');
    checkDecendents(resourceListItem);
}


function checkDecendents(resourceListItem) {
    resourceListItem.find('.denyunchecked,.allowunchecked').hide();
    resourceListItem.find('.denychecked,.allowchecked').show();
    var resourceId = resourceListItem.attr('id');
    
    var children = getChilden(resourceListItem);
    if(children.length===0) {
        return;
    }else {
        children.each(function(idx, li){
          checkDecendents($(li));
        });
    }
}

function uncheckDecendents(resourceListItem) {
    resourceListItem.find('.denychecked,.allowchecked').hide();
    resourceListItem.find('.denypartialchecked').hide();
    resourceListItem.find('.denyunchecked,.allowunchecked').show();
    var resourceId = resourceListItem.attr('id');
    
    var children = getChilden(resourceListItem);
    if(children.length===0) {
        return;
    }else {
        children.each(function(idx, li){
          uncheckDecendents($(li));
        });
    }
}

function partialCheckAncestors(resourceListItem) {
    var parentId = resourceListItem.attr('parentid');
    if(parentId !== undefined) {
        var parentResourceListItem = $('li[id="' + parentId + '"]');
        var parentCheckedImg = parentResourceListItem.find('.denychecked');
        var parentUncheckedImg = parentResourceListItem.find('.denyunchecked');
        var parentPartialCheckedImg = parentResourceListItem.find('.denypartialchecked');

        parentPartialCheckedImg.show();
        parentCheckedImg.hide();
        parentUncheckedImg.hide();
        
        partialCheckAncestors(parentResourceListItem);
    }
}

function changePartialCheckAncestors(resourceListItem) {
    var parentId = resourceListItem.attr('parentid');
    if(parentId !== undefined) {
        var parentResourceListItem = $('li[id="' + parentId + '"]');
        var childrenListItem = getChilden(resourceListItem);
        var allChecked = true;
        var allUnchecked = true;
        childrenListItem.each(function(idx, li) {
            if(!$(li).find('.denychecked').is(':visible')) {
                allChecked = false;
            }
            if(!$(li).find('.denyunchecked').is(':visible')) {
                allUnchecked = false;
            }
        });
        if(allChecked) {
            parentResourceListItem.find('.denychecked').show();
            parentResourceListItem.find('.denypartialchecked').hide();
            parentResourceListItem.find('.denyunchecked').hide();
            changePartialCheckAncestors(parentResourceListItem);
        }
        if(allUnchecked) {
            parentResourceListItem.find('.denyunchecked').show();
            parentResourceListItem.find('.denypartialchecked').hide();
            parentResourceListItem.find('.denychecked').hide();
            changePartialCheckAncestors(parentResourceListItem);
        }
    }
}

function getCheckState(resourceListItem) {
    if($(resourceListItem).find('.denychecked,.allowchecked').is(':visible')) {
        return 'checked';
    }
    if($(resourceListItem).find('.denypartialchecked').is(':visible')) {
        return 'partialchecked';
    }
    if($(resourceListItem).find('.denyunchecked,.allowunchecked').is(':visible')) {
        return 'unchecked';
    }
    return 'nothing';  //no checkbox picture means that the corresponding resource permission is not to be changed.
}

function noDenyAncestors(resourceListItem) {
    var parentId = $(resourceListItem).attr('parentId');
    if(parentId !== undefined) {
        var parentListItem = $('li[id="' + parentId + '"]');
        var parentValue = $(parentListItem).find('input').val();
        if($.inArray(parentValue, noDenyList) === -1) {
            noDenyList.push(parentValue);
        }
        noDenyAncestors(parentListItem);
    }
}

function noDenyDecendents(resourceListItem) {
    var resourceId = $(resourceListItem).attr('id');
    var children = getChilden(resourceListItem);
    if(children.length===0) {
        return;
    }else {
        children.each(function(idx, li){
            var childValue = $(li).find('input').val();
            if(childValue !== undefined) {
                noDenyList.push(childValue);
            }
            noDenyDecendents(li);
        });
    }
}


function getChilden(resourceListItem) {
    var resourceId = $(resourceListItem).attr('id');
    if ($(resourceListItem).attr('title') === undefined) {
        return $('li[parentid="' + resourceId + '"]');
    }
    if ($(resourceListItem).attr('title').indexOf('sid:') >= 0) {
        return $('li[parentid="' + resourceId + '"][title^="stguid:"],li[parentid="' + resourceId + '"][title^="bid:"],li[parentid="' + resourceId + '"][title^="pguid:"]').not('li[parentid="' + resourceId + '"][title^="bid:"][title*="stguid:"],li[parentid="' + resourceId + '"][title^="pguid:"][title*="stguid:"]');
    }
    if ($(resourceListItem).attr('title').indexOf('stguid:') >= 0) {
        return $('li[parentid="' + resourceId + '"][title^="bid:"],li[parentid="' + resourceId + '"][title^="pguid:"]');
    }
    return "";
}

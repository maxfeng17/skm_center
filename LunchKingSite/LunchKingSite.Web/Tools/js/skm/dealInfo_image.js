﻿function handleAppFiles(files, holder) {
    for (var i = 0; i < files.length; i++) {
        var fd = new FormData();
        fd.append('upload', files[i]);
        console.log('try send file');
        sendAppFileToServer(fd, holder);
    }
}

function handleImgFiles(files, holder) {
    for (var i = 0; i < files.length; i++) {
        var fd = new FormData();
        fd.append('upload', files[i]);
        console.log('try send file');
        sendImgFileToServer(fd, holder);
    }
}

function sendAppFileToServer(formData, holder) {
    var request = new XMLHttpRequest();
    request.onload = function () {
        if (request.status === 200) {
            var res = $.parseJSON(request.responseText);
            console.log('ok: got file. id is ' + res[0].FileId);
            $('#hidAppDealPic').val(res[0].FileId);

        } else {
            alert('發生錯誤. 找 IT 處理');
        }
    }
    request.open("POST", "/SKMDeal/UploadFile", true);
    request.send(formData);
}

function sendImgFileToServer(formData, holder) {
    var request = new XMLHttpRequest();
    request.onload = function () {
        if (request.status === 200) {
            var res = $.parseJSON(request.responseText);
            console.log('ok: got file. id is ' + res[0].FileId);
            $('#hidImagePath').val(res[0].FileId);

        } else {
            alert('發生錯誤. 找 IT 處理');
        }
    }
    request.open("POST", "/SKMDeal/UploadFile", true);
    request.send(formData);
}

function showImage(targetSrc, holder, type) {
    if (type === 'img') {
        if ($('#hidTempImagePath').val() === "") { $('#hidTempImagePath').val(targetSrc); }
        else { targetSrc = $('#hidTempImagePath').val() }
    }


    var image = new Image();

    $(image).attr('src', targetSrc);

    $(image).css('position', 'absolute')
        .css('z-index', 1)
        .css('top', '0px')
        .css('left', '0px')
        .css('width', '480px')
        .css('height', '267px')
        .addClass('previewImage');

    $(image).css("height", 267);
    $(image).css("width", 480);
    $(holder).append(image);
    $(holder).addClass('drag-drop-upload-ok');


    $(image).load(function () {
        var w = $(this).prop('naturalWidth');
        var h = $(this).prop('naturalHeight');
        if (h !== 267 || w !== 480) {
            //alert('僅能上傳圖片檔案，大小為 480*267');
        }
    });
    //建立刪除按鈕
    var btnDelete = $('<button></button>').html('<i class="fa fa-times"></i>').attr('type', 'button');
    $(holder).append(btnDelete);
    $(btnDelete).css('position', 'absolute').addClass('ddu-edit')
        .css('top', '-20px').css('left', '485px').css('cursor', 'pointer');
    $(btnDelete).click(function () {
        if (confirm('確定刪除？')) {
            if (type === 'img') {
                deleteDragDropUploadElemtns(holder);
                $(holder).parents('.drag-drop-upload-container-img').find(':file').val('');
            } else {
                deleteDragDropUploadAppElemtns(holder);
                $(holder).parents('.drag-drop-upload-container').find(':file').val('');
            }
        }
    });
}

function showAppImage(targetSrc, holder, type) {
    if (type === 'app') {
        if ($('#hidTempAppDealPic').val() === "") { $('#hidTempAppDealPic').val(targetSrc); }
        else { targetSrc = $('#hidTempAppDealPic').val() }
    }

    var image = new Image();
    $(image).attr('src', targetSrc);
    $(image)
        .css('position', 'absolute')
        .css('z-index', 1)
        .css('top', '0px')
        .css('left', '0px')
        .css('width', '267px')
        .css('height', '267px')
        .addClass('previewImage');

    $(image).css("height", 267);
    $(image).css("width", 267);
    $(holder).append(image);
    $(holder).addClass('drag-drop-upload-ok');

    //建立刪除按鈕
    var btnDelete = $('<button></button>').html('<i class="fa fa-times"></i>').attr('type', 'button');
    $(holder).append(btnDelete);
    $(btnDelete).css('position', 'absolute').addClass('ddu-edit')
        .css('top', '-20px').css('left', '485px').css('cursor', 'pointer');
    $(btnDelete).click(function () {
        if (confirm('確定刪除？')) {
            if (type === 'app') {
                deleteDragDropUploadElemtns(holder);
                $(holder).parents('.drag-drop-upload-container-img').find(':file').val('');
            } else {
                deleteDragDropUploadAppElemtns(holder);
                $(holder).parents('.drag-drop-upload-container').find(':file').val('');
            }
        }
    });
}

function deleteDragDropUploadElemtns(holder) {
    $(holder).find('.ddu-edit').remove();
    $(holder).find('.previewImage').remove();
    $(holder).find('.ddu-edit').remove();
    $('#hidImagePath').val('');
    $('#hidTempImagePath').val('');
    $(holder).removeClass('drag-drop-upload-ok');
}

function deleteDragDropUploadAppElemtns(holder) {
    $(holder).find('.ddu-edit').remove();
    $(holder).find('.previewImage').remove();
    $(holder).find('.ddu-edit').remove();
    $('#hidAppDealPic').val('');
    $('#hidTempAppDealPic').val('');
    $(holder).removeClass('drag-drop-upload-ok');
}

function previewImageFile(file, holder) {
    deleteDragDropUploadElemtns(holder);
    var reader = new FileReader();
    reader.onload = function (event) {
        console.log('previewImageFile');
        showImage(event.target.result, holder, 'img');
    };
    reader.readAsDataURL(file);
}

function previewAppImageFile(file, holder) {
    deleteDragDropUploadAppElemtns(holder);
    var reader = new FileReader();
    reader.onload = function (event) {
        console.log('previewImageFile');
        showAppImage(event.target.result, holder, 'app');
    };

    reader.readAsDataURL(file);
}
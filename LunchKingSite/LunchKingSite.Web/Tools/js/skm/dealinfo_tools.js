﻿function padLeft(obj) {
    var len = $(obj).val().length;
    var max = $(obj).attr('maxLength');

    if (len < max) {
        $(obj).val('0' + $(obj).val());
        padLeft(obj);
    }
}

function leftPad(maxLen, text) {
    var len = text.length;
    var max = maxLen;

    if (len < max) {
        text = '0' + text;
        leftPad(maxLen, text);
    }
    return text;
}

function numberOnly(e) {
    $(e).keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
}

function getCharLength(obj) {
    var char = $(obj).val().length;
    var max = $(obj).attr('maxLength');
    obj.parent().parent().find('span').text(max - char);
}
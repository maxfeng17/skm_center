//訂單查詢頁的資料
var order_list = {
    isloaded: false,
    OrderId: 0,
    OrderNo: "",
    TradeNo: "",
    RefundTradeNo: "",
    CouponId: 0,
    OrderStatus: 0,
    OrderStatusDesc: "",
    VerifyStatusDesc: "",
    RefundStatus: 0,
    RefundStatusDesc: "",
    ShopCode: "",
    ShopName: "",
    OrderTime: "",
    VerifyTime: "",
    FreeReturnDate: "",
    ReturnDate: "",
    DealName: "",
    ProductCode: "",
    ItemNo: "",
    SpecialItemNo: "",
    OrderAmount: "",
    IsReturnable: false,
    IsApposRefundTime: true,
    VipNo: "",
    PaymentDetail: {
        PaymentMethodDesc: "",
        PaymentMethod: 0,
        PayAmount: "",
        RedeemAmount: "",
        RedeemPoint: "",
        BurningSkmPoint: ""
    },
    Invoice: {
        InvoiceDate: "",
        InvoiceNo: "",
        InvoiceAmount: "",
        IsDonate: false,
        DonateCode: "",
        CarrierType: 0,
        CarrierNo: "",
        CompId: ""
    }
};
var gift = {
    isloaded: false,
    OrderNo: "",
    GiftList: [
        {
            DealCode: "",
            GiftCode: "",
            GiftExchangeQuantity: 0,
            GiftName: "",
            DealName: "",
            GiftExchangeStatus: "",
            GiftCertificateNo: "",
            RedeemerName: "",
            ExchangeTime: "",
            GiftType: "",
            GiftCertificateAmount: "",
            GiftExchangeNo: "",
            ExchangeStoreName: "",
            MemberNo: "",
            CardTypeNo: ""
        }
    ],
    RequestXml: null,
    ResponseXml: null
};

if (document.getElementById("order-list-zone") !== null) {
    var order_list_vm = new Vue({
        el: document.getElementById("order-list-zone"),
        data: {
            list: order_list,
            giftinfo: gift,
            seen: false,
            isShowErrMsg: false,
            errMsg: "",
            isShowGift: false,
            isRedeem: false,
            isDonate: false,
            isCompInvoice: false,
            isReturnable: false,
            refunddata: null,
            storeData: null,
            shopData: null,
            defaultStore: null,
            defaultShop: null,
            isShowQuery: false
        },
        methods: {
            setStoreShop: function () {
                var storeKey = this.defaultStore;
                axios
                    .get('../skmdeal/getshopdata?sg=' + storeKey)
                    .then(response => {
                        if (response.data !== null) {
                            this.storeData = response.data.Store;
                            this.shopData = response.data.Shop;
                            for (var s of response.data.Store) {
                                if (s.IsDefault === true) {
                                    this.defaultStore = s.Key;
                                }
                            }
                            if (response.data.Shop.length > 0) {
                                this.defaultShop = response.data.Shop[0].Key;
                            }
                            this.$nextTick(function () {
                                //渲染完毕
                                this.isShowQuery = true;
                            });
                        }
                    })
                    .catch(error => {
                        alert("系統錯誤 (" + error.response.status + ")!");
                    });;
            },
            clear: function () {
                document.getElementById("order-list-number").value = "";
                this.seen = false;
            },
            query: function () {
                this.seen = false;
                this.isShowGift = false;
                this.isShowQuery = false;
                this.isShowErrMsg = false;
                this.giftinfo.GiftList = [];
                var searchType = document.getElementById("searchType").value;
                var searchNo = document.getElementById("order-list-number").value;
                var orderNo = '';
                var tradeNo = '';
                if (searchType === '0') {
                    orderNo = searchNo;
                }
                else {
                    tradeNo = searchNo;
                }
                var shopCode = this.defaultShop;
                axios
                    .get('../SKMDeal/GetSkmPayOrderByOrderNo?orderNo=' + orderNo + '&shopCode=' + shopCode + '&tradeNo=' + tradeNo)
                    .then(response => {
                        if (response.data !== null && response.data.Order !== null) {
                            var order = response.data.Order;
                            this.list.isloaded = true;
                            this.list.OrderId = order.OrderId;
                            this.list.OrderNo = order.OrderNo;
                            this.list.TradeNo = order.TradeNo;
                            this.list.RefundTradeNo = order.RefundTradeNo;
                            this.list.CouponId = order.CouponId;
                            this.list.OrderStatus = order.OrderStatus;
                            this.list.OrderStatusDesc = order.OrderStatusDesc;
                            this.list.VerifyStatusDesc = order.VerifyStatusDesc;
                            this.list.RefundStatus = order.RefundStatus;
                            this.list.RefundStatusDesc = order.RefundStatusDesc;
                            this.list.ShopCode = order.ShopCode;
                            this.list.ShopName = order.ShopName;
                            this.list.OrderTime = order.OrderTime;
                            this.list.VerifyTime = order.VerifyTime;
                            this.list.FreeReturnDate = order.FreeReturnDate;
                            this.list.ReturnDate = order.ReturnDate;
                            this.list.DealName = order.DealName;
                            this.list.ProductCode = order.ProductCode;
                            this.list.ItemNo = order.ItemNo;
                            this.list.SpecialItemNo = order.SpecialItemNo;
                            this.list.OrderAmount = order.OrderAmount;
                            this.list.IsReturnable = order.IsReturnable;
                            this.list.IsApposRefundTime = order.IsApposRefundTime;
                            this.list.VipNo = order.VipNo;
                            if (order.PaymentDetail !== null) {
                                this.list.PaymentDetail.PaymentMethodDesc = order.PaymentDetail.PaymentMethodDesc;
                                this.list.PaymentDetail.PaymentMethod = order.PaymentDetail.PaymentMethod;
                                this.list.PaymentDetail.PayAmount = order.PaymentDetail.PayAmount;
                                this.list.PaymentDetail.RedeemAmount = order.PaymentDetail.RedeemAmount;
                                this.list.PaymentDetail.RedeemPoint = order.PaymentDetail.RedeemPoint;
                                this.list.PaymentDetail.BurningSkmPoint = order.PaymentDetail.BurningSkmPoint;
                                this.isRedeem = order.PaymentDetail.RedeemAmount > 0;
                            }
                            if (order.Invoice !== null) {
                                this.list.Invoice.InvoiceDate = order.Invoice.InvoiceDate;
                                this.list.Invoice.InvoiceNo = order.Invoice.InvoiceNo;
                                this.list.Invoice.InvoiceAmount = order.Invoice.InvoiceAmount;
                                this.list.Invoice.IsDonate = order.Invoice.IsDonate;
                                this.list.Invoice.DonateCode = order.Invoice.DonateCode;
                                this.list.Invoice.CarrierType = order.Invoice.CarrierType;
                                this.list.Invoice.CarrierNo = order.Invoice.CarrierNo;
                                this.list.Invoice.CompId = order.Invoice.CompId;
                                this.isDonate = order.Invoice.IsDonate;
                                this.isCompInvoice = order.Invoice.CompId !== "" && order.Invoice.CompId !== null;
                            }

                            this.isReturnable = order.IsReturnable;
                            this.seen = this.list.isloaded;
                        }
                        else {
                            this.isShowErrMsg = true;
                            this.errMsg = "查無訂單!";
                        }
                        if (response.data !== null && response.data.Gift !== null) {
                            var orderGift = response.data.Gift;
                            if (orderGift.IsLoad === true) {
                                this.giftinfo.OrderNo = orderGift.OrderNo;
                                if (orderGift.GiftList !== null) {
                                    this.giftinfo.GiftList = orderGift.GiftList;
                                    this.isShowGift = true;
                                }
                            }
                            this.giftinfo.RequestXml = orderGift.RequestXml;
                            this.giftinfo.ResponseXml = orderGift.ResponseXml;
                        }
                        this.isShowQuery = true;
                    })
                    .catch(error => {
                        alert("系統錯誤!");
                        this.isShowQuery = true;
                    });
            },
            refund: function () {
                if (!confirm("提醒您!\n\r您確認將該筆訂單退款嗎?"))
                    return;
                //var orderNo = document.getElementById("order-list-number").value;
                axios
                    .get('../SKMDeal/RefundSkmPayOrderByOrderNo?orderNo=' + this.list.OrderNo)
                    .then(refundResult => {
                        if (refundResult.data !== null) {
                            this.refunddata = refundResult.data;
                            if (this.refunddata.Code === 1000) {
                                alert("本訂單已成功退款!");
                            }
                            else {
                                alert(this.refunddata.Message);
                            }
                            this.query();
                        }
                    })
                    .catch(error => {
                        alert("系統錯誤!");
                    });

            }
        },
        beforeMount: function () {
            this.setStoreShop();
        }
    });
}

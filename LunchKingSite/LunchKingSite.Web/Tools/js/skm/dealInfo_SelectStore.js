﻿var SkmDealInfoSelectShoppe = (function () {
    var _this;
    var id;
    var showDataGroups;
    var generateDataLogic;
    var canEdit;

    var divTypeArray = ["Show", "Select"];
    ///傳入資料的類型
    var dataTypeEnum = {
        WriteOffShoppe: 1,
        ShowShoppe: 2
    };
    //範例用
    var groupDataObject = {
        key: "",//shop code
        name: "",//store name
        marketCode: "",//Market Cost Center Code成本中心用
        sellCode: "",//Sell Cost Center Code成本中心用
        groupData: [{
            storeGuid: "",      //館號
            storeName: "",      //館位名稱
            sellerGuid: "",     //櫃號
            shopCode: "",       //shop code
            shopName: "",       //櫃位名稱
            showDataType: "",   //櫃位資料類型
            mainShopCode: "",   //Main Shop Code (核銷資料用)
            selectAllAttr: ""   //全選屬性
        }]
    };

    function SkmDealInfoSelectShoppe(id, showData, dataType) {
        _this = this;
        _this.id = id;
        _this.dataType = dataType;
        _this.canEdit = true;
        switch (dataType) {
            case dataTypeEnum.WriteOffShoppe:
                _this.generateDataLogic = WriteOffShoppeDataLogic;
                break;
            case dataTypeEnum.ShowShoppe:
                _this.generateDataLogic = ShowShoppeDataLogic;
                break;
            default:
                return;
        }
        _this.showDataGroups = _this.generateDataLogic.groupDataByShopCode(showData);
        _this.setShoppeClickDelegate(checkBoxShoppeClickDelegate);
        generateSelectShoppes(_this.showDataGroups);
    }

    ///建置初始的兌換地點選擇容器(可選擇&已選擇)
    ///showDataGroups:已分群組的櫃位資訊
    function generateSelectShoppes(showDataGroups) {
        let divShoppeContainer = $("#" + _this.id);
        if (divShoppeContainer) {
            //div_showStore
            let divShowStore = $("<div class='store flo-left'></div>")
                .attr("id", "div_" + _this.id + "_" + divTypeArray[0]);

            //div_slectStore
            let divSelectStore = $("<div class='store flo-left'></div>")
                .attr("id", "div_" + _this.id + "_" + divTypeArray[1]);
            
            _this.generateDataLogic.getStoreShopInfo(showDataGroups)
                .forEach(function (item) {
                    let ulStoreShoppes = $("<ul><p></p><div class='divLiList'></div></ul>");
                    $(ulStoreShoppes)
                        .attr({
                            "id": _this.id + "_Store_" + item.shopCode,
                            "store-guid": item.storeGuid,
                            "shop-code": item.shopCode,
                            "market-code": item.marketCode,
                            "sell-code": item.sellCode
                        })
                        .hide();
                    $("p", ulStoreShoppes)
                        .text("---------- " + item.storeName + " ----------")
                        .attr( "onClick", _this.id + ".setSelectStoreUlIsShow(\"" + _this.id + "_Store_" + item.shopCode+"\")");
                    divShowStore.append(ulStoreShoppes);
                    divSelectStore.append(ulStoreShoppes.clone());
                });

            //上方Div
            let storeShoppes = $("<div class='mrg10-btm'></div>");
            $(storeShoppes)
                .text("查詢   ")
                .append($("<input type='text' class='fr-input-defaul'/>").attr({
                    "id": "searchShoppes_" + _this.id + "_" + divTypeArray[0],
                    "oninput": _this.id + ".searchTextChange(\"div_" + _this.id + "_" + divTypeArray[0] + "\")",
                }));
            $("#" + _this.id).append(storeShoppes)
                .append($("<h5 class='flo-left'></h5>").text(_this.generateDataLogic.title))
                .append("<h5 class='flo-left'>已選擇店舖</h5><br>")
                .append(divShowStore)
                .append(divSelectStore);
        }
    }

    ///產生Li的Shoppe內容
    ///ulShowShoppeDom:div id like'Show'中 ul id='{id}_Store_{data.shopCode}'的物件
    ///ulSelectShoppeDom:div id like'Select'中 ul id='{id}_Store_{data.shopCode}'的物件
    function generateLiSelectShoppes(ulShowShoppeDom, ulSelectShoppeDom) {
        let shopCode = $(ulSelectShoppeDom).attr("shop-code");
        let showDataByShopCode = $.grep(_this.showDataGroups, function (item) { return item.key == shopCode });
        let liStoreShoppes = "";
        showDataByShopCode[0].groupData.forEach(function (item) {
            var checkboxStoreShoppe = $("<input type='checkbox'/>");
            $(checkboxStoreShoppe).attr(
                {
                    "id": _this.id + "_" + item.sellerGuid,
                    "name": _this.id + "_" + item.sellerGuid,
                    "value": item.sellerGuid,
                    "data-type": _this.id + "_" + item.sellerGuid,
                    "shop-name": showDataByShopCode[0].name,
                    "shop-code": item.shopCode,
                    "shop-data": item.mainShopCode,
                    "onClick": _this.id + ".shoppeCheckedChange(\"" + _this.id + "_" + item.sellerGuid+"\")",
                });
            if (!_this.canEdit) {
                $(checkboxStoreShoppe).prop("disabled", "disabled");
            }
            var liStoreShoppe = $("<li><label></label></li>");
            $(liStoreShoppe).attr(
                {
                    "id": "li_" + _this.id + "_" + item.sellerGuid,
                    "data-type": item.showDataType,
                });
            if (item.selectAllAttr != undefined && item.selectAllAttr != "") {
                $(liStoreShoppe).attr("select-all", item.selectAllAttr);
                $(checkboxStoreShoppe).attr("select-all", item.selectAllAttr);
            }
            $("label", liStoreShoppe).append(checkboxStoreShoppe).after(item.shopName);
            $(ulShowShoppeDom).find("div[class='divLiList']").append(liStoreShoppe);
            $(ulSelectShoppeDom).find("div[class='divLiList']").append(liStoreShoppe.clone());
        });
        //移除被選擇的物件中全選的項目
        $(ulSelectShoppeDom).find("[select-all]").remove();
        //隱藏新建的項目，要不要顯示由此之後自行決定
        $(ulShowShoppeDom).find("li").hide();
        $(ulSelectShoppeDom).find("li").hide();
    }

    function checkBoxShoppeClickDelegate(item) {
        /*預設給外部帶入方法用*/
    }

    // #region prototype

    ///設定指定店下的櫃位是否顯示
    ///storeGuid:店編號
    ///showDataType:顯示的資料內容(一般櫃or贈品處or快取bar)(groupDataObject.showDataType)
    ///isShow:是否顯示
    SkmDealInfoSelectShoppe.prototype.setShoppesIsShow = function (storeGuid, showDataType, isShow) {
        _this = this;
        let ulShowShoppeDoms = $("#div_" + _this.id + "_" + divTypeArray[0] + " [id^='" + _this.id + "_Store_'][store-guid='" + storeGuid + "']");
        let ulSelectShoppeDoms = $("#div_" + _this.id + "_" + divTypeArray[1] + " [id^='" + _this.id + "_Store_'][store-guid='" + storeGuid + "']");
        if (ulShowShoppeDoms.length <= 0) return;

        for (let i = 0; i < ulShowShoppeDoms.length; i++) {
            if (!$(ulSelectShoppeDoms[i]).find("div[class='divLiList']").text()) {
                generateLiSelectShoppes(ulShowShoppeDoms[i], ulSelectShoppeDoms[i], _this.showDataGroups);
            }
            let liShowShoppeDoms;
            let liSelectShoppeDoms;
            if (showDataType == 0) {
                liShowShoppeDoms = $(ulShowShoppeDoms[i]).find("div[class='divLiList'] li");
                liSelectShoppeDoms = $(ulSelectShoppeDoms[i]).find("div[class='divLiList'] li");
            }
            else {
                liShowShoppeDoms = $(ulShowShoppeDoms[i]).find("div[class='divLiList'] li[data-type='" + showDataType + "']");
                liSelectShoppeDoms = $(ulSelectShoppeDoms[i]).find("div[class='divLiList'] li[data-type='" + showDataType + "']");
            }
            if (isShow) {
                $(ulShowShoppeDoms[i]).show();
                $(ulSelectShoppeDoms[i]).show();
                $(liShowShoppeDoms).attr("show", "show").show();
                $(liSelectShoppeDoms).attr("show", "show");
            }
            else {
                //隱藏時順便取消選擇
                $(ulShowShoppeDoms[i]).hide().find("[type='checkbox']").prop('checked', false);
                $(ulSelectShoppeDoms[i]).hide().find("[type='checkbox']").prop('checked', false);
                $(liShowShoppeDoms).removeAttr("show").hide();
                $(liSelectShoppeDoms).removeAttr("show").hide();
            }
        }
    };

    ///清空所有選擇的櫃位，並設定成不可見
    SkmDealInfoSelectShoppe.prototype.clearAllShoppes = function () {
        _this = this;
        $("#" + _this.id + " [type='checkbox']").prop('checked', false);
        $("#" + _this.id + " li").removeAttr("show").hide();
        $("#" + _this.id + " ul").hide();
    };

    ///設定已選擇的資料
    ///selectedData:已選擇的資料
    ///showDataType:傳入資料的類型
    SkmDealInfoSelectShoppe.prototype.setData = function (selectedData, showDataType) {
        _this = new Object(this);
        if (selectedData == null || selectedData.length <= 0) return;
        let id = _this.id;
        let taget = new Object(this);
        _this.generateDataLogic.groupDataByShopCode(selectedData).forEach(function (store) {
            SkmDealInfoSelectShoppe.prototype.setShoppesIsShow(store.groupData[0].SellerGuid, true, showDataType);
            store.groupData.forEach(function (shoppe) {
                $("[id='" + id + "_" + shoppe.sellerGuid + "']").prop("checked", true);
                _this.shoppeCheckedChange(id + "_" + shoppe.sellerGuid, taget);
            });
        });
    }

    ///取得已選擇櫃位資訊
    SkmDealInfoSelectShoppe.prototype.getSelectShoppeCode = function () {
        var _this = this;
        var returnValue = [];
        var selectAllShoppes = [];
        $.each($("#div_" + _this.id + "_Show [type='checkbox'][select-all]:checked"),
            function (index, shoppe) {
                var item = new Object();
                item.Guid = $(shoppe).val();
                item.ShopCode = $(shoppe).attr("shop-code");
                selectAllShoppes.push(item);
            });
        $("#div_" + _this.id + "_Show [type='checkbox']:checked").each(function (index, shoppe) {
            var item = new Object();
            item.Guid = $(shoppe).val();
            item.ShopCode = $(shoppe).attr("shop-code");
            //若已經選擇全館則不加入同shopcode的資料
            let tempGroupDatas = $.grep(selectAllShoppes, function (selectAllShoppe) {
                return selectAllShoppe.ShopCode == item.ShopCode;
            });
            if (tempGroupDatas.length <= 0) {
                returnValue.push(item);
            }
        });
        $.each(selectAllShoppes,
            function (index, item) {
                returnValue.push(item);
            });
        return returnValue;
    },

    ///給外部設定當按下櫃位checkbox時，需要執行的動作
    SkmDealInfoSelectShoppe.prototype.setShoppeClickDelegate = function (event) { this.checkBoxShoppeClickDelegate = event; };

    ///查詢欄位輸入值事件
    ///DIV id like'Show' 物件ID
    SkmDealInfoSelectShoppe.prototype.searchTextChange = function (id) {
        //#region 這邊很想改掉，但要改動太大，就.....先留著吧
        if (isNotDraft())
            return false;
        var selectStores = [];
        $('.stores:checked').each(function () {
            selectStores.push($(this).val());
        });
        //#endregion
        $("#" + id + " li").hide();
        if (selectStores.length > 0) {
            let serachText = $("#searchShoppes_" + id.replace("div_", "")).val();
            //查詢字如果是空的就恢復狀態
            if (serachText == undefined || serachText == null || serachText == "") {
                for (var i = 0; i < selectStores.length; i++) {
                    $("#" + id + " ul[store-guid='" + selectStores[i] + "'] li[show='show']").show();
                }
            }
            else {
                for (var i = 0; i < selectStores.length; i++) {
                    let shoppes = $("#" + id + " ul[store-guid='" + selectStores[i] + "'] li[show='show']");
                    for (var j = 0; j < shoppes.length; j++) {
                        if ($(shoppes[j]).text().toUpperCase().indexOf(serachText) >= 0) {
                            $(shoppes[j]).show();
                        }
                    }
                }
            }
        }
    }

    ///櫃位選項改變事件
    SkmDealInfoSelectShoppe.prototype.shoppeCheckedChange = function (checkBoxId, taget) {
        if (taget == undefined)
            _this = this;
        else
            _this = taget;
        let clickShoppe = $("#div_" + _this.id + "_" + divTypeArray[0] + " #" + checkBoxId);
        
        //若是選擇的是全選
        if ($(clickShoppe).attr("select-all") != undefined) {
            let shopCode = $(clickShoppe).attr("shop-code");
            if ($(clickShoppe).is(":checked")) {
                $("#div_" + _this.id + "_" + divTypeArray[0] + " ul[shop-code='" + shopCode + "'] li[show='show']").show()
                    .find("[type='checkbox']").prop('checked', true);
                $("#div_" + _this.id + "_" + divTypeArray[1] + " ul[shop-code='" + shopCode + "'] li[show='show']").show()
                    .find("[type='checkbox']").prop('checked', true);
            }
            else {
                $("[id='" + _this.id + "'] ul[shop-code='" + shopCode + "'] [type='checkbox']").prop('checked', false);
                $("#div_" + _this.id + "_" + divTypeArray[1] + " ul[shop-code='" + shopCode + "'] li").hide();
            }
        }
        else {
            if ($(clickShoppe).is(":checked")) {
                $("[id='li_" + checkBoxId + "']").show();
                $("[id='" + checkBoxId + "']").prop('checked', true);
            }
            else {
                $("#div_" + _this.id + "_" + divTypeArray[1] + " [id='li_" + checkBoxId + "']").hide();
                $("[id='" + checkBoxId + "']").prop('checked', false);
            }
        }
        _this.checkBoxShoppeClickDelegate(clickShoppe);
    };

    SkmDealInfoSelectShoppe.prototype.SetAllVisibleShoppeIsChecked = function (isCheck) {
        _this = this;
        if (isCheck) {
            //適用分店下的所有館櫃勾選
            $("#" + _this.id + " li[show='show']").show();
            $("#" + _this.id + " li[show='show'] [type='checkbox']").prop('checked', true);

            //自動勾選適用分店下的憑證數量/成本中心選項
            $('.stores').each(function () {
                if ($(this).attr('checked')) {
                    AutoSetAll70000Shop($(this).val(), true);
                }
            });
        }
        else {
            //取消適用分店下的所有館櫃勾選
            $("[id='" + _this.id + "'] [type='checkbox']").prop('checked', false);
            $("#div_" + _this.id + "_" + divTypeArray[1] + " li").hide();

            //自動取消適用分店下的憑證數量/成本中心選項
            $('.stores').each(function () {
                if ($(this).attr('checked')) {
                    AutoSetAll70000Shop($(this).val(), false);
                }
            });
        }
    }
    SkmDealInfoSelectShoppe.prototype.setCanEdit = function (canEdit) {
        _this = this;
        _this.canEdit = canEdit;
        if (_this.canEdit) {
            $("#" + _this.id + " [type='checkbox']").removeProp("disabled");
        }
        else {
            $("#" + _this.id + " [type='checkbox']").prop("disabled", "disabled");
        }
    }

    SkmDealInfoSelectShoppe.prototype.setSelectStoreUlIsShow = function (selectStoreUlId) {
        /*let selectStoreUlDoms = $("[id='" + selectStoreUlId + "']");
        if (selectStoreUlDoms.length <= 0) return;
        for (var i = 0; i < selectStoreUlDoms.length; i++) {
            let selectStoreUlDom = selectStoreUlDoms[i];
            if ($(selectStoreUlDom).find(".divLiList").is(":visible")) {
                $(selectStoreUlDom).find("p").html($(selectStoreUlDom).find("p").html().replace(new RegExp("[+]2", "g"), "--"));
                $(selectStoreUlDom).find(".divLiList").hide();
            } else {
                $(selectStoreUlDom).find("p").html($(selectStoreUlDom).find("p").html().replace(new RegExp("--", "g"), "++"));
                $(selectStoreUlDom).find(".divLiList").show();
            }
        }*/
    }
    //#endregion

    // #region 以下為依不同資料格式(dataTypeEnum)產生各自的資料處理邏輯
    ///核銷櫃位資料邏輯
    var WriteOffShoppeDataLogic = {
        title: "請從下列清單挑選[核銷櫃位]",

        showDataTypeEnum: function () {
            return {
                "Normal": 1,    //一般櫃
                "Exchange": 2   //贈品處
            };
        },

        ///將要顯示的資料依照shop code分不同群組
        groupDataByShopCode: function (data) {
            let returnShowDataGroups = [];
            let tempGroupDatas;
            let tempGroupData;
            data.forEach(function (item) {
                //查看要回傳的群組資料內有沒有key=此item的shopCode的資料
                tempGroupDatas = $.grep(returnShowDataGroups, function (returnShowDataGroup) {
                    return returnShowDataGroup.key == item.ShopCode;
                });
                //若有就將第一個值放入tempGroupData，沒有就建一個新的tempGroupData並加入returnShowDataGroups
                if (tempGroupDatas.length >= 1) {
                    tempGroupData = tempGroupDatas[0];
                }
                else {
                    tempGroupData = new Object({
                        key: item.ShopCode,
                        marketCode: item.MarketCostCenterCode,
                        sellCode: item.SellCostCenterCode,
                        groupData: []
                    });
                    let tempShoppes = $.grep(data, function (tempData) {
                        return !tempData.IsShoppe && tempData.ShopCode == item.ShopCode;
                    });
                    if (tempShoppes.length > 0) {
                        tempGroupData.name = tempShoppes[0].ShopName;
                    }
                    else {
                        tempGroupData.name = item.ShopName;
                    }
                    returnShowDataGroups.push(tempGroupData);
                }
                //將Item加入tempGroupData的groupData中，此動作會影響到原本returnShowDataGroups的同一個Objcet
                let shoppeName = item.Name;
                if (!item.IsShoppe) {
                    shoppeName = "全館（7000000）";
                }
                let tempData = {
                    storeName: tempGroupData.name,
                    storeGuid: item.SellerGuid,
                    sellerGuid: item.StoreGuid,
                    shopCode: item.ShopCode,
                    shopName: shoppeName,
                    showDataType: item.IsExchange ? "2" : "1",
                    mainShopCode: item.MainShopCode,
                    selectAllAttr: item.IsShoppe ? "" : "select-all"
                };
                tempGroupData.groupData.push(tempData);
            });
            return returnShowDataGroups;
        },

        ///取得館別資訊
        ///shoppeDatas:櫃位分群組資料
        getStoreShopInfo: function (shoppeDatas) {
            let returnValue = [];
            shoppeDatas.forEach(function (item) {
                let tempShoppes = $.grep(item.groupData, function (item) {
                    return item.selectAllAttr == "select-all";
                });
                if (tempShoppes.length > 0) {
                    let subItem = tempShoppes[0];
                    subItem.marketCode = item.marketCode;
                    subItem.sellCode = item.sellCode;
                    returnValue.push(subItem);
                }
            });
            return returnValue;
        },
    };

    ///APP顯示櫃位用資料邏輯
    var ShowShoppeDataLogic = {
        title: "請從下列清單挑選[APP顯示櫃位]",

        showDataTypeEnum: {
            "Normal": 1,    //一般櫃
            "Exchange": 2,  //贈品處
            "Cache": 3,     //快取Bar
        },

        ///將要顯示的資料依照shop code分不同群組
        groupDataByShopCode: function (data) {
            let returnShowDataGroups = [];
            let tempGroupDatas;
            let tempGroupData;
            data.forEach(function (item) {
                //查看要回傳的群組資料內有沒有key=此item的shopCode的資料
                tempGroupDatas = $.grep(returnShowDataGroups, function (returnShowDataGroup) {
                    return returnShowDataGroup.key == item.ShopCode;
                });
                //若有就將第一個值放入tempGroupData，沒有就建一個新的tempGroupData並加入returnShowDataGroups
                if (tempGroupDatas.length >= 1) {
                    tempGroupData = tempGroupDatas[0];
                }
                else {
                    tempGroupData = new Object({
                        key: item.ShopCode,
                        name: item.ShopName,
                        marketCode: "",
                        sellCode: "",
                        groupData: []
                    });
                    returnShowDataGroups.push(tempGroupData);
                }
                //將Item加入tempGroupData的groupData中，此動作會影響到原本returnShowDataGroups的同一個Objcet
                let tempData = {
                    storeName: item.ShopName,
                    storeGuid: item.SellerGuid,
                    sellerGuid: item.Guid,
                    shopCode: item.ShopCode,
                    shopName: item.DisplayName,
                    showDataType: item.ExchangeType,
                    mainShopCode: "",
                    selectAllAttr: ""
                };
                tempGroupData.groupData.push(tempData);
            });
            return returnShowDataGroups;
        },

        ///取得館別資訊
        ///shoppeDatas:櫃位分群組資料
        getStoreShopInfo: function (shoppeDatas) {
            let returnValue = [];
            shoppeDatas.forEach(function (item) {
                var subItem = item.groupData[0];
                returnValue.push(subItem);
            });
            return returnValue;
        },
    };
    // #endregion
    return SkmDealInfoSelectShoppe;
}())
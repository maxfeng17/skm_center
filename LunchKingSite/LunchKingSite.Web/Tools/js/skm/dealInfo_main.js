﻿
//商品主類別切換;子類別reset
function categoryChange(id) {
	$("#subCategorySection-" + id).toggle();
	$(".subCategoryList-" + id).prop('checked', false);
}

function setCkeditor() {
	CKEDITOR.config.removePlugins = 'save';
	CKEDITOR.replace("product_description", { width: '70%', height: '160px' });//,filebrowserImageUploadUrl: imageupload});
	CKEDITOR.replace("product_remark", { width: '70%', height: '160px' });
	CKEDITOR.replace("shoppe_description", { width: '70%', height: '160px' });//filebrowserImageUploadUrl: imageupload});
	CKEDITOR.replace("shoppe_remark", { width: '70%', height: '160px' });
	CKEDITOR.replace("recommend_description", { width: '70%', height: '160px' });//filebrowserImageUploadUrl: imageupload});
	CKEDITOR.replace("buy_description", { width: '70%', height: '160px' });//filebrowserImageUploadUrl: imageupload});
	CKEDITOR.replace("buy_remark", { width: '70%', height: '160px' });
}

//切換然點或兌換價
function burningOrCashChange() {
	var val = $('input[name=burningOrCash]:checked').val();

	var productIntroduction;
	if (val === "Point" || val === "PointAndCash") {
		productIntroduction = "• 扣點商品:\n1. 點數一經兌換恕不得 退還或更換其他品項。\n2. 優惠限使用一次，不可重複使用。";
	} else {
		productIntroduction = "• 兌換價商品:\n優惠限使用一次，不可重複使用。";
	}

	//有guid代表狀態為編輯商品建檔
	if (location.search.indexOf("guid") === -1) {
		//預設，兌換說明
		if (!$("input[id=isSkmPay]").is(":checked")) {
			$('#product_introduction').text(productIntroduction);
			$('#shoppe_introduction').text(productIntroduction);
			$('#buy_introduction').text(productIntroduction);
			getCharLength($('#product_introduction'));
			getCharLength($('#shoppe_introduction'));
			getCharLength($('#buy_introduction'));
		}
	} else {
		//非預設，編輯狀態不能再修改是否扣點radio
		if ((val === "Point" || val === "PointAndCash") && !$("input[id=isSkmPay]").is(":checked")) {
			$(".stores").attr("disabled", "disabled");
			$("#orderDateS").attr("disabled", "disabled");
			$("#orderTimeS").attr("disabled", "disabled");
			$("#orderDateE").attr("disabled", "disabled");
			$("#orderTimeE").attr("disabled", "disabled");
			$("#deliverDateS").attr("disabled", "disabled");
			$("#deliverTimeS").attr("disabled", "disabled");
			$("#deliverDateE").attr("disabled", "disabled");
			$("#deliverTimeE").attr("disabled", "disabled");
			$("#orderDateS").attr("style", "background-color:#EBEBE4");
			$("#orderTimeS").attr("style", "background-color:#EBEBE4");
			$("#orderDateE").attr("style", "background-color:#EBEBE4");
			$("#orderTimeE").attr("style", "background-color:#EBEBE4");
			$("#deliverDateS").attr("style", "background-color:#EBEBE4");
			$("#deliverTimeS").attr("style", "background-color:#EBEBE4");
			$("#deliverDateE").attr("style", "background-color:#EBEBE4");
			$("#deliverTimeE").attr("style", "background-color:#EBEBE4");
			$("input[id=setTheSameTime]").attr("disabled", "disabled");
		}

		if (!$("input[id=isSkmPay]").is(":checked") || ($("input[id=isSkmPay]").is(":checked") && $("input[id=isAllowEdit]").val() === "False")) {
			$('input[name=burningOrCash]').attr("disabled", "disabled");
			$(".burningPoint").attr("disabled", "disabled");
			$(".burningPointAndCashP").attr("disabled", "disabled");
			$("#divBurningCash").find(".activeType").attr("disabled", "disabled");
		}
		$("#divactiveType").find(".activeType").attr("disabled", "disabled");
		$("input[id=isSkmPay]").attr("disabled", "disabled");
		$(".exchangeNo").attr("disabled", "disabled");
		$(".exchangeNo").attr("style", "background-color:#EBEBE4");
		$("input[id=giftWalletId]").attr("disabled", "disabled");
		$("input[id=giftWalletId]").attr("style", "background-color:#EBEBE4");
		$("input[id=giftPoint]").attr("disabled", "disabled");
		$("input[id=giftPoint]").attr("style", "background-color:#EBEBE4");
		$(".orderTotalLimit").attr("disabled", "disabled");
		$("input[name=activityStatus]").attr("disabled", "disabled");
		$("#costCenterTable").find(":input").attr("disabled", "disabled");
		$("#costCenterTable").find(":input").attr("style", "background-color:#EBEBE4");
	}

	burningOrCashDisplay(val);
	checkSkmPayCheck();
}

function burningOrCashDisplay(val) {
	switch (val) {
		case "Point":
			$('.burningPointAndCashC').val(0);
			$('#div_settlement').hide();
			break;
		case "PointAndCash":
			$('.exchangeNoDiv').show();
			$('.costCenterAllDiv').show();
			$('.activityStatusDiv').show();
			$('#div_settlement').hide();
			$('.burningCash').val(0);
			break;
		case "Cash":
			$('.exchangeNoDiv').hide();
			$('.costCenterAllDiv').hide();
			$('.activityStatusDiv').hide();
			$('#div_settlement').show();
			$('.burningPoint').val(0);
			$(".burningPointAndCashP").val(0);
			break;
	}

	if ($("input[id=isSkmPay]").is(":checked")) {
		$('#div_settlement').hide();
		$(".exchangeNoDiv").hide();
		$('.costCenterAllDiv').hide();
		$('.activityStatusDiv').hide();
	} else {
		if ($("input[name=burningOrCash]:checked").val() !== "Cash") {
			$(".exchangeNoDiv").show();
			$('.costCenterAllDiv').show();
			$('.activityStatusDiv').show();
		}
	}
}

function productCodeNotFound(message) {
	alert(message);
	$('.actionBtn').hide();
	$('.selectedItemNo').val('');
	$('.selectedItemName').val('');
	$('.selectedItemNo').removeClass("selectedItemNo");
	$('.selectedItemName').removeClass("selectedItemName");
	stop();
}

function addBuyButtonClick() {
	$('.buyDel').each(function () {
		$('.buyDel').on('click', function () {
			var divCount = $('.buyDel').length;
			if (divCount > 1) {
				$(this).parent($(".div_buy_item")).remove();
			}
		});
	});
}

function addFreeButtonClick() {
	$('.freeDel').each(function () {
		$('.freeDel').on('click', function () {
			var divCount = $('.freeDel').length;
			if (divCount > 1) {
				$(this).parent($(".div_free_item")).remove();
			}
		});
	});
}

function sameDateTime(obj) {
	var checked = $(obj).is(":checked");
	if (checked) {
		$('#deliverDateS').val($('#orderDateS').val());
		$('#deliverTimeS').val($('#orderTimeS').val());
		$('#deliverDateE').val($('#orderDateE').val());
		$('#deliverTimeE').val($('#orderTimeE').val());
	}
}

function onSelectAll() {
	//適用分店列表
	var sellers = [];
	$('.stores:checked')
		.each(function () {
			sellers.push($(this).val());
		});

	if (sellers.length > 0) {
		//分店對應全館
		$('.hdShop').each(function () {
			var hdseller = $(this).val();
			if ($.inArray(hdseller, sellers) > -1) {
				$(this).next().next().find('input').attr('checked', true);
				$(this).next().next().show();


				//成本中心
				var isSupermarket = $(this).text().match("超市") !== null;
				var shopeName = $(this).parent().find('.allShoppes').prev().attr('value');
				var shopeCode = $(this).parent().find('.allShoppes').prev().attr('id');
				var marketCostCenterCodeId = $(this).parent().find('.marketCostCenterCode').val();
				var sellCostCenterCodeId = $(this).parent().find('.sellCostCenterCode').val();
				var selected = "";
				var costCenterExisted = getCostCenterExisted(shopeCode);
				var storeQtyExisted = getStoreQtyExisted(shopeCode);

				if (costCenterExisted === false) {
					if (!isSupermarket) {
						selected = "<option value='" +
							sellCostCenterCodeId +
							"'  selected='true'>" +
							leftPad(4, shopeCode) +
							"-行銷</option>";
						if (marketCostCenterCodeId !== "") {
							selected += "<option value='" +
								marketCostCenterCodeId +
								"'>" +
								leftPad(4, shopeCode) +
								"-超市事業部</option>";
						}
					} else {
						selected = "<option value='" +
							sellCostCenterCodeId +
							"'>" +
							leftPad(4, shopeCode) +
							"-行銷</option>" +
							"<option value='" +
							marketCostCenterCodeId +
							"' selected='true'>" +
							leftPad(4, shopeCode) +
							"-超市事業部</option>";
					}

					var html = "<tr class='costCenterli' value='" +
						shopeCode +
						"'><td> " +
						shopeName +
						"</td><td><select style='width:150px'>" +
						selected +
						"</select></td><td>" +
						"<input type='text' maxLength='12' placeholder='請輸入內部訂單編號,最多12碼' onblur='padLeft(this)' style='width:190px'>" +
						"</td><tr/>";
					$("#costCenterTable").append(html);
				}

				if (storeQtyExisted === false) {
					genStoreQtyHtml(shopeCode, shopeName, 0);
				}
			}

		});
		//分店對應櫃位
		$('.shoppesItem').each(function () {
			var hdseller = $(this).parent().find('input').eq(0).val();
			if ($.inArray(hdseller, sellers) > -1) {
				$(this).find('input').attr('checked', true);
				$(this).show();
			}
		});
		//分店對應以選擇櫃位
		$('.shoppesItemSelected').each(function () {
			var hdseller = $(this).parent().find('input').eq(0).val();
			if ($.inArray(hdseller, sellers) > -1) {
				$(this).find('input').attr('checked', true);
				$(this).show();
			}
		});
	}

	//不包含贈品櫃
	$('.isExchange').hide();
	$('.isExchangeSelected').hide();
	$('.isExchange').find('input').attr('checked', false);
	$('.isExchangeSelected').find('input').attr('checked', false);
}

function onSelectCounter() {
	var sellers = [];
	$('.stores:checked')
		.each(function () {
			sellers.push($(this).val());
		});

	if (sellers.length > 0) {
		//分店對應全館
		$('.hdShop')
			.each(function () {
				var hdseller = $(this).val();
				if ($.inArray(hdseller, sellers) > -1) {
					$(this).next().next().show();
				}
			});
		//分店對應櫃位
		$('.shoppesItem')
			.each(function () {
				var hdseller = $(this).parent().find('input').eq(0).val();
				if ($.inArray(hdseller, sellers) > -1) {
					$(this).show();
				}
			});

	}
	//不包含贈品櫃
	$('.isExchange').hide();
}

function onSelectExchange() {
	//隱藏非贈品櫃
	$('.allShoppes').hide();
	$('.shoppesItem').hide();
	loadExchange();
}

function loadExchange() {
	var sellers = [];
	$('.stores:checked')
		.each(function () {
			sellers.push($(this).val());
		});

	if (sellers.length > 0) {
		//分店對應贈品櫃位
		$('.isExchange')
			.each(function () {
				var hdseller = $(this).parent().find('input').eq(0).val();
				if ($.inArray(hdseller, sellers) > -1) {
					$(this).show();
				}
			});
	}
}

function onSetIsSelectAll() {
	let isSelectAll = $("#IsSelectAll").prop("checked");
	if (isSelectAll) {
		onSelectAll();
	}
	else {
		onSelectCounter();
	}
}

function onSetIsUseShowName() {
	let isUseShowName = $("#IsUseShowName").prop("checked");
	if (isUseShowName) {
		$("#appShowStoreObjects").show();
	}
	else {
		$("#appShowStoreObjects").hide();
		appShowStoreObjects.clearAllShoppes();
	}
}

function getCostCenterExisted(shopCode) {
	var result = false;
	$("#costCenterTable").find(".costCenterli").each(function () {
		if ($(this).attr("value") === shopCode) {
			result = true;
		}
	});
	return result;
}

function getStoreQtyExisted(shopCode) {
	var result = false;
	$("#storesQty").find(".sqp").each(function () {
		if ($(this).attr("value") === shopCode) {
			result = true;
		}
	});
	return result;
}

function genStoreQtyHtml(shopeCode, shopeName, qty) {

	shopeName = shopeName.replace("全館", "");
	var storeQty = "<p id='sqp" + shopeCode + "' class='sqp' value='" + shopeCode + "'>" + shopeName + "&nbsp;<input id='storeQty' class='storeQty" + shopeCode + "' type='number' min='0' value='" + qty + "' style='width:100px' />&nbsp;筆</p>";
	if ($("#storesQty").find(".sqp").length > 0 && ($("#storesQty").find(".sqp").length + 1) % 2 === 0) {
		storeQty += "<br />";
	}
	$("#storesQty").append(storeQty);
}

function genCostCenterHtml(shopeCode, shopeName, mainShopCode, selected) {
	if (selected !== "" && location.search.indexOf("guid") === -1) {
		var html =
			"<tr class='costCenterli' value='" + shopeCode + "'>" +
			"<td style='width:170px'>" +
			"<label>" + shopeName + "</label>" +
			"<input type='hidden' value='" + mainShopCode + "' id='mainStoreCode' />" +
			"</td>" +
			"<td>" +
			"<select style='width:150px;margin-top: 3px;margin-bottom: 0px;'>" +
			selected +
			"</select>" +
			"</td>" +
			"<td>" +
			"<input type='text' maxLength='12' placeholder='請輸入內部訂單編號,最多12碼' onblur='padLeft(this)' style='width:190px'>" +
			"</td>" +
			"<tr/>";
		$("#costCenterTable").append(html);
	}
}

function genCostCenterSelected(isSupermarket, sellCostCenterCodeId, shopeCode, marketCostCenterCodeId) {
	if (location.search.indexOf("guid") !== -1) {
		return "";
	}
	if (!isSupermarket) {
		var selected = "<option value='" + sellCostCenterCodeId + "'  selected='true'>" + leftPad(4, shopeCode) + "-行銷</option>";
		if (marketCostCenterCodeId !== "") {
			selected += "<option value='" + marketCostCenterCodeId + "'>" + leftPad(4, shopeCode) + "-超市事業部</option>";
		}
		return selected;
	}
	else {
		return "<option value='" + sellCostCenterCodeId + "'>" + leftPad(4, shopeCode) + "-行銷</option>" +
			"<option value='" + marketCostCenterCodeId + "' selected='true'>" + leftPad(4, shopeCode) + "-超市事業部</option>";
	}
}

function removeCostCenterAndStoreQty(costCenterExisted, storeQtyExisted, anyChecked, shopCode) {
	//無選取則刪除成本中心
	if (!anyChecked) {
		if (costCenterExisted && location.search.indexOf("guid") === -1) {
			$(".costCenterli").each(function () {
				if ($(this).attr("value") === shopCode) {
					$(this).remove();
				}
			});
		}

		if (storeQtyExisted) {
			$(".sqp").each(function () {
				if ($(this).attr("value") === shopCode) {
					$(this).next("br").remove();
					$(this).remove();
				}
			});
		}
	}
}

//先作指定商品優惠
function alibabaCheckUmallProductCode(productCode) {
    if ($('input[name=activeType]:checked').val() !== $("input[id=activeTypeBuyGetFree]").val() &&
        $("input[id=enableQueryProductCode]").val() === "True") {
        //var productCode = $(this).val();
        var dataCode = {
            "code": $('.active:visible').find('.productCode').val(),
            "bForAli":true
        };
        $.ajax({
            url: "/SKMDeal/GetProductCode",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(dataCode),
            success: function (data) {
                if (data.Result === $("input[id=SkmItemNoApiResultSuccess]").val()) {
                    var obj = jQuery.parseJSON(data.Message);
                    if ($("input:checked").val() !== $("input[id=activeTypeBuyGetFree]").val()) {
                        var errormsg = "";
                        if ($('.active:visible').find('.productCode').val() !== obj.PLU_NO) {
                            errormsg = "ProductCode is wrong."
                        }
                        if ($('.active:visible').find('.itemNo').val() !== obj.GOODS_NO) {
                            errormsg = "ItemNo is wrong."

                        }
                        if (errormsg !== "") {
                            $("#saveBtn").remove();
                            alert(errormsg);
                        }

                    } else {
                        if (data.Message.length > 0) {
                            alert(data.Message);
                        }
                        $("#saveBtn").remove();
                    }
                } else {
                    $("#saveBtn").remove();
                    if (data.Message.length > 0) {
                        alert(data.Message);
                    } else {
                        alert("Got nothing from api of checking [ProductCode]");
                    }
                }
            }, error: function (xhr) {
                $("#saveBtn").remove();
                alert("It's failed to call api of checking [ProductCode]");
            }
        });
    }
}

function getNextInput(productCode, buyGetFreeType, resultSuccessCode, skmDealTypesPromotionalStr) {
	$(productCode).nextAll('input:text').first().addClass('selectedItemNo');
	$(productCode).nextAll('input:text').first().nextAll('input:text').first().addClass('selectedItemName');
	checkProductCode(buyGetFreeType, resultSuccessCode, skmDealTypesPromotionalStr);
}

function checkProductCode(buyGetFreeType, resultSuccessCode, skmDealTypesPromotionalStr) {
	if ($("input[name=activeType]:checked").val() === buyGetFreeType) {
		$(".productCode").focusout(function () {
			var productCode = $(this).val();
			var data = {
                "code": productCode,
                "bForAli":false
			};
			$.ajax({
				url: "/SKMDeal/GetProductCode",
				type: "POST",
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(data),
				success: function (data) {
					if (data.Result === resultSuccessCode) {
						var obj = jQuery.parseJSON(data.Message);
						$(".selectedItemNo").val(obj.GOODS_NO);
						$(".selectedItemName").val(obj.GOODS_NM);
						$(".selectedItemNo").removeClass("selectedItemNo");
						$(".selectedItemName").removeClass("selectedItemName");
						$(".actionBtn").show();

					} else {
						if (data.Message.length > 0) {
							productCodeNotFound(data.Message);
						} else {
							if (productCode === "" &&
								$(".btn-primary").attr("name") === skmDealTypesPromotionalStr) {
								$(".actionBtn").hide();
							} else {
								$(".actionBtn").show();
							}
						}
					}
				}
			});
		});
	}
}

function checkData() {
	var parent = $('.active:visible');
	if ($('.beaconType:checked').val() === $("input[id=beaconTypeCustomer]").val()) {
		if ($.trim($("#beaconMessage").val()) === "") {
			alert("請輸入Beacon自訂訊息");
			return false;
		}
	}

	var discountVal = 0;
	if ($("input[name=discountG]:checked").val() === "2") {
		discountVal = parseInt($("input[name=discountG]:checked").parent().find(".discount").val());
		if (discountVal < 1) {
			alert("請填寫折抵現金金額");
			$("input[name=discountG]:checked").parent().find(".discount").focus();
			return false;
		}
		if ($("input[name=discountG]:checked").parent().find(".discount").val().indexOf(".", 0) > -1) {
			alert("折抵現金請填寫整數");
			$("input[name=discountG]:checked").parent().find(".discount").focus();
			return false;
		}
	}

	if ($("input[name=discountG]:checked").val() === "3") {
		discountVal = parseInt($("input[name=discountG]:checked").parent().find(".discount").val());
		if (discountVal < 1 || discountVal > 100) {
			alert("折數填寫錯誤 1 ≦ 折數 ≦ 100");
			$("input[name=discountG]:checked").parent().find(".discount").focus();
			return false;
		}
		if ($("input[name=discountG]:checked").parent().find(".discount").val().indexOf(".", 0) > -1) {
			alert("折數請填寫整數");
			$("input[name=discountG]:checked").parent().find(".discount").focus();
			return false;
		}
	}

	if ($('input[name=activeType]:checked').val() !== $("input[id=activeTypeGiftCertificate]").val() &&
		$('input[name=activeType]:checked').val() !== $("input[id=activeTypeCashCoupon]").val()) {
		if (parent.find('.productCode').val() === '' && $('.btn-primary').attr('name') === $("input[id=skmPromotional]").val()) {
			alert('必須輸入商品條碼');
			return false;
		}
	}

	if (!$("input[id=isSkmPay]").is(":checked") && ($('input[name=burningOrCash]:checked').val() === "Point" || $('input[name=burningOrCash]:checked').val() === "PointAndCash")
		&& parent.find('.exchangeNo').val() === '') {
		alert('必須輸入贈品編號');
		return false;
	}

	var orderDtS = $('#orderDateS').val() + ' ' + $('#orderTimeS').val();
	var orderDtE = $('#orderDateE').val() + ' ' + $('#orderTimeE').val();
	if (new Date(orderDtS).getTime() > new Date(orderDtE).getTime()) {
		alert('截止日期需大於起始日期');
		return false;
	}

	//注目熱賣 orderdt = deliverydt
	if ($("#div_deliver").is(":hidden")) {
		$('#deliverDateS').val($('#orderDateS').val());
		$('#deliverTimeS').val($('#orderTimeS').val());
		$('#deliverDateE').val($('#orderDateE').val());
		$('#deliverTimeE').val($('#orderTimeE').val());
	}
	var deliverDtS = $('#deliverDateS').val() + ' ' + $('#deliverTimeS').val();
	var deliverDtE = $('#deliverDateE').val() + ' ' + $('#deliverTimeE').val();
	if (new Date(deliverDtS).getTime() > new Date(deliverDtE).getTime()) {
		alert('截止日期需大於起始日期');
		return false;
	}

	if (new Date(orderDtS).getTime() > new Date(deliverDtS).getTime()) {
		alert('憑證有效起始時間設定早於檔次上架起始時間');
		return false;
	}


	if (new Date(orderDtE).getTime() > new Date(deliverDtE).getTime()) {
		alert('憑證有效結束時間設定早於檔次上架結束時間');
		return false;
	}

	var category = [];
	$('.category input').each(function (idx, obj) {
		if ($(obj).is(":checked")) {
			category.push($(this).val());
		}
	});

	if (category.length <= 0) {
		alert('分類設定至少要勾選一個');
		return false;
	}
	var tags = [];
	parent.find('.tag input').each(function (idx, obj) {
		if ($(obj).is(":checked")) {
			tags.push($(this).val());
		}
	});
	if (tags.length <= 0) {
		alert('頁簽設定至少要勾選一個');
		return false;
	}
	if ($(".stores").is(":checked") === false) {
		alert('適用分店至少要勾選一個');
		return false;
	}

	if ($('input[name=activeType]:checked').val() !== $("input[id=activeTypeGiftCertificate]").val() &&
		$('input[name=activeType]:checked').val() !== $("input[id=activeTypeCashCoupon]").val()) {
		if ($('#hidAppDealPic').val() === "" && $('#hidAppDealPicOld').val() === "") {
			alert('商品圖未選擇');
			return false;
		}
	}

	if ($('#settlementTime').val() === "" && $("input[name='settlemnet']:checked").val() === "1") {
		alert('清算時間未填寫');
		return false;
	}
	if ($('#orderDateS').val() === "" || $('#orderDateE').val() === "") {
		alert('檔次上架時間未填寫');
		$('#div_deliver').show();
		return false;
	}
	if ($('#deliverDateS').val() === "" || $('#deliverDateE').val() === "") {
		alert('憑證有效時間未填寫');
		return false;
	}

	$.each(parent.find('.discount'), function (n) {
		var d = $(parent.find('.discount')[n]).val();
		if (d > 0) {
			discount = d;
		}
	});


	var selectShoppes = [];
	$.each(writeOffStoreObjects.getSelectShoppeCode(),
		function (index, item) {
			selectShoppes.push(item.Guid);
		}
	);

	//電子贈品與電子抵用金預設選適用分店就自動選全館, 固然不用檢查店鋪
	if ($('input[name=activeType]:checked').val() != $("input[id=activeTypeGiftCertificate]").val()
		&& $('input[name=activeType]:checked').val() != $("input[id=activeTypeCashCoupon]").val()
		&& selectShoppes.length <= 0) {
		alert('店鋪至少要勾選一個');
		return false;
	}

	if (!$('input[name=burningOrCash]:checked').val()) {
		alert('請選擇是否扣點');
		return false;
	}

	if ($('input[name=burningOrCash]:checked').val() === "Point" && (parseInt($('.burningPoint').val()) <= 0 || $('.burningPoint').val() === "")) {
		alert('扣點點數不可為0');
		return false;
	}

	if ($('input[name=burningOrCash]:checked').val() === "PointAndCash" && (parseInt($('.burningPointAndCashP').val()) <= 0 || $('.burningPointAndCashP').val() === "")) {
		alert('扣點點數不可為0');
		return false;
	}

	if ($('input[name=activeType]:checked').val() === $("input[id=activeTypeProduct]").val()
		&& $('input[name=burningOrCash]:checked').val() === "PointAndCash" && (parseInt($('.burningPointAndCashC').val()) <= 0 || $('.burningPointAndCashC').val() === "")) {
		alert('兌換金額不可為0');
		return false;
	}

	if ($('input[name=activeType]:checked').val() === $("input[id=activeTypeProduct]").val()
		&& $('input[name=burningOrCash]:checked').val() === "Cash" && (parseInt($('.burningCash').val()) <= 0 || $('.burningCash').val() === "")) {
		alert('兌換金額不可為0');
		return false;
	}

	if (($('input[name=burningOrCash]:checked').val() === "Point" || $('input[name=burningOrCash]:checked').val() === "PointAndCash") && $('input[name=activityStatus]:checked').val() === null) {
		alert('請勾選活動結案狀態');
		return false;
	}

	if ($("input[name=RepeatPurchaseType]:checked").val() > 0) {
		if ($("#repeatPurchaseCount" + $("input[name=RepeatPurchaseType]:checked").val()).val() <= 0) {
			alert('限購數量不可小於/等於0');
			return false;
		}
	}

	return true;
}

function burningTimes(x) {
	//判斷分店燒點是否為十的倍數
	if ($("input[id=isSkmHqPower]").val() === 'False') {
		if ($(x).val() % 10 !== 0) {
			$(x).val(0);
			alert("燒點點數須為10的倍數");
		}
	}
}

function getData() {
	var parent = $('.active:visible');
	var chk = new Array();
	$.each($("#divLocation input"),
		function (idx, obj) {
			if ($(obj).is(":checked")) {
				chk.push($(this).val());
			}
		});

	var shoppes = new Array();
	//為電子贈品/電子抵用金模式
	if ($('input[name=activeType]:checked').val() == $("input[id=activeTypeGiftCertificate]").val() ||
		$('input[name=activeType]:checked').val() == $("input[id=activeTypeCashCoupon]").val()) {
		if ($('#chk_All').is(":checked")) {//勾全選
			all70000Shop.forEach(function (element) {
				shoppes.push(element.StoreGuid);
			});
		} else {//只選擇幾家店
			all70000Shop.forEach(function (element) {
				if (chk.indexOf(element.SellerGuid) != -1) {
					shoppes.push(element.StoreGuid);
				}
			});
		}
	} else {//一般正常指定商品優惠模式
		$.each(writeOffStoreObjects.getSelectShoppeCode(),
			function (index, item) {
				shoppes.push(item.Guid);
			}
		);
	}

	var category = [];
	$('.category input').each(function (idx, obj) {
		if ($(obj).is(":checked")) {
			category.push($(this).val());
		}
	});

	var tags = [];
	parent.find('.tag input')
		.each(function (idx, obj) {
			if ($(obj).is(":checked")) {
				tags.push($(this).val());
			}
		});

	var discountType;
	$.each(parent.find('.discountType'),
		function (n) {
			var d = $(parent.find('.discountType')[n]);
			if (d.attr('checked')) {
				discountType = d.val();
			}
		});

	var discount = 0;
	if ($('input:radio[name="burningOrCash"]:checked').val() === "Cash"
		&& $('input[name=activeType]:checked').val() === $("input[id=activeTypeProduct]").val()) {
		discount = $('.burningCash').val();
	}
	else if ($('input:radio[name="burningOrCash"]:checked').val() === "PointAndCash" &&
		$('input[name=activeType]:checked').val() === $("input[id=activeTypeProduct]").val()) {
		discount = $('.burningPointAndCashC').val();
	}
	else {
		$.each(parent.find('.discount'),
			function (n) {
				var d = $(parent.find('.discount')[n]).val();
				if (d > 0) {
					discount = d;
				}
			});
	}

	var activeType = $('.activeType:checked').val();
	var remark = "";
	var description = "";
	var introduction = "";

	if (activeType == $("input[id=activeTypeProduct]").val()) {
		remark = CKEDITOR.instances.product_remark.getData();
		description = CKEDITOR.instances.product_description.getData();
		introduction = $('#product_introduction').val();
	} else if (activeType == $("input[id=activeTypeShoppe]").val()) {
		remark = CKEDITOR.instances.shoppe_remark.getData();
		description = CKEDITOR.instances.shoppe_description.getData();
		introduction = $('#shoppe_introduction').val();
	} else if (activeType == $("input[id=activeTypeBuyGetFree]").val()) {
		remark = CKEDITOR.instances.buy_remark.getData();
		description = CKEDITOR.instances.buy_description.getData();
		introduction = $('#buy_introduction').val();
		discountType = $("input[id=discountTypeDiscountBuyGetFree]").val();
	} else {
		remark = CKEDITOR.instances.product_remark.getData();
		description = CKEDITOR.instances.product_description.getData();
		introduction = $('#product_introduction').val();
	}

	var productCodeBuy = [];
	$('.div_buy_item .productCode').each(function () {
		if ($(this).val() !== '') {
			productCodeBuy.push($(this).val());
		}
	});
	var itemNoBuy = [];
	$('.div_buy_item .itemNo').each(function () {
		if ($(this).val() !== '') {
			itemNoBuy.push($(this).val());
		}
	});
	var itemNameBuy = [];
	$('.div_buy_item .itemName').each(function () {
		if ($(this).val() !== '') {
			itemNameBuy.push($(this).val());
		}
	});
	var relationItem = [];
	var lenBuy = productCodeBuy.length;
	for (var i = 0; i < lenBuy; i++) {
		relationItem.push({
			ProductCode: productCodeBuy[i],
			ItemNo: itemNoBuy[i],
			ItemName: itemNameBuy[i],
			IsFree: false
		});
	}

	var productCodeFree = [];
	$('.div_free_item .productCode').each(function () {
		if ($(this).val() !== '') {
			productCodeFree.push($(this).val());
		}
	});
	var itemNoFree = [];
	$('.div_free_item .itemNo').each(function () {
		if ($(this).val() !== '') {
			itemNoFree.push($(this).val());
		}
	});
	var itemNameFree = [];
	$('.div_free_item .itemName').each(function () {
		if ($(this).val() !== '') {
			itemNameFree.push($(this).val());
		}
	});

	var lenFree = productCodeFree.length;
	for (var j = 0; j < lenFree; j++) {
		relationItem.push({
			ProductCode: productCodeFree[j],
			ItemNo: itemNoFree[j],
			ItemName: itemNameFree[j],
			IsFree: true
		});
	}

	$.urlParam = function (name) {
		var results = new RegExp("[\?&]" + name + "=([^&#]*)").exec(window.location.href);
		if (results === null) {
			return null;
		} else {
			return results[1] || 0;
		}
	};

	//是否燒點
	var burningOrCash = $('input:radio[name="burningOrCash"]:checked').val();
	var burningPoint = 0;
	var isBurningEvent;
	switch (burningOrCash) {
		case "Point":
		case "PointAndCash":
			isBurningEvent = true;
			burningPoint = $('.burningPoint').val();
			if (burningPoint === "" || burningPoint === "0") {
				burningPoint = $('.burningPointAndCashP').val();
			}
			break;
		case "Cash":
			isBurningEvent = false;
			break;
	}

	if ($("input[id=isSkmPay]").is(":checked")) {
		isBurningEvent = false;
	}

	//成本中心
	var costCenterList = [];
	$('.costCenterli').each(function () {
		if ($(this).attr('value') !== '') {
			var costCenter = {};
			costCenter.StoreName = $(this).find("label").text();
			costCenter.StoreCode = $(this).attr('value');
			costCenter.SelectCenterCode = $(this).find('select').val();
			if ($(this).find('option')[1] !== undefined) {
				costCenter.MarketCostCenterCode = $(this).find('option')[1].value;
			}
			costCenter.SellCostCenterCode = $(this).find('option')[0].value;
			costCenter.OrderNo = $(this).find('input').val();
			costCenterList.push(costCenter);
		}
	});

	//館別憑證數
	var comboDeals = [];
	$(".sqp").each(function () {
		if ($(this).attr('value') !== "") {
			var cd = { ShopCode: $(this).attr('value'), ShopName: $(this).text().replace("筆", "").trim(), Qty: $(this).find('input[id=storeQty]').val() };
			comboDeals.push(cd);
		}
	});

	//贈品編號
	var exchangeNo = "";
	$.each(parent.find('.exchangeNo'), function (n) {
		var e = $(parent.find('.exchangeNo')[n]).val();
		if (e > 0) {
			exchangeNo = e;
		}
	});
	var oriPrice = parent.find('.itemOrigPrice').val();
	var skmoriPrice = parent.find('.skmOrigPrice').val();
	var freeVal = parent.find('.free').val();
	var buyVal = parent.find('.buy').val();

	if (oriPrice === undefined) {
		oriPrice = "0";
	}
	if (skmoriPrice === undefined) {
		skmoriPrice = "0";
	}
	if (freeVal === undefined) {
		freeVal = "0";
	}
	if (buyVal === undefined) {
		buyVal = "0";
	}

	//限購數量
	let repeatPurchaseCount = 1;
	if ($("input[name=RepeatPurchaseType]:checked").val() != undefined) {
		repeatPurchaseCount = $("#repeatPurchaseCount" + $("input[name=RepeatPurchaseType]:checked").val()).val();
	}

	var data = {
		"Guid": $('#dealGuid').val(),
		"ActiveType": activeType, //$('.activeType:checked').val(), //$('.btn-primary').attr('name'),
		"Title": parent.find('.title').val(),
		"ProductCode": parent.find('.productCode').val(),
		"ItemNo": parent.find('.itemNo').val(),
		"SpecialItemNo": $('.SpecialItemNo').val(), //parent.find('.SpecialItemNo').val(),
		"ItemOrigPrice": parseInt(oriPrice),
		"SkmOrigPrice": parseInt(skmoriPrice),
		"DiscountType": discountType,
		"Discount": discount,
		"OrderTotalLimit": parent.find('.orderTotalLimit').val(),
		"Remark": encodeURIComponent(remark),
		"Description": encodeURIComponent(description),
		"Introduction": encodeURIComponent(introduction),
		"Category": category,
		"Tags": tags,
		"BusinessHourOrderDateS": $('#orderDateS').val(),
		"BusinessHourOrderDateE": $('#orderDateE').val(),
		"BusinessHourOrderTimeS": $('#orderTimeS').val(),
		"BusinessHourOrderTimeE": $('#orderTimeE').val(),
		"BusinessHourDeliverDateS": $('#deliverDateS').val(),
		"BusinessHourDeliverDateE": $('#deliverDateE').val(),
		"BusinessHourDeliverTimeS": $('#deliverTimeS').val(),
		"BusinessHourDeliverTimeE": $('#deliverTimeE').val(),
		"SettlementTime": $('#settlementTime').val(),
		"Stores": chk,
		"Shoppes": shoppes,
		"BeaconType": $('.beaconType:checked').val(),
		"BeaconMessage": $('#beaconMessage').val(),
		"ImagePath": $('#hidImagePath').val() === "" || $('#hidTempImagePath').val() === ""
			? $('#hidImagePathOld').val()
			: $('#hidImagePath').val(),
		"AppDealPic": $('#hidAppDealPic').val() === "" || $('#hidTempAppDealPic').val() === ""
			? $('#hidAppDealPicOld').val()
			: $('#hidAppDealPic').val(),
		"TempAppDealPic": $('#hidTempAppDealPic').val(),
		"TempImagePath": $('#hidTempImagePath').val(),
		"Buy": buyVal,
		"Free": freeVal,
		"RelationItemJsonList": JSON.stringify(relationItem),
		"SourceSeller": $.urlParam('seller'),
		"DealVersion": 2,
		"IsBurningEvent": isBurningEvent,
		"BurningPoint": burningPoint,
		"ExchangeNo": exchangeNo,
		"CostCenterTemp": JSON.stringify(costCenterList),
		"ActivityStatus": $('input[name=activityStatus]:checked').val(),
		"IsHqDeal": $("input[id=isHqDeal]").val(),
		"IsPrize": $("input[id=isPrize]").val(),
		"Status": $("input[id=skmDealStatus]").val(),
		"DealIconId": $("input[name=dealIconStyle]:checked").val(),
		"ExhibitionData": $("input[id=exhibitionData]").val(),
		"IsCrmDeal": $("input[id=isCrmDeal]").prop('checked'),
		"IsSkmPay": $("input[id=isSkmPay]").prop("checked"),
		"IsRepeatPurchase": $(".repeatPurchase").is(":visible") && ($("input[id=isRepeatPurchase]").is(":checked") || $("input[name=RepeatPurchaseType]:checked").val() == "0") ? true : false,
		"RepeatPurchaseType": $("input[name=RepeatPurchaseType]:checked").val() == undefined ? ($("input[id=isRepeatPurchase]").is(":checked") ? "0" : "2") : $("input[name=RepeatPurchaseType]:checked").val(),
		"RepeatPurchaseCount": repeatPurchaseCount,
		"ComboDealTemp": JSON.stringify(comboDeals),
		"TaxRate": $("#tax_rate").val(),
		"IsCostCenterHq": $("#radio_costcenterHQ").is(":checked"),//歸帳總公司
		"IsAllStore": $('#chk_All').is(":checked"),//適用分店全選
		"EnableInstall": $('input[name=enableInstall]:checked').val()
	};
	//APP顯示櫃位
	var storeDisplayNames = [];
	$.each(appShowStoreObjects.getSelectShoppeCode(),
		function (index, item) {
			data["StoreDisplayNames[" + index + "].Guid"] = item.Guid;
			data["StoreDisplayNames[" + index + "].ShopCode"] = item.ShopCode;
		});
	data["ExchangeType"] = getExchangeType($("input[name='stores']:checked").val());
	return data;
}

function newModel() {
	if (confirm("確定取消？")) {
		if ($("input[id=isPrize]").val() == true) {
			location.href = '/SKMDeal/PrizeList';
		} else {
			location.href = '/SKMDeal/skmlist';
		}
	}
}

function getBurningData() {
	var parent = $('.active:visible');

	//是否燒點
	var burningOrCash = $('input:radio[name="burningOrCash"]:checked').val();
	var burningPoint = 0;
	switch (burningOrCash) {
		case "Point":
		case "PointAndCash":
			burningPoint = $('.burningPoint').val();
			if (burningPoint === "" || burningPoint === "0") {
				burningPoint = $('.burningPointAndCashP').val();
			}
			break;
		case "Cash":
			break;

	}

	//成本中心
	var costCenterList = [];
	$('.costCenterli').each(function () {
		if ($(this).attr('value') !== '') {
			var costCenter = {};
			costCenter.StoreCode = $(this).attr('value');
			costCenter.CostCenterCode = $(this).find('select').val();
			costCenter.OrderCode = $(this).find('input[type=text]').val();
			costCenter.ExChangeQty = $(".storeQty" + costCenter.StoreCode).val();
			costCenterList.push(costCenter);
		}
	});

	//贈品編號
	var exchangeNo = "";
	$.each(parent.find('.exchangeNo'), function (n) {
		var e = $(parent.find('.exchangeNo')[n]).val();
		if (e > 0) {
			exchangeNo = e;
		}
	});

	var burningData = {};
	var burningDataJsonFormat = [];
	burningData.ActiveType = $('.activeType:checked').val();
	burningData.ExternalGuid = $('#dealGuid').val();
	burningData.EventName = parent.find('.title').val();
	burningData.EventId = "";
	burningData.BurningEventSid = "";
	burningData.DeliveryTimeS = $('#deliverDateS').val() + " " + $('#deliverTimeS').val();
	burningData.DeliveryTimeE = $('#deliverDateE').val() + " " + $('#deliverTimeE').val();
	burningData.DealStartTime = $('#orderDateS').val() + " " + $('#orderTimeS').val();
	burningData.DealEndTime = $('#orderDateE').val() + " " + $('#orderTimeE').val();
	burningData.ExchangeQty = parent.find('.orderTotalLimit').val();
	burningData.BurningPoint = burningPoint;
	burningData.ExchangeItemId = exchangeNo;
	burningData.CostCenter = costCenterList;
	burningData.CloseProjectStatus = $('input[name=activityStatus]:checked').val();
	burningData.GiftWalletId = $("input[id=giftWalletId]").val();
	burningData.GiftPoint = $("input[id=giftPoint]").val();
	burningData.PrizeDeal = $("input[id=isPrize]").val();
	burningData.DealVersion = 2;
	burningData.IsCostCenterHq = $("#radio_costcenterHQ").is(":checked");
	burningDataJsonFormat = JSON.stringify(burningData);
	return burningDataJsonFormat;
}

function gotoPreview(apiUrl, preiewUrl) {
	$.urlParam = function (name) {
		var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
		if (results === null) {
			return null;
		} else {
			return results[1] || 0;
		}
	};
	if (checkData()) {
		var data = getData();

		$.post(apiUrl, $.param(data, true), function (data) {
			if (data.Result) {
				if ($.urlParam('seller') !== null) {
					location.href = preiewUrl + '?seller=' + $.urlParam('seller');
				} else {
					location.href = preiewUrl;
				}
			} else {
				alert(data.Message);
			}
		});
	}
}

function onSaveDraft(obj, saveDraftWithBurningEventUrl, saveDraftUrl, skmPrizeListUrl, skmlistUrl) {

	if (checkData()) {
		$.blockUI({ message: "<center><font color='blue'>處理中，請稍候...</font></center>", css: { width: '250px' } });
		$(obj).attr('onclick', '').unbind('click');
		var data = getData();

		if (data.ActiveType === $("input[id=activeTypeShoppe]").val() && (data.DiscountType === undefined || data.DiscountType === "" || data.DiscountType === null)) {
			alert('請選擇優惠方式');
			$.unblockUI();
			$("#onSaveDr").on('click', function () {
				onSaveDraft(this, saveDraftWithBurningEventUrl, saveDraftUrl, skmPrizeListUrl, skmlistUrl);
			});
			return false;
		}

		var burningData = null;
		if (!$("input[id=isSkmPay]").is(":checked")) {
			burningData = getBurningData();
			var burningObj = JSON.parse(burningData);
			var ischeckBurningSetting = true;
			if (burningObj.ExchangeQty === "0" || parseInt(burningObj.ExchangeQty) === 0) {
				alert('憑證數量不可小於1');
				ischeckBurningSetting = false;
			}
			if ((data.ActiveType === $("input[id=activeTypeGiftCertificate]").val() || data.ActiveType === $("input[id=activeTypeCashCoupon]").val())
				&& (burningObj.GiftWalletId === "" || burningObj.GiftWalletId.length !== 10)) {
				alert('Wallet ID 長度必需為 10碼');
				ischeckBurningSetting = false;
			}
			if ((data.ActiveType === $("input[id=activeTypeGiftCertificate]").val() || data.ActiveType === $("input[id=activeTypeCashCoupon]").val())
				&& (burningObj.GiftWalletId.charAt(0) !== "S" && burningObj.GiftWalletId.charAt(0) !== "R")) {
				alert('Wallet ID 開頭必需為 S 或 R');
				ischeckBurningSetting = false;
			}

			if (ischeckBurningSetting == false) {
				$.unblockUI();
				$("#onSaveDr").on('click', function () {
					onSaveDraft(this, saveDraftWithBurningEventUrl, saveDraftUrl, skmPrizeListUrl, skmlistUrl);
				});
				return false;
			}
		}

		var passFlag = false;

		//skm3.7ver調整開關 && 為扣點商品時
		if (!$("input[id=isSkmPay]").is(":checked") && ($('input:radio[name="burningOrCash"]:checked').val() === "Point" || $('input:radio[name="burningOrCash"]:checked').val() === "PointAndCash") &&
			(parseInt($("input[id=dealStatus]").val()) < parseInt($("input[id=createdBurningEvent]").val()) ||
				location.search.indexOf("guid") === -1)) {

			//呼叫環遊API
			$.ajax({
				type: "POST",
				url: saveDraftWithBurningEventUrl,
				data: burningData,
				async: false,
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				success: function (datas) {
					if (!datas.Result) {
						passFlag = false;
						alert(datas.Message);
						if (location.search.indexOf("guid") === -1) {
							$('#dealGuid').val(jQuery.Guid.New());
						}
						$(obj).attr('onclick', 'onSaveDraft(this, "' + saveDraftWithBurningEventUrl + '","' + saveDraftUrl + '","' + skmPrizeListUrl + '","' + skmlistUrl + '");').bind('click');
						$.unblockUI();
					} else {
						passFlag = true;
					}
				},
				complete: function () {
				},
				error: function (xhr) {
					console.log(xhr.responseText);
				}
			});
		} else {
			passFlag = true;
		}

		//存檔
		if (passFlag) {
			$.post(saveDraftUrl,
				$.param(data, true),
				function (data) {
					if (data.Result) {
						if ($("input[id=isPrize]").val() === "True") {
							location.href = skmPrizeListUrl + data.BackUrl;
						} else {
							location.href = skmlistUrl + data.BackUrl;
						}
					}
					else {
						alert(data.Message);
						$.unblockUI();
						$("#onSaveDr").on('click', function () {
							onSaveDraft(this, saveDraftWithBurningEventUrl, saveDraftUrl, skmPrizeListUrl, skmlistUrl);
						});
					}
				});
		}
	}
	$.unblockUI();
	return false;
}

//是否顯示全選及兌換地點邏輯
//當為電子贈品禮券 & 電子抵用金才顯示適用分店(全選) & 隱藏兌換地點
//當先選了電子贈品禮券 & 電子抵用金, 再切換為其他優惠類型, 要先將其N店下的憑證數量&成本中心欄位清空
function AllStoreModel(isShow) {

	if (isShow) {
		$('#exchangeStoreDiv').hide();//兌換地點DIV
		$('#AllStoreLabel').show();//適用分店全選按鈕
	} else {
		//判斷兌換地點是否隱藏(若隱藏代表之前是選擇電子贈品禮券&抵用金), 如果是則先清掉所有分店憑證數欄位跟成本欄位
		$('#exchangeStoreDiv').show();//兌換地點DIV
		$('#AllStoreLabel').hide();//適用分店全選按鈕
	}

	//dealStatus== -1代表新增模式
	if ($("#dealStatus").val() == "-1") {
		//清除所有適用分店/憑證數量/成本中心
		setSelectStores("All", false);
		//清除所有館櫃
		writeOffStoreObjects.clearAllShoppes();
	}
}

function activeMode(type) {
	$('.active').hide();
	if (type === $("input[id=activeTypeShoppeStr]").val()) {//全櫃位優惠
		$('#div_shoppe').show();
		$('#memo').text('※  有單品管理的商品，可行特定單品優惠');
		$('#div_deliver').show();
		$('#div_timeset').show();
		$('#divactiveType').show();
		$('#burningCashLabel').text('否');
		$('#burningdollarLabel').text('');
		$(".skmPayAlert").hide();
		$('.burningCash').hide();
		$(".burningPointAndCash").hide();
		$('#div_settlement').hide();
		$(".product-img").show();
		$(".gift-data").hide();
		$(".usingSkmPay").show();
		$("#divInstallment").show();
		AllStoreModel(false);
	}
	else if (type === $("input[id=activeTypeExperienceStr]").val()) {
		$('#div_recommend').show();
		$('#memo').text('※  有單品管理的商品，可行特定單品優惠');
		$('#div_deliver').hide();
		$('#div_timeset').hide();
		$('.actionBtn').show();
		$('#divactiveType').hide();
		$('#divSkmHot').hide();
		$('#burningCashLabel').text('否');
		$('#burningdollarLabel').text('');
		$('.burningCash').hide();
		$(".burningPointAndCash").hide();
		$(".product-img").show();
		$(".product-img").show();
		$(".gift-data").hide();
		$(".burningOnlyCash").show();
		$("#divInstallment").hide();
		AllStoreModel(false);
	}
	else if (type === $("input[id=activeTypeBuyGetFreeStr]").val()) { //買幾送幾
		$(".usingSkmPay").hide();
		$('#div_buyGetFree').show();
		$('#memo').text('※  此優惠方式限自營價格相同商品');
		$('#div_deliver').show();
		$('#div_timeset').show();
		$('.actionBtn').show();
		$('#divactiveType').show();
		$('#divSkmHot').hide();
		$('#burningCashLabel').text('否');
		$('#burningdollarLabel').text('');
		$(".skmPayAlert").hide();
		$('.burningCash').hide();
		$(".burningPointAndCash").hide();
		$(".gift-data").hide();
		$(".product-img").show();
		$("input[id=isSkmPay]").prop("checked", false);
		$(".burningOnlyCash").show();
		$("#divInstallment").hide();
		AllStoreModel(false);
	} else if (type === $("input[id=activeTypeCashCouponStr]").val() || type === $("input[id=activeTypeGiftCertificateStr]").val()) {
		$(".usingSkmPay").hide();
		$('.burningCash').show();
		$('#div_product').show();
		$(".product-desc").hide();
		//現在需要上架燒點販售, 
		//所以要有圖/描述/備註/說明
		$(".giftcashcoupon-div").show();
		$(".gift-data").show();
		$('.actionBtn').show();
		$(".burningPointAndCash").hide();
		$(".burningOnlyCash").hide();
		$(".skmPayAlert").hide();
		$("#divInstallment").hide();
		AllStoreModel(true);

		$("#onlyPoint").attr("checked", true);
		$("#onlyPoint").click();
	} else {
		$(".usingSkmPay").show();
		$('#div_product').show();
		$(".product-desc").show();
		$(".product-img").show();
		$('#memo').text('※  有單品管理的商品，可行特定單品優惠');
		$('#div_deliver').show();
		$('#div_settlement').show();
		$('#div_timeset').show();
		$('#burningCashLabel').text('否,兌換金額');
		$('#burningdollarLabel').text('元');
		$('.burningCash').show();
		$(".burningPointAndCash").show();
		$(".gift-data").hide();
		$(".skmPayAlert").show();
		$(".burningOnlyCash").show();
		$("#divInstallment").show();
		AllStoreModel(false);
	}
}

function skmMode(type) {
	$('.active').hide();
	if (type === $("input[id=skmDealTypesHotStr]").val()) {//熱賣
		$('#div_recommend').show();
		$('#memo').text('※  有單品管理的商品，可行特定單品優惠');
		$('.actionBtn').show();
		$('#divactiveType').hide();
		$('#divSkmProductInfo').hide();
		$('#divSkmHot').show();
		$('#divSkmPromotional').hide();
		$('.activeType').attr("checked", $("input[id=activeTypeexperience]").val());
		$("#chkSkmHot").attr("checked", true);
		$("#chkNewSkmHot").show();
		$("#chkNewSkmHot").attr("checked", true);
		$("#chkNewSkmProductInfo").hide();
		$("#chkNewSkmProductInfo").attr("checked", false);
		$("#txtSkmHot").show();
		$("#txtSkmProductInfo").hide();
		$("#lblSkmHot").text("熱賣商品");
		$("#lblSkmProductInfo").text("");
		$('#div_deliver').hide();
		$('#div_settlement').hide();
		$('#div_timeset').hide();
	} else if (type === $("input[id=skmDealTypesProductInfoStr]").val()) {//注目
		$('#div_recommend').show();
		$('#memo').text('※  有單品管理的商品，可行特定單品優惠');
		$('.actionBtn').show();
		$('#divactiveType').hide();
		$('#divSkmHot').hide();
		$('#divSkmProductInfo').show();
		$('#divSkmPromotional').hide();
		$('.activeType').attr("checked", $("input[id=activeTypeexperience]").val());
		$("#chkNewSkmProductInfo").show();
		$("#chkNewSkmProductInfo").attr("checked", true);
		$("#chkNewSkmHot").hide();
		$("#chkNewSkmHot").attr("checked", false);
		$("#txtSkmHot").hide();
		$("#txtSkmProductInfo").show();
		$("#lblSkmProductInfo").text("注目商品");
		$("#lblSkmHot").text("");
		$('#div_deliver').hide();
		$('#div_settlement').hide();
		$('#div_timeset').hide();
	} else {
		$('#div_product').show();
		$('#memo').text('※ 有單品管理的商品，可行特定單品優惠');
		$('#div_deliver').show();
		$('#div_settlement').show();
		$('#div_timeset').show();
		$('#divactiveType').show();
		$('#divSkmPromotional').show();
		$('#divSkmHot').hide();
		$('#divSkmProductInfo').hide();
	}
}

function bindDealInfo() {
	$("input[name=exhibitionCode]").blur(function () {
		var d = queryExhibitionCode($("input[id=queryExhibitionCodeUrl]").val(), $(this).val());
		if (d !== undefined && d.Success) {
			$("#qexhName").html(d.Name);
			$("#qexhId").val(d.Code);
		} else {
			$("#qexhName").html("");
			$("#qexhId").val("");
		}
	});

	$(".burningCash").blur(function () {
		checkSkmPayCheck();
	});

	$(".burningPointAndCashC").blur(function () {
		checkSkmPayCheck();
	});

	$('input[id=isCrmDeal]').change(function () {
		if (this.checked) {
			$("#crmAlert").show();
		} else {
			$("#crmAlert").hide();
		}
	});

	$('.drag-drop-upload-container :file').change(function () {
		previewAppImageFile(this.files[0], $(this).closest('.drag-drop-upload'));
		handleAppFiles(this.files, $(this).closest('.drag-drop-upload'));
	});

	$('.drag-drop-upload-container-img :file').change(function () {
		previewImageFile(this.files[0], $(this).closest('.drag-drop-upload'));
		handleImgFiles(this.files, $(this).closest('.drag-drop-upload'));
	});

	$('#applyForm').on('input', '.active:visible .title', function () {
		getCharLength($(this));
	});

	$('#applyForm').on('input', '.active:visible .introduction', function () {
		getCharLength($(this));
	});

	$('#applyForm').on('input', '.active:visible .itemName', function () {
		getCharLength($(this));
	});

	$('#applyForm').on('click', '.discountType', function () {
		var subParent = $(this).parent();
		var value = subParent.find('.discount').val();
		subParent.find('.discount').val('');
		subParent.find('.discount').val(value);
		subParent.find('.discount').focus();
	});

	$('#applyForm').on('click', '.active:visible .discount', function () {
		var value = $(this).val();
		$('.active:visible .discount').val('');
		$(this).parent().find('.discountType').attr('checked', 'checked');
		$(this).val(value);
		$(this).focus();
	});

	$('.stores').on('click', function () {
		if (isNotDraft())
			return false;
		var checked = $(this).is(':checked');
		var seller = $(this).val();
		if (!checked) {
			$("." + seller).remove();
		}

		setSelectStores(seller, checked);

		if (!checked) {
			$.each($("[store-guid='" + seller + "']"), function (index, item) {
				removeCostCenterAndStoreQty(true, true, checked, $(item).attr('shop-code'));
			});
		}

		return true;
	});

	$('.buyAdd').on('click', function () {
		var divCount = $('.buyDel').length;
		if (divCount < 5) {
			$("#div_buy").append($('.div_buy_item:has(p):first').clone());
			addBuyButtonClick();
		}
	});

	$('.freeAdd').on('click', function () {
		var divCount = $('.freeDel').length;
		if (divCount < 5) {
			$("#div_free").append($(".div_free_item:has(p):first").clone());
			addFreeButtonClick();
		}
	});

	$('.shoppesItemc').on('click', function () {
		if (isNotDraft())
			return false;
		var val = $(this).find('input').val();
		var checked = $(this).find("input").is(":checked");
		if (!checked) {
			$(this).parent().parent().find('.allShoppes').find('input').attr('checked', false);
		}
		$('.shoppesItemSelected').each(function () {
			if ($(this).find('input').val() === val) {
				if (checked === true) {
					$(this).find('input').attr('checked', true);
					$(this).show();
				} else {
					$(this).hide();
				}
			}
		});

		//成本中心
		var isSupermarket = $(this).text().match("超市") !== null;
		var mainShopCode = $(this).parent().parent().attr('shop-data');
		var shopCode = $(this).parent().parent().attr('shop-code');
		//檢查該館是否已有選取
		var costCenterExisted = getCostCenterExisted(shopCode);
		var storeQtyExisted = getStoreQtyExisted(shopCode);
		var anyChecked = $(this).parent().parent().find('input[type=checkbox]:checked').length > 0;

		if (checked) {
			var shopeName = $(this).parent().parent().find('.allShoppes').prev().attr('value');
			var shopeCode = $(this).parent().parent().find('.allShoppes').prev().attr('id');
			//成本中心無資料時新增一筆
			if (costCenterExisted === false) {
				$(".sc" + shopCode).each(function () {
					var marketCostCenterCodeId = $(this).find('.marketCostCenterCode').val();
					var sellCostCenterCodeId = $(this).find('.sellCostCenterCode').val();
					var selected = genCostCenterSelected(isSupermarket, sellCostCenterCodeId, shopeCode, marketCostCenterCodeId);
					genCostCenterHtml(shopeCode, shopeName, mainShopCode, selected);
				});
			}
			if (storeQtyExisted === false) {
				genStoreQtyHtml(shopeCode, shopeName, 0);
			}
		} else {
			//無選取則刪除成本中心
			removeCostCenterAndStoreQty(costCenterExisted, storeQtyExisted, anyChecked, shopCode);
		}
	});

	$('.shoppesItemcSelected').on('click', function () {
		if (isNotDraft())
			return false;
		var val = $(this).find('input').val();
		var thisShoppesItem;
		$(this).parent().hide();
		$('.shoppesItemc').each(function () {
			if ($(this).find('input').val() === val) {
				thisShoppesItem = $(this);
				$(this).find('input').attr('checked', false);
				$(this).parent().parent().find('.allShoppes').find('input').attr('checked', false);
			}
		});

		//成本中心
		var shopCode = $(thisShoppesItem).parent().parent().attr('shop-code');
		//檢查該館是否已有選取
		var costCenterExisted = getCostCenterExisted(shopCode);
		var storeQtyExisted = getStoreQtyExisted(shopCode);
		var anyChecked = $(this).parent().parent().find('input[type=checkbox]:visible:checked').length > 0;
		//無選取則刪除成本中心
		removeCostCenterAndStoreQty(costCenterExisted, storeQtyExisted, anyChecked, shopCode);
	});

	$('.allShoppes').on('click', function () {
		if (isNotDraft())
			return false;
		var checked = $(this).find("input").is(":checked");
		var parent = $(this).prev().text();
		var item = $(this).next();
		while (item.hasClass('shoppesItem')) {
			$(item).find('input').attr('checked', checked);
			item = item.next();
		}
		var selected = $('#shoppesSelected').find('p:contains("' + parent + '")');
		var items = $(selected).next();
		while (items.hasClass('shoppesItemSelected')) {
			if (checked && !items.hasClass('isExchangeSelected')) {
				$(items).find('input').attr('checked', checked);
				$(items).show();
			} else {
				$(items).hide();
			}
			items = items.next();
		}

		//成本中心
		var isSupermarket = $(this).text().match("超市") !== null;
		var mainShopCode = $(this).parent().attr('shop-data');
		var shopCode = $(this).parent().attr('shop-code');
		var costCenterExisted = getCostCenterExisted(shopCode);
		var storeQtyExisted = getStoreQtyExisted(shopCode);
		var anyChecked = $(this).parent().find('input[type=checkbox]:checked').length > 0;

		if (checked) {
			var shopeName = $(this).parent().find('.allShoppes').prev().attr('value');
			var shopeCode = $(this).parent().find('.allShoppes').prev().attr('id');
			if (costCenterExisted === false) {
				$(".sc" + shopCode).each(function () {
					var marketCostCenterCodeId = $(this).find('.marketCostCenterCode').val();
					var sellCostCenterCodeId = $(this).find('.sellCostCenterCode').val();
					var selected = genCostCenterSelected(isSupermarket, sellCostCenterCodeId, shopeCode, marketCostCenterCodeId);
					genCostCenterHtml(shopeCode, shopeName, mainShopCode, selected);
				});
			}
			if (storeQtyExisted === false) {
				genStoreQtyHtml(shopeCode, shopeName, 0);
			}
		} else {
			removeCostCenterAndStoreQty(costCenterExisted, storeQtyExisted, anyChecked, shopCode);
		}
	});

	$('#beaconMessage').on('click', function () {
		$('.beaconType:input[value="2"]').attr('checked', true);
	});

	$('input[type=radio][name=activeType]').on('change', function () {

		switch ($(this).val()) {
			case $("input[id=activeTypeShoppe]").val():
				activeMode($("input[id=activeTypeShoppeStr]").val());
				break;
			case $("input[id=activeTypeProduct]").val():
				activeMode($("input[id=activeTypeProductStr]").val());
				break;
			case $("input[id=activeTypeBuyGetFree]").val():
				activeMode($("input[id=activeTypeBuyGetFreeStr]").val());
				break;
			case $("input[id=activeTypeGiftCertificate]").val():
				activeMode($("input[id=activeTypeGiftCertificateStr]").val());
				break;
			case $("input[id=activeTypeCashCoupon]").val():
				activeMode($("input[id=activeTypeCashCouponStr]").val());
				break;
		}
		$("input[id=isSkmPay]").prop("checked", false);
		checkSkmPayCheck();
	});

	$('input[type=radio][name=settlemnet]').on('change',
		function () {
			switch ($(this).val()) {
				case '2':
					$("#settlementTime").text("");
					$("#settlementTime").val("");

					break;
			}
		});

	$(".productCode").on("change", function () { $('.active:visible').find('.title').val(""); $('.active:visible').find('.itemOrigPrice').val(""); })

	$('.productCode').on('focusout', function () {
		if ($('input[name=activeType]:checked').val() !== $("input[id=activeTypeBuyGetFree]").val() &&
			$("input[id=enableQueryProductCode]").val() === "True") {
			var productCode = $(this).val();
			if (productCode == null || productCode == "") return;
			var data = {
                "code": productCode,
                "bForAli": false
			};
			$.ajax({
				url: '/SKMDeal/GetProductCode',
				type: 'POST',
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				data: JSON.stringify(data),
				success: function (data) {
					if (data.Result === $("input[id=SkmItemNoApiResultSuccess]").val()) {
						var obj = jQuery.parseJSON(data.Message);
						if ($("input:checked").val() !== $("input[id=activeTypeBuyGetFree]").val()) {
							$('.active:visible').find('.productCode').val(obj.PLU_NO);
							$('.active:visible').find('.itemNo').val(obj.GOODS_NO);
							if ($('.active:visible').find('.title').val() == "" ||
								$('.active:visible').find('.title').val() == null)
								$('.active:visible').find('.title').val(obj.GOODS_NM);
							if ($('.active:visible').find('.itemOrigPrice').val() == "" ||
								$('.active:visible').find('.itemOrigPrice').val() == null)
								$('.active:visible').find('.itemOrigPrice').val(Math.floor(obj.PRICE_NO));
							$('.active:visible').find('.skmOrigPrice').val(Math.floor(obj.PRICE_NO));
							$('#tax_rate').val(obj.GOODS_TAX);
							$('#tax_type').text(obj.GOODS_TAX > 0 ? '應稅' : '免稅');
							$('.actionBtn').show();
						}
					} else {
						if (data.Message.length > 0) {
							alert(data.Message);
							if (data.Result === $("input[id=SkmItemNoApiResultMultiPrice]").val()) {
								$('.actionBtn').show();
								if (data.Result === $("input[id=SkmItemNoApiResultMultiPrice]").val()) {
									parent.find('.itemNo').removeAttr('disabled');
								} else {
									parent.find('.itemNo').attr('disabled', 'disabled');
								}
							} else {
								$('.actionBtn').hide();
								parent.find('.itemNo').attr('disabled', 'disabled');
							}
						} else {
							if (productCode === '' &&
								$('.btn-primary').attr('name') === $("input[id=SkmDealTypesSkmPromotional]").val()) {
								$('.actionBtn').hide();
							} else {
								$('.actionBtn').show();
							}
						}
					}
				},
				error: function () {
					$('.actionBtn').show();
				}
			});
		}
		if ($("input[id=enableQueryProductCode]").val() === "False") {
			$('.itemNo').removeAttr('disabled');
			$('.actionBtn').show();
		}
	});

	$("input[id=isSkmPay]").change(function () {
		setSelectStores();
		checkSkmPayCheck();
		setUsingSkmPay(this);
		var val = $('input[name=burningOrCash]:checked').val();
		burningOrCashDisplay(val);
		$("input[name='enableInstall'][value='0']").prop("checked", true);
		var productIntroduction = "•請於領取期間，至指定分店櫃位開啟取貨條碼取貨。\n"
			+ "•若選擇取得電子發票證明聯，則需至取貨店之新光三越服務台或貴賓中心洽詢。開立電子發票證明聯之退貨請至官網自行列印「銷售退回證明單」。\n"
			+ "•若有退貨需求，請至會員專區 > 更多服務 > 其他 > 客服中心提出申請。\n"
			+ "•數量有限，售完為止。";
		//+ "•若有退貨需求，請於領取商品後鑑賞期7日內提出退貨申請並至原取貨專櫃辦理。"

		if (location.search.indexOf("guid") === -1 && $(this).is(":checked")) {
			$('#product_introduction').text(productIntroduction);
			$('#shoppe_introduction').text(productIntroduction);
			$('#buy_introduction').text(productIntroduction);
			getCharLength($('#product_introduction'));
			getCharLength($('#shoppe_introduction'));
			getCharLength($('#buy_introduction'));
		} else if (location.search.indexOf("guid") === -1) {
			var productIntroduction2;
			if (val === "Point" || val === "PointAndCash") {
				productIntroduction2 = "• 扣點商品:\n1. 點數一經兌換恕不得 退還或更換其他品項。\n2. 優惠限使用一次，不可重複使用。";
			} else {
				productIntroduction2 = "• 兌換價商品:\n優惠限使用一次，不可重複使用。";
			}

			//有guid代表狀態為編輯商品建檔
			$('#product_introduction').text(productIntroduction2);
			$('#shoppe_introduction').text(productIntroduction2);
			$('#buy_introduction').text(productIntroduction2);
			getCharLength($('#product_introduction'));
			getCharLength($('#shoppe_introduction'));
			getCharLength($('#buy_introduction'));
		}
	});

	$("input[id=isRepeatPurchase]").change(function () {
		var val = $('input[name=burningOrCash]:checked').val();
		burningOrCashDisplay(val);
		checkRepeatPurchase();
	});

	$("input[name=RepeatPurchaseType]").change(function () {
		var val = $('input[name=burningOrCash]:checked').val();
		burningOrCashDisplay(val);
		checkRepeatPurchase();
	});

}//bind

function setUsingSkmPay(obj) {
	if ($(obj).is(":checked")) {
		$(".exchangeNoDiv").hide();
		$(".isExchange").each(function () {
			var chkItem = $(this).find("input[name=checkbox]:checked");
			chkItem.prop('checked', false);

		});
		$(".isExchangeSelected").each(function () {
			var chkItem = $(this).find("input[name=checkbox]:checked");
			chkItem.prop('checked', false);
			chkItem.parent().parent().hide();
		});
		$(".allShoppes").each(function () {
			$(this).find("input[name=checkbox]:checked").prop('checked', false);
		});
		$(".allShoppes").find("input").attr("disabled", "disabled");

		//$($("input[name=stores]")[0]).click();
		$('.isExchange').hide();
		//$($("input[name=stores]")[1]).attr("disabled", "disabled");
		//$($("input[name=stores]")[2]).attr("disabled", "disabled");
	} else {
		if ($("input[name=burningOrCash]:checked").val() !== "Cash") {
			$(".exchangeNoDiv").show();
		}
		//$($("input[name=stores]")[1]).removeAttr("disabled");
		//$($("input[name=stores]")[2]).removeAttr("disabled");
		$(".allShoppes").find("input").removeAttr("disabled");
	}

	if ($("input[name=burningOrCash]:checked").val() === "Cash") {
		if ($(obj).is(":checked")) {
			$('#div_settlement').hide();
		} else {
			$('#div_settlement').show();
		}
	}
}

function checkSkmPayCheck() {
	if ($("input[id=isSkmPay]").is(":checked")) {
		if ($("input[name=activeType]:checked").val() === $("input[id=activeTypeShoppe]").val()) {
			setShoppeDiscountType(false);
		} else {
			if ($("input[id=onlyPoint]").is(":checked")) {
				$("input[id=onlyPoint]").prop("checked", false);
			}
			$("input[id=onlyPoint]").attr("disabled", "disabled");
			$("input[type=number].burningPoint").val("0");
			$("input[type=number].burningPoint").attr("disabled", "disabled");
			$(".excludingTax").show();
			$("div[id=div_settlement]").hide();
		}
		$("#divInstallment").show();
	} else {
		if ($("input[name=activeType]:checked").val() === $("input[id=activeTypeShoppe]").val()) {
			setShoppeDiscountType(true);
		} else {
			if (location.search.indexOf("guid") === -1 && $("#isAllowEdit").val() === "True") {
				$("input[id=onlyPoint]").removeAttr("disabled");
				$("input[type=number].burningPoint").removeAttr("disabled");
			}
		}
		$(".excludingTax").hide();
		$("#divInstallment").hide();
	}
}

function setShoppeDiscountType(isOn) {
	$("input[id=onlyPoint]").removeAttr("disabled");
	$("input[type=number].burningPoint").removeAttr("disabled");
	var idx = 0;
	$("#shoppeDiscountType").find(".dt").each(function () {
		if (idx > 0) {
			$(this).find("input").each(function () {
				if (isOn === true) {
					$(this).removeAttr("disabled");
				} else {
					$(this).attr("disabled", "disabled");
					if ($(this).attr("type") === "number") {
						$(this).val(0);
					} else if ($(this).attr("type") === "radio") {
						$(this).prop("checked", false);
					}
				}
			});
		}
		idx++;
	});
}

function checkRepeatPurchase() {
	let isRepeatPurchase = false;
	if ($("input[id=isRepeatPurchase]").length > 0) {
		isRepeatPurchase = $("input[id=isRepeatPurchase]").is(":checked");
	}
	else {
		isRepeatPurchase = $("input[name=RepeatPurchaseType]:checked").val() == 0;
	}
	if (isRepeatPurchase) {
		$($("input[name=settlemnet]")[1]).prop("checked", true);
		$("input[id=settlementTime]").val("");
		$('#div_settlement').hide();
	} else {
		if (!$("input[id=isSkmPay]").is(":checked") && $("input[name=burningOrCash]:checked").val() === "Cash") {
			$('#div_settlement').show();
		}
	}
}

function getExchangeType(radioStoresValue) {
	switch (radioStoresValue) {
		case "onSelectCounter":
			return 1;
		case "onSelectExchange":
			return 2;
		case "onSelectCache":
			return 3;
		default:
			return -1;
	}
}
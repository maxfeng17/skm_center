﻿var exCodeSample = "<div><p class='inputMEMO'>{code1}</p>"
    + "<input type='button' class='remove btn  btn-default btn-small  btn-long' onclick='delExhibitionCode({code2});' style='margin-top: 2px; margin-left: -4px;' value='刪除' />"
    + "<p class='f-small'>{exName}</p></div>";


function genExistCodes(d) {
    var existCodes = JSON.parse(d);
    $("input[id=exhibitionData]").val(JSON.stringify(existCodes));
    $("#exhibitionCodeZone").html("");
    for (var i = 0; i < existCodes.length; i++) {
        $("#exhibitionCodeZone").append(getExhibitionCodeHtml(existCodes[i]));
    }
}

function getExhibitionCodeHtml(d) {
    var s = exCodeSample.replace("{code1}", d.exhibitionCodeId)
        .replace("{code2}", d.exhibitionCodeId)
        .replace("{exName}", d.exhibitionName);
    return s;
}

function queryExhibitionCode(u, s) {
    var result;
    $.ajax({
        type: "POST",
        url: u + "?code=" + s,
        async: false,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function(data) {
            result = data;
        },
        error: function (xhr) {
            return null;
        }
    });
    return result;
}

function delExhibitionCode(c) {
    var exhData = JSON.parse($("input[id=exhibitionData]").val());
    var delIdx = null;
    $.each(exhData, function (i, item) {
        if (item.exhibitionCodeId === c) // delete index
        {
            delIdx = i;
        }
    });
    exhData.splice(delIdx, 1);
    $("input[id=exhibitionData]").val(JSON.stringify(exhData));
    genExistCodes(JSON.stringify(exhData));
}

function addExhibitionCode(u) {
    var g = $("input[id=dealGuid]").val();
    var code = $("input[id=qexhId]").val();
    if (code === undefined || code === "") {
        return false;
    }

    var exhObj = queryExhibitionCode(u, code);
    if (exhObj === undefined || exhObj.Id === 0) {
        return false;
    }

    var exhData = JSON.parse($("input[id=exhibitionData]").val());
    var checkExist = false;
    $.each(exhData,
        function(i, item) {
            if (item.exhibitionCodeId.toString() === code) {
                checkExist = true;
            }
        });

    if (checkExist) { return false; }

    var newData = {
        "externalDealGuid": g,
        "exhibitionCodeId": exhObj.Code,
        "exhibitionName": exhObj.Name
    };

    exhData.push(newData);
    $("input[id=exhibitionData]").val(JSON.stringify(exhData));
    var exHtml = getExhibitionCodeHtml(newData);
    $("#exhibitionCodeZone").append(exHtml);
    $("input[id=qexhId]").val("");
    $("#qexhName").html("");
    return false;
}
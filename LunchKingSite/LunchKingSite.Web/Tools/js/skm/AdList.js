﻿$().ready(function () {
    var ad_list_zone = new Vue({
        el: "#ad-list-zone",
        data: {
            adDatas: [
                /*{範例用
                    Id: "",
                    StartDate: "",
                    EndDate: "",
                    Name: "",
                    IsEnable: "",
                    Status: "",
                    SetIsEnableText: "",
                }*/
            ],
            isBusying: false,
            totalPages: 2,
            totalCount: 0,
            pageIndex: 1,
            pageSize: 10,
        },
        methods: {
            deleteAdBoard: function (id) {
                this.isBusying = true;
                if (confirm("確認刪除此蓋板廣告？")) {
                    axios.post("/SKMDeal/DeleteAdBoard", { Id: id })
                        .then(response => {
                            if (response.data.IsSuccess) {
                                this.getAdList(this.pageIndex);
                            }
                            else {
                                alert(response.data.Message);
                            }
                            this.isBusying = false;
                        })
                        .catch(error => {
                            console.log(error);
                            alert("刪除失敗");
                            this.isBusying = false;
                        });
                }
                else {
                    this.isBusying = false;
                }
            },
            setIsEnable: function (id) {
                this.isBusying = true;
                let adDataIndex = this.adDatas.findIndex(item => item.Id === id);
                if (adDataIndex > -1) {
                let newIsEnable = !this.adDatas[adDataIndex].IsEnable;
                    axios.post("/SKMDeal/SetAdBoardIsEnable", { id: id, isEnable: newIsEnable })
                        .then(response => {
                            if (response.data.IsSuccess) {
                                this.adDatas[adDataIndex].IsEnable = response.data.IsEnable;
                                this.adDatas[adDataIndex].SetIsEnableText = response.data.SetIsEnableText;
                                this.adDatas[adDataIndex].Status = response.data.Status;
                                console.log(this.adDatas);
                            }
                            else {
                                alert(response.data.Message);
                            }
                            this.isBusying = false;
                        })
                        .catch(error => {
                            console.log(error);
                            alert("更新狀態失敗");
                            this.isBusying = false;
                        });
                }
                else {
                    alert("查無此資料");
                    this.query(1);
                    this.isBusying = false;
                }
            },
            getAdList: function (page) {
                this.isBusying = true;
                axios.post('/SKMDeal/GetAdList', { "pageIndex": page, "pageSize": this.pageSize})
                    .then(response => {
                        if (response.status == 200 && response.data !== null) {
                            this.adDatas = response.data.Data.SkmAdBoardList;
                            this.pageIndex = response.data.Data.PageIndex;
                            this.totalPages = response.data.Data.TotalPages;
                            this.totalCount = response.data.Data.TotalCount;
                        }
                        this.isBusying = false;
                    })
                    .catch(error => {
                        alert("系統錯誤!");
                        console.log(error);
                        this.isBusying = false;
                    });
            },
            pageIndexChange: function() {
                this.getAdList(event.target.value);
            }
        },
        beforeMount: function () {
            this.getAdList(1);
        }
    });
})
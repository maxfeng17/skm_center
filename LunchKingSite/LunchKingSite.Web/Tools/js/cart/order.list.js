﻿var OrderList = OrderList || {};

OrderList = {
    version: "1.0.0",
    createDate: "2016/09/30",
    dependencies: {
        jQuery: "1.9.1"
    }
}

OrderList.Page = {
    Init: (function () {
        //InitChkAll();
        PageInit();
        InitSpinnerNumber();
        var ticketId = loadStorage(cartTicketName);
        ReloadCookie(ticketId);
    })
};

function InitSpinnerNumber() {
    $.each($(".wan-spinner-order"), function (idx, obj) {
        var _MaxItemCount = $(obj).parent().find("#hidMaxItemCount").val();
        var _IsGroupCoupon = $(obj).parent().find("#hidIsGroupCoupon").val();
        if (_IsGroupCoupon == "true") {
            _MaxItemCount = 1;
        }
        addOrderSpinnerNumberEvent($(obj), parseInt(_MaxItemCount), 1);
    });
}

function addOrderSpinnerNumberEvent(obj, max, min) {
    //購物車內的數量+-
    $(obj).unbind("click");
    //var minData = $(obj).find("input").val();

    $(obj).WanSpinner({
        maxValue: max,//可選最大數
        minValue: min,//可選最小數
        step: 1,//加減一次多少
        inputWidth: 20,//數字框寬度
        plusClick: function (val) {
        },
        minusClick: function (val) {
        },
        exceptionFun: function (val) {
            //console.log("excep: " + val);
        },
        valueChanged: function (val) {
            var __Quantity = val.find("input[type='text']").val();
            var __objAmount = val.parent().find(".amount");
            var __oriAmount = val.parent().find("#hidItemPrice");
            var BuyPrice = parseInt(__Quantity) * parseInt(__oriAmount.val());
            __objAmount.html("$" + ThousandComma(BuyPrice));

            var plus = $(obj).find(".plus");
            if (__Quantity == max) {
                plus.addClass("disabled");
                plus.css("cursor", "not-allowed");
            } else {
                plus.removeClass("disabled");
                plus.css("cursor", "pointer");
            }

            var minus = $(obj).find(".minus");
            if (__Quantity == 1) {
                minus.addClass("disabled");
                minus.css("cursor", "not-allowed");
            } else {
                minus.removeClass("disabled");
                minus.css("cursor", "pointer");
            }


            var row = $(val).parent().parent().parent();
            //是否為小購物車的第一列
            var IsFirstRow = $(row).find('td[rowspan]').length > 0 ? true : false
            var ThisRowSellerGuid = null;
            var FirstRow = null;
            var TmpFirstRow = null;

            //抓該小購物車的第一列
            if (IsFirstRow) {
                FirstRow = $(row);
            }
            else {
                $(row).prevAll().each(function () {
                    if ($(this).find('td[rowspan]').length > 0) {
                        FirstRow = $(this);
                        return false;
                    }
                })
            }

            //計算總價
            TmpFirstRow = FirstRow;

            var RowSpan = TmpFirstRow.find('td[rowspan]').attr('rowspan');
            var Amount = 0;
            for (var i = 1; i <= RowSpan; i++) {
                Amount = Amount + parseInt(TmpFirstRow.find('.amount').text().replace("$", "").replace(",", ""));
                //移到下一筆
                TmpFirstRow = TmpFirstRow.next();

            }

            var NoFreightsLimit = FirstRow.find('#hidNoFreightLimit').val();
            var Freights = FirstRow.find('#hidFreights').val();

            //更新總和
            FirstRow.find('td[rowspan]')[0].innerText = "$" + ThousandComma(Amount);

            //更新運費
            if (Amount > NoFreightsLimit) {
                var html = FirstRow.find('td[rowspan]')[1].innerHTML;
                var span = html.substring(html.indexOf('<span>'), html.indexOf('</span>') + 7);
                FirstRow.find('td[rowspan]')[1].innerHTML = FirstRow.find('td[rowspan]')[1].innerHTML.replace(span, "<span>$免運</span>");
            }
            else {
                var html = FirstRow.find('td[rowspan]')[1].innerHTML;
                var span = html.substring(html.indexOf('<span>'), html.indexOf('</span>') + 7);
                FirstRow.find('td[rowspan]')[1].innerHTML = FirstRow.find('td[rowspan]')[1].innerHTML.replace(span, "<span>$" + Freights + "</span>");
            }



            var TicketId = row.find("#TicketId").html();
            updateServerFile(TicketId, FirstRow.find('#hidFreightsId').val(), __Quantity)

        }
    });
}

function InitChkAll() {
    var __checked = true;
    $.each($(".OrderDecisionTable input[type=checkbox]").not("input[id='chkAll']"), function (idx, obj) {
        if (__checked) {
            if (!$(obj).prop("disabled")) {
                $(obj).prop("checked", "checked");
            }
        } else {
            $(obj).prop("checked", __checked);
        }
    });
    countCheck();
}


function PageInit() {
    $("#btnGoToPay").addClass("disabled");
    $("#chkAll").click(function () {
        var __checked = $(this).is(":checked");
        $.each($(".OrderDecisionTable input[type=checkbox]").not("input[id='chkAll']"), function (idx, obj) {
            if (__checked) {
                if (!$(obj).prop("disabled")) {
                    $(obj).prop("checked", "checked");
                }
            } else {
                $(obj).prop("checked", __checked);
            }
        });
        countCheck();
    });
    $('.cash').change(function () {

        var id = $(this).attr('id');

        var Amount = $(this).parent().parent().children().eq(0).text().replace(",", "").replace("$", "").trim();
        var Freight = $(this).parent().parent().children().eq(2).text().replace(",", "").replace("$", "").trim();
        var total = parseInt(Amount) + parseInt(Freight) - parseInt($('#txtbcash').val()) - parseInt($('#txtscash').val());
        $(this).parent().parent().children(':last').text(ThousandComma('$' + total));


        var oid = loadStorage();
        $.ajax({
            type: "POST",
            url: "/UserOrder/UpdateCash",
            contentType: "application/json; charset=utf-8",
            data: "{'oid': '" + oid + "','bcash': '" + $('#txtbcash').val() + "','scash': '" + $('#txtscash').val() + "'}",
            success: function (response) {
                
            }, error: function (xhr) {
                console.log(xhr.responseText);
            }
        });

        

    });
}


function countCheck() {
    var selectedChk = 0;
    var notCheck = false;
    $.each($(".OrderDecisionTable input[type=checkbox]").not("input[id='chkAll']"), function (idx, obj) {
        if ($(obj).prop("checked")) {
            selectedChk += 1;
        } else {
            if (!$(obj).prop("disabled")) {
                notCheck = true;
            }
        }
    });
    if (notCheck) {
        $("#chkAll").prop("checked", false);
    } else {
        $("#chkAll").prop("checked", "checked");
    }
    if (selectedChk > 0) {
        $("#btnGoToPay").removeClass("disabled");
    } else {
        $("#btnGoToPay").addClass("disabled");
    }

    $("#delCart span").html(selectedChk);
}

function chooseCoupon(obj) {
    var _parent = $(obj).parent();
    var bid = _parent.find("#Bid").html();
    var ticketId = _parent.parent().parent().find("#TicketId").html();
    $(".message_box").hide();
    // 當一個「選擇折價券」的按鈕被按時，首先把展開中的折價券選單關閉
    _parent.find(".message_box").fadeOut(0, function () {
        $(".message_box").children().remove();

        //放到fadeOut區塊內，確保事件已完成後，才去做下面動作，否則快速Double Click，會留下殘影
        // 找到藏於該按鈕下的tooltip容器(message_box)，載入折價券選單並秀出來。以此確保各按鈕下方皆能秀出各自可用的折價券選單
        $.ajax({
            type: "POST",
            url: "/UserOrder/GetDiscountList",
            contentType: "application/json; charset=utf-8",
            data: "{'Bid': '" + bid + "', 'ticketId': '" + ticketId + "'}",
            success: function (response) {
                debugger;
                $(".hidden-when-uncheck ul").empty();
                var html = "";
                $.each(response, function (_idx, _obj) {
                    if (_obj.DiscountState == "used") {
                        html += '<li>';
                    } else {
                        html += '<li onclick="chooseDiscount(this)">';
                    }
                    html += '<span id="DiscountCode" style="display:none">' + _obj.DiscountCode + '</span>'
                    html += '<a href="#" class="' + _obj.DiscountState + '">';
                    html += '        <i class="fa fa-check fa-lg fa-fw"></i>折價券<span style="color:#BF0000">' + _obj.Amount + '</span>元，';
                    if (_obj.MinimumAmount == 0) {
                        html += '        無使用門檻。';
                    } else {
                        html += '        消費滿<span class="hint">' + _obj.MinimumAmount + '</span>可使用。';
                    }
                    html += '        <span class="date">' + _obj.EndTime + '前有效</span>';
                    html += '    </a>';
                    html += '</li>';
                });
                $(".hidden-when-uncheck ul").append(html);
                $(".message_box_content .hint").html(response.length);
            }, error: function (xhr) {
                console.log(xhr.responseText);
            }
        });
        var message_box_content = $('.message_box_content').html();
        _parent.find(".message_box").fadeIn(250).append(message_box_content);
        // 關閉限金券選單
        $(".btn_message_close_web").click(function () {
            closeMessageBoxContent();
        });
    });
}

function closeMessageBoxContent() {
    $(".message_box-shoppingcart").fadeOut(250, function () {
        $(this).children().remove();
    });
}

function chooseDiscount(obj) {
    var _parent = $(obj).parent().parent().parent().parent().parent().parent();
    var chooseDiscountCode = _parent.find("#ChooseDiscountCode").text();
    var discountCode = $(obj).find("#DiscountCode").text();    

    //選擇相同代表取消
    if (discountCode == chooseDiscountCode)
    {
        discountCode = '';
    }

    updateDistinctState(obj, discountCode);
    closeMessageBoxContent();
}

function typeDiscount(obj) {
    var _parent = $(obj).parent().parent().parent().parent().parent().parent();
    var bid = _parent.find("#Bid").text();
    var discountCode = $('#tbDiscount').val();
    
    if (!discountCode) {
        $('#couponMessage').css('display', '');
        $('#couponMessage').text('無效的序號');
        return;
    }

    var ticketId = _parent.parent().parent().find("#TicketId").text();
    var data = {
        discountCode: discountCode,
        ticketId: ticketId,
        bid: bid
    }

    $.ajax({
        type: "POST",
        url: "/UserOrder/CheckDiscountCode",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(data),
        success: function (res) {
            if (res.result) {
                updateDistinctState(obj, discountCode);
                closeMessageBoxContent();
            }
            else {
                $('#couponMessage').css('display', '');
                $('#couponMessage').text(res.message);
            }
        }
    })
}

function updateDistinctState(obj, discountCode) {
    var _parent = $(obj).parent().parent().parent().parent().parent().parent();
    var bid = _parent.find("#Bid").text();
    var ticketId = _parent.parent().parent().find("#TicketId").text();

    var data = {
        discountCode: discountCode,
        ticketId: ticketId,
        bid: bid
    }

    $.ajax({
        type: "POST",
        url: "/UserOrder/UpdateDiscountCode",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(data),
        success: function (res) {
            if (res.result) {
                if (discountCode) {
                    _parent.find('a[class^="coupon-message"]').text("選擇折價券:-$" + res.discountAmount);
                }
                else {
                    _parent.find('a[class^="coupon-message"]').text("選擇折價券");
                }

                _parent.find("#ChooseDiscountCode").text(discountCode);
            }
        }
    })
}

function deleteOrder(obj) {

    //刪除的那一列
    var $tr = $(obj).closest("tr");


    var firstrow = $tr.find('td[rowspan]').length;//該列的rowspan,可以用來判斷是否為該購物車的第一列
    var row = null;
    if (firstrow > 0)
    {
        row = $tr.find('td[rowspan]');//第一列
    }
    else
    {
        row = $tr.prevAll('tr:has(td[rowspan]):first').find('td[rowspan]');//非第一列
    }

    var column1 = null;
    var column2 = null;
    var newfirstrow = null;
    var rowspan = null;

    if (firstrow > 0)
    {
        //代表刪第一列
        //儲存最後兩列的html
        //抓取新的第一列及rowspan
        column1 = $tr.find('td[rowspan]')[0].innerHTML;
        column2 = $tr.find('td[rowspan]')[1].innerHTML;
        newfirstrow = $tr.next()[0];
        rowspan = $tr.find('td[rowspan]').attr('rowspan') - 1;
    }


    //調整 rowspan 數量
    row.attr('rowspan', function (i, rs) { return rs - 1; })
            .end()
            .end()
            .remove();


    $tr.fadeOut("slow", function () {
        $(this).remove();

        if (firstrow > 0) {
            //第一列且不為唯一的一列(唯一的一列就直接刪除了)
            if (rowspan != 0) {
                //新增cell
                var cell3 = newfirstrow.insertCell(3);
                var cell4 = newfirstrow.insertCell(4);

                //rowspan
                cell3.rowSpan = rowspan;
                cell4.rowSpan = rowspan;

                //塞回class
                cell3.className = 'OrderDecisionTable-cart_total';
                cell4.className = 'OrderDecisionTable-cart_shipment';

                //塞回html
                cell3.innerHTML = column1;
                cell4.innerHTML = column2;
            }
        }

        $("#btnGoToPay").removeClass("disabled");
        var dealCnt = 0;
        $.each($(".OrderDecisionTable tr"), function (midx, mobj) {
            var Bid = $(mobj).find("#Bid").html();
            if (Bid != undefined && Bid != "undefined" && Bid != "") {
                dealCnt += 1;
            }
        });
        if (dealCnt == 0) {
            $("#btnGoToPay").addClass("disabled");
        }
    });

    //重新計算按鈕的數量
    countCheck();

    var TicketId = $tr.find("#TicketId").html();
    deleteServerFile(TicketId);
}

function deleteServerFile(tid) {
    var oid = loadStorage();
    $.ajax({
        type: "POST",
        url: "/UserOrder/DeleteOrder",
        contentType: "application/json; charset=utf-8",
        data: "{'tid': '" + tid + "','oid': '" + oid + "'}",
        success: function (response) {
        }, error: function (xhr) {
            console.log(xhr.responseText);
        }
    });
}



function deleteMultiOrder() {
    $.each($(".OrderDecisionTable input[type=checkbox]").not("input[id='chkAll']"), function (idx, obj) {
        if ($(obj).prop("checked")) {
            var row = $(obj).parent().parent();
            deleteOrder(row);
        }
    });

}

function deleteDisableOrder() {
    $.each($(".OrderDecisionTable input[type=checkbox]").not("input[id='chkAll']"), function (idx, obj) {
        if ($(obj).prop("disabled")) {
            var row = $(obj).parent().parent();
            deleteOrder(row);
        }
    });

}

function updateServerFile(tid, freightsId, quantity) {
    var oid = loadStorage();
    $.ajax({
        type: "POST",
        url: "/UserOrder/UpdateOrder",
        contentType: "application/json; charset=utf-8",
        data: "{'tid': '" + tid + "','oid': '" + oid + "','freightsId': '" + freightsId + "','quantity': '" + quantity + "'}",
        success: function (response) {
            $('.OrderDecision-total_cost-cost').children().eq(0).text('$' + ThousandComma(response.Amount));
            $('.OrderDecision-total_cost-cost').children().eq(2).text('$' + ThousandComma(response.Freights));
            $('.OrderDecision-total_cost-cost').children(':last').text('$' + ThousandComma(response.Result));

        }, error: function (xhr) {
            console.log(xhr.responseText);
        }
    });
}
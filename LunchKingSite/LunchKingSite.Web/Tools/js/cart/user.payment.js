﻿$(document).ready(function () {
    //web 折價券 fadein
    $(".btn_message").click(function () {
        $(".message_box").fadeIn(500);
    });
    //web 折價券 fadeout
    $(".btn_message_close_web").click(function () {
        $(".message_box").fadeOut(500);
    });
    //rwd 折價券 出現
    $(".btn_message_rwd").click(function () {
        $(".content-group-MoneyTicket-web").css("display", "none");
        $(".content-group-MoneyTicket-rwd").css("display", "block")
    });
    //rwd 折價券 關閉
    $(".btn_message_close_rwd").click(function () {
        $(".content-group-MoneyTicket-web").css("display", "block");
        $(".content-group-MoneyTicket-rwd").css("display", "none")
    });
    //台新儲值支付 說明視窗
    $(".surplus img").hover(
      function () {
          $(".lmMemoryLog-wrap").find(".lmMemoryLog-box").stop(false, true).fadeIn(500);
      }, function () {
          $(".lmMemoryLog-wrap").find(".lmMemoryLog-box").stop(false, true).fadeOut(500);
      }
    );
    //選擇地址
    $("#ddlBuyerCity").change(function () {
        selectBuyerCity($(this).val(), "");
    });
    //付款方式
    SlideContent();
    //預設關閉釘單明細
    ToggleContent(this, 500);
    //選擇收件人資料
    $("#receiverSelect").change(function () {
        updateReceiver($(this).val());
    });
    
});

//付款方式顯示畫面
function SlideContent() {
    if ($("input[id='rbPayWayByCreditCard']").is(":checked")) {
        //信用卡
        $(".wrapper_moneylimit .input").hide();
        if ($(".CreditCardForms").html() == "") {
            $(".CreditCardForms").load("../Themes/cart/CreditCardPayment.html", function () {
                IntiCreditCardPaymentData();
            });
        }        
        $(".CreditCardForms2").html("");
        $(".CreditCardContent").show().slideDown();
    } else if ($("input[id='rbInstallmentDefault']").is(":checked")) {
        //台新卡友享分期
        $(".wrapper_moneylimit .input").hide();
        if ($(".CreditCardForms2").html() == "") {
            $(".CreditCardForms2").load("../Themes/cart/CreditCardPayment.html", function () {
                IntiCreditCardPaymentData();
            });
        }        
        $(".CreditCardForms").html("");
        $(".phInstallmentContent").show().slideDown();
        IntiCreditCardPaymentData();
    } else if ($("input[id='rbPayWayByMasterPass']").is(":checked")) {
        //MasterPass
        $(".wrapper_moneylimit .input").hide();
        $(".MasterPassContent").show().slideDown();
    } else if ($("input[id='rbPayWayTaishinPay']").is(":checked")) {
        //台新儲值支付
        $(".wrapper_moneylimit .input").hide();
        $(".TaishinContent").show().slideDown();
    } else if ($("input[id='rbPayWayByAtm']").is(":checked")) {
        //ATM轉帳
        $(".wrapper_moneylimit .input").hide();
        $(".AtmPayContent").show().slideDown();
    } else if ($("input[id='rbPayWayByLinePay']").is(":checked")) {
        //LinePay
        $(".wrapper_moneylimit .input").hide();
        $(".LinePayContent").show().slideDown();
    }
}
function IntiCreditCardPaymentData() {
    $("#paymentTradeDate").text($(".TradeDate").text());//交易日期
    $("#paymentTradeAmount").text($(".TradeAmount").text());//交易金額

    var objExpYear = $("#ExpYear");
        
    for (str = 0; str < 15; str++) {
        var d = new Date();
        var ny = d.getFullYear();
        ny = ny + str;
        var data = ny.toString().substr(2);
        objExpYear.append($("<option></option>").attr("value", data).text(data));
    }

    //信用卡輸入四碼往後自動往後跳
    $(".cardInputs").keyup(function () {
        if (this.value.length == this.maxLength) {
            var $next = $(this).next('.cardInputs');
            if ($next.length)
                $(this).next('.cardInputs').focus();
            else
                $(this).blur();
        }
    });
}
//付款方式顯示畫面 End

//按下展開明細時，訂單明細會打開、且字樣改為關閉明細 Start
function ToggleContent(obj, speed) {
    $('.OrderDetail-recheck').slideToggle(speed);
    if ($.trim($(obj).val()) === '展開明細') {
        $(obj).val('關閉明細');
    } else {
        $(obj).val('展開明細');
    }
}
// 按下展開明細時，訂單明細會打開、且字樣改為關閉明細 End


//發票
function ChangeInvoiceMode(pezDonationCode, genesisDonationCode, sunDonationCode) {
    $('#divInvoice2').hide();
    $('#divInvoiceDonation').hide();
    $('#divInvoice3').hide();
    $('#tdInvoiceSave').hide();
    $('#PaperInvoice').hide();

    if ($("#rbInvoiceDuplicate").is(":checked")) {
        $('#divInvoice2').css('display', '');
        $('#chkPersonalCarrier').attr('checked', false);
        CheckPersonalCarrier($('#chkPersonalCarrier'));
    } else if ($("#rbInvoiceTriplicate").is(":checked")) {

        $('#invoTitleErr').hide();
        $("#tdInvoTitle").removeClass("error");

        $('#invoNameErr').hide();
        $("#tdInvoName").removeClass("error");

        $('#invoAddrErr').hide();
        $("#tdInvoAddr").removeClass("error");

        $('#invoNumberErr').hide();
        $("#tdInvoNumber").removeClass("error");

        $('#divInvoice3').css('display', '');
        $('#tdInvoiceSave').show();
        $('#PaperInvoice').css('display', '');

    } else if ($("#rbInvoiceDonation").is(":checked")) {
        $('#divInvoiceDonation').css('display', '');
        $('#ddlInvoiceDonation').prop('selectedIndex', 0);
        ChangeInvoiceDonation($('#ddlInvoiceDonation'), pezDonationCode, genesisDonationCode, sunDonationCode);
    }
}
//發票 End

//選擇縣市
function selectBuyerCity(city, town) {
    if (city == "") {
        var $ddlBuyerArea = $("#ddlBuyerArea");
        $ddlBuyerArea.empty();
        $ddlBuyerArea.append($("<option></option>").attr("value", "").text("請選擇"));
    } else {
        $.ajax({
            type: "POST",
            url: "/UserOrder/GetBuyerCity",
            contentType: "application/json; charset=utf-8",
            data: "{'city': '" + city + "'}",
            success: function (response) {
                var $ddlBuyerArea = $("#ddlBuyerArea");
                $ddlBuyerArea.empty();
                $ddlBuyerArea.append($("<option></option>").attr("value", "").text("請選擇"));
                $.each(response, function (idx, obj) {
                    if (town == obj.Id) {
                        $ddlBuyerArea.append($("<option></option>").attr("value", obj.Id).attr("selected", true).text(obj.CityName));
                    } else {
                        $ddlBuyerArea.append($("<option></option>").attr("value", obj.Id).text(obj.CityName));
                    }
                });

            }, error: function (xhr) {
                console.log(xhr.responseText);
            }
        });
    }
}
//選擇縣市 End

function CheckPersonalCarrier(obj, CarrierTypeMember, CarrierTypePhone, CarrierTypePersonalCertificate, CarrierTypeNone) {
    if ($(obj).is(":checked")) {
        if ($('[id*=ddlCarrierType]').val() == CarrierTypeMember) {
            $('[id*=ddlCarrierType]').val(CarrierTypePhone);
        }
        $('[id*=ddlCarrierType]').find('[value=' + CarrierTypeMember + ']').hide();
        $('#PersonalCarrier').show();
        ChangeCarrierType($('[id*=ddlCarrierType]'), CarrierTypeMember, CarrierTypePhone, CarrierTypePersonalCertificate, CarrierTypeNone);
    } else {
        $('[id*=ddlCarrierType]').val(CarrierTypeMember);
        $('#PersonalCarrier').hide();
        $('#PaperInvoice').hide();
    }
}

function ChangeCarrierType(obj, CarrierTypeMember, CarrierTypePhone, CarrierTypePersonalCertificate, CarrierTypeNone) {
    $('#tdPhoneCarrier').hide();
    $('#tdPersonalCertificateCarrier').hide();
    $('#PaperInvoice').css('display', 'none');
    $('#invoNameErr').hide();
    $("#tdInvoName").removeClass("error");
    $('#invoAddrErr').hide();
    $("#tdInvoAddr").removeClass("error");
    $('#tdPhoneCarrier').removeClass("error");
    $('#phoneCarrierErr').hide();
    $('#phoneCarrierInvalid').hide();
    $('#tdPersonalCertificateCarrier').removeClass("error");
    $('#personalCertificateCarrierErr').hide();
    $('#personalCertificateCarrierInvalid').hide();
    $('#hidCarrIdVerify').val('false');

    var opt = $(obj).find(":selected").val();
    if (opt == CarrierTypePhone) {
        $('#tdPhoneCarrier').show();
        $('#carrierNote').text('使用手機條碼載具或自然人憑證載具，將由財政部通知中獎。');
    } else if (opt == CarrierTypePersonalCertificate) {
        $('#tdPersonalCertificateCarrier').show();
        $('#carrierNote').text('使用手機條碼載具或自然人憑證載具，將由財政部通知中獎。');
    } else if (opt == CarrierTypeNone) {
        $('#PaperInvoice').show();
        $('#carrierNote').text('響應發票無紙化，建議使用自動兌獎無負擔的電子發票。');
    } else if (opt == CarrierTypeMember) {
        $('#PaperInvoice').show();
        $('#carrierNote').text('響應發票無紙化，建議使用自動兌獎無負擔的電子發票。');
    }
}

//愛心捐獻
function ChangeInvoiceDonation(obj, pezDonationCode, genesisDonationCode, sunDonationCode) {
    $('#txtLoveCode').val('');
    $('#divInvoiceDonationOther').css("display", "none");
    $('#divInvoicePezDonation').css("display", "none");
    $('#divInvoiceGenesisDonation').css("display", "none");
    $('#divInvoiceSunDonation').css("display", "none");
    $('#tdLoveCode').removeClass('error');
    $('#loveCodeErr').hide();
    $("#LoveCodeDesc").html('請於上方輸入正確的愛心碼。');

    if ($(obj).val() == '') {
        $('#divInvoiceDonationOther').show();
    } else {
        if ($(obj).val() == pezDonationCode) {
            $('#divInvoicePezDonation').show();
        } else if (($(obj).val() == genesisDonationCode)) {
            $('#divInvoiceGenesisDonation').show();
        } else if (($(obj).val() == sunDonationCode)) {
            $('#divInvoiceSunDonation').show();
        }
        $("#txtLoveCode").val($(obj).val());
        $('#LoveCodeDesc').html($('#ddlInvoiceDonation').find(':selected').text().replace('捐贈', ''));
    }
}

//同購買人資訊
function copyBuyerInfo(address) {
    $("#receiverNameErr").hide();
    $("#tdRecName").removeClass("error").addClass("InvoiceTitleName");
    $("#receiverMobileErr").hide();
    $("#tdRecMobile").removeClass("error").addClass("InvoiceTitleName");
    $("#receiverAddrErr").hide();
    $("#receiverLostAddrErr").hide();
    $("#notDeliveryIslandsAddrErr").hide();
    $("#tdRecAddr").removeClass("error").addClass("InvoiceTitleName");
    $("#ReceiverName").val($("#BuyerName").val());
    $("#ReceiverMobile").val($("#BuyerMobile").val());
    //地址
    if (address != "") {
        var arrAddress = address.split("|");
        if (arrAddress.length >= 4) {
            BindAddress(arrAddress[0], arrAddress[1], arrAddress[3]);
        }
    }
}

function updateReceiver(recId) {
    if (0 == recId) {
        $("#ReceiverName").val("");
        $("#ReceiverMobile").val("");
        $.cascadeAddress.update(
            $("#ddlReceiverCity"),
            $('#ddlReceiverArea'),
            $("#ReceiverAddress"),
            -1, -1, '', false);
    } else {
        $("#receiverNameErr").hide();
        $("#tdRecName").removeClass("error").addClass("InvoiceTitleName");
        $("#receiverMobileErr").hide();
        $("#tdRecMobile").removeClass("error").addClass("InvoiceTitleName");
        $("#receiverAddrErr").hide();
        $("#receiverLostAddrErr").hide();
        $("#notDeliveryIslandsAddrErr").hide();
        $("#tdRecAddr").removeClass("error").addClass("InvoiceTitleName");
        $.ajax({
            type: "POST",
            url: "/service/ppon/GetReceiver",
            contentType: "application/json; charset=utf-8",
            data: "{'id': '" + recId + "'}",
            success: function (response) {
                if (response == null) return;
                $("#ReceiverName").val(response.name);
                $("#ReceiverMobile").val(response.mobile);
                BindAddress(response.city, response.town, response.addr);

            }, error: function (xhr) {
                console.log(xhr.responseText);
            }
        });
    }
}

function BindAddress(city, area, address) {
    $.each($("#ddlBuyerCity option"), function (idx, obj) {
        if (obj.value == city) {
            obj.selected = true;
            selectBuyerCity(obj.value, area);
            return;
        }
    });
    $("#ReceiverAddress").val(address);
}

﻿var cartTicketName = "oid";

$(document).ready(function () {
    //網頁右上角，小購物車，滑鼠移過去的動作
    $('.mini-cart-btn').hover(function () {
        //帶入小購物車資訊
        GetSamllCartList();
        $('.funtion-menu-block .mini-cart-btn').addClass('mini-cart-btn-on');
    }, function () {
        $('.funtion-menu-block .mini-cart-btn').removeClass('mini-cart-btn-on');
        $('.popup_bg').css('display', 'none');
    });

    //收藏愛心
    LoadMemberCollection();

    //購物車內的Tabs事件
    bindCartTabsEvent();

    //購物車內的數量+-
    addCartSpinnerNumberEvent($(".wan-spinner-obj"), 1, 1);

    //帶入小購物車資訊
    GetSamllCartList();

    //右側購物明細頁，下拉選項控制
    $(".cart_item").change(function () {
        checkAllItemSelected(20);
    });


    //切換方案時，下方的購物項目全數清除
    $("#item_options_selected").change(function () {
        loadDealComboPackCount($(this).val());
        $("#item-selector-mixitemcart li").remove();
        $("#item-selector-mixitemcart").hide();
        $("#item-selector-mixitemcart").html("");

        //下拉全部歸回原位
        $(".cart_select_items").val("");

        if ($(this).val() == "") {
            //未選擇方案，下方按鈕要 disabled
            $("#cartQuantity").val("1");
            $("#btnAddCartAndCloseLg").addClass("disabled");
            $("#btnBuyAndCloseLg").addClass("disabled");
        } else {
            $("#cartQuantity").val("1");
            if ($("#item-selector-mixitemselect .item-selector-dropdown").length > 0) {
                $("#btnAddCartAndCloseLg").addClass("disabled");
                $("#btnBuyAndCloseLg").addClass("disabled");
            }
            loadDealComboPackCount($(this).val());
        }

        //取得售價
        var Quantity = $("#cartQuantity").val();
        totalAmount(Quantity);

        //方案數量
        BindCartNumberEvent($(this).val());

        //重新計算尚須購買數量
        AddCartAndCloseLgStutas();

    });
});

function LoadMemberCollection() {
    if ($(".item_grid_float_collect").length > 0) {
        //頁面有愛心收藏
        $.ajax({
            type: "POST",
            url: "/api/CartPpon/GetMemberCollectDeal",
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var data = response.Data;
                var bids = $(".item_grid_float_collect #hidBusinessHourGuid");
                BindMemberCollection(data, bids);
            }, error: function (xhr) {
                console.log(xhr.responseText);
            }
        });
    } else {
        //頁面無收藏
    }
}

function BindMemberCollection(collection, bids) {
    $.each(bids, function (midx, mobj) {
        if (jQuery.inArray($(mobj).html(), collection) != -1) {
            $(mobj).closest("#HKL_Collect").hide();
            $(mobj).parent().parent().next().show();
        }
    });
}

function loadDealComboPackCount(bid) {
    //取得該檔次是否為倍數購買
    $.ajax({
        type: "POST",
        url: "/api/CartPpon/GetDealComboPackCount",
        contentType: "application/json; charset=utf-8",
        data: "{'BusinessHourGuid': '" + bid + "'}",
        async: false,
        success: function (response) {
            $("#item_hidComboPack").html(response.Data);
        }, error: function (xhr) {
            console.log(xhr.responseText);
        }
    });
}

function bindCartTabsEvent() {
    $("ul.item-detail-tabs li").click(function (e) {
        if (!$(this).hasClass("active")) {
            var tabNum = $(this).index();
            var nthChild = tabNum + 1;
            $("ul.item-detail-tabs li.active").removeClass("active");
            $(this).addClass("active");
            $("ul.item-detail-tabcontent li.active").removeClass("active");
            $("ul.item-detail-tabcontent li:nth-child(" + nthChild + ")").addClass("active");
        }
        $('.item-detail-tabcontent').animate({ scrollTop: 0 }, 200);
    });
}

function addCartSpinnerNumberEvent(obj, max, min) {
    //購物車內的數量+-
    $(obj).unbind("click");
    //var minData = $(obj).find("input").val();

    $(obj).WanSpinner({
        maxValue: max,//可選最大數
        minValue: min,//可選最小數
        step: 1,//加減一次多少
        inputWidth: 20,//數字框寬度
        plusClick: function (val) {
            AddCartAndCloseLgStutas();
        },
        minusClick: function (val) {
            AddCartAndCloseLgStutas();
        },
        exceptionFun: function (val) {
            //console.log("excep: " + val);
        },
        valueChanged: function (val) {
            var __Quantity = (val.find("input[type='text']").val());
            var id = (val.find("input[type='text']").attr("id"));
            if (id == "cartQuantity") {
                changeQuantity(__Quantity);
            }
            var plus = $(obj).find(".plus");
            if (__Quantity == max) {
                plus.addClass("disabled");
                plus.css("cursor", "not-allowed");
            } else {
                plus.removeClass("disabled");
                plus.css("cursor", "pointer");
            }

            var minus = $(obj).find(".minus");
            if (__Quantity == 1) {
                minus.addClass("disabled");
                minus.css("cursor", "not-allowed");
            } else {
                minus.removeClass("disabled");
                minus.css("cursor", "pointer");
            }
        }
    });
}

function GetDealDetail(BusinessHourGuid) {
    loadDealComboPackCount(BusinessHourGuid);
    $.ajax({
        type: "POST",
        url: "/api/CartPpon/GetDealDetail",
        data: "{'BusinessHourGuid': '" + BusinessHourGuid + "'}",
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (response) {
            var ddlLength = 0;
            var data = response.Data;

            //購買按鈕預設disabled
            $("#item_options_selected").val("");
            $("#btnAddCartAndCloseLg").addClass("disabled");
            $("#btnBuyAndCloseLg").addClass("disabled");

            //如果是品生活，要換掉Tabs
            if (data.isPiinLife) {
                //替換Tabs
                var piinTab = $("ul.item-detail-tabs");
                piinTab.empty();
                var piinLi = "";
                var piiIdx = 0;
                $.each(data.PiinLifeTabList, function (piiKey, piiValue) {
                    if (piiIdx == 0) {
                        piinLi += "<li>" + piiKey + "</li>";
                    } else {
                        piinLi += "<li>" + piiKey + "</li>";
                    }
                    piiIdx++;
                });
                piinTab.append(piinLi);

                //替換Tabs下方內文
                var piinContent = $("ul.item-detail-tabcontent");
                piinContent.empty();
                var piinLiContent = "";
                $.each(data.PiinLifeTabList, function (piiKey, piiValue) {
                    piinLiContent += "<li>" + piiValue + "</li>";
                });
                piinContent.append(piinLiContent);

                
            } else {
                //替換Tabs
                var piinTab = $("ul.item-detail-tabs");
                piinTab.empty();
                piinTab.append('<li>詳細介紹</li>');
                piinTab.append('<li>商品規格</li>');
                piinTab.append('<li>權益說明</li>');

                //替換Tabs下方內文
                var piinContent = $("ul.item-detail-tabcontent");
                piinContent.empty();
                var piinLiContent = "";
                piinContent.append('<li><div id="detailDesc"></div></li>');
                piinContent.append('<li></li>');
                piinContent.append('<li><div class="EntryZone"><div id="dds" class="dds"></div><div id="Detailinner" class="Equity"></div></div></li>');
                piinContent.append(piinLiContent);

                //初始化下方清單
                $("#item-selector-mixitemcart li").remove();
                $("#item-selector-mixitemcart").hide();
                $("#item-selector-mixitemselect").html(""); //多重選項清空

                //詳細介紹
                $(".item-detail-tabcontent #detailDesc").html(data.Description);
                //權益說明
                $(".item-detail-tabcontent #Detailinner").html(data.Restrictions);
            }

            bindCartTabsEvent();

            //重新點擊第一個頁籤，否則品生活會變空白
            $("ul.item-detail-tabs li").eq(0).click();
            $("#cartQuantity").val("1");    //數量預設帶回1
            AddCartAndCloseLgStutas();

            //BusinessHourGuid
            $("#spanBusinessHourGuid").html(BusinessHourGuid);

            
            //方案數量
            var tmpMaxItemCount = 1;
            if (data.ComboDeals != null) {
               
            } else {
                tmpMaxItemCount = data.MaxItemCount;
            }
            BindCartNumber(tmpMaxItemCount, data.isGroupCoupon);
            
            //icon
            $(".item-detail-tabcontent #dds").html(data.IconTags);
            //多重選項
            var tmpBusinessHourGuid = "";
            var tmpDealName = "";
            if (data.ComboDeals == null) {
                //單檔
                tmpBusinessHourGuid = data.Deal.BusinessHourGuid;
                tmpDealName = data.Deal.DealName;
            }
            ddlLength += BindMultiDeals(tmpBusinessHourGuid, tmpDealName, data.ComboDeals);

            //分店
            $("#store_ppon_selected").empty();
            $("#store_ppon_selected").hide();
            $("#store_ppon_span").html("");
            if (data.PponStores != null) {
                if (data.PponStores.length > 1) {
                    $("#store_ppon_selected").show();
                    //有多分店
                    ddlLength += 1;
                } else {
                    if (data.PponStores.length == 1) {
                        //先不秀文字
                        //$("#store_ppon_span").html(data.PponStores[0].StoreDescription);
                    }
                }
                $("#store_ppon_selected").append($("<option></option>").attr("value", "").text("分店選擇"));
                $.each(data.PponStores, function (key, val) {
                    if (val.IsDisabled) {
                        $("#store_ppon_selected").append($("<option></option>").attr("disabled", "disabled").attr("value", val.StoreGuid).text(val.StoreDescription + "(已售完)"));
                    } else {
                        $("#store_ppon_selected").append($("<option></option>").attr("value", val.StoreGuid).text(val.StoreDescription));
                    }
                });
            }
            //多重選項
            ddlLength += BindItemOptions(data.MultiOptions);

            //預帶金額
            $(".item-selector-form-unit .item-selector-total").html("$" + data.ItemPrice);

            if (ddlLength == 0) {
                OpenAddCartBtnEvent();
            }
        }, error: function (xhr) {
            console.log(xhr.responseText);
        }
    });
}

function BindItemOptions(MultiOptions, MaxItemCount) {
    var ddlLength = 0;
    if (MultiOptions != null && MultiOptions.length > 0) {
        var item_selector_mixitemselect = $("#item-selector-mixitemselect");
        item_selector_mixitemselect.html("");
        item_selector_mixitemselect.html('<label class="">商品規格選擇</label>');
        item_selector_mixitemselect.show();
        $.each(MultiOptions, function (idx, obj) {
            var s1 = $('<select class="item-selector-dropdown cart_item cart_select_items" onchange="clearNextOptions(this);checkAllItemSelected(' + MaxItemCount + ');" />');
            s1.append($("<option></option>").attr("value", "").text(obj.ItemAccessoryGroupListName));
            $.each(obj.GroupMembers, function (midx, mobj) {
                if (mobj.Quantity == null || mobj.Quantity > 0) {
                    s1.append($("<option></option>").attr("value", mobj.AccessoryGroupMemberGUID).text(mobj.AccessoryName));
                } else {
                    s1.append($("<option></option>").attr("value", mobj.AccessoryGroupMemberGUID).attr("disabled", "disabled").text(mobj.AccessoryName + "(已售完)"));
                }

            });
            item_selector_mixitemselect.append(s1);
        });
        //有多選項
        ddlLength += 1;
    } else {
        $("#item-selector-mixitemselect").hide();
    }
    return ddlLength;
}

function BindCartNumberEvent(bid) {
    $.ajax({
        type: "POST",
        url: "/api/CartPpon/GetDealBase",
        contentType: "application/json; charset=utf-8",
        data: "{'BusinessHourGuid': '" + bid + "'}",
        async:false,
        success: function (response) {
            var data = response.Data;
            //重新綁定數量紐
            BindCartNumber(data.MaxItemCount, data.isGroupCoupon);
            //重新載入多重選項
            BindItemOptions(data.MultiOptions, data.MaxItemCount);

        }, error: function (xhr) {
            console.log(xhr.responseText);
        }
    });
}

function BindCartNumber(MaxItemCount, isGroupCoupon) {
    var plus = $("#CartNumber").find(".plus");
    plus.removeClass("disabled");
    plus.css("cursor", "pointer");
    addCartSpinnerNumberEvent($("#CartNumber"), MaxItemCount, 1);

    //成套票券
    if (isGroupCoupon) {
        //數字+-重新給予事件
        plus.addClass("disabled");
        plus.css("cursor", "not-allowed");
        addCartSpinnerNumberEvent($("#CartNumber"), 1, 1);
    }

}

function BindMultiDeals(BusinessHourGuid, DealName, ComboDeals) {
    var ddlLength = 0;
    $("#item_options_selected").empty();    
    $("#item_options_span").html("");
    if (ComboDeals != null) {
        //母子檔
        $("#item_options_selected").show();
        $("#item_options_selected").append($("<option></option>").attr("value", "").text("方案選擇"));
        $.each(ComboDeals, function (key, val) {
            if (val.length > 30) {
                val = val.substring(0, 30);
            }
            $("#item_options_selected").append($("<option></option>").attr("value", key).text(val));
        });
        //有多方案
        ddlLength += 1;
    } else {
        //單檔
        $("#item_options_selected").append($("<option></option>").attr("value", BusinessHourGuid).text(DealName));
        //先不秀文字
        //$("#item_options_span").html(data.Deal.DealName);
    }
    return ddlLength;
}

function OpenAddCartBtnEvent() {
    //如果上方沒有任何一選項需要選擇，則可直接加入購物車
    $(".item-selector-notification").hide();
    $("#btnAddCartAndCloseLg").removeClass("disabled");
    $("#btnBuyAndCloseLg").removeClass("disabled");
}

// 按下搶購/購買鈕後叫出規格彈窗 
function PopWindowLg(obj) {
    if ($(obj).find("#div_combodeallist") != undefined) {
        $(obj).find("#div_combodeallist").removeAttr('style');
    }
    PopBlockUI(obj);

    // 規格彈窗展開時，依規格選項數量、動態調整被選規格清單的最大高度 Start
    $(obj).find('.item-selector-mixitemcart').css('max-height', $(obj).find(".item-detail-tabcontent").height() - $(obj).find(".item-selector-multigrade-setting").height() - $(obj).find(".item-selector-mixitemselect").height() - 105);
    // 規格彈窗展開時，依規格選項數量、動態調整被選規格清單的最大高度 End

}
// 按下右上角X 能關閉彈窗
function blockUICloseLg(obj) {
    $.unblockUI();
    return false;
}
//檢查是否所有項目都已選取
function checkAllItemSelected(MaxItemCount) {
    var flag = true;
    var itemEntry;
    var arrAccessoryEntry = [];
    var pId = "";
    var pName = "";
    var pStoreGuid = "";

    var options = 0;
    var diff = DealCanBuy(MaxItemCount);
    


    $.each($(".cart_item"), function (idx, obj) {
        if ($(obj).is(':visible')) {
            var data = $(obj).val();
            var ItemEntry = {};
            if ($(obj).attr("id") == "item_options_selected") {
                //方案選擇
                pId = $(obj).val();
                pName = $(obj).find('option:selected').text();
            } else if ($(obj).attr("id") == "store_ppon_selected") {
                //分店選擇
                pStoreGuid = $(obj).val();
            } else {
               
                if (diff == 0) {
                    return false;
                }
                options++;
                //好康選擇
                var oKey = $(obj).val();
                var oValue = $(obj).find('option:selected').text();
                var AccessoryEntry = {
                    Name: oValue,
                    Price: 0,
                    AccessoryGroupMemberGUID: oKey,
                    Selected: ""
                };
                arrAccessoryEntry.push(AccessoryEntry);
            }

            if ($.trim(data) == "") {
                flag = false;
            } else {

            }
        }
    });
    itemEntry = {
        Id: pId,
        StoreGuid: pStoreGuid,
        Name: pName,
        OrderQuantity: "",
        SelectedAccessory: arrAccessoryEntry
    };

    if (flag) {
        //判斷有多重選項才要放到購物清單
        if (options > 0 && arrAccessoryEntry.length > 0) {
            //將項目放入下方清單
            AddCartItemList(itemEntry, MaxItemCount);
        } else {
            OpenAddCartBtnEvent();
        }
    }
}

// 當「加入購物車」鈕處可按狀態(沒掛disabled)時，按下會顯示出加入成功的訊息
function AddCartAndCloseLg(obj) {
    if ($(obj).hasClass("disabled")) {
        return false;
    }
    else {

        /*
        * Start
        */
        var bid = $("#spanBusinessHourGuid").html();

        if (bid == "") {
            return false;
        }

        $.ajax({
            type: "POST",
            url: "/api/CartPpon/GetAppPic",
            contentType: "application/json; charset=utf-8",
            data: "{'BusinessHourGuid': '" + bid + "'}",
            async: false,
            success: function (response) {
                $(".add-cart-success-wrap .add-cart-success img").attr("src", response.Data);
            }, error: function (xhr) {
                console.log(xhr.responseText);
            }
        });

        

        
        var BusinessHourGuid = bid;
        var ticketId = loadStorage(cartTicketName);
        if (ticketId == "" || ticketId == null || ticketId == "null" || ticketId == undefined || ticketId == "undefined") {
            ReloadCookie(ticketId);
            ticketId = loadStorage(cartTicketName);
        }
        

        

        var arrItemEntry = getSelectedItems();

        var CartItemDTO = {
            BusinessHourGuid: BusinessHourGuid,
            StoreGuid: $("#store_ppon_selected").val(),
            ItemOptions: arrItemEntry,
            Quantity: $("#cartQuantity").val(),
            TicketId: ticketId
        };

        PutItemToServreCart(CartItemDTO);


        /*
        * End
        */
        


        flyToElement($(".add-cart-success-wrap .add-cart-success img"), $('.mini-cart-btn'), obj);

        $('.blockOverlay').click($.unblockUI);
        setTimeout($.unblockUI, 500);
    }
}

function ReloadCookie(ticketId) {
    if (ticketId == "undefined" || ticketId == undefined || ticketId == "null" || ticketId == null || ticketId == "") {
        $.ajax({
            type: "POST",
            url: "/api/CartPpon/GetTicketId",
            contentType: "application/json; charset=utf-8",
            async: false,
            success: function (response) {
                ticketId = response.Data;
                saveToStorage(ticketId);
            }, error: function (xhr) {
                console.log(xhr.responseText);
            }
        });
    }
}


/*
* 將資料寫入Server
*/
function PutItemToServreCart(CartItemDTO) {
    $.ajax({
        type: "POST",
        url: "/api/CartPpon/PutItemToCart",
        data: JSON.stringify(CartItemDTO),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            var carts = response.Data;
            LoadSmallCartData(carts);
            if (loadStorage() != carts.TransactionId) {
                saveToStorage(carts.TransactionId);
            }
        }, error: function (xhr) {
            console.log(xhr.responseText);
        }
    });
}

function SetHfBusinessHourId(bid) {
    $("#HfBusinessHourId").val(bid);
}

/*
* 重新帶入網頁右上角的小購物車購買數量及金額
*/
function LoadSmallCartData(carts) {
    $(".mini-cart-btn .coupon-item-amount").html(ThousandComma(carts.Amount));
    $(".mini-cart-btn .coupon-item-count").html(carts.Quantity);
}

/*
* 取得購物車下方清單
*/
function getSelectedItems() {
    var AccessoryEntry = [];
    if ($("#item-selector-mixitemcart li").length > 0) {
        //多品項
        
        $.each($("#item-selector-mixitemcart li"), function (idx, objLI) {
            var itemList = [];
            var ospan = $(objLI).find(".selected-item-name").find("span");
            var onum = $(objLI).find(".selected-item-setting").find("input").val();

            $.each(ospan, function (oidx, oobjSpan) {
                var pName = $(oobjSpan).find("div[id='memberName']").html();
                var pGuid = $(oobjSpan).find("div[id='memberGuid']").html();

                var CartItemOption = {
                    OptionName: pName,
                    OptionGuid : pGuid
                };
                itemList.push(CartItemOption);
            });
            var CartItemOptionList = {
                ItemList: itemList,
                ItemQuantity: onum
            };
            AccessoryEntry.push(CartItemOptionList);
        });
    } else {
        //單品項
        AccessoryEntry.push("|#|" + $("#cartQuantity").val());
    }
    //if ($("#item-selector-mixitemcart li").length > 0) {
    //    //多品項
    //    $.each($("#item-selector-mixitemcart li"), function (idx, objLI) {
    //        var ospan = $(objLI).find(".selected-item-name").find("span");
    //        var onum = $(objLI).find(".selected-item-setting").find("input").val();

    //        if (idx > 0) {
    //            AccessoryEntry += "||";
    //        }
    //        $.each(ospan, function (oidx, oobjSpan) {
    //            var pName = $(oobjSpan).find("div[id='memberName']").html();
    //            var pGuid = $(oobjSpan).find("div[id='memberGuid']").html();

    //            if (oidx > 0) {
    //                AccessoryEntry += ",";
    //            }
    //            AccessoryEntry += pName + "|^|" + pGuid;
    //        });
    //        AccessoryEntry += "|#|" + onum;
    //    });
    //} else {
    //    //單品項
    //    AccessoryEntry += $("#item_options_selected").text() + "|^|" + $("#item_options_selected").val() + "|#|" + $("#cartQuantity").val();
    //}
    
    return AccessoryEntry;
}

/*
* 購物車加入下方列表
*/
function AddCartItemList(itemEntry, MaxItemCount) {
    var html = '<li class="selected-item">';
    html += '<div class="selected-item-name pull-left">';
    $.each(itemEntry.SelectedAccessory, function (idx, obj) {
        html += "(" + obj.Name + ")";
        html += "<span>";
        html += "<div id='memberName' style='display:none'>" + obj.Name + "</div>";
        html += "<div id='memberGuid' style='display:none'>" + obj.AccessoryGroupMemberGUID + "</div>";
        html += "</span>";
    });
    html += '</div>';
    html += '<div class="selected-item-setting pull-right">';



    html += '   <div class="wan-spinner wan-spinner-obj">';
    html += '       <a href="javascript:void(0)" class="minus">-</a>';
    html += '           <input type="text" value="1">';
    html += '       <a href="javascript:void(0)" class="plus">+</a>';
    html += '   </div>';
    //html += $(d).html();
    html += '   <a>';
    html += '       <i class="fa fa-trash-o pull-right" aria-hidden="true" onclick="DeleteCartItemList(this)"></i>';
    html += '   </a>';
    html += '</div>';
    html += '</li>';


    var mixitemcart = $("#item-selector-mixitemcart");
    mixitemcart.append(html);
    mixitemcart.show();

    //數字+-重新給予事件
    addCartSpinnerNumberEvent($(".wan-spinner-obj"), MaxItemCount, 1);

    AddCartAndCloseLgStutas();
}

/*
* 購物車刪除下方列表
*/
function DeleteCartItemList(obj) {
    var $tr = $(obj).closest('li');
    $tr.remove();
    var mixitemcartLi = $("#item-selector-mixitemcart li");
    if (mixitemcartLi.length == 0) {
        $("#item-selector-mixitemcart").hide();
    }
    AddCartAndCloseLgStutas();
}

/*
* 清空下一個下拉選項，避免每次選擇時，資料直接加到下方清單列表
*/
function clearNextOptions(obj) {
    var nobj = $(obj).next();
    $(nobj).val("");
    var diff = DealCanBuy();
    if (diff == 0) {
        flag = false;
        alert("您目前購買數量已滿，請調整「方案數量」，或是刪除「商品規格」後，再重新加入。");
        return false;
    }
}

function AddCartAndCloseLgStutas() {
    //還需N件才可加入購物車
    var diff = DealCanBuy();

    var mixitemcartLi = $("#item-selector-mixitemcart li");
    if (mixitemcartLi.length == 0) {
        //尚未選擇任何款式
        $("#btnAddCartAndCloseLg").addClass("disabled");
        $("#btnBuyAndCloseLg").addClass("disabled");

        if ($("#item-selector-mixitemselect .item-selector-dropdown").length > 0 && diff != 0) {
            $(".item-selector-notification").html('<span>還差<span class="text-primary">' + diff + '</span>件商品才可以結帳！</span>');
        } else {
            //沒有選項需要選擇
            var bid = $("#item_options_selected").val();
            if (bid != "") {
                $("#btnAddCartAndCloseLg").removeClass("disabled");
                $("#btnBuyAndCloseLg").removeClass("disabled");
            }
        }
    } else {
        //已選擇款式
        if (diff == 0) {
            //已選購數量 = 必須購買數量
            $("#btnAddCartAndCloseLg").removeClass("disabled");
            $("#btnBuyAndCloseLg").removeClass("disabled");
            $(".item-selector-notification").html("");
        } else {
            $(".item-selector-notification").show();
            if ($("#item-selector-mixitemselect .item-selector-dropdown").length > 0 && diff != 0) {
                $(".item-selector-notification").html('<span>還差<span class="text-primary">' + diff + '</span>件商品才可以結帳！</span>');
            }
            $("#btnAddCartAndCloseLg").addClass("disabled");
            $("#btnBuyAndCloseLg").addClass("disabled");
        }       
    }
}


function DealCanBuy(MaxItemCount) {
    var hidComboPack = $("#item_hidComboPack");
    if (isNaN(hidComboPack.html()) || hidComboPack.html() == "") {
        hidComboPack.html("1");
    }
    var cartQuantity = $("#cartQuantity");
    if (isNaN(cartQuantity.val()) || cartQuantity.val() == "") {
        hidComboPack.html("1");
    }
    //必須購買數量
    var canBuyLimit = parseInt(cartQuantity.val()) * parseInt(hidComboPack.html());

    //計算已選擇數量 
    var totalQuantity = 0;
    $.each($("#item-selector-mixitemcart li"), function (idx, objLI) {
        var oQuantity = $(objLI).find(".selected-item-setting").find("input").val();
        totalQuantity += parseInt(oQuantity);
    });
    var diff = canBuyLimit - totalQuantity;
    if (diff <= 0 || isNaN(diff)) {
        //可購買數量已超過
        //diff = 0;
        $.each($("#item-selector-mixitemcart li"), function (idx, objLI) {
            var max = $(objLI).find(".selected-item-setting").find("input").val();
            var plus = $(objLI).find(".selected-item-setting").find(".plus");
            plus.addClass("disabled");
            plus.css("cursor","not-allowed");
            var obj = $(plus).parent(".wan-spinner-obj");
            //Rebind WanSpinner Event
            addCartSpinnerNumberEvent(obj, max, 1);
        });
    } else {
        $.each($("#item-selector-mixitemcart li"), function (idx, objLI) {
            var max = $(objLI).find(".selected-item-setting").find("input").val();
            var plus = $(objLI).find(".selected-item-setting").find(".plus");
            plus.removeClass("disabled");
            plus.css("cursor", "pointer");
            addCartSpinnerNumberEvent($(".wan-spinner-obj"), MaxItemCount, 1);
        });       
    }

    return diff;
}

/*
* 取得小購物車列表
*/
function GetSamllCartList() {
    var TicketId = loadStorage(cartTicketName);
    if (TicketId == "undefined" || TicketId == undefined || TicketId == "null" || TicketId == null || TicketId == "") {
        TicketId = "";
    }
    $.ajax({
        type: "POST",
        url: "/api/CartPpon/GetCartItem",
        data: "{'TicketId': '" + TicketId + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            var carts = response.Data;
            var html = "";
            $.each(carts.LiteCart, function (idx, liteCarts) {
                $.each(liteCarts.PaymentDTOList, function (sidx, items) {
                    html += '<div class="mini-cart-item" onclick="RedirectUrl(\'' + items.BusinessHourGuid + '\')">';
                    html += '<img class="mini-cart-item-pic" src="' + items.DealPic + '" style="" alt=""  />';
                    html += '<div class="mini-cart-item-title">';
                    html += '<p>' + items.DealName + '</p>';
                    html += '</div>';
                    html += '</div>';

                    if (TicketId == "undefined" || TicketId == undefined) {
                        if (items.TicketId != "") {
                            TicketId = items.TicketId;
                            saveToStorage(TicketId);
                        }
                    }
                });
            });
            $(".mini-cart-wrap .mini-cart-item-list").html(html);
            LoadSmallCartData(carts);
        }, error: function (xhr) {
            console.log(xhr.responseText);
        }
    });
}



/*
* 千分位數字
*/
function ThousandComma(number) {
    var num = number.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(num)) {
        num = num.replace(pattern, "$1,$2");
    }
    return num;
}



function flyToElement(flyer, flyingTo, obj) {
    var $func = $(this);
    var scroll = $(window).scrollTop();
    var divider = 3;
    var flyerClone = $(flyer).clone();
    $(flyerClone).css({ position: 'absolute', top: $(obj).offset().top + "px", left: $(obj).offset().left + "px", opacity: 1, 'z-index': 1000 });
    $('body').append($(flyerClone));
    var gotoX = $(flyingTo).offset().left + ($(flyingTo).width() / 2) - ($(flyer).width() / divider) / 2;
    var gotoY = $(flyingTo).offset().top + ($(flyingTo).height() / 2) - ($(flyer).height() / divider) / 2;

    $(flyerClone).animate({
        opacity: 0.4,
        left: gotoX,
        top: gotoY + scroll,
        width: $(flyer).width() / divider,
        height: $(flyer).height() / divider
    }, 1000,
	function () {
	    $(flyingTo).fadeOut('fast', function () {
	        $(flyingTo).fadeIn('fast', function () {
	            $(flyerClone).fadeOut('fast', function () {
	                $(flyerClone).remove();

                    //動態調整加入成功購物車的位置
	                var gotoX1 = $(flyingTo).offset().left - $(flyingTo).width() - ($(flyingTo).width()/2);
	                $('.add-cart-success-wrap').css({ top: gotoY + $(flyingTo).height() , left: gotoX1, position: 'fixed' });
                    //顯示加入成功購物車
	                $.blockUI({
	                    message: $('.add-cart-success-wrap'),
	                    fadeIn: 500,
	                    fadeOut: 500,
	                    showOverlay: true,
	                    centerY: false,
	                    css: {
	                        border: 'none',
	                    }
	                });
                    //2秒後關閉成功訊息
	                setTimeout($.unblockUI, 2000);
	            });
	        });
	    });
	});
}

function RedirectUrl(url) {
    location.href = url;
}

function loadStorage() {
    var data = window.localStorage[cartTicketName];
    if (data == "undefined" || data == undefined || data == "null" || data == null || data == "") {
        data = getCookie(cartTicketName);
    }
    if (data == "undefined" || data == undefined || data == "null" || data == null || data == "") {
        data = "";
    }
    return data;
}

function saveToStorage(data) {
    if (localStorage[cartTicketName] == "undefined" || localStorage[cartTicketName] == undefined || localStorage[cartTicketName] == "null" || localStorage[cartTicketName] == null || localStorage[cartTicketName] == "" || localStorage[cartTicketName] != data) {
        window.localStorage[cartTicketName] = data;
        if (getCookie(cartTicketName) == "undefined" || getCookie(cartTicketName) == undefined || getCookie(cartTicketName) == "null" || getCookie(cartTicketName) == null || getCookie(cartTicketName) == "" || getCookie(cartTicketName) != data) {
            setCookie(cartTicketName, data, 30);
        }
    }
}

function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value + "; path=/";
}

function getCookie(c_name) {
    var cookievalue = null;
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            cookievalue = unescape(y);
            return cookievalue;
        }
    }
    return "undefined";
}

/*
* 異動數量
*/
function changeQuantity(__Quantity) { 
    totalAmount(__Quantity);
}
function getItemPrice() {
    var bid = $("#item_options_selected").val();
    var itemPrice = 0;
    if (bid != "") {
        $.ajax({
            type: "POST",
            url: "/api/CartPpon/GetDealItemPrice",
            contentType: "application/json; charset=utf-8",
            data: "{'BusinessHourGuid': '" + bid + "'}",
            async: false,
            success: function (response) {
                itemPrice = response.Data;
                
            }, error: function (xhr) {
                console.log(xhr.responseText);
            }
        });
    }
    return itemPrice;
}

/*
* 計算購買總金額
*/
function totalAmount(Quantity) {
    var itemPrice = getItemPrice();

    var amount = 0;
    try {
        amount = itemPrice * parseInt(Quantity);
    } catch (e) {
        amount = 0;
    }
    $(".item-selector-form-unit .item-selector-total").html("$" + ThousandComma(amount));
}

/*
* 前往付款頁面
*/
function gotoOrderList() {
    var tid = loadStorage(cartTicketName);
    location.href = "/UserOrder/OrderList?tid=" + tid;
}

function gotoPay() {
    var tid = loadStorage(cartTicketName);
    location.href = "/UserOrder/OrderPayment?tid=" + tid;
}
﻿window.ProductDeliveryType = {
    Normal: 0,
    FamilyIsp: 1,
    SevenIsp: 2
};
window.getCurrentProductDeliveryType = function() {
    var productDelivery = $(':radio[name="ctl00$ctl00$ML$MC$rdoProductDelivery"]:checked').val();
    if (productDelivery === 'rdoProductDeliveryNormal') {
        return window.ProductDeliveryType.Normal;
    } else if (productDelivery === 'rdoProductDeliveryFamily') {
        return window.ProductDeliveryType.FamilyIsp;
    } else if (productDelivery === 'rdoProductDeliverySeven') {
        return window.ProductDeliveryType.SevenIsp;
    }    
}

window.pay = {
    applePay: {
        beginPayment: function (e) {

            e.preventDefault();

            var subtotal = buyData.subtotal.toString();
            var totoal = buyData.pendingAmount.toString();
           
            var discount = (Number(totoal) - Number(subtotal)).toString();

            var totalForDelivery = {
                label: '17Life',
                amount: totoal
            };

            var lineItemsForDelivery = [
                { label: "商品價格", amount: subtotal, type: "final" },
                { label: "折扣", amount: discount, type: "final" }
            ];

            var paymentRequest = {
                currencyCode: 'TWD',
                countryCode: 'TW',
                lineItems: lineItemsForDelivery,
                total: totalForDelivery,
                supportedNetworks: ['masterCard', 'visa'],
                merchantCapabilities: ['supports3DS'],
                requiredBillingContactFields: ["name"],
                shippingType: 'delivery',
            };
            var version = Number($('#hdSessionVersion').val());
            var session = new ApplePaySession(version, paymentRequest);

            session.onvalidatemerchant = function (event) {
                $.ajax({
                    type: "POST",
                    url: window.ServerLocation + "service/ppon/Validate",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: "{validationURL:'" + event.validationURL + "',domainName:'" + window.location.host + "'}",
                }).then(function (merchantSession) {
                    // Complete validation by passing the merchant session to the Apple Pay session.
                    session.completeMerchantValidation(merchantSession);
                });
            };

            session.onpaymentauthorized = function (event) {

                $.urlParam = function (name) {
                    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                    return results[1] || 0;
                }
                var bid = $.urlParam('bid');

                // Get the contact details for use, for example to
                // use to create an account for the user.
                var billingContact = event.payment.billingContact;
                var shippingContact = event.payment.shippingContact;

                // Get the payment data for use to capture funds from
                // the encrypted Apple Pay token in your server.
                var token = event.payment.token.paymentData;
                var _header = { ephemeralPublicKey: token.header.ephemeralPublicKey, publicKeyHash: token.header.publicKeyHash, transactionId: token.header.transactionId };
                var data = { data: token.data, header: _header, signature: token.signature, version: token.version, bid: bid };
                $.ajax({
                    type: "POST",
                    url: window.ServerLocation + "service/ppon/GoPay",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(data),
                }).success(function (result) {
                    if (result.Code == window.ApiSuccessCode) {
                        $('#hdPrebuiltOrderGuid').val(result.Data.PrebuiltOrderGuid);
                        session.completePayment(ApplePaySession.STATUS_SUCCESS);
                        $('#btnPayWayProcess').map(function () {
                            var obj = this;
                            $(obj).click();
                        });
                    } else {
                        session.completePayment(ApplePaySession.STATUS_FAILURE);
                        return false;
                    }
                });

                window.pay.applePay.showSuccess();
            }
            session.begin();
        },
        supportedByDevice: function () {
            return "ApplePaySession" in window;
        },
        supportsSetup: function () {
            return "openPaymentSetup" in ApplePaySession;
        },
        showButton: function () {
            var button = $("#btnForApplePay");
            button.attr("lang", window.pay.applePay.getPageLanguage());
            button.bind("click", window.pay.applePay.beginPayment);
            button.addClass("btn");
            button.addClass("apple-pay");
            button.addClass("apple-pay-button");
            button.addClass("apple-pay-button-black");
        },
        showSetupButton: function () {
            var button = $("#set-up-apple-pay-button");
            button.attr("lang", window.pay.applePay.getPageLanguage());
            button.on("click", window.pay.applePay.setupApplePay);
            button.removeClass("hide");
        },
        showError: function (text) {
            var error = $(".apple-pay-error");
            error.text(text);
            error.removeClass("hide");
        },
        showSuccess: function (text) {
            $(".apple-pay-intro").hide();
            var success = $(".apple-pay-success");
            success.removeClass("hide");
            success.show();
        },
        getPageLanguage: function () {
            return $("html").attr("lang") || "en";
        },
    },
}

$(document).ready(function() {

    window.BuyEvents = {};

    window.BuyEvents.OnIspQuantityChange = $.Callbacks();
    window.BuyEvents.OnIspQuantityChange.add(function() {
        var qty = Math.ceil(window.getQtyAtCart() / window.getComboPack());
        var ispQtyLimit = window.getIspQtyLimit();
        var productDelivery = getCurrentProductDeliveryType();
        //if (productDelivery === window.ProductDeliveryType.Normal) {
            //return;
        //}
        var excceedLimit = qty > ispQtyLimit;

        if (excceedLimit) {
            $('#rdoProductDeliveryNormal').attr('checked', true);
            if (productDelivery !== window.ProductDeliveryType.Normal) {
                window.BuyEvents.OnProductDeliveryChange.fire();
            }
            window.setTimeout(function() {
                    $('.productDeliveryGroup').not($('#rdoProductDeliveryNormal').parents('.productDeliveryGroup'))
                        .hide();
                },
                10);
        } else {
            $('.productDeliveryGroup').show();
        }
        window.BuyEvents.OnIspOptionsShowOrHide.fire();
    });

    window.BuyEvents.OnIspOptionsShowOrHide = $.Callbacks();
    window.BuyEvents.OnIspOptionsShowOrHide.add(function() {
        window.setTimeout(function() {
                $('.productDeliveryGroup').find('.title').hide();
                $('.productDeliveryGroup:visible').eq(0).find('.title').show();
            },
            10);
    });

    window.BuyEvents.OnBtnApplePayClick = $.Callbacks();
    window.BuyEvents.OnBtnApplePayClick.add(function () {
        if (window.ApplePaySession) {
            if (ApplePaySession.canMakePayments() === true) {
                window.pay.applePay.showButton();
            }
        }
    });

    window.BuyEvents.OnProductDeliveryChange = $.Callbacks();
    window.BuyEvents.OnProductDeliveryChange.add(function() {
        window.setTimeout(function() {
                $('#tdRecAddr,#tdStorePickup').hide();
                $('#pickupStoreSelect,#pickupStoreShow').hide();
                $('#iconCvsSeven,#iconCvsFamily').hide();
                var productDelivery = getCurrentProductDeliveryType();

                if (productDelivery === window.ProductDeliveryType.Normal) {
                    $('#tdRecAddr').show();
                    $('#receiverMobileLabel').text('聯絡電話');
                    $('#receiverMobileErr').find('errorMessage').text('請填寫收件人的聯絡電話！');
                    $('.ispHint').hide();
                } else if (productDelivery === window.ProductDeliveryType.FamilyIsp) {
                    $('#tdStorePickup').show();
                    $('#iconCvsFamily').show();
                    $('#receiverMobileLabel').text('收件人手機');
                    $('#receiverMobileErr').find('errorMessage').text('請確認格式，僅可輸入純數字10碼，ex：0912345678！');
                    $('.ispHint').show();
                    if ($('#hidFamilyPickupStoreId').val() === '') {
                        $('#pickupStoreSelect').show();
                    } else {
                        $('#pickupStoreShow').show();
                        $('#pPickupTitle').text($('#hidFamilyPickupStoreName').val() +
                            " " +
                            $('#hidFamilyPickupStoreAddr').val());
                        $("#ConfirmReceiverAddress").text($('#pPickupTitle').text());
                        $('#productDeliveryUnitTitle').text('取貨門市');
                        $('#cProductDeliveryTypeRow').show();
                        $('#cProductDeliveryType').text('全家取貨');
                    }
                } else if (productDelivery === window.ProductDeliveryType.SevenIsp) {
                    $('#tdStorePickup').show();
                    $('#iconCvsSeven').show();
                    $('#receiverMobileLabel').text('收件人手機');
                    $('#receiverMobileErr').find('errorMessage').text('請確認格式，僅可輸入純數字10碼，ex：0912345678！');
                    $('.ispHint').show();
                    if ($('#hidSevenPickupStoreId').val() === '') {
                        $('#pickupStoreSelect').show();
                    } else {
                        $('#pickupStoreShow').show();
                        $('#pPickupTitle').text($('#hidSevenPickupStoreName').val() +
                            " " +
                            $('#hidSevenPickupStoreAddr').val());
                        $("#ConfirmReceiverAddress").text($('#pPickupTitle').text());
                        $('#productDeliveryUnitTitle').text('超商取貨');
                        $('#cProductDeliveryTypeRow').show();
                        $('#cProductDeliveryType').text('7-11取貨');
                    }
                }
            },10);
    });

    window.BuyEvents.OnProductDeliveryChange.add(function () {
        var productDelivery = getCurrentProductDeliveryType();
        if (productDelivery === window.ProductDeliveryType.Normal) {
            window.setTimeout(function() {
                $('#familyIspPayWay').hide();
                $('#sevenIspPayWay').hide();
                $('#rbPayWayByCreditCard').attr('checked', true);
            },10);
        } else if (productDelivery === window.ProductDeliveryType.FamilyIsp) {
            window.setTimeout(function() {                    
                    $('#familyIspPayWay').show();
                    $('#sevenIspPayWay').hide();
                    $('#rbPayWayByCreditCard').attr('checked', true);
                },
                10);
        } else if (productDelivery === window.ProductDeliveryType.SevenIsp) {
            window.setTimeout(function () {
                    $('#sevenIspPayWay').show();
                    $('#familyIspPayWay').hide();
                    $('#rbPayWayByCreditCard').attr('checked', true);
                },
               10);
        }
        $(".form-unit.SelectedPayWay").find('.paywayLabel').remove();
        $(".form-unit.SelectedPayWay:visible").first().find(".data-input").before("<label for='' class='unit-label title paywayLabel'>付款方式</label>");
    });

});

//公司統編驗證
function checkCompanyTaxNumber(taxId) {
    var invalidList = "00000000,11111111";
    if (/^\d{8}$/.test(taxId) === false || invalidList.indexOf(taxId) !== -1) {
        return false;
    }

    var validateOperator = [1, 2, 1, 2, 1, 2, 4, 1],
        sum = 0,
        calculate = function (product) { // 個位數 + 十位數
            var ones = product % 10,
                tens = (product - ones) / 10;
            return ones + tens;
        };
    for (var i = 0; i < validateOperator.length; i++) {
        sum += calculate(taxId[i] * validateOperator[i]);
    }

    return sum % 10 === 0 || (taxId[6] === "7" && (sum + 1) % 10 === 0);
};

//Pcash綁定帳號
function BindingPezMember(userName, memNum) {
    var isSuccess = false;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "../ppon/buy.aspx/BindingPezMember",
        async: false,
        data: JSON.stringify({
            'userName': userName,
            'memNum': memNum
        }),
        dataType: "json",
        success: function (data) {
            var result = $.parseJSON(data.d);
            if (!result.IsSuccess) {
                alert(result.Message);
            }
            isSuccess = result.IsSuccess;
        },
        error: function (data) {
            isSuccess = false;
            alert(data);
        }
    });

    return { 'isSuccess': isSuccess};
}

//Pcash儲值至17life
function DepositScash(userName, memNum, deductContent, merchantContent, returnCode) {

    var sumScash = 0;
    var newMerchantContent = '';
    var isSuccess = false;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "../ppon/buy.aspx/DepositScash",
        async: false,
        data: JSON.stringify({
            'userName': userName,
            'memNum': memNum,
            'deductContent': deductContent,
            'merchantContent': merchantContent,
            'returnCode': returnCode
        }),
        dataType: "json",
        success: function (data) {
            var result = $.parseJSON(data.d);
            if (result.IsSuccess)
            {
                sumScash = result.SumSacsh;
                newMerchantContent = result.NewMerchantContent;
                isSuccess = true;
            }
            else
            {
                isSuccess = false;
                alert(result.Message);
            }
        },
        error: function (data) {
            isSuccess = false;
            alert(data);
        }
    });

    return { 'isSuccess': isSuccess, 'sumScash': sumScash, 'newMerchantContent': newMerchantContent };


}

//Pcash儲值ErrorLog
function DepositErrorLog(userName, returnCode) {

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "../ppon/buy.aspx/DepositErrorLog",
        async: false,
        data: JSON.stringify({
            'userName': userName,
            'returnCode': returnCode
        }),
        dataType: "json",
        success: function (data) {
            
        },
        error: function (data) {
            isSuccess = false;
            alert(data);
        }
    });

}
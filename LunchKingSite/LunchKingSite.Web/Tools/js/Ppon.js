﻿//------------------------------------------------//
//版本:0.0.0.6
//目前只整合了default,delivery,todaydeal
//------------------------------------------------//

//公用函數
//site.master的JS先放在公用函數,因為部分的JS不知道有沒有作用,所以先原封不動的移過來,有問題再說
var ServerLocation = "/";
var fbShareWindow;
var fbJumpWindowLeft = ($(window).width() - 700) / 2;
var fbJumpWindowTop = ($(window).height() - 420) / 2;
window.ApiSuccessCode = 1000;
window.EmailDomainSource = ["yahoo.com.tw", "hotmail.com", "gmail.com", "outlook.com", "kimo.com", "msa.hinet.net", "msn.com",
    "yahoo.com", "17life.com", "hotmail.com.tw", "taishinbank.com.tw", "xuite.net", "livemail.tw", "yam.com", "payeasy.com.tw",
    "ymail.com", "mail2000.com.tw", "seed.net.tw", "facebook.com", "tsmc.com", "taipower.com.tw", "itri.org.tw"];
//判斷正式站跟測試站
if (window.document.location.pathname.toLowerCase().indexOf('lksite/') > 0) {
    ServerLocation = "/lksite/";
}
if (window.document.location.pathname.toLowerCase().indexOf('releasebranch/') > 0) {
    ServerLocation = "/releasebranch/";
}
if (window.document.location.pathname.toLowerCase().indexOf('uat/') > 0) {
    ServerLocation = "/uat/";
}

//jQuery升級1.9 兼容$.browser()
jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();

function $$(id, context) {
    var el = $("#" + id, context);
    if (el.length < 1)
        el = $("*[id$=" + id + "]", context);
    return el;
}


function doCookie(days) {
    var date = new Date();
    //obselete
    //setCookie("EventEmailCookie", date, parseInt(days, 16));
}

function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    c_value = c_value + "; Secure";
    document.cookie = c_name + "=" + c_value + "; path=/";
}

function getCookie(c_name) {
    var cookievalue = null;
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            cookievalue = unescape(y);
            return cookievalue;
        }
    }
    return "undefined";
}

function redirect(id) {
    $.ajax({
        type: "POST",
        url: ServerLocation + "ppon/default.aspx/GetUrl",
        data: "{'city': '" + id + "' }",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            url = msg.d;
            setTimeout("comedownredirect()", 3000);
        }
    });
}
function comedownredirect() {
    $.unblockUI();
    document.location.href = url;
}
function PopBlockUI(obj) {
    var top = 0;
    if ($(obj).height() < $(window).height()) {
        top = ($(window).height() - $(obj).height()) / 2;
    } else {
        if ($(obj).find(".mgs-item-box").length > 7 && $(obj).find("#div_combodeallist") != undefined) {
            var combodealheight = $(window).height() - (150);
            $(obj).find("#div_combodeallist").css('height', combodealheight + 'px').css('overflow', 'auto');
            top = ($(window).height() - $(obj).height()) / 2;
        }
    }
    var left = 0;
    if ($(obj).width() < $(window).width()) {
        left = ($(window).width() - $(obj).width()) / 2;
    }
    if (obj.selector = '#Pop_MailCoupon') {
        switch ($(window).width()) {
            case 280:
                left = "-220";
                break;
            case 320:
                left = "-190";
                break;
            case 360:
                left = "-170";
                break;
            case 375:
                left = "-160";
                break;
            case 412:
                left = "-135";
                break;
            case 414:
                left = "-142";
                break;            
        }
    }
    $.blockUI({ message: $(obj), css: { border: 'none', background: 'transparent', width: '0px', height: '0px', cursor: 'default', top: top + 'px', left: left + 'px' } });    
}
function PopWindow(obj) {
    if ($(obj).find("#div_combodeallist") != undefined) {
        $(obj).find("#div_combodeallist").removeAttr('style');
    }
    if (document.documentElement.clientWidth > 1000 || obj.selector == '#Pop_MailCoupon') {
        PopBlockUI(obj);
    } else {
        $(obj).css('display', '');
        $('.block-background').css('display', 'none');
        $('.center').css('width', '100%').css('margin', '0');
        $('#Left').css('display', 'none');
        gototop();
    }
}
function blockUIClose(obj) {
    if (document.documentElement.clientWidth > 1000) {
        $('#iMap').attr('src', 'about:blank'); // 避免unblockUI reload iframe內容，因此先清空src使其沒有內容
        $.unblockUI();
    }
    else {
        var CityId = $('#HfCityId').val();
        $(obj).parent('div').hide();

        $('.center').css('width', '').css('margin', '');
        $('#Left').show();
        $("#NaviCity").hide();
        $.unblockUI();
    }
    return false;
}

function popupOpen() {
    $.blockUI({
        message: $("div#open1"),
        css: {
            top: ($(window).height() - 500) / 2 + 'px',
            left: ($(window).width() - 700) / 2 + 'px',
            width: '0px',
            height: '0px',
            border: 'none',
            cursor: 'default'
        },
        overlayCSS: { cursor: 'default' }
    });
    $('.blockOverlay').click($.unblockUI);
}

function setupPopup(desc) {
    if ("PEZ" == desc) {
        $("div#PEZdesc").show();
    } else {
        $("div#Normaldesc").show();
    }

    $("div.LMPezCertificationClose").click(function () { $.unblockUI(); });

    $("#mtxtPassword").focus(function () {
        $("#spanPassword").html('6~12碼，英文或數字，英文請區分大小寫');
        $("#linePassword").removeClass("LMROPBgColor").addClass("LMROPBg");
        result = false;
    });

    $("#mtxtConPassword").focus(function () {
        $("#spanConPassword").html('請重新再輸入一次密碼！');
        $("#lineConPassword").removeClass("LMROPBgColor").addClass("LMROPBg");
        result = false;
    });
}

function reSend(mail) {
    $.dnajax(ServerLocation + 'service/ppon/SendAuthMail', "{ email: '" + mail + "' }",
        function (res) {
            if (res.Code == window.ApiSuccessCode) {
                alert("17Life已將認證信寄到您的信箱中，請您前往查看並盡速認證，謝謝！");
            } else {
                alert(res.Message);
            }
        });
}

function checkOpenInfo() {
    var result = true;

    // password check
    if (!passwordCheck($("#mtxtPassword").val())) {
        $("#spanPassword").html('<span style="color: #F00; letter-spacing: normal;">密碼格式錯誤！</span>');
        $("#linePassword").removeClass("LMROPBg").addClass("LMROPBgColor");
        result = false;
    }

    // confirm password check
    if (!passwordConfirm($("#mtxtPassword").val(), $("#mtxtConPassword").val())) {
        $("#spanConPassword").html('<span style="color: #F00; letter-spacing: normal;">您輸入的密碼不符合，請重新輸入！</span>');
        $("#lineConPassword").removeClass("LMROPBg").addClass("LMROPBgColor");
        result = false;
    }

    if (result) {
        $("#mhidPassword").val(encodeURIComponent($("#mtxtPassword").val()));
    }

    return result;
}

function checkMobile(mobile) {
    var tenNumber = /^[0][9]\d{8}$/;
    if (tenNumber.test(mobile)) {
        return true;
    }
    return false;
}

function checkAccount(account) {
    if (account == null) return false;
    account = $.trim(account);
    if (account == "") {
        return false;
    }
    var regMail = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
    var res = regMail.test(account);
    if (res) {
        res = checkEmailTwoDots(account);
    }
    return res;
}

function checkEmailOrPhone(value) {
    var isPhone = (/^09\d{2}-?\d{3}-?\d{3}$/.test(value)) || value.length === 10;
    return isPhone || checkAccount(value);
}

function keyCodeToNumberForIOS(keyCode) {
    switch (keyCode) {
        case 48:
            return 0;
        case 49:
            return 1;
        case 50:
            return 2;
        case 51:
            return 3;
        case 52:
            return 4;
        case 53:
            return 5;
        case 54:
            return 6;
        case 55:
            return 7;
        case 56:
            return 8;
        case 57:
            return 9;
        default:
            return undefined;
    }
}

function keyCodeToNumber(keyCode) {
    switch (keyCode) {
        case 96:
            return 0;
        case 97:
            return 1;
        case 98:
            return 2;
        case 99:
            return 3;
        case 100:
            return 4;
        case 101:
            return 5;
        case 102:
            return 6;
        case 103:
            return 7;
        case 104:
            return 8;
        case 105:
            return 9;
        default:
            return undefined;
    }
}


// <summary>
// 檢查2個點dot不能重複相鄰的出現
// </summary>
// <param name="email"></param>
// <returns>false 表示這個狀態發生，此 email 不符合格式</returns>
function checkEmailTwoDots(email) {
    var pos = -1;
    for (var i = 0; i < email.length; i++) {
        var ch = email.charAt(i);
        if (ch == '.') {
            if (pos >= 0 && i - pos == 1) {
                return false;
            }
            pos = i;
        }
    }
    return true;
}

function passwordCheck(password) {
    var pwdPattern = /^[0-9a-zA-Z]{6,100}$/;
    if (pwdPattern.test(password)) {
        return true;
    } else {
        return false;
    }
}

function passwordConfirm(password, conPassword) {
    if (password == conPassword) {
        return true;
    } else {
        return false;
    }
}
jQuery.fn.watermark = function (className, message) {
    $(this).addClass(className);
    $(this).val(message);
    $(this).focus(function () {
        var str = $(this).val();
        if (str == message) {
            $(this).val("");
            $(this).toggleClass(className);
        }
    });
    $(this).blur(function () {
        var str = $(this).val();
        if (str.length <= 0) {
            $(this).toggleClass(className);
            $(this).val(message);
        }
    });
};
function bii() {
    $.blockUI({ message: $('#PeaceOfMindGuarantee'), css: { border: 'none', background: 'transparent', width: '0px', height: '0px', top: ($(window).height() - 350) / 2 + 'px', left: ($(window).width() - 700) / 2 + 'px' } });
}
function bi() {
    $.blockUI({ message: $('#newpoplogin'), css: { border: 'none', background: 'transparent', width: '0', top: ($(window).height()) / 8 + 'px', left: ($(window).width() - 700) / 2 + 'px' } });

    $('#hc').click(function () {
        $.unblockUI();
        return false;
    });
}

function showmap(o, enableOpenStreetMap) {

    var address = $(o).attr('address');
    var longitude = $(o).attr('longitude');
    var latitude = $(o).attr('latitude');

    var q = latitude != 'Null' && latitude != undefined && latitude != 0 && latitude != '' &&
            longitude != 'Null' && longitude != undefined && longitude != 0 && longitude != ''
            ? latitude + ',' + longitude : address;

    if (address.length > 0) {

        if (document.documentElement.clientWidth > 1000) {
            $.unblockUI();
            $.blockUI({
                message: $('#blockMap'),
                css: {
                    backgroundcolor: 'transparent',
                    border: 'none',
                    top: $(window).height() / 8 + 'px',
                    left: ($(window).width() - 686) / 2 + 'px',
                    cursor: 'pointer',
                    width: '700px',
                    height: '520px'
                }
            });
            if (enableOpenStreetMap.toLowerCase() == 'true') {

                // use OpenStreetMap
                $('#iMap').css('display', 'none');
                $('#map').empty(); // 因OpenStreetMap會一直append新的地圖，必須清空

                map = new OpenLayers.Map("map");
                map.addLayer(new OpenLayers.Layer.OSM());

                var lonLat = new OpenLayers.LonLat(longitude, latitude)
                      .transform(
                        new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
                        map.getProjectionObject() // to Spherical Mercator Projection
                      );
                var zoom = 16;

                var markers = new OpenLayers.Layer.Markers("Markers");
                map.addLayer(markers);
                markers.addMarker(new OpenLayers.Marker(lonLat));
                map.setCenter(lonLat, zoom);

            } else {
                // use GoogleMap with iframe
                $('#iMap').attr('src', 'https://maps.google.com/maps?f=q&source=s_q&z=16&hl=zh-TW&geocode=&ie=UTF8&iwloc=A&output=embed&q=' + q);
            }
        } else {
            // RWD Map need open window
            window.open('http://maps.google.com.tw/maps?f=q&hl=zh-TW&z=16&t=&iwloc=near&q=' + q);
        }
    } else {
        alert("Google Map 查無此地址!!");
    }
}
function SortCategory(type) {
    $.ajax({
        type: "POST",
        url: ServerLocation + 'ppon/default.aspx/SetSortCategory',
        data: '{"type":' + type + '}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (msg) {
            location.href = location.href;
        }
    });
}

//篩選相關
function SetSelectedFilter(channel, id) {
    var isChecked = $('#filter' + id).is(':checked');
    $.ajax({
        type: "POST",
        url: ServerLocation + 'ppon/default.aspx/SetSelectedFilter',
        data: JSON.stringify({ "channel": channel, "id": id, "isChecked": isChecked }),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (msg) {
            location.href = location.href;
        }, error: function (err) {
            alert('error');
        }
    });
}

function Redirect(url, params) {

    url = url || window.location.href || '';
    url = url.match(/\?/) ? url : url + '?';

    for (var key in params) {
        var re = RegExp('&?' + key + '=?[^&;]*', 'g');
        url = url.replace(re, '');
        if (params[key] != '') {
            url += '&' + key + '=' + params[key];
        }
    }
    // cleanup url 
    url = url.replace(/[&]$/, '');
    url = url.replace(/\?[&]/, '?');
    url = url.replace(/[&]{2}/g, '&');
    url = url.replace(/\?$/, '');
    window.location.replace(url);
};

function getUrlQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair.length < 2) return "";
        if (pair[0].toLowerCase() == variable.toLowerCase()) {
            return vars[i].substring(vars[i].indexOf('=') + 1, vars[i].length);
        }
    }
    return "";
}

function UserTrackSet(t, u) {
    switch (t) {
        case 1:
            $.ajax({
                type: "POST",
                url: ServerLocation + 'ppon/default.aspx/PostToFaceBook',
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '') {
                        location.href = u.href;
                    }
                }
            });
            break;
        case 2:
            $.ajax({
                type: "POST",
                url: ServerLocation + 'ppon/default.aspx/PostToPlurk',
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '') {
                        location.href = u.href;
                    }
                }
            });
            break;
    }
    return false;
}
function push() {
    $.ajax({
        type: "POST",
        url: ServerLocation + 'ppon/default.aspx/GetDescription',
        data: { 'bid': Ppon.module.Default.BusinessHourId },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.d != '') {
                location.href = response.d;
            }
            else {
                alert('請登入後再發佈呦!!');
            }
        }
    });
}
function ShowInfo(IsShow) {

    if (IsShow) {
        $("#einfo").show();
    }
    else {
        $("#einfo").hide();
    }
}
function gototop() {
    $('html, body').animate({
        scrollTop: 0
    }, 800);
    return false;
}
function ShareLink() {
    $.ajax({
        type: "POST",
        url: ServerLocation + 'ppon/default.aspx/GetShareLink',
        data: "{businessHourId:'" + Ppon.module.GetBusinessHourGuid() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg.d != '') {
                showshare(msg.d);
            } else {
                bi();
            }
        }
    });
}
function ShareFB() {

    $.ajax({
        type: "POST",
        url: ServerLocation + 'ppon/default.aspx/PostToFaceBook',
        data: "{businessHourId:'" + Ppon.module.GetBusinessHourGuid() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg.d != '') {
                showshareFB(Ppon.module.GetBusinessHourGuid());
            } else {
                fbShareWindow.close();
                bi();
            }
        }
    });
}
function showshare(tinyurl) {
    $('#tbx_ReferralShare').val(tinyurl);
    $.blockUI({ message: $('#Referral'), css: { border: 'none', width: '0px', background: 'transparent', top: ($(window).height() - 420) / 2 + 'px', left: ($(window).width() - 700) / 2 + 'px' } });
}
function showshareFB(bid) {
    fbShareWindow.location = ServerLocation + 'ppon/fbshare_proxy.aspx?type=facebook&bid=' + bid;
}
function preventDobuleSubmit() {
    $('form').submit(function () {
        if (window.submitted) {
            $.blockUI({
                message: "<h2 style='padding:20px; letter-spacing:2px'> 資料已送出，請稍後 ...</h2>",
                css: { left: ($(window).width() - 300) / 2 + 'px', width: '300px' }
            });
            return false;
        }
        window.submitted = true;
    });
}
var delayFunction = function (mainFunc, option) {
    var delayTimer;
    option = option || {};
    if (!option.delay) {
        return mainFunc;
    } else {
        return function () {
            var that = this;
            var args = arguments;
            if (delayTimer) clearTimeout(delayTimer);
            delayTimer = setTimeout(function () {
                mainFunc.apply(that, args);
            }, option.delay);
        };
    }
};
function RedirectUrl(url) {
    var is_safari_or_uiwebview = /(iPhone|iPod|iPad).*AppleWebKit/i.test(navigator.userAgent);
    if (is_safari_or_uiwebview) {
        window.open(url);
    } else {
        location.href = url;
    }
}
function setScrollTopVisible() {
    var showValue = ($("#smartbanner").css("top") == "0px") ? (150 + 86) : 150;
    var scroll_top = $(window).scrollTop();
    if (scroll_top >= showValue) {
        $("#btnBackTop").css('display', 'inline');
    } else {
        $("#btnBackTop").css('display', 'none');
    }
    if ($('#Left').length > 0) {
        var left_area_position_top = $('#Left').offset().top;
        var right_area_height = $('#Rightarea').height();

        var right_margin_top = parseInt($('#Rightarea').css('marginTop').split('px')[0]);
        var window_height = $(window).height();
        var foot_height = $('.p3footer').height();

        if (isNaN(right_margin_top)) {
            right_margin_top = 0;
        }
        var left_area_height = $('#Left').height();
        var max = left_area_height - right_area_height;

        if (max > 0) {
            if ((scroll_top + window_height) >= (right_area_height + left_area_position_top) && (scroll_top > left_area_position_top)) { 
                if ((left_area_height) > (right_area_height + right_margin_top)) { 
                    var new_top = scroll_top + window_height - right_area_height - left_area_position_top; 
                    if (new_top > max) {
                        new_top = max;
                    }
                    //console.log('max = ' + max);
                    //console.log('new_top = ' + new_top);
                    $('#Rightarea').animate({ marginTop: new_top },20);
                }
                else {
                    var new_top = scroll_top - right_area_height + window_height - left_area_position_top;
                    if (new_top <= max) {
                        console.log('new_top = ' + new_top);
                        $('#Rightarea').animate({ marginTop: new_top }, 20);
                    } else {
                        console.log('max = ' + max);
                        console.log('left_area_height = ' + left_area_height);
                        console.log('right_area_height = ' + right_area_height);
                        console.log('right_margin_top = ' + right_margin_top);
                        $('#Rightarea').animate({ marginTop: max }, 20);
                    }
                }
            } else {
                $('#Rightarea').animate({ marginTop: 0 }, 20);
            }
        }
    }
}

//***********************************************************************************//
//所有跟17life相關的JS
//命名原則依據頁面在module後面加上class EX:default.aspx的命名就是Ppon.module.default
//對於每個頁面用一個div包住該頁面所有的element,id就用此頁面命名(暫定,規則可以隨時修改)
//dModule參數是從頁面取得的dom obj,所以要用jquery的選擇器的話,要用$(dModule)
//***********************************************************************************//
var Ppon = {};
Ppon.module = {
    MasterPageType: '',
    PhoneMaxWidth: 480,
    BusinessHourGuid: null,
    GetBusinessHourGuid: function () {
        if (Ppon.module.BusinessHourGuid == null) {
            if ($.Guid.IsEmpty($('#HfBusinessHourId').val())) {
                return $.Guid.Empty();
            } else {
                Ppon.module.BusinessHourGuid = $('#HfBusinessHourId').val();
                return Ppon.module.BusinessHourGuid;
            }
        } else {
            return Ppon.module.BusinessHourGuid;
        }
    },
    //defaule.aspx以及delivery.aspx有共用的function,全部整理到這邊來
    Common: function (dModule) {
        var HfIsOpenNewWindow = $(dModule).find('#HfIsOpenNewWindow');
        var HfIsMobileBroswer = $(dModule).find('#HfIsMobileBroswer');
        var WebNaviCity = $(dModule).find('#WebNaviCity');
        var NaviCity = $(dModule).find('#NaviCity');
        var mbeSwitch = $(dModule).find('#mbeSwitch');
        var center = $(dModule).find('.center');
        var btnBackTop = $(dModule).find('#btnBackTop');
        var ousideAd = $(dModule).find('#ousideAd');
        var discount_min_inner = $(dModule).find('#discount_min_inner');
        var tbx_encore_email = $(dModule).find('#tbx_encore_email');
        var einfo = $(dModule).find('#einfo');
        var subRegionId = $(dModule).find('#HfSubRegionId').val();
        var categoryId = $(dModule).find('#HfDealCategoryId').val();
        var travelCategoryId = $(dModule).find('#HfTravelCategoryId').val();
        var femaleCategoryId = $(dModule).find('#HfFemaleCategoryId').val();
        var navBtnCouponList = $(dModule).find('.navbtnCouponList');
        var navBtnDiscountList = $(dModule).find('.navbtnDiscountList');
        var navBtnBonusListNew = $(dModule).find('.navbtnBonusListNew');
        var navBtnUserAccount = $(dModule).find('.navbtnUserAccount');
        var navBtnCollect = $(dModule).find('.navbtnCollect');
        var navBtnServiceRecord = $(dModule).find('.navbtnServiceRecord');
        var eventBannerClose = $(dModule).find('.close-button');
        var mMenu = $(dModule).find('#mMenu');
        var overChannel = $('.overlay-channel');
        var cityBtn = $(dModule).find(".city a");
        var classifyBtn = $(dModule).find(".classify a");
        var sortBtn = $(dModule).find(".sort a");
        var filterBtn = $(dModule).find(".filter a");
        var x = $(window).width();
        if (HfIsMobileBroswer.val() == "True") {
            $(dModule).find('#ulSwitch').hide();
        }

        center.css('overflow', 'visible');

        if ($.trim(discount_min_inner.text()).length == 0) {
            discount_min_inner.parent().css("display", "none;");
        }
        if (HfIsOpenNewWindow.val() == "True" && HfIsMobileBroswer.val() == "False") {
            $('.open_new_window').attr('target', '_blank');
            $('.sidedealbuybtn').attr('target', '_blank');
        }

        //分類3
        $('.NaviCityML .areatip-zone').hide();
        $('.NaviCityML a').css("cursor", "pointer");
        $('.NaviCityML .areatip-zone span:eq(1)').html("");

        $('.NaviCitySort').show();
        //今日開賣
        if (location.pathname.toLowerCase().indexOf('/ppon/todaydeal.aspx') >= 0 ||
            location.pathname.toLowerCase().indexOf('/m/todaydeals') >= 0) {
            $('#naviTodaydeal').removeClass().addClass('navbtn_inpage');
            $('#m_navToday').removeClass().addClass('on');
            $('#m_ChooseChannel').text($('#m_navToday').text());
            if (x > Ppon.module.PhoneMaxWidth) {
                //NaviCity.show();
                WebNaviCity.show();
            }
        }
        //優惠特輯
        else if (location.pathname.toLowerCase().indexOf('/event/themecurationchannel') >= 0 ||
            location.pathname.toLowerCase().indexOf('/event/themecurationchannelmobile') >= 0) {
            $('#m_navThemeChannel').removeClass().addClass('on');
            $('#m_ChooseChannel').text($('#m_navThemeChannel').text());
        }
        else if (categoryId == PponJSCityGroup.Travel) {
            $('#mOverlayChannelName').html('旅遊');
            mMenu.removeClass("col-1-2").removeClass("col-1-3").addClass("col-1-4");
            $('#naviTravelLink').removeClass().addClass('navbtn_inpage');
            $('#m_naviTravelLink').removeClass().addClass('navbtn_hit');
            $('#m_navTravel').removeClass().addClass('on');
            $('#m_ChooseChannel').text($('#m_navTravel').text());
            if (x > Ppon.module.PhoneMaxWidth) {
                //NaviCity.show();
                WebNaviCity.show();
            }
        }
        else if (categoryId == PponJSCityGroup.PBeautyLocation) {
            $('#mOverlayChannelName').html('玩美‧休閒');
            mMenu.removeClass("col-1-2").removeClass("col-1-3").addClass("col-1-4");
            $('.naviPeautyLink').removeClass().addClass('navbtn_inpage');
            $('#m_naviPeautyLink').removeClass().addClass('navbtn_hit');
            $('#m_navBeauty').removeClass().addClass('on');
            $('#m_ChooseChannel').text($('#m_navBeauty').text());
            if (x > Ppon.module.PhoneMaxWidth) {
                WebNaviCity.show();
            }
        }
        else if (categoryId == PponJSCityGroup.AllCountry || location.pathname.toLowerCase().indexOf('fastdelivery') > 0) {
            $('#mOverlayChannelName').html('宅配');
            mMenu.removeClass("col-1-2").removeClass("col-1-4").addClass("col-1-3");
            $('.naviDeliveryLink').removeClass('navbtn').addClass('navbtn_inpage');
            $('#m_naviDeliveryLink').removeClass('navBtn').addClass('navbtn_hit');
            $('#m_navDelivery').removeClass().addClass('on');
            $('#m_ChooseChannel').text($('#m_navDelivery').text());
            NaviCity.removeClass('NaviCityMP');
            NaviCity.hide();
            WebNaviCity.hide();
            mbeSwitch.removeClass('mbe-switch');
            $("li[id^=PiinlifeCategory_]").hide();
        }
        else if (categoryId == PponJSCityGroup.Family) {
            $('#mOverlayChannelName').html('全家專區');
            mMenu.hide();
            NaviCity.hide();
            $('#navFamiLink').removeClass().addClass('navbtn_inpage');
            $('#m_navFamiLink').removeClass().addClass('navbtn_hit');
            $('#m_navFamily').removeClass().addClass('on');
            $('#m_ChooseChannel').text($('#m_navFamily').text());
            NaviCity.removeClass('NaviCityMP');
            mbeSwitch.removeClass('mbe-switch');
        }
        else if (categoryId == PponJSChannelId.Food) {
            $('#mOverlayChannelName').html('美食');
            mMenu.removeClass("col-1-2").removeClass("col-1-3").addClass("col-1-4");
            $('#naviIndexLink').removeClass().addClass('navbtn_inpage');
            $('#m_naviIndexLink').removeClass().addClass('navbtn_hit');
            $('#m_navIndex').removeClass().addClass('on');
            $('#m_ChooseChannel').text($('#m_navIndex').text());
            if (x > Ppon.module.PhoneMaxWidth) {
                //NaviCity.show();
                WebNaviCity.show();
            }
        }
        
        if (categoryId != PponJSCityGroup.Travel) {
            if ($("#PiinlifeCategory_149").length > 0) {
                $("#PiinlifeCategory_149").insertAfter("#CategoryUl li:eq(0)");
            }
        }
        if ($("#PiinlifeCategory_322").length > 0) {
            $("#PiinlifeCategory_322").insertAfter("#CategoryUl li:eq(0)");
        }
        if ($("#PiinlifeCategory_156").length > 0) {
            $("#PiinlifeCategory_156").insertAfter("#CategoryUl li:eq(0)");
        }

        $('.side_item a').hover(function () {
            $(this).find('.side_item_price span.price').css('color', '#fff');
            $(this).find('.side_item_price span.discount').css('color', '#fff');
            $(this).find('.side_item_price .oriprice').css('color', '#fff');
            $(this).find('.side_item_price .tag_buycounter').css('color', '#fff');
        }, function () {
            $(this).find('.side_item_price span.price').css('color', '#bf0000');
            $(this).find('.side_item_price span.discount').css('color', '#999');
            $(this).find('.side_item_price .oriprice').css('color', '#999');
            $(this).find('.side_item_price .tag_buycounter').css('color', '#999');
        });

        //棋盤熱銷輪播
        if ($('.tc_big_content_slider_wrap').length > 0) {
            $(function () {
                // 設定每隔 N 秒切換下一張
                // 及淡出淡入的速度
                var timer = 40000;
                var speed = 5000;
                var hint_r = 0;
                var hint_l = 0;
                var ind;
                // 先隱藏 $myImages
                $('.list .b_content_box').hide();
                $('.tc_big_content_slider_wrap .link').append('<ul />');
                for (var i = 0; i < $('.list .b_content_box').length * 0.5; i++) {
                    // 依 $myImages 中的數量來產生相對數量的 li
                    $('.tc_big_content_slider_wrap .link ul').append('<li>' + '</li>');
                }

                $('.slider-btn-next').click(function () {
                    if (ind >= $('.list .b_content_box').length * 0.5 - 1) {
                        ind = 0;
                        $('.tc_big_content_slider_wrap .link li').eq(ind).click();
                    } else {
                        ind = ind + 1;
                        $('.tc_big_content_slider_wrap .link li').eq(ind).click();
                    }
                });
                $('.slider-btn-prev').click(function () {
                    if (ind <= 0) {
                        ind = 1;
                        $('.tc_big_content_slider_wrap .link li').eq(ind).click();
                    } else {
                        ind = ind - 1;
                        $('.tc_big_content_slider_wrap .link li').eq(ind).click();
                    }
                });

                // 在.link 中加入一個ul
                $('.tc_big_content_slider_wrap .link ul li').click(function () {
                    //取得目前點擊的序號 
                    var num_now = $(this).index();
                    ind = num_now;
                    var $link = $('.list .b_content_box').eq($(this).index()); //取得待顯示的         
                    $('.list .b_content_box').fadeIn(400).hide();
                    $('.list .b_content_box').eq(num_now * 2).show();
                    $('.list .b_content_box').eq(num_now * 2 + 1).show();
                    $(this).addClass('on').siblings('.on').removeClass('on'); //讓 li 加上 on 的樣式
                });

                // 當滑鼠移入到 #big_content_area 時
                $('.tc_big_content_slider_wrap').hover(function () {
                    clearTimeout(timer); //移入時停止計時器
                    $('.tc_big_content_slider_wrap .slider-btn').fadeIn(300).css('display', 'block');
                }, function () {
                    timer = setTimeout(autoShowSlider, speed); //滑出時啟動計時器
                    $('.tc_big_content_slider_wrap .slider-btn').css('display', 'none');
                });

                // 控制輪播用function
                function autoShowSlider() {
                    ind = $('.tc_big_content_slider_wrap .link .on').index();
                    ind = (ind + 1) % 2;              //2組輪播 
                    $('.tc_big_content_slider_wrap .link ul li').eq(ind).click();   //點擊下一位 
                    timer = setTimeout(autoShowSlider, speed);    //計時器開始 
                }
                autoShowSlider();
            });
        }

        var locationUrl = window.document.location.pathname.toLowerCase();
        if (locationUrl.indexOf('coupon_list') > 0) {
            navBtnCouponList.removeClass('navbtn').addClass('navbtn_inpage');
        } else if (locationUrl.indexOf('discountlist') > 0) {
            navBtnDiscountList.removeClass('navbtn').addClass('navbtn_inpage');
        } else if (locationUrl.indexOf('bonuslistnew') > 0) {
            navBtnBonusListNew.removeClass('navbtn').addClass('navbtn_inpage');
        } else if (locationUrl.indexOf('useraccount') > 0) {
            navBtnUserAccount.removeClass('navbtn').addClass('navbtn_inpage');
        } else if (locationUrl.indexOf('collect') > 0) {
            navBtnCollect.removeClass('navbtn').addClass('navbtn_inpage');
        } else if (locationUrl.indexOf('servicerecord') > 0) {
            navBtnServiceRecord.removeClass('navbtn').addClass('navbtn_inpage');
        }

        //M版排序
        switch ($('#HfShortType').val()) {
            case CategorySortType.Default:
                $(".sortName").html('推薦排序<span class="icon-chevron"></span>');
                break;
            case CategorySortType.TopNews:
                $(".sortName").html('最新排序<span class="icon-chevron"></span>');
                break;
            case CategorySortType.TopOrderTotal:
                $(".sortName").html('銷量排序<span class="icon-chevron"></span>');
                break;
            case CategorySortType.PriceAsc:
                $(".sortName").html('價格排序<span class="icon-chevron"></span>');
                break;
            case CategorySortType.PriceDesc:
                $(".sortName").html('價格排序<span class="icon-chevron"></span>');
                break;
            default:
                $('.sortName').html('推薦排序<span class="icon-chevron"></span>');
                break;
        }

        //M版分類
        var categoryName = $.trim($(".NaviCitySortRank").find("a.selected:first").text());
        if (categoryName == "全部") { categoryName += "分類"; }
        $(".categoryName").html(categoryName + '<span class="icon-chevron"></span>');
        if ($(window).width() > 480) {
            $(".NaviCitySortRank  ul[id^='sub-classify_']").each(function () {
                if ($(this).find("a.selected").length > 0) {
                    $(this).show();
                    var parentCategoryId = $(this).attr('id').replace('sub-classify_', '');
                    if ($(".NaviCitySortRank  li[id='dealcategory_" + parentCategoryId + "']").length > 0) {
                        $(".NaviCitySortRank  li[id='dealcategory_" + parentCategoryId + "']").removeClass('on').addClass('on');
                    }
                }
            });
        }

        navBtnCouponList.click(function () {
            location.href = ServerLocation + 'User/coupon_List.aspx';
            navBtnCouponList.unbind("click");;
            return false;
        });

        navBtnDiscountList.click(function () {
            location.href = ServerLocation + 'User/DiscountList.aspx';
            navBtnCouponList.unbind("click");;
            return false;
        });

        navBtnBonusListNew.click(function () {
            location.href = ServerLocation + 'User/bonuslistnew.aspx';
            navBtnCouponList.unbind("click");;
            return false;
        });

        navBtnUserAccount.click(function () {
            location.href = ServerLocation + 'User/UserAccount.aspx';
            navBtnCouponList.unbind("click");;
            return false;
        });

        navBtnCollect.click(function () {
            location.href = ServerLocation + 'User/Collect.aspx';
            navBtnCouponList.unbind("click");;
            return false;
        });

        navBtnServiceRecord.click(function () {
			location.href = ServerLocation + 'User/ServiceList';
            navBtnServiceRecord.unbind("click");;
            return false;
        });

        eventBannerClose.click(function () {
            $(this).parent().hide();
            return false;
        });

        $(window).scroll(setScrollTopVisible);

        $(window).scroll(function () {
            var main_top = 0;
            var divCategory = $("[id$='_divCategory']");//分類區塊
            var maincontent = $("#maincontent");//檔次列表區塊
            if (divCategory != null && divCategory.position() != null) {
                main_top = divCategory.position().top;
            }
            else if (maincontent != null && maincontent.position() != null) {
                main_top = maincontent.position().top;
            }

            var scroll_top = $(window).scrollTop();
            if (ousideAd != null) {
                var ousideAdPosition = ousideAd.position();

                if (scroll_top >= main_top) {
                    ousideAd.addClass('OAD_fixed');
                } else {
                    ousideAd.removeClass('OAD_fixed');
                }
            }
        });

        function ShowInfo(IsShow) {
            if (IsShow) {
                einfo.show();
            }
            else {
                einfo.hide();
            }
        }

        // 搜尋bar
        // SELECT
        $('#querystring').on('keydown', function (e) {
            if ($('.suggestionList li').size() > 0){
                if (e.keyCode == 40) {
                    if ($('.active').size() == 0) {
                        $('.suggestionList li:eq(0)').addClass('active');
                    } else {
                        if ($('.suggestionList li').last().is($('.active'))) {
                            $('.active').removeClass('active');
                            $('.suggestionList li').first().addClass('active').focus();
                        } else {
                            $('.active').removeClass('active').next('li').addClass('active').focus();
                        }
                    }
                    $('#querystring').val($('.active').text());
                } else if (e.keyCode == 38) {
                    if ($('.suggestionList li').first().is($('.active'))) {
                        $('.active').removeClass('active');
                        $('.suggestionList li').last().addClass('active').focus();
                    }
                    else {
                        $('.active').removeClass('active').prev('li').addClass('active').focus();
                    }
                    $('#querystring').val($('.active').text());
                }
            }
        });

        // ENTER TO SEARCH
        $('#querystring').keypress(function (e) {
            if (e.keyCode == 13) {
                $('#btnPponSearch').click();
                return false;
            }
        });

        $('#querystring').on('keyup', function (e) {
            $('#querystring').on('keydown', function (e) { console.log('keydown:' + e.keyCode); });

            // BUILD SUGGESTION
            if ((e.keyCode == 32) || !(e.keyCode >= 9 && e.keyCode <= 46) || (e.keyCode >= 112 && e.keyCode <= 137)) {
                var searchWords = $.trim($('#querystring').val());
                if (searchWords.length > 0) {
                    $.ajax({
                        type: "POST",
                        url: "https://" + window.location.host + "/m/GetKeywordPromptList",
                        data: JSON.stringify({ 'word': encodeURI(searchWords), 'takeCnt': 10 }),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (result) {
                            if (result.success) {
                                if (result.promptList.length > 0) {
                                    var data = '';
                                    $.each(result.promptList, function (index, value) {
                                        if (index == 0) {
                                            data += "<li class='sugg' >" + value + "</li>";
                                        }
                                        else {
                                            data += "<li class='sugg' >" + value + "</li>";
                                        }
                                    });
                                    $('.suggestionList ul').html(data);
                                    $('.suggestionBox').show();
                                } else {
                                    $('.suggestionList ul li').remove();
                                }
                            } else {
                                $('.suggestionList ul li').remove();
                            }
                        }
                    });

                    $('.suggestionList').on("click", ".sugg", function () {
                        $('#querystring').val($(this).text());
                        $('#btnPponSearch').click();
                    });
                }
            }
            // BACKSPACE
            if (e.keyCode == 8 && $('#querystring').val() == "" || e.keyCode == 27) {
                $('.suggestionBox').hide();
                $('.suggestionList ul li').remove();
            }
        });
        
        // CLICK TO CANCEL
        $('body').click(function (e) {
            $('.suggestionBox').hide();
        });


    },

    NewsInit: function () {

        $('.slide-bn-area').css("display", "");
        var bannerSwiper = new Swiper('#pponnews', {            
            pagination: '.swiper-pagination',
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            paginationClickable: true,
            spaceBetween: 3,
            centeredSlides: true,
            loop: true,
            autoplay: (($('#pponnews .swiper-slide').length == 1) ? false : 5000),
            autoplayDisableOnInteraction: false
        });

        var mobileSwiper = new Swiper('#pponnews_mbrowser', {
            pagination: '.pagination',
            loop: true,
            grabCursor: true,
            paginationClickable: true,
            slidesPerView: 'auto',
            autoplay: (($('#pponnews_mbrowser .swiper-slide').length == 1) ? false : 5000)
        });

        if ($.browser.msie && 9 > $.browser.version) {
            $('#newscontent').hide();
            $('#newscontent').parent('[id*=TOPBanner]').parent('.slide-bn-block').hide();

            $('#newscontent-m').hide();
            $('#newscontent-m').parent('[id*=TOPBanner]').parent('.slide-bn-block').hide();
        } else {

            if ($(window).width() >= 768) {
                $('.slide-m-browser').hide();
                if ($('#pponnews .swiper-slide').length == 1) {
                    $('#newscontent').parent('[id*=TOPBanner]').parent('.slide-bn-block').find('.prev').hide();
                    $('#newscontent').parent('[id*=TOPBanner]').parent('.slide-bn-block').find('.next').hide();
                }

            } else {
                $('.slide-bn-block').hide();
                if ($('#pponnews_mbrowser .swiper-slide').length == 1) {
                    $('#newscontent-m').parent('[id*=TOPBanner]').parent('.slide-bn-block').find('.prev').hide();
                    $('#newscontent-m').parent('[id*=TOPBanner]').parent('.slide-bn-block').find('.next').hide();
                }

            }
        }

        if ($(window).width() >= 768) {

            $('.slide-bn-block .prev').on('click', function (e) {
                e.preventDefault();
                bannerSwiper.swipePrev();
            });
            $('.slide-bn-block .next').on('click', function (e) {
                e.preventDefault();
                bannerSwiper.swipeNext();
            });

        } else {

            $('.slid-banner-hide .prev').on('click', function (e) {
                e.preventDefault();
                mobileSwiper.swipePrev();
            });
            $('.slid-banner-hide .next').on('click', function (e) {
                e.preventDefault();
                mobileSwiper.swipeNext();
            });
        }

    },
    NewsVisible: function (newscontent, Marketingarea) {
        if ($.trim(newscontent.find('div:first').html()) == '' || newscontent.find('div:first').html() == null) {
            newscontent.hide();
            newscontent.parent('[id*=TOPBanner]').parent('.slide-bn-block').hide();
        } else {
            newscontent.show();
            newscontent.parent('[id*=TOPBanner]').parent('.slide-bn-block').show();
        }
        if ($.trim(Marketingarea.find('div:first').html()) == '' || Marketingarea.find('div:first').html() == null) {
            Marketingarea.hide();
        }
    },
    OutSideADInit: function (ousideAd) {
        if ((navigator.userAgent.indexOf('Android') != -1)) {
            ousideAd.css('display', 'none');
        }
        if ((navigator.userAgent.indexOf('iPhone') != -1)
            || (navigator.userAgent.indexOf('iPod') != -1)
            || (navigator.userAgent.indexOf('iPad') != -1)) {
            ousideAd.css('display', 'none');
        }
    },
    SetDealCollect: function (isCollecting, isLeftBtn) {
        if (!$.Guid.IsEmpty(Ppon.module.GetBusinessHourGuid())) {
            var cityId = $('#HfCityId').val();
            var seperator = (window.location.href.indexOf("?") === -1) ? "?" : "&";

            var loginUrl = $('#HfLoginUrl').val() + ((isLeftBtn == true) ? encodeURIComponent(seperator + "action=collecting") : encodeURIComponent(seperator + "action=collect"));
            $.ajax({
                type: "POST",
                url: ServerLocation + "Ppon/Default.aspx/SetMemberCollectDeal",
                data: "{businessHourGuid:'" + Ppon.module.GetBusinessHourGuid() + "', cityId:" + cityId + ", isCollecting:" + isCollecting + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var result = $.parseJSON(msg.d);
                    if (result.Success) {
                        if (result.IsCollecting) {
                            x = $(window).width();
                            if (x >= 753) {
                                var collectSuccessMsg = $('#collectSuccessMsg');
                                var leftCollectSuccessMsg = $('#leftCollectSuccessMsg');
                                if (isLeftBtn) {
                                    leftCollectSuccessMsg.show();
                                    setTimeout(function () {
                                        leftCollectSuccessMsg.fadeOut(2000);
                                    }, 2500);
                                } else {
                                    collectSuccessMsg.show();
                                    setTimeout(function () {
                                        collectSuccessMsg.fadeOut(2000);
                                    }, 3000);
                                }
                            }
                            Ppon.module.SetCollectDiv(true);
                        } else {
                            Ppon.module.SetCollectDiv(false);
                        }
                    }
                }, error: function (response, q, t) {
                    if (t == 'userNotLogin') {
                        location.href = loginUrl;
                    }
                }
            });
        }
        return false;
    },
    SetCollectDiv: function (IsCollected) {
        var collectDeal = $("#divCollectDeal");
        var leftCollectDeal = $("#divLeftCollectDeal");
        var infoHeart = $('.info_heart');
        if (IsCollected) {
            collectDeal.html("<div id='HKL_Collect-ok'><a href='#' OnClick='return Ppon.module.SetDealCollect(false, false)'>已收藏</a></div>");
            leftCollectDeal.html("<div class='outTOPT-collect-ok'><p class='outTOPT outTOPT-cohover' OnClick='return Ppon.module.SetDealCollect(false, true)'>已收藏</p></div>");
            infoHeart.html("<a href='javascript:void(0)' onclick='return Ppon.module.SetDealCollect(false, false)'  class='on'><i class='fa fa-heart' aria-hidden='true'></i><span class='timer_text'> 收藏成功</span></a>");
        } else {
            collectDeal.html("<div id='HKL_Collect'><a href='#' OnClick='return Ppon.module.SetDealCollect(true, false)'>收藏</a></div>");
            leftCollectDeal.html("<div class='outTOPT-collect'><p class='outTOPT outTOPT-cohover' OnClick='return Ppon.module.SetDealCollect(true, true)'>收藏</p></div>");
            infoHeart.html("<a href='javascript:void(0)' onclick='return Ppon.module.SetDealCollect(true, false)' class='off'><i class='fa fa-heart-o' aria-hidden='true'></i><span class='timer_text'> 收藏</span></a>");
        }
    },
    OpenSharePopWindow: function (shareUrl, shareType) {
        if ($("#HfIsLogin").val() == "0") {
            $("#HfMShareType").val(shareType);
            $.blockUI({ message: $('.share-popW'), css: { border: 'none', background: 'transparent', width: '0', top: ($(window).height()) / 4 + 'px', left: ((($(window).width() / 2) - ($('.share-popW').width() / 2)) - 10) + 'px' } });
        }
        else {
            window.open(shareUrl);
        }
    },
    ShareWithOutLogin: function (shareContent, shareUrl) {
        var shareType = $("#HfMShareType").val();
        if (shareType == "fb") {
            RedirectUrl("http://www.facebook.com/share.php?u=" + shareUrl);
        }
        else if (shareType == "line") {
            RedirectUrl("http://line.me/R/msg/text/?" + encodeURI(shareContent + " " + shareUrl));
        }
        else {
            $.unblockUI();
        }
    },
    RedirectLoginPage: function (loginUrl) {
        RedirectUrl(loginUrl);
    },
    CloseSharePopWindow: function () {
        $.unblockUI();
    },
    CloseCollectSuccessMsg: function () {
        $('.collectSuccessBox').hide();
    },
    SendAppDownloadSMS: function (phoneNo) {
        $.ajax({
            type: "POST",
            url: ServerLocation + "Ppon/Promo.aspx/SendAppDownloadSms",
            data: "{phone:'" + phoneNo + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                var result = $.parseJSON(msg.d);
                alert(result.Message);
                $(".PhoneBTN").parent().find("input[id=textfield]").val("");
            }
        });
    },
    Default: function(dModule) {
        var top = 0 //讓下載APP往下沉
        var device = '';
        var scale = 'auto';
        var HfHami = $(dModule).find('#HfHami');
        var TaiShinPop = $(dModule).find('#TaiShinPop');
        var HfCloseAppBlockHiddenDays = $(dModule).find('#HfCloseAppBlockHiddenDays');
        var HfViewAppBlockHiddenDays = $(dModule).find('#HfViewAppBlockHiddenDays');
        var map = $(dModule).find('#map');
        var newscontent = $(dModule).find('#newscontent');
        var newscontent_m = $(dModule).find('#newscontent-m');
        var Marketingarea = $(dModule).find('#Marketingarea');
        var HaokangList = $(dModule).find('.HaokangList');
        var MobileSlideBox = $(dModule).find('#slideBox');
        //var outlTAll = $(dModule).find('[id*="outlTAll"]');
        var outlTAll = $(dModule).find('div.forlist');
        var dealAnchorPoint = $(dModule).find('#dealAnchorPoint');
        var isDeliveryDeal = $(dModule).find('#HfIsDeliveryDeal');
        var goTop3 = $(dModule).find('.goTop3');
        var isLogin = $(dModule).find('#HfIsLogin');
        var mMenu = $('#mMenu');
        var mHotSaleArea = $(dModule).find('#mHotSaleArea');
        var overChannel = $('.overlay-channel');
        var isComplete = true;
        var HfIsMobileBroswer = $(dModule).find('#HfIsMobileBroswer');
        var stickers = $(dModule).find('#Stickers');
        var insideAd = $(dModule).find('.inside_AD');
        var activeBlock = $(dModule).find('#active_block');
        var newsBlock = $(dModule).find('#news_block');
        var sideBlock = $(dModule).find('.side_block');
        var saleBBlock = $(dModule).find('#saleB_block');
        var sidedeal = $(dModule).find('#Sidedeal');
        var isFromTaishinBankApp = $(dModule).find('#HfIsFromTaishinBankApp');
        var relatedDealsLoaded = false;

        //m版熱銷
        var isSelectedMobileChannel = true;
        var partialurl = window.location.href.substring(window.location.href.indexOf($('#PponLOGO').attr('href')), window.location.href.length);
        var partialDefaulturl = $('#PponLOGO').attr('href');
        if (partialurl != null && (partialurl == partialDefaulturl || partialurl == partialDefaulturl + 'default.aspx')) {
            isSelectedMobileChannel = false;
        }
        if (window.addEventListener && !isSelectedMobileChannel && $(window).width() <= Ppon.module.PhoneMaxWidth && $.Guid.IsEmpty(Ppon.module.GetBusinessHourGuid())) {
            mHotSaleArea.show();
            if ($.browser.msie && 9 > $.browser.version) {
                $("#slideBox").slide({ titCell: ".hd ul", mainCell: ".bd ul", effect: "leftLoop", autoPage: true, autoPlay: false });
            } else {
                TouchSlide({ slideCell: "#slideBox", titCell: ".hd ul", mainCell: ".bd ul", effect: "leftLoop", autoPage: true, autoPlay: false });
            }
            $('#slideBox li').each(function () {
                var obj = $(this).find('#Hotdeal1');
                if ($(obj).find(".open_new_window").length > 0) {
                    var link = $(obj).find(".open_new_window")[0].href;
                    $(obj).click(function () {
                        location.href = link;
                    });
                }
            });

            //mMenu.css("display", "none");
            //overChannel.hide();
            $("#maincontent").hide();

            MobileGEOLocationCheck(); //手機定位檢查
            //setCookie('IsMobileSelectedChannel', "true", 1);
        } else {
            $("#maincontent").show();
        }

        if ($(window).width() > Ppon.module.PhoneMaxWidth) {
            $("#maincontent").show();
        }

        //如果有 bid
        if (!$.Guid.IsEmpty(Ppon.module.GetBusinessHourGuid())) {
            //$("NaviCity").hide();
            $(".NaviCitySequence").hide();
            mMenu.css("display", "none").hide();
            $('.m-browser').parent().hide();
            $('.NaviCityML').hide();
            // 詳細介紹移除img width & height setting
            $('#detailDesc img').css('height', 'auto');
            // 宅配檔隱藏店家資訊錨點
            if (isDeliveryDeal.val() == 'True') { goTop3.hide(); }
            $(".slider").each(function (index, elem) {
                if ($(this).find("img").length > 1) {
                    $(this).nivoSlider();
                }
                else {
                    $(this).removeAttr("id");
                }
            });
            //台新銀行App進入隱藏分享送500
            if (isFromTaishinBankApp.val() == 'True') { $('.m-share').hide(); }

            $("#detailDesc").find("img").lazyload({ placeholder: ServerLocation + "Themes/PCweb/images/ppon-M1_pic.jpg", effect: "fadeIn" });

            var picAlt = $("#HfPicAlt").val();
            if (picAlt) {
                $("#detailDesc").find("img").each(function () {
                    if (!$(this).attr("alt")) {
                        $(this).attr("alt", picAlt);
                    }
                });
            }
            //取得收藏狀態
            if (isLogin.val() == "1") {
                var action = getUrlQueryVariable("action");
                if (action == "collect") {
                    Ppon.module.SetDealCollect(true, false);
                }
                else if (action == "collecting") {
                    Ppon.module.SetDealCollect(true, true);
                } else {
                    $.ajax({
                        type: "POST",
                        url: ServerLocation + "Ppon/Default.aspx/CheckMemberCollectDealStatus",
                        data: "{businessHourGuid:'" + Ppon.module.GetBusinessHourGuid() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            if (msg.d == true) {
                                Ppon.module.SetCollectDiv(true);
                            } else {
                                Ppon.module.SetCollectDiv(false);
                            }
                        }
                    });
                }
            } else {
                Ppon.module.SetCollectDiv(false);
            }
        }
        else {
            $('#relateDealArea').hide();
            if ($(window).width() <= Ppon.module.PhoneMaxWidth) {
                $(".HaokangList").each(function (i, obj) {
                    if ($(obj).find(".open_new_window").length > 0) {
                        var link = $(obj).find(".open_new_window")[0].href;
                        $(obj).click(function () {
                            location.href = link;
                        });
                    }
                });
                $("#HfBindDivClick").val("1");
            }
            $(window).resize(delayFunction(function () { bindMultiDealClickResize(); }, { delay: 300 }));
            
            if (location.href.indexOf("TodayDeal.aspx") > -1) {
                $("img.multipleDealLazy").lazyload({ placeholder: ServerLocation + "Themes/PCweb/images/ppon-M1_pic.jpg", threshold: 1500, effect: "fadeIn" });
                //default load top 6 deal img
                var dealPicLength = $("img.multipleDealLazy").length;
                dealPicLength = (dealPicLength > 6) ? 6 : dealPicLength;
                for (i = 0; i < dealPicLength; i++) {
                    $($("img.multipleDealLazy")[i]).attr("src", $($("img.multipleDealLazy")[i]).attr("data-original"));
                    $($("img.multipleDealLazy")[i]).removeAttr("data-original");
                }
            }
        }

        if (Ppon.module.MasterPage.categoryId == PponJSCityGroup.Family) {
            mMenu.hide();
        }

        if (dealAnchorPoint.html() != null) {
            $(window).scroll(delayFunction(function () {
                var scroll_top = $(window).scrollTop();
                if (scroll_top >= 162) {
                    dealAnchorPoint.addClass('OutTop_fixed');
                } else {
                    dealAnchorPoint.removeClass('OutTop_fixed');
                }
            }, { delay: 300 }));
        }
        if (HfHami.val() == 'hinet_hammi') {
            top = 43;
            $('#wrap .toolbarMenu').show();
            $('html').css('margin-top', top);
        }
        // 行動裝置(不含iPad)出現建議下載app
        if (navigator.userAgent.match(/Android/i)) {
            device = 'android';
        } else if (navigator.userAgent.match(/iPod|iPhone|iPad/i)) {
            if (navigator.userAgent.indexOf('iPad') < 0) {
                if (navigator.userAgent.match(/crios/i)) {
                    device = 'ios';
                } else if (navigator.userAgent.match(/safari/i)) {
                    device = ''; // Safari有設定自動產生App元件
                } else {
                    device = 'ios';
                }
            }
        } else {
            // 行動裝置不跳窗
            if (Ppon.module.MasterPageType == "taishin") {
                TaiShinPop.css("background-image", "url(https://www.17life.com/images/17P/Promo/Taishin/popwindow.jpg)");
                $.blockUI({
                    message: TaiShinPop, css: {
                        backgroundcolor: 'transparent', cursor: 'auto', border: 'none',
                        top: ($(window).height() - 440) / 2 + 'px', left: ($(window).width() - 640) / 2 + 'px'
                    }
                });
            } else if (window.showEventActivity == true) {
                window.showEventActivityTime = new Date();
                Ppon.module.MasterPage.showSubscription();
            }
        }

        Ppon.module.NewsVisible(newscontent, Marketingarea);
        Ppon.module.NewsInit();
        if (outlTAll.length != 0) {
            new DealCountdown().trigger(outlTAll, true);
        }
        
        function bindMultiDealClickResize() {
            if ($(window).width() <= Ppon.module.PhoneMaxWidth) {
                if ($("#HfBindDivClick").val() == "0") {
                    $(".HaokangList").each(function (i, obj) {
                        var link = $(obj).find(".open_new_window")[0].href;
                        $(obj).click(function () {
                            location.href = link;
                        });
                    });
                    $("#HfBindDivClick").val("1");
                }
            } else {
                if ($("#HfBindDivClick").val() == "1") {
                    $(".HaokangList").each(function (i, obj) {
                        $(obj).unbind("click");
                    });
                    $("#HfBindDivClick").val("0");
                }
            }
        }

        $(".slide-bn-area").css('display', '');

        function multiplecountdown(outlTAll, year, month, day, hour, minute, sec) {
            var eday = new Date(year, month, day, hour, minute, sec);
            outlTAll.countdown({
                until: eday, format: 'dHMS',
                layout: '<span class="icon-clock-o"></span><div id="TimerField"><div class="TimeDayCounter" >{dn}{dl}</div><div class="TimeConDigit">{hn}</div><div class="TimeConUnit">{hl}</div><div class="TimeConDigit">{mn}</div><div class="TimeConUnit">{ml}</div><div class="TimeConDigit">{sn}</div><div class="TimeConUnit">{sl}</div></div>',
                expiryUrl: document.location.href
            });
        }

        if (detectBrowser() > 0) {
            $(window).load(delayFunction(function () { defaultResizefun(); }, { delay: 900 }));
        } else if (detectBrowser() == 0) {
            $(window).load(delayFunction(function () { defaultResizefun(); }, { delay: 300 }));
        } else {
            $(window).load(delayFunction(function () { defaultResizefun(); }, { delay: 300 }));
        }

        $(window).resize(delayFunction(function () { defaultResizefun(); }, { delay: 300 }));

        function detectBrowser() {
            //-1:Chrome and Other,0:Firefox,0 up:IE
            var sAgent = window.navigator.userAgent;
            var Idx = sAgent.indexOf("MSIE");

            if (Idx > 0) {
                return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf(".", Idx)));
            } else if (!!navigator.userAgent.match(/Trident\/7\./)) {
                return 11;
            } else {
                if (navigator.userAgent.match("Firefox")) {
                    return 0;
                } else {
                    return -1;
                }
            }
        }

        function defaultResizefun() {
            
            if (HfIsMobileBroswer.val() == "True") {
                SetWidthHeigth();
            }

            x = $(window).width();
            
            $('.hover_place').css('display', 'none');
            $('.item_wrap').unbind();//不管resize觸發後 會作動幾次每一次就先unbind 反正最後一次都會再重新加入hover
            if (x > 1025) {
                $('.item_wrap').hover(function () {
                    $(this).find('.hover_place').css('display', 'block');
                }, function () {
                    $(this).find('.hover_place').css('display', 'none')
                });
            }
            if (x < 1025 && x > 1023) {
                $("#Left .item_wrap").removeClass("item_wrap_grid").addClass("item_wrap_list")
                $('.item_price_more').css('display', 'block');
                $('.item_price_less').css('display', 'none');
            }
            if (x <= 1023) {
                $("#Left .item_wrap").removeClass("item_wrap_list").addClass("item_wrap_grid");
                $('.item_price_more').css('display', 'none');
                $('.item_price_less').css('display', 'block');
                $('#divhotsaleDeals').hide();
            }

            if (x > 1281) {
                $('.outside_AD').css('display', 'block');
                $('#Rightarea .inside_AD').css('display', 'none');
            }
            if (x <= 1280) {
                $('.outside_AD').css('display', 'none');
                $('#Rightarea .inside_AD').css('display', 'block');
            }

            //每次resize、切換棋盤/條列瀏覽 皆重新抓一次寬高
            $('.item_wrap_list .item_pic').css('height', '267');
            $('.item_wrap_list .item_pic').css('width', '480');
            $('.item_wrap_grid .item_pic').css('height', $('.item_soldout_480').height());
            $('.item_wrap_grid .item_pic').css('width', $('.item_soldout_480').width());

            //side deal
            if (x > 1000) {
                $("#btnEvent").css("right", "");
                $("#btnEvent").css("margin-left", "970px");
                $("#btnEvent").css("bottom", "15%");
                $("#btnEvent").css("width", "40px");
                $("#btnEvent").css("height", "40px");

                $("#btnBackTop").css("right", "");
                $("#btnBackTop").css("bottom", "");
                $("#btnBackTop").css("width", "");
                $("#btnBackTop").css("height", "");

                if ($("#fbfans").attr("src") == undefined) {
                    var fbsrc = $("#fbfans").attr("data-src");
                    $("#fbfans").attr("src", fbsrc).removeAttr("data-src");
                }
            } else {
                $("#btnEvent").css("right", "10px");
                $("#btnEvent").css("bottom", "38px");
                $("#btnEvent").css("width", "50px");
                $("#btnEvent").css("height", "50px");

                $("#btnEvent").css("margin-left", "");
                $("#btnBackTop").css("right", "10px");
                $("#btnBackTop").css("bottom", "25px");
                $("#btnBackTop").css("width", "50px");
                $("#btnBackTop").css("height", "50px");
            }
            //deal info page
            /*
            if (!$.Guid.IsEmpty(Ppon.module.GetBusinessHourGuid())) {
                if (x <= Ppon.module.PhoneMaxWidth) {
                    if ($.trim($("#storeInfoZone").html()) == "") {
                        $(".StoreInfo").hide();
                    }

                    $(function () {
                        var icons = {
                            header: "ui-icon-circle-arrow-e",
                            activeHeader: "ui-icon-circle-arrow-s"
                        };
                        if ($('#EntryContent').is(':ui-accordion') == false) {
                            $("#EntryContent").accordion({
                                header: '.EntryTitle',
                                collapsible: true,
                                autoHeight: false,
                                active: 0,
                                change: function (event, ui) {
                                    if ($(ui.newContent).offset() != null) {
                                        $('html, body').animate({
                                            scrollTop: $(ui.newContent).offset().top - 65
                                        }, 500);
                                    }
                                }
                            });
                        }
                    });
                }
                else {
                    $(".shop").show();
                    if (typeof $("#EntryContent").data("ui-accordion") != "undefined") {
                        $("#EntryContent").accordion("destroy");
                    }
                }
            }
            */
            if (partialurl != null && (partialurl == partialDefaulturl || partialurl == partialDefaulturl + 'default.aspx')) {
                isSelectedMobileChannel = false;
            }

            if (window.addEventListener && !isSelectedMobileChannel && $(window).width() <= Ppon.module.PhoneMaxWidth && $.Guid.IsEmpty(Ppon.module.GetBusinessHourGuid())) {
                mHotSaleArea.show();
                if ($("#slideBox").find('.hd > ul li').length <= 0) {
                    if ($.browser.msie && 9 > $.browser.version) {
                        $("#slideBox").slide({ titCell: ".hd ul", mainCell: ".bd ul", effect: "leftLoop", autoPage: true, autoPlay: false });
                    } else {
                        TouchSlide({ slideCell: "#slideBox", titCell: ".hd ul", mainCell: ".bd ul", effect: "leftLoop", autoPage: true, autoPlay: false });
                    }
                }

                $('#slideBox li').each(function () {
                    var obj = $(this).find('#Hotdeal1');
                    if ($(obj).find(".open_new_window").length > 0) {
                        var link = $(obj).find(".open_new_window")[0].href;
                        $(obj).click(function () {
                            location.href = link;
                        });
                    }
                });

                $("#maincontent").hide();
                MobileGEOLocationCheck(); //手機定位檢查
            } else {
                $("#maincontent").show();
            }

            if ($(window).width() > Ppon.module.PhoneMaxWidth) {
                $("#maincontent").show();
                mHotSaleArea.hide();
            }

            if ($(window).width() > 1024) {
                $('.picforsdeal, .deal_pic').mouseenter(function () {
                    $(this).find('.deal_hover').stop(true, true).fadeIn(150);
                }).mouseleave(function () {
                    $(this).find('.deal_hover').stop(true, true).fadeOut(150);
                });
            }
        }

        function showRelateDeals() {
            if (!relatedDealsLoaded) {
                relatedDealsLoaded = true;
                $('#relateDealArea').show();
                var showValue = ($("#smartbanner").css("top") == "0px") ? (250 + 86) : 250;
                var scroll_top = $(window).scrollTop();
                if (scroll_top >= showValue) {
                    var travelCategoryId = $('#HfTravelCategoryId').val();
                    var femaleCategoryId = $('#HfFemaleCategoryId').val();
                    var filterCategoryIdList = $('#HfFilterCategoryIdList').val();
                    var cityId = $('#HfCityId').val();
                    var categoryId = $('#HfDealCategoryId').val();
                    fillRelatedDeals(Ppon.module.GetBusinessHourGuid(), categoryId, cityId, travelCategoryId, femaleCategoryId, filterCategoryIdList);
                }
            }
        }

        //取回相關檔次列表資料
        function fillRelatedDeals(bid, categoryId, cityId, travelCategoryId, femaleCategoryId, filterCategoryIdList) {
            $("#loadingRelateDeals").show();
            $.dnajax("/service/ppon/GetRelatedDeals", "{bid:'" + bid + "', categoryId:" + categoryId + ", workCityId:" + cityId + ", travelCategoryId:" + travelCategoryId + ", femaleCategoryId:" + femaleCategoryId + ", filterCategoryIdList:[" + filterCategoryIdList + "]}", function (res) {
                if (res.Code != window.ApiSuccessCode) {
                    relatedDealsLoaded = false;
                    return;
                }

                bindRelatedDeals($.parseJSON(res.Data));
                $("#loadingRelateDeals").hide();
            });
        }

        //將相關檔次填入列表
        function bindRelatedDeals(jsonData) {
            var deals = $.parseJSON(jsonData);
            $("#relateDealsData").val(jsonData);
            $("#relateDeals").html("");
            var maxRecord = 20;
            if (deals.RelatedDeals.length < maxRecord) {
                maxRecord = deals.RelatedDeals.length;
            }
            $("#relateDealsIndex").val(maxRecord);
            for (var i = 0; i < maxRecord; i++) {
                $('#relateDeals').append(getRelatedDealHtml(i, maxRecord, deals.RelatedDeals[i]));
            }
            $("img.relateDealLazy" + maxRecord).lazyload({ threshold: 500, effect: "fadeIn" });
        }

        ////接著讀取相關檔次列表後20筆(目前已不啟用)
        //function loadRelatedDeals() {
        //    if ($(window).width() <= Ppon.module.PhoneMaxWidth && $("#relateDeals").html()) {
        //        if (($(window).scrollTop() + $(window).height()) >= ($('body').height() * 0.88) && isComplete) {
        //            isComplete = false;
        //            var deals = $.parseJSON($("#relateDealsData").val());
        //            var currentIndex = parseInt($("#relateDealsIndex").val());
        //
        //            if (deals.RelatedDeals.length > currentIndex) {
        //                var maxRecord = currentIndex + 20;
        //                if (maxRecord > deals.RelatedDeals.length) {
        //                    maxRecord = deals.RelatedDeals.length;
        //                }
        //                $("#relateDealsIndex").val(maxRecord);
        //                for (var i = currentIndex; i < maxRecord; i++) {
        //                    $('#relateDeals').append(getRelatedDealHtml(i, maxRecord, deals.RelatedDeals[i]));
        //                }
        //                $("img.relateDealLazy" + maxRecord).lazyload({ threshold: 500, effect: "fadeIn" });
        //            }
        //            isComplete = true;
        //        }
        //    }
        //}

        //Render相關檔次列表Html
        function getRelatedDealHtml(index, maxRecord, deal) {

            var dealTemp = $("#relateDealsTemplate").clone();

            dealTemp.attr("id", "relateDeals" + index);
            dealTemp.css('display', '');
            dealTemp.find('#dealLink').attr('href', ServerLocation + "deal/" + deal.BusinessHourGuid);
            dealTemp.find('.lineview-dts').html(deal.DealIcon);
            dealTemp.find('.lineview-counter').html(deal.SellCount);
            dealTemp.find('.relateDealImg').attr('data-original', deal.DealImageUrl);
            dealTemp.find('.relateDealImg').addClass("relateDealLazy" + maxRecord);
            dealTemp.find('.dealPromo').html(deal.DealPromoImage);
            if (deal.IsSoldOut) {
                dealTemp.find('.relateDealPic').append("<div class='lineview-buy-sold-out'><img src='../Themes/PCweb/images/soldout_bar_290.png' width='290' height='162' /></div>");
            }
            dealTemp.find('#dealTitle').html(deal.DealTitle);
            dealTemp.find('.lineview-discount').html(deal.SaleMessage);
            dealTemp.find('.lineview-price').html(deal.ItemPrice);

            return dealTemp;
        }

        function SetWidthHeigth() {
            var height = $(window).height();
            var width = $(window).width();
            $.ajax({
                url: ServerLocation + "Ppon/BrowserWindowSize.ashx",
                data: {
                    'Height': height,
                    'Width': width
                },
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            }).done(function (data) {
                if (data.isFirst && width >= Ppon.module.PhoneMaxWidth) {
                    window.location.reload();
                };
            }).fail(function (xhr) {
                //alert("Problem to retrieve browser size.");
            });
        }

        function MobileGEOLocationCheck() {
            //geolocation API start
            var latlong = getCookie('latlong');
            if (latlong != "undefined") {
                try {
                    var latlongArray = latlong.split(',');
                    var latitude = latlongArray[0];
                    var longitude = latlongArray[1];
                    moblieMenubulid(latitude, longitude);
                } catch (err) {
                    $('#menu-channel').menu({ backLabel: "返 回" });
                }
            } else {
                if (navigator.userAgent.match("Firefox")) {
                    $('#menu-channel').menu({ backLabel: "返 回" });
                } else {
                    if (navigator.geolocation) {
                        var geo = navigator.geolocation;
                        var option = {
                            enableAcuracy: false,
                            maximumAge: 0,
                            timeout: 60000 //暫定1分鐘      //600000 = 10分鐘  
                        };
                        geo.getCurrentPosition(successCallback,
                                               errorCallback,
                                               option
                                               );
                    } else {
                        //alert("此瀏覽器不支援地理定位功能!");
                        $('#menu-channel').menu({ backLabel: "返 回" });
                    }
                }
            }

            function successCallback(position) {
                setCookie('latlong', position.coords.latitude + ',' + position.coords.longitude, 7);
                moblieMenubulid(position.coords.latitude, position.coords.longitude);
            }
            function errorCallback(error) {
                var errorTypes = {
                    0: "不明原因錯誤",
                    1: "使用者拒絕提供位置資訊",
                    2: "無法取得位置資訊",
                    3: "位置查詢逾時"
                };
                //alert(errorTypes[error.code]);
                $('#menu-channel').menu({ backLabel: "返 回" });
                //alert("code=" + error.code + " " + error.message); //開發測試時用
            }
            //geolocation API  end            
            function moblieMenubulid(latitude, longitude) {
                if (latitude > 0 && longitude > 0) {
                    $.ajax({
                        type: "POST",
                        url: ServerLocation + "Ppon/Default.aspx/GetCityNameByGEOLocation",
                        data: "{latitude:'" + latitude + "',longitude:'" + longitude + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            $('#menu-channel .menu-ch-icon:eq(0)').each(function () {
                                var fatherLi = $(this).parent().parent();
                                fatherLi.find('ul').find('li').each(function () {
                                    $(this).find('a').each(function () {
                                        if ($(this).text() == msg.d) {
                                            var locationUL = $(this).parent().find('ul:eq(0)').html();
                                            fatherLi.find('ul').remove();
                                            fatherLi.find('a:eq(0)').after('<ul>' + locationUL + '</ul>');
                                        }
                                    });
                                });

                            });
                            $('#menu-channel .menu-ch-icon:eq(3)').each(function () {
                                var fatherLi = $(this).parent().parent();

                                fatherLi.find('ul').find('li').each(function () {
                                    $(this).find('a').each(function () {
                                        if ($(this).text() == msg.d) {
                                            var locationUL = $(this).parent().find('ul:eq(0)').html();
                                            fatherLi.find('ul').remove();
                                            fatherLi.find('a:eq(0)').after('<ul>' + locationUL + '</ul>');
                                        }
                                    });
                                });
                            });
                            $('#menu-channel').menu({ backLabel: "返 回" });
                        }
                    });
                } else {
                    $('#menu-channel').menu({ backLabel: "返 回" });
                }
            }
        }


        if (location.search != "") {
            var obj = {}
            var string_array = [];
            var string_array = location.search.replace('?', '').split("&");
            for (var i = 0; i < string_array.length; i++) {
                obj[string_array[i].split('=')[0]] = string_array[i].split('=')[1] || '';
            }
            if (obj.FastDeal == "1") {
                $('#ulSort').hide();
                $('#ulFilter').hide();
                $('.NaviCityW:last').hide();
                $(".city-switch").css('margin-bottom', '20px');

                $("#naviIndexLink").removeClass("navbtn_inpage");
                $("#naviIndexLink").addClass("navbtn");
                $("#navFastDealLink").removeClass("navbtn");
                $("#navFastDealLink").addClass("navbtn_inpage");
            }
        }

    },

    Todaydeals: function (dModule) {
        var IsSliding = false;
        var SlideTimer;
        var speed = 1200;
        var startX;
        var startY;
        var isMoving = false;
        var OutContainer = $(dModule).find('div .outsidecontainer');
        var ousideAd = $(dModule).find('#ousideAd');
        var InnerItem = $(dModule).find('div .BDOutside');
        var DirectBtn = $(dModule).find('.BigDeal > a');
        var BigDeal = $(dModule).find('.BigDeal');
        var InnerItemSize = InnerItem.size();
        Ppon.module.OutSideADInit(ousideAd);
        $.each($('.city0img'), function () {
            var ssrc = $(this).attr('ssrc');
            $(this).attr('src', ssrc);
        });
        $('#city0').attr('imgload', 'true');
        if ('ontouchstart' in document.documentElement) {
            BigDeal[0].addEventListener('touchstart', onTouchStart, false);
        }
        InnerItem.show();
        InnerItem.css('left', '0px;');
        if (InnerItemSize < 3) {
            DirectBtn.remove();
            InnerItem.each(function (idx) {
                $(this).css('left', idx * 480);
            });
        } else {
            var addItem = InnerItem.clone();
            addItem.each(function (idx) {
                InnerItem.push($(this));
                OutContainer.append($(this));
            });
            InnerItem.each(function (idx) {
                $(this).css('left', (idx * 480) - 480);
            });
            DirectBtn.click(function () {
                if (!IsSliding) {
                    IsSliding = true;
                    if ($(this).hasClass('BDSelector_L')) {
                        gotoDirect(1);
                    } else {
                        gotoDirect(0);
                    }
                }
            });
            startSliding();

            OutContainer.mouseover(function () {
                stopSliding();
            }).mouseout(function () {
                startSliding();
            });
            DirectBtn.mouseover(function () {
                stopSliding();
            }).mouseout(function () {
                startSliding();
            });
        }
        $("img.Lazy").lazyload({ placeholder: ServerLocation + "Themes/PCweb/images/ppon-M1_pic.jpg", threshold: 300, effect: "fadeIn" });
        function cancelTouch() {
            BigDeal[0].removeEventListener('touchmove', onTouchMove);
            startX = null;
            isMoving = false;
        }
        function onTouchMove(e) {
            e.preventDefault();
            if (isMoving) {
                var x = e.touches[0].pageX;
                var y = e.touches[0].pageY;
                var dx = startX - x;
                var dy = startY - y;
                if (Math.abs(dy) > 80) {
                    cancelTouch();
                    if (dy > 0) {
                        $('html,body').animate({ scrollTop: 1000 + dy * 2 }, 500);
                    }
                    else {
                        $('html,body').animate({ scrollTop: 0 }, 500);
                    }
                }
                else if (Math.abs(dx) >= 20) {
                    if (InnerItem.size() > 2) {
                        cancelTouch();
                        if (dx > 0) {
                            if (IsSliding) return false;
                            stopSliding();
                            speed = 500;
                            gotoDirect(0);
                        }
                        else {
                            if (IsSliding) return false;
                            stopSliding();
                            speed = 500;
                            gotoDirect(1);
                        }
                        speed = 1200;
                        startSliding();
                    }
                }
            }
        }
        function onTouchStart(e) {
            if (e.touches.length == 1) {
                startX = e.touches[0].pageX;
                startY = e.touches[0].pageY;
                isMoving = true;
                BigDeal[0].addEventListener('touchmove', onTouchMove, false);
            }
        }
        function gotoDirect(type) {
            var childindex = 0;
            var leftmove;
            if (type == 1) {
                childindex = -1;
                leftmove = '+=480';
            }
            else {
                childindex = 0;
                leftmove = '-=480';
            }

            var infect = OutContainer.children().eq(childindex).detach();
            if (type == 1) {
                infect.prependTo(OutContainer);
            }
            else {
                infect.appendTo(OutContainer);
            }
            OutContainer.children().each(function (idx) {
                $(this).clearQueue().animate({ 'left': leftmove }, speed, function () {
                    if (idx + 1 == InnerItem.size()) {
                        OutContainer.children().each(function (index) {
                            $(this).css('left', (index * 480) - 480);
                        });
                    }
                    IsSliding = false;
                });
            });
        }
        function startSliding() {
            SlideTimer = setInterval(function () {
                gotoDirect(0);
            }, 4000);
        }
        function stopSliding() {
            clearInterval(SlideTimer);
        }
    },
    MasterPage: function (dModule) {
        Ppon.module.MasterPage.EMSInfo = $(dModule).find('#EMSInfo');
        var HfMasterPageType = $(dModule).find('#HfMasterPageType');
        var middle = $(dModule).find('#middle');
        var p3footer = $(dModule).find('.p3footer');
        var LoginBox = $(dModule).find('.LoginBox');
        var footer_content = $(dModule).find('.footer_content');
        var head_title = $(dModule).find('head title');
        var NextButton = $(dModule).find('.NextButton');
        var ConfirmButton = $(dModule).find('.ConfirmButton');
        var EventMailSub = $(dModule).find('.EventMailSub');
        var Area = $(dModule).find('.Area');
        var ddl_EventArea = $$('ddl_EventArea');
        var SelectedCity = $(dModule).find('#SelectedCity');

        var HfEdmBg1 = $(dModule).find('#HfEdmBg1');
        var HfEdmBg2 = $(dModule).find('#HfEdmBg2');
        var HfEventId = $('[id*=HfEventId]');
        var center = $(dModule).find('.center');
        var target = $(dModule).find('#target');
        var target2 = $(dModule).find('#target2');
        var naviCityArea = $(dModule).find('.NaviCityArea');
        var mcMenuBtn = $(dModule).find('.mc-menu-btn');
        var mHDmenu = $(dModule).find('.mbe-menu_HDmenu');
        var mHDmenuBtn = $(dModule).find('.mc-menu-btn_HDmenu');
        var mMenu = $(dModule).find('#mMenu');
        var targetHDmenu = $(dModule).find('#target_HDmenu');
        var sildeWrap = $(dModule).find('.sildeWrap');
        var mcMenubtnHD = $(dModule).find('.mc-menu-btn_HD');
        var slide_m_browser = $(dModule).find('.m-browser');
        var search_block = $(dModule).find('.search-block');
        var slide_bn_block = $(dModule).find('.slide-bn-block');
        var sliderWrapMenber = $(dModule).find('.sliderWrapMenber');
        var categoryId = $(dModule).find('#HfDealCategoryId').val();
        var newscontent = $(dModule).find('#newscontent');
        var newscontent_m = $(dModule).find('#newscontent-m');
        var Marketingarea = $(dModule).find('#Marketingarea');
        var mixedContent = $(dModule).find('#mixedContent');
        var overChannel = $('.overlay-channel');
        Ppon.module.Common(dModule);

        Ppon.module.NewsVisible(newscontent, Marketingarea);
        //Ppon.module.NewsVisible(newscontent_m, Marketingarea);
        Ppon.module.NewsInit();
        Ppon.module.MasterPageType = HfMasterPageType.val();
        Ppon.module.MasterPage.categoryId = categoryId;

        //如果是即買即用就隱藏
        if (window.location.href.indexOf('channel/100001') > -1) {
            $('#ulSort').hide();
            $('#ulFilter').hide();
            $('.NaviCityW:last').hide();
            $(".city-switch").css('margin-bottom', '20px');

            $("#naviIndexLink").removeClass("navbtn_inpage");
            $("#naviIndexLink").addClass("navbtn");
            $("#navFastDealLink").removeClass("navbtn");
            $("#navFastDealLink").addClass("navbtn_inpage");
        }

        if (window.location.href.indexOf('/piinlife/') > -1) {
            $('.overlay-channel').hide();
            $('.header_wrap').hide();
            $('.header').hide();
        }

        var isSelectedMobileChannel = true;
        var partialurl = window.location.href.substring(window.location.href.indexOf($('#PponLOGO').attr('href')), window.location.href.length);
        var partialDefaulturl = $('#PponLOGO').attr('href');
        if (partialurl != null && (partialurl == partialDefaulturl || partialurl == partialDefaulturl + 'default.aspx')) {
            isSelectedMobileChannel = false;
        }

        if (window.addEventListener && !isSelectedMobileChannel && $(window).width() <= Ppon.module.PhoneMaxWidth && $.Guid.IsEmpty(Ppon.module.GetBusinessHourGuid())) {
            $("#maincontent").hide();
        } else {
            if ($("#NaviCity").is(":hidden") || $(".NaviCityW  so-awesome").is(":hidden") || $("#ulSort").is(":hidden")) {
                $("#maincontent").hide();
            } else {
                $("#maincontent").show();
            }
        }

        if (document.getElementById("Default") == null) {
            mMenu.hide();
        }

        mcMenuBtn.toggle(function () {
            //target.css("display", "none");//隱藏選單
            mixedContent.css("display", "none");//隱藏滑動選單
            target2.css("display", "block");
            target2.stop().animate({ marginRight: '0%' }, 200);
            sliderWrapMenber.css("height", "auto");
            mHDmenuBtn.css("display", "none");
            mMenu.css("display", "none");
            center.css("display", "none");
            p3footer.css("display", "none");
            slide_m_browser.css("visibility", "hidden");
            search_block.css("visibility", "hidden");
            slide_bn_block.css("visibility", "hidden");

            naviCityArea.hide();
            if ($.Guid.IsEmpty(Ppon.module.GetBusinessHourGuid()) && $(window).width() <= Ppon.module.PhoneMaxWidth) {
                mMenu.hide();
            }
            $("#footerfix").css("min-height", ($(window).height() - 50) + "px");
            $('body').click(function (evt) {
                if ($(evt.target).parents(".NaviCitySequence").length == 0 &&
                    !$(evt.target).parents('a').hasClass("mc-menu-btn")) {
                    $('body').unbind("click");
                    $("#footerfix").removeAttr("style");
                    showMainContent();
                }
            });
        }, function () {
            $('body').unbind("click");
            $("#footerfix").removeAttr("style");
            showMainContent();
        });

        mHDmenuBtn.toggle(function () {
            targetHDmenu.stop().animate({ marginLeft: '0%' }, 400);
            sildeWrap.css("height", "auto");
            mcMenubtnHD.css("display", "none");
            mMenu.css("display", "none");
            center.css("display", "none");
            p3footer.css("display", "none");
        }, function () {
            targetHDmenu.stop().animate({ marginLeft: '-100%' }, 500);
            sildeWrap.css("height", "0");
            mcMenubtnHD.css("display", "block");
            mMenu.css("display", "none");
            center.css("display", "block");
            p3footer.css("display", "block");
        });

        $(".overlay-scale").on('click', function () {
            $('.overlay-close').get(0).click()
        });

        function showMainContent() {
            //target.css("display", "block");
            mixedContent.css("display", "none");//隱藏滑動選單
            target2.stop().animate({ marginRight: '-100%' }, 500);
            sliderWrapMenber.css("height", "0");
            mHDmenuBtn.css("display", "block");
            mMenu.css("display", "none");
            naviCityArea.show();
            center.css("display", "block");
            p3footer.css("display", "block");
            slide_m_browser.css("visibility", "visible");
            search_block.css("visibility", "visible");
            slide_bn_block.css("visibility", "visible");

            if ($.Guid.IsEmpty(Ppon.module.GetBusinessHourGuid()) && $(window).width() <= Ppon.module.PhoneMaxWidth) {
                if (document.getElementById("Default") != null && Ppon.module.MasterPage.categoryId != PponJSCityGroup.Family) {
                    if ($('#mHotSaleArea').css('display') == 'none') {
                        mMenu.show();
                    }
                }
            }
        }

        if (Ppon.module.MasterPageType == 'taishin') {
            middle.addClass('fortaishin');
            p3footer.addClass('fortaishin');
            LoginBox.after('<div class="provided"></div>');
            footer_content.html('<div class="footerlogo"></div><div class="clearfix"></div>');
            p3footer.before('<div class="tsprovided"></div>');
            head_title.html('台新網銀');
        }

        $('#btnBackTop').click(function () {
            gototop();
        })

        Ppon.module.MasterPage.showSubscription = function () {
            x = $(window).width();
            if (x > 700) {
                EventMailSub.css('background-image', 'url(' + HfEdmBg1.val() + ')');
                Ppon.module.MasterPage.BlockDiv = "EventMailSub";
                PopBlockUI(EventMailSub);
            }
        };

        function resizefun() {

            Ppon.module.NewsVisible(newscontent, Marketingarea);
            x = $(window).width();
            //寬度小於 700 自動解除跳窗edm
            if (x < 701) {
                if (Ppon.module.MasterPage.BlockDiv == "EventMailSub") {
                    Ppon.module.MasterPage.BlockDiv = "";
                    $.unblockUI();
                }
            }
            
            if ($(window).width() < 768) {
                $('#newscontent').parent('[id*=TOPBanner]').parent('.slide-bn-block').hide();
            }

            //if (x > 1000) {
            //    target2.css('display', 'none');
            //    if (window.document.location.pathname.toLowerCase().indexOf('ppon/contactus.aspx') > 0) {
            //    } else {
            //        center.css("display", "block");
            //    }

            //    //資安標章
            //    if ($("#dppMarkUrl").val()) {
            //        $("#dppMark").html("<img border='0' style='width: 98px;' src='" + $("#dppMarkUrl").val() + "' title='dpmark資料隱私保護標章' id='dpmarkseal1'>");
            //        $("#dppMarkUrl").val("");
            //    }
            //    $('.header_wrap').hide();
            //} else {
            //    $('.header_wrap').show();
            //}
            
            if (partialurl != null && (partialurl == partialDefaulturl || partialurl == partialDefaulturl + 'default.aspx')) {
                isSelectedMobileChannel = false;
            }

            if (window.addEventListener && !isSelectedMobileChannel && $(window).width() <= Ppon.module.PhoneMaxWidth && $.Guid.IsEmpty(Ppon.module.GetBusinessHourGuid())) {
                $("#maincontent").hide();
            } else {
                if ($(window).width() >= 768) {
                    $("#maincontent").show();
                }
            }
            if (window.location.href.indexOf('/piinlife/') > -1) {
                $('.overlay-channel').hide();
                $('.header_wrap').hide();
                $('#Headermiddle').show();
            } else {
                $('#Headermiddle').hide();
            }

            if (document.URL.toLowerCase().indexOf('/event/exhibition') > 0) {
                $('.slide-bn-block.m-browser').css('display', 'none');
            }
        }

        resizefun();
        $(window).resize(delayFunction(function () { resizefun(); }, { delay: 300 }));

        //一般訂閱
        $('#btnEdmSubmit').click(function () {
            var email = $('#txt_Mail').val();
            var categoryId = $('#ddl_EdmCity').val();
            Ppon.module.SubscriptionEDM(email, categoryId, "PponSelect", true);
        });

        //精選好康訂閱
        $('#btnEdmPponSelect').click(function () {
            var email = $('#txt_Mail').val();
            var categoryId = $('#pponSelectCategoryId').val();
            Ppon.module.SubscriptionEDM(email, categoryId, "PponSelect", true);
        });

        //APP Download 簡訊
        $(".PhoneBTN").parent().find("input[id=textfield]").numeric();
        $(".PhoneBTN").parent().find("input[id=textfield]").attr('maxlength', '10');
        $(".PhoneBTN").bind("click", function () {
            var phone = $.trim($(this).parent().find("input[id=textfield]").val());
            if (phone.length != 10) {
                alert("您輸入的手機格式錯誤。");
            } else {
                pattern = /09[0-9]{2}[0-9]{6}/;
                if (pattern.test(phone)) {
                    Ppon.module.SendAppDownloadSMS(phone);
                } else {
                    alert("您輸入的手機格式錯誤。");
                }
            }
            return false;
        });

        //策展
        if ($.Guid.IsEmpty(Ppon.module.GetBusinessHourGuid()) && $('.Curatorial-main a').length > 0) {
            $('#divCuration').show();
            $('#divCuration').find('li a').each(function () {
                var cityId = $('#HfCityId').val();
                var url = $(this).attr('href');
                $(this).attr('href', url + '&cid=' + cityId);
            });
        } else {
            $('#divCuration').hide();
        }
    },

    SubscriptionEDM: function subscriptionEdm(email, categoryId, showPponSelectDiv, showResultMsg) {
        var mailReg = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;
        var postData = {
            email: email,
            categoryId: categoryId
        };
        if (mailReg.test(email)) {
            $.ajax({
                type: "POST",
                url: ServerLocation + "service/ppon/AddEdm",
                data: JSON.stringify(postData),
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    if (res.ShowPponSelect) {
                        $("#" + showPponSelectDiv).show();
                    } else {
                        $("#" + showPponSelectDiv).hide();
                        if (showResultMsg) {
                            alert(res.Message);
                        }
                    }
                }
            });
        } else {
            alert('您的電子信箱格式不符合');
        }
    }

};
//確定dom obj存在再去載入相關對應的JS,避免一部分的js壞掉而導致頁面無法動作
(function () {
    if (typeof console === "undefined") {
        console = {};
        console.log = function () { };
    }
    var doWhileExist = function (ModuleID, objFunction) {
        var dTarget = document.getElementById(ModuleID);
        console.log(ModuleID + ' Checking......');
        if (dTarget) {
            console.log(ModuleID + ' exist');
            objFunction(dTarget);
            console.log('function loaded');
        }
    };
    doWhileExist('MasterPageRender', Ppon.module.MasterPage); //載入MasterPage相關的JS
    doWhileExist('Default', Ppon.module.Default); //載入Default相關的JS
    doWhileExist('Todaydeals', Ppon.module.Todaydeals); //載入Todaydeals相關的JS
})();
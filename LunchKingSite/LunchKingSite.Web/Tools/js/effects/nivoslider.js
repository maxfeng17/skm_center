﻿(function ($) {
	var NivoSlider = function (element, options) {
		var settings = $.extend({}, $.fn.nivoSlider.defaults, options);
		//Useful variables. Play carefully.
		var vars = {
			currentSlide: 0,
			currentImage: '',
			totalSlides: 0,
			randAnim: '',
			running: false,
			paused: false,
			stop: false
		};
		//Get this slider
		var slider = $(element);
		slider.data('nivo:vars', vars);
		slider.css('position', 'relative');
		slider.addClass('nivoSlider');
		//Find our slider children
		var kids = slider.children();
		kids.each(function () {
			vars.totalSlides++;
		});
		//Set startSlide
		if (settings.startSlide > 0) {
			if (settings.startSlide >= vars.totalSlides) settings.startSlide = vars.totalSlides - 1;
			vars.currentSlide = settings.startSlide;
		}
		//Get initial image
		vars.currentImage = $(kids[vars.currentSlide]);
		//Set first background
		slider.css('background', 'url("' + vars.currentImage.attr('src') + '") no-repeat');
		slider.css('background-size', 'contain');
		slider.css('filter', 'progid:DXImageTransform.Microsoft.AlphaImageLoader( src="' + vars.currentImage.attr('src') + '", sizingMethod="scale")');
		slider.css('-ms-filter', 'progid:DXImageTransform.Microsoft.AlphaImageLoader( src="' + vars.currentImage.attr('src') + '", sizingMethod="scale")');
		//Add initial slices
		for (var i = 0; i < settings.slices; i++) {
			var sliceWidth = Math.round(slider.width() / settings.slices);
			if (i == settings.slices - 1) {
				slider.append(
					$('<div class="nivo-slice"></div>').css({ left: (sliceWidth * i) + 'px', width: '100%' })
				);
			} else {
				slider.append(
					$('<div class="nivo-slice"></div>').css({ left: (sliceWidth * i) + 'px', width: '100%' })
				);
			}
		}
		//In the words of Super Mario "let's a go!"
		var timer = 0;
		if (!settings.manualAdvance && kids.length > 1) {
			timer = setInterval(function () { nivoRun(slider, kids, settings, false); }, settings.pauseTime);
		}
		//Add Direction nav
		if (settings.directionNav) {
			if (kids.length > 1) {
				slider.append('<div class="nivo-directionNav"><a class="nivo-prevNav">Prev</a><a class="nivo-nextNav">Next</a></div>');
				//Hide Direction nav
				if (settings.directionNavHide) {
					$('.nivo-directionNav', slider).hide();
					slider.hover(function () {
						$('.nivo-directionNav', slider).show();
					}, function () {
						$('.nivo-directionNav', slider).hide();
					});
				}
				$('a.nivo-prevNav', slider).live('click', function () {
					if (vars.running) return false;
					clearInterval(timer);
					timer = '';
					vars.currentSlide -= 2;
					nivoRun(slider, kids, settings, 'prev');
				});
				$('a.nivo-nextNav', slider).live('click', function () {
					if (vars.running) return false;
					clearInterval(timer);
					timer = '';
					nivoRun(slider, kids, settings, 'next');
				});
			}
		}


		//hand gestures to slide
		//new parameters for hand gestures
//        var mousedown = false;
//        var mouseX = 0;
//        var xdiff = 0;

//        slider.mousedown(function (event) {
//            if (!this.mousedown) {
//                this.mousedown = true;
//                this.mouseX = event.pageX;
//            }
//            return false;
//        });

//        slider.mousemove(function (event) {
//            if (this.mousedown) {
//                xdiff = event.pageX - this.mouseX;
//                //_this.css('left', -imagesCurrent * containerWidth + xdiff);
//            }

//            return false;
//        });

//        slider.mouseup(function (event) {
//            this.mousedown = false;
//            if (!xdiff) return false;

//            //var fullWidth = parseInt(settings.width);
//            //fix the width to 450px for ppon default.aspx
//            var fullWidth = 450;
//            var halfWidth = fullWidth / 2;
//            //0.25 is the value of tolerance, can be  adjusted  
//            if (-xdiff > halfWidth - fullWidth * 0.25) {
//                alert('1');
//                if (vars.running) return false;
//                clearInterval(timer);
//                timer = '';
//                settings.effect = "slideInLeft";
//                settings.slices = 10;
//                nivoRun(slider, kids, settings, 'next');
//            } else if (xdiff > halfWidth - fullWidth * 0.25) {
//                alert('2');
//                if (vars.running) return false;
//                clearInterval(timer);
//                timer = '';
//                alert('2-2');
//                vars.currentSlide -= 2;
//                settings.effect = "slideInRight";
//                settings.slices = 10;
//                nivoRun(slider, kids, settings, 'prev');
//            } else {
//                //_this.animate({ left: -imagesCurrent * containerWidth }, settings.delay);
//            }

//            xdiff = 0;

//            return false;
//        });


		var startX;
		var startY;
		var isMoving = false;
		function cancelTouch() {
			slider[0].removeEventListener('touchmove', onTouchMove);
			startX = null;
			isMoving = false;
		}
		function onTouchMove(e) {
			// if (config.preventDefaultEvents) {
			e.preventDefault();
			//}
			if (isMoving) {
				var x = e.touches[0].pageX;
				var y = e.touches[0].pageY;
				var dx = startX - x;
				var dy = startY - y;
				if (Math.abs(dx) >= 20) {
					cancelTouch();
					if (dx > 0) {
						if (vars.running) return false;
						clearInterval(timer);
						timer = '';
						settings.effect = "slideInLeft";
						settings.slices = 10;
						nivoRun(slider, kids, settings, 'next');
					}
					else {
						if (vars.running) return false;
						clearInterval(timer);
						timer = '';
						vars.currentSlide -= 2;
						settings.effect = "slideInRight";
						settings.slices = 10;
						nivoRun(slider, kids, settings, 'prev');

					}
				}

			}
		}

		function onTouchStart(e) {
			if (e.touches.length == 1) {
				startX = e.touches[0].pageX;
				startY = e.touches[0].pageY;
				isMoving = true;
				slider[0].addEventListener('touchmove', onTouchMove, false);
			}
		}
		if ('ontouchstart' in document.documentElement) {
			slider[0].addEventListener('touchstart', onTouchStart, false);
		}

		//Add Control nav
		if (settings.controlNav) {
			if (kids.length > 1) {
				var nivoControl = $('<div class="nivo-controlNav"></div>');
				slider.append(nivoControl);
				for (var i = 0; i < kids.length; i++) {
					nivoControl.append('<a class="nivo-control" rel="' + i + '">' + (i + 1) + '</a>');
				}
				//Set initial active link
				$('.nivo-controlNav a:eq(' + vars.currentSlide + ')', slider).addClass('active');
				$('.nivo-controlNav a', slider).live('click', function () {
					if (vars.running) return false;
					if ($(this).hasClass('active')) return false;
					clearInterval(timer);
					timer = '';
					slider.css('background', 'url("' + vars.currentImage.attr('src') + '") no-repeat');
					slider.css('background-size', 'contain');
					slider.css('filter', 'progid:DXImageTransform.Microsoft.AlphaImageLoader( src="' + vars.currentImage.attr('src') + '", sizingMethod="scale")');
					slider.css('-ms-filter', 'progid:DXImageTransform.Microsoft.AlphaImageLoader( src="' + vars.currentImage.attr('src') + '", sizingMethod="scale")');
					vars.currentSlide = $(this).attr('rel') - 1;
					nivoRun(slider, kids, settings, 'control');
				});
			}
		}
		//For pauseOnHover setting
		if (settings.pauseOnHover) {
			slider.hover(function () {
				vars.paused = true;
				clearInterval(timer);
				timer = '';
			}, function () {
				vars.paused = false;
				//Restart the timer
				if (timer == '' && !settings.manualAdvance) {
					timer = setInterval(function () { nivoRun(slider, kids, settings, false); }, settings.pauseTime);
				}
			});
		}
		//Event when Animation finishes
		slider.bind('nivo:animFinished', function () {
			vars.running = false;
			settings.effect = "fade";
			settings.slices = 1;
			//Hide child links
			$(kids).each(function () {
				if ($(this).is('a')) {
					$(this).css('display', 'none');
				}
			});
			//Show current link
			if ($(kids[vars.currentSlide]).is('a')) {
				$(kids[vars.currentSlide]).css('display', 'block');
			}
			//Restart the timer
			if (timer == '' && !vars.paused && !settings.manualAdvance) {
				timer = setInterval(function () { nivoRun(slider, kids, settings, false); }, settings.pauseTime);
			}
		});

		// Private run method
		var nivoRun = function (slider, kids, settings, nudge) {
			//Get our vars
			var vars = slider.data('nivo:vars');
			// Stop
			if ((!vars || vars.stop) && !nudge) return false;
			//Set current background before change
			if (!nudge) {
				slider.css('background', 'url("' + vars.currentImage.attr('src') + '") no-repeat');
				slider.css('background-size', 'contain');
				slider.css('filter', 'progid:DXImageTransform.Microsoft.AlphaImageLoader( src="' + vars.currentImage.attr('src') + '", sizingMethod="scale")');
				slider.css('-ms-filter', 'progid:DXImageTransform.Microsoft.AlphaImageLoader( src="' + vars.currentImage.attr('src') + '", sizingMethod="scale")');
			} else {
				if (nudge == 'prev') {
					slider.css('background', 'url("' + vars.currentImage.attr('src') + '") no-repeat');
					slider.css('background-size', 'contain');
					slider.css('filter', 'progid:DXImageTransform.Microsoft.AlphaImageLoader( src="' + vars.currentImage.attr('src') + '", sizingMethod="scale")');
					slider.css('-ms-filter', 'progid:DXImageTransform.Microsoft.AlphaImageLoader( src="' + vars.currentImage.attr('src') + '", sizingMethod="scale")');
				}
				if (nudge == 'next') {
					slider.css('background', 'url("' + vars.currentImage.attr('src') + '") no-repeat');
					slider.css('background-size', 'contain');
					slider.css('filter', 'progid:DXImageTransform.Microsoft.AlphaImageLoader( src="' + vars.currentImage.attr('src') + '", sizingMethod="scale")');
					slider.css('-ms-filter', 'progid:DXImageTransform.Microsoft.AlphaImageLoader( src="' + vars.currentImage.attr('src') + '", sizingMethod="scale")');
				}
			}
			vars.currentSlide++;
			//Trigger the slideshowEnd callback
			if (vars.currentSlide == vars.totalSlides) {
				vars.currentSlide = 0;
			}
			if (vars.currentSlide < 0) vars.currentSlide = (vars.totalSlides - 1);
			//Set vars.currentImage
			if ($(kids[vars.currentSlide]).is('img')) {
				vars.currentImage = $(kids[vars.currentSlide]);
			} else {
				vars.currentImage = $(kids[vars.currentSlide]).find('img:first');
			}
			//Set acitve links
			if (settings.controlNav) {
				$('.nivo-controlNav a', slider).removeClass('active');
				$('.nivo-controlNav a:eq(' + vars.currentSlide + ')', slider).addClass('active');
			}
			//Set new slice backgrounds
			var i = 0;
			$('.nivo-slice', slider).each(function () {
				var sliceWidth = Math.round(slider.width() / settings.slices);
				$(this).css({
					'height': '0px',
					'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader( src="' + vars.currentImage.attr('src') + '", sizingMethod="scale")',
					'-ms-filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader( src="' + vars.currentImage.attr('src') + '", sizingMethod="scale")',
					'background': 'url("' + vars.currentImage.attr('src') + '") no-repeat -' + ((sliceWidth + (i * sliceWidth)) - sliceWidth) + 'px 0%',
					'background-size': 'contain',
					'opacity': '0'
				});
				i++;
			});
			//set effects of the random
			if (settings.effect == 'random') {
				var anims = new Array("fade");
				vars.randAnim = anims[Math.floor(Math.random() * (anims.length + 1))];
				if (vars.randAnim == undefined) vars.randAnim = 'fade';
			}
			//Run random effect from specified set (eg: effect:'fold,fade')
			if (settings.effect.indexOf(',') != -1) {
				var anims = settings.effect.split(',');
				vars.randAnim = $.trim(anims[Math.floor(Math.random() * anims.length)]);
			}
			//Run effects
			vars.running = true;
			if (settings.effect == 'sliceDown' || settings.effect == 'sliceDownRight' || vars.randAnim == 'sliceDownRight' ||
				settings.effect == 'sliceDownLeft' || vars.randAnim == 'sliceDownLeft') {
				var timeBuff = 0;
				var i = 0;
				var slices = $('.nivo-slice', slider);
				if (settings.effect == 'sliceDownLeft' || vars.randAnim == 'sliceDownLeft') slices = $('.nivo-slice', slider)._reverse();
				slices.each(function () {
					var slice = $(this);
					slice.css('top', '0px');
					if (i == settings.slices - 1) {
						setTimeout(function () {
							slice.animate({ height: '100%', opacity: '1.0' }, settings.animSpeed, '', function () { slider.trigger('nivo:animFinished'); });
						}, (100 + timeBuff));
					} else {
						setTimeout(function () {
							slice.animate({ height: '100%', opacity: '1.0' }, settings.animSpeed);
						}, (100 + timeBuff));
					}
					timeBuff += 50;
					i++;
				});
			}
			else if (settings.effect == 'sliceUp' || settings.effect == 'sliceUpRight' || vars.randAnim == 'sliceUpRight' ||
					settings.effect == 'sliceUpLeft' || vars.randAnim == 'sliceUpLeft') {
				var timeBuff = 0;
				var i = 0;
				var slices = $('.nivo-slice', slider);
				if (settings.effect == 'sliceUpLeft' || vars.randAnim == 'sliceUpLeft') slices = $('.nivo-slice', slider)._reverse();
				slices.each(function () {
					var slice = $(this);
					slice.css('bottom', '0px');
					if (i == settings.slices - 1) {
						setTimeout(function () {
							slice.animate({ height: '100%', opacity: '1.0' }, settings.animSpeed, '', function () { slider.trigger('nivo:animFinished'); });
						}, (100 + timeBuff));
					} else {
						setTimeout(function () {
							slice.animate({ height: '100%', opacity: '1.0' }, settings.animSpeed);
						}, (100 + timeBuff));
					}
					timeBuff += 50;
					i++;
				});
			}
			else if (settings.effect == 'sliceUpDown' || settings.effect == 'sliceUpDownRight' || vars.randAnim == 'sliceUpDown' ||
					settings.effect == 'sliceUpDownLeft' || vars.randAnim == 'sliceUpDownLeft') {
				var timeBuff = 0;
				var i = 0;
				var v = 0;
				var slices = $('.nivo-slice', slider);
				if (settings.effect == 'sliceUpDownLeft' || vars.randAnim == 'sliceUpDownLeft') slices = $('.nivo-slice', slider)._reverse();
				slices.each(function () {
					var slice = $(this);
					if (i == 0) {
						slice.css('top', '0px');
						i++;
					} else {
						slice.css('bottom', '0px');
						i = 0;
					}

					if (v == settings.slices - 1) {
						setTimeout(function () {
							slice.animate({ height: '100%', opacity: '1.0' }, settings.animSpeed, '', function () { slider.trigger('nivo:animFinished'); });
						}, (100 + timeBuff));
					} else {
						setTimeout(function () {
							slice.animate({ height: '100%', opacity: '1.0' }, settings.animSpeed);
						}, (100 + timeBuff));
					}
					timeBuff += 50;
					v++;
				});
			}
			else if (settings.effect == 'fold' || vars.randAnim == 'fold') {
				var timeBuff = 0;
				var i = 0;
				$('.nivo-slice', slider).each(function () {
					var slice = $(this);
					var origWidth = slice.width();
					slice.css({ top: '0px', height: '100%', width: '0px' });
					if (i == settings.slices - 1) {
						setTimeout(function () {
							slice.animate({ width: origWidth, opacity: '1.0' }, settings.animSpeed, '', function () { slider.trigger('nivo:animFinished'); });
						}, (100 + timeBuff));
					} else {
						setTimeout(function () {
							slice.animate({ width: origWidth, opacity: '1.0' }, settings.animSpeed);
						}, (100 + timeBuff));
					}
					timeBuff += 50;
					i++;
				});
			}
			else if (settings.effect == 'fade' || vars.randAnim == 'fade') {
				var i = 0;
				$('.nivo-slice', slider).each(function () {
					$(this).css('height', '100%');
					if (i == settings.slices - 1) {
						$(this).animate({ opacity: '1.0' }, (settings.animSpeed * 2), '', function () { slider.trigger('nivo:animFinished'); });
					} else {
						$(this).animate({ opacity: '1.0' }, (settings.animSpeed * 2));
					}
					i++;
				});
			}
			else if (settings.effect == 'slideInRight') {
				var firstSlice = $('.nivo-slice:first', slider);
				firstSlice.css({
					'height': '100%',
					'width': '0px',
					'opacity': '1',
					'-webkit-border-top-right-radius': settings.borderRadius + 'px',
					'-webkit-border-bottom-right-radius': settings.borderRadius + 'px',
					'-moz-border-radius-topright': settings.borderRadius + 'px',
					'-moz-border-radius-bottomright': settings.borderRadius + 'px',
					'border-top-right-radius': settings.borderRadius + 'px',
					'border-bottom-right-radius': settings.borderRadius + 'px'
				});

				firstSlice.animate({ width: slider.width() + 'px' }, (settings.animSpeed * 2), '', function () { slider.trigger('nivo:animFinished'); });
			} else if (settings.effect == 'slideInLeft') {
				var firstSlice = $('.nivo-slice:first', slider);
				firstSlice.css({
					'height': '100%',
					'width': '0px',
					'opacity': '1',
					'left': '',
					'right': '0px',
					'-webkit-border-top-right-radius': settings.borderRadius + 'px',
					'-webkit-border-bottom-right-radius': settings.borderRadius + 'px',
					'-moz-border-radius-topright': settings.borderRadius + 'px',
					'-moz-border-radius-bottomright': settings.borderRadius + 'px',
					'border-top-right-radius': settings.borderRadius + 'px',
					'border-bottom-right-radius': settings.borderRadius + 'px'
				});

				firstSlice.animate({ width: slider.width() + 'px' }, (settings.animSpeed * 2), '', function () {
					// Reset positioning
					firstSlice.css({
						'left': '0px',
						'right': '',
						'-webkit-border-top-right-radius': settings.borderRadius + 'px',
						'-webkit-border-bottom-right-radius': settings.borderRadius + 'px',
						'-moz-border-radius-topright': settings.borderRadius + 'px',
						'-moz-border-radius-bottomright': settings.borderRadius + 'px',
						'border-top-right-radius': settings.borderRadius + 'px',
						'border-bottom-right-radius': settings.borderRadius + 'px'
					});
					slider.trigger('nivo:animFinished');
				});
			}
		}
		// Start / Stop
		this.stop = function () {
			if (!$(element).data('nivo:vars').stop) {
				$(element).data('nivo:vars').stop = true;
				trace('Stop Slider');
			}
		}
		this.start = function () {
			if ($(element).data('nivo:vars').stop) {
				$(element).data('nivo:vars').stop = false;
				trace('Start Slider');
			}
		}
	};

	$.fn.nivoSlider = function (options) {
		return this.each(function () {
			var element = $(this);
			// Return early if this element already has a plugin instance
			if (element.data('nivoslider')) return;
			// Pass options to plugin constructor
			var nivoslider = new NivoSlider(this, options);
			// Store plugin object in this element's data
			element.data('nivoslider', nivoslider);
		});
	};
	//Default settings
	$.fn.nivoSlider.defaults = {
		effect: 'fade',
		slices: 1,
		animSpeed: 500,
		pauseTime: 4000,
		startSlide: 0,
		directionNav: true, //direction visible
		directionNavHide: true, //direction auto-hide
		controlNav: true, //below controller visible
		pauseOnHover: true,
		manualAdvance: false,
		borderRadius: 0
	};
	$.fn._reverse = [].reverse;
})(jQuery);




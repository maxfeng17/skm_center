﻿//後臺檔次設定頁，設定銷售分析分類
DealTypeOperator = function DealTypeOperator(jsonStr, elemContainer, elemVal, elemSettings) {
    this.levelLimit = 4;
    if (jsonStr == '') {
        return;
    }
    this.allNodes = JSON.parse(jsonStr);
    this.elemVal = elemVal;
    this.elemContainer = elemContainer;
    this.selectElems = $('');
    this.elemSettings = elemSettings || [];
    if (this.elemSettings.length < this.levelLimit) {
        for (var i = this.elemSettings.length; i < this.levelLimit; i++) {
            this.elemSettings.push({ id: null });
        }
    }
    return this;
};

DealTypeOperator.prototype = {
    init: function () {
        if (this.allNodes == null || this.allNodes.length == 0) {
            return this;
        }
        this.createElems();
        this.registerEvent();
        this.render();
        return this;
    },
    createElems: function () {
        for (var i in this.elemSettings) {
            if (this.elemSettings[i].id != null && $('#' + this.elemSettings[i].id).length > 0) {
                var selDealType = $('#' + this.elemSettings[i].id);
            } else {
                var selDealType = $('<select></select>').appendTo(this.elemContainer).css('margin-right', '12px');
            }
            this.selectElems = this.selectElems.add(selDealType);
            $(this.selectElems).hide();
        }        
    },
    registerEvent: function () {
        var self = this;

        this.selectElems.change(function () {
            var pos = $.inArray(this, self.selectElems);
            for (var i = pos + 1; i < self.selectElems.length; i++) {
                self.selectElems.eq(i).empty().hide();
            }

            if ($(this).val() != '0') {
                var theNode = self.findNode($(this).val());
                var nodes = theNode.Children;
                if (nodes.length > 0 && self.selectElems.length > pos + 1) {
                    var selDealType = self.selectElems.eq(pos + 1);

                    selDealType.show().empty();
                    selDealType.append($('<option></option>').val('0').html('=請選擇='));
                    for (var key in nodes) {
                        var node = nodes[key];
                        selDealType.append($('<option></option>').val(node.CodeId).html(node.CodeName));
                    }
                }
            }
            self.setDealTypeValue();
        });
    },
    render: function () {
        var dealTypeValue = this.getDealTypeValue();
        var selectedIds = this.getSelectedIds();
        
        //render
        var lvl = 0;

        for (lvl = 0; lvl < selectedIds.length; lvl++) {
            if (lvl == 0) {
                var nodes = this.allNodes;
            } else {
                var nodes = this.findNode(selectedIds[lvl - 1]).Children;
            }

            var selDealType = this.selectElems.eq(lvl); 
            selDealType.show();
            selDealType.append($('<option></option>').val('0').html('=請選擇='));
            
            for (var key in nodes) {
                var node = nodes[key];
                if (selectedIds[lvl] == node.CodeId) {
                    selDealType.append($('<option selected="selected"></option>').val(node.CodeId).html(node.CodeName));
                } else {
                    selDealType.append($('<option></option>').val(node.CodeId).html(node.CodeName));
                }
            }
        }
        this.selectElems.eq(lvl-1).trigger('change');
    },
    findNode: function (codeId) {
        if (codeId == null) {
            return null;
        }
        var nodes = this.allNodes;
        return this.findNodeCore(nodes, codeId);
    },
    findNodeCore: function (nodes, codeId) {
        var result = null;
        for (var key in nodes) {
            var node = nodes[key];
            if (node.CodeId == codeId) {
                result = node;
            }
            if (result == null && node.Children.length > 0) {
                result = this.findNodeCore(node.Children, codeId);
            }
        }
        return result;
    },
    getSelectedIds: function () {
        var dealTypeValue = this.getDealTypeValue();
        var selectedIds = [];
        selectedIds.push(dealTypeValue);
        var theNode = this.findNode(dealTypeValue);
        if (theNode != null) {
            var theParent = this.findNode(theNode.ParentCodeId);
            while (theParent != null) {
                selectedIds.push(theParent.CodeId);
                var theParent = this.findNode(theParent.ParentCodeId);
            };
        }
        selectedIds.reverse();
        return selectedIds;
    },
    getDealTypeValue: function () {
        return $(this.elemVal).val();
    },
    setDealTypeValue: function (val) {
        for (var i = 3; i >= 0; i --) {
            var newVal = this.selectElems.eq(i).val();            
            if (newVal == null || newVal == '0' || i == 0) {
                $(this.elemVal).val('0');
            } else {
                $(this.elemVal).val(newVal);
                break;
            }
        }
    }
}



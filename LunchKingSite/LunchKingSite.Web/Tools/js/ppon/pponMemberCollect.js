﻿/*
 * defult頁載入檔次列表
 * 相依於jquery, lazyload plugin,
 * Ppon.js (棋盤、條列式切換、倒數計時器)
 * mrel.js (倒數計時器)
 */
(function ($) {
    var MemberModule = function (elem, options) {
        this.elem = elem;
        this.$elem = $(elem);
        this.options = options;
    };

    MemberModule.prototype = {
        defaults: {
      memberCollectionDealGuids: []
        },
        initMember: function () {
            this.config = $.extend({}, this.defaults, this.options);
            this.render();
            this.initMemberCollect();
            return this;
        },
        render: function () {
           //會員檔次收藏
            this.loadMemberCollection();
        },
       initMemberCollect: function () {
            //收藏
            $(document).on('click', '.item_collect', function (evt) {
                var isCollected = $('a', this).hasClass('on');
                var bid = $(this).data('bid');
                window.setDealCollect(bid, !isCollected, this);
            });
            window.setDealCollect = function (bid, isCollecting, elem) {
                var returnUrl = encodeURI(window.location.href);
                var loginUrl = '/m/login?ReturnUrl=' + returnUrl;
                $.ajax({
                    type: "POST",
                    url: "/Ppon/Default.aspx/SetMemberCollectDeal",
                    data: JSON.stringify({ "businessHourGuid": bid, cityId: 0, isCollecting: isCollecting }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        var result = $.parseJSON(msg.d);
                        if (result.Success) {
                            window.setDealCollectUI(elem, result.IsCollecting);
                        }
                    }, error: function (response, q, t) {
                        if (t === 'userNotLogin') {
                            //put bid to storage
                            if (window.localStorage != null) {
                                window.localStorage.setItem("collectDealData",
                                    JSON.stringify({
                                        'bid': bid,
                                        'collecting': isCollecting,
                                        'expires': new Date().getTime() + (1000 * 60 * 10)
                                    }));
                            }
                            location.href = loginUrl;
                        }
                    }
                });
                return false;
            }

            window.setDealCollectUI = function (elem, isCollecting) {
                if (isCollecting) {
                    //未收藏
                    $(elem).find('a').addClass('on');
                    $(elem).find('i').removeClass('fa-heart-o').addClass('fa-heart');
                } else {
                    //收藏
                    $(elem).find('a').removeClass('on');
                    $(elem).find('i').removeClass('fa-heart').addClass('fa-heart-o');
                }
            }

            if (window.localStorage != null) {
                var ccdStr = window.localStorage.getItem("collectDealData");
                if (ccdStr != null) {
                    try {
                        var ccd = $.parseJSON(ccdStr);
                        if (ccd != null && ccd.expires > new Date().getTime()) {
                            var bid = ccd.bid;
                            var collecting = ccd.collecting;
                            $.ajax({
                                type: "POST",
                                url: "/Ppon/Default.aspx/SetMemberCollectDeal",
                                data: "{businessHourGuid:'" + bid + "', cityId:0, isCollecting:" + collecting + "}",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (msg) {
                                    var result = $.parseJSON(msg.d);
                                    if (result.Success) {
                                        var elem = $('.item_collect').filter(function () {
                                            return $(this).data("bid") === bid;
                                        });
                                        window.setDealCollectUI(elem, result.IsCollecting);
                                    }
                                }
                            });
                            window.localStorage.removeItem("collectDealData");
                        }
                    } catch (ex) {
                    }
                }
            }
        },
        loadMemberCollection: function () {
            var mcBids = this.config.memberCollectionDealGuids;
            $('.item_collect').each(function () {
                if ($(this).attr('memberCollectInit') == null) {
                    $(this).attr('memberCollectInit', '1');
                    var bid = $(this).data('bid');
                    if ($.grep(mcBids, function (mcBid) {
                        return mcBid === bid;
                    }).length > 0) {
                        //收藏
                        $(this).find('a').addClass('on');
                        $(this).find('i').removeClass('fa-heart-o').addClass('fa-heart');
                    }
                }
            });
        }
    }

    MemberModule.defaults = MemberModule.prototype.defaults;

    $.fn.pponMemberCollectList = function (options) {
        return new MemberModule(this, options).initMember();
    };
}(jQuery));

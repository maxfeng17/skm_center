﻿/*
 * defult頁載入檔次列表
 * 相依於jquery, lazyload plugin,
 * Ppon.js (棋盤、條列式切換、倒數計時器)
 * mrel.js (倒數計時器)
 */
(function ($) {
    var DealListModule = function (elem, options) {
        this.elem = elem;
        this.$elem = $(elem);
        this.options = options;
    };

    DealListModule.prototype = {
        defaults: {
            hoverLocationClass: '.item_wrap',
            hoverLocationClassSub: '.hover_place',
            lazyloadPic: '.multipleDealLazy',//img
            lazyLoaded: '.picLoaded',
            lazyloadConfig: {
                placeholder: location.origin + "/Themes/PCweb/images/ppon-M1_pic.jpg",
                threshold: 1500,
                effect: "fadeIn"
            },
            loadingMask: '#loadingMask',
            page: 2,
            lock: false,
            endToken: '#endToken',
            memberCollectionDealGuids: []
        },
        init: function () {
            this.config = $.extend({}, this.defaults, this.options);
            this.registerEvent();
            //this.getData(this);//第一次改由Server端載入，隨default.aspx一起回傳
            this.render();

            this.initMemberCollect();
            return this;
        },
        registerEvent: function () {
            this.delegateContinueGetData();
            this.delegateHoverPlace(this.config.hoverLocationClass, this.config.hoverLocationClassSub);

            var self = this;
            $('.sub-classify').find('li').click(function () {
                var page = 1;
                var url = $(this).data('url');
                if (url == null || url == '') {
                    return;
                }
                self.getData({ 'page': 1, 'url': url });
                $(this).siblings('.on').removeClass('on');
                $(this).addClass('on');
                window.history.pushState("", "", url);
            });
            $('#CategoryUl li').click(function () {
                var page = 1;
                var url = $(this).data('url');
                if (url == null || url == '') {
                    return;
                }
                self.getData({ 'page': 1, 'url': url });
                window.history.pushState("", "", url);

                $('#CategoryUl li.on').removeClass('on');
                $(this).addClass('on');

                var subClassifyIndex = $(this).index();
                $('.sub-classify').hide();
                var subClassify = $('.sub-classify').eq(subClassifyIndex);
                if (subClassify.find('li').length == 0) {
                    return;
                }
                subClassify.find('li').removeClass('on');
                subClassify.find('li').eq(0).addClass('on');
                subClassify.show();
            });
            //about sort
            window.oldSortCategory = window.SortCategory;
            window.SortCategory = function (type) {
                var page = 1;
                var url = $('.sub-classify:visible li.on').data('url');
                if (url == null) {
                    url = $('#CategoryUl li.on').data('url');
                }
                if (url == null || url == '') {
                    //use old way
                    window.oldSortCategory(type);
                    return;
                }
                
                $('#ulSort li a').removeClass('on');
                if (type <= 3) {
                    $('#ulSort li a').eq(type).addClass('on');
                } else {
                    $('#ulSort li a').eq(type-1).addClass('on');
                }
                $.ajax({
                    type: "POST",
                    url: '/ppon/default.aspx/SetSortCategory',
                    data: '{"type":' + type + '}',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        self.getData({ 'page': 1, 'url': url });
                        window.history.pushState("", "", url);
                    }
                });
            };
            window.oldSetSelectedFilter = window.SetSelectedFilter;
            window.SetSelectedFilter = function (channel, id) {
                var page = 1;
                var url = $('.sub-classify:visible li.on').data('url');
                if (url == null) {
                    url = $('#CategoryUl li.on').data('url');
                }
                if (url == null || url == '') {
                    //use old way
                    window.oldSetSelectedFilter(channel, id);
                    return;
                }

                var isChecked = $('#filter' + id).is(':checked');
                $.ajax({
                    type: "POST",
                    url: ServerLocation + 'ppon/default.aspx/SetSelectedFilter',
                    data: JSON.stringify({ "channel": channel, "id": id, "isChecked": isChecked }),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        self.getData({ 'page': 1, 'url': url });
                        window.history.pushState("", "", url);
                    }, error: function (err) {
                        alert('error');
                    }
                });
            }
        },
        render: function () {
            //頁面處理相關
            this.imageLazyLoad(this.config.lazyloadPic, this.config.lazyloadConfig, this.config.lazyLoaded);
            this.startCountDown();

            //是否繼續讀取資料相關
            this.checkIfUnlock();
            this.loadingAnimation(false);

            //會員檔次收藏
            this.loadMemberCollection();
        },
        
        getData: function (model) {
            console.log('getData start.');
            var self = this;
            $.ajax({
                method: "POST",
                url: "/ppon/partial/PponDealList",
                beforeSend:self.loadingAnimation(true),
                data: {
                    url: model.url,
                    page: model.page
                },
                dataType: "html",
                success: function (data) {
                    console.log('getData success.');
                    if (model.page == 1) {
                        self.$elem.empty();
                    }
                    self.config.page++;                        
                    self.$elem.append(data);
                    self.render();
                },
                error: function () {
                    console.log("載入檔次列表失敗!");
                }
            });
        },
        //快到底部時，加載分頁資料
        delegateContinueGetData: function () {
            var self = this;
            $(window).on('scroll', function () {
                if (!self.isLocked()) {                    
                    var approachEnd = $(window).scrollTop() + $(window).height() > $(document).height() - 800;
                    if (approachEnd) {
                        self.lock();
                        self.getData({ 'page': self.config.page, 'url': location.href });
                    }
                }
            });
        },
        //圖片lazyload (從Ppon.js移植)
        imageLazyLoad: function (lazyPic, lazyConfig, lazyLoaded) {
            $(lazyPic, this.elem).lazyload(lazyConfig).removeClass(lazyPic.replace('.', '')).addClass(lazyLoaded.replace('.', ''));

            //前6檔直接顯示
            var dealPicLength = $(lazyPic).length;
            dealPicLength = (dealPicLength > 6) ? 6 : dealPicLength;
            for (i = 0; i < dealPicLength; i++) {
                $($(lazyPic)[i]).attr("src", $($(lazyPic)[i]).attr("data-original"));
                $($(lazyPic)[i]).removeAttr("data-original");
            }
        },
        //隱藏檔次所在區域
        delegateHoverPlace: function (hoverLocationClass, hoverLocationClassSub) {
            this.$elem.on({
                mouseenter: function () {
                    $(this).find(hoverLocationClassSub).show();
                },
                mouseleave: function () {
                    $(this).find(hoverLocationClassSub).hide();
                }
            }, hoverLocationClass);
        },
        //啟動倒數計時 (從Ppon.js移植)
        startCountDown: function () {
            var outlTAll = $('div.TimerTitleField');
            if ($(window).width() > Ppon.module.PhoneMaxWidth) {
                new DealCountdown().trigger(outlTAll, true);
            } else {
                //m版不執行倒數計時
                outlTAll.find("#TimerField").hide();
                outlTAll.find(".icon-clock-o").hide();
            }
        },
        /////////////////tool/////////////////<
        isLocked: function () {
            return this.config.lock;
        },
        checkIfUnlock: function () {
            var token = $(this.config.endToken);
            if (token[0] === undefined) {
                this.unlock();
            }
        },
        lock: function () {
            this.config.lock = true;
        },
        unlock: function () {
            this.config.lock = false;
        },
        loadingAnimation: function (enable) {
            if (this.config.page > 1 && enable) {
                $(this.config.loadingMask).show();
            }
            else {
                $(this.config.loadingMask).hide();
            }
        },
        initMemberCollect: function () {
            //收藏
            $(document).on('click', '.item_collect', function (evt) {
                var isCollected = $('a', this).hasClass('on');
                var bid =  $(this).data('bid');
                window.setDealCollect(bid, !isCollected, this);
            });
            window.setDealCollect = function (bid, isCollecting, elem) {
                var returnUrl = encodeURI(window.location.href);
                var loginUrl = '/m/login?ReturnUrl=' + returnUrl;
                $.ajax({
                    type: "POST",
                    url:  "/Ppon/Default.aspx/SetMemberCollectDeal",
                    data: JSON.stringify({"businessHourGuid": bid, cityId:0, isCollecting: isCollecting }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        var result = $.parseJSON(msg.d);
                        if (result.Success) {
                            window.setDealCollectUI(elem, result.IsCollecting);
                        }
                    }, error: function (response, q, t) {
                        if (t === 'userNotLogin') {
                            //put bid to storage
                            if (window.localStorage != null) {
                                window.localStorage.setItem("collectDealData",
                                    JSON.stringify({
                                        'bid': bid,
                                        'collecting': isCollecting,
                                        'expires': new Date().getTime() + (1000 * 60 * 10)
                                    }));
                            }
                            location.href = loginUrl;
                        }
                    }
                });
                return false;
            }

            window.setDealCollectUI = function (elem, isCollecting) {
                if (isCollecting) {
                    //未收藏
                    $(elem).find('a').addClass('on');
                    $(elem).find('i').removeClass('fa-heart-o').addClass('fa-heart');                                  
                } else {
                    //收藏
                    $(elem).find('a').removeClass('on');
                    $(elem).find('i').removeClass('fa-heart').addClass('fa-heart-o');
                }
            }

            if (window.localStorage != null) {
                var ccdStr = window.localStorage.getItem("collectDealData");
                if (ccdStr != null) {
                    try {
                        var ccd = $.parseJSON(ccdStr);
                        if (ccd != null && ccd.expires > new Date().getTime()) {
                            var bid = ccd.bid;
                            var collecting = ccd.collecting;
                            $.ajax({
                                type: "POST",
                                url: "/Ppon/Default.aspx/SetMemberCollectDeal",
                                data: "{businessHourGuid:'" + bid + "', cityId:0, isCollecting:" + collecting + "}",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (msg) {
                                    var result = $.parseJSON(msg.d);
                                    if (result.Success) {
                                        var elem = $('.item_collect').filter(function () {
                                            return $(this).data("bid") === bid;
                                        });
                                        window.setDealCollectUI(elem, result.IsCollecting);
                                    }
                                }
                            });
                            window.localStorage.removeItem("collectDealData");
                        }
                    } catch (ex) {
                    }
                }            
            }
        },
        loadMemberCollection: function () {
            var mcBids = this.config.memberCollectionDealGuids;
            $('.item_collect').each(function () {                
                if ($(this).attr('memberCollectInit') == null) {
                    $(this).attr('memberCollectInit', '1');
                    var bid = $(this).data('bid');
                    if ($.grep(mcBids, function (mcBid) {
                        return mcBid === bid;
                    }).length > 0) {
                        //收藏
                        $(this).find('a').addClass('on');
                        $(this).find('i').removeClass('fa-heart-o').addClass('fa-heart');
                    }                    
                }
            });
        }
        /////////////////tool/////////////////
    }

    DealListModule.defaults = DealListModule.prototype.defaults;

    $.fn.pponDealList = function (options) {
        return this.each(function () {
            new DealListModule(this, options).init();
        });
    };
}(jQuery));

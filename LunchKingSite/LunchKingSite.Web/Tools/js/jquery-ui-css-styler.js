﻿/*
To use this styler:
1. Wrap the group of radio inputs inside a div tag, <div styler="radio"... />
2. Give each radio input a text attribute specifying the text to show on the screen, <input text="ShowMe" ... />

p.s. 
jquery-ui buttonset() requires that a corresponding label for each input to be specified, 
implying that an 'id' attribute must be specified for each input.
*/
function radioStyler(divElem) {
	
	var radioGroupName = divElem.find('input:first').attr('name');
	if(radioGroupName === null || radioGroupName.length === 0) {
		return;
	}

	var eleCnt = 0;
	divElem.addClass('ui-buttonset');
	divElem.find('input').each(function (idx, input) {
		eleCnt++;

		var id;
		if ($(input).attr('id') !== undefined) {
			id = $(input).attr('id');
		} else {
			id = radioGroupName + idx;
			$(input).attr('id', id);
		}
		var text = $(input).attr('text');

		$(input).after('<label for="' + id + '"' + 'class="ui-button ui-widget ui-state-default ui-button-text-only"><span class="ui-button-text">' + text + '</span></label>');
		$(input).addClass('ui-helper-hidden-accessible');

		if ($(input).is(':checked')) {
			$(input).next().addClass('ui-state-active');
		}

		$(input).change(function () {
			if ($(this).is(':checked')) {
				$('input[name="' + radioGroupName + '"]').each(function (idx, ele) {
					$(ele).next().removeClass('ui-state-active');
				});
				$(this).next().addClass('ui-state-active');
			}
			else {
				$(this).next().removeClass('ui-state-active');
			}
		});
	});
	
	//add round corners
	if (eleCnt >= 2) {
		divElem.find('label:first').addClass('ui-corner-left');
		divElem.find('label:last').addClass('ui-corner-right');
	}
}

$(document).ready(function () {

    //style all radios
    $('div[styler="radio"]').each(function (idx, div) {
        radioStyler($(div));
    });

    
    $(".jtable td").css('font-weight', 'normal');
    $(".jtable").css('border-collapse', 'collapse');
    $(".jtable th").each(function () {
        $(this).addClass("ui-state-default");
    });
    $(".jtable td").each(function () {
        $(this).addClass("ui-widget-content");
    });
    $(".jtable.hoverable tr").hover(
        function () {
            $(this).children("td").addClass("ui-state-hover");
        },
        function () {
            $(this).children("td").removeClass("ui-state-hover");
        }
    );
    $(".jtable.highlightable tr").click(function () {
        $(this).children('td').toggleClass("ui-state-highlight");
    });

});
﻿$(document).ready(function () {
    try {
        var imgSrc = [];
        var bids = getUrlBid();
        var urlHead = ("https:" == document.location.protocol ? "https://r.turn.com/r/beacon?b2=" : "http://r.turn.com/r/beacon?b2=");
       
        if (bids != null) {
            if (bids[0] == '90c56295-82fc-4d79-a0d9-59aba1ea34ce') {
                imgSrc.push(urlHead + "Q8kEYYtCjQskNPIR28oAKzpvNG9YYaPbTQ87sQVrrIxjsf3sM0j-lLHjyyTS3e7VT3yG-ESuXS2S7QXKGJhZwQ&cid=");
                imgSrc.push(urlHead + "kTAANQD_X_kwGW9j_uzNNWk2NbgqP2fScJz4f0WRFPpjsf3sM0j-lLHjyyTS3e7VuAZnWWLG2RsZLHI2K9GM6Q&cid=");
            }
        }
        
        if (imgSrc.length > 0) {
            for (var i in imgSrc) {
                document.body.appendChild(beaconImgCreate(imgSrc[i], "beaconImg" + i));
            }
        }
    } catch (i) {
    }
});

function beaconImgCreate(src, imgId) {
    var ua = window.navigator.userAgent;
    var isIe = ua.indexOf("MSIE ") > -1;

    var img = isIe ? new Image() : document.createElement('img');
    img.src = src;
    img.id = imgId;
    return img;
}
function getUrlBid() {
    var checkStr = location.href.toLowerCase();
    var myRegExp = /[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}/;
    return checkStr.match(myRegExp);
}

﻿$().ready(function () {
    if (typeof(Sys) != 'undefined') {
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(AutoCompleteEvents); //解決 jQuery 配 UpdatePanel 後第二次執行失效問題
    }
    $('body').css('margin-top', '0px').css('padding-top', '0px');
    $('#menuPanel div br').remove();
    $('.leftpanel').css('width', '180px').css('max-width', '180px');
    $('.rightpanel').css('padding', '5px');
    $(window).bind('resize', function () {
        $('#menuPanel').height($(window).height() - 10);
    });
    $(window).trigger('resize');

    //hover effect
    var menuPanelItemStyle = {
        'display': 'block',
        'padding-top': '3px',
        'text-decoration': 'none',
        'color': '#FD4C47',
        'border-bottom': 'dotted 1px rgb(87, 124, 224)'
    };
    $('#menuPanel div a').css(menuPanelItemStyle);
    $('#menuPanel div a').mouseenter(function () {
        $(this).css('padding-left', '5px');
    }).mouseleave(function () {
        $(this).css('padding-left', '0px');
    });
    //remember panels are hidden or visible
    //    var collapseImg = '@Url.Content("~/images/icons/collapse.jpg")';
    //    var expandImg = '@Url.Content("~/images/icons/expand.jpg")';

    var menuPanelHeaderStyle = { 'border-bottom': 'solid 3px orange', 'color': '#4E57FF', 'width': '97%' };
    $('.collapsePanelHeader').css(menuPanelHeaderStyle);
    //fix image slidedown fail
    $(':image', '.collapsePanelHeader').live('click', function () {
        $(this).parent().parent().trigger('click');
        return false;
    });
    $('.collapsePanelHeader').live('click', function () {
        if ($(this).next().is(':hidden')) {
            $(this).next().slideDown('fast', function () {
                memoryPanel();
            });
            $(':image', this).attr('src', expandImg);
        } else {
            $(this).next().slideUp('fast', function () {
                memoryPanel();
            });
            $(':image', this).attr('src', collapseImg);
        }
    });

    function memoryPanel() {
        var hiddenPanels = new Array();
        $('.collapsePanelHeader').each(function () {
            if ($(this).next().is(':hidden')) {
                hiddenPanels.push($(this).attr('id'));
            }
        });
        localStorage.setItem('menuHiddenPanels', hiddenPanels.join(','));
    }

    function restorePanel(s) {
        if (s == null || s == "") {
            return;
        }
        var hiddenPanelIDs = s.split(',');
        for (var idx in hiddenPanelIDs) {
            var target = $('#' + hiddenPanelIDs[idx]);
            target.next().hide();
            $(':image', target).attr('src', collapseImg);
        }
    }

    var hiddenPanels = localStorage.getItem("menuHiddenPanels");
    //restorePanel(hiddenPanels);

    //favorite links
    var landmark = $('[id$=pMembershipHead]');
    var idolBody = $('[id$=pMembership]');
    var favoHeader = $(landmark).clone().insertBefore(landmark).attr('id', 'favoHeader');
    $(favoHeader).children('div').children(':contains("會員管理")').text("最近連結");

    var favoBody = idolBody.clone().insertAfter(favoHeader).attr('id', 'favoPanel');
    favoBody.find('a').remove();
    restorePanel(hiddenPanels);

    addRenderLink = function (text, url) {
        var str = localStorage.getItem("favoriteLinks");
        if (str == null || str == "")
            var links = new Array();
        else {
            var links = str.split(',');
        }
        //filter duplicated urls
        links.push(text + '|' + url);
        var links2 = new Array();
        for (var i in links) {
            var isExist = false;
            for (var j in links2) {
                if (links[i].toLowerCase() == links2[j].toLowerCase()) {
                    isExist = true;
                    break;
                }
            }
            if (isExist && links2[j] == text + '|' + url) {
                links2.splice(j, 1);
                links2.push(links[i]);
            }
            if (isExist == false) {
                links2.push(links[i]);
            }
        }
        localStorage.setItem("favoriteLinks", links2.join(','));
    };

    $('#menuPanel div a').click(function () {
        if ($(this).parent().attr('id') == 'favoPanel') {
            return false;
        }
        text = $(this).text();
        url = this.href; // $(this).attr('href') is not absolute url;
        addRenderLink(text, url);
        location.href = url;
        return false;
    });


    renderRecentLink = function (text, url) {
        var a = $('<a />').css(menuPanelItemStyle).text(text).attr('href', url).addClass('recentLink');
        a.css('position', 'relative');
        var imgDel = $('<img />').attr('src', '/Themes/default/images/delete_icon.gif').css('opacity', '0.05')
            .css('position', 'absolute').css('right', 0);
        imgDel.mouseenter(function () {
            $(this).css('opacity', '1');
        }).mouseleave(function () {
            $(this).css('opacity', '0.05');
        }).click(function () {
            var idx = $('.recentLink').index($(this).parent());
            removeRecentLink(idx);
            return false;
        });
        a.append(imgDel);
        return a;
    };

    var tmp = localStorage.getItem("favoriteLinks");
    if (tmp != null && tmp != '') {
        var links = tmp.split(',');
        //keep only 10 links
        if (links.length >= 16) {
            links = links.slice(links.length - 10, links.length);
        }
        if (links.length >= 1) {
            for (var i = links.length - 1; i >= 0; i--) {
                var link = links[i];
                var text = link.split('|')[0];
                var url = link.split('|')[1];
                renderRecentLink(text, url).appendTo(favoBody);
            }
        }
    }

    removeRecentLink = function (idx) {
        var tmpfl = localStorage.getItem("favoriteLinks");
        if (tmpfl != null && tmp != '') {
            var links = tmpfl.split(',');
            links.splice(links.length - idx - 1, 1);
            localStorage.setItem("favoriteLinks", links);
            $('.recentLink').eq(idx).remove();
        }
    };
    function backendRedirect(url) {
        var linkto = $('<a></a>').attr('href', url).attr('target', '_blank').appendTo('body')[0];
        linkto.click();
        $(linkto).remove();
    }
    //press alt + a to add current link to favorite links
    $(document).keydown(function (evt) {
        window['key' + evt.keyCode] = true;
        if (window['key18'] && window['key65']) { //atl + a
            window['key18'] = window['key65'] = false;
            var url = location.href;
            var text = $.trim($('title', 'head').text());
            if (text == '17Life後台') {
                text = window.prompt("連結名稱", text);
                if (text == null) {
                    return;
                }
                if ($.trim(text) == '') {
                    text = '17Life後台';
                }
            }
            addRenderLink(text, url);
            renderRecentLink(text, url).prependTo(favoBody);
        }
        var shortcutUrls = new Array();
        shortcutUrls.push(window.siteUrl + '/ControlRoom/system/DataManagerReset.aspx');
        shortcutUrls.push(window.siteUrl + '/ControlRoom/ppon/PponDealTimeSlot.aspx');
        shortcutUrls.push(window.siteUrl + '/ControlRoom/Order/order_list.aspx');
        shortcutUrls.push(window.siteUrl + '/ControlRoom/Seller/seller_list.aspx');
		shortcutUrls.push(window.siteUrl + '/ControlRoom/User/ServiceIntegrate.aspx');
		shortcutUrls.push(window.siteUrl + '/controlroom/system/jobs.aspx');
        shortcutUrls.push('');
        shortcutUrls.push('');
        shortcutUrls.push('');
        shortcutUrls.push(window.siteUrl + '/ControlRoom/System/Config.aspx');
        if (window['key18'] && window['key48']) { //atl + 0
            window['key18'] = window['key48'] = false;
            backendRedirect(shortcutUrls[0]);
        }
        if (window['key18'] && window['key49']) { //atl + 1
            window['key18'] = window['key49'] = false;
            backendRedirect(shortcutUrls[1]);
        }
        if (window['key18'] && window['key50']) { //atl + 2
            window['key18'] = window['key50'] = false;
            backendRedirect(shortcutUrls[2]);
        }
        if (window['key18'] && window['key51']) { //atl + 3
            window['key18'] = window['key51'] = false;
            backendRedirect(shortcutUrls[3]);
        }
        if (window['key18'] && window['key52']) { //atl + 4
            window['key18'] = window['key52'] = false;
            backendRedirect(shortcutUrls[4]);
		}
		if (window['key18'] && window['key53']) { //atl + 5
			window['key18'] = window['key53'] = false;
			backendRedirect(shortcutUrls[5]);
		}
        if (window['key18'] && window['key57']) { //atl + 9
            window['key18'] = window['key57'] = false;
            backendRedirect(shortcutUrls[9]);
        }
        if (window['key18'] && window['key77']) { //atl + m
            window['key18'] = window['key77'] = false;
        }
        if (window['key18'] && window['key67']) { //atl + c
            window['key18'] = window['key67'] = false;
            if (window.location.href.toLowerCase().indexOf('ppon/setup.aspx') == -1) {
                return;
            }
            if (JSON.stringify == null) return;
            var formDatas = [];
            $(':text,textarea,select', '#tabs').each(function () {
                var id = $(this).attr('id');
                var val = $(this).val();
                var type = 0;
                formDatas.push({ id: id, val: val, type: type });
            });
            $(':checkbox:checked').not(':disabled').each(function () {
                var id = $(this).attr('id');
                var val = '1';
                var type = 1;
                formDatas.push({ id: id, val: val, type: type });
            });
            var editors = CKEDITOR.instances;
            formDatas.push({ id: 'ctl00_ContentPlaceHolder1_tbSlrInt', val: editors['ctl00_ContentPlaceHolder1_tbSlrInt'].getData(), type: 2 });
            formDatas.push({ id: 'ctl00_ContentPlaceHolder1_rmkTxt', val: editors['ctl00_ContentPlaceHolder1_rmkTxt'].getData(), type: 2 });
            formDatas.push({ id: 'ctl00_ContentPlaceHolder1_tbInt', val: editors['ctl00_ContentPlaceHolder1_tbInt'].getData(), type: 2 });
            formDatas.push({ id: 'ctl00_ContentPlaceHolder1_tbPdtl', val: editors['ctl00_ContentPlaceHolder1_tbPdtl'].getData(), type: 2 });

            //alert(JSON.stringify(formDatas));

            if ($('#clipboard').size() == 0) {
                $("<div id='clipboardDialog'><TEXTAREA id='clipboard' name='clipboard' style='width:100%;height:100%'></TEXTAREA></div>")
                    .hide().appendTo('body');
            }
            $('#clipboard').val(JSON.stringify(formDatas));
            $('#clipboardDialog').dialog({
                resizable: false,
                height: 300,
                width: 400,
                modal: true,
                buttons: {
                    "關閉": function () {
                        $('#clipboard').val('');
                        $(this).dialog("close");
                    }
                }
            });
            $('#clipboard').select();
        }
        if (window['key18'] && window['key80']) { //atl + p
            window['key18'] = window['key80'] = false;
            if (window.location.href.toLowerCase().indexOf('ppon/setup.aspx') == -1) {
                return;
            }
            if ($('#clipboard').size() == 0) {
                $("<div id='clipboardDialog'><TEXTAREA id='clipboard' name='clipboard' style='width:100%;height:100%'></TEXTAREA></div>")
                    .hide().appendTo('body');
            }
            $('#clipboardDialog').dialog({
                resizable: false,
                height: 300,
                width: 400,
                modal: true,
                buttons: {
                    "貼上": function () {
                        if (JSON.stringify == null) return;
                        var formDatas = $("#clipboard").val();
                        $("#clipboard").val('');
                        if (formDatas != null) {
                            formDatas = $.parseJSON(formDatas);
                        }
                        var editors = CKEDITOR.instances;
                        for (var i = 0; i < formDatas.length; i++) {
                            var id = formDatas[i].id;
                            var val = formDatas[i].val;
                            var type = formDatas[i].type;
                            if (type == 0) {
                                $('#' + id).val(val);
                            } else if (type == 1 && val == '1') {
                                $('#' + id).attr('checked', true);
                            } else if (type == 2) {
                                editors[id].setData(val);
                            }
                        }
                        $(this).dialog("close");
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
        if (window['key18'] && window['key109']) { //alt + '-' clooapse all
            $('.collapsePanelHeader').each(function () {
                $(this).next().slideUp('fast', function () {
                    memoryPanel();
                });
                $(':image', this).attr('src', collapseImg);
            });
        }
        if (window['key18'] && window['key107']) { // alt then '+' expand all
            $('.collapsePanelHeader').each(function () {
                $(this).next().slideDown('fast', function () {
                    memoryPanel();
                });
                $(':image', this).attr('src', expandImg);
            });
        }
        if (window['key18'] && window['key122']) { // alt then f11
            $('.leftpanel').toggle();
        }
    }).keyup(function (evt) {
        window['key' + evt.keyCode] = false;
    });

    function AutoCompleteEvents() {
        // ATM申請退款受款帳戶資料  銀行名稱自動完成
        $('#ctl00_ContentPlaceHolder1_txtBankName').on('keydown', function (e) {
            if ($('.suggestionList li').size() > 0) {
                if (e.keyCode == 40) {
                    if ($('.active').size() == 0) {
                        $('.suggestionList li:eq(0)').addClass('active');
                    } else {
                        if ($('.suggestionList li').last().is($('.active'))) {
                            $('.active').removeClass('active');
                            $('.suggestionList li').first().addClass('active').focus();
                        } else {
                            $('.active').removeClass('active').next('li').addClass('active').focus();
                        }
                    }
                    $('#ctl00_ContentPlaceHolder1_txtBankName').val($('.active').text());
                } else if (e.keyCode == 38) {
                    if ($('.suggestionList li').first().is($('.active'))) {
                        $('.active').removeClass('active');
                        $('.suggestionList li').last().addClass('active').focus();
                    }
                    else {
                        $('.active').removeClass('active').prev('li').addClass('active').focus();
                    }
                    $('#ctl00_ContentPlaceHolder1_txtBankName').val($('.active').text());
                }
            }
        });

        $('#ctl00_ContentPlaceHolder1_txtBankName').on('keyup', function (e) {
            $('#ctl00_ContentPlaceHolder1_txtBankName').on('keydown', function (e) { console.log('keydown:' + e.keyCode); });
            $('#ctl00_ContentPlaceHolder1_labBankNameErr').text('');
            // BUILD SUGGESTION
            if ((e.keyCode == 32) || !(e.keyCode >= 9 && e.keyCode <= 46) || (e.keyCode >= 112 && e.keyCode <= 137)) {
                var bankInfo = $('#ctl00_ContentPlaceHolder1_txtBankName').val().trim();
                if (bankInfo.length > 0) {
                    $.ajax({
                        type: "POST",
                        url: "../../ControlRoom/Order/GetBankInfo",
                        data: JSON.stringify({ 'editBank': encodeURI(bankInfo), 'editBranch':'', 'bankType': 'bank' }),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (result) {
                            if (result.success) {
                                if (result.bankList.length > 0) {
                                    var data = '';
                                    $.each(result.bankList, function (index, value) {
                                        if (index == 0) {
                                            data += "<li class='sugg' >" + value + "</li>";
                                        } else {
                                            data += "<li class='sugg' >" + value + "</li>";
                                        }
                                    });
                                    $('.suggestionList ul').html(data);
                                    $('.suggestionBox').show();
                                } else {
                                    $('.suggestionList ul li').remove();
                                    $('#ctl00_ContentPlaceHolder1_labBankNameErr').text('查無銀行名稱資料，請再確認。');
                                }
                            } else {
                                $('.suggestionList ul li').remove();
                                $('#ctl00_ContentPlaceHolder1_labBankNameErr').text('查無銀行名稱資料，請再確認。');
                            }
                        }
                    });

                    $(document).on('click','.sugg', function () {
                        $('#ctl00_ContentPlaceHolder1_txtBankName').val($(this).text()); return;
                    });
                } else {
                    $('#ctl00_ContentPlaceHolder1_labBankNameErr').text('查無銀行名稱資料，請再確認。');
                }
            }
            // BACKSPACE
            if (e.keyCode == 8 && $('#ctl00_ContentPlaceHolder1_txtBankName').val() == "" || e.keyCode == 27) {
                $('.suggestionBox').hide();
                $('.suggestionList ul li').remove();
            }
        });

        // ATM申請退款受款帳戶資料  銀行分行別自動完成
        $('#ctl00_ContentPlaceHolder1_txtBankBranch').on('keydown', function (e) {
            if ($('.suggestionList2 li').size() > 0) {
                if (e.keyCode == 40) {
                    if ($('.active').size() == 0) {
                        $('.suggestionList2 li:eq(0)').addClass('active');
                    } else {
                        if ($('.suggestionList2 li').last().is($('.active'))) {
                            $('.active').removeClass('active');
                            $('.suggestionList2 li').first().addClass('active').focus();
                        } else {
                            $('.active').removeClass('active').next('li').addClass('active').focus();
                        }
                    }
                    $('#ctl00_ContentPlaceHolder1_txtBankBranch').val($('.active').text());
                } else if (e.keyCode == 38) {
                    if ($('.suggestionList2 li').first().is($('.active'))) {
                        $('.active').removeClass('active');
                        $('.suggestionList2 li').last().addClass('active').focus();
                    }
                    else {
                        $('.active').removeClass('active').prev('li').addClass('active').focus();
                    }
                    $('#ctl00_ContentPlaceHolder1_txtBankBranch').val($('.active').text());
                }
            }
        });

        $('#ctl00_ContentPlaceHolder1_txtBankBranch').on('keyup', function (e) {
            $('#ctl00_ContentPlaceHolder1_txtBankBranch').on('keydown', function (e) { console.log('keydown:' + e.keyCode); });
            $('#ctl00_ContentPlaceHolder1_labBankBranchErr').text('');
            // BUILD SUGGESTION
            if ((e.keyCode == 32) || !(e.keyCode >= 9 && e.keyCode <= 46) || (e.keyCode >= 112 && e.keyCode <= 137)) {
                var bankInfo = $('#ctl00_ContentPlaceHolder1_txtBankName').val().trim().substring(0, 3);
                if (!$.isNumeric(bankInfo)) {
                    $('#ctl00_ContentPlaceHolder1_labBankNameErr').text('銀行名稱錯誤，請再確認。');
                    return;
                }
                var bankBranchInfo = $('#ctl00_ContentPlaceHolder1_txtBankBranch').val().trim();
                if (bankInfo.length > 0) {
                    $.ajax({
                        type: "POST",
                        url: "../../ControlRoom/Order/GetBankInfo",
                        data: JSON.stringify({ 'editBank': encodeURI(bankInfo), 'editBranch': encodeURI(bankBranchInfo), 'bankType': 'bankBranch' }),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (result) {
                            if (result.success) {
                                if (result.bankList.length > 0) {
                                    var data = '';
                                    $.each(result.bankList, function (index, value) {
                                        if (index == 0) {
                                            data += "<li class='sugg2' >" + value + "</li>";
                                        } else {
                                            data += "<li class='sugg2' >" + value + "</li>";
                                        }
                                    });
                                    $('.suggestionList2 ul').html(data);
                                    $('.suggestionBox2').show();
                                } else {
                                    $('.suggestionList2 ul li').remove();
                                    $('#ctl00_ContentPlaceHolder1_labBankBranchErr').text('查無銀行分行別資料，請再確認。');
                                }
                            } else {
                                $('.suggestionList2 ul li').remove();
                                $('#ctl00_ContentPlaceHolder1_labBankBranchErr').text('查無銀行分行別資料，請再確認。');
                            }
                        }
                    });

                    $(document).on('click', '.sugg2', function () {
                        $('#ctl00_ContentPlaceHolder1_txtBankBranch').val($(this).text()); return;
                    });
                } else {
                    $('#ctl00_ContentPlaceHolder1_labBankBranchErr').text('查無銀行分行別資料，請再確認。');
                }
            }
            // BACKSPACE
            if (e.keyCode == 8 && $('#ctl00_ContentPlaceHolder1_txtBankBranch').val() == "" || e.keyCode == 27) {
                $('.suggestionBox2').hide();
                $('.suggestionList2 ul li').remove();
            }
        });

        // CLICK TO CANCEL
        $('body').click(function (e) {
            $('.suggestionBox').hide();
            $('.suggestionBox2').hide();
        });
    }

    //remember scorll position
    $(window).bind('resize', function () {
        $('#menuPanel').height($(window).height() - 10);
    });
    $(window).trigger('resize');
    $('#menuPanel').css('overflow-y', 'scroll').css('width', '100%');
    $('#menuPanel').scroll(function () {
        localStorage.setItem('menuPanelsScrollTop', $(this).scrollTop());
    });
    var scrollPositionTop = localStorage.getItem("menuPanelsScrollTop");
    if (scrollPositionTop != null) {
        $('#menuPanel').scrollTop(scrollPositionTop);
    }
});
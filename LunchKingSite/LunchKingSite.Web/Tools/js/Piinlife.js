﻿//------------------------------------------------//
//版本:0.0.0.1
//------------------------------------------------//

//判斷正式站跟測試站
if (window.document.location.pathname.indexOf('lksite/') > 0) {
    ServerLocation = "/lksite/";
}

function preventDobuleSubmit() {
    $('form').submit(function () {
        if (window.submitted) {
            $.blockUI({
                message: "<h2 style='padding:20px; letter-spacing:2px'> 資料已送出，請稍後 ...</h2>",
                css: { left: ($(window).width() - 300) / 2 + 'px', width: '300px' }
            });
            return false;
        }
        window.submitted = true;
    });
}
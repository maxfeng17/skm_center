﻿<%@ Page Language="C#" MasterPageFile="~/NewMember/NewMember.Master" AutoEventWireup="true"
    CodeBehind="AboutUs.aspx.cs" Inherits="LunchKingSite.Web.AboutUs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Themes/PCweb/css/MasterPage.css")%>" />
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MC">
    <div class="Homepagecenter">
        <div class="LifeMemberFrame">
            <div class="LifeMemberTopBlack">
                <div class="LifeMemberTopText">17Life-領先全台虛實整合網站</div>
            </div>
            <div class="LifeMemberCenter">
                <div class="MemberPrivacy">
                    
                    <div style="font-size: 15px; padding: 10px 20px 20px 20px; border: 0px solid #76A045;">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;17Life音近「一起Life」、「一起生活」，希望大家能一起過生活，滿足消費者食衣住行育樂各方面的需求，舉凡知名餐飲集團、人氣美食、頂級SPA、豪華旅遊行程，或是時尚美妝服飾、3C家電、宅配熱銷、民生用品等，天天提供3折起超低優惠，17Life是全台第一個也是最完善以區域生活網概念出發的虛擬消費平台，在網路平台及行動裝置上，提供超值商品或實體店面優惠券服務，讓消費者可即時搶購好康並享受服務。
                    </div>
                    <div style="font-size:15px; padding:10px 20px 20px 20px;border:0px solid #76A045;">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;17Life擁有強力的會員基礎，包含：17Life會員250萬人次、全國最大女性購物網─PayEasy 400萬的優質會員、全國發卡量 No.2的台新金控(400萬發卡量)，還有國內323家知名企業指定的企業福利網，及28萬FB粉絲，會員素質強且具有消費力。
                    </div>
                    <div style="font-size:15px; padding:10px 20px 20px 20px;border:0px solid #76A045;">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;自2012年12月開始17Life與全家便利商店合作，導入網購族群進店消費的創新促銷模式，推出在17Life網站上索取Pin碼，再到全家店舖取貨付款的「線上Coupon」機制，合作至今，帶動了近100萬人次進入全家便利商店消費，象徵虛實整合時代又向前邁出一大步。
                    </div>
                    <div style="font-size:15px; padding:10px 20px 20px 20px;border:0px solid #76A045;">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;17Life在2013年開始正式推出專屬原生APP，網羅全台店家，上千張優惠券立即使用，隨著電子商務行動化趨勢，流量占比大幅成長，2014年2月份17Life推出LINE官方頻道，目前已有380萬粉絲數，結合社群軟體的推廣運用，不僅提高品牌知名度，還能隨時服務更多的消費者，加速市場增長；更因為貼心及優良品質的服務，榮獲「2014臺灣服務業大評鑑」大型購物網站銅牌之肯定。
                    </div>
                    <div style="font-size:15px; padding:10px 20px 20px 20px;border:0px solid #76A045;">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;網路平台具有無遠弗屆的特性，實體通路則能夠直接服務不同區域的消費者，O2O(Online To Offline )的虛實整合，創造虛擬通路、實體業者及消費者三贏的局面，17Life致力於不停的創新，以領先全台的虛實整合平台之姿，提供消費者更多即時且便利的優惠。
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/piinlife/hideal.Master" AutoEventWireup="true"
    CodeBehind="HiDeal.aspx.cs" Inherits="LunchKingSite.Web.piinlife.HiDeal" %>

<%@ Import Namespace="LunchKingSite.WebLib" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ScriptContent" runat="server">
    <link href="<%#ResolveUrl("~/Themes/PCweb/HighDeal/SingleProduct.css")%>" rel="stylesheet" type="text/css" />
    <meta property="fb:app_id" content="<%# FacebookAppId %>" />
    <meta property="og:title" content="<%# FacebookOgTitle %>" />
    <meta property="og:url" content="<%# FacebookOgUrl %>" />
    <meta property="og:image" content="<%# FacebookOgImage %>" />
    <meta property="og:description" content="<%# FacebookOgDescription %>" />
    <link rel="image_src" href="<%# ImageSrcLinkHref %>" />
    <link rel="canonical" href="<%# CanonicalLinkHref %>" />
    <script type="text/javascript" src="../Tools/js/effects/nivoslider.js"></script>
    <script type="text/javascript" src="../Tools/js/osm/OpenLayers.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            <% if (ShowEventEmail)
               { %>
            showSubscription();
            <% } %>
        });

        $(function () {
            //預設書籤位置
            ShowTab(1);

            /*facebook like*/
            addFbLike();

            $('#divPictureSlider').nivoSlider({
                effect: 'fade', //Specify sets like: 'fold,fade,sliceDown'
                slices: 1,
                animSpeed: 500, //Slide transition speed
                pauseTime: 3500,
                startSlide: 0, //Set starting Slide (0 index)
                directionNav: true, //Next & Prev
                prevText: 'does not work',
                nextText: 'shit',
                directionNavHide: true, //Only show on hover
                controlNav: true, //1,2,3...
                keyboardNav: true, //Use left & right arrows
                pauseOnHover: true, //Stop animation while hovering
                manualAdvance: false, //Force manual transitions
                captionOpacity: 0.8, //Universal caption opacity
                beforeChange: function () { },
                afterChange: function () { },
                slideshowEnd: function () { }
            });
        });

        function showmap(o, enableOpenStreetMap) {

            var address = $(o).attr('address');
            var longitude = $(o).attr('longitude');
            var latitude = $(o).attr('latitude');

            var q = latitude != 'Null' && latitude != undefined && latitude != 0 && latitude != '' &&
                    longitude != 'Null' && longitude != undefined && longitude != 0 && longitude != ''
                    ? latitude + ',' + longitude : address;

            if (address.length > 0) {

                $.unblockUI();
                $.blockUI({
                    message: $('#blockMap'),
                    css: {
                        backgroundcolor: 'transparent',
                        border: 'none',
                        top: $(window).height() / 8 + 'px',
                        left: ($(window).width() - 686) / 2 + 'px',
                        cursor: 'pointer',
                        width: '700px',
                        height: '520px'
                    }
                });
                if (enableOpenStreetMap.toLowerCase() == 'true') {

                    // use OpenStreetMap
                    $('#iMap').css('display', 'none');
                    $('#map').empty(); // 因OpenStreetMap會一直append新的地圖，必須清空

                    map = new OpenLayers.Map("map");
                    map.addLayer(new OpenLayers.Layer.OSM());

                    var lonLat = new OpenLayers.LonLat(longitude, latitude)
                          .transform(
                            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
                            map.getProjectionObject() // to Spherical Mercator Projection
                          );
                    var zoom = 16;

                    var markers = new OpenLayers.Layer.Markers("Markers");
                    map.addLayer(markers);
                    markers.addMarker(new OpenLayers.Marker(lonLat));
                    map.setCenter(lonLat, zoom);

                } else {
                    // use GoogleMap with iframe
                    $('#iMap').attr('src', 'https://maps.google.com/maps?f=q&source=s_q&z=16&hl=zh-TW&geocode=&ie=UTF8&iwloc=A&output=embed&q=' + q);
                }

            } else {
                alert("Google Map 查無此地址!!");
            }
        }

        function addFbLike() {
            var fblink = '<%=confProv.SiteUrl %>/piinlife/FBDeal.aspx?did=<%=FbDealId %>';
            $('.divFbLike').append('<div id="TheFbLike" class="fb-like" href="' + fblink + '" data-send="true" data-layout="button_count" data-show-faces="true" data-action="recommend"></div>');
            //$('.fb-Like').attr('href', window.location.href);
        }
        function activateTabStyle(anchor) {
            $(anchor).closest('ul', this).find('li').removeClass('active');
            $(anchor).closest('li', this).addClass('active');
        }

        function ShowTab(index) {
            var tabList = new Array(<%=TabSeqListShow%>);
            for (var i = 0; i < tabList.length; i++) {
                if (index != tabList[i]) {
                    $("#tabContent" + tabList[i].toString()).hide();
                    $("#TabLinks" + tabList[i].toString()).removeClass("active");
                }
            }
            $("#tabContent" + index.toString()).show();
            $("#TabLinks" + index.toString()).addClass("active");
        }

        function GoToTab(index) {
            ShowTab(index);
            $(window).scrollTop($("#ulTab").offset().top);
        }
    </script>
    <style type="text/css">
        .style1
        {
            width: 473px;
        }
        section.Main
        {
            height: 100%;
            width: 100%;
        }
        
        .sliderContainer
        {
            position: relative;
            width: 650px;
            height: 360px;
        }
        
        #divPictureSlider img
        {
            position: absolute;
            left: 0px;
            top: 0px;
            visibility: hidden;
        }
        .nivo-controlNav
        {
            position: absolute;
            bottom: 0px;
            right: 0%;
            margin-left: -50px;
            height: 20px;
            z-index: 10;
        }
        #divSliderBottomBandDeal
        {
            background: -moz-linear-gradient(left, #000000, #AAAAAA);
            background: -webkit-gradient(linear, left top, right top, from(#000000), to(#AAAAAA));
            filter: progid:DXImageTransform.Microsoft.Gradient( StartColorStr='#000000', EndColorStr='#AAAAAA', GradientType=1);
            background-color: black;
            height: 20px;
            left: 0;
            opacity: 0.6;
            position: absolute;
            bottom: 0;
            width: 100%;
            z-index: 1;
        }
        
        a.nivo-control
        {
            position: relative;
            margin-left: 4px;
            margin-right: 4px;
            background: url(<%=VirtualPathUtility.ToAbsolute(@"~/Themes/HighDeal/images/SingleProduct/Single_Product_Split.png")%>) no-repeat;
            background-position: -687px -17px;
            height: 16px;
            width: 16px;
            cursor: pointer;
            display: block;
            float: left;
            text-indent: -99999px;
            z-index: 99;
        }
        .nivo-controlNav a.active
        {
            background-position: -687px -39px;
        }
        
        /* nivo slider: the div containing "previous" and "next"  link*/
        div.nivo-directionNav
        {
            height: 48px;
            width: 100%;
            position: absolute;
            top: 150px;
            z-index: 21;
        }
        
        /*nivo slider "previous" link */
        a.nivo-prevNav
        {
            background: url(<%=VirtualPathUtility.ToAbsolute(@"~/Themes/HighDeal/images/SingleProduct/Single_Product_Split.png")%>) no-repeat;
            background-position: -567px -18px;
            text-indent: -99999px;
            width: 49px;
            height: 48px;
            float: left;
            z-index: 25;
        }
        a.nivo-prevNav:hover
        {
            background-position: -724px -18px;
            cursor: pointer;
        }
        
        /*nivo slider "next" link */
        a.nivo-nextNav
        {
            background: url(<%=VirtualPathUtility.ToAbsolute(@"~/Themes/HighDeal/images/SingleProduct/Single_Product_Split.png")%>) no-repeat;
            background-position: -628px -18px;
            text-indent: -99999px;
            width: 49px;
            height: 48px;
            float: right;
            z-index: 25;
        }
        a.nivo-nextNav:hover
        {
            background-position: -786px -18px;
            cursor: pointer;
        }
        
        .nivo-slice
        {
            display: block;
            position: absolute;
            z-index: 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="wrap">
        <div class="SingleProductSplit">
            <aside class="SideBarRight">
                <h2 class="Time_alert">
                    結束販售時間:<asp:Literal ID="lCloseTime" runat="server"></asp:Literal></h2>
                <div class="Packages Sidebar_item Whitebox">
                    <asp:Literal ID="lProducts" runat="server"></asp:Literal>
                    <asp:Label ID="lblscript" runat="server" Text=""></asp:Label>
                </div>
                <%--Packages--%>
                <asp:Repeater ID="rpt_Location" runat="server" OnItemDataBound="rpt_LocationBinding">
                    <HeaderTemplate>
                        <div class="Packages Sidebar_item Whitebox Location">
                            <h2 class="icon">
                                位置資訊</h2>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <h2 class="Map_Name">
                            <asp:Literal ID="lit_L_StoreName" runat="server"></asp:Literal></h2>
                        <asp:Literal ID="lit_L_Map" runat="server"></asp:Literal>
                        <p>
                            <asp:Literal ID="lit_L_Contents" runat="server"></asp:Literal>
                        </p>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div><%--Packages Location--%>
                    </FooterTemplate>
                </asp:Repeater>
            </aside>
            <section class="Main">
                <div class="Category_bar">                    
                    <div class="Category_Container">
                        <%=RenderCategoryBar() %>
                    </div>
                    <%--Category_Container--%>
                </div>
                <%--Category_bar--%>
                <hgroup class="Mainbox Whitebox">
                    <div class="Title">
                        <div class="City">
                            <span style="float: left; margin-right: 10px;">
                                <asp:Literal ID="lcity" runat="server"></asp:Literal></span>
                            <asp:PlaceHolder runat="server" ID="visaPl" EnableViewState ="false" Visible="false">
                                <p class="VISA">
                                </p>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" ID="phVisaPrivate" EnableViewState ="false" Visible="false">
                                <p class="VISA">
                                </p>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" ID="phVisaPriority" EnableViewState ="false" Visible="false">
                                <p class="VISA_sp">
                                </p>
                            </asp:PlaceHolder>
                            <asp:Literal ID="llSpecialCategoryTag" runat="server" ></asp:Literal>
                        </div>
                        <span class="divFbLike" style="float: right; width: 130px;"></span>
                        <h1 class="Name">
                            <asp:Literal ID="lDealName" runat="server"></asp:Literal></h1>
                        <h2 class="Tagline">
                            <asp:Literal ID="ldesc" runat="server"></asp:Literal></h2>
                    </div>
                    <%--title--%>
                    <%--圖片輪播--%>
                    <div class="sliderContainer">
                        <asp:ListView runat="server" ID="lvPictureSlider">
                            <LayoutTemplate>
                                <div id="divPictureSlider" style="width: 650px; height: 360px;">
                                    <image runat="server" id="itemPlaceholder" />
                                </div>
                            </LayoutTemplate>
                            <ItemTemplate>
                            <img  id="prodImg" src='<%# Eval("Url") %>' style="width: 650; height: 360;" alt="" />
                            </ItemTemplate>
                        </asp:ListView>
                        <div id="divSliderBottomBandDeal">
                        </div>
                    </div>
                </hgroup>
                <asp:Literal ID="lTap" runat="server"></asp:Literal>
                <%------------------------------------------------檔次頁籤------------------------------------------------%>
                <article class="More_Detail">
                    <ul class="Detail_tab_buttons" id="ulTab">
                        <asp:Repeater runat="server" ID="rpTabLinks">
                            <ItemTemplate>
                                <li class="" id='TabLinks<%#Eval("Seq") %>'><a href="javascript: ShowTab(<%#Eval("Seq") %>);">
                                    <%# Eval("TabTitle") %></a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <article class="info_tabs Mainbox Whitebox">
                        <asp:Repeater runat="server" ID="rpTabContents" OnItemDataBound="RpTabContentsItemDataBound">
                            <ItemTemplate>
                                <div class="Content" id='<%#"tabContent" + Eval("Seq") %>'>
                                    <div class="Detail_info">
                                        <%# Eval("Context")%>
                                        <asp:Label ID="lbNoInvoiceCreate" runat="server" ForeColor="Red"></asp:Label>
                                    </div>
                                    <asp:Literal runat="server" ID="lbUse"></asp:Literal>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </article>
                </article>
                <%------------------------------------------------檔次頁籤 end--------------------------------------------%>
            </section>
            <%--Main--%>
        </div>
        <%--SingleProductSplit--%>
    </div>
    <%--wrap--%>
    <div class="SingleProductSplit_UpWindow" id="blockMap" style="display: none">
        <div class="UpWindowClose" onclick=" $.unblockUI();" style="margin-top: -37px;">
        </div>
        <section class="Background">
            <div id="map" style="width: 700px; height: 520px;">
                <iframe id="iMap" style="width: 100%; height: 100%;"></iframe>
            </div>
        </section>
    </div>
</asp:Content>

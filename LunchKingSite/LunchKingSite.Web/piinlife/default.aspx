﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="default.aspx.cs" Inherits="LunchKingSite.Web.piinlife._default" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.I18N" %>
<%@ MasterType VirtualPath="~/PPon/PPon.Master" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/RandomParagraph.ascx" TagName="RandomParagraph"
    TagPrefix="ucR" %>
<asp:Content ID="Content3" ContentPlaceHolderID="SSC" runat="server">
    <link rel="stylesheet" type="text/css" href='<%= ResolveUrl("~/Themes/PCweb/HighDeal/Buttons.css") %>' />
    <link rel="stylesheet" type="text/css" href='<%= ResolveUrl("~/Themes/PCweb/HighDeal/MailSubscrible.css")%>' />
    <link rel="stylesheet" type="text/css" href='<%= ResolveUrl("~/Themes/PCweb/HighDeal/MasterPage_HD.css")%>' />
    <link rel="stylesheet" type="text/css" href='<%= ResolveUrl("~/Themes/PCweb/HighDeal/ppon_HD.css")%>' />
    <link rel="stylesheet" type="text/css" href='<%= ResolveUrl("~/Themes/PCweb/HighDeal/HDA1.css")%>' />
    <link rel="stylesheet" type="text/css" href='<%= ResolveUrl("~/themes/PCweb/css/RDL-L.css")%>' />
    <link rel="stylesheet" type="text/css" href='<%= ResolveUrl("~/themes/PCweb/css/RDL-M.css")%>' />
    <link rel="stylesheet" type="text/css" href='<%= ResolveUrl("~/themes/PCweb/css/RDL-S.css")%>' />
    <link rel="stylesheet" type="text/css" href='<%= ResolveUrl("~/Themes/PCweb/HighDeal/RDL_HD.css")%>' />
    <style type="text/css">
        .DealView {
            display: none;
        }

        a.fansLink {
            color: White;
        }

            a.fansLink:visited {
                color: White;
            }

            a.fansLink:hover {
                color: White;
                text-decoration: underline;
            }

        .smallslider {
            position: relative;
            padding: 0;
            margin: 0;
            height: 300px;
        }

            .smallslider ul {
                list-style-type: none;
                padding: 0;
                margin: 0;
                position: absolute;
                width: auto;
                height: auto;
            }

            .smallslider li {
                margin: 0;
                padding: 0;
            }

                .smallslider li a {
                    margin: 0;
                    padding: 0;
                }

                    .smallslider li a img {
                        border: 0;
                        padding: 0;
                        margin: 0;
                        vertical-align: top;
                    }

            .smallslider h1 {
                position: absolute;
                font-size: 12px;
                margin: 0 0 36px 2px;
                padding: 0;
                text-indent: 2%;
                line-height: 26px;
                z-index: 102;
                width: 98%;
                color: #CCC;
            }

                .smallslider h1 a {
                    padding: 0;
                    margin: 0;
                    color: #333;
                    text-decoration: none;
                    cursor: default;
                    font-size: 14px;
                    font-size: 18px;
                    line-height: 18px;
                }

            .smallslider p {
                position: absolute;
                font-size: 12px;
                margin: 0 0 28px 2px;
                padding: 0;
                text-indent: 2%;
                line-height: 26px;
                z-index: 102;
                width: 98%;
                color: #CCC;
            }

                .smallslider p span {
                    width: 400px;
                    position: absolute;
                    font-size: 13px;
                    line-height: 13px;
                    color: #F70;
                }

            .smallslider li.current-li {
            }

        .smallslider-btns {
            position: absolute;
            margin: 100px 0 0 0;
            z-index: 103;
        }

            .smallslider-btns span {
                width: 12px;
                height: 12px;
                background: url(../Themes/HighDeal/images/MailSubscrible/DSS.png) no-repeat;
                font-size: 0;
                text-indent: -9999px;
                display: inline-block;
                cursor: pointer;
                float: left;
                height: 16px;
                line-height: 16px;
                text-align: center;
                width: 16px;
            }

                .smallslider-btns span.current-btn {
                    background: url(../Themes/HighDeal/images/MailSubscrible/DSS_hover.png) no-repeat;
                }

        .smallslider-lay {
            position: absolute;
            background: black;
            height: 26px;
            width: 100%;
            z-index: 101;
        }

        #blockui-close {
            position: absolute;
            top: 19px;
            right: 7px;
            z-index: 1103;
            width: 18px;
            height: 18px;
            margin: 6px 8px auto;
            background: url(../Themes/HighDeal/images/MailSubscrible/DSS_Close.png) no-repeat;
            display: inline-block;
        }

            #blockui-close:hover {
                background: url(../Themes/HighDeal/images/MailSubscrible/DSS_Close.png) 0 -18px no-repeat;
            }
    </style>
    <link href="../Tools/js/jquery.smartbanner.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=ResolveUrl("~/Tools/js/effects/jquery.smallslider.js")%>"></script>
    <script type="text/javascript" src="../Tools/js/jquery.smartbanner.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            // set piinlife icon
            $('link[rel*=shortcut]').attr('href', '<%= ResolveUrl("~/Themes/PCweb/HighDeal/favicon.ico") %>');

            newsVisible();

            if ($(window).width() > 1024 && '<%= ShowEventEmail%>'.toLowerCase() == 'true') {
                showPiinlifeSubscription();
            }

            $('#flashbox').smallslider({});

            $(window).scroll(function () {
                if ($(document).scrollTop() >= 200) {
                    $('#btnBackTop').css('display', 'inline');
                } else {
                    $('#btnBackTop').css('display', 'none');
                }
            });

            $.fn.queueInterruptibleAction = function (delayMilseconds, actionCallback) {
                //cancel if other action exist
                this.cancelInterruptibleAction();
                // delay execute delayCallback
                var timerId = window.setTimeout(function () {
                    $.removeData(this, 'timerId');
                    actionCallback.call(this);
                }, delayMilseconds);
                $.data(this, 'timerId', timerId);
            };

            $.fn.cancelInterruptibleAction = function () {
                var timerId = $.data(this, 'timerId');
                if (timerId != null) {
                    $.removeData(this, 'timerId');
                    window.clearTimeout(timerId);
                }
            };

            $.fn.delayHover = function (tClass, pClass, enterDelay, leaveDelay) {
                if (enterDelay == null) enterDelay = 150;
                if (leaveDelay == null) leaveDelay = 400;
                return this.each(function () {
                    var trigger = $(this); //button
                    var target = $(this).closest(pClass).find(tClass); //view       
                    trigger.mouseenter(function () {
                        target.queueInterruptibleAction(enterDelay, function () {
                            target.show();
                        });
                    });
                    trigger.mouseleave(function () {
                        target.queueInterruptibleAction(leaveDelay, function () {
                            target.hide();
                        });
                    });
                    target.mouseenter(function () {
                        target.cancelInterruptibleAction();
                    });
                    target.mouseleave(function () {
                        target.queueInterruptibleAction(leaveDelay, function () {
                            target.hide();
                        });
                    });
                });
            };

            $('.DealButton').delayHover('.DealView', '.Deal, .SideDeal', 150, 400);

            // 非手機瀏覽檔次另開窗
            if ('<%= IsMobileBroswer %>' == 'False') {
                $('.dealLink').attr('target', '_blank');
            }

            //piinlife banner
            if (document.URL.toLowerCase().indexOf('/piinlife') > 0 && $(window).width() > 768) {
                if ($.trim($('#TOPBanner').find('div:first').html()) == '' || $('#TOPBanner').find('div:first').html() == null) {
                } else {
                    if ($('#TOPBanner').find('div:first a').length > 0) {
                        $('.slide-bn-block').show();
                        $('#TOPBanner').show();
                        $('#newscontent').show();
                    } else {
                        $('.slide-bn-block').hide();
                        $('#TOPBanner').hide();
                        $('#newscontent').hide();
                    }
                }
            }

            $(window).resize(delayFunction(function () {
                if (document.URL.toLowerCase().indexOf('/piinlife') > 0 && $(window).width() > 768) {
                    if ($.trim($('#TOPBanner').find('div:first').html()) == '' || $('#TOPBanner').find('div:first').html() == null) {
                    } else {
                        if ($('#TOPBanner').find('div:first a').length > 0) {
                            $('.slide-bn-block').show();
                            $('#TOPBanner').show();
                            $('#newscontent').show();
                        } else {
                            $('.slide-bn-block').hide();
                            $('#TOPBanner').hide();
                            $('#newscontent').hide();
                        }
                    }
                }
            }, { delay: 300 }));

        });//end of document.ready

        function newsVisible() {
            if ($.trim($('#newscontent div:first').html()) == '' || $('#newscontent div:first').html() == null) {
                $('#newscontent').hide();
                $('#newscontent').parent('#TOPBanner').hide();
                $('#newscontent .close').hide();
            }
            if ($.trim($('#MarketingBNArea div:first').html()) == '' || $('#MarketingBNArea div:first').html() == null) {
                $('#Marketingarea').hide();
            }
        }
        var mailReg = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;
        function PopSubscribe(obj) {
            $(obj).attr('disabled', 'disabled');
            var email = $('#txtPopSubscribeEmail').val();
            var categoryId = '<%= Channel.CategoryId%>';
            if (mailReg.test(email)) {
                Subscribe(email, categoryId);
            } else {
                alert('您的電子信箱格式不符合');
                $(obj).attr('disabled', false);
            }
        }
        function LeftSubscribe() {
            var email = $('#txtSubscribeEmail').val();
            var categoryId = '<%= Channel.CategoryId%>';
            if (mailReg.test(email)) {
                Subscribe(email, categoryId);
            } else {
                alert('您的電子信箱格式不符合');
            }
        }
        function Subscribe(email, categoryId) {
            var postData = { email: email, categoryId: categoryId };
            $.ajax({
                type: "POST",
                url: '<%= ResolveUrl("~/service/ppon/AddEdm")%>',
                data: JSON.stringify(postData),
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    alert(res.Message);
                    $.unblockUI();
                }
            });
        }
        function showPiinlifeSubscription() {
            $.blockUI({
                message: $("#Event_Mail"), css: {
                    backgroundcolor: 'transparent', cursor: 'auto', border: 'none',
                    top: ($(window).height() - 400) / 2 + 'px', left: ($(window).width() - 865) / 2 + 'px'
                }
            });
            $($(".blockUI")[2]).css('background-color', 'transparent');
        }

        function gototop() {
            $('html, body').animate({
                scrollTop: 0
            }, 800);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MC" runat="server">
    <div class="wrap">
        <div id="Div1" class="Button_back_to_top_command" style="display: none;"
            title="返回頂部" onclick="return gototop();">
        </div>
        <aside class="Offers_Sidebar">
            <nav class="SideBar">
                <asp:Repeater runat="server" ID="rptCategory">
                    <HeaderTemplate>
                        <ul class="category">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li runat="server" id="category_all" style="display: block;" class='<%# GetCategoryBgClass(((CategoryDealCount)(Container.DataItem)).CategoryId)%>'>
                            <a id="A1" runat="server" class="category" href='<%# ((CategoryDealCount)(Container.DataItem)).CategoryId != 0 ? string.Format(ResolveUrl(@"~/piinlife/default.aspx?cat={0}"), ((CategoryDealCount)(Container.DataItem)).CategoryId) : ResolveUrl(@"~/piinlife/default.aspx") %>'>
                                <i class="<%# GetCategoryIconClass(((CategoryDealCount)(Container.DataItem)).CategoryId)%>">
                            </i>
                                <p><%# ((CategoryDealCount)(Container.DataItem)).CategoryName%></p></a>
                            <div class='<%# ((CategoryDealCount)(Container.DataItem)).CategoryId.ToString() == CategoryId.ToString() ? "arrow" : ""%>'>
                            </div>
                        </li>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ul>
                    </FooterTemplate>
                </asp:Repeater>
            </nav>
            <nav class="SideBar">
                <asp:Repeater runat="server" ID="rptVisaCategory">
                    <HeaderTemplate>
                        <ul class="category">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li runat="server" id="Li1" style="display: block;" class='<%# GetCategoryBgClass(((CategoryDealCount)(Container.DataItem)).CategoryId)%>'>
                            <a id="A2" runat="server" class="category" href='<%# ((CategoryDealCount)(Container.DataItem)).CategoryId != 0 ? string.Format(ResolveUrl(@"~/piinlife/default.aspx?cat={0}"), ((CategoryDealCount)(Container.DataItem)).CategoryId) : ResolveUrl(@"~/piinlife/default.aspx") %>'>
                                <i class="<%# GetCategoryIconClass(((CategoryDealCount)(Container.DataItem)).CategoryId)%>">
                            </i><p>
                                <%# ((CategoryDealCount)(Container.DataItem)).CategoryName%></p></a>
                            <div class='<%# ((CategoryDealCount)(Container.DataItem)).CategoryId.ToString() == CategoryId.ToString() ? "arrow" : ""%>'>
                            </div>
                        </li>
                    </ItemTemplate>
                    <FooterTemplate>
                        </ul>
                    </FooterTemplate>
                </asp:Repeater>
            </nav>
            <aside class="Theme_Email">
                <h3>
                    訂閱追蹤</h3>
                <div class="Socialconnect">
                    <div class="followFB_Text" style="margin: 0px 10px auto 25px">
                        <a href="https://www.facebook.com/PiinLife" target="_blank" class="fansLink">加入粉絲
                        </a>
                    </div>
                    <div class="followFB" style="height: 31px">
                        <div class="fb-like" data-href="http://www.facebook.com/PiinLife" data-send="false"
                            data-layout="button_count" data-width="90" data-show-faces="true">
                        </div>
                    </div>
                </div>
                <!--Socialconnect-->
                <p style="text-align: center;">
                    <input type="text" id="txtSubscribeEmail" class="Emailarea" style="color: black;" placeholder="請輸入您的email" />
                </p>
                <input type="button" id="btnSubscribe" class="Button" value="訂閱" onclick="LeftSubscribe();" />
                <a href="<%= ResolveUrl("~/newmember/privacy.aspx") %>" class="PrivacyPolicy">隱私權政策</a>
                <h4>
                    <asp:Literal ID="Literal1" runat="server" Visible="false"></asp:Literal></h4>
            </aside>
            <aside>
                <ucR:RandomParagraph ID="RandomParagraph1" runat="server" />
            </aside>
            <aside class="BannerPromo">
                <uc1:Paragraph ID="Paragraph1" runat="server" />
            </aside>
            <aside class="BannerPromo">
                <uc1:Paragraph ID="Paragraph2" runat="server" />
            </aside>
            <aside class="BannerPromo">
                <uc1:Paragraph ID="Paragraph3" runat="server" />
            </aside>
        </aside>
        <section class="OfferList">
            <asp:Repeater ID="rptMultiDeals" runat="server" OnItemDataBound="rptMultiDeals_ItemDataBound">
                <ItemTemplate>
                    <article class="Deal">
                        <div class="text">
                            <h3>
                                <asp:Literal ID="liIcon" runat="server"></asp:Literal>
                            </h3>
                            <h2><asp:Literal ID="liEventName" runat="server"></asp:Literal></h2>
                            <h3 class="Subtitle"><asp:Literal ID="liEventTitle" runat="server"></asp:Literal></h3>
                            <asp:PlaceHolder ID="phInstallmentInfo" runat="server">
                                <h3 class="installment-m">
                                <i class="fa fa-credit-card fa-fw"></i>分期0利率
                                </h3>
                            </asp:PlaceHolder>
                            <section class="View">
                                <ul class="DealView" style="display: none;">
                                    <asp:Repeater ID="rptSubDeals" runat="server">
                                        <ItemTemplate>
                                            <li class="Listitems" style="display: block; overflow: hidden; margin-top: 0px; margin-bottom: 0px; 
                                                padding-top: 0px; padding-bottom: 0px;">
                                                <a class="dealLink" href="<%# string.Format(ResolveUrl(@"~/piinlife/deal.aspx?cat={0}&bid={1}&sub={2}"), CategoryId, ((ViewComboDeal)(Container.DataItem)).MainBusinessHourGuid, ((ViewComboDeal)(Container.DataItem)).BusinessHourGuid)%>">
                                                    <div class="Listitem">
                                                    <div class="Price">$<%# ((ViewComboDeal)(Container.DataItem)).ItemPrice.ToString("F0")%></div>
                                                    <div class="ListTitle"><%# ((ViewComboDeal)(Container.DataItem)).Title%></div>
                                                    </div>
                                                </a>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                                <asp:MultiView id="mvButton" runat="server">
                                    <asp:View runat="server">
                                        <div class="Button DealButton"></div>        
                                    </asp:View>
                                    <asp:View runat="server">
                                        <div class="Button DealButton installment"></div>
                                    </asp:View>
                                </asp:MultiView>
                            </section>
                        </div>
                        <figure class="Dealimg">
                            <a class="dealLink" href="<%# CategoryId != null ? string.Format(ResolveUrl(@"~/piinlife/{1}?cat={0}"), CategoryId, ((KeyValuePair<MultipleMainDealPreview, IList<ViewComboDeal>>)(Container.DataItem)).Key.PponDeal.BusinessHourGuid) : string.Format(ResolveUrl(@"~/piinlife/{0}"), ((KeyValuePair<MultipleMainDealPreview, IList<ViewComboDeal>>)(Container.DataItem)).Key.PponDeal.BusinessHourGuid) %>">
                                <asp:Image ID="img" runat="server" Width="700" />
                            </a>
                        </figure>
                        <asp:Literal ID="liSoldOut" runat="server"></asp:Literal>
                    </article>
                </ItemTemplate>
            </asp:Repeater>
        </section>
    </div>
    <div id="Event_Mail" class="Subscrible_Box SBox_for_pop" style="display: none; width: 865px; height: 400px; position: relative;">
        <div class="left">
            <div class="Mail_Sub_Box">
                <p>
                    這是一個以優惠價格，提供您頂級享受的服務網站。<br />
                    現在就開始訂閱您的城市品味，<br />
                    我們將帶領您展開多樣的專屬體驗！
                </p>
                <br />
                <p>
                    請輸入您的電子信箱&nbsp;&nbsp;<a href="<%= ResolveUrl("~/newmember/privacy.aspx") %>" class="MPrivacyPolicy">隱私權政策</a><br />
                    <input type="text" id="txtPopSubscribeEmail" class="Text_Input" placeholder="請輸入您的email" />
                    <br />
                    <input type="button" onclick="PopSubscribe(this);" value="訂閱品生活" class="Button" />
                    <span id="erEmail" class="Error_Note" style="display: none;">請輸入正確的電子信箱</span> <span
                        id="erCity" class="Error_Note" style="display: none;">您尚未選擇城市</span> <span id="erMsg"
                            class="Error_Note" style="display: none;">請選擇城市並輸入正確的電子信箱</span>
                    <asp:Label ID="lblFail" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div class="right">
            <div class="Deal_Close_Bar">
            </div>
            <div class="Deal_Slideshow_Box">
                <div id="flashbox" class="smallslider">
                    <ul>
                        <asp:Repeater ID="repSubscribeDealContent" runat="server" OnItemDataBound="repSubscribeDealContent_ItemDataBound">
                            <ItemTemplate>
                                <li>
                                    <a href="<%# ResolveUrl("~/piinlife/" + Eval("Key"))%>">
                                        <asp:Image ID="imgDeal" runat="server" Width="420px" Height="233px" />
                                    </a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
            </div>
            <div id="divSliderBottomBand" class="Deal_Slideshow_Bar Deal_SBar_at_Bottom">
            </div>
        </div>
        <div id="blockui-close" style="cursor: pointer;" onclick="$.unblockUI();return false;">
        </div>
    </div>
</asp:Content>

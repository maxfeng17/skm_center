﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Autofac;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.UI;

namespace LunchKingSite.Web.piinlife
{
    public partial class hideal : MemberMasterPage
    {
        public static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region properties

        //public bool BypassSetNowUrl { get; set; }

        public string FacebookAppId
        {
            get
            {
                return config.FacebookApplicationId;
            }
        }

        public ISysConfProvider SystemConfig
        {
            get { return config; }
        }

        public string WebUrl
        {
            get
            {
                //string site = config.SiteUrl;
                //return site.EndsWith("/") ? site : site + "/";
                return WebUtility.GetSiteRoot() + "/";
            }
        }

        public int CurrentRegionId
        {
            get
            {
                int defId = HiDealRegionManager.DealOverviewRegion;
                int regId;

                //第一優先: 判斷 query string  是否有regid
                #region querystring regid 判斷
                if (Request.QueryString["regid"] != null)
                {
                    if (int.TryParse(Request.QueryString["regid"], out regId))
                    {
                        //if (SystemCodeManager.GetSystemCodeListByGroup("HiDealRegion").Find("CodeId", regId) != -1)
                        if (HiDealRegionManager.GetRegionOfShow().Find("CodeId", regId) != -1)
                        {
                            Session[HiDealSession.HiDealRegionId.ToString()] = regId;
                            tbRegionId.Text = regId.ToString();
                        }
                    }
                }
                #endregion

                //tbRegionId取代 Session[LkSiteSession.HiDealRegionId.ToString()]儲存個頁面目前的Region
                //當tbRegionId無值則使用defId，有值則使用tbRegionId.Text
                
                if (tbRegionId.Text == string.Empty)
                {
                    regId = defId;
                    tbRegionId.Text = defId.ToString();
                    Session[HiDealSession.HiDealRegionId.ToString()] = defId;
                }
                else 
                {
                    regId = int.Parse(tbRegionId.Text);
                }

                return regId  ;
            }

            set {
                Session[HiDealSession.HiDealRegionId.ToString()] = value;
                tbRegionId.Text=value.ToString();
            }
        }

        /// <summary>
        /// 將目前網址寫到Session中
        /// </summary>
        protected void SetNowUrlIntoSession()
        {
            string url = Request.Url.AbsoluteUri;
            if (url.IndexOf("member/login") < 0)
            {
              //  Session[LkSiteSession.NowUrl.ToString()] = url;
                Session[HiDealSession.PiinLifeNowUrl.ToString()] = url;  
            }
        }

        #endregion

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Page.Header.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetNowUrlIntoSession();
            Page.Header.DataBind();
            //Page.Title = "品味生活~HiDeal";
            if (!Page.IsPostBack)
            {
                setLoginPanel(Page.User != null && Page.User.Identity.IsAuthenticated);
                // for blockui
                SetCityList(CityManager.Citys.Where(x => x.Code != "com" && x.Code != "SYS").ToList());
                SetMemberData(mp.MemberGet(UserName));
                GetImageContenData();
            }
            //cpa處理
            if (config.EnableCpa)
                RegisterReferrerSource();


            imgCurrentRegion.Src = string.Format("~/Themes/HighDeal/images/index/Current_city_{0}.png", CurrentRegionId);
        }

        private void setLoginPanel(bool isLogin)
        {
            if (isLogin)
            {       
                UserNameTag.Text = this.MemberName;           
            }

            LoginLink.PostBackUrl = ResolveUrl(FormsAuthentication.LoginUrl);
            RegisterLink.PostBackUrl = MemberUtilityCore.RegisterUrl;
            //UserLink.PostBackUrl = WebUrl + "piinlife/member/UserAccount.aspx";
            //OrderLink.PostBackUrl = WebUrl + "piinlife/member/OrderList.aspx";
            //ServiceLink.PostBackUrl = WebUrl + "piinlife/member/service.aspx";
            UserLink.NavigateUrl = WebUrl + "User/UserAccount.aspx";
            OrderLink.NavigateUrl = WebUrl + "piinlife/member/OrderList.aspx";
            ServiceLink.NavigateUrl = WebUrl + config.SiteServiceUrl.TrimStart('/');

            AboutLink.NavigateUrl = WebUrl + "about/us.html";
            TermsLink.NavigateUrl = WebUrl + "Ppon/PrivacyPolicy.aspx";
            PolicyLink.NavigateUrl = WebUrl + "Ppon/PrivacyGoogle.aspx";
            InvoiceLink.NavigateUrl = WebUrl + "piinlife/InvoiceHosting.aspx";
            NewbieGuideLink.NavigateUrl = WebUrl + "Ppon/NewbieGuide.aspx";
            BookingSystemLink.NavigateUrl = WebUrl + "piinlife/member/OrderList.aspx";

            greetingPanel.Visible = isLogin;
            loginPanel.Visible = !isLogin;
        }

        protected void SM1_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
        {
            WebUtility.LogExceptionAnyway(e.Exception);
        }

        protected void lvRegions_OnItemCommand(object sender, ListViewCommandEventArgs e)
        {
            int id;
            int.TryParse(e.CommandArgument.ToString(), out id);
            CurrentRegionId = id;
            Response.Redirect(string.Format(@"~/piinlife/default.aspx?regid={0}", id));
        }

        private void RegisterReferrerSource()
        {
            if (!string.IsNullOrWhiteSpace(Page.Request.QueryString["rsrc"]))
            {
                var campaignId = Page.Request.QueryString["rsrc"];
                // TODO: rewrite
                try
                {
                    var cmp = ServiceLocator.Container.Resolve<CpaService>().GetCampaign(campaignId);
                    if (cmp == null)
                        return;
                    var cookie = CookieManager.NewCookie(LkSiteCookie.ReferrerSourceId.ToString());
                    cookie = Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()];
                    if (cookie == null)
                    {
                        cookie = CookieManager.NewCookie(LkSiteCookie.ReferrerSourceId.ToString());
                        if (cmp.SessionDuration > 0)
                            cookie.Expires = DateTime.Now.AddMinutes(cmp.SessionDuration);
                        cookie.Value = campaignId;
                        Page.Response.SetCookie(cookie);
                    }
                }
                catch (Exception e)
                {
                    WebUtility.LogExceptionAnyway(e);
                }
            }
        }

        #region for blockui

        IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public string ReturnUrl
        {
            get
            {
                string theUrl = (string)Session[LkSiteSession.NowUrl.ToString()];
                return ResolveUrl(string.IsNullOrEmpty(theUrl) ? config.SiteUrl : theUrl);
            }
            set
            {
                Session[LkSiteSession.NowUrl.ToString()] = value;
            }
        }

        /// <summary>
        /// CPA追蹤碼
        /// </summary>
        public string Cpa
        {
            get
            {
                return Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()] != null ? Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()].Value : null;
            }
        }

        protected void lkEneterPiinLife_Click(object sender, EventArgs e)
        {
            SetCookie(1);
            Response.Redirect(ReturnUrl);
        }

        protected void repDealContent_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                KeyValuePair<int, string[]> item = (KeyValuePair<int, string[]>)e.Item.DataItem;
                Image imgDeal = (Image)e.Item.FindControl("imgDeal");

                int limitLength = 28;
                string url = item.Value[0];
                string title = item.Value[1];
                string desc = item.Value[2];
                imgDeal.ImageUrl = url;
                imgDeal.AlternateText = title;
                imgDeal.Attributes.Add("title", desc.Length < limitLength ? desc.Substring(0, desc.Length) : desc.Substring(0, limitLength) + "...");
            }
        }

        public void SetCityList(List<City> list)
        {
            ddlCity.DataSource = list;
            ddlCity.DataBind();
        }

        public void SetMemberData(Member member)
        {
            if (Page.User != null && Page.User.Identity.IsAuthenticated)
            {
                ddlCity.SelectedValue = member.CityId != null ? member.CityId.ToString() : string.Empty;
                txtEmail.Text = member.UserName;
            }
        }

        public void SetImageContent(Dictionary<int, string[]> imgUrls)
        {
            repDealContent.DataSource = imgUrls;
            repDealContent.DataBind();
        }

        protected void SetCookie(int days)
        {
            HttpCookie cookie = CookieManager.NewCookie(LkSiteCookie.PiinLifeSubscriptionCookie.ToString());
            cookie.Expires = DateTime.Now.AddDays(days);
            cookie.Value = "2";
            Response.SetCookie(cookie);
        }

        protected void GetImageContenData()
        {
            int dealCount = 3;
            HiDealCityCollection hdcc = hp.HiDealCityTodayDealsGetByCityId(CurrentRegionId, DateTime.Now);

            if (hdcc.Count > 0)
            {
                Dictionary<int, string[]> imgUrlList = new Dictionary<int, string[]>();
                int count = hdcc.Count < dealCount ? hdcc.Count : dealCount;
                for (int i = 0; i < count; i++)
                {
                    HiDealDeal deal = hp.HiDealDealGet(hdcc[i].HiDealId);
                    string url = ImageFacade.GetHiDealPhoto(deal, MediaType.HiDealPrimarySmallPhoto, false);
                    string[] dealDesc = new string[] { url, deal.Name, deal.PromoLongDesc, deal.Id.ToString() };
                    imgUrlList.Add(deal.Id, dealDesc);
                }
                SetImageContent(imgUrlList);
            }
        }

        #endregion
    }
}

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="deal.aspx.cs" Inherits="LunchKingSite.Web.piinlife.deal" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.WebLib" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Register Src="~/PPon/AvailabilityCtrl.ascx" TagName="AvailabilityCtrl" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" Src="~/UserControls/LoginPanel.ascx" TagName="LoginPanel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Themes/PCweb/HighDeal/MasterPage.css")%>" />
    <link rel="stylesheet" type="text/css" href='<%= ResolveUrl("~/Themes/PCweb/HighDeal/Buttons.css") %>' />
    <link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Themes/PCweb/HighDeal/SingleProduct.css")%>" />
    <link rel="stylesheet" type="text/css" href='<%= ResolveUrl("~/Themes/PCweb/HighDeal/MasterPage_HD.css") %>' />
    <link rel="stylesheet" type="text/css" href='<%= ResolveUrl("~/Themes/PCweb/HighDeal/ppon_HD.css") %>' />
    <link rel="stylesheet" type="text/css" href='<%= ResolveUrl("~/themes/PCweb/css/RDL-L.css")%>' />
    <link rel="stylesheet" type="text/css" href='<%= ResolveUrl("~/themes/PCweb/css/RDL-M.css")%>' />
    <link rel="stylesheet" type="text/css" href='<%= ResolveUrl("~/themes/PCweb/css/RDL-S.css")%>' />
    <link rel="stylesheet" type="text/css" href='<%= ResolveUrl("~/Themes/PCweb/HighDeal/RDL_HD.css")%>' />
    <meta property="fb:app_id" content="<%= FacebookAppId %>" />
    <meta property="og:title" content="<%= FacebookOgTitle %>" />
    <meta property="og:url" content="<%= FacebookOgUrl %>" />
    <meta property="og:image" content="<%= FacebookOgImage %>" />
    <meta property="og:description" content="<%= FacebookOgDescription %>" />
    <link rel="image_src" href="<%= ImageSrcLinkHref %>" />
    <link rel="canonical" href="<%= CanonicalLinkHref %>" />
    <script type="application/ld+json"><%=MicroDataJson%></script>
    <script type="text/javascript" src="../Tools/js/effects/nivoslider.js"></script>
    <script type="text/javascript" src="../Tools/js/osm/OpenLayers.js"></script>
    <script type="text/javascript">
        
        $(function () {

            // set piinlife icon
            $('link[rel*=shortcut]').attr('href', '<%= ResolveUrl("~/Themes/PCweb/HighDeal/favicon.ico") %>');

            //預設書籤位置
            ShowTab(0);

            /*facebook like*/
            addFbLike();

            if (document.documentElement.clientWidth > 1000) {
                $('#divPictureSlider').nivoSlider();
            } else {
                $('#divPictureSlider').nivoSlider({
                    directionNav: false //Next & Prev
                });
            }

            // 若有傳入subBid則顯示開啟該檔；未傳入subBid則顯示開啟第一檔；若僅有一檔則顯示
            if ('<%= SubBusinessHourId != Guid.Empty %>'.toLowerCase() == 'true') {
                var sub = $('.Sidetitle[bid=' + '<%= SubBusinessHourId %>' + ']');
                if (sub != undefined) {
                    SeletedDeal(sub);
                }
            } else {
                SeletedDeal($('.Sidetitle').eq(0));
            }

            // 頁籤內的圖片高度調整
            $('.Detail_box img').css('height','auto');

            if (document.documentElement.clientWidth <= 1000)
            {
                // 若詳細內容內有表格則置換為可浮動的div
                var divList = [];
                $('.Detail_box table').each(function(){
                    var t = $(this);
                    var remove = false;
                    $(this).find('tr').each(function(){
                        if ($(this).find('td').length == 2) {
                            var boxTabs = '';
                            $(this).find('td').each(function(){
                                boxTabs += '<div class="Detail_box_tabs">' + $(this).html() + '</div>';
                            });
                            t.before('<div class="Detail_box">' + boxTabs + '</div>');
                            remove = true;
                        }
                    });
                    if (remove) {
                        $(this).remove();
                    }
                });
            }

            var redirectUrl = '<%=ExpireRedirectUrl%>';
            if (redirectUrl.length > 0 && window.confirm('<%=ExpireRedirectDisplay%>')) { 
                location.href = '<%=ExpireRedirectUrl%>';
            }
        });

        function showmap(o, enableOpenStreetMap) {

            var address = $(o).attr('address');
            var longitude = $(o).attr('longitude');
            var latitude = $(o).attr('latitude');

            var q = latitude != 'Null' && latitude != undefined && latitude != 0 && latitude != '' &&
                    longitude != 'Null' && longitude != undefined && longitude != 0 && longitude != ''
                    ? latitude + ',' + longitude : address;

            if (address.length > 0) {

                $.unblockUI();
                $.blockUI({
                    message: $('#blockMap'),
                    css: {
                        backgroundcolor: 'transparent',
                        border: 'none',
                        top: $(window).height() / 8 + 'px',
                        left: ($(window).width() - 686) / 2 + 'px',
                        cursor: 'pointer',
                        width: '700px',
                        height: '520px'
                    }
                });
                if (enableOpenStreetMap.toLowerCase() == 'true') {

                    // use OpenStreetMap
                    $('#iMap').css('display', 'none');
                    $('#map').empty(); // 因OpenStreetMap會一直append新的地圖，必須清空

                    map = new OpenLayers.Map("map");
                    map.addLayer(new OpenLayers.Layer.OSM());

                    var lonLat = new OpenLayers.LonLat(longitude, latitude)
                          .transform(
                            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
                            map.getProjectionObject() // to Spherical Mercator Projection
                          );
                    var zoom = 16;

                    var markers = new OpenLayers.Layer.Markers("Markers");
                    map.addLayer(markers);
                    markers.addMarker(new OpenLayers.Marker(lonLat));
                    map.setCenter(lonLat, zoom);

                } else {
                    // use GoogleMap with iframe
                    $('#iMap').attr('src', 'https://maps.google.com/maps?f=q&source=s_q&z=16&hl=zh-TW&geocode=&ie=UTF8&iwloc=A&output=embed&q=' + q);
                }

            } else {
                alert("Google Map 查無此地址!!");
            }
        }

        function addFbLike() {
            var fblink = '<%= config.SiteUrl %>/piinlife/<%= FbDealId %>';
            $('.divFbLike').append('<div id="TheFbLike" class="fb-like" href="' + fblink + '" data-send="true" data-share="false" data-layout="button_count" data-show-faces="true" data-action="recommend"></div>');
            //$('.fb-Like').attr('href', window.location.href);
        }
        function activateTabStyle(anchor) {
            $(anchor).closest('ul', this).find('li').removeClass('active');
            $(anchor).closest('li', this).addClass('active');
        }

        function ShowTab(index) {
            $('[id*=TabLinks]').each(function (index, item) {
                $(this).removeClass("active");
            });
            $('#TabLinks' + index).addClass("active");
            if (!$('.Detail_tab_buttons_m').eq(index).find('.ui_icon_slide').hasClass('ui_icon_slide_active')) {
                ShowTabContent($('.Detail_tab_buttons_m').eq(index), 0);
            }
        }

        function GoToTab(index) {
            ShowTab(index);
            $(window).scrollTop($("#ulTab").offset().top);
        }

        function GoToLastTab() {
            GoToTab($('[id*=TabLinks]').length - 1);
        }

        function SeletedDeal(obj) {
            var ultitle = $(obj).parent();
            if (ultitle.hasClass('active')) {
                $(ultitle).find('.dealContent').slideUp(500);
                $(ultitle).removeClass('active');
                $(ultitle).find('.ui_icon_slide_white').removeClass('ui_icon_slide_white_active');
            }else {
                $('.SinglePackage.active').find('.dealContent').slideUp(500);
                $('.ui_icon_slide_white').removeClass('ui_icon_slide_white_active');
                $('.SinglePackage.active').removeClass('active');
                $(ultitle).find('.dealContent').slideDown(500);
                ultitle.addClass('active');
                $(ultitle).find('.ui_icon_slide_white').addClass('ui_icon_slide_white_active');
            }
        }

        function ShowTabContent(obj, speed) {
            var tab_content = $(obj).next('.info_tabs');
            var tab = $(obj).find('.ui_icon_slide');
            if (tab.hasClass('ui_icon_slide_active')) {
                tab_content.slideUp(speed);
                tab.removeClass('ui_icon_slide_active')
            }else {
                if ($(window).width() < 1000) {
                    $('.info_tabs').slideUp(speed);
                }else {
                    $('.info_tabs').not($('.Location')).slideUp(speed);
                }
                $('.ui_icon_slide').removeClass('ui_icon_slide_active')
                tab_content.slideDown(speed);
                tab.addClass('ui_icon_slide_active')
            }
        }

        function OpenSharePopWindow(shareUrl, shareType) {
            if ('<%= Page.User.Identity.IsAuthenticated %>' == 'False') {
                $("#hidShareType").val(shareType);
                $.blockUI({ message: $('.share-popW'), css: { border: 'none', background: 'transparent', width: '0', top: ($(window).height()) / 4 + 'px', left: ((($(window).width() / 2) - ($('.share-popW').width() / 2)) - 10) + 'px' } });
            }
            else {
                window.open(shareUrl);
            }
        }

        function ShareWithOutLogin(shareContent, shareUrl) {
            var shareType = $("#hidShareType").val();
            if (shareType == "fb") {
                RedirectUrl("http://www.facebook.com/share.php?u=" + shareUrl);
            }
            else if (shareType == "line") {
                RedirectUrl("http://line.me/R/msg/text/?" + encodeURI(shareContent + " " + shareUrl));
            }
            else {
                $.unblockUI();
            }
        }

        function RedirectLoginPage(loginUrl) {
            RedirectUrl(loginUrl);
        }

        function RedirectUrl(url) {
            var is_safari_or_uiwebview = /(iPhone|iPod|iPad).*AppleWebKit/i.test(navigator.userAgent);
            if (is_safari_or_uiwebview) {
                window.open(url);
            } else {
                location.href = url;
            }
        }

        function CloseSharePopWindow() {
            $.unblockUI();
        }

       

    </script>
    <style type="text/css">
        .style1
        {
            width: 473px;
        }
        section.Main
        {
            height: 100%;
            width: 100%;
        }
        
        #divPictureSlider img
        {
            position: absolute;
            left: 0px;
            top: 0px;
            visibility: hidden;
        }
        .nivo-controlNav
        {
            position: absolute;
            bottom: 0px;
            right: 0%;
            margin-left: -50px;
            height: 20px;
            z-index: 10;
        }
        #divSliderBottomBandDeal
        {
            background: -moz-linear-gradient(left, #000000, #AAAAAA);
            background: -webkit-gradient(linear, left top, right top, from(#000000), to(#AAAAAA));
            filter: progid:DXImageTransform.Microsoft.Gradient( StartColorStr='#000000', EndColorStr='#AAAAAA', GradientType=1);
            background-color: black;
            height: 20px;
            left: 0;
            opacity: 0.6;
            position: absolute;
            bottom: 0;
            width: 100%;
            z-index: 1;
        }
        
        a.nivo-control
        {
            position: relative;
            margin-left: 4px;
            margin-right: 4px;
            background: url(<%=VirtualPathUtility.ToAbsolute(@"~/Themes/HighDeal/images/SingleProduct/Single_Product_Split.png")%>) no-repeat;
            background-position: -687px -17px;
            height: 16px;
            width: 16px;
            cursor: pointer;
            display: block;
            float: left;
            text-indent: -99999px;
            z-index: 99;
        }
        .nivo-controlNav a.active
        {
            background-position: -687px -39px;
        }
        
        /* nivo slider: the div containing "previous" and "next"  link*/
        div.nivo-directionNav
        {
            height: 48px;
            width: 100%;
            position: absolute;
            top: 150px;
            z-index: 21;
        }
        
        /*nivo slider "previous" link */
        a.nivo-prevNav
        {
            background: url(<%=VirtualPathUtility.ToAbsolute(@"~/Themes/HighDeal/images/SingleProduct/Single_Product_Split.png")%>) no-repeat;
            background-position: -567px -18px;
            text-indent: -99999px;
            width: 49px;
            height: 48px;
            float: left;
            z-index: 25;
        }
        a.nivo-prevNav:hover
        {
            background-position: -724px -18px;
            cursor: pointer;
        }
        
        /*nivo slider "next" link */
        a.nivo-nextNav
        {
            background: url(<%=VirtualPathUtility.ToAbsolute(@"~/Themes/HighDeal/images/SingleProduct/Single_Product_Split.png")%>) no-repeat;
            background-position: -628px -18px;
            text-indent: -99999px;
            width: 49px;
            height: 48px;
            float: right;
            z-index: 25;
        }
        a.nivo-nextNav:hover
        {
            background-position: -786px -18px;
            cursor: pointer;
        }
        
        .nivo-slice
        {
            display: block;
            position: absolute;
            z-index: 0;
        }
        #TOPBanner
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="wrap">
        <div class="SingleProductSplit">
            <section class="Main">
				<div class="Category_bar">
					<div class="Category_Container">
                        <h3>
                            <span><a href='<%= ResolveUrl("~/piinlife/default.aspx") + (CategoryId != 0 ? "?cat=" + CategoryId : string.Empty) %>'>
                            <%= CategoryName %></a></span>
                        </h3>
                        <div class="right_arrow">&gt;</div>
                        <h3>
                            <span><asp:Literal ID="liBreadDealName" runat="server"></asp:Literal></span>
                        </h3>
                    </div>
				</div>
				<hgroup class="Mainbox Whitebox">
					<div class="Title">
						<div class="City" style="position:relative">
                            <asp:Literal ID="liSpecialCategoryTag" runat="server" ></asp:Literal>
                            <div class="m-collect" style="display:none;">
							    <a href="#">收藏</a>
						    </div>
                        </div>
                        <div class="LikeArea">
                            <span class="divFbLike" style="float: right; width: 130px;"></span>
                        </div>
						<h1 class="Name">
                            <asp:Literal ID="lDealName" runat="server"></asp:Literal></h1>
                        <h2 class="Tagline">
                            <asp:Literal ID="ldesc" runat="server"></asp:Literal></h2>
					</div>
					<figure class="img">
						<%--圖片輪播--%>
                        <div id="divPictureSlider" class="Slides-container">
                            <asp:Repeater ID="rptImage" runat="server">
                                <ItemTemplate>
                                    <img  id="prodImg" src='<%# Eval("Url") %>' alt=""/>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div id="divSliderBottomBandDeal">
                        </div>
					</figure>
				</hgroup>
			</section>

            <div class="mobile_btn">
                <h2 class="Time_alert">結束販售時間:<asp:Literal ID="lCloseTime_m" runat="server"></asp:Literal>
                </h2>
                <ul class="mobile_share">
                    <li>
						<div class="share_text">
							分享送
							<span class="bigtext">500元!</span>
						</div>
					</li>
					<li class="m-fbBtn">
						 <a onclick='return OpenSharePopWindow("<%=ResolveUrl("~/ppon/fbshare_proxy.aspx?type=facebook&bid=") + BusinessHourGuid %>", "fb");'>
                        </a>
					</li>
					<li class="m-lineBtn">
						<a onclick='return OpenSharePopWindow("<%=ResolveUrl("~/ppon/fbshare_proxy.aspx?type=line&bid=") + BusinessHourGuid + "&itemname=" + Server.UrlEncode(AppTitle)%>", "line");'>
                        </a>
					</li>
					<li>
						<p>親友用你分享的連結首購並核銷成功，就送你500元！</p>
					</li>
					<li>
						<a target="_blank" href="<%=ResolveUrl("~/Ppon/NewbieGuide.aspx")%>" class="share-mobile-link">詳見常見問題</a>
					</li>
                </ul>
            </div>

            <div class="pad_share">
				<div class="share_text">
					分享送
					<span class="bigtext">500元!</span>
				</div>
				<div class="share_btn_box">
                    <a class="fb_btn" onclick="fbShareWindow = window.open(this.href, 'fbShareWindow', 'width=630,height=360','left=' + fbJumpWindowLeft + ',top=' + fbJumpWindowTop);ShareFB();" style="cursor: pointer;"></a>
                    <a class="link_btn" onclick="ShareLink();" style="cursor: pointer;"></a>
				</div>
				<h2 class="Time_alert">結束販售時間:<asp:Literal ID="lCloseTime_pad" runat="server"></asp:Literal></h2>
			</div>
            

            <aside class="SideBarRight">
                <h2 class="Time_alert">
                    結束販售時間:<asp:Literal ID="lCloseTime" runat="server"></asp:Literal></h2>
                <div class="Packages Sidebar_item Whitebox">
                    <asp:Repeater ID="rptDeals" runat="server">
                        <ItemTemplate>
                            <ul class="SinglePackage">
                                <li class="Sidetitle" onclick="SeletedDeal(this);" bid="<%# ((KeyValuePair<IViewPponDeal, string>)(Container.DataItem)).Key.BusinessHourGuid %>">
                                    <%# ((KeyValuePair<IViewPponDeal, string>)(Container.DataItem)).Key.OrderedQuantity >= ((KeyValuePair<IViewPponDeal, string>)(Container.DataItem)).Key.OrderTotalLimit ? "<div class='SoldOut'></div>" : string.Empty %>
                                    <span class="ui_icon_slide_white"></span>
                                    <div class="SubText Name"><%# ((KeyValuePair<IViewPponDeal, string>)(Container.DataItem)).Value %></div>
                                    <div class="Cost">$<%# ((KeyValuePair<IViewPponDeal, string>)(Container.DataItem)).Key.ItemPrice.ToString("N0") %></div>
                                    <div class="Cost_decoration">
                                        <span class="fixed_price">$<%# ((KeyValuePair<IViewPponDeal, string>)(Container.DataItem)).Key.ItemOrigPrice.ToString("N0") %></span>
                                        <%# GetDiscountString(Helper.IsFlagSet(((KeyValuePair<IViewPponDeal, string>)(Container.DataItem)).Key.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), ((KeyValuePair<IViewPponDeal, string>)(Container.DataItem)).Key.ItemPrice, ((KeyValuePair<IViewPponDeal, string>)(Container.DataItem)).Key.ItemOrigPrice) %>
                                    </div>
                                </li>
                                <li class="desc" style="display: list-item;">
                                    <div class="dealContent" style="display: none;">
                                        <div class="descTop"></div>
                                            <div class="descContent">
                                                <p class="Detail_description">
                                                    <%# GetDealContentTabList(((KeyValuePair<IViewPponDeal, string>)(Container.DataItem)).Key).FirstOrDefault().Value %>
                                                </p>
                                                <p class="Detail_Link">
                                                    <a href="javascript:GoToTab(0);">詳細說明</a>
                                                </p>
                                                <p class="Terms_Link" <%# EnableGrossMarginRestrictions && 
                                                                          ((GrossProfit * 100) < MinGross) && 
                                                                          ((((KeyValuePair<IViewPponDeal, string>)(Container.DataItem)).Key.BusinessHourStatus & (int) BusinessHourStatus.LowGrossMarginAllowedDiscount) == 0) 
                                                                          ? "style='display:none'" : string.Empty %> >購買即表示同意本商品的
                                                    <a href="javascript:GoToLastTab();">權益說明</a>內容。</p>
                                                <div class="Cost_decoration">
                                                    <span class="fixed_price">$<%# ((KeyValuePair<IViewPponDeal, string>)(Container.DataItem)).Key.ItemPrice.ToString("N0") %></span>
                                                    <%# ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(((KeyValuePair<IViewPponDeal, string>)(Container.DataItem)).Key.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), ((KeyValuePair<IViewPponDeal, string>)(Container.DataItem)).Key.ItemPrice, ((KeyValuePair<IViewPponDeal, string>)(Container.DataItem)).Key.ItemOrigPrice) %>折
                                                    <span class="CostDown_before">$<%# ((KeyValuePair<IViewPponDeal, string>)(Container.DataItem)).Key.ItemOrigPrice.ToString("N0") %></span>
                                                </div>
                                                <%# GetBuyButtonClassName(((KeyValuePair<IViewPponDeal, string>)Container.DataItem).Key)  %>
                                                <div class="clearfix"></div>
                                                <asp:PlaceHolder ID="phInstallmentInfo" runat="server">
						                            <div class="installment_Limited">
							                            <i class="fa fa-credit-card fa-fw"></i>分期0利率
						                            </div>
                                                </asp:PlaceHolder>
                                                <div class="DealPrice_CouponLimited" >  <%#  EnableGrossMarginRestrictions && 
                                                                                             ((GrossProfit * 100) < MinGross) && 
                                                                                             ((((KeyValuePair<IViewPponDeal, string>)(Container.DataItem)).Key.BusinessHourStatus & (int) BusinessHourStatus.LowGrossMarginAllowedDiscount) == 0) 
                                                                                             ? "優惠商品不適用折價券" : "&nbsp;" %></div>
                                            </div>
                                        <div class="descBottom"></div>
                                    </div>
                                </li>
                            </ul>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </aside>
            <input id="HfBusinessHourId" type="hidden" value="<%=BusinessHourGuid%>" />
            <div class="Main web_share">
				<div class="share_text">
					分享送
					<span class="bigtext">500元!</span>
				</div>
                <div class="share_btn_box">    
                    <a class="fb_btn" onclick="fbShareWindow = window.open(this.href, 'fbShareWindow', 'width=630,height=360','left=' + fbJumpWindowLeft + ',top=' + fbJumpWindowTop);ShareFB();" style="cursor: pointer;"></a>
                    <a class="link_btn" onclick="ShareLink();" style="cursor: pointer;"></a>

                </div>
			</div>

            <div style="display: none">
                <%=DealArgs%>
            </div>

            <section class="Main">
                <article class="More_Detail">
                    <ul class="Detail_tab_buttons" id="ulTab">
                        <asp:Repeater runat="server" ID="rpTabLinks">
                            <ItemTemplate>
                                <li class="" id="TabLinks<%# Container.ItemIndex %>" onclick="ShowTab(<%# Container.ItemIndex %>);" style="cursor: pointer;">
                                    <a><%# Eval("TabTitle") %></a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <asp:Repeater runat="server" ID="rpTabContents">
                            <ItemTemplate>
                                <div class="Detail_tab_buttons_m" onclick="ShowTabContent(this, 500);">
			                        <%# Eval("TabTitle") %>
			                        <span class="ui_icon_slide"></span>
		                        </div>
                                <article id='<%#"tabContent" + Container.ItemIndex %>' class="info_tabs Mainbox Whitebox">
                                    <div class="Content">
                                        <div class="Detail_box">
                                            <%# Eval("Context") %>
                                        </div>
                                        <div class="Detail_info">
                                        </div>
                                        <div style='<%# Eval("TabTitle") == LunchKingSite.I18N.Phrase.PiinlifeIntroductionTitle ? "" : "display:none" %>'>
                                            <p class="editorial">品生活優惠憑證使用條款</p>
                                            <ul class="Detail_rule" style="list-style:decimal outside;padding-left:1.5em;">
                                                <li>此憑證由等值購物金兌換之</li>
                                                <li>本憑證不得與其他優惠合併使用，且恕無法兌換現金</li>
                                                <li>好康憑證不限本人使用，使用時店家將會核對憑證編號，一組編號限用一次</li>
                                                <li>信託期間：發售日起一年內</li>
                                                <li>本商品(服務)禮券所收取之金額，已存入發行人於台新銀行開立之信託專戶，專款專用。所稱專用，係指供發行人履行交付商品或提供服務義務使用<a href=' https://www.17life.com/newmember/privacypolicy.aspx#restrictions' target='_blank'><span style='color:#0000cd;'>查看好康憑證使用條款</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </article>
                            </ItemTemplate>
                        </asp:Repeater>
                </article>
            </section>
            <asp:Panel ID="divAddressInfo" runat="server" Visible="false">
                <aside class="SideBarRight">
                <div class="Detail_tab_buttons_m" onclick="ShowTabContent(this, 500);">
		            位置資訊
		            <span class="ui_icon_slide"></span>
	            </div>
                <div class="Packages Sidebar_item Whitebox Location info_tabs">
		            <h2 class="icon">位置資訊</h2>
                    <asp:Repeater ID="rpt_Location" runat="server" OnItemDataBound="rpt_LocationBinding">
                        <ItemTemplate>
                            <h2 class="Map_Name">
                                <asp:Literal ID="lit_L_StoreName" runat="server"></asp:Literal></h2>
                            <asp:Literal ID="lit_L_Map" runat="server"></asp:Literal>
                            <p>
                                <asp:Literal ID="lit_L_Contents" runat="server"></asp:Literal>
                            </p>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </aside>
            </asp:Panel>
        </div>
    </div>
    <div class="SingleProductSplit_UpWindow" id="blockMap" style="display: none">
        <div class="UpWindowClose" onclick=" $.unblockUI();" style="margin-top: -37px;">
        </div>
        <section class="Background">
            <div id="map" style="width: 700px; height: 520px;">
                <iframe id="iMap" style="width: 100%; height: 100%;"></iframe>
            </div>
        </section>
    </div>
    <input id="hidShareType" type="hidden" value="fb" />
    
    <div class="share-popW" style="display: none">
        <div class="share-popZone">
            <h3>分享送500元!</h3>
            <hr>
            <p>
                親友用你分享的連結首購並核銷成功，
                <br>
                就送你500元！
            </p>
            <div class="text-center">
                <input onclick='RedirectLoginPage("<%= config.SiteUrl.Replace("http://", "https://").Replace("/lksite", "").Replace("/releasebranch", "") + FormsAuthentication.LoginUrl %>    ")'
                    type="submit" class="btn btn-large btn-primary m-btn100" value="登入分享賺500">
                <input onclick='ShareWithOutLogin("<%= AppTitle%>", "<%=string.Format(@"{0}/{1}/{2}",config.SiteUrl, PponCityGroup.DefaultPponCityGroup.Piinlife.CityId, BusinessHourGuid) %>    ")'
                    type="submit" class="btn btn-large m-btn100" value="純分享">
            </div>
        </div>
        <div class="share-popX" onclick="return CloseSharePopWindow();"></div>
    </div>

    <div id="Referral" style="display: none">
        <div id="ReturnClose" style="cursor: pointer; margin-top: 3px; margin-right: 3px"
            onclick="$.unblockUI();return false;">
        </div>
        <div id="ReferralContent">
            <div class="Referralinvite">
                <h1>邀請親友好康有獎</h1>
            </div>
            <div id="ReferralTitle">
                <span class="icon-smile-o-2"></span>
                <p>以下是您的專屬邀請連結</p>
            </div>
            <!--ReferralTitle-->
            <div id="ReferralLinkarea">
                <asp:TextBox ID="tbx_ReferralShare" ClientIDMode="Static" runat="server" ReadOnly="true"></asp:TextBox>
            </div>
            <div id="ReferralStep" style="text-align: left;">
                <li>
                    <span class="list-step">步驟1</span>
                    複製上面的邀請連結
                </li>
                <li>
                    <span class="list-step">步驟2</span>
                    把連結傳給好友
                </li>
                <li>
                    <span class="list-step">步驟3</span>
                    親友點連結後完成首次購買並核銷，宅配商品過鑑賞期且無退貨
                </li>
                <li>
                    <span class="list-step">步驟4</span>
                    您拿到推薦獎勵，折價券500元
                            <span class="list-warning">(請注意使用期限)</span>
                </li>
            </div>

            <div id="Referralexplain">
                <li>您可使用下圖fb分享，或是複製分享連結並轉貼，</li>
                <li>
                    <span class="list-point">邀請次數無上限～</span>
                    分享越多賺越多喔！
                </li>
                <li>
                    <span class="list-point">訂單金額50元(含)以下恕不適用此折價券贈送活動，詳見<a target="_blank" href="<%=ResolveUrl("~/Ppon/NewbieGuide.aspx")%>">常見問題</a>
                    </span>
                </li>
            </div>

            <div id="referral_FB_area">
                <a onclick="fbShareWindow = window.open(this.href, 'fbShareWindow', 'width=630,height=360','left=' + fbJumpWindowLeft + ',top=' + fbJumpWindowTop);ShareFB();" style="cursor: pointer;">
                    <img src="<%= ResolveUrl("~/Themes/PCweb/images/FB_Btn.png")%>" />
                </a>
            </div>
        </div>
    </div>
    <div id="newpoplogin" style="display: none">
       <uc1:LoginPanel ID="LoginPanel1" runat="server" />
    </div>
</asp:Content>

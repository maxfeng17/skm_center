﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.Web.piinlife.member;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace LunchKingSite.Web.piinlife
{
    public partial class HiDealList : BasePage, IHiDealDefaultView
    {
        #region event

        public event EventHandler<DataEventArgs<string[]>> AddSubscript;

        #endregion event

        #region Property

        private HiDealDefaultPresenter _presenter;

        public HiDealDefaultPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        private static ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public ISysConfProvider SystemConfig
        {
            get
            {
                return cp;
            }
        }

        protected bool IsVisa2013
        {
            get
            {
                return cp.IsVisa2013;
            }
        }

        public string CityName { get; set; }

        public int CityId
        {
            get
            {
                return ((hideal)this.Master).CurrentRegionId;
            }
        }

        public string CategoryId
        {
            get
            {
                if (Request["category"] != null)
                    return Request["category"];
                else
                    return "0";
            }
        }

        public int? SpecialId
        {
            get
            {
                int specialId;
                if (int.TryParse(Request["special"], out specialId))
                {
                    return specialId;
                }
                return null;
            }
        }

        public HiDealDefaultViewPageMode PageMode
        {
            get
            {
                if (Request["mode"] != null)
                {
                    if (Request["mode"].Trim() == "preview")
                    {
                        return HiDealDefaultViewPageMode.Preview;
                    }
                }
                return HiDealDefaultViewPageMode.General;
            }
        }

        public Guid DealGuid
        {
            get
            {
                try
                {
                    if (Request["dealgid"] != null)
                    {
                        return new Guid(Request["dealgid"].Trim());
                    }
                    return Guid.Empty;
                }
                catch (Exception)
                {
                    return Guid.Empty;
                }
            }
        }

        /// <summary>
        /// CPA追蹤碼
        /// </summary>
        public string ReferrerSourceId
        {
            get
            {
                return Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()] != null ? Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()].Value : null;
            }
        }

        public bool ShowEventEmail { get; set; }

        public string GoogleApiKey
        {
            get
            {
                return cp.GoogleMapsAPIKey;
            }
        }

        public string AndroidUserAgent
        {
            get
            {
                return cp.AndroidUserAgent;
            }
        }

        public string iOSUserAgent
        {
            get
            {
                return cp.iOSUserAgent;
            }
        }

        #endregion Property

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckEventEmail();
                if (Request["category"] != null)
                {
                    Session[HiDealSession.CategoryId.ToString()] = int.Parse(Request["category"]);
                    string cName = SystemCodeManager.GetHiDealCatgName(int.Parse(Request["category"]));
                }
            }
            this.Presenter.OnViewLoaded();
        }

        protected void RandomPponNewsInit(object sender, EventArgs e)
        {
            pponnews.CityId = CityId;
            pponnews.Type = RandomCmsType.PiinLifeRandomCms;
        }

        #region Presenter Call

        private string GetDealLink(ViewHiDeal deal)
        {
            if (this.SpecialId == null)
            {
                return string.Format("deal.aspx?regid={0}&did={1}", this.CityId, deal.DealId);
            }
            else
            {
                return string.Format("deal.aspx?special={0}&did={1}", this.SpecialId, deal.DealId);
            }
        }

        private string GetProductLink(HiDealProduct product)
        {
            if (this.SpecialId == null)
            {
                return string.Format("deal.aspx?regid={0}&pid={1}", this.CityId, product.Id);
            }
            else
            {
                return string.Format("deal.aspx?special={0}&pid={1}", this.SpecialId, product.Id);
            }
        }

        public void SetContent(ViewHiDealCollection MainDeals, ViewHiDealCollection LastDeals)
        {
            #region MainDeal

            string strMain = string.Empty;

            foreach (ViewHiDeal mDeal in MainDeals)
            {
                #region 設定是否賣完的變數賣完

                bool isSoldOut = HiDealDealManager.DealProductIsSoldOut(mDeal.DealId);
                string viewOnClick = string.Empty;
                if (isSoldOut == false)
                {
                    //viewOnClick = @" onclick=""location.href='" + GetDealLink(mDeal) + @"'"" ";
                    viewOnClick = @" onclick=""window.open('" + GetDealLink(mDeal) + @"');return false;"" ";
                }

                #endregion 設定是否賣完的變數賣完

                #region 取得圖片

                long dealModifyTime = (mDeal.DealModifyTime == null ? DateTime.MinValue : mDeal.DealModifyTime.Value).Ticks;
                string imgPath = ImageFacade.GetHiDealPhoto(mDeal, MediaType.HiDealPrimaryBigPhoto, true);

                if (!string.IsNullOrEmpty(mDeal.PrimaryBigPicture))
                {
                    string[] imgPathArray = mDeal.PrimaryBigPicture.Split('.');
                    //避免舊資料沒有 此類型的圖檔 判斷式
                    if (ImageUtility.isFileExit(HttpContext.Current.Server.MapPath("~/media/" + imgPathArray[0] + "_" + mDeal.DealModifyTime.GetValueOrDefault().ToString("yyyyMMddHHmmss") + "." + imgPathArray[1])))
                    {
                        imgPath = imgPath.Replace("HidealBigPic", "HidealBigPic_" + mDeal.DealModifyTime.Value.ToString("yyyyMMddHHmmss"));
                    }
                }
                imgPath += "?" + dealModifyTime;

                #endregion 取得圖片

                #region 組合Deal Content

                //預設顯示的區域名稱
                string dealCityName = mDeal.CityName;
                //總覽，則顯示除了總覽以外第一個區域名稱
                if (mDeal.CityCodeId == HiDealRegionManager.DealOverviewRegion)
                {
                    HiDealRegionCollection regions = HiDealDealManager.HiDealRegionGetListByDealId(mDeal.DealId);
                    var tmpRegions = regions.Where(x => !x.CodeId.Equals(HiDealRegionManager.DealOverviewRegion)).ToList();
                    if (tmpRegions.Count > 0)
                    {
                        dealCityName = tmpRegions.First().CodeName;
                    }
                    else
                    {
                        dealCityName = string.Empty;
                    }
                }
                //主檔次的顯示
                strMain += @"<article class=""Deal"">

<div class=""text"">
<h3><span style=""float:left; margin-top:1px; margin-right:10px;"">" + dealCityName + @"</span>";

                #region IsVisa

                if (this.IsVisa2013 == false)
                {
                    if (mDeal.IsVisa != null && mDeal.IsVisa.Value)
                    {
                        strMain += @"<p class=""VISA""></p>";
                    }
                }
                else
                {
                    if (mDeal.VisaType == VisaDealType.VisaPrivate)
                    {
                        strMain += @"<p class=""VISA""></p>";
                    }
                    else if (mDeal.VisaType == VisaDealType.VisaPriority)
                    {
                        strMain += @"<p class=""VISA_sp""></p>";
                    }
                }

                #endregion IsVisa

                //活動專區Tag  [system_code] where [code_group] = 'HiDealCatg' and code_id =19
                if (HiDealDealManager.IsCategorySpecialTag(mDeal.DealId, 19))
                {
                    strMain += GetEventTagHtml();
                }

                //即買即用Tag [system_code] where [code_group] = 'HiDealCatg' and code_id =20
                if (HiDealDealManager.IsCategorySpecialTag(mDeal.DealId, 20))
                {
                    strMain += GetBuyUseTagHtml();
                }

                //情人專屬Tag [system_code] where [code_group] = 'HiDealCatg' and code_id =21
                if (HiDealDealManager.IsCategorySpecialTag(mDeal.DealId, 21))
                {
                    strMain += GetValentinesTagHtml();
                }

                //父親節精選Tag [system_code] where [code_group] = 'HiDealCatg' and code_id =22
                if (HiDealDealManager.IsCategorySpecialTag(mDeal.DealId, 22))
                {
                    strMain += GetFatherTagHtml();
                }

                //賞悅中秋Tag [system_code] where [code_group] = 'HiDealCatg' and code_id =23
                if (HiDealDealManager.IsCategorySpecialTag(mDeal.DealId, 23))
                {
                    strMain += GetMoonTagHtml();
                }

                strMain += @"</h3>
<h2>" + mDeal.DealName + @"</h2>
<h3 class=""Subtitle"">" + mDeal.DealPromoLongDesc + @"</h3>
<section class=""View"">
" + GetMainProductContent(mDeal.DealId) + @"
<div id=""sButton" + mDeal.DealId.ToString() + @""" class=""Button DealButton"" " + viewOnClick + @" ></div>
</section><!--View-->
</div>
<figure class=""Dealimg""><a href='" + GetDealLink(mDeal) + @"' target=""_blank""><img src=""" + imgPath + @""" width=""700"" height=""280""></a></figure>";

                #region Is SoldOut

                if (isSoldOut)
                {
                    strMain += @"<div class=""image soldout"">

<div class=""offer_mosaic_image largeSoldout"" width=""700"" height=""280""></div>

<div class=""overlay""></div>
</div>";
                }

                #endregion Is SoldOut

                strMain += "</article>";

                #endregion 組合Deal Content
            }
            lblMainDeal.Text = strMain;

            #endregion MainDeal

            #region SubDeal

            string strSub = string.Empty;

            int i = 0; //要用來判斷 odd or even
            foreach (ViewHiDeal sDeal in LastDeals)
            {
                #region 設定是否賣完的變數賣完

                bool isSoldOut = HiDealDealManager.DealProductIsSoldOut(sDeal.DealId);
                string viewOnClick = "";
                if (isSoldOut == false)
                {
                    //viewOnClick = @" onclick=""location.href='" + GetDealLink(sDeal) + @"'"" ";
                    viewOnClick = @" onclick=""window.open('" + GetDealLink(sDeal) + @"');return false;"" ";
                }

                #endregion 設定是否賣完的變數賣完

                #region 取得圖片

                string imgPath = ImageFacade.GetHiDealPhoto(sDeal, MediaType.HiDealPrimarySmallPhoto);

                #endregion 取得圖片

                #region 組合Deal Content

                i++;

                string dealCityName = sDeal.CityName;
                //總覽，則顯示除了總覽以外第一個區域名稱
                if (sDeal.CityCodeId == HiDealRegionManager.DealOverviewRegion)
                {
                    HiDealRegionCollection regions = HiDealDealManager.HiDealRegionGetListByDealId(sDeal.DealId);
                    var tmpRegions = regions.Where(x => !x.CodeId.Equals(HiDealRegionManager.DealOverviewRegion)).ToList();
                    if (tmpRegions.Count > 0)
                    {
                        dealCityName = tmpRegions.First().CodeName;
                    }
                    else
                    {
                        dealCityName = string.Empty;
                    }
                }
                //子檔次的顯示
                strSub += @"<article class='SideDeal " + (i % 2 == 0 ? "even" : "odd") + @"'>
<div class=""SideDealtext"">
<h3><span style=""float:left; margin-top:1px; margin-right:10px;"">" + dealCityName + "</span>";

                #region IsVisa

                if (this.IsVisa2013 == false)
                {
                    if (sDeal.IsVisa != null && sDeal.IsVisa.Value)
                    {
                        strSub += @"<p class=""VISA""></p>";
                    }
                }
                else
                {
                    if (sDeal.VisaType == VisaDealType.VisaPrivate)
                    {
                        strSub += @"<p class=""VISA""></p>";
                    }
                    else if (sDeal.VisaType == VisaDealType.VisaPriority)
                    {
                        strSub += @"<p class=""VISA_sp""></p>";
                    }
                }

                #endregion IsVisa

                //活動專區Tag  [system_code] where [code_group] = 'HiDealCatg' and code_id =19
                if (HiDealDealManager.IsCategorySpecialTag(sDeal.DealId, 19))
                {
                    strSub += GetEventTagHtml();
                }

                //即買即用Tag [system_code] where [code_group] = 'HiDealCatg' and code_id =20
                if (HiDealDealManager.IsCategorySpecialTag(sDeal.DealId, 20))
                {
                    strSub += GetBuyUseTagHtml();
                }

                //情人專屬Tag [system_code] where [code_group] = 'HiDealCatg' and code_id =21
                if (HiDealDealManager.IsCategorySpecialTag(sDeal.DealId, 21))
                {
                    strSub += GetValentinesTagHtml();
                }

                //父親節精選Tag [system_code] where [code_group] = 'HiDealCatg' and code_id =22
                if (HiDealDealManager.IsCategorySpecialTag(sDeal.DealId, 22))
                {
                    strSub += GetFatherTagHtml();
                }

                //賞悅中秋Tag [system_code] where [code_group] = 'HiDealCatg' and code_id =23
                if (HiDealDealManager.IsCategorySpecialTag(sDeal.DealId, 23))
                {
                    strSub += GetMoonTagHtml();
                }

                strSub += @"</h3>
<h2>" + sDeal.DealName + @"</h2>
<h3 class=""Subtitle"">" + sDeal.DealPromoShortDesc + @"</h3>
<section class=""SideDealView"">
" + GetSubProductContent(sDeal.DealId) + @"
<div class=""Button DealButton"" id=""ssButton" + sDeal.DealId.ToString() + @""" " + viewOnClick + @" ></div>
</section><!--SideDealView-->
</div>
<figure class=""SideDealimg""><a href='" + GetDealLink(sDeal) + @"' target=""_blank""><img src=""" + imgPath + @""" width=""345"" height=""190""></a></figure>";

                #region Isoldout

                if (isSoldOut)
                {
                    strSub += @"<div class=""image_s Soldout"">

<div class=""offer_mosaic_image smallSoldout"" width=""345"" height=""190""></div>

<div class=""SideDealoverlay""></div>
</div>";
                }

                #endregion Isoldout

                strSub += @"</article>";

                #endregion 組合Deal Content
            }

            #endregion SubDeal

            lsubDeal.Text = strSub;
            if (string.IsNullOrWhiteSpace(strSub))
            {
                divSubDealTitle.Visible = false;
            }
        }

        public void SetCitys(SystemCodeCollection citys)
        {
            ddlsCtiy.DataSource = citys;
            ddlsCtiy.DataBind();

            string overViewRegion = HiDealRegionManager.DealOverviewRegion.ToString();
            if (ddlsCtiy.Items.FindByValue(overViewRegion) != null)
            {
                ddlsCtiy.SelectedValue = overViewRegion;
            }
        }

        public void SetCategory(DataTable Category)
        {
            lvDealCatg.DataSource = Category;
            lvDealCatg.DataBind();
        }

        protected Dictionary<HiDealSpecialDiscountType, int> Specials { get; set; }

        public void SetSpecials(Dictionary<HiDealSpecialDiscountType, int> specials)
        {
            this.Specials = specials;
        }

        public void ShowAlert(string message)
        {
            if (lSresult.Visible == false)
            {
                lSresult.Visible = true;
            }

            lSresult.Text = message;
        }

        #endregion Presenter Call

        #region Method

        private string GetMainProductContent(int dealId)
        {
            IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            string strContent = @"<ul class='DealView' id=""sView" + dealId.ToString() + @""">";
            HiDealProductCollection products = hp.HiDealProductGetList(-1, -1, "seq", HiDealProduct.Columns.DealId + "=" + dealId);
            IEnumerable<HiDealProduct> onlineProducts = products.Where(prod => prod.IsOnline == true).ToList();

            foreach (HiDealProduct product in onlineProducts)
            {
                #region 產品資訊

                strContent += @"<li class=""Listitems"" style=""display: block; overflow: hidden; margin-top: 0px; margin-bottom: 0px;
padding-top: 0px; padding-bottom: 0px;""><a href='" + GetProductLink(product) + @"' target=""_blank"">
<div class=""Listitem"">
<div class=""Price"">$" + (product.PriceInPage.ToString("f0")) + @"</div>
<div class=""ListTitle"">" + product.Name + @"</div>
</div><!--Listitem--></a></li>";

                #endregion 產品資訊
            }
            strContent += "</ul>";
            return strContent;
        }

        private string GetSubProductContent(int dealId)
        {
            IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            string strContent = @"<ul class='DealView DealView' id=""ssView" + dealId.ToString() + @""">";

            HiDealProductCollection products = hp.HiDealProductGetList(-1, -1, "seq", HiDealProduct.Columns.DealId + "=" + dealId);
            IEnumerable<HiDealProduct> onlineProducts = products.Where(prod => prod.IsOnline == true);

            foreach (HiDealProduct product in onlineProducts)
            {
                #region 產品資訊

                strContent += @"<a href='" + GetProductLink(product) + @"' target=""_blank""><li class=""Listitems"" style=""display: block; overflow: hidden; margin-top: 0px; margin-bottom: 0px;
padding-top: 0px; padding-bottom: 0px;"">
<div class=""Listitem"">
<div class=""Price"">$" + product.PriceInPage.ToString("f0") + @"</div>
<div class=""ListTitle"">" + product.Name + @"</div>
</div><!--Listitem--></li></a>";

                #endregion 產品資訊
            }
            strContent += "</ul>";
            return strContent;
        }

        private string GetEventTagHtml()
        {
            return @"<p class='event_sp'></p>";
        }

        private string GetBuyUseTagHtml()
        {
            return @"<p class='use_sp'></p>";
        }

        private string GetValentinesTagHtml()
        {
            return @"<p class='valentines_sp'><svg><image xlink:href='../Themes/HighDeal/images/index/valentines_sp.svg' src='../Themes/HighDeal/images/index/valentines_sp.jpg' width='80' height='22' /></svg></p>";
        }

        private string GetFatherTagHtml()
        {
            return @"<p class='father_sp'><svg><image xlink:href='../Themes/HighDeal/images/index/father_sp.svg' src='../Themes/HighDeal/images/index/father_sp.png' width='80' height='22' /></svg></p>";
        }

        private string GetMoonTagHtml()
        {
            return @"<p class='moon_sp'><svg><image xlink:href='../Themes/HighDeal/images/index/moon_sp.svg' src='../Themes/HighDeal/images/index/moon_sp.png' width='80' height='22' /></svg></p>";
        }


        public string GetNormalCategoryIconClass()
        {
            string className = string.Empty;

            if (this.SpecialId == null && Eval("code_id").ToString() == CategoryId)
            {
                className = "category_icon";
                //活動專區
                if (Eval("code_id").ToString() == "19" || Eval("code_id").ToString() == "21" || Eval("code_id").ToString() == "22" || Eval("code_id").ToString() == "23")
                {
                    className = "category_iconevent";
                }
                //即買即用
                if (Eval("code_id").ToString() == "20")
                {
                    className = "category_iconuse";
                }
            }
            else
            {
                className = "category_icon02";
                //活動專區
                if (Eval("code_id").ToString() == "19" || Eval("code_id").ToString() == "21" || Eval("code_id").ToString() == "22" || Eval("code_id").ToString() == "23")
                {
                    className = "category_iconevent02";
                }
                //即買即用
                if (Eval("code_id").ToString() == "20")
                {
                    className = "category_iconuse02";
                }
            }

            return className;
        }


        protected void CheckEventEmail()
        {
            HttpCookie cookie = Request.Cookies["PiinLifeSubscriptionCookie"];
            if (Page.User != null && Page.User.Identity.IsAuthenticated)
            {
                if (!HiDealMailFacade.IsHiDealSubscribe(Page.User.Identity.Name))
                {
                    ShowEventEmail = cookie == null;
                }
                else
                {
                    ShowEventEmail = false;
                }
            }
            else
                ShowEventEmail = cookie == null;
        }

        #endregion Method

        #region Local Event

        protected void btnsubscript_Click(object sender, EventArgs e)
        {
            if (AddSubscript != null)
            {
                //檢查email是否合法
                if (RegExRules.CheckEmail(txtSubscript.Text))
                {
                    string[] data = new string[2];
                    data[0] = ddlsCtiy.SelectedItem.Value;
                    data[1] = txtSubscript.Text;
                    AddSubscript(sender, new DataEventArgs<string[]>(data));
                }
                else
                {
                    ShowAlert("電子郵件格式錯誤。");
                }
            }
        }

        #endregion Local Event

        #region for blockui

        [WebMethod]
        public static int SubscribeEvent(string city, string mail, string cpa)
        {
            bool isEmailValidate = RegExRules.CheckEmail(mail);
            if (!isEmailValidate && string.IsNullOrWhiteSpace(city))
            {
                return 4;
            }
            else if (string.IsNullOrWhiteSpace(city))
            {
                return 3;
            }
            else if (!isEmailValidate)
            {
                return 2;
            }
            else
            {
                HiDealSubscription subscript = HiDealMailFacade.HiDealSubscribeCheckSet(mail, 0, Convert.ToInt32(city), true);
                if (!subscript.Flag.Equals((int)PiinlifeSubscriptionFlag.Cancel))
                {
                    CpaUtility.RecordSubscriptionByReferrer(cpa, subscript);
                }

                return 1;
            }
        }

        #endregion for blockui
    }
}
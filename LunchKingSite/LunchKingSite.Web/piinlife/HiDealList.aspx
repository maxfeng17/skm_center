﻿<%@ Page Title="" Language="C#" MasterPageFile="~/piinlife/hideal.Master" AutoEventWireup="true"
    CodeBehind="HiDealList.aspx.cs" Inherits="LunchKingSite.Web.piinlife.HiDealList" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.I18N" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/RandomParagraph.ascx" TagName="RandomParagraph"
    TagPrefix="ucR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ScriptContent" runat="server">
    <link rel="stylesheet" type="text/css" href='<%#ResolveUrl("~/Themes/PCweb/HighDeal/HDA1.css")%>' />
    <style type="text/css">
        .DealView
        {
            display: none;
        }
        a.fansLink
        {
            color: White;
        }
        a.fansLink:visitedG
        {
            color: White;
        }
        a.fansLink:hover
        {
            color: White;
            text-decoration: underline;
        }
    </style>
    <script type="text/javascript" src="//www.google.com/jsapi?key=<%=GoogleApiKey%>"></script>
    <link href="../Tools/js/jquery.smartbanner.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Tools/js/jquery.smartbanner.js"></script>
    <script type="text/javascript">      
        $(document).ready(function () {
            newsVisible();
            $("#<%=txtSubscript.ClientID%>").focus(function () { focusSubscript(); });
            $("#<%=txtSubscript.ClientID%>").blur(function () { checkSubscript(); });

            if (!navigator.userAgent.match(/<%= AndroidUserAgent %>|<%= iOSUserAgent %>/i)) {
                // 行動裝置不跳窗
                <% if(ShowEventEmail) { %>
                    showSubscription();
                <% } %>
        }

            $(window).scroll(function() {
                if ($(document).scrollTop() >= 200) {
                    $('#btnBackTop').css('display','inline');
                }else {
                    $('#btnBackTop').css('display','none');
                }
            });

        $.fn.queueInterruptibleAction = function(delayMilseconds, actionCallback) {
            //cancel if other action exist
            this.cancelInterruptibleAction();                
            // delay execute delayCallback
            var timerId = window.setTimeout(function() {                    
                $.removeData(this, 'timerId');
                actionCallback.call(this);
            }, delayMilseconds);                     
            $.data(this, 'timerId', timerId);
        };

        $.fn.cancelInterruptibleAction = function() {
            var timerId = $.data(this, 'timerId');
            if (timerId != null) {
                $.removeData(this, 'timerId');
                window.clearTimeout(timerId);
            }
        };

        $.fn.delayHover = function(tClass, pClass, enterDelay, leaveDelay) {
            if (enterDelay == null) enterDelay = 150;
            if (leaveDelay == null) leaveDelay = 400;
            return this.each(function() {
                var trigger = $(this); //button
                var target = $(this).closest(pClass).find(tClass); //view       
                trigger.mouseenter(function() {                        
                    target.queueInterruptibleAction(enterDelay,function() {
                        target.show();
                    });
                });
                trigger.mouseleave(function() {
                    target.queueInterruptibleAction(leaveDelay, function() {
                        target.hide();
                    });
                });
                target.mouseenter(function() {
                    target.cancelInterruptibleAction();
                });
                target.mouseleave(function() {                        
                    target.queueInterruptibleAction(leaveDelay, function() {
                        target.hide();
                    });                        
                });
            });
        };

        $('.DealButton').delayHover('.DealView', '.Deal, .SideDeal', 150,400);

        });//end of document.ready

        function newsVisible() {
            if ($.trim($('#newscontent div:first').html()) == '' || $('#newscontent div:first').html() == null) {
                $('#newscontent').hide();$('#newscontent').parent('#TOPBanner').hide();
            }
            if ($.trim($('#MarketingBNArea div:first').html()) == '' || $('#MarketingBNArea div:first').html() == null) {
                $('#Marketingarea').hide();
            }
        }

        function focusSubscript() {
            var tb = $("#<%=txtSubscript.ClientID%>");
            var mail = $.trim(tb.val());
            if (mail == "" || mail == "請輸入您的email") {
                tb.val("");
                tb.css("color", "Black");
            }
        }

        function checkSubscript() {
            var rtn = true;
            var tb = $("#<%=txtSubscript.ClientID%>");
            var mail = $.trim(tb.val());
            if (mail == "" || mail == "請輸入您的email") {
                tb.val("請輸入您的email");
                tb.css("color", "#CCC");
                rtn = false;
            }
            else {
                tb.css("color", "Black");
            }
            return rtn;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script language="javascript" type="text/javascript" src="../Tools/js/jquery-ui.js"></script>
    <script language="javascript" type="text/javascript" src="../Tools/js/jquery-ui-1.8.custom.min.js"></script>
    <div class="wrap">
        <button id="btnBackTop" class="Button_back_to_top_command" style="display: none;"
            title="返回頂部" onclick="return GoToTheTop();">
        </button>
        <section class="Top">
            <section class="Top">
                <div id="newscontent">
                    <a class="close" onclick="$('#newscontent').hide();$('#newscontent').parent('#TOPBanner').hide();" style="cursor: pointer"></a>
                    <ucR:RandomParagraph ID="pponnews" runat="server" OnInit="RandomPponNewsInit" />
                </div>
            </section>
        </section>
        <aside class="Offers_Sidebar">
            <%-------------------------------------------類別選單------------------------------------------------%>
            <nav class="SideBar">
                <asp:ListView runat="server" ID="lvDealCatg">
                    <LayoutTemplate>
                        <ul class="category">
                            <li runat="server" id="itemPlaceHolder"></li>
                        </ul>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <li runat="server" id="category_all" style="display: block;" class='<%# this.SpecialId == null && Eval("code_id").ToString() == CategoryId ? "selected": "" %>'>
                            <span class='<%# GetNormalCategoryIconClass() %>'>
                            </span><a id="A1" runat="server" class="category" href='<%# string.Format(@"~\piinlife\HiDealList.aspx?regid={0}&category={1}",CityId, Eval("code_id"))   %>'>
                                <%# Eval("code_name") %></a>
                            <div class='<%# this.SpecialId == null && Eval("code_id").ToString() == CategoryId ? "arrow": "" %>'>
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:ListView>
            </nav>
            <% if (this.IsVisa2013)
               { %>
            <nav class="SideBar">
                <ul class="category">
                    <% if (this.Specials[HiDealSpecialDiscountType.VisaPrivate] > 0)
                       { %>
                    <li style="display: block;" class="<%= this.SpecialId == 1 ? "sp_bg selected" : "sp" %>">
                        <span class="<%= this.SpecialId == 1 ? "category_icon" : "category_icon02" %>"></span>
                        <a href="HiDealList.aspx?special=1" class="category">
                            <%=SystemCodeManager.GetSystemCodeNameByEnumValue(HiDealSpecialDiscountType.VisaPrivate) %></a>
                        <div class='<%= this.SpecialId == 1 ? "arrow" : "" %>'>
                        </div>
                    </li>
                    <% } %>
                    <% if (this.Specials[HiDealSpecialDiscountType.VisaPriority] > 0)
                       { %>
                    <li style="display: block;" class="<%= this.SpecialId == 2 ? "sp_bg selected" : "sp" %>">
                        <span class="<%= this.SpecialId == 2 ? "category_icon" : "category_icon02" %>"></span>
                        <a href="HiDealList.aspx?special=2" class="category">
                            <%=SystemCodeManager.GetSystemCodeNameByEnumValue(HiDealSpecialDiscountType.VisaPriority)%></a>
                        <div class='<%= this.SpecialId == 2 ? "arrow" : "" %>'>
                        </div>
                    </li>
                    <% } %>
                </ul>
            </nav>
            <% } %>
            <%-------------------------------------------類別選單 end------------------------------------------------%>
            <aside class="Theme_Email">
                <h3>
                    訂閱追蹤</h3>
                <div class="Socialconnect">
                    <div class="followFB_Text" style="margin: 0px 10px auto 25px">
                        <a href="https://www.facebook.com/PiinLife" target="_blank" class="fansLink">加入粉絲
                        </a>
                    </div>
                    <div class="followFB" style="height: 31px">
                        <div class="fb-like" data-href="http://www.facebook.com/PiinLife" data-send="false"
                            data-layout="button_count" data-width="90" data-show-faces="true">
                        </div>
                    </div>
                </div>
                <!--Socialconnect-->
                <p style="text-align: center;">
                    <asp:DropDownList ID="ddlsCtiy" runat="server" DataTextField="codename" DataValueField="codeid"
                        Font-Size="12px" Visible="false">
                    </asp:DropDownList>
                    <asp:TextBox CssClass="Emailarea" ID="txtSubscript" runat="server" Text="請輸入您的email"></asp:TextBox>
                </p>
                <asp:Button ID="btnsubscript" runat="server" Text="訂閱" OnClick="btnsubscript_Click"
                    CssClass="Button" />
                <a href="/Ppon/PrivacyGoogle.aspx" class="PrivacyPolicy">隱私權政策</a>
                <h4>
                    <asp:Literal ID="lSresult" runat="server" Visible="false"></asp:Literal></h4>
            </aside>
            <aside>
                <ucR:RandomParagraph ID="visa" runat="server" />
            </aside>
            <aside class="BannerPromo">
                <uc1:Paragraph ID="hidealad1" runat="server" />
            </aside>
            <aside class="BannerPromo">
                <uc1:Paragraph ID="hidealad2" runat="server" />
            </aside>
            <aside class="BannerPromo">
                <uc1:Paragraph ID="hidealad3" runat="server" />
            </aside>
        </aside>
        <%if (this.IsVisa2013 == false && this.SpecialId != null)
          {
              //功能未on，無檔次，但又進本頁，顯示預告畫面%>
        <section class="OfferList">
            <div>
                <img src="https://cache.gizmodo.com/assets/images/4/2011/08/visa640x2.jpg" />
            </div>
        </section>
        <% } %>
        <%if (this.SpecialId == null || (this.IsVisa2013 == true && this.SpecialId != null))
          { %>
        <section class="OfferList">
            <asp:Literal ID="lblMainDeal" runat="server"></asp:Literal>
            <div id="divSubDealTitle" runat="server" class="Offer_EndingTitle">
                <a href="/"></a>
            </div>
            <asp:Literal ID="lsubDeal" runat="server"></asp:Literal>
        </section>
        <% } %>
    </div>
    <!--wrap-->
</asp:Content>

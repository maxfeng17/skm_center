﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VboBanner.aspx.cs" Inherits="LunchKingSite.Web.piinlife.VboBanner" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript" src="<%=ResolveUrl("~/Tools/js/jquery-1.7.2.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Tools/js/jquery.cycle.all.min.js") %>"></script>
    <script type="text/javascript">
        $().ready(function () {            
            $('#pinnTopBanners').cycle({
                fx: 'fade',
                sync: true,
                speed: 750,
                timeout: 5000
            });
        });
    </script>
</head>
<body style="margin:0px ; padding:0px;background-color:#323031;">
    <form id="form1" runat="server">
    <div style="text-align:center">
        <div id="pinnTopBanners" style="width: 760px; display: inline-block; height: 90px">
            <asp:Literal ID="li_preview" runat="server" />
        </div>
    </div>
    </form>
</body>
</html>

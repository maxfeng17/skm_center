﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System.Security.Cryptography;
using System.Text;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.BizLogic.Component;


namespace LunchKingSite.Web.piinlife
{
    public partial class Unsubscription : BasePage, IHiDealUnsubscriptionView
    {
        public event EventHandler<DataEventArgs<string[]>> UnSubscribe;

        #region interface properties

        private HiDealUnsubscriptionPresenter _presenter;
        public HiDealUnsubscriptionPresenter presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get { return this._presenter; }
        }

        #endregion

        public string theEmail
        {
            get { return Request.QueryString["email"]; }
            set { theEmail = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetSubscribeData();
                presenter.OnViewInitialized();
            }
            presenter.OnViewLoaded();
        }

        [WebMethod]
        private bool GetSubscribeData()
        {
            string email = Request["email"] != null ? Request["email"].ToString() : (string)Session[HiDealSession.SubscribeEmail.ToString()];
            string regionId = Request["region"] != null ? Request["region"].ToString() : (string)Session[HiDealSession.CityId.ToString()];
            string codeId = Request["code"];
            return UnSubscription(regionId, email, codeId);
        }

        //判斷email,region,code值是否正確
        private bool UnSubscription(string city, string email, string codeId)
        {
            //md5編碼，編碼相同才進入
            Cryptography cp = new Cryptography(2);
            string thecode = cp.Md5(new string[] {city, email});

            if (codeId == thecode)
            {
                IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
                HiDealSubscriptionCollection subscripts = hp.HiDealSubscriptionGetList(0, 0, string.Empty, HiDealSubscription.EmailColumn + " = " + email);
                if (subscripts.Count > 0)
                {
                    unsubscribeArea.Visible = true;
                    ADPIC.Visible = true;
                }
                else
                {
                    ShowAlert(3);
                }
            }
            else
            {
                ShowAlert(2);
            }
            return (codeId == thecode) ? true : false;
        }

        protected void btnUnSubscribe_Click(object sender, EventArgs e)
        {
            if (UnSubscribe != null)
            {
                string[] data = new string[1];
                data[0] = theEmail;
                UnSubscribe(sender, new DataEventArgs<string[]>(data));
            }
        }

        /// <summary>
        /// 顯示執行結果的訊息
        /// </summary>
        /// <param name="value">欲執行的結果參數(int)，請見下面設定<br />
        /// 1.回傳成功<br />
        /// 2.參數有誤<br />
        /// 3.無此email<br /></param>
        public void ShowAlert(int value)
        {
            if (value == 1)
            {
                unsubscribeArea.Visible = false;
                unsubscribeOK.Visible = true;
                ADPIC.Visible = true;
            }
            else if (value == 2)
            {
                unsubscribeArea.Visible = false;
                unsubscribeFailed.Visible = true;
                ADPIC.Visible = true;
            }
            else if (value == 3)
            {
                Response.Write("<script languge='javascript'>alert('請確認Email'); window.location.href='default.aspx'</script>");
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.piinlife
{
    public partial class FBDeal : BasePage, IHiDealDealView
    {
        protected ISysConfProvider confProv = ProviderFactory.Instance().GetConfig();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.Presenter.OnViewLoaded();
            }
        }

        #region Fields

        #endregion


        #region property
        private HiDealDealPresenter _presenter;
        public HiDealDealPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get { return this._presenter; }
        }


        public HiDealRegionCollection Regions { get; set; }

        public string did
        {
            get
            {
                if (Request["did"] != null)
                    return Request["did"];
                else
                    return "";
            }
        }
        public string pid
        {
            get
            {
                if (Request["pid"] != null)
                    return Request["pid"];
                else
                    return "";
            }
        }
        public Guid ProductGuid
        {
            get
            {
                Guid prodGuid;
                Guid.TryParse(Request.QueryString["gpid"], out prodGuid);
                return prodGuid;
            }
        }
        public bool IsPreviewMode
        {
            get { return Request.QueryString["mode"] == "preview"; }
        }

        public string DealShortName
        { get; set; }

        public string GoogleApiKey
        {
            get { return ProviderFactory.Instance().GetConfig().GoogleMapsAPIKey; }
        }

        public string FacebookAppId
        {
            get { return ProviderFactory.Instance().GetConfig().FacebookApplicationId; }
        }
        public string FacebookOgTitle { get; set; }
        public string FacebookOgUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(did))
                    return string.Format("{0}{1}FbDeal.aspx?did={2}", WebUtility.GetSiteRoot(), Page.AppRelativeTemplateSourceDirectory.Substring(1), did);
                if (!string.IsNullOrEmpty(pid))
                    return string.Format("{0}{1}FbDeal.aspx?pid={2}", WebUtility.GetSiteRoot(), Page.AppRelativeTemplateSourceDirectory.Substring(1), pid);
                return "";
            }
        }
        public string FacebookOgImage { get; set; }
        public string FacebookOgDescription
        {
            get { return Phrase.FacebookHiDealShareDescription; }
        }
        public string ImageSrcLinkHref
        {
            get { return FacebookOgImage; }
        }
        public string CanonicalLinkHref
        {
            get
            {
                if (!string.IsNullOrEmpty(did))
                    return string.Format("{0}/piinlife/deal?did={1}", WebUtility.GetSiteRoot(), did);
                if (!string.IsNullOrEmpty(pid))
                    return string.Format("{0}/piinlife/deal?pid={1}", WebUtility.GetSiteRoot(), pid);
                return "";
            }
        }

        #endregion


        #region class fields
        public string TabSeqListShow { get; set; }
        /// <summary>
        /// 麵包屑的區域編號
        /// </summary>
        public int BreadcrumbCityId
        {
            get
            {
                if (Regions != null && Regions.Count > 0)
                {
                    var region = ((hideal)this.Master).CurrentRegionId;
                    if (Regions.Where(x => x.CodeId == region).Count() > 0)
                    {
                        return region;
                    }
                    else
                    {
                        return HiDealRegionManager.DealOverviewRegion;
                    }
                }
                return HiDealRegionManager.DealOverviewRegion;
            }
        }
        /// <summary>
        /// 麵包屑的區域名稱
        /// </summary>
        public string BreadcrumbCityName
        {
            get
            {
                return SystemCodeManager.GetHiDealRegionsName(BreadcrumbCityId);
            }
        }
        /// <summary>
        /// 麵包屑的分類編號
        /// </summary>
        public int BreadcrumbCategoryId
        {
            get
            {
                if (Session[HiDealSession.CategoryId.ToString()] != null)
                    return int.Parse(Session[HiDealSession.CategoryId.ToString()].ToString());
                else
                    return 0;
            }
        }
        /// <summary>
        /// 麵包屑的分類名稱
        /// </summary>
        public string BreadcrumbCategoryName
        {
            get
            {
                return SystemCodeManager.GetHiDealCatgName(BreadcrumbCategoryId);
            }
        }


        public string CityName
        {
            get { return SystemCodeManager.GetHiDealRegionsName(CityId); }
        }

        /// <summary>
        /// 商品內頁的左上方區域灰字，會依照該檔的區域勾選，去顯示。但若只勾選總覽，擇不顯示任何訊息。
        /// 同時勾選多個區域，則會依照user所進入的路徑做顯示，若是從總覽進來，則顯示總覽顯示的區域。
        /// </summary>
        public int CityId
        {
            get
            {
                //master的區域
                var region = ((hideal)Master).CurrentRegionId;
                //異常，無區域，直接顯示總覽
                if (Regions==null)
                {
                    return HiDealRegionManager.DealOverviewRegion;
                }

                //如果master為總覽
                if(region == HiDealRegionManager.DealOverviewRegion)
                {
                    //顯示除了總覽以外的第一個區域
                    var tmps = Regions.Where(x => !x.CodeId.Equals(HiDealRegionManager.DealOverviewRegion)).ToList();
                    if (tmps.Count > 0)
                    {
                        return tmps.First().CodeId;
                    }
                }
                return region;
            }
        }

        #endregion class fields


        public void SetDealContent(HiDealDeal deal, HiDealSpecialDiscountType specialType)
        {


        }

        public void SetProductContent(HiDealDeal deal, HiDealProductCollection products)
        {
         
        }

        public void SetTap(HiDealContentCollection contents, HiDealDeal deal)
        {

        }



        public void SetStoresInfo(ViewHiDealStoresInfoCollection stores)
        {



        }
        

        public void RedirectToDefault()
        {
            Response.Redirect("~/piinlife/default.aspx");
        }

        protected void CheckEventEmail()
        {

        }

        protected void SetCookie(int days, string name)
        {
        }

    }
}
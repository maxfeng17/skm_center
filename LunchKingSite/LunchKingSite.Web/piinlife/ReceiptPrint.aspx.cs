﻿using System;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.UI;

namespace LunchKingSite.Web.piinlife
{
    public partial class ReceiptPrint : LocalizedBasePage
    {
        private IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        protected void Page_Load(object sender, EventArgs e)
        {
            Guid oid;
            
            if ( Request.QueryString["mode"] == "piin" && Guid.TryParse(Request.QueryString["ogid"], out oid))
            {
                ConstructPageElements(oid);
            }
            else
            {
                receiptContainer.Visible = false;
            }
        }

        private void ConstructPageElements(Guid oid)
        {
            #region pre-execution check

            HiDealOrder order = hp.HiDealOrderGet(oid);
            if (!order.IsLoaded || order.OrderStatus != (int)HiDealOrderStatus.Completed)
            {
                receiptContainer.Visible = false;
                return;
            }

            HiDealOrderDetailCollection details = hp.HiDealOrderDetailGetListByOrderGuid(oid);
            if (details == null)
            {
                receiptContainer.Visible = false;
                return;
            }

            CashTrustLogCollection ctlogs = mp.CashTrustLogGetListByOrderDetailGuids(details.Select<HiDealOrderDetail, Guid>(x => x.Guid), OrderClassification.HiDeal);

            if (ctlogs.Count == 0)   //沒有cash_trust_log就沒有完整的金額資料 (hi_deal_order 雖然有付款的金額, 但若有部分退貨的狀況就不可使用)
            {
                receiptContainer.Visible = false;
                return;
            }

            #endregion

            HiDealDeal deal = hp.HiDealDealGet(order.HiDealId);

            orderId.Text = order.OrderId;
            eventName.Text = deal.PromoShortDesc;
            if (order.CompletedTime.HasValue)
            {
                paymentDate.Text = order.CompletedTime.Value.ToString("yyyy/MM/dd");
            }
            quantity.Text = details.Where(x => !int.Equals((int)HiDealDeliveryType.Other, x.DeliveryType)).Sum(x => x.ItemQuantity).ToString();
            HiDealOrderDetail oneDetail = details.FirstOrDefault();
            if (oneDetail != null)
            {
                itemUnitPrice.Text = oneDetail.UnitPrice.ToString("########");
            }

            #region 各種付款方式的金額
            var paymentTypesAmount = ctlogs.GroupBy(x => x.BusinessHourGuid)    //找個沒意義的東西 group by
                .Select(x => new
            {
                CreditCard = x.Sum(y => y.CreditCard),
                Atm = x.Sum(y => y.Atm),
                PCash = x.Sum(y => y.Pcash),
                SCash = x.Sum(y => y.Scash),
                BCash = x.Sum(y => y.Bcash),
                DCash = x.Sum(y => y.DiscountAmount)
            });
            var amount = paymentTypesAmount.FirstOrDefault();
            if (amount != null)
            {
                creditCardPaymentContainer.Visible = !int.Equals(0, amount.CreditCard);
                atmPaymentContainer.Visible = !int.Equals(0, amount.Atm);
                pcashPaymentContainer.Visible = !int.Equals(0, amount.PCash);
                scashPaymentContainer.Visible = !int.Equals(0, amount.SCash);
                bcashPaymentContainer.Visible = !int.Equals(0, amount.BCash);
                dcashPaymentContainer.Visible = !int.Equals(0, amount.DCash);
                creditCardAmount.Text = amount.CreditCard.ToString();
                atmAmount.Text = amount.Atm.ToString();
                pcashAmount.Text = amount.PCash.ToString();
                scashAmount.Text = amount.SCash.ToString();
                bcashAmount.Text = amount.BCash.ToString();
                dcashAmount.Text = amount.DCash.ToString();
                totalAmount.Text = (amount.CreditCard + amount.Atm + amount.PCash + amount.BCash + amount.DCash).ToString();
            }
            #endregion

            #region 運費
            CashTrustLog freight = ctlogs.Where(x => x.SpecialStatus == (int)TrustSpecialStatus.Freight).FirstOrDefault();
            if (freight != null)
            {
                freightContainer.Visible = true;
                freightAmount.Text = (freight.CreditCard + freight.Atm + freight.Pcash + freight.Scash + freight.Bcash + freight.DiscountAmount).ToString();
            }
            #endregion
        }
    }
}
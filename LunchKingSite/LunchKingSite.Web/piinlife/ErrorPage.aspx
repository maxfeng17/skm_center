﻿<%@ Page Title="" Language="C#" MasterPageFile="~/piinlife/hideal.Master" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="LunchKingSite.Web.piinlife.ErrorPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ScriptContent" runat="server">
    <link href="<%#ResolveUrl("~/Themes/HighDeal/images/Register/Register_member.css")%>" rel="stylesheet" type="text/css">
    <link href="<%#ResolveUrl("~/Themes/HighDeal/images/Failure/Error.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SMC" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <section class="ErrorBlock">
        <content class="Left-column">
        <h1 class="padding">很抱歉，此頁面不存在！</h1>
        <p>您瀏覽的頁面可能被刪除，或者已經失效、故障。<br/>
        若您是從電子郵件連結，請嘗試複製完整的網址，確認是否因此而造成頁面無法顯示。</p>

        <p>您可以繼續享受我們為您提供的各項服務，回到<a href="<%=ResolveUrl("~/piinlife/") %>">品生活首頁</a>。</p>
        </content>
        <content class="Right-column" style="display:block;">
        <figure class="RegisterAD_Short"><img src="<%=ResolveUrl("~/Themes/HighDeal/images/Register/wait_-certifiyAD.jpg") %>" width="300" height="600">
        </figure>
        </content>
    </section>
</asp:Content>

﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.SqlServer.Types;

namespace LunchKingSite.Web.piinlife
{
    public partial class HiDeal : BasePage, IHiDealDealView
    {
        protected ISysConfProvider confProv;

        //判斷不開立發票狀態顯示使用參數宣告
        protected string strNoInvoiceCreateProductName;

        protected int DealProductCount;
        protected int NoInvoiceCreateCount;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckEventEmail();
                confProv = ProviderFactory.Instance().GetConfig();

                this.Presenter.OnViewLoaded();
            }
        }

        #region property

        private HiDealDealPresenter _presenter;

        public HiDealDealPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public HiDealRegionCollection Regions { get; set; }

        public string did
        {
            get
            {
                if (Request["did"] != null)
                {
                    return Request["did"];
                }
                else
                {
                    return "";
                }
            }
        }

        public string pid
        {
            get
            {
                if (Request["pid"] != null)
                {
                    return Request["pid"];
                }
                else
                {
                    return "";
                }
            }
        }

        public Guid ProductGuid
        {
            get
            {
                Guid prodGuid;
                Guid.TryParse(Request.QueryString["gpid"], out prodGuid);
                return prodGuid;
            }
        }

        public bool IsPreviewMode
        {
            get
            {
                return Request.QueryString["mode"] == "preview";
            }
        }

        public string DealShortName { get; set; }

        public string GoogleApiKey
        {
            get
            {
                return ProviderFactory.Instance().GetConfig().GoogleMapsAPIKey;
            }
        }

        public string FacebookAppId
        {
            get
            {
                return ProviderFactory.Instance().GetConfig().FacebookApplicationId;
            }
        }

        public string FacebookOgTitle { get; set; }

        public string FacebookOgUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(did))
                {
                    return string.Format("{0}{1}deal.aspx?regid={2}&did={3}", WebUtility.GetSiteRoot(), Page.AppRelativeTemplateSourceDirectory.Substring(1), MasterCurrentRegionId, did);
                }

                if (!string.IsNullOrEmpty(pid))
                {
                    return string.Format("{0}{1}deal.aspx?regid={2}&pid={3}", WebUtility.GetSiteRoot(), Page.AppRelativeTemplateSourceDirectory.Substring(1), MasterCurrentRegionId, pid);
                }

                return string.Empty;
            }
        }

        public string FacebookOgImage { get; set; }

        public string FacebookOgDescription
        {
            get
            {
                return Phrase.FacebookHiDealShareDescription;
            }
        }

        public string ImageSrcLinkHref
        {
            get
            {
                return FacebookOgImage;
            }
        }

        public string CanonicalLinkHref
        {
            get
            {
                if (!string.IsNullOrEmpty(did))
                {
                    return string.Format("{0}/piinlife/HiDeal.aspx?regid={1}&did={2}", WebUtility.GetSiteRoot(), MasterCurrentRegionId, did);
                }

                if (!string.IsNullOrEmpty(pid))
                {
                    return string.Format("{0}/piinlife/HiDeal.aspx?regid={1}&pid={2}", WebUtility.GetSiteRoot(), MasterCurrentRegionId, pid);
                }

                return string.Empty;
            }
        }

        public bool ShowEventEmail { get; set; }

        #endregion property

        #region class fields

        public string TabSeqListShow { get; set; }

        /// <summary>
        /// 麵包屑的區域編號
        /// </summary>
        public int BreadcrumbCityId
        {
            get
            {
                if (Regions != null && Regions.Count > 0)
                {
                    var region = ((hideal)this.Master).CurrentRegionId;
                    if (Regions.Where(x => x.CodeId == region).Count() > 0)
                    {
                        return region;
                    }
                    else
                    {
                        return HiDealRegionManager.DealOverviewRegion;
                    }
                }
                return HiDealRegionManager.DealOverviewRegion;
            }
        }

        /// <summary>
        /// 麵包屑的區域名稱
        /// </summary>
        public string BreadcrumbCityName
        {
            get
            {
                return SystemCodeManager.GetHiDealRegionsName(BreadcrumbCityId);
            }
        }

        /// <summary>
        /// 麵包屑的分類編號
        /// </summary>
        public int BreadcrumbCategoryId
        {
            get
            {
                if (Session[HiDealSession.CategoryId.ToString()] != null)
                {
                    return int.Parse(Session[HiDealSession.CategoryId.ToString()].ToString());
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// 麵包屑的分類名稱
        /// </summary>
        public string BreadcrumbCategoryName
        {
            get
            {
                return SystemCodeManager.GetHiDealCatgName(BreadcrumbCategoryId);
            }
        }

        public string CityName
        {
            get
            {
                return SystemCodeManager.GetHiDealRegionsName(CityId);
            }
        }

        /// <summary>
        /// 商品內頁的左上方區域灰字，會依照該檔的區域勾選，去顯示。但若只勾選總覽，擇不顯示任何訊息。
        /// 同時勾選多個區域，則會依照user所進入的路徑做顯示，若是從總覽進來，則顯示總覽顯示的區域。
        /// </summary>
        public int CityId
        {
            get
            {
                //master的區域
                var region = ((hideal)Master).CurrentRegionId;
                //異常，無區域，直接顯示總覽
                if (Regions == null)
                {
                    return HiDealRegionManager.DealOverviewRegion;
                }

                //如果master為總覽
                if (region == HiDealRegionManager.DealOverviewRegion)
                {
                    //顯示除了總覽以外的第一個區域
                    var tmps = Regions.Where(x => !x.CodeId.Equals(HiDealRegionManager.DealOverviewRegion)).ToList();
                    if (tmps.Count > 0)
                    {
                        return tmps.First().CodeId;
                    }
                }
                return region;
            }
        }

        /// <summary>
        /// 取得Master.CurrentRegionId取得頁面目前的區域類別設定
        /// </summary>
        public int MasterCurrentRegionId
        {
            get
            {
                return ((hideal)Master).CurrentRegionId;
            }
        }

        private DateTime DearStartTime { get; set; }

        /// <summary>
        /// FB分享功能用的檔次編號
        /// </summary>
        public string FbDealId { get; set; }

        #endregion class fields

        private HiDealSpecialDiscountType DealSpecialType { get; set; }

        public void SetDealContent(HiDealDeal deal, HiDealSpecialDiscountType specialType)
        {
            Page.MetaKeywords = Page.MetaKeywords + "," + deal.Name; //meta keyword
            Page.MetaDescription = deal.Name + deal.PromoLongDesc; //meta data: 主標+行銷標
            lDealName.Text = deal.Name;//好康名稱
            FbDealId = deal.Id.ToString();
            //總覽不顯示，其他則顯示城市名稱
            if (CityId == HiDealRegionManager.DealOverviewRegion)
            {
                lcity.Text = string.Empty;
            }
            else
            {
                lcity.Text = CityName;//城市
            }
            ldesc.Text = deal.PromoLongDesc;//好康說明
            DealShortName = deal.Name;

            #region 圖片產生

            List<string> imgUrls = ImageFacade.GetHiDealBigPhotos(deal, MediaType.HiDealSecondaryBigPhoto);
            lvPictureSlider.DataSource = imgUrls.Select(x => new { Url = x }).ToList();
            lvPictureSlider.DataBind();

            #endregion 圖片產生

            lCloseTime.Text = deal.DealEndTime.Value.ToString("yyyy/MM/dd hh:mm");//好康結束時間
            //Visa檔次
            if (ProviderFactory.Instance().GetConfig().IsVisa2013)
            {
                if (specialType == HiDealSpecialDiscountType.VisaPrivate)
                {
                    phVisaPrivate.Visible = true;
                }
                else if (specialType == HiDealSpecialDiscountType.VisaPriority && (DateTime.Now - deal.DealStartTime.Value).Days < 3)
                {
                    phVisaPriority.Visible = true;
                }
            }
            else
            {
                visaPl.Visible = deal.IsVisa != null && deal.IsVisa.Value;
            }

            //SpecialCategoryTag

            //活動專區Tag  [system_code] where [code_group] = 'HiDealCatg' and code_id =19
            if (HiDealDealManager.IsCategorySpecialTag(deal.Id, 19))
            {
                llSpecialCategoryTag.Text += GetEventTagHtml();
            }

            //即買即用Tag [system_code] where [code_group] = 'HiDealCatg' and code_id =20
            if (HiDealDealManager.IsCategorySpecialTag(deal.Id, 20))
            {
                llSpecialCategoryTag.Text += GetBuyUseTagHtml();
            }

            //情人專屬Tag [system_code] where [code_group] = 'HiDealCatg' and code_id =21
            if (HiDealDealManager.IsCategorySpecialTag(deal.Id, 21))
            {
                llSpecialCategoryTag.Text += GetValentinesTagHtml();
            }

            //父親節精選Tag [system_code] where [code_group] = 'HiDealCatg' and code_id =22
            if (HiDealDealManager.IsCategorySpecialTag(deal.Id, 22))
            {
                llSpecialCategoryTag.Text += GetFatherTagHtml();
            }

            //賞悅中秋Tag [system_code] where [code_group] = 'HiDealCatg' and code_id =23
            if (HiDealDealManager.IsCategorySpecialTag(deal.Id, 23))
            {
                llSpecialCategoryTag.Text += GetMoonTagHtml();
            }


            this.DealSpecialType = specialType;
            this.DearStartTime = deal.DealStartTime.Value;
        }

        public void SetProductContent(HiDealDeal deal, HiDealProductCollection products)
        {
            //設定該檔次商品不開立發票資訊 初始值
            strNoInvoiceCreateProductName = "";
            NoInvoiceCreateCount = 0;

            //如果只有一商品直接展開，如果有指定商品，直接打開此商品
            IEnumerable<HiDealProduct> onlineProducts;
            if (!IsPreviewMode)
            {
                onlineProducts = products.Where(prod => prod.IsOnline == true).ToList();
            }
            else
            {
                onlineProducts = products.Select(prod => prod).ToList();
            }

            if (onlineProducts.Count() == 1)
            {
                lblscript.Text = @"<script language=""javascript"" type=""text/javascript"">$(function () {$('#content" + onlineProducts.Single().Id.ToString() + @"').show();$('#title" + onlineProducts.Single().Id.ToString() + @"').addClass('SinglePackage active');})</script>";
            }
            else
            {
                if (Request["pid"] != null)
                {
                    lblscript.Text = @"<script language=""javascript"" type=""text/javascript"">$(function () {$('#content" + Request["pid"] + @"').show();$('#title" + Request["pid"] + @"').addClass('SinglePackage active');})</script>";
                }
            }
            DealProductCount = onlineProducts.Count();
            foreach (HiDealProduct product in onlineProducts)
            {
                string discountString = ((Math.Round(((product.Price ?? 0) / (product.OriginalPrice ?? 1)), 2, MidpointRounding.AwayFromZero)) * 10).ToString("0.0") + @"折";

                lProducts.Text += @"<script language=""javascript"" type=""text/javascript"">
                                        $(function () {
                                            $('#t" + product.Id.ToString() + @"').click(function () {
                                                $('#all div').each(function () { $(this).hide();});
                                                $('#content" + product.Id.ToString() + @"').show();
                                                $('#desc" + product.Id.ToString() + @"').show();
                                                $('#button" + product.Id.ToString() + @"').show();
                                                $('#bottom" + product.Id.ToString() + @"').show();
                                                $('#title ul').each(function(){$(this).removeClass();$(this).addClass('SinglePackage')});
                                                $('#title" + product.Id.ToString() + @"').addClass('SinglePackage active');
                                                return false;
                                            });

                                            $('#content" + product.Id.ToString() + @"').hide();
                                        })
                                    </script>";

                lProducts.Text += @"<div id='title'><ul id=""title" + product.Id.ToString() + @"""  class=""SinglePackage"">

<li id=""t" + product.Id.ToString() + @""" class=""Sidetitle"">";

                #region 售完的判斷

                //if (product.IsSoldout != null && product.IsSoldout.Value)

                if ((deal.DealStartTime != null && DateTime.Now >= deal.DealStartTime.Value) &&
                    (HiDealCouponManager.GetCouponInventoryQuantity(product.Id) == 0))
                    lProducts.Text += @"<div class=""SoldOut""></div>";

                #endregion 售完的判斷

                lProducts.Text +=
@"<div class=""SubText Name"">" + product.Name + @"</div>
<div class=""Cost"">$" + product.PriceInPage.ToString("N0") + @"</div><div class=""Cost_decoration""><span class=""fixed_price"">" + (product.IsShowPriceDiscount ? "$" + product.OriginalPriceInPage.ToString("N0") : string.Empty) + @"</span> "
                                                              + (product.IsShowPriceDiscount ? discountString : string.Empty) +
@"</div></li>

<li  class=""desc"">
<div id=""all"">
<div id=""content" + product.Id.ToString() + @""">
<div class=""descTop""></div>
<div id=""desc" + product.Id.ToString() + @"""  class=""descContent"">
<p class=""Detail_description"">
" + product.Description + @"</p>
<p class=""Detail_Link""><a href=""javascript:GoToTab(1);"">詳細說明</a></p>
<p class=""Terms_Link"">購買即表示同意本商品的<a href=""javascript:GoToTab(5);"">權益說明</a>內容。</p>";

                #region 購買按鈕控制

                if (deal.DealStartTime != null && DateTime.Now < deal.DealStartTime.Value)
                {
                    //尚未開賣，目前時間小於起始時間，顯示即將開賣
                    lProducts.Text += @"<div id=""button" + product.Id.ToString() + @""" class=""Button_Single_CountDown""></div>";
                }
                else if (deal.DealEndTime != null && DateTime.Now > deal.DealEndTime.Value)
                {
                    //現在時間大於 結束時間 ，檔次活動已結束，顯示已售完。
                    lProducts.Text += @"<div id=""button" + product.Id.ToString() + @""" class=""Button_Single_SoldOut""></div>";
                }
                else if (HiDealCouponManager.GetCouponInventoryQuantity(product.Id) == 0)
                {
                    //可銷售數量為0，顯示已售完
                    lProducts.Text += @"<div id=""button" + product.Id.ToString() + @""" class=""Button_Single_SoldOut""></div>";
                }
                else
                {
                    //可銷售。
                    lProducts.Text += @"<a href=""pay.aspx?regid=" + MasterCurrentRegionId + @"&pid=" + product.Id.ToString() + @"""><div id=""button" + product.Id.ToString() + @""" class=""Button_Single_Buy""></div></a>";
                }

                #endregion 購買按鈕控制

                lProducts.Text += @"</div>
<div id=""bottom" + product.Id.ToString() + @""" class=""descBottom""></div>
</div>
</div>
</li>

</ul>
</div>";
                //抓取該檔次商品不開立發票資訊
                if (!product.IsInvoiceCreate)
                {
                    strNoInvoiceCreateProductName += product.Name + "、";
                    NoInvoiceCreateCount++;
                }
            }
        }

        public void SetTap(HiDealContentCollection contents, HiDealDeal deal)
        {
            var dealContents = contents.OrderByAsc(HiDealContent.Columns.Seq);
            rpTabLinks.DataSource = dealContents.Select(content => new { TabTitle = content.Title, Seq = content.Seq });
            rpTabLinks.DataBind();
            rpTabContents.DataSource = dealContents;
            rpTabContents.DataBind();
            var seqList = dealContents.Select(dealContent => dealContent.Seq).ToList();
            TabSeqListShow = string.Join(",", seqList);
        }

        public void SetStoresInfo(ViewHiDealStoresInfoCollection stores)
        {
            if (stores.Count == 0)
            {
                rpt_Location.Visible = false;
            }
            else
            {
                rpt_Location.Visible = true;
                rpt_Location.DataSource = stores;
                rpt_Location.DataBind();
            }
        }

        /// <summary>
        /// 計算折數使用(for商品)
        /// </summary>
        /// <param name="orgPrice">原價</param>
        /// <param name="price">售價</param>
        /// <returns>折數</returns>
        public string CalSale(string orgPrice, string price)
        {
            return (Double.Parse(price) / Double.Parse(orgPrice) * 100).ToString("F0");
        }

        public void RpTabContentsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var data = (HiDealContent)e.Item.DataItem;
            if (data.Seq == 5)
            {
                //不開立發票資訊顯示
                if (NoInvoiceCreateCount > 0)
                {
                    var lbNoInvoiceCreate = ((Label)e.Item.FindControl("lbNoInvoiceCreate"));
                    //該檔次部分商品為不開立發票 須加註個別商品名稱顯示不開立發票訊息
                    if (NoInvoiceCreateCount == DealProductCount)
                        lbNoInvoiceCreate.Text = "●此檔次販售價格已內含18%娛樂稅，故將不另外開立發票。";
                    else
                    {
                        strNoInvoiceCreateProductName = strNoInvoiceCreateProductName.Substring(0, strNoInvoiceCreateProductName.Length - 1);
                        lbNoInvoiceCreate.Text = "●此檔次商品" + strNoInvoiceCreateProductName + "販售價格已內含18%娛樂稅，故將不另外開立發票。";
                    }
                }

                var lbUse = ((Literal)e.Item.FindControl("lbUse"));
                lbUse.Text =
                @"<div class='editorialArea'>
                <p class='editorial'>品生活憑證使用條款</p>
                <ul style='list-style:decimal outside'>
                <li>依優惠憑證提供該項商品或服務內容者為本網站所簽約合作之店家，本網站則為行銷與銷售服務提供者。</li>
                <li>本憑證不得與其他優惠合併使用，且恕無法兌換現金及找零。</li>
                <li>本憑證不限本人使用，使用時店家將會核對憑證編號，一組編號限用一次。</li>
                <li>本憑證所載之序號，應妥善私密保存。若因憑證遺失或公開序號造成序號被第三方兌換，所造成損失，需由消費者承擔。</li>
                <li>本優惠為品生活與該項商品或服務內容提供者，針對特定時間與特定商品所共同規劃，請務必於兌換優惠期間內依照憑證上所載明之使用方式包括：使用時段、事前預約規定、內用或外帶、每人使用限制等使用完畢。</li>
                <li>您於購買本商品後，系統將引導您列印或以簡訊方式獲得該優惠憑證，屆時您須請您持有該列印後之紙本憑證或手機簡訊前往店家兌換該項商品或服務，店員將會依據您所持有之紙本憑證或簡訊進行序號之核對，請您務必配合店員之核對程序。</li>
                <li>於購買本網站專案商品時，消費者可選擇將購買發票委由本網站捐贈予第三方之公益團體，當然在完成捐贈後，不得要求將該發票退回。</li>
                <li>若優惠專案所指稱之內容為依據特定單一時間與特定場合所提供之服務，例如演唱會或音樂會，當好康憑證持有人因故未能於該特定時間與特定場合兌換該服務，恕無法要求退費或另行補足差額重現該服務。</li>
                <li>優惠憑證不限本人使用，使用時店家將會核對憑證編號，一組編號限用一次，若持有者刻意偽造以獲取各項商品或服務，將可能觸犯中華民國刑事訴訟法中詐欺罪等相關規定。</li>
                <li>若憑證超過優惠期限未使用，可向本網站提出申請全額退費，作業手續費$0；提醒您儘量於期限內使用完畢。</li>
                <li>若提供商品或服務之店家於消費者使用好康憑證前倒閉或因由而未提供商品/服務，則消費者已支付之金額將由本網站全額退費。</li>
                </ul>
                </div>";
            }
        }

        //分店資訊
        protected void rpt_LocationBinding(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewHiDealStoresInfo)
            {
                ViewHiDealStoresInfo s = (ViewHiDealStoresInfo)e.Item.DataItem;
                Literal clit_L_StoreName = ((Literal)e.Item.FindControl("lit_L_StoreName"));//店名
                Literal clit_L_Contents = ((Literal)e.Item.FindControl("lit_L_Contents"));//相關內容
                Literal clit_L_Map = ((Literal)e.Item.FindControl("lit_L_Map"));//地圖

                clit_L_StoreName.Text = s.StoreName;
                if (!string.IsNullOrEmpty(s.AddressString))
                {
                    string address = CityManager.CityTownShopStringGet(s.TownshipId == null ? -1 : s.TownshipId.Value) + s.AddressString;
                    clit_L_Contents.Text += "地址：" + address + "<br>";

                    string latitude = string.Empty;
                    string longitude = string.Empty;
                    if (confProv.EnableOpenStreetMap)
                    {
                        SqlGeography geo = LocationFacade.GetGeographyByCoordinate(s.Coordinate);
                        if (geo.Lat.IsNull || geo.Lat == 0 || geo.Long.IsNull || geo.Long == 0)
                        {
                            geo = LocationFacade.GetGeography(address);
                        }
                        if (geo != null && !geo.IsNull)
                        {
                            latitude = geo.Lat.ToString();
                            longitude = geo.Long.ToString();
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(latitude) && !string.IsNullOrWhiteSpace(longitude))
                    {
                        clit_L_Map.Text = "<h3  class='Map' address='" + address + "' longitude='" + longitude + "' latitude='" + latitude + "' onclick='showmap(this, \"" + confProv.EnableOpenStreetMap + "\");' style='cursor:pointer;'></h3>";
                    }
                }

                if (!string.IsNullOrEmpty(s.Phone))
                {
                    clit_L_Contents.Text += "電話：" + s.Phone + "<br>";
                }

                if ((!string.IsNullOrEmpty(s.UseTime)) && (!s.UseTime.Trim().Equals("<br />")))
                {
                    clit_L_Contents.Text += "兌換時間：<br>" + s.UseTime.Replace("\n", "<br />") + "<br>";
                }
                else if ((!string.IsNullOrEmpty(s.OpenTime)) && (!s.OpenTime.Trim().Equals("<br />")))
                {
                    clit_L_Contents.Text += "兌換時間：<br>" + s.OpenTime + "<br>";
                }

                if (!string.IsNullOrEmpty(s.CloseDate))
                {
                    clit_L_Contents.Text += "公休時間：<br>" + s.CloseDate + "<br/>";
                }

                if (!string.IsNullOrEmpty(s.OtherVehicles))
                {
                    clit_L_Contents.Text += "交通方式：<br>" + s.OtherVehicles + "<br/>";
                }

                if (!string.IsNullOrWhiteSpace(s.WebUrl))
                {
                    clit_L_Contents.Text += "官網：<a href='" + s.WebUrl + "' target='_blank'>" + s.WebUrl + "</a><br/>";
                }

                if (!string.IsNullOrWhiteSpace(s.FacebookUrl))
                {
                    clit_L_Contents.Text += "Facebook：<a href='" + s.FacebookUrl + "' target='_blank'>" + s.FacebookUrl + "</a><br/>";
                }

                if (!string.IsNullOrWhiteSpace(s.PlurkUrl))
                {
                    clit_L_Contents.Text += "Plurk：<a href='" + s.PlurkUrl + "' target='_blank'>" + s.PlurkUrl + "</a><br/>";
                }

                if (!string.IsNullOrWhiteSpace(s.PlurkUrl))
                {
                    clit_L_Contents.Text += "Blog：<a href='" + s.BlogUrl + "' target='_blank'>" + s.BlogUrl + "</a><br/>";
                }
            }
        }

        public void RedirectToDefault()
        {
            Response.Redirect("~/piinlife/default.aspx");
        }

        protected void CheckEventEmail()
        {
            HttpCookie cookie = Request.Cookies["PiinLifeSubscriptionCookie"];
            if (Page.User != null && Page.User.Identity.IsAuthenticated)
            {
                if (!HiDealMailFacade.IsHiDealSubscribe(Page.User.Identity.Name))
                {
                    ShowEventEmail = cookie == null;
                }
                else
                {
                    ShowEventEmail = false;
                }
            }
            else
            {
                ShowEventEmail = cookie == null;
            }
        }

        protected string RenderCategoryBar()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat(@"<h3>
                            <span><a href='default.aspx?regid={0}'>
                            {1}</a></span></h3>", MasterCurrentRegionId, BreadcrumbCityName);

            sb.AppendLine("<div class='right_arrow'>&gt;</div>");

            if (this.DealSpecialType == HiDealSpecialDiscountType.NotVisa ||
                (this.DealSpecialType == HiDealSpecialDiscountType.VisaPriority && (DateTime.Now - this.DearStartTime).Days >= 3))
            {
                sb.AppendFormat(@"<h3>
                            <span><a href='default.aspx?regid={0}&category={1}'>
                            {2}</a></span>
                        </h3>", MasterCurrentRegionId, BreadcrumbCategoryId, BreadcrumbCategoryName);
            }
            else if (this.DealSpecialType == HiDealSpecialDiscountType.VisaPrivate)
            {
                sb.AppendFormat(@"<h3>
                            <span><a href='default.aspx?regid={0}&special={1}'>
                            {2}</a></span>
                        </h3>", MasterCurrentRegionId, (int)HiDealSpecialDiscountType.VisaPrivate,
                        SystemCodeManager.GetSystemCodeNameByEnumValue(HiDealSpecialDiscountType.VisaPrivate));
            }
            else if (this.DealSpecialType == HiDealSpecialDiscountType.VisaPriority)
            {
                sb.AppendFormat(@"<h3>
                            <span><a href='default.aspx?regid={0}&special={1}'>
                            {2}</a></span>
                        </h3>", MasterCurrentRegionId, (int)HiDealSpecialDiscountType.VisaPriority,
                        SystemCodeManager.GetSystemCodeNameByEnumValue(HiDealSpecialDiscountType.VisaPriority));
            }
            sb.AppendLine("<div class='right_arrow'>&gt;</div>");

            sb.AppendFormat("<h3><span>{0}</span></h3>", DealShortName);

            return sb.ToString();
        }

        private string GetEventTagHtml()
        {
            return @"<p class=""event_sp""></p>";
        }

        private string GetBuyUseTagHtml()
        {
            return @"<p class=""use_sp""></p>";
        }

        private string GetValentinesTagHtml()
        {
            return @"<p class='valentines_sp'><svg><image xlink:href='../Themes/HighDeal/images/index/valentines_sp.svg' src='../Themes/HighDeal/images/index/valentines_sp.jpg' width='80' height='22' /></svg></p>";
        }

        private string GetFatherTagHtml()
        {
            return @"<p class='father_sp'><svg><image xlink:href='../Themes/HighDeal/images/index/father_sp.svg' src='../Themes/HighDeal/images/index/father_sp.png' width='80' height='22' /></svg></p>";
        }

        private string GetMoonTagHtml()
        {
            return @"<p class='moon_sp'><svg><image xlink:href='../Themes/HighDeal/images/index/moon_sp.svg' src='../Themes/HighDeal/images/index/moon_sp.png' width='80' height='22' /></svg></p>";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.piinlife
{
    public partial class returned : BasePage, IHiDealReturnedView
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!_presenter.OnViewInitialized())
                {
                    Response.Redirect("~/error-sgo.aspx");
                    return;
                }
            }
            _presenter.OnViewLoaded();
        }

        #region Event

        public event EventHandler<DataEventArgs<String>> OrderIdInput = null;
        public event EventHandler<DataEventArgs<HiDealReturnedViewReturnOrderData>> ReasonInput = null;
        public event EventHandler<DataEventArgs<HiDealReturnedViewReturnOrderData>> CouponReturn = null;
        public event EventHandler<DataEventArgs<HiDealReturnedViewReturnOrderData>> GoodsReturn = null;
        public event EventHandler<DataEventArgs<HiDealReturnedViewReturnOrderData>> ToCouponRefund = null;
        public event EventHandler<DataEventArgs<HiDealReturnedViewReturnOrderData>> ToGoodsRefund = null;
        public event EventHandler<DataEventArgs<HiDealReturnedViewReturnOrderData>> CouponCashBackConfirm = null;
        public event EventHandler<DataEventArgs<HiDealReturnedViewReturnOrderData>> GoodsCashBackConfirm = null;

        #endregion Event

        #region interface properties

        private HiDealReturnedPresenter _presenter;

        public HiDealReturnedPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get { return this._presenter; }
        }

        public string UserName
        {
            get { return User.Identity.Name; }
        }

        public int UserId
        {
            get { return MemberFacade.GetUniqueId(User.Identity.Name); }
        }

        public string RefundFormUrl
        {
            get { return ResolveClientUrl("~/service/hidealrefundform.ashx"); }
        }

        public bool IsRefundCashOnly
        {
            get { return (ViewState["isRefundCashOnly"] != null) ? (bool)ViewState["isRefundCashOnly"] : false; }
            set { ViewState["isRefundCashOnly"] = value; }
        }

        public bool IsCreateEinvoice
        {
            get { return (ViewState["isCreateEinvoice"] != null) ? (bool)ViewState["isCreateEinvoice"] : false; }
            set { ViewState["isCreateEinvoice"] = value; }
        }

        public bool IsCreditCardPay
        {
            get { return (ViewState["IsCreditCardPay"] != null) ? (bool)ViewState["IsCreditCardPay"] : false; }
            set { ViewState["IsCreditCardPay"] = value; }
        }

        static ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public ISysConfProvider SystemConfig
        {
            get { return cp; }
        }
        #endregion interface properties


        #region interface method
        public void RedirectToLogin()
        {
            FormsAuthentication.RedirectToLoginPage("lt=1");
        }

        public void ShowOrderIdInputError(string errorMessage)
        {
            SetPageWorkType(HiDealReturnedViewWorkType.OrderIdInput);

            litOrderIdError.Text = errorMessage;
        }

        public void ShowReturnReasonPage(HiDealReturnedViewReturnOrderData data, string message)
        {
            ShowPageDefaultWork(data);
            litReasonPageMessage.Text = message;

            SetPageWorkType(HiDealReturnedViewWorkType.ReasonInput);
        }

        /// <summary>
        /// 轉只到憑證退貨頁
        /// </summary>
        public void ShowCouponReturnPage(HiDealReturnedViewReturnOrderData data)
        {
            ShowPageDefaultWork(data);

            //刷退連結是否顯示
            lkbToCouponCashBack.Visible = IsCreditCardPay;

            if (IsRefundCashOnly)
            {
                pnBackBtnToshop.Visible = false;
                SetPageWorkType(HiDealReturnedViewWorkType.CouponCashBack);
            }
            else
            {
                pnBackBtnToshop.Visible = true;
                SetPageWorkType(HiDealReturnedViewWorkType.CouponReturn);
            }
        }
        /// <summary>
        /// 顯示憑證退回購物金完成的畫面
        /// </summary>
        public void ShowCouponPointBackSuccess(HiDealReturnedViewReturnOrderData data, HiDealReturned returned)
        {
            ShowPageDefaultWork(data);
            SetPageWorkType(HiDealReturnedViewWorkType.CouponPointBackSuccess);
            phCouponPointBackShowDiscountSingle.Visible = IsCreateEinvoice;
            var pageId = HiDealReturnedManager.GetRefundFormPageId(returned.Id, data.OrderPk, UserName);
            RefundFormUrlShow = SystemConfig.SiteUrl + "/service/hidealrefundform.ashx?FormType=RAF&id=" + pageId;
            DiscountSingleFormUrlShow = SystemConfig.SiteUrl + "/service/hidealrefundform.ashx?FormType=DSF&id=" + pageId;

        }
        /// <summary>
        /// 顯示憑證刷退申請畫面
        /// </summary>
        /// <param name="data"></param>
        public void ShowCouponCashBack(HiDealReturnedViewReturnOrderData data)
        {
            ShowPageDefaultWork(data);
            SetPageWorkType(HiDealReturnedViewWorkType.CouponCashBack);
        }
        /// <summary>
        /// 顯示憑證刷退申請完成頁
        /// </summary>
        /// <param name="data"></param>
        /// <param name="returned"></param>
        public void ShowCouponCashBackSuccess(HiDealReturnedViewReturnOrderData data, HiDealReturned returned)
        {
            ShowPageDefaultWork(data);
            SetPageWorkType(HiDealReturnedViewWorkType.CouponCashBackSuccess);
            phCouponCashBackShowDiscountSingle.Visible = IsCreateEinvoice;
            var pageId = HiDealReturnedManager.GetRefundFormPageId(returned.Id, data.OrderPk, UserName);
            RefundFormUrlShow = SystemConfig.SiteUrl + "/service/hidealrefundform.ashx?FormType=RAF&id=" + pageId;
            DiscountSingleFormUrlShow = SystemConfig.SiteUrl + "/service/hidealrefundform.ashx?FormType=DSF&id=" + pageId;
        }
        /// <summary>
        /// 顯示憑證刷退完成頁-結檔前
        /// </summary>
        /// <param name="data"></param>
        /// <param name="returned"></param>
        public void ShowCouponCashBackSuccessBeforeClose(HiDealReturnedViewReturnOrderData data, HiDealReturned returned)
        {
            ShowPageDefaultWork(data);
            SetPageWorkType(HiDealReturnedViewWorkType.CouponCashBackSuccessBeforeClose);
            var pageId = HiDealReturnedManager.GetRefundFormPageId(returned.Id, data.OrderPk, UserName);
            RefundFormUrlShow = SystemConfig.SiteUrl + "/service/hidealrefundform.ashx?FormType=RAF&id=" + pageId;
            DiscountSingleFormUrlShow = SystemConfig.SiteUrl + "/service/hidealrefundform.ashx?FormType=DSF&id=" + pageId;

        }

        /// <summary>
        /// 轉址到商品退貨頁
        /// </summary>
        public void ShowGoodsReturnPage(HiDealReturnedViewReturnOrderData data)
        {
            ShowPageDefaultWork(data);

            //刷退連結是否顯示
            lkbToGoodsCashBack.Visible = IsCreditCardPay;

            SetPageWorkType(HiDealReturnedViewWorkType.GoodsReturn);
            if (IsRefundCashOnly)
            {
                pnBackBtnTohome.Visible = false;
                SetPageWorkType(HiDealReturnedViewWorkType.GoodsCashBack);
            }
            else
            {
                pnBackBtnTohome.Visible = true;
                SetPageWorkType(HiDealReturnedViewWorkType.GoodsReturn);
            }
        }
        /// <summary>
        /// 顯示商品退回購物金完成的畫面
        /// </summary>
        public void ShowGoodsPointBackSuccess(HiDealReturnedViewReturnOrderData data, HiDealReturned returned)
        {
            ShowPageDefaultWork(data);
            SetPageWorkType(HiDealReturnedViewWorkType.GoodsPointBackSuccess);
            phGoodsPointBackShowDiscountSingle.Visible = IsCreateEinvoice;
            var pageId = HiDealReturnedManager.GetRefundFormPageId(returned.Id, data.OrderPk, UserName);
            RefundFormUrlShow = SystemConfig.SiteUrl + "/service/hidealrefundform.ashx?FormType=RAF&id=" + pageId;
            DiscountSingleFormUrlShow = SystemConfig.SiteUrl + "/service/hidealrefundform.ashx?FormType=DSF&id=" + pageId;
        }
        /// <summary>
        /// 顯示商品刷退申請畫面
        /// </summary>
        /// <param name="data"></param>
        public void ShowGoodsCashBack(HiDealReturnedViewReturnOrderData data)
        {
            ShowPageDefaultWork(data);
            SetPageWorkType(HiDealReturnedViewWorkType.GoodsCashBack);
        }
        /// <summary>
        /// 顯示商品刷退完成頁
        /// </summary>
        /// <param name="data"></param>
        /// <param name="returned"></param>
        public void ShowGoodsCashBackSuccess(HiDealReturnedViewReturnOrderData data, HiDealReturned returned)
        {
            ShowPageDefaultWork(data);
            SetPageWorkType(HiDealReturnedViewWorkType.GoodsCashBackSuccess);
            phGoodsCashBackShowDiscountSingle.Visible = IsCreateEinvoice;
            var pageId = HiDealReturnedManager.GetRefundFormPageId(returned.Id, data.OrderPk, UserName);
            RefundFormUrlShow = SystemConfig.SiteUrl + "/service/hidealrefundform.ashx?FormType=RAF&id=" + pageId;
            DiscountSingleFormUrlShow = SystemConfig.SiteUrl + "/service/hidealrefundform.ashx?FormType=DSF&id=" + pageId;
        }
        /// <summary>
        /// 顯示商品刷退完成頁-未結檔
        /// </summary>
        /// <param name="data"></param>
        /// <param name="returned"></param>
        public void ShowGoodsCashBackSuccessBeforeClose(HiDealReturnedViewReturnOrderData data, HiDealReturned returned)
        {
            ShowPageDefaultWork(data);
            SetPageWorkType(HiDealReturnedViewWorkType.GoodsCashBackSuccessBeforeClose);
            var pageId = HiDealReturnedManager.GetRefundFormPageId(returned.Id, data.OrderPk, UserName);
            RefundFormUrlShow = SystemConfig.SiteUrl + "/service/hidealrefundform.ashx?FormType=RAF&id=" + pageId;
            DiscountSingleFormUrlShow = SystemConfig.SiteUrl + "/service/hidealrefundform.ashx?FormType=DSF&id=" + pageId;

        }
        #endregion interface method

        #region class properties
        public string OrderIdShow { get; set; }
        public string RefundFormUrlShow { get; set; }
        public string DiscountSingleFormUrlShow { get; set; }
        #endregion class properties

        #region class method
        public void ShowPageDefaultWork(HiDealReturnedViewReturnOrderData data)
        {
            hdOrderId.Value = data.OrderId;
            OrderIdShow = data.OrderId;
            SetReturnOrderDataToSession(data, data.OrderId);
        }
        public void SetPageWorkType(HiDealReturnedViewWorkType workType)
        {
            plOrderIdInput.Visible = false;
            plReasonInput.Visible = false;
            plCouponReturn.Visible = false;
            plCouponPointBackSuccess.Visible = false;
            plCouponCashBack.Visible = false;
            plCouponCashBackSuccess.Visible = false;
            plCouponCashBackSuccessBeforeClose.Visible = false;
            plGoodsReturn.Visible = false;
            plGoodsPointBackSuccess.Visible = false;
            plGoodsCashBack.Visible = false;
            plGoodsCashBackSuccess.Visible = false;
            plGoodsCashBackSuccessBeforeClose.Visible = false;

            switch (workType)
            {
                case HiDealReturnedViewWorkType.OrderIdInput:
                    plOrderIdInput.Visible = true;
                    break;
                case HiDealReturnedViewWorkType.ReasonInput:
                    plReasonInput.Visible = true;
                    break;
                case HiDealReturnedViewWorkType.CouponReturn:
                    plCouponReturn.Visible = true;
                    break;
                case HiDealReturnedViewWorkType.CouponPointBackSuccess:
                    plCouponPointBackSuccess.Visible = true;
                    break;
                case HiDealReturnedViewWorkType.CouponCashBack:
                    plCouponCashBack.Visible = true;
                    break;
                case HiDealReturnedViewWorkType.CouponCashBackSuccess:
                    plCouponCashBackSuccess.Visible = true;
                    break;
                case HiDealReturnedViewWorkType.CouponCashBackSuccessBeforeClose:
                    plCouponCashBackSuccessBeforeClose.Visible = true;
                    break;
                case HiDealReturnedViewWorkType.CouponAtmBack:
                    break;
                case HiDealReturnedViewWorkType.CouponAtmDataInput:
                    break;
                case HiDealReturnedViewWorkType.CouponAtmDataCheck:
                    break;
                case HiDealReturnedViewWorkType.CouponAtmBackSuccess:
                    break;
                case HiDealReturnedViewWorkType.GoodsReturn:
                    plGoodsReturn.Visible = true;
                    break;
                case HiDealReturnedViewWorkType.GoodsPointBackSuccess:
                    plGoodsPointBackSuccess.Visible = true;
                    break;
                case HiDealReturnedViewWorkType.GoodsCashBack:
                    plGoodsCashBack.Visible = true;
                    break;
                case HiDealReturnedViewWorkType.GoodsCashBackSuccess:
                    plGoodsCashBackSuccess.Visible = true;
                    break;
                case HiDealReturnedViewWorkType.GoodsCashBackSuccessBeforeClose:
                    plGoodsCashBackSuccessBeforeClose.Visible = true;
                    break;
                case HiDealReturnedViewWorkType.GoodsAtmBack:
                    break;
                case HiDealReturnedViewWorkType.GoodsAtmDataInput:
                    break;
                case HiDealReturnedViewWorkType.GoodsAtmDataCheck:
                    break;
                case HiDealReturnedViewWorkType.GoodsAtmBackSuccess:
                    break;

            }
        }
        /// <summary>
        /// 儲存退貨作業中的暫存資料
        /// </summary>
        /// <param name="data"></param>
        /// <param name="orderId"></param>
        public void SetReturnOrderDataToSession(HiDealReturnedViewReturnOrderData data, string orderId)
        {
            Session[HiDealSession.HiDealReturnData + "_" + orderId] = data;
        }
        /// <summary>
        /// 取出退貨作業中的暫存資料
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public HiDealReturnedViewReturnOrderData GetReturnOrderDataFormSession(string orderId)
        {
            if (Session[HiDealSession.HiDealReturnData + "_" + orderId] == null)
            {
                return new HiDealReturnedViewReturnOrderData();
            }
            return (HiDealReturnedViewReturnOrderData)Session[HiDealSession.HiDealReturnData + "_" + orderId];
        }

        public void OnOrderIdInput(object sender, object e)
        {
            if (OrderIdInput != null)
            {
                var orderId = tbOrderId.Text.Trim();
                OrderIdInput(this, new DataEventArgs<string>(orderId));
            }
        }

        public void OnReasonInput(object sender, object e)
        {
            if (ReasonInput != null)
            {
                var data = GetReturnOrderDataFormSession(hdOrderId.Value);
                data.Reason = tbReason.Text.Trim();
                ReasonInput(this, new DataEventArgs<HiDealReturnedViewReturnOrderData>(data));
            }
        }

        public void OnCouponReturn(object sender, object e)
        {
            if (CouponReturn != null)
            {
                var data = GetReturnOrderDataFormSession(hdOrderId.Value);
                CouponReturn(this, new DataEventArgs<HiDealReturnedViewReturnOrderData>(data));
            }
        }

        public void OnGoodsReturn(object sender, object e)
        {
            if (GoodsReturn != null)
            {
                var data = GetReturnOrderDataFormSession(hdOrderId.Value);
                GoodsReturn(this, new DataEventArgs<HiDealReturnedViewReturnOrderData>(data));
            }
        }

        public void OnToCouponRefund(object sender, object e)
        {
            if (ToCouponRefund != null)
            {
                var data = GetReturnOrderDataFormSession(hdOrderId.Value);
                ToCouponRefund(this, new DataEventArgs<HiDealReturnedViewReturnOrderData>(data));
            }
        }
        public void OnToGoodsRefund(object sender, object e)
        {
            if (ToGoodsRefund != null)
            {
                var data = GetReturnOrderDataFormSession(hdOrderId.Value);
                ToGoodsRefund(this, new DataEventArgs<HiDealReturnedViewReturnOrderData>(data));
            }
        }

        public void OnCouponCashBackConfirm(object sender, object e)
        {
            if (CouponCashBackConfirm != null)
            {
                var data = GetReturnOrderDataFormSession(hdOrderId.Value);
                CouponCashBackConfirm(this, new DataEventArgs<HiDealReturnedViewReturnOrderData>(data));
            }
        }

        public void OnGoodsCashBackConfirm(object sender, object e)
        {
            if (GoodsCashBackConfirm != null)
            {
                var data = GetReturnOrderDataFormSession(hdOrderId.Value);
                GoodsCashBackConfirm(this, new DataEventArgs<HiDealReturnedViewReturnOrderData>(data));
            }
        }
        #endregion class method
    }
}
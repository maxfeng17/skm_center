﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReceiptPrint.aspx.cs" Inherits="LunchKingSite.Web.piinlife.ReceiptPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PiinlifeReceipt</title>
    <link href="../Themes/HighDeal/images/Pay/HDReceipt.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="Receipt" id="receiptContainer" runat="server">
            <div class="Top">
              <div class="logo"></div>
              <div class="title">收據</div>              
            </div>
            <div class="Main">
              <div class="dealdetail">
              <p>
              訂單編號：<asp:Literal ID="orderId" runat="server" /><br />
              優惠名稱：<asp:Literal ID="eventName" runat="server" /><br />
              單價：<asp:Literal ID="itemUnitPrice" runat="server" />元<br />
              數量：<asp:Literal ID="quantity" runat="server" /><br />
              </p>
              </div>
              <div class="paydetail">
                <ul>
                  <li>
                    <div class="col30">付款方式：</div>
                    <div class="col70">&nbsp;</div>
                    <div class="clear"></div>
                  </li>
                  <li id="creditCardPaymentContainer" runat="server">
                    <div class="col30">&nbsp;</div>
                    <div class="col70">刷卡：<asp:Literal ID="creditCardAmount" runat="server" />元</div>
                    <div class="clear"></div>
                  </li>
                  <li id="atmPaymentContainer" runat="server">
                    <div class="col30">&nbsp;</div>
                    <div class="col70">ATM：<asp:Literal ID="atmAmount" runat="server" />元</div>
                    <div class="clear"></div>
                  </li>
                  <li id="pcashPaymentContainer" runat="server">
                    <div class="col30">&nbsp;</div>
                    <div class="col70">PayEasy購物金：<asp:Literal ID="pcashAmount" runat="server" />元</div>
                    <div class="clear"></div>
                  </li>
                  <li id="scashPaymentContainer" runat="server">
                    <div class="col30">&nbsp;</div>
                    <div class="col70">購物金：<asp:Literal ID="scashAmount" runat="server" />元</div>
                    <div class="clear"></div>
                  </li>
                  <li id="bcashPaymentContainer" runat="server">
                    <div class="col30">&nbsp;</div>
                    <div class="col70">紅利金：<asp:Literal ID="bcashAmount" runat="server" />元</div>
                    <div class="clear"></div>
                  </li>
                  <li id="dcashPaymentContainer" runat="server">
                    <div class="col30">&nbsp;</div>
                    <div class="col70">17Life折價券：<asp:Literal ID="dcashAmount" runat="server" />元</div>
                    <div class="clear"></div>
                  </li>
                  <li>
                    <div class="col30">付款金額：</div>
                    <div class="col70">
                        <asp:Literal ID="totalAmount" runat="server" />元整
                        <span id="freightContainer" runat="server" visible="false">
                            (含運費<asp:Literal ID="freightAmount" runat="server"/>元)
                        </span>
                    </div>
                    <div class="clear"></div>
                  </li>
                  <li>
                    <div class="col30">付款日期：</div>
                    <div class="col70"><asp:Literal ID="paymentDate" runat="server" /></div>
                    <div class="clear"></div>
                  </li>
                </ul>
              </div>
              <div class="clear"></div>
            </div>
            <div class="Notification">
                <p>
                提醒您，此收據為消費者留底之用，而非發票。<br />
                17Life將於憑證使用／商品購買完成後開立發票給您；但若使用已開立過發票的17Life購物金或PayEasy購物金付款，將不再開立。申請三聯式發票者，發票於品生活結檔後第10個工作天以郵局平信寄出，請您不用擔心。
                </p>
            </div>
        </div>
    </form>
</body>
</html>

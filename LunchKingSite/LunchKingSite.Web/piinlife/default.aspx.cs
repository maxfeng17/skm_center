﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.Web.piinlife.member;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.piinlife
{
    public partial class _default : BasePage, IPiinlifeListView
    {
        #region props

        public static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        private PiinlifeListPresenter _presenter;

        public PiinlifeListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public CategoryNode Channel
        {
            get
            {
                return CategoryManager.Default.Piinlife;
            }
        }

        public int? CategoryId
        {
            get
            {
                int cat;
                string cat_querystring = string.Empty;
                if (!string.IsNullOrWhiteSpace(Request.QueryString["cat"]))
                {
                    cat_querystring = Request.QueryString["cat"];
                }
                if (!string.IsNullOrWhiteSpace(cat_querystring) && int.TryParse(cat_querystring, out cat) && cat != 0)
                {
                    return cat;
                }
                else
                {
                    return null;
                }
            }
        }

        public Guid BusinessHourGuid
        {
            get
            {
                string bid_querystring = string.Empty;
                if (Page.RouteData.Values["bid"] != null)
                {
                    bid_querystring = Page.RouteData.Values["bid"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["bid"]))
                {
                    bid_querystring = Request.QueryString["bid"];
                }

                Guid bid = Guid.Empty;
                if (!string.IsNullOrWhiteSpace(bid_querystring))
                {
                    if (!Guid.TryParse(bid_querystring, out bid))
                    {
                        bid = Guid.Empty;
                    }
                }
                else if (ViewState["bid"] != null)
                {
                    bid = new Guid(ViewState["bid"].ToString());
                }
                return bid;
            }
        }

        public bool ShowEventEmail { get; set; }

        public bool IsMobileBroswer
        {
            get
            {
                return CommonFacade.ToMobileVersion();
            }
        }

        #endregion Property

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckEventEmail();
                SetMetadata();
                this.Presenter.OnViewInitialized();
                Master.PponNews.CityId = Channel.CityId.Value;
                Master.PponNews.Type = RandomCmsType.PponRandomCms;
            }
            this.Presenter.OnViewLoaded();
        }

        protected void rptMultiDeals_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<MultipleMainDealPreview, IList<ViewComboDeal>>)
            {
                KeyValuePair<MultipleMainDealPreview, IList<ViewComboDeal>> dataItem = 
                    (KeyValuePair<MultipleMainDealPreview, IList<ViewComboDeal>>)e.Item.DataItem;

                Literal liEventName = (Literal)e.Item.FindControl("liEventName");
                Literal liEventTitle = (Literal)e.Item.FindControl("liEventTitle");
                Repeater rptSubDeals = (Repeater)e.Item.FindControl("rptSubDeals");
                Image img = (Image)e.Item.FindControl("img");
                Literal liSoldOut = (Literal)e.Item.FindControl("liSoldOut");

                PlaceHolder phInstallmentInfo = (PlaceHolder)e.Item.FindControl("phInstallmentInfo");
                MultiView mvButton = (MultiView)e.Item.FindControl("mvButton");

                liEventName.Text = dataItem.Key.PponDeal.EventName;
                liEventTitle.Text = dataItem.Key.PponDeal.EventTitle;
                rptSubDeals.DataSource = dataItem.Value;
                rptSubDeals.DataBind();

                Literal liIcon = (Literal)e.Item.FindControl("liIcon");
                liIcon.Text = PponFacade.GetPiinlifeDealIconHtmlContent(dataItem.Key.PponDeal, 3);
                img.ImageUrl = ImageFacade.GetMediaPathsFromRawData(dataItem.Key.PponDeal.EventImagePath, MediaType.PponDealPhoto)
                                .DefaultIfEmpty(config.SiteUrl + "/Themes/HighDeal/images/index/img.png").First();
                if (dataItem.Key.PponDeal.OrderedQuantity >= dataItem.Key.PponDeal.OrderTotalLimit)
                {
                    liSoldOut.Text = "<div class='image soldout'><div class='Soldout_HD_RDL'><a href='" + string.Format(ResolveUrl(@"~/piinlife/{0}/{1}"), CategoryId, dataItem.Key.PponDeal.BusinessHourGuid) + "'><img src='../../Themes/HighDeal/images/index/SoldOut.png'></div><div class='offer_mosaic_image largeSoldout' width='700' height='280'></a></div></div>";
                }

                if (config.InstallmentPayEnabled && dataItem.Key.PponDeal.Installment3months.GetValueOrDefault())
                {
                    phInstallmentInfo.Visible = true;
                    mvButton.ActiveViewIndex = 1;
                }
                else
                {
                    phInstallmentInfo.Visible = false;
                    mvButton.ActiveViewIndex = 0;
                }
            }
        }

        protected void repSubscribeDealContent_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                KeyValuePair<Guid, string[]> item = (KeyValuePair<Guid, string[]>)e.Item.DataItem;
                Image imgDeal = (Image)e.Item.FindControl("imgDeal");

                int limitLength = 28;
                string url = item.Value[0];
                string title = item.Value[1];
                string desc = item.Value[2];
                imgDeal.ImageUrl = url;
                imgDeal.AlternateText = title;
                imgDeal.Attributes.Add("title", desc.Length < limitLength ? desc.Substring(0, desc.Length) : desc.Substring(0, limitLength) + "...");
            }
        }

        #endregion

        #region interface methods

        public void SetSubscribeDealContent(Dictionary<Guid, string[]> imgUrls)
        {
            repSubscribeDealContent.DataSource = imgUrls;
            repSubscribeDealContent.DataBind();
        }

        public void SetMultipleCategories(List<CategoryDealCount> categoryDealCounts, List<CategoryDealCount> filterNodes)
        {
            // 過濾沒有檔次的分類
            categoryDealCounts = categoryDealCounts.Where(x => x.DealCount > 0).ToList();
            filterNodes = filterNodes.Where(x => x.DealCount > 0).ToList();

            // 第一個分類為全部，從第二個位置insert即買即用等特殊分類
            if (categoryDealCounts.Count > 0)
            {
                categoryDealCounts.InsertRange(1, filterNodes);

                Func<CategoryDealCount, bool> checkVisa =
                    x => CategoryManager.GetDealLabelSystemCodeByCategoryId(x.CategoryId) == DealLabelSystemCode.Visa;

                rptCategory.DataSource = categoryDealCounts.Where(x => !checkVisa(x));
                rptCategory.DataBind();

                rptVisaCategory.DataSource = categoryDealCounts.Where(x => checkVisa(x));
                rptVisaCategory.DataBind();
            }
        }

        public void SetMutilpMainDeals(Dictionary<MultipleMainDealPreview, IList<ViewComboDeal>> multiplemaindeals)
        {
            rptMultiDeals.DataSource = multiplemaindeals.OrderBy(x => x.Key.Sequence);
            rptMultiDeals.DataBind();
        }

        public void RedirectToPponDefault()
        {
            Response.Redirect(string.Format("{0}/{1}", config.SiteUrl, "ppon/default.aspx"));
        }

        #endregion

        #region private method
        private void SetMetadata()
        {
            Page.Title = string.Format("品生活 - {0}", config.Title); ;
            Page.MetaDescription = "走入屬於您的城市品味，就從17life品生活開始！提供您具時尚品味的會員專屬頂級體驗。";
            Page.MetaKeywords = config.DefaultPiinlifeKeywords;
        }

        private void CheckEventEmail()
        {
            HttpCookie cookie = Request.Cookies[LkSiteCookie.PiinLifeSubscriptionCookie.ToString()];
            if (cookie != null)
            {
                ShowEventEmail = false;
            }
            else if (Page.User.Identity.IsAuthenticated && MemberFacade.SubscribeCheck(Page.User.Identity.Name, PponCityGroup.DefaultPponCityGroup.Piinlife.CategoryId))
            {
                // 若已訂閱則不需跳窗
                ShowEventEmail = false;
            }
            else
            {
                ShowEventEmail = true;
                SetCookie(cookie);
            }
        }

        private void SetCookie(HttpCookie cookie)
        {
            cookie = CookieManager.NewCookie(LkSiteCookie.PiinLifeSubscriptionCookie.ToString());
            cookie.Expires = DateTime.Now.AddDays(7);
            cookie.Value = "1";
            Response.SetCookie(cookie);
        }
        #endregion

        #region WebMethod

        [WebMethod]
        public string GetCategoryIconClass(int categoryId)
        {
            string className = "fa fa-dot fa-lg fa-fw";
            DealLabelSystemCode? code = CategoryManager.GetDealLabelSystemCodeByCategoryId(categoryId);
            if (code.HasValue)
            {
                switch (code.Value)
                {
                    case DealLabelSystemCode.CanBeUsedImmediately:
                        className = "fa fa-bolt fa-lg fa-fw";
                        break;
                    case DealLabelSystemCode.PiinlifeEvent:
                    case DealLabelSystemCode.PiinlifeValentinesEvent:
                    case DealLabelSystemCode.PiinlifeFatherEvent:
                    case DealLabelSystemCode.PiinlifeMoonEvent:
                    case DealLabelSystemCode.PiinlifeMajiEvent:
                        className = "fa fa-star fa-lg";
                        break;
                    case DealLabelSystemCode.PiinlifeRecommend:
                        className = "fa fa-trophy fa-lg fa-fw";
                        break;
                    default:
                        break;
                }
            }
            return className;
        }

        [WebMethod]
        public string GetCategoryBgClass(int categoryId)
        {
            bool seleted = categoryId == (CategoryId ?? 0);
            string className = seleted ? "selected" : string.Empty;
            DealLabelSystemCode? code = CategoryManager.GetDealLabelSystemCodeByCategoryId(categoryId);
            if (code.HasValue)
            {
                switch (code.Value)
                {
                    case DealLabelSystemCode.Visa:
                        className += seleted ? " sp_bg" : " sp";
                        break;
                    default:
                        break;
                }
            }
            return className;
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.piinlife
{
    public partial class VboBanner : BasePage, IVboBannerView
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Presenter.OnViewInitialized();
            }
            else
            {
                Presenter.OnViewLoaded();
            }
        }

        public void FillPreviewData(IList<ViewCmsRandom> items)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < items.Count; i++)
            {
                var item = items[i];
                if (i == 0)
                {
                    sb.Append("<div class='pinnTopBannerItem'>");
                }
                else
                {
                    sb.Append("<div style='display:none'>");
                }
                sb.Append(item.Body);
                sb.AppendLine("</div>");
            }
            li_preview.Text = sb.ToString();             
        }


        public void OutputJsonp(string jsonStr)
        {
            Response.ContentType = "application/json; charset=utf-8";            
            Response.Write(jsonStr);
            Response.Flush();
            Response.End();
        }

        private VboBannerPresenter _presenter;
        public VboBannerPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string OutputFormat
        {
            get { return Request.Params["output"]; }
        }
        /// <summary>
        /// jquery的ajax，當datatype是jsonp時，會自己補callback=xyz(xyz為jquery自己產生)，
        /// 而回傳時格式需符合xyz(json資料)，據說是為了抓跨網域的的資料。
        /// </summary>
        public string CallbackKey
        {
            get { return Request.Params["callback"]; }
        }
    }

}
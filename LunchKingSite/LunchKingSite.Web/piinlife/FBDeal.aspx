﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FBDeal.aspx.cs" MasterPageFile="~/piinlife/hideal.Master"
    Inherits="LunchKingSite.Web.piinlife.FBDeal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptContent" runat="server">
    <link href="<%#ResolveUrl("~/Themes/PCweb/HighDeal/SingleProduct.css")%>" rel="stylesheet" type="text/css" />
    <meta property="fb:app_id" content="<%= FacebookAppId %>" />
    <meta property="og:title" content="<%= FacebookOgTitle %>" />
    <meta property="og:url" content="<%= FacebookOgUrl %>" />
    <meta property="og:image" content="<%= FacebookOgImage %>" />
    <meta property="og:description" content="<%# FacebookOgDescription %>" />
    <link rel="image_src" href="<%= ImageSrcLinkHref %>" />
    <link rel="canonical" href="<%= CanonicalLinkHref %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        var fblink = '<%=confProv.SiteUrl %>/piinlife/deal.aspx?did=<%=did %>';
        location.href = fblink;
    </script>
    <a href="<%=confProv.SiteUrl %>/piinlife/deal.aspx?did=<%=did %>">
    <img src="<%= ImageSrcLinkHref %>" alt="<%= FacebookOgTitle %>"/>
    </a>
    <br/>
    <a href="<%=confProv.SiteUrl %>/piinlife/deal.aspx?did=<%=did %>"><%= FacebookOgTitle %></a>
</asp:Content>

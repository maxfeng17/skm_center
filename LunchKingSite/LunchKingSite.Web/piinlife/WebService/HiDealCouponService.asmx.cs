﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.Web.hideal.WebService
{
    /// <summary>
    /// Summary description for HiDealCouponService
    /// </summary>
    [WebService(Namespace = "http://www.17life.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class HiDealCouponService : System.Web.Services.WebService
    {
        /// <summary>
        /// 傳入商品序號與需要的憑證數量，回傳分配的憑證編號，
        /// 如果沒有該商品或憑證數量不足，則回傳空的List
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        [WebMethod]
        public List<long> CouponIdGetListByProduct(int productId , int quantity)
        {
            return HiDealCouponManager.RequestCouponList(productId, quantity,true);
        }
        /// <summary>
        /// 傳入商品序號、店鋪GUID與需要的憑證數量，回傳分配的憑證編號，
        /// 如果沒有該商品或憑證數量不足，則回傳空的List
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="storeGuid"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        [WebMethod]
        public List<long> CouponIdGetListByProductAndStore(int productId, Guid storeGuid , int quantity)
        {
            return HiDealCouponManager.RequestCouponList(productId, storeGuid, quantity,true);
        }
        /// <summary>
        /// 傳入商品編號，取得該商品目前可發放的coupon數量
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [WebMethod]
        public int GetCouponCountByProduct(int productId)
        {
            return HiDealCouponManager.GetCouponInventoryQuantity(productId,true);
        }
        /// <summary>
        /// 傳入商品編號與分店編號，取得該商品目前可發放的coupon數量
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="storeGuid"></param>
        /// <returns></returns>
        [WebMethod]
        public int GetCouponCountByProductAndStore(int productId,Guid storeGuid)
        {
            return HiDealCouponManager.GetCouponInventoryQuantity(productId, storeGuid,true);
        }

        /// <summary>
        /// 依據productId取出分店的所有庫存資料
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [WebMethod]
        public List<HiDealProductStoreInventory> GetAllStoreHiDealProductStoreInventory(int productId)
        {
            return HiDealCouponManager.GetAllStoreInventory(productId,true);
        }
    }
}

﻿using System;
using System.Web.UI;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Component;
using System.Web.UI.WebControls;
using LunchKingSite.DataOrm;
using System.Web;
using System.Web.Security;

namespace LunchKingSite.Web.piinlife.member
{
    public partial class member : MasterPage
    {

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region properties

        public bool BypassSetNowUrl { get; set; }

        public string FacebookAppId
        {
            get
            {
                return config.FacebookApplicationId;
            }
        }

        public string LinkWebUrl
        {
            get
            {
                return WebUtility.GetSiteRoot() + "/";
            }
        }
        public string WebUrl
        {
            get 
            { 
                return Request.ApplicationPath == "/" ? Request.ApplicationPath : (Request.ApplicationPath + "/"); 
            }
        }

        public int CurrentRegionId
        {
            get
            {
                int defId = HiDealRegionManager.DealOverviewRegion;
                int regId ;

                //第一優先: 判斷 query string  是否有regid
                #region querystring regid 判斷
                if (Request.QueryString["regid"] != null)
                {
                    if (int.TryParse(Request.QueryString["regid"], out regId))
                    {
                        //if (SystemCodeManager.GetSystemCodeListByGroup("HiDealRegion").Find("CodeId", regId) != -1)
                        if (HiDealRegionManager.GetRegionOfShow().Find("CodeId", regId) != -1)
                        {
                            Session[HiDealSession.HiDealRegionId.ToString()] = regId;
                        }
                    }
                }
                else
                {
                    regId = defId;
                }
                #endregion


                return regId;
            }

            set
            {
                Session[HiDealSession.HiDealRegionId.ToString()] = value;
            }
        }


        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!BypassSetNowUrl)
                SetNowUrlIntoSession();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            AboutLink.NavigateUrl = LinkWebUrl + "about/us.html";
            TermsLink.NavigateUrl = LinkWebUrl + "Ppon/PrivacyPolicy.aspx";
            PolicyLink.NavigateUrl = LinkWebUrl + "Ppon/PrivacyGoogle.aspx";
            InvoiceLink.NavigateUrl = LinkWebUrl + "piinlife/InvoiceHosting.aspx";
            NewbieGuideLink.NavigateUrl = LinkWebUrl + "Ppon/NewbieGuide.aspx";
        }

        /// <summary>
        /// 將目前網址寫到Session中
        /// </summary>
        protected void SetNowUrlIntoSession()
        {
            string url = Request.Url.AbsoluteUri;
            if (url.IndexOf("member/") < 0)
            {
                //Session[LkSiteSession.NowUrl.ToString()] = url;
                Session[HiDealSession.PiinLifeNowUrl.ToString()] = url;
            }
        }
    }
}
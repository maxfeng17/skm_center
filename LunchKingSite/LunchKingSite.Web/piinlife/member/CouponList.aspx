﻿<%@ Page Language="C#" MasterPageFile="~/piinlife/hideal.Master" AutoEventWireup="true"
    CodeBehind="CouponList.aspx.cs" Inherits="LunchKingSite.Web.piinlife.member.CouponList"
    Title="訂單明細" %>

<%@ Register Src="~/ControlRoom/Controls/EinvoiceFooterControl.ascx" TagName="EinvoiceFooter" TagPrefix="uc" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ScriptContent" runat="server">
    <link rel="stylesheet" href="../../Themes/HighDeal/images/style/blueprint/screen.css" type="text/css" media="screen, projection" />
    <link href="../../Themes/HighDeal/images/Account/PersonalAccount.css" rel="stylesheet" type="text/css" />
    <link href="../../Themes/HighDeal/images/MyAccount/PopUpWindow.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .MailCoupon
        {
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script language="javascript" type="text/javascript">
        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (regex.test(email)) return true;
            else return false;
        }
        function block_ui(target, iwidth, iheight) {
            unblock_ui();
            $.blockUI({ message: $(target), css: { backgroundcolor: 'transparent', border: 'none', top: ($(window).height()) / 8 + 'px', left: ($(window).width() - 700) / 2 + 'px', width: iwidth, height: iheight } })
        }
        function unblock_ui() {
            $.unblockUI();
            $('#ReasonAlert').hide();
            return false;
        }
        function paperinvoicecheck() {

            var leng1 = $("#<%=tbx_Einvoice_Receiver.ClientID %>").val();
            var leng2 = $("#<%=tbx_Einvoice_Address.ClientID %>").val();
            var receiver = $('#EinvoiceRequestReceiver');
            var message = $('#EinvoiceRequestMessage');

            if (leng1.length > 0 && leng2.length > 0) {
                $("#<%=hidEinvoiceRequestReceiver.ClientID %>").val(leng1);
                $("#<%=hidEinvoiceRequestAddress.ClientID %>").val(leng2);
                receiver.hide();
                message.hide();
                unblock_ui();
                return true;
            }
            else {

                receiver.show();
                message.show();

                if (leng1.length > 0) {
                    receiver.hide();
                } else {
                    receiver.show();
                    receiver.css('color', 'red');
                }

                if (leng2.length > 0) {
                    message.hide();
                } else {
                    message.show();
                    message.css('color', 'red');
                }
                return false;
            }
        }
        function inserteinvoiceinfo() {
            //$('#EinvoiceRequestReceiver').hide();
            //$('#EinvoiceRequestMessage').hide();
            $("#<%=tbx_Einvoice_Receiver.ClientID %>").val($("#<%=lbl_Einvoice_BuyerName.ClientID %>").text());
            $("#<%=tbx_Einvoice_Address.ClientID %>").val($("#<%=lbl_Einvoice_BuyerAddress.ClientID %>").text());
        }
        function getsmsmobile(mode) {
            $("#<%=lab_Pop_SMSMobile.ClientID %>").text($("#<%=hidMobile.ClientID %>").val());
            $("#<%=lab_Pop_SMSMobile2.ClientID %>").text($("#<%=hidMobile.ClientID %>").val());
            $("#<%=lab_Pop_SMSAlreadyCount.ClientID %>").text($("#<%=hidSmsAlreadyCount.ClientID %>").val());
            $("#<%=lab_Pop_SMSRemainCount.ClientID %>").text($("#<%=hidSmsRemainCount.ClientID %>").val());
            $("#<%=lab_Pop_SMSAlreadyCount3.ClientID %>").text($("#<%=hidSmsAlreadyCount.ClientID %>").val());
            $("#<%=lab_Pop_SMSRemainCount3.ClientID %>").text($("#<%=hidSmsRemainCount.ClientID %>").val());
            if (mode == "2") {
                $('#sms_2').show();
                $('#sms_3').hide();
            }
            else if (mode == "3") {
                $('#sms_3').show();
                $('#sms_2').hide();
            }
        }

        $(document).ready(function () {
            var hidealSmsVisible = $("#<%= hidealSmsVisible.ClientID %>").val();
            
            $('.MailCoupon').click(function () {
                var couponId = $(this).attr('couponId');
                $('#btn_MailCoupon').attr('couponId', couponId);
                //block_ui($('#Pop_MailCoupon'), 0, 0);
                unblock_ui();
                $.blockUI({
                    message: $('#Pop_MailCoupon'), css: {
                        backgroundcolor: 'transparent', border: 'none',
                        top: ($(window).height() - 150) / 2 + 'px',
                        left: ($(window).width() - 400) / 2 + 'px',
                        width: 400,
                        height: 0
                    }
                });
                $('#email_pop_content').show();
                $('#email_pop_result').hide();
                $('.email_pop_error_info').hide();
                window.couponMailSent = false;
            });

            $('#btn_MailCoupon').click(function () {
                if (window.couponMailSent) {
                    return;
                } else {
                    window.couponMailSent = true;
                }
                var couponId = $(this).attr('couponId');
                var userEmail = $('#userEmail').val();
                if (isEmail(userEmail) == false) {
                    $('.email_pop_error_info').show();
                    return;
                } else {
                    $('.email_pop_error_info').hide();
                }
                $.ajax({
                    type: "POST",
                    url: "CouponList.aspx/SendCouponMail",
                    data: "{ emailTo: '" + userEmail + "', couponId: " + couponId + " }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        window.couponMailSent = false;
                        if (msg.d == false) {
                            alert('系統繁忙中，請稍後再試!!');
                        } else {
                            //unblock_ui();
                            $('#email_pop_content_email').text(userEmail);
                            $('#email_pop_content').hide();
                            $('#email_pop_result').show();
                        }
                    }, error: function (response, q, t) {
                        if (t == 'userNotLogin') {
                            location.href = '<%=ResolveUrl(FormsAuthentication.LoginUrl) %>';
                        }
                    }
                });

            });
        });

        //下載收據
        function btnDownloadReceipt_Click() {
            window.location.href = $("#<%=hidDownloadReceiptLink.ClientID %>").val();
        }
    </script>
    <section class="AccountContainer">
        <aside class="Offers_Sidebar">
            <nav class="SideBar">
                <ul class="category">
                    <li id="category_all" class="" style="display: block;"><span class="category_icon"></span>
                        <a id="all" class="category" href="/User/UserAccount.aspx">個人資料</a> </li>
                    <li id="Li1" class="first selected"><span class="category_icon02"></span><a
                        id="a1" class="category">訂單查詢</a>
                        <div class="arrow">
                        </div>
                    </li>
                    <li id="Li2" class="" style="display: block;"><span class="category_icon02">
                    </span><a id="a2" class="category" href="/User/BonusListNew.aspx">紅利與購物金</a> </li>
                    <li id="Li3" class="" style="display: block;"><span class="category_icon02">
                    </span><a id="a3" class="category" href="/User/MemberShippingInfo.aspx">收件人管理</a> </li>
                    <li id="Li4" class="" style="display: block;"><span class="category_icon02">
                    </span><a id="a1l" class="category" href="/User/ServiceList">過往客服紀錄</a> </li>
                    <li id="Li5" class="" style="display: block;"><span class="category_icon02">
                    </span><a id="a4" class="category" href="UserPassword.aspx">變更密碼</a> </li>
                </ul>
            </nav>
        </aside>
        <article class="Main">
            <div id="BLeft">
                <asp:Panel ID="pan_Detail" runat="server">
                    <h1 class="padding">
                        <asp:LinkButton ID="lbt_CloseDetail" runat="server" PostBackUrl="~/piinlife/member/OrderList.aspx">訂單列表</asp:LinkButton>
                        <span class="TitleArrow" id="sArrow1" runat="server" visible="false"></span><span
                            id="sAtmPay" runat="server" visible="false">繼續付款</span> <span class="TitleArrow"
                                id="sArrow2" runat="server"></span>訂單編號：
                        <asp:Label ID="lblOrderId" runat="server"></asp:Label>
                        &nbsp;&nbsp;
                        <asp:LinkButton ID="lkService" runat="server" Font-Size="13px">
                        <span class="ButtonBox"><span class="ButtonField_S"><span><span class="BtnName">
                                    <input name="" id="" type="button" value="詢問" class="InputBtn_S"></span></span>
                                </span></span>
                        </asp:LinkButton>
                    </h1>
                    <hr class="Line"></hr>
                    <div class="OrderTableArea">
                        <%--HiddenField--%>
                        <asp:HiddenField ID="hidCouponListVisible" runat="server" />
                        <asp:HiddenField ID="hidPaymentType" runat="server" />
                        <asp:HiddenField ID="hidOrderStatus" runat="server" />
                        <asp:HiddenField ID="hidOrderShowStatus" runat="server" />
                        <asp:HiddenField ID="hidOrderDate" runat="server" />
                        <asp:HiddenField ID="hidOrderEndDate" runat="server" />
                        <asp:HiddenField ID="hidCouponId" runat="server" />
                        <asp:HiddenField ID="hidMobile" runat="server" />
                        <asp:HiddenField ID="hidSmsAlreadyCount" runat="server" />
                        <asp:HiddenField ID="hidSmsRemainCount" runat="server" />
                        <asp:HiddenField ID="hidEinvoiceRequestReceiver" runat="server" />
                        <asp:HiddenField ID="hidProductName" runat="server" />
                        <asp:HiddenField ID="hidEinvoiceRequestAddress" runat="server" />
                        <asp:HiddenField ID="hidealSmsVisible" runat="server" />
                        <asp:HiddenField ID="hidDownloadReceiptLink" runat="server" />
                        <%-- 收據下載連結--%>
                        <%--優惠憑證內容--%>
                        <asp:Panel ID="pan_OrderDetailSequence" runat="server">
                            <div class="OrderTableArea">
                                <table class="OrderTableTb" width="680" border="0" cellspacing="0" cellpadding="0">
                                    <tr class="TableTitle">
                                        <td width="480" colspan="7" style="text-align: left;">
                                            &nbsp;優惠憑證（<asp:Label ID="lblProductName" runat="server"></asp:Label>）
                                        </td>
                                        <td width="200" colspan="2" style="text-align: right;">
                                            <a href="http://get.adobe.com/tw/reader/" target="_blank">
                                                <div class="PDFIcon">
                                                </div>
                                                <div class="TextFix">
                                                    下載憑證專用閱讀器</div>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr class="TableHeader">
                                        <td width="15" style="text-align: center;">
                                            &nbsp;
                                        </td>
                                        <td width="120" style="text-align: center;">
                                            憑證編號
                                        </td>
                                        <td width="90" style="text-align: center;">
                                            使用期限
                                        </td>
                                        <td width="240" colspan="4" style="text-align: center;">
                                            索取憑證
                                        </td>
                                        <td width="95" style="text-align: center;">
                                            憑證狀態
                                        </td>
                                        <td width="120" style="text-align: center;">
                                            <div class="TextFix">
                                                信託狀態</div>
                                            <a onclick="block_ui($('#Pop_WhatIsTrust'),700,0);" style="cursor: pointer">
                                                <div class="HintIcon">
                                                </div>
                                            </a>
                                        </td>
                                    </tr>
                                    <asp:Repeater ID="repCouponList" runat="server" OnItemDataBound="repCouponList_ItemBound"
                                        OnItemCommand="repCouponList_ItemCommand">
                                        <ItemTemplate>
                                            <tr>
                                                <td width="15" class="TableBd" style="text-align: center;">
                                                    <%# (Container.ItemIndex + 1).ToString().PadLeft(2, '0')%>
                                                </td>
                                                <td width="120" class="TableBd" style="text-align: center;">
                                                    <%# ((ViewHiDealCouponListSequence)(Container.DataItem)).Prefix + ((ViewHiDealCouponListSequence)(Container.DataItem)).Sequence%>
                                                </td>
                                                <td width="90" class="TableBd" style="text-align: center;">
                                                    <%# ((ViewHiDealCouponListSequence)(Container.DataItem)).UseEndTime.Value.ToString("yyyy/MM/dd")%>
                                                    <asp:HiddenField ID="hif_rpCouponId" runat="server" Value="<%# ((ViewHiDealCouponListSequence)(Container.DataItem)).Id%>" />
                                                </td>
                                                <td width="60" class="TableBd" style="text-align: center;">
                                                    <asp:HyperLink ID="hyp_rpCouponSequencePrint" runat="server" CssClass="PrinterIcon" Width="30" Target="_blank"></asp:HyperLink>                                                    
                                                    <br />
                                                    <asp:HyperLink ID="hyp_rpCouponSequencePrintText" runat="server" Text="列印" Target="_blank"></asp:HyperLink>
                                                </td>
                                                <td width="60" class="TableBd" style="text-align: center;">
                                                    <asp:HyperLink runat="server" ID="hyp_rpCouponSequenceDownLoad" runat="server" CssClass="DownloadIcon"
                                                        Width="30" Target="_blank">
                                                    </asp:HyperLink>                                                    
                                                    <br />
                                                    <asp:HyperLink ID="hyp_rpCouponSequenceDownLoadText" runat="server" Text="下載" Target="_blank"></asp:HyperLink>
                                                </td>
                                                <td width="60" class="TableBd" style="text-align: center;">
                                                    <asp:HyperLink ID="hyp_email" runat="server" CssClass="EmailIcon"
                                                        Width="30">
                                                    </asp:HyperLink><br />
                                                    <asp:HyperLink ID="hyp_emailtext" runat="server" Text="Email"></asp:HyperLink>
                                                </td>
                                                <td width="60" class="TableBd" style="text-align: center;">
                                                    <asp:LinkButton ID="lbt_rpCouponSequenceSMS" runat="server" CommandArgument="<%# ((ViewHiDealCouponListSequence)(Container.DataItem)).Id%>"
                                                        CommandName="SendMS" CssClass="SMSIcon" Width="31">
                                                    </asp:LinkButton><br />
                                                    <asp:LinkButton ID="lbt_rpCouponSequenceSMSText" runat="server" CommandArgument="<%# ((ViewHiDealCouponListSequence)(Container.DataItem)).Id%>"
                                                        CommandName="SendMS" OnClientClick="return getsmsmobile();" Text="簡訊"></asp:LinkButton>
                                                </td>
                                                <td width="95" class="TableBd" style="text-align: center;">
                                                    <span class="OrderDetailGrayText">
                                                        <asp:Label ID="lab_rpCouponSequenceStatus" runat="server"></asp:Label>
                                                    </span>
                                                </td>
                                                <td width="120" class="TableBd" style="text-align: center;">
                                                    <asp:Label ID="lab_rpCouponSequenceTrustStatus" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>                                      
                                    </asp:Repeater>
                                </table>
                            </div>
                        </asp:Panel>
                        <%--ATM付款前置文字--%>
                        <asp:Panel ID="panOrderDetailAtmParagraph" runat="server" Visible="false">
                            <p>
                                感謝您以ATM付款方式訂購【<span style="color: #F60"><asp:Label ID="lab_ATM_ItemName2" runat="server"></asp:Label></span>】，我們已為您保留訂單。<br>
                                請於付款期限內使用ATM或網路ATM完成付款。
                            </p>
                        </asp:Panel>
                        <asp:Panel ID="pan_ShipInfo" runat="server">
                            <div class="OrderTableArea">
                                <table class="OrderTableTb" width="680" border="0" cellspacing="0" cellpadding="0">
                                    <tr class="TableTitle">
                                        <td width="680" colspan="3" style="text-align:left;">&nbsp;配送資訊</td>
                                    </tr>
                                    <tr class="TableHeader">
                                        <td width="100" style="text-align:left;">收件人</td>
                                        <td width="370" style="text-align:left;">收件地址</td>
                                        <td width="210" style="text-align:left;">廠商出貨資訊</td>
                                    </tr>
                                    <tr class="LineOdd">
                                        <td class="TableBdpaad" style="text-align:left;"><asp:Label ID="lab_RecipientName" runat="server"/></td>
                                        <td class="TableBdpaad" style="text-align:left;"><asp:Label ID="lab_DeliveryAddress" runat="server"/></td>
                                        <td class="TableBdpaad" style="text-align:left;"><asp:Literal ID="lit_ShippedInfo" runat="server"/></td>
                                    </tr>
                                </table>
                            </div>
                        </asp:Panel>
                        <%--訂單明細--%>
                        <asp:Panel ID="pan_OrderDetail" runat="server">
                            <div class="OrderTableArea">
                                <table class="OrderTableTb" width="680" border="0" cellspacing="0" cellpadding="0">
                                    <tr class="TableTitle">
                                        <td width="680" colspan="4" style="text-align: left;">
                                            &nbsp;訂單明細
                                        </td>
                                    </tr>
                                    <tr class="TableHeader">
                                        <td width="400" style="text-align: center;">
                                            商品名稱
                                        </td>
                                        <td width="100" style="text-align: center;">
                                            單價
                                        </td>
                                        <td width="80" style="text-align: center;">
                                            數量
                                        </td>
                                        <td width="100" style="text-align: center;">
                                            小計
                                        </td>
                                    </tr>
                                    <asp:Repeater ID="rpt_Details" runat="server">
                                        <ItemTemplate>
                                            <tr class="LineOdd">
                                                <td width="400" class="TableBd" style="text-align: left;">
                                                    <span style="padding-left: 5px;">
                                                        <%# ((HiDealOrderDetail)(Container.DataItem)).ProductName%>
                                                    </span>
                                                </td>
                                                <td width="100" class="TableBd" style="text-align: center;">
                                                    <span style="padding-left: 5px;">
                                                        <%# ((HiDealOrderDetail)(Container.DataItem)).UnitPrice.ToString("N0")%>
                                                    </span>
                                                </td>
                                                <td width="80" class="TableBd" style="text-align: center;">
                                                    <%# ((HiDealOrderDetail)(Container.DataItem)).ItemQuantity%>
                                                </td>
                                                <td width="100" class="TableBd" style="text-align: right;">
                                                    <span style="padding-right: 5px;">
                                                        <%# ((HiDealOrderDetail)(Container.DataItem)).DetailTotalAmt.ToString("N0")%>
                                                    </span>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <tr class="LineOdd strong">
                                        <td width="580" colspan="3" class="TableBd" style="text-align: right;">
                                            運費：
                                        </td>
                                        <td width="100" class="TableBd" style="text-align: right;">
                                            <asp:Label ID="lab_ODInfo_Ship" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr class="LineOdd strong">
                                        <td width="580" class="TableBd" colspan="3" style="text-align: right;">
                                            抵扣金額：
                                        </td>
                                        <td width="100" class="TableBd" style="text-align: right;">
                                            <span class="OrderDetailPriceText">
                                                <asp:Label ID="lab_ODInfo_DiscountAmount" runat="server"></asp:Label>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="LineOdd strong">
                                        <td width="580" class="TableBd" colspan="3" style="text-align: right;">
                                            刷卡或ATM付款總金額：
                                        </td>
                                        <td width="100" class="TableBd" style="text-align: right;">
                                            <asp:Label ID="lab_ODInfo_TotalAmount" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:Panel>
                        <%--訂單備註--%>
                        <asp:Panel ID="divOrderCategory" runat="server">
                            <div class="OrderTableArea">
                                <asp:Repeater ID="rpProductCategory" runat="server" OnItemDataBound="rpProductCategory_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:Table ID="tbCategory" runat="server" CssClass="OrderTableTb" border="0" CellSpacing="0"
                                            CellPadding="0">
                                            <asp:TableRow CssClass="TableTitle">
                                                <asp:TableCell Width="680" ColumnSpan="2" Style="text-align: left;">
                                            &nbsp;訂單備註
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow CssClass="LineOdd">
                                                <asp:TableCell Width="120" CssClass="TableBd" Style="text-align: left;">
                                                    <asp:Label ID="lblCatgName1" runat="server"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell Width="560" CssClass="TableBd" Style="text-align: left;">
                                                    <asp:Label ID="lblOptionName1" runat="server"></asp:Label>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow CssClass="LineOdd">
                                                <asp:TableCell Width="120" CssClass="TableBd" Style="text-align: left;">
                                                    <asp:Label ID="lblCatgName2" runat="server"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell Width="560" CssClass="TableBd" Style="text-align: left;">
                                                    <asp:Label ID="lblOptionName2" runat="server"></asp:Label>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow CssClass="LineOdd">
                                                <asp:TableCell Width="120" CssClass="TableBd" Style="text-align: left;">
                                                    <asp:Label ID="lblCatgName3" runat="server"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell Width="560" CssClass="TableBd" Style="text-align: left;">
                                                    <asp:Label ID="lblOptionName3" runat="server"></asp:Label>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow CssClass="LineOdd">
                                                <asp:TableCell Width="120" CssClass="TableBd" Style="text-align: left;">
                                                    <asp:Label ID="lblCatgName4" runat="server"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell Width="560" CssClass="TableBd" Style="text-align: left;">
                                                    <asp:Label ID="lblOptionName4" runat="server"></asp:Label>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow CssClass="LineOdd">
                                                <asp:TableCell Width="120" CssClass="TableBd" Style="text-align: left;">
                                                    <asp:Label ID="lblCatgName5" runat="server"></asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell Width="560" CssClass="TableBd" Style="text-align: left;">
                                                    <asp:Label ID="lblOptionName5" runat="server"></asp:Label>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </asp:Panel>
                        <%--付款明細--%>
                        <asp:Panel ID="pan_OrderDetailPayments" runat="server">
                            <div class="OrderTableArea">
                                <table class="OrderTableTb" width="680" border="0" cellspacing="0" cellpadding="0">
                                    <tr class="TableTitle">
                                        <td width="680" colspan="8" style="text-align: left;">
                                            &nbsp;付款明細
                                        </td>
                                    </tr>
                                    <tr class="TableHeader">
                                        <td width="450" colspan="5" style="text-align: center;">
                                            抵扣
                                        </td>
                                        <td width="180" colspan="2" style="text-align: center; background-color: #FFE;">
                                            支付
                                        </td>
                                    </tr>
                                    <tr class="TableHeader">
                                        <td width="110" style="text-align: center;">
                                            PayEasy購物金
                                        </td>
                                        <td width="115" style="text-align: center;">
                                            購物金
                                        </td>
                                        <td width="115" style="text-align: center;">
                                            紅利金
                                        </td>
                                        <td width="140" colspan="2" style="text-align: center;">
                                            折價券
                                        </td>
                                        <td width="100" style="text-align: center; background-color: #FFE;">
                                            刷卡
                                        </td>
                                        <td width="100" style="text-align: center; background-color: #FFE;">
                                            ATM
                                        </td>
                                    </tr>
                                    <tr class="LineOdd">
                                        <td width="130" class="TableBd" style="text-align: right;">
                                            <asp:Label ID="lab_ODInfo_PEZ" runat="server"></asp:Label>
                                        </td>
                                        <td width="100" class="TableBd" style="text-align: right;">
                                            <asp:Label ID="lab_ODInfo_Scach" runat="server"></asp:Label>
                                        </td>
                                        <td width="100" class="TableBd" style="text-align: right;">
                                            <asp:Label ID="lab_ODInfo_Bouns" runat="server"></asp:Label>
                                        </td>
                                        <td width="90" class="TableBd" style="text-align: right;">
                                            <asp:Label ID="lab_ODInfo_DiscountCode" runat="server"></asp:Label>
                                        </td>
                                        <td width="40" class="TableBd" style="text-align: center;">
                                            <asp:Label ID="lab_ODInfo_Discount" runat="server"></asp:Label>
                                        </td>
                                        <td width="100" class="TableBd" style="text-align: right;">
                                            <asp:Label ID="lab_ODInfo_CreditCard" runat="server"></asp:Label>
                                        </td>
                                        <td width="100" class="TableBd" style="text-align: right;">
                                            <asp:Label ID="lab_ODInfo_ATM" runat="server" Text="0"></asp:Label>
                                        </td>
                                        <%--                                                <td width="100" style="text-align: right;">
                                                    <span style="padding-right: 5px;">
                                        --%><asp:Label ID="lab_ODInfo_Total" runat="server" Visible="false"></asp:Label><%--
                                                    </span>
                                                </td>--%>
                                    </tr>
                                </table>
                            </div>
                        </asp:Panel>
                        <%--下載相關表單--%>
                        <div class="OrderTableArea">
                            <table class="OrderTableTb" width="680" border="0" cellspacing="0" cellpadding="0">
                                <tr class="TableTitle">
                                    <td width="680" style="text-align: left;">
                                        &nbsp;下載相關表單
                                    </td>
                                </tr>
                                <tr class="LineOdd">
                                    <td width="680" class="TableBd" style="text-align: center;">
                                        <span class="ButtonBox"><span class="ButtonField_S"><span><span class="BtnName">
                                            <input name="btnDownloadReceipt" id="btnDownloadReceipt" type="button" value="收 據"
                                                class="InputBtn_S" onclick="btnDownloadReceipt_Click()" />
                                        </span></span></span></span>
                                        <asp:PlaceHolder runat="server" ID="phReturnedDownload" Visible="false">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ButtonBox"><span class="ButtonField_S"><span><span class="BtnName">
                                            <input name="btReturnedDownload" id="btReturnedDownload" type="button" value="退貨申請書" 
                                                class="InputBtn_S" onclick="document.location.href='<%=RefundFormUrlShow.Replace("https","http")%>    ';" />
                                        </span></span></span></span>
                                        </asp:PlaceHolder>
                                        <asp:PlaceHolder runat="server" ID="phDiscountSingleDownload" Visible="false">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ButtonBox"><span class="ButtonField_S"><span><span class="BtnName">
                                            <input name="btDiscountSingleDownload" id="btDiscountSingleDownload" type="button" value="退回或折讓證明單" 
                                                class="InputBtn_S" onclick="document.location.href='<%=DiscountSingleFormUrlShow.Replace("https","http")%>    ';" />
                                        </span></span></span></span>
                                        </asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <%--退貨明細--%>
                        <asp:Panel ID="pan_OrderDetailRefund" runat="server">
                            <div class="OrderTableArea">
                                <asp:HiddenField ID="hidOrderPk" runat="server" />
                                <asp:Repeater ID="repRefund" runat="server" OnItemDataBound="repRefund_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="tbRefund" class="OrderTableTb" width="680" border="0" cellspacing="0"
                                            cellpadding="0">
                                            <tr class="TableTitle">
                                                <td width="680" colspan="8" style="text-align: left;">
                                                    &nbsp;退貨明細
                                                </td>
                                            </tr>
                                            <tr class="TableHeader">
                                                <td width="100" style="text-align: center;">
                                                    退貨申請日
                                                </td>
                                                <td width="100" style="text-align: center;">
                                                    憑證編號
                                                </td>
                                                <td width="190" style="text-align: center;">
                                                    退回款項
                                                </td>
                                                <td width="290" style="text-align: center;">
                                                    退貨進度
                                                </td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class="LineOdd">
                                            <td class="TableBd" width="100" style="text-align: center;">
                                                <asp:Label ID="lblReturnedCreateTime" runat="server" Text="-"></asp:Label>
                                            </td>
                                            <td class="TableBd" width="100" style="text-align: center;">
                                                <asp:Label ID="lblSequenceNumber" runat="server"></asp:Label>
                                            </td>
                                            <td class="TableBd" width="190" style="text-align: right;">
                                                <asp:Label ID="lblPcash" runat="server"></asp:Label><br />
                                                <asp:Label ID="lblScash" runat="server"></asp:Label><br />
                                                <asp:Label ID="lblBcash" runat="server"></asp:Label><br />
                                                <asp:Label ID="lblCash" runat="server"></asp:Label>
                                            </td>
                                            <td class="TableBd" width="290" style="text-align: center;">
                                                <asp:Label ID="lblModifyTime" runat="server"></asp:Label>
                                                <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </asp:Panel>
                        <%--發票明細--%>
                        <asp:Panel ID="pan_EinvoiceMain" runat="server" Visible="false">
                            <div class="OrderDetailInvoice">
                                <asp:Panel ID="pan_EinvoiceDetail" runat="server">
                                    <div class="OrDetElectricinvoice">
                                        <table class="OrderTableTb" width="680" border="0" cellspacing="0" cellpadding="0">
                                            <tr class="TableTitle">
                                                <td width="680" colspan="2" style="text-align: left;">
                                                    &nbsp;發票明細
                                                </td>
                                            </tr>
                                            <tr class="LineOdd">
                                                <td width="680" colspan="2" style="text-align: left;">
                                                    <asp:Panel ID="InvoiceContainer" runat="server">
                                                        <%-- 舊發票流程用 --%>
                                                        <asp:Panel runat="server" ID="OldInvoiceContainer">
                                                            <asp:Label ID="lblEinvoiceWinner" runat="server" Text="●本發票已中獎" Visible="false" />
                                                            <br />
                                                            <asp:Label ID="lblEinvoicePaperRequest" runat="server" Text="●您已索取電子發票紙本" Visible="false" />
                                                            <table width="640" class="EInvoice" border="0" cellspacing="0" cellpadding="0" align="center">
                                                                <tr>
                                                                    <td width="240" class="border">
                                                                        發票號碼:<asp:Label ID="lbl_Einvoice_InvoiceNum" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td colspan="4" rowspan="4" class="border" style="text-align: center;">
                                                                        康太數位整合股份有限公司<br />
                                                                        電子計算機統一發票<br />
                                                                        中華民國<asp:Label ID="lbl_Einvoice_InvoiceDate" runat="server"></asp:Label><br />
                                                                    </td>
                                                                    <td width="210" rowspan="3" class="border bfix">
                                                                        <asp:literal id="litEinvoicePass" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="border">
                                                                        買受人:<asp:Label ID="lbl_Einvoice_BuyerName" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="border">
                                                                        統一編號:<asp:Label ID="lbl_Einvoice_ComId" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="border">
                                                                        地址:<asp:Label ID="lbl_Einvoice_BuyerAddress" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td class="border bfix">
                                                                        訂單編號:<asp:Label ID="lbl_Einvoice_OrderId" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" class="border" style="text-align: center;">
                                                                        品名
                                                                    </td>
                                                                    <td class="border" style="text-align: center;">
                                                                        數量
                                                                    </td>
                                                                    <td class="border" style="text-align: center;">
                                                                        單價
                                                                    </td>
                                                                    <td class="border" style="text-align: center;">
                                                                        金額
                                                                    </td>
                                                                    <td class="border bfix" style="text-align: center;">
                                                                        備註
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" rowspan="3" class="border">
                                                                        <asp:Label ID="lbl_Einvoice_ItemName" runat="server"></asp:Label><br />
                                                                        <br />
                                                                        <br />
                                                                    </td>
                                                                    <td rowspan="3" class="border" style="text-align: center;">
                                                                        1
                                                                    </td>
                                                                    <td rowspan="3" class="border" style="text-align: center;">
                                                                        <asp:Label ID="lbl_Einvoice_OrderAmount1" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td rowspan="3" class="border" style="text-align: center;">
                                                                        <asp:Label ID="lbl_Einvoice_OrderAmount2" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td colspan="3" class="border bfix" style="text-align: left;word-wrap: break-word; word-break:break-all;">
                                                                        <asp:Label ID="lbl_Einvoice_OrderItem" runat="server"></asp:Label><br />
                                                                        <br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="border bfix" style="text-align: center;">
                                                                        營業人蓋用統一發票專用章
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td rowspan="6" class="border bfix" style="text-align: center;">
                                                                        副本<br />
                                                                        <span style="color: red;">此副本僅供參考，<br />
                                                                            不可持本聯兌換。</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" class="border" style="text-align: center;">
                                                                        銷售額合計
                                                                    </td>
                                                                    <td class="border">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td rowspan="2" class="border" style="text-align: center;">
                                                                        營業稅
                                                                    </td>
                                                                    <td class="border" style="text-align: center;">
                                                                        應稅
                                                                    </td>
                                                                    <td class="border" style="text-align: center;">
                                                                        零稅率
                                                                    </td>
                                                                    <td class="border" style="text-align: center;">
                                                                        免稅
                                                                    </td>
                                                                    <td class="border" rowspan="2" style="text-align: center;">
                                                                        <asp:Label ID="lbl_Einvoice_TaxAmount" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="border" style="text-align: center;">
                                                                        <asp:Label ID="lbl_Einvoice_HasTax" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td class="border" />
                                                                    <td class="border" style="text-align: center;">
                                                                        <asp:Label ID="lbl_Einvoice_NoTax" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" class="border" style="text-align: center;">
                                                                        總計
                                                                    </td>
                                                                    <td class="border" style="text-align: center;">
                                                                        <asp:Label ID="lbl_Einvoice_OrderAmount3" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" class="border" style="text-align: center;">
                                                                        總計新臺幣<asp:Label ID="lbl_Einvoice_ChineseAmount" runat="server"></asp:Label>整
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="6" style="text-align: center;">
                                                                        *應稅、零稅、免稅之銷售額應分別開立統一發票，並應於各該欄位打「V」<br />
                                                                        本發票依財政部臺北市國稅局中南稽徵所99年8月20日財北國稅中南營業一字第0990017821號函核准使用
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                        <%-- 可申請紙本的發票; csv格示--%>
                                                        <asp:Panel ID="NewInvoiceContainer" runat="server">
                                                            <asp:ListView runat="server" ID="Invoices" OnItemDataBound="Invoices_ItemDataBound">
                                                                <LayoutTemplate>
                                                                    <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                                                                </LayoutTemplate>
                                                                <ItemTemplate>
                                                                    <br />
                                                                    <asp:Label ID="lblEinvoiceWinner" runat="server" Text="●本發票已中獎" Visible="false" Style="clear: both; display: block;" />
                                                                    <asp:Label ID="lblEinvoicePaperRequest" runat="server" Text="●您已索取紙本發票" Visible="false" Style="clear: both; display: block;" />
                                                                    <table width="640" class="EInvoice" border="0" cellspacing="0" cellpadding="0" align="center">
                                                                        <tr>
                                                                            <td width="240" class="border">
                                                                                發票號碼:<asp:Literal ID="litInvoiceNumber" runat="server" Text='<%# Eval("InvoiceNumber") %>'></asp:Literal>
                                                                            </td>
                                                                            <td colspan="4" rowspan="4" class="border" style="text-align: center;">
                                                                                康太數位整合股份有限公司<br />
                                                                                電子計算機統一發票<br />
                                                                                中華民國<asp:Literal ID="litInvoiceDate" runat="server" /><br />
                                                                            </td>
                                                                            <td width="210" rowspan="2" class="border bfix">
                                                                                <asp:Literal id="litEinvoicePass" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="border">
                                                                                買受人:<asp:Literal ID="litBuyerName" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="border">
                                                                                統一編號:<asp:Literal ID="litComanyId" runat="server" Text='<%# Eval("InvoiceComId") %>' />
                                                                            </td>
                                                                            <td class="border bfix" style="text-align: left;">
                                                                                訂單編號:<asp:Literal ID="litOrderId" runat="server" Text='<%# Eval("OrderId") %>' />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="border">
                                                                                地址:<asp:Literal ID="litBuyerAddress" runat="server" Text='<%# Eval("InvoiceBuyerAddress") %>' />
                                                                            </td>
                                                                            <td class="border bfix" style="text-align: left;">
                                                                                憑證號碼:<asp:Literal ID="litCouponSequenceNumber" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" class="border" style="text-align: center;">
                                                                                品名
                                                                            </td>
                                                                            <td class="border" style="text-align: center;">
                                                                                數量
                                                                            </td>
                                                                            <td class="border" style="text-align: center;">
                                                                                單價
                                                                            </td>
                                                                            <td class="border" style="text-align: center;">
                                                                                金額
                                                                            </td>
                                                                            <td class="border bfix" style="text-align: center;">
                                                                                備註
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" rowspan="3" class="border">
                                                                                <asp:Literal ID="litItemName" runat="server" /><br />
                                                                                <br />
                                                                                <br />
                                                                            </td>
                                                                            <td rowspan="3" class="border" style="text-align: center;">
                                                                                1
                                                                            </td>
                                                                            <td rowspan="3" class="border" style="text-align: center;">
                                                                                <asp:Literal ID="litItemUnitPrice" runat="server" />
                                                                            </td>
                                                                            <td rowspan="3" class="border" style="text-align: center;">
                                                                                <asp:Literal ID="litItemSumAmount" runat="server" />
                                                                            </td>
                                                                            <td colspan="3" class="border bfix" style="text-align: left;word-wrap: break-word; word-break:break-all;">
                                                                                <asp:Literal ID="litMemo" runat="server" /><br />
                                                                                <br />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="border bfix" style="text-align: center;">
                                                                                營業人蓋用統一發票專用章
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td rowspan="6" class="border bfix" style="text-align: center;">
                                                                                副本<br />
                                                                                <span style="color: red;">此副本僅供參考，<br />
                                                                                    不可持本聯兌換。</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4" class="border" style="text-align: center;">
                                                                                銷售額合計
                                                                            </td>
                                                                            <td class="border">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td rowspan="2" class="border" style="text-align: center;">
                                                                                營業稅
                                                                            </td>
                                                                            <td class="border" style="text-align: center;">
                                                                                應稅
                                                                            </td>
                                                                            <td class="border" style="text-align: center;">
                                                                                零稅率
                                                                            </td>
                                                                            <td class="border" style="text-align: center;">
                                                                                免稅
                                                                            </td>
                                                                            <td class="border" rowspan="2" style="text-align: center;">
                                                                                <asp:Literal ID="litTaxAmount" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="border" style="text-align: center;">
                                                                                <asp:Literal ID="litHasTax" runat="server" />
                                                                            </td>
                                                                            <td class="border" style="text-align: center;" />
                                                                            <td class="border" style="text-align: center;">
                                                                                <asp:Literal ID="litNoTax" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4" class="border" style="text-align: center;">
                                                                                總計
                                                                            </td>
                                                                            <td class="border" style="text-align: center;">
                                                                                <asp:Literal ID="litTotalAmount" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="5" class="border" style="text-align: center;">
                                                                                總計新臺幣
                                                                                <asp:Literal ID="litChineseAmount" runat="server" />
                                                                                整
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="6" style="text-align: center;">
                                                                                *應稅、零稅、免稅之銷售額應分別開立統一發票，並應於各該欄位打「V」<br />
                                                                                本發票依財政部臺北市國稅局中南稽徵所99年8月20日財北國稅中南營業一字第0990017821號函核准使用
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <br />
                                                                </ItemTemplate>
                                                            </asp:ListView>
                                                        </asp:Panel>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr class="LineOdd">
                                                <td width="680" colspan="2" style="text-align: center;">
                                                    <asp:Panel ID="panEinvoiceInfo" runat="server">
                                                        <div class="InvoiceState">
                                                            <asp:Label ID="lbl_Einvoice_Others" runat="server"></asp:Label>
                                                        </div>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="680">
                                                    <asp:Panel ID="pan_PaperRequest" runat="server">
                                                        <span class="ButtonBox" id="btn_RequestPaperInvoice" runat="server" style="margin-left: 280px;">
                                                            <span class="ButtonField_S"><span><span class="BtnName">
                                                                <input name="" id="Button1" type="button" value="索取紙本發票" class="InputBtn_S" onclick="block_ui($('#Pop_PaperRequest1'), 0, 0);" />
                                                            </span></span></span></span>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr class="LineOdd">
                                                <td width="680" colspan="2" style="text-align: left;">
													<uc:EinvoiceFooter id="invoiceFooter" runat="server" Version="Old" DealType="HiDeal" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </div>
                        </asp:Panel>
                        <!--繼續付款-->
                        <asp:Panel ID="pan_OrderDetailATM" runat="server" Visible="false">
                            <p>
                                以下為本次ATM付款資訊：
                            </p>
                            <p class="Tips FontPlus">
                                付款期限：<asp:Label ID="lab_ATM_ExpiredDay1" runat="server"></asp:Label>
                                23:59:59
                                <br />
                                銀行代碼：<label>中國信託商業銀行(822)</label><br>
                                銀行帳號：<asp:Label ID="lab_ATM_Account" runat="server"></asp:Label>
                                (共16碼，僅提供本次交易使用)<br />
                                付款金額：$<asp:Label ID="lab_ATM_Total" runat="server"></asp:Label>
                            </p>
                            <p>
                                ATM繳費提醒：<br />
                                1. 若您超過付款期限繳款，此保留訂單將會自動取消，購買也會不成立。<br />
                                2. 您必須自行負擔轉帳手續費$17<br />
                                3. 繳費代碼為單筆訂單專屬，請勿重複轉帳。<br />
                                4. 本繳費代碼僅限於ATM或網路ATM付款，請勿使用金融機構臨櫃或網路銀行轉帳。<br />
                                5. 請妥善保留ATM付款證明，確保您的購買權益。
                            </p>
                            <p>
                                更多<a href="#">ATM轉帳相關問題</a></p>
                        </asp:Panel>
                        <!--ATM付款失敗-->
                        <asp:Panel ID="pan_OrderDetailATMFail" runat="server" Visible="false">
                            <div class="ATMOrderTitleArea">
                                感謝您以ATM付款方式訂購<span style="color: #F60">【<asp:Label ID="lab_ATM_ItemName" runat="server"></asp:Label>】</span>優質好康。<br />
                                由於已經超過匯款期限，您的好康已經被取消囉！<br />
                                提醒您，避免影響您的好康權益，下次請盡早付款唷。
                            </div>
                        </asp:Panel>
                    </div>
                </asp:Panel>
            </div>
        </article>
    </section>
    <!--甚麼是信託-->
    <div class="PopUpWindow" style="display: none; text-align: left;" id="Pop_WhatIsTrust">
        <section class="Background">
            <div class="PopUpWindowClose" style="cursor:pointer; width:380px" onclick="unblock_ui();" style="cursor: pointer">
            </div>
            <div class="Tittle">
                信託讓您的消費更有保障</div>
            <p class="ExplainedText">
                您購買17Life購物金所支付的金額，將於兩個星期內存入17Life（康太數位整合股份有限公司）於台新銀行所開立之信託專戶，專款專用；所稱專用，係指供17Life履行交付商品或提供服務義務使用。
            </p>
            <p class="ExplainedText">
                在此提供您個人在品生活被信託保障中的品項查詢。在您確實使用掉17Life購物金之前（包括但不限於到店家使用以17Life購物金兌換的品生活憑證），我們將提供給您全面的信託保障，讓您安心購買。
            </p>
        </section>
    </div>
    <!--電子紙本索取1-->
    <div class="PopUpWindow" style="display: none; text-align: left" id="Pop_PaperRequest1">
        <section class="Background">
            <div class="PopUpWindowClose" onclick="unblock_ui();" style="cursor: pointer">
            </div>
            <div class="Tittle">
                索取紙本發票
            </div>
            <p class="brown_Subtitle">
                請您支持使用電子發票，好處多多</p>
            <p class="ExplainedText">
                1.使用電子發票，將減少地球上樹木的消失，節能減碳愛地球！</p>
            <p class="ExplainedText">
                2.辦理退貨時不需翻找發票，也不用擔心發票遺失無法退貨。</p>
            <p class="ExplainedText">
                3.奇數月26日由電腦自動幫您對獎，中獎時將以email通知，並掛號寄送至本發票給您。</p>
            <div class="UpWindowBottomFarm">
                <div class="Button_Paper_invoices_No" onclick="unblock_ui();" style="cursor: pointer">
                </div>
                <div class="Button_Paper_invoices_Yes" onclick="inserteinvoiceinfo();block_ui($('#Pop_PaperRequest2'),0,0);"
                    style="cursor: pointer">
                </div>
            </div>
            <div class="OrderDetailRequestBottom">
            </div>
        </section>
    </div>
    <!--電子紙本索取2-->
    <div class="PopUpWindow" style="display: none; text-align: left" id="Pop_PaperRequest2">
        <section class="Background">
            <div class="PopUpWindowClose" onclick="unblock_ui();" style="cursor: pointer">
            </div>
            <div class="Tittle">
                索取紙本發票
            </div>
            <p class="ExplainedText">
                請正確填寫收件人名稱與收件地址，我們將依您所提供的資訊寄出紙本發票。
            </p>
            <ul class="PaperInvoices_Fill_in">
                <li><span class="PaperInvoices_Fill_in_Name">收件</span>人<span class="Paper_Name">
                    <asp:TextBox ID="tbx_Einvoice_Receiver" runat="server"></asp:TextBox>
                </span><span class="PaperCommentGray" id="EinvoiceRequestReceiver">請確實填寫收件人。</span>
                </li>
                <li>發票地址 <span class="Paper_Address">
                    <asp:TextBox ID="tbx_Einvoice_Address" runat="server"></asp:TextBox>
                </span><span class="PaperCommentGray" id="EinvoiceRequestMessage">請填寫收件地址。 </span>
                </li>
            </ul>
            <p class="ExplainedText">
                發票已開立，索取紙本發票時，已無法更改為三聯式發票或其他姓名。</p>
            <br />
            <div class="UpWindowBottomFarm">
                <div class="Button_PaperInvoices_Fill_in_Sent">
                    <asp:LinkButton ID="lbt_Einvoice_PaperRequest" runat="server" CssClass="OrdDetReqBtn3"
                        OnClick="RequestPaperInvoice" OnClientClick="return paperinvoicecheck();" Width="98"
                        Height="46"></asp:LinkButton>
                </div>
            </div>
        </section>
    </div>
    <!--SMS 1-->
    <div class="PopUpWindow" id="Pop_SMS1" style="display: none; text-align: left">
        <section class="Background">
            <div class="PopUpWindowClose" style="cursor: pointer" onclick="unblock_ui();">
            </div>
            <div class="Tittle">
                索取簡訊憑證
            </div>
            <p class="brown_Subtitle">
                <asp:Label ID="lab_Pop_SMSMobile" runat="server"></asp:Label>
            </p>
            <p class="ExplainedText">
                您的簡訊憑證，將會被傳送到上方所顯示的號碼，確定接收的號碼正確嗎？
            </p>
            <ul class="SMS_Note">
                <li>注意事項：</li>
                <li>1.索取發送簡訊憑證是免費的，您不需額外支付任何費用。</li>
                <li>2.每則簡訊憑證最多可索取發送三次。</li>
                <li>3.您已索取此則簡訊憑證
                    <asp:Label ID="lab_Pop_SMSAlreadyCount" runat="server"></asp:Label>
                    次，尚可索取
                    <asp:Label ID="lab_Pop_SMSRemainCount" runat="server"></asp:Label>
                    次。 </li>
            </ul>
            <div class="UpWindowBottomFarm">
                <div class="Button_SMS_Certificate_Modify" onclick="javascript:window.open('/User/UserAccount.aspx','_self');"
                    style="cursor: pointer">
                </div>
                <div class="Button_SMS_Certificate_Confirm">
                    <asp:LinkButton ID="lbt_Pop_SmsConfirm" runat="server" CssClass="ReturnsRequestBtn"
                        Width="127" Height="32" OnCommand="SendCouponSms"></asp:LinkButton>
                </div>
            </div>
        </section>
    </div>
    <!--SMS Fail & 3-->
    <div class="PopUpWindow" id="Pop_SMS3" style="display: none; text-align: left">
        <section class="Background">
            <div class="PopUpWindowClose" style="cursor: pointer" onclick="unblock_ui();">
            </div>
            <div class="Tittle">
                索取簡訊憑證
            </div>
            <div class="brown_Subtitle" style="text-align: left">
                <div id="sms_2">
                    <p class="brown_Subtitle">
                        您已索取超過三次，無法再索取。
                    </p>
                </div>
                <div id="sms_3">
                    <p class="brown_Subtitle">
                        發送成功。
                    </p>
                    <p class="ExplainedText">
                        您的簡訊憑證已被發送至：<asp:Label ID="lab_Pop_SMSMobile2" runat="server"></asp:Label></p>
                </div>
                <ul class="SMS_Note">
                    <li>注意事項：</li>
                    <li>1.索取發送簡訊憑證是免費的，您不需額外支付任何費用。</li>
                    <li>2.每則簡訊憑證最多可索取發送三次。</li>
                    <li>3.您已索取此則簡訊憑證
                        <asp:Label ID="lab_Pop_SMSAlreadyCount3" runat="server"></asp:Label>
                        次，尚可索取
                        <asp:Label ID="lab_Pop_SMSRemainCount3" runat="server"></asp:Label>
                        次。 </li>
                </ul>
            </div>
        </section>
    </div>
    <!--mail coupon-->
    <div class="email_pop" id="Pop_MailCoupon" style="display: none;">
        <div class="PopUpWindowClose" style="cursor: pointer" onclick="unblock_ui();"></div>
        <div class="email_pop_content" id="email_pop_content">
            <p>請確認憑證寄達的的電子信箱</p>
            <input type="text" id="userEmail" name="userEmail" style="width: 240px" value="<%=this.UserEmail %>" />
            <div class="email_pop_error">
                <span class="email_pop_error_info" style="display: none">電子信箱格式錯誤！</span>
            </div>
            <span class="ButtonBox">
                <span class="ButtonField_S">
                    <span><span class="BtnName"><a href="javascript:void(0)" id="btn_MailCoupon" class="InputBtn_S">確認寄送</a></span></span>
                </span>
            </span>
        </div>
        <div class="email_pop_content" id="email_pop_result" style="display: none">
            <br />

            <p><span id="email_pop_content_email"></span>
                <br />
                寄送成功，請收取信件，並下載付件檔。</p>
            <p>若您尚未安裝PDF閱讀器，請先<a href="http://get.adobe.com/tw/reader/" target="_blank">下載</a>安裝</p>
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;


namespace LunchKingSite.Web.piinlife.member
{
    public partial class RefundForm : BasePage, IHiDealRefundFormView
    {
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!_presenter.OnViewInitialized())
                {
                    Response.Redirect("~/error-sgo.aspx");
                    return;
                }
            }
            _presenter.OnViewLoaded();
        }

        #region interface properties
        private HiDealRefundFormPresenter _presenter;
        public HiDealRefundFormPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get
            {
                return Request.QueryString["id"] != null ? HiDealReturnedManager.GetUserNameFromRefundFormPageId(Request.QueryString["id"]) : string.Empty;
            }
        }
        public int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(UserName);
            }
        }

        public int? ReturnedId
        {
            get
            {
                return Request.QueryString["id"] != null ? HiDealReturnedManager.GetReturnedIdFromRefundFormPageId(Request.QueryString["id"]) : null;
            }
        }

        public int? OrderPk
        {
            get
            {
                return Request.QueryString["id"] != null
                           ? HiDealReturnedManager.GetOrderPkFromRefundFormPageId(Request.QueryString["id"])
                           : null;
            }
        }

        public int? CouponId
        {
            get
            {
                int cid;
                if (Request.QueryString["cid"] != null)
                {
                    if (int.TryParse(Request.QueryString["cid"], out cid))
                    {
                        return cid;
                    }
                }
                return null;
            }
        }


        public HiDealRefundFormType RefundFormType
        {
            get
            {
                if (Request.QueryString["FormType"] != null)
                {
                    if (string.Compare(Request.QueryString["FormType"], "RAF", true) == 0)
                        return HiDealRefundFormType.RefundApplicationForm;
                    if (string.Compare(Request.QueryString["FormType"], "DSF", true) == 0)
                        return HiDealRefundFormType.DiscountSingleForm;
                    if (string.Compare(Request.QueryString["FormType"], "ALL", true) == 0)
                        return HiDealRefundFormType.All;
                }
                return HiDealRefundFormType.None;
            }
        }

        public string OrderId { get; set; }
        public DateTime ReturnedCreateTime { get; set; }
        public int HiDealId { get; set; }
        public string HiDealProductName { get; set; }

        public string EncryptedCouponCode
        {
            set
            {
                //設定OrderId的條碼
                iVC_RAF.ImageUrl = ResolveUrl("~/service/barcode.ashx") + "?type=orderid&code=" + value;
            }
        }

        #endregion interface properties

        #region interface method
        public void ShowMessageAndRedirectToDefault(string message)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                                                "alert('" + message + "');window.location.href='" +
                                                ResolveClientUrl("~/piinlife/") + "';",
                                                true);
        }


        public void SetRefundFormInfo(EinvoiceMainCollection einvoices, CashTrustLogCollection cs, ViewHiDealReturnedCollection viewHiDealReturnedCollection, HiDealDeliveryType hiDealDeliveryType, HiDealRefundFormType hiDealRefundFormType = HiDealRefundFormType.None)
        {
            //PDF顯示那些PlaceHolder
            phReturnApplicationFrom.Visible = false;
            phDiscountSingleForm.Visible = false;
            switch (hiDealRefundFormType)
            {
                case HiDealRefundFormType.None:
                    break;
                case HiDealRefundFormType.RefundApplicationForm:
                    phPageBreak.Visible = false;
                    phReturnApplicationFrom.Visible = true;
                    phDiscountSingleForm.Visible = false;
                    break;
                case HiDealRefundFormType.DiscountSingleForm:
                    phPageBreak.Visible = false;
                    phReturnApplicationFrom.Visible = false;
                    phDiscountSingleForm.Visible = true;
                    break;
                case HiDealRefundFormType.All:
                    phPageBreak.Visible = true;
                    phReturnApplicationFrom.Visible = true;
                    phDiscountSingleForm.Visible = true;

                    break;
                default:
                    break;
            }
            PL_ReApFormTop2.ImageUrl = ResolveUrl("~/Themes/HighDeal/images/Returns/PL_ReApFormTop2.png");
            OrderId = viewHiDealReturnedCollection.First().OrderId;
            HiDealId = viewHiDealReturnedCollection.First().HiDealId;
            HiDealProductName = viewHiDealReturnedCollection.First().ProductName;
            ReturnedCreateTime = viewHiDealReturnedCollection.First().CreateTime;
            //避免沒有圖形時的問題
            iVC_RAF.AlternateText = OrderId;

            List<HiDealRefundFormListClass> rfs = new List<HiDealRefundFormListClass>();
            int i = 0;

            foreach (var einvoice in (CouponId != null) ? einvoices.Where(x => x.CouponId == CouponId) : einvoices)
            {
                int mode = einvoice.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? 3 : 2;
                decimal tax = einvoice.InvoiceTax;
                bool istax = einvoice.InvoiceTax == 0.05m;

                var tempcs = cs.ToList();

                if (tempcs.Count > 0)
                {
                    if (hiDealDeliveryType == HiDealDeliveryType.ToShop)
                    {
                        #region 憑證折讓單
                        //後台單一筆折讓單畫面
                        if (einvoice.CouponId != null)
                        {
                            tempcs = tempcs.Where(x => x.CouponId == einvoice.CouponId).ToList();
                        }

                        foreach (var item in tempcs)
                        {
                            if (!einvoice.OrderIsPponitem && item.Status < ((int)TrustStatus.Verified))
                            {
                                decimal salesamount = (Math.Round(((item.CreditCard + item.Atm + item.Scash) / (1 + tax)), 0));

                                rfs.Add(new HiDealRefundFormListClass(i, einvoice.OrderId,
                                                                      string.IsNullOrEmpty(item.CouponSequenceNumber)
                                                                          ? string.Empty
                                                                          : ("#" + item.CouponSequenceNumber),
                                                                      mode,
                                                                      einvoice.InvoiceNumberTime == null
                                                                          ? einvoice.OrderTime
                                                                          : einvoice.InvoiceNumberTime.Value,
                                                                      einvoice.InvoiceNumberTime == null,
                                                                      string.IsNullOrEmpty(einvoice.InvoiceNumber)
                                                                          ? string.Empty.PadLeft(10, ' ')
                                                                          : einvoice.InvoiceNumber,
                                                                      Helper.IsFlagSet(item.SpecialStatus,
                                                                                       TrustSpecialStatus.Freight)
                                                                          ? "運費" : NoComma(einvoice.OrderItem.Substring(einvoice.OrderItem.IndexOf('X') + 1, einvoice.OrderItem.Length -(einvoice.OrderItem.IndexOf('X') + 1))),
                                                                      item.CreditCard + item.Atm + item.Scash, (int)salesamount,
                                                                      (item.CreditCard + item.Atm + item.Scash) - (int)salesamount,
                                                                      istax,
                                                                      einvoice.OrderIsPponitem));
                                i++;
                            }
                        }

                        #endregion 憑證折讓單
                    }
                    else
                    {
                        #region  商品折讓單

                        int sumItemAmount = 0;
                        int sumItemNoTaxAmount = 0;
                        int sumItemTax = 0;

                        HiDealRefundFormListClass hiDealRefundFormListClass = new HiDealRefundFormListClass();

                        hiDealRefundFormListClass.Id = i;
                        hiDealRefundFormListClass.OrderId = einvoice.OrderId;
                        hiDealRefundFormListClass.CouponSequence = string.Empty; //商品檔不顯示憑證資訊
                        hiDealRefundFormListClass.EinvoiceMode = mode;
                        hiDealRefundFormListClass.EinvoiceDate = einvoice.InvoiceNumberTime == null
                                                                     ? einvoice.OrderTime
                                                                     : einvoice.InvoiceNumberTime.Value;
                        hiDealRefundFormListClass.EinvoiceNumberDate_Year = (einvoice.InvoiceNumberTime == null)
                                                                                ? string.Empty
                                                                                : hiDealRefundFormListClass.EinvoiceDate
                                                                                                           .Year
                                                                                                           .ToString();
                        hiDealRefundFormListClass.EinvoiceNumberDate_Month = (einvoice.InvoiceNumberTime == null)
                                                                                 ? string.Empty
                                                                                 : hiDealRefundFormListClass
                                                                                       .EinvoiceDate.Month.ToString();
                        hiDealRefundFormListClass.EinvoiceNumberDate_Day = (einvoice.InvoiceNumberTime == null)
                                                                               ? string.Empty
                                                                               : hiDealRefundFormListClass.EinvoiceDate
                                                                                                          .Day.ToString();
                        hiDealRefundFormListClass.EinvoiceNumber = string.IsNullOrEmpty(einvoice.InvoiceNumber)
                                                                       ? string.Empty.PadLeft(10, ' ')
                                                                       : einvoice.InvoiceNumber;
                        hiDealRefundFormListClass.EinvoiceItemName =
                            NoComma(einvoice.OrderItem.Substring(einvoice.OrderItem.IndexOf('X') + 1,
                                                                 einvoice.OrderItem.Length -
                                                                 (einvoice.OrderItem.IndexOf('X') + 1)));
                        //

                        foreach (var item in tempcs)
                        {
                            if ((einvoice.OrderIsPponitem && item.Status < ((int)TrustStatus.Returned)) ||
                                (!einvoice.OrderIsPponitem && item.Status < ((int)TrustStatus.Verified)))
                            {
                                decimal salesamount = (Math.Round(((item.CreditCard + item.Atm + item.Scash) / (1 + tax)), 0));

                                //sumItemAmount += item.CreditCard + item.Atm ;
                                sumItemAmount += item.CreditCard + item.Atm + item.Scash;
                                sumItemNoTaxAmount += (int)salesamount;
                                sumItemTax += (item.CreditCard + item.Atm + item.Scash) - (int)salesamount;

                            }
                        }
                        hiDealRefundFormListClass.ItemAmount = sumItemAmount;
                        hiDealRefundFormListClass.ItemNoTaxAmount = sumItemNoTaxAmount;
                        hiDealRefundFormListClass.ItemTax = sumItemTax;
                        hiDealRefundFormListClass.IsTax = istax;
                        hiDealRefundFormListClass.IsProduct = einvoice.OrderIsPponitem;


                        rfs.Add(hiDealRefundFormListClass);


                        #endregion  商品折讓單
                    }
                }
            }

            if (rfs.Count > 0)
                rfs.Last().breakstring = string.Empty;
            #region ATM部分(先不做)
            //pan_ATM.Visible = isatm;
            //if (isatm && atmrefund.Si != 0)
            //{
            //    lab_ATM_AccountName.Text = atmrefund.AccountName;
            //    lab_ATM_UserID.Text = atmrefund.Id;
            //    lab_ATM_BankName.Text = atmrefund.BankName;
            //    lab_ATM_BranchName.Text = atmrefund.BranchName;
            //    lab_ATM_Account.Text = atmrefund.AccountNumber;
            //    lab_ATM_Phone.Text = atmrefund.Phone;
            //}
            #endregion ATM部分
            rpt_Form.DataSource = rfs;
            rpt_Form.DataBind();

            rpCouponSequenceNumber.DataSource = rfs;
            rpCouponSequenceNumber.DataBind();
        }

        #endregion interface method

        #region class properties
        public DateTime Now
        {
            get { return DateTime.Now; }
        }
        #endregion class properties

        public static string NoComma(string input, string comma = ",")
        {
            return string.IsNullOrEmpty(input) ? string.Empty : input.Replace(comma, string.Empty);
        }

        protected void rpt_Form_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.SelectedItem)
            {
                ((Label)e.Item.FindControl("RefundTitle")).Text = ((HiDealRefundFormListClass)e.Item.DataItem).IsProduct ? "" : "憑證編號";
                ((Image)e.Item.FindControl("iVC_DSF")).ImageUrl = iVC_RAF.ImageUrl;
            }
        }
    }

}

//擴充Datetime民國年顯示方式
namespace System
{
    public static class DateTimeExtensions
    {
        public static string ToTaiwanLongDateString(this DateTime datetime)
        {
            return datetime.ToTaiwanString(" yyy 年 MM 月 dd 日");
        }

        public static string ToTaiwanString(this DateTime datetime, string format)
        {
            CultureInfo info = new CultureInfo("zh-TW");
            TaiwanCalendar calendar = new TaiwanCalendar();
            info.DateTimeFormat.Calendar = calendar;

            string tmpString;

            if (datetime.Year < 1912)
            {
                int offsetYear = 1912 - datetime.Year;
                datetime = datetime.AddYears(offsetYear * 2 - 1);
                tmpString = datetime.ToString(format, info);
                tmpString = "民國前" + tmpString;
            }
            else
            {
                tmpString = "民國" + datetime.ToString(format, info);
            }

            return tmpString;
        }
    }
}


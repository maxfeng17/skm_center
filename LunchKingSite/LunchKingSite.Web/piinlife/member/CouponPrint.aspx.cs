﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System.Web.Security;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.piinlife.member
{
    public partial class CouponPrint : LocalizedBasePage, IHiDealCouponDetailView
    {
        #region props
        private HidealCouponDetailPresenter _presenter;
        public HidealCouponDetailPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return string.IsNullOrEmpty(Request.QueryString["u"]) ? Page.User.Identity.Name : Request.QueryString["u"]; }
        }

        public int? CouponId
        {
            get
            {
                int id;
                return int.TryParse(Request.QueryString["cid"], out id) ? new int?(id) : null;
            }
        }

        public string ActionMode
        {
            get
            {
                return  Request.QueryString["mode"] ??  string.Empty;
            }
        }
        public int? DealId
        {
            get
            {
                int dealId;
                return int.TryParse(Request.QueryString["did"], out dealId) ? new int?(dealId) : null;
            }
        }
        public int? ProductId
        {
            get
            {
                int productId;
                return int.TryParse(Request.QueryString["pid"], out productId) ? new int?(productId) : null;
            }
        }



        public string GoogleApiKey { protected get; set; }

        public string EncryptedCouponCode
        {
            set
            {
                iVC.ImageUrl = ResolveUrl("~/service/barcode.ashx") + "?code=" + value;
                iQC.ImageUrl = ResolveUrl("~/service/qrcode.ashx") + "?code=" + value;
            }
        }

        public bool ForceStaticMap
        {
            get { return !string.IsNullOrEmpty(Request.QueryString["s"]); }
        }

        protected string MapAddress { get; set; }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                if (!_presenter.OnViewInitialized())
                {
                    Response.Redirect("../default.aspx");
                    return;
                }
            _presenter.OnViewLoaded();
        }

        protected string GetAddressString(int townshipId)
        {
            return CityManager.CityTownShopStringGet(townshipId);
        }

        #region ICouponDetailView Methods

        public void SetDetail(ViewHiDealCoupon coupon, HiDealContent content, List<ViewHiDealStoresInfo> storeList)
        {
            lblDealName.Text = coupon.DealName;
            lblProductName.Text = coupon.Name;
            liUseStartTime.Text = coupon.UseStartTime.Value.ToShortDateString();
            liUseEndTime.Text = coupon.UseEndTime.Value.ToShortDateString();
            liContent.Text = content != null ? content.Context : string.Empty;
            lCS.Text = string.Format("{0}{1}", coupon.Prefix, coupon.Sequence);
            liCouponCode.Text = coupon.Code;
            iVC.AlternateText = coupon.Code;
            //avc.DataBind();
            hidSellerName.Value = coupon.SellerName;
            if (storeList.Any(x => x.Guid.Equals(coupon.StoreGuid)))
            {
                repStore.DataSource = storeList.Where(x => x.Guid.Equals(coupon.StoreGuid));
            }
            else
            {
                repStore.DataSource = storeList;
            }
            repStore.DataBind();
        }
        #endregion

        #region Repeater Event
        #endregion
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/piinlife/hideal.Master" AutoEventWireup="true"
    CodeBehind="OrderList.aspx.cs" Inherits="LunchKingSite.Web.piinlife.member.OrderList"
    Title="訂單列表管理" %>

<%@ Register Src="~/piinlife/member/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ScriptContent" runat="server">
    <link rel="stylesheet" href="../../Themes/HighDeal/images/style/blueprint/screen.css" type="text/css" media="screen, projection" />
    <link href="../../Themes/HighDeal/images/Account/PersonalAccount.css" rel="stylesheet" type="text/css" />
    <link href="../../Themes/HighDeal/images/MyAccount/PopUpWindow.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script language="javascript" type="text/javascript">
        function openBookingSystem(orderGuid, apiKey) {
            if (orderGuid) {
                window.open('<%=ResolveUrl("../../BookingSystem/ReservationService.aspx")%>?ApiKey=' + apiKey + '&gid=' + orderGuid, '_blank');
            } else {
                window.open('<%=ResolveUrl("../../BookingSystem/ReservationRecords.aspx")%>?ApiKey=' + apiKey, '_blank');
            }
            return false;
        }
    </script>
    <asp:HiddenField ID="hfApiKey" runat="server" />
    <section class="AccountContainer">
        <aside class="Offers_Sidebar">
            <nav class="SideBar">
                <ul class="category">
                    <li id="category_all" class="" style="display: block;"><span class="category_icon"></span>
                        <a id="all" class="category" href="/User/UserAccount.aspx">個人資料</a> </li>
                    <li id="Li1" class="first selected"><span class="category_icon02"></span><a
                        id="a1" class="category">訂單查詢</a>
                        <div class="arrow">
                        </div>
                    </li>
                    <li id="Li2" class="" style="display: block;"><span class="category_icon02">
                    </span><a id="a2" class="category" href="/User/BonusListNew.aspx">紅利與購物金</a> </li>
                    <li id="Li3" class="" style="display: block;"><span class="category_icon02">
                    </span><a id="a3" class="category" href="/User/UserAccount.aspx">收件人管理</a> </li>
                    <li id="Li4" class="" style="display: block;"><span class="category_icon02">
                    </span><a id="a4" class="category" href="/User/ServiceList">過往客服紀錄</a> </li>
                    <%--<li id="Li1" class="" style="display: block;"><span class="category_icon02">
                    </span><a id="a3" class="category" href="UserPassword.aspx">變更密碼</a> </li>--%>
                </ul>
            </nav>
        </aside>
        <article class="Main">
            <div id="BLeft">
                <asp:Panel ID="pan_Main" runat="server">
                    <h1 class="padding">
                        訂單列表管理
                    </h1>
                    <% if (DateTime.Now > config.NewPiinlifeDate)
                       { %>
                    <span class="OrderSearch">只供查詢<%= config.NewPiinlifeDate.ToString("yyyy-MM-dd") %>以前的訂單。新購買請 <a href='<%=ResolveUrl("~/User/coupon_List.aspx")%>'>查看新訂單</a></span>
                    <% } %>
                    <hr class="Line" />
                    <div class="OrderFilter">
                        <asp:CheckBox ID="cbx_LatestOrders" runat="server" Text="只顯示近4個月的訂單" AutoPostBack="true"
                            Checked="true" OnCheckedChanged="ChangeFilter" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:DropDownList ID="ddl_Filter" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChangeFilter">
                        </asp:DropDownList>
                    </div>
                    <br />
                    <div class="OrderTableArea">
                        <asp:Repeater ID="repOrderList" runat="server" OnItemDataBound="repOrderList_ItemBound">
                            <HeaderTemplate>
                                <table class="OrderTableTb" width="680" border="0" cellspacing="0" cellpadding="0">
                                    <tr class="TableHeader">
                                        <td width="90" style="text-align: center;">
                                            訂購日期
                                        </td>
                                        <td width="180" style="text-align: center;">
                                            訂單編號
                                        </td>
                                        <td width="230" style="text-align: center;">
                                            商品名稱
                                        </td>
                                        <td width="90" style="text-align: center;">
                                            使用狀態
                                        </td>
                                        <td width="90" style="text-align: center;">
                                            訂單狀態
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="LineOdd">
                                    <td width="90" class="TableBd" style="text-align: center;">
                                        <%# ((HiDealOrderShow)(Container.DataItem)).OrderCreateTime.ToString("yyyy/MM/dd")%>
                                    </td>
                                    <td width="180" class="TableBd" style="text-align: center;">
                                        <asp:HyperLink ID="hkOrderDetail" runat="server"></asp:HyperLink>
                                        <span class="ButtonBox">
                                            <asp:LinkButton ID="lkService" runat="server" Font-Size="13px">
                                                <span class="ButtonField_S"><span><span class="BtnName">
                                                <input name="" id="" type="button" value="詢問" class="InputBtn_S"></span></span>
                                                </span>
                                            </asp:LinkButton>
                                        
                                            <asp:LinkButton ID="lkBookingReservation" runat="server" Font-Size="13px" Visible="false">
                                                <span class="ButtonField_S"><span><span class="BtnName">
		                                        <input name="" id="Button1" type="button" value="訂位" class="InputBtn_S"></span></span>
                                                </span>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="lkBookingReservationRoom" runat="server" Font-Size="13px" Visible="false">
                                                <span class="ButtonField_S"><span><span class="BtnName">
		                                        <input name="" id="Button3" type="button" value="訂房" class="InputBtn_S"></span></span>
                                                </span>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="lkQueryReservation" runat="server" Font-Size="13px" Visible="false">
                                                <span class="ButtonField_S"><span><span class="BtnName">
		                                            <input name="" id="Button2" type="button" value="查詢訂位" class="InputBtn_S"></span></span>
                                                </span>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="lkQueryReservationRoom" runat="server" Font-Size="13px" Visible="false">
                                                <span class="ButtonField_S"><span><span class="BtnName">
		                                            <input name="" id="Button4" type="button" value="查詢訂房" class="InputBtn_S"></span></span>
                                                </span>
                                            </asp:LinkButton>
                                        </span>
                                    </td>
                                    <td width="230" class="TableBd" style="text-align: left;">
                                        <asp:HyperLink ID="hkProductName" runat="server" Target="_blank" ForeColor="#414141"></asp:HyperLink>
                                    </td>
                                    <td width="100" class="TableBd" style="text-align: center;">
                                        <span>
                                            <asp:Label ID="lblUseCount" Text="-" runat="server"></asp:Label>
                                        </span>
                                        <asp:HyperLink ID="hkCouponList" runat="server">
                                            <span class="ButtonBox"><span class="ButtonField_S"><span>
                                            <span class="BtnName">
                                                <span class="InputBtn_S">索取憑證</span>
                                            </span>
                                        </span></span></span>
                                        </asp:HyperLink>
                                    </td>
                                    <td width="90" class="TableBd" style="text-align: center;">
                                        <asp:HyperLink ID="hkPayAtm" runat="server" Visible="false">
                                            <span class="ButtonBox"><span class="ButtonField_S"><span>
                                            <span class="BtnName">
                                                <span class="InputBtn_S">繼續付款</span>
                                            </span>
                                        </span></span></span>
                                        </asp:HyperLink>
                                        <asp:Label ID="lblOrderStatus" Text="-" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="LineEven">
                                    <td width="90" class="TableBd" style="text-align: center;">
                                        <%# ((HiDealOrderShow)(Container.DataItem)).OrderCreateTime.ToString("yyyy/MM/dd")%>
                                    </td>
                                    <td width="180" class="TableBd" style="text-align: center;">
                                        <asp:HyperLink ID="hkOrderDetail" runat="server"></asp:HyperLink>
                                        <asp:LinkButton ID="lkService" runat="server" Font-Size="13px">
                                        <span class="ButtonBox"><span class="ButtonField_S"><span><span class="BtnName">
                                            <input name="" id="" type="button" value="詢問" class="InputBtn_S"></span></span>
                                        </span></span>
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="lkBookingReservation" runat="server" Font-Size="13px" Visible="false">
                                            <span class="ButtonBox"><span class="ButtonField_S"><span><span class="BtnName">
		                                    <input name="" id="Button1" type="button" value="訂位" class="InputBtn_S"></span></span>
                                            </span></span>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lkBookingReservationRoom" runat="server" Font-Size="13px" Visible="false">
                                            <span class="ButtonBox"><span class="ButtonField_S"><span><span class="BtnName">
		                                    <input name="" id="Button3" type="button" value="訂房" class="InputBtn_S"></span></span>
                                            </span></span>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lkQueryReservation" runat="server" Font-Size="13px" Visible="false">
                                            <span class="ButtonBox"><span class="ButtonField_S"><span><span class="BtnName">
		                                        <input name="" id="Button2" type="button" value="查詢訂位" class="InputBtn_S"></span></span>
                                            </span></span>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lkQueryReservationRoom" runat="server" Font-Size="13px" Visible="false">
                                            <span class="ButtonBox"><span class="ButtonField_S"><span><span class="BtnName">
		                                        <input name="" id="Button4" type="button" value="查詢訂房" class="InputBtn_S"></span></span>
                                            </span></span>
                                        </asp:LinkButton>
                                    </td>
                                    <td width="230" class="TableBd" style="text-align: left;">
                                        <asp:HyperLink ID="hkProductName" runat="server" Target="_blank" ForeColor="#414141"></asp:HyperLink>
                                    </td>
                                    <td width="90" class="TableBd" style="text-align: center;">
                                        <span>
                                            <asp:Label ID="lblUseCount" Text="-" runat="server"></asp:Label>
                                        </span>
                                        <asp:HyperLink ID="hkCouponList" runat="server">
                                            <span class="ButtonBox"><span class="ButtonField_S"><span>
                                            <span class="BtnName">
                                                <span class="InputBtn_S">索取憑證</span>
                                            </span>
                                        </span></span></span>
                                        </asp:HyperLink>
                                    </td>
                                    <td width="90" class="TableBd" style="text-align: center;">
                                        <asp:HyperLink ID="hkPayAtm" runat="server" Visible="false">
                                            <span class="ButtonBox"><span class="ButtonField_S"><span>
                                            <span class="BtnName">
                                                <span class="InputBtn_S">繼續付款</span>
                                            </span>
                                        </span></span></span>
                                        </asp:HyperLink>
                                        <asp:Label ID="lblOrderStatus" Text="-" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="TableIndex">
                        <uc1:Pager ID="gridPager" runat="server" PageSize="10" OnGetCount="RetrieveOrderCount"
                            OnUpdate="UpdateHandler" />
                    </div>
                </asp:Panel>
            </div>
        </article>
    </section>
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using System.ComponentModel;

namespace LunchKingSite.Web.piinlife.member
{
    public partial class Pager : BaseUserControl
    {
        public delegate int GetCountHandler();
        public delegate void UpdateHandler(int pageNumber);

        private event GetCountHandler _getCountFunc = null;
        [Browsable(true), Category("Behavior"), Description("Handler for getting record count"), DesignOnly(true)]
        public event GetCountHandler GetCount
        {
            add { _getCountFunc += new GetCountHandler(value); }
            remove { _getCountFunc = null; }
        }

        private event UpdateHandler _updateFunc = null;
        [Browsable(true), Category("Behavior"), Description("Handler for getting record count"), DesignOnly(true)]
        public event UpdateHandler Update
        {
            add { _updateFunc += new UpdateHandler(value); }
            remove { _updateFunc = null; }
        }

        /// <summary>
        /// 資料總頁數
        /// </summary>
        private int _pageCount = 0;
        public int PageCount
        {
            get { return _pageCount; }
        }

        /// <summary>
        /// 一頁的資料顯示數量
        /// </summary>
        public int PageSize
        {
            get { return ViewState["ps"] != null ? (int)ViewState["ps"] : 15; }
            set { ViewState["ps"] = value; }
        }

        /// <summary>
        /// 目前頁數
        /// </summary>
        int _curPage = 0;
        public int CurrentPage
        {
            get { return Convert.ToInt32(hidCurrentPage.Value); }
            set { hidCurrentPage.Value = value.ToString(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetupPaging();
                ResolvePagerView(1);
            }
        }

        /// <summary>
        /// 取得 資料總頁數
        /// </summary>
        public void SetupPaging()
        {
            int totalRecords = 0;
            int pageSize = this.PageSize;

            if (_getCountFunc != null)
                totalRecords = _getCountFunc();
            else
                totalRecords = 0;

            _pageCount = totalRecords / pageSize;
            if (totalRecords % pageSize > 0)
                _pageCount++;

            lblPageCount.Text = _pageCount.ToString();

            lrc.Text = totalRecords.ToString();
        }

        public void ResolvePagerView(int currentPage)
        {
            ResolvePagerView(currentPage, false);
            if (!currentPage.Equals(PageCount))
                lblRecentCount.Text = (currentPage * PageSize).ToString();
            else
                lblRecentCount.Text = lrc.Text;
        }
        /// <summary>
        /// 設定pager
        /// </summary>
        /// <param name="currentPage"></param>
        /// <param name="recount"></param>
        public void ResolvePagerView(int currentPage, bool recount)
        {
            int pageCount = 1;

            if (recount)
            {
                SetupPaging();
            }

            int.TryParse(lblPageCount.Text, out pageCount);

            if (currentPage > pageCount)
                currentPage = 1;

            int nextPage = currentPage + 1;
            int prevPage = currentPage - 1;

            lPrev.Visible = lNext.Visible = lLast.Visible = lFirst.Visible = true;

            if (currentPage >= pageCount)
            {
                lNext.Visible = false;
                lLast.Visible = false;
            }
            if (currentPage == 1)
            {
                lPrev.Visible = false;
                lFirst.Visible = false;
            }

            hidCurrentPage.Value = currentPage.ToString();
            _curPage = currentPage;

            if (Convert.ToInt32(lblPageCount.Text) == 0)
                rpPage.Visible = false;
            
            SetPagerData(_curPage);
        }

        #region HyperLink Event

        protected void linkPage_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            string pageCommand = btn.CommandArgument;

            CurrentPage = Convert.ToInt32(btn.CommandArgument);

            //reload the grid
            if (_updateFunc != null)
                _updateFunc(CurrentPage);
            ResolvePagerView(CurrentPage);
        }

        protected void link_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            string pageCommand = btn.CommandArgument;
            int currentPage = CurrentPage;

            switch (pageCommand)
            {
                case "First":
                    currentPage = 1;
                    break;
                case "Prev":
                    currentPage--;
                    break;
                case "Next":
                    currentPage++;
                    break;
                case "Last":
                    currentPage = int.Parse(lblPageCount.Text);
                    break;
            }

            //reload the grid
            if (_updateFunc != null)
                _updateFunc(currentPage);
            ResolvePagerView(currentPage);
        }
        #endregion

        #region Private Method
        /// <summary>
        /// 設定Pager頁數資料
        /// </summary>
        /// <param name="curPage"></param>
        private void SetPagerData(int curPage)
        {
            int pagesLength = 7;
            int pageCount = Convert.ToInt32(lblPageCount.Text);
            int startPage = (curPage - pagesLength / 2) >= 1 ? curPage - pagesLength / 2 : 1;
            int endPage = pageCount < (curPage + pagesLength / 2) ? pageCount : (curPage + pagesLength / 2);
            List<int> pages = new List<int>();
            for (int i = startPage; i <= endPage; i++)
            {
                if (i <= pageCount)
                    pages.Add(i);
                else
                    break;
            }

            rpPage.DataSource = pages;
            rpPage.DataBind();

            SetLinkPagesCssClass(startPage, curPage, endPage);
        }
        /// <summary>
        /// 設定目前頁樣式
        /// </summary>
        private void SetLinkPagesCssClass(int startPage, int curPage, int endPage)
        {
            for (int i = 0; i < endPage - startPage + 1; i++)
            {
                LinkButton lkPages = (LinkButton)rpPage.Items[i].FindControl("lkPages");
                if (curPage.Equals(startPage + i))
                    lkPages.CssClass = "BtnSelected";
                else
                    lkPages.CssClass = "Btn";
            }
        }
        #endregion
    }
}
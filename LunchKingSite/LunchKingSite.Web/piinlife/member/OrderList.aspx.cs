﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System.Web.Security;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.piinlife.member
{
    public partial class OrderList : LocalizedBasePage, IHiDealMemberOrderListView
    {
        #region Property
        private HiDealMemberOrderListPresenter _presenter;
        public HiDealMemberOrderListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public string UserName
        {
            get { return User.Identity.Name; }
        }
        public int UserId
        {
            get { return MemberFacade.GetUniqueId(User.Identity.Name); }
        }
        public bool IsLatest
        {
            get { return cbx_LatestOrders.Checked; }
        }
        public CouponListFilterType FilterType
        {
            get
            {
                CouponListFilterType type;
                if (Enum.TryParse<CouponListFilterType>(ddl_Filter.SelectedValue, out type))
                    return type;
                else
                    return CouponListFilterType.None;
            }
        }
        public int PageCount
        {
            get
            {
                int pagecount;
                return int.TryParse((ViewState["pagecount"] == null ? string.Empty : ViewState["pagecount"].ToString()), out pagecount) ? pagecount : 0;
            }
            set
            {
                ViewState["pagecount"] = value;
            }
        }
        public int PageSize
        {
            get { return gridPager.PageSize; }
            set { gridPager.PageSize = value; }
        }
        public string ApiKey 
        {
            get { return hfApiKey.Value; }
            set { hfApiKey.Value = value; }
        }
        
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();

        #endregion

        #region EventHandler
        public event EventHandler<DataEventArgs<int>> PageChange = null;
        public event EventHandler FilterChange = null;
        #endregion

        #region Method
        public void GetOrderList(HiDealOrderShowCollection data)
        {
            repOrderList.DataSource = data;
            repOrderList.DataBind();
        }
        protected void ChangeFilter(object sender, EventArgs e)
        {
            if (this.FilterChange != null)
                this.FilterChange(this, e);
        }
        #endregion

        #region Page Method
        /// <summary>
        /// Page_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage("lt=1");
                return;
            }
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
                InitSetUp();
            }
            _presenter.OnViewLoaded();
        }
        #endregion

        #region Control Event

        #region Repeater
        protected void repOrderList_ItemBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is HiDealOrderShow)
            {
                DateTime now = DateTime.Now;
                Label lblUseCount = ((Label)e.Item.FindControl("lblUseCount"));//憑證狀態
                Label lblOrderStatus = ((Label)e.Item.FindControl("lblOrderStatus"));//訂單狀態
                HyperLink hkProductName = ((HyperLink)e.Item.FindControl("hkProductName"));//商品名稱
                HyperLink hkCouponList = ((HyperLink)e.Item.FindControl("hkCouponList"));//憑證鈕
                HyperLink hkOrderDetail = ((HyperLink)e.Item.FindControl("hkOrderDetail"));//訂單編號連結
                HyperLink hkPayAtm = ((HyperLink)e.Item.FindControl("hkPayAtm"));//ATM鈕
                HiDealOrderShow main = (HiDealOrderShow)(e.Item.DataItem);
                LinkButton lkService = (LinkButton)e.Item.FindControl("lkService");
                LinkButton lkBookingReservation = (LinkButton)e.Item.FindControl("lkBookingReservation");
                LinkButton lkBookingReservationRoom = (LinkButton)e.Item.FindControl("lkBookingReservationRoom");
                LinkButton lkQueryReservation = (LinkButton)e.Item.FindControl("lkQueryReservation");
                LinkButton lkQueryReservationRoom = (LinkButton)e.Item.FindControl("lkQueryReservationRoom");

                hkCouponList.NavigateUrl = hkOrderDetail.NavigateUrl = "~/piinlife/member/CouponList.aspx?oid=" + main.OrderGuid;
                hkOrderDetail.Text = main.OrderId;
                hkProductName.Text = string.Format("{0} - {1}", main.DealName, main.ProductName);
                hkProductName.NavigateUrl = "~/piinlife/HiDeal.aspx?pid=" + main.ProductId;
                lkService.Visible = ProviderFactory.Instance().GetConfig().PiinlifeServiceButtonVisible;

                // 訂單已取消
                if (main.OrderShowStatus.Equals((int)HiDealOrderShowStatus.RefundSuccess))
                {
                    hkCouponList.Visible = hkPayAtm.Visible = false;
                    lblOrderStatus.Text = "訂單已取消";
                }
                // 退貨處理中
                else if (main.OrderShowStatus.Equals((int)HiDealOrderShowStatus.RefundProcessing) && main.RemainCount > 0)
                {
                    hkCouponList.Visible = hkPayAtm.Visible = false;
                    lblOrderStatus.Text = "退貨處理中";
                }
                // 退貨失敗
                else if (main.OrderShowStatus.Equals((int)HiDealOrderShowStatus.RefundFail) && main.RemainCount > 0)
                {
                    hkCouponList.Visible = hkPayAtm.Visible = false;
                    lblOrderStatus.Text = "退貨失敗";
                }
                // 宅配商品
                else if (main.DeliveryType.Equals((int)HiDealDeliveryType.ToHouse))
                {
                    lblUseCount.Text = "宅配商品";
                    hkCouponList.Visible = false;
                    HiDealProduct HiDealPro = hp.HiDealProductGet(main.ProductId);
                    //撈取出貨回覆日期，否則使用UseEndTime
                    DateTime useEndTime = (HiDealPro.ShippedDate.HasValue) ? Convert.ToDateTime(HiDealPro.ShippedDate) : Convert.ToDateTime(HiDealPro.UseEndTime);
                    
                    //if (HiDealReturnedManager.CheckIsOverTrialPeriod(HiDealPro.Id, useEndTime, main.OrderGuid)) //todo: charlene保留
                    if (HiDealPro.UseEndTime != null && useEndTime.AddDays(config.GoodsAppreciationPeriod).Date < DateTime.Now.Date)
                    {
                        lblOrderStatus.Text = "已過鑑賞期";
                    }
                    else
                    {
                        //訂單狀態顯示出貨資訊
                        //商品檔出貨資訊顯示需區別新/舊對帳方式而有所不同
                        //舊對帳方式
                        if (HiDealPro.VendorBillingModel.Equals((int)VendorBillingModel.None))
                            lblOrderStatus.Text = "最後出貨日<br/>" + useEndTime.ToString("yyyy/MM/dd");
                        else //新對帳方式
                        {
                            //檢查是否已出貨
                            //暫時隱藏出貨資訊 待確認廠商確實填寫出貨資訊後再顯示
                            //if (PponOrderManager.CheckIsShipped(main.OrderGuid, OrderClassification.HiDeal))
                            //    lblOrderStatus.Text = "已出貨";
                            //else
                            lblOrderStatus.Text = "最後出貨日<br/>" + useEndTime.ToString("yyyy/MM/dd");

                        }

                        //ATM
                        if (main.PaymentType.Equals((int)HiDealOrderShowPaymentType.ATM) && !main.IsOrderComplete && main.OrderEndTime > now)
                        {
                            lblUseCount.Text = string.Empty;
                            if (main.OrderCreateTime.Year == now.Year && main.OrderCreateTime.Month == now.Month && main.OrderCreateTime.Day == now.Day)
                            {
                                hkPayAtm.Visible = true;
                                lblOrderStatus.Visible = false; //ATM未付款無須顯示出貨資訊
                            }
                            else
                            {
                                hkPayAtm.Visible = false;
                                lblOrderStatus.Text = "逾期未付款";
                            }
                        }
                    }
                }
                // 非宅配商品
                else
                {
                    HiDealProduct HiDealPro = hp.HiDealProductGet(main.ProductId);
                    //撈取出貨回覆日期，否則使用UseEndTime
                    DateTime useEndTime =  Convert.ToDateTime(HiDealPro.UseEndTime);

                    // 無未使用憑證
                    if (main.RemainCount.Equals(0))
                    {

                        if ( HiDealPro.BookingSystemType == (int)BookingType.Coupon  && !string.IsNullOrEmpty(ApiKey))
                        {
                           lkQueryReservation.OnClientClick = "return openBookingSystem('','" + ApiKey + "');";
                           lkQueryReservation.Visible = true;
                        }
                        hkCouponList.Visible = false;
                        lblUseCount.Text = "使用完畢";
                    }
                    else if (useEndTime != null && useEndTime.AddDays(1) < now)
                    {
                        hkCouponList.Visible = false;
                        lblUseCount.Text = "憑證已過期";
                    }
                    // ATM 未付款
                    else if (main.PaymentType.Equals((int)HiDealOrderShowPaymentType.ATM) && !main.IsOrderComplete)
                    {
                        hkCouponList.Visible = false;
                        lblUseCount.Text = string.Empty;
                        if (main.OrderCreateTime.Year == now.Year && main.OrderCreateTime.Month == now.Month && main.OrderCreateTime.Day == now.Day)
                        {
                            hkPayAtm.Visible = true;
                        }
                        else
                        {
                            hkPayAtm.Visible = false;
                            lblOrderStatus.Text = "逾期未付款";
                        }
                    }
                    else
                    {
                        if (HiDealPro.BookingSystemType == (int)BookingType.Coupon || HiDealPro.BookingSystemType == (int)BookingType.Travel)
                        {
                            bool HaveBookingSetting = BookingSystemFacade.CheckStoreBookingSetting(main.OrderGuid.ToString(), (BookingType)HiDealPro.BookingSystemType);

                            if (HaveBookingSetting)
                            {
                                if (HaveBookingSetting && BookingSystemFacade.CheckCouponsStatus(main.OrderGuid.ToString(), (BookingType)HiDealPro.BookingSystemType, BookingSystemServiceName.HiDeal))
                                {
                                    lkBookingReservationRoom.OnClientClick = lkBookingReservation.OnClientClick = "return openBookingSystem('" + main.OrderGuid + "','" + ApiKey + "');";
                                    if (HiDealPro.BookingSystemType == (int)BookingType.Travel)
                                    {
                                        lkBookingReservationRoom.Visible = true;
                                    }
                                    else if (HiDealPro.BookingSystemType == (int)BookingType.Coupon)
                                    {
                                        lkBookingReservation.Visible = true;
                                    }
                                }
                                else
                                {
                                    lkQueryReservationRoom.OnClientClick = lkQueryReservation.OnClientClick = "return openBookingSystem('','" + ApiKey + "');";
                                    if (HiDealPro.BookingSystemType == (int)BookingType.Travel)
                                    {
                                        lkQueryReservationRoom.Visible = true;
                                    }
                                    else if (HiDealPro.BookingSystemType == (int)BookingType.Coupon)
                                    {
                                        lkQueryReservation.Visible = true;
                                    }
                                }
                            }
                        }
                        lblUseCount.Text = string.Empty;
                    }
                }

                lkService.PostBackUrl = ResolveUrl("~" + config.SiteServiceUrl);
            }
        }

        protected void repOrderList_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "ShowDetail":
                    Response.Redirect("CouponList.aspx?oid=" + e.CommandArgument);
                    break;
            }
        }
        #endregion

        #region Pager
        protected int RetrieveOrderCount()
        {
            return PageCount;
        }
        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChange != null)
                this.PageChange(this, new DataEventArgs<int>(pageNumber));
        }
        public void SetUpPageCount()
        {
            gridPager.ResolvePagerView(PageCount > 0 ? 1 : 0, true);
        }
        #endregion

        #endregion

        #region Private Method
        /// <summary>
        /// 篩選條件
        /// </summary>
        private void InitSetUp()
        {
            ddl_Filter.Items.Clear();
            ddl_Filter.Items.AddRange(WebUtility.GetListItemFromEnum(CouponListFilterType.None).Where(x => x.Value != "3" && x.Value != "4").ToArray());
        }
        #endregion
    }

}
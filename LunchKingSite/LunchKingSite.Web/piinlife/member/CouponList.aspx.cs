﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System.Web.Security;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.Component;
using System.Web.Services;
using log4net;
  
namespace LunchKingSite.Web.piinlife.member
{
    public partial class CouponList : LocalizedBasePage, IHiDealMemberCouponListView
    {
        DateTime DateTimeNow = DateTime.Now;
        private static ILog logger = LogManager.GetLogger(typeof(CouponList));
        protected static ISysConfProvider sysConf = ProviderFactory.Instance().GetConfig();

        #region Enum
        /// <summary>
        /// 索取憑證
        /// </summary>
        private enum CouponGetType
        {
            Print,
            PrintText,
            Download,
            DownloadText,
            Sms,
            SmsText,
        }
        #endregion

        #region property
        private HiDealMemberCouponListPresenter _presenter;
        public HiDealMemberCouponListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public int OrderStatus
        {
            get
            {
                int orderstatus;
                return int.TryParse(hidOrderStatus.Value, out orderstatus) ? orderstatus : 0;
            }

            set { hidOrderShowStatus.Value = value.ToString(); }
        }

        public int OrderShowStatus
        {
            get
            {
                int ordershowstatus;
                return int.TryParse(hidOrderShowStatus.Value, out ordershowstatus) ? ordershowstatus : 0;
            }

            set { hidOrderShowStatus.Value = value.ToString(); }
        }
        public string UserName
        {
            get { return User.Identity.Name; }
        }

        public int UserId
        {
            get { return MemberFacade.GetUniqueId(User.Identity.Name); }
        }

        public string UserEmail
        {
            get { return User.Identity.Name; }
        }
        //public DateTime ExpiredTime
        //{
        //    get
        //    {
        //        DateTime expiredtime;
        //        return DateTime.TryParse(hidExpiredTime.Value, out expiredtime) ? expiredtime : DateTime.Now;
        //    }
        //}
        public DateTime OrderEndDate
        {
            get
            {
                DateTime orderenddate;
                return DateTime.TryParse(hidOrderEndDate.Value, out orderenddate) ? orderenddate : DateTime.Now;
            }
            set
            {
                hidOrderEndDate.Value = value.ToString();
            }
        }
        public Guid OrderGuid
        {
            get
            {
                Guid oGuid = Guid.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["oid"]))
                {
                    try { oGuid = new Guid(Request.QueryString["oid"]); }
                    catch { oGuid = Guid.Empty; }
                }
                return oGuid;
            }
        }
        public int? OrderPkCondition
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["opk"]))
                {
                    int orderPk;
                    if (int.TryParse(Request.QueryString["opk"], out orderPk))
                    {
                        return orderPk;
                    }
                }
                return null;
            }
        }


        public string Mobile
        {
            get
            {
                return hidMobile.Value;
            }
            set
            {
                hidMobile.Value = value;
            }
        }
        public string ProductName
        {
            get
            {
                return hidProductName.Value;
            }
            set
            {
                hidProductName.Value = value;
            }
        }
        public string OrderId
        {
            get
            {
                return lblOrderId.Text;
            }
            set
            {
                lblOrderId.Text = value;
            }
        }
        public int OrderPk
        {
            get
            {
                int orderPk;
                return int.TryParse(hidOrderPk.Value, out orderPk) ? orderPk : 0;
            }
            set
            {
                hidOrderPk.Value = value.ToString();
            }
        }
        public bool CouponListVisible
        {
            get
            {
                bool value;
                return bool.TryParse(hidCouponListVisible.Value, out value) ? value : false;
            }
            set
            {
                hidCouponListVisible.Value = value.ToString();
            }
        }
        public int PaymentType
        {
            get
            {
                int paymentType;
                return int.TryParse(hidPaymentType.Value, out paymentType) ? paymentType : 0;
            }
        }

        public bool HidealSmsVisible
        {
            get
            {
                return bool.Parse(hidealSmsVisible.Value);
            }
            set
            {
                hidealSmsVisible.Value = value.ToString();
            }
        }

        public string RefundFormUrlShow { get; set; }
        public string DiscountSingleFormUrlShow { get; set; }
        public bool IsCreateEInvoice { get; set; }

        #endregion

        #region event
        //public event EventHandler GetOrderDetail = null;
        public event EventHandler<DataEventArgs<HiDealSMSQuery>> SendSMS = null;
        public event EventHandler<DataEventArgs<HiDealSMSQuery>> GetSMS = null;
        public event EventHandler<DataEventArgs<MultipleEinvoiceRequestQuery>> RequestEinvoice = null;
        #endregion

        #region method
        public void SetCouponList(ViewHiDealCouponListSequenceCollection vhcsc)
        {
            // 優惠憑證內容
            pan_OrderDetailSequence.Visible = CouponListVisible;

            if (vhcsc.Count > 0)
            {
                lblProductName.Text = hidProductName.Value;
                repCouponList.DataSource = vhcsc;
                repCouponList.DataBind();
            }
        }

        public void SetShipInfo(ViewOrderShipList osInfo, DateTime? deliveryDate)
        {
            if (osInfo != null) //已成立訂單才需顯示出貨資訊
            {
                //配送資訊
                //憑證訂單不顯示配送資訊
                pan_ShipInfo.Visible = !CouponListVisible;
                //收件人資訊
                lab_RecipientName.Text = osInfo.MemberName;
                lab_DeliveryAddress.Text = osInfo.DeliveryAddress;
                //已出貨:顯示廠商出貨資訊

                if (deliveryDate.HasValue)
                {
                    if (DateTime.Now.Date.CompareTo(deliveryDate.Value.AddDays(sysConf.GoodsAppreciationPeriod).Date) > 0)
                    {
                        lit_ShippedInfo.Text = "已過鑑賞期";
                    }
                    else
                    {
                        lit_ShippedInfo.Text = "最後出貨日：" + deliveryDate.Value.ToString("yyyy/MM/dd");
                    }
                }
            }
            else
                pan_ShipInfo.Visible = false;
        }

        public void SetOrderDetail(HiDealOrderDetailCollection hdodc)
        {
            // 訂單明細            
            if (hdodc.Count > 0)
            {
                rpt_Details.DataSource = hdodc.Where(x => x.ProductType == (int)HiDealProductType.Product);
                rpt_Details.DataBind();
            }

            //收據
            HiDealOrderDetail hdod = hdodc.FirstOrDefault();
            if (hdod != null)
            {
                hidDownloadReceiptLink.Value = string.Format("../../service/receipt.ashx?ogid={0}&mode=piin", hdod.HiDealOrderGuid);
            }
        }

        public void SetOrderCategory(HiDealOrderDetailCollection hdodc)
        {
            // 訂單備註
            IList<HiDealOrderDetail> hodList = hdodc.Where(x => x.ProductType == (int)HiDealProductType.Product && x.Category1 != null).ToList<HiDealOrderDetail>();
            if (hodList.Count > 0)
            {
                rpProductCategory.DataSource = hodList;
                rpProductCategory.DataBind();
            }
            else
                divOrderCategory.Visible = false;
        }

        public void SetOrderPaymentDetail(CashTrustLogCollection ctlc, DiscountCode dc)
        {
            // 付款明細
            ClearDetailText("0", lab_ODInfo_Ship, lab_ODInfo_CreditCard, lab_ODInfo_Bouns, lab_ODInfo_Scach, lab_ODInfo_Discount, lab_ODInfo_PEZ, lab_ODInfo_Total, lab_ODInfo_TotalAmount);
            int shipamount = ctlc.Where(x => (x.SpecialStatus & 16) > 0).Sum(x => x.CreditCard + x.Pcash + x.Scash + x.Atm + x.Bcash);
            lab_ODInfo_Ship.Text = shipamount == 0 ? "免運費" : shipamount.ToString("F0");
            lab_ODInfo_CreditCard.Text = ctlc.Sum(x => x.CreditCard).ToString("F0");
            lab_ODInfo_Bouns.Text = ctlc.Sum(x => x.Bcash).ToString("F0");
            lab_ODInfo_Scach.Text = ctlc.Sum(x => x.Scash).ToString("F0");
            lab_ODInfo_DiscountCode.Text = dc.Code;
            lab_ODInfo_Discount.Text = ctlc.Sum(x => x.DiscountAmount).ToString("F0");
            lab_ODInfo_PEZ.Text = ctlc.Sum(x => x.Pcash).ToString("F0");

            // 訂單明細
            // 抵扣金額 = Bcash + Scash + DiscountAmount + Pcash
            decimal amount = ctlc.Sum(x => x.Bcash + x.Scash + x.DiscountAmount + x.Pcash);
            lab_ODInfo_DiscountAmount.Text = amount != 0 ? "-" + amount.ToString("F0") : "0";
            // 刷卡或ATM付款總金額 = CreditCard + Atm
            lab_ODInfo_TotalAmount.Text = ctlc.Sum(x => x.CreditCard + x.Atm).ToString("F0");
        }

        public void SetOrderReturned(HiDealReturnedCollection hdrc, CashTrustLogCollection ctlc)
        {
            // 退貨明細
            Dictionary<HiDealReturned, IList<string>> list = GetReturnedList(hdrc, ctlc);
            pan_OrderDetailRefund.Visible = list.Count > 0;
            RefundFormUrlShow = "#";
            DiscountSingleFormUrlShow = "#";

            if (list.Count > 0)
            {
                if (list.Keys.OrderByDescending(x => x.Id).First().Status == (int)HiDealReturnedStatus.Create)
                {
                    phReturnedDownload.Visible = true;
                    string pageId = HiDealReturnedManager.GetRefundFormPageId(list.Keys.OrderByDescending(x => x.Id).First().Id, this.OrderPk, this.UserName);
                    //hkReturnedDownload.NavigateUrl = ResolveClientUrl("~/service/hidealrefundform.ashx") + "?id=" + pageId;
                    //退貨申請書
                    RefundFormUrlShow = sysConf.SiteUrl + "/service/hidealrefundform.ashx?FormType=RAF&id=" + pageId;
                    //折讓單
                    if (IsCreateEInvoice)
                    {
                        phDiscountSingleDownload.Visible = true;
                        DiscountSingleFormUrlShow = sysConf.SiteUrl + "/service/hidealrefundform.ashx?FormType=DSF&id=" + pageId;
                    }
                }
            }

            repRefund.DataSource = list;
            repRefund.DataBind();
        }

        public void SetEnvoiceDetail(List<ViewEinvoiceMail> viewEinvoices, List<EinvoiceAllowanceInfo> veac)
        {
            // 發票明細
            pan_EinvoiceMain.Visible = false;
            panEinvoiceInfo.Visible = false;
            OldInvoiceContainer.Visible = false;
            NewInvoiceContainer.Visible = false;
            ViewEinvoiceMail einvoice = viewEinvoices.FirstOrDefault();
            invoiceFooter.Version = (InvoiceVersion)einvoice.Version;

            if (einvoice != null && !String.IsNullOrEmpty(einvoice.InvoiceNumber))
            {
                pan_EinvoiceMain.Visible = true;
                IsCreateEInvoice = true;
                if (einvoice.IsDonateMark)
                {
                    panEinvoiceInfo.Visible = true;
                    pan_PaperRequest.Visible = false;
                    lbl_Einvoice_Others.Text =
                        string.Format("發票已捐贈：{0}", LoveCodeManager.Instance.QueryLoveCodeString(einvoice.LoveCode));
                    invoiceFooter.Visible = false;
                }
                else
                {
                    pan_EinvoiceDetail.Visible = true;
                    OldInvoiceContainer.Visible = true;
                    //可申請紙本的發票
                    //2014/03/01後，我們可不另開紙本，會員只有在購買時可以選2聯紙本。
                    lblEinvoicePaperRequest.Visible = einvoice.InvoiceRequestTime.HasValue;
                    lblEinvoiceWinner.Visible = einvoice.InvoiceWinning;
                    if (einvoice.Version == (int)InvoiceVersion.Old)
                    {
                        pan_PaperRequest.Visible = (einvoice.InvoicePaperedTime.HasValue == false);                        
                        lblEinvoicePaperRequest.Text = "●您已索取紙本發票";                        
                        pan_PaperRequest.Visible = (einvoice.InvoiceRequestTime.HasValue == false);
                    }
                    else if (einvoice.Version == (int)InvoiceVersion.CarrierEra)
                    {
                        lblEinvoicePaperRequest.Text = "●您已索取電子發票紙本";
                        pan_PaperRequest.Visible = false;
                    }

                    decimal saleamount = Math.Round((einvoice.OrderAmount / (1 + einvoice.InvoiceTax)), 0);
                    lbl_Einvoice_InvoiceNum.Text = einvoice.InvoiceNumber;
                    lbl_Einvoice_InvoiceDate.Text = einvoice.InvoiceNumberTime.Value.AddYears(-1911)
                        .ToString("yyyy年MM月dd日").TrimStart('0');

                    lbl_Einvoice_BuyerAddress.Text = einvoice.InvoiceBuyerAddress;
                    lbl_Einvoice_BuyerName.Text = (einvoice.IsEinvoice3) ? einvoice.InvoiceComName : einvoice.InvoiceBuyerName;
                    lbl_Einvoice_ComId.Text = einvoice.InvoiceComId;
                    lbl_Einvoice_OrderId.Text = einvoice.OrderId;
                    lbl_Einvoice_OrderAmount1.Text = lbl_Einvoice_OrderAmount2.Text = (einvoice.IsEinvoice3) ? saleamount.ToString("F0") : einvoice.OrderAmount.ToString("F0");
                    lbl_Einvoice_OrderAmount3.Text = einvoice.OrderAmount.ToString("F0");
                    lbl_Einvoice_HasTax.Text = einvoice.InvoiceTax > 0 ? "V" : string.Empty;
                    lbl_Einvoice_NoTax.Text = einvoice.InvoiceTax == 0 ? "V" : string.Empty;
                    lbl_Einvoice_TaxAmount.Text = (einvoice.IsEinvoice3) ? (einvoice.OrderAmount - saleamount).ToString("F0") : "0";
                    litEinvoicePass.Text = einvoice.InvoicePassAsString;
                    lbl_Einvoice_ChineseAmount.Text = CommonFacade.GetChineseAmountString((int)einvoice.OrderAmount) + I18N.Phrase.Dollar;
                    if (einvoice.OrderIsPponitem || decimal.Equals(0, einvoice.InvoiceTax))
                    {
                        lbl_Einvoice_ItemName.Text = string.Format("{0}-{1}", einvoice.SellerName, ProductName);
                    }
                    else
                    {
                        lbl_Einvoice_ItemName.Text = string.Format("17Life購物金-{0}", einvoice.OrderItem);
                        lbl_Einvoice_OrderItem.Text = einvoice.OrderItem; //備註部分: 只有應稅憑證型才需要
                    }
                }
                if ((OrderStatus & ((int)Core.OrderStatus.Cancel)) > 0 && (veac.Sum(x => x.Amount)) == einvoice.OrderAmount)
                {
                    pan_EinvoiceDetail.Visible = OldInvoiceContainer.Visible = panEinvoiceInfo.Visible = pan_EinvoiceMain.Visible = false;
                }
            }
            else if (lab_ODInfo_CreditCard.Text == "0" && (OrderStatus & (int)Core.OrderStatus.ATMOrder) == 0)
            {
                pan_EinvoiceMain.Visible = panEinvoiceInfo.Visible = false;
                lbl_Einvoice_Others.Text = "不開立發票:本訂單消費僅使用購物金/紅利折抵金額";
                pan_PaperRequest.Visible = false;
                invoiceFooter.Visible = false;
            }
        }

        private Dictionary<int, string> couponSN = new Dictionary<int, string>();
        public void SetNewEInvoiceDetail(ViewEinvoiceMailCollection mails, CashTrustLogCollection ctlogs)
        {
            /*
             * EinvoiceMain
             * --EinvoiceDetail
             * ----OldInvoiceContainer
             * ----NewInvoiceContainer
             * --EinvoiceInfo
             */
            pan_EinvoiceMain.Visible = panEinvoiceInfo.Visible = OldInvoiceContainer.Visible
                = NewInvoiceContainer.Visible = pan_EinvoiceDetail.Visible = false;

            List<int> nonReturnBackCouponIds = ctlogs
                    .Where(x => x.Status != (int)TrustStatus.Refunded && x.Status != (int)TrustStatus.Returned)
                    .Select(x => x.CouponId ?? 0).ToList();     // 退貨/刷退 以外的 coupon id

            //可顯示在畫面上的發票 (有發票號碼 & 發票沒作廢 & 憑證型 & 沒退貨)
            List<ViewEinvoiceMail> viewableInvoice = mails.Where(x =>
                        string.IsNullOrEmpty(x.InvoiceNumber) == false
                        && x.InvoiceStatus != (int)EinvoiceType.C0501
                        && x.CouponId.HasValue
                        && nonReturnBackCouponIds.Contains(x.CouponId.Value)
                    ).ToList();

            if (viewableInvoice.Count > 0)
            {
                pan_EinvoiceMain.Visible = true;
                pan_EinvoiceDetail.Visible = true;
                NewInvoiceContainer.Visible = true;
                IsCreateEInvoice = true;
                ViewEinvoiceMail oneEinvoice = viewableInvoice.First();
                invoiceFooter.Version = (InvoiceVersion)oneEinvoice.Version;

                //索取紙本按鈕預設隱藏
                pan_PaperRequest.Visible = false;
                if (oneEinvoice.IsDonateMark)
                {
                    lbl_Einvoice_Others.Text =
                        string.Format("發票已捐贈：{0}", LoveCodeManager.Instance.QueryLoveCodeString(oneEinvoice.LoveCode));
                    invoiceFooter.Visible = false;
                    panEinvoiceInfo.Visible = true;
                    return;
                }

                //可申請紙本的發票
                List<string> printableCouponNumbers = viewableInvoice
                    .Where(t => t.InvoiceRequestTime.HasValue == false && t.InvoiceWinning == false)
                    .Select(t => t.InvoiceNumber).ToList();
                if (oneEinvoice.Version == (int)InvoiceVersion.Old)
                {
                    pan_PaperRequest.Visible = printableCouponNumbers.Count > 0;
                }
                else
                {
                    pan_PaperRequest.Visible = false;
                }

                foreach (ViewEinvoiceMail invoice in viewableInvoice)
                {
                    if (invoice.CouponId.HasValue)
                    {
                        IEnumerable<CashTrustLog> logs = ctlogs.Where(x => x.CouponId == invoice.CouponId);
                        if (logs.Any())
                        {
                            couponSN.Add(invoice.CouponId.Value, logs.First().CouponSequenceNumber);
                        }
                    }
                }

                Invoices.DataSource = viewableInvoice;
                Invoices.DataBind();
            }
        }

        public void Invoices_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ViewEinvoiceMail em = (ViewEinvoiceMail)e.Item.DataItem;

            if (em != null)
            {
                Label lblEinvoiceWinner = (Label)e.Item.FindControl("lblEinvoiceWinner");
                Label lblEinvoicePaperRequest = (Label)e.Item.FindControl("lblEinvoicePaperRequest");
                Literal litInvoiceDate = (Literal)e.Item.FindControl("litInvoiceDate");
                Literal litInvoiceDateMon = (Literal)e.Item.FindControl("litInvoiceDateMon");
                Literal litBuyerName = (Literal)e.Item.FindControl("litBuyerName");
                Literal litEinvoicePass = (Literal)e.Item.FindControl("litEinvoicePass");
                Literal litItemName = (Literal)e.Item.FindControl("litItemName");     //品名
                Literal litCouponSequenceNumber = (Literal)e.Item.FindControl("litCouponSequenceNumber"); //憑證編號
                Literal litMemo = (Literal)e.Item.FindControl("litMemo");    //備註
                Literal litItemUnitPrice = (Literal)e.Item.FindControl("litItemUnitPrice");    //單價
                Literal litItemSumAmount = (Literal)e.Item.FindControl("litItemSumAmount"); //金額
                Literal litTotalAmount = (Literal)e.Item.FindControl("litTotalAmount");    //總計
                Literal litHasTax = (Literal)e.Item.FindControl("litHasTax");
                Literal litTaxAmount = (Literal)e.Item.FindControl("litTaxAmount");
                Literal litNoTax = (Literal)e.Item.FindControl("litNoTax");
                Literal litChineseAmount = (Literal)e.Item.FindControl("litChineseAmount");

                lblEinvoicePaperRequest.Visible = em.InvoiceRequestTime.HasValue;
                lblEinvoiceWinner.Visible = em.InvoiceWinning;
                if (em.Version == (int)InvoiceVersion.Old)
                {
                    lblEinvoicePaperRequest.Text = "●您已索取紙本發票";
                }
                else
                {
                    lblEinvoicePaperRequest.Text = "●您已索取電子發票紙本";
                }

                litEinvoicePass.Text = em.InvoicePassAsString;
                if (int.Parse(em.InvoiceNumberTime.Value.Month.ToString()) % 2 == 1)
                {
                    litInvoiceDateMon.Text = em.InvoiceNumberTime.Value.AddYears(-1911).ToString("yyyy 年 ").TrimStart('0') + em.InvoiceNumberTime.Value.Month.ToString().PadLeft(2, '0') + "-" + (int.Parse(em.InvoiceNumberTime.Value.Month.ToString()) + 1).ToString().PadLeft(2, '0') + "月";
                }
                else
                {
                    litInvoiceDateMon.Text = em.InvoiceNumberTime.Value.AddYears(-1911).ToString("yyyy 年 ").TrimStart('0') + (int.Parse(em.InvoiceNumberTime.Value.Month.ToString()) - 1).ToString().PadLeft(2, '0') + "-" + em.InvoiceNumberTime.Value.Month.ToString().PadLeft(2, '0') + "月";
                }   
                litInvoiceDate.Text = em.InvoiceNumberTime.Value.AddYears(-1911)
                    .ToString("yyyy-MM-dd hh:mm:ss").TrimStart('0');

                string sn;
                if (em.CouponId.HasValue && couponSN.TryGetValue(em.CouponId.Value, out sn))
                {
                    litCouponSequenceNumber.Text = sn;
                }

                if (int.Equals((int)InvoiceMode2.Triplicate, em.InvoiceMode2)) //是否三聯式發票
                {
                    decimal saleAmount = Math.Round((em.OrderAmount / (1 + em.InvoiceTax)), 0);
                    litBuyerName.Text = em.InvoiceComName;
                    litItemUnitPrice.Text = litItemSumAmount.Text = saleAmount.ToString("F0");
                    litTaxAmount.Text = (em.OrderAmount - saleAmount).ToString("F0");
                }
                else
                {
                    litBuyerName.Text = em.InvoiceBuyerName;
                    litItemUnitPrice.Text = litItemSumAmount.Text = em.OrderAmount.ToString("F0");
                    litTaxAmount.Text = "0";
                }

                litTotalAmount.Text = em.OrderAmount.ToString("F0");

                if (decimal.Equals(0, em.InvoiceTax) || em.OrderIsPponitem)
                {
                    litItemName.Text = em.OrderItem;
                }
                else
                {
                    litItemName.Text = string.Format("17Life購物金-{0}", em.OrderItem);
                    litMemo.Text = em.OrderItem;
                }

                if (decimal.Equals(0, em.InvoiceTax))
                {
                    litNoTax.Text = "V";
                }
                else
                {
                    litHasTax.Text = "V";
                }

                litChineseAmount.Text = CommonFacade.GetChineseAmountString((int)em.OrderAmount) + I18N.Phrase.Dollar;
            }
        }

        public void FinishEinvoiceRequestRequest()
        {
            lblEinvoicePaperRequest.Visible = true;
            btn_RequestPaperInvoice.Visible = false;
            ScriptManager.RegisterStartupScript(this, typeof(Button), "panblock", "getdetailheight();", true);
        }

        public void GetAlreadySmsCount(int count, int type)
        {
            hidSmsAlreadyCount.Value = count.ToString();
            hidSmsRemainCount.Value = (3 - count).ToString();
            if (type == 1) // 您已索取超過三次，無法再索取
                ScriptManager.RegisterStartupScript(this, typeof(Button), "refundsuccess", "getsmsmobile(2);block_ui($('#Pop_SMS3'),0,0);", true); // 已索取超過三次
            else if (type == 0)
                ScriptManager.RegisterStartupScript(this, typeof(Button), "refundsuccess", "getsmsmobile(1);block_ui($('#Pop_SMS1'),0,0);", true); // 索取簡訊憑證
            else // 發送成功
                ScriptManager.RegisterStartupScript(this, typeof(Button), "refundsuccess", "getsmsmobile(3);block_ui($('#Pop_SMS3'),0,0);", true); // 已索取超過三次
        }

        public void ShowAlert(string msg)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Button), "alert", "alert('" + msg + "');", true);
        }

        public void IlleagalUser()
        {
            Response.Redirect("../default.aspx");
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (String.IsNullOrEmpty(Request.QueryString["oid"]))
            //    Response.Redirect("~/default.aspx");

            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage("lt=1");
                return;
            }
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
                lkService.PostBackUrl = ResolveUrl("~" + sysConf.SiteServiceUrl);
                lkService.Visible = sysConf.PiinlifeServiceButtonVisible;
            }
            _presenter.OnViewLoaded();

            //if (GetOrderDetail != null)
            //    this.GetOrderDetail(this, e);
        }

        protected void repCouponList_ItemBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewHiDealCouponListSequence)
            {
                ViewHiDealCouponListSequence coupon = (ViewHiDealCouponListSequence)e.Item.DataItem;
                Label clab_rpUseCount = ((Label)e.Item.FindControl("lab_rpCouponSequenceStatus"));
                Label clab_rpOrderStatus = ((Label)e.Item.FindControl("lab_rpCouponSequenceTrustStatus"));

                HyperLink hyp_rpCouponSequencePrint = ((HyperLink)e.Item.FindControl("hyp_rpCouponSequencePrint"));
                HyperLink hyp_rpCouponSequencePrintText = ((HyperLink)e.Item.FindControl("hyp_rpCouponSequencePrintText"));
                HyperLink hyp_rpCouponSequenceDownLoad = ((HyperLink)e.Item.FindControl("hyp_rpCouponSequenceDownLoad"));
                HyperLink hyp_rpCouponSequenceDownLoadText = ((HyperLink)e.Item.FindControl("hyp_rpCouponSequenceDownLoadText"));

                HyperLink hyp_email = ((HyperLink)e.Item.FindControl("hyp_email"));
                HyperLink hyp_emailtext = ((HyperLink)e.Item.FindControl("hyp_emailtext"));

                LinkButton clbt_rpCouponSequenceSMS = ((LinkButton)e.Item.FindControl("lbt_rpCouponSequenceSMS"));
                LinkButton clbt_rpCouponSequenceSMSText = ((LinkButton)e.Item.FindControl("lbt_rpCouponSequenceSMSText"));
                HiddenField chif_rpCouponId = ((HiddenField)e.Item.FindControl("hif_rpCouponId"));

                Dictionary<string, LinkButton> lkList = new Dictionary<string, LinkButton>();
                lkList.Add(CouponGetType.Sms.ToString(), clbt_rpCouponSequenceSMS);
                lkList.Add(CouponGetType.SmsText.ToString(), clbt_rpCouponSequenceSMSText);

                Dictionary<string, HyperLink> hypList = new Dictionary<string, HyperLink>();
                hypList.Add(CouponGetType.Print.ToString(), hyp_rpCouponSequencePrint);
                hypList.Add(CouponGetType.PrintText.ToString(), hyp_rpCouponSequencePrintText);
                hypList.Add(CouponGetType.Download.ToString(), hyp_rpCouponSequenceDownLoad);
                hypList.Add(CouponGetType.DownloadText.ToString(), hyp_rpCouponSequenceDownLoadText);


                // 預設狀態
                VisibleDisableControl(false, false, false, chif_rpCouponId.Value, lkList, hypList, false, hyp_email, hyp_emailtext);

                #region 信託狀態
                if (coupon.BankStatus == 0)
                    clab_rpOrderStatus.Text = "信託準備中";
                else if (coupon.BankStatus == 1)
                    clab_rpOrderStatus.Text = "已信託";
                else if (coupon.BankStatus == 3)
                    clab_rpOrderStatus.Text = "信託已核銷";
                #endregion

                #region 憑證狀態
                // 未進信託
                if (coupon.CtlStatus < (int)TrustStatus.Initial)
                {
                    CouponReturned(coupon, clab_rpUseCount);
                }
                else // 已進信託
                {
                    // 未核銷
                    if (coupon.CtlStatus < (int)TrustStatus.Verified)
                    {
                        BusinessHourCloseOrNonClose(coupon, clab_rpUseCount, lkList, hypList, chif_rpCouponId, hyp_email, hyp_emailtext);
                    }
                    // 已核銷
                    else if (coupon.CtlStatus.Equals((int)TrustStatus.Verified))
                    {
                        clab_rpUseCount.Text = "已使用";
                    }
                    // 已退貨
                    else if (coupon.CtlStatus > (int)TrustStatus.Verified)
                    {
                        CouponReturned(coupon, clab_rpUseCount);
                    }
                }
                // 退貨處理中
                if (OrderShowStatus.Equals((int)HiDealOrderShowStatus.RefundProcessing))
                {
                    VisibleDisableControl(false, false, false, chif_rpCouponId.Value, lkList, hypList, false, hyp_email, hyp_emailtext);
                }
                // 取消單不發簡訊
                if (OrderShowStatus.Equals((int)HiDealOrderShowStatus.RefundSuccess))
                {
                    VisibleDisableControlSMS(false, clbt_rpCouponSequenceSMS, clbt_rpCouponSequenceSMSText);
                }
                #endregion
            }
        }

        protected void repCouponList_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "SendMS":
                    hidCouponId.Value = e.CommandArgument.ToString();
                    if (GetSMS != null)
                        this.GetSMS(this, new DataEventArgs<HiDealSMSQuery>(new HiDealSMSQuery(e.CommandArgument.ToString(), Mobile)));
                    break;
            }
        }

        protected void rpProductCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is HiDealOrderDetail)
            {
                HiDealOrderDetail hod = (HiDealOrderDetail)e.Item.DataItem;

                Label lblCatgName1 = (Label)e.Item.FindControl("lblCatgName1");
                Label lblOptionName1 = (Label)e.Item.FindControl("lblOptionName1");
                Label lblCatgName2 = (Label)e.Item.FindControl("lblCatgName2");
                Label lblOptionName2 = (Label)e.Item.FindControl("lblOptionName2");
                Label lblCatgName3 = (Label)e.Item.FindControl("lblCatgName3");
                Label lblOptionName3 = (Label)e.Item.FindControl("lblOptionName3");
                Label lblCatgName4 = (Label)e.Item.FindControl("lblCatgName4");
                Label lblOptionName4 = (Label)e.Item.FindControl("lblOptionName4");
                Label lblCatgName5 = (Label)e.Item.FindControl("lblCatgName5");
                Label lblOptionName5 = (Label)e.Item.FindControl("lblOptionName5");

                if (hod.CatgName1 != null)
                {
                    lblCatgName1.Text = hod.CatgName1;
                    lblOptionName1.Text = hod.OptionName1;
                }
                else
                    lblCatgName1.Parent.Parent.Visible = false;
                if (hod.CatgName2 != null)
                {
                    lblCatgName2.Text = hod.CatgName2;
                    lblOptionName2.Text = hod.OptionName2;
                }
                else
                    lblCatgName2.Parent.Parent.Visible = false;
                if (hod.CatgName3 != null)
                {
                    lblCatgName3.Text = hod.CatgName3;
                    lblOptionName3.Text = hod.OptionName3;
                }
                else
                    lblCatgName3.Parent.Parent.Visible = false;
                if (hod.CatgName4 != null)
                {
                    lblCatgName4.Text = hod.CatgName4;
                    lblOptionName4.Text = hod.OptionName4;
                }
                else
                    lblCatgName4.Parent.Parent.Visible = false;
                if (hod.CatgName5 != null)
                {
                    lblCatgName5.Text = hod.CatgName5;
                    lblOptionName5.Text = hod.OptionName5;
                }
                else
                    lblCatgName5.Parent.Parent.Visible = false;
            }
        }

        protected void repRefund_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<HiDealReturned, IList<string>>)
            {
                KeyValuePair<HiDealReturned, IList<string>> item = (KeyValuePair<HiDealReturned, IList<string>>)e.Item.DataItem;

                Label lblReturnedCreateTime = (Label)e.Item.FindControl("lblReturnedCreateTime");
                Label lblSequenceNumber = (Label)e.Item.FindControl("lblSequenceNumber");
                Label lblPcash = (Label)e.Item.FindControl("lblPcash");
                Label lblScash = (Label)e.Item.FindControl("lblScash");
                Label lblBcash = (Label)e.Item.FindControl("lblBcash");
                Label lblCash = (Label)e.Item.FindControl("lblCash");
                Label lblModifyTime = (Label)e.Item.FindControl("lblModifyTime");
                Label lblStatus = (Label)e.Item.FindControl("lblStatus");
                //HyperLink hkReturnedDownload = (HyperLink)e.Item.FindControl("hkReturnedDownload");


                switch ((HiDealReturnedStatus)item.Key.Status)
                {
                    case HiDealReturnedStatus.Create:
                        lblReturnedCreateTime.Text = item.Key.CreateTime.ToString("yyyy/MM/dd");
                        //hkReturnedDownload.Visible = true;
                        //string pageId = HiDealReturnedManager.GetRefundFormPageId(item.Key.Id, this.OrderPk, this.UserName);
                        //hkReturnedDownload.NavigateUrl = ResolveClientUrl("~/service/hidealrefundform.ashx") + "?id=" + pageId;

                        lblStatus.Text = "退貨處理中";
                        break;
                    case HiDealReturnedStatus.Completed:
                        lblReturnedCreateTime.Text = item.Key.CreateTime.ToString("yyyy/MM/dd");
                        lblSequenceNumber.Text = string.Join("<br />", item.Value);
                        lblPcash.Text = "PayEasy購物金:" + item.Key.ReturnPcash.ToString("N0") ?? "0";
                        lblScash.Text = "購物金:" + item.Key.ReturnScash.ToString("N0") ?? "0";
                        lblBcash.Text = "紅利金:" + item.Key.ReturnBcash.ToString("N0") ?? "0";
                        lblCash.Text = "刷退或轉帳退款:" + (item.Key.ReturnCreditCardAmt + item.Key.ReturnAtmAmount).ToString("N0") ?? "0";
                        lblModifyTime.Text = item.Key.ModifyTime.Value.ToString("yyyy/MM/dd");
                        lblStatus.Text = "退貨完成";
                        break;
                    case HiDealReturnedStatus.Cancel:
                        lblStatus.Text = "退貨申請已取消";
                        break;
                    default:
                        break;
                }
            }
        }


        protected void RequestPaperInvoice(object sender, EventArgs e)
        {
            var eventArgs = new DataEventArgs<MultipleEinvoiceRequestQuery>(
                new MultipleEinvoiceRequestQuery(
                    this.OrderGuid, hidEinvoiceRequestReceiver.Value,
                    hidEinvoiceRequestAddress.Value)
                );
            if (RequestEinvoice != null)
            {
                this.RequestEinvoice(this, eventArgs);
            }
        }

        protected void SendCouponSms(object sender, CommandEventArgs e)
        {
            if (SendSMS != null)
                this.SendSMS(this, new DataEventArgs<HiDealSMSQuery>(new HiDealSMSQuery(hidCouponId.Value, Mobile)));
        }
        #endregion

        #region private method
        private Dictionary<HiDealReturned, IList<string>> GetReturnedList(HiDealReturnedCollection hdrc, CashTrustLogCollection ctlc)
        {
            Dictionary<HiDealReturned, IList<string>> list = new Dictionary<HiDealReturned, IList<string>>();
            foreach (HiDealReturned item in hdrc)
            {
                IList<string> sequence = new List<string>();
                if (ctlc.Count > 0)
                    sequence = ctlc.Where(x => x.Status.Equals((int)TrustStatus.Refunded) || x.Status.Equals((int)TrustStatus.Returned)).Select(x => x.CouponSequenceNumber).ToArray();

                list.Add(item, sequence);
            }
            return list;
        }

        #region 清除資料
        /// <summary>
        /// 清除資料
        /// </summary>
        /// <param name="controls"></param>
        protected void ClearDetailText(params object[] controls)
        {
            foreach (object item in controls)
            {
                if (item is HiddenField)
                    ((HiddenField)item).Value = string.Empty;
                else if (item is Label)
                    ((Label)item).Text = string.Empty;
            }
        }

        /// <summary>
        /// 清除資料並設定default值
        /// </summary>
        /// <param name="default_string"></param>
        /// <param name="controls"></param>
        protected void ClearDetailText(string default_string, params object[] controls)
        {
            foreach (object item in controls)
            {
                if (item is HiddenField)
                    ((HiddenField)item).Value = default_string;
                else if (item is Label)
                    ((Label)item).Text = default_string;
            }
        }
        #endregion

        #region 索取憑證控制顯示
        /// <summary>
        /// 索取憑證控制顯示
        /// </summary>
        private void VisibleDisableControl(bool print, bool download, bool sms, string couponid,
            Dictionary<string, LinkButton> lkList, Dictionary<string, HyperLink> hypList, bool email, HyperLink hyp_email, HyperLink hyp_emailtext)
        {
            VisibleDisableControlDownLoad(download, hypList[CouponGetType.Download.ToString()], hypList[CouponGetType.DownloadText.ToString()], couponid);
            VisibleDisableControlPrint(print, hypList[CouponGetType.Print.ToString()],
                                       hypList[CouponGetType.PrintText.ToString()], couponid);

            VisibleDisableControlEmail(email, hyp_email, hyp_emailtext, couponid);
            //是否關閉簡訊發送按鈕 :是 true(1)   否 false(0)
            if (HidealSmsVisible)
            {
                VisibleDisableControlSMS(false, lkList[CouponGetType.Sms.ToString()], lkList[CouponGetType.SmsText.ToString()]);
            }
            else
            {
                VisibleDisableControlSMS(sms, lkList[CouponGetType.Sms.ToString()], lkList[CouponGetType.SmsText.ToString()]);
            }
        }

        private void VisibleDisableControlEmail(bool email, HyperLink hyp_email, HyperLink hyp_emailtext, string couponId)
        {
            if (email)
            {
                hyp_email.CssClass = "EmailIcon MailCoupon";
                hyp_email.Enabled = hyp_emailtext.Enabled = true;
                hyp_email.Attributes["couponId"] = couponId;
                hyp_emailtext.CssClass = "MailCoupon";
                hyp_emailtext.ForeColor = System.Drawing.Color.FromName("#FC7D49");
                hyp_emailtext.Attributes["couponId"] = couponId;
            }
            else
            {
                hyp_email.Attributes.Remove("couponId");
                hyp_email.CssClass = "Non-EmailIcon";
                hyp_email.Enabled = hyp_emailtext.Enabled = false;
                hyp_emailtext.Attributes.Remove("couponId");
                hyp_emailtext.ForeColor = System.Drawing.Color.Gray;
            }
        }

        /// <summary>
        /// 索取憑證-列印 控制顯示
        /// </summary>
        /// <param name="print">是否可列印憑證</param>
        /// <param name="hyp_print">列印憑證(圖)</param>
        /// <param name="hyp_printtext">列印憑證</param>
        /// <param name="couponid">CouponId</param>
        private void VisibleDisableControlPrint(bool print, HyperLink hyp_print, HyperLink hyp_printtext, string couponid)
        {
            if (print)
            {
                string linkUrl = ResolveUrl("~/piinlife/member/CouponPrint.aspx") + "?cid=" + (couponid);
                hyp_print.NavigateUrl = linkUrl;
                hyp_printtext.NavigateUrl = linkUrl;
            }
            hyp_print.CssClass = print ? "PrinterIcon" : "Non-PrinterIcon";
            hyp_print.Enabled = hyp_printtext.Enabled = print;
            hyp_printtext.ForeColor = print ? System.Drawing.Color.FromName("#FC7D49") : System.Drawing.Color.Gray;
        }


        /// <summary>
        /// 索取憑證-下載 控制顯示
        /// </summary>
        /// <param name="download">是否可下載憑證</param>
        /// <param name="hyp_download">下載憑證(圖)</param>
        /// <param name="hyp_downloadtext">下載憑證</param>
        /// <param name="couponid">CouponId</param>
        private void VisibleDisableControlDownLoad(bool download, HyperLink hyp_download, HyperLink hyp_downloadtext, string couponid)
        {
            if (download)
            {
                string linkUrl = WebUtility.GetSiteRoot() + "/service/HidealCouponDownload.ashx?cid=" + (couponid);
                hyp_download.NavigateUrl = linkUrl;
                hyp_downloadtext.NavigateUrl = linkUrl;
            }
            hyp_download.CssClass = download ? "DownloadIcon" : "Non-DownloadIcon";
            hyp_download.Enabled = hyp_downloadtext.Enabled = download;
            hyp_downloadtext.ForeColor = download ? System.Drawing.Color.FromName("#FC7D49") : System.Drawing.Color.Gray;
        }
        /// <summary>
        /// 索取憑證-簡訊 控制顯示
        /// </summary>
        /// <param name="sms">是否可簡訊憑證</param>
        /// <param name="lbt_sms">簡訊憑證(圖)</param>
        /// <param name="lbt_smstext">簡訊憑證</param>
        private void VisibleDisableControlSMS(bool sms, LinkButton lbt_sms, LinkButton lbt_smstext)
        {
            lbt_sms.CssClass = sms ? "SMSIcon" : "Non-SMSIcon";
            lbt_sms.Enabled = lbt_smstext.Enabled = sms;
            lbt_smstext.ForeColor = sms ? System.Drawing.Color.FromName("#FC7D49") : System.Drawing.Color.Gray;
        }
        #endregion

        #region 憑證狀態
        /// <summary>
        /// 已退貨
        /// </summary>
        /// <param name="coupon"></param>
        private void CouponReturned(ViewHiDealCouponListSequence coupon, Label clab_rpUseCount)
        {
            if (coupon.CtlStatus.Equals((int)TrustStatus.Refunded) || coupon.CtlStatus.Equals((int)TrustStatus.Returned))
                clab_rpUseCount.Text = "已退貨";
        }
        /// <summary>
        /// 已結檔or未結檔
        /// </summary>
        /// <param name="coupon"></param>
        private void BusinessHourCloseOrNonClose(ViewHiDealCouponListSequence coupon, Label clab_rpUseCount, Dictionary<string, LinkButton> lkList, Dictionary<string, HyperLink> hypList,
            HiddenField chif_rpCouponId, HyperLink hyp_email, HyperLink hyp_emailtext)
        {
            if (coupon.UseEndTime.Value.AddDays(1) < DateTimeNow)
            {
                clab_rpUseCount.Text = "憑證過期";
                VisibleDisableControl(false, false, false, chif_rpCouponId.Value, lkList, hypList, false, hyp_email, hyp_emailtext);
            }
            else
            {
                clab_rpUseCount.Text = "未使用";
                VisibleDisableControl(true, true, true, chif_rpCouponId.Value, lkList, hypList, true, hyp_email, hyp_emailtext);
            }
        }

        #endregion
        #endregion

        [WebMethod]
        public static bool SendCouponMail(string emailTo, int couponId)
        {
            //check login
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                HttpContext.Current.Response.StatusCode = 500;
                HttpContext.Current.Response.StatusDescription = "userNotLogin";
                return false;
            }
            string userName = HttpContext.Current.User.Identity.Name;
            try
            {
                HiDealMailFacade.SendCouponToMember(emailTo, userName, couponId);
                return true;
            }
            catch (Exception ex)
            {
                logger.Warn(
                    string.Format("品生活寄送憑證失敗. emailTo={0}, couponId={1}, userName={2}", emailTo, couponId, userName),
                    ex);
                return false;
            }
        }


    }

}

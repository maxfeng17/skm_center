﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RefundForm.aspx.cs" Inherits="LunchKingSite.Web.piinlife.member.RefundForm"
    Title="HighDeal退貨申請書" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.Core.Component" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>品生活 退貨申請書</title>
    <%--    <link href="../../Themes/HighDeal/images/Returns/ReturnApplication.css" rel="stylesheet" type="text/css" />--%>
    <link href="../../Themes/default/images/17Life/G7/ReturnApplication.css" rel="stylesheet" type="text/css" />
<style type="text/css">
 .print {
     page-break-after: always;
 }
 .printlast {
     page-break-after: auto;
}

</style>    
</head>
    
<body>
    <form id="form1" runat="server">
        <asp:PlaceHolder runat="server" ID="phReturnApplicationFrom">
            <div class="ReturnApplicationForm">

                <div class="ReAppFoTop">
                    <%--<img src="../../Themes/HighDeal/images/Returns/PL_ReApFormTop2.png" width="740" height="45" />--%>
                    <asp:Image ID="PL_ReApFormTop2" runat="server"  width="740" height="45" />
                    <div class="ReAppNumber">訂單編號：<%=OrderId %> </div>
                    <div class="ReAppBarcode">
                        -----------
                        <%--<img src="../../Themes/default/images/17Life/G7/ReApBarcode.jpg" width="166" height="44" />--%>
                        <asp:Image ID="iVC_RAF" runat="server" Width="166" Height="44" />
                        -----------
                    </div>
                </div>
                <div class="ReturnForm">
                    <table width="725" border="0" cellspacing="0" cellpadding="0" class="ReturnFormTable">
                        <tr>
                            <td width="199">退貨申請日期</td>
                            <td width="489"><%=ReturnedCreateTime.ToTaiwanLongDateString() %></td>
                        </tr>
                        <tr>
                            <td>品生活檔名</td>
                            <td>檔號 - <%=HiDealId +" " + HiDealProductName %></td>
                        </tr>
                        <tr>
                            <td>憑證編號<br />
                                (商品則無需填寫)</td>
                            <td>&nbsp;
                                <asp:Repeater ID="rpCouponSequenceNumber" runat="server" Visible="False">
                                    <ItemTemplate>
                                        <p><%# ((HiDealRefundFormListClass)(Container.DataItem)).CouponSequence%></p>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        <tr>
                            <td>公司抬頭(或原買受人)</td>
                            <td align="right" style="padding-right: 30px; font-size: 14px;">(請簽章)</td>
                        </tr>
                        <tr>
                            <td>營利事業統一編號<br />
                                (或個人身分證字號)</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    <table width="725" border="0" cellspacing="0" cellpadding="0" class="ReturnFormTable">
                        <tr>
                            <td colspan="2">受款退貨金額金融帳戶資料</td>
                        </tr>
                        <tr>
                            <td width="360" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;戶名：</td>
                            <td width="365" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;身分證字號或統編：</td>
                        </tr>
                        <tr>
                            <td width="360" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;銀行名稱：</td>
                            <td width="365" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;分行別：</td>
                        </tr>
                        <tr>
                            <td width="360" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帳號：</td>
                            <td width="365" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;連絡電話：</td>
                        </tr>
                        <tr>
                            <td colspan="2">●請勿直接修改受款內容以免造成退款失敗，如有問題請與品生活客服聯繫</td>
                        </tr>
                    </table>
                    <div class="ReturnFormRedText">
                        ※若您訂單已開立發票，請連同退回或折讓證明單填寫完整（若為您訂單申請三聯式發票請填寫「營業事業統一編號」並於「公司抬頭」蓋上公司發票章），以郵局掛號或傳真方式寄回品生活退貨處理中心；如於訂購時已索取紙本發票，請將紙本發票一併郵寄掛號寄回，否則恕無法辦理退費。
                    </div>
                    <div class="ReturnFormAddress">
                        品生活退貨處理中心寄件地址：  104 台北市中山區中山北路一段11號13樓    品生活退貨處理中心收<br />
                        品生活退貨處理中心傳真號碼：  (02)2511-9110
                    </div>
                    <div class="ReturnFormRedText">
                        （為確保您的退貨權益，請您於傳真完成後主動告知您傳真的日期、時間、訂單編號等資訊，我們將盡速為您確認是否已收到您的文件。）
                    </div>
                </div>

                <div class="ReAppFoInfo">

                    <div class="ReAppFpOffLine">
                        <div class="ReAppFoContent1">
                            <div class="ReAppFoTittle">
                                <span style="font-size: 15px;">※</span> 退貨須知<span style="font-size: 15px;">※</span>
                            </div>
                            <div class="ReAppFoSecondTi"><span style="font-family: Arial, Helvetica, sans-serif;">1.</span> 申請退貨方式：</div>
                            <div class="ReAppFo">

                                <span class="ReAppFoText">《憑證退貨相關規定》</span>
                                <ul>
                                    <li><strong>(1)</strong> 未使用之優惠憑證，皆可提出退貨申請，作業手續費$0；</li>
                                    <li><strong>(2)</strong> 部分憑證所指稱之內容為依據特定單一時間與特定場合所提供之服務（包括但不限於演唱會、音樂會或展覽），當憑證持有人因故未能於該特定時間與特定場合兌換該服務，恕無法要求退費或另行補足差額重現該服務。</li>
                                </ul>
                                <span class="ReAppFoText">《宅配商品退換貨相關規定》</span>
                                <ul>
                                    <li>商品在收貨7天鑑賞期內可申請退貨；生鮮食品類需在收貨後24小時內申請退貨。<br />
                                        特殊商品退貨規則需依權益說明為主。</li>
                                    <li><strong>(1)</strong> 如欲退貨，商品必須為全新狀態且包裝完整（包含商品、內外包裝及所有隨付配件等）。</li>
                                    <li><strong>(2)</strong> 如商品含附送之贈品，請務必隨商品一併退回。</li>
                                    <li><strong>(3)</strong> 如您收到商品時有損壞，請不要拆封，請來信或來電告訴我們您要辦理換貨，以保障您換貨權益。</li>
                                </ul>
                            </div>
                            <div class="ReAppFoSecondTi"><span style="font-family: Arial, Helvetica, sans-serif;">2.</span> 退款方式及時間：一般退貨處理時間為10-20個工作天</div>
                            <table border="0" cellspacing="0" cellpadding="0" class="ReAppFoTwoTable">
                                <tr bgcolor="#888686">
                                    <td width="119" height="25" align="center" style="color: #FFF; font-weight: bold; font-size: 15px;">付款方式</td>
                                    <td width="119" align="center" style="color: #FFF; font-weight: bold; font-size: 15px;">退款方式</td>
                                    <td width="430" align="center" style="color: #FFF; font-weight: bold; font-size: 15px;">退款工作日</td>
                                </tr>
                                <tr>
                                    <td align="center">刷卡</td>
                                    <td align="center">退刷</td>
                                    <td align="left" style="padding-left: 8px;">收到退款申請書後，以一般退貨處理時間再額外等待30-60個工作天退至您刷卡帳戶。<br />
                                        ※由於各家銀行的作業時間不一，申請到退款完成可能需等待30-60個工作天。</td>
                                </tr>
                                <%--      <tr>
        <td align="center">ATM轉帳</td>
        <td align="center">退款至指定帳戶</td>
        <td align="left" style="padding-left:8px;"> 收到退款申請書後，以一般退貨處理時間再額外等待25-30個工作日退款至您的指定帳戶 。</td>
      </tr>--%>
                                <tr>
                                    <td align="center">17Life購物金</td>
                                    <td align="center">購物金退款</td>
                                    <td align="left" style="padding-left: 8px;">為一般退貨處理時間為 10-20 個工作天 。</td>
                                </tr>
                                <tr>
                                    <td align="center">PayEasy購物金</td>
                                    <td align="center">購物金退款</td>
                                    <td align="left" style="padding-left: 8px;">為一般退貨處理時間為 10-20 個工作天 。</td>
                                </tr>
                                <tr>
                                    <td align="center">17Life 紅利金</td>
                                    <td align="center">紅利金退款</td>
                                    <td align="left" style="padding-left: 8px;">為一般退貨處理時間為 10-20 個工作天 。</td>
                                </tr>
                                <tr>
                                    <td align="center">17Life折價券</td>
                                    <td align="center">不退款</td>
                                    <td align="left" style="padding-left: 8px;">使用<span lang="EN-US" xml:lang="EN-US">17Life</span>折價券折抵，無論是否退貨皆不得返還 。</td>
                                </tr>
                            </table>
                            <span style="font-size: 15px; padding-left: 4px;">※收到您的退貨申請書並確認資料無誤後，品生活退貨處理中心將會依各別退款方式處理。</span>

                            <!--<table width="640" border="0" cellspacing="0" cellpadding="0" class="ReAppFoTwoTable2">
  <tr>
    <td height="28">若退貨商品發票為紙本發票，請將發票連同退貨申請書簽章一併寄回，否則恕無法辦理退費。</td>
  </tr>
  <tr>
    <td bgcolor="#646464" class="SignatureTeMark">請於下方簽名欄位簽章並填妥資料後，寄至17Life</td>
  </tr>
</table>-->

                        </div>

                        <!--<div class="ReAppFoContent2">
      <div class="ReAppFoTittle"><span style="font-size:17px;">●</span> 受款退貨金額金融帳戶資料 
        <span style="font-size:14px; margin-left:4px; font-weight:normal;">
        請勿直接修改受款內容以免造成退款失敗，如有問題請與17Life客服聯繫</span>
      </div>
      <ul>
        <li>戶名：</li>
        <li>身分證號碼或統編：</li>
        <li>銀行名稱：<span style="margin-left:160px;">分行別：</span></li>
        <li>帳號：</li>
        <li>連絡電話：</li>
      </ul>
    </div>-->

                    </div>

                    <table border="0" cellspacing="0" cellpadding="0" class="ReAppFoFooterTable">
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr>
                            <td>客服專線：<%=config.ServiceTel %> | 服務時間：平日 9:00~18:00</td>
                        </tr>
                        <tr>
                            <td>康太數位整合股份有限公司   版權所有 轉載必究 17Life 17life.com <script language="javascript"> var Today = new Date(); document.write(Today.getFullYear()); </script></td>
                        </tr>
                        <tr>
                            <td height="5"></td>
                        </tr>
                    </table>
                </div>

            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phPageBreak" runat="server" Visible="False">
           <div style="page-break-after: always;">
                        &nbsp;
           </div>
           <br />
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="phDiscountSingleForm">
            <asp:Repeater ID="rpt_Form" runat="server" OnItemDataBound="rpt_Form_ItemDataBound" >
                <ItemTemplate>
                    <div>&nbsp;</div>
                    <br />
                    <div class="ReturnApplicationForm">
                        <div class="ReAppFoInfo">
                            <div class="ReAppFoContent3">
                                <!--Invoice--1---ST----->
                                <div class="ReAppFoInvoice">
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" class="ReAppFoCont3Table1_3">
                                        <tr>
                                            <td align="center" height="13"></td>
                                        </tr>
                                        <tr>
                                            <td align="center" height="29" style="font-size: 21px;"><asp:Label ID="RefundTitle" runat="server" Text="憑證編號"></asp:Label><span style="font-weight: bold; font-family: Arial, Helvetica, sans-serif;"><%# ((HiDealRefundFormListClass)(Container.DataItem)).CouponSequence%> </span>退回或折讓證明單</td>
                                        </tr>
                                        <tr>
                                            <td align="center" height="23" style="font-size: 17px;">(<%=HiDealId%>)訂單編號：<%# ((HiDealRefundFormListClass)(Container.DataItem)).OrderId%></td>
                                        </tr>
                                        <tr>
                                            <td align="center">-----------
                                                <%--<img src="../../Themes/default/images/17Life/G7/ReApBarcode.jpg" width="166" height="44" />--%>
                                                <asp:Image ID="iVC_DSF" runat="server" Width="166" Height="44" />
                                                -----------
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" height="7"></td>
                                        </tr>
                                    </table>

                                    <table border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table">
                                        <tr>
                                            <td width="369" rowspan="3">
                                                <table width="359" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_1">
                                                    <tr>
                                                        <td width="78" align="right" style="padding-right: 6px;"><span style="letter-spacing: 3.2em;">名</span>稱</td>
                                                        <td width="288" align="left" style="padding-left: 8px;">康太數位整合股份有限公司</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="padding-right: 6px;"><span style="letter-spacing: 0.4em;">統一編</span>號</td>
                                                        <td align="left" style="padding-left: 8px;">
                                                            <%# ProviderFactory.Instance().GetConfig().ContactCompanyId%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="padding-right: 6px;"><span style="letter-spacing: 3.2em;">地</span>址</td>
                                                        <td align="left" style="padding-left: 8px;">台北市中山區中山北路一段11號12號</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="354" height="28" align="center" style="font-size: 18px;"><strong>營業人銷貨退回或折讓證明單</strong></td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="line-height: 23px; font-size: 15px;">
                                                <%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceNumberDate_Year %> &nbsp;&nbsp;  年 &nbsp;&nbsp;
                                        <%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceNumberDate_Month %> &nbsp;&nbsp; 月 &nbsp;&nbsp;
                                        <%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceNumberDate_Day%> &nbsp;&nbsp; 日
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center" height="18" style="line-height: 18px; color: #506A00;">第ㄧ聯：交付原銷貨人作為銷項稅額之扣減憑證  </td>
                                        </tr>
                                        <tr>
                                            <td height="10"></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_2">
                                                    <tr>
                                                        <td colspan="6" height="20">開立發票</td>
                                                        <td colspan="5">退貨或折讓內容</td>
                                                        <td colspan="3" rowspan="2">(打V處)</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="3%" rowspan="2">聯式</td>
                                                        <td width="6%" rowspan="2">年</td>
                                                        <td width="3%" rowspan="2">月</td>
                                                        <td width="3%" rowspan="2">日</td>
                                                        <td width="4%" rowspan="2">字<br />
                                                            軌</td>
                                                        <td width="11%" rowspan="2">號碼</td>
                                                        <td width="18%" rowspan="2">品名</td>
                                                        <td width="5%" rowspan="2">數量</td>
                                                        <td width="6%" rowspan="2">單價</td>
                                                        <td colspan="2">退出或折讓</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="19%">金額(不含稅之進貨額)</td>
                                                        <td width="9%">營業稅額</td>
                                                        <td width="4%">應稅</td>
                                                        <td width="6%">零稅率</td>
                                                        <td width="3%" style="line-height: 16px;">免稅</td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20"><%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceMode%></td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceDate.Year%></td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceDate.Month%></td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceDate.Day%></td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceNumber.Substring(0, 2)%></td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceNumber.Substring(2, 8)%></td>
                                                        <td style="text-align: left; padding-left: 4px;"><%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceItemName%></td>
                                                        <td>1</td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).ItemAmount%></td>
                                                        <td>$<%# ((HiDealRefundFormListClass)(Container.DataItem)).ItemNoTaxAmount%></td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).ItemTax%></td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).IsTax ? "v" : string.Empty%></td>
                                                        <td>&nbsp;</td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).IsTax ? string.Empty : "v"%></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="9" height="20">合計</td>
                                                        <td>$<%# ((HiDealRefundFormListClass)(Container.DataItem)).ItemAmount%></td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <span class="ReAppFoText1">本證明單所列銷貨退回進貨退出或折讓，確屬事實，特此證明。</span>

                                    <div class="ReAppFoSitBlockOut">
                                        <table height="80" border="0" cellpadding="0" cellspacing="0" class="ReAppFoSitBlockTable">
                                            <tr>
                                                <td width="167" align="right">公司抬頭(或原買受人)：</td>
                                                <td width="170">&nbsp;</td>
                                                <td width="368">營利事業統一編號（此欄二聯式發票免填）：</td>
                                            </tr>
                                            <tr>
                                                <td height="15"></td>
                                                <td align="center"><span style="color: #666;">(請簽章)</span></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                                <!--Invoice--1--END-->


                                <hr style="width: 700px; margin: 15px auto;" />


                                <!--Invoice--2--ST--->
                                <div class="ReAppFoInvoice">
                                    <table border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table">
                                        <tr>
                                            <td width="379" rowspan="3">
                                                <table width="359" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_1">
                                                    <tr>
                                                        <td width="78" align="right" style="padding-right: 6px;"><span style="letter-spacing: 3.2em;">名</span>稱</td>
                                                        <td width="288" align="left" style="padding-left: 8px;">康太數位整合股份有限公司</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="padding-right: 6px;"><span style="letter-spacing: 0.4em;">統一編</span>號</td>
                                                        <td align="left" style="padding-left: 8px;">
                                                            <%# ProviderFactory.Instance().GetConfig().ContactCompanyId%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="padding-right: 6px;"><span style="letter-spacing: 3.2em;">地</span>址</td>
                                                        <td align="left" style="padding-left: 8px;">台北市中山區中山北路一段11號12號</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="344" height="28" align="center" style="font-size: 18px;"><strong>營業人銷貨退回或折讓證明單</strong></td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="line-height: 23px; font-size: 15px;">
                                                <%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceNumberDate_Year %> &nbsp;&nbsp;  年 &nbsp;&nbsp;
                                                <%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceNumberDate_Month %> &nbsp;&nbsp; 月 &nbsp;&nbsp;
                                                <%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceNumberDate_Day%> &nbsp;&nbsp; 日
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center" height="18" style="line-height: 18px; color: #506A00;">第二聯：交付原銷貨人作為記帳憑證</td>
                                        </tr>
                                        <tr>
                                            <td height="10"></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_2">
                                                    <tr>
                                                        <td colspan="6" height="20">開立發票</td>
                                                        <td colspan="5">退貨或折讓內容</td>
                                                        <td colspan="3" rowspan="2">(打V處)</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="3%" rowspan="2">聯式</td>
                                                        <td width="6%" rowspan="2">年</td>
                                                        <td width="3%" rowspan="2">月</td>
                                                        <td width="3%" rowspan="2">日</td>
                                                        <td width="4%" rowspan="2">字<br />
                                                            軌</td>
                                                        <td width="11%" rowspan="2">號碼</td>
                                                        <td width="18%" rowspan="2">品名</td>
                                                        <td width="5%" rowspan="2">數量</td>
                                                        <td width="6%" rowspan="2">單價</td>
                                                        <td colspan="2">退出或折讓</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="19%">金額(不含稅之進貨額)</td>
                                                        <td width="9%">營業稅額</td>
                                                        <td width="4%">應稅</td>
                                                        <td width="6%">零稅率</td>
                                                        <td width="3%" style="line-height: 16px;">免稅</td>
                                                    </tr>
                                                    <tr>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceMode%></td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceDate.Year%></td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceDate.Month%></td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceDate.Day%></td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceNumber.Substring(0, 2)%></td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceNumber.Substring(2, 8)%></td>
                                                        <td style="text-align: left; padding-left: 4px;">
                                                            <%# ((HiDealRefundFormListClass)(Container.DataItem)).EinvoiceItemName%>
                                                        </td>
                                                        <td>1</td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).ItemAmount%></td>
                                                        <td>$<%# ((HiDealRefundFormListClass)(Container.DataItem)).ItemNoTaxAmount%></td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).ItemTax%></td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).IsTax ? "v" : string.Empty%></td>
                                                        <td>&nbsp;</td>
                                                        <td><%# ((HiDealRefundFormListClass)(Container.DataItem)).IsTax ? string.Empty : "v"%></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="9">合計</td>
                                                        <td>$<%# ((HiDealRefundFormListClass)(Container.DataItem)).ItemAmount%></td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <span class="ReAppFoText1">本證明單所列銷貨退回進貨退出或折讓，確屬事實，特此證明。</span>

                                    <div class="ReAppFoSitBlockOut">
                                        <table height="80" border="0" cellpadding="0" cellspacing="0" class="ReAppFoSitBlockTable">
                                            <tr>
                                                <td width="167" align="right">公司抬頭(或原買受人)：</td>
                                                <td width="170">&nbsp;</td>
                                                <td width="368">營利事業統一編號（此欄二聯式發票免填）：</td>
                                            </tr>
                                            <tr>
                                                <td height="15"></td>
                                                <td align="center"><span style="color: #666;">(請簽章)</span></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                                <!--Invoice--2-END---->

                                <div class="ReAppFoFooterTeMark">
                                    請您將此份<span style="font-weight: bold;">退回或折讓證明單</span>列印出來，如開立為三聯式發票，請務必填寫「營利事業統一編號」並蓋上公司發票章，以郵局掛號或傳真方式寄回品生活退貨處理中心。
                                </div>
                                <div class="ReturnFormAddress">
                                    品生活退貨處理中心寄件地址：  104 台北市中山區中山北路一段11號13樓    品生活退貨處理中心收<br />
                                    品生活退貨處理中心傳真號碼：  (02)2511-9110
                                </div>
                                <div class="ReturnFormRedText">
                                    （為確保您的退貨權益，請您於傳真完成後主動告知您傳真的日期、時間、訂單編號等資訊，我們將盡速為您確認是否已收到您的文件。）
                                </div>
                            </div>



                            <table border="0" cellspacing="0" cellpadding="0" class="ReAppFoFooterTable">
                                <tr>
                                    <td height="10"></td>
                                </tr>
                                <tr>
                                    <td>客服專線： <%=config.ServiceTel %>  | 服務時間：平日 9:00~18:00</td>
                                </tr>
                                <tr>
                                    <td>康太數位整合股份有限公司   版權所有 轉載必究 17Life 17life.com <script language="javascript"> var Today = new Date(); document.write(Today.getFullYear()); </script></td>
                                </tr>
                                <tr>
                                    <td height="5"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="<%# Container.ItemIndex == ((IList)((Repeater)Container.Parent).DataSource).Count-1  ? "printlast" : "print" %>" >
                        &nbsp;
                    </div>
                    <br />
                </ItemTemplate>
            </asp:Repeater>
        </asp:PlaceHolder>
    </form>
</body>
</html>

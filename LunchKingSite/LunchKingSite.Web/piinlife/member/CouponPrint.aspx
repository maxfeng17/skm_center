﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CouponPrint.aspx.cs" Inherits="LunchKingSite.Web.piinlife.member.CouponPrint"
    Title="憑證" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../Themes/HighDeal/images/Certificate/Certificate.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="//www.google.com/jsapi?key=<%=GoogleApiKey%>"></script>
    
    <%if(ActionMode!="preview"){
         Response.Write("<script type='text/javascript'>google.setOnLoadCallback(function () {var timeout = 2000;setTimeout('window.print()', timeout);});</script>");
    }%>
    
</head>
<body>
    <form id="form1" runat="server">
    <div class="Certificate">
        <table width="750" border="0" cellspacing="0" cellpadding="0" class="CerFrameTable">
            <tr>
                <td valign="middle">
                    <div class="Cer_HighDeal_logo">
                        <div class="Number">
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="Cer_top-column">
                        <!---Cer_top-column--->
                        <table width="725" border="0" cellspacing="0" cellpadding="0" class="CerTable1">
                            <!----標題table---->
                            <tr>
                                <td valign="top" align="left">
                                    <div class="CerTittle">
                                        <asp:HiddenField ID="hidSellerName" runat="server" />
                                        <asp:Label ID="lblDealName" runat="server"></asp:Label>
                                    </div>
                                    <div class="Subtitle">
                                        <asp:Label ID="lblProductName" runat="server"></asp:Label>
                                    </div>
                                    <div class="Deadline">
                                        優惠期限：
                                        <asp:Literal ID="liUseStartTime" runat="server" />
                                        &nbsp;~&nbsp;
                                        <asp:Literal ID="liUseEndTime" runat="server" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <!----標題table---->
                        <div class="Note">
                            使用注意事項：</div>
                        <ul class="NoteInfoText">
                            <asp:Label ID="lblRegularProvision" runat="server" Text="●本憑證不限本人使用，使用時店家將會核對憑證編號，一組編號限用一次。<br />"
                                Font-Size="16px"></asp:Label>
                            <asp:Literal ID="liContent" runat="server" />
                        </ul>
                    </div>
                    <!---Cer_top-column--->
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <div class="Cer_StInfo-AD">
                        <!---Cer_StInfo-AD--->
                        <div class="Cer_HighDeal_bottom-text">
                            <!---Cer_HighDeal_bottom-text--->
                            <p>
                                位置資訊：</p>
                            <asp:Repeater ID="repStore" runat="server">
                                <ItemTemplate>
                                    <ul class="StoreInformation">
                                        <li id="liSellerStoreName" runat="server">
                                            <%# hidSellerName.Value + ((ViewHiDealStoresInfo)(Container.DataItem)).StoreName%></li>
                                        <li id="liAddress" runat="server">地址：<%# GetAddressString(((ViewHiDealStoresInfo)(Container.DataItem)).TownshipId ?? 0) + ((ViewHiDealStoresInfo)(Container.DataItem)).AddressString%></li>
                                        <li id="liPhone" runat="server">電話：<%# ((ViewHiDealStoresInfo)(Container.DataItem)).Phone%></li>
                                        <li id="liOpenDate" runat="server">兌換時間：<%# !string.IsNullOrEmpty(((ViewHiDealStoresInfo)(Container.DataItem)).UseTime) ? ((ViewHiDealStoresInfo)(Container.DataItem)).UseTime : ((ViewHiDealStoresInfo)(Container.DataItem)).OpenTime %></li>
                                        <li id="liRemark" runat="server" visible="<%# !string.IsNullOrEmpty(((ViewHiDealStoresInfo)(Container.DataItem)).Remarks) %>">
                                            備註：<%# ((ViewHiDealStoresInfo)(Container.DataItem)).Remarks%></li>
                                    </ul>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <!---Cer_HighDeal_bottom-text--->
                    </div>
                    <!---Cer_StInfo-AD--->
                </td>
            </tr>
            <tr>
                <td>
                    <div class="Cer_info-column">
                        <div class="Cer_info">
                            驗證資訊：&nbsp;&nbsp;&nbsp;( ※ 「預約時，請勿提供確認碼」，以免影響正式使用權利。請確實到店消費時再提供給店家。 )</div>
                        <div class="Cer_number">
                            <p>
                                憑證編號：<asp:Literal ID="lCS" runat="server" /></p>
                            <p>
                                確認碼：<asp:Literal ID="liCouponCode" runat="server" /></p>
                        </div>
                        <div class="Cer_code">
                            <table>
                                <tr>
                                    <td>
                                        <div class="Code_box">
                                            <asp:Image ID="iVC" runat="server" Width="250" Height="80" /></div>
                                    </td>
                                    <td>
                                        <div class="Code_box">
                                            <asp:Image ID="iQC" runat="server" Width="82px" Height="82px" /></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>

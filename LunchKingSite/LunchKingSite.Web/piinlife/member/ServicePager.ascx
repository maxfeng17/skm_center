﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Pager.ascx.cs" Inherits="LunchKingSite.Web.piinlife.member.Pager" %>
<table border="0" cellspacing="0" cellpadding="0" class="Past_records_sub">
    <tr>
        <td>
            共有&nbsp;<asp:Label ID="lblRecentCount" runat="server" />/<asp:Label ID="lrc" runat="server" />&nbsp;記錄
        </td>
        <td style="text-align: right;">
            <asp:LinkButton ID="lPrev" runat="server" CommandArgument="Prev" OnClick="link_Click"> < 較新紀錄
            </asp:LinkButton>
            &nbsp;
            <asp:LinkButton ID="lNext" runat="server" CommandArgument="Next" OnClick="link_Click">較舊紀錄 >
            </asp:LinkButton>
        </td>
    </tr>
</table>
<div style="display: none;">
    <asp:LinkButton ID="lFirst" CssClass="Btn" runat="server" CommandArgument="First"
        OnClick="link_Click">首頁
    </asp:LinkButton>
    <asp:Repeater ID="rpPage" runat="server">
        <ItemTemplate>
            <asp:LinkButton ID="lkPages" CssClass="Btn" runat="server" CommandArgument="<%# Container.DataItem %>"
                OnClick="linkPage_Click" Text="<%# Container.DataItem %>">
            </asp:LinkButton>
        </ItemTemplate>
    </asp:Repeater>
    <asp:LinkButton ID="lLast" CssClass="Btn" runat="server" CommandArgument="Last" OnClick="link_Click">末頁
    </asp:LinkButton>
    <asp:Label ID="lblPageCount" Font-Bold="true" runat="server"></asp:Label>
    <asp:HiddenField ID="hidCurrentPage" runat="server" />
</div>

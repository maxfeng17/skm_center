﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.SqlServer.Types;
using System.Web.Services;
using LunchKingSite.Core.Models;

namespace LunchKingSite.Web.piinlife
{
    public partial class deal : BasePage, IPiinlifeDealView
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region property

        private PiinlifeDealPresenter _presenter;

        public PiinlifeDealPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public Guid BusinessHourGuid
        {
            get
            {
                string bid_querystring = string.Empty;
                if (Page.RouteData.Values["bid"] != null)
                {
                    bid_querystring = Page.RouteData.Values["bid"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["bid"]))
                {
                    bid_querystring = Request.QueryString["bid"];
                }

                Guid bid = Guid.Empty;
                if (!string.IsNullOrWhiteSpace(bid_querystring))
                {
                    if (!Guid.TryParse(bid_querystring, out bid))
                    {
                        bid = Guid.Empty;
                    }
                }
                else if (ViewState["bid"] != null)
                {
                    bid = new Guid(ViewState["bid"].ToString());
                }
                return bid;
            }
        }

        public Guid SubBusinessHourId
        {
            get
            {
                string bid_querystring = string.Empty;
                if (Page.RouteData.Values["sub"] != null)
                {
                    bid_querystring = Page.RouteData.Values["sub"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["sub"]))
                {
                    bid_querystring = Request.QueryString["sub"];
                }

                Guid bid = Guid.Empty;
                if (!string.IsNullOrWhiteSpace(bid_querystring))
                {
                    if (!Guid.TryParse(bid_querystring, out bid))
                    {
                        bid = Guid.Empty;
                    }
                }
                return bid;
            }
        }

        public string MicroDataJson { get; set; }
        public string Rsrc
        {
            get
            {
                if (Page.RouteData.Values["rsrc"] != null)
                {
                    return Page.RouteData.Values["rsrc"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["rsrc"]))
                {
                    return Request.QueryString["rsrc"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string AppTitle { set; get; }

        public int CategoryId
        {
            get
            {
                string cat_querystring = string.Empty;
                int cat;
                if (Page.RouteData.Values["cat"] != null)
                {
                    cat_querystring = Page.RouteData.Values["cat"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["cat"]))
                {
                    cat_querystring = Request.QueryString["cat"];
                }

                if (!string.IsNullOrWhiteSpace(cat_querystring) && int.TryParse(cat_querystring, out cat))
                {
                    return cat;
                }
                else
                {
                    return CategoryManager.GetDefaultCategoryNode().CategoryId;
                }
            }
        }

        public string CategoryName
        {
            get
            {
                CategoryNode channel = CategoryManager.Default.Piinlife;
                if (CategoryId != 0)
                {
                    CategoryNode node = channel.GetSubCategoryNodeByCategoryId(CategoryId);
                    if (node != null)
                    {
                        return node.CategoryName;
                    }
                }
                return CategoryManager.GetDefaultCategoryNode().CategoryName;
            }
        }

        public decimal MinGross
        {
            get
            {
                return config.GrossMargin;
            }
        }

        public bool EnableGrossMarginRestrictions
        {
            get
            {
                return config.EnableGrossMarginRestrictions;
            }
        }

        public decimal GrossProfit
        {
            get
            {
                string bid_querystring = string.Empty;
                if (Page.RouteData.Values["bid"] != null)
                {
                    bid_querystring = Page.RouteData.Values["bid"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["bid"]))
                {
                    bid_querystring = Request.QueryString["bid"];
                }
                Guid bid = Guid.Empty;
                if (!string.IsNullOrWhiteSpace(bid_querystring))
                {
                    if (!Guid.TryParse(bid_querystring, out bid))
                    {
                        bid = Guid.Empty;
                    }
                }
                else if (ViewState["bid"] != null)
                {
                    bid = new Guid(ViewState["bid"].ToString());
                }

                var grossProfit = ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(bid);

                return grossProfit.BaseGrossMargin;
            }
        }

        #region SEO

        public string FacebookAppId
        {
            get
            {
                return ProviderFactory.Instance().GetConfig().FacebookApplicationId;
            }
        }

        public string FacebookOgTitle { get; set; }

        public string FacebookOgUrl
        {
            get
            {
                if (BusinessHourGuid != Guid.Empty)
                {
                    return string.Format("{0}{1}deal.aspx?bid={2}", WebUtility.GetSiteRoot(), Page.AppRelativeTemplateSourceDirectory.Substring(1), BusinessHourGuid);
                }
                return string.Empty;
            }
        }

        public string FacebookOgImage { get; set; }

        public string FacebookOgDescription { get; set; }
       
        public string ImageSrcLinkHref
        {
            get
            {
                return FacebookOgImage;
            }
        }

        public string CanonicalLinkHref { get; set; }
        public string ExpireRedirectDisplay { get; set; }
        public string ExpireRedirectUrl { get; set; }

        #endregion

        /// <summary>
        /// FB分享功能用的檔次編號
        /// </summary>
        public string FbDealId { get; set; }

        public string DealArgs { set; get; }

        public int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(Page.User.Identity.Name);
            }
        }

        #endregion property

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.Presenter.OnViewInitialized();
            }
            
        }

        protected void rpt_LocationBinding(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is AvailableInformation)
            {
                AvailableInformation ai = (AvailableInformation)e.Item.DataItem;
                Literal clit_L_StoreName = ((Literal)e.Item.FindControl("lit_L_StoreName"));//店名
                Literal clit_L_Contents = ((Literal)e.Item.FindControl("lit_L_Contents"));//相關內容
                Literal clit_L_Map = ((Literal)e.Item.FindControl("lit_L_Map"));//地圖

                clit_L_StoreName.Text = ai.N;
                if (!string.IsNullOrEmpty(ai.A))
                {
                    clit_L_Contents.Text += "地址：" + ai.A + "<br>";

                    string googleMapUrl = "https://maps.google.com.tw/maps?f=q&hl=zh-TW&z=16&t=m&iwloc=near&output=&q=";
                    string latitude = string.Empty;
                    string longitude = string.Empty;
                    if (config.GoogleMapInDefaultPage)
                    {
                        string url = string.Empty;
                        if (ai.HasLongLat)
                        {
                            url = googleMapUrl + ai.Latitude + "," + ai.Longitude;
                        }
                        else if (String.IsNullOrEmpty(ai.A) == false)
                        {
                            url = googleMapUrl + ai.A;
                        }
                        clit_L_Map.Text = "<h3  class='Map' onclick=\"window.open('" + url + "');\" style='cursor:pointer;'></h3>";
                    }
                    else if (config.EnableOpenStreetMap)
                    {
                        if (ai.HasLongLat)
                        {
                            SqlGeography geo = LocationFacade.GetGeography(ai.A);
                            if (geo != null && !geo.IsNull)
                            {
                                if (!geo.Lat.IsNull && !geo.Long.IsNull)
                                {
                                    latitude = geo.Lat.ToString();
                                    longitude = geo.Long.ToString();
                                }
                            }
                        }
                        else
                        {
                            latitude = ai.Latitude;
                            longitude = ai.Longitude;
                        }

                        if (!string.IsNullOrWhiteSpace(latitude) && !string.IsNullOrWhiteSpace(longitude))
                        {
                            clit_L_Map.Text = "<h3  class='Map' address='" + ai.A + "' longitude='" + longitude + "' latitude='" + latitude + "' onclick='showmap(this, \"" + config.EnableOpenStreetMap + "\");' style='cursor:pointer;'></h3>";
                        }
                    }
                    else
                    {
                        latitude = ai.Latitude;
                        longitude = ai.Longitude;
                    }

                }

                if (!string.IsNullOrEmpty(ai.P))
                {
                    clit_L_Contents.Text += "電話：" + ai.P + "<br>";
                }

                if ((!string.IsNullOrEmpty(ai.UT)) && (!ai.UT.Trim().Equals("<br />")))
                {
                    clit_L_Contents.Text += "兌換時間：<br>" + ai.UT.Replace("\n", "<br />") + "<br>";
                }
                else if ((!string.IsNullOrEmpty(ai.OT)) && (!ai.OT.Trim().Equals("<br />")))
                {
                    clit_L_Contents.Text += "營業時間：<br>" + ai.OT + "<br>";
                }

                if (!string.IsNullOrEmpty(ai.CD))
                {
                    clit_L_Contents.Text += "公休時間：<br>" + ai.CD + "<br/>";
                }

                if (!string.IsNullOrEmpty(ai.OV))
                {
                    clit_L_Contents.Text += "交通方式：<br>" + ai.OV + "<br/>";
                }

                if (!string.IsNullOrWhiteSpace(ai.U))
                {
                    clit_L_Contents.Text += "官網：<a href='" + ai.U + "' target='_blank'>" + ai.U + "</a><br/>";
                }

                if (!string.IsNullOrWhiteSpace(ai.FB))
                {
                    clit_L_Contents.Text += "Facebook：<a href='" + ai.FB + "' target='_blank'>" + ai.FB + "</a><br/>";
                }

                if (!string.IsNullOrWhiteSpace(ai.PL))
                {
                    clit_L_Contents.Text += "Plurk：<a href='" + ai.PL + "' target='_blank'>" + ai.PL + "</a><br/>";
                }

                if (!string.IsNullOrWhiteSpace(ai.PL))
                {
                    clit_L_Contents.Text += "Blog：<a href='" + ai.BL + "' target='_blank'>" + ai.BL + "</a><br/>";
                }
            }
        }

        #endregion

        #region interface methods

        public void SetDealContent(IViewPponDeal mainDeal, Dictionary<IViewPponDeal, string> subDeals)
        {
            // FB分享檔次ID
            FbDealId = mainDeal.BusinessHourGuid.ToString();

            // 上方分類bar顯示檔次名稱
            liBreadDealName.Text = mainDeal.ItemName;

            // 右方檔次區塊
            lCloseTime.Text = lCloseTime_m.Text = lCloseTime_pad.Text = mainDeal.BusinessHourOrderTimeE.ToString("yyyy/MM/dd HH:mm");
            rptDeals.ItemDataBound += rptDeals_ItemDataBound;
            rptDeals.DataSource = subDeals;
            rptDeals.DataBind();

            var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(mainDeal);
            // 位置資訊
            AvailableInformation[] avc = ProviderFactory.Instance().GetSerializer()
                .Deserialize<AvailableInformation[]>(contentLite.Availability);
            if (avc.Length > 0)
            {
                rpt_Location.DataSource = avc;
                rpt_Location.DataBind();
                divAddressInfo.Visible = true;
            }

            #region 檔次內容

            // icon
            liSpecialCategoryTag.Text = PponFacade.GetPiinlifeDealIconHtmlContent(mainDeal, 3);

            // 檔次名稱
            lDealName.Text = mainDeal.EventName;
            ldesc.Text = mainDeal.EventTitle;

            // APP Title
            AppTitle = string.IsNullOrEmpty(mainDeal.AppTitle) ? mainDeal.CouponUsage : mainDeal.AppTitle;

            // 圖片輪播
            string defaultImgPath = config.SiteUrl + "/Themes/HighDeal/images/index/img.png";
            string[] imgs = ImageFacade.GetMediaPathsFromRawData(mainDeal.EventImagePath, MediaType.PponDealPhoto);
            rptImage.DataSource = imgs.Skip(3).DefaultIfEmpty(defaultImgPath).Select(x => new { Url = x }).ToList();
            rptImage.DataBind();

            // 檔次頁籤
            Dictionary<string, string> tabList = GetDealContentTabList(mainDeal).Skip(1).ToDictionary(x => x.Key, y => y.Value); //第一筆資料為右方說明
            rpTabLinks.DataSource = tabList.Select(x => new { TabTitle = x.Key });
            rpTabLinks.DataBind();
            rpTabContents.DataSource = tabList.Select(x => new { TabTitle = x.Key, Context = x.Value });
            rpTabContents.DataBind();

            #endregion

            #region SEO

            FacebookOgImage = imgs == null ? defaultImgPath : (imgs.Any() ? imgs[0] : defaultImgPath);
            FacebookOgTitle = HttpUtility.HtmlEncode(string.Format("{0} {1}", mainDeal.ItemName, mainDeal.EventTitle));
            FacebookOgDescription = HttpUtility.HtmlEncode(mainDeal.ItemName.Replace('-',' '));
            CanonicalLinkHref = CanonicalUrl(mainDeal);

            #endregion

            ExpireRedirectDisplay = Helper.GetEnumDescription((ExpireRedirectDisplay)(mainDeal.ExpireRedirectDisplay ?? 1));

            string expireRedirectUrl = mainDeal.ExpireRedirectUrl;
            expireRedirectUrl = PponFacade.GetClosedDealExpireRedirectUrl(mainDeal, expireRedirectUrl);

            ExpireRedirectUrl = expireRedirectUrl;
        }

        private string CanonicalUrl(IViewPponDeal mainDeal)
        {
            string dealUrl = BusinessHourGuid != Guid.Empty
                ? string.Format("{0}/piinlife/{1}", WebUtility.GetSiteRoot(), BusinessHourGuid)
                : string.Empty;
            return (mainDeal.UseExpireRedirectUrlAsCanonical ?? false) ? mainDeal.ExpireRedirectUrl : dealUrl;
        }

        void rptDeals_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                KeyValuePair<IViewPponDeal, string> data = (KeyValuePair<IViewPponDeal, string>)e.Item.DataItem;
                PlaceHolder phInstallmentInfo = (PlaceHolder)e.Item.FindControl("phInstallmentInfo");
                if (config.InstallmentPayEnabled && data.Key.Installment3months.GetValueOrDefault())
                {
                    phInstallmentInfo.Visible = true;
                }
                else
                {
                    phInstallmentInfo.Visible = false;
                }
            }
        }

        public void SeoSetting(IViewPponDeal deal, string appendTitle = "")
        {
            if (String.IsNullOrWhiteSpace(deal.PageDesc))
            {
                this.Page.MetaDescription = deal.CouponUsage + "-" + deal.EventTitle; //好康券+橘標
            }
            else
            {
                this.Page.MetaDescription = deal.PageDesc;
            }

            if (String.IsNullOrWhiteSpace(deal.PageTitle))
            {
                if (!string.IsNullOrEmpty(appendTitle))
                {
                    string dealon = "";
                    if (deal.Slug == null)
                    {
                        dealon = "，熱銷中";
                    }
                    this.Page.Title = string.Format("{0}，{1}，{2}", deal.CouponUsage, appendTitle, config.DefaultPiinlifeTitle + dealon);
                }
                else
                {
                    this.Page.Title = string.Format("{0}-{1}，{2}", deal.CouponUsage, deal.EventTitle, config.DefaultPiinlifeTitle);
                }
            }
            else
            {
                this.Page.Title = deal.PageTitle;
            }

            string picAlt = string.IsNullOrWhiteSpace(deal.PicAlt) ? (string.IsNullOrEmpty(deal.AppTitle) ? Page.MetaDescription : deal.AppTitle) : deal.PicAlt;
            this.Page.MetaKeywords = string.Format("{0},{1}", string.IsNullOrEmpty(picAlt) ? string.Empty : picAlt.Replace("/", ","), config.DefaultPiinlifeKeywords);
        }

        public void RedirectToPiinlifeDefault()
        {
            Response.Redirect(ResolveUrl("~/piinlife/default.aspx" + (!string.IsNullOrWhiteSpace(Rsrc) ? "?rsrc=" + Rsrc : string.Empty)));
        }

        public void RedirectToComboDealMain(Guid? mainbid, Guid? subbid)
        {
            if (mainbid == null || mainbid == Guid.Empty)
            {
                Response.Redirect(ResolveUrl("~/piinlife/default.aspx" + (!string.IsNullOrWhiteSpace(Rsrc) ? "?rsrc=" + Rsrc : string.Empty)));
            }
            else
            {
                if (subbid != null && subbid != Guid.Empty)
                {
                    Response.Redirect(ResolveUrl(string.Format("~/piinlife/deal.aspx?bid={0}&sub={1}", mainbid, subbid) + (!string.IsNullOrWhiteSpace(Rsrc) ? "&rsrc=" + Rsrc : string.Empty)));
                }
                else
                {
                    Response.Redirect(ResolveUrl("~/piinlife/" + mainbid) + (!string.IsNullOrWhiteSpace(Rsrc) ? "/" + Rsrc : string.Empty));
                }
            }
        }

        #endregion

        #region WebMethod

        [WebMethod]

        public string GetDiscountString(bool noDiscountShown, decimal itemPrice, decimal itemOrigPrice)
        {
            string discount = ViewPponDealManager.GetDealDiscountString(noDiscountShown, itemPrice, itemOrigPrice);
            if (!string.IsNullOrWhiteSpace(discount))
            {
                return discount + "折";
            }
            else
            {
                return string.Empty;
            }
        }

        public string GetDealMainContent(string remark)
        {
            return remark.Split(new[] { "||" }, StringSplitOptions.None)[2];
        }

        public string GetBuyButtonClassName(IViewPponDeal vpd)
        {
            string buyButton = string.Empty;
            if (DateTime.Now < vpd.BusinessHourOrderTimeS)
            {
                //尚未開賣，目前時間小於起始時間，顯示即將開賣
                buyButton = @"<div class=""Button_Single_CountDown""></div>";
            }
            else if (DateTime.Now > vpd.BusinessHourOrderTimeE)
            {
                //現在時間大於 結束時間 ，檔次活動已結束，顯示已售完。
                buyButton = @"<div class=""Button_Single_SoldOut""></div>";
            }
            else if (vpd.OrderedQuantity >= vpd.OrderTotalLimit)
            {
                //可銷售數量為0，顯示已售完
                buyButton = @"<div class=""Button_Single_SoldOut""></div>";
            }
            else
            {
                //可銷售
                string buyUrl = ResolveUrl("~/piinlife/buy.aspx?bid=" + vpd.BusinessHourGuid);
                buyButton = @"<a href=""" + buyUrl + @"""><div class=""Button_Single_Buy""></div></a>";
            }
            return buyButton;
        }

        #endregion

        #region private method
        public Dictionary<string, string> GetDealContentTabList(IViewPponDeal vpd)
        {
            Dictionary<string, string> tabs = new Dictionary<string, string>();
            var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(vpd);
            if (contentLite.Remark !=null)
            {
                // 各項頁籤說明
                foreach (string tab in contentLite.Remark.Split(new[] { "||" }, StringSplitOptions.None))
                {
                    string[] item = tab.Split(new[] { "##" }, StringSplitOptions.None);
                    if (item.Length == 2 && !string.IsNullOrWhiteSpace(item[0]))
                    {
                        if (!tabs.ContainsKey(item[0]))
                        {
                            tabs.Add(item[0], item[1]);
                        }
                    }
                }
            }
            else
            {
                tabs.Add("", "");
            }

            // 最後再加入權益說明
            tabs.Add(I18N.Phrase.PiinlifeIntroductionTitle, contentLite.Introduction);
            return tabs;
        }
        #endregion
    }
}
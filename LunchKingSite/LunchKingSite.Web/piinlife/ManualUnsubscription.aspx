﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManualUnsubscription.aspx.cs" 
MasterPageFile="~/piinlife/hideal.Master" Inherits="LunchKingSite.Web.piinlife.ManualUnsubscription"  Title="手動取消訂閱品生活"%>

<asp:Content ID="Content1" ContentPlaceHolderID="SMC" runat="server">
    <link href="../Themes/HighDeal/images/Pay/PaymentTerms.css" rel="stylesheet" type="text/css">
    <link href="../Themes/HighDeal/images/EDM/EDM.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="../Tools/js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" src="../Tools/js/effects/jquery.smallslider.js"></script>

    <script type="text/javascript">
        function LinkUrl(value) {
            switch (value) {
                case 1:
                    document.location.reload();
                    break;
                case 2:
                    document.location.href = "../piinlife/default.aspx";
                    break;
            }
        }

    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <div class="wrap">
        <section class="EDM">
            <content class="Left-column" style="padding-right: 0px;">
                <!--取消訂閱頁-->
                 <asp:Panel ID="unsubscribeArea" class="EDMunsubscribeArea" runat="server" Visible="false">
                <p class="Title">取消訂閱?</p>
                <p class="Info">您確定不要再收到品生活的最新活動通知訊息嗎？&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="ButtonBox">
                <asp:LinkButton ID="btnUnSubscribe" runat="server" CssClass="ButtonField_S" BorderWidth="0"  OnClick="btnUnSubscribe_Click" >
                <span><span class="BtnName">確 定</span></span>
                </asp:LinkButton>
                </span>&nbsp;&nbsp;&nbsp;
                <span class="ButtonBox">
                <asp:LinkButton ID="lb1" runat="server" CssClass="ButtonField_S" BorderWidth="0" OnClientClick="LinkUrl(1); return false;" >
                <span><span class="BtnName">取 消</span></span> 
                </asp:LinkButton>
                </span>
                </p>
                </asp:Panel >

                <!--成功頁-->
                 <asp:Panel ID="unsubscribeOK" class="unsubscribeOK" runat="server"  Visible="false">
                <p class="Title">取消訂閱成功。</p>
                <p class="Info">您的申請已被記錄。若有任何問題，請 <a href="<%=SystemConfig.SiteUrl + SystemConfig.SiteServiceUrl%>" target="_blank" >連絡客服</a>。<br>
                您可隨時於品生活首頁左方的 [訂閱追蹤] 訂閱最新電子報。
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="ButtonBox">
                <asp:LinkButton ID="lb2" runat="server" CssClass="ButtonField_S" BorderWidth="0"  OnClientClick="LinkUrl(2); return false;">
                <span><span class="BtnName">回首頁</span></span>
                </asp:LinkButton>
                </span>
                </p>
            </asp:Panel >

               <!--失敗頁-->
                 <asp:Panel ID="unsubscribeFailed" class="unsubscribeFailed" runat="server"  Visible="false">
                <p class="Title">取消訂閱失敗。</p>
                <p class="Info">系統發生異常，取消訂閱失敗，請稍候再試。
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="ButtonBox">
                <asp:LinkButton ID="lb3" runat="server" CssClass="ButtonField_S" BorderWidth="0"   OnClientClick="LinkUrl(2); return false;" >
                <span><span class="BtnName">回首頁</span></span>
                </asp:LinkButton>
                </span>
                </p>
                </asp:Panel >
              </content>  
        <content class="Right-AD">
        <asp:Panel ID="ADPIC" BorderWidth="0" runat="server" Visible="false" >
        <img src="../Themes/HighDeal/images/Pay/AD-Fake.png" width="300" height="450">
          </asp:Panel>
        </content>

        </section>
    </div>

</asp:Content>

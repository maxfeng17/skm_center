﻿<%@ Page Title="" Language="C#" MasterPageFile="~/piinlife/hideal.Master" AutoEventWireup="true" CodeBehind="returned.aspx.cs" Inherits="LunchKingSite.Web.piinlife.returned" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptContent" runat="server">  
    <link href="<%#ResolveUrl("../Themes/HighDeal/images/Returns/Returns.css") %>" rel="stylesheet" type="text/css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField runat="server" ID="hdOrderId" />
    <asp:PlaceHolder runat="server" ID="plOrderIdInput" Visible="true">
        <script type="text/javascript" language="javascript">
            $(document).ready(function () {
            });
        </script>
        <section class="Returns">
            <h1>退貨處理中心</h1>
            <hr class="Line" />
            <article>
                <h2>請輸入您要申請退貨的訂單編號：</h2>
                <p>您可至<a href="<%=ResolveUrl("../piinlife/member/OrderList.aspx") %>" target="_blank" >訂單查詢</a>查看訂單編號，請勿輸入憑證號碼。</p>
                <div class="SearchBar">
                    <asp:textbox ID="tbOrderId" runat="server"></asp:textbox>
                    <span id="errMessage" class="Notice" style="display:inline;"><asp:Literal runat="server" ID="litOrderIdError"></asp:Literal>  </span>
                </div>
                <div class="ButtonBox">
                    <span class="ButtonField_B">
                    <span>
                        <span class="BtnName">
                            <asp:button runat="server" CssClass="InputBtn_B" ID="btnOrderIdInput"  OnClick="OnOrderIdInput" BorderStyle="None" Text="送 出" />
                        </span>
                    </span>
                    </span>
                </div>
            </article>
        </section>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plReasonInput" Visible="false">
        <section class="Returns">
            <h1>退貨處理中心
                <span class="TitleArrow"></span>訂單編號：<label><%=OrderIdShow %></label></h1>
            <hr class="Line" />
            <article>
                <h2>申請退貨原因</h2><br>
                <p>為了提供您更優質的商品與售後服務，請協助我們了解您的退貨原因與建議。
                    <asp:TextBox ID="tbReason" runat="server" style="width:740px; height:200px;" TextMode="MultiLine"/>
                </p>
                <div class="Notice" style="display:block;"><asp:Literal runat="server" ID="litReasonPageMessage"></asp:Literal></div>
                <div class="ButtonBox">
                    <span class="ButtonField_B">
                    <span>
                        <span class="BtnName">
                            <asp:button runat="server" CssClass="InputBtn_B" ID="btnReason"  OnClick="OnReasonInput" BorderStyle="None" Text="送 出" />
                        </span>
                    </span>
                    </span>
                </div>
            </article>
        </section>
    </asp:PlaceHolder>

    <asp:PlaceHolder runat="server" ID="plCouponReturn" Visible="false">
        <section class="Returns">
<h1>退貨處理中心<span class="TitleArrow"></span>訂單編號：<label><%=OrderIdShow %></label></h1>
<hr class="Line" />
<article>
<h2>退貨確認</h2>
<br>
<p>
提醒您，退貨一經申請成功，將會退掉訂單內尚未使用完畢的憑證。<br>
購買時，若有使用購物金抵扣消費<span class="Tips">（17 Life購物金、17 Life紅利金或PayEasy購物金）</span>，將會依原抵扣金額退回，<br>
若使用17 Life現金劵折抵則無法退還。
</p><br>
<p>按下［確認退貨］後，即無法再取消退貨。</p><br>
<div class="ButtonBox">
<span class="ButtonField_B">
<span><span class="BtnName"><asp:button runat="server" CssClass="InputBtn_B" ID="btnCouponReturn"  OnClick="OnCouponReturn" BorderStyle="None" Text="確認退貨" /></span></span>
</span>
</div>
<div class="ExtraLink"><asp:LinkButton runat="server" ID="lkbToCouponCashBack" OnClick="OnToCouponRefund" Visible="False" >刷退請點此</asp:LinkButton></div>
</article>
</section>

    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plCouponPointBackSuccess" Visible="false">
        <section class="Returns">
<h1>退貨處理中心<span class="TitleArrow"></span>訂單編號：<label><%=OrderIdShow %></label></h1>
<hr class="Line" />
<article>
<h2>退貨申請成功</h2>
<br>
<p><strong>請您依序以下步驟進行作業：</strong></p>
<div class="InnerContent">
<h2><span class="Tips">1.列印（退貨申請書）或（退回或折讓證明單）</span></h2>
<p>送出刷退申請後，我們將會寄發一封【PiinLife品生活】退貨申請通知信至您的會員註冊Email信箱中，<br/>
 請您依序下載（退貨申請書）及（退回或折讓證明單）。或者您也可隨時登入至訂單查詢中自行下載。</p>
<p class="Notice" style="text-align:left;">※系統將會依您的訂單狀況，自動判別您是否須提供（退回或折讓證明單），若訂單尚未開立發票，<br>下載之連結僅會有（退貨申請書）。</p>

<h2><span class="Tips">2.填妥資料並簽章</span></h2>
<p>於（退貨申請書）中，請以正楷簽名或蓋章擇一（須與買受人相符），若為三聯式發票請
填寫「營利<br/>事業統一編號」並蓋上公司發票章。</p>
<p class="Notice" style="text-align:left;">※如您須提供（退回或折讓證明單），請比照（退貨申請書）填寫格式，務必填寫完整。</p>

<h2><span class="Tips">3.傳真或寄回品生活退貨處理中心</span></h2>
<p>為加速作業處理，請將（退貨申請書）以郵局掛號或傳真方式寄回品生活退貨處理中心<br>
地址：104 台北市中山區中山北路一段11號13樓<br>
傳真電話：(02)2511-9110</p>
<p class="Notice" style="text-align:left;">※若已開立發票，請將（退回或折讓證明單）傳真或寄回品生活退貨處理中心；若於訂購時已索取紙本<br/>發票，亦請
務必將發票一併寄回，否則恕無法辦理退費。</p>

<h2><span class="Tips">4.完成退貨申請</span></h2>
<p>待我們收到（退貨申請書），確定無誤後即會進行退款作業，處理完畢將會另以E-mail通知您。</p><br>
<p style="text-align:center;">
<asp:PlaceHolder runat="server" ID="phCouponPointBackShowDiscountSingle" Visible="false">
<span class="ButtonBox">
<span class="ButtonField_B">
<span><span class="BtnName">
          <input name="" type="button" value="下載退回或折讓證明單" class="InputBtn_B" onclick="document.location.href='<%=DiscountSingleFormUrlShow%>';" />  
      </span></span>
</span>
</span>
    </asp:PlaceHolder>
</p>
</div>
<br>
</article>
</section>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plCouponCashBack" Visible="false">
        <section class="Returns">
            <h1>退貨處理中心<span class="TitleArrow"></span>訂單編號：<label><%=OrderIdShow%></label></h1>
            <hr class="Line" />
            <article>
                <h2>刷退申請</h2>
                <br>
                <p><strong>刷退步驟：</strong></p>
                <div class="InnerContent">
                <ul>
	                <li>(1)列印(退貨申請書)或(退回或折讓證明單)</li>
                    <li>(2)填妥資料、簽章，並傳真或寄回品生活退貨處理中心</li>
                    <li>(3)完成退貨申請</li>
                </ul>
                <p>退款金額將直接退至您的信用卡內，因各家銀行作業時間不同，完成上述步驟後，您應可於下一期信用卡帳單看到此退款(視信用卡結帳週期而定)，請您耐心等候。</p>
                    <asp:panel ID="pnBackBtnToshop" runat="server">
                    <p>一般退貨(轉退17Life購物金)處理時間只要10~20個工作天。
                    <span class="ButtonBox">
                        <span class="ButtonField_S">
                            <span><span class="BtnName">
                                <asp:button ID="btnBackToCouponReturn" runat="server" CssClass="InputBtn_S"  BorderStyle="None" OnClick="OnCouponReturn" Text="改退購物金"/>
                            </span></span>
                        </span>
                    </span>
                    </p>
                </asp:panel>
                </div>
                <br><p>
                提醒您，退貨一經申請成功，將會退掉訂單內尚未使用完畢的憑證。<br>
                購買時，若有使用購物金扣抵消費<span class="Tips">（17Life購物金、17Life紅利金或PayEasy購物金）</span>，將會依原抵扣金額退回，<br>
                若使用17 Life現金劵折抵則無法退還。<br>
                </p><br>
                <p>按下［確認申請］按鈕後，即無法再取消退貨。</p><br>
                <div class="ButtonBox">
                    <span class="ButtonField_B">
                        <span>
                            <span class="BtnName">
                                <asp:button ID="btnCCashBackConfirm" runat="server" CssClass="InputBtn_B"  BorderStyle="None" OnClick="OnCouponCashBackConfirm" Text="確認申請"/>
                            </span>
                        </span>
                    </span>
                </div>
            </article>
        </section>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plCouponCashBackSuccess" Visible="false">
        <section class="Returns">
<h1>退貨處理中心<span class="TitleArrow"></span>訂單編號：<label><%=OrderIdShow %></label></h1>
<hr class="Line" />
<article>
<h2>退貨申請成功</h2><br>
<p>退款金額將直接退至您的信用卡內，因各家銀行作業時間不同，您應可於下一期信用卡帳單看到此退款（視信用卡結帳週期而定），請您耐心等候。</p>
<br>
<p><strong>請您依序以下步驟進行作業：</strong></p>
<div class="InnerContent">
<h2><span class="Tips">1.列印（退貨申請書）或（退回或折讓證明單）</span></h2>
<p>我們將會寄發一封【PiinLife品生活】退貨申請通知信至您的會員註冊Email信箱中，<br/>
 請您依序下載（退貨申請書）及（退回或折讓證明單）。或者您也可隨時登入至訂單查詢中自行下載。</p>
<p class="Notice" style="text-align:left;">※系統將會依您的訂單狀況，自動判別您是否須提供（退回或折讓證明單），若訂單尚未開立發票，<br>下載之連結僅會有（退貨申請書）。</p>

<h2><span class="Tips">2.填妥資料並簽章</span></h2>
<p>於（退貨申請書）中，請以正楷簽名或蓋章擇一（須與買受人相符），若為三聯式發票請
填寫「營利<br/>事業統一編號」並蓋上公司發票章。</p>
<p class="Notice" style="text-align:left;">※如您須提供（退回或折讓證明單），請比照（退貨申請書）填寫格式，務必填寫完整。</p>

<h2><span class="Tips">3.傳真或寄回品生活退貨處理中心</span></h2>
<p>為加速作業處理，請將（退貨申請書）以郵局掛號或傳真方式寄回品生活退貨處理中心<br>
地址：104 台北市中山區中山北路一段11號13樓<br>
傳真電話：(02)2511-9110</p>
<p class="Notice" style="text-align:left;">※若已開立發票，請將（退回或折讓證明單）傳真或寄回品生活退貨處理中心；若於訂購時已索取紙本<br/>發票，亦請
務必將發票一併寄回，否則恕無法辦理退費。</p>

<h2><span class="Tips">4.完成退貨申請</span></h2>
<p>待我們收到（退貨申請書），確定無誤後即會進行退款作業，處理完畢將會另以E-mail通知您。</p><br>
<p style="text-align:center;">
<asp:PlaceHolder runat="server" ID="phCouponCashBackShowDiscountSingle" Visible="False">
<span class="ButtonBox">
<span class="ButtonField_B">
<span><span class="BtnName">
          <input name="" type="button" value="下載退回或折讓證明單" class="InputBtn_B" onclick="document.location.href='<%=DiscountSingleFormUrlShow.Replace("https","http")%>';" /> 
      </span></span>
</span>
</span>
    </asp:PlaceHolder>
</p>
</div>
<br>
</article>
</section>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plCouponCashBackSuccessBeforeClose" Visible="false">
        <section class="Returns">
<h1>退貨處理中心<span class="TitleArrow"></span>訂單編號：<label><%=OrderIdShow %></label></h1>
<hr class="Line" />
<article>
<h2>退貨申請成功</h2><br>
<p>退款金額將直接退至您的信用卡內，因各家銀行作業時間不同，您應可於下一期信用卡帳單看到此退款（視信用卡結帳週期而定），請您耐心等候。</p>
<br>
<p><strong>請您依序以下步驟進行作業：</strong></p>
<div class="InnerContent">
<h2><span class="Tips">1.列印（退貨申請書）或（退回或折讓證明單）</span></h2>
<p>我們將會寄發一封【PiinLife品生活】退貨申請通知信至您的會員註冊Email信箱中，<br/>
 請您依序下載（退貨申請書）及（退回或折讓證明單）。或者您也可隨時登入至訂單查詢中自行下載。</p>
<p class="Notice" style="text-align:left;">※系統將會依您的訂單狀況，自動判別您是否須提供（退回或折讓證明單），若訂單尚未開立發票，<br>下載之連結僅會有（退貨申請書）。</p>

<h2><span class="Tips">2.填妥資料並簽章</span></h2>
<p>於（退貨申請書）中，請以正楷簽名或蓋章擇一（須與買受人相符），若為三聯式發票請
填寫「營利<br/>事業統一編號」並蓋上公司發票章。</p>
<p class="Notice" style="text-align:left;">※如您須提供（退回或折讓證明單），請比照（退貨申請書）填寫格式，務必填寫完整。</p>

<h2><span class="Tips">3.傳真或寄回品生活退貨處理中心</span></h2>
<p>為加速作業處理，請將（退貨申請書）以郵局掛號或傳真方式寄回品生活退貨處理中心<br>
地址：104 台北市中山區中山北路一段11號13樓<br>
傳真電話：(02)2511-9110</p>
<p class="Notice" style="text-align:left;">※若已開立發票，請將（退回或折讓證明單）傳真或寄回品生活退貨處理中心；若於訂購時已索取紙本<br/>發票，亦請
務必將發票一併寄回，否則恕無法辦理退費。</p>

<h2><span class="Tips">4.完成退貨申請</span></h2>
<p>待我們收到（退貨申請書），確定無誤後即會進行退款作業，處理完畢將會另以E-mail通知您。</p><br>
<p style="text-align:center;">
<span class="ButtonBox">
<span class="ButtonField_B">
<span><span class="BtnName">
          <input name="" type="button" value="下載退回或折讓證明單" class="InputBtn_B" onclick="document.location.href='<%=DiscountSingleFormUrlShow.Replace("https","http")%>';" />      
      </span></span>
</span>
</span>
</p>
</div>
<br>
</article>
</section>
    </asp:PlaceHolder>

    <asp:PlaceHolder runat="server" ID="plGoodsReturn" Visible="false">
        <section class="Returns">
<h1>退貨處理中心<span class="TitleArrow"></span>訂單編號：<label><%=OrderIdShow %></label></h1>
<hr class="Line" />
<article>
<h2>退貨確認</h2>
<br>
<p>
※一般商品於收貨後七天鑑賞期內可申請退貨。<br>
※生鮮冷藏食品類之商品於收貨後24小時內可申請退貨，基於衛生考量，商品一經拆封恕無法受理退貨，<br>若有損壞等情況，請務必於收貨後24小時內拍照存證。<br>
※部分特殊商品退貨規則，請依權益說明為主。<br>
</p><br>
<p>提醒您，退貨一經申請成功，將會退掉訂單內尚未使用完畢的憑證。</p>
<p>
購買時，若有使用購物金抵扣消費<span class="Tips">（17Life購物金、17Life紅利金或PayEasy購物金）</span>，將會依原抵扣金額退回，<br>
若使用17 Life現金劵折抵則無法退還。
</p><br>
<p>按下［確認退貨］後，即無法再取消退貨。</p><br>
<div class="ButtonBox">
<span class="ButtonField_B">
<span><span class="BtnName"><asp:button runat="server" CssClass="InputBtn_B" ID="btnGoodsReturn"  OnClick="OnGoodsReturn" BorderStyle="None" Text="確認退貨" /></span></span>
</span>
</div>
<div class="ExtraLink"><asp:LinkButton runat="server" ID="lkbToGoodsCashBack" OnClick="OnToGoodsRefund" Visible="False" >刷退請點此</asp:LinkButton></div>
</article>
</section>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plGoodsPointBackSuccess" Visible="false">
        <section class="Returns">
<h1>退貨處理中心<span class="TitleArrow"></span>訂單編號：<label><%=OrderIdShow %></label></h1>
<hr class="Line" />
<article>
<h2>退貨申請成功</h2><br>
<p>我們已收到您的退貨申請，後續將通知廠商儘速安排物流人員於3-5工作天前往取件。</p>
<p>若您尚未收到商品，我們將會通知廠商取消出貨作業，但因傳遞訊息時間差的關係，商品有可能已進行出貨而來不及取消，再請您接獲物流人員通知時直接拒收即可。</p>
<br>
<p><strong>請您依序以下步驟進行作業：</strong></p>
<div class="InnerContent">
<h2><span class="Tips">1.備妥商品</span></h2>
<p>所退回的商品必須保持商品本體、附件、內外包裝、配件、贈品、保證書、原廠包裝及所有隨附文件或資料的完整性，切勿缺漏任何配件或損毀原廠外盒。<br>
請儘量以送貨使用之原包裝紙箱將退貨商品包裝妥當，若原紙箱已遺失，請另使用其他紙箱包覆於商品原廠包裝之外，切勿直接於原廠包裝上黏貼紙張或書寫文字。<br>
我們將會安排物流人員於3-5個工作天內前往取件。</p>

<h2><span class="Tips">2.列印（退貨申請書）或（退回或折讓證明單）</span></h2>
<p>送出刷退申請後，我們將會寄發一封【PiinLife品生活】退貨申請通知信至您的會員註冊Email信箱中，<br>
請您依序下載（退貨申請書）及（退回或折讓證明單）。或者您也可隨時登入至訂單查詢中自行下載。</p>
<p class="Notice" style="text-align:left;">※系統將會依您的訂單狀況，自動判別您是否須提供（退回或折讓證明單），若訂單尚未開立發票，下載之連結僅
會有（退貨申請書）。</p>

<h2><span class="Tips">3.填妥資料並簽章</span></h2>
<p>於（退貨申請書）中，請以正楷簽名或蓋章擇一（須與買受人相符），若為三聯式發票請填寫「營利事<br>業統一編號」並蓋上公司發票章。</p>
<p class="Notice" style="text-align:left;">※如您須提供（退回或折讓證明單），請比照（退貨申請書）填寫格式，務必填寫完整。</p>

<h2><span class="Tips">4.傳真或寄回品生活退貨處理中心</span></h2>
<p>為加速作業處理，請將（退貨申請書）以郵局掛號或傳真方式寄回品生活退貨處理中心<br>
地址：104 台北市中山區中山北路一段11號13樓<br>
傳真電話：(02)2511-9110</p>
<p class="Notice" style="text-align:left;">※若已開立發票，請將（退回或折讓證明單）傳真或寄回品生活退貨處理中心；若於訂購時已索取紙本發票，<br>
亦請務必將發票一併寄回，否則恕無法辦理退費。</p>

<h2><span class="Tips">5.完成退貨申請</span></h2>
<p>待我們收到（退貨申請書），確定無誤後即會進行退款作業，處理完畢將會另以E-mail通知您。</p><br>

<p style="text-align:center;">
    <asp:PlaceHolder runat="server" ID="phGoodsPointBackShowDiscountSingle" Visible="False">
<span class="ButtonBox">
<span class="ButtonField_B">
<span><span class="BtnName">
          <input name="" type="button" value="下載退回或折讓證明單" class="InputBtn_B" onclick="document.location.href='<%=DiscountSingleFormUrlShow.Replace("https","http")%>';" />      
      </span></span>
</span>
</span>
    </asp:PlaceHolder>
</p>
</div>
<br>
</article>
</section>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plGoodsCashBack" Visible="false">
        <section class="Returns">
<h1>退貨處理中心<span class="TitleArrow"></span>訂單編號：<label><%=OrderIdShow %></label></h1>
<hr class="Line" />
<article>
<h2>刷退申請</h2>
<br>
<p>
※一般商品於收貨後七天鑑賞期內可申請退貨。<br>
※生鮮冷藏食品類之商品於收貨後24小時內可申請退貨，基於衛生考量，商品一經拆封恕無法受理退貨，<br>若有損壞等情況，請務必於收貨後24小時內拍照存證。<br>
※部分特殊商品退貨規則，請依權益說明為主。<br>
</p><br>
<p><strong>申請刷退的退貨步驟：</strong></p>
<div class="InnerContent">
<ul>
	<li>(1)備妥商品</li>
	<li>(2)列印（退貨申請書）或（退回或折讓證明單）</li>
    <li>(3)填妥資料、簽章，並傳真或寄回品生活退貨處理中心</li>
    <li>(4)完成退貨申請</li>
</ul>
<p>退款金額將直接退至您的信用卡內，因各家銀行作業時間不同，完成上述步驟後，您應可於下一期信用卡帳單看到此退款（視信用卡結帳週期而定），請您耐心等候。</p><br>
                    <asp:panel ID="pnBackBtnTohome" runat="server">
                        <p>一般退貨(轉退17Life購物金)處理時間只要10~20個工作天。
                        <span class="ButtonBox">
                            <span class="ButtonField_S">
                               <span><span class="BtnName">
                                   <asp:button ID="btnBackToGoodsReturn" runat="server" CssClass="InputBtn_S"  BorderStyle="None" OnClick="OnGoodsReturn" Text="改退購物金"/>
                               </span></span>
                            </span>
                        </span>
                        </p>
                    </asp:panel>

</div>
<br><p>
提醒您，退貨一經申請成功，將會退掉訂單內尚未使用完畢的憑證。<br>
購買時，若有使用購物金抵扣消費<span class="Tips">（17Life購物金、17Life紅利金或PayEasy購物金）</span>，將會依原抵扣金額退回，<br>
若使用17 Life現金劵折抵則無法退還。<br>
</p><br>
<p>按下［確認申請］後，即無法再取消退貨。</p><br>
<div class="ButtonBox">
<span class="ButtonField_B">
<span><span class="BtnName"><asp:button ID="btnGCashBackConfirm" runat="server" CssClass="InputBtn_B"  BorderStyle="None" OnClick="OnGoodsCashBackConfirm" Text="確認申請"/></span></span>
</span>
</div>
</article>
</section>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plGoodsCashBackSuccess" Visible="false">
        <section class="Returns">
<h1>退貨處理中心<span class="TitleArrow"></span>訂單編號：<label><%=OrderIdShow %></label></h1>
<hr class="Line" />
<article>
<h2>退貨申請成功</h2><br>
<p>我們已收到您的退貨申請，後續將通知廠商儘速安排物流人員於3-5工作天前往取件。</p>
<p>若您尚未收到商品，我們將會通知廠商取消出貨作業，但因傳遞訊息時間差的關係，商品有可能已進行出貨而來不及取消，再請您接獲物流人員通知時直接拒收即可。</p>
<br>
<p>退款金額將直接退至您的信用卡內，因各家銀行作業時間不同，完成以下步驟後，您應可於下一期信用卡帳單看到此退款（視信用卡結帳週期而定），請您耐心等候。</p>
<br>
<p><strong>請您依序以下步驟進行作業：</strong></p>
<div class="InnerContent">
<h2><span class="Tips">1.備妥商品</span></h2>
<p>所退回的商品必須保持商品本體、附件、內外包裝、配件、贈品、保證書、原廠包裝及所有隨附文件或資料的完整性，切勿缺漏任何配件或損毀原廠外盒。<br>
請儘量以送貨使用之原包裝紙箱將退貨商品包裝妥當，若原紙箱已遺失，請另使用其他紙箱包覆於商品原廠包裝之外，切勿直接於原廠包裝上黏貼紙張或書寫文字。<br>
我們將會安排物流人員於3-5個工作天內前往取件。</p>

<h2><span class="Tips">2.列印（退貨申請書）或（退回或折讓證明單）</span></h2>
<p>送出刷退申請後，我們將會寄發一封【PiinLife品生活】退貨申請通知信至您的會員註冊Email信箱中，<br>
請您依序下載（退貨申請書）及（退回或折讓證明單）。或者您也可隨時登入至訂單查詢中自行下載。</p>
<p class="Notice" style="text-align:left;">※系統將會依您的訂單狀況，自動判別您是否須提供（退回或折讓證明單），若訂單尚未開立發票，下載之連結僅
會有（退貨申請書）。</p>

<h2><span class="Tips">3.填妥資料並簽章</span></h2>
<p>於（退貨申請書）中，請以正楷簽名或蓋章擇一（須與買受人相符），若為三聯式發票請填寫「營利事<br>業統一編號」並蓋上公司發票章。</p>
<p class="Notice" style="text-align:left;">※如您須提供（退回或折讓證明單），請比照（退貨申請書）填寫格式，務必填寫完整。</p>

<h2><span class="Tips">4.傳真或寄回品生活退貨處理中心</span></h2>
<p>為加速作業處理，請將（退貨申請書）以郵局掛號或傳真方式寄回品生活退貨處理中心<br>
地址：104 台北市中山區中山北路一段11號13樓<br>
傳真電話：(02)2511-9110</p>
<p class="Notice" style="text-align:left;">※若已開立發票，請將（退回或折讓證明單）傳真或寄回品生活退貨處理中心；若於訂購時已索取紙本發票，<br>
亦請務必將發票一併寄回，否則恕無法辦理退費。</p>

<h2><span class="Tips">5.完成退貨申請</span></h2>
<p>待我們收到（退貨申請書），確定無誤後即會進行退款作業，處理完畢將會另以E-mail通知您。</p><br>

<p style="text-align:center;">
<asp:PlaceHolder runat="server" ID="phGoodsCashBackShowDiscountSingle" Visible="False">
<span class="ButtonBox">
<span class="ButtonField_B">
<span><span class="BtnName">
          <input name="" type="button" value="下載退回或折讓證明單" class="InputBtn_B" onclick="document.location.href='<%=DiscountSingleFormUrlShow.Replace("https","http")%>';">
      </span></span>
&nbsp;&nbsp;</span></span>
        </asp:PlaceHolder>
</p>
</div>
<br>
</article>
</section>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="plGoodsCashBackSuccessBeforeClose" Visible="false">
        <section class="Returns">
<h1>退貨處理中心<span class="TitleArrow"></span>訂單編號：<label><%=OrderIdShow %></label></h1>
<hr class="Line" />
<article>
<h2>退貨申請成功</h2><br>
<p>我們已收到您的退貨申請，後續將通知廠商儘速安排物流人員於3-5工作天前往取件。</p>
<p>若您尚未收到商品，我們將會通知廠商取消出貨作業，但因傳遞訊息時間差的關係，商品有可能已進行出貨而來不及取消，再請您接獲物流人員通知時直接拒收即可。</p>
<br>
<p>退款金額將直接退至您的信用卡內，因各家銀行作業時間不同，完成以下步驟後，您應可於下一期信用卡帳單看到此退款（視信用卡結帳週期而定），請您耐心等候。</p>
<br>
<p><strong>請您依序以下步驟進行作業：</strong></p>
<div class="InnerContent">
<h2><span class="Tips">1.備妥商品</span></h2>
<p>所退回的商品必須保持商品本體、附件、內外包裝、配件、贈品、保證書、原廠包裝及所有隨附文件或資料的完整性，切勿缺漏任何配件或損毀原廠外盒。<br>
請儘量以送貨使用之原包裝紙箱將退貨商品包裝妥當，若原紙箱已遺失，請另使用其他紙箱包覆於商品原廠包裝之外，切勿直接於原廠包裝上黏貼紙張或書寫文字。<br>
我們將會安排物流人員於3-5個工作天內前往取件。</p>

<h2><span class="Tips">2.列印（退貨申請書）或（退回或折讓證明單）</span></h2>
<p>送出刷退申請後，我們將會寄發一封【PiinLife品生活】退貨申請通知信至您的會員註冊Email信箱中，<br>
請您依序下載（退貨申請書）及（退回或折讓證明單）。或者您也可隨時登入至訂單查詢中自行下載。</p>
<p class="Notice" style="text-align:left;">※系統將會依您的訂單狀況，自動判別您是否須提供（退回或折讓證明單），若訂單尚未開立發票，下載之連結僅
會有（退貨申請書）。</p>

<h2><span class="Tips">3.填妥資料並簽章</span></h2>
<p>於（退貨申請書）中，請以正楷簽名或蓋章擇一（須與買受人相符），若為三聯式發票請填寫「營利事<br>業統一編號」並蓋上公司發票章。</p>
<p class="Notice" style="text-align:left;">※如您須提供（退回或折讓證明單），請比照（退貨申請書）填寫格式，務必填寫完整。</p>

<h2><span class="Tips">4.傳真或寄回品生活退貨處理中心</span></h2>
<p>為加速作業處理，請將（退貨申請書）以郵局掛號或傳真方式寄回品生活退貨處理中心<br>
地址：104 台北市中山區中山北路一段11號13樓<br>
傳真電話：(02)2511-9110</p>
<p class="Notice" style="text-align:left;">※若已開立發票，請將（退回或折讓證明單）傳真或寄回品生活退貨處理中心；若於訂購時已索取紙本發票，<br>
亦請務必將發票一併寄回，否則恕無法辦理退費。</p>

<h2><span class="Tips">5.完成退貨申請</span></h2>
<p>待我們收到（退貨申請書），確定無誤後即會進行退款作業，處理完畢將會另以E-mail通知您。</p><br>

<p style="text-align:center;">
<span class="ButtonBox">
<span class="ButtonField_B">
<span><span class="BtnName">
          <input name="" type="button" value="下載退回或折讓證明單" class="InputBtn_B" onclick="document.location.href='<%=DiscountSingleFormUrlShow.Replace("https","http")%>';" />
      </span></span>
</span>
</span>
</p>
</div>
<br>
</article>
</section>
    </asp:PlaceHolder>
</asp:Content>

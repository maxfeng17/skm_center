﻿using System;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System.Web.Security;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.piinlife
{
    public partial class ManualUnsubscription : BasePage, IHiDealManualUnsubscriptionView
    {
        public event EventHandler<DataEventArgs<string>> UnSubscribe = null;

        #region property
        private HiDealManualUnsubscriptionPresenter _presenter;
        public HiDealManualUnsubscriptionPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public ISysConfProvider SystemConfig
        {
            get { return ProviderFactory.Instance().GetConfig(); }
        }

        public string theEmail
        {
            get { return Request.QueryString["email"]; }
            set { theEmail = value; }
        }

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        public int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(User.Identity.Name);
            }
        }

        public void ShowTheSuccess()
        {
            unsubscribeArea.Visible = false;
            unsubscribeOK.Visible = true;
            ADPIC.Visible = true;
        }

        public void ShowTheError()
        {
            unsubscribeArea.Visible = false;
            unsubscribeFailed.Visible = true;
            ADPIC.Visible = true;
        }

        public void ShowTheUnSubscribe()
        {
            unsubscribeArea.Visible = true;
            ADPIC.Visible = true;
            unsubscribeOK.Visible = false;
        }

        #endregion

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!_presenter.OnViewInitialized())
                {
                    ShowTheError();
                }
            }
            else
            {
                _presenter.OnViewLoaded(); 
            }

        }

        protected void btnUnSubscribe_Click(object sender, EventArgs e)
        {
            string email = Request["email"];
            if (UnSubscribe != null)
                UnSubscribe(this, new DataEventArgs<string>(email));
        }

        #endregion endpage
    }

}
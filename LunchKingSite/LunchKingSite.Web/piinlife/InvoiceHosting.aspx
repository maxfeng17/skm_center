﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvoiceHosting.aspx.cs" Inherits="LunchKingSite.Web.piinlife.InvoiceHosting"
MasterPageFile="~/piinlife/hideal.Master" Title="電子發票" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptContent" runat="server">
    <link rel="stylesheet" type="text/css" href='<%#ResolveUrl("~/Themes/PCweb/HighDeal/HDA1.css")%>' />
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="MainContent">
<link rel="stylesheet" href="../Themes/HighDeal/images/style/blueprint/screen.css" type="text/css" media="screen, projection" />
<link href="../Themes/HighDeal/images/AboutPolicy/AboutPolicy.css" rel="stylesheet" type="text/css">

<div class="wrap">

<section class="AboutPolicy">

<article class="AbInvoiceHosting">
<h1>發票託管流程說明</h1>
<hr width="840px" class="Line"></hr>
  <div class="AbInvoiceDetail">   
      <p>根據財政部令 台財稅字第0952400194號之「電子發票實施作業要點」，於品生活憑證使用完畢、商品購買完成後開立之二聯式統一發票將以託管方式，不主動寄送，若需發票正本，請來函來電客服中心索取，或至「訂單查詢」索取，品生活亦會將發票號碼上傳至政府平台及「訂單查詢」中，供讀者查閱。</p>
    <ul class="AbInvotext">
      <li>核准機關：財政部臺北市國稅局中南稽徵所</li>
      <li>核准文號：財北國稅中南營業一字第0990019967號</li>
    </ul>
    <div class="InvoiceProcessImg"></div>
  </div>
  <br/>
  
  <div class="AbInvoiceDetail">
  <h1><label>●</label>發票狀態說明</h1>
  <hr width="840px" class="Line"></hr>   
      <p>在您憑證使用完畢、商品購買完成後，品生活就會開立電子發票（三聯式發票除外），並寄MAIL通知您，您可隨時到「訂單查詢」頁查詢您的發票資料或索取正本。查詢介面請見下方圖示：</p>
      <p>1. 進入「訂單查詢」，並點擊欲查詢的訂單編號進入訂單明細內</p>
      <div class="InvoiceHostingImg">
      <img src="../Themes/HighDeal/images/AboutPolicy/AboutPolicyInvoiceHostingPic1.jpg" width="740" height="252">
      </div>
      <p>2. 進入訂單明細後，將畫面捲動至最下方，可查看到正被托管的發票明細副本。</p>
      <div class="InvoiceHostingImg">
      <img src="../Themes/HighDeal/images/AboutPolicy/AboutPolicyInvoiceHostingPic2.jpg" width="740" height="535">
      </div>
  </div>
  <br/>
  <div class="AbInvoiceDetail">
  <h1><label>●</label>電子發票作業重點說明</h1>
  <hr width="840px" class="Line"></hr>  
    <ul class="ElectronicInvoice">
    <li>所有二聯式發票（即未打統編），皆於發票開立後，以 E-MAIL 方式通知客戶，並於訂單查詢頁呈現發票檔，如您有需要，可於收到商品後，至
    訂單查詢並按下索取紙本發票按扭，填妥收件人姓名╱地址等資料後，即可索取發票正本，預計到貨後約10個工作天以平信寄出發票。</li>
    <li>電子發票說明：電子發票之效力與實體發票相同，且有下列好處，為響應環保，請多多使用，減少紙本發票的索取。</li>
      <ul class="AbInvoExplain">
       <li>a.系統將會自動為您核對中獎，中獎後會立即以掛號方式寄送發票給您兌換獎金。</li>
       <li>b.系統會為您保留消費資料。</li>
       <li>c.退貨時，不用寄送發票來作廢，系統可自動處理，退款可加速完成。</li>
      </ul>
    <li>如您於訂購時選擇將發票捐贈，將無法查詢發票內容。依據法令規定，已捐贈的發票無法索回，如有退換貨需求，品生活將會作廢該發票。</li>
    <li>捐贈發票之實際受贈單位以開立發票當時受贈單位為主。</li>
    <li>三聯式發票(有打統編、抬頭者)或申請正本發票，將會於出貨後10個工作天依索取發票時所提供的發票寄送地址來寄送，約10個工作天寄出。
    <br/><span style="color:#b37b53; font-family: '新細明體';">注意:若您當初於訂購時已選擇三聯式發票，訂單成立後就無法更改為二聯式發票，當初已選擇二聯式發票亦無法變更為三聯式發票</span>，請於消費
    時確認應取得二聯（個人消費）或三聯（報帳用）發票，本公司已盡告知義務，且為配合國稅局勸止二聯換開三聯之政策，本公司有權利考量各
    因素後拒絕換開發票。若誤選請盡速取消訂單重新訂購。</li>
    <li>有關三聯式發票之退貨，為求雙方報稅資料之作業單純化，一律採銷折單方式處理，請加蓋"公司發票章"後寄回。此外，為避免嚴重影響您的退
    款，請於消費時確認發票開立屬性是個人消費或公司消費。</li>
    <li>每逢單月26日系統將自動對獎（已索取╱已捐贈╱已作廢發票除外），並寄發票中獎通知mail通知您，收到通知再請您依照信件步驟進入「統一
    發票中獎確認」頁中填寫您要寄送發票的地址等資訊，我們將以掛號寄至您提供的發票寄送地址。</li>
    <li>倘若發票寄送地址及相關聯絡資料因消費者填寫不完整或錯誤，導致發票無法寄送或超過兌獎期間內送達，本公司將不受理補償及其他相關賠償
    責任。</li>
   </ul>
  </div>
  <br/>
  <div class="AbInvoiceDetail">
  <h1><label>●</label>節錄「電子發票實施作業要點」說明之</h1>
  <hr width="840px" class="Line"></hr>   
    <p>根據財政部令 台財稅字第0952400194號 訂定之「電子發票實施作業要點」之第十九條：
營業人於網路上銷售貨物或勞務予非營 業人且使用電子發票者，應於網頁明顯處提供整合平台網站連結，並於交易完成前將下列事項告知買受人，提供買受人接受電子發票或索取電子發票之選擇機制…（以下節略）</p>
    <p>本作業辦法，全文請見電子發票作業平台公告之內文：「<a href="<%=sysConf.InvoiceKeyPointUrl%>" target="_blank">電子發票實施作業要點</a>」</p>
  </div>
   
</article>
</section>

</div><!--wrap-->
</asp:Content>

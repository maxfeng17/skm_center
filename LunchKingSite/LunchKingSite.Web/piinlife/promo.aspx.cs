﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using System.Web;

namespace LunchKingSite.Web.Piinlife
{
    public partial class promo : BasePage
    {
        public bool ShowEventEmail { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["cn"]))
                pMain.ContentName = Request.QueryString["cn"];

            if (!Page.IsPostBack)
            {
                
            }
        }
    }
}
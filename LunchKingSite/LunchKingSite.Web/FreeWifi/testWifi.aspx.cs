﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.FreeWifi
{
    public partial class testWifi : System.Web.UI.Page
    {
        public string MacAddress
        {
            get
            {
                if (string.IsNullOrEmpty(Request.QueryString["mac"]))
                    return string.Empty;

                return Request.QueryString["mac"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMac.Text = MacAddress;
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WifiNightMarket.aspx.cs" Inherits="LunchKingSite.Web.FreeWifi.WifiNightMarket" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>17Life Free Wifi</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 	<meta name="viewport" content="width=device-width initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
 	<link rel="shortcut icon" href="../Themes/PCweb/images/favicon.ico" />
 	<link href="../FreeWifi/images/wifiNM.css" rel="stylesheet" type="text/css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  $(window).scroll(function () {
    var scrollVal = $('body').scrollTop();
    if(scrollVal > 420){
    	$(function(){
    		$('.wifi-footer').animate({bottom:'0px'},80);
    	});
    }else{
    	$(function(){
    		$('.wifi-footer').animate({bottom:'-80px'},80);
    	});
    }
  });
});
</script>

</head>
<body>
    <form id="form1" runat="server">
	<div class="wifi-center">
		<div class="wifi-promo">
			<h1 class="text-title low">17Life APP</h1>
			<p class="pink-color">馬上下載安裝並開啟17LifeApp</p>
			<p class="pink-color">即可免費使用Wifi上網</p>
			<img src="../FreeWifi/images/iphone6.png">
		</div>
		<div class="wifi-download">
			<a href="https://itunes.apple.com/tw/app/id543439591?mt=8">
				<img src="../FreeWifi/images/ios-icon.png">
			</a>
			<a href="https://play.google.com/store/apps/details?id=com.uranus.e7plife&feature=search_result#?t=W251bGwsMSwxLDEsImNvbS51cmFudXMuZTdwbGlmZSJd">
				<img src="../FreeWifi/images/android-icon.png">
			</a>
			<h2 class="text-title low">體驗熟客卡</h2>
			<p class="pink-color">享受VIP級優惠</p>
			<p>全台美食餐券、旅遊住宿，吃喝玩樂3折起，宅配24H到貨。全家專區免費索取優惠。</p>
		</div>
		<div class="nmBan">
			<img src="../FreeWifi/images/nm.jpg">
		</div>
		<div class="wifi-footer">
			<p>下載安裝17LifeApp，可免費使用Wifi上網</p>
			<a href="https://itunes.apple.com/tw/app/id543439591?mt=8">
				<img src="../FreeWifi/images/ios-icon.png">
			</a>
			<a href="https://play.google.com/store/apps/details?id=com.uranus.e7plife&feature=search_result#?t=W251bGwsMSwxLDEsImNvbS51cmFudXMuZTdwbGlmZSJd">
				<img src="../FreeWifi/images/android-icon.png">
			</a>
		</div>
	</div>
    </form>
</body>
</html>

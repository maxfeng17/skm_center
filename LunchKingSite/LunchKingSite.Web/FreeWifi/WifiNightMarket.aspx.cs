﻿using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;

namespace LunchKingSite.Web.FreeWifi
{
    public partial class WifiNightMarket : System.Web.UI.Page, IWifiNightMarketView
    {
        private WifiNightMarketPresenter _presenter;
        public WifiNightMarketPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        #region Property
        public string MacAddress
        {
            get
            {
                if (string.IsNullOrEmpty(Request.QueryString["mac_address"]))
                    return string.Empty;

                return Request.QueryString["mac_address"];
            }
        }

        public string SourceIP
        {
            get
            {
                if (Request.UrlReferrer != null)
                {
                    return Request.UrlReferrer.ToString();
                }
                else
                {
                    return "";
                }
            }
        }
        #endregion Property

        //public event EventHandler<DataEventArgs<KeyValuePair<string, string>>> SetFreewifiUseLog = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    if (SourceIP == "" && MacAddress != "")
            //    {
            //        Presenter.OnViewLoaded();
            //        SetFreewifiUseLog(sender, new DataEventArgs<KeyValuePair<string, string>>(new KeyValuePair<string, string>(SourceIP, MacAddress)));
            //    }
            //}
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Vbs
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //修正線上QRCODE核銷和線上核銷系統MVC Route 模糊定義問題
            //string redirUrl = UrlHelper.GenerateUrl("Default", "Login", "", new RouteValueDictionary(), new RouteCollection(), new RequestContext(), false);
            string redirUrl = "~/vbs/login";

            Response.Redirect(redirUrl);
        }
    }
}
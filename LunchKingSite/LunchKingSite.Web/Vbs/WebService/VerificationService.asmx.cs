﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.Web.piinlife.member;
using LunchKingSite.WebLib.Models.VendorBillingSystem;
using LunchKingSite.WebLib.Component;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using System.Web;
using System.Web.Services;

namespace LunchKingSite.Web.Vbs.WebService
{

    public class WebServiceBase : System.Web.Services.WebService
    {
        protected ISerializer jsonSerialzer = ProviderFactory.Instance().GetSerializer();
        protected static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected static IVerificationProvider vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
        protected static ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        protected const int LoginTicketValidityDay = 60;
        //設定輸出格式為json格式

        protected bool CheckApiUser(string userId)
        {
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                ApiReturnObject rtnObject = new ApiReturnObject();
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonSerialzer.Serialize(rtnObject));
                return false;
            }
            return true;
        }
    }


    /// <summary>
    /// Summary description for VbsVerification
    /// </summary>
    [WebService(Namespace = "http://www.17life.com/Vbs/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class VbsVerificationService : WebServiceBase
    {
        /// <summary>
        /// QR核銷系統Login           //參考 LunchKingSite.Web.WebService.sso ContactLogin
        /// </summary>
        /// <param name="userName">使用者帳號</param>
        /// <param name="password">密碼</param>
        /// <param name="userId">API的使用者編號</param>
        /// <param name="driverId">裝置識別碼</param>
        [WebMethod]
        public void Login(string userName, string password, string userId, string driverId)
        {
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證API使用者
            if (CheckApiUser(userId) == false)
            {
                return;
            }

            //登入核銷系統驗證
            bool loginSuccess = false;
            if (string.Compare("DEMONO0001",userName , true) == 0)
            {
                #region DemoUser登入
                // loginSuccess = VbsMemberUtility.DemoUserSignIn(userName, password);
                if (string.Compare("123456", password, false) == 0)
                {
                    VbsMemberUtility.AppVbsUserSignIn(userName);
                    ApiReturnObject rtnObject = new ApiReturnObject();
                    //VbsMembership vbsMembership = mp.VbsMembershipGetByAccountId(userName);
                    VbsMembership vbsMembership = new VbsMembership();
                    vbsMembership.AccountId = "DEMONO0001";
                    vbsMembership.AccountType = (int)VbsMembershipAccountType.DemoUser;
                    vbsMembership.Id = 0;
                    vbsMembership.Name = "17Life生活館";
                    vbsMembership.HasInspectionCode = false;
                    vbsMembership.Password = "123456";
                    vbsMembership.LastLoginDate=DateTime.Now;
                    vbsMembership.LastPasswordChangedDate = DateTime.Now;

                    VbsSignInReplyData data = new VbsSignInReplyData(SignInReply.Success, vbsMembership, SingleSignOnSource.ContactDigitalIntegration, string.Empty);

                    //依據登入結果產生回傳資料
                    VbsApiSignInReplyData replyData = UserSignInManager.VbsApiSignInReplyDataGet(data, Helper.GetClientIP(), driverId, null);
                    rtnObject.Data = replyData;
                    rtnObject.Code = ApiReturnCode.Success;
                    rtnObject.Message = ApiReturnCode.Success.ToString();
                    
                    this.Context.Response.Write(jsonSerialzer.Serialize(rtnObject));
                    return;
                }
                #endregion DemoUser登入
            }
            else
            {
                #region 一般登入
                loginSuccess = VbsMemberUtility.UserSignIn(userName, password);
                if (loginSuccess)
                {
                    ApiReturnObject rtnObject = new ApiReturnObject();
                    VbsMembership vbsMembership = mp.VbsMembershipGetByAccountId(userName);
                    VbsSignInReplyData data = new VbsSignInReplyData(SignInReply.Success, vbsMembership, SingleSignOnSource.ContactDigitalIntegration, string.Empty);

                    //依據登入結果產生回傳資料
                    VbsApiSignInReplyData replyData = UserSignInManager.VbsApiSignInReplyDataGet(data, Helper.GetClientIP(), driverId, null);
                    rtnObject.Data = replyData;
                    rtnObject.Code = ApiReturnCode.Success;
                    rtnObject.Message = ApiReturnCode.Success.ToString();
                    this.Context.Response.Write(jsonSerialzer.Serialize(rtnObject));

                    ApiUserManager.ApiUsedLogSet(ApiUserManager.ApiUserGetByUserId(userId), "Vbs/Verification/Login", jsonSerialzer.Serialize(
                                                     new
                                                     {
                                                         userName = userName,
                                                         password = password,
                                                         userId = userId,
                                                         driverId = driverId
                                                     }));
                    return;
                }
                #endregion 一般登入
            }

            #region 登入失敗
            ApiReturnObject apiRtnObject = new ApiReturnObject();
            apiRtnObject.Code = ApiReturnCode.UserNoSignIn;
            apiRtnObject.Message = I18N.Phrase.LoginFailure;
            this.Context.Response.Write(jsonSerialzer.Serialize(apiRtnObject));
            #endregion 登入失敗


        }
        /// <summary>
        /// 修改密碼
        /// </summary>
        /// <param name="userName">使用者帳號</param>
        /// <param name="password">舊密碼</param>
        /// <param name="newpassword">新密碼</param>
        /// <param name="userId">API的使用者編號</param>
        /// <param name="driverId">裝置識別碼</param>
        [WebMethod]
        public void ChangePassword(string userName, string password, string newpassword, string userId, string driverId)
        {
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證API使用者
            if (CheckApiUser(userId) == false)
            {
                return;
            }

            vbsApiChangePasswordInfo info = new vbsApiChangePasswordInfo();

            string retypedNewPassword = newpassword;
            if (string.Compare(userName, "DEMONO0001", true) == 0)
            {
                #region DemoUser password check
                if (string.Compare(password, "123456", false) != 0)
                {
                    info.IsSuccess = false;
                    info.Message = I18N.Message.PasswordVerificationFailure;
                }

                if (info.IsSuccess && !string.Equals(newpassword, retypedNewPassword))
                {
                    info.IsSuccess = false;
                    info.Message = I18N.Message.PasswordInconsistent;
                }

                if (info.IsSuccess && !VbsMemberUtility.CheckPassword(newpassword))
                {
                    info.IsSuccess = false;
                    info.Message = I18N.Message.PasswordFormatRule;
                }

                #endregion DemoUser password check
            }
            else
            {
                #region  IsOriginalPasswordCorrect

                if (!VbsMemberUtility.VerifyPassword(userName, password))
                {
                    info.IsSuccess = false;
                    info.Message = I18N.Message.PasswordVerificationFailure;
                }
                #endregion IsOriginalPasswordCorrect

                #region  IsNewPasswodAndRetypedNewPasswordTheSame

                if (info.IsSuccess && !string.Equals(newpassword, retypedNewPassword))
                {
                    info.IsSuccess = false;
                    info.Message = I18N.Message.PasswordInconsistent;
                }
                #endregion IsNewPasswodAndRetypedNewPasswordTheSame

                #region  IsPasswordFormatCorrect
                if (info.IsSuccess && !VbsMemberUtility.CheckPassword(newpassword))
                {
                    info.IsSuccess = false;
                    info.Message = I18N.Message.PasswordFormatRule;
                }
                #endregion IsPasswordFormatCorrect

                #region IsChagnePassword
                if (info.IsSuccess && !VbsMemberUtility.ChangePassword(userName, password, newpassword))
                {
                    info.IsSuccess = false;
                    info.Message = I18N.Message.PasswordChangeFailure;
                }
                #endregion IsChagnePassword
            }
            //產生回傳資料
            ApiReturnObject rtnObject = new ApiReturnObject();

            rtnObject.Data = info;
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = ApiReturnCode.Success.ToString();
            this.Context.Response.Write(jsonSerialzer.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(ApiUserManager.ApiUserGetByUserId(userId), "Vbs/ChangePassword", jsonSerialzer.Serialize(
                                             new
                                             {
                                                 userName = userName,
                                                 oldPassword = password,
                                                 newPassword = newpassword,
                                                 userId = userId,
                                                 driverId = driverId
                                             }));
        }

        /// <summary>
        /// 利用ticket進行登入Vbs
        /// </summary>
        /// <param name="loginTicketId"></param>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <param name="driverId"></param>
        [WebMethod]
        public void LoginByTicket(string loginTicketId, int id, string userId, string driverId)
        {
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證API使用者
            if (CheckApiUser(userId) == false)
            {
                return;
            }
            //demoUser case
            if (id == 0)
            {
                #region DemoUserlogin
                //DemoUser Data
                VbsMembership vbsMembership = new VbsMembership();
                vbsMembership.AccountId = "DEMONO0001";
                vbsMembership.AccountType = (int)VbsMembershipAccountType.DemoUser;
                vbsMembership.Id = 0;
                vbsMembership.Name = "17Life生活館";
                vbsMembership.HasInspectionCode = false;
                vbsMembership.Password = "123456";
                vbsMembership.LastLoginDate = DateTime.Now;
                vbsMembership.LastPasswordChangedDate = DateTime.Now;

                LoginTicket loginTicket = mp.LoginTicketGetExpiration(loginTicketId, id, null, LoginTicketType.VbsLogin);

                if (loginTicket != null && loginTicket.IsLoaded)
                {
                    //登入核銷系統設定cookie
                    VbsMemberUtility.AppVbsUserSignIn(vbsMembership.AccountId);
                    //loginSuccess = VbsMemberUtility.UserSignIn(memOriginal.AccountId, memOriginal.Password);

                    ApiReturnObject rtnObject = new ApiReturnObject();

                    VbsSignInReplyData data = new VbsSignInReplyData(SignInReply.Success, vbsMembership, SingleSignOnSource.ContactDigitalIntegration, string.Empty);

                    //依據登入結果產生回傳資料
                    VbsApiSignInReplyData replyData = UserSignInManager.VbsApiSignInReplyDataGet(data, Helper.GetClientIP(), driverId, loginTicketId);
                    rtnObject.Data = replyData;
                    rtnObject.Code = ApiReturnCode.Success;
                    rtnObject.Message = ApiReturnCode.Success.ToString();
                    this.Context.Response.Write(jsonSerialzer.Serialize(rtnObject));

                    ApiUserManager.ApiUsedLogSet(ApiUserManager.ApiUserGetByUserId(userId), "vbs/LoginByTicket",
                                                 jsonSerialzer.Serialize(new { ticket = loginTicketId, Id = id, userId = userId, driverId = driverId }));
                    return;
                }
                #endregion DemoUserlogin
            }
            else
            {
                #region 一般登入byTicket
                VbsMembership memOriginal = mp.VbsMembershipGetById(id);
                LoginTicket loginTicket = mp.LoginTicketGetExpiration(loginTicketId, id,null, LoginTicketType.VbsLogin);

                if (loginTicket != null && loginTicket.IsLoaded)
                {
                    //登入核銷系統設定cookie
                    VbsMemberUtility.AppVbsUserSignIn(memOriginal.AccountId);

                    ApiReturnObject rtnObject = new ApiReturnObject();
                    VbsMembership vbsMembership = mp.VbsMembershipGetById(id);
                    VbsSignInReplyData data = new VbsSignInReplyData(SignInReply.Success, vbsMembership, SingleSignOnSource.ContactDigitalIntegration, string.Empty);

                    //依據登入結果產生回傳資料
                    VbsApiSignInReplyData replyData = UserSignInManager.VbsApiSignInReplyDataGet(data, Helper.GetClientIP(), driverId, loginTicketId);
                    rtnObject.Data = replyData;
                    rtnObject.Code = ApiReturnCode.Success;
                    rtnObject.Message = ApiReturnCode.Success.ToString();
                    this.Context.Response.Write(jsonSerialzer.Serialize(rtnObject));

                    ApiUserManager.ApiUsedLogSet(ApiUserManager.ApiUserGetByUserId(userId), "vbs/LoginByTicket",
                                                 jsonSerialzer.Serialize(new { ticket = loginTicketId, Id = id, userId = userId, driverId = driverId }));
                    return;
                }
                #endregion 一般登入byTicket
            }

            #region loginTicket無效
            ApiReturnObject apiRtnObject = new ApiReturnObject();
            apiRtnObject.Code = ApiReturnCode.UserNoSignIn;
            apiRtnObject.Message = I18N.Phrase.LoginFailure;
            this.Context.Response.Write(jsonSerialzer.Serialize(apiRtnObject));
            #endregion loginTicket無效

        }

        
        /// <summary>
        /// 品生活 & P好康
        /// </summary>
        /// <param name="loginTicketId">登入記錄Id</param>
        /// <param name="id">Vbs帳號ID</param>
        /// <param name="userId">API UserID</param>
        /// <param name="couponSerial">憑證序號</param>
        /// <param name="couponCode">憑證確認碼</param>
        [WebMethod]
        public void VerifyCheck(string loginTicketId, int id, string userId, string couponSerial, string couponCode)
        {
            if (CheckApiUser(userId) == false)
            {
                return;
            }
            //demoUser case
            if (id == 0)
            {
                //DemoUser Data
                VbsMembership vbsMembership = new VbsMembership();
                vbsMembership.AccountId = "DEMONO0001";
                vbsMembership.AccountType = (int)VbsMembershipAccountType.DemoUser;
                vbsMembership.Id = 0;
                vbsMembership.Name = "17Life生活館";
                vbsMembership.HasInspectionCode = false;
                vbsMembership.Password = "123456";
                vbsMembership.LastLoginDate = DateTime.Now;
                vbsMembership.LastPasswordChangedDate = DateTime.Now;

                LoginTicket loginTicket = mp.LoginTicketGetExpiration(loginTicketId, id,  null , LoginTicketType.VbsLogin);

                if (loginTicket != null && loginTicket.IsLoaded)
                {
                    #region loginTicket有效，確認可核銷憑證

                    bool isDuInTime = false;  //判斷Coupon是否在可核銷時限區間
                    string errMsg = "";

                    DemoData demoData = new DemoData();


                    List<VbsVerifyCouponInfo> infos = (couponSerial == "9527") ? demoData.VerifyCouponInfos.Where(x => x.TrustId == couponCode).ToList() : new List<VbsVerifyCouponInfo>();

                    #region 後續處理回傳APP資訊處理

                    var items = new List<VbsApiVerifyCouponInfo>();
                    if (infos.Count() > 0)
                    {
                        //撈取可核銷Coupon

                        #region 撈取可核銷Coupon
                        foreach (var couponinfo in infos)
                        {
                            //憑證為未使用並於有效使用期限內 即回傳檔次及憑證購買資料 供使用者確認是否進行核銷
                            if (couponinfo.VerifyState == VbsCouponVerifyState.UnUsed 
                                && couponinfo.ExchangePeriodStartTime <= DateTime.Now.Date
                                && DateTime.Now.Date <= couponinfo.ExchangePeriodEndTime.AddDays(cp.VerificationBuffer))
                            {
                                //判斷是否已過使用期限 卻仍在可核銷時限區間內
                                if (couponinfo.ExchangePeriodEndTime < DateTime.Now.Date
                                    && DateTime.Now.Date <= couponinfo.ExchangePeriodEndTime.AddDays(cp.VerificationBuffer))
                                {
                                    isDuInTime = true;
                                }

                                items.Add(new VbsApiVerifyCouponInfo
                                {
                                    DealId = couponinfo.DealId,
                                    DealName = couponinfo.DealName,
                                    CouponId = couponinfo.CouponId,
                                    CouponCode = couponCode,
                                    DealType = couponinfo.DealType,
                                    VerifyState = couponinfo.VerifyState,
                                    PayerName = couponinfo.PayerName,
                                    ExchangePeriodStartTime = ApiSystemManager.DateTimeToDateTimeString(couponinfo.ExchangePeriodStartTime),
                                    ExchangePeriodEndTime = ApiSystemManager.DateTimeToDateTimeString(couponinfo.ExchangePeriodEndTime),
                                    ReturnDateTime = (couponinfo.ModifyTime.HasValue) ? ApiSystemManager.DateTimeToDateTimeString(couponinfo.ModifyTime.Value) : "",
                                    TrustId = couponinfo.TrustId,
                                    IsDuInTime = isDuInTime
                                });
                            }
                        }
                        #endregion 撈取可核銷Coupon

                        //無可核銷憑證，取vdlModel回傳錯誤訊息
                        if (items.Count() == 0)
                        {
                            //憑證為已使用、退貨或逾期狀態
                            if (infos.First().VerifyState == VbsCouponVerifyState.Return)
                            {
                                errMsg = "此憑證已退貨\n退貨時間：" + infos.First().ModifyTime.ToString();
                            }
                            else if (infos.First().VerifyState == VbsCouponVerifyState.Verified)
                            {
                                errMsg = "此憑證已使用過了，\n核銷時間" + infos.First().ModifyTime;
                            }
                            else if (infos.First().ExchangePeriodStartTime > DateTime.Now.Date
                                || infos.First().ExchangePeriodEndTime.AddDays(cp.VerificationBuffer) < DateTime.Now.Date)
                            {
                                errMsg = "此憑證不在使用期限內。\n未開放使用，或已超過使用期限";
                            }
                        }
                    }
                    else
                    {
                        //撈不到對應的資料，無效憑證或是非本帳號所能核銷的憑證。
                        errMsg = "查無此憑證。可能為無效憑證，\n或非本店所能核銷的憑證。";
                    }

                    VbsVerifyCheckCouponReplyData vbsVerifyCheckCouponReplyData = new VbsVerifyCheckCouponReplyData();
                    ApiReturnObject rtnObject = new ApiReturnObject();

                    if (errMsg != "")
                    {
                        vbsVerifyCheckCouponReplyData.VbsApiCouponReturnType = VbsApiCouponReturnType.NoneVaileCoupon;
                        vbsVerifyCheckCouponReplyData.Message = errMsg;
                        rtnObject.Code = ApiReturnCode.Success;
                        rtnObject.Data = vbsVerifyCheckCouponReplyData;
                        this.Context.Response.Write(jsonSerialzer.Serialize(rtnObject));
                        return;
                    }

                    vbsVerifyCheckCouponReplyData.VbsApiCouponReturnType = (items.Count() == 1) ? VbsApiCouponReturnType.SingleAvailableCoupon : VbsApiCouponReturnType.MultipleAvailableCoupon;
                    vbsVerifyCheckCouponReplyData.VbsVerifyCoupons = items;
                    vbsVerifyCheckCouponReplyData.Message = string.Empty;

                    rtnObject.Code = ApiReturnCode.Success;
                    rtnObject.Data = vbsVerifyCheckCouponReplyData;
                    this.Context.Response.Write(jsonSerialzer.Serialize(rtnObject));


                    #endregion

                    return;

                    #endregion loginTicket有效，確認可核銷憑證
                }
            }
            else
            {
                VbsMembership memOriginal = mp.VbsMembershipGetById(id);
                LoginTicket loginTicket = mp.LoginTicketGetExpiration(loginTicketId, id,null, LoginTicketType.VbsLogin);

                if (loginTicket != null && loginTicket.IsLoaded)
                {
                    #region loginTicket有效，確認可核銷憑證

                    bool isDuInTime = false;  //判斷Coupon是否在可核銷時限區間
                    string errMsg = "";

                    var couponResponse =
                        VerificationFacade.GetAccessibleCoupon(memOriginal, couponSerial, couponCode, Guid.Empty);

                    List<VbsVerifyCouponInfo> infos = couponResponse
                        .Select(info => new VbsVerifyCouponInfo
                                            {
                                                //DealName = info.DealName,
                                                DealName = info.ItemName,
                                                DealId = info.DealId,
                                                DealType = info.DealType,
                                                CouponId = info.CouponSequence,
                                                ExchangePeriodStartTime = info.ExchangePeriodStartTime,
                                                ExchangePeriodEndTime = info.ExchangePeriodEndTime,
                                                VerifyState = info.VerifyState,
                                                ModifyTime = info.ModifyTime,
                                                PayerName = VerificationFacade.SetPayerNameHidden(info.BuyerName),
                                                TrustId = info.TrustId.ToString()
                                            })
                        .ToList();

                    #region 後續處理回傳APP資訊處理

                    var items = new List<VbsApiVerifyCouponInfo>();
                    if (infos.Count() > 0)
                    {
                        //撈取可核銷Coupon

                        #region 撈取可核銷Coupon
                        foreach (var couponinfo in infos)
                        {
                            //憑證為未使用並於有效使用期限內 即回傳檔次及憑證購買資料 供使用者確認是否進行核銷
                            if (couponinfo.VerifyState == VbsCouponVerifyState.UnUsed
                                && couponinfo.ExchangePeriodStartTime <= DateTime.Now.Date
                                && DateTime.Now.Date <= couponinfo.ExchangePeriodEndTime.AddDays(cp.VerificationBuffer))
                            {
                                //判斷是否已過使用期限 卻仍在可核銷時限區間內
                                if (couponinfo.ExchangePeriodEndTime < DateTime.Now.Date
                                    && DateTime.Now.Date <= couponinfo.ExchangePeriodEndTime.AddDays(cp.VerificationBuffer))
                                {
                                    isDuInTime = true;
                                }

                                items.Add(new VbsApiVerifyCouponInfo
                                {
                                    DealId = couponinfo.DealId,
                                    DealName = couponinfo.DealName,
                                    CouponId = couponinfo.CouponId,
                                    CouponCode = couponCode,
                                    DealType = couponinfo.DealType,
                                    VerifyState = couponinfo.VerifyState,
                                    PayerName = couponinfo.PayerName,
                                    ExchangePeriodStartTime = ApiSystemManager.DateTimeToDateTimeString(couponinfo.ExchangePeriodStartTime),
                                    ExchangePeriodEndTime = ApiSystemManager.DateTimeToDateTimeString(couponinfo.ExchangePeriodEndTime),
                                    ReturnDateTime = (couponinfo.ModifyTime.HasValue) ? ApiSystemManager.DateTimeToDateTimeString(couponinfo.ModifyTime.Value) : "",
                                    TrustId = couponinfo.TrustId,
                                    IsDuInTime = isDuInTime
                                });
                            }
                        }
                        #endregion 撈取可核銷Coupon

                        //無可核銷憑證，取vdlModel回傳錯誤訊息
                        if (items.Count() == 0)
                        {
                            //憑證為已使用、退貨或逾期狀態
                            if (infos.First().VerifyState == VbsCouponVerifyState.Return)
                            {
                                errMsg = "此憑證已退貨\n退貨時間：" + infos.First().ModifyTime.ToString();
                            }
                            else if (infos.First().VerifyState == VbsCouponVerifyState.Verified)
                            {
                                errMsg = "此憑證已使用過了，\n核銷時間" + infos.First().ModifyTime;
                            }
                            else if (infos.First().ExchangePeriodStartTime > DateTime.Now.Date
                                || infos.First().ExchangePeriodEndTime.AddDays(cp.VerificationBuffer) < DateTime.Now.Date)
                            {
                                errMsg = "此憑證不在使用期限內。\n未開放使用，或已超過使用期限";
                            }
                        }
                    }
                    else
                    {
                        //撈不到對應的資料，無效憑證或是非本帳號所能核銷的憑證。
                        errMsg = "查無此憑證。可能為無效憑證，\n或非本店所能核銷的憑證。";
                    }

                    VbsVerifyCheckCouponReplyData vbsVerifyCheckCouponReplyData = new VbsVerifyCheckCouponReplyData();
                    ApiReturnObject rtnObject = new ApiReturnObject();

                    if (errMsg != "")
                    {
                        vbsVerifyCheckCouponReplyData.VbsApiCouponReturnType = VbsApiCouponReturnType.NoneVaileCoupon;
                        vbsVerifyCheckCouponReplyData.Message = errMsg;
                        rtnObject.Code = ApiReturnCode.Success;
                        rtnObject.Data = vbsVerifyCheckCouponReplyData;
                        this.Context.Response.Write(jsonSerialzer.Serialize(rtnObject));
                        return;
                    }

                    vbsVerifyCheckCouponReplyData.VbsApiCouponReturnType = (items.Count() == 1) ? VbsApiCouponReturnType.SingleAvailableCoupon : VbsApiCouponReturnType.MultipleAvailableCoupon;
                    vbsVerifyCheckCouponReplyData.VbsVerifyCoupons = items;
                    vbsVerifyCheckCouponReplyData.Message = string.Empty;

                    rtnObject.Code = ApiReturnCode.Success;
                    rtnObject.Data = vbsVerifyCheckCouponReplyData;
                    this.Context.Response.Write(jsonSerialzer.Serialize(rtnObject));


                    #endregion

                    return;

                    #endregion loginTicket有效，確認可核銷憑證
                }
            }

            #region loginTicket無效，回傳LoginFailure
            ApiReturnObject apiRtnObject = new ApiReturnObject();
            apiRtnObject.Code = ApiReturnCode.UserNoSignIn;
            apiRtnObject.Message = I18N.Phrase.LoginFailure;
            this.Context.Response.Write(jsonSerialzer.Serialize(apiRtnObject));
            #endregion loginTicket無效，回傳LoginFailure

        }

        /// <summary>
        /// 針對 trustId 核銷 (cash_trust_log)
        /// </summary>
        /// <param name="loginTicketId">登入記錄Id</param>
        /// <param name="id">Vbs帳號ID</param>
        /// <param name="userId">API UserID</param>
        /// <param name="trustId">憑證ID</param>
        [WebMethod]
        public void VerifyCoupon(string loginTicketId, int id, string userId, string trustId)
        {
            if (CheckApiUser(userId) == false)
            {
                return;
            }

            bool isVerifySuccess = false;
            string replayMessage = "";

            if (id == 0)
            {
                //DemoUser Data
                VbsMembership vbsMembership = new VbsMembership();
                vbsMembership.AccountId = "DEMONO0001";
                vbsMembership.AccountType = (int)VbsMembershipAccountType.DemoUser;
                vbsMembership.Id = 0;
                vbsMembership.Name = "17Life生活館";
                vbsMembership.HasInspectionCode = false;
                vbsMembership.Password = "123456";
                vbsMembership.LastLoginDate = DateTime.Now;
                vbsMembership.LastPasswordChangedDate = DateTime.Now;

                LoginTicket loginTicket = mp.LoginTicketGetExpiration(loginTicketId, id,null, LoginTicketType.VbsLogin);

                if (loginTicket != null && loginTicket.IsLoaded)
                {
                    #region     loginTicket有效，進行核銷動作
                    DemoData demoData = new DemoData();


                    VerificationStatus status;
                    if (demoData.VerifyResultMsg.Where(x => x.Key == trustId).Count() > 0)
                        status = demoData.VerifyResultMsg.Where(x => x.Key == trustId).FirstOrDefault().Value;
                    else
                        status = VerificationStatus.CouponOk;

                    switch (status)
                    {
                        case VerificationStatus.CouponOk:
                            isVerifySuccess = true;
                            replayMessage = "已核銷成功，請繼續下一筆核銷。";
                            break;
                        case VerificationStatus.CouponUsed:
                            isVerifySuccess = false;
                            replayMessage = "此憑證已使用過了，\n核銷時間：" + DateTime.Now; //demo 用不到
                            break;
                        case VerificationStatus.CouponReted:
                            isVerifySuccess = false;
                            replayMessage = "此憑證已退貨\n退貨時間：" + DateTime.Now; //demo 用不到
                            break;
                        case VerificationStatus.CouponError:
                        case VerificationStatus.CashTrustLogErr:
                            isVerifySuccess = false;
                            replayMessage = "系統繁忙中，請30秒後再試一次。";
                            break;
                        case VerificationStatus.CashTrustStatusLogErr:
                            default:
                            isVerifySuccess = false;
                            replayMessage = "目前網路品質欠佳，請檢查您的網路連線，或稍後再試一次。";
                            break;
                    }

                    var model = new { IsSuccess = isVerifySuccess, ReplyMessage = replayMessage };

                    ApiReturnObject rtnObject = new ApiReturnObject();
                    rtnObject.Code = ApiReturnCode.Success;
                    rtnObject.Data = model;


                    this.Context.Response.Write(jsonSerialzer.Serialize(rtnObject));
                    
                    return;
                    #endregion loginTicket有效，進行核銷動作
                }
            }
            else
            {
                VbsMembership memOriginal = mp.VbsMembershipGetById(id);
                LoginTicket loginTicket = mp.LoginTicketGetExpiration(loginTicketId, id,null, LoginTicketType.VbsLogin);

                if (loginTicket != null && loginTicket.IsLoaded)
                {
                    #region     loginTicket有效，進行核銷動作

                    VerificationStatus status = OrderFacade.VerifyCoupon(new Guid(trustId), false, memOriginal.AccountId,
                                                                             Helper.GetClientIP(), "");
                    CashTrustLog cashTrustLog ;
                    switch (status)
                    {
                        case VerificationStatus.CouponOk:
                            isVerifySuccess = true;
                            replayMessage = "已核銷成功，請繼續下一筆核銷。";
                            break;
                        case VerificationStatus.CouponUsed:
                            cashTrustLog = mp.CashTrustLogGet(new Guid(trustId));
                            isVerifySuccess = false;
                            replayMessage = "此憑證已使用過了，\n核銷時間：" + cashTrustLog.ModifyTime;
                            break;
                        case VerificationStatus.CouponReted:
                            cashTrustLog = mp.CashTrustLogGet(new Guid(trustId));
                            isVerifySuccess = false;
                            replayMessage = "此憑證已退貨\n退貨時間：" + cashTrustLog.ModifyTime;
                            break;
                        case VerificationStatus.CouponError:
                        case VerificationStatus.CashTrustLogErr:
                        case VerificationStatus.CashTrustStatusLogErr:
                        default:
                            isVerifySuccess = false;
                            replayMessage = "系統繁忙中，請30秒後再試一次。";
                            break;
                    }

                    var model = new { IsSuccess = isVerifySuccess, ReplyMessage = replayMessage };

                    ApiReturnObject rtnObject = new ApiReturnObject();
                    rtnObject.Code = ApiReturnCode.Success;
                    rtnObject.Data = model;


                    this.Context.Response.Write(jsonSerialzer.Serialize(rtnObject));
                    return;

                    #endregion loginTicket有效，進行核銷動作
                }
            }

            #region    loginTicket無效
            ApiReturnObject apiRtnObject = new ApiReturnObject();
            apiRtnObject.Code = ApiReturnCode.UserNoSignIn;
            apiRtnObject.Message = I18N.Phrase.LoginFailure;
            this.Context.Response.Write(jsonSerialzer.Serialize(apiRtnObject));
            #endregion loginTicket無效
        }


        /// <summary>
        /// 品生活 & P好康
        /// </summary>
        /// <param name="loginTicketId">登入記錄Id</param>
        /// <param name="id">Vbs帳號ID</param>
        /// <param name="userId">API UserID</param>
        /// <param name="couponSerial">憑證序號</param>
        /// <param name="couponCode">憑證確認碼</param>
        [WebMethod]
        public void APPVerifyCheck(string loginTicketId, int id, string userId, string couponSerial, string couponCode)
        {
            if (CheckApiUser(userId) == false)
            {
                return;
            }

            LoginTicket loginTicket = mp.LoginTicketGetExpiration(loginTicketId, id, null, LoginTicketType.VbsLogin);
            
            if (loginTicket != null && loginTicket.IsLoaded)
            {
                IEnumerable<VbsVerifyCouponInfo> infos;

                #region loginTicket有效，確認可核銷憑證

                string errMsg = string.Empty;
                    
                //demoUser case
                if (id == 0)
                {
                    DemoData demoData = new DemoData();
                    infos = (couponSerial == "9527") ? demoData.VerifyCouponInfos.Where(x => x.TrustId == couponCode).ToList() : new List<VbsVerifyCouponInfo>();
                }
                else
                {

                    VbsMembership memOriginal = mp.VbsMembershipGetById(id);
                    infos = VerificationFacade.GetAccessibleCoupon(memOriginal, couponSerial, couponCode, Guid.Empty)
                        .Select(info => new VbsVerifyCouponInfo
                                            {
                                                DealName = info.ItemName,
                                                DealId = info.DealId,
                                                DealType = info.DealType,
                                                CouponId = info.CouponSequence,
                                                CouponCode = info.CouponCode,
                                                ExchangePeriodStartTime = info.ExchangePeriodStartTime,
                                                ExchangePeriodEndTime = info.ExchangePeriodEndTime,
                                                VerifyState = info.VerifyState,
                                                ModifyTime = info.ModifyTime,
                                                PayerName = VerificationFacade.SetPayerNameHidden(info.BuyerName),
                                                TrustId = info.TrustId.ToString()
                                            })
                        .ToList();
                }

                #region 後續處理回傳APP資訊處理

                var items = new List<VbsApiVerifyCouponInfo>();
                if (infos.Any())
                {                    
                    #region 撈取Coupon info
                    //檢查憑證核銷狀態 並回傳檢查憑證狀態結果
                    infos = VerificationFacade.GetVbsAccessibleCoupon(infos);
                    foreach (var couponinfo in infos)
                    {
                        items.Add(new VbsApiVerifyCouponInfo
                        {
                            DealId = couponinfo.DealId,
                            DealName = couponinfo.DealName,
                            CouponId = couponinfo.CouponId,
                            CouponCode = couponinfo.CouponCode,
                            DealType = couponinfo.DealType,
                            VerifyState = couponinfo.VerifyState,
                            PayerName = couponinfo.PayerName,
                            ExchangePeriodStartTime = ApiSystemManager.DateTimeToDateTimeString(couponinfo.ExchangePeriodStartTime),
                            ExchangePeriodEndTime = ApiSystemManager.DateTimeToDateTimeString(couponinfo.ExchangePeriodEndTime),
                            ReturnDateTime = (couponinfo.ModifyTime.HasValue) ? ApiSystemManager.DateTimeToDateTimeString(couponinfo.ModifyTime.Value) : "",
                            TrustId = couponinfo.TrustId,
                            IsDuInTime = couponinfo.IsDuInTime,
                            IsCouponAvailable = couponinfo.IsCouponAvailable,
                            ErrMessage = couponinfo.Message
                        });
                    }

                    #endregion 撈取Coupon info
                }
                else
                {
                    //撈不到對應的資料，無效憑證或是非本帳號所能核銷的憑證。
                    errMsg = "查無此憑證。可能為無效憑證，\n或非本店所能核銷的憑證。";
                }

                VbsVerifyCheckCouponReplyData vbsVerifyCheckCouponReplyData = new VbsVerifyCheckCouponReplyData();
                ApiReturnObject rtnObject = new ApiReturnObject();

                if (string.IsNullOrEmpty(errMsg))
                {
                    vbsVerifyCheckCouponReplyData.VbsApiCouponReturnType = VbsApiCouponReturnType.NoneVaileCoupon;
                }
                else
                {
                    vbsVerifyCheckCouponReplyData.VbsApiCouponReturnType = (items.Count() == 1) ? VbsApiCouponReturnType.SingleAvailableCoupon : VbsApiCouponReturnType.MultipleAvailableCoupon;
                }
                
                vbsVerifyCheckCouponReplyData.VbsVerifyCoupons = items;
                vbsVerifyCheckCouponReplyData.Message = errMsg;

                rtnObject.Code = ApiReturnCode.Success;
                rtnObject.Data = vbsVerifyCheckCouponReplyData;
                this.Context.Response.Write(jsonSerialzer.Serialize(rtnObject));


                #endregion 後續處理回傳APP資訊處理

                return;

                #endregion loginTicket有效，確認可核銷憑證
            }

            #region loginTicket無效，回傳LoginFailure
            ApiReturnObject apiRtnObject = new ApiReturnObject();
            apiRtnObject.Code = ApiReturnCode.UserNoSignIn;
            apiRtnObject.Message = I18N.Phrase.LoginFailure;
            this.Context.Response.Write(jsonSerialzer.Serialize(apiRtnObject));
            #endregion loginTicket無效，回傳LoginFailure

        }

        /// <summary>
        /// 針對 trustId 核銷 (cash_trust_log)
        /// </summary>
        /// <param name="loginTicketId">登入記錄Id</param>
        /// <param name="id">Vbs帳號ID</param>
        /// <param name="userId">API UserID</param>
        /// <param name="trustId">憑證ID</param>
        [WebMethod]
        public void APPVerifyCoupon(string loginTicketId, int id, string userId, string trustId)
        {
            if (CheckApiUser(userId) == false)
            {
                return;
            }

            LoginTicket loginTicket = mp.LoginTicketGetExpiration(loginTicketId, id, null, LoginTicketType.VbsLogin);
            VerificationStatus status;

            //demoUser case
            if (id == 0)
            {
                DemoData demoData = new DemoData();

                if (demoData.VerifyResultMsg.Where(x => x.Key == trustId).Count() > 0)
                {
                    status = demoData.VerifyResultMsg.Where(x => x.Key == trustId).FirstOrDefault().Value;
                }
                else
                {
                    status = VerificationStatus.CouponOk;
                }
            }
            else
            {
                VbsMembership memOriginal = mp.VbsMembershipGetById(id);
                status = OrderFacade.VerifyCoupon(new Guid(trustId), false, memOriginal.AccountId,
                                                                            Helper.GetClientIP(), "");
            }

            if (loginTicket != null && loginTicket.IsLoaded)
            {
                #region     loginTicket有效，進行核銷動作

                VbsVerifyResult verifyResult = VerificationFacade.GetVbsVerifyResult(status, trustId);

                var model = new { IsSuccess = verifyResult.IsVerifySuccess, ReplyMessage = verifyResult.Message };

                ApiReturnObject rtnObject = new ApiReturnObject();
                rtnObject.Code = ApiReturnCode.Success;
                rtnObject.Data = model;


                this.Context.Response.Write(jsonSerialzer.Serialize(rtnObject));
                return;

                #endregion loginTicket有效，進行核銷動作
            }

            #region    loginTicket無效
            ApiReturnObject apiRtnObject = new ApiReturnObject();
            apiRtnObject.Code = ApiReturnCode.UserNoSignIn;
            apiRtnObject.Message = I18N.Phrase.LoginFailure;
            this.Context.Response.Write(jsonSerialzer.Serialize(apiRtnObject));
            #endregion loginTicket無效
        }
    }

    /// <summary>
    /// for mobile app trasnfer Vbs VerifyCoupon data
    /// </summary>
    public class VbsVerifyCheckCouponReplyData
    {
        public VbsApiCouponReturnType VbsApiCouponReturnType { get; set; }
        public IEnumerable<VbsApiVerifyCouponInfo> VbsVerifyCoupons { get; set; }
        public string Message { get; set; }

        public VbsVerifyCheckCouponReplyData()
        {
            this.VbsApiCouponReturnType = VbsApiCouponReturnType.NoneVaileCoupon;
            this.VbsVerifyCoupons = null;
        }
    }

    /// <summary>
    /// 可核銷憑證資訊加一個已過使用期限卻仍在可核銷時限區間內的Boolean值
    /// </summary>
    public class VbsApiVerifyCouponInfo
    {
        public string DealName { get; set; }
        public string DealId { get; set; }
        public BusinessModel DealType { get; set; }
        public string CouponId { get; set; }
        public string CouponCode { get; set; }
        public string ExchangePeriodStartTime { get; set; }
        public string ExchangePeriodEndTime { get; set; }
        public VbsCouponVerifyState VerifyState { get; set; }
        public string ReturnDateTime { get; set; }
        public string PayerName { get; set; }
        public string TrustId { get; set; }
        public Boolean IsDuInTime { get; set; }
        public Boolean IsCouponAvailable { get; set; }
        public string ErrMessage {get;set;}

    }

    public class vbsApiChangePasswordInfo
    {
        public bool IsSuccess;
        public string Message;

        public vbsApiChangePasswordInfo()
        {
            this.IsSuccess = true;
            this.Message = "";

        }
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PezExample.aspx.cs" Inherits="LunchKingSite.Web.PezExample" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://www.payeasy.com.tw/static/gateway/js/ui.js"></script>
    <script>
        $(document).ready(function () {

        var mem = null;
        if ($('#memNum').val() != '')
            mem = $('#memNum').val();

        $('#point').renderUi({
          memNum: mem,                                  // 加密的顧客流水號 (AES)
          clientId: 'testPay',                          // PayEasy的ID(CAS使用)
          clientSecret: 'kd8c73j7llP',                  // PayEasy的串接碼(CAS使用)
          origin: '<%=config.SSLSiteUrl%>' + '/',       // 廠商首頁網址 https://www.17life.com/ or https://shopping.pchome.com.tw/
          merchantId: 'M0025',                          // 廠商編碼PChome:M0025;17Life:M0026
          merchantContent: $('#merchantContent').val(), // 廠商交易碼(每次交換均不同) + 加密的轉換點數上限(AES)2000
          bindingCallback: function(memNum, callback) {
            console.log('請使用ajax呼叫貴公司api進行會員綁定', memNum)
            callback({
              success: true
            })
          },
          successCallback: function(response, callback) {
            //mouse modify
            console.log('請使用ajax呼叫貴公司api進行頁面點數儲值與重新顯示', response.returnCode, response.deductContent)
            console.log('response', response)
            callback({
              success: true
            })
          }
        })
      })
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <asp:textbox runat="server" ID="merchantContent"></asp:textbox>
        <asp:textbox runat="server" ID="memNum"></asp:textbox>
        <a id="point" href="javascript:void(0)">使用PayEasy購物金</a>
    </div>
    </form>
</body>
</html>


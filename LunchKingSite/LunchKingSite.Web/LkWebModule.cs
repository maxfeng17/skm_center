﻿using System.ComponentModel.Composition;
using Autofac;
using Vodka.Container;
using Vodka.Mongo;

namespace LunchKingSite.Web
{
    [Export(typeof(IModuleRegistrar))]
    public class LkWebModule : IModuleRegistrar
    {
        public void RegisterWithContainer(ContainerBuilder builder)
        {
            
        }

        public void Initialize(IContainer container)
        {
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BookingSystem/BookingSystem.Master" AutoEventWireup="true" CodeBehind="ReservationRecords.aspx.cs" Inherits="LunchKingSite.Web.BookingSystem.ReservationRecords" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CSS" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SMC" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hfMemberKey" ClientIDMode="Static" runat="server" />
    <div class="bok-wrap">
        <div class="ly-booking-box">
            <div class="bok-logo">
                <img src="../Themes/Booking/lifeLogo.png" width="400" height="150" />
            </div>
            <div class=" bok-infotop ">
                <strong>預約記錄</strong>
                <asp:DropDownList ID="ddlIsExpiredReservation1" AutoPostBack="true" runat="server" CssClass="box-rdl-select  bok-rdl-topdisappear  bok-rdl-appear" OnSelectedIndexChanged="ddlIsExpired_SelectedIndexChanged">
                    <asp:ListItem Text="未過期的預約" Value="0" />
                    <asp:ListItem Text="已過期的預約" Value="1" />
                </asp:DropDownList>
            </div>
            <div class=" bok-infotop clearfix">

                <div class="bok-lablebox bok-lablebox-top2">
                    <label class="bok-lab bk-red">提醒您：</label>
                </div>
                <div class="bok-lablebox bok-lablebox-top2">
                    <label class="bok-lab bk-red">
                        為維護您與店家的權益，請依照預約時間到達；如餐廳預約，預約僅保留10分鐘。<br />
                        預約未到且未取消，如需再次預約，請主動與店家聯繫。</label>
                </div>

            </div>

            <div class="bok-infobelow  clearfix">
                <div class="bok-lablebox bok-lablebox-top2">
                    <div class="bok-lab-selt bok-rdl-disappear bok-rdl-appear">
                        篩選：
                        <asp:RadioButtonList ID="rdlExpiredReservation" RepeatLayout="Flow" AutoPostBack="true" RepeatColumns="2" OnSelectedIndexChanged="rdlExpiredReservation_SelectedIndexChanged" runat="server">
                            <asp:ListItem Text="未過期預約" Value="0" Selected="True" />
                            <asp:ListItem Text="已過期預約" Value="1" />
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="bok-lablebox bok-lablebox-top2">
                    <asp:Repeater ID="rpt_ReservationRecords" runat="server" OnItemDataBound="rpt_ReservationRecords_ItemDataBound" OnItemCommand="rpt_ReservationRecords_ItemCommand" >
                        <HeaderTemplate>
                            <table class="bok-detail" border="0" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr class="bok-title bok-rdl-disappear">
                                    <td colspan="6" >
                                        <table class="bok-tbox" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="bok-list-bbox">預約日期及時間</td>
                                                <td class="bok-list-bbox bok-list-mbox">預約人數</td>
                                                <td class="bok-list-bbox bok-list-mbox">憑證編號</td>
                                                <td class="bok-list-bbox">店家資訊及優惠內容</td>
                                                <td class="bok-list-bbox bok-list-sbox">備註</td>
                                                <td class="bok-list-bbox bok-list-mbox">取消預約</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </thead>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tbody>
                                <tr class="<%# (((ViewBookingSystemReservationRecord)(Container.DataItem)).IsCancel) ? "bok-bgcor-gary" : Container.ItemIndex % 2 == 0 ? "bok-tdcolor" : string.Empty%>">
                                    <td colspan="6">
                                        <table class="bok-tbox" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="bok-list-bbox">
                                                    <span class="bok-rd-text<%# (((ViewBookingSystemReservationRecord)(Container.DataItem)).IsCancel) ? string.Empty : " bk-red"%>">預約日期及時間：</span>
                                                    <%# ((ViewBookingSystemReservationRecord)(Container.DataItem)).ReservationDate %>
                                                    (<%# GetDayOfWeek(((DateTime)((ViewBookingSystemReservationRecord)(Container.DataItem)).ReservationDateTime).DayOfWeek) %>)
                                                    <%# ((ViewBookingSystemReservationRecord)(Container.DataItem)).TimeSlot %>
                                                </td>
                                                <td class="bok-list-mbox">
                                                    <span class="bok-rd-text<%# (((ViewBookingSystemReservationRecord)(Container.DataItem)).IsCancel) ? string.Empty : " bk-red"%>">訂位人數：</span>
                                                    <%# ((ViewBookingSystemReservationRecord)(Container.DataItem)).NumberOfPeople %>人
                                                </td>
                                                <td class="bok-list-mbox">
                                                    <span class="bok-rd-longtext<%# (((ViewBookingSystemReservationRecord)(Container.DataItem)).IsCancel) ? string.Empty : " bk-red"%>">憑證編號：</span>
                                                    <asp:Literal ID="litCoupons" runat="server"></asp:Literal>
                                                </td>
                                                <td class="bok-list-bbox">
                                                    <span class="bok-rd-longtext<%# (((ViewBookingSystemReservationRecord)(Container.DataItem)).IsCancel) ? string.Empty : " bk-red"%>">店家資訊及優惠內容：</span>
                                                    <a href="<%# ((ViewBookingSystemReservationRecord)(Container.DataItem)).CouponLink %>" target="_blank">
                                                        <%# ((ViewBookingSystemReservationRecord)(Container.DataItem)).CouponInfo %>
                                                    </a>
                                                </td>
                                                <td class="bok-list-sbox">
                                                    <span class="bok-rd-text<%# (((ViewBookingSystemReservationRecord)(Container.DataItem)).IsCancel) ? string.Empty : " bk-red"%>">備註：</span>
                                                    <%# ((ViewBookingSystemReservationRecord)(Container.DataItem)).Remark %>
                                                </td>
                                                <td class="bok-list-mbox<%# ((ViewBookingSystemReservationRecord)(Container.DataItem)).IsCancel ? " bok-textcen" : (((ViewBookingSystemReservationRecord)(Container.DataItem)).ReservationDate == System.DateTime.Now.ToString("yyyy/MM/DD")) ? " bok-textcen rd-orgtt bk-red" : " bok-textcen" %>">
                                                    <asp:Literal ID="litReservationCancelNote" runat="server"></asp:Literal>
                                                    <asp:Button ID="btnCancelReservation" CommandName="CancelReservation" OnClientClick="return confirm('是否確定要取消?');" CommandArgument="<%# ((ViewBookingSystemReservationRecord)(Container.DataItem)).Id %>" class="bok-btn bok-canceltn bok-rd-left" runat="server" Text="取消預約" Visible="false" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>

        </div>

        <img class="bok-bg" src="https://www.17life.com/images/17P/20130926-booking/bg.jpg" width="1500" height="1300" />
    </div>
</asp:Content>

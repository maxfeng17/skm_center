﻿using Autofac;
using LunchKingSite.WebLib;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Mongo.Services;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.WebLib.Models.Mobile;

namespace LunchKingSite.Web.BookingSystem
{
    public partial class BookingSystem : System.Web.UI.MasterPage
    {
        public string Rsrc
        {
            get
            {
                if (Page.RouteData.Values["rsrc"] != null)
                {
                    return Page.RouteData.Values["rsrc"].ToString();
                }
                else if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["rsrc"]))
                {
                    return HttpContext.Current.Request.QueryString["rsrc"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public bool BypassSetNowUrl { get; set; }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!BypassSetNowUrl)
            {
                SetNowUrlIntoSession();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        protected void SM1_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
        {
            WebUtility.LogExceptionAnyway(e.Exception);
        }
        /// <summary>
        /// 將目前網址寫到Session中
        /// </summary>
        protected void SetNowUrlIntoSession()
        {
            string url = Request.Url.AbsoluteUri;
            if (url.ToLower().IndexOf("newmember/") < 0)
            {
                Session[LkSiteSession.NowUrl.ToString()] = url;
            }
        }
    }
}
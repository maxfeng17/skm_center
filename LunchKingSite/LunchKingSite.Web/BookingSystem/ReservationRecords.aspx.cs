﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using Newtonsoft.Json.Linq;

namespace LunchKingSite.Web.BookingSystem
{
    public partial class ReservationRecords : BasePage, IBookingSystemReservationRecordsView
    {
        #region property

        private BookingSystemReservationRecordsPresenter _presenter;

        public BookingSystemReservationRecordsPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }

        public bool IsExpiredRecord 
        {
            get 
            {
                return (int.Parse(ddlIsExpiredReservation1.SelectedValue) == (int)BookingSystemRecordType.Expired) ? true : false;
            }
            set 
            {
                ddlIsExpiredReservation1.SelectedValue = rdlExpiredReservation.SelectedValue = (value) ? Convert.ToString((int)BookingSystemRecordType.Expired) : Convert.ToString((int)BookingSystemRecordType.Unexpired);
            }
        }

        public string ApiKey
        {
            get
            {
                return (String.IsNullOrEmpty(Request.QueryString["ApiKey"])) ? string.Empty : Request.QueryString["ApiKey"];
            }
        }

        public string MemberKey
        {
            set { hfMemberKey.Value = value; }
            get { return hfMemberKey.Value; }
        }

        #endregion

        #region event

        public event EventHandler<DataEventArgs<int>> IsExpiredReservationChange = null;
        public event EventHandler<DataEventArgs<int>> CancelReservation = null;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
            }
            this.Presenter.OnViewLoaded();
        }

        public void RedirectToLogin()
        {
            FormsAuthentication.RedirectToLoginPage();
        }

        protected void ddlIsExpired_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.IsExpiredReservationChange != null)
            {
                this.IsExpiredReservationChange(sender, new DataEventArgs<int>(int.Parse(((DropDownList)sender).SelectedValue)));
            }
        }
        
        protected void rdlExpiredReservation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.IsExpiredReservationChange != null)
            {
                this.IsExpiredReservationChange(sender, new DataEventArgs<int>(int.Parse(((RadioButtonList)sender).SelectedValue)));
            }
        }

        public void BindReservationRecord(ViewBookingSystemReservationRecordCollection ReservationRecords) 
        {
            rpt_ReservationRecords.DataSource = ReservationRecords;
            rpt_ReservationRecords.DataBind();
        }

        protected void rpt_ReservationRecords_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewBookingSystemReservationRecord)
            {
                ViewBookingSystemReservationRecord record = (ViewBookingSystemReservationRecord)e.Item.DataItem;
                Literal coupons = (Literal)e.Item.FindControl("litCoupons");
                Literal ReservationCancelNote = (Literal)e.Item.FindControl("litReservationCancelNote");

                BookingSystemCouponCollection CouponCollection = BookingSystemFacade.GetBookingSystemCouponsByReservationId(record.Id);
                coupons.Text = string.Join("、", CouponCollection.Select(x => x.Prefix + x.CouponSequenceNumber));

                if (record.IsCancel)
                {
                    ReservationCancelNote.Text = "已取消";
                }
                else if (record.ReservationDate == DateTime.Now.ToString("yyyy/MM/dd") && record.BookingType == (int)BookingType.Travel && record.IsLock)
                    {
                        ReservationCancelNote.Text = "如欲取消訂房，請連絡店家";
                    }
                else if (record.ReservationDateTime <= DateTime.Now) 
                {
                    //ReservationCancelNote.Text = "已過期";
                    Button cancelReservation = (Button)e.Item.FindControl("btnCancelReservation");
                    cancelReservation.Visible = true;
                }
                else
                {
                    Button cancelReservation = (Button)e.Item.FindControl("btnCancelReservation");
                    cancelReservation.Visible = true;
                }
            }
        }


        protected string GetDayOfWeek(DayOfWeek day) 
        {
            switch (day) 
            {
                case DayOfWeek.Monday:
                    return "一";
                case DayOfWeek.Tuesday:
                    return "二";
                case DayOfWeek.Wednesday:
                    return "三";
                case DayOfWeek.Thursday:
                    return "四";
                case DayOfWeek.Friday:
                    return "五";
                case DayOfWeek.Saturday:
                    return "六";
                case DayOfWeek.Sunday:
                    return "日";
                default:
                    return string.Empty;
            }
        }

        protected void rpt_ReservationRecords_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "CancelReservation")
            {
                if (this.CancelReservation != null)
                {
                    this.CancelReservation(source, new DataEventArgs<int>(int.Parse(e.CommandArgument.ToString())));
                }
            }
        }
    }
}
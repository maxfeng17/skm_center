﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Security;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using Newtonsoft.Json.Linq;
using SubSonic;


namespace LunchKingSite.Web.BookingSystem
{
    public partial class ReservationService : BasePage, IBookingSystemReservationServiceView
    {
        #region property

        private const int ValidReserveDays = 15;
        private BookingSystemReservationServicePresenter _presenter;
        public BookingSystemReservationServicePresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        private Guid? _storeGuid;
        public Guid? StoreGuid
        {
            get { return this._storeGuid; }
            set
            {
                this._storeGuid = value;
                hfStoreGuid.Value = value.ToString();
            }
        }

        public Guid? OrderGuid
        {
            get
            {
                if (!String.IsNullOrEmpty(Request.QueryString["gid"]))
                {
                    Guid oGuid;
                    return Guid.TryParse(Request.QueryString["gid"], out oGuid) ? (Guid?)oGuid : null;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                hfOrderKey.Value = value.ToString();
            }
        }

        public string ApiKey
        {
            get
            {
                return (String.IsNullOrEmpty(Request.QueryString["ApiKey"])) ? string.Empty : Request.QueryString["ApiKey"];
            }
            set
            {
                hfApiKey.Value = value;
            }
        }

        public string UserEmail
        {
            get { return litUserName.Text; }
            set { litUserName.Text = litUserName2.Text = litUserName3.Text = hfUserName.Value = value; }
        }

        public string CouponUsage
        {
            set { hfCouponUsage.Value = litCouponUsage.Text = litCouponUsage2.Text = litCouponUsage3.Text = value; }
            get { return litCouponUsage.Text; }
        }

        public string UserName
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }

        public int BookingSystemType
        {
            set { hfBookingSystemType.Value = value.ToString(); }
            get { return int.Parse(hfBookingSystemType.Value); }
        }

        public int ReservationMinDay
        {
            set { hfReservationMinDay.Value = value.ToString(); }
            get
            {
                int minDay = 0;
                return (int.TryParse(hfReservationMinDay.Value, out minDay)) ? minDay : 0;
            }
        }

        public int CouponUsers
        {
            set { hfCouponUsers.Value = value.ToString(); }
        }

        public int MemberKey
        {
            set { hfMemKey.Value = value.ToString(); }
        }

        public string StoreAddress
        {
            set { litStoreAddress.Text = litStoreAddress2.Text = value; }
        }

        public string StoreName
        {
            set { hfStoreName.Value = litStoreName.Text = litStoreName2.Text = value; }
        }

        public string StorePhoneNumber
        {
            set { litStorePhoneNumber.Text = litStorePhoneNumber2.Text = value; }
        }

        public string CouponLink
        {
            set { hfCouponLink.Value = value; }
        }

        public string FullBookingDate
        {
            set { hfFullBookingDate.Value = value; }
        }

        public string CouponCanUseDate
        {
            set { hfCouponCanUseDate.Value = value; }
        }


        #endregion

        #region Event
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
            }
            this.Presenter.OnViewLoaded();
        }

        public void SetCouponSequenceCheckBox(CashTrustLogCollection cashTrustLogCol)
        {
            cblCouponList.Items.Clear();
            cblCouponList.Items.AddRange(cashTrustLogCol.ToList().Select(x => new ListItem(x.Prefix + x.CouponSequenceNumber, x.CouponSequenceNumber)).ToArray());
            cblCouponList.Items.Cast<ListItem>().ForEach(i => i.Selected = true);
        }

        public void RedirectToLogin()
        {
            FormsAuthentication.RedirectToLoginPage();
        }

        public void SetDefaultValue(string reservationName, int? gender, string reservationPhoneNumber, Dictionary<string, string> sellers, bool IsNoRestrictedStore)
        {
            txtReservationName.Text = reservationName;

            if (gender == 1)
            {
                rdoMale.Checked = true;
            }
            else if (gender == 2)
            {
                rdoFemale.Checked = true;
            }

            txtMobile.Text = reservationPhoneNumber;

            chkNoRestrictedStore.Checked = IsNoRestrictedStore;

            ddlNoRestrictedStore.DataSource = sellers;
            ddlNoRestrictedStore.DataTextField = "Value";
            ddlNoRestrictedStore.DataValueField = "Key";
            ddlNoRestrictedStore.DataBind();
        }

        [WebMethod]
        public static string GetStoreTimeSlot(string storeGuid, string queryDate, int bookingType, int numberOfPeople)
        {
            DateTime checkDate = DateTime.Now;
            DateTime.TryParse(queryDate, out checkDate);
            int dateId = 0;
            if (BookingSystemFacade.IsOpenReservationServiceByDate((BookingType)bookingType, storeGuid, checkDate, out dateId))
            {
                var timeSlotCol = (BookingSystemFacade.GetBookingSystemTimeSlotListByBookingSystemDateId(dateId).ToList()).Where(x => x.MaxNumberOfPeople > 0).OrderBy(x => x.TimeSlot).ToList();

                for (int i = 0; i < timeSlotCol.Count; i++)
                {
                    if ((BookingSystemFacade.GetReservationPeopleOfNumberByDate(timeSlotCol.ElementAt(i).Id, checkDate) + numberOfPeople) > timeSlotCol.ElementAt(i).MaxNumberOfPeople)
                    {
                        timeSlotCol.RemoveAt(i);
                        i--;
                    }
                }
                return new JsonSerializer().Serialize(timeSlotCol);
            }
            else
            {
                return new JsonSerializer().Serialize(new BookingSystemTimeSlotCollection());
            }
        }

        [WebMethod]
        public static string MakeReservationRecord(int bookingType, int timeSlot, string reservationDate, int numberOfPeople, string remark, string contactName, bool contactGender, string contactNumber, string memberKey, string orderKey, string couponInfo, string couponLink, string apiKey, string[] couponsSequence, string email, string storeGuid, int advanceReservationDays)
        {
            DateTime reserveDate;

            MemberFacade.CashTrustLogStoreGuidSet(couponsSequence, orderKey, storeGuid);

            if (DateTime.TryParse(reservationDate, out reserveDate) )
            {
                if (DateTime.Now.Date.AddDays(advanceReservationDays > 0 ? advanceReservationDays : 1) < reserveDate && reserveDate > DateTime.Now.Date.AddDays(ValidReserveDays))
                {
                    return new JsonSerializer().Serialize(new { Success = false, Message = "預約已額滿或店家尚不接受預約!" });
                }
            }
            else
            {
                return new JsonSerializer().Serialize(new { Success = false, Message = "該店家沒有提供訂位服務!" });
            }

            int dateId;
            if (BookingSystemFacade.IsOpenReservationServiceByDate((BookingType) bookingType, storeGuid, reserveDate,
                out dateId))
            {
                if (BookingSystemFacade.GetBookingSystemTimeSlotListByBookingSystemDateId(dateId).All(x => x.Id != timeSlot))
                {
                    return new JsonSerializer().Serialize(new { Success = false, Message = "日期與時段錯誤，請重新選擇日期時段!" });
                }
            }
            else
            {
                return new JsonSerializer().Serialize(new { Success = false, Message = "預約已額滿或店家尚不接受預約!" });
            }

            if ((BookingSystemFacade.GetReservationPeopleOfNumberByDate(timeSlot, reserveDate) + numberOfPeople) > BookingSystemFacade.GetTimeSlotMaxNumberOfPeople(timeSlot))
            {
                return new JsonSerializer().Serialize(new { Success = false, Message = "該時段訂位人數已滿!" });
            }
            else
            {
                Guid guid;
                if (Guid.TryParse(storeGuid, out guid) )
                {
                    BookingSystemStore store = BookingSystemFacade.GetBookingSystemStoreByStoreGuid(guid, (BookingType)bookingType);
                    if (!BookingSystemFacade.CheckDayTypeIsOpen(store, reserveDate))
                    {
                        return new JsonSerializer().Serialize(new { Success = false, Message = "預約已額滿或店家尚不接受預約!" });
                    }

                    BookingSystemServiceName serviceName = BookingSystemFacade.GetBookingSystemServiceName(apiKey, BookingSystemApiServiceType.PponService);
                    BookingSystemMakeReservationRecordResult reservationResult = BookingSystemFacade.MakeReservationRecord((BookingType)bookingType, reserveDate, numberOfPeople, timeSlot, remark, contactName, contactGender, contactNumber, orderKey, memberKey, couponInfo, couponLink, serviceName, couponsSequence, email, guid);
                    if (reservationResult == BookingSystemMakeReservationRecordResult.Success)
                    {
                        return new JsonSerializer().Serialize(new { Success = true, Message = "訂位成功!" });
                    }
                    else if (reservationResult == BookingSystemMakeReservationRecordResult.DoubleUsingCoupon)
                    {
                        return new JsonSerializer().Serialize(new { Success = false, Message = "您選取的憑證編號有部份已訂過位!" });
                    }
                    else
                    {
                        return new JsonSerializer().Serialize(new { Success = false, Message = "訂位失敗!" });
                    }
                }
                else
                {
                    return new JsonSerializer().Serialize(new { Success = false, Message = "該店家沒有提供訂位服務!" });
                }
            }
        }

        [WebMethod]
        public static bool CheckApiKeyAndOrderId(string userName, string apiKey, string gid)
        {
            Guid guid;
            if (!Guid.TryParse(gid, out guid))
            {
                return false;
            }
            else
            {
                BookingSystemServiceName serviceName = BookingSystemFacade.GetBookingSystemServiceName(apiKey, BookingSystemApiServiceType.PponService);
                if (serviceName == BookingSystemServiceName.None)
                {
                    return false;
                }
                else if (serviceName == BookingSystemServiceName._17Life && !MemberFacade.ViewCouponListMainGetByOrderGuid(MemberFacade.GetUniqueId(userName), guid).Any())
                {
                    return false;
                }
                else if (serviceName == BookingSystemServiceName.HiDeal && !HiDealOrderFacade.CheckOrderGuidOwner(userName, guid))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        /// <summary>
        /// 取得開放預約日期期間已額滿日期字串
        /// </summary>
        /// <param name="advanceReservationMinDay"></param>
        /// <param name="storeGuid"></param>
        /// <param name="bookingType"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetFullBookingDate(int advanceReservationMinDay, string storeGuid, int bookingType)
        {
            Guid sGuid;
            if (!Guid.TryParse(storeGuid, out sGuid))
            {
                return new JsonSerializer().Serialize(new { Success = false, Message = "GUID format exception.", Data = string.Empty });
            }

            BookingSystemStore store = BookingSystemFacade.GetBookingSystemStoreByStoreGuid(sGuid, (BookingType)bookingType);
            if (!store.IsLoaded)
            {
                return new JsonSerializer().Serialize(new { Success = false, Message = "查無此店家", Data = string.Empty });
            }

            DateTime indexDay = DateTime.Now.AddDays((double)advanceReservationMinDay);
            DateTime maxDate = DateTime.Now.AddDays(15);
            List<string> fullDate = new List<string>();

            while (indexDay <= maxDate)
            {
                int dateId = 0;
                if (!BookingSystemFacade.CheckDayTypeIsOpen(store, indexDay))
                {
                    fullDate.Add(indexDay.ToString("yyyy/MM/dd"));
                }
                else if (!BookingSystemFacade.IsOpenReservationServiceByDate((BookingType)bookingType, sGuid.ToString(), indexDay, out dateId))
                {
                    fullDate.Add(indexDay.ToString("yyyy/MM/dd"));
                }
                else if (BookingSystemFacade.IsFullReservation(sGuid.ToString(), (BookingType)bookingType, indexDay))
                {
                    fullDate.Add(indexDay.ToString("yyyy/MM/dd"));
                }
                indexDay = indexDay.AddDays(1);
            }
            return new JsonSerializer().Serialize(new { Success = true, Message = "Success", Data = string.Join(",", fullDate) });
        }
    }
}
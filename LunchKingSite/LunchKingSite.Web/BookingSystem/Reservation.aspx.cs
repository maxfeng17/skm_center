﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Security;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using Newtonsoft.Json.Linq;


namespace LunchKingSite.Web.BookingSystem
{
    public partial class Reservation : BasePage, IBookingSystemReservationView
    {
        #region property

        private BookingSystemReservationPresenter _presenter;
        public BookingSystemReservationPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string ApiKey
        {
            get
            {
                return (String.IsNullOrEmpty(Request.QueryString["ApiKey"])) ? string.Empty : Request.QueryString["ApiKey"];
            }
            set
            {
                hfApiKey.Value = value;
            }
        }

        public string SellerName 
        {
            set { lblSellerName.Text = value; }
        }

        public string FullBookingDate
        {
            set { hfFullBookingDate.Value = value; }
        }

        public Guid? StoreGuid 
        {
            get 
            {
                Guid guid;
                if (Guid.TryParse(ddlStoreList.SelectedValue, out guid))
                {
                    return guid;
                }
                else 
                {
                    return null;
                }
            }
        }

        #endregion

        #region Event
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
            }
            this.Presenter.OnViewLoaded();
        }

        public void SetStoreList(ViewBookingSystemStoreCollection stores) 
        {
            foreach (ViewBookingSystemStore store in stores) 
            {
                ddlStoreList.Items.Add(new ListItem(store.StoreName, store.StoreGuid.ToString()));
            }
        }

        [WebMethod]
        public static string GetStoreInfo(string storeGuid) 
        {
            Guid guid;
            if (Guid.TryParse(storeGuid, out guid))
            {
                ViewBookingSystemStore store = BookingSystemFacade.GetBookingSystemStoreInfo(guid, BookingType.Coupon);
                return new JsonSerializer().Serialize(new { DataFormatSuccess = true, StoreName = store.StoreName, StorePhone = store.Phone, StoreTownshipId = store.TownshipId, StoreAddress = store.AddressString, StoreWebUrl = store.WebUrl });
            }
            else 
            {
                return new JsonSerializer().Serialize(new { DataFormatSuccess = false, StoreInfo = string.Empty });
            }
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BookingSystem/BookingSystem.Master" AutoEventWireup="true" CodeBehind="Reservation.aspx.cs" Inherits="LunchKingSite.Web.BookingSystem.Reservation" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CSS" runat="server">
    <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <style type="text/css">
        .ui-datepicker {
            font-size: 12px;
        }
        .ui-datepicker-header
        {
	        background: url(book-bn-bg3.jpg) 0 0 repeat-x;
	        background:linear-gradient(top,#ffc600,#ff7e00);
            background:-moz-linear-gradient(top,#ffc600,#ff7e00);
            background:-webkit-linear-gradient(top,#ffc600,#ff7e00);
        }
        .ui-datepicker-prev, .ui-datepicker-next {

	        background: url(book-bn-bg3.jpg) 0 0 repeat-x;
	        background:linear-gradient(top,#ffc600,#ff7e00);
            background:-moz-linear-gradient(top,#ffc600,#ff7e00);
            background:-webkit-linear-gradient(top,#ffc600,#ff7e00);
        } 
        .ui-datepicker-calendar .ui-state-active {  
            background: #fffe7e;
        }

        .ui-datepicker-calendar .ui-state-fullBooking span {
            background: #ff2525 !important; 
            color: #000000 !important;
        }

        .bok-redcolor
        {
            width: 15px;
            height: 15px;
            float: right;
            background: #ffA0A0;
            margin: 1px 0 0 7px;
        }

        .ui-widget-header .ui-icon {
	        background-image: url(../Tools/js/css/ui-lightness/images/ui-icons_ff5800_256x240.png);
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <script src="//code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
    <script src="../Tools/js/jquery.ui.datepicker-zh-TW.js" type="text/javascript"></script>
    <script src="../Tools/js/json2.js" type="text/javascript"></script>
    <script src="../Tools/js/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(window).bind('beforeunload', function () {
                if ($('#hfStepStatus').val() == "2") {
                    return '你還沒有完成預約程序，確定要離開此頁面?';
                }
            });

            if (!checkStoreList()) {
                alert('店家目前不提供預約服務!');
                return false;
            }
            else {
                onDdlStoreListSelect();
                $('#Step1button').show();
                $('#divSendReservation').show();
                $('#hfStoreGuid').val($('#ddlStoreList option:selected').val());
            }

            $('#txtMobile').numeric();
            formatDatapicker();
            $(".ui-state-active").removeClass('ui-state-active');
            $(".ui-state-hover").removeClass('ui-state-hover');
            $('#datepicker').datepicker().val('');
            textChangeBind();
            radioChangeBind();
        });

        function formatDatapicker() {
            $("#datepicker").datepicker({
                defaultDate: '',
                dataFormat: 'yyyy/mm/dd',
                minDate: +1,
                maxDate: "+15D",
                onSelect: function (dateText, inst) {
                    onDatepickerSelect(dateText);
                },
                onChangeMonthYear: function (year, month, inst) {
                    $('#hfDatepickerShowMonth').val(month);
                },
                beforeShowDay: noOpenOrBookingIsFull
            });
        }

        function noOpenOrBookingIsFull(date) {
            var minDate = new Date();
            minDate = dateAdd("d", 1, minDate);
            if (date >= new Date() && differenceDate(minDate, date) < 14) {
                if (!$('#hfDatepickerShowMonth').val() || $('#hfDatepickerShowMonth').val() == (date.getMonth() + 1)) {
                    $('#hfDatepickerShowMonth').val(date.getMonth() + 1);
                    var fullDateArray = $('#hfFullBookingDate').val().split(',');
                    if ($.inArray(date.yyyymmdd(), fullDateArray) > -1) {
                        return [false, 'ui-state-fullBooking', '預約已額滿或店家尚不接受預約'];
                    }
                }
            }
            return [true];
        }

        function checkStoreList() {
            return ($('#ddlStoreList > option').length > 0) ? true : false;
        }
        function onDatepickerSelect(selectedDate) {
            var storeGuid = $('#ddlStoreList option:selected').val();
            var peopleOfNumber = $('#ddlPeopleOfNumber option:selected').val();
            $('#hfSelectDate').val(selectedDate);
            makeTimeSlotButton(storeGuid, selectedDate, peopleOfNumber);
        }
        function onDdlPeopleOfNumberSelect(selectItem) {
            var storeGuid = $('#ddlStoreList option:selected').val();
            var selectedDate = $('#datepicker').datepicker({ dateFormat: 'yyyy/mm/dd' }).val();
            if (selectedDate) {
                makeTimeSlotButton(storeGuid, selectedDate, selectItem.value);
            }
        }
        function onDdlStoreListSelect() {
            $('#Step1button').block({ message: null, overlayCSS: { backgroundColor: '#FFF', opacity: '0.0' } });
            var selectValue = $('#ddlStoreList option:selected').val();
            $('#hfStoreGuid').val(selectValue);
            $.ajax({
                type: "POST",
                url: "Reservation.aspx/GetStoreInfo",
                data: "{storeGuid:'" + selectValue + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) { bindStoreInfo(data.d); }
            });
            $('#Step1button').unblock();
        }
        function bindStoreInfo(jsonData) {
            var store = $.parseJSON(jsonData);
            if (!store.DataFormatSuccess)
            {
                alert('分店目前不提供預約服務!');
                $('#Step1button').hide();
            }
            else {
                $('.editStoreName').html($('#lblSellerName').html() + store.StoreName);
                $('.editStorePhone').html(store.StorePhone);
                $('.editStoreAddress').html(store.StoreAddress);
                $('#Step1button').show();
            }
        }

        function checkGenderRequired() {
            if ($(':radio[id*=rdoFemale]').is(':checked') || $(':radio[id*=rdoMale]').is(':checked')) {
                return true;
            }
            else {
                return false;
            }
        }
        function getGenderVal() {
            if ($(':radio[id*=rdoMale]').is(':checked')) {
                return true;
            }
            else {
                return false;
            }
        }

        function textChangeBind() {
            $("#txtReservationName").bind("keyup", function () {
                if (checkGenderRequired()) {
                    requiredName(false);
                }
            });
            $("#txtMobile").bind("keyup", function () {
                $('#phoneNumber').removeClass('bk-bg-alert');
                $('#requiredPhoneNumber').hide();
            });
            $("#txtEmail").bind("keyup", function () {
                $('#emailAddress').removeClass('bk-bg-alert');
                $('.validationEmail').hide();
                $('.noteEmail').show();
            });
        }

        function radioChangeBind() {
            $(':radio[id*=rdoFemale]').live('change', function () {
                if ($(this).is(':checked') && $('#txtReservationName').val()) {
                    requiredName(false);
                }
            });
            $(':radio[id*=rdoMale]').live('change', function () {
                if ($(this).is(':checked') && $('#txtReservationName').val()) {
                    requiredName(false);
                }
            });
        }

        function requiredName(ShowType) {
            if (ShowType) {
                $('#reservationName').addClass('bk-bg-alert');
                $('#requiredName').show();
            }
            else {
                $('#reservationName').removeClass('bk-bg-alert');
                $('#requiredName').hide();
            }
        }
        function checkStep1() {
            var check = true;
            if (!$('#txtReservationName').val() || !checkGenderRequired()) {
                requiredName(true);
                check = false;
            }
            if (!$('#txtMobile').val()) {
                $('#phoneNumber').addClass('bk-bg-alert');
                $('#requiredPhoneNumber').show();
                check = false;
            }

            if ($('#txtEmail').val() && !isEmail($('#txtEmail').val())) {
                $('#emailAddress').addClass('bk-bg-alert');
                $('.noteEmail').hide();
                $('.validationEmail').show();
                check = false;
            }

            if (check) {
                var gender = '';
                if ($(':radio[id*=rdoFemale]').is(':checked')) {
                    gender = '小姐';
                }
                else {
                    gender = '先生';
                }
                $('.editReservationNameGender').html($('#txtReservationName').val() + gender);
                $('.editMobile').html($('#txtMobile').val());
                $('.editEmail').html($('#txtEmail').val());
                $('#hfStepStatus').val('2');
                $('#ReservationStep2').show();
                $('#ReservationStep1').hide();
            }
            return false;
        }
        function prevStep() {
            $('#hfStepStatus').val('1');
            $('#ReservationStep2').hide();
            $('#ReservationStep1').show();
            $('html, body').animate({ scrollTop: 0 }, 'normal');
        }
        function makeTimeSlotButton(storeGuid, queryDate, numberOfPeople) {
            $('#divTimeSlot').block({ message: "<img src='../Themes/Booking/loading.gif' style='border-style:None;border-width:0px;' />", css: { width: '150px'}, overlayCSS:{backgroundColor:'#FFF', opacity:'0.0'}});
            $('#hfTimeSlotId').val('');
            $.ajax({
                type: "POST",
                url: "ReservationService.aspx/GetStoreTimeSlot",
                data: "{storeGuid:'" + storeGuid + "',queryDate:'" + queryDate + "',bookingType:" + 1 + ",numberOfPeople:" + numberOfPeople + "}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) { bindTimeSlotData(data.d); }
            });
            $('#divTimeSlot').unblock();
        }

        function bindTimeSlotData(jsonData) {
            var timeSlots = $.parseJSON(jsonData);
            var timeSlotCnt = 0;
            $('#divTimeSlot').html('');
            $.each(timeSlots, function (i, item) {
                timeSlotCnt++;
                var content = "<input type='button' id='timeSlot" + item.Id + "' class='bok-time' value='" + item.TimeSlot + "' OnClick='return setTimeSlotId(" + item.Id + ", this);'></input>";
                $('#divTimeSlot').append(content);
            });

            if (timeSlotCnt == 0) {
                $('#fullOrClose .bk-alert').html('預約已額滿或店家尚不接受預約');
                $('#fullOrClose').show();
            }
            else {
                $('#fullOrClose').hide();
            }
            
        }

        function setTimeSlotId(timeSlotId, btnObj) {
            var oldId = $('#hfTimeSlotId').val();
            if (oldId != '') {
                $('#timeSlot' + oldId).removeClass('bok-time-visited');
                $('#timeSlot' + oldId).addClass('bok-time');
            }
            $('#timeSlot' + timeSlotId).removeClass('bok-time');
            $('#timeSlot' + timeSlotId).addClass('bok-time-visited');
            $('#hfTimeSlotId').val(timeSlotId);
            $('#ReservationTime').html(btnObj.value);
            $('#RequiredDateTime').hide();
            return false;
        }

        function sendReservationRequest() {
            if (!$('#hfSelectDate').val()) {
                $('#RequiredDate').show();
            }
            else if (!$('#hfTimeSlotId').val()) {
                $('#RequiredTime').show();
            }
            else {
                var selectedValues = [];
                $('#divSendReservation').block({ message: null, overlayCSS:{backgroundColor:'#FFF', opacity:'0.0'} });

                $.ajax({
                    type: "POST",
                    url: "ReservationService.aspx/MakeReservationRecord",
                    data: "{bookingType:1"
                        + ",timeSlot:" + $('#hfTimeSlotId').val()
                        + ",reservationDate:'" + $('#datepicker').datepicker({ dateFormat: 'yyyy/mm/dd' }).val() + "'"
                        + ",numberOfPeople:" + $('#ddlPeopleOfNumber option:selected').val()
                        + ",remark:'" + htmlEncode($('#txtRemarks').val()) + "'"
                        + ",contactName:'" + htmlEncode($('#txtReservationName').val()) + "'"
                        + ",contactGender:" + getGenderVal()
                        + ",contactNumber:'" + $('#txtMobile').val() + "'"
                        + ",memberKey:''"
                        + ",orderKey:''"
                        + ",couponInfo:''"
                        + ",couponLink:''"
                        + ",apiKey:'" + $('#hfApiKey').val() + "'"
                        + ",couponsSequence:[" + selectedValues + "]"
                        + ",email:'" + $("#txtEmail").val() + "'"
                        + ",storeGuid:'" + $('#ddlStoreList option:selected').val() + "'"
                        + ",advanceReservationDays:1" 
                        +"}",                    
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                            var result = $.parseJSON(data.d);
                            if (result.Success) {
                                showSuccess();
                            } else {
                                alert(result.Message);
                                setFullReservationValue();
                                if (result.Message == '該時段預約人數已滿!') {
                                    $('#fullOrClose .bk-alert').html('預約已額滿或店家尚不接受預約');
                                }
                            }
                        }
                    });
                $('#divSendReservation').unblock();
            }
            return false;
        }


        function setFullReservationValue() {
            $.ajax({
                type: "POST",
                url: "ReservationService.aspx/GetFullBookingDate",
                data: "{advanceReservationMinDay:1"
                    + ",storeGuid:'" + $('#hfStoreGuid').val() + "',bookingType:1}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var result = $.parseJSON(data.d);
                    if (result.Success) {
                        $('#hfFullBookingDate').val(result.Data);
                        $("#datepicker").datepicker("refresh");
                        $('#divTimeSlot').empty();
                        $('#hfTimeSlotId').val('');
                        $('#hfSelectDate').val('');
                    }
                }
            });
        }

        function showSuccess()
        {
            var ResDate = new Date($('#datepicker').datepicker().val());
            $('#ReservationDate').html($('#datepicker').datepicker().val() + ' (' + getDayCName(ResDate.getDay()) + ')');
            $('#ReservationNumberOfPeople').html($('#ddlPeopleOfNumber option:selected').val());
            $('#editRemarks').html(htmlEncode($('#txtRemarks').val()));
            $('#hfStepStatus').val('3');
            $('#ReservationStep3').show();
            $('#ReservationStep2').hide();
            $('html, body').animate({ scrollTop: 0 }, 'normal');
        }
        function htmlEncode(value) {
            return $('<div/>').text(value).html();
        }
        function htmlDecode(value) {
            return $('<div/>').html(value).text();
        }
        function getDayCName(getDayValue) {
            var dayList = ['日', '一', '二', '三', '四', '五', '六'];
            return dayList[getDayValue];
        }
        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

        function differenceDate(minDate, checkDate) {
            var ONE_DAY = 1000 * 60 * 60 * 24;
            var date1_ms = minDate.getTime();
            var date2_ms = checkDate.getTime();
            var difference_ms = Math.abs(date1_ms - date2_ms);
            return Math.round(difference_ms / ONE_DAY);
        }

        function dateAdd(timeU, byMany, dateObj) {
            var millisecond = 1;
            var second = millisecond * 1000;
            var minute = second * 60;
            var hour = minute * 60;
            var day = hour * 24;
            var year = day * 365;

            var newDate;
            var dVal = dateObj.valueOf();
            switch (timeU) {
                case "ms": newDate = new Date(dVal + millisecond * byMany); break;
                case "s": newDate = new Date(dVal + second * byMany); break;
                case "mi": newDate = new Date(dVal + minute * byMany); break;
                case "h": newDate = new Date(dVal + hour * byMany); break;
                case "d": newDate = new Date(dVal + day * byMany); break;
                case "y": newDate = new Date(dVal + year * byMany); break;
            }
            return newDate;
        }
        Date.prototype.yyyymmdd = function () {
            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = this.getDate().toString();

            return yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
        };
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SMC" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <div class="bok-wrap">
        <div class="ly-booking-box">
            <div class="bok-logo">
                <img src="../Themes/Booking/loading.gif" alt="" style="display: none" />
                <img src="../Themes/Booking/lifeLogo.png" width="400" height="150" alt="" />
                <asp:HiddenField ID="hfStepStatus" ClientIDMode="Static" Value="1" runat="server" />
                <asp:HiddenField ID="hfStoreGuid" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="hfTimeSlotId" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="hfSelectDate" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="hfApiKey" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="hfSellerGuid" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="hfDatepickerShowMonth" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="hfFullBookingDate" ClientIDMode="Static" runat="server" />
            </div>
            <div id="ReservationStep1">
                <div class="bok-info clearfix">

                    <div class="bok-lablebox">
                        <label class="bok-lab"><span class="bk-t-let-mid">店家名</span>稱：</label>
                        <p>
                            <asp:Label ID="lblSellerName" ClientIDMode="Static" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                    <div class="bok-lablebox">
                        <label class="bok-lab"><span class="bk-t-let-mid">分店名</span>稱：</label>
                        <asp:DropDownList ID="ddlStoreList" ClientIDMode="Static" onchange="onDdlStoreListSelect();" CssClass="box-big-select" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="bok-lablebox">
                        <label class="bok-lab"><span class="bk-t-let-mid">店家店</span>話：</label>
                        <p class="editStorePhone"></p>
                    </div>

                    <div class="bok-lablebox">
                        <label class="bok-lab"><span class="bk-t-let-mid">店家地</span>址：</label>
                        <p class="editStoreAddress"></p>
                    </div>

                    <div id="reservationName" class="bok-lablebox">
                        <label class="bok-lab">預約人姓名：</label>
                        <div class="bok-labinpu-box">
                            <asp:TextBox ID="txtReservationName" ClientIDMode="Static" CssClass="bok-input" runat="server"></asp:TextBox>
                            <span class="bok-rdl-breaks">
                                <asp:RadioButton ID="rdoFemale" GroupName="GenderGroup" Text="小姐" runat="server" />
                                <asp:RadioButton ID="rdoMale" GroupName="GenderGroup" Text="先生" runat="server" />
                            </span>
                            <span id="requiredName" class="bk-red" style="margin-left: 10px; display: none">請輸入姓名/性別</span>
                        </div>
                    </div>

                    <div id="phoneNumber" class="bok-lablebox">
                        <label class="bok-lab"><span class="bk-t-let-mid">連絡電</span>話：</label>
                        <div class="bok-labinpu-box">
                            <asp:TextBox ID="txtMobile" ClientIDMode="Static" MaxLength="10" CssClass="bok-input" runat="server"></asp:TextBox>
                            <span class="bok-rdl-breaks">
                                <span id="requiredPhoneNumber" class="bk-red" style="margin-left: 10px; display: none;">請輸入連絡電話</span>
                            </span>
                        </div>
                    </div>

                    <div id="emailAddress" class="bok-lablebox">
                        <label class="bok-lab"><span class="bk-t-let-mid">電子信</span>箱：</label>
                        <div class="bok-labinpu-box">
                            <asp:TextBox ID="txtEmail" ClientIDMode="Static" CssClass="bok-input" runat="server"></asp:TextBox>
                            <span class="bok-rdl-brkdsp">
                                <span class="noteEmail" style="margin-left:10px;">(您將由此信箱收到預約成功及提醒通知信)</span>
                                <span class="bk-red validationEmail" style="margin-left: 10px; display: none;align-content:center">Email格式錯誤</span>
                            </span>                                    
                        </div>
                        <span class="bok-brktext">
                            <span class="noteEmail" style="margin-left:10px;">(您將由此信箱收到預約成功及提醒通知信)</span>
                            <span class="bk-red validationEmail" style="margin-left: 10px; display: none;text-align:center;align-content:center">Email格式錯誤</span>
                        </span>
                    </div>

                    <div id="Step1button" class="bok-btnfarm" style="display: none">
                        <asp:Button ID="btnStep1" CssClass="bok-btn" OnClientClick="return checkStep1();" runat="server" Text="下一步" />
                    </div>
                </div>
            </div>
            <div id="ReservationStep2" style="display: none">
                <div class=" bok-infotop clearfix">

                    <div class="bok-lablebox bok-lablebox-top2">
                        <label class="bok-lab"><span class="bk-t-let-mid">店家名</span>稱：</label>
                        <p class="bok-2width editStoreName">
                        </p>
                    </div>
                    <div class="bok-lablebox bok-lablebox-top2">
                        <label class="bok-lab"><span class="bk-t-let-mid">店家電</span>話：</label>
                        <p class="bok-2width editStorePhone">
                        </p>
                    </div>
                    <div class="bok-lablebox bok-lablebox-top2">
                        <label class="bok-lab"><span class="bk-t-let-mid">店家地</span>址：</label>
                        <p class="bok-2width editStoreAddress">
                        </p>
                    </div>

                </div>

                <div class="bok-infobelow  clearfix">
                    <div class="bok-lablebox bok-rdl-topdisappear bok-rdl-appear">
                        <span class="bok-note bk-org">※如欲更改/取消預約時間，請主動與店家聯繫！</span>
                        <span class="bok-note">※17Life系統提供15日內之預約服務，如有特別需求，還請與店家聯繫喔！</span>
                    </div>


                    <span class="bok-rdl-disappear">
                        <div class="bok-lablebox bok-lablebox-top2">
                            <label class="bok-lab">預約人姓名：</label>
                            <p class="bok-2width editReservationNameGender" id="ReservationNameGender">
                            </p>
                        </div>
                        <div class="bok-lablebox bok-lablebox-top2">
                            <label class="bok-lab"><span class="bk-t-let-mid">連絡電</span>話：</label>
                            <p class="bok-2width editMobile"></p>
                        </div>
                        <div class="bok-lablebox bok-lablebox-top2">
                          <label class="bok-lab"><span class="bk-t-let-mid">電子信</span>箱：</label>
                          <p class="bok-2width editEmail"></p>
                        </div>
                    </span>

                    <div class="bok-date-lablebox bok-textcen">
                        <div class="bok-damo-outbox">
                            <div class="bok-Mo-l">
                                <div class="bok-lab-selt">
                                    <span class="bk-red bk-bigtext">*</span><strong>預約日期</strong>
                                    <div class="bok-redcolor"></div>
                                    <div class="bok-redt">額滿</div>
                                </div>
                                <div id="datepicker" style="float:right"></div>
                            </div>

                            <div class="bok-Mo-r">
                                <div class="bok-lab-selt">
                                    <span class="bk-red bk-bigtext">*</span><strong>預約人數</strong>：
                                    <asp:DropDownList ID="ddlPeopleOfNumber" onchange="onDdlPeopleOfNumberSelect(this);" ClientIDMode="Static" CssClass="box-select" runat="server">
                                        <asp:ListItem Text="1" Value="1" />
                                        <asp:ListItem Text="2" Value="2" />
                                        <asp:ListItem Text="3" Value="3" />
                                        <asp:ListItem Text="4" Value="4" />
                                        <asp:ListItem Text="5" Value="5" />
                                        <asp:ListItem Text="6" Value="6" />
                                        <asp:ListItem Text="7" Value="7" />
                                        <asp:ListItem Text="8" Value="8" />
                                        <asp:ListItem Text="9" Value="9" />
                                        <asp:ListItem Text="10" Value="10" />
                                        <asp:ListItem Text="11" Value="11" />
                                        <asp:ListItem Text="12" Value="12" />
                                        <asp:ListItem Text="13" Value="13" />
                                        <asp:ListItem Text="14" Value="14" />
                                        <asp:ListItem Text="15" Value="15" />
                                        <asp:ListItem Text="16" Value="16" />
                                        <asp:ListItem Text="17" Value="17" />
                                        <asp:ListItem Text="18" Value="18" />
                                        <asp:ListItem Text="19" Value="19" />
                                        <asp:ListItem Text="20" Value="20" />
                                    </asp:DropDownList>
                                </div>
                                <div id="selectTimeSlot" class="bok-lab-selt"><span class="bk-red bk-bigtext">*</span><strong>預約時間：</strong></div>
                                <div id="divTimeSlot" class="bok-timefarm">
                                </div>
                                <div id="fullOrClose" class="bok-timefarm bk-org" style="display: none">
                                    <p class="bk-alert">預約已額滿或店家尚不接受預約</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="bok-remark">
                        <label class="bok-lab">備註：</label>
                        <asp:TextBox ID="txtRemarks" ClientIDMode="Static" CssClass="bok-textarea" Rows="3" TextMode="MultiLine" runat="server"></asp:TextBox>
                    </div>

                    <div class="bok-lablebox bok-rdl-disappear">
                        <span class="bok-note bk-org">※如欲更改/取消預約時間，請主動與店家聯繫！</span>
                        <span class="bok-note">※17Life系統提供15日內之預約服務，如有特別需求，還請與店家聯繫喔！</span>
                    </div>

                    <div class="bok-lablebox bok-textcen">
                        <label id="RequiredDate" class="bok-che-datelab bk-red" style="display: none">請選擇「預約日期」</label>
                        <label id="RequiredTime" class="bok-che-datelab bk-red" style="display: none">請選擇「預約時間」</label>
                    </div>

                    <div id="divSendReservation" class="bok-btnfarm" style="display: none">
                        <input type="button" class="bok-btn bok-btngray" value="上一步" onclick="return prevStep();"></input>
                        <input type="button" class="bok-btn" value="確認預約" onclick="return sendReservationRequest();"></input>
                    </div>
                </div>
            </div>
            <div id="ReservationStep3" style="display: none">
                <div class=" bok-infotop clearfix">


                    <div class="bok-lablebox bok-lablebox-top2">
                        <label class="bok-lab bk-red bk-fw" id="SuccessMessage">恭喜您預約成功！！</label>
                    </div>

                    <div class="bok-lablebox bok-lablebox-top2">
                        <label class="bok-lab bk-red">
                            提醒您：<br />
                            <span id="SuccessNote">為維護您與店家的權益，預約僅保留10分鐘。</span>
                            <br />
                            如欲更改/取消預約時間，請主動與店家聯繫！</label>
                    </div>

                </div>

                <div class="bok-infobelow clearfix">
                    <div class="bok-lablebox bok-lablebox-top2">
                        <label class="bok-lab">以下為您的預約資訊：</label>
                    </div>
                    <div class="bok-lablebox bok-lablebox-top2">
                        <label class="bok-lab"><span class="bk-t-let-mid">預約日</span>期：</label>
                        <p id="ReservationDate"></p>
                    </div>
                    <div id="divReservationTime" class="bok-lablebox bok-lablebox-top2">
                        <label class="bok-lab"><span class="bk-t-let-mid">預約時</span>間：</label>
                        <p id="ReservationTime"></p>
                    </div>
                    <div class="bok-lablebox bok-lablebox-top2">
                        <label class="bok-lab"><span class="bk-t-let-mid">預約人</span>數：</label>
                        <p id="ReservationNumberOfPeople"></p>
                    </div>
                    <div class="bok-lablebox bok-lablebox-top2">
                        <label class="bok-lab">預約人姓名：</label>
                        <p class="editReservationNameGender"></p>
                    </div>
                    <div class="bok-lablebox bok-lablebox-top2">
                        <label class="bok-lab"><span class="bk-t-let-mid">連絡電</span>話：</label>
                        <p class="editMobile"></p>
                    </div>
                    <div class="bok-lablebox bok-lablebox-top2">
                        <label class="bok-lab"><span class="bk-t-let-mid">電子信</span>箱：</label>
                        <p class="editEmail"></p>
                    </div>
                    <div class="bok-lablebox bok-lablebox-top2">
                        <label class="bok-lab"><span class="bk-t-let-big">備</span>註：</label>
                        <p id="editRemarks"></p>
                    </div>

                    <div class=" bok-confirbox ">
                        <div class="bok-lablebox bok-lablebox-top2">
                            <label class="bok-lab"><span class="bk-t-let-mid">店家名</span>稱：</label>
                            <p class="bok-2width editStoreName">
                            </p>
                        </div>
                        <div class="bok-lablebox bok-lablebox-top2">
                            <label class="bok-lab"><span class="bk-t-let-mid">店家電</span>話：</label>
                            <p class="bok-2width editStorePhone">
                            </p>
                        </div>
                        <div class="bok-lablebox bok-lablebox-top2">
                            <label class="bok-lab"><span class="bk-t-let-mid">店家地</span>址：</label>
                            <p class="bok-2width editStoreAddress">
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <img class="bok-bg" src="https://www.17life.com/images/17P/20130926-booking/bg.jpg" width="1500" height="1300" />
        </div>
</asp:Content>

﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>17P好康</title>
</head>
<body>
    <table width="750" align="center" style="background-color: #C66C0A; border-collapse: collapse; border: 0px;">
        <tr>
            <td height="45" align="center" style="background-color: #FFF; font-family: '微軟正黑體';
                font-size: 13px; color: #000; text-align: center;">
                若信件內容無法正常顯示，您可以點選 <a href="{pre_link}?rsrc=17_systemEDM" target="_blank" style="color: #F60;"><strong>此處查看</strong></a> 在線版本。
            </td>
        </tr>
        <tr>
            <td width="750" height="72" style="background-repeat: repeat; background-color: #c66c0a; padding: 0;">
                <table width="750">
                    <tr>
                        <td width="120" height="67" style="color: #FFF; font-size: 24px;">
                            <a href="{deal_link}" target="_blank">
                                <img src="http://www.17life.com/Themes/default/images/17Life/EDM/EDMLOGO.jpg" width="150" height="68" border="0" alt="17P好康" />
                            </a>
                        </td>
                        <td width="370" style="color: #FFF; font-size: 20px; font-weight: bold; text-align: left;">
                            所在城市:{main_city}
                        </td>
                        <td width="244" style="padding-right: 10px; padding-left: 80px; color: #FFF;" align="right">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
				{ad}
                <table width="748" style="background-color: #F9F5EE;">
                    <tr>
                        <td height="10">
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table width="726" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <table width="726" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="454" align="center">
                                                    <a href="{deal_link_photo}" target="_blank">
                                                        <img src="{deal_photo}" width="454" height="253" border="0" alt="{deal_title}" /></a>
														</td>
                                                    <td width="272" valign="middle">
                                                        <table width="272" align="right" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td align="center">
                                                                    <table width="265" style="background-color: #000;" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td height="100" width="118" align="right" style="color: #FFF; font-size: 48px; font-family: Arial, Helvetica, sans-serif;">
                                                                                {deal_price}
                                                                            </td>
                                                                            <td height="100" width="135" align="center">
                                                                                <a href="{deal_link_buy}" target="_blank">
                                                                                    <img src="http://www.17life.com/Themes/default/images/17Life/EDM/EDMPponBUY.jpg" width="135" height="100" border="0" alt="buy" /></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <table width="265" style="background-color: #EAD9C1;">
                                                                        <tr>
                                                                            <td width="88" height="78" align="center">
                                                                                <table width="50" border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td height="50" align="center" style="font-family: '微軟正黑體'; font-size: 18px; color: #000;">
                                                                                            原價<br />
                                                                                            {deal_price_ori}
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td width="77" align="center">
                                                                                <table width="50" border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td height="50" align="center" style="font-family: '微軟正黑體'; font-size: 18px; color: #000;">
                                                                                            折扣<br />
                                                                                            {deal_rate}
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td width="84" align="center">
                                                                                <table width="50" border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td height="50" align="center" style="font-family: '微軟正黑體'; font-size: 18px; color: #000;">
                                                                                            節省<br />
                                                                                            {deal_save}
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30" align="center">
                                        <table width="726" style="font-size: 16px; font-family: '微軟正黑體'; color: #000;">
                                            <tr>
                                                <td height="5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="line-height: 24px; text-align: left;">
                                                    <a href="{deal_link_title}" target="_blank" style="font-size: 18px; color: #F60; font-family: 微軟正黑體;">{deal_title}</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="3">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="line-height: 23px; text-align: left;">
                                                    <a href="{deal_link_subtitle}" target="_blank" style="font-size: 16px; color: #000; font-family: 微軟正黑體; text-decoration: none;"> {deal_name} </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="25">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table width="748" style="background-color: #F9F5EE;" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center">
                            <table width="726" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										{side_deal_1}
									</td>
									<td>
										{side_deal_2}
									</td>
								</tr>
							</table>
							<table width="726" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										{side_deal_3} 
									</td>
									<td>
										{side_deal_4}
									</td>
								</tr>
							</table>
							<table width="726" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										{side_deal_5} 
									</td>
									<td>
										{side_deal_6}
									</td>
								</tr>
							</table>
							<table width="726" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										{side_deal_7} 
									</td>
									<td>
										{side_deal_8}
									</td>
								</tr>
							</table>
							<table width="726" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										{side_deal_9} 
									</td>
									<td>
										{side_deal_10}
									</td>
								</tr>
							</table>
							<table width="726" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										{side_deal_11} 
									</td>
									<td>
										{side_deal_12}
									</td>
								</tr>
							</table>
                        </td>
                    </tr>
                    <tr>
                        <td height="15">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
		{pez}
		{piinlife}
        <tr>
            <td align="center" valign="top" height="65" style="background-color: #000;">
                <table width="730" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td width="10" height="8">
                        </td>
                    </tr>
                    <tr>
                        <td height="25" align="center" style="font-size: 13px; color: #FFF;">
                            <a href="http://www.17life.com/user/service.aspx" target="_blank" style="color: #FFF;">客服中心</a>
                            {member_link}
							| <a href="http://www.17life.com/Ppon/WhiteListGuide.aspx" target="_blank" style="color: #FFF;">加入白名單</a> 
							| 服務時間：週一至週六9:00~18:00
                        </td>
                    </tr>
                    <tr>
                        <td height="25" align="center" style="font-size: 13px; color: #FFF;">
                            康迅數位整合股份有限公司 版權所有 轉載必究
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
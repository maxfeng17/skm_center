<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:param name="ServiceCenterUrl"/>
	<xsl:template match="/RefundFailureNotificationInformation">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>PiinLife</title>
			</head>

			<body>
				<table width="750" align="center">
					<tr>
						<td>
							<div style="background-color:#F6EADF;">
								<table width="700" align="center" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td>
											<!--MailHeader_Start-->
											<div id="MailHeader">
												<table width="700" height="80" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td height="20"/>
													</tr>
													<tr>
                            <tr>
                              <td>
                                <img width="700" height="80" alt="PiinLife" style="display:block;">
                                  <xsl:attribute name="src">
                                    <xsl:value-of select='HDmailHeaderUrl' />
                                  </xsl:attribute>
                                </img>
                              </td>
                            </tr>

                          </tr>
												</table>
											</div>
											<!--MailHeader_End-->
										</td>
									</tr>
									<tr>
										<td>
											<!--MailContent_Start-->
											<div id="MailContent">
												<table width="700" border="0" cellspacing="0" cellpadding="0" 
														style="background-color:white; border-left:1px solid #E1D6CC; border-right:1px solid #E1D6CC;">
													<tr>
														<td height="10" colspan="3"/>
													</tr>
													<tr>
														<td>
															<table width="698" border="0" cellspacing="0" cellpadding="0"  align="center" style="font-size:13px; color:#414141;">
																<tr>
																	<td width="25"/>
																	<td style="word-break:break-all; word-wrap:break-word; text-align:left;">
																		<p>親愛的<xsl:value-of select="BuyerName"/>，您好︰</p>
																		<p>很抱歉，您日前的退貨退款申請失敗。【<label>
																				<a style="color:#FC7D49;">
																					<xsl:attribute name="href">
																						<xsl:value-of select="RefundDetailUrl"/>
																					</xsl:attribute>查看完整訂單明細</a>
																			</label>】</p>
																		<p>退貨退款失敗原因：<span style="color:#B37B53;">
																				<label>
																					<xsl:value-of select="FailureReason"/>
																				</label>
																			</span>
																		</p>
																		<br />
																		<table width="640" align="center" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;">
																			<tr>
																				<td width="20" height="30" style="background-color:#605247;"/>
																				<td width="160" height="30" style="background-color:#605247; color:white;">退貨明細</td>
																				<td width="460" height="30" style="background-color:#605247;">&#160;</td>
																			</tr>
																			<tr>
																				<td width="20" height="40"/>
																				<td width="620" height="40" colspan="2">
																					<label>
																						<xsl:value-of select="RefundItemName"/>
																					</label>
																				</td>
																			</tr>
																			<tr>
																				<td width="20" height="25"/>
																				<td width="160" height="25">訂單商品（憑證）數：</td>
																				<td width="460" height="25">
																					<label>
																						<xsl:value-of select="OrderItemsCount"/>
																					</label>
																				</td>
																			</tr>
																			<tr>
																				<td width="20" height="25"/>
																				<td width="160" height="25">本次實際退貨數：</td>
																				<td width="460" height="25">
																					<label>
																						<xsl:value-of select="RefundItemsCount"/>
																					</label>
																				</td>
																			</tr>
																			<tr>
																				<td width="20" height="25"/>
																				<td width="160" height="25"/>
																				<td width="460" height="25"/>
																			</tr>
																		</table>
																		<hr style="border-top:none; border-bottom:1px solid #C8C8C8" />
																		<p>※ 客服人員不會主動以email或電話要求您更改結帳方式或提供個人資料，若您接獲類似訊息，請拒絕回應，<br />
																			&#160;&#160;&#160;&#160;&#160;並立刻與我們聯絡。<br />
																			※ 本信件由系統發送，請勿直接回覆。若有任何問題，請洽
																			<a style="color:#FC7D49;">
																				<xsl:attribute name="href">
																					<xsl:value-of select="$ServiceCenterUrl"/>
																				</xsl:attribute>客服中心</a>。</p>
																	</td>
																	<td width="25"/>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td height="10" colspan="3"/>
													</tr>
												</table>
											</div>
											<!--MailContent_End-->
										</td>
									</tr>
									<tr>
										<td>
											<!--MailFooter_Start-->
											<div id="MailFooter">
												<table width="700" height="80" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td>
                              <img width="700" height="80" alt="PiinLife" style="display:block;" >
                                <xsl:attribute name="src">
                                  <xsl:value-of select='HDmailFooterUrl' />
                                </xsl:attribute>
                              </img>
                            </td>
                          </tr>
													<tr>
														<td height="20"/>
													</tr>
												</table>
											</div>
											<!--MailFooter_End-->
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:param name="ServiceCenterUrl"/>
	<xsl:template match="/EInvoiceInformation">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>PiinLife</title>
			</head>

			<body>
				<table width="750" align="center">
					<tr><td>
							<div style="background-color:#F6EADF;">
								<table width="700" align="center" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td>
											<!--MailHeader_Start-->
											<div id="MailHeader">
												<table width="700" height="80" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td height="20"></td>
													</tr>
                          <tr>
                            <td>
                              <img width="700" height="80" alt="PiinLife" style="display:block;">
                                <xsl:attribute name="src">
                                  <xsl:value-of select='HDmailHeaderUrl' />
                                </xsl:attribute>
                              </img>
                            </td>
                          </tr>

                        </table>
											</div>
											<!--MailHeader_End-->
										</td>
									</tr>
									<tr>
										<td>
											<!--MailContent_Start-->
											<div id="MailContent">
												<table width="700" border="0" cellspacing="0" cellpadding="0" 
														style="background-color:white; border-left:1px solid #E1D6CC; border-right:1px solid #E1D6CC;">
													<tr><td height="10" colspan="3"></td></tr>
													<tr>
														<td>
															<table width="698" border="0" cellspacing="0" cellpadding="0"  align="center" style="font-size:13px; color:#414141;">
																<tr>
																	<td width="25"></td>
																	<td style="word-break:break-all; word-wrap:break-word; text-align:left;">
																		<p>親愛的<xsl:value-of select="BuyerName"/>，您好︰</p>
																		<p>感謝您購買【<label><xsl:value-of select="PurchasedItemName"/></label>】 ，我們已為您開立電子發票，附上發票副本供您參閱。</p>
																		<br />
																		<table width="640" border="1" cellspacing="0" cellpadding="0" align="center" style="border-color:#71AAC6; font-size:13px;">
																			<tr>
																				<td width="240">發票號碼:<xsl:value-of select="InvoiceNumber"/></td>
																				<td colspan="4" rowspan="4" align="center">
                                          17Life康太數位整合股份有限公司<br/>
																					電子計算機統一發票<br/>
																					中華民國<xsl:value-of select="InvoiceDate"/><br/>
																				</td>
																				<td rowspan="3">
																				</td>
																			</tr>
																			<tr>
																				<td>買受人:
																					<xsl:choose>
																						<xsl:when test='IsCompanyInvoice="true"'>
																							<xsl:value-of select="InvoiceCompanyName"/>
																						</xsl:when>
																						<xsl:otherwise>
																							<xsl:value-of select="InvoiceIndividualName"/>
																						</xsl:otherwise>
																					</xsl:choose>
																				</td>
																			</tr>
																			<tr>
																				<td>統一編號:<xsl:if test='IsCompanyInvoice="true"'><xsl:value-of select="InvoiceCompanyId"/></xsl:if></td>
																			</tr>
																			<tr>
																				<td>地址:<xsl:value-of select="InvoiceAddress"/></td>
																				<td align="center">訂單編號:<xsl:value-of select="OrderId"/></td>
																			</tr>
																			<tr>
																				<td colspan="2" align="center">品名</td>
																				<td align="center">數量</td>
																				<td align="center">單價</td>
																				<td align="center">金額</td>
																				<td align="center">備註</td>
																			</tr>
																			<tr>
																				<td colspan="2" rowspan="3">
																					<xsl:comment><!--商品檔次:填購買的品名 ; 憑證檔次:填17Life購物金--></xsl:comment>
																					<xsl:choose>
																						<xsl:when test='IsPhysicalProductItem = "true"'>
																							<xsl:for-each select="Items/Item">
																								&#160;&#160;<xsl:value-of select="text()"/><br/>
																							</xsl:for-each>
																						</xsl:when>
																						<xsl:otherwise>&#160;&#160;17Life購物金<br/>
																						</xsl:otherwise>
																					</xsl:choose>

																				</td>
																				<td rowspan="3" align="center">1</td>
																				<td rowspan="3" align="center"><xsl:choose>
																						<xsl:when test='IsCompanyInvoice="true"'>																							
																							<xsl:value-of select='format-number( TotalAmount div (1 + TaxRate) , "#" )'/>
																						</xsl:when>
																						<xsl:otherwise><xsl:value-of select="TotalAmount"/></xsl:otherwise>
																					</xsl:choose></td>
																				<td rowspan="3" align="center">
																					<xsl:choose>
																						<xsl:when test='IsCompanyInvoice="true"'>																							
																							<xsl:value-of select='format-number( TotalAmount div (1 + TaxRate) , "#" )'/>
																						</xsl:when>
																						<xsl:otherwise><xsl:value-of select="TotalAmount"/></xsl:otherwise>
																					</xsl:choose>
																				</td>
																				<td colwspan="3" align="center">
																					<xsl:comment><!--憑證檔次: 購買的品名放在備註--></xsl:comment>
																					<xsl:if test='IsPhysicalProductItem = "false"'>
																						<xsl:for-each select="Items/Item">
																							&#160;&#160;<xsl:value-of select="text()"/><br/>
																						</xsl:for-each>
																					</xsl:if></td>
																			</tr>
																			<tr>
																				<td align="center">營業人蓋用統一發票專用章</td>
																			</tr>
																			<tr>
																				<td rowspan="6" align="center">副本<br /><font style="color:red;">此副本僅供參考，<br />不可持本聯兌換。</font></td>
																			</tr>
																			<tr>
																				<td colspan="4" align="center">銷售額合計</td>
																				<td></td>
																			</tr>
																			<tr>
																				<td rowspan="2" align="center">營業稅</td>
																				<td align="center">應稅</td>
																				<td align="center">零稅率</td>
																				<td align="center">免稅</td>
																				<td align="center" rowspan="2"><xsl:value-of select='TotalAmount - format-number( TotalAmount div (1 + TaxRate) , "#" )'/></td>
																			</tr>
																			<tr>
																				<td align="center"><xsl:if test="TaxRate > 0">V</xsl:if></td>
																				<td align="center"></td>
																				<td align="center"><xsl:if test="TaxRate = 0">V</xsl:if></td>
																			</tr>
																			<tr>
																				<td colspan="4" align="center">總計</td>
																				<td align="center"><xsl:value-of select="TotalAmount"/></td>
																			</tr>
																			<tr>
																				<td colspan="5" align="center">總計新臺幣<xsl:value-of select="TotalAmountTaiwanFormat"/>元整</td>
																			</tr>
																			<tr>
																				<td colspan="6" align="center">
																					*應稅、零稅、免稅之銷售額應分別開立統一發票，並應於各該欄位打「V」<br/>
																					本發票依財政部臺北市國稅局中南稽徵所99年8月20日財北國稅中南營業一字第0990017821號函核准使用
																				</td>
																			</tr>
																		</table>
																		<br />
																		<p>
																			電子發票相關提醒：<br />
																			<span style="color:#B37B53;">1. 本信件所提供為電子發票副本，請勿直接持此本聯兌獎。</span><br />
																			2. 您的發票將以託管方式，於奇數月的26日主動為您進行對獎。<br />
																			3. 若您的發票中獎，將會寄發中獎通知到您的電子信箱。為確保您的權益，請您依信中指示，連結回指定頁面，<br />
																			&#160;&#160;&#160;&#160;&#160;回填買受人及寄件地址，我們將在收到您回覆後10 個工作天內，統一以掛號方式將中獎發票寄送給您。<br />
																			4. 如欲索取紙本發票，請於發票開立後至［訂單查詢］>［訂單明細］內索取。<br />
																		5. 如您已辦理退貨，待退貨完成，即會為您將發票作廢或折讓。
																	</p>
																	<hr style="border-top:none; border-bottom:1px solid #C8C8C8" />
																	<p>
                                    ※ 品生活客服人員不會主動以E-mail或電話要求您更改結帳方式或提供個人資料，若您接獲類似訊息，請拒絕回應，<br />
																		&#160;&#160;&#160;&#160;&#160;並立刻與品生活客服人員聯絡。<br />
                                    ※ 此信函為系統自動發出，請勿直接回覆，有任何問題請至<a style="color:#FC7D49;">
																		<xsl:attribute name="href">
																					<xsl:value-of select="$ServiceCenterUrl"/>
																				</xsl:attribute></a>與我們聯繫。</p>
																</td>
																<td width="25"></td>
															</tr>
														</table>
													</td>
												</tr>
												<tr><td height="10" colspan="3"></td></tr>
											</table>
										</div>
										<!--MailContent_End-->
									</td>
								</tr>
								<tr>
									<td>
										<!--MailFooter_Start-->
										<div id="MailFooter">
											<table width="700" height="80" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td>
                            <img width="700" height="80" alt="PiinLife" style="display:block;" >
                              <xsl:attribute name="src">
                                <xsl:value-of select='HDmailFooterUrl' />
                              </xsl:attribute>
                            </img>
                          </td>
                        </tr>
												<tr>
													<td height="20"></td>
												</tr>
											</table>
										</div>
										<!--MailFooter_End-->
									</td>
								</tr>
							</table>
						</div>
					</td></tr>
			</table>
		</body>
	</html>
</xsl:template>
</xsl:stylesheet>
﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:param name="ServiceCenterUrl"/>
  <xsl:template match="/ApplyRefundCouponNotificationInformation">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>【PiinLife品生活】退貨申請通知</title>
      </head>

      <body>
        <table width="100%" align="center">
          <tr>
            <td>
              <div style="background-color:#F6EADF;">
                <table width="700" align="center" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>
                      <!--MailHeader_Start-->
                      <div id="MailHeader">
                        <table width="700" height="80" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="20"></td>
                          </tr>
                          <tr>
                            <td>
                              <img width="700" height="80" alt="PiinLife" style="display:block;">
                                <xsl:attribute name="src">
                                  <xsl:value-of select='HDmailHeaderUrl' />
                                </xsl:attribute>
                              </img>
                            </td>
                          </tr>
                        </table>
                      </div>
                      <!--MailHeader_End-->
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <!--MailContent_Start-->
                      <div id="MailContent">
                        <table width="700" border="0" cellspacing="0" cellpadding="0"
                             style="background-color:white; border-left:1px solid #E1D6CC; border-right:1px solid #E1D6CC;">
                          <tr>
                            <td height="10" colspan="3"></td>
                          </tr>
                          <tr>
                            <td>
                              <table width="698" border="0" cellspacing="0" cellpadding="0"  align="center" style="font-size:13px; color:#414141;">
                                <tr>
                                  <td width="25"></td>
                                  <td style="word-break:break-all; word-wrap:break-word; text-align:left;">
                                    <p>親愛的<xsl:value-of select="BuyerName"/>，您好︰</p>
                                    <p>
                                      我們已收到您的退貨申請，<br />
                                      還請您將下列連結的﹝退貨申請書﹞/﹝退回或折讓證明單﹞列印出來，﹝退貨申請書﹞僅需正楷簽名或蓋章，若開立為三聯式發票，均請務必填寫「營利事業統一編號」並蓋上公司發票章，以郵局掛號或傳真方式寄回品生活退貨處理中心，待我們收到資料後會立即為您處理退款作業。<br />
                                      <span style="color:#B37B53;">★系統將會依訂單狀況自動判斷須提供的單據（﹝退貨申請書﹞、﹝退回或折讓證明單﹞），若尚未開立發票，下載連結僅有﹝退貨申請書﹞。</span>
                                    </p>
                                    <br />

                                    <table width="640" align="center" border="0" cellspacing="0" cellpadding="0" style="font-size:13px;">
                                      <tr>
                                        <td width="20" height="30" style="background-color:#605247;"></td>
                                        <td width="160" height="30" style="background-color:#605247; color:white;">退貨明細</td>
                                        <td width="460" height="30" style="background-color:#605247;">&#160;</td>
                                      </tr>
                                      <tr>
                                        <td width="20" height="40"></td>
                                        <td width="620" height="40" colspan="2">
                                          <label>
                                            <xsl:value-of select="RefundItemName"/>
                                          </label>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="20" height="25"></td>
                                        <td width="160" height="25">訂單商品﹝憑證﹞數：</td>
                                        <td width="460" height="25">
                                          <label>
                                            <xsl:value-of select="OrderItemsCount"/>
                                          </label>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td width="20" height="25"></td>
                                        <td width="160" height="25">本次實際退貨數：</td>
                                        <td width="460" height="25">
                                          <label>
                                            <xsl:value-of select="RefundItemsCount"/>
                                          </label>
                                        </td>
                                      </tr>
                                    </table>
                                    <br />
                                    <p>
                                      本次退貨專屬下載：<a style="color:#FC7D49;">
                                        <xsl:attribute name="href">
                                          <xsl:value-of select="RefundApplicationUrl"/>
                                        </xsl:attribute>退貨申請書</a>&#160;&#160;&#160;&#160;&#160;
                                      <xsl:variable name="isCreateEinvoice">
                                        <xsl:value-of select="IsCreateEinvoice"/>
                                      </xsl:variable>
                                        <xsl:choose>
                                          <xsl:when test=" $isCreateEinvoice = 'true'">
                                            <a style="color:#FC7D49;">
                                              <xsl:attribute name="href">
                                                <xsl:value-of select="InvoiceMailbackAllowanceUrl"/>
                                              </xsl:attribute>退回或折讓證明單
                                            </a>
                                          </xsl:when>
                                          <xsl:otherwise>
                                            &#160;&#160;
                                          </xsl:otherwise>
                                        </xsl:choose>                                      
                                    </p>
                                    <br />
                                    <p>
                                      品生活退貨處理中心<br />
                                      地址：104 台北市中山區中山北路一段11號13樓<br />
                                      傳真電話：(02)2511-9110<br />
                                    </p>
                                    <hr style="border-top:none; border-bottom:1px solid #C8C8C8" />
                                    <p>
                                      注意事項：<br />
                                      <span style="color:#B37B53;">1. 如於訂購時您已索取紙本發票，亦請務必將發票一併以郵局掛號方式寄回，否則恕無法辦理退費。</span><br />
                                      2. 您隨時可至訂單明細中下載﹝退貨申請書﹞/﹝退回或折讓證明單﹞。<br />
                                      3. 退貨處理約須10-20個工作天，退貨完成後，退款作業將依照您選擇之退款方式及原消費扣抵方式退還金額；如<br />
                                      &#160;&#160;&#160;&#160;&#160;選擇信用卡刷退，退款金額會直接退至您下訂單時所使用的信用卡中，因各家銀行作業時間不同，您應可於下
                                      &#160;&#160;&#160;&#160;&#160;一期信用卡帳單看到此退款（視信用卡結帳週期而定）。<br />
                                      &#160;&#160;&#160;&#160;&#160;如選擇ATM轉帳付款，款項退至您指定帳戶約須7-10個工作天，若退款資訊不全，將影響退貨處理時間。
                                      <br />
                                      &#160;&#160;&#160;&#160;&#160;（若訂單使用17Life折價券折抵，無論是否退貨皆不得返還）
                                    </p>
                                    <hr style="border-top:none; border-bottom:1px solid #C8C8C8" />
                                    <p>
                                      ※ 品生活客服人員不會主動以E-mail或電話要求您更改結帳方式或提供個人資料，若您接獲類似訊息，<br />
                                      &#160;&#160;&#160;&#160;&#160;請拒絕回應，並立刻與品生活客服人員聯絡。<br />
                                      ※ 此信函為系統自動發出，請勿直接回覆，有任何問題請至<a target="_blank" style="color:#FC7D49;">
                                        <xsl:attribute name="href">
                                          <xsl:value-of select="$ServiceCenterUrl"/>
                                        </xsl:attribute>客服信箱</a>與我們聯繫。
                                    </p>
                                  </td>
                                  <td width="25"></td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td height="10" colspan="3"></td>
                          </tr>
                        </table>
                      </div>
                      <!--MailContent_End-->
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <!--MailFooter_Start-->
                      <div id="MailFooter">
                        <table width="700" height="80" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td>
                              <img width="700" height="80" alt="PiinLife" style="display:block;" >
                                <xsl:attribute name="src">
                                  <xsl:value-of select='HDmailFooterUrl' />
                                </xsl:attribute>
                              </img>
                            </td>
                          </tr>
                          <tr>
                            <td height="20"></td>
                          </tr>
                        </table>
                      </div>
                      <!--MailFooter_End-->
                    </td>
                  </tr>
                </table>
              </div>
            </td>
          </tr>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
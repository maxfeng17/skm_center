﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/CouponMailInformation">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>【PiinLife品生活】寄送憑證</title>
            </head>

            <body>
                <table width="100%" align="center">
                    <tr>
                        <td>
                            <div style="background-color:#F6EADF;">
                                <table width="700" align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <!--MailHeader_Start-->
                                            <div id="MailHeader">
                                                <table width="700" height="80" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td height="20"></td>
                                                    </tr>
                                                  <tr>
                                                    <td>
                                                      <img width="700" height="80" alt="PiinLife" style="display:block;">
                                                        <xsl:attribute name="src">
                                                          <xsl:value-of select='HDmailHeaderUrl' />
                                                        </xsl:attribute>
                                                      </img>
                                                    </td>
                                                  </tr>

                                                </table>
                                            </div>
                                            <!--MailHeader_End-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <!--MailContent_Start-->
                                            <div id="MailContent">
                                                <table width="700" border="0" cellspacing="0" cellpadding="0"
                                                       style="background-color:white; border-left:1px solid #E1D6CC; border-right:1px solid #E1D6CC;">
                                                    <tr>
                                                        <td height="10" colspan="3"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="698" border="0" cellspacing="0" cellpadding="0"  align="center" style="font-size:13px; color:#414141;">
                                                                <tr>
                                                                    <td width="25"></td>
                                                                    <td style="word-break:break-all; word-wrap:break-word; text-align:left;">
                                                                        <p>
                                                                            親愛的<xsl:value-of select="UserName"/>，您好︰
                                                                        </p>
                                                                        <p>
                                                                            您的憑證檔已經附件在信件中，請下載列印即可使用。
                                                                        </p>
                                                                        <br />
                                                                        <hr style="border-top:none; border-bottom:1px solid #C8C8C8" />
                                                                        <p>
                                                                            ※ 品生活客服人員不會主動以E-mail或電話要求您更改結帳方式或提供個人資料，若您接獲類似訊息，<br />
                                                                            　 請拒絕回應，並立刻與品生活客服人員聯絡。<br />
                                                                            ※ 此信函為系統自動發出，請勿直接回覆，有任何問題請至<a href="https://www.17life.com/Ppon/NewbieGuide.aspx" target="_blank" style="color:#FC7D49;">客服信箱</a>與我們聯繫。
                                                                        </p>
                                                                    </td>
                                                                    <td width="25"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10" colspan="3"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <!--MailContent_End-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <!--MailFooter_Start-->
                                            <div id="MailFooter">
                                                <table width="700" height="80" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                      <tr>
                                                        <td>
                                                          <img width="700" height="80" alt="PiinLife" style="display:block;" >
                                                            <xsl:attribute name="src">
                                                              <xsl:value-of select='HDmailFooterUrl' />
                                                            </xsl:attribute>
                                                          </img>
                                                        </td>
                                                      </tr>
                                                    </tr>
                                                    <tr>
                                                        <td height="20"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <!--MailFooter_End-->
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
﻿<%@ Argument Name="account_id" Type="string" %>
<%@ Argument Name="apply_password" Type="string" %>
<%@ Argument Name="site_url" Type="string" %>
<%@ Argument Name="hour_limit" Type="string" %>
<html>
<body>
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" style="background-color:#EBE0CF; border:solid; border-color:#CBB48F; border-width:1px;">
  <tr>
    <td width="800" height="72" align="center" valign="bottom">
       <table width="795" height="60" border="0" cellspacing="0" cellpadding="0" style="background-color:#EBE0CF; font-family:'微軟正黑體'; color:#4e2d13; font-size:18px; font-weight:bold;">
                <tr>
                  <td width="10" height="5"></td>
                  <td width="140" ></td>
                  <td width="595"></td>
                </tr>
                <tr>
                   <td width="10"></td>
                   <td width="140" height="50">
                      <a href="<%=site_url%>">
                      <img src="https://www.17life.com/Themes/default/images/17Life/NewMember/17lifelogo.jpg" width="136" height="60" border="0"/></a></td>
                   <td width="595" valign="bottom" align="left" style="text-align:left;">屬於您的生活團購網</td>
                </tr>
                <tr>
                   <td width="10" height="5"></td>
                   <td></td>
                   <td ></td>
                </tr>
             </table>
    </td>
  </tr>
   <tr>
    <td align="center" valign="middle" >
           <table width="795" style="background-color:#d4c0a2;" border="0" cellspacing="0" cellpadding="0">
             <tr>
               <td height="10"></td>
             </tr>
             <tr>
               <td valign="middle" align="center" >
                    <table align="center" width="780"border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed; font-family:'新細明體'; line-height:27px; color:#333; font-size:15px; background-color:#FFF; border:dashed; border-color:#C9AB98; border-width:1px;">
                        <tr>
                          <td width="10"></td>
                          <td align="left" style="word-wrap: break-word;word-break: break-all; text-align:left; line-height:35px;color:#333;">
                           您好︰<br/>
                           17Life商家系統所屬帳號 <%=account_id%> 的臨時密碼為：<%=apply_password%><br/>
						   臨時密碼是由系統因申請而主動發出，將於 <%=hour_limit%> 小時後或重設密碼後失效，請儘速登入系統，並進行密碼重設。<br/>
							※ 本信件由系統發送，若您無此需求，請忽略本信件，勿直接回覆。<br/>
							※ 如有其他相關問題，則請與我們的服務/業務人員聯繫 ( 17Life 代表號：(02)25219131，週一~週五 09:00~18:00 )<br/>
                            <div style="text-align:right">17Life  敬上。</div>
                         </td>
                         <td width="10" ></td>
                       </tr>
                    </table>
               </td>
             </tr>
             <tr>
               <td height="10"></td>
             </tr>
           </table>
    </td>
  </tr>
   <tr>
    <td height="43" align="center" valign="top">
      <table width="795"  border="0" cellspacing="0" cellpadding="0" style="background-color:#EBE0CF;">
       <tr>
          <td>
             <table width="400" border="0" align="right" cellpadding="0" cellspacing="0" style="font-family:'新細明體'; font-size:15px; color:#4e2d13;">
               <tr>
                  <td height="5"></td>
                  <td></td>
                  <td></td>
               </tr>
               <tr>
                  <td height="30">&nbsp;</td>
                  <td width="90"></td>
                  <td width="75"></td>
               </tr>
               <tr>
                  <td height="5"></td>
                  <td></td>
                  <td></td>
               </tr>
             </table>
            </td>
         </tr>
       </table>
    </td>
  </tr>
</table>
</body>
</html>
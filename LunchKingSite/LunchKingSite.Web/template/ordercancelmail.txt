﻿<%@ Argument Name="member_name" Type="string" %>
<%@ Argument Name="order_id" Type="string" %>
<%@ Argument Name="order_guid" Type="string" %>
<%@ Argument Name="item_name" Type="string" %>
<%@ Argument Name="site_url" Type="string" %>
<%@ Argument Name="site_service_url" Type="string" %>
<%@ Argument Name="coupon_counts" Type="string" %>
<%@ Argument Name="return_coupon_counts" Type="string" %>
<%@ Argument Name="apply_cancel" Type="bool" %>
<%@ Argument Name="cancel_success" Type="bool" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>退貨通知</title>
</head>
<body>
    <div style="width: 100%; margin: 0 auto; font-family: Arial, Microsoft JhengHei, Helvetica, sans-serif">
        <!--EDM Notification-Start-->
        <!--EDM Notification-End-->
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="font-size: 13px; background: #fff;">
                    <table width="770" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <tr>
                                <td>
                                    <!--EDM Header-Start-->
                                    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE; color: #333;">
                                        <tr>
                                            <td width="20" height="80">
											</td>
											<td width="140" height="80">
												<a href="https://www.17life.com" target="_blank">
													<img alt="17Life" src="https://www.17life.com/Themes/PCweb/images/EDMLOGO.png"
														width="132" height="80" /></a>
											</td>
											<td width="480" height="80" style="font-size: 26px; font-weight: bold;">
												<a href="https://www.17life.com/piinlife/" target="_blank">
													<img alt="17Life" src="https://www.17life.com/Themes/default/images/17Life/EDM/EDMLOGO_HD.png"
														width="260" height="80" /></a>
											</td>
											<td width="110" height="80" style="text-align: right;">
												<a href="https://www.17life.com" target="_blank" style="color: #999;">www.17life.com</a>
												<a href="https://www.17life.com/piinlife/" target="_blank" style="color: #999;">www.Piinlife.com</a>
											</td>
											<td width="20" height="80">
											</td>
                                        </tr>
                                    </table>
                                    <!--EDM Header-End-->
                                    <!--EDM Main-Start-->
                                    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE;">
                                        <tr>
                                            <td width="25"></td>
                                            <td width="740">
                                                <table width="740" cellpadding="0" cellspacing="0" align="center">
                                                    <tr>
                                                        <td height="15" style="border-top: 1px solid #DDD;"></td>
                                                    </tr>
                                                </table>
                                                <table width="740" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: '微軟正黑體'; color: #000; font-size: 12px; background-color: #FFF; border: 1px solid #DDD;">
                                                    <tr>
                                                        <td height="5"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <!--Content Area Start-->
                                                            <table width="700" border="0" cellspacing="1" cellpadding="1" style="background-color: #FFF; text-align: left;">
                                                                <tr>
                                                                    <td height="50">親愛的<span style="color: #F60; font-size: 16px;"> <%=member_name%> </span>
                                                                        您好：
                                                                    </td>
                                                                </tr>
																<% if ((bool)apply_cancel) { %>
																	<tr>
																		<td>
																			17Life 已收到您的訂單取消申請，我們將盡速為您處理此訂單，以下為訂單明細：
																		</td>
																	</tr>
																<% } %>
																<% else if ((bool)!apply_cancel && cancel_success){ %>
																	<tr>
																		<td>
																			您的訂單<%=order_id%>已取消，以下為訂單明細：
																		</td>
																	</tr>
																<% } %>
																<% else if ((bool)!apply_cancel && !cancel_success){ %>
																	<tr>
																		<td>
																			您的訂單取消申請，審核後有異常，故無法為您辦理訂單取消。以下為訂單明細：
																		</td>
																	</tr>
																<% } %>
                                                                <tr>
                                                                    <td height="60" style="line-height: 26px;">
                                                                        <span style="font-weight: bold;">訂單編號：</span> <span style="color: #BF0000; font-weight: bold;"><%=order_id%></span><br />
                                                                        <span style="font-weight: bold;">好康名稱：</span> <span style="color: #BF0000; font-weight: bold;"><%=item_name%></span><br />

																		<% if ((bool)apply_cancel) { %>
																			<span style="font-weight: bold;">取消進度：</span> <span style="color: #BF0000; font-weight: bold;">訂單取消中</span><br />
																		<% } %>
																		<% else if ((bool)!apply_cancel && cancel_success){ %>
																			<span style="font-weight: bold;">取消進度：</span> <span style="color: #BF0000; font-weight: bold;">訂單已取消</span><br />
																		<% } %>
																		<% else if ((bool)!apply_cancel && !cancel_success){ %>
																			<span style="font-weight: bold;">取消進度：</span> <span style="color: #BF0000; font-weight: bold;">取消訂單失敗</span><br />
																		<% } %>


                                                                        <span style="font-weight: bold;">訂單商品數：</span> <span style="font-weight: bold;"><%=coupon_counts%></span><br />
																		<span style="font-weight: bold;">本次申請取消數：</span> <span style="color: #BF0000; font-weight: bold;"><%=return_coupon_counts%></span><br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20"></td>
                                                                </tr>
                                                                <tr>
																	<td style="border-top:1px dashed #6d9345; margin-top: 10px; margin-bottom: 10px; line-height: 20px;">
																		<% if ((bool)apply_cancel) { %>
																			<table>
																				<tr>
																					<td colspan="2">
																						取消訂單說明：
																					</td>
																				</tr>
																				<tr>
																					<td width="1%" valign="top">
																						◆
																					</td>
																					<td width="*">
																						處理作業時間約10~20個工作天。處理完畢之後，17Life 將會寄發取消成功的通知信給您。
																					</td>
																				</tr>
																				<tr>
																					<td valign="top">
																						◆
																					</td>
																					<td width="*">
																						我們將請廠商不要進行出貨，此筆訂單將會自動取消。若因時間差，導致您收到取貨的簡訊，請勿前往取貨。若您前往取貨，系統便視此訂單成立；屆時若仍需要退貨，請重新於訂單列表中，進行退貨申請。
																					</td>
																				</tr>
																				<tr>
																					<td valign="top">
																						◆
																					</td>
																					<td width="*">
																						17Life 客服人員不會主動以E-mail或電話，要求您更改結帳方式或提供個人資料。若您接獲類似訊息，請拒絕回應，並立刻與 17Life 客服人員聯絡。
																					</td>
																				</tr>
																				<tr>
																					<td valign="top">
																						◆
																					</td>
																					<td width="*">
																						此信函為系統自動發出，請勿直接回覆，有任何問題請至 <a href="<%=site_url + site_service_url%>" style="color: #08C;">客服信箱</a> 與我們聯繫。
																					</td>
																				</tr>
																			</table>
																		<% } %>
																		<% else if ((bool)!apply_cancel && cancel_success){ %>
																			<table>
																				<tr>
																					<td colspan="2">
																						取消訂單說明：
																					</td>
																				</tr>
																				<tr>
																					<td width="1%" valign="top">
																						◆
																					</td>
																					<td width="*">
																						若您未辦理取消訂單的申請，但訂單被取消，可能是因為逾期未取件、超過ATM付款期限或者訂單異常。若有任何疑問，請聯繫17Life客服。
																					</td>
																				</tr>
																				<tr>
																					<td valign="top">
																						◆
																					</td>
																					<td width="*">
																						17Life 客服人員不會主動以E-mail或電話，要求您更改結帳方式或提供個人資料。若您接獲類似訊息，請拒絕回應，並立刻與 17Life 客服人員聯絡。
																					</td>
																				</tr>
																				<tr>
																					<td valign="top">
																						◆
																					</td>
																					<td width="*">
																						此信函為系統自動發出，請勿直接回覆，有任何問題請至 <a href="<%=site_url + site_service_url%>" style="color: #08C;">客服信箱</a> 與我們聯繫。
																					</td>
																				</tr>
																			</table>
																		<% } %>
																		<% else if ((bool)!apply_cancel && !cancel_success){ %>
																			<table>
																				<tr>
																					<td colspan="2">
																						取消訂單說明：
																					</td>
																				</tr>
																				<tr>
																					<td width="1%" valign="top">
																						◆
																					</td>
																					<td width="*">
																						可能因為時間差，導致此次申請失敗。若仍欲辦理退貨，請於前台重新辦理申退。有任何疑問，請聯繫17Life客服。
																					</td>
																				</tr>
																				<tr>
																					<td valign="top">
																						◆
																					</td>
																					<td width="*">
																						7Life 客服人員不會主動以E-mail或電話，要求您更改結帳方式或提供個人資料。若您接獲類似訊息，請拒絕回應，並立刻與 17Life 客服人員聯絡。
																					</td>
																				</tr>
																				<tr>
																					<td valign="top">
																						◆
																					</td>
																					<td width="*">
																						此信函為系統自動發出，請勿直接回覆，有任何問題請至 <a href="<%=site_url + site_service_url%>" style="color: #08C;">客服信箱</a> 與我們聯繫。
																					</td>
																				</tr>
																			</table>
																		<% } %>
																	</td>
																</tr>
                                                            </table>
                                                            <!--Content Area End-->
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="5"></td>
                                                    </tr>
                                                </table>
												<table width="740" cellpadding="0" cellspacing="0" align="center">
													<tr>
														<td height="15" style="border-top: 1px solid #DDD;"></td>
													</tr>
												</table>
                                            </td>
                                            <td width="25"></td>
                                        </tr>
                                    </table>
                                    <!--EDM Main-End-->
                                </td>
                            </tr>
                    </table>
                    <!--EDM Footer-Start-->
                    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #333; color: #FFF;">
                        <tr>
                            <td width="25"></td>
                            <td height="80">
                                <br />
                                <a href="<%=site_url + site_service_url%>" target="_blank" style="color: #FFF">客服中心</a> | <a href="https://www.17life.com/Ppon/WhiteListGuide.aspx" target="_blank"
                                    style="color: #FFF">加入白名單</a> | 服務時間：平日 9:00~18:00<br />
                                17Life 康太數位整合股份有限公司 版權所有 轉載必究 © 2016
                            </td>
                        </tr>
                    </table>
                    <!--EDM Footer-End-->
                </td>
            </tr>
            <tr>
                <td height="25" style="background: #FFF;"></td>
            </tr>
        </table>
    </div>
</body>
</html>
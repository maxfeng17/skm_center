﻿<%@ Assembly Name="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.Core.Component.Template" %>
<%@ Import NameSpace="System.Collections.Generic" %>

<%@ Argument Name="seller_name" Type="string" %>
<%@ Argument Name="site_url" Type="string" %>
<%@ Argument Name="https_site_url" Type="string" %>
<%@ Argument Name="site_service_url" Type="string" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>新增出貨訂單通知</title>
    <style type="text/css">
        .auto-style1 {
            text-align: center;
        }
    </style>
</head>

<body>
    <div style="width: 100%; margin: 0 auto; font-family: Arial, Microsoft JhengHei, Helvetica, sans-serif">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="font-size: 13px; background: #fff;">
                    <table width="770" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <td>
                                <!--EDM Header-Start-->
                                <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE; color: #333;">
                                    <tr>
                                        <td width="20" height="80"></td>
                                        <td width="110" height="80">
                                            <a href="https://www.17life.com" target="_blank">
                                                <img src="https://www.17life.com/Themes/PCweb/images/EDMLOGO.png" width="110" height="80" alt="" border="0" />
                                            </a>
                                        </td>
                                        <td width="480" height="80" style="font-size: 26px; font-weight: bold;">
                                            <a href="https://www.17life.com/piinlife/" target="_blank">
                                                <img src="https://www.17life.com/Themes/default/images/17Life/EDM/EDMLOGO_HD.png" width="260" height="80" alt="" border="0" />
                                            </a>
                                        </td>
                                        <td width="110" height="80" style="text-align: right;"></td>
                                        <td width="20" height="80"></td>
                                    </tr>
                                </table>
                                <!--EDM Header-End-->
                                <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE;">
                                    <tr>
                                        <td width="15"></td>
                                        <td width="740">
                                            <!--Ad Area-Start-->
                                            <table width="740" cellpadding="0" cellspacing="0" align="center">
                                                <tr>
                                                    <td height="15" style="border-top: 1px solid #DDD;"></td>
                                                </tr>
                                            </table>
                                            <!--Ad Area-End-->

                                            <table width="740" cellpadding="0" cellspacing="0" align="center" style="background: #FFF; padding: 10px; border: 1px solid #DDD;">
                                                <tr>
                                                    <td>
                                                        <table width="720" border="0" align="center" cellpadding="0" cellspacing="0" style="color: #000; font-size: 13px; background-color: #FFF;">
                                                            <tr>
                                                                <td height="5"></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <table width="720" border="0" cellspacing="1" cellpadding="1" style="background-color: #FFF; text-align: left;">
                                                                        <tr>
                                                                            <td height="30">親愛的親愛的供應商夥伴，您好：
                                                                            </td>
                                                                        </tr>                                                                        
                                                                        <tr>
                                                                            <td height="40"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="auto-style1">
                                                                                    <!-- 案件內容-->
                                                                                    <div style="text-align:left; line-height:1.5">                                                                                       
                                                                                        商品於 Google shopping Ads (GSA) 曝光，業績近150%成長！消費者搜尋精準度UP！<br />
                                                                                        根據近期市場數據報告顯示，商品曝光於Google shopping Ads (GSA)，能有效提升商品搜尋的精準度，並且直接為您的商品帶來銷售業績成長！<br />
                                                                                        <br />
                                                                                        如何讓您的商品趕上這波成長動能？<br />
                                                                                        僅須完成以下兩項任務<br />
                                                                                        <div style="margin: 10px;">
                                                                                            1. 商品提案時的詳細資訊，包含:<br />
                                                                                            <div style="margin-left: 10px;">
                                                                                                - 品牌名稱<br />
                                                                                                - 商品 <a href="https://support.google.com/merchants/answer/6219078?hl=zh-Hant" target="_blank" style="color: cornflowerblue">GTIN</a> (全球交易品項識別碼)<br />
                                                                                                - 商品 <a href="https://support.google.com/merchants/answer/6324482?hl=zh-Hant" target="_blank" style="color: cornflowerblue">MPN</a> (製造商零件編號)<br />
                                                                                            </div>
                                                                                            <br/>
                                                                                            2. 商品的去背圖
                                                                                        </div>
                                                                                        <br />
                                                                                        詳細填寫規範與教學如下方連結，您亦可至商家系統左側功能區「文件下載-系統教學」自行下載檢閱。<br />
                                                                                        若有任何不清楚的地方，請您與17Life團隊聯絡，謝謝。<br />
                                                                                        <br />
                                                                                        文件連結: <a href="<%=site_url%>/vbs/VbsDocumentDownloadSys?t=1&d=2" target="_blank" style="color: cornflowerblue">17Life_Google去背圖xGTIN教學_V1.1</a>
                                                                                        <br />
                                                                                    </div>
                                                                                </div>
                                                                                <!-- 案件內容-->

                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="40"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>※ 本信件為系統自動發送與統計，請勿直接回覆，若您有任何問題請與您的服務專員連繫，謝謝。
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="5"></td>
                                                </tr>
                                            </table>

                                        </td>
                                        <td width="15"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="770" cellpadding="0" cellspacing="0" align="center" style="font-size: 13px; background: #333; color: #FFF;">
                        <tr>
                            <td width="25"></td>
                            <td height="80">
                                <br />
                                <a href="<%=site_url + site_service_url%>" target="_blank" style="color: #FFF">客服中心</a>
                                |
              <a href="https://www.17life.com/Ppon/WhiteListGuide.aspx" target="_blank" style="color: #FFF">加入白名單</a>
                                | 
    服務時間：平日 9:00~18:00
              <br />
                                17Life 康太數位整合股份有限公司 版權所有 轉載必究 © 2016
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>

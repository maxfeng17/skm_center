﻿<%@ Argument Name="member_name" Type="string" %>
<%@ Argument Name="event_name" Type="string" %>
<%@ Argument Name="item_quantity" Type="int" %>
<%@ Argument Name="amount" Type="decimal" %>
<%@ Argument Name="withdrawal_promotion_value" Type="string" %>
<%@ Argument Name="site_url" Type="string" %>

<html>
<body>
<table width="900" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td><img src="https://www.17life.com/Themes/default/images/17Life/G1-8/17PEmail_Top.jpg" /></td>
  </tr>
  <tr>
    <td>
    <br />
    <p>親愛的&nbsp;<font color="#FF00FF"><%=member_name%></font>&nbsp;您好</p>
	<p>17Life已收到您的<天天一起P好康>退貨申請書通知，目前正為您處理中。</p>
    <hr color="#BF008B" />
    <table width="850" border="0" cellspacing="0" cellpadding="0" align="center">
      <tr>
        <td>
        <p>
        <font size="+2" color="#FF0000"><strong><%=event_name%></strong></font><br />
        份數：<font color="#FF0000"><%=item_quantity.ToString()%></font><br />
		<br />
		刷卡退回金額：<font color="#FF0000">$<%=amount.ToString("N0")%></font><br />
		紅利退回金額：<font color="#FF0000">$<%=withdrawal_promotion_value%></font><br />
		PayEasy購物金退回金額：<font color="#FF0000">$0</font><br />
		<br />

        <p><strong><font size="+2">退款須知：</font></strong></p>
		※退貨後約7至14個工作天退至信用卡帳戶。<br />
		※請依您的結帳日判斷，刷退款項可能列於本月或次月帳單。<br/>
		</td>
      </tr>
    </table>
    <hr color="#BF008B" />
	<p>感謝您參與17Life！也別忘了天天回來<a href="<%=site_url%>/ppon/">一起P好康</a>喔!</p>   
	<p>17Life 敬上</p>
    <br />
	</td>
  </tr>
  <tr>
    <td></td>
  </tr><img src="https://www.17life.com/Themes/default/images/17Life/G1-8/17PEmail_Bottom.jpg" />
</table>
</body>
</html>
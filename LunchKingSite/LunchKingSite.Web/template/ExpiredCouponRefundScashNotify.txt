<%@ Assembly Name="LunchKingSite.Core" %>
<%@ Import NameSpace="LunchKingSite.Core.Component" %>
<%@ Import NameSpace="System.Collections.Generic" %>
<%@ Import NameSpace="LunchKingSite.Core.Component.Template" %>  
<%@ Argument Name="member_name" Type="string" %>
<%@ Argument Name="site_url" Type="string" %>
<%@ Argument Name="site_service_url" Type="string" %>
<%@ Argument Name="order_id" Type="string" %>
<%@ Argument Name="item_url" Type="string" %>
<%@ Argument Name="item_name" Type="string" %>
<%@ Argument Name="item_price" Type="string" %>
<%@ Argument Name="refund_scash" Type="string" %>
<%@ Argument Name="member_scash" Type="string" %>
<%@ Argument Name="ad_deals" Type="List<ExpiredCouponRefundScashNotify.AdDeal>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>17P New EDM</title>
</head>

<body>
<div style=" width:100%; margin:0 auto; font-family:Arial, Microsoft JhengHei, Helvetica, sans-serif">
	<!--EDM Notification-Start-->
    <!--EDM Notification-End-->
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td style="font-size:13px; background:#fff;">

    <table width="770" cellpadding="0" cellspacing="0" align="center">
    
    <!--EDM Header-Start-->
    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background:#F3F3EE; color:#333;">
    
    <tr>
    <td width="20" height="80"></td>
    <td width="140" height="80">
    <a href="https://www.17life.com" target="_blank"><img src="https://www.17life.com/Themes/PCweb/images/EDMLOGO.png" width="132" height="80" alt="" border="0" /></a>
    </td>
    <td width="480" height="80" style="font-size:26px; font-weight:bold;"></td>
    <td width="110" height="80" style="text-align:right;"><a href="https://www.17life.com" target="_blank" style="color:#999;">www.17life.com</a></td>
    <td width="20" height="80"></td>
    </tr>
    </table>
    <!--EDM Header-End-->
    <!--EDM Main-Start-->
    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background:#F3F3EE;">
    <tr>
    <td width="25"></td>
    <tr>
    <td align="center">
    <table width="740" cellpadding="0" cellspacing="0" align="center">
    	<tr><td height="15" style="border-top:1px solid #DDD;"></td></tr>
        <tr>
        </tr>
        </table>
    <table width="740" border="0" cellspacing="0" cellpadding="0" bgcolor="#6d9345">
       <tr>
         <td >
         
            <table width="740" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:'Microsoft JhengHei'; color:#000; line-height:30px; font-size:14px; background-color:#FFF;border:1px solid #DDD;">
        <tr>
           <td height="5"></td>
        </tr>
        <tr>
           <td align="center">
           
              <table width="700" border="0" cellspacing="1" cellpadding="1" style="background-color:#FFF;font-weight: bold; text-align:left;">
                <tr>
                  <td height="50">親愛的<span style="color:#F60; font-size:16px;"><%=member_name%></span>您好：
               <tr>
                  <td>
					由於您的憑證逾期未使用，17Life已自動為您辦理轉購物金，且已成功轉入您的帳戶。				  
                  </td>
               </tr>
               <tr>
                  <td>
          此購物金無使用期限，您可於購買任一好康時（需登入會員）進行扣抵。         
                  </td>
               </tr>
               <tr>
                  <td height="15">                   
                  </td>
               </tr>
               
              <tr>
                 <td>
                   <table cellpadding="5" height="6" cellspacing="0" border="1" bordercolor="#000000" width="100%" style="border-collapse:collapse;">
                    <tr style="font-size:14px; line-height:24px; background:#bf0000; color:#fff;">
                      <th align="center">訂單編號</th>
                      <th align="center">好康名稱</th>
                      <th align="center">轉購物金</th>
                      <th align="center">您的購物金餘額</th>
                    </tr>
                    <tr style="align:left; padding:40px 0 40px 0; font-weight:bold; font-size:13px;line-height:18px;">
                      <td align="center"><%=order_id%></td>
                      <td>
                        <a href="<%=item_url%>"><%=item_name%></a>
                        <br>
                        <span style="color:#666;">好康價$<%=item_price%></span>
                      </td>
                      <td align="center">$<%=refund_scash%></td>
                      <td align="center">$<%=member_scash%></td>
                    </tr>
                   </table >
                 </td>
              </tr>
              <tr>
                  <td height="15">                   
                  </td>
               </tr>
              <tr>
                  <td>
          ◆ 您可隨時至會員中心><a href="https://www.17life.com/User/BonusListNew.aspx" target="_blank">餘額明細</a>查詢購物金餘額         
                  </td>
               </tr>
               <tr>
                  <td>
          ◆ 17Life折價券為一次折抵，退貨不返還         
                  </td>
               </tr>
               <tr>
                  <td>
          ◆ 若您的訂單中含多張憑證，僅退還未使用且已逾期的憑證
                  </td>
               </tr>
              <tr>
                                                                <td height="100" style="line-height:25px; font-size: 13px;">
                                                                    17Life 敬上<br/>
                                                                    此信函為系統發出，請勿直接回覆，有任何問題請至  <a href="<%=site_url + site_service_url%>" target="_blank">客服中心</a> 聯繫。
                                                                </td>
                                                            </tr>
             </table>

          </td>
        </tr>
        <tr>
           <td height="5"></td>
        </tr>
      </table>
             
         </td>
       </tr>
     </table>
     <!-- ad deals -->
		<% if (ad_deals.Count>0) { %>
		<table width="740" align="center">
			<tr>
				<td align="center">
				</td>
			</tr>
			<tr>
				<td>
					<table width="740" align="center" style="background-color: #ffffff; border: 1px solid #DDD;">
						<!--DealSixBlock_Title--Start-->
						<tr>
							<td height="28" style="padding-left: 10px; color: #FFF; background-color: #BF0000;
								font-weight: bold; font-family: '微軟正黑體';">
								限時好康推薦
							</td>
						</tr>
						<!--DealSixBlock_Title--End -->
						<tr>
							<td align="center">
								<table width="710">
									<tr>
										<td height="7" colspan="3">
										</td>
									</tr>
									<%for (int i=0;i<ad_deals.Count;i++) {%>
									<tr>
										<%for (int j=0;j<3;j++) {
											int idx = i*3+j;
											if (idx >= ad_deals.Count) break;
											ExpiredCouponRefundScashNotify.AdDeal deal = ad_deals[idx];
										%>
										<td width="230" align="center">
											<table width="225" style="border: #BF0000 solid 3px; background-color: #FFF;">
												<tr>
													<td>
														<a href="<%=deal.Url%>">
															<img src="<%=deal.Pic%>" width="215" height="123" border="0" alt="<%=deal.Title%>" /></a>
													</td>
												</tr>
												<tr>
													<td align="center">
														<div style="height: 42px">
															<a href="<%=deal.Url%>" style="font-size: 13px; font-family: '微軟正黑體'; color: #000;
																text-decoration: none; line-height: 20px;">
																<%=deal.Title%></a>
														</div>
													</td>
												</tr>
												<tr>
													<td align="center">
														<a href="<%=deal.Url%>" style="color: #F60; font-size: 18px; font-weight: bold;">特價$<%=deal.Price%>
															搶購</a>
													</td>
												</tr>
												<tr>
													<td height="3">
													</td>
												</tr>
											</table>
										</td>
										<% } %>
									</tr>
									<tr>
										<td height="7" colspan="3">
										</td>
									</tr>
									<% } %>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height="7">
				</td>
			</tr>
		</table>
		<% } %>
		<br />
    </td>
  </tr>
    <td width="25"></td>
    </tr>
    </table>
    <!--EDM Main-End-->
    </td></tr>
    </table>
    <!--EDM Footer-Start-->
    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background:#333; color:#FFF;">
    <tr>
    <td width="25"></td>
    <td height="80">
    <br />
    <a href="<%=site_url + site_service_url%>" target="_blank" style="color:#FFF">客服中心</a> | 
    <a href="https://www.17life.com/Ppon/WhiteListGuide.aspx" target="_blank" style="color:#FFF">加入白名單</a> | 
    服務時間：平日 9:00~18:00<br />
	17Life 康太數位整合股份有限公司 版權所有 轉載必究 © 2016
    </td>
    </tr>
    </table>
    <!--EDM Footer-End-->

</td>
</tr>
<tr>
<td height="25" style="background:#FFF;"></td>
</tr>
</table>

</div>
</body>
</html>

﻿<%@  Argument Name="site_url" Type="string" %>
<%@  Argument Name="site_service_url" Type="string" %>
<%@  Argument Name="member_name" Type="string" %>
<%@  Argument Name="event_name" Type="string" %>
<%@  Argument Name="discount_name" Type="string" %>
<%@  Argument Name="discount_amount" Type="string" %>
<%@  Argument Name="discount_limit" Type="string" %>
<%@  Argument Name="deal1title" Type="string" %>
<%@  Argument Name="deal1pic" Type="string" %>
<%@  Argument Name="deal1url" Type="string" %>
<%@  Argument Name="deal1price" Type="string" %>
<%@  Argument Name="deal2title" Type="string" %>
<%@  Argument Name="deal2pic" Type="string" %>
<%@  Argument Name="deal2url" Type="string" %>
<%@  Argument Name="deal2price" Type="string" %>
<%@  Argument Name="deal3title" Type="string" %>
<%@  Argument Name="deal3pic" Type="string" %>
<%@  Argument Name="deal3url" Type="string" %>
<%@  Argument Name="deal3price" Type="string" %>
<%@  Argument Name="deal4title" Type="string" %>
<%@  Argument Name="deal4pic" Type="string" %>
<%@  Argument Name="deal4url" Type="string" %>
<%@  Argument Name="deal4price" Type="string" %>
<%@  Argument Name="deal5title" Type="string" %>
<%@  Argument Name="deal5pic" Type="string" %>
<%@  Argument Name="deal5url" Type="string" %>
<%@  Argument Name="deal5price" Type="string" %>
<%@  Argument Name="deal6title" Type="string" %>
<%@  Argument Name="deal6pic" Type="string" %>
<%@  Argument Name="deal6url" Type="string" %>
<%@  Argument Name="deal6price" Type="string" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <title>17P New EDM</title>

  <link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
  <style>
    

    /* 讓按鈕有hover效果(但gmail中無效) */
    .goahead_btn:hover{
        font-size: 24px!important;
    }

    /* 讓說明文字與call for action按鈕在手機中放大(但gmail中無效) */
    @media screen and  (max-width: 480px) {
      .once_again_msg{
        font-size: 24px!important;
        line-height: 1.2em!important;
      }
      .goahead_btn,.goahead_btn:hover{
        font-size: 36px!important;
      }
    }

  </style>

</head>

<body>
  <div style=" width:100%; margin:0 auto; font-family:Arial, Microsoft JhengHei, Helvetica, sans-serif">
    <!--EDM Notification-Start-->
    <!--EDM Notification-End-->
    <table cellpadding="0" cellspacing="0" width="100%">
  <tbody  >
    <tr>
      <td style="background:rgb(255,255,255) none repeat scroll 0% 50%;font-size:13px">
      <table align="center" cellpadding="0" cellspacing="0" width="770">
        <tbody>
          <tr>
            <td>
            <!-- 17life EDM Logo -->
            <table style="background:rgb(243,243,238) none repeat scroll 0% 50%;color:rgb(51,51,51)" align="center" cellpadding="0" cellspacing="0" width="770">
              <tbody>
                <tr>
                  <td height="80" width="19"><br>
                  </td>
                  <td height="80" width="139"> <a href="https://www.17life.com/ppon/default.aspx?rsrc=eDM_sale" target="_blank"><img src="https://www.17life.com/Themes/PCweb/images/EDMLOGO.png" alt="" border="0" height="96" width="132" class="CToWUd"></a> </td>
                  <td style="font-size:26px;font-weight:bold" height="80" width="337"><br>
                  </td>
                  <td style="text-align:right" height="80" width="250"></td>
                  <td height="80" width="23"><br>
                  </td>
                </tr>
              </tbody>
            </table>

            <table style="background:rgb(243,243,238) none repeat scroll 0% 50%" align="center" cellpadding="0" cellspacing="0" width="770">
              <tbody>
                <tr>
                  <td width="25"><br>
                  </td>
                  <td width="740">
                  <table align="center" cellpadding="0" cellspacing="0" width="740">
                    <tbody>
                      <tr>
                        <td style="border-top:1px solid rgb(221,221,221)" height="15"><br>
                        </td>
                      </tr>

                    </tbody>
                  </table>

                  <table style="border:0px solid #4b7a18;color:rgb(0,0,0);font-size:13px;background-color:rgb(255,255,255)" align="center" border="0" cellpadding="0" cellspacing="0" width="740">
                    <tbody>
                      <tr>
                        <td align="center">
                        <table border="0" cellpadding="0" cellspacing="0" width="700">
                          <tbody>
                            <tr>
                              <td height="20"><br>
                              </td>
                            </tr>

                            <!-- 領折價券按鈕 Start -->
                            <tr style="background-color:#7ac82c;background-image:url(https://www.17life.com/images/17P/active/20161111_login/bg2.png);cursor:pointer;" >
                              <td colspan="5" height="245" style="overflow:hidden;font-family:'\005fae\008edf\006b63\009ed1\009ad4';font-size:18px;color:#666;line-height:25px;">

                                <!-- 繼續使用折價券的超連結，由此帶入 -->
                                <a href="https://www.17life.com/event/exhibitionlist.aspx" style="text-decoration:none;color:#fff;">
                                  <div style="border: 3px dashed #fff;margin: 10px;height: 219px;width: 96%;display: table;">
                                    <p align="left" class="once_again_msg" style="margin:30px;font-size:20px;text-align:center;color:#fff;vertical-align: middle;display: table-cell;">
                                      
                                       <span class="">
                                         17Life雙11天天開展!! 總額逾$50,000無限次現抵折價券大方送!!
                                         <br />
                                         <%=discount_name%> ,已回饋至您的帳戶!!
                                         <br />
                                       </span>
                                       <!-- 折價券金額，從2位數到5位數皆不跑版 -->
                                       <span style="font-size:96px;line-height:1em;font-weight:bold;margin-left:140px;font-family:'Open Sans',Arial,sans-serif;">$<%=discount_amount%></span>
                                       <!-- 折價券使用條件，從2位數到5位數皆不跑版，如決定不顯示條件，請依然保留此<span>、以免影響置中對齊 -->
                                       <span style="margin-top: 62px;color:#4b7a18;display:inline-block;width:140px;text-align:left;">
                                         滿<%=discount_limit%>可用
                                       </span>
                                       <br />
                                       <span style="margin-top:10px;font-size:22px;text-decoration:none;border-bottom: 3px solid #fff;padding-bottom: 4px;color:#fff;margin-left:0.8em;padding-left:0.4em;display:inline-block;" class="goahead_btn">立即使用折價券 》</span>
                                    </p>

                                  </div>
                                </a>


                              </td>
                            </tr>
                            <!-- 領折價券按鈕 End -->

                            <!-- 雙11檔次title -->
                            <tr>
                              <td height="5"><br>
                              </td>
                            </tr>
                            <tr>
                              <td colspan="5">
                                
                                <!-- 以下開始為new7.html之HTML結構 -->
                                <table width="700" align="center">
                                  <tbody><tr>
                                      <td align="center"></td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <table width="700" align="center" style="">
                                              <!--DealSixBlock_Title--Start-->
                                              <tbody><tr>
                                                  <td  style="background:;font-family:'\005fae\008edf\006b63\009ed1\009ad4';font-size:21px;color:#fff;padding-left:0px;line-height:35px;border-bottom:1px solid #7ac82c;color:#666;">
													雙11強檔推薦
												  </td>
                                              </tr>
                                              <!--DealSixBlock_Title--End -->

                                               <!-- 六檔雙11商品 Start-->
                                              <tr>
                                                  <td align="center" width="700">
                                                      <table width="700" style="border-collapse: collapse;">
                                                          <tbody><tr>
                                                              <td height="7" colspan="3"></td>
                                                          </tr>
                                                          <tr width="700">
                                                              <!--DealBlock_1--Start-->
                                                              <td width="233" align="center" style="padding:0;border: 1px solid #7ac82c;">
                                                                  
                                                                  <table width="233" style="background-color: #FFF;width:;height:180px;padding-top:10px;padding-bottom:10px;">
                                                                      <tbody><tr>
                                                                          <td align="center">
                                                                              <a href="https://www.17life.com/ppon/detail.aspx?bid=e912054e-c209-46d8-a5c7-0ecad93c0c34">
                                                                                  <img src="https://www.17life.com/media/A0-04-957/e912054eEDMc209EDM46d8EDMa5c7EDM0ecad93c0c34.jpg" width="215" height="123" border="0" alt="床包/被套/AGAPE/四件式/雙人/棉被"></a>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td align="center">
                                                                              <div style="width:220px;min-height:40px;line-height:20px;word-break:break-all;overflow:hidden">
                                                                                  <a href="https://www.17life.com/ppon/detail.aspx?bid=e912054e-c209-46d8-a5c7-0ecad93c0c34" style="font-size: 13px; font-family: '微軟正黑體'; color: #000; text-decoration: none; line-height: 20px;">
                                                                                      AGAPE-2015年秋冬百貨專櫃新花被套床包組標準雙人四件式
																				  </a>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td align="center">
                                                                              <a href="https://www.17life.com/ppon/detail.aspx?bid=e912054e-c209-46d8-a5c7-0ecad93c0c34" style="color:#f60;font-size:20px">特價$780
                                                                                  搶購</a>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td height="3"></td>
                                                                      </tr>
                                                                  </tbody></table>
                                                              </td>
                                                              <!--DealBlock_1--End-->
                                                              <!--DealBlock_2--Start-->
                                                              <td width="233" align="center" style="padding:0;border: 1px solid #7ac82c;">
                                                                  <table width="" style="background-color: #FFF;width:;height:180px;padding-top:10px;padding-bottom:10px;">
                                                                      <tbody><tr>
                                                                          <td align="center">
                                                                              <a href="https://www.17life.com/ppon/detail.aspx?bid=2de6f68b-b1e6-4ab7-a0fb-093c8b805e92">
                                                                                  <img src="https://www.17life.com/media/A0-09-701/2de6f68bEDMb1e6EDM4ab7EDMa0fbEDM093c8b805e92.jpg" width="215" height="123" border="0" alt="大尺寸/外銷/日本/珪藻土/地墊/杯墊/防滑/吸水/珪藻土地墊/珪藻土杯墊/腳踏墊/杯墊"></a>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td align="center">
                                                                              <div style="width:220px;min-height:40px;line-height:20px;word-break:break-all;overflow:hidden">
                                                                                  <a href="https://www.17life.com/ppon/detail.aspx?bid=2de6f68b-b1e6-4ab7-a0fb-093c8b805e92" style="font-size: 13px; font-family: '微軟正黑體'; color: #000; text-decoration: none; line-height: 20px;">
                                                                                      大尺寸外銷日本珪藻土速吸水地墊(買二送二)</a>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td align="center">
                                                                              <a href="https://www.17life.com/ppon/detail.aspx?bid=2de6f68b-b1e6-4ab7-a0fb-093c8b805e92" style="color:#f60;font-size:20px">特價$899
                                                                                  搶購</a>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td height="3"></td>
                                                                      </tr>
                                                                  </tbody></table>
                                                              </td>
                                                              <!--DealBlock_2--End-->
                                                              <!--DealBlock_3--Start-->
                                                              <td width="233" align="center" style="padding:0;border: 1px solid #7ac82c; ">
                                                                  <table width="" style="background-color: #FFF;width:;height:180px;padding-top:10px;padding-bottom:10px;">
                                                                      <tbody><tr>
                                                                          <td align="center">
                                                                              <a href="https://www.17life.com/ppon/detail.aspx?bid=3aad4605-7e5f-4856-a1a2-ff907e8b2350">
                                                                                  <img src="https://www.17life.com/media/A0-10-840/3aad4605EDM7e5fEDM4856EDMa1a2EDMff907e8b2350.jpg" width="215" height="123" border="0" alt="Kolin/歌林/負離子/微電腦/電子/除濕機 /KJ-HC02"></a>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td align="center">
                                                                              <div style="width:220px;min-height:40px;line-height:20px;word-break:break-all;overflow:hidden">
                                                                                  <a href="https://www.17life.com/ppon/detail.aspx?bid=3aad4605-7e5f-4856-a1a2-ff907e8b2350" style="font-size: 13px; font-family: '微軟正黑體'; color: #000; text-decoration: none; line-height: 20px;">
                                                                                      Kolin歌林-負離子微電腦電子除濕機(KJ-HC02)-白</a>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td align="center">
                                                                              <a href="https://www.17life.com/ppon/detail.aspx?bid=3aad4605-7e5f-4856-a1a2-ff907e8b2350" style="color:#f60;font-size:20px">特價$1880
                                                                                  搶購</a>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td height="3"></td>
                                                                      </tr>
                                                                  </tbody></table>
                                                              </td>
                                                              <!--DealBlock_3--End-->
                                                          </tr>
                                                          <tr>
                                                              <!--DealBlock_4--Start-->
                                                              <td width="233" align="center" style="padding:0;border: 1px solid #7ac82c; ">
                                                                  <table width="" style="background-color: #FFF;width:;height:180px;padding-top:10px;padding-bottom:10px;">
                                                                      <tbody><tr>
                                                                          <td align="center">
                                                                              <a href="https://www.17life.com/ppon/detail.aspx?bid=4bc242ff-a53e-4683-9bd7-76111190a823">
                                                                                  <img src="https://www.17life.com/media/A0-06-299/4bc242ffEDMa53eEDM4683EDM9bd7EDM76111190a823.jpg" width="215" height="123" border="0" alt="團購/熱銷/下午茶/美食/樂天/韓國/Samlip/Nuneddine/義式/焦糖/奶油/千層"></a>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td align="center">
                                                                              <div style="width:220px;min-height:40px;line-height:20px;word-break:break-all;overflow:hidden">
                                                                                  <a href="https://www.17life.com/ppon/detail.aspx?bid=4bc242ff-a53e-4683-9bd7-76111190a823" style="font-size: 13px; font-family: '微軟正黑體'; color: #000; text-decoration: none; line-height: 20px;">
                                                                                      韓國樂天Samlip Nuneddine-義式焦糖奶油千層200條(買100條加贈100條)</a>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td align="center">
                                                                              <a href="https://www.17life.com/ppon/detail.aspx?bid=4bc242ff-a53e-4683-9bd7-76111190a823" style="color:#f60;font-size:20px">特價$599
                                                                                  搶購</a>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td height="3"></td>
                                                                      </tr>
                                                                  </tbody></table>
                                                              </td>
                                                              <!--DealBlock_4--End-->
                                                              <!--DealBlock_5--Start-->
                                                              <td width="233" align="center" style="padding:0;border: 1px solid #7ac82c; ">
                                                                  <table width="" style="background-color: #FFF;width:;height:180px;padding-top:10px;padding-bottom:10px;border-left:0;border-right:0;">
                                                                      <tbody><tr>
                                                                          <td align="center">
                                                                              <a href="https://www.17life.com/ppon/detail.aspx?bid=fe50f765-d3e3-426f-aca1-bc915bfbaa9a">
                                                                                  <img src="https://www.17life.com/media/A0-09-064/fe50f765EDMd3e3EDM426fEDMaca1EDMbc915bfbaa9a.jpg" width="215" height="123" border="0" alt="雙11/貓頭鷹/傳統/風味/黑咖啡"></a>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td align="center">
                                                                              <div style="width:220px;min-height:40px;line-height:20px;word-break:break-all;overflow:hidden">
                                                                                  <a href="https://www.17life.com/ppon/detail.aspx?bid=fe50f765-d3e3-426f-aca1-bc915bfbaa9a" style="font-size: 13px; font-family: '微軟正黑體'; color: #000; text-decoration: none; line-height: 20px;">
                                                                                      貓頭鷹-傳統風味黑咖啡(買2盒送2盒)</a>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td align="center">
                                                                              <a href="https://www.17life.com/ppon/detail.aspx?bid=fe50f765-d3e3-426f-aca1-bc915bfbaa9a" style="color:#f60;font-size:20px">特價$339
                                                                                  搶購</a>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td height="3"></td>
                                                                      </tr>
                                                                  </tbody></table>
                                                              </td>
                                                              <!--DealBlock_5--End-->
                                                              <!--DealBlock_6--Start-->
                                                              <td width="233" align="center" style="padding:0;border: 1px solid #7ac82c; ">
                                                                  <table width="" style="background-color: #FFF;width:;height:180px;padding-top:10px;padding-bottom:10px;">
                                                                      <tbody><tr>
                                                                          <td align="center">
                                                                              <a href="https://www.17life.com/ppon/detail.aspx?bid=ba8c35e8-f7db-4f03-9c10-e0378bfcc6c4">
                                                                                  <img src="https://www.17life.com/media/A0-02-363/ba8c35e8EDMf7dbEDM4f03EDM9c10EDMe0378bfcc6c4.jpg" width="215" height="123" border="0" alt="防風/防雨/三穿/保暖/外套"></a>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td align="center">
                                                                              <div style="width:220px;min-height:40px;line-height:20px;word-break:break-all;overflow:hidden">
                                                                                  <a href="https://www.17life.com/ppon/detail.aspx?bid=ba8c35e8-f7db-4f03-9c10-e0378bfcc6c4" style="font-size: 13px; font-family: '微軟正黑體'; color: #000; text-decoration: none; line-height: 20px;">
                                                                                      OUTDOOR 真防風防雨三穿保暖外套</a>
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td align="center">
                                                                              <a href="https://www.17life.com/ppon/detail.aspx?bid=ba8c35e8-f7db-4f03-9c10-e0378bfcc6c4" style="color:#f60;font-size:20px">特價$1249
                                                                                  搶購</a>
                                                                          </td>
                                                                      </tr>
                                                                      <tr>
                                                                          <td height="3"></td>
                                                                      </tr>
                                                                  </tbody></table>
                                                              </td>
                                                              <!--DealBlock_6--End-->
                                                          </tr>
                                                          <tr>
                                                              <td height="7" colspan="3"></td>
                                                          </tr>
                                                      </tbody></table>
                                                  </td>
                                              </tr>
                                              <!-- 六檔雙11商品 End-->
                                          </tbody></table>
                                      </td>
                                  </tr>
                                </tbody></table>
                                <!-- new7.html之HTML結構 End -->

                              </td>
                            </tr>

                          </tbody>
                        </table>
                        </td>
                      </tr>

                      <tr>
                        <td colspan="5" style="font-family:'\005fae\008edf\006b63\009ed1\009ad4';font-size:14px;overflow:hidden;color:#404040;line-height:25px">
                          <blockquote>
                            <p><strong>注意事項：</strong> </p>
                            <ol>
                              <li>此優惠限17Life會員本人使用，若該信箱未認證正式會員，快前往會員認證完成認證享優惠。</li>
                              <li>本優惠僅適用於此策展頁指定商品，且折價券恕無法轉換成現金或其他贈品。</li>
                              <li>參加者於參加本活動之同時，即同意接受本活動注意事項之規範。</li>
                              <li>本活動有不可抗力特殊原因無法執行時，主辦單位「17Life」有權決定取消、終止、修改或暫停本活動。</li>
                              <li> 現金卷適用方式，請詳閱<a href="https://www.17life.com/User/DiscountList.aspx">使用規則</a></li>                             
                            </ol>
                          </blockquote>
                        </td>
                      </tr>



                      <tr>
                        <td align="center" height="70">
                        <table border="0" cellpadding="0" cellspacing="0" width="700">
                          <tbody>
                            <tr>
                              <td align="left" width="105"><a href="https://www.17life.com/Ppon/WhiteListGuide.aspx" target="_blank"><img src="https://ci5.googleusercontent.com/proxy/0Yi2RVFY3N1PfMxbzi1wnIHOzmXnsByz2WAkY1oryLiDDXj5sQ6UqlZmKJ1ori4ldxzQXRDcajyF5heNxIvKG9xmJo6Omq2jnnSJ=s0-d-e1-ft#https://www.17life.com/images/17P/edm/pedmNEW/bn-2.jpg" alt="" border="0" height="25" width="105" class="CToWUd"></a></td>
                              <td align="right" width="370"><a href="https://www.17life.com/Event/ExhibitionJoinPiinlife.aspx?u=2016FathersDay&rsrc=eDM_sale" target="_blank"><img src="https://ci4.googleusercontent.com/proxy/Bvz368cF-MixzPeLb5bBubxCw6ti21bCQAR4BOv71lPGyGU_Kd8A0WlzGmER4nQ6GgjOr39-4hpFS2vzJFhvNoN32g8srQPjMJHp=s0-d-e1-ft#https://www.17life.com/images/17P/edm/pedmNEW/bn-1.jpg" alt="" border="0" height="25" width="90" class="CToWUd"></a></td>
                            </tr>
                          </tbody>
                        </table>
                        </td>
                      </tr>

                    </tbody>
                  </table>
                  <br>
                  </td>
                  <td width="25"><br>
                  </td>
                </tr>
              </tbody>
            </table>
 </td>
          </tr>
        </tbody>
      </table>

      <table style="background:rgb(51,51,51) none repeat scroll 0% 50%;color:rgb(255,255,255)" align="center" cellpadding="0" cellspacing="0" width="770">
        <tbody>
          <tr>
            <td width="25"><br>
            </td>
            <td height="80"><a href="<%=site_url + site_service_url%>" style="color:rgb(255,255,255)" target="_blank">客服中心</a> | <a href="https://www.17life.com/Ppon/WhiteListGuide.aspx" style="color:rgb(255,255,255)" target="_blank">加入白名單</a> | <a href="https://www.17life.com/User/UserAccount.aspx" style="color:rgb(255,255,255)" target="_blank">取消訂閱</a> |
服務時間：平日 9:00~18:00<br>
17Life 康太數位整合股份有限公司 版權所有 轉載必究 © 2016 </td>
          </tr>
        </tbody>
      </table>
</td>
    </tr>
    <tr>
      <td style="background:rgb(255,255,255) none repeat scroll 0% 50%" height="25"><br>
      </td>
    </tr>
  </tbody>
</table>

</div>
</body>
</html>
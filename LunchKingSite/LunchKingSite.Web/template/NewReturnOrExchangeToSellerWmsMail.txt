﻿<%@ Assembly Name="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.Core.Component.Template" %>
<%@ Import NameSpace="System.Collections.Generic" %>
<%@ Import NameSpace="System.Linq" %>

<%@ Argument Name="return_info" Type="List<NewReturnOrExchangeToSellerMail.ProductRefundOrExchangeModel>" %>
<%@ Argument Name="site_url" Type="string" %>
<%@ Argument Name="https_site_url" Type="string" %>
<%@ Argument Name="site_service_url" Type="string" %>
<%@ Argument Name="vbs_url" Type="string" %>
<%@ Argument Name="order_id" Type="string" %>
<%@ Argument Name="ship_infos" Type="List<NewReturnOrExchangeToSellerWmsMail.ShipInfo>" %>
<%@ Argument Name="ship_return_date" Type="string" %>
<%@ Argument Name="check_deadline" Type="string" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>新增出貨訂單通知</title>
    <style type="text/css">
        .auto-style1 {
            text-align: center;
        }
    </style>
</head>


<body>
  <div style=" width:100%; margin:0 auto; font-family:Arial, Microsoft JhengHei, Helvetica, sans-serif">
    <table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td style="font-size:13px; background:#fff;">
          <table width="920" cellpadding="0" cellspacing="0" align="center">
              <tr>
                <td>
                  <!--EDM Header-Start-->
                  <table width="920" cellpadding="0" cellspacing="0" align="center" style="background:#F3F3EE; color:#333;">
                    <tr>
                      <td width="20" height="80"></td>
                      <td width="110" height="80">
                        <a href="https://www.17life.com" target="_blank">
                          <img src="https://www.17life.com/Themes/PCweb/images/EDMLOGO.png" width="110" height="80" alt="" border="0" />
                        </a>
                      </td>
                      <td width="480" height="80" style="font-size:26px; font-weight:bold;">
                        <a href="https://www.17life.com/piinlife/" target="_blank">
                          <img src="https://www.17life.com/Themes/default/images/17Life/EDM/EDMLOGO_HD.png" width="260" height="80" alt="" border="0" />
                        </a>
                      </td>
                      <td width="110" height="80" style="text-align:right;">                                                
                      </td>
                      <td width="20" height="80"></td>
                    </tr>
                  </table>
                  <!--EDM Header-End-->                  
                  <table width="920" cellpadding="0" cellspacing="0" align="center" style="background:#F3F3EE;">
                    <tr>
                      <td width="15"></td>
                      <td width="890">
                        <!--Ad Area-Start-->
                        <table width="890" cellpadding="0" cellspacing="0" align="center">
                          <tr>
                            <td height="15" style="border-top:1px solid #DDD;"></td>
                          </tr>                        
                        </table>
                        <!--Ad Area-End-->
                        
                        <table width="890" cellpadding="0" cellspacing="0" align="center" style="background:#FFF; padding:10px; border:1px solid #DDD;">
                            <tr>
                                <td>
                                    <table width="850" border="0" align="center" cellpadding="0" cellspacing="0" style=" color:#000; font-size:13px; background-color:#FFF;">
                                        <tr>
                                            <td height="5"></td>
                                        </tr>
                                        <tr>
                                            <td align="center">                                   
                                              <table width="850" border="0" cellspacing="1" cellpadding="1" style="background-color:#FFF;text-align:left;">
                                                <tr>
                                                    <td height="30">
                                                        親愛的&nbsp;<span style="font-weight:bold;"><%=return_info.FirstOrDefault().SellerName%>&nbsp;</span>您好:
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td height="40">
                                                        <div style="padding: 0px 0px 0px 0px;line-height: 24px;">
                                                          24H到貨退貨訂單(<%=order_id%>)已於<%=ship_return_date%>退回至貴端，煩請於三日內檢查退回商品狀況，如有品項短缺，破損或是其他瑕疵異常問題，請您優先點選"無法完成退貨"<a href="<%=vbs_url%>">(請點我)</a>，說明原因並立即與17Life企服人員聯繫，<span style="color:red">如逾<%=check_deadline%>未回覆處理，系統將視為收回確認無誤</span>，並會優先完成訂單退款，請您務必留意，如有其他任何問題，也請隨時與我們聯繫，謝謝
                                                            <span style="color:red"></span>
                                                        </div>
                                                    </td>
                                                </tr>   
                                                <tr>
                                                    <td height="40"></td>
                                                </tr>                                                                         
                                                <tr>
                                                    <td>                                                        
                                                        <div class="auto-style1">   <!-- 案件內容-->                                                                                
                                                          <table width="850" cellspacing="0" border="1" bordercolor="#000000" style="border-collapse:collapse";>
                                                              <tr style="font-size: 14px; line-height: 24px; background: #eee; color: #444444;">
                                                                  <td class="auto-style1">退貨<br/>申請日</td>
                                                                  <td class="auto-style1">訂單編號</td>
                                                                  <td class="auto-style1">檔號</td>
                                                                  <td class="auto-style1">收件者姓名</td>
                                                                  <td class="auto-style1">商品名稱</td>
                                                                  <td class="auto-style1">退貨<br/>規格數量</td>
                                                                  <td class="auto-style1">退貨<br/>原因與備註</td>
																  <td class="auto-style1">物流編號</td>
                                                              </tr>
															  <%
                                                              foreach(var info in return_info) {
                                                              %>                                                              
                                                              <tr style="font-size: 12px; line-height: 24px; color: #444444;">
                                                                  <td class="auto-style1"><%=info.CreateTime%></td>
																  <td class="auto-style1"><a href="<%=info.VbsOrderIdUrl%>"><%=info.OrderId%></a></td>
																  <td class="auto-style1"><a href="<%=info.VbsDealIdUrl%>"><%=info.DealUniqueId%></a></td>
                                                                  <td class="auto-style1"><%=info.ReciverName%></td>
                                                                  <td class=""><%=info.ItemName%></td>
                                                                  <td class=""><%=info.SpecAndQuanty%></td>
                                                                  <td class=""><%=info.Reason%></td>
																  <td class="">
																	<%
																	foreach(var ship in ship_infos) {
																	%> 
																	<%=ship.LogisticName%><br /><a href="<%=ship.ShipCompanySite%>"><%=ship.ShipId%></a><br />
																	<%
																	} //end foreach
																	%> 
																	
																  </td>
                                                              </tr>
															  <%
                                                              } //end foreach
                                                              %>                                                              
                                                          </table>
                                                        </div>  <!-- 案件內容-->
                                                                                                               
                                                    </td>
                                                </tr>
												<tr>
													<td height="30"></td>
												</tr>
                                                <tr >
                                                    <td height="60"></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        ※ 本信件為系統自動發送與統計，請勿直接回覆，若您有任何問題請與您的服務專員連繫，謝謝。
                                                    </td>
                                                </tr>
                                              </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                              <td height="5"></td>
                            </tr>
                        </table>

                       </td>
                      <td width="15"></td>
                    </tr>
                  </table>                  
                </td>                
              </tr>
          </table>          
        </td>
      </tr>
      <tr>
          <td>
        <table width="920" cellpadding="0" cellspacing="0" align="center" style="font-size:13px;background:#333; color:#FFF;">
          <tr>
            <td width="25"></td>
            <td height="80">
              <br />
              <a href="<%=site_url + site_service_url%>" target="_blank" style="color:#FFF">客服中心</a>
              |
              <a href="https://www.17life.com/Ppon/WhiteListGuide.aspx" target="_blank" style="color:#FFF">加入白名單</a>
              | 
    服務時間：平日 9:00~18:00
              <br />
              17Life 康太數位整合股份有限公司 版權所有 轉載必究 © 2016
            </td>
          </tr>
        </table>
          </td>
      </tr>
    </table>    
  </div>
</body>
</html>



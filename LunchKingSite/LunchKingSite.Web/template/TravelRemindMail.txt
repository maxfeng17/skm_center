﻿<%@ Argument Name="site_url" Type="string" %>
<%@ Argument Name="site_service_url" Type="string" %>

<%@ Argument Name="member_name" Type="string" %>
<%@ Argument Name="reservation_date" Type="string" %>
<%@ Argument Name="reservation_date_of_week" Type="string" %>
<%@ Argument Name="number_of_people" Type="int" %>
<%@ Argument Name="member_email" Type="string" %>
<%@ Argument Name="contact_name" Type="string" %>
<%@ Argument Name="contact_number" Type="string" %>
<%@ Argument Name="remark" Type="string" %>
<%@ Argument Name="coupon_sequence" Type="string" %>
<%@ Argument Name="coupon_info" Type="string" %>
<%@ Argument Name="coupon_link" Type="string" %>
<%@ Argument Name="seller_name" Type="string" %>
<%@ Argument Name="store_name" Type="string" %>
<%@ Argument Name="store_address" Type="string" %>
<%@ Argument Name="is_concur_coupon" Type="bool" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>17P New EDM</title>
</head>

<body>
<p>&nbsp;</p>
<div style=" width:100%; margin:0 auto; font-family:Arial, Microsoft JhengHei, Helvetica, sans-serif">
	<!--EDM Notification-Start-->
    <!--EDM Notification-End-->
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td style="font-size:13px; background:#fff;">

    <table width="770" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <tr><td>
    <!--EDM Header-Start-->
    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background:#F3F3EE; color:#333;">
    
    <tr>
    <td width="20" height="80"></td>
    <td width="140" height="80">
    <a href="<%=site_url%>/ppon/" target="_blank"><img src="https://www.17life.com/Themes/PCweb/images/EDMLOGO.png" width="132" height="80" alt="" border="0" /></a>
    </td>
    <td width="480" height="80" style="font-size:26px; font-weight:bold;">
    <a href="<%=site_url%>/piinlife/" target="_blank"><img src="https://www.17life.com/Themes/default/images/17Life/EDM/EDMLOGO_HD.png" width="260" height="80" alt="" border="0" /></a>
	</td>
    <td width="110" height="80" style="text-align:right;">
	<a href="<%=site_url%>" target="_blank" style="color:#999;">www.17life.com</a>
	<a href="<%=site_url%>/piinlife/" target="_blank" style="color:#999;">www.Piinlife.com</a>
	</td>
    <td width="20" height="80"></td>
    </tr>
    </table>
    <!--EDM Header-End-->
    <!--EDM Main-Start-->
    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background:#F3F3EE;">
    <tr>
    <td width="25"></td>
    <td width="740">
    	<!--Ad Area-Start-->
    	<table width="740" cellpadding="0" cellspacing="0" align="center">
    	<tr><td height="15" style="border-top:1px solid #DDD;"></td></tr>
        </table>
        <!--Ad Area-End-->
        <!--MainDeal Block-Start-->
        <table width="740" border="0" align="center" cellpadding="0" cellspacing="0" style=" color:#000; font-size:13px; background-color:#FFF;border:1px solid #DDD;">
        <tr>
           <td height="5"></td>
        </tr>
        <tr>
           <td align="center">
           
              <table width="700" border="0" cellspacing="1" cellpadding="1" style="background-color:#FFF;text-align:left;">
                <tr>
                  <td height="50">親愛的<span style="color:#F60; font-size:16px;"><%=member_name%> </span>您好：
               <tr>
                  <td height="40" style=" line-height:26px;">
                   貼心提醒，您已於【<%=seller_name%> <%=store_name%>】預約完成，附上相關資訊給您，以利準時到達喔！
                  </td>
               </tr>
               <tr>
                  <td height="15">                                    
                  </td>
              <tr>
              <tr>
                 <td style="border-top:dashed; border-top-color:#6d9345; border-top-width:1px; line-height:23px;">
                   以下為您的預約資訊：<br/>
                   <span style="font-weight:bold;">預約日期：</span>
                   <span style="color:#bf0000;font-weight:bold; font-size:15px;"><%=reservation_date%> (<%=reservation_date_of_week%>)</span><br/>
                   <span style="font-weight:bold;">預約人數： </span>
                   <span style="color:#bf0000;font-weight:bold; font-size:15px;"><%=number_of_people%>人</span><br/>
                   <span style="font-weight:bold;"><% if (is_concur_coupon) { %>帳　　號：<%}else{%>電子信箱：<%}%></span><%=member_email%><br/>
                   <span style="font-weight:bold;">訂位人姓名： </span><%=contact_name%><br/>
                   <span style="font-weight:bold;">連絡電話： </span><%=contact_number%><br/>
				   <% if (is_concur_coupon) { %>
				      <span style="font-weight:bold;">憑證編號： </span><%=coupon_sequence%>
				   <br/>
				   <%}%>
                   <span style="font-weight:bold;">備　　註： </span><%=remark%><br/>
                 </td>
              </tr>
              <tr>
                 <td height="15" >
                 </td>
              </tr>
              <tr>
                 <td height="70" style="line-height:23px;">
				 <% if (is_concur_coupon) { %>
                   <span style="font-weight:bold;">優惠內容： </span><%=coupon_info%><br/>
				 <%}%>
                   <span style="font-weight:bold;">店家名稱： </span><%=seller_name%> <%=store_name%> <br/>
                   <span style="font-weight:bold;">店家地址： </span><%=store_address%><br/>
                 </td>
              </tr>
              
              <tr>
                 <td height="35" ></td>
              </tr>
              
                 <td style="border:dashed  1px #6d9345; margin-top:10px; margin-bottom:10px;line-height:23px;">
                   <table border="0" cellspacing="0" cellpadding="0">
                     <tr>
                       <td height="5"></td>
                     </tr>
                     <tr>
                       <td style=" font-size:13px;">
                       提醒您：<br/>
                       ◆ 為維護您與店家的權益，請依照預約時間到達；如餐廳訂位，訂位僅保留10分鐘。<br/>
                       ◆ 此信函為系統自動發出，請勿直接回覆，有任何問題請至<a href="<%=site_url + site_service_url%>" style="color:#bf0000;">客服信箱</a>與我們聯繫。
                       </td>
                     </tr>
                      <tr>
                       <td height="5"></td>
                     </tr>
                   </table>

                 
                 </td>
              </tr>
              <tr>
                 <td height="70" valign="bottom" style="line-height:26px;">
                 別忘了每天回來17Life逛逛喔！<br/>
                 17Life  敬上</td>
              </tr>
             </table>

          </td>
        </tr>
        <tr>
           <td height="5"></td>
        </tr>
      </table>
        <br />

    </td>
    <td width="25"></td>
    </tr>
    </table>
    <!--EDM Main-End-->
    </td></tr>
    </table>
    <!--EDM Footer-Start-->
    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background:#333; color:#FFF;">
    <tr>
    <td width="25"></td>
    <td height="80">
    <br />
    <a href="<%=site_url + site_service_url%>" target="_blank" style="color:#FFF">客服中心</a> | 
    <a href="https://www.17life.com/Ppon/WhiteListGuide.aspx" target="_blank" style="color:#FFF">加入白名單</a> | 
    服務時間：平日 9:00~18:00<br />
	17Life 康太數位整合股份有限公司 版權所有 轉載必究 © 2016
    </td>
    </tr>
    </table>
    <!--EDM Footer-End-->
</td>
</tr>
<tr>
<td height="25" style="background:#FFF;"></td>
</tr>
</table>

</div>
</body>
</html>

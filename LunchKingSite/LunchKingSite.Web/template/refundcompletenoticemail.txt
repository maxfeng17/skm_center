﻿<%@ Argument Name="member_name" Type="string" %>
<%@ Argument Name="order_id" Type="string" %>
<%@ Argument Name="order_guid" Type="string" %>
<%@ Argument Name="item_name" Type="string" %>
<%@ Argument Name="site_url" Type="string" %>
<%@ Argument Name="site_service_url" Type="string" %>
<%@ Argument Name="returned_counts" Type="int" %>
<%@ Argument Name="return_total" Type="int" %>
<%@ Argument Name="return_pcash" Type="int" %>
<%@ Argument Name="return_scash" Type="int" %>
<%@ Argument Name="return_pscash" Type="int" %>
<%@ Argument Name="return_bcash" Type="int" %>
<%@ Argument Name="return_creditcard" Type="int" %>
<%@ Argument Name="return_atm" Type="int" %>
<%@ Argument Name="return_tcash" Type="int" %>
<%@ Argument Name="thirdPartyPayment" Type="string" %>
<%@ Argument Name="is_show_allowance" Type="bool" %>
<%@ Argument Name="is_invoiced" Type="bool" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>退貨通知</title>
</head>
<body>
    <div style="width: 100%; margin: 0 auto; font-family: Arial, Microsoft JhengHei, Helvetica, sans-serif">
        <!--EDM Notification-Start-->
        <!--EDM Notification-End-->
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="font-size: 13px; background: #fff;">
                    <table width="770" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <tr>
                                <td>
                                    <!--EDM Header-Start-->
                                    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE; color: #333;">
                                        <tr>
                                            <td width="20" height="80">
											</td>
											<td width="140" height="80">
												<a href="https://www.17life.com" target="_blank">
													<img alt="17Life" src="https://www.17life.com/Themes/PCweb/images/EDMLOGO.png"
														width="132" height="80" /></a>
											</td>
											<td width="480" height="80" style="font-size: 26px; font-weight: bold;">
												<a href="https://www.17life.com/piinlife/" target="_blank">
													<img alt="17Life" src="https://www.17life.com/Themes/default/images/17Life/EDM/EDMLOGO_HD.png"
														width="260" height="80" /></a>
											</td>
											<td width="110" height="80" style="text-align: right;">
												<a href="https://www.17life.com" target="_blank" style="color: #999;">www.17life.com</a>
												<a href="https://www.17life.com/piinlife/" target="_blank" style="color: #999;">www.Piinlife.com</a>
											</td>
											<td width="20" height="80">
											</td>
                                        </tr>
                                    </table>
                                    <!--EDM Header-End-->
                                    <!--EDM Main-Start-->
                                    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE;">
                                        <tr>
                                            <td width="25"></td>
                                            <td width="740">
                                                <table width="740" cellpadding="0" cellspacing="0" align="center">
                                                    <tr>
                                                        <td height="15" style="border-top: 1px solid #DDD;"></td>
                                                    </tr>
                                                </table>
                                                <table width="740" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: '微軟正黑體'; color: #000; font-size: 12px; background-color: #FFF; border: 1px solid #DDD;">
                                                    <tr>
                                                        <td height="5"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <!--Content Area Start-->
                                                            <table width="700" border="0" cellspacing="1" cellpadding="1" style="background-color: #FFF; text-align: left;">
                                                                <tr>
                                                                    <td height="50">親愛的<span style="color: #F60; font-size: 16px;"> <%=member_name%> </span>
                                                                        您好：
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="60" style="line-height: 26px;">
																		<span style="background-color:gray;color:white;">退貨明細</span><br /><br />
                                                                        <span style="font-weight: bold;">訂單編號：</span> <span style="color: #BF0000; font-weight: bold;"><%=order_id%></span><br />
                                                                        <span style="font-weight: bold;">好康名稱：</span> <span style="color: #BF0000; font-weight: bold;"><%=item_name%></span><br />
																		<span style="font-weight: bold;">退貨進度：</span> <span style="color: #BF0000; font-weight: bold;">退貨完成</span><br /> 
																		<% if ((bool)is_show_allowance) { 
																				if ((bool)is_invoiced) { %>
																					<span style="font-weight: bold;">電子折讓證明單:</span>
																					<a href="<%=site_url%>/service/returndiscount.ashx?oid=<%=order_guid%>" target="_blank">點此查看</a>
																				<% }
																		} %><br />
                                                                        <span style="font-weight: bold;">本次完成退貨數：</span> <span style="color: #BF0000; font-weight: bold;"><%=returned_counts%></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table width="250" align="left">
                                                                            <tr>
                                                                                <td style="font-weight: bold; width: 60%;">退款總額：
                                                                                </td>
                                                                                <td style="font-weight: bold; color: #bf0000;">$<%=return_total%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>PayEasy購物金：
                                                                                </td>
                                                                                <td style="font-weight: bold; color: #bf0000;">$<%=return_pcash%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>17Life購物金：
                                                                                </td>
                                                                                <td style="font-weight: bold; color: #bf0000;">$<%=return_scash%>
                                                                                </td>
                                                                            </tr>
																			<tr>
                                                                                <td>由Payeasy兌換：
                                                                                </td>
                                                                                <td style="font-weight: bold; color: #bf0000;">$<%=return_pscash%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>17Life紅利金：
                                                                                </td>
                                                                                <td style="font-weight: bold; color: #bf0000;">$<%=return_bcash%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>刷卡：
                                                                                </td>
                                                                                <td style="font-weight: bold; color: #bf0000;">$<%=return_creditcard%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>ATM付款：
                                                                                </td>
                                                                                <td style="font-weight: bold; color: #bf0000;">$<%=return_atm%>
                                                                                </td>
                                                                            </tr>
																			<%if(thirdPartyPayment != "") {%>
                                                                            <tr>
                                                                                <td><%=thirdPartyPayment%>付款：
                                                                                </td>
                                                                                <td style="font-weight: bold; color: #bf0000;">$<%=return_tcash%>
                                                                                </td>
                                                                            </tr>
																			<%}%>
                                                                            <tr>
                                                                                <td height="20"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <a style="color: #08c;" href="<%=site_url%>/User/coupon_list.aspx">查看完整訂單明細</a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="border-top: 1px dashed #6d9345; margin-top: 10px; margin-bottom: 10px; line-height: 20px;">
                                                                        <table>
                                                                            <tr>
                                                                                <td colspan="2">貼心提醒：
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="1%" valign="top">◆
                                                                                </td>
                                                                                <td width="*">若您申請快速退購物金,購物金將於退款完成日當下即匯回您的帳戶。
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top">◆
                                                                                </td>
                                                                                <td width="*">若為申請刷退,刷退金額將直接退至您信用卡內,因各家銀行作業時間不同,您應可於下一期信用卡帳單內看到此筆退款(視信用卡結帳週期而定),請您耐心等待。
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top">◆
                                                                                </td>
                                                                                <td width="*">若選擇指定帳戶退款，退款處理約需10-20個工作天，若因帳戶資料填寫有誤，將影響退款處理時間。
                                                                                </td>
                                                                            </tr>
																			<tr>
                                                                                <td valign="top">◆
                                                                                </td>
                                                                                <td width="*">若申請退貨訂單,須提供退回或折讓證明單,則退款時程會依您回傳日開始計算。
                                                                                </td>
                                                                            </tr>
																			<tr>
                                                                                <td valign="top">◆
                                                                                </td>
                                                                                <td width="*">退貨完成後，將依照您選擇之退款方式及消費扣抵退還金額。若訂單使用17Life折價券折抵，將不會因退貨而返還。
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top">◆
                                                                                </td>
                                                                                <td width="*">17Life 客服人員不會主動以E-mail或電話要求您更改結帳方式或提供個人資料，若您接獲類似訊息，請拒絕回應，並立刻與 17Life 客服人員聯絡。
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top">◆
                                                                                </td>
                                                                                <td width="*">此信函為系統自動發出，請勿直接回覆，有任何問題請至 <a href="<%=site_url + site_service_url%>" style="color: #08C;">客服信箱</a> 與我們聯繫。
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!--Content Area End-->
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="5"></td>
                                                    </tr>
                                                </table>
                                                <table width="740" cellpadding="0" cellspacing="0" align="center">
                                                    <tr>
                                                        <td height="15" style="border-top: 1px solid #DDD;"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="25"></td>
                                        </tr>
                                    </table>
                                    <!--EDM Main-End-->
                                </td>
                            </tr>
                    </table>
                    <!--EDM Footer-Start-->
                    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #333; color: #FFF;">
                        <tr>
                            <td width="25"></td>
                            <td height="80">
                                <br />
                                <a href="<%=site_url + site_service_url%>" target="_blank" style="color: #FFF">客服中心</a> | <a href="https://www.17life.com/Ppon/WhiteListGuide.aspx" target="_blank"
                                    style="color: #FFF">加入白名單</a> | 服務時間：平日 9:00~18:00<br />
                                17Life 康太數位整合股份有限公司 版權所有 轉載必究 © 2016
                            </td>
                        </tr>
                    </table>
                    <!--EDM Footer-End-->
                </td>
            </tr>
            <tr>
                <td height="25" style="background: #FFF;"></td>
            </tr>
        </table>
    </div>
</body>
</html>
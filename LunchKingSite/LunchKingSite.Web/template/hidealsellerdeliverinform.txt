﻿<%@ Argument Name="deal_name" Type="string" %>
<%@ Argument Name="item_name" Type="string" %>
<%@ Argument Name="dateS" Type="string" %>
<%@ Argument Name="dateE" Type="string" %>
<%@ Argument Name="site_url" Type="string" %>
<%@ Argument Name="mail_subject" Type="string" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><%=mail_subject%>_<%=deal_name%>_<%=item_name%></title>
</head>
<body>
    <div style="width: 100%; margin: 0 auto; font-family: Arial, Microsoft JhengHei, Helvetica, sans-serif">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="font-size: 13px; background: #fff;">
                    <table width="770" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <td>
                                <!--EDM Header-Start-->
                                <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE;
                                    color: #333;">
                                    <tr>
                                        <td width="20" height="80">
                                        </td>
                                        <td width="140" height="80">
                                            <a href="https://www.17life.com" target="_blank">
                                                <img alt="17Life" src="https://www.17life.com/Themes/PCweb/images/EDMLOGO.png"
                                                    width="132" height="80" /></a>
                                        </td>
                                        <td width="480" height="80" style="font-size: 26px; font-weight: bold;">
											<a href="https://www.17life.com/piinlife/" target="_blank">
                                                <img alt="17Life" src="https://www.17life.com/Themes/default/images/17Life/EDM/EDMLOGO_HD.png"
                                                    width="260" height="80" /></a>
                                        </td>
                                        <td width="110" height="80" style="text-align: right;">
                                            <a href="https://www.17life.com" target="_blank" style="color: #999;">www.17life.com</a>
                                            <a href="https://www.17life.com/piinlife/" target="_blank" style="color: #999;">www.Piinlife.com</a>
                                        </td>
                                        <td width="20" height="80">
                                        </td>
                                    </tr>
                                </table>
                                <!--EDM Header-End-->
                                <!--EDM Main-Start-->
                                <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE;">
                                    <tr>
                                        <td width="25">
                                        </td>
                                        <td width="740">
                                            <table width="740" cellpadding="0" cellspacing="0" align="center">
                                                <tr>
                                                    <td height="15" style="border-top: 1px solid #DDD;">
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="740" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: '微軟正黑體';
                                                color: #000; font-size: 13px; background-color: #FFF; border: 1px solid #DDD;">
                                                <tr>
                                                    <td height="5">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <!--Content Area Start-->
                                                        <table width="700" border="0" cellspacing="1" cellpadding="1" style="background-color: #FFF;
                                                            font-weight: bold; text-align: left; font-size:16;">
                                                            <tr>
                                                                <td>
                                                                    親愛的<span style="color: #F60;">
                                                                        廠商 </span> 您好：<br />
                                                                    <br />
                                                                    您販售的［<%=deal_name%>
                                                                    
                                                                    <%=item_name%>］，可以開始出貨囉。<br />
                                                                    出貨期間為：<%=dateS%>
                                                                    ~
                                                                    <%=dateE%>。<br />
                                                                    請您登入 <a href="https://www.17life.com/vbs">17Life商家系統</a> 查看線上清冊，並請於出貨管理中，填寫出貨資料。<br />
                                                                    <br />
                                                                    請務必於
                                                                    <%=dateE%>
                                                                    完成所有出貨，並即時於系統中回填出貨資訊，以利請款。<br />
                                                                    <br />
                                                                    <br />
                                                                    誠摯的謝謝您<br />
                                                                    17Life團隊
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    ----------------------------------------------------------------------------------------------------------------<br /><br />
                                                                    若有任何問題，請洽您的業務員，<br />
                                                                    或電洽客服專線：(02) 3316 - 9105　<span style="font-family:Arial;font-size:13px;font-weight:normal;font-style:normal;text-decoration:none;color:#666666;">　服務時間09:00 ~ 18:00（平日）</span>
                                                                    <br /><br />
                                                                    <span style="font-family:Arial;font-size:15px;font-weight:normal;font-style:normal;text-decoration:none;color:#990000;">提醒：電子郵件通知服務為系統自動發送，請勿直接回覆這一封信。</span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!--Content Area End-->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="5">
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <br />
                                        </td>
                                        <td width="25">
                                        </td>
                                    </tr>
                                </table>
                                <!--EDM Main-End-->
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="25" style="background: #FFF;">
                </td>
            </tr>
        </table>
    </div>
</body>
</html>

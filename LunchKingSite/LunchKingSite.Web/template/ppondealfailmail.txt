﻿<%@ Argument Name="member_name" Type="string" %>
<%@ Argument Name="event_name" Type="string" %>
<%@ Argument Name="item_name" Type="string" %>
<%@ Argument Name="site_url" Type="string" %>
<%@ Argument Name="site_service_url" Type="string" %>
<%@ Argument Name="show_promo" Type="bool" %>
<%@ Argument Name="deal1title" Type="string" %>
<%@ Argument Name="deal1pic" Type="string" %>
<%@ Argument Name="deal1url" Type="string" %>
<%@ Argument Name="deal1price" Type="string" %>
<%@ Argument Name="deal2title" Type="string" %>
<%@ Argument Name="deal2pic" Type="string" %>
<%@ Argument Name="deal2url" Type="string" %>
<%@ Argument Name="deal2price" Type="string" %>
<%@ Argument Name="deal3title" Type="string" %>
<%@ Argument Name="deal3pic" Type="string" %>
<%@ Argument Name="deal3url" Type="string" %>
<%@ Argument Name="deal3price" Type="string" %>
<%@ Argument Name="deal4title" Type="string" %>
<%@ Argument Name="deal4pic" Type="string" %>
<%@ Argument Name="deal4url" Type="string" %>
<%@ Argument Name="deal4price" Type="string" %>
<%@ Argument Name="deal5title" Type="string" %>
<%@ Argument Name="deal5pic" Type="string" %>
<%@ Argument Name="deal5url" Type="string" %>
<%@ Argument Name="deal5price" Type="string" %>
<%@ Argument Name="deal6title" Type="string" %>
<%@ Argument Name="deal6pic" Type="string" %>
<%@ Argument Name="deal6url" Type="string" %>
<%@ Argument Name="deal6price" Type="string" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>17P New EDM</title>
</head>

<body>
<div style=" width:100%; margin:0 auto; font-family:Arial, Microsoft JhengHei, Helvetica, sans-serif">
	<!--EDM Notification-Start-->
    <!--EDM Notification-End-->
<table width="100%" cellpadding="0" cellspacing="0">
  <tr>
<td style="font-size:13px; background:#fff;">

    <table width="770" cellpadding="0" cellspacing="0" align="center">
    <tr><td>
    <!--EDM Header-Start-->
    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background:#F3F3EE; color:#333;">
    
    <tr>
    <td width="20" height="80"></td>
    <td width="140" height="80">
    <a href="<%=site_url%>/ppon/" target="_blank"><img src="https://www.17life.com/Themes/PCweb/images/EDMLOGO.png" width="132" height="80" alt="" border="0" /></a>
    </td>
    <td width="480" height="80" style="font-size:26px; font-weight:bold;">
    <a href="<%=site_url%>/piinlife/" target="_blank"><img src="https://www.17life.com/Themes/default/images/17Life/EDM/EDMLOGO_HD.png" width="260" height="80" alt="" border="0" /></a>
	</td>
    <td width="110" height="80" style="text-align:right;">
	<a href="<%=site_url%>" target="_blank" style="color:#999;">www.17life.com</a>
	<a href="<%=site_url%>/piinlife/" target="_blank" style="color:#999;">www.Piinlife.com</a>
	</td>
    <td width="20" height="80"></td>
  </tr>
    </table>
    <!--EDM Header-End-->
    <!--EDM Main-Start-->
    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background:#F3F3EE;">
  <tr>
    <td width="25"></td>
    <td width="740">
    	<!--Ad Area-Start-->
    	<table width="740" cellpadding="0" cellspacing="0" align="center">
    	<tr><td height="15" style="border-top:1px solid #DDD;"></td></tr>
        <tr></tr>
        </table>
        <!--Ad Area-End-->
        <!--MainDeal Block-Start-->
        <table width="740" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:'微軟正黑體'; color:#000; line-height:30px; font-size:14px; background-color:#FFF;border:1px solid #DDD;">
        <tr>
           <td height="5"></td>
  </tr>
  <tr>
           <td align="center">
           
              <table width="700" border="0" cellspacing="1" cellpadding="1" style="background-color:#FFF;font-weight: bold; text-align:left;">
                <tr>
                  <td height="50">親愛的<span style="color:#F60; font-size:16px;"><%=member_name%></span>您好：
               <tr>
                  <td >感謝您參加本次的17Life好康，<br/>
                  很遺憾本次17Life好康未達購買數量門檻，下次記得多邀請親朋好友與您一起參加17Life好康！

  </td>
  </tr>
  <tr>
                  <td height="30">                   
                  </td>
               </tr>
              
               <tr>
                 <td > 
                    <table width="700" border="0" cellspacing="0" cellpadding="0" style="line-height:23px;">
                       <tr>
                         <td style="line-height:28px;border-bottom:dashed; border-bottom-color:#6d9345; border-bottom-width:1px;border-top:dashed; border-top-color:#6d9345; border-top-width:1px;"> 
						 <%=event_name%><br />
						 <%=item_name%>
						 </td>
                       </tr>  <tr>
                         <td height="30"></td>
                       </tr>  
                        <tr>
                         <td style=" line-height:25px;"><span style="color:#bf0000;">為保障您的權益，請盡早填寫申請退款資訊</span><br/>請至【<a href="https://www.17life.com/User/coupon_List.aspx" style="color:#bf0000;" target="_blank">會員專區</a>】→【<a href="https://www.17life.com/User/coupon_List.aspx" target="_blank" style="color:#bf0000;">憑證/訂單</a>】，<br/>查看該筆訂單編號點選【申請退款】填寫退款申請，費用將全額退於您提供的帳戶。請您放心。 
    </td>
                       </tr>    
                       <tr>
                         <td height="40"></td>
                       </tr>
      <tr>
                         <td height="40"></td>
                       </tr>                   
                      
    </table>
	
		</td>
      </tr>
              <tr>
                 <td style="border:dashed  1px #6d9345; margin-top:10px; margin-bottom:10px;line-height:23px;">
                   <table border="0" cellspacing="0" cellpadding="0">
                     <tr>
                       <td height="5"></td>
                     </tr>
                     <tr>
                       <td style=" font-size:13px;">
                       提醒您～<br/>
◆ 17Life客服人員不會主動以email或電話要求您更改結帳方式或提供個人資料，若您接獲類似訊息，請拒絕回應，並立刻與17Life聯絡。 <br/>
                       ◆ 此信函為系統自動發出，請勿直接回覆，有任何問題請至
                       <a href="<%=site_url + site_service_url%>" style="color:#bf0000; font-family:'微軟正黑體';">
                       客服中心</a>與我們聯繫。</td>
                     </tr>
                      <tr>
                       <td height="5"></td>
                     </tr>
    </table>
	
	
                 </td>
              </tr>
              <tr>
                 <td height="70" valign="bottom" style="line-height:25px;">                   17Life  敬上</td>
              </tr>
             </table>

          </td>
        </tr>
        <tr>
           <td height="5"></td>
        </tr>
      </table>
    <br />
	<% if (show_promo) { %>
		 <table width="730" align="center">
          <tr><td align="center"></td></tr>
          <tr><td>
          <table width="730" align="center" style="background-color:#ffffff;">
          <!--DealSixBlock_Title--Start-->
          <tr><td height="28" style="padding-left:10px; color:#FFF; background-color:#6D9345; font-weight:bold; font-family:'微軟正黑體';">限時好康推薦</td></tr>
          <!--DealSixBlock_Title--End -->
          <tr>
          <td align="center">
          <table width="710">
          <tr><td height="7" colspan="3"></td></tr>
          <tr>
          <!--DealBlock_1--Start-->
          <td width="230" align="center">                                    
          <table width="225" style="border:#6D9345 solid 3px; background-color:#FFF;">
          <tr>
          <td><a href="<%=deal1url%>"><img src="<%=deal1pic%>" width="215" height="123" border="0" alt="<%=deal1title%>" /></a></td>
          </tr>
          <tr>
          <td align="center"><div style="height:42px">
          <a href="<%=deal1url%>" style="font-size:13px; font-family:'微軟正黑體'; color:#000; text-decoration:none; line-height:20px;"><%=deal1title%></a>
		  </div>
          </td>
          </tr>
          <tr>
          <td align="center"><a href="<%=deal1url%>" style="color:#F60; font-size:18px; font-weight:bold;">特價$<%=deal1price%> 搶購</a></td>
          </tr>
          <tr>
          <td height="3"></td>
          </tr>
          </table>                              
          </td>
          <!--DealBlock_1--End-->
          <!--DealBlock_2--Start-->
          <td width="230" align="center">                                    
          <table width="225" style="border:#6D9345 solid 3px; background-color:#FFF;">
          <tr>
          <td><a href="<%=deal2url%>"><img src="<%=deal2pic%>" width="215" height="123" border="0" alt="<%=deal2title%>" /></a></td>
          </tr>
          <tr>
          <td align="center"><div style="height:42px">
          <a href="<%=deal2url%>" style="font-size:13px; font-family:'微軟正黑體'; color:#000; text-decoration:none; line-height:20px;"><%=deal2title%></a>
		  </div>
          </td>
          </tr>
          <tr>
          <td align="center"><a href="<%=deal2url%>" style="color:#F60; font-size:18px; font-weight:bold;">特價$<%=deal2price%> 搶購</a></td>
          </tr>
          <tr>
          <td height="3"></td>
          </tr>
          </table>                              
          </td>
          <!--DealBlock_2--End-->
          <!--DealBlock_3--Start-->
          <td width="230" align="center">                                    
          <table width="225" style="border:#6D9345 solid 3px; background-color:#FFF;">
          <tr>
          <td><a href="<%=deal3url%>"><img src="<%=deal3pic%>" width="215" height="123" border="0" alt="<%=deal3title%>" /></a></td>
          </tr>
          <tr>
          <td align="center"><div style="height:42px">
          <a href="<%=deal3url%>" style="font-size:13px; font-family:'微軟正黑體'; color:#000; text-decoration:none; line-height:20px;"><%=deal3title%></a>
		  </div>
          </td>
          </tr>
          <tr>
          <td align="center"><a href="<%=deal3url%>" style="color:#F60; font-size:18px; font-weight:bold;">特價$<%=deal3price%> 搶購</a></td>
          </tr>
          <tr>
          <td height="3"></td>
          </tr>
          </table>                              
          </td>
          <!--DealBlock_3--End-->
          </tr>                                
          <tr><td colspan="3" height="5"></td></tr>
          <tr>                                 
          <!--DealBlock_4--Start-->
          <td width="230" align="center">                                    
          <table width="225" style="border:#6D9345 solid 3px; background-color:#FFF;">
          <tr>
          <td><a href="<%=deal4url%>"><img src="<%=deal4pic%>" width="215" height="123" border="0" alt="<%=deal4title%>" /></a></td>
          </tr>
          <tr>
          <td align="center"><div style="height:42px">
          <a href="<%=deal4url%>" style="font-size:13px; font-family:'微軟正黑體'; color:#000; text-decoration:none; line-height:20px;"><%=deal4title%></a>
		  </div>
          </td>
          </tr>
          <tr>
          <td align="center"><a href="<%=deal4url%>" style="color:#F60; font-size:18px; font-weight:bold;">特價$<%=deal4price%> 搶購</a></td>
          </tr>
          <tr>
          <td height="3"></td>
          </tr>
          </table>                              
          </td>
          <!--DealBlock_4--End-->
          <!--DealBlock_5--Start-->
          <td width="230" align="center">                                    
          <table width="225" style="border:#6D9345 solid 3px; background-color:#FFF;">
          <tr>
          <td><a href="<%=deal5url%>"><img src="<%=deal5pic%>" width="215" height="123" border="0" alt="<%=deal5title%>" /></a></td>
          </tr>
          <tr>
          <td align="center"><div style="height:42px">
          <a href="<%=deal5url%>" style="font-size:13px; font-family:'微軟正黑體'; color:#000; text-decoration:none; line-height:20px;"><%=deal5title%></a>
		  </div>
          </td>
          </tr>
          <tr>
          <td align="center"><a href="<%=deal5url%>" style="color:#F60; font-size:18px; font-weight:bold;">特價$<%=deal5price%> 搶購</a></td>
          </tr>
          <tr>
          <td height="3"></td>
          </tr>
          </table>                              
          </td>
          <!--DealBlock_5--End-->                        
          <!--DealBlock_6--Start-->
          <td width="230" align="center">                                    
          <table width="225" style="border:#6D9345 solid 3px; background-color:#FFF;">
          <tr>
          <td><a href="<%=deal6url%>"><img src="<%=deal6pic%>" width="215" height="123" border="0" alt="<%=deal6title%>" /></a></td>
          </tr>
          <tr>
          <td align="center"><div style="height:42px">
          <a href="<%=deal6url%>" style="font-size:13px; font-family:'微軟正黑體'; color:#000; text-decoration:none; line-height:20px;"><%=deal6title%></a>
		  </div>
          </td>
          </tr>
          <tr>
          <td align="center"><a href="<%=deal6url%>" style="color:#F60; font-size:18px; font-weight:bold;">特價$<%=deal6price%> 搶購</a></td>
          </tr>
          <tr>
          <td height="3"></td>
          </tr>
          </table>                              
          </td>
          <!--DealBlock_6--End-->                     
          </tr>
          <tr><td height="7" colspan="3"></td></tr>
          </table>          </td>          </tr>          </table>          </td>          </tr>          <tr><td height="7"></td></tr>
          </table>
		  <% } %>
    </td>
    <td width="25"></td>
    </tr>
    </table>
    <!--EDM Main-End-->
    </td></tr>
    </table>
    <!--EDM Footer-Start-->
    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background:#333; color:#FFF;">
    <tr>
    <td width="25"></td>
    <td height="80">
    <br />
    <a href="<%=site_url + site_service_url%>" target="_blank" style="color:#FFF">客服中心</a> | 
    <a href="<%=site_url%>/Ppon/WhiteListGuide.aspx" target="_blank" style="color:#FFF">加入白名單</a> | 
    服務時間：平日 9:00~18:00<br />
	17Life 康太數位整合股份有限公司 版權所有 轉載必究 © 2016
    </td>
    </tr>
    </table>
    <!--EDM Footer-End-->
	</td>
  </tr>
  <tr>
<td height="25" style="background:#FFF;"></td>
  </tr>
</table>

</div>
</body>
</html>
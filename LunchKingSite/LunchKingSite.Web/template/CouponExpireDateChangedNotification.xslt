﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="html" indent="yes"/>

    <xsl:template match="/CouponExpireDateChangedInformation">

        <html xmlns="http://www.w3.org/1999/xhtml">

            <head>
                <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
                <title>Untitled 1</title>               
            </head>

            <body>

                <table style="width: 100%; border-collapse: collapse; border: 1px solid #000000;">
                    <tr>
                        <td style="border: 1px solid #000000;">親愛的同仁，您好<br/><br/>

                            <xsl:if test="string-length(SellerName/text())!=0">
                              店家名稱：<xsl:value-of select="SellerName/text()"/>
                                <xsl:if test="string-length(StoreName/text())!=0">
                                    <xsl:value-of select="StoreName/text()"/>
                                </xsl:if><br/>
                            </xsl:if>

                            檔名：<xsl:value-of select="DealName/text()"/><br/>

                            檔次連結：<xsl:value-of select="DealUrl/text()"/><br/>

                            憑證使用期限：<xsl:value-of select="CouponUseStartDate/text()"/> ~ <xsl:value-of select="CouponUseOriginalExpireDate/text()"/><br/>

                            <xsl:choose>
                                <xsl:when test="IsExtend/text() = 'false' ">
                                    停止使用日期：
                                </xsl:when>
                                <xsl:when test="IsExtend/text() = 'true' ">
                                    延長使用日期：
                                </xsl:when>
                            </xsl:choose>
                            <xsl:value-of select="ChangedExpireDate/text()"/><br/>
                            
                            <xsl:choose>
                                <xsl:when test="IsExtend/text() = 'false' ">
                                    <span style="color: rgb(255, 0, 0); font-family: Arial; font-size: 18px; font-style: normal; font-variant: normal; font-weight: bold; letter-spacing: normal; line-height: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; display: inline !important; float: none; ">
                                        提醒您！此店家憑證停止使用，請相關人員注意~謝謝！
                                    </span>
                                </xsl:when>
                                <xsl:when test="IsExtend/text() = 'true' ">
                                    <span style="color: rgb(255, 0, 0); font-family: Arial; font-size: 18px; font-style: normal; font-variant: normal; font-weight: bold; letter-spacing: normal; line-height: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; display: inline !important; float: none; ">
                                        提醒您！此店家延長使用期限，請相關人員注意~謝謝！
                                    </span>
                                </xsl:when>
                            </xsl:choose>
                            
                        </td>
                    </tr>
                </table>

            </body>

        </html>

    </xsl:template>
</xsl:stylesheet>


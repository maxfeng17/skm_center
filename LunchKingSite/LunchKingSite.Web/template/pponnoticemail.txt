﻿<%@ Argument Name="member_name" Type="string" %>
<%@ Argument Name="statement" Type="string" %>
<%@ Argument Name="download" Type="string" %>
<%@ Argument Name="fax" Type="string" %>
<%@ Argument Name="order_id" Type="string" %>
<%@ Argument Name="item_name" Type="string" %>
<%@ Argument Name="site_url" Type="string" %>
<%@ Argument Name="site_service_url" Type="string" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>17P New EDM</title>
</head>
<body>
    <div style="width: 100%; margin: 0 auto; font-family: Arial, Microsoft JhengHei, Helvetica, sans-serif">
        <!--EDM Notification-Start-->
        <!--EDM Notification-End-->
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="font-size: 13px; background: #fff;">
                    <table width="770" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                        <tr>
                            <td>
                                <!--EDM Header-Start-->
                                <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE;
                                    color: #333;">
                                    <tr>
                                        <td width="20" height="80">
                                        </td>
                                        <td width="140" height="80">
                                            <a href="https://www.17life.com" target="_blank">
                                                <img alt="17Life" src="https://www.17life.com/Themes/PCweb/images/EDMLOGO.png"
                                                    width="132" height="80" /></a>
                                        </td>
                                        <td width="480" height="80" style="font-size: 26px; font-weight: bold;">
											<a href="https://www.17life.com/piinlife/" target="_blank">
                                                <img alt="17Life" src="https://www.17life.com/Themes/default/images/17Life/EDM/EDMLOGO_HD.png"
                                                    width="260" height="80" /></a>
                                        </td>
                                        <td width="110" height="80" style="text-align: right;">
                                            <a href="https://www.17life.com" target="_blank" style="color: #999;">www.17life.com</a>
                                            <a href="https://www.17life.com/piinlife/" target="_blank" style="color: #999;">www.Piinlife.com</a>
                                        </td>
                                        <td width="20" height="80">
                                        </td>
                                    </tr>
                                </table>
                                <!--EDM Header-End-->
                                <!--EDM Main-Start-->
                                <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE;">
                                    <tr>
                                        <td width="25">
                                        </td>
                                        <td width="740">
                                            <table width="740" cellpadding="0" cellspacing="0" align="center">
                                                <tr>
                                                    <td height="15" style="border-top: 1px solid #DDD;">
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="740" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: '微軟正黑體';
                                                font-size:12px; color: #000; background-color: #FFF; border: 1px solid #DDD;">
                                                <tr>
                                                    <td height="5">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <!--Content Area Start-->
                                                        <table width="700" border="0" cellspacing="1" cellpadding="1" style="background-color: #FFF;
                                                            text-align: left;">
                                                            <tr>
                                                                <td height="50">
                                                                    親愛的<span style="color: #F60; font-size: 15px;"> <%=member_name%> </span>您好：
                                                                </td>
                                                            </tr>
                                                                    <tr>
                                                                        <td height="60" style="line-height: 26px;">
                                                                    <span style="font-weight: bold;">訂單編號：</span> <span style="color: #BF0000; font-weight: bold;">
                                                                                <%=order_id%></span><br />
                                                                    <span style="font-weight: bold;">好康名稱：</span> <span style="color: #BF0000; font-weight: bold;">
                                                                        <%=item_name%></span><br />
																	<span style="font-weight: bold;">尚須下載並寄回：</span> <span style="color: #08C; font-weight: bold;">
                                                                        <%=download%></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                <td>&nbsp;</td>
															</tr>
                                                            <tr>
                                                                        <td height="60" style="line-height: 23px;">
                                                                            <%=statement%>
                                                                        </td>
                                                                    </tr>
															<tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
																<td>若您已索取紙本發票，亦請將發票一併以郵局掛號方式寄回，以利退款作業。</td>
															</tr>
															<tr>
                                                                <td>&nbsp;</td>
															</tr>
                                                            <tr>
                                                                <td style="line-height: 23px;">
                                                                    17Life退貨處理中心<br />
                                                                    寄件地址：104 台北市中山區中山北路一段11號13樓
                                                                    <%=fax%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="line-height: 23px;">
                                                                    <br />
                                                                    您可至17Life【會員專區】→【<a href="<%=site_url%>/User/coupon_List.aspx" style="color: #08C;">訂單/憑證</a>】，點選該筆訂單編號即可進入查詢，隨時了解您申請取消訂單的處理狀況，亦可自行下載退貨申請書/退回或折讓證明單。
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border-top:1px dashed #6d9345; margin-top: 10px; margin-bottom: 10px; line-height: 23px;">
                                                                    <table>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                貼心提醒：
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="1%" valign="top">
                                                                                ◆
                                                                            </td>
                                                                            <td width="*">
                                                                                退貨處理約10~20個工作天，退貨完成後，退款作業將依照您選擇之退款方式及原消費扣抵方式退還金額。若訂單使用17Life折價券折抵，將不會因退貨而返還。
                                                                                
                                                                            </td>
                                                                        </tr>
																		<tr>
																			<td valign="top">
                                                                                ◆
                                                                            </td>
																			<td>
																				選擇信用卡刷退，退款金額會直接退至您下訂單時所使用的信用卡中，因各家銀行作業時間不同，您應可於下一期信用卡帳單看到此退款（視信用卡結帳週期而定）。
																			</td>
																		</tr>
																		<tr>
																			<td valign="top">
                                                                                ◆
                                                                            </td>
																			<td>
																				選擇指定帳戶退款，退貨處理約需30個工作天，若因帳戶資料填寫有誤，將影響退貨處理時間。
																			</td>
																		</tr>
                                                                        <tr>
                                                                            <td valign="top">
                                                                                ◆
                                                                                        </td>
                                                                            <td width="*">
                                                                                17Life 客服人員不會主動以E-mail或電話要求您更改結帳方式或提供個人資料，若您接獲類似訊息，請拒絕回應，並立刻與 17Life 客服人員聯絡。
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                            <td valign="top">
                                                                                ◆
                                                                                        </td>
                                                                            <td width="*">
                                                                                此信函為系統自動發出，請勿直接回覆，有任何問題請至<a href="<%=site_url + site_service_url%>" style="color: #08C;">客服信箱</a>與我們聯繫。
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                                    </tr>
                                                                                </table>
                                                        <!--Content Area End-->
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                    <td height="5">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
											<table width="740" cellpadding="0" cellspacing="0" align="center">
                                                                        <tr>
													<td height="15" style="border-top: 1px solid #DDD;"></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                        <td width="25">
                                        </td>
                                    </tr>
                                </table>
                                <!--EDM Main-End-->
                            </td>
                        </tr>
                    </table>
                    <!--EDM Footer-Start-->
                    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #333;
                        color: #FFF;">
                        <tr>
                            <td width="25">
                            </td>
                            <td height="80">
                                <br />
                                <a href="<%=site_url + site_service_url%>" target="_blank" style="color: #FFF">
                                    客服中心</a> | <a href="<%=site_url%>/Ppon/WhiteListGuide.aspx" target="_blank"
                                        style="color: #FFF">加入白名單</a> | 服務時間：平日 9:00~18:00<br />
                                17Life 康太數位整合股份有限公司 版權所有 轉載必究 © 2016
                            </td>
                        </tr>
                    </table>
                    <!--EDM Footer-End-->
                </td>
            </tr>
            <tr>
                <td height="25" style="background: #FFF;">
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
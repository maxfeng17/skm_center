<%@ Argument Name="member_name" Type="string" %>
<%@ Argument Name="item_name" Type="string" %>
<%@ Argument Name="deliver_date_end" Type="string" %>
<%@ Argument Name="site_url" Type="string" %>
<%@ Argument Name="site_service_url" Type="string" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>17Life New EDM</title>
</head>
<body>
    <div style="width: 100%; margin: 0 auto; font-family: Arial, Microsoft JhengHei, Helvetica, sans-serif;max-width:770px;">
        <!--EDM Notification-Start-->
        <!--EDM Notification-End-->
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="font-size: 13px; background: #fff;">
                    <table width="100%" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <td>
                                <!--EDM Header-Start-->
                                <table width="100%" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE;
                                    color: #333;">
                                    <tr>
                                        <td width="20" height="80"></td>
                                        <td width="110" height="80">
                                            <a href="https://www.17life.com" target="_blank">
                                                <img alt="17Life" src="https://www.17life.com/Themes/PCweb/images/EDMLOGO.png"
                                                     border="0" style="min-width:50px;max-width:110px;width:100%;max-height:80px;" />
											</a>
                                        </td>
                                        <td height="80" style="font-size: 26px; font-weight: bold;">
											<a href="https://www.17life.com/piinlife/" target="_blank">
                                                <img alt="17Life" src="https://www.17life.com/Themes/default/images/17Life/EDM/EDMLOGO_HD.png"
                                                     border="0" style="min-width:120px;max-width:260px;width:100%;max-height:80px;" />
											</a>
                                        </td>
                                        <td width="50" height="80" style="text-align: right;">
                                            <a href="https://www.17life.com" target="_blank" style="color: #999;">www.17life.com</a>
                                            <a href="https://www.17life.com/piinlife/" target="_blank" style="color: #999;">www.Piinlife.com</a>
                                        </td>
                                        <td width="20" height="80">
                                        </td>
                                    </tr>
                                </table>
                                <!--EDM Header-End-->
                                <!--EDM Main-Start-->
                                <table width="100%" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE;">
                                    <tr>
                                        <td width="25">
                                        </td>
                                        <td width="100%">
                                            <table width="95%" cellpadding="0" cellspacing="0" align="center">
                                                <tr>
                                                    <td height="15" style="border-top: 1px solid #DDD;">
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: '微軟正黑體';
                                                color: #000; font-size: 13px; background-color: #FFF; border: 1px solid #DDD;">
                                                <tr>
                                                    <td height="5">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <!--Content Area Start-->
                                                        <table width="95%" border="0" cellspacing="1" cellpadding="1" style="background-color: #FFF;
                                                            font-weight: bold; text-align: left;">
                                                            <tr>
                                                                <td height="50">
                                                                    親愛的<span style="color: #F60; font-size: 16px;"><%=member_name%></span>您好：
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    您訂購的好康【<%=item_name%>】獨家優惠已經開跑囉~<br /><br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="30">
                                                                    <span style="color: #BF0000">提醒您：好康優惠期間至<%=deliver_date_end%>止，</span>請盡快前往兌換。<br />
                                                                    <br />

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="" style="background:#e2f2ff;padding:10px 0;" align="center">
                                                                    <table style="margin-bottom:10px;">
                                                                        <tr>
                                                                            <td>
                                                                                <img src="https://www.17life.com/Themes/PCweb/images/app_icon_relay.png" alt="" width="80" />
                                                                            </td>
                                                                            <td style="font-size:120%;line-height:1.2em;padding-left:4px;">
                                                                                下載17Life App，一手掌握吃喝玩樂最新優惠！
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <a href="https://itunes.apple.com/tw/app/id543439591?mt=8" target="_blank" style="text-decoration:none;margin-right:10px;">
                                                                        <img alt="17Life" src="https://www.17life.com/Themes/PCweb/images/appstore_download.png" width="120" height="">
                                                                    </a>
                                                                    <a href="https://play.google.com/store/apps/details?id=com.uranus.e7plife" target="_blank">
                                                                        <img alt="17Life" src="https://www.17life.com/Themes/PCweb/images/googleplay_download.png" width="120" height="">
                                                                    </a>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td height="15">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="border: dashed  1px #999; margin-top: 10px; margin-bottom: 10px; line-height: 23px;">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td height="5">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="font-size: 13px;">
                                                                                提醒您～<br />
                                                                                ◆ 若您已使用此好康憑證，則不需理會該通知。<br />
                                                                                ◆ 17Life客服人員不會主動以email或電話要求您更改結帳方式或提供個人資料，若您接獲類似訊息，請拒絕回應，並立刻與17Life聯絡。<br />
                                                                                ◆ 此信函為系統自動發出，請勿直接回覆，有任何問題請至 <a href="<%=site_url + site_service_url%>" style="color: #F60;
                                                                                    font-family: Arial, Helvetica, sans-serif;" target="_blank">客服信箱</a>與我們聯繫。<br />
                                                                                ◆ 本公司保留接受或取消本訂購的權利。
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="5">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="100">
                                                                    別忘了回來<a href="<%=site_url%>/ppon/default.aspx" style="color: #F60;" target="_blank">17Life</a>逛逛喔！<br />
                                                                    17Life 敬上
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!--Content Area End-->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="5">
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                        </td>
                                        <td width="25">
                                        </td>
                                    </tr>
                                </table>
                                <!--EDM Main-End-->
                            </td>
                        </tr>
                    </table>
                    <!--EDM Footer-Start-->
                    <table width="100%" cellpadding="0" cellspacing="0" align="center" style="background: #333;
                        color: #FFF;">
                        <tr>
                            <td width="25">
                            </td>
                            <td height="80">
                                <br />
                                <a href="<%=site_url + site_service_url%>" target="_blank" style="color: #FFF">
                                    客服中心</a> | <a href="https://www.17life.com/Ppon/WhiteListGuide.aspx" target="_blank"
                                        style="color: #FFF">加入白名單</a> | 服務時間：平日 9:00~18:00<br />
                                17Life 康太數位整合股份有限公司 版權所有 轉載必究 © 2016
                            </td>
                        </tr>
                    </table>
                    <!--EDM Footer-End-->
                </td>
            </tr>
            <tr>
                <td height="25" style="background: #FFF;">
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
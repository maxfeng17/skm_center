﻿<%@ Argument Name="header" Type="string" %>
<%@ Argument Name="footer" Type="string" %>
<%@ Argument Name="member_name" Type="string" %>
<%@ Argument Name="order_id" Type="string" %>
<%@ Argument Name="order_guid" Type="string" %>
<%@ Argument Name="item_name" Type="string" %>
<%@ Argument Name="site_url" Type="string" %>
<%@ Argument Name="site_service_url" Type="string" %>
<%@ Argument Name="is_show_allowance" Type="bool" %>
<%@ Argument Name="is_invoiced" Type="bool" %>
<%@ Argument Name="use_new_refund" Type="bool" %>
<%@ Argument Name="coupon_counts" Type="string" %>
<%@ Argument Name="return_coupon_counts" Type="string" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>退貨通知</title>
</head>
<body>
    <div style="width: 100%; margin: 0 auto; font-family: Arial, Microsoft JhengHei, Helvetica, sans-serif">
        <!--EDM Notification-Start-->
        <!--EDM Notification-End-->
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="font-size: 13px; background: #fff;">
                    <table width="770" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <tr>
                                <td>
									<!--EDM Header-Start-->
									<%=header%>
									<!--EDM Header-End-->
                                    <!--EDM Main-Start-->
                                    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE;">
                                        <tr>
                                            <td width="25"></td>
                                            <td width="740">
                                                <table width="740" cellpadding="0" cellspacing="0" align="center">
                                                    <tr>
                                                        <td height="15" style="border-top: 1px solid #DDD;"></td>
                                                    </tr>
                                                </table>
                                                <table width="740" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: '微軟正黑體'; color: #000; font-size: 12px; background-color: #FFF; border: 1px solid #DDD;">
                                                    <tr>
                                                        <td height="5"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <!--Content Area Start-->
                                                            <table width="700" border="0" cellspacing="1" cellpadding="1" style="background-color: #FFF; text-align: left;">
                                                                <tr>
                                                                    <td height="50">親愛的<span style="color: #F60; font-size: 16px;"> <%=member_name%> </span>
                                                                        您好：
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="60" style="line-height: 26px;">
																		<span style="background-color:gray;color:white;">退貨明細</span><br />
                                                                        <span style="font-weight: bold;">訂單編號：</span> <span style="color: #BF0000; font-weight: bold;"><%=order_id%></span><br />
                                                                        <span style="font-weight: bold;">好康名稱：</span> <span style="color: #BF0000; font-weight: bold;"><%=item_name%></span><br />
																		<span style="font-weight: bold;">退貨進度：</span> <span style="color: #BF0000; font-weight: bold;">退貨申請處理中</span><br />
                                                                        <span style="font-weight: bold;">訂單憑證數：</span> <span style="font-weight: bold;"><%=coupon_counts%></span><br />
																		<% if ((bool)use_new_refund) { %>
                                                                        <span style="font-weight: bold;">本次申請退貨數：</span> <span style="color: #BF0000; font-weight: bold;"><%=return_coupon_counts%></span><br />
																		<% } %>
																		<% if ((bool)is_show_allowance) { %>
																			<span style="font-weight: bold;">尚須下載並寄回：</span>
																			<% if ((bool)is_invoiced) { %>
																				<span style="color: #BF0000; font-weight: bold;">建議使用電腦開啟並下載此折讓單。</span>
																				<span style="font-weight: bold;">
																				<a href="<%=site_url%>/service/returndiscount.ashx?oid=<%=order_guid%>" target="_blank">
																					<img alt="點此下載紙本折讓" src="https://www.17life.com/Themes/default/images/17Life/EDM/downloadAllowance.PNG" width="145" height="30"/></a>
																				</span>
																			<% }else{ %>
																			<span style="color: #BF0000; font-weight: bold;">尚待供應商確認，請與客服聯繫索取折讓證明單</span>
																			<% } %>
																		<% } %>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="line-height: 23px;">
                                                                        您可至17Life【會員專區】→【<a href="<%=site_url%>/user/coupon_list.aspx"
                                                                                style="color:#08C;">訂單/憑證</a>】，點選訂單編號即可進入訂單明細查詢，隨時了解您的退貨申請狀態<% if ((bool)is_show_allowance) { %>，亦可在明細中自行下載［紙本折讓單］<% } %>。
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20"></td>
                                                                </tr>
                                                                <% if ((bool)is_show_allowance) { %>
                                                                <tr>
                                                                    <td style="line-height: 23px;">
																		<span style="background-color:gray;color:white;">退貨須知</span><br />
																		<span style="color: #BF0000; font-weight: bold;">1.折讓單文件遞交</span><br />
                                                                        <span style="color: #8c1aff; font-weight: bold;">【折讓單】</span>
																		請下載［紙本折讓證明單］，填寫「營利事業統一編號」並蓋上公司發票章後寄回。<br />
																		<span style="color: #8c1aff; font-weight: bold;">【提交】</span>
																		以郵局掛號或傳真方式寄回17Life退貨處理中心，待我們收到資料後，會立即為您處理退款作業。
                                                                        <br />
																		&nbsp;&nbsp;收件名稱：17Life退貨處理中心 收<br />
																		&nbsp;&nbsp;收件地址：104 台北市中山區中山北路一段11號13樓<br />
																		&nbsp;&nbsp;傳真號碼：（02）2511-9110
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20"></td>
                                                                </tr>
																<% } %>
                                                                <tr>
                                                                <td style="border-top:1px dashed #6d9345; margin-top: 10px; margin-bottom: 10px; line-height: 20px;">
                                                                    <table>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                貼心提醒：
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="1%" valign="top">
                                                                                ◆
                                                                            </td>
                                                                            <td width="*">
                                                                                退貨處理約10~20個工作天，退貨完成後，將依照您選擇之退款方式及消費扣抵退還金額。若訂單使用17Life折價券折抵，將不會因退貨而返還。
                                                                            </td>
                                                                        </tr>
																		<% if ((bool)!is_show_allowance) { %>
																		<tr>
                                                                            <td width="1%" valign="top">
                                                                                ◆
                                                                            </td>
                                                                            <td width="*">
                                                                                <span style="color:red;">退款完成後，會再次寄發信件通知您。若發票已經開立，隨函會附上電子折讓單。</span>
                                                                            </td>
                                                                        </tr>
																		<% } %>
																		<tr>
                                                                            <td valign="top">
                                                                                ◆
                                                                            </td>
                                                                            <td width="*">
                                                                                選擇信用卡刷退，款項將直接退至下單時所使用的信用卡中。因各家銀行作業時間不同，您應可於下一期信用卡帳單看到此退款（視信用卡結帳週期而定）。
                                                                            </td>
                                                                        </tr>
																		<tr>
                                                                            <td valign="top">
                                                                                ◆
                                                                            </td>
                                                                            <td width="*">
                                                                                選擇指定帳戶退款，退貨處理約需14-21個工作天，若因帳戶資料填寫有誤，將影響退貨處理時間。
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top">
                                                                                ◆
                                                                            </td>
                                                                            <td width="*">
                                                                                17Life 客服人員不會主動以E-mail或電話要求您更改結帳方式或提供個人資料，若您接獲類似訊息，請拒絕回應，並立刻與 17Life 客服人員聯絡。
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top">
                                                                                ◆
                                                                            </td>
                                                                            <td width="*">
                                                                                此信函為系統自動發出，請勿直接回覆，有任何問題請至 <a href="<%=site_url + site_service_url%>" style="color: #08C;">客服信箱</a> 與我們聯繫。
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </table>
                                                            <!--Content Area End-->
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="5"></td>
                                                    </tr>
                                                </table>
												<table width="740" cellpadding="0" cellspacing="0" align="center">
													<tr>
														<td height="15" style="border-top: 1px solid #DDD;"></td>
													</tr>
												</table>
                                            </td>
                                            <td width="25"></td>
                                        </tr>
                                    </table>
                                    <!--EDM Main-End-->
                                </td>
                            </tr>
                    </table>
                    <!--EDM Footer-Start-->
                    <%=footer%>
                    <!--EDM Footer-End-->
                </td>
            </tr>
            <tr>
                <td height="25" style="background: #FFF;"></td>
            </tr>
        </table>
    </div>
</body>
</html>
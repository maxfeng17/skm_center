﻿<%@  Argument Name="site_url" Type="string" %>
<%@  Argument Name="site_service_url" Type="string" %>
<%@  Argument Name="member_name" Type="string" %>
<%@  Argument Name="event_name" Type="string" %>
<%@  Argument Name="amount" Type="string" %>
<%@  Argument Name="code" Type="string" %>
<%@  Argument Name="start_date" Type="string" %>
<%@  Argument Name="end_date" Type="string" %>
<%@  Argument Name="discount_limit" Type="string" %>
<%@  Argument Name="promoInfo" Type="string" %>
<%@  Argument Name="deal1title" Type="string" %>
<%@  Argument Name="deal1pic" Type="string" %>
<%@  Argument Name="deal1url" Type="string" %>
<%@  Argument Name="deal1price" Type="string" %>
<%@  Argument Name="deal2title" Type="string" %>
<%@  Argument Name="deal2pic" Type="string" %>
<%@  Argument Name="deal2url" Type="string" %>
<%@  Argument Name="deal2price" Type="string" %>
<%@  Argument Name="deal3title" Type="string" %>
<%@  Argument Name="deal3pic" Type="string" %>
<%@  Argument Name="deal3url" Type="string" %>
<%@  Argument Name="deal3price" Type="string" %>
<%@  Argument Name="deal4title" Type="string" %>
<%@  Argument Name="deal4pic" Type="string" %>
<%@  Argument Name="deal4url" Type="string" %>
<%@  Argument Name="deal4price" Type="string" %>
<%@  Argument Name="deal5title" Type="string" %>
<%@  Argument Name="deal5pic" Type="string" %>
<%@  Argument Name="deal5url" Type="string" %>
<%@  Argument Name="deal5price" Type="string" %>
<%@  Argument Name="deal6title" Type="string" %>
<%@  Argument Name="deal6pic" Type="string" %>
<%@  Argument Name="deal6url" Type="string" %>
<%@  Argument Name="deal6price" Type="string" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>17Life New EDM</title>
</head>
<body>
    <div style="width: 100%; margin: 0 auto; font-family: Arial, Microsoft JhengHei, Helvetica, sans-serif">
        <!--EDM Notification-Start-->
        <!--EDM Notification-End-->
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="font-size: 13px; background: #fff;">
                    <table width="770" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <td>
                                <!--EDM Header-Start-->
                                <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE;
                                    color: #333;">
                                    <tr>
                                        <td width="20" height="80">
                                        </td>
                                        <td width="140" height="80">
                                            <a href="https://www.17life.com" target="_blank">
                                                <img alt="17Life" src="https://www.17life.com/Themes/PCweb/images/EDMLOGO.png"
                                                    width="132" height="80" /></a>
                                        </td>
                                        <td width="480" height="80" style="font-size: 26px; font-weight: bold;">
											<a href="https://www.17life.com/piinlife/" target="_blank">
                                                <img alt="17Life" src="https://www.17life.com/Themes/default/images/17Life/EDM/EDMLOGO_HD.png"
                                                    width="260" height="80" /></a>
                                        </td>
                                        <td width="110" height="80" style="text-align: right;">
                                            <a href="https://www.17life.com" target="_blank" style="color: #999;">www.17life.com</a>
                                            <a href="https://www.piinlife.com/piinlife/default.aspx" target="_blank" style="color: #999;">www.Piinlife.com</a>
                                        </td>
                                        <td width="20" height="80">
                                        </td>
                                    </tr>
                                </table>
                                <!--EDM Header-End-->
                                <!--EDM Main-Start-->
                                <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE;">
                                    <tr>
                                        <td width="25">
                                        </td>
                                        <td width="740">
                                            <table width="740" cellpadding="0" cellspacing="0" align="center">
                                                <tr>
                                                    <td height="15" style="border-top: 1px solid #DDD;">
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="740" border="0" cellspacing="0" cellpadding="0" bgcolor="#6d9345">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <table width="740" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: 'Microsoft JhengHei';
                                                                color: #000; line-height: 30px; font-size: 14px; background-color: #FFF; border: 1px solid #DDD;">
                                                                <tbody>
                                                                    <tr>
                                                                        <td height="5">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <table width="700" border="0" cellspacing="1" cellpadding="1" style="background-color: #FFF;
                                                                                font-weight: bold; text-align: left;">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td height="50">
                                                                                            親愛的<span style="color: #F60; font-size: 16px;"><%=member_name%></span>您好：
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            感謝您參與【<%=event_name%>】活動。
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="70" bgcolor="#FFFFD9" border="0" cellspacing="0" cellpadding="0" style="line-height: 20px;
                                                                                            border: solid 1px #FFE066; padding-left: 15px; font-size: 16px;">
                                                                                            您所獲得的17life折價券序號：<%=code%>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="15">
                                                                                            此活動折價券的使用期限為：<%=start_date%> ~ <%=end_date%>
																							<%=discount_limit%>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="50">
																							<%=promoInfo%>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="50" valign="bottom" style="line-height: 25px;">
                                                                                            17Life 敬上
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="5">
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table width="740" align="center">
                                                <tr>
                                                    <td align="center">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table width="740" align="center" style="background-color: #ffffff; border: 1px solid #DDD;">
                                                            <!--DealSixBlock_Title--Start-->
                                                            <tr>
                                                                <td height="28" style="padding-left: 10px; color: #FFF; background-color: #BF0000;
                                                                    font-weight: bold; font-family: '微軟正黑體';">
                                                                    限時好康推薦
                                                                </td>
                                                            </tr>
                                                            <!--DealSixBlock_Title--End -->
                                                            <tr>
                                                                <td align="center">
                                                                    <table width="710">
                                                                        <tr>
                                                                            <td height="7" colspan="3">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <!--DealBlock_1--Start-->
                                                                            <td width="230" align="center">
                                                                                <table width="225" style="border: #BF0000 solid 3px; background-color: #FFF;">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <a href="<%=deal1url%>">
                                                                                                <img src="<%=deal1pic%>" width="215" height="123" border="0" alt="<%=deal1title%>" /></a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <div style="height: 42px">
                                                                                                <a href="<%=deal1url%>" style="font-size: 13px; font-family: '微軟正黑體'; color: #000;
                                                                                                    text-decoration: none; line-height: 20px;">
                                                                                                    <%=deal1title%></a>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <a href="<%=deal1url%>" style="color: #F60; font-size: 18px; font-weight: bold;">特價$<%=deal1price%>
                                                                                                搶購</a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="3">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <!--DealBlock_1--End-->
                                                                            <!--DealBlock_2--Start-->
                                                                            <td width="230" align="center">
                                                                                <table width="225" style="border: #BF0000 solid 3px; background-color: #FFF;">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <a href="<%=deal2url%>">
                                                                                                <img src="<%=deal2pic%>" width="215" height="123" border="0" alt="<%=deal2title%>" /></a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <div style="height: 42px">
                                                                                                <a href="<%=deal2url%>" style="font-size: 13px; font-family: '微軟正黑體'; color: #000;
                                                                                                    text-decoration: none; line-height: 20px;">
                                                                                                    <%=deal2title%></a>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <a href="<%=deal2url%>" style="color: #F60; font-size: 18px; font-weight: bold;">特價$<%=deal2price%>
                                                                                                搶購</a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="3">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <!--DealBlock_2--End-->
                                                                            <!--DealBlock_3--Start-->
                                                                            <td width="230" align="center">
                                                                                <table width="225" style="border: #BF0000 solid 3px; background-color: #FFF;">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <a href="<%=deal3url%>">
                                                                                                <img src="<%=deal3pic%>" width="215" height="123" border="0" alt="<%=deal3title%>" /></a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <div style="height: 42px">
                                                                                                <a href="<%=deal3url%>" style="font-size: 13px; font-family: '微軟正黑體'; color: #000;
                                                                                                    text-decoration: none; line-height: 20px;">
                                                                                                    <%=deal3title%></a>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <a href="<%=deal3url%>" style="color: #F60; font-size: 18px; font-weight: bold;">特價$<%=deal3price%>
                                                                                                搶購</a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="3">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <!--DealBlock_3--End-->
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3" height="5">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <!--DealBlock_4--Start-->
                                                                            <td width="230" align="center">
                                                                                <table width="225" style="border: #BF0000 solid 3px; background-color: #FFF;">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <a href="<%=deal4url%>">
                                                                                                <img src="<%=deal4pic%>" width="215" height="123" border="0" alt="<%=deal4title%>" /></a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <div style="height: 42px">
                                                                                                <a href="<%=deal4url%>" style="font-size: 13px; font-family: '微軟正黑體'; color: #000;
                                                                                                    text-decoration: none; line-height: 20px;">
                                                                                                    <%=deal4title%></a>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <a href="<%=deal4url%>" style="color: #F60; font-size: 18px; font-weight: bold;">特價$<%=deal4price%>
                                                                                                搶購</a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="3">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <!--DealBlock_4--End-->
                                                                            <!--DealBlock_5--Start-->
                                                                            <td width="230" align="center">
                                                                                <table width="225" style="border: #BF0000 solid 3px; background-color: #FFF;">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <a href="<%=deal5url%>">
                                                                                                <img src="<%=deal5pic%>" width="215" height="123" border="0" alt="<%=deal5title%>" /></a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <div style="height: 42px">
                                                                                                <a href="<%=deal5url%>" style="font-size: 13px; font-family: '微軟正黑體'; color: #000;
                                                                                                    text-decoration: none; line-height: 20px;">
                                                                                                    <%=deal5title%></a>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <a href="<%=deal5url%>" style="color: #F60; font-size: 18px; font-weight: bold;">特價$<%=deal5price%>
                                                                                                搶購</a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="3">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <!--DealBlock_5--End-->
                                                                            <!--DealBlock_6--Start-->
                                                                            <td width="230" align="center">
                                                                                <table width="225" style="border: #BF0000 solid 3px; background-color: #FFF;">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <a href="<%=deal6url%>">
                                                                                                <img src="<%=deal6pic%>" width="215" height="123" border="0" alt="<%=deal6title%>" /></a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <div style="height: 42px">
                                                                                                <a href="<%=deal6url%>" style="font-size: 13px; font-family: '微軟正黑體'; color: #000;
                                                                                                    text-decoration: none; line-height: 20px;">
                                                                                                    <%=deal6title%></a>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <a href="<%=deal6url%>" style="color: #F60; font-size: 18px; font-weight: bold;">特價$<%=deal6price%>
                                                                                                搶購</a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td height="3">
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <!--DealBlock_6--End-->
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="7" colspan="3">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="7">
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                        </td>
                                        <td width="25">
                                        </td>
                                    </tr>
                                </table>
                                <!--EDM Main-End-->
                            </td>
                        </tr>
                    </table>
                    <!--EDM Footer-Start-->
                    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #333;
                        color: #FFF;">
                        <tr>
                            <td width="25">
                            </td>
                            <td height="80">
                                <br />
                                <a href="<%=site_url + site_service_url%>" target="_blank" style="color: #FFF">
                                    客服中心</a> | <a href="https://www.17life.com/Ppon/WhiteListGuide.aspx" target="_blank"
                                        style="color: #FFF">加入白名單</a> | 服務時間：平日 9:00~18:00<br />
                                17Life 康太數位整合股份有限公司 版權所有 轉載必究 © 2016
                            </td>
                        </tr>
                    </table>
                    <!--EDM Footer-End-->
                </td>
            </tr>
            <tr>
                <td height="25" style="background: #FFF;">
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
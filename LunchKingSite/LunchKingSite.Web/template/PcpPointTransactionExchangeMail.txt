﻿<%@ Argument Name="site_url" Type="string" %>
<%@ Argument Name="site_service_url" Type="string" %>
<%@ Argument Name="seller_name" Type="string" %>
<%@ Argument Name="exchange_amount" Type="int" %>
<html>
<body>
<div style=" width:100%; margin:0 auto; font-family:Arial, Microsoft JhengHei, Helvetica, sans-serif">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td style="font-size:13px; background:#fff;">
    <tr><td>
    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background:#FFD5C4; color:#333;">    
    <tr>
    <td width="20" height="80"></td>
    <td width="110" height="80">
    <a href="https://www.17life.com" target="_blank"><img src="https://www.17life.com/Themes/PCweb/images/EDMLOGO.png" width="132" height="80" alt="" border="0" /></a>
    </td>
    <td width="480" height="80" >
      <div style="font-size:33px; color:#646464; padding-top:7px;">商家客服中心</div>
    </td>
    <td width="110" height="80" style="text-align:right;">
      <a href="https://www.17life.com" target="_blank" style="color:#646464;">www.17life.com</a>
      <a href="https://www.17life.com/piinlife/" target="_blank" style="color:#646464;">www.Piinlife.com</a>
    </td>
    <td width="20" height="80"></td>
    </tr>
    </table>
    <!--EDM Header-End-->
    <!--EDM Main-Start-->
    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background:#FFD5C4;">
    <tr>
    <td width="25"></td>
    <td width="740">
    	<!--Ad Area-Start-->
    	<table width="740" cellpadding="0" cellspacing="0" align="center">
    	<tr><td height="5" style="border-top:1px solid #FFB99E;"></td></tr>
        <tr>
        </tr>
        </table>
        <table width="740" border="0" align="center" cellpadding="0" cellspacing="0" style=" color:#000; font-size:13px; background-color:#FFF;border:1px solid #DDD;">
        <tr>
           <td height="5"></td>
        </tr>
        <tr>
           <td align="center">
           
              <table width="700" border="0" cellspacing="1" cellpadding="1" style="background-color:#FFF;text-align:left;">
                <tr>
                  <td height="50">親愛的<span style="color:#F60; font-size:16px;"><%=seller_name%></span>您好：
               <tr>
                  <td height="60" style="line-height:23px;">
				     超級紅利金請款已處理完成，款項<span style="color:#F60;"><%=exchange_amount%></span>元已經匯入指定帳戶內，<br />
					 麻煩您於近日至指定帳戶確認是否已收到款項，<br />
					 若有需要協助的部分，請隨時與我們聯繫，謝謝。
                   </td>          
              <tr>
                 <td height="70" valign="bottom" style="line-height:26px;"> 祝  商祺    17Life  敬上</td>
              </tr>
             </table>

          </td>
        </tr>
        <tr>
           <td height="5"></td>
        </tr>
      </table>
        <br />

    </td>
    <td width="25"></td>
    </tr>
    </table>
    <!--EDM Main-End-->
    </td></tr>
    </table>
    <!--EDM Footer-Start-->
    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background:#333; color:#FFF;">
    <tr>
    <td width="25"></td>
    <td height="80">
    <br />
    <a href="<%=site_url + site_service_url%>" target="_blank" style="color:#FFF">客服中心</a> | 
    <a href="https://www.17life.com/Ppon/WhiteListGuide.aspx" target="_blank" style="color:#FFF">加入白名單</a> | 
    服務時間：平日 9:00~18:00<br />
	17Life 康太數位整合股份有限公司 版權所有 轉載必究 © 2016
    </td>
    </tr>
    </table>
    <!--EDM Footer-End-->
</td>
</tr>
<tr>
<td height="25" style="background:#FFF;"></td>
</tr>
</table>

</div>
</body>
</html>

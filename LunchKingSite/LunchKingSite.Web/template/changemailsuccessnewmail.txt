﻿<%@ Argument Name="member_name" Type="string" %>
<%@ Argument Name="site_url" Type="string" %>
<%@ Argument Name="site_service_url" Type="string" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>17P New EDM</title>
</head>

<body>
<div style=" width:100%; margin:0 auto; font-family:Arial, Microsoft JhengHei, Helvetica, sans-serif">
	<!--EDM Notification-Start-->
    <!--EDM Notification-End-->
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td style="font-size:13px; background:#fff;">

    <table width="770" cellpadding="0" cellspacing="0" align="center">
    <tr><td>
    <!--EDM Header-Start-->
    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background:#F3F3EE; color:#333;">
    
    <tr>
    <td width="20" height="80"></td>
    <td width="140" height="80">
    <a href="https://www.17life.com" target="_blank"><img src="https://www.17life.com/Themes/PCweb/images/EDMLOGO.png" width="132" height="80" alt="" border="0" /></a>
    </td>
    <td width="480" height="80" style="font-size:26px; font-weight:bold;">
	<a href="https://www.17life.com/piinlife/" target="_blank"><img src="https://www.17life.com/Themes/default/images/17Life/EDM/EDMLOGO_HD.png" width="260" height="80" alt="" border="0" /></a>
	</td>
    <td width="110" height="80" style="text-align:right;">
	<a href="https://www.17life.com" target="_blank" style="color:#999;">www.17life.com</a>
	<a href="https://www.17life.com/piinlife/" target="_blank" style="color:#999;">www.Piinlife.com</a>
	</td>
    <td width="20" height="80"></td>
    </tr>
    </table>
    <!--EDM Header-End-->
    <!--EDM Main-Start-->
    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background:#F3F3EE;">
    <tr>
    <td width="25"></td>
    <td width="740">
        <!--MainDeal Block-Start-->
        <table width="740" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:'微軟正黑體'; color:#000; line-height:30px; font-size:14px; background-color:#FFF;border:1px solid #DDD;">
        <tr>
           <td height="5"></td>
        </tr>
        <tr>
           <td align="center">
           
              <table width="700" border="0" cellspacing="1" cellpadding="1" style="background-color:#FFF; font-weight: bold; text-align:left;">
                <tr>
                  <td height="50">親愛的<span style="color:#F60; font-size:16px;"><%=member_name%></span>您好：
                      </td>
                    </tr>
               <tr>
                  <td height="70">
                    <p>
                      您已完成17Life會員信箱變更，日後所有17Life相關通知信，將統一發送至此信箱。<br />
                      若您曾經開通17Life帳號，也請同時改以此電子信箱，做為登入帳號。
                    </p>
                  </td>
               </tr>
              <tr>
                 <td height="20"></td>
              </tr>
              <tr>
                 <td height="30" style=" line-height:24px;">17Life團隊</td>
              </tr>
              <tr>
                 <td height="20"></td>
              </tr>
              <tr>
                 <td style=" border-top: 1px dashed #6d9345; margin-top:10px; margin-bottom:10px;line-height:23px;">
                   <table border="0" cellspacing="0" cellpadding="0">
                     <tr>
                       <td height="5"></td>
                     </tr>
                     <tr>
                       <td style=" font-size:13px;"><br/>
                       若有任何問題，請聯絡<a href="<%=site_url + site_service_url%>" target="_blank">17Life客服中心</a><br/>
                       </td>
                     </tr>
                      <tr>
                       <td height="5"></td>
                     </tr>
                   </table>

                 
                 </td>
              </tr>
              <tr>
                 <td height="30" valign="bottom" style="color:#bf0000;">提醒：電子郵件通知服務為系統自動發送，請勿直接回覆這一封信。</td>
              </tr>
             </table>

          </td>
        </tr>
        <tr>
           <td height="5"></td>
        </tr> 
      </table>
        <br />

    </td>
    <td width="25"></td>
    </tr>
    </table>
    <!--EDM Main-End-->
    </td></tr>
    </table>
    <!--EDM Footer-Start-->
    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background:#333; color:#FFF;">
    <tr>
    <td width="25"></td>
    <td height="80">
    <br />
    <a href="<%=site_url + site_service_url%>" target="_blank" style="color:#FFF">客服中心</a> | 
    <a href="https://www.17life.com/Ppon/WhiteListGuide.aspx" target="_blank" style="color:#FFF">加入白名單</a> | 
    服務時間：平日 9:00~18:00<br />
	17Life 康太數位整合股份有限公司 版權所有 轉載必究 © 2016
    </td>
    </tr>
    </table>
    <!--EDM Footer-End-->
</td>
</tr>
<tr>
<td height="25" style="background:#FFF;"></td>
</tr>
</table>

</div>
</body>
</html>

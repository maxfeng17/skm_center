﻿<%@ Argument Name="member_name" Type="string" %>
<%@ Argument Name="item_name" Type="string" %>
<%@ Argument Name="order_id" Type="string" %>
<%@ Argument Name="credit" Type="string" %>
<%@ Argument Name="freight" Type="string" %>
<%@ Argument Name="total_price" Type="int" %>
<%@ Argument Name="site_url" Type="string" %>
<%@ Argument Name="site_service_url" Type="string" %>
<%@ Argument Name="r_credit" Type="string" %>
<%@ Argument Name="r_freight" Type="string" %>
<%@ Argument Name="r_total_price" Type="int" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>17Life New EDM</title>
</head>
<body>
    <div style="width: 100%; margin: 0 auto; font-family: Arial, Microsoft JhengHei, Helvetica, sans-serif">
        <!--EDM Notification-Start-->
        <!--EDM Notification-End-->
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="font-size: 13px; background: #fff;">
                    <table width="770" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <td style="font-size: 13px; color: #000; padding: 15px; line-height: 27px; background-color: #FFF;">
                                <!--EDM Header-Start-->
                                <font size="3" style="background-color: transparent; font-size: 1em;"><b>本服務由雙北行動購物牆與17Life共同提供</b></font>
                                <br/>
                                <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE;
                                    color: #333;">
                                    <tr>
                                        <td width="20" height="80">
                                        </td>
                                        <td width="140" height="80">
                                            <a href="https://www.17life.com" target="_blank">
                                                <img alt="17Life" src="https://www.17life.com/Themes/PCweb/images/EDMLOGO.png"
                                                    width="132" height="80" /></a>
                                        </td>
                                        <td width="480" height="80" style="font-size: 26px; font-weight: bold;">
											<a href="https://www.17life.com/piinlife/" target="_blank">
                                                <img alt="17Life" src="https://www.17life.com/Themes/default/images/17Life/EDM/EDMLOGO_HD.png"
                                                    width="260" height="80" /></a>
                                        </td>
                                        <td width="110" height="80" style="text-align: right;">
                                            <a href="https://www.17life.com" target="_blank" style="color: #999;">www.17life.com</a>
                                            <a href="https://www.17life.com/piinlife/" target="_blank" style="color: #999;">www.Piinlife.com</a>
                                        </td>
                                        <td width="20" height="80">
                                        </td>
                                    </tr>
                                </table>
                                <!--EDM Header-End-->
                                <!--EDM Main-Start-->
                                <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE;">
                                    <tr>
                                        <td width="25">
                                        </td>
                                        <td width="740">
                                            <table width="740" cellpadding="0" cellspacing="0" align="center">
                                                <tr>
                                                    <td height="15" style="border-top: 1px solid #DDD;">
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="740" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family: '微軟正黑體';
                                                color: #000; font-size: 13px; background-color: #FFF; border: 1px solid #DDD;">
                                                <tr>
                                                    <td height="5">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <!--Content Area Start-->
                                                        <table width="700" border="0" cellspacing="1" cellpadding="1" style="background-color: #FFF;
                                                            font-weight: bold; text-align: left;">
                                                            <tr>
                                                                <td height="50">
                                                                    親愛的<span style="color: #F60; font-size: 16px;"><%=member_name%></span>您好：
                                                                    <tr>
                                                                        <td height="30" style="border-bottom: 1px dashed #999;">
                                                                            您申請取消的訂單已成功，訂單明細如下：
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="15">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            訂單編號：<span style="color: #6d9345;"><%=order_id%></span><br />
                                                                            好康名稱：<span style="color: #6d9345;"><%=item_name%></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="10">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="130">
                                                                            <table border="0" cellpadding="0" cellspacing="0" style="color: #6d9345; line-height: 25px;">
                                                                                <tr>
                                                                                    <td width="200" style="color: #000;">
                                                                                        付款明細：
                                                                                    </td>
                                                                                    <td>
                                                                                    </td>
                                                                                </tr>
                                                                                <%=credit%>
                                                                                <tr>
                                                                                    <td width="200">
                                                                                        總金額
                                                                                    </td>
                                                                                    <td>
                                                                                        NT$ <%=total_price%>元<%=freight%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="30" colspan="2">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="200" style="color: #000;">
                                                                                        退款明細：
                                                                                    </td>
                                                                                    <td>
                                                                                    </td>
                                                                                </tr>
                                                                                <%=r_credit%>
                                                                                <tr>
                                                                                    <td width="200">
                                                                                        總金額
                                                                                    </td>
                                                                                    <td>
                                                                                        NT$ <%=total_price%>元<%=freight%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="30">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            當17Life退貨處理中心為您取消訂單後，將會依照您選擇之退款方式及原扣抵方式，將費用全額退回。ATM退款則將退至您提供的受款帳戶，請您放心！
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="20">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="border: dashed  1px #999; margin-top: 10px; margin-bottom: 10px; line-height: 23px;">
                                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                                <tr>
                                                                                    <td height="5">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="font-size: 13px;">
                                                                                        貼心提醒：<br />
                                                                                        ◆ 客服退貨完成後，因各銀行作業時間不同，如選擇刷退，約需額外等待30-60個工作天退至信用卡帳戶；如選擇ATM付款，款項退至您指定帳戶約需額外等待30個工作天，若退款資訊不全，將影響退貨處理時間。<br />
                                                                                        ◆ 17Life客服人員不會主動以email或電話要求您更改結帳方式或提供個人資料，若您接獲類似訊息，請拒絕回應，並立刻與17Life客服人員聯絡。<br />
                                                                                        ◆ 此信函為系統自動發出，請勿直接回覆，有任何問題請至<a href="<%=site_url + site_service_url%>" style="color: #F60;
                                                                                            font-family: Arial, Helvetica, sans-serif;">客服信箱</a>與我們聯繫。<br />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="5">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="100">
                                                                            17Life 敬上
                                                                        </td>
                                                                    </tr>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!--Content Area End-->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="5">
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                        </td>
                                        <td width="25">
                                        </td>
                                    </tr>
                                </table>
                                <!--EDM Main-End-->
                            </td>
                        </tr>
                    </table>
                    <!--EDM Footer-Start-->
                    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #333;
                        color: #FFF;">
                        <tr>
                            <td width="25">
                            </td>
                            <td height="80">
                                <br />
                                <a href="<%=site_url + site_service_url%>" target="_blank" style="color: #FFF">
                                    客服中心</a> | <a href="https://www.17life.com/Ppon/WhiteListGuide.aspx" target="_blank"
                                        style="color: #FFF">加入白名單</a> | 服務時間：平日 9:00~18:00<br />
                                17Life 康太數位整合股份有限公司 版權所有 轉載必究 © 2016
                            </td>
                        </tr>
                    </table>
                    <!--EDM Footer-End-->
                </td>
            </tr>
            <tr>
                <td height="25" style="background: #FFF;">
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
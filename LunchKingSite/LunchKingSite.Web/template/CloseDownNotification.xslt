﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="html" indent="yes"/>

    <xsl:template match="/SellerOrStoreCloseDownInformation">
        <html xmlns="http://www.w3.org/1999/xhtml">

            <head>
                <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                <title>Untitled 1</title>
                <style type="text/css">
                    .auto-style1 {
                    border-collapse: collapse;
                    border: 1px solid #000000;
                    }
                    .auto-style2 {
                    border: 1px solid #000000;
                    }
                </style>
            </head>

            <body>

                <table class="auto-style1" style="width: 100%">
                    <tr>
                        <td class="auto-style2">

                            親愛的同仁，您好<br /><br/>

                            店名：<xsl:value-of select="SellerName/text()"/>
                                                <xsl:if test="string-length(StoreName/text())!=0">
                                                    <xsl:value-of select="StoreName/text()"/>
                                                </xsl:if><br />

                            結束營業日期：<xsl:value-of select="/SellerOrStoreCloseDownInformation/CloseDownDate/text()"/><br />
                            <span style="color: rgb(255, 0, 0); font-family: Arial; font-size: 18px; font-style: normal; font-variant: normal; font-weight: bold; letter-spacing: normal; line-height: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; display: inline !important; float: none; ">
                                提醒您！此店家結束營業，請相關人員注意~謝謝！
                            </span>
                        </td>
                    </tr>
                </table>

            </body>

        </html>

    </xsl:template>
</xsl:stylesheet>


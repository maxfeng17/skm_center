var main_view_swiper = new Swiper('#main-view-swiper',{
    spaceBetween: 30,
    effect: 'fade',
    loop:true,
    pagination: {
    el: '.swiper-pagination',
    clickable: true,
    },
    autoplay: {
        delay: 4000,
        disableOnInteraction: false,
    },
    navigation: {
        nextEl: '#main-view-next',
        prevEl: '#main-view-prev',
    },
})
var main_view_swiper_2 = new Swiper('#main-view-swiper-bg',{
    spaceBetween: 30,
    effect: 'fade',
    loop:true,
    pagination: {
    el: '.swiper-pagination',
    clickable: true,
    },
    autoplay: {
        delay: 4000,
        disableOnInteraction: false,
    },
    navigation: {
        nextEl: '#main-view-next',
        prevEl: '#main-view-prev',
    },
})
window.ApiSuccessCode = 1000;
window.ApiToken = '705c0cb742d24d7fb85823e052124a64_cf56566a9de546bb840f9a5eae89ddda';
window.EmailDomainSource = ["yahoo.com.tw", "hotmail.com", "gmail.com", "outlook.com", "kimo.com", "msa.hinet.net", "msn.com",
	"yahoo.com", "17life.com", "hotmail.com.tw", "taishinbank.com.tw", "xuite.net", "livemail.tw", "yam.com", "payeasy.com.tw",
	"ymail.com", "mail2000.com.tw", "seed.net.tw", "facebook.com", "tsmc.com", "taipower.com.tw", "itri.org.tw"];

function passwordCheck(password) {
	var pwdPattern = /^[0-9a-zA-Z]{6,100}$/;
	if (pwdPattern.test(password)) {
		return true;
	} else {
		return false;
	}
}

$.disalbeAutoFill = function () {
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
		$('input').filter(function () {
			return $(this).attr('autocomplete') != null;
		}).attr('autocomplete', 'off');
	} else {
		//chrome sucks
		$('input').filter(function () {
			return $(this).attr('autocomplete') != null;
		}).attr('autocomplete', 'new-password');
	}
};

$.getQueryParam = function (sParam) {
	var sPageURL = decodeURIComponent(window.location.search.substring(1)),
		sURLVariables = sPageURL.split('&'),
		sParameterName,
		i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : sParameterName[1];
		}
	}
};


$(function () {
    /***
    *
    *商品頁:頁面捲動到詳細內容nav以下時，nav置頂
    *
    ***/
	var maniNav_height = $('.detail_mainNav').height();  //商品頁的詳細內容nav高度
	/*捲軸低於mainNav則讓mainNav固定置頂*/
	$(window).scroll(function () {
		if ($(this).scrollTop() > $(".detail_mainIntro").offset().top - maniNav_height) {
			$('.detail_mainNav').addClass("fixedNav");
			$('.detail_mainIntro').css('margin-top', maniNav_height);
		} else {
			$('.detail_mainNav').removeClass("fixedNav");
			$('.detail_mainIntro').css('margin-top', '0px');
		}
	});
	/*錨點*/
	var mainNav_1_pos = $('.description').position().top - maniNav_height;   //mainNav_1對應的位置
	var mainNav_2_pos = $('.explain').position().top - maniNav_height;       //mainNav_2對應的位置
	var mainNav_3_pos = $('.related').position().top - maniNav_height;       //mainNav_3對應的位置
	var flag_mainNav_1 = 0;
	var flag_mainNav_2 = 0;
	var flag_mainNav_3 = 0;

	/*隨著捲軸移動nav會自動顯示正確顏色(位置)*/
	$(window).scroll(function () {
		mainNav_1_pos = $('.description').position().top - maniNav_height;   //mainNav_1對應的位置
		mainNav_2_pos = $('.explain').position().top - maniNav_height;       //mainNav_2對應的位置
		mainNav_3_pos = $('.related').position().top - maniNav_height;       //mainNav_3對應的位置
		var scrollTop_now = $(window).scrollTop() + maniNav_height;

		if (scrollTop_now >= mainNav_1_pos && scrollTop_now < mainNav_2_pos) {
			$('.mainNav').removeClass('active');
			$('.mainNav_1').addClass('active');
			flag_mainNav_1 = 1;
			flag_mainNav_2 = 0;
			flag_mainNav_3 = 0;
		}
		else if (scrollTop_now >= mainNav_2_pos && scrollTop_now < mainNav_3_pos) {
			$('.mainNav').removeClass('active');
			$('.mainNav_2').addClass('active');
			flag_mainNav_1 = 0;
			flag_mainNav_2 = 1;
			flag_mainNav_3 = 0;
		}
		else if (scrollTop_now >= mainNav_3_pos) {
			$('.mainNav').removeClass('active');
			$('.mainNav_3').addClass('active');
			flag_mainNav_1 = 0;
			flag_mainNav_2 = 0;
			flag_mainNav_3 = 1;
		}
	});

	/*點擊mainNav 畫面自動捲到對應位置*/
	$('.detail_mainNav').find('.mainNav_1').click(function () {
		if (flag_mainNav_1 == 0) {
			$("html, body").animate({
				scrollTop: ($(".detail_mainIntro").position().top - 0)
			}, 'slow');
			$('.mainNav').removeClass('active');
			$(this).addClass('active');
			stop(true, false);
			flag_mainNav_1 = 1;
			flag_mainNav_2 = 0;
			flag_mainNav_3 = 0;
		}
	});
	$('.detail_mainNav').find('.mainNav_2').click(function () {
		if (flag_mainNav_2 == 0) {
			stop(false, false);
			$("html, body").animate({
				scrollTop: ($(".explain").position().top - maniNav_height)
			}, 'slow');
			$('.mainNav').removeClass('active');
			$(this).addClass('active');
			stop(true, false);
			flag_mainNav_1 = 0;
			flag_mainNav_2 = 1;
			flag_mainNav_3 = 0;
		}
	});
	$('.detail_mainNav').find('.mainNav_3').click(function () {
		if (flag_mainNav_3 == 0) {
			stop(false, false);
			$("html, body").animate({
				scrollTop: ($(".related").position().top - maniNav_height)
			}, 'slow');
			$('.mainNav').removeClass('active');
			$(this).addClass('active');
			stop(true, false);
			flag_mainNav_1 = 0;
			flag_mainNav_2 = 0;
			flag_mainNav_3 = 1;
		}
	});

	/*頁面捲動到相關商品以下時，buyAction_wrap回歸原始位置*/
	$(window).scroll(function () {
		var scrollBottom = $(window).scrollTop() + $(window).height();
		if (scrollBottom > $(".related").offset().top + $(".related").height()) {
			$('.buyAction_wrap').removeClass("fixedBuyAction");
		} else {
			$('.buyAction_wrap').addClass("fixedBuyAction");
		}
	});

	/*收藏功能*/


	window.SetDealCollectUI = function (collected) {
		if (collected) {
			//加入最愛
			$('.fav').find('i').removeClass('fa-heart-o');
			$('.fav').find('i').addClass('active');
			$('.text_fav').html('已收藏');
		} else {
			//取消加入
			$('.fav').find('i').addClass('fa-heart-o');
			$('.fav').find('i').removeClass('active');
			$('.text_fav').html('收藏');
		}
	}

	$.ajax({
		type: "POST",
		url: "/Ppon/Default.aspx/CheckMemberCollectDealStatus",
		data: "{businessHourGuid:'" + window.businessHourGuid + "'}",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (msg) {
			if (msg.d) {
				window.isDealCollected = true;
			} else {
				window.isDealCollected = false;
			}
			console.log('collected: ' + window.isDealCollected);
			SetDealCollectUI(window.isDealCollected);
		}
	});

	$.ajax({
		type: "POST",
		url: '/Event/GetBrandId',
		data: "{bid:'" + window.businessHourGuid + "'}",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (res) {
			if (res.length === 0) {
				return;
			}
			$('#bn_coupon').show();
			console.log('brand: ' + res[0].BrandId);
			$('#bn_coupon').attr('brandId', res[0].BrandId);

			window.doPageAction();
		},
		error: function (err) {
			alert("伺服器忙碌中，請重新操作一次。");
		}
	});

	$('#bn_coupon').click(function () {
		var brandId = $(this).attr('brandId');
		$.ajax({
			type: "POST",
			url: '/Event/GetDiscountCode',
			data: {
				brandId: brandId,
				bid: window.businessHourGuid
			},
			dataType: "html",
			success: function (data) {
				console.log(data);
				if (window["localStorage"] != null) {
					window["localStorage"].setItem('dealShowCoupn', window.dealId);
				};
				$('#renderpage').empty().append(data);
				$('#contentDetail').hide();
				$('#coupon_wrapper').show();
			}
		});
	});

	$('#coupon_wrapper').on('click', '#buy_btn_moblie', function () {
		$('#coupon_wrapper').hide();
		$('.buyAction_wrap li.buy_now').trigger('click');
	});

	window.unblock_coupon_mb = function () {
		$('#contentDetail').show();
		$('#coupon_wrapper').hide();
		if (window["localStorage"] != null) {
			window["localStorage"].removeItem('dealShowCoupn');
		};
	}

	// bn_share 
	$('.bn_share').click(function (e) {
		e.preventDefault();
		$('.share_normal').css('display', 'block');
		$('.share_gotologin').css('display', 'none');

		// 現使用bpopup 啟動
		$('.share_wrap').bPopup({
			amsl: 20,
			modalClose: false,
			positionStyle: 'fixed',
			onOpen: function () {
			},
			onClose: function () {
			}
		});

	});

	$('.fav').click(function () {
		SetDealCollect(!window.isDealCollected);
	});

	window.SetDealCollect = function (isCollecting) {
		if (window.businessHourGuid != null && window.businessHourGuid.length > 0) {
			var returnUrl = encodeURI(window.location.href);
			var loginUrl = '/m/login?ReturnUrl=' + returnUrl;
			$.ajax({
				type: "POST",
				url: "/Ppon/Default.aspx/SetMemberCollectDeal",
				data: JSON.stringify({ "businessHourGuid": window.businessHourGuid, cityId: 0, isCollecting: isCollecting }),
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (msg) {
					var result = $.parseJSON(msg.d);
					if (result.Success) {
						window.isDealCollected = result.IsCollecting;
						SetDealCollectUI(window.isDealCollected);
					}
				}, error: function (response, q, t) {
					if (t == 'userNotLogin') {
						location.href = loginUrl;
					}
				}
			});
		}
		return false;
	}


	/*overHeight 高度太高的滑動開關*/
	//如果overHeight高度大於400px
	//限制高度並出現按鈕            
	var overHeight_height = $('.overHeight').height();
	if (overHeight_height >= 400) {
		var flag_overHeight = 0; //0收合;1展開
		$(this).find('.overHeight_sliderBtn a').click(function () {
			if (flag_overHeight == 0) {
				$(this).parent().parent().addClass('overHeight_SlideDown');
				$(this).find('.fa-angle-down').css('display', 'none');
				$(this).find('.fa-angle-up').css('display', 'inline-block');
				$(this).find('span').html('收合');
				flag_overHeight = 1;
			} else {
				$(this).parent().parent().removeClass('overHeight_SlideDown');
				$(this).find('.fa-angle-down').css('display', 'inline-block');
				$(this).find('span').html('展開看詳細');
				$(this).find('.fa-angle-up').css('display', 'none');
				flag_overHeight = 0;
			}
		});
	} else {
		$('.overHeight_sliderBtn').css('display', 'none');
	}

	$('.buyAction_wrap li.buy_now').click(function () {
		if ($(this).attr('buyUrl') === "soldout") {
			return;
		}
		if ($(this).attr('buyUrl') != null && $(this).attr('buyUrl') !== "") {
			location.href = $(this).attr('buyUrl');
		} else {
			$('#contentDetail').hide();
			$('.header_wrap').hide();
			$('.footer_wrap').hide();

			$('#contentChoice').show();
			window.scrollTo(0, 0);
		}
		return;
	});

	$('#btnCloseChoice').click(function () {
		$('#contentDetail').show();
		$('.header_wrap').show();
		$('.footer_wrap').show();
		$('#contentChoice').hide();
		window.scrollTo(0, 0);
		return;
	});

	$('#btnAppBuy').click(function () {
		var deepLink = $(this).attr('href');
		if (navigator.userAgent != null && navigator.userAgent.toLowerCase().match("iphone|ipod")) {
			var now = new Date().valueOf();
			window.setTimeout(function () {
				if (new Date().valueOf() - now > 100) return;
				window.location = "https://itunes.apple.com/tw/app/17life/id543439591";
			}, 50);
			window.location = deepLink;
		} else if (navigator.userAgent.toLowerCase().match("android")) {
			window.setTimeout(function () {
				window.location = "market://details?id=com.uranus.e7plife";
			}, 25);
			window.location = deepLink;
		}
	});

	$("#detailDesc").find("img").lazyload({ placeholder: "/Themes/PCweb/images/ppon-M1_pic.jpg", effect: "fadeIn" });
	//移除跟屁蟲
	$("div.header_wrap.header_wrap_slider").remove();

	window.doPageAction = function () {
		//執行額外的action
		if (window["localStorage"] != null && window["localStorage"].getItem('dealShowCoupn') === window.dealId) {
			$('#bn_coupon').trigger('click');
		}
	}

});

window.openSharePopWindowForLine = function () {
	window.shareType = 'line';
	openSharePopWindow('https://www.17life.com/ppon/fbshare_proxy.aspx?type=line&bid=' + window.dealId + '&itemname=' + encodeURI(window.dealName));
}

window.openSharePopWindowForFB = function () {
	window.shareType = 'fb';
	openSharePopWindow('https://www.17life.com/ppon/fbshare_proxy.aspx?type=facebook&bid=' + window.dealId);
}

window.openSharePopWindowForFBM = function () {
	window.shareType = 'fbm';
	openSharePopWindow('https://www.17life.com/ppon/fbshare_proxy.aspx?type=fbm&bid=' + window.dealId);
}

window.openSharePopWindowForClipboard = function () {
	window.shareType = 'clipboard';
	openSharePopWindow();
}

window.openSharePopWindow = function (shareUrl) {
	if (window.isAuthenticated === false) {
		$('.share_normal').hide();
		$('.share_gotologin').show();
	} else if (window.shareType === 'clipboard') {
		var url = getShareLink(window.dealId);
		if (copyStringToClipboard(url)) {
			window.setTimeout(
				function () {
					$('.b-close').click();
				}, 10);
		}
	} else {
		openWin(shareUrl);
	}
}

$('#btnDirectlyShare').click(function () {
	$('.share_normal').show();
	$('.share_gotologin').hide();
	if (guestShare(window.shareType, window.dealName, window.dealId)) {
		window.setTimeout(
			function () {
				$('.b-close').click();
			}, 10);
	}
});

window.guestShare = function (shareType, shareContent, dealId) {
	var shareUrl = 'https://www.17life.com/deal/' + dealId;
	if (shareType === "fb") {
		openWin("http://www.facebook.com/share.php?u=" + encodeURI(shareUrl));
	} else if (shareType === "fbm") {
		openWin("fb-messenger://share/?link=" + encodeURI(shareUrl));
	} else if (shareType === "line") {
		openWin("http://line.me/R/msg/text/?" + encodeURI(shareContent + " " + shareUrl));
	} else if (shareType === 'clipboard') {
		return copyStringToClipboard(shareUrl);
	}
	return true;
}

window.openWin = function (url) {
	var isSafari = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
	if (isSafari) {
		newWin = window.open('');
		newWin.location.href = url;
	} else {
		window.open(url);
	}
}

function copyStringToClipboard(string) {
	if ($("#clipboardText").length == 0) {
		$('<input type="text" id="clipboardText" />').appendTo('body');
		$("#clipboardText").css({
			position: 'absolute',
			top: '-100px'
		});
	}
	$("#clipboardText").val(string);
	var node = $('#clipboardText')[0];
	document.body.style.webkitUserSelect = 'initial';

	var selection = document.getSelection();
	selection.removeAllRanges();

	var range = document.createRange();
	range.selectNodeContents(node);
	selection.addRange(range);
	// This makes it work in all desktop browsers (Chrome)
	node.select();
	// This makes it work on Mobile Safari
	node.setSelectionRange(0, 999999);

	try {
		if (document.execCommand('copy')) {
			selection.removeAllRanges();
			return true;
		} else {
			return false;
		}
	} catch (ex) {
		return false;
	}
}

function getShareLink(dealId) {
	var res;
	$.ajax({
		type: "POST",
		url: '/ppon/default.aspx/GetShareLink',
		data: "{businessHourId:'" + dealId + "'}",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (msg) {
			if (msg.d != '') {
				res = msg.d;
			} else {
				location.href = window.loginUrl;
			}
		},
		async: false
	});
	return res;
}

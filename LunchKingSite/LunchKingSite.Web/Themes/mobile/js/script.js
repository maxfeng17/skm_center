//Facebook Pixel Code
!function (f, b, e, v, n, t, s) {
	if (f.fbq) return; n = f.fbq = function () {
		n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments)
	}; if (!f._fbq) f._fbq = n; n.push = n; n.loaded = !0; n.version = '2.0'; n.queue = []; t = b.createElement(e); t.async = !0;
	t.src = v; s = b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s)
}(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '274288942924758');
fbq('track', 'PageView');


$(function () {
    /***
	*
	*正常選單(預設:關)
	*
	***/
	$('.header_wrap_normal .fa-angle-up').hide();
	$('.header_wrap_normal .nav').hide();
	$('.header_wrap_normal').css('border-bottom', '0px');

    /***
	*
	*跟屁蟲選單(預設)
	*
	***/
	$('.header_wrap_slider').hide();
	$('.header_wrap_slider .nav').hide();
	$('.header_wrap_slider .fa-angle-down').css('display', 'inline-block');
	$('.header_wrap_slider .fa-angle-up').css('display', 'none');
	$('.header_wrap_slider').css('border-bottom', '0px');

    /***
	*
	*index 預設(展開)
	*
	***/
	$('.open_header_nav .fa-angle-up').show();
	$('.open_header_nav .fa-angle-down').hide();
	$('.open_header_nav .nav').show();
	$('.open_header_nav ').css('border-bottom', '1px solid #ddd');
    /***
	*
	*選單 上下滑開關
	*
	***/
	//prevent duplicated event
	if ($(".slider_btn").length > 0 && (
		$._data($(".slider_btn")[0], "events") == null || $._data($(".slider_btn")[0], "events").click == null)) {
		$('.header_wrap .slider_btn').click(function () {
			var head = $(this).closest('.header_wrap');
			var nav = head.find('.nav');
			if (nav.is(':visible')) {
				nav.slideUp({ duration: 500, easing: "easeOutExpo" });
				head.find('.fa-angle-up').hide();
				head.find('.fa-angle-down').show();
				head.css('border-bottom', '0px');
			} else {
				nav.slideDown({ duration: 500, easing: "easeOutExpo" });
				head.find('.fa-angle-up').show();
				head.find('.fa-angle-down').hide();
				head.css('border-bottom', '1px solid #ddd');
			}
		});
	}

	$('.header_wrap .nav a').click(function () {
		$('.header_wrap .nav a span').removeClass('on');
		$(this).find('span').addClass('on');
		$.blockUI({
			css: {
				border: 'none',
				padding: '15px',
				backgroundColor: '#000',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px',
				opacity: .5,
				color: '#fff'
			}, message: '讀取中'
		});
	});

    /***
	*
	*跟屁蟲選單 超過一定高度自己跑出來(目前設定為1136:iphone5的螢幕高)
	*
	***/
	$(window).scroll(function () {
		$('.header_wrap_slider .fa-angle-up').hide();
		$('.header_wrap_slider .fa-angle-down').show();
		var triggerHeight = $('.header_wrap_normal .header').height();
		if ($('.header_wrap_normal .nav').is(':visible')) {
			triggerHeight += $('.header_wrap_normal .nav').height();
		}
		if ($(this).scrollTop() > triggerHeight) {
			$('.header_wrap_slider').slideDown({ duration: 500, easing: "easeOutExpo" });
			$('.header_wrap_slider .nav').hide();
		} else {
			$('.header_wrap_slider').slideUp({ duration: 500, easing: "easeOutExpo" });
		}

		//滑動即隱藏top廣告
		if ($(document).scrollTop() > 0) {
			$(".appbn_top").css({ "display": "none" });
		}
	});


	/** app download bn 201601 start **/
	var device = '';
	// 行動裝置(不含iPad)出現建議下載app
	if (navigator.userAgent.match(/Android/i)) {
		device = 'android';
	} else if (navigator.userAgent.match(/iPod|iPhone|iPad/i)) {
		if (navigator.userAgent.indexOf('iPad') < 0) {
			if (navigator.userAgent.match(/crios/i)) {
				device = 'ios';
				detectiOSAppBanner();
			} else if (navigator.userAgent.match(/safari/i)) {
				device = ''; // Safari有設定自動產生App元件
			} else {
				device = 'ios';
				detectiOSAppBanner();
			}
		}
	}

	if (device != '') {
		var standalone = navigator.standalone; // Check if it's already a standalone web app or running within a webui view of an app (not mobile safari)
		var height_device;

		//判斷裝置高度
		height_device = $(window).height();
		console.log("h=" + height_device);

		// Don't show banner if device isn't iOS or Android, website is loaded in app or user dismissed banner       
		if (standalone || getCookie('sb-closed')) {
			$(".appbn_top").hide();
		}
		else if ($("input[id=channelAgent]").val() == "0") {
			$(".appbn_top").show();
		}

		//top廣告關閉後動作
		$(".appbn_top .btn_close").click(function () {
			$(".appbn_top").animate(
				{ "height": "0px", "width": "100%", "opacity": "0" },
				"800");
			$(".appbn_top .btn_close").css("zIndex", "-10");
			$(".appbn_top").css("zIndex", "-10");

			setCookie('sb-closed', 'true', 1); //1天有效
		});
	}

	function detectiOSAppBanner() {
		$('#topappbn').on('click', function (e) {
			var timers = [];
			var storeUrl = "https://itunes.apple.com/tw/app/17life/id543439591";
			var link = $(this).attr('href');

			setTimeout(function () {
				var storeLaunched = false;
				var gotStoreURI = "string" == typeof storeURI;
				gotStoreURI && timers.push(window.setTimeout(function () {
					storeLaunched = true;
					window.top.location = storeURI;
				}, 1000));
				timers.push(window.setTimeout(function () {
					storeLaunched && window.location.reload();
				}, 6000));
				window.location = link;
			}.bind(this), 0);

			timers.map(clearTimeout);
			timers = [];
		});
	}

	/** app download bn 201601 end **/

	//top button //此為M版專用語法
	$(window).scroll(setScrollTopVisible);
	function setScrollTopVisible() {
		var showValue = ($("#smartbanner").css("top") == "0px") ? (150 + 86) : 150;
		var scroll_top = $(window).scrollTop();
		if (scroll_top >= showValue) {
			$("#btnBackTop").css('display', 'inline');
		} else {
			$("#btnBackTop").css('display', 'none');
		}
	}
	$('#btnBackTop').click(function () {
		gototop();
	});

	function gototop() {
		$('html, body').animate({
			scrollTop: 0
		}, 800);
		return false;
	}
});

function setCookie(name, value, exdays) {
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + parseInt(exdays));
	value = escape(value) + ((exdays == null) ? '' : '; expires=' + exdate.toUTCString());
	document.cookie = name + '=' + value + '; path=/;';
}

function getCookie(name) {
	var i, x, y, ARRcookies = document.cookie.split(";");
	for (i = 0; i < ARRcookies.length; i++) {
		x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
		y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
		x = x.replace(/^\s+|\s+$/g, "");
		if (x == name) {
			return unescape(y);
		}
	}
	return null;
}

$.disalbeAutoFill = function () {
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
		$('input').filter(function () {
			return $(this).attr('autocomplete') != null;
		}).attr('autocomplete', 'off');
	} else {
		//chrome sucks
		$('input').filter(function () {
			return $(this).attr('autocomplete') != null;
		}).attr('autocomplete', 'new-password');
	}
};
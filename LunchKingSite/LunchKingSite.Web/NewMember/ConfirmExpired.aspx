﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfirmExpired.aspx.cs"
    Inherits="LunchKingSite.Web.NewMember.ConfirmExpired" MasterPageFile="~/NewMember/NewMember.master" %>

<%@ MasterType VirtualPath="~/NewMember/NewMember.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnReSend").click(function () {
                reSend("<%=UserEmail%>");
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="MemberCardFrame">
        <div class="MemberCard">
            <div class="MemberCardTopBlack">
                <div class="LifeMemberTopText">認證連結逾時！</div>
            </div>
            <div class="LifeMemberCenter">
                <div class="LifeMemberConfirmMessage">
                    <img src="../Themes/PCweb/images/memberPassBig.png" width="60" height="60" />
                    <span class="LifeMemberConfirmEPText">很抱歉，您的認證信已經超過可認證期限!</span>
                    <div class="text-center">
                        <input type="button" id="btnReSend" value="重發認證信" class="btn btn-large btn-primary">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

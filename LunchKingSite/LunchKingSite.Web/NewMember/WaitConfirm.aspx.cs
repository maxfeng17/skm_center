﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.WebApi.Controllers;
using LunchKingSite.WebApi.Controllers.Services;

namespace LunchKingSite.Web.NewMember
{
    public partial class WaitConfirm : BasePage
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (null == Session["newRegisterMail"])
            {
                Response.Redirect(config.SiteUrl);
            }
            else if (Request["send"] == "1")
            {
                new PponServiceController().SendAuthMail(new PponServiceController.AuthMailModel
                {
                    email = Session["newRegisterMail"].ToString()
                });
            }            
        }
    }
}
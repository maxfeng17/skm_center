﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfirmPass.aspx.cs" Inherits="LunchKingSite.Web.NewMember.ConfirmPass"
    MasterPageFile="~/NewMember/NewMember.master" EnableViewState="false" %>

<%@ MasterType VirtualPath="~/NewMember/NewMember.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("#btnLogin").click(function () {
                window.location.href = "<%=ResolveUrl(FormsAuthentication.LoginUrl+"?ReturnUrl=" + HttpUtility.UrlEncode("/NewMember/mobileAuth.aspx")) %>";
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="MemberCardFrame">
        <div class="MemberCard">
            <div class="MemberCardTopBlack">
                <div class="LifeMemberTopText">E-mail 認證成功</div>
            </div>
            <div class="LifeMemberCenter">
                <div class="LifeMemberConfirmMessage">
                    <img src="../Themes/PCweb/images/memberPassBig.png" width="60" height="60" />
                    <span class="LifeMemberConfirmEPText">馬上登入，並進行手機認證，確保您的帳戶安全。</span>
                    <div class="text-center">
                        <input type="button" id="btnLogin" value="立即登入" class="btn btn-large btn-primary">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

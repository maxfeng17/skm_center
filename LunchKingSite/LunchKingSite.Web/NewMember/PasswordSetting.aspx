﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PasswordSetting.aspx.cs"
    Inherits="LunchKingSite.Web.NewMember.PasswordSetting" MasterPageFile="~/NewMember/NewMember.master" %>

<%@ MasterType VirtualPath="~/NewMember/NewMember.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href='<%= ResolveUrl("../Themes/default/style/RDL.css") %>' rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("#txtPassword").focus(function () {
                $("#spanPassword").html('6~12碼，英文或數字，英文請區分大小寫');
                $("#linePassword").removeClass("LMPasswordInColor").addClass("LMPasswordIn");
            });


            $("#txtConPassword").focus(function () {
                $("#spanConPassword").html('請重新再輸入一次密碼！');
                $("#lineConPassword").removeClass("LMPasswordInColor").addClass("LMPasswordIn");
            });

            $(".LMPasswordBtn").click(function (e) {
                if (!checkInfo()) {
                    e.preventDefault();
                } else {
                    document.forms[0].submit();
                }
            });
        });

        function checkInfo() {
            // password check
            if (!passwordCheck($("#txtPassword").val())) {
                $("#spanPassword").html('<span style="color: #F00; letter-spacing: normal;">密碼格式錯誤！</span>');
                $("#linePassword").removeClass("LMPasswordIn").addClass("LMPasswordInColor");
            } else {
                // confirm password check
                if (!passwordConfirm($("#txtPassword").val(), $("#txtConPassword").val())) {
                    $("#spanConPassword").html('<span style="color: #F00; letter-spacing: normal;">您輸入的密碼不符合，請重新輸入！</span>');
                    $("#lineConPassword").removeClass("LMPasswordIn").addClass("LMPasswordInColor");
                } else {
                    return true;
                }
            }

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="LifeMemberFrame">
        <div class="LifeMember">
            <div class="LifeMemberTop">
                <div class="LifeMemberTopText">
                    <img src="../Themes/default/images/17Life/NewMember/memberPassword3.jpg" alt="" width="25"
                        height="25" />現在可以開始變更您的密碼了！</div>
            </div>
            <div class="LifeMemberCenter">
                <div class="LMPasswordFrame">
                    <ul class="LMPasswordIn">
                        <li id="linePassword" class="LMPasswordIn"><span class="LMPasswordFrameTittle">自訂新密碼：</span><span
                            class="LMPasswordFrameText rdl-LMPawdFTe">
                            <input type="password" id="txtPassword" name="txtPassword" />
                        </span><span id="spanPassword" class="LMPasswordFrameNote">6~12碼，英文或數字，英文請區分大小寫</span>
                        </li>
                        <li id="lineConPassword" class="LMPasswordIn"><span class="LMPasswordFrameTittle rdl-LMPawdTit">確認密碼：</span><span class="LMPasswordFrameText rdl-LMPawdFTe">
                                <input type="password" id="txtConPassword" />
                            </span><span id="spanConPassword" class="LMPasswordFrameNote">請重新再輸入一次密碼。</span>
                        </li>
                    </ul>
                    <div class="LMPasswordBtn" style="cursor: pointer;">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

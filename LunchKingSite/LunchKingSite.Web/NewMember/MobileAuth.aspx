﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MobileAuth.aspx.cs" Inherits="LunchKingSite.Web.NewMember.MobileAuth"
    MasterPageFile="~/Ppon/Ppon.master" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>

<%@ MasterType VirtualPath="~/NewMember/NewMember.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-L.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-M.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-S.css")%>' rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.1/angular.min.js"></script>
    <script type="text/javascript" src="../Tools/js/jquery.numeric.js"></script>
    <script type="text/javascript">
        <%
            if (mm != null)
            {
                if (mm.IsLoaded)
                {
                    Response.Write("window.isFirstAuth = false; \r\n");
                    if (mm.Status == (int)MobileMemberStatusType.NumberChecked && Request[ForceDontSetPassword] != "0")
                    {
                        Response.Write("\twindow.mobile = '" + mm.MobileNumber + "'; \r\n");
                        Response.Write("\twindow.needSetPassword = true; \r\n");
                    }
                    else if (Request[this.SkipIfAuthed] == "1")
                    {
                        Response.Redirect(Config.SiteUrl);
                    }
                    else
                    {
                        Response.Write("\twindow.needSetPassword = false; \r\n");
                    }
                }
                else
                {
                    Response.Write("window.isFirstAuth = true; \r\n");
                }
            }
        %>
        
        $(document).ready(function() {
            $('.numeric').attr('maxlength', 10).numeric();


            $('#btnGoBack').click(function() {
                location.href = "../user/UserAccount.aspx";
            });

            $('.btnHome').click(function() {
                location.href = "/";
            });            

            $(document).ajaxStart(function () {
                $.blockUI({
                    css: {
                        opacity: 1,
                        width: "120px",
                        height: "120px",
                        top: ($(window).height() - 120) / 2 + 'px',
                        left: ($(window).width() - 120) / 2 + 'px',
                    }, message: $('#imgLoading')
                });
            });
            $(document).ajaxStop(function() {
                $.unblockUI();
            });

        });

        var app = angular.module("mainApp", []);
        app.controller("mainCtrl", function ($scope) {

            $scope.state = 0;

            $scope.errmsg0 = '';
            $scope.errmsg2 = '';

            $scope.error21_1 = false;
            $scope.error21_2 = false;

            $scope.firstSent = false;
            $scope.sendCount = 0;
            $scope.canSent = 0;
            $scope.canInput = false;
            $scope.code = "";
            //正在輸入資料，隱藏錯誤訊息
            $scope.keying = false;

            $scope.password = "";
            $scope.password2 = "";

            if (window.isFirstAuth) {
                $scope.authTitle = "手機認證";
                $scope.authDesc = "確保交易安全及方便您用手機登入，需請您輸入電話，我們將立即發送認證碼簡訊";
                $scope.mobileSuccessTitle = "認證成功！";
                $scope.mobileSuccessDesc = "恭喜您，已成功認證！";
            } else {
                $scope.authTitle = "變更手機號碼，需認證";
                $scope.authDesc = "請您輸入電話，我們將立即發送認證碼簡訊：";
                $scope.mobileSuccessTitle = "認證成功！您的手機號碼已更新";
                $scope.mobileSuccessDesc = "恭喜您，已成功認證！手機號碼已更新！";
                if (window.needSetPassword) {
                    $scope.state = 21;
                    $scope.mobile = window.mobile;
                }
            }

            $scope.sendSms = function () {                
                console.log('send sms');
                $scope.errmsg0 = '';
                $scope.code = '';
                $scope.keying = false;
                $scope.canSent = 0;

                if (/^[09]{2}[0-9]{8}$/.test($scope.mobile) == false) {
                    console.log('number invalid.');
                    $scope.errmsg0 = '請輸入十位數字號碼。';
                    return;
                } else {
                    console.log('number valid.');
                }


                $.ajax({
                    type: "POST",
                    url: "MobileAuth.aspx/SendSms",
                    data: "{ mobile: '" + $scope.mobile + "' }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        console.log('reply:' + msg.d.result);
                        $scope.$apply(function () {
                            if (msg.d.result == '<%=MemberFacade._USER_NOT_LOGIN%>') {
                                location.href = '../User/UserAccount.aspx';
                            } if (msg.d.result == '<%=SendMobileAuthCodeReply.MobileUsedByOtherMember%>') {
                                $scope.errmsg0 = '此號碼已認證過，請輸入別隻號碼';
                            } else if (msg.d.result == '<%=SendMobileAuthCodeReply.TooManySmsSent%>') {
                                $scope.errmsg0 = '此號碼7天內索取次數已達上限(3次)，請輸入其他手機號碼';
                                $scope.canSent = 2;
                            } else if (msg.d.result == '<%=SendMobileAuthCodeReply.SmsError%>') {
                                $scope.errmsg0 = '簡訊發送失敗，請稍後再試!!';
                            } else if (msg.d.result == '<%=SendMobileAuthCodeReply.Success%>') {
                                $scope.sendCount = msg.d.sendCount;
                                $scope.firstSent = true;
                                $scope.canInput = true;
                                if (msg.d.sendCount >= 3) {
                                    $scope.canSent = 2;
                                } else {
                                    $scope.canSent = 1;
                                }
                                
                            } else {
                                $scope.errmsg0 = '系統繁忙中，請稍後再試!! ' + msg.d.result;
                            }
                        });
                    }, error: function (response, q, t) {
                        $scope.$apply(function () {
                            console.log('error at sendSms');
                            location.href = '../User/UserAccount.aspx';
                        });
                    }
                });

            }

            $scope.checkCode = function () {
                $scope.keying = false;
                $scope.canSent = 0;
                if (/[0-9]{6}/.test($scope.code) == false) {
                    $scope.stateError3 = true;
                    $scope.errmsg2 = '驗證碼輸入有誤，麻煩您重新輸入。';
                    return;
                }

                $.ajax({
                    type: "POST",
                    url: "MobileAuth.aspx/ValidateCode",
                    data: "{ mobile: '" + $scope.mobile + "', code:'" + $scope.code + "' }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        $scope.$apply(function () {
                            console.log('res:' + msg.d);
                            if (msg.d.result == '<%=MemberFacade._USER_NOT_LOGIN%>') {
                                location.href = '../User/UserAccount.aspx';
                            } else if (msg.d.result == '<%=MemberValidateMobileCodeReply.Success%>') {
                                if (msg.d.needSetPassword) {
                                    $scope.state = 2;
                                } else {
                                    $scope.state = 1;
                                }
                            } else if (msg.d.result == '<%=MemberValidateMobileCodeReply.ActivedAndSkip%>') {
                                if (msg.d.needSetPasswod) {
                                    $scope.state = 2;
                                } else {
                                    $scope.state = 1;
                                }
                            } else if (msg.d.result == '<%=MemberValidateMobileCodeReply.CodeError%>') {
                                $scope.errmsg2 = '驗證碼輸入有誤，麻煩您重新輸入。';
                            } else if (msg.d.result == '<%=MemberValidateMobileCodeReply.TimeError%>') {
                                $scope.errmsg2 = '驗證碼已過有效時間。';
                            } else if (msg.d.result == '<%=MemberValidateMobileCodeReply.MobileUsedByOtherMember%>') {
                                $scope.errmsg2 = '這個手機號碼己經被使用了。';
                            } else {
                                $scope.errmsg2 = '系統繁忙中，請稍後再試!!';
                            }

                            
                        });
                    }, error: function (response, q, t) {
                        $scope.$apply(function () {
                            console.log('error at validateCode');
                            location.href = '../User/UserAccount.aspx';
                        });
                    }
                });
            }

            $scope.inputPassword = function () {
                $scope.state = 21;
            };
        
            $scope.setPassword = function () {
                $scope.error21_1 = false;
                $scope.error21_2 = false;
                if (/^[0-9a-zA-Z]{6,12}$/.test($scope.password) == false) {
                    $scope.error21_1 = true;
                    return;
                }
                if ($scope.password != $scope.password2) {
                    $scope.error21_2 = true;
                    return;
                }

                $.ajax({
                    type: "POST",
                    url: "MobileAuth.aspx/SetPassword",
                    data: "{ password: '" + $scope.password + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        $scope.$apply(function () {
                            console.log('res:' + msg.d);
                            if (msg.d.result == '<%=MemberFacade._USER_NOT_LOGIN%>') {
                                location.href = '../User/UserAccount.aspx';
                            } else if (msg.d.result == '<%=MemberSetPasswordReply.Success%>') {
                                $scope.state = 22;
                            } else if (msg.d.result == '<%=MemberSetPasswordReply.EmptyError%>') {
                                $scope.errmsg2 = '驗證碼輸入有誤，麻煩您重新輸入。';
                            } else if (msg.d.result == '<%=MemberSetPasswordReply.DataError%>') {
                                $scope.errmsg2 = '驗證碼已過有效時間。';
                            } else if (msg.d.result == '<%=MemberSetPasswordReply.OtherError%>') {
                                $scope.errmsg2 = '這個手機號碼己經被使用了。';
                            } else if (msg.d.result == '<%=MemberSetPasswordReply.PasswordFormatError%>') {
                                $scope.errmsg2 = '系統繁忙中，請稍後再試!!';
                            }
                        });
                    }, error: function (response, q, t) {
                        $scope.$apply(function () {
                            console.log('error at setPassword');
                            location.href = '../User/UserAccount.aspx';
                        });
                    }
                });                
            }

            $scope.$watch('mobile', function (oldVal, newVal) {
                if (newVal != oldVal && newVal != '') {
                    $scope.keying = true;
                    $scope.canSent = 0;
                }
            });
            $scope.$watch('code', function (oldVal, newVal) {
                if (newVal != oldVal && newVal != '') {
                    $scope.keying = true;
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <div class="center" ng-app="mainApp">
        <div class="phone-approve" ng-controller="mainCtrl">
            <div class="mc-content" ng-show="state == 0">
                <h1 class="rd-smll">{{authTitle}}</h1>
                <hr class="header_hr" />
                <div>
                    <p>
                        {{authDesc}}
                    </p>
                </div>
                <div class="grui-form">
                    <div class="form-unit">
                       <div class="data-input">
                           <input type="text" class="input-half input-fixwidth numeric" placeholder="手機號碼, 請輸入09開頭的十位數字" autocomplete="off" ng-model="mobile">
                           <div class="fixwidth" ng-show="firstSent">
                               <p class="info btn-center">
                                   <input type="button" id="btnResend" value="重發認證碼" ng-click="sendSms()" 
                                       ng-disabled="canSent==2" ng-class="{true:'btn', false:'btn btn-primary'}[canSent==2]" />
                               </p>
                               <br>
                               <div ng-show="canSent==1 && keying == false">
                                   您已索取{{sendCount}}次，限3次
                               </div>
                               <div ng-show="canSent==2 && keying == false">
                                   您已索取{{sendCount}}次，7天內
                               <br>
                                   無法以此號碼索取認證碼
                               </div>
                           </div>
                       </div>                        
                        <div class="clearfix"></div>
                        <div class="enter-error" ng-show="errmsg0 != '' && keying == false">
                            <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                            <p>{{errmsg0}}</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div ng-show="canInput">
                    <div>
                        <p><br>請您稍後，<span style="font-weight:bold; color:#bf0000;">2分鐘內</span>您將收到認證簡訊，若無收到可點選＂重發認證碼＂
                        <br>（同一手機號碼限點3次）。請於下方輸入認證碼：
                        </p>
                    </div>
                </div>
                <div class="grui-form" ng-show="canInput">
                    <div class="form-unit">
                           <div class="data-input">
                               <input type="text" class="input-half input-fixwidth numeric" placeholder="6碼認證碼" autocomplete="off" ng-model="code">
                               <div class="fixwidth">
                                   <p class="info btn-center">
                                       <input type="button" class="btn btn-primary " value="送出" ng-click="checkCode()">
                                   </p>
                               </div>
                           </div>
                       <div class="clearfix"></div>
                        <div class="enter-error" ng-show="errmsg2 != '' && keying == false">
                            <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                            <p>{{errmsg2}}</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <br />
                <div class="text-center">
                    <input type="button" id="btSendSms" class="btn btn-large btn-primary" value="送出" ng-click="sendSms()" ng-show="firstSent == false">
                    <input type="button" id="btnGoBack" class="btn btn-large" value="取消">
                </div>
                <div class="clearfix"></div>

            </div>
            

 

            <div class="mc-content content-group" ng-show="state==1" style="display:none">
                <div class="member_success"></div>
                <h1 class="success">{{mobileSuccessTitle}}</h1>
                <hr class="header_hr" />
                <div>
                    <p>
                        恭喜您，已成功認證！您可使用手機號碼登入
                    </p>
                </div>
                <br />
                <p class="info btn-center" style="position:relative">
                    <input type="button" class="btn btn-large btn-primary rd-payment-xlarge-btn btnHome" value="看今日好康">
                </p>
            </div>
            
            
            
            <div class="mc-content content-group" ng-show="state == 2" style="display:none">
                <div class="member_success"></div>
                <h1 class="success">{{mobileSuccessTitle}}</h1>
                <hr class="header_hr" />
                <div>
                    <p>
                        {{mobileSuccessDesc}}<br>
                        請繼續設定密碼，下次就可以用手機號碼登入囉！
                    </p>
                </div>
                <br />
                <p class="info btn-center">
                    <input type="button" class="btn btn-large btn-primary rd-payment-xlarge-btn" value="設定密碼" ng-click="inputPassword()"/>
                </p>
            </div>
            
            <div class="mc-content content-group" ng-show="state == 21" style="display:none">
                <div class="member_success"></div>
                <h1 class="success">設定密碼！</h1>
                <hr class="header_hr" />
                <div>
                  <p>您的手機號碼：{{mobile}}
                  </p>
                </div>

                <div class="grui-form">
                  <div class="form-unit">
                    <div class="data-input">
                      <input type="password" class="input-half" placeholder="請輸入登入密碼" autocomplete="off" ng-model="password">
                      <p >6~12碼，英文或數字，英文請區分大小寫</p>
                    </div>
                    <!--需要時請display block 不需要請display none-->
                    <div class="enter-error" ng-show="error21_1">
                      <image src="../Themes/PCweb/images/enter-error.png" width="20" height="20" />
                      <p>密碼格式不正確，請輸入6~12碼的英文或數字</p>
                    </div>

                    <div class="data-input">
                      <input type="password" class="input-half" placeholder="請再次確認密碼" autocomplete="off" ng-model="password2">
                    </div>

                    <div class="enter-error" ng-show="error21_2">
                      <image src="../Themes/PCweb/images/enter-error.png" width="20" height="20" />
                      <p>兩組密碼不一致，請重新輸入</p>
                    </div>
                  </div>
                </div>

                <br/>
                <p class="info btn-center">
                  <input type="button" class="btn btn-large btn-primary rd-payment-xlarge-btn" value="送出" ng-click="setPassword()">
                </p>
                <div class="clearfix"></div>

            </div>
            
            <div class="mc-content content-group" ng-show="state == 22" style="display:none">
                <div class="member_success"></div>
                <h1 class="success">密碼設定完成！</h1>
                <hr class="header_hr" />
                <div>
                    <p>
                        恭喜您，已成功設定密碼！您可使用手機號碼登入
                    </p>
                </div>

                <br/>
                <p class="info btn-center">
                  <input type="button" class="btn btn-large btn-primary rd-payment-xlarge-btn btnHome" value="看今日好康">
                </p>
                <div class="clearfix"></div>

            </div>
        </div>
    </div>
    <img src="~/Themes/OWriteOff/loading.gif" runat="server" id="imgLoading" style="display:none" clientidmode="Static" />
</asp:Content>

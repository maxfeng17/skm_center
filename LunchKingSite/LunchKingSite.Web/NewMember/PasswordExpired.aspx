﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PasswordExpired.aspx.cs"
    Inherits="LunchKingSite.Web.NewMember.PasswordExpired" MasterPageFile="~/NewMember/NewMember.master" %>

<%@ MasterType VirtualPath="~/NewMember/NewMember.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href='<%= ResolveUrl("../Themes/default/style/RDL.css") %>' rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $(".LifeMemberConfirmEPBtn").click(function () {
                window.location.href = encodeURI("<%=webUrl %>");
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="LifeMemberFrame">
        <div class="LifeMember">
            <div class="LifeMemberTop">
                <div class="LifeMemberTopText">
                    <img src="../Themes/default/images/17Life/NewMember/memberPassword.jpg" alt="" width="25"
                        height="25" />重新設定密碼逾時！</div>
            </div>
            <div class="LifeMemberCenter">
                <div class="LifeMemberConfirmMessage">
                    <img src="../Themes/default/images/17Life/NewMember/memberTimeout.png" alt="" width="60"
                        height="60" /><span class="LifeMemberConfirmEPText">很抱歉！基於安全理由，系統發送給您的密碼重新設定連結只在24小時內有效。</span>
                    <div class="LifeMemberConfirmEPBtn" style="cursor: pointer;">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

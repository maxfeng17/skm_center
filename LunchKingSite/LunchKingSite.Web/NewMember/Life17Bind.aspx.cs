﻿using System;
using System.Net;
using System.Web;
using log4net;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.UI;

namespace LunchKingSite.Web.NewMember
{
    public partial class Life17Bind : MemberPage
    {
        private ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
        private string Password
        {
            get
            {
                return Request["txtPassword"];
            }
        }

        private string ConPassword
        {
            get
            {
                return Request["txtConPassword"];
            }
        }

        private int userId;
        /// <summary>
        /// 密碼是否需要設定
        /// 如果本身是手機會員，綁定Email會員，節不需設定
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private bool PasswordNeedSet(int userId)
        {
            if (User.Identity.IsAuthenticated)
            {
                MobileMember mm = MemberFacade.GetMobileMember(userId);
                if (mm.IsLoaded && mm.Status == (int)MobileMemberStatusType.Activated)
                {
                    return false;
                }
            }
            return true;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                userId = this.UserId;
            }
            else if (Session[LkSiteSession.Bind17UserId.ToString()] != null && Request["pez"] == "1")
            {
                userId = int.Parse(Session[LkSiteSession.Bind17UserId.ToString()].ToString());
            }
            else
            {
                if (Request["pez"] == "1")
                {
                    LogManager.GetLogger("pezLogin")
                        .Warn("session lost when redirect page." + this.Request.Url.AbsoluteUri);
                }
                Response.Redirect(conf.SiteUrl);
            }
            if (IsPostBack == false)
            {
                txtEmail.Text = MemberFacade.GetUserName(userId);
                if (PasswordNeedSet(userId) == false)
                {
                    txtEmail.Text = "";
                    phPassword.Visible = false;
                }
            }
        }

        private string CheckData(int userId, out bool correctness)
        {
            string result = string.Empty;
            correctness = true;

            if (PasswordNeedSet(userId))
            {
                if (RegExRules.CheckPassword(Password) == false)
                {
                    result += "wrongPassword();";
                    correctness = false;
                }

                if (Password.Equals(ConPassword) == false)
                {
                    result += "passwordNotMatch();";
                    correctness = false;
                }
            }
            return result;
        }


        protected void btnNext_Click(object sender, EventArgs e)
        {
            bool dataCorrectness;
            string script = CheckData(userId, out dataCorrectness);

            if (dataCorrectness)
            {
                string errMsg;
                string newMail = txtEmail.Text.Trim();
                Member mem = MemberFacade.GetMember(userId);
                if (mem.UserName.Equals(newMail, StringComparison.OrdinalIgnoreCase) == false)
                {
                    EmailAvailableStatus emailResult = MemberUtilityCore.CheckMailAvailable(newMail, true);
                    if (emailResult == EmailAvailableStatus.Authorizing || emailResult == EmailAvailableStatus.WasUsed)
                    {
                        ClientScript.RegisterStartupScript(GetType(), "wrongEmail",
                            "$(function(){wrongEmail('" + I18N.Phrase.RegisterReturnMessageEmailIsUser + "')});", true);
                        return;
                    }
                    if (emailResult == EmailAvailableStatus.IllegalFormat || emailResult == EmailAvailableStatus.Undefined)
                    {
                        ClientScript.RegisterStartupScript(GetType(), "wrongEmail",
                            "$(function(){wrongEmail('" + I18N.Phrase.InvalidEmail + "')});", true);
                        return;
                    }
                    if (emailResult == EmailAvailableStatus.DisposableEmailAddress)
                    {
                        ClientScript.RegisterStartupScript(GetType(), "wrongEmail",
                            "$(function(){wrongEmail('" + I18N.Phrase.RegisterReturnMessageDisposableEmailAddress +
                            "')});", true);
                        return;
                    }
                    //RegisterReturnMessageDisposableEmailAddress
                    if (NewMemberUtility.ChangeEmailSaveChange(mem.UniqueId, newMail) == false)
                    {
                        ClientScript.RegisterStartupScript(GetType(), "wrongEmail",
                            "$(function(){wrongEmail('" + "系統發生錯誤，申請綁定Email失敗，請洽客服處理。" + "')});", true);
                        return;
                    }
                }

                if (MemberFacade.BindMemberLinkReCreate(
                    userId, SingleSignOnSource.ContactDigitalIntegration, userId.ToString(), out errMsg) == false)
                {
                    string url = Helper.CombineUrl(conf.SiteUrl, "/User/UserAccount.aspx");
                    Response.Write(String.Format("<script>alert('{0}'); location.href='{1}';</script>", errMsg, url));
                    return;
                }
                if (PasswordNeedSet(userId))
                {
                    NewMemberUtility.SetPasswordAndBindEmailSend(userId, Password);
                }
                else
                {
                    NewMemberUtility.BindEmailSend(UserId, newMail);
                }
                if (User.Identity.IsAuthenticated)
                {
                    //設定成功
                    string msg = "設定成功，請接收認證信進行開通";
                    string url = Helper.CombineUrl(conf.SiteUrl, "/User/UserAccount.aspx");
                    Response.Write(String.Format("<script>alert('{0}'); location.href='{1}';</script>", msg, url));
                    Response.End();
                }
                else
                {
                    divDefault.Visible = false;
                    divResult.Visible = true;
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(GetType(), "wrongData",
                    "$(function(){" + script + "});", true);
            }
        }
    }
}
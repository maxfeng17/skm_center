﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePostAddress.aspx.cs" 
    Inherits="LunchKingSite.Web.NewMember.ChangePostAddress"
    MasterPageFile="~/NewMember/NewMember.master" ClientIDMode="Static" EnableViewState="false" %>

<%@ MasterType VirtualPath="~/NewMember/NewMember.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href="../Themes/PCweb/css/MasterPage.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/default/style/RDL.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/PCweb/plugin/font-awesome/font-awesome.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">
        $(function () {
            $("#btnHome").click(function () {
                window.location.href = "/";
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server" EnableViewState="false">
    <asp:Panel ID="panPass" runat="server" Visible="false">
        <div class="LifeMemberFrame lmf-fix">
            <div class="LifeMember">
                <div class="LifeMemberTopBlack rd-fd-sn">
                    <div class="LifeMemberTopText">電子信箱認證成功！</div>
                </div>
                <div class="LifeMemberCenter">
                    <div class="LMPasswordFrame text-center  rd-fd-sn">
                        <div class="grui-form">
                            <div class="form-unit" style="border: 0;">
                                <p class="note" style="display:inline-block;">
                                    您已成功變更您的17Life電子信箱。
                                    <br />
                                    日後所有17Life相關通知信，將統一改發送至此信箱：<%=this.UserName %><br />若您曾經開通17Life帳號，也請同時改以此電子信箱，做為登入帳號。
                                </p>
                            </div>
                        </div>
                        <div class="text-center">
                            <input type="button" id="btnHome" value="回首頁" class="btn btn-large btn-primary">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:PlaceHolder ID="phFail" runat="server" Visible="false">
        <div class="LifeMemberFrame lmf-fix">
            <div class="LifeMember">
                <div class="LifeMemberTopBlack rd-fd-sn">
                    <div class="LifeMemberTopText">電子信箱認證失效！</div>
                </div>                
                <div class="LifeMemberCenter">
                    <div class="LMPasswordFrame text-center  rd-fd-sn">
                        <div class="grui-form">
                            <div class="form-unit" style="border: 0;">
                                <p class="note" style="display:inline-block;">
                                    無效的連結，或已超過可認證期限。<br />
                                    若您需變更會員電子信箱，請重新申請變更，並於七天內完成電子信箱認證。
                                </p>
                            </div>
                        </div>
                        <div class="text-center">
                            <input type="button" id="btnHome" value="回首頁" class="btn btn-large btn-primary">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:PlaceHolder>
</asp:Content>

﻿using System;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.NewMember
{
    public partial class PasswordExpired : BasePage
    {
        public string webUrl = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            webUrl = ResolveUrl("~/NewMember/ForgetPassword.aspx");
        }
    }
}
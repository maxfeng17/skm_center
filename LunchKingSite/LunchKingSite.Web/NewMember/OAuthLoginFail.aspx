﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OAuthLoginFail.aspx.cs" MasterPageFile="~/Ppon/Ppon.Master" 
    Inherits="LunchKingSite.Web.NewMember.OAuthLoginFail" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
<div class="msg-block clearfix">
 <h1>無效的API金鑰</h1>
 <p class="msg-p">你正嘗試使用的應用程式並不存在或已被停用</p>
</div>
</asp:Content>
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="17LifeLETSPAY.aspx.cs" Inherits="LunchKingSite.Web.NewMember._17LifeLETSPAY" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="<%=ResolveUrl("~/Themes/PCweb/css/MasterPage.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Themes/PCweb/css/17lifeMember.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Themes/PCweb/css/MasterPage17life.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css")%>" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<%=ResolveUrl("~/Themes/PCweb/images/favicon.ico")%>" />
    <link href="<%=ResolveUrl("~/Themes/PCweb/css/lifeMemberIE6.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Themes/PCweb/css/lifeMemberIE7.css")%>" rel="stylesheet" type="text/css" />
    <link href="https://www.17life.com/images/custom/override.css" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Themes/default/style/AjaxStyleSheet.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Themes/default/style/web.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Themes/default/images/17P/a17_p.css")%>" rel="stylesheet" type="text/css" />
    <meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1" />
</head>
<body>
    <div class="Homepagecenter">
        <div class="MemberCardFrame">
            <div class="MemberCardTopBlack">
                <div class="LifeMemberTopText">
                    17LifeLETSPAY行動錢包 服務條款
                </div>
                <div class="MemberUserpolicydate">
                
                </div>
                <br />
            </div>
            <div class="LifeMemberCenter">
                <div class="MemberPrivacy">
                    <div style="font-size: 15px; padding: 10px 20px 20px 20px; border: 0px solid #76A045;">
                       歡迎使用「17Life LETSPAY服務」(下稱本服務)，服務說明：<br />
                        <br />

                        <p style="text-indent: -1.6em; margin-left: 1.6em;">1. 本服務為康太數位整合股份有限公司提供，使用者於使用本服務前，請務必詳細審閱並決定是否同意遵守本服務條款，當使用者勾選「同意」時，即視為使用者已充分閱讀、瞭解並同意遵守本服務條款，若不同意任一項服務條款，請勿啟用本服務。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">2. 本公司有權隨時修改或變更本服務條款，若使用者於本服務條款修改或變更後，繼續使用本服務者，視同使用者同意並接受該服務條款之修改或變更。<br />
                        </p>
                        當使用者勾選「同意」時，即視為使用者已充分審閱、瞭解及同意本服務條款，基於上述充分認知，使用者同意使用本服務，並遵守以下約定：<br />

                        <br />
                        <span style="font-weight: bold; font-size: 18px;">服務內容與費用<br />
                        </span>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">1. 本服務為一適用於行動終端裝置上之應用服務，使用者需先下載「17Life生活電商」APP軟體(以下簡稱本服務APP)並加入17Life會員方可使用。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">2. 本服務主要係提供使用者行動支付等服務，詳細服務內容及功能以本服務APP實際提供為準，且本公司有權隨時增減或變更各項服務內容及功能。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">3. 本服務現行所提供之信用卡交易功能悉依該信用卡相關約定條款規定辦理。<br />
                        </p>
                        <br />

                        <span style="font-weight: bold; font-size: 18px;">信用卡功能<br />
                        </span>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">1. 使用者須先申請綁訂信用卡為支付工具，並輸入信用卡資訊（例如信用卡號、有效月年、安全檢核碼末3碼）及信用卡交易密碼，以確認資訊正確有效，輸入之信用卡資訊將依照支付卡產業資料安全標準(Payment Card Industry Data Security Standard，PCI DSS)加密傳輸至台新銀行進行進行3D驗證及保存，不會保留於本服務APP。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">2. 使用者完成信用卡驗證無誤後，即可開始使用本服務之信用卡支付功能，以本服務後台系統與台新銀行所約定之代碼化卡號取代真實信用卡資訊。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">3. 使用者每次使用本服務APP軟體進行支付前，均須輸入信用卡交易密碼，供本服務系統驗證、確認透過本服務所使用之信用卡消費所支付之款項，為使用者所為。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">4. 使用者透過本服務以約定之信用卡支付交易款項者，無須使用簽帳單、當場簽名或另立書面契約，使用者同意並保證日後不得以該筆信用卡交易款項未經使用者本人簽名為由，拒絕支付該筆交易款項。如使用者於本服務系統上註冊、新增約定之信用卡因掛失、偽冒、停用等原因失效時，該張信用卡即無法於本服務系統進行任何消費、交易服務。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">5. 本服務限使用者本人使用，不得轉讓、質借或提供他人占有或使用；若使用者違反前揭規定，所生之糾紛或損失，概由使用者自行負責。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">6. 使用者不得有任何複製或改作本服務之行為；一經發現，本銀行得報請有關機關追究相關法律責任，並得請求使用者賠償本銀行或其他第三人所受之損失。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">7. 使用者透過智慧型行動終端設備使用本服務進行交易消費款項之支付時，限制如下：使用者使用本服務認證該信用卡後，限於該張信用卡之發卡行核定之信用額度內使用，惟該發卡行就該筆交易授權保有准駁與否之權利。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">8. 使用者使用本服務，除應妥善保管本服務信用卡交易密碼外，並應不定期變更本服務信用卡交易密碼，以維護本服務使用安全性。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">9. 為積極有效維護使用者權益，當使用者輸入本服務信用卡交易密碼，連續錯誤超過5次時，本服務將自動鎖定，內建之信用卡資料將被刪除且由系統強制登出本服務APP，使用者需致電客服解除鎖定、並重新登入，始可使用本服務，使用者亦須重新新增信用卡並進行3D驗證；若使用者將密碼遺失或忘記密碼時，請透過本服務APP之忘記密碼功能，進行密碼重設作業。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">10. 使用者若係因使用信用卡所衍生之相關問題，請逕洽該張信用卡之發卡行(信用卡背面24小時服務專線)。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">11. 本公司將於使用者所指示之每筆交易處理完畢後，由本服務系統畫面將呈現交易訊息結果通知或智慧型行動終端設備交易介面所示之消費記錄或其他方式通知使用者，使用者應核對其交易內容之正確性。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">12. 另本公司亦提供使用者於本服務APP查詢交易記錄。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">13. 使用者若對透過本服務所完成之指示交易消費帳款有疑義時，請洽詢交易商家或逕洽發卡行(信用卡背面24小時服務專線)。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">14. 使用者使用本服務進行消費支付，應自行仔細檢核個人或商家所輸入之資訊。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">15. 倘因使用者或商家端之輸入或操作錯誤導致該筆交易款項支付錯誤時，請立即洽商家修正該筆錯誤交易。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">16. 使用者透過本服務已完成之交易若需進行退款，需依商家之退款政策與流程進行退款，有關退款作業請逕洽商家，以處理退款。<br />
                        </p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">17. 本服務並未介入商品、服務之交付、買賣或商品、服務瑕疵等之實體法律關係，相關商品退貨或服務取消之退款等事宜，持卡人應先洽商家或商品、服務之製造商或提供商尋求解決。<br />
                        </p>
                        <br />


                        <span style="font-weight: bold; font-size: 18px;">本公司訊息效力<br />
                        </span>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">本公司與使用者間同意以電子訊息、電子文件作為表示方法，為本服務之使用、提供所交換之電子訊息、電子文件，效力與書面文件相同。<br />
                        </p>
                        <br />


                        <span style="font-weight: bold; font-size: 18px;">智慧財產權<br />
                        </span>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">本服務使用之商標（包括但不限於名稱、商標及標誌）及內容（包括但不限於所有軟體、文字、圖片、影音及其他一切內容），其所有權、智慧財產權及其他一切權利皆為康太數位整合股份有限公司、台新國際商業銀行或相關合法權利人所有，概受著作權法、其他智慧財產權法令及其他中華民國及國際法令之保護。<br />
                        </p>
                        <br />

                        <span style="font-weight: bold; font-size: 18px;">使用者責任與義務<br/>
                        </span>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">1. 本服務僅供使用者個人正常合法使用，使用者不得轉售或以任何方式轉讓予他人使用。<br /></p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">2. 未經本公司事前書面同意，使用者不得對本服務之內容、商標及本服務APP軟體之全部或一部擅自進行出售、交易、轉售、轉授權、重製、傳輸、散佈、修改、改作、連結、進行還原工程、解編、反向組譯、製作衍生著作或以其他任何形式、基於任何目的加以使用、出版或發行之行為。使用者若違反前述責任與義務，導致本公司或任何第三人之損害者，應負擔賠償責任。<br /></p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">3. 使用者提供予本公司或本服務之文字、圖片及其他一切內容應確保依法享有相關權利(包括但不限於肖像權、著作權、姓名權等)，且絕無侵害第三人智慧財產權或其他權利之情事。<br /></p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">4. 使用者同意不得進行下列行為：<br /></p>
                        違反法律、法規、行政命令或本服務相關業務所屬之主管機關之要求；<br />
                        以任何方式干擾或破壞本服務軟體及系統；<br />
                        用於不法交易行為或有任何不當/不合常理之使用行為；<br />
                        提供虛偽不實之資料或冒用他人個人資料；<br />
                        違反本服務條款之規定。<br />
 
                        <br/>
                        <span style="font-weight: bold; font-size: 18px;">賠償<br/>
                        </span>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">1. 使用者若違反法律、法規、行政命令或銀行、本服務相關業務所屬之主管機關之要求、或以任何方式干擾或破壞本服務APP軟體或本銀行系統、或用於不法交易行為或有任何不當、不合常理之使用行為、或提供虛偽不實之資料或冒用他人個人資料之情形者，本公司得不經通知逕行限制或終止使用者使用本服務帳戶權限，並終止使用者使用本服務之權利(含日後拒絕使用者申請本服務)，使用者不得因此要求任何補償或賠償。<br /></p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">2. 使用者提供予本公司或本服務之文字、圖片及其他一切內容遭主管機關或第三人主張違反法令規定、侵害其權益或請求損害賠償，使用者應負責解決之，並應確保本公司不受任何損害。本公司得因此等糾紛，不經通知逕行限制或終止使用者使用本服務帳戶權限，並終止使用者使用本服務之權利(含日後拒絕使用者申請本服務)。<br /></p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">3. 如上述兩點事宜，導致本公司、關係企業、受雇人、代理人、合作業者及履行輔助人等，受有損害或支出費用(包括但不限於因進行民事、刑事及行政訴訟程序所支出之訴訟費用及律師費)時，使用者應負損害賠償責任。<br /></p>

                        <br/>
                        <span style="font-weight: bold; font-size: 18px;">使用者資料之使用及授權<br/>
                        </span>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">1. 使用者應提供完整、詳實且符合真實之個人資料，若資料嗣後有變更、異動時，應隨時依本公司規定更新。<br /></p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">2. 若使用者所提供之個人資料填寫不實、原先提供之資料已不符真實而未更新者，本公司有權隨時終止使用者使用本銀行之部份或全部之權利。<br /></p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">3. 本公司就使用者所提供之個人資料(包括但不限於姓名、地址、電話號碼、使用者名稱、帳號、密碼、電子郵件地址、信用卡資料及其他依法受保護之個人資料等)將依法妥善維護，使用者瞭解並同意本公司及共同開發本服務之台新銀行就使用者所提供之個人資料得蒐集及處理之，並得於使用者/會員管理及檔案維護、行銷、廣告宣傳、調查分析統計、提供本服務及相關服務等目的範圍內傳遞及利用使用者之個人資料。<br /></p>
 
                        <br/>
                        <span style="font-weight: bold; font-size: 18px;">使用者規範及擔保<br/>
                        </span>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">1. 使用者使用本服務前，應詳閱本服務條款及相關說明，以瞭解與本服務系統相容之軟硬體設備規格。<br /></p>使用者並應自行備妥因使用本服務所需之相關設備，並自行負擔相關設備連結有線、無線或行動網際網路之衍生費用(如傳輸費)。<br /></p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">2. 使用者應自行確認所使用之軟硬體設備，基本即具有連結行動通信與數據網路、收發文字簡訊、多媒體訊息或網址簡訊等功能；若因使用者所使用之軟硬體設備不具備前述功能，本銀行不保證服務得予提供，亦不負擔服務提供或品質之義務。<br /></p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">3. 使用者有義務妥善保管申請/使用本服務時所填寫的帳號及密碼等資料(使用者實際應填寫之資料以本服務平台之檢核機制為主)，並為帳號/密碼登入本服務後所進行之一切行為負責。<br /></p>為維護使用者權益，請勿將上述資料洩露或提供予第三人知悉、或出借或轉讓他人使用。<br /></p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">4. 若使用者發現上述資料遭人非法使用或有任何異常破壞使用安全之情形時，應立即通知本公司客服專線電話：<%=config.ServiceTel %>。<br /></p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">5. 若因以下原因造成使用者權益受損，本銀行不負任何法律或賠償責任：
                        使用者未妥善保管本服務帳號、密碼。
                        使用者自行將本服務帳號、密碼提供與他人。
                        使用者未使用本服務所提供的帳號安全機制。
                        其他因使用者之故意或重大過失所致之事由。
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">6. 本公司有權決定是否受理使用者申請本服務，亦有權決定或隨時變更本服務之申請資格。<br /></p>
 
                        <br/>
                        
                        <br/>
                        <span style="font-weight: bold; font-size: 18px;">服務更新停止或中斷<br/>
                        </span>

                        <p style="text-indent: -1.6em; margin-left: 1.6em;">1. 為有效提供本服務，本公司得隨時要求使用者自動下載與安裝更新之本服務之軟體、程式，以利使用者繼續使用本服務；若使用者不同意或未更新，本銀行有權終止或限制使用者，使用本服務之權利，且無須對使用者或第三人負擔任何責任。<br/></p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">2. 若發生下列情形，本公司有權不經通知使用者逕行停止或中止服務之提供，且不負任何法律責任：<br/></p>
                        對本服務系統、相關軟硬體設備或電子通信設備緊急保養、維護、升級或施工時；<br/>
                        發生突發性之系統/設備故障時；<br/>
                        因所連結本服務之通訊相關通信服務，中斷、停止，導致服務無法提供時；<br/>
                        本服務因天災等不可抗力之因素致無法提供時。<br/>
 
                        <br/>
                        <span style="font-weight: bold; font-size: 18px;">品質擔保及責任限制<br/>
                        </span>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">1. 本公司不保證本服務運作時不會發生中斷、延遲或錯誤等情形，某些情形可能會影響使用者的通訊品質以及本服務之使用狀況，並可能導致通訊中斷或延遲。因此，使用者瞭解並同意於使用本服務時，應自行採取防護措施。<br /></p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">2. 因上述情事致使用者無法使用本服務而衍生任何直接或間接之損害時，本銀行不負任何賠償責任。<br /></p>
 
                        <br/>
                        <span style="font-weight: bold; font-size: 18px;">服務終止<br/>
                        </span>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">1. 若發生特定情形或使用者有違反本服務條款情事者，本公司有權不經催告逕行終止本服務之提供，或限制使用者使用本服務之權利。<br /></p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">2. 前項所稱之特定情形，包括但不限於本公司營業政策之調整，停止本服務之提供、依法律/法令/法院/主管機關之命令，或本服務發生無法繼續或無法預期之技術或安全因素或問題等。於前揭情形，本銀行對使用者或任何第三人均不負擔任何責任。<br /></p>
 
                        <br/>
                        <span style="font-weight: bold; font-size: 18px;">服務轉讓<br/>
                        </span>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">1. 使用者不得轉讓本服務之任何權利或義務。<br /></p>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">2. 本銀行可將本服務之權利或義務轉讓予第三人，或委由第三人處理，且無須事先通知使用者。<br /></p>
 
                        <br/>
                        <span style="font-weight: bold; font-size: 18px;">其他使用條款和條件<br/>
                        </span>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">本公司得隨時公布、張貼或傳送本服務之其他使用規範、條件或說明等，該等使用規範、條件或說明等亦視為本服務條款之一部分，使用者亦同意遵守之。<br /></p>
 
                        <br/>
                        <span style="font-weight: bold; font-size: 18px;">準據法與管轄<br/>
                        </span>
                        <p style="text-indent: -1.6em; margin-left: 1.6em;">本服務條款適用中華民國相關法律，若因本服務條款或本服務之使用而涉訟者，雙方同意以臺灣臺北地方法院為第一審管轄法院。<br /></p>
 

                        <p>個人資料保護之法定告知事項</p>

                        <p>由於個人資料之蒐集涉及您的隱私權益，本服務向您蒐集個人資料時，依據個人資料保護法（以下簡稱「個資法」）第8條第1項規定，應明確告知您下列事項：非公務機關名稱、蒐集之目的、個人資料之類別、個人資料利用之期間、地區、對象及方式、當事人依個人資料保護法第3條規定得行使之權利及方式、當事人得自由選擇提供個人資料時，不提供將對其權益之影響。</p>
                        <br/>
                        <span style="font-weight: bold; font-size: 18px;">一、 本服務蒐集您個人資料之特定目的如下：<br/>
                        </span>
                        <p>040行銷、067信用卡、轉帳卡或電子票證業務、063非公務機關依法定義務所進行個人資料之蒐集處理及利用、069契約、類似契約或其他法律關係管理之事務、090消費者、客戶管理與服務、091消費者保護、136資(通)訊及資料庫管理、157調查、統計與研究分析、181其他經營合於營業登記項目或組織章程所定之業務。</p>
                        <br/>
                        <span style="font-weight: bold; font-size: 18px;">二、 本服務蒐集您的個人資料類別，
                        </span>如您的姓名、身分證統一編號暨換補領資料(例如身分證字號、換補領日期、換補領代碼、換補領地點、證件上有無列印相片)、出生年月日、通訊電話、聯絡方式等及其他在本服務填寫之內容所實際蒐集之個人資料。<br/>
                        <br/>
                        <span style="font-weight: bold; font-size: 18px;">三、 本服務將您的個人資料為利用之期間、地區、對象及方式：<br/>
                        </span>
                        (一)、 期間：<br />
                        1. 特定目的存續期間。<br />
                        2. 依相關法令所定(例如商業會計法等)或因執行業務所必須之保存期間或依個別契約就資料之保存所定之保存年限(以期限最長者為準)。<br />
                        (二)、 地區：<br />
                        以下第(三)點所列之利用對象，其國內及國外所在地。<br />
                        (三)、 對象：<br />
                        1. 康太數位整合股份有限公司、台新國際商業銀行股份有限公司(含受台新銀行委託處理事務之委外機構)。<br />
                        2. 依法令規定利用之機構(例如：台新銀行所屬金融控股公司等)。<br />
                        3. 其他業務相關之機構(例如：財團法人金融聯合徵信中心、臺灣票據交換所、財金資訊股份有限公司、信用保證機構、信用卡國際組織、收單機構暨特約商店等)。<br />
                        4. 依法有權機關或金融監理機關。<br />
                        5. 您所同意之對象(例如本行共同行銷或交互運用客戶資料之公司、與本行合作推廣業務之公司等)。<br />
                        6. 與我國政府簽訂條約、協議之外國政府機構。<br />
                        (四)、 方式：符合個人資料保護相關法令以自動化機器或其他非自動化之利用方式。<br />

                        <br/>
                        <span style="font-weight: bold; font-size: 18px;">四、 依據個人資料保護法第3條規定，您就本行本公司保有您的個人資料，得行使下列權利：<br/>
                        </span>
                        (一)、 除有個人資料保護法第10條所規定之例外情形外，得向本公司查詢、請求閱覽或請求製給複製本，惟本公司依個人資料保護法第14條規定，得酌收必要成本費用。<br />
                        (二)、 得向本公司請求補充或更正個人資料，惟依個人資料保護法施行細則第19條規定，您應適當釋明其原因及事實。<br />
                        (三)、 本公司如有違反個人資料保護法規定蒐集、處理或利用您的個人資料，依個人資料保護法第11條第4項規定，您得向本公司請求停止蒐集您的個人資料。<br />
                        (四)、 依個人資料保護法第11條第2項規定，個人資料正確性有爭議者，得向本公司請求停止處理或利用您的個人資料。惟依該項但書規定，本公司因執行業務所必須並註明其爭議或經您書面同意者，不在此限。<br />
                        (五)、 依個人資料保護法第11條第3項規定，個人資料蒐集之特定目的消失或期限屆滿時，得向本公司請求刪除、停止處理或利用您的個人資料。惟依該項但書規定，本公司因執行業務所必須或經您書面同意者，不在此限。<br />
                        您如欲行使上述個人資料保護法第3條規定之各項權利，有關如何行使之方式，您得向17Life客服中心 <%=config.ServiceTel %> 或由登入17Life網站利用客服系統提出申請)查詢。<br />
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
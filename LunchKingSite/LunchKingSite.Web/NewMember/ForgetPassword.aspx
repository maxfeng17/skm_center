﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgetPassword.aspx.cs" EnableViewState="false"
    Inherits="LunchKingSite.Web.NewMember.ForgetPassword" MasterPageFile="~/NewMember/NewMember.master" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>

<%@ MasterType VirtualPath="~/NewMember/NewMember.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href='<%= ResolveUrl("../Themes/default/style/RDL.css") %>' rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Tools/js/jquery.numeric.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.numeric').attr('maxlength', 10).numeric();

            $(':radio[name=notifyType]').eq(0).attr('checked', true);

            $('.btnGoBack').click(function () {
                location.href = '<%=ResolveUrl(FormsAuthentication.LoginUrl) %>';
            });

            $('form').submit(function (event) {
                $(':button, button').attr('disable', true);
                window.setTimeout(function() {
                    $.blockUI({
                        css: {
                            opacity: 1,
                            width: "120px",
                            height: "120px",
                            top: ($(window).height() - 120) / 2 + 'px',
                            left: ($(window).width() - 120) / 2 + 'px',
                        },
                        message: $('#imgLoading')
                    });
                }, 500);
            });

            //

            $("#txtEmail").focus(function () {
                $("#spanMail").html('<%=MemberFacade.IsMobileAuthEnabled ? "請輸入您在17Life的手機號碼或電子信箱" : "請輸入您在17Life的電子信箱"%>');
                $("#lineMail").removeClass("LMPasswordInColor").addClass("LMPasswordIn");
            });

            $("#captchaResponse").focus(function () {
                $("#spanCaptcha").html('請輸入下方顯示的文字，英文可不區分大小寫！');
                $("#lineCaptcha").removeClass("LMPasswordInColor").addClass("LMPasswordIn");
            });

            getCaptcha('<%= SessionId %>');
        });

        function getCaptcha(id) {
            $("#imgCaptcha").attr("src", "Captcha.ashx?x=" + Math.random() + "&sessionId=" + id);
        }

        function wrongCaptcha() {
            $("#spanCaptcha").html('<span style="color: #F00; letter-spacing: normal;">圖形驗證輸入錯誤！</span>');
            $("#lineCaptcha").removeClass("LMPasswordIn").addClass("LMPasswordInColor");
        }

        function showNoMail() {
            $("#spanMail").html('<span style="color: #F00; letter-spacing: normal;">查無此帳號！</span>');
            $("#lineMail").removeClass("LMPasswordIn").addClass("LMPasswordInColor");
        }

        function showNot17Member() {
            $("#spanMail").html('<span style="color: #F00; letter-spacing: normal;">您尚未開通17Life帳號，無法查詢。請使用FB或PayEasy登入。</span>');
            $("#lineMail").removeClass("LMPasswordIn").addClass("LMPasswordInColor");
        }

        function showInactive() {
            $("#spanMail").html('<span style="color: #F00; letter-spacing: normal;">您的信箱未完成註冊！</span>');
            $("#lineMail").removeClass("LMPasswordIn").addClass("LMPasswordInColor");
        }

        function showIsOtherSiteMember(memberType) {
            var msg = '';
            switch (memberType) {
                case "facebook":
                    msg = "此帳號需使用Facebook登入，您可至Facebook取回密碼。";
                    break;
                case "payeasy":
                    msg = "此帳號需使用Payeasy登入，您可至Payeasy取回密碼。";
                    break;
                default:
                    break;
            }
            $("#spanMail").html('<span style="color: #F00; letter-spacing: normal;">' + msg + '</span>');
            $("#lineMail").removeClass("LMPasswordIn").addClass("LMPasswordInColor");
        }

        function showError() {
            $.blockUI({
                message: $("div#ThreeTimes"),
                css: {
                    top: ($(window).height() - 400) / 2 + 'px',
                    left: ($(window).width() - 710) / 2 + 'px',
                    width: '0px',
                    height: '0px',
                    border: 'none',
                    cursor: 'default'
                },
                overlayCSS: { cursor: 'default' }
            });
            $('.blockOverlay').click($.unblockUI);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="LifeMemberFrame" ng-app="mainApp">
        <div class="LifeMember" ng-controller="mainCtrl">
            <asp:PlaceHolder ID="phInputAccount" runat="server">
                <div class="LifeMemberTopBlack">
                    <div class="LifeMemberTopText">忘記密碼了嗎？我們將協助您登入！</div>
                </div>
                <div class="LifeMemberCenter">
                    <div class="LMPasswordFrame">
                        <ul class="LMPasswordIn">
                            <li id="lineMail" class="LMPasswordIn"><span class="LMPasswordFrameTittle">　　帳號：</span>
                                <span class="LMPasswordFrameText">
                                    <input type="text" id="txtEmail" name="txtEmail" runat="server" clientidmode="Static" />
                                </span>
                                <span id="spanMail" class="LMPasswordFrameNote"><%=MemberFacade.IsMobileAuthEnabled ? "請輸入您在17Life的手機號碼或電子信箱" : "請輸入您在17Life的電子信箱" %></span>
                            </li>
                            <li id="lineCaptcha" class="LMPasswordIn"><span class="LMPasswordFrameTittle">圖形驗證：</span>
                                <span class="LMPasswordFrameText">
                                    <input type="text" id="captchaResponse" name="captchaResponse" autocomplete="off"/>
                                </span>
                                <span id="spanCaptcha" class="LMPasswordFrameNote">請輸入下方顯示的文字，英文可不區分大小寫</span>
                            </li>
                        </ul>
                        <div class="LifeMemberVerification">
                            <div class="LifeMemberVFCode">
                                <img id="imgCaptcha" alt="" />
                            </div>
                            <div class="LifeMemberVFCodeText">
                                <a href="javascript:getCaptcha('<%= SessionId %>');">更換一組驗證碼</a>
                            </div>
                        </div>
                        <div class="text-center">
                            <asp:Button ID="btnNext" Text="下一步" runat="server" CssClass="btn btn-large btn-primary" OnClick="btnNext_Click" />
                            <input type="button" class="btn btn-large btnGoBack" value="取消">
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phNotifyWay" runat="server">
                <div class="LifeMemberTopBlack">
                    <div class="LifeMemberTopText">忘記密碼了嗎？我們將協助您登入！</div>
                </div>
                <div class="LifeMemberCenter">
                    <div class="LMInfo">
                        為了確保帳號安全，請選擇任一方式以完成密碼設定，我們將發送"設定密碼連結"，此連結24小時內有效。
                        <div class="selectSetting">
                            <asp:PlaceHolder ID="phNotifyByEmail" runat="server">
                                <label>
                                    <input type="radio" name="notifyType" value="byEmail" ng-model="notifyType">
                                    <div class="input-content">
                                        <img src="../Themes/PCweb/images/p-mail.png">
                                        以電子郵件傳送連結，好讓我設定密碼<br />
                                        <span class="LifeMemberNote">
                                            <asp:Literal ID="litEmailDisplay" runat="server" EnableViewState="false" /></span>
                                    </div>
                                </label>
                                &nbsp;&nbsp;<br />
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="phNotifyByMobile" runat="server">
                                <label>
                                    <input type="radio" name="notifyType" value="byMobile" ng-model="notifyType">
                                    <div class="input-content">
                                        <img src="../Themes/PCweb/images/p-phone.png">
                                        以簡訊傳送連結，好讓我設定密碼<br />
                                        <span class="LifeMemberNote">
                                            <asp:Literal ID="litMobileDisplay" runat="server" EnableViewState="false" /></span>
                                    </div>
                                </label>
                                &nbsp;&nbsp;<br />
                            </asp:PlaceHolder>
                        </div>
                        <div class="text-center">
                            <asp:Button CssClass="btn btn-large btn-primary btnSubmit" id="btnNotify" runat="server" Text="送出" OnClick="btnNotify_Click" />
                            <input type="button" class="btn btn-large btnGoBack" value="取消">
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phNotifyResult" runat="server" Visible="false">
                <div class="LifeMemberTopBlack">
                    <div class="LifeMemberTopText">請檢查信箱，以進行密碼設定</div>
                </div>
                <div class="LifeMemberCenter">
                    <div class="LMInfo">
                        我們已發送"設定密碼連結"至以下信箱，請檢查您的信件，此連結24小時內有效。<br />
                        <span class="LifeMemberNote"><asp:Literal ID="litAccountDisplay" runat="server" EnableViewState="false" /></span><br />
                        <asp:LinkButton ID="btnNotifyAgain" runat="server" OnClick="btnNotifyAgain_Click" Text="我沒收到信件，請重新發送" CssClass="resent-mail" />
                        <br /><br />
                        <p class="info btn-center">
                            <input type="button" class="btn btn-large btn-primary btnGoBack" value="回登入頁">
                        </p>
                    </div> 
                </div>             
            </asp:PlaceHolder>

            <asp:PlaceHolder ID="phSmsNotiy" runat="server">
                <div class="LifeMemberTopBlack ">
                    <div class="LifeMemberTopText">請輸入認證碼，並進行密碼設定</div>
                </div>
                <div class="LifeMemberCenter">
                  <div class="LMInfo">
                    我們已寄出6碼的認證碼簡訊至以下號碼，請檢查手機，您也可直接從手機提供的連結進行設定。
                    <div class="grui-form">
                      <div class="form-unit">
                        <div class="data-input">
                          <input type="text" class="input-half input-fixwidth" id="txtMobile" runat="server" clientidmode="Static"  autocomplete="off" disabled="true" />
                          <div class="fixwidth">
                            <p class="info btn-center">
                                <asp:Button ID="btnReSms" runat="server" Text="重發認證碼" CssClass="btn btn-primary" OnClick="btnReSms_Click" />
                            </p>
                          </div>  
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="grui-form">
                      <div class="form-unit">
                        <div class="data-input">
                          <input type="text" name="code" class="input-half input-fixwidth" placeholder="請輸入認證碼" autocomplete="off">
                          <div class="fixwidth">
                            <p class="info btn-center">
                                <asp:Button CssClass="btn btn-primary" id="btnValidateCode" runat="server" Text="送出" OnClick="btnValidateCode_Click" />
                            </p>
                          </div>  
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <asp:PlaceHolder ID="phError" runat="server">
                          <div class="enter-error" style="display:block">
                            <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                            <p><asp:literal id="litErrorMessage" runat="server" /></p>
                            <br>
                          </div>
                      </asp:PlaceHolder>
                    </div>
                    <br/>
                    <p class="info btn-center">
                      <input type="button" class="btn btn-large rd-payment-xlarge-btn btnGoBack" value="取消">
                    </p>
                  </div>      
                </div>
            </asp:PlaceHolder>
            <input type="hidden" id="hidUserId" runat="server"/>
            <input type="hidden" id="hidNotifyType" runat="server" />
            <input type="hidden" id="hidMobile" runat="server" />
        </div>
    </div>
    <img src="/Themes/OWriteOff/loading.gif" id="imgLoading" style="display:none" />
</asp:Content>

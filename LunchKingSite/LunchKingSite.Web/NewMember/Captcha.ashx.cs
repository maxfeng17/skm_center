﻿using System;
using System.Web;
using System.Web.SessionState;
using System.Drawing;
using System.Drawing.Drawing2D;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.Web.NewMember
{
    /// <summary>
    /// Summary description for Captcha
    /// </summary>
    public class Captcha : IHttpHandler, IRequiresSessionState
    {
        private static string[] Code = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

        public void ProcessRequest(HttpContext context)
        {
            CreateCheckCodeImage(GenerateCheckCode(context), context);
        }

        private string GenerateCheckCode(HttpContext context)
        {
            string key = context.Request["x"];
            string checkCode = string.Empty;
            int codeLength = 6;

            Random random = new Random();

            for (int i = 0; i < codeLength; i++)
            {
                checkCode += Code[random.Next(Code.Length)];
            }

            //儲存在session 
            context.Session[context.Request["sessionId"]] = checkCode;
            if (string.IsNullOrEmpty(key) == false)
            {
                NewMemberUtility.StoreCaptcha(key, checkCode);
            }

            return checkCode;
        }

        private void CreateCheckCodeImage(string checkCode, HttpContext context)
        {
            if (checkCode == null || checkCode.Trim() == string.Empty)
            {
                return;
            }

            int height = int.TryParse(context.Request["CapthcaHeight"] != null ? context.Request["CapthcaHeight"] : string.Empty, out height) ? height : 57;
            int width = int.TryParse(context.Request["CapthcaWidth"] != null ? context.Request["CapthcaWidth"] : string.Empty, out width) ? width : 300;
            Bitmap image = new Bitmap(width, height);//new Bitmap((int)Math.Ceiling((checkCode.Length * 12.5)), 22);
            Graphics g = Graphics.FromImage(image);

            try
            {
                Random random = new Random();

                //清空圖片背景色 
                g.Clear(Color.White);

                //圖片的背景噪音   
                for (int i = 0; i < 25; i++)
                {
                    int x1 = random.Next(image.Width);
                    int x2 = random.Next(image.Width);
                    int y1 = random.Next(image.Height);
                    int y2 = random.Next(image.Height);

                    g.DrawLine(new Pen(Color.Silver), x1, y1, x2, y2);
                }

                Font font = new Font("Arial", 22, (FontStyle.Bold | FontStyle.Italic));
                LinearGradientBrush brush = new LinearGradientBrush(new Rectangle(0, 0, image.Width, image.Height), Color.Blue, Color.DarkRed, 1.2f, true);
                int intWidth = (int)g.MeasureString(checkCode, font).Width;
                int intHeight = (int)g.MeasureString(checkCode, font).Height;
                g.DrawString(checkCode, font, brush, (image.Width - intWidth) / 2, (image.Height - intHeight) / 2);

                //圖片的前景噪音 
                for (int i = 0; i < 100; i++)
                {
                    int x = random.Next(image.Width);
                    int y = random.Next(image.Height);

                    image.SetPixel(x, y, Color.FromArgb(random.Next()));
                }

                //圖片的外框 
                g.DrawRectangle(new Pen(Color.Silver), 0, 0, image.Width - 1, image.Height - 1);

                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                context.Response.ClearContent();
                context.Response.ContentType = "image/Gif";
                context.Response.BinaryWrite(ms.ToArray());
            }
            finally
            {
                g.Dispose();
                image.Dispose();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}

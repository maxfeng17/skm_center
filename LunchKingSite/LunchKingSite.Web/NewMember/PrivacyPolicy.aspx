﻿<%@ Page Language="C#" MasterPageFile="~/NewMember/NewMember.Master"
    AutoEventWireup="true" CodeBehind="PrivacyPolicy.aspx.cs" Inherits="LunchKingSite.Web.NewMember.PrivacyPolicy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/Themes/PCweb/css/MasterPage.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="MemberCardFrame">
        <div class="MemberCardTopBlack">
            <div class="LifeMemberTopText">
                會員服務條款
            </div>
            <div class="MemberUserpolicydate">
                民國106年10月16日修訂
            </div>
            <br />
        </div>
        <div class="LifeMemberCenter">
            <div class="MemberPrivacy">
                <div style="font-size: 15px; padding: 10px 20px 20px 20px; border: 0px solid #76A045;">
                    歡迎您來到https://www.17life.com(以下簡稱「本網站」)。本網站由康太數位整合股份有限公司負責經營。本網站係依據會員服務條款（以下簡稱「本條款」）提供各項服務（以下簡稱「本服務」），當您登錄成為本網站會員使用本服務時，即表示您已閱讀、瞭解並同意接受本條款之所有內容。此外，當您使用本網站的特定服務時，可能會依據該特定服務之性質，而須遵守本網站所另行公告之服務條款或相關規定。此另行公告之服務條款或相關規定亦均併入屬於本服務條款之一部分。 本網站有權於任何時間修改或變更本條款之內容，並以網站公告、電子郵件發送、簡訊或其他方式通知會員確認，若您於修改或變更後繼續使用本服務，視為您已閱讀、瞭解並同意接受本條款之修改或變更。如果您無法遵守本條款內容，或不同意本條款內容時，請您立即停止使用本服務。為確保您的權益，建議您經常確認本條款之最新條文內容。<br />
                    <br />
                    若您為未滿二十歲，應於您的家長（或監護人）閱讀、瞭解並同意本服務條款之所有內容及其後修改變更之內容後，才可以使用或繼續使用本服務。當您使用或繼續使用本服務時，即推定您的家長（或監護人）已閱讀、瞭解並同意接受本條款之所有內容及其後修改變更。<br />
                    <br />
                    當您違反本網站所聲明之條款時，本網站得終止您使用本服務。<br />

                    <br />
                    
                    <span style="font-weight: bold; font-size: 18px;">1.資料連結<br />
                    </span>

                    您可能會因本網站上的連結而因此連結至其他業者經營的網站，但不表示本網站與該等業者有任何關係。其他業者經營的網站均由該業者自行負責，不屬本網站控制及負責範圍之內。您同意本網站無須為您連結至非屬於本網站之網站所生之任何損害，負損害賠償之責任。 本網站隨時會與其他公司、廠商等第三人（「內容提供者」）合作，由其提供內容供本網站刊登，本網站於刊登時均將註明內容提供者。基於尊重內容提供者之智慧財產權，本網站對其所提供之內容並不做實質之審查或修改，對該等內容之正確真偽亦不負任何責任。<br />
                    <br />


                    <span style="font-weight: bold; font-size: 18px;">2.註冊義務<br />
                    </span>

                    為了能使用本網站服務，您必須先留下個人資料並登錄成為我們的使用會員。並同意以下事項：<br />
                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(1) 您應於本網站註冊時，登錄正確真實及完整的資料。如果您提供任何錯誤或不實的資料進行登錄，本網站有權暫停或終止您的帳號，並拒絕您使用本服務。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(2) 在本網站會員資料或公開發表內容中填入任何不當或不雅字眼，本網站將保留刪除您會員資格與發表內容的權力。<br />
                    </p>
                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(3) 同意將您所提供的個人資料用於本網站各項行銷活動；包括但不限於免費與付費贈獎活動。<br />
                    </p>
                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(4) 同意本網站不定期發送電子報或商品訊息(EDM)至您所登錄的電子郵件位址，或利用電子郵件、實體郵件、電話、簡訊或其他方式進行商品行銷資訊之提供及其他服務。若您欲停止訂閱，可直接於電子報點選【取消訂閱】或來信客服中心，由客服人員為您處理。<br />
                    </p>
                    <br />
                    您進行本網站註冊程序時，可能會透過您於第三方之帳號、密碼登入，當您以第三方之帳號、密碼第一次登入時，本網站將會需要您額外填寫本網站提供服務時所需之其它必要資料。當您填寫完畢，即視同您登錄成為本網站的會員。<br />
                    <br />


                    <span style="font-weight: bold; font-size: 18px;">3.隱私權政策<br />
                    </span>

                    關於您的會員註冊以及其他特定資料依「<a href="https://www.17life.com/newmember/privacy.aspx" target="_blank">會員隱私保護權政策</a>」受到保護與規範。您了解當您使用本服務時，您同意本網站依據<a href="https://www.17life.com/newmember/privacy.aspx" target="_blank">隱私權保護政策</a>進行您個人資料的蒐集與利用。本網站亦保留未來修改隱私權政策之權利，並以網站公告、電子郵件發送、簡訊或其他方式通知會員確認，惟為確保您的權益，建議您經常確認隱私權政策之最新條文內容。<br />
                    <br />

                    <span style="font-weight: bold; font-size: 18px;">4.會員帳號、密碼及安全<br />
                    </span>

                    完成本網站的會員註冊及登入程序後，您會有一組帳號及密碼。您有責任維持帳號及密碼的機密安全，並應負起個人帳號使用的所有相關責任，請勿將帳號及密碼提供給您以外之第三人使用並定期更新密碼為保會員帳號安全。若發現個人帳號遭到盜用或有其他任何安全問題發生時，請您立即通知本網站。本網站有權依自行之考量，於通知及未通知之情形下，關閉長時間未使用的會員帳號。<br />

                    <br />
                    <span style="font-weight: bold; font-size: 18px;">5.交易行為<br />
                    </span>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(1)本網站的【預約系統、核銷系統】服務，僅提供訂單資訊系統方便您與各廠商進行連絡慿証兌換交易。當您透過本網站的預約系統服務與核銷系統進行聯繫或是額外產生個人交易時，所產生的買賣行為僅存在您與各廠商或個人兩造之間。若有任何交易上的爭議與賠償問題，本網站將不做任何相關擔保責任。<br />
                    </p>


                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(2) 您同意在本網站所進行的所有線上消費，都以本網站系統所自動記錄的電子交易資料為準，如有糾紛，亦以該電子交易資料為認定標準。若您發現交易資料不正確，應立即通知本網站。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(3) 若您於本網站訂購產品後，經由本網站退換貨、取消訂單之頻率過高，或任何對本網站造成系統負擔與資訊作業上之困擾或損害，經本網站判定為惡意行為時，本網站將保留拒絕交易或停止對您提供本服務之權利。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">
                        (4) 選擇以刷卡方式付款時，必須填寫確實的信用卡資料。若發現有不實登錄或任何未經持卡人許可而盜刷其信用卡或Visa金融卡的情形時，本網站得以暫停或終止您的會員資格，並停止您尚未兌換的好康憑證之使用。若違反相關法律，亦將依法追究。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">
                        (5) 您可選擇將發票委由本網站捐贈予第三方之公益團體，並同意捐贈後即無法將該發票退回或重新開立。<br />
                        <br />
                    </p>
                    本網站之刷卡機制系採用台新銀行之刷卡金流系統；台新銀行已通過ISO 27001：2005/BS 7799：2005 國際資訊安全管理標準驗證，可鑑別並進行有效管理各項資訊運用所潛在的威脅與風險。為保護會員的隱私及權益，本網站並不會留存會員在付款時所登錄的卡號等金融資料。您的刷卡資料將會經由SSL安全協定加密傳輸至台新銀行，您的個人及信用卡資料均可獲得安全措施的維護。<br />
                    <br />
                    <span style="font-weight: bold; font-size: 18px;"><a id="restrictions" name="restrictions" style="text-decoration:none;color:black;">6.使用條款</a><br />
                    </span>
                    
                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(1) 好康憑證得以您持有之本網站紅利點數、17Life購物金與17Life折價券扣抵兌換金額。當您持有的紅利點數、17Life購物金與17Life折價券的總額不足以兌換時，可立即以刷卡或其他付費方式購買17Life購物金，完成該次的好康憑證兌換。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(2) 每筆好康憑證於您購買時，將分別存入發行人於台新銀行、陽信銀行開立之信託專戶，專款專用，信託期間自發售日起算1年。所稱專用，係指供發行人履行交付商品或提供服務義務使用。全程保障您的權益！<br />
                        發行資訊：康太數位整合股份有限公司<br />
                        地      址：台北市中山區中山北路一段11號13樓
                        <br />
                        統一編號：24317014
                        <br />
                        負 責 人：李易騰<br />
                        實收資本額：180,000,000元<br />
                    </p>
                    
                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(3) 好康憑證之商品或服務內容由本網站合作之店家提供，本網站則為行銷與銷售服務提供者。請務必於兌換優惠期間內使用，並確實了解好康憑證及商品頁面所載明之使用說明（包括但不限於：使用時段、事前預約規定、內用或外帶、每人使用限制）。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(4) 好康憑證碼類型分為一維條碼、二維條碼、憑證序號，請您依本平台系統指示之方式持17Life app、手機簡訊或列印出之紙本前往店家兌換該項商品或服務，店員將會依據您出示之17Life app、手機簡訊或列印出之紙本進行憑證碼之核對，請您務必配合店員之核對程序，並於核對無誤後於店家之兌換商品簽收單簽上完整清晰之姓名與聯繫電話；消費者不得以其他格式之憑證要求兌換商品，目前並非每一檔好康皆提供簡訊憑證，請以該檔好康說明為主。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(5) 好康憑證不得與其他優惠合併使用，且於使用時恕無法兌換現金。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(6) 好康憑證所載之序號，應妥善保存。若因好康憑證遺失或公開序號造成序號被第三方兌換所產生之損失，須由會員自行承擔。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(7) 好康憑證超過使用期限未使用，您可向本網站進行後續退款流程，作業手續費$0，相關流程請參「<a href="<%=SystemConfig.SiteUrl + SystemConfig.SiteServiceUrl %>?s=20445&q=20485" target="_blank">客服中心</a>」，提醒您儘量於期限內使用完畢。另2011/04/01以前上檔的好康，不適用全程退貨保證，但仍可至店家現場抵票面價值之商品。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(8) 部分好康憑證是特定時間、特定場合所提供之服務（包括但不限於演唱會、音樂會或展覽），若因故未能於該特定時間、場合使用該服務，依法恕無法要求退費或另行補足差額重現該服務。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(9) 若提供商品或服務之店家於消費者使用好康憑證前倒閉或因故無法提供商品或服務，您同意本網站得直接為您辦理後續退貨及退款。<br />
                    </p>
                    <br />

                    <span style="font-weight: bold; font-size: 18px;">7.紅利點數使用規範<br />
                    </span>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(1) 紅利點數為本網站行銷活動所贈予之消費兌換點數，在有效期限內，可以紅利點數10點 折抵新台幣1元進行消費，若於期限內未消費抵用完畢，點數將自動歸零。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(2) 紅利點數餘額與使用期限，可至［<a href="https://www.17life.com/User/bonuslistnew.aspx" target="_blank">餘額明細</a>］內查詢。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(3) 紅利點數與其兌換折抵之商品，不得要求轉換為現金或本網站之購物金，亦無法進行會員 與會員之間的轉換、販售或合併使用。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(4) 本網站保留可隨時修改或是取消紅利活動之權利。當您違反本條款時，本網站保留取 消您所有的紅利積點權利。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(5) 紅利點數僅限本網站使用。<br />
                    </p>
                    <br />

                    <span style="font-weight: bold; font-size: 18px;">8.17Life購物金使用規範<br />
                    </span>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(1) 17Life購物金係本網站所發行之線上虛擬貨幣。可用於本網站包括但不限於公益捐贈商品的折抵。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(2) 您可在本網站以新台幣1:1購得17Life購物金；並再用17Life購物金進行商品的折抵消費與兌換。17Life購物金為17Life會員專屬，若您使用信用卡付費，退款時可選擇以17Life購物金作為退款方式。17Life購物金1元可折抵新台幣1元。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(3) 17Life購物金無使用期限，且僅限於本網站使用。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(4) 17Life購物金不得要求轉讓，但本網站可接受並協助您辦理17Life購物金退款之相關 事宜。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(5) 本網站在您購買17Life購物金時所對您收取之金額，將分別存入發行人於台新銀行、陽信銀行開立之信託專戶，專款專用；所稱專用，係指供發行人履行交付商品或提供服 務義務使用。全程保障您的權益！<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">(6) 若您對17Life購物金有任何問題，請洽<a href="<%=SystemConfig.SiteUrl + SystemConfig.SiteServiceUrl %>?s=20445&q=20485" target="_blank">客服中心</a>。<br />
                    </p>

                    <p style="text-indent: -1.6em; margin-left: 1.6em;">
                        (7) 17Life購物金發行資訊：
                        <br />
                        發行單位：康太數位整合股份有限公司<br />
                        地      址：台北市中山區中山北路一段11號13樓
                        <br />
                        統一編號：24317014
                        <br />
                        負 責 人：李易騰<br />
                        實收資本額：180,000,000元<br />
                    </p>
                    <br />

                    <span style="font-weight: bold; font-size: 18px;">9.系統中斷或故障<br />
                    </span>

                    本服務有時候可能會出現中斷或故障等現象，或許將造成您使用上的不便、資料喪失、錯誤或其他經濟上損失等情形。您於使用本服務時宜自行採取防護措施。本網站對於您因使用（或無法使用）本服務而造成的損害，除故意或重大過失外，不負任何賠償責任。<br />

                    <br />
                    <span style="font-weight: bold; font-size: 18px;">10.廣告<br />
                    </span>

                    您在本網站中瀏覽到的所有廣告內容均由各該廣告商、產品與服務的供應商所設計與提出。您對於廣告之正確性與可信度應自行斟酌與判斷。本網站僅接受委託予以刊登，不對前述廣告負擔保責任。<br />

                    <br />
                    <span style="font-weight: bold; font-size: 18px;">11.未成年之兒童及青少年之保護<br />
                    </span>

                    為確保兒童及青少年使用網路的安全，並避免隱私權受到侵犯，家長（或監護人）應協助未成年之兒童及青少年檢查並確認本網站保護個人資料的<a href="https://www.17life.com/newmember/privacy.aspx" target="_blank">隱私權政策</a>，再決定是否同意提出相關個人資料。<br />
                    <br />

                    <span style="font-weight: bold; font-size: 18px;">12.著作權及智慧財產權的保護<br />
                    </span>

                    本服務所使用之軟體或程式、網站上所有內容，包括但不限於著作、圖片、檔案、資訊、資料、網站架構、網站畫面的安排、網頁設計，除本公司有特別約定外，皆由本公司或其他權利人依法擁有其智慧財產權，包括但不限於商標權、專利權、著作權、營業秘密與專有技術等。 任何人不得擅自使用、修改、重製、公開播送、改作、散布、發行、公開發表、進行還原工程、解編或反向組譯。任何人欲引用或轉載前述軟體、程式或網站內容，必須依法取得本公司或其他權利人的事前書面同意。如有違反，您應對本公司負損害賠償責任（包括但不限於訴訟費用及律師費用等）。
                    <br />
                    <br />

                    <span style="font-weight: bold; font-size: 18px;">13.您的法律義務及承諾<br />
                    </span>

                    絕不為任何非法目的或以任何非法方式使用本服務，並承諾遵守中華民國相關法規及一切使用網際網路之國際慣例。您若係中華民國以外之使用者，並同意遵守所屬國家或地域之法令。<br />
                    您同意並保證不得利用本服務從事侵害他人權益或違法之行為，包括但不限於：<br />
                    (1) 違反依法律或契約所應負之保密義務；<br />
                    (2) 冒用他人名義使用本服務；<br />
                    (3) 傳輸或散佈電腦病毒；<br />
                    (4) 其他本網站有正當理由認為不適當之行為<br />



                    <br />
                    <span style="font-weight: bold; font-size: 18px;">14.本條款之效力、準據法與管轄法院<br />
                    </span>

                    本條款中，任何條款之全部或一部份無效時，不影響其他約定之效力。<br />
                    您與本公司之權利義務關係，應依網路交易指導原則及中華民國法律定之；若發生任何爭議，以台灣台北地方法院為第一審管轄法院。<br />
                    本公司的任何聲明、條款如有未盡完善之處，將以最大誠意，依誠實信用、平等互惠原則，共商解決之道。<br />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

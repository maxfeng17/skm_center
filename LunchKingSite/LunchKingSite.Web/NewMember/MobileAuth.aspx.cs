﻿using System;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.UI;
using log4net;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.NewMember
{
    public partial class MobileAuth : MemberPage
    {
        protected ISysConfProvider Config = ProviderFactory.Instance().GetConfig();
        protected MobileMember mm;

        /// <summary>
        /// 強制不設密碼
        /// </summary>
        public readonly string ForceDontSetPassword = "g";
        /// <summary>
        /// 如果已認證手機，就不再認證，改成導到首頁
        /// </summary>
        public readonly string SkipIfAuthed = "skipIfAuthed";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }
            mm = MemberFacade.GetMobileMember(User.Identity.Name);
        }

        [WebMethod]
        public static dynamic SendSms(string mobile)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return new
                {
                    result = MemberFacade._USER_NOT_LOGIN,
                    sendcount = 0
                };
            }
            string userName = HttpContext.Current.User.Identity.Name;
            int userId = MemberFacade.GetUniqueId(userName, true);
            int sendCount;
            DateTime nexCanSendSmsDate;
            SendMobileAuthCodeReply result = MemberUtility.SendMobileAuthCode(userId, mobile, false, out sendCount,out nexCanSendSmsDate);
            //故意等待，分擔一點簡訊好慢的感覺
            Thread.Sleep(1000);
            return new
            {
                result = result.ToString(), 
                sendCount
            };
        }

        [WebMethod]
        public static dynamic ValidateCode(string mobile, string code)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return new
                {
                    result = MemberFacade._USER_NOT_LOGIN,
                    hasPasswod = false,
                };                
            }
            Member mem = MemberFacade.GetMember(HttpContext.Current.User.Identity.Name);
            bool needSetPassword;
            string resetPasswordKey;
            MemberValidateMobileCodeReply result = MemberFacade.ValidateMobileCode(mem.UniqueId, mobile, code, false, 
                out needSetPassword, out resetPasswordKey);
            return new
            {
                needSetPassword,
                result = result.ToString()
            };
        }

        [WebMethod]
        public static dynamic SetPassword(string password)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return new
                {
                    result = MemberFacade._USER_NOT_LOGIN
                };
            }
            Member mem = MemberFacade.GetMember(HttpContext.Current.User.Identity.Name);
            MemberSetPasswordReply result = MemberFacade.SetMobleMemberPassword(mem.UniqueId, password);
            return new
            {
                result = result.ToString()
            };
        }
    }


}
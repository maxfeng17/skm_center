﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WaitConfirm.aspx.cs" Inherits="LunchKingSite.Web.NewMember.WaitConfirm"
    MasterPageFile="~/NewMember/NewMember.master" %>

<%@ MasterType VirtualPath="~/NewMember/NewMember.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("#mailSample").show();
            $("#noMail").hide();
            $("#wrongPassword").hide();
            $("#noPassword").hide();

            $("#txtEmail").focus(function () {
                $("#noMail").hide();
                $("#mailSample").show();
                $("#lineMail").removeClass("LifeMemberLogingColor").addClass("LMPasswordIn");
            });

            $("#txtPassword").focus(function () {
                $("#linePassword").removeClass("LifeMemberLogingColor").addClass("LMPasswordIn");
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="MemberCardFrame">
        <div class="MemberCard">
            <div class="MemberCardTopBlack">
                <div class="LifeMemberTopText">立即啟用索取好康！</div>
            </div>
            <div class="LifeMemberCenter">
                <div class="LifeMemberConfirmMessage rdl-cmsgT">
                    系統已經寄發認證信到您的電子信箱：<span class="LifeMemberMessageMail"><%=Session["newRegisterMail"]%><br />
                    </span>17Life的更多好康等著您，請立即前往收信完成認證，才能正式啟用17Life的各項服務唷！<br />
                    <span style="color: #F00;">( 請在7天內完成帳號認證，逾期需重新再註冊一次唷！)</span>
                    <div class="LifeMemberConfirmRemarks">
                        ✽沒有收到認證信！<br />
                        (1) 請檢查信件是不是被分類倒「垃圾信箱」中了！為了避免日後漏接好康訊息，建議 <a href="../Ppon/WhiteListGuide.aspx" target="_blank">
                            將17Life設為白名單</a><br />
                        (2) 若確認未收到認證信，可以再重新補寄認證信： <a href="javascript:reSend('<%=Session["newRegisterMail"]%>');">
                            馬上補寄認證信</a><br />
                        (3) 還是沒收到認證信！是不是您的mail填寫錯誤呢？ <a href="javascript:window.location.href='<%=ResolveUrl("~/mvc/NewMember/RegisterByEmail") %>';">
                            立即修改登入mail</a><br />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

﻿using System;
using System.Web.Security;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.Web.NewMember
{
    public partial class PEZOpenPass : BasePage
    {
        public string webUrl = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            webUrl = WebUtility.GetLoginPath();

            if (Page.User != null && Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                foreach (string key in Request.Cookies.AllKeys)
                {
                    Response.Cookies[key].Expires = DateTime.Now;
                }
                Response.Redirect("PEZOpenPass.aspx");
            }
        }
    }
}
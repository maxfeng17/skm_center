﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;

namespace LunchKingSite.Web.NewMember
{
    public partial class OAuthAllowClient : System.Web.UI.Page
    {
        private static Dictionary<TokenScope, string> permDescs = new Dictionary<TokenScope, string>();

        static OAuthAllowClient()
        {
            //TODO 說明內容應該可以改記到 SystemCode
            permDescs[TokenScope.User] = "個人資料";        }

        private string AppKey
        {
            get { return (string)Session[LkSiteSession.OAuthAppKey.ToString()]; }
        }

        private string NextUrl
        {
            get { return Request["next_url"]; }
        }

        private string CancelUrl
        {
            get { return Request["cancel_url"]; }
        }

        protected string ClientTitle { get; set; }

        protected string ClientPermsDescription { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false
                || AppKey == null || NextUrl == null || CancelUrl == null)
            {
                Response.Redirect("/");
                return;
            }

            var client = OAuthFacade.GetClient(AppKey);
            if (client.Enabled == false)
            {
                Response.Redirect(CancelUrl);
                return;
            }

            ClientTitle = client.Name;
            ClientPermsDescription = "";
            foreach (TokenScope scope in Enum.GetValues(typeof(TokenScope)))
            {
                if (permDescs.ContainsKey(scope))
                {
                    if (ClientPermsDescription.Length > 0)
                    {
                        ClientPermsDescription += ", ";
                    }
                    ClientPermsDescription += permDescs[scope];
                }                
            }
            if (ClientPermsDescription.Length == 0)
            {
                ClientPermsDescription = "資料";
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            OAuthFacade.ClientSetUser(AppKey, MemberFacade.GetUniqueId(this.User.Identity.Name));
            Response.Redirect(NextUrl);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(CancelUrl);
        }
    }
}
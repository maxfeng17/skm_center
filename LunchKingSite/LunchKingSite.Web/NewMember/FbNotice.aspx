﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FbNotice.aspx.cs" Inherits="LunchKingSite.Web.NewMember.FbNotice" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>17Life</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
<div id="Notification" style=" width:960px; margin:0 auto; text-align:center;">
<img src="<%=SystemConfig.SiteUrl %>/images/Notification/Notification_1.png" alt="" width="960" height="398" border="0" usemap="#Map" />
<map name="Map" id="Map">
  <area shape="rect" coords="551,296,683,324" href="<%=LunchKingSite.BizLogic.Component.MemberActions.MemberUtilityCore.RegisterUrl %>" />
</map>
<br />
<img src="<%=SystemConfig.SiteUrl %>/images/Notification/Notification_2.png" alt="" width="960" height="466" border="0" usemap="#Map2" />
<map name="Map2" id="Map2">
  <area shape="rect" coords="281,50,747,69" href="<%=SystemConfig.SiteUrl.Replace("http", "https") %>/NewMember/ForgetPassword.aspx" target="_blank" />
</map>
<br />
<img src="<%=SystemConfig.SiteUrl %>/images/Notification/Notification_3.png" width="960" height="426" border="0" alt="" /><br />
<img src="<%=SystemConfig.SiteUrl %>/images/Notification/Notification_4.png" width="960" height="311" border="0" alt="" /><br />
<img src="<%=SystemConfig.SiteUrl %>/images/Notification/Notification_5.png" alt="" width="960" height="499" border="0" usemap="#Map3" />
<map name="Map3" id="Map3">
  <area shape="rect" coords="180,49,857,70" href="<%=SystemConfig.SiteUrl.Replace("http", "https") %>/m/Login?ReturnUrl=/ppon/default.aspx" target="_blank" />
</map>
<br />
</div>

<div id="footer">
  <table border="0" align="center" cellpadding="0" cellspacing="0" style="text-align:center;">
    <tr>
		<td style="TEXT-ALIGN: center; LINE-HEIGHT: 20px; COLOR: #333; FONT-SIZE: 14px; TEXT-DECORATION: none; FONT-FAMILY: Arial, "Microsoft JhengHei"; "  valign="middle" align="center">
        <span >104 台北市中山區中山北路一段11號13樓 17Life</span>
        </td>
    </tr>
    <tr>
		<td style="TEXT-ALIGN: center; LINE-HEIGHT: 20px; COLOR: #333; FONT-SIZE: 14px; TEXT-DECORATION: none; FONT-FAMILY: Arial, "Microsoft JhengHei"; "  valign="middle" align="center">
        客服專線：<span ><%=SystemConfig.ServiceTel %></span>　客服信箱：<span><a style="COLOR: #666; TEXT-DECORATION: none"  href="mailto:service@17life.com">service@17life.com</a></span>
 　		服務時間：<span>平日&nbsp;&nbsp;9:00~18:00</span>
        </td>
    </tr>
    <tr>
        <td style="TEXT-ALIGN: center; LINE-HEIGHT: 20[x; COLOR: #333; FONT-SIZE: 14px; TEXT-DECORATION: none; FONT-FAMILY: Arial, "Microsoft JhengHei"; "  valign="middle" align="center">
        版權所有 © <script language="javascript"> var Today = new Date(); document.write(Today.getFullYear()); </script> 康太數位整合股份有限公司&nbsp;&nbsp;17life.com </span>
        </td>
    </tr>
  </table>
</div>
    </div>
    </form>
</body>
</html>

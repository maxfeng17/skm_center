﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MemberAuth.aspx.cs"
    Inherits="LunchKingSite.Web.NewMember.MemberAuth" MasterPageFile="~/NewMember/NewMember.master"
    ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        $(document).ready(function () {

            $("#txtPassword").focus(function () {
                $("#spanPassword").html('6~12碼，英文或數字，英文請區分大小寫');
                $("#linePassword").removeClass("LMPasswordInColor").addClass("LMPasswordIn");
            });


            $("#txtConPassword").focus(function () {
                $("#spanConPassword").html('請重新再輸入一次密碼！');
                $("#lineConPassword").removeClass("LMPasswordInColor").addClass("LMPasswordIn");
            });

            $("#btnSubmit").click(function () {
                if (checkInfo()) {
                    $('form').submit();
                }
            });

        });

        function checkInfo() {
            // password check
            if (!passwordCheck($("#txtPassword").val())) {
                $("#spanPassword").html('<span style="color: #F00; letter-spacing: normal;">密碼格式錯誤！</span>');
                $("#linePassword").removeClass("LMPasswordIn").addClass("LMPasswordInColor");
            } else {
                // confirm password check
                if (!passwordConfirm($("#txtPassword").val(), $("#txtConPassword").val())) {
                    $("#spanConPassword").html('<span style="color: #F00; letter-spacing: normal;">您輸入的密碼不符合，請重新輸入！</span>');
                    $("#lineConPassword").removeClass("LMPasswordIn").addClass("LMPasswordInColor");
                } else {
                    return true;
                }
            }

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="MemberCardFrame">
        <div class="MemberCard">
            <asp:Panel ID="panPasswordSet" runat="server">
                <div class="MemberCardTopBlack">
                    <div class="LifeMemberTopText">認證成功！請繼續設定密碼</div>
                </div>

                <div class="LifeMemberCenter">
                    <div class="LMPasswordFrame">
                        <ul class="LMPasswordIn">
                            <li class="LMPasswordIn">
                                <span class="LMPasswordFrameTittle rdl-LMPawdTit">你的帳號：</span>
                                <span class="LMPasswordFrameText rdl-LMPawdFTe "><%=UserEmail %></span>
                                <span class=""></span>
                            </li>

                            <li id="linePassword" class="LMPasswordIn">
                                <span class="LMPasswordFrameTittle">自訂新密碼：</span>
                                <span class="LMPasswordFrameText rdl-LMPawdFTe ">
                                    <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" />
                                </span>
                                <span id="spanPassword" class="LMPasswordFrameNote" style="dispaly: inline-block">6~12碼，英、數混合，英文請區分大小寫。</span>
                            </li>

                            <li id="lineConPassword" class="LMPasswordIn">
                                <span class="LMPasswordFrameTittle rdl-LMPawdTit">確認密碼：</span>
                                <span class="LMPasswordFrameText rdl-LMPawdFTe ">
                                    <asp:TextBox ID="txtConPassword" TextMode="Password" runat="server" />
                                </span>
                                <span id="spanConPassword" class="LMPasswordFrameNote" style="dispaly: inline-block">請重新再輸入一次密碼。</span>
                            </li>
                        </ul>
                        <br />
                        <div class="text-center">
                            <input type="button" value="送出" id="btnSubmit" class="btn btn-large btn-primary"/>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>

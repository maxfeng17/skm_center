﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Web.User;
using LunchKingSite.WebLib.Component;
using PayEasy.NetPlatform.Friends.SSO;
using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace LunchKingSite.Web.NewMember
{
    public partial class PezLogin : Page
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PezLogin));
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        protected void Page_Load(object sender, EventArgs e)
        {
            CASAuthentication casAuth = new CASAuthentication(new PezSsoProcess());

            // ** 先處理 Single Sign Out
            // 因為登入時,送給 cas server 的 service 參數是本頁(因此 cas server 認得這個網址),
            // 所以使用者有遠端登出行為時, cas server 會發出 single sign out 的 request 給全部的登入網址,
            // 通知各系統特定使用者已登出(以CasTicket做識別).
            if (casAuth.ProcessSingleSignOutRequest())
            {
                return;
            }

            // ** 登入
            try
            {
                long memNum = casAuth.LoginProcess("", config.PayEasyCasServerUrl);

                // 授權相關處理是放在 IAuthProcess.UserLogin() 的實作中,也可以放這裡.
                if (memNum > 0)
                {
                    // 登入成功,取得memNum
                    VerifyAndRedirectUser(SingleSignOnSource.PayEasy, memNum.ToString());
                }
            }
            catch (GeneralSSOException ex)
            {
                log.Info("PEZ SSO LoginProcess error.", ex);
                // 登入失敗
                Response.Redirect(FormsAuthentication.LoginUrl);                
            }            
        }

        private void VerifyAndRedirectUser(SingleSignOnSource source, string externMemberId)
        {
            ExternalMemberInfo memDict = new ExternalMemberInfo();
            Session[LkSiteSession.ExternalMemberId.ToString()] = memDict;
            memDict[source] = externMemberId;

            //判斷福利網
            if (string.IsNullOrEmpty(externMemberId))
            {
                log.Error("PEZId空值");
                Session[LkSiteSession.IsWelfareMember.ToString()] = PEZType.none;
            }
            else
            {
                if (PCashWorker.IsWelfareMember(externMemberId))
                {
                    Session[LkSiteSession.IsWelfareMember.ToString()] = PEZType.WelfareMember;
                }
                else
                {
                    Session[LkSiteSession.IsWelfareMember.ToString()] = PEZType.none;
                }
            }

            string redirUrl = "~";
            if (!string.IsNullOrEmpty(Request["v"]) && Request["v"].Replace("?FromId=pez", "") == "r")
            {
                redirUrl = ResolveUrl("~/NewMember/confirmmember");
            }
            else
            {
                if ((Session[LkSiteSession.NowUrl.ToString()] != null) &&
                    (!string.IsNullOrEmpty(Session[LkSiteSession.NowUrl.ToString()].ToString().Trim())))
                {
                    redirUrl = Session[LkSiteSession.NowUrl.ToString()].ToString();
                }
                else
                {
                    redirUrl = ResolveUrl("~/ppon/");
                }
            }

            MemberLink link = mp.MemberLinkGetByExternalId(externMemberId, source);

            if (link.IsLoaded)
            {
                #region link.IsLoaded=true

                //如果使用者還有登入類別尚未串接，且沒有勾選不要詢問的話，就轉址到串接頁面
                Member mem = mp.MemberGet(link.UserId);
                if (mem.IsLoaded == false)
                {
                    throw new MemberAccessException("member [" + link.UserId + "] does not exist!");
                }
                
                #endregion link.IsLoaded=true

                #region 福利網會員狀態註記

                //TODO 準備移除 福利網會員狀態註記
                /* 
                bool isWelfareMember = Session[LkSiteSession.IsWelfareMember.ToString()] != null &&
                                       (PEZType) Session[LkSiteSession.IsWelfareMember.ToString()] ==
                                       PEZType.WelfareMember;
                MemberFacade.SetMemberStatusFlag(link.UserId, isWelfareMember, MemberStatusFlag.WelfareMember);
                */
                #endregion

                /*
                 * 這段暫時保留，如果哪天"又"想強制讓pez串接會員開通 17帳號時，可以再拿來用
                 * 
                if (Check17LifeMember(link.UserId) == false)
                {
                    Session[LkSiteSession.Bind17UserId.ToString()] = link.UserId;
                    Response.Redirect("Life17Bind.aspx?pez=1", false);
                    return;
                }
                 */

                NewMemberUtility.UserSignIn(MemberFacade.GetUserName(link.UserId), SingleSignOnSource.PayEasy, false);
            }
            else
            {
                redirUrl = ResolveUrl("~/NewMember/confirmmember");
            }

            // 將從註冊頁登入的導回首頁
            redirUrl = redirUrl.Replace("/NewMember/Register.aspx", "/default.aspx");

            Response.Redirect(redirUrl);
        }

        private bool Check17LifeMember(int userId)
        {
            MemberLink ml = mp.MemberLinkGet(userId, SingleSignOnSource.ContactDigitalIntegration);
            return ml.IsLoaded;
        }
    }
}
﻿using System;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.Web.NewMember
{
    public partial class ChangePostAddress : BasePage
    {
        public enum ChangePostAddressResult
        {
            Fail = 0,
            LinkExpired = 1,
            OK = 2,
            SameEmailSkip = 3
        }

        IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        protected string SiteUrl
        {
            get
            {
                return config.SiteUrl;
            }
        }

        public int UserId
        {
            get
            {
                int uid;
                int.TryParse(Request["uid"], out uid);
                return uid;
            }
        }

        public string UserName
        {
            get
            {
                return MemberFacade.GetUserName(UserId);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            switch (AuthChangeEmail())
            {
                case ChangePostAddressResult.OK:                    
                    panPass.Visible = true;
                    break;
                case ChangePostAddressResult.SameEmailSkip:
                    // member didn't change email ? or member auth change already. redriect directly.
                    Response.Redirect(config.SiteUrl);
                    break;
                default:
                    phFail.Visible = true;
                    break;
            }
        }

        private ChangePostAddressResult AuthChangeEmail()
        {            
            string authKey = Request["key"];
            string authCode = Request["code"];

            if (UserId == 0 || string.IsNullOrWhiteSpace(authKey) || string.IsNullOrWhiteSpace(authCode))
            {
                return 0;
            }
            Member mem = mp.MemberGetbyUniqueId(UserId);
            MemberAuthInfo mai = mp.MemberAuthInfoGet(UserId);

            if (mem.IsLoaded && mai.IsLoaded)
            {
                if (mai.AuthDate == null || string.IsNullOrWhiteSpace(mai.AuthKey) || string.IsNullOrWhiteSpace(mai.AuthCode))
                {
                    // link error
                    return ChangePostAddressResult.Fail;
                }
                if (mai.LinkExpired)
                {
                    // link expired
                    return ChangePostAddressResult.LinkExpired;
                }
                if (mai.Email.Equals(mem.UserName, StringComparison.OrdinalIgnoreCase))
                {
                    return ChangePostAddressResult.SameEmailSkip;
                }
                if (string.Equals(mai.AuthCode, authCode) && string.Equals(mai.AuthKey, authKey))
                {
                    if (NewMemberUtility.ChangeEmailSaveChange(UserId))
                    {
                        return ChangePostAddressResult.OK;
                    }                    
                }
            }
            return  ChangePostAddressResult.Fail;
        }
    }
}
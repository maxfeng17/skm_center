﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="LunchKingSite.Web.NewMember.Login"
    MasterPageFile="~/NewMember/NewMember.master" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.WebLib.Component" %>
<%@ MasterType VirtualPath="~/NewMember/NewMember.master" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" media="only screen and (min-width: 481px) and (max-width: 767px)" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" media="only screen and (max-width: 480px)" />

    <style type="text/css">
        .placeholder {
            color: Gray !important;
        }

        input:-ms-input-placeholder {
            color: Gray;
        }
    </style>
    <script type="text/javascript" src="../Tools/js/jquery.placeholder.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            if (localStorage.getItem("chkKeepLoginStatus")) {
                $("input[id=chkKeepLogin]").prop('checked', 'checked');
            }

            $("#pezLogin").click(function () { pezlogin(); });
            $("#fbLogin").click(function () { fblogin(); return false; });
            $("#pezLogo").click(function () { pezlogin(); });
            $("#LoginNew").click(function () {
                if (CheckPassWord() && checkAccountName()) {
                    document.forms[0].submit();
                } else {
                    return false;
                }

                if ($("input[id=chkKeepLogin]").is(':checked')) {
                    localStorage.setItem("chkKeepLoginStatus", true);
                } else {
                    localStorage.removeItem("chkKeepLoginStatus");
                }
            });

            $('#txtEmail, #txtPassword, #txtText,#captchaResponse').keypress(function (evt) {
                if (evt.which == 13) {
                    $("#LoginNew").trigger('click');
                    return false;
                }
            });

            $('#captchaLink').focus(function () {
                $('#captchaResponse').focus();
            });
            getCaptcha('<%= SessionId %>');

            $('input').placeholder();
            var passwordNext = $("#txtPassword").next();
            if (passwordNext.attr('type') == "password") {
                passwordNext.attr("name", "txtPassword");
            }
            //because txtEmail has default value from url
            if ($('#txtEmail').val() == "") {
                $('#txtEmail').focus();
            } else {
                $('#txtPassword').focus();
            }

            var pageType = '<%=Request["p"]%>';
            if (pageType == 'member17') {
                $('.LifeMemberCenter').addClass('member17');
            }
        });

        function getCaptcha(id) {
            $("#imgCaptcha").attr("src", "Captcha.ashx?x=" + Math.random() + "&sessionId=" + id + "&CapthcaHeight=30&CapthcaWidth=120");
            $('#captchaResponse').focus();
        }

        function pezlogin() {
            x = $(window).width();
            if (x > 480) {
                window.location.href = "<%=PezLoginUrl%>";
            } else {
                window.location.href = "<%=PezMobileLoginUrl%>";
            }
        }

        function fblogin() {
            window.location.href = "https://www.facebook.com/v2.0/dialog/oauth?client_id=<%=FacebookApplicationId %>&redirect_uri=<%=ReturnUrl %>&scope=<%=Scope %>&state<%=AcsCode%>";
        }

        function noPassword() {
            alert("請輸入密碼！");
            if ($('#txtText').css('display') != 'none') {
                $('#txtText').focus();
            } else if ($('#txtPassword').css('display') != 'none') {
                $('#txtPassword').focus();
            }
        }

        function wrongPassword() {
            alert("帳號密碼不符。\n密碼6~12碼，請區分大小寫。\n為了您的帳號安全，連續錯誤5次，帳號將被鎖定。\n若您忘了密碼，請點選\"忘記密碼\"按鈕。");
        }

        function emptyEmail() {
            alert("請輸入帳號！");
        }

        function wrongEmail() {
            alert("<%=MemberFacade.IsMobileAuthEnabled ? "無此帳號！\\n\\n請輸入已認證的手機號碼或電子信箱" : "無此帳號！\\n\\n請輸入已認證的電子信箱"%>");
        }

        function wrongCaptcha() {
            alert("圖形驗證輸入錯誤！");
        }

        function otherLink() {
            alert("您是用其它串接方式登入！");
        }

        function payeasyLink() {
            alert("您的帳號應使用PayEasy登入！");
        }

        function facebookLink() {
            alert("您的帳號應使用Facebook登入！");
        }

        function CheckPassWord() {
            if ($('#txtPassword').val() == '') {
                noPassword();
                return false;
            } else {
                return true;
            }
        }

        function checkAccountName() {
            if ($("#txtEmail").val() === '') {
                alert('請輸入帳號!');
                return false;
            }
            var regMail = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
            var regMobile = /\d{10}/;
            if (!regMail.test($("#txtEmail").val())) {
                if (!regMobile.test($("#txtEmail").val())) {
                    alert('查無此信箱。');
                    return false;
                }
            }
            return true;
        }

    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="LifeMemberFrame">
        <div id="pezCloseBanner" style="display: none;">
            <uc1:Paragraph ID="pA" runat="server" />
        </div>
        <div class="LifeMember">
            <div class="LifeMemberTopBlack">
                <div class="LifeMemberTopText">登入會員</div>
            </div>
            <div class="LifeMemberCenter">
                <div class="LifeMemberLogFrame">
                    <!--17life會員登入-->
                    <div id="tabs-1" class="tab_content">
                        <div class="LifeMemberOtherLoging1">
                            <div class="lmbecOtloginfor">
                                <img class="LifeMember17logo" src="../Themes/PCweb/images/17LIFElogoBig.png" width="280" />
                                <div class="lmbeclogiTi">17Life帳號登入獨享好康!也可免註冊直接買</div>
                                <div class="lmbecloging">
                                    <ul class="lmbecloging">
                                        <li class="lmbecloging">
                                            <input id="txtEmail" name="txtEmail" placeholder="<%=MemberFacade.IsMobileAuthEnabled ? "手機號碼或電子信箱" : "電子信箱" %>" type="text" value="<%=DefaultUser %>" />
                                        </li>
                                        <li class="lmbecloging">
                                            <input id="txtPassword" name="txtPassword" type="password" placeholder="登入密碼(請注意密碼有區分大小寫)" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <asp:Panel ID="plCatcha" CssClass="lmbecloging lmbeclogingProof" runat="server" Visible="False">
                                <div class="imgCaptcha_120">
                                    <img id="imgCaptcha" alt="" width="120" />
                                </div>
                                <a id="captchaLink" href="javascript:getCaptcha('<%= SessionId %>');">更換一組驗證碼</a>
                                <ul class="lmbecloging lmbecloging_160">
                                    <li class="lmbecloging">
                                        <input id="captchaResponse" name="captchaResponse" type="text" placeholder="請輸入顯示的文字(英文可不區分大小寫)" style="color: gray; display: inline-block;" />
                                    </li>
                                </ul>
                            </asp:Panel>

                            <div class="lmMemoryLog">
                                <label>
                                    <input type="checkbox" id="chkKeepLogin" name="chkKeepLogin" checked="checked" title="保持我的登入狀態">保持我的登入狀態</label>
                                <div class="lmQuestionIcon">
                                    <img src='../Themes/PCweb/images/lmQuestion.png' />
                                    <div class="lmMemoryLog-box">
                                        <p>為了您的登入方便，請勾選此項目，若您使用的是共用裝置，建議不要勾選。<a target="_blank" href="/ppon/newbieguide.aspx?s=20566&q=20587">了解更多</a></p>
                                    </div>
                                </div>
                            </div>

                            <div class="lmbelogGO">
                                <a id="LoginNew" style="cursor: pointer;">
                                    <div class="lmbeclogingBtn"></div>
                                </a>
                                <div class="LifeMemberLoginText3">
                                    <a href="ForgetPassword.aspx" style="margin-left: 2px; letter-spacing: 0.1em;">忘記密碼</a>
                                    <span>|</span> <a href="<%=RegisterUrl %>" style="margin-left: 3px; letter-spacing: 0.1em;">註冊</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="fbLoginBlock" class="LifeMemberOtherLoging2">
                        <!--FB會員登入-->
                        <div id="fbLogo" class="lmbecOtloginfor">
                            <div class="lmbeclogFBPEZTi">或以其他方式登入</div>
                            <a id="fbLogin" style="cursor: pointer;">
                                <div class="lmbecFbBtn"></div>
                            </a>
                        </div>
                        <!--PEZ會員登入-->
                        <div class="lmbecOtloginfor">
                            <a id="pezLogin" style="cursor: pointer;">
                                <div class="lmbecPezBtn"></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="checkResult" name="checkResult" />
    </div>

</asp:Content>

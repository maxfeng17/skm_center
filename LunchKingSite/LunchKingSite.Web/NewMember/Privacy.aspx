﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Privacy.aspx.cs"
    Inherits="LunchKingSite.Web.NewMember.Privacy" MasterPageFile="~/NewMember/NewMember.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/Themes/PCweb/css/MasterPage.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css")%>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">

      <div class="MemberCardFrame">
        <div class="MemberCardTopBlack">
            <div class="LifeMemberTopText">會員隱私權保護政策</div>   
         <div class="MemberUserpolicydate">民國102年07月04日修訂</div><br/>
    
    
        </div>
        <div class="LifeMemberCenter">
        <div class="MemberPrivacy">
        <div style="text-align:left; font-size:15px; padding:10px 20px 20px 20px;border:0px solid #76A045;">
          <P>
            ◎17Life網站個人資料蒐集告知及隱私權保護政策
            <br>
            親愛的朋友~感謝您進入17Life網站，17Life網站一向將使用者個人資料及隱私權的尊重及保障，視為17Life網站經營的基本政策。為了幫助您瞭解我們是如何蒐集、處理及利用您在17Life網站所提供的個人資料，請您詳細閱讀17Life網站個人資料蒐集告知及隱私權保護政策。這份政策將告訴您，17Life在網站內所蒐集、處理的資料內容及利用方式，以及我們對使用者隱私權保護政策採行的措施，這份政策亦構成17Life會員約定同意書的一部分。
          </P><br/>
          <P> <b>一、依個人資料保護法第8條規定之告知事項如下： </b>
          </P>
          <p>1. 個人資料蒐集之主體：康太數位整合股份有限公司(即17Life)所蒐集，並由前述主體處理與利用。</p>
          <P>
            2. 個人資料蒐集之目的：
            <br>
            <p style="margin-left:1.6em;">040 行銷（包含金控共同行銷業務</p>
            <p style="margin-left:1.6em;">067 信用卡、現金卡、轉帳卡或電子票證業務</p>
            <p style="margin-left:1.6em;">069 契約、類似契約或其他法律關係事務</p>
            <p style="margin-left:1.6em;">077 訂位、住宿登記與購票業務</p>
            <p style="margin-left:1.6em;">090 消費者、客戶管理與服務</p>
            <p style="margin-left:1.6em;">098 商業與技術資訊</p>
            <p style="margin-left:1.6em;">127 募款（包含公益勸募</p>
            <p style="margin-left:1.6em;">129 會計與相關服務</p>
            <p style="margin-left:1.6em;">135 資（通）訊服務</p>
            <p style="margin-left:1.6em;">136 資（通）訊與資料庫管理</p>
            <p style="margin-left:1.6em;">137 資通安全與管理</p>
            <p style="margin-left:1.6em;">148 網路購物及其他電子商務服務157 調查、統計與研究分析</p>
            <p style="margin-left:1.6em;">除上述項目外，其他符合17Life營業登記項目或章程所定得合法經營之業務目的。</p>
          </P>
          <P>
            3. 蒐集的個人資料類別：
            <br>
            <p style="margin-left:1.6em;">C001 辨識個人者。如姓名、地址、室內電話號碼、行動電話、即時通帳號、網路平臺申請之帳號、通訊及戶籍地址、電子郵遞地址、提供網路身分認證或申辦查詢服務之紀錄及其他任何可辨識資料本人者等。</p>
            <p style="margin-left:1.6em;">C002 識財務者。如金融機構帳戶之號碼與姓名、信用卡或簽帳卡之號碼、個人之其他號碼或帳戶等。</p>
            <p style="margin-left:1.6em;">C003 政府資料中之辨識者。如身分證統一編號、護照號碼等。</p>
            <p style="margin-left:1.6em;">C011 個人描述。如年齡、性別、出生年月日、出生地、國籍、聲音等。</p>
            <p style="margin-left:1.6em;">C012 身體描述。如身高、體重、血型等。</p>
            <p style="margin-left:1.6em;">C035 休閒活動及興趣。如嗜好、運動及其他興趣等。</p>
            <p style="margin-left:1.6em;">C036 生活格調。如使用消費品之種類及服務之細節、個人之消費模式等。</p>
            <p style="margin-left:1.6em;">C093 財務交易。如收付金額、支付方式、往來紀錄等。</p>
            <p style="margin-left:1.6em;">C102 約定或契約。如關於交易、商業、法律或其他契約、代理等。</p>
            <p style="margin-left:1.6em;">除上述類別外，其他詳如17Life網站資料欄位或相關申請書、同意書或契約文件。</p>
          </P>

          <P>
            4. 個人資料利用的期間、地區、對象及方式：
            <br>
            <p style="text-indent:-1.6em; margin-left:3.2em;">(1) 期間：自您加入為17Life網站會員之日起，至您或17Life終止會員契約之日止；個人資料蒐集目的消失或17Life執行業務所須保存之期間。(以上述各期間較長者為準，但法令另有規定者，依其規定。)
            <p style="text-indent:-1.6em; margin-left:3.2em;">(2) 地區：中華民國地區及17Life相關業務合作夥伴(含供貨商、物流商及其他履行輔助人或代理人)所在地區與提供服務地區。</p>
            <p style="text-indent:-1.6em; margin-left:3.2em;">(3) 對象：康太數位整合股份有限公司及17Life為提供會員服務所必要之相關業務合作夥伴或履行輔助人、政府機關等。</p>
            <p style="text-indent:-1.6em; margin-left:3.2em;">(4) 方式：以電子、書面、電話、電信、網際網路及其他方式於蒐集之特定目的範圍內處理並利用個人資料。</p>
          </P>
          
          <P>
            5. 您可向17Life主張之權利種類及行使方式：
            <p style="text-indent:-1.6em; margin-left:3.2em;">(1) 權利種類：</p>
            <p style="text-indent:-1.6em; margin-left:4.8em;">a.查詢或請求閱覽</p>
            <p style="text-indent:-1.6em; margin-left:4.8em;">b.製給複製本</p>
            <p style="text-indent:-1.6em; margin-left:4.8em;">c.補充或更正</p>
            <p style="text-indent:-1.6em; margin-left:4.8em;">d.請求停止蒐集、處理或利用。</p>
            <p style="text-indent:-1.6em; margin-left:4.8em;">e.請求刪除。</p>
            <p style="text-indent:-1.6em; margin-left:3.2em;">(2) 行使方式：</p>
            <p style="text-indent:-1.6em; margin-left:4.8em;">a.來電17Life客服中心 <%=config.ServiceTel %> 提出申請。</p>
            <p style="text-indent:-1.6em; margin-left:4.8em;">b.得以會員帳號密碼登入17Life網站由您利用客服系統提出申請。</p>
            <p style="text-indent:-1.6em; margin-left:3.2em;">(3) 17Life將依據您提出可資確認的身分證明文件申請及所需的事實釋明，履行上述義務，但法令另有規定，或17Life依法執行業務所須保存或利用您的個人資料者，不在此限。若您委託他人代為申請者，請另外提出可資確認的身分及授權證明，以備查核。若您申請查詢或請求閱覽個人資料或製給複製本，17Life得酌收相當費用。收費將依17Life所規定的標準辦理。若17Life未能於法定期間內處理您的申請，將會將原因以書面方式告知使您知悉。您亦可在17Life網站的「會員中心」網頁登入您的帳號及密碼，線上即時查閱您的個人資料檔案。若您委託他人代為登入者，您將負完全的責任。</p>
          </P>

          <P>
            6. 您有自由選擇提供個人資料之權利，但若不提供時，您將不能使用17Life網站之全部或部分的個人化服務或加值服務，亦無從使17Life履行對您的契約相關權利或義務。
          </P>
          <br>
          <P> <b>二、17Life網站隱私權保護政策如下：</b>
          </P>
          <P style="text-indent:-1.6em; margin-left:1.6em;">
            1. 您於17Life網站所登錄或留存的個人資料，係由17Life網站蒐集、處理、利用，在未獲得您的同意以前，絕不對17Life或相關業務合作夥伴(以下稱「17Life相關業務合作夥伴」)以外的第三人任意揭露您的姓名、地址、電子郵件地址及其他依法受保護的個人資料。</p>
          </P>
          <P style="text-indent:-1.6em; margin-left:1.6em;">
            2. 為提供本服務行銷、市場分析、統計或研究、或為提供會員個人化服務或加值服務之目的，您同意本公司及本公司相關業務夥伴，得記錄、保存及利用您在本網站所留存或產生的資料及記錄，並得將您的資料及記錄提供予相關業務夥伴，同時在不揭露各該資料的情形下，得公開或使用統計資料。
          </P>
          <P style="text-indent:-1.6em; margin-left:1.6em;">
            3. 17Life網站會記錄使用者上站的IP位址、及在網站內瀏覽活動等資料，但這些資料僅供17Life及17Life相關業務夥伴內部作網站流量和網路行為調查的總量分析，利於提昇網站的服務品質，同時在不揭露各該資料的情形下，公開或使用統計資料。
          </P>
          <P style="text-indent:-1.6em; margin-left:1.6em;">
            4. 除符合下列情形之一，17Life不得將個人資料進行前述特定目的外之利用：
            <p style="text-indent:-1.6em; margin-left:3.2em;">一、法律明文規定；</p>
            <p style="text-indent:-1.6em; margin-left:3.2em;">二、為增進公共利益；</p>
            <p style="text-indent:-1.6em; margin-left:3.2em;">三、為免除當事人之生命、身體、自由或財產上之危險；</p>
            <p style="text-indent:-1.6em; margin-left:3.2em;">四、為防止他人權益之重大危害；</p>
            <p style="text-indent:-1.6em; margin-left:3.2em;">五、經當事人書面同意。</p>
          </P>
          <P style="text-indent:-1.6em; margin-left:1.6em;">
            5. 為了便利使用者的網頁瀏覽及操作，17Life網站可能使用cookie技術或其他類似技術，以提供更適合使用者個人需要的服務。cookie是網站伺服器用來和使用者瀏覽器進行溝通的一種技術，它可能在使用者的電腦中儲存某些資訊。使用者可以經由瀏覽器的設定，取消或限制此項功能。
          </P>
          <P style="text-indent:-1.6em; margin-left:1.6em;">
            6. 為了保障你的隱私與安全，您的帳號資料由17Life網站現行通行標準的SSL保全系統，保障資料傳送的安全。
          </P>
          <P style="text-indent:-1.6em; margin-left:1.6em;">
            7. 17Life相關網站或網頁可能包含其他網站或網頁的連結。對於此等不屬於17Life網站的網站或網頁，其內容或隱私權政策，均與17Life網站無關。 
          </P>
          <P style="text-indent:-1.6em; margin-left:1.6em;">
            8. 17Life於個人資料於蒐集、處理及利用時，皆尊重當事人之權益，依誠實及信用方法為之，且在被規範之特定目的範圍內，最小化處理及利用個人資料。 
          </P>
          <P style="text-indent:-1.6em; margin-left:1.6em;">
            9. 使用者若對於本政策，或與個人資料有關的其他事項，可與17Life客服中心聯絡，我們將會提供最完整的說明。 
          </P>
          <P style="text-indent:-1.6em; margin-left:1.6em;">
            10.17Life將會視需要或法令異動，隨時修改本政策，以落實保障您個人資料及隱私權的立意。當我們完成本政策條款修改時，將立即刊登於本網站上，請您隨時注意前往點選詳閱，若屬於重大的變更，我們會另外在網站上醒目標示或是以個別E-mail提醒您注意點選閱讀。
          </P>
          <P style="margin-left:1.6em;">願您有個愉快、豐富的網站之旅！</P>
          <br>
          <p style="float:right">17Life敬上</p>
          <br>

        </div><!--div style-->
        </div><!--MemberPrivacy-->

    </div>
  </div>   
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfirmMember.aspx.cs" Inherits="LunchKingSite.Web.NewMember.ConfirmMember"
    MasterPageFile="~/NewMember/NewMember.master" ClientIDMode="static" %>

<%@ MasterType VirtualPath="~/NewMember/NewMember.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href="../Themes/PCweb/css/MasterPage.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/default/style/RDL.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" media="only screen and (min-width: 481px) and (max-width: 767px)" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" media="only screen and (max-width: 480px)" />
    <script type="text/javascript" src="../Tools/js/completer.js"></script>
    <link href="../Tools/js/css/completer.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        input[type=checkbox]
        {
            top: 0px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("#mailMsg").html("請提供您的e-mail，輕鬆成為17Life的正式會員");
            $("#mailbox").focus(function () {
                $("#lineMail").removeClass("PezFbFillInColor");
                $("#mailMsg").html("請提供您的e-mail，輕鬆成為17Life的正式會員");
            });
            $("#LifeMemberBtn").click(function () {
                if (checkMail()) {
                    $('form').trigger('submit');
                }
            });

            preventDobuleSubmit();

            $("#mailbox").completer({
                separator: "@",
                source: window.EmailDomainSource,
                itemSize: 7
            });

            $('select,:text,:checkbox').focus(function () {
                $('#lineMail').find('.enter-error').find('p').text('請使用字母、數字及英文句點，如17life@example.com');
                $(this).closest('.form-unit').removeClass('error').find('.enter-error').hide();
            });
        });

        function checkMail() {
            var mail = $("#mailbox").val();
            var city = $('#ddlCity').val();
            var check = true;
            if (checkAccount(mail)==false) {               
                $('#lineMail').addClass('error').find('.enter-error').show();
                check = false;
            }

            if (city == '0') {
                $('#lineCity').addClass('error').find('.enter-error').show();
                check = false;
            }

            if ($("#chkAgree").is(':checked')==false) {
                $('#lineAgree').addClass('error').find('.enter-error').show();
                check = false;
            }
            return check;
        }

        function wrongEmail(msg) {
            if (msg != null && msg.length > 0) {
                $('#lineMail').find('.enter-error').find('p').text(msg);
            }
            $('#lineMail').addClass('error').find('.enter-error').show();
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="LifeMemberFrame">
    <div class="LifeMember">
        <div class="LifeMemberTopBlack rd-fd-sn">
        <div class="LifeMemberTopText">歡迎您註冊17Life！</div>
        </div>
        <!---->
        <div class="LifeMemberCenter">
        <div class="LMPasswordFrame rd-fd-sn">

            <div class="grui-form">
            <div class="form-unit" id="lineMail">
                <label for="" class="unit-label">電子信箱</label>
                <div class="data-input">
                <input id="mailbox" type="text" runat="server" class="input-50per" />
                <input id="fbEmail" type="hidden" runat="server" />
                <p class="login-note">請提供您的e-mail，輕鬆成為17Life的正式會員</p>
                </div>
                <div class="data-input enter-error" style="display:none">
                <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                <p>請使用字母、數字及英文句點，如17life@example.com</p>
                </div>
            </div>

            <div class="form-unit" id="lineCity">
                <label for="" class="unit-label">所在地區</label>
                <div class="data-input">
                <select id="ddlCity" runat="server" class="select-wauto"></select>
                <asp:CheckBox ID="chkReceiveEdm" runat="server" Checked="true" />
                    <p class="cancel-mr">
                    訂閱EDM
                    </p>
                </div>
                <div class="data-input enter-error" style="display:none">
                <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                <p>請選擇您的所在地區！</p>
                </div>
            </div>
            <div class="form-unit  st-rec-border-none" id="lineAgree">
                <div class="data-input">
                  <label>
                    <asp:CheckBox ID="chkAgree" runat="server" />
                    <p class="cancel-mr">
                    我已閱讀並同意
                    <a href="../newmember/privacypolicy.aspx" target="_blank">服務條款</a>
                    與
                    <a href="../newmember/privacy.aspx" target="_blank">隱私權政策</a>
                  </label>
                    <p style="display: none">
                    <span class="st-rec-warningred">您還未同意隱私權政策</span>
                    </p>
                </div>
                <div class="data-input enter-error" style="display:none">
                  <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                  <p>您是否已經閱讀並同意17Life的各項條款？</p>
                </div>
           </div>

                <div class="form-unit end-unit rd-pay-pos-fix btn-align-center">
                <div class="data-input rd-pay-span-ctr btn-center">
                    <p class="login">
                    <input type="button" class="btn btn-large btn-primary rd-payment-xlarge-btn" id="LifeMemberBtn" value="確定">
                    </p>
                </div>
                </div>
            </div>

            </div>
            <!--<div class="LifeMemberBottom"></div>
        -->
        </div>
    </div>
    </div>
</asp:Content>

﻿using System;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Component.MemberActions;

namespace LunchKingSite.Web.NewMember
{
    public partial class Login : BasePage
    {
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private IOAuthProvider oap = ProviderFactory.Instance().GetProvider<IOAuthProvider>();
        #region properties

        public string DefaultUser
        {
            get
            {
                return Request["def"] ?? string.Empty;
            }
        }

        public string PreviousUrl
        {
            get
            {
                string theUrl = (string)Session[LkSiteSession.NowUrl.ToString()];
                return ResolveUrl(string.IsNullOrEmpty(theUrl) ? config.SiteUrl : theUrl);
            }
        }

        public string PezLoginUrl = "";

        public string PezMobileLoginUrl
        {
            get
            {
                return string.Format("{0}/login?mode=mobile&service={1}/NewMember/PezLogin.aspx", config.PayEasyCasServerUrl, config.SiteUrl.ToLower().Replace("http://", "https://")).Replace("//login", "/login").Replace("//NewMember", "/NewMember");
            }
        }

        public string RegisterUrl = "";
        public string AcsCode = string.Empty;
        private string _acsCode = string.Empty;

        private string _returnUrl;
        public string ReturnUrl
        {
            get
            {
                string returnUrl = string.IsNullOrEmpty(_returnUrl)
                    ? config.SiteUrl + "/newmember/fbauth.ashx"
                    : _returnUrl;

                return ResolveUrl(returnUrl);
            }
        }

        public string FacebookApplicationId
        {
            get { return config.FacebookApplicationId; }
        }

        public string Scope = FacebookUser.DefaultScope;

        private string AppKey
        {
            get { return (string)Session[LkSiteSession.OAuthAppKey.ToString()]; }
            set { Session[LkSiteSession.OAuthAppKey.ToString()] = value; }
        }

        public string SessionId = LkSiteSession.CaptchaLogin.ToString();

        public int SessionPasswordErrorTimes
        {
            get
            {
                if (Session["PasswordErrorTimes"] == null)
                {
                    return 0;
                }
                int errortime;
                if (int.TryParse(Session["PasswordErrorTimes"].ToString(), out errortime))
                {
                    return errortime;
                }
                return 0;
            }
            set { Session["PasswordErrorTimes"] = value; }
        }

        private string CaptchaResponse
        {
            get
            {
                return Request["captchaResponse"];
            }
        }


        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            string redirectUrl = config.SSLSiteUrl + config.LoginUrl;

            if (Request["next_url"] != null)
            {
                redirectUrl += Request.Url.Query;
            }
            else if (Request["ReturnUrl"] != null)
            {
                redirectUrl += Request.Url.Query;
            }

            Response.Redirect(redirectUrl);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("/m/login");
            return;
        }

        private string GetNextUrl()
        {
            string url = string.Format(
                "OAuthAllowClient.aspx?app_key={0}&next_url={1}&cancel_url={2}",
                Request["app_key"],
                HttpUtility.UrlEncode(Request["next_url"]),
                HttpUtility.UrlEncode(Request["cancel_url"]));
            return url;
        }

        private string CheckData(string email, string password)
        {
            string result = string.Empty;
            if (string.IsNullOrWhiteSpace(email))
            {
                result += "emptyEmail();";
            }
            else
            {
                if (RegExRules.CheckEmail(email) == false)
                {
                    if (RegExRules.CheckMobile(email) == false)
                    {
                        result += "wrongEmail();";
                    }
                }
                if (string.IsNullOrWhiteSpace(password))
                {
                    result += "noPassword();";
                }
            }

            return result;
        }

        private void SpecialConsumingDiscountSet(string email)
        {
            if (config.IsEnabledSpecialConsumingDiscount)
            {
                DateTime dateStart = DateTime.Parse(config.SpecialConsumingDiscountPeriod.Split('|')[0]);
                DateTime dateEnd = DateTime.Parse(config.SpecialConsumingDiscountPeriod.Split('|')[1]);
                if (DateTime.Now > dateStart && DateTime.Now < dateEnd)
                {
                    if (Session[LkSiteSession.SpecialConsumingDiscount.ToString()] == null)
                    {
                        Session[LkSiteSession.SpecialConsumingDiscount.ToString()] = PromotionFacade.GetSpecialConsumingAmount(MemberFacade.GetUniqueId(email));
                    }
                }
            }
        }
        private void RedirectUserOrConfirmOAuthPermission(string userName)
        {
            if (AppKey != null)
            {
                //check client valid
                OauthClient client = oap.ClientGet(AppKey);
                if (client.IsLoaded == false)
                {
                    Response.Redirect("OAuthLoginFail.aspx");
                    return;
                }

                if (OAuthFacade.ClientHasUser(client.Id, userName) == false)
                {
                    Response.Redirect(GetNextUrl());
                    return;
                }
            }

            if (!string.IsNullOrWhiteSpace(Request["ReturnUrl"]))
            {
                Response.Redirect(Request["ReturnUrl"]);
            }
            else
            {
                Response.Redirect(PreviousUrl);
            }
        }
    }
}
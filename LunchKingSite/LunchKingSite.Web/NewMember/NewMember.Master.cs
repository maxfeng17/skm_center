﻿using System;
using System.Web.UI;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib;
using System.Text.RegularExpressions;

namespace LunchKingSite.Web.NewMember
{
    public partial class NewMember : MasterPage
    {
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public ISysConfProvider SystemConfig
        {
            get { return cp; }
        }

        protected void Page_Load(object sender, EventArgs e)
        { }
    }
}
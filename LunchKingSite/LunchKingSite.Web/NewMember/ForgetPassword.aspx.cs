﻿using System;
using log4net;
using LunchKingSite.Core.UI;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.WebLib.Component;
using System.Collections;
using System.Collections.Generic;

namespace LunchKingSite.Web.NewMember
{
    public partial class ForgetPassword : BasePage
    {
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(ForgetPassword).Name);
        protected string SessionId = LkSiteSession.CaptchaForgetPassword.ToString();

        private string CaptchaResponse
        {
            get
            {
                return Request["captchaResponse"];
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            phInputAccount.Visible = false;
            phNotifyWay.Visible = false;
            phNotifyResult.Visible = false;
            phSmsNotiy.Visible = false;

        }

        public string VbsAccount{
            get
            {
                string encryMail = Request["vbsaccount"] ?? string.Empty;
                return string.IsNullOrEmpty(encryMail) ? "" : Helper.Decrypt(encryMail);           
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                phInputAccount.Visible = true;
                txtEmail.Value = Request["account"] ?? string.Empty;
                if (config.IsEnableVbsBindAccount)
                {
                    if (!string.IsNullOrEmpty(VbsAccount))
                    {
                        txtEmail.Value = VbsAccount;
                        btnNext_Click(null, null);
                    }
                }                
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            string email;
            string mobile;
            int userId;
            string account = txtEmail.Value;
            string script = string.Empty;
            if (string.IsNullOrEmpty(VbsAccount))
            {
                //一般忘記密碼
                script = CheckAccount(account, false, out email, out mobile, out userId);
            }
            else
            {
                //商家導頁，跳過驗證碼
                script = CheckAccount(account, true, out email, out mobile, out userId);
            }
            
            if (!string.IsNullOrWhiteSpace(script))
            {
                ClientScript.RegisterStartupScript(GetType(), "noMail",
                    "$(function(){$('#txtEmail').val('" + account + "');" + script + "});", true);
                phInputAccount.Visible = true;
            }
            else
            {
                phNotifyByEmail.Visible = false;
                phNotifyByMobile.Visible = false;

                hidUserId.Value = userId.ToString();
                IList<SingleSignOnSource> ssoSourceList = GetSingleSignOnSoureceList(userId);

                if (!ssoSourceList.Contains(SingleSignOnSource.ContactDigitalIntegration) &&
                    !ssoSourceList.Contains(SingleSignOnSource.Mobile17Life))
                {
                    phInputAccount.Visible = true;
                    if (ssoSourceList.Contains(SingleSignOnSource.PayEasy))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "showIsOtherSiteMember", "showIsOtherSiteMember(\"payeasy\");", true);
                    }
                    else if (ssoSourceList.Contains(SingleSignOnSource.Facebook))
                    {
                        ClientScript.RegisterStartupScript(GetType(), "showIsOtherSiteMember", "showIsOtherSiteMember(\"facebook\");", true);
                    }
                }
                else
                {
                    phInputAccount.Visible = false;
                    phNotifyWay.Visible = true;

                    if (ssoSourceList.Contains(SingleSignOnSource.ContactDigitalIntegration))
                    {
                        litEmailDisplay.Text = Helper.MaskEmail(email);
                        phNotifyByEmail.Visible = true;
                    }
                    if (ssoSourceList.Contains(SingleSignOnSource.Mobile17Life))
                    {
                        litMobileDisplay.Text = Helper.MaskMobile(mobile);
                        phNotifyByMobile.Visible = true;
                    }
                }
            }
        }

        private string CheckAccount(string account, bool passCaptcha, out string email, out string mobile, out int userId)
        {
            email = string.Empty;
            mobile = string.Empty;
            userId = 0;
            string script = "";
            bool isForAutoTest = config.AccountForAutoTest.Equals(account) && config.GeneralForgotPasswordCaptcha.Equals(CaptchaResponse);

            if (!isForAutoTest && passCaptcha)
            {
                isForAutoTest = true;
            }

            //是自動化測試的帳號，且是測試用的萬用驗證碼的話，跳過圖形驗證碼的檢查
            if (!isForAutoTest)
            {
                if (Session[SessionId] == null ||
                Session[SessionId].ToString().Equals(CaptchaResponse, StringComparison.OrdinalIgnoreCase) == false)
                {
                    script += "wrongCaptcha();";
                }
            }

            if (string.IsNullOrWhiteSpace(account))
            {
                script += "showNoMail();";
            }
            else
            {

                MobileMember mm;
                Member mem;
                if (RegExRules.CheckEmail(account))
                {
                    mem = MemberFacade.GetMember(account);
                    if (mem.IsLoaded)
                    {
                        mm = MemberFacade.GetMobileMember(mem.UniqueId);
                        email = mem.UserEmail;
                        mobile = mm.MobileNumber;
                        userId = mem.UniqueId;
                    }
                    else
                    {
                        script += "showNoMail();";
                    }
                }
                else if (RegExRules.CheckMobile(account))
                {
                    mm = MemberFacade.GetMobileMember(account);
                    if (mm.IsLoaded)
                    {
                        mem = MemberFacade.GetMember(mm.UserId);
                        email = mem.UserEmail;
                        mobile = mm.MobileNumber;
                        userId = mm.UserId;
                    }
                    else
                    {
                        script += "showNoMail();";
                    }
                }
                else
                {
                    script += "showNoMail();";
                }
            }
            return script;
        }

        protected void btnNotify_Click(object sender, EventArgs e)
        {
            hidNotifyType.Value = Request["notifyType"];
            Notify();
        }

        protected void btnNotifyAgain_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "startupScript", "alert('已重新發送信件，請再次檢查信箱，可能在您的垃圾信件中')", true);
            Notify();
        }

        private void Notify()
        {
            int userId;
            if (int.TryParse(hidUserId.Value, out userId) == false)
            {
                phInputAccount.Visible = true;
                return;
            }

            string notifyType = hidNotifyType.Value;
            IList<SingleSignOnSource> ssoSourceList = GetSingleSignOnSoureceList(userId);

            if (notifyType == "byEmail" && ssoSourceList.Contains(SingleSignOnSource.ContactDigitalIntegration))
            {
                SendForgetMail(userId);
                phNotifyResult.Visible = true;
            }
            else if (notifyType == "byMobile" && ssoSourceList.Contains(SingleSignOnSource.Mobile17Life))
            {
                MobileMember mm = MemberFacade.GetMobileMember(userId);
                string mobile = mm.MobileNumber;
                txtMobile.Value = mobile;
                hidMobile.Value = mobile;
                phSmsNotiy.Visible = true;
                SendSms(mobile);
            }
        }

        protected void btnReSms_Click(object sender, EventArgs e)
        {
            string mobile = hidMobile.Value;
            txtMobile.Value = hidMobile.Value;
            phSmsNotiy.Visible = true;
            SendSms(mobile);
        }

        protected void btnValidateCode_Click(object sender, EventArgs e)
        {
            string code = Request["code"] ?? "";
            string mobile = hidMobile.Value;

            int userId;
            if (int.TryParse(hidUserId.Value, out userId))
            {
                string authUrl = GetAuthUrlByCode(userId, mobile, code);
                if (String.IsNullOrEmpty(authUrl) == false)
                {
                    Response.Redirect(authUrl);
                    return;
                }
            }

            txtMobile.Value = hidMobile.Value;
            phSmsNotiy.Visible = true;
            ShowErrorMessage("認證碼輸入有誤，麻煩您重新輸入");

        }

        private string GetAuthUrlByCode(int userId, string mobile, string code)
        {
            bool needSetPassword;
            string resetPasswordKey;
            MemberValidateMobileCodeReply result = MemberFacade.ValidateMobileCode(userId, mobile, code, true,
                out needSetPassword, out resetPasswordKey);
            if (result != MemberValidateMobileCodeReply.Success && result != MemberValidateMobileCodeReply.ActivedAndSkip)
            {
                return string.Empty;
            }
            MemberAuthInfo mai = mp.MemberAuthInfoGet(userId);
            return MemberUtility.GetMemberAuthUrl(mai);
        }

        private IList<SingleSignOnSource> GetSingleSignOnSoureceList(int userId)
        {
            IList<SingleSignOnSource> ssoSourceList = new List<SingleSignOnSource>();
            MemberLinkCollection mlCol = mp.MemberLinkGetList(userId);

            foreach (MemberLink ml in mlCol)
            {
                if (Enum.IsDefined(typeof(SingleSignOnSource), ml.ExternalOrg))
                {
                    ssoSourceList.Add((SingleSignOnSource)ml.ExternalOrg);
                }
            }

            return ssoSourceList;
        }

        private bool SendForgetMail(int userId)
        {
            Member mem = MemberFacade.GetMember(userId);
            if (mem.IsLoaded == false)
            {
                return false;
            }
            try
            {
                MemberAuthInfo mai = MemberFacade.GetOrAddMemberAuthInfo(mem.UniqueId, mem.UserName);
                MemberFacade.ResetMemberAuthInfo(mai);
                MemberFacade.SendForgetPasswordMail(
                    mem.UserEmail, mem.UniqueId.ToString(), mai.ForgetPasswordKey, mai.ForgetPasswordCode);
                return true;
            }
            catch (Exception ex)
            {
                logger.Error("SendForgetMail error. userId=" + mem.UniqueId, ex);
                return false;
            }
        }

        private void SendSms(string mobile)
        {
            SendMobileAuthCodeReply result = MemberUtility.SendMobileAuthCode(mobile);
            ShowErrorMessage(result);
        }

        private void ShowErrorMessage(SendMobileAuthCodeReply errReply)
        {
            switch (errReply)
            {
                case SendMobileAuthCodeReply.MobileError:
                    ShowErrorMessage("您輸入的手機格式有誤，請輸入半形數字，Ex:0922171717");
                    break;
                case SendMobileAuthCodeReply.Success:
                    phError.Visible = false;
                    break;
                default:
                    ShowErrorMessage("發生錯誤，請稍後再試");
                    break;
            }
        }

        private void ShowErrorMessage(string message)
        {
            phError.Visible = true;
            litErrorMessage.Text = message;
        }
    }
}
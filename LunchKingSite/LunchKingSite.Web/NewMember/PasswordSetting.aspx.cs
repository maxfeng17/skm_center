﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Component;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.Web.NewMember
{
    public partial class PasswordSetting : BasePage
    {
        IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        #region properties

        public string Password
        {
            get
            {
                return HttpUtility.UrlDecode(Request["txtPassword"]);
            }
        }

        public int UserId
        {
            get
            {
                int userId;
                int.TryParse(Request["uid"], out userId);
                return userId;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                bool checkResult = CheckUser();
                if (checkResult)
                {
                    Regex re = new Regex(@"^[0-9a-zA-Z]{6,12}$");
                    if (!re.Match(Password).Success)
                    {
                        ClientScript.RegisterStartupScript(GetType(), "alerterR", "$(\"#spanPassword\").html('<span style=\"color: #F00; letter-spacing: normal;\">密碼格式錯誤！</span>');$(\"#linePassword\").removeClass(\"LMPasswordIn\").addClass(\"LMPasswordInColor\");", true);
                    }
                    else
                    {
                        ResetPassword();
                        ClientScript.RegisterStartupScript(GetType(), "alerterR", "alert('密碼修改成功！請重新登入。');window.location.href='" + 
                            WebUtility.GetLoginPath() + "';", true);
                    }
                }
                else
                {
                    Response.Redirect("LinkError.aspx");
                }
            }
        }

        private bool CheckUser()
        {
            string authKey = Request["key"];
            string authCode = Request["code"];

            if (int.Equals(0, UserId) || string.IsNullOrWhiteSpace(authKey) || string.IsNullOrWhiteSpace(authCode))
            {
                return false;
            }

            MemberAuthInfo mai = mp.MemberAuthInfoGet(UserId);
            if (mai != null && mai.IsLoaded)
            {
                if (string.Equals(mai.ForgetPasswordKey, authKey) && string.Equals(mai.ForgetPasswordCode, authCode))
                {
                    return true;
                }
            }

            return false;
        }

        private void ResetPassword()
        {
            Member m = mp.MemberGet(UserId);
            if (m != null && m.IsLoaded)
            {
                MemberFacade.ResetMemberPassword(m.UserName, Password, ResetPasswordReason.MemberForgot);

                MemberAuthInfo mai = mp.MemberAuthInfoGet(m.UniqueId);
                mai.ForgetPasswordCode = null;
                mai.ForgetPasswordKey = null;
                mai.PasswordQueryTime = null;
                mp.MemberAuthInfoSet(mai);
            }
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Life17Bind.aspx.cs" Inherits="LunchKingSite.Web.NewMember.Life17Bind"
    MasterPageFile="~/NewMember/NewMember.master" ClientIDMode="Static" %>

<%@ MasterType VirtualPath="~/NewMember/NewMember.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href="../Themes/PCweb/css/MasterPage.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/default/style/RDL.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/PCweb/plugin/font-awesome/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" media="only screen and (min-width: 481px) and (max-width: 767px)" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" media="only screen and (max-width: 480px)" />
    <script type="text/javascript" src="../Tools/js/completer.js"></script>
    <link href="../Tools/js/css/completer.css" rel="stylesheet" type="text/css" />
    <style>
        .LifeMemberInfoColor span {
            color: #F00;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            preventDobuleSubmit();

            $("#txtEmail").completer({
                separator: "@",
                source: window.EmailDomainSource,
                itemSize: 7
            });

            $('#btnToLogin').click(function () {
                location.href = '<%=ResolveUrl(FormsAuthentication.LoginUrl+"?p=member17") %>';
            });

            $('select,input').focus(function () {
                $('#lineMail').find('.enter-error').find('p').text('請使用字母、數字及英文句點，如17life@example.com');
                $(this).closest('.form-unit').removeClass('error').find('.enter-error').hide();
            });

            $('#txtEmail').focus();
        });


        function validateAll() {
            $(".form-unit").removeClass("error").find('.enter-error').hide();
            if (validateEmail() == false || validatePasswordFormat() == false || validatePasswordMatch() == false) {
                return false;
            }
            return true;
        }

        function validatePasswordFormat() {
            if (passwordCheck($('#txtPassword').val()) == false) {
                wrongPassword();
                return false;
            }
            return true;
        }

        function validatePasswordMatch() {
            if ($('#txtPassword').val() != $('#txtConPassword').val()) {
                passwordNotMatch();
                return false;
            }
            return true;
        }

        function validateEmail() {
            if (checkAccount($("#txtEmail").val()) == false) {
                wrongEmail();
                return false;
            }
            return true;
        }

        function wrongEmail(msg) {
            $("#lineMail").addClass("error");
            $("#lineMail").find('.enter-error').show();
            if (msg != null && msg.length > 0) {
                $("#lineMail").find('.enter-error').find('p').text(msg);
            }
        }

        function wrongPassword() {
            $("#linePassword").addClass("error");
            $("#linePassword").find('.enter-error').show();
        }

        function passwordNotMatch() {
            $("#lineConPassword").addClass("error");
            $("#lineConPassword").find('.enter-error').show();
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <div class="LifeMemberFrame" id="divDefault" runat="server">
        <div class="LifeMember">
            <div class="LifeMemberTopBlack rd-fd-sn">
                <div class="LifeMemberTopText">設定17Life帳號及密碼，方便快速登入</div>
            </div>            
            <div class="LifeMemberCenter">
                <div class="LMPasswordFrame rd-fd-sn">

                    <div class="grui-form">
                        <div class="form-unit" id="lineMail">
                            <label for="" class="unit-label">您的帳號</label>
                            <div class="data-input">
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="input-50per" />
                                <!--1級會員-->
                                <p class="login-note">
                                    請確認您的Email，此為17Life的登入帳號
                                </p>
                            </div>
                            <div class="data-input enter-error" style="display: none">
                                <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                <p>
                                    請使用字母、數字及英文句點，如17life@example.com
                                </p>
                            </div>
                        </div>
                        <asp:PlaceHolder ID="phPassword" runat="server">
                        <div class="form-unit" id="linePassword">
                            <label for="" class="unit-label">登入密碼</label>
                            <div class="data-input">
                                <input type="password" id="txtPassword" name="txtPassword" class="input-50per">
                                <p class="login-note">
                                    6~12碼，英文或數字，英文請區分大小寫
                                </p>
                            </div>
                            <div class="data-input enter-error" style="display: none">
                                <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                <p>密碼格式錯誤，請輸入6~12碼英文或數字</p>
                            </div>
                        </div>
                        <div class="form-unit" id="lineConPassword">
                            <label for="" class="unit-label">確認密碼</label>
                            <div class="data-input">
                                <input type="password" id="txtConPassword" name="txtConPassword" class="input-50per">
                                <p class="login-note">
                                    請重新再輸入一次密碼
                                </p>
                            </div>
                            <div class="data-input enter-error" style="display: none">
                                <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                <p>您輸入的密碼不符合，請重新輸入</p>
                            </div>
                        </div>
                        </asp:PlaceHolder>
                        <div class="form-unit end-unit rd-pay-pos-fix btn-align-center">
                            <div class="data-input rd-pay-span-ctr btn-center">
                                <p class="login">
                                    <asp:Button ID="btnNext" Text="確定" runat="server" CssClass="btn btn-large btn-primary rd-payment-xlarge-btn" OnClientClick="if (validateAll() == false) {return false;}" OnClick="btnNext_Click" />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="LifeMemberFrame" id="divResult" runat="server" visible="false">
        <div class="LifeMember">
            <div class="LifeMemberTopBlack rd-fd-sn">
                <div class="LifeMemberTopText">設定成功！</div>
            </div>
            <div class="LifeMemberCenter">
                <div class="LMPasswordFrame text-center rd-fd-sn">
                    <div class="grui-form">
                        <div class="form-unit">
                            <i class="fa fa-check-circle fa-lg green-color"></i>
                            <p class="note" style="display: inline-block;">
                                您已設定成功，馬上使用17Life帳號登入會員！
                            </p>
                        </div>
                    </div>
                    <div class="form-unit end-unit rd-pay-pos-fix">
                        <div class="data-input rd-pay-span-ctr">
                            <p class="login">
                                <input type="button" class="btn btn-large btn-primary rd-payment-xlarge-btn" value="登入" id="btnToLogin">
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PasswordSent.aspx.cs" Inherits="LunchKingSite.Web.NewMember.PasswordSent"
    MasterPageFile="~/NewMember/NewMember.master" %>

<%@ MasterType VirtualPath="~/NewMember/NewMember.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href='<%= ResolveUrl("../Themes/default/style/RDL.css") %>' rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="LifeMemberFrame">
        <div class="LifeMember">
            <div class="LifeMemberTopBlack">
                <div class="LifeMemberTopText">請檢查信箱，以進行密碼設定</div>
            </div>
            <div class="LifeMemberCenter">
                <div class="LMInfo">
                    我們已發送"設定密碼連結"至以下信箱，請檢查您的信件，此連結24小時內有效。<br />
                    <span class="LifeMemberNote"><%=forgetPasswordMail %></span><br />
                    <a class="resent-mail" href="#">我沒收到信件，請重新發送</a><br /><br />
                    <p class="info btn-center">
                        <input type="button" class="btn btn-large btn-primary rd-payment-xlarge-btn" value="回登入頁" onclick="location.href = '<%=ResolveUrl(FormsAuthentication.LoginUrl) %>'">
                    </p>
                </div> 
            </div>
        </div>
    </div>
</asp:Content>

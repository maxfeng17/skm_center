﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Web.SessionState;
using log4net;
using log4net.Core;

namespace LunchKingSite.Web.NewMember
{
    /// <summary>
    /// Summary description for FbAuth
    /// </summary>
    public class FbAuth : IHttpHandler, IRequiresSessionState
    {
        ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
        private ILog logger = LogManager.GetLogger(typeof(FbAuth));
        public string ReturnCode
        {
            get
            {
                return (string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["code"])) ? null : HttpContext.Current.Request.QueryString["code"];
            }
        }
        public string RedirectUri = "/newmember/fbauth.ashx";

        public void ProcessRequest(HttpContext context)
        {
            if (!string.IsNullOrEmpty(ReturnCode))
            {
                string AccessToken = GetAccessToken();
                string responseUrl = "~/user/sso.aspx?atk=" + AccessToken;
                if (context.Request.Url.Query.Contains("state")) //檢查是否有accessCode
                {
                    string str = context.Request.Url.Query;
                    int keyWord = str.LastIndexOf("state"); 
                    string acsCode = str.Substring(keyWord);
                    acsCode = acsCode.Substring(acsCode.IndexOf("="));
                    responseUrl += "&accessCode" + acsCode;
                    responseUrl += "&source=" + (int)SingleSignOnSource.Facebook;
                }
                
                context.Response.Redirect(responseUrl);
            }
        }
       
        private string GetAccessToken()
        {
            string url = string.Format("{0}facebook.com/oauth/access_token?client_id={1}&redirect_uri={2}&client_secret={3}&code={4}",
                "https://graph." + (conf.EnableBetaGraphAPI ? "beta." : "")
                , conf.FacebookApplicationId,
                Helper.CombineUrl(conf.SiteUrl, RedirectUri),
                conf.FacebookApplicationSecret,
                ReturnCode);

            return FacebookUtility.GetOAuhToken(url);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
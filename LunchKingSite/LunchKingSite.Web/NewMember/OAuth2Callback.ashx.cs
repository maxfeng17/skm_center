﻿using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.SessionState;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using Newtonsoft.Json.Linq;

namespace LunchKingSite.Web.NewMember
{
    /// <summary>
    /// 經過google驗證身份後，導回平臺的網頁，帶回 code，藉以取得token後，進一步做single sign on.
    /// </summary>
    public class OAuth2Callback : IHttpHandler, IRequiresSessionState
    {
        private static ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
        public void ProcessRequest(HttpContext context)
        {
            string state = context.Request["state"];
            if (state != HttpContext.Current.Session.SessionID)
            {
                context.Response.Redirect("~/user/sso.aspx?ggtk=");
                return;
            }
            string code = context.Request["code"];
            string accessToken = GetAccessToken(code);
            context.Response.Redirect("~/user/sso.aspx?ggtk=" + accessToken);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private string GetAccessToken(string code)
        {
            string url = "https://accounts.google.com/o/oauth2/token";
            string param = string.Format("code={0}&client_id={1}&client_secret={2}&redirect_uri={3}&grant_type=authorization_code",
                    code, conf.GoogleApiKey,conf.GoogleApiSecret, "http://localhost/newmember/oauth2callback.ashx");
            byte[] bs = Encoding.ASCII.GetBytes(param);
            HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = bs.Length;
            using (Stream reqStream = req.GetRequestStream())
            {
                reqStream.Write(bs, 0, bs.Length);
            }
            try
            {
                using (HttpWebResponse response = req.GetResponse() as HttpWebResponse)
                {
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    string retVal = reader.ReadToEnd();

                    dynamic tokens = JObject.Parse(retVal);
                    return tokens.access_token;
                }
            }
            catch
            {
                return null;
            }
        }

    }
}
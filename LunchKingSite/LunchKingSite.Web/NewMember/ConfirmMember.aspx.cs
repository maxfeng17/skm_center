﻿using System;
using System.Linq;
using System.Web.Security;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Component.MemberActions;
using System.Collections.Generic;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.NewMember
{
    public partial class ConfirmMember : BasePage
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private IMGMProvider mgm = ProviderFactory.Instance().GetProvider<IMGMProvider>();
        IMemberProvider mp = ProviderFactory.Instance().GetDefaultProvider<IMemberProvider>();
        public ExternalMemberInfo SsoMemberInfo
        {
            get
            {
                return (ExternalMemberInfo)Session[LkSiteSession.ExternalMemberId.ToString("g")];
            }
        }

        private string Area
        {
            get
            {
                return ddlCity.Value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //尚未登入導到首頁
            if (SsoMemberInfo == null || SsoMemberInfo.Count == 0)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }
            if (IsPostBack == false)
            {
                var items = CityManager.Citys.Where(x => x.Code != "com" && x.Code != "SYS");
                ddlCity.Items.Clear();
                ddlCity.Items.Add(new ListItem { Text = "請選擇", Value = "0" });
                foreach (var item in items)
                {
                    ddlCity.Items.Add(new ListItem { Text = item.CityName, Value = item.Id.ToString() });
                }
                string userName = Helper.DecryptEmail(Request.QueryString["fbm"]);
                mailbox.Value = userName;
                fbEmail.Value = userName;
            }
            else
            {
                string script = string.Empty;
                string userName;
                string userEmail;
                if (mp.MemberGet(fbEmail.Value).IsLoaded == false)
                {
                    userName = fbEmail.Value;
                    userEmail = mailbox.Value;
                }
                else
                {
                    userName = mailbox.Value;
                    userEmail = mailbox.Value;
                }
                bool isValid = ValidateData(userName, out script);
                if (isValid)
                {
                    string errMsg;
                    var res = RegisterNew(userName, userEmail, out errMsg);
                    if (res == MemberRegisterReplyType.RegisterSuccess)
                    {
                        string redirUrl = string.Empty;
                        Member mem = mp.MemberGet(userName);
                        MemberFacade.SendMemberRegisterSuccessMail(mem.UserEmail, mem.UniqueId);
                        NewMemberUtility.UserSignIn(mem.UserName, SsoMemberInfo[0].Source, false);

                        //多筆accessCode，塞通知訊息，Redirect /user/GiftGet
                        if (string.IsNullOrWhiteSpace(Request["accessCode"]) == false)
                        {
                            //List<string> accessCodes = Request["accessCode"].Split(new char[] { '|' }).Distinct().ToList();
                            //foreach (var code in accessCodes)
                            //{
                            //    var giftData = mgm.GiftGetByAccessCode(code).FirstOrDefault();
                            //    if (giftData!=null)
                            //    {
                            //        var giftVersion = mgm.GiftVersionGetByGiftiId(giftData.Id);
                            //        MGMFacade.GiftMessageSet(giftVersion.SenderId, userId, code, GiftMessageType.GiftCode, giftVersion.SendTime);
                            //        MGMFacade.GiftMessageCountSet(userId,(int)GiftMessageType.GiftCode);
                            //    }
                            //}
                        }
                        else
                        {
                            redirUrl = config.SiteUrl;
                        }
                        redirUrl = config.SiteUrl;
                        //redirUrl = string.IsNullOrWhiteSpace(Request["accessCode"])
                        //    ? config.SiteUrl
                        //    : config.SiteUrl + "/user/GiftGet"; //從FB 邀請禮物來

                        Response.Redirect(redirUrl);
                        /*
                        Session[LkSiteSession.Bind17UserId.ToString()] = userId;                        
                        if (SsoMemberInfo[0].Source == SingleSignOnSource.PayEasy)
                        {
                            Response.Redirect("Life17Bind.aspx?pez=1", false);
                            return;
                        }
                        else
                        {
                            NewMemberUtility.UserSignIn(email, SsoMemberInfo[0].Source, false);
                            Response.Redirect(config.SiteUrl);
                            return;
                        }
                         */
                    }
                    else if (res == MemberRegisterReplyType.ExternalUserIdExists)
                    {
                        //pezlogin/sso 會判斷，如果串接已存在，會做導頁而不會進本頁
                        //所以，這是一個例外的處理
                        Response.Redirect(config.SiteUrl);
                        return;
                    }
                    else
                    {
                        script += string.Format("wrongEmail('{0}');", errMsg);
                    }
                }
                ClientScript.RegisterStartupScript(GetType(), "checkMail", "$(function(){" + script + "checkMail();});", true);
            }
        }

        private bool ValidateData(string email, out string script)
        {
            script = "";
            int city;
            if (int.TryParse(Area, out city) == false || city == 0)
            {
                return false;
            }

            if (chkAgree.Checked == false)
            {
                return false;
            }

            if (RegExRules.CheckEmail(email) == false)
            {
                script += "wrongEmail();";
                return false;
            }
            return true;
        }

        private MemberRegisterReplyType RegisterNew(string userName,string email, out string errMsg)
        {
            IMemberRegister reg;
            if (SsoMemberInfo[0].Source == SingleSignOnSource.Facebook)
            {
                reg = new FacebookMemberRegister(SsoMemberInfo[0].ExternalId, userName, email, int.Parse(Area));
            }
            else if (SsoMemberInfo[0].Source == SingleSignOnSource.PayEasy)
            {
                reg = new PayeasyMemberRegister(SsoMemberInfo[0].ExternalId, userName, int.Parse(Area));
            }
            else
            {
                errMsg = "系統目前忙碌中，請稍後再嘗試。";
                return MemberRegisterReplyType.RegisterError;
            }
            var result = reg.Process(chkReceiveEdm.Checked);
            errMsg = reg.Message;
            return result;
        }
    }
}
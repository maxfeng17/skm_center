﻿using System;
using System.Linq;
using System.Web;
using LunchKingSite.WebLib.Component.MemberActions;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.Web.NewMember
{
    public partial class Register : BasePage
    {

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private IMGMProvider mgm = ProviderFactory.Instance().GetProvider<IMGMProvider>();
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        #region properties

        public string SessionId = LkSiteSession.CaptchaRegister.ToString();

        public string pezLoginUrl = "";

        public string loginUrl = "";

        public string DEAfilterApiKey
        {
            get { return ProviderFactory.Instance().GetConfig().DEAfilterApiKey; }
        }

        private string Email
        {
            get
            {
                return Request["txtEmail"];
            }
        }

        private string Password
        {
            get
            {
                return Request["txtPassword"];
            }
        }

        private string ConPassword
        {
            get
            {
                return Request["txtConPassword"];
            }
        }

        private string CaptchaResponse
        {
            get
            {
                return Request["captchaResponse"];
            }
        }

        private string Area
        {
            get
            {
                return ddlcity.SelectedValue;
            }
        }

        #endregion properties

        protected void Page_Load(object sender, EventArgs e)
        {
            pezLoginUrl = WebUtility.GetPayEasySsoPath();
            loginUrl = WebUtility.GetLoginPath();



            if (!Page.IsPostBack)
            {
                ddlcity.DataSource = CityManager.Citys.Where(x => x.Code != "com" && x.Code != "SYS");
                ddlcity.DataBind();
                ddlcity.Items.Insert(0, "請選擇");
            }
            else
            {
                if (Page.User != null && Page.User.Identity.IsAuthenticated)
                {
                    if (string.IsNullOrWhiteSpace(Request["ReturnUrl"]))
                    {
                        Response.Redirect(config.SiteUrl);
                    }
                    else
                    {
                        Response.Redirect(Request["ReturnUrl"]);
                    }
                }
                else
                {
                    bool dataCorrectness;
                    string script = CheckData(out dataCorrectness);

                    if (dataCorrectness)
                    {
                        string errMsg;
                        MemberRegisterReplyType reply = RegisterMember(Email, Password, Area, out errMsg);
                        if (reply == MemberRegisterReplyType.RegisterSuccess)
                        {
                            //MGM 新會員接受禮物後塞一筆通知
                            if (Request["accessCode"] != null)
                            {
                                string accessCode = Request["accessCode"];
                                var giftData = mgm.GiftGetByAccessCode(accessCode).FirstOrDefault();
                                if (giftData!=null)
                                {
                                    var giftVersion = mgm.GiftVersionGetByGiftiId(giftData.Id);
                                    int userId = MemberFacade.GetUniqueId(Email);
                                    MGMFacade.GiftMessageSet(giftVersion.SenderId, userId, accessCode,
                                        GiftMessageType.GiftCode, giftVersion.SendTime);
                                    MGMFacade.GiftMessageCountSet(userId, (int)GiftMessageType.GiftCode);
                                }
                            }

                            string authKey = MemberFacade.GetMemberAuthKey(Email);
                            Response.Redirect(Helper.CombineUrl(config.SSLSiteUrl, "/mvc/NewMember/RegisterByEmailWaitConfirm?key=" + authKey));

                            return;
                        }
                        script += "wrongEmail('" + errMsg + "')";
                    }
                    ClientScript.RegisterStartupScript(GetType(), "wrongData",
                        "$(function(){$('#txtEmail').val('" + Email + "');" + script + "});", true);
                }
            }
        }

        private string CheckData(out bool correctness)
        {
            string result = string.Empty;
            correctness = true;

            if (RegExRules.CheckPassword(Password) == false)
            {
                result += "wrongPassword();";
                correctness = false;
            }

            if (Password.Equals(ConPassword) == false)
            {
                result += "passwordNotMatch();";
                correctness = false;
            }

            if (int.Equals(0, ddlcity.SelectedIndex))
            {
                result += "noCity();";
                correctness = false;
            }
            else
            {
                result += "$('#" + ddlcity.ClientID + "').val('" + ddlcity.SelectedValue + "');";
            }

            if (CheckBoxAgree.Checked)
            {
                result += "$('#" + CheckBoxAgree.ClientID + "').val('checked','checked');";
            }
            else
            {
                result += "noAgree();";
                correctness = false;
            }

            if (null == Session[SessionId] || !string.Equals(Session[SessionId].ToString().ToLower(), CaptchaResponse.ToLower()))
            {
                result += "wrongCaptcha();";
                correctness = false;
            }

            if (String.IsNullOrWhiteSpace(Email))
            {
                result += "wrongEmail('Email未輸入');";
                correctness = false;
            }

            if (RegExRules.CheckEmail(Email) == false)
            {
                result += "wrongEmail('Email格式不正確');";
                correctness = false;
            }

            return result;
        }

        private MemberRegisterReplyType RegisterMember(string mail, string password, string area, out string errMsg)
        {
            IMemberRegister reg = new Life17MemberRegister(mail, password, int.Parse(area));
            MemberRegisterReplyType reply = reg.Process(chkReceiveEDM.Checked);
            errMsg = reg.Message;
            return reply;
        }
    }
}
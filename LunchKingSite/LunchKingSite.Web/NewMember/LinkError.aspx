﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LinkError.aspx.cs" Inherits="LunchKingSite.Web.NewMember.LinkError"
    MasterPageFile="~/NewMember/NewMember.master" %>

<%@ MasterType VirtualPath="~/NewMember/NewMember.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="MemberCardFrame">
        <div class="MemberCard">
            <div class="MemberCardTopBlack">
                <div class="LifeMemberTopText"></div>
            </div>
            <div class="LifeMemberCenter">
                <div class="LifeMemberConfirmMessage">
                    <img src="../Themes/default/images/17Life/NewMember/memberErrorBig.png" width="60" height="60" />
                    <span class="LifeMemberConfirmEPText">很抱歉！此連結已經失效。</span>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

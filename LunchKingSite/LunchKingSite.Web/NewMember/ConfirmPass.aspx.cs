﻿using System;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.NewMember
{
    public partial class ConfirmPass : BasePage
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected int UserId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Session[LkSiteSession.UserId.ToString()] == null)
            {
                Response.Redirect(config.SiteUrl);
            }
            if (Int32.TryParse(Session[LkSiteSession.UserId.ToString()].ToString(), out UserId) == false)
            {
                Response.Redirect(config.SiteUrl);
            }
            Member mem = MemberFacade.GetMember(UserId);
            if (mem.IsLoaded == false)
            {
                Response.Redirect(config.SiteUrl);
            }
            MemberFacade.SendMemberRegisterSuccessMail(mem.UserEmail, mem.UniqueId);
        }
    }
}
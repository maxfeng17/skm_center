﻿using System;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.Web.NewMember
{
    public partial class Logout : Page
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected void Page_Load(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            //isPageLoaded :首頁區域提示Tip顯示用 排除被清掉
            foreach (string key in Request.Cookies.AllKeys.Where(x => x != LkSiteCookie.isPageLoaded.ToString()))
            {
                if (key != LkSiteCookie.PiinLifeSubscriptionCookie.ToString())
                {
                    Response.Cookies[key].Expires = DateTime.Now;
                }

            }

            Session.Abandon();

            bool isPez = false;
            if (config.NewPezSso && null != Session[LkSiteSession.ExternalMemberId.ToString()])
            {
                ExternalMemberInfo memDict = (ExternalMemberInfo)Session[LkSiteSession.ExternalMemberId.ToString()];
                if (!string.IsNullOrWhiteSpace(memDict[SingleSignOnSource.PayEasy]))
                {
                    isPez = true;
                }
            }
            if (isPez && config.PezSyncLogout)
            {
                NewMemberUtility.PezSsoLogout(Helper.CombineUrl(config.SiteUrl, "ppon/"));
            }
            else if (Request["login"] != null)
            {
                Response.Redirect(FormsAuthentication.LoginUrl);
            }
            else
            {
                if ((Session[LkSiteSession.NowUrl.ToString()] != null) && (Session[LkSiteSession.NowUrl.ToString()].ToString().IndexOf("piinlife") > 0))
                {
                    Response.Redirect(ResolveUrl("~/piinlife/"));
                }
                if (Request["ref"] != null)
                {
                    if (Request["appId"] != null)
                    {
                        Response.Redirect(Request["ref"] + "&appId=" + Request["appId"]);
                    }
                    //如果是這頁，就不導回
                    if (Request["ref"].EndsWith("MobileAuth.aspx", StringComparison.OrdinalIgnoreCase) == false)
                    {
                        Response.Redirect(Request["ref"]);
                    }
                }
                if (Response.IsRequestBeingRedirected == false)
                {
                    Response.Redirect(config.SiteUrl);
                }
            }
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PEZOpenPass.aspx.cs" Inherits="LunchKingSite.Web.NewMember.PEZOpenPass"
    MasterPageFile="~/NewMember/NewMember.master" %>

<%@ MasterType VirtualPath="~/NewMember/NewMember.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $(".LifeMemberPEZOpenPassBtn").click(function () {
                window.location.href = encodeURI("<%=webUrl %>");
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="LifeMemberFrame">
        <div class="LifeMember">
            <div class="LifeMemberTop">
                <div class="LifeMemberTopText">
                    <img src="../Themes/default/images/17Life/NewMember/memberPen.jpg" alt="" width="25"
                        height="25" />恭喜您開通成功！</div>
            </div>
            <div class="LifeMemberCenter">
                <div class="LifeMemberConfirmMessage">
                    <div class="LifeMemberPEZOpenPassaPic">
                        <img src="../Themes/PCweb/images/memberPassBig.png" alt="" width="60"
                            height="60" /></div>
                    <div class="LifeMemberPEZOpenPassText">
                        馬上登入，立即體驗17Life超多省錢好康～<br />
                        您仍可選擇使用Facebook與PayEasy登入；或在登入異常時改以17Life備用帳號登入！</div>
                    <div class="LifeMemberPEZOpenPassBtn" style="cursor: pointer;">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EMailInvalid.aspx.cs"
    Inherits="LunchKingSite.Web.NewMember.EMailInvalid" MasterPageFile="~/NewMember/NewMember.master" EnableViewState="False" ClientIDMode="Static" %>

<%@ MasterType VirtualPath="~/NewMember/NewMember.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href="../Themes/PCweb/css/MasterPage.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/default/style/RDL.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" media="only screen and (min-width: 481px) and (max-width: 767px)" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" media="only screen and (max-width: 480px)" />
    <link href="../Themes/PCweb/plugin/font-awesome/font-awesome.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        $(function () {
            $("#btnChange").click(function () {
                window.location.href = "/User/UserAccount.aspx";
            });
            $("#btnDefault").click(function () {
                if ($('#hidNextUrl').val() != '') {
                    window.location.href = $('#hidNextUrl').val();
                } else {
                    window.location.href = "/";
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <input type="hidden" id="hidNextUrl" runat="server" />
    <div class="LifeMemberFrame">
        <div class="LifeMember">
            <div class="LifeMemberTopBlack rd-fd-sn">
                <div class="LifeMemberTopText">請重新設定E-mail</div>
            </div>
            <div class="LifeMemberCenter">
                <div class="LMPasswordFrame text-center  rd-fd-sn">
                    <div class="grui-form">
                        <div class="form-unit" style="border: 0;">
                            <i class="fa fa-warning fa-lg red-color"></i>
                            <p style="display: inline">您的E-mail格式有誤(<asp:Literal ID="litUserName" runat="server" />)，請重新設定，避免漏接重要訊息。</p>
                            <br />
                        </div>
                    </div>
                    <div class="form-unit end-unit rd-pay-pos-fix">
                        <div class="data-input rd-pay-span-ctr">
                            <p class="login">
                                <input type="button" id="btnChange" value="進行設定" class="btn btn-large btn-primary">
                                <input type="button" id="btnDefault" value="下次設定" class="btn btn-large rd-payment-large-btn">
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

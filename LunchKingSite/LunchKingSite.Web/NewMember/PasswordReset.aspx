﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PasswordReset.aspx.cs"
    Inherits="LunchKingSite.Web.NewMember.PasswordReset" MasterPageFile="~/NewMember/NewMember.master" %>

<%@ MasterType VirtualPath="~/NewMember/NewMember.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href='/themes/PCweb/css/RDL-L.css' rel="stylesheet" type="text/css" />
    <link href='/themes/PCweb/css/RDL-M.css' rel="stylesheet" type="text/css" />
    <link href='/themes/PCweb/css/RDL-S.css' rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("#txtPassword").focus(function () {
                $("#spanPassword").html('6~12碼，英文或數字，英文請區分大小寫');
                $("#linePassword").removeClass("LMPasswordInColor").addClass("LMPasswordIn");
            });


            $("#txtConPassword").focus(function () {
                $("#spanConPassword").html('請重新再輸入一次密碼！');
                $("#lineConPassword").removeClass("LMPasswordInColor").addClass("LMPasswordIn");
            });

            $("#btnSubmit").click(function () {
                if (checkInfo()) {
                    $('form').submit();
                }
            });

            $(".btnRedirect").click(function() {
                location.href = "/NewMember/ForgetPassword.aspx";
            });

            $(".btnHome").click(function() {
                location.href = "/";
            });
        });

        function checkInfo() {
            // password check
            if (!passwordCheck($("#txtPassword").val())) {
                $("#spanPassword").html('<span style="color: #F00; letter-spacing: normal;">密碼格式錯誤！</span>');
                $("#linePassword").removeClass("LMPasswordIn").addClass("LMPasswordInColor");
            } else {
                // confirm password check
                if (!passwordConfirm($("#txtPassword").val(), $("#txtConPassword").val())) {
                    $("#spanConPassword").html('<span style="color: #F00; letter-spacing: normal;">您輸入的密碼不符合，請重新輸入！</span>');
                    $("#lineConPassword").removeClass("LMPasswordIn").addClass("LMPasswordInColor");
                } else {
                    return true;
                }
            }

            return false;
        }
    </script>    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="LifeMemberFrame">
        <asp:PlaceHolder ID="phPasswordSetting" runat="server">
        <div class="LifeMember">
            <div class="LifeMemberTopBlack">
                <div class="LifeMemberTopText">設定密碼</div>
            </div>
            <div class="LifeMemberCenter">
                <div class="LMPasswordFrame">
                    <ul class="LMPasswordIn" >
                        <li class="LMPasswordIn" style="height:30px" id="displayBox" runat="server"><span class="LMPasswordFrameTittle rdl-LMPawdTit">你的帳號：</span><span
                            class="LMPasswordFrameText rdl-LMPawdFTe"><asp:Literal ID="litDisplay" runat="server" />
                        </span>
                        </li>                        
                        <li id="linePassword" class="LMPasswordIn"><span class="LMPasswordFrameTittle">自訂新密碼：</span><span
                            class="LMPasswordFrameText rdl-LMPawdFTe">
                            <input type="password" id="txtPassword" name="txtPassword" />
                        </span><span id="spanPassword" class="LMPasswordFrameNote">6~12碼，英文或數字，英文請區分大小寫</span>
                        </li>
                        <li id="lineConPassword" class="LMPasswordIn"><span class="LMPasswordFrameTittle rdl-LMPawdTit">確認密碼：</span><span class="LMPasswordFrameText rdl-LMPawdFTe">
                                <input type="password" id="txtConPassword" />
                            </span><span id="spanConPassword" class="LMPasswordFrameNote">請重新再輸入一次密碼。</span>
                        </li>
                    </ul>
                    <br />
                    <div class="text-center">
                        <input type="button" id="btnSubmit" class="btn btn-large btn-primary" value="送出">
                    </div>
                </div>
            </div>
        </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phLinkExpired" runat="server">
            <div class="LifeMemberTopBlack">
                <div class="LifeMemberTopText">重新設定密碼逾時！</div>
            </div>
            <div class="LifeMemberCenter">
                <div class="LifeMemberConfirmMessage">
                    <img src="../Themes/default/images/17Life/NewMember/memberTimeout.png" alt="" width="60"
                        height="60" /><span class="LifeMemberConfirmEPText">很抱歉！基於安全理由，系統發送給您的密碼重新設定連結只在24小時內有效。</span>
                    <br />
                    <p class="info btn-center">
                        <input type="button" class="btn btn-large btn-primary rd-payment-xlarge-btn btnRedirect" value="重新索取">
                    </p>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phLinkError" runat="server">
            <div class="LifeMemberTopBlack">
                <div class="LifeMemberTopText">重新設定密碼錯誤！</div>
            </div>
            <div class="LifeMemberCenter">
                <div class="LifeMemberConfirmMessage">
                    <img src="../Themes/default/images/17Life/NewMember/memberErrorBig.png" alt="" width="60"
                        height="60" /><span class="LifeMemberConfirmEPText">很抱歉！此連結已經失效。</span>
                    <br />
                    <p class="info btn-center">
                        <input type="button" class="btn btn-large btn-primary rd-payment-xlarge-btn btnRedirect" value="重新索取">
                    </p>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phPasswordResult" runat="server">
            <div class="LifeMemberTopBlack">
                <div class="LifeMemberTopText">密碼設定完成！</div>
            </div>
            <div class="LifeMemberCenter">
                <div class="LifeMemberConfirmMessage">
                    <img src="../Themes/PCweb/images/memberPassBig.png" alt="" width="60"
                        height="60" /><span class="LifeMemberConfirmEPText">密碼設定成功！請重新登入</span>
                    <br />
                    <p class="info btn-center">
                        <input type="button" class="btn btn-large btn-primary rd-payment-xlarge-btn btnHome" value="看今日好康">
                    </p>
                </div>
            </div>
        </asp:PlaceHolder>
    </div>    
</asp:Content>

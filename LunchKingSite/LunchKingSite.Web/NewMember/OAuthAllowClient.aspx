﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OAuthAllowClient.aspx.cs" MasterPageFile="~/Ppon/Ppon.Master" 
    Inherits="LunchKingSite.Web.NewMember.OAuthAllowClient" %>

<asp:Content ContentPlaceHolderID="SSC" runat="server">
    <style type="text/css">
        .sliderWrapMenber {
            display: none;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
<div class="msg-block clearfix">
   <p class="msg-p"><span class="nam-value"><%=ClientTitle %></span>想存取您在17Life上的<%=ClientPermsDescription %></p>
 <div class="msg-div"> 
     <asp:Button ID="btnSubmit" runat="server" Text="確定" CssClass="btn btn-primary rd-payment-xlarge-btn" OnClick="btnSubmit_Click" />
     <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn" OnClick="btnCancel_Click" />
 </div>
</div>
</asp:Content>
﻿using System;
using System.Linq;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.Web.NewMember
{
    public partial class MemberAuth : BasePage
    {

        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public string UserEmail { get; set; }

        public string Password
        {
            get { return txtPassword.Text.Trim(); }
        }        

        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private const int LinkValidDays = 30;

        protected enum AuthResult
        {
            LinkError,
            LinkExpired,
            OK,
            OKPass,
            AuthAndPassword
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int userId;
            int.TryParse(Request["uid"], out userId);
            this.Session[LkSiteSession.UserId.ToString()] = userId;
            SwitchView(AuthAccount());
        }

        private AuthResult AuthAccount()
        {
            int userId;
            int.TryParse(Request["uid"], out userId);
            string authKey = Request["key"];
            string authCode = Request["code"];

            if (int.Equals(0, userId) || string.IsNullOrWhiteSpace(authKey) || string.IsNullOrWhiteSpace(authCode))
            {
                return AuthResult.LinkError;
            }


            Member mem = mp.MemberGetbyUniqueId(userId);
            UserEmail = mem.UserName;
            MemberAuthInfo mai = mp.MemberAuthInfoGet(userId);

            if (mem.IsLoaded && mai.IsLoaded)
            {
                if (mai.AuthDate == null || string.IsNullOrEmpty(mai.AuthKey) || string.IsNullOrEmpty(mai.AuthCode))
                {
                    return AuthResult.LinkError;
                }

                if ((DateTime.Now - mai.AuthDate.Value).TotalDays > LinkValidDays)
                {
                    return AuthResult.LinkExpired;
                }
                Session[LkSiteSession.NowUrl.ToString()] = config.SiteUrl;
                MemberLinkCollection moCol = mp.MemberLinkGetList(mem.UniqueId);
                bool is17Life = moCol.Any(t => t.ExternalOrg == (int)SingleSignOnSource.ContactDigitalIntegration);
                if (is17Life == false)
                {
                    return AuthResult.LinkError;
                }

                if ((mem.Status & (int) MemberStatusFlag.Active17Life) > 0)
                {
                    return AuthResult.OK;
                }

                if (mai.AuthCode == authCode && mai.AuthKey == authKey)
                {
                    if (mem.IsGuest == false)
                    {
                        MemberFacade.SetMemberActive(mem.UniqueId);
                        return AuthResult.OK;
                    }
                    else if (mem.IsGuest && RegExRules.CheckPassword(txtPassword.Text))
                    {
                        MemberFacade.SetMemberActive(mem.UniqueId);
                        MemberFacade.ResetMemberPassword(mem.UserName, Password,
                            ResetPasswordReason.GuestMemberSetPassword);
                        return AuthResult.OK;                        
                    } 
                    else
                    {
                        return AuthResult.AuthAndPassword;                        
                    }
                }
            }

            return AuthResult.LinkError;
        }


        private void SwitchView(AuthResult result)
        {
            switch (result)
            {
                case AuthResult.LinkExpired:
                    Response.Redirect("ConfirmExpired.aspx?m=" + Helper.EncryptEmail(UserEmail));
                    break;
                case AuthResult.OK:
                    Response.Redirect("ConfirmPass.aspx");
                    break;
                case AuthResult.OKPass:
                    Response.Redirect("PEZOpenPass.aspx");
                    break;
                case AuthResult.LinkError:
                    Response.Redirect("LinkError.aspx");
                    break;
                case AuthResult.AuthAndPassword:
                    break;
            }
        }
    }
}
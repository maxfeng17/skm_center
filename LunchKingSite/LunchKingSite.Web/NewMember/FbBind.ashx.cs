﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Web.SessionState;
using System;
using log4net;
using log4net.Core;

namespace LunchKingSite.Web.NewMember
{
    /// <summary>
    /// Summary description for FbAuth
    /// </summary>
    public class FbBind : IHttpHandler, IRequiresSessionState
    {
        private ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
        private ILog logger = LogManager.GetLogger(typeof(FbBind));
        public string ReturnCode
        {
            get
            {
                return (string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["code"])) ? null : HttpContext.Current.Request.QueryString["code"];
            }
        }
        public string RedirectUri = "/newmember/fbbind.ashx";

        public void ProcessRequest(HttpContext context)
        {
            if (HttpContext.Current.User.IsInRole(MemberRoles.Administrator.ToString()) == false && conf.EnableMemberLinkBind == false)
            {
                throw new HttpException((int)HttpStatusCode.Unauthorized, "no access.");
            }

            if (!string.IsNullOrEmpty(ReturnCode))
            {
                string AccessToken = GetAccessToken();
                string errMsg;
                FacebookUser fbUser = FacebookUser.Get(AccessToken);
                if (MemberFacade.BindMemberLink(MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name),
                        SingleSignOnSource.Facebook, fbUser.Id, out errMsg) == false)
                {
                    string url = Helper.CombineUrl(conf.SiteUrl, "/User/UserAccount.aspx");
                    context.Response.Write(String.Format("<script>alert('{0}'); location.href='{1}';</script>", errMsg, url));
                    return;
                }
                context.Response.Redirect("~/User/UserAccount.aspx");
            }
        }

        private string GetAccessToken()
        {
            string url = string.Format("https://graph." + (conf.EnableBetaGraphAPI ? "beta." : "") + "facebook.com/oauth/access_token?client_id={0}&redirect_uri={1}&client_secret={2}&code={3}"
                                       , conf.FacebookApplicationId, conf.SiteUrl + RedirectUri, conf.FacebookApplicationSecret, ReturnCode);

            return FacebookUtility.GetOAuhToken(url);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
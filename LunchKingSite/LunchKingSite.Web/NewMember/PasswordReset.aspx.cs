﻿using System;
using System.Web;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.NewMember
{
    public partial class PasswordReset : BasePage
    {
        IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        #region properties

        public string Password
        {
            get
            {
                return HttpUtility.UrlDecode(Request["txtPassword"]);
            }
        }

        public int UserId
        {
            get
            {
                int userId;
                int.TryParse(Request["uid"], out userId);
                return userId;
            }
        }

        private string AuthKey
        {
            get
            {
                return Request["key"];
            }
        }

        private string AuthCode
        {
            get
            {
                return Request["code"];
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            phPasswordSetting.Visible = false;
            phLinkExpired.Visible = false;
            phLinkError.Visible = false;
            phPasswordResult.Visible = false;

            CheckAuthLinkResult result = CheckAuthLink();
            if (result == CheckAuthLinkResult.Expired)
            {
                phLinkExpired.Visible = true;
            }
            else if (result == CheckAuthLinkResult.LinkError)
            {
                phLinkError.Visible = true;
            }
            else if (IsPostBack == false)
            {
                {
                    phPasswordSetting.Visible = true;
                    if (Request["display"] != null)
                    {
                        if (Request["display"] == "email")
                        {
                            litDisplay.Text = MemberFacade.GetMember(UserId).UserName;
                        }
                        else if (Request["display"] == "mobile")
                        {
                            litDisplay.Text = MemberFacade.GetMobileMember(UserId).MobileNumber;
                        }
                    }
                    else
                    {
                        displayBox.Visible = false;
                    }
                }
            }
            else
            {
                if (RegExRules.CheckPassword(Password) == false)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alerterR", "$(\"#spanPassword\").html('<span style=\"color: #F00; letter-spacing: normal;\">密碼格式錯誤！</span>');$(\"#linePassword\").removeClass(\"LMPasswordIn\").addClass(\"LMPasswordInColor\");", true);
                    phPasswordSetting.Visible = true;
                }
                else
                {
                    ResetPassword();
                    phPasswordResult.Visible = true;
                }
            }
        }


        private enum CheckAuthLinkResult
        {
            LinkError,
            Expired,
            Valid
        }

        private CheckAuthLinkResult CheckAuthLink()
        {
            if (UserId == 0 || string.IsNullOrWhiteSpace(AuthKey) || string.IsNullOrWhiteSpace(AuthCode))
            {
                return CheckAuthLinkResult.LinkError;
            }

            MemberAuthInfo mai = mp.MemberAuthInfoGet(UserId);

            if (mai.IsLoaded)
            {
                if (mai.ForgetPasswordDate == null || string.IsNullOrEmpty(mai.ForgetPasswordKey)
                    || string.IsNullOrEmpty(mai.ForgetPasswordCode))
                {
                    return CheckAuthLinkResult.LinkError;
                }

                // 連結只在24小時內有效
                if (DateTime.Now > mai.ForgetPasswordDate.Value.AddDays(1))
                {
                    return CheckAuthLinkResult.Expired;
                }

                if (mai.ForgetPasswordCode == AuthCode && mai.ForgetPasswordKey == AuthKey)
                {
                    return CheckAuthLinkResult.Valid;
                }
            }

            return CheckAuthLinkResult.LinkError;
        }

        private void ResetPassword()
        {
            Member mem = mp.MemberGet(UserId);
            if (mem.IsLoaded)
            {
                if (Request["display"] == "email")
                {
                    if (MemberFacade.ResetMemberPassword(mem.UserName, Password, ResetPasswordReason.MemberForgot))
                    {
                        MemberFacade.SetMemberActive(mem.UniqueId);
                    }
                }
                else if (Request["display"] == "mobile")
                {
                    MemberFacade.SetMobleMemberPassword(UserId, Password);
                    mp.MobileAuthInfoDelete(UserId);
                }
                MemberAuthInfo mai = mp.MemberAuthInfoGet(mem.UniqueId);
                mai.ForgetPasswordCode = null;
                mai.ForgetPasswordKey = null;
                mai.PasswordQueryTime = null;
                mp.MemberAuthInfoSet(mai);
            }
        }
    }
}
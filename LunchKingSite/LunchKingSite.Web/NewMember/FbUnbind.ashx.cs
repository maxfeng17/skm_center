﻿using System;
using System.Net;
using System.Web;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using System.Web.SessionState;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.NewMember
{
    /// <summary>
    /// Summary description for FbAuth
    /// </summary>
    public class FbUnbind : IHttpHandler, IRequiresSessionState
    {
        private ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
        public void ProcessRequest(HttpContext context)
        {
            if (HttpContext.Current.User.IsInRole(MemberRoles.Administrator.ToString()) == false && conf.EnableMemberLinkBind == false)
            {
                throw new HttpException((int)HttpStatusCode.Unauthorized, "no access.");
            }

            string errMsg;
            if (MemberFacade.UnbindMemberLink(MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name),
                SingleSignOnSource.Facebook, out errMsg) == false)
            {
                string url = Helper.CombineUrl(conf.SiteUrl, "/User/UserAccount.aspx");
                context.Response.Write(String.Format("<script>alert('{0}'); location.href='{1}';</script>", errMsg, url));
                return;
            }
            context.Response.Redirect("~/User/UserAccount.aspx");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
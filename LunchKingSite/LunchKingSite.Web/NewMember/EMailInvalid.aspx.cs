﻿using System;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.NewMember
{
    public partial class EMailInvalid : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                Response.Redirect("/");
            }
            Member member = MemberFacade.GetMember(this.User.Identity.Name);
            litUserName.Text = member.UserEmail;
            if (RegExRules.CheckEmail(member.UserEmail))
            {
                Response.Redirect("/");
            }

            if (Session[LkSiteSession.NowUrl.ToString()] != null)
            {
                hidNextUrl.Value = Session[LkSiteSession.NowUrl.ToString()].ToString();
            }
        }
    }
}
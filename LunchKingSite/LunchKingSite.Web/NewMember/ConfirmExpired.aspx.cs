﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib;

namespace LunchKingSite.Web.NewMember
{
    public partial class ConfirmExpired : BasePage
    {
        protected string UserEmail
        {
            get
            {
                return Helper.DecryptEmail(Request["m"]);
            }
        }
    }
}
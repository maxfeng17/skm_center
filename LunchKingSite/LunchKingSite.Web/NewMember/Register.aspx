﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="LunchKingSite.Web.NewMember.Register"
    MasterPageFile="~/NewMember/NewMember.master" %>

<%@ MasterType VirtualPath="~/NewMember/NewMember.master" %>
<%@ Register Src="~/UserControls/FacebookLoginButton.ascx" TagName="FacebookLoginButton"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-L.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-M.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-S.css") %>' rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Tools/js/completer.js"></script>
    <link href="../Tools/js/css/completer.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        $(function () {
            $("#txtEmail").focus(function () {
                $("#spanMail").html('您輸入的e-mail將作為17Life的登入帳號！');
                $("#lineMail").removeClass("LifeMemberInfoColor").addClass("LifeMemberInfo");
            });

            $("#txtPassword").focus(function () {
                $("#spanPassword").html('6碼以上，英文或數字，英文請區分大小寫');
                $("#linePassword").removeClass("LifeMemberInfoColor").addClass("LifeMemberInfo");
            });

            $("#txtConPassword").focus(function () {
                $("#spanConPassword").html('請重新再輸入一次密碼！');
                $("#lineConPassword").removeClass("LifeMemberInfoColor").addClass("LifeMemberInfo");
            });

            $("#captchaResponse").focus(function () {
                $("#spanCaptcha").html('請輸入下方顯示的文字，英文可不區分大小寫！');
                $("#lineCaptcha").removeClass("LifeMemberInfoColor").addClass("LifeMemberInfo");
            });

            $("#<%=ddlcity.ClientID %>").change(function () {
                if ($("#<%=ddlcity.ClientID %>").val() == "請選擇") {
                    $("#spanCity").show();
                    $("#lineArea").removeClass("LifeMemberInfo").addClass("LifeMemberInfoColor");
                } else {
                    $("#spanCity").hide();
                    $("#lineArea").removeClass("LifeMemberInfoColor").addClass("LifeMemberInfo");
                }
            });

            $('#<%= CheckBoxAgree.ClientID %>').change(function () {
                if ($('#<%= CheckBoxAgree.ClientID %>').attr("checked")) {
                    $("#lineAgree").removeClass("LifeMemberInfoColor_f").addClass("LifeMemberInfo_f");
                    $("#agreeAlert").hide();
                } else {
                    $("#lineAgree").removeClass("LifeMemberInfo_f").addClass("LifeMemberInfoColor_f");
                    $("#agreeAlert").show();
                }
            });

            $("#LifeMemberBtn").click(function (e) {
                $('form').trigger('submit');
            });

            preventDobuleSubmit();

            getCaptcha('<%= SessionId %>');

            $("#txtEmail").completer({
                separator: "@",
                source: window.EmailDomainSource,
                itemSize: 7
            });
        });

        function getCaptcha(id) {
            $("#imgCaptcha").attr("src", "Captcha.ashx?x=" + Math.random() + "&sessionId=" + id);
        }

        function wrongCaptcha() {
            $("#spanCaptcha").html('<span style="color: #F00; letter-spacing: normal;">圖形驗證輸入錯誤！</span>');
            $("#lineCaptcha").removeClass("LifeMemberInfo").addClass("LifeMemberInfoColor");
        }

        function noAgree() {
            $("#lineAgree").removeClass("LifeMemberInfo_f").addClass("LifeMemberInfoColor_f");
            $("#agreeAlert").show();
        }

        function noCity() {
            $("#lineArea").removeClass("LifeMemberInfo").addClass("LifeMemberInfoColor");
            $("#spanCity").show();
        }

        function passwordNotMatch() {
            $("#spanConPassword").html('<span style="color: #F00; letter-spacing: normal;">您輸入的密碼不符合，請重新輸入！</span>');
            $("#lineConPassword").removeClass("LifeMemberInfo").addClass("LifeMemberInfoColor");
        }

        function wrongEmail(msg) {
            $("#spanMail").html('<span style="color: #F00; letter-spacing: normal;">' + msg + '</span>');
            $("#lineMail").removeClass("LifeMemberInfo").addClass("LifeMemberInfoColor");
        }

        function wrongPassword() {
            $("#spanPassword").html('<span style="color: #F00; letter-spacing: normal;">密碼格式錯誤！</span>');
            $("#linePassword").removeClass("LifeMemberInfo").addClass("LifeMemberInfoColor");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="LifeMemberLoginPic_wrap">
        <div class="LifeMemberLoginPic">
            <div class="LifeMemberLoginPicText">用您已有的帳號登入</div>
            <a href="<%=loginUrl %>">
                <img alt="" src="../Themes/PCweb/images/Login_17life.jpg" width="32" height="39" border="0" />
            </a>
            <uc1:FacebookLoginButton ID="fbl" runat="server" ButtonImageUrl="~/Themes/PCweb/images/Login_fb.jpg" />
            <a href="<%=pezLoginUrl %>">
                <img alt="" src="../Themes/PCweb/images/Login_pez.jpg" width="32" height="39" border="0" />
            </a>
        </div>
    </div>
    <div class="MemberCardFrame">
        <div class="MemberCard">
            <div class="MemberCardTopBlack">
                <div class="LifeMemberTopText">
                    <span class="rdl-Tdis">加入17Life後，可以獲得更多生活上的便宜好康唷！</span>
                    <span class="rdl-tittleText">加入17Life，可獲得生活上的好康唷！</span>
                </div>
            </div>
            <div class="LifeMemberCenter">
                <div class="LMInfo">
                    <ul class="LifeMemberInfo">
                        <li id="lineMail" class="LifeMemberInfo"><span class="LifeMemberInfoTittle">電子信箱：</span><span
                            class="LifeMemberText ">
                            <input type="text" id="txtEmail" name="txtEmail" />
                        </span><span id="spanMail" class="LifeMemberNote">您輸入的e-mail將作為17Life的登入帳號</span>
                        </li>
                        <li id="linePassword" class="LifeMemberInfo"><span class="LifeMemberInfoTittle">登入密碼：</span>
                            <span class="LifeMemberText">
                                <input type="password" id="txtPassword" name="txtPassword" />
                            </span><span id="spanPassword" class="LifeMemberNote">6碼以上，英文或數字，英文請區分大小寫</span>
                        </li>
                        <li id="lineConPassword" class="LifeMemberInfo"><span class="LifeMemberInfoTittle">確認密碼：</span><span
                            class="LifeMemberText ">
                            <input type="password" id="txtConPassword" name="txtConPassword" />
                        </span><span id="spanConPassword" class="LifeMemberNote">請重新再輸入一次密碼</span> </li>
                        <li id="lineArea" class="LifeMemberInfo"><span class="LifeMemberInfoTittle">所在地區：</span>
                            <span class="LifeMemberText ">
                                <asp:DropDownList ID="ddlcity" runat="server" DataTextField="CityName" DataValueField="id" />
                            </span><span class="LifeMemberTXT2">
                                <asp:CheckBox ID="chkReceiveEDM" runat="server" Text="訂閱EDM" Checked="true" /></span>
                            <span class="LifeMemberNote"><span id="spanCity" class="onlyRED" style="display: none;">請選擇您的所在地區！</span></span> </li>
                        <li id="lineCaptcha" class="LifeMemberInfo"><span class="LifeMemberInfoTittle">圖形驗證：</span>
                            <span class="LifeMemberText ">
                                <input type="text" id="captchaResponse" name="captchaResponse" />
                            </span><span id="spanCaptcha" class="LifeMemberNote">請輸入下方顯示的文字，英文可不區分大小寫</span>
                        </li>
                    </ul>
                    <div class="LifeMemberVerification">
                        <div class="LifeMemberVFCode">
                            <img id="imgCaptcha" alt="" />
                        </div>
                        <div class="LifeMemberVFCodeText">
                            <a href="javascript:getCaptcha('<%= SessionId %>');">更換一組驗證碼</a>
                        </div>
                    </div>
                    <ul class="LifeMemberInfo_f">
                        <li id="lineAgree" class="LifeMemberInfo_f">
                            <span class="LifeMemberAgree">
                                <label class="LifeMemberLabel">
                                    <asp:CheckBox ID="CheckBoxAgree" runat="server" />
                                    我同意17Life<a href="../newmember/PrivacyPolicy.aspx" target="_blank">服務條款</a>和<a href="../newmember/privacy.aspx" target="_blank">隱私權政策</a>。
                                    <span id="agreeAlert" class="meeaar" style="display: none;">您是否已經閱讀並同意17Life的各項條款？</span>
                                </label>
                            </span>
                        </li>
                    </ul>

                    <div class="text-center">
                        <p class="subnote">
                            <input id="LifeMemberBtn" type="button" class="btn btn-large btn-primary rd-payment-xlarge-btn" value="下一步">
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

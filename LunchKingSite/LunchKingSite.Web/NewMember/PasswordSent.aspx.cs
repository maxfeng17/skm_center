﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.NewMember
{
    public partial class PasswordSent : BasePage
    {
        protected string forgetPasswordMail;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["forgetPasswordMail"] == null)
            {
                Response.Redirect("ForgetPassword.aspx");
                return;
            }
            forgetPasswordMail = (string)Session["forgetPasswordMail"];
            string local = Helper.GetEmailLocal(forgetPasswordMail);
            if (local.Length > 2)
            {
                forgetPasswordMail = string.Format("{0}{1}{2}@{3}",
                    local.Substring(0, 1),
                    new String('*', local.Length - 2),
                    local.Substring(local.Length - 1, 1),
                    Helper.GetEmailDomain(forgetPasswordMail));
            }
        }
    }
}
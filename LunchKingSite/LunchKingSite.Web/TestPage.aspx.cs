﻿using System;
using System.Web;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.Web
{
    public partial class TestPage : System.Web.UI.Page
    {
        protected bool TestCookie;


        protected int CookieDay
        {
            get
            {
                int d = 0;
                if (Request["d"] != null)
                {
                    d = int.Parse(Request["d"]);
                }
                return d;
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["cookieCheck"] == null)
            {
                phOutline.Visible = true;
            }
            else
            {
                phContent.Visible = true;
                if (this.IsCookieDisabled())
                {
                    TestCookie = false;
                }
                else
                {
                    TestCookie = true;
                }
            }
        }

        private bool IsCookieDisabled()
        {
            if (Request.QueryString["cookieCheck"] == "false")
            {
                try
                {
                    HttpCookie c = CookieManager.NewCookie("SupportCookies", "true");
                    if (CookieDay > 0)
                    {
                        c.Expires = DateTime.Now.AddDays(CookieDay);
                    }
                    Response.Cookies.Add(c);

                    Response.Redirect("TestPage.aspx?cookieCheck=true");
                }
                catch
                {
                }
            }

            if (Request.Browser.Cookies == false || Request.Cookies["SupportCookies"] == null)
            {
                return true;
            }

            return false;
        }

        protected string GetClientIP()
        {
            if (Request.ServerVariables["HTTP_VIA"] == null)
            {
                return Request.ServerVariables["REMOTE_ADDR"];
            }
            else
            {
                return Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            }
        }
    }
}
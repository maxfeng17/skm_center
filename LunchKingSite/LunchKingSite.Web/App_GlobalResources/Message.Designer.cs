//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Message {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Message() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Message", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 很抱歉, 此路段目前尚未有商家, 我們會盡快在此開闢新的商店. 若您有您喜愛的餐廳, 也希望您推薦給我們, 讓您也能夠在17Life上享受您喜愛的餐點.
        /// </summary>
        internal static string AreaNoStore {
            get {
                return ResourceManager.GetString("AreaNoStore", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 我們會將您所兌換的物品在近日寄至您所填的地址. 若一切資訊無誤, 請按下送出鈕, 您的兌換訂單將會送出..
        /// </summary>
        internal static string BonusExchangeCheck {
            get {
                return ResourceManager.GetString("BonusExchangeCheck", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 您的兌換訂單已經送出, 感謝您利用此紅利兌換系統, 若您要查看您的兌換記錄請至兌換記錄列表查詢, 謝謝!.
        /// </summary>
        internal static string BonusExchangeComplete {
            get {
                return ResourceManager.GetString("BonusExchangeComplete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 您的瀏覽器不支援內置框架或目前的設定為不顯示內置框架。.
        /// </summary>
        internal static string BrowserNotSupport {
            get {
                return ResourceManager.GetString("BrowserNotSupport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 若您無法找到您的大樓, 請由以下大樓列表選擇您的大樓.
        /// </summary>
        internal static string BuildingNoMatch {
            get {
                return ResourceManager.GetString("BuildingNoMatch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ■ Company 請選擇您的公司.
        /// </summary>
        internal static string ChooseYourCompany {
            get {
                return ResourceManager.GetString("ChooseYourCompany", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ■ Company 請選擇您的單位.
        /// </summary>
        internal static string ChooseYourDepartment {
            get {
                return ResourceManager.GetString("ChooseYourDepartment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 很抱歉，此區尚未有商家加入，因此沒有可供配送的路段.
        /// </summary>
        internal static string CityNoStreets {
            get {
                return ResourceManager.GetString("CityNoStreets", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 我要推薦新店家!!.
        /// </summary>
        internal static string CommendNewSeller {
            get {
                return ResourceManager.GetString("CommendNewSeller", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;div style=&quot; text-align:left; padding:5px;&quot;&gt; 1.感謝您使用&quot;PayEasy保養品團購平台&quot;來蒐集團購訂單，我們已經將
        ///&lt;span class=&quot;font_89bc48&quot;&gt;&quot;訂單明細&quot;&lt;/span&gt;寄到您的eMail信箱中。&lt;/div&gt;
        ///&lt;div class=&quot;dotted&quot;&gt;&lt;/div&gt;
        ///&lt;div style=&quot; text-align:left; padding:5px;&quot;&gt; 2.請注意，您尚未完成訂購，請您至 &lt;a href=&quot;{0}&quot; target=&quot;_blank&quot; class=&quot;font_89bc48&quot;&gt;&lt;strong&gt;{1}&lt;/strong&gt;&lt;/a&gt; 完成您的訂購。&lt;/div&gt;
        ///&lt;div class=&quot;dotted&quot;&gt;&lt;/div&gt;
        ///&lt;div style=&quot;text-align:left; padding:5px;&quot;&gt; 3.您可以在訂購完成之後，給予賣家評價，共同創造更好的賣家服務品質，謝謝!&lt;/div&gt;
        ///&lt;div class=&quot;dotted&quot;&gt;&lt;/div&gt;.
        /// </summary>
        internal static string CosmeticPay1 {
            get {
                return ResourceManager.GetString("CosmeticPay1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;div style=&quot; text-align:left; padding:5px;&quot;&gt; 1.感謝您使用&quot;PayEasy保養品團購平台&quot;來蒐集團購訂單，我們已經將
        ///&lt;span class=&quot;font_89bc48&quot;&gt;&quot;訂單明細&quot;&lt;/span&gt;寄到您的eMail信箱中。&lt;/div&gt;
        ///&lt;div class=&quot;dotted&quot;&gt;&lt;/div&gt;
        ///&lt;div style=&quot; text-align:left; padding:5px;&quot;&gt; 2.由於此商家並未與午餐王連線，請您&lt;span class=&quot;font_89bc48&quot;&gt;&lt;strong&gt;自行打電話或傳真給商家訂餐&lt;/strong&gt;&lt;/span&gt;。&lt;/div&gt;
        ///&lt;div class=&quot;dotted&quot;&gt;&lt;/div&gt;
        ///&lt;div style=&quot;text-align:left; padding:5px;&quot;&gt; 3.您可以在訂購完成之後，給予賣家評價，共同創造更好的賣家服務品質，謝謝!&lt;/div&gt;
        ///&lt;div class=&quot;dotted&quot;&gt;&lt;/div&gt;.
        /// </summary>
        internal static string CosmeticPay2 {
            get {
                return ResourceManager.GetString("CosmeticPay2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;object classid=&quot;clsid:D27CDB6E-AE6D-11cf-96B8-444553540000&quot; codebase=&quot;http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0&quot;
        ///  width=&quot;537&quot; height=&quot;111&quot;&gt;
        ///  &lt;param name=&quot;movie&quot; value=&quot;URL&quot;&gt;
        ///  &lt;param name=&quot;quality&quot; value=&quot;high&quot;&gt;
        ///  &lt;param name=&quot;wmode&quot; value=&quot;transparent&quot; /&gt;
        ///  &lt;embed src=&quot;URL&quot; quality=&quot;high&quot;
        ///    pluginspage=&quot;http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&quot;
        ///    type=&quot;application/x-shockwave-flash&quot; width=&quot;537&quot; height=&quot;111&quot;  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string FlashContext {
            get {
                return ResourceManager.GetString("FlashContext", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 請輸入您的帳號, 我們會將您的密碼寄至您的信箱.
        /// </summary>
        internal static string ForgetPasswordInstruction {
            get {
                return ResourceManager.GetString("ForgetPasswordInstruction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 系統無法處理您的帳號, 請重試. 或聯絡客服人員.
        /// </summary>
        internal static string ForgetPasswordNoUser {
            get {
                return ResourceManager.GetString("ForgetPasswordNoUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 您的17Life密碼.
        /// </summary>
        internal static string ForgetPasswordSubject {
            get {
                return ResourceManager.GetString("ForgetPasswordSubject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 已將您的密碼寄至您的信箱.
        /// </summary>
        internal static string ForgetPasswordSuccess {
            get {
                return ResourceManager.GetString("ForgetPasswordSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 親愛的訪客您好, 我們無法確認您的大樓. 請問您的大樓是下列哪一棟呢?.
        /// </summary>
        internal static string Location1 {
            get {
                return ResourceManager.GetString("Location1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 您可以藉由查詢交易紀錄來確認您的訂單內容。.
        /// </summary>
        internal static string MemberArea1 {
            get {
                return ResourceManager.GetString("MemberArea1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 因為安全性考量, 請輸入您的密碼來確認資料的更新.
        /// </summary>
        internal static string MemberArea10 {
            get {
                return ResourceManager.GetString("MemberArea10", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to E-Mail 更改失敗, 此信箱可能已經有人註冊, 或是您提供了不合規格的電子信箱.
        /// </summary>
        internal static string MemberArea11 {
            get {
                return ResourceManager.GetString("MemberArea11", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 您可在此用您的紅利點數兌換商品, 或.
        /// </summary>
        internal static string MemberArea12 {
            get {
                return ResourceManager.GetString("MemberArea12", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 您可以在此查詢您的紅利點數累積情形，並且填寫您對商家的評鑑紀錄或是留言。.
        /// </summary>
        internal static string MemberArea2 {
            get {
                return ResourceManager.GetString("MemberArea2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 若您的個人基本資料有所更動，您可以在此修改您的個人基本資料。.
        /// </summary>
        internal static string MemberArea3 {
            get {
                return ResourceManager.GetString("MemberArea3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 您可以在此修改變更您的密碼.
        /// </summary>
        internal static string MemberArea4 {
            get {
                return ResourceManager.GetString("MemberArea4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 此欄不可空白.
        /// </summary>
        internal static string MemberArea5 {
            get {
                return ResourceManager.GetString("MemberArea5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 密碼不符.
        /// </summary>
        internal static string MemberArea6 {
            get {
                return ResourceManager.GetString("MemberArea6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 舊密碼錯誤.
        /// </summary>
        internal static string MemberArea7 {
            get {
                return ResourceManager.GetString("MemberArea7", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 密碼更改完畢.
        /// </summary>
        internal static string MemberArea8 {
            get {
                return ResourceManager.GetString("MemberArea8", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 您的密碼已經更新!.
        /// </summary>
        internal static string MemberArea9 {
            get {
                return ResourceManager.GetString("MemberArea9", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 很抱歉，目前的時間商家無法接單，&lt;a href=&quot;order/seller_default.aspx&quot; class=&quot;orange15&quot;&gt;請選擇其他商家&lt;/a&gt;。.
        /// </summary>
        internal static string MsgBizHourClosed {
            get {
                return ResourceManager.GetString("MsgBizHourClosed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 很抱歉，此商家訂單已滿, 無法再接單，&lt;a href=&quot;order/seller_default.aspx&quot; class=&quot;orange15&quot;&gt;請選擇其他商家或其他時段&lt;/a&gt;。.
        /// </summary>
        internal static string MsgBizHourFull {
            get {
                return ResourceManager.GetString("MsgBizHourFull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 如有任何問題請與我們客服人員聯絡, 謝謝. 17Life客服專線：(02)3316-9105.
        /// </summary>
        internal static string MsgContactUs {
            get {
                return ResourceManager.GetString("MsgContactUs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 很抱歉, 由於某些技術上的原因, 您所要求的網頁發生錯誤,&lt;br/&gt;
        ///系統已自動通報我們的技術人員. 造成您的不便敬請見諒..
        /// </summary>
        internal static string MsgException {
            get {
                return ResourceManager.GetString("MsgException", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 您所要求的團購不存在，或是已經結束.
        /// </summary>
        internal static string MsgGroupOrderError {
            get {
                return ResourceManager.GetString("MsgGroupOrderError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 點擊可放大圖片並查看商品詳細資料.
        /// </summary>
        internal static string MsgItemPhotoClick {
            get {
                return ResourceManager.GetString("MsgItemPhotoClick", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 您無進入此頁面 ({0}) 的權限..
        /// </summary>
        internal static string MsgNoAccess {
            get {
                return ResourceManager.GetString("MsgNoAccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 非常抱歉, 此商家已無法配送, 請另選其他商家. 造成您的不便敬請見諒..
        /// </summary>
        internal static string MsgNoDeliveryTimeError {
            get {
                return ResourceManager.GetString("MsgNoDeliveryTimeError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 很抱歉, 此商家菜單仍在準備中, 我們會儘快將其上線..
        /// </summary>
        internal static string MsgNoMenu {
            get {
                return ResourceManager.GetString("MsgNoMenu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 您所要求的網頁 ({0}) 不存在..
        /// </summary>
        internal static string MsgPageNotFound {
            get {
                return ResourceManager.GetString("MsgPageNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 注意：因為您尚未選擇您的所在地點, 因此此商家也許不在您的配送範圍內!&lt;br/&gt;&lt;br/&gt;.
        /// </summary>
        internal static string NoLocation {
            get {
                return ResourceManager.GetString("NoLocation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 由於您目前累計金額未達該賣家最低配送金額，無法接受訂單，請修改您的訂單總額。.
        /// </summary>
        internal static string OrderMinimumNotReached {
            get {
                return ResourceManager.GetString("OrderMinimumNotReached", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 由於配送時間較難預估，請給予賣家前後15分鐘的時間作為彈性時段。.
        /// </summary>
        internal static string PayConfirm1 {
            get {
                return ResourceManager.GetString("PayConfirm1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 17Life客服專線  (02)3316-9105.
        /// </summary>
        internal static string PayConfirm10 {
            get {
                return ResourceManager.GetString("PayConfirm10", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 訂購人：{0} / 分機 {1}
        ///&lt;br/&gt;&lt;br/&gt;
        ///您的商品已經加入團購訂單！&lt;br/&gt;加點商品請回訂購頁面，若要修改請聯繫邀請您的主購人&lt;br /&gt;為維持訂單安全，恕無法讓您直接修改訂單。.
        /// </summary>
        internal static string PayConfirm11 {
            get {
                return ResourceManager.GetString("PayConfirm11", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 您的訂單正在送出中, 請稍待.
        /// </summary>
        internal static string PayConfirm12 {
            get {
                return ResourceManager.GetString("PayConfirm12", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 請選擇配送日期!.
        /// </summary>
        internal static string PayConfirm13 {
            get {
                return ResourceManager.GetString("PayConfirm13", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;2&quot; cellspacing=&quot;2&quot;&gt;
        ///                &lt;tr&gt;
        ///                  &lt;td align=&quot;right&quot; valign=&quot;top&quot;&gt;&lt;img src=&quot;../Themes/default/images/17Life/B9/B9_Icon_yellow.jpg&quot; width=&quot;18&quot; height=&quot;18&quot;&gt;&lt;/td&gt;
        ///                  &lt;td valign=&quot;top&quot; class=&quot;ablack2&quot;&gt;&lt;b&gt;感謝您使用17Life訂購餐點，我們已經將&lt;font color=&quot;#FF9100&quot;&gt;訂購確認信&lt;/font&gt;寄到您的eMail信箱中。&lt;br&gt;
        ///                  &lt;/b&gt;&lt;/td&gt;
        ///                &lt;/tr&gt;
        ///                &lt;tr&gt;
        ///                  &lt;td align=&quot;right&quot; valign=&quot;top&quot;&gt;&lt;img src=&quot;../Themes/default/im [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string PayConfirm14 {
            get {
                return ResourceManager.GetString("PayConfirm14", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;2&quot; cellspacing=&quot;2&quot;&gt;
        ///                  &lt;tr&gt;
        ///                    &lt;td align=&quot;right&quot; valign=&quot;top&quot;&gt;&lt;img src=&quot;../Themes/default/images/17Life/B9/B9_Icon_yellow.jpg&quot; width=&quot;18&quot; height=&quot;18&quot;&gt;&lt;/td&gt;
        ///                    &lt;td valign=&quot;top&quot; class=&quot;ablack2&quot;&gt;&lt;b&gt;感謝您使用17Life來蒐集團購訂單，我們已經將&lt;font color=&quot;#FF9100&quot;&gt;&amp;quot;訂單明細&amp;quot;&lt;/font&gt;寄到您的eMail信箱中。&lt;br&gt;
        ///                    &lt;/b&gt;&lt;/td&gt;
        ///                  &lt;/tr&gt;
        ///                  &lt;tr&gt;
        ///                    &lt;td align=&quot;right&quot; valign=&quot;top&quot;&gt;&lt;i [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string PayConfirm15 {
            get {
                return ResourceManager.GetString("PayConfirm15", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 感謝您的訂購，我們已將這次的訂購明細寄到您的信箱中。.
        /// </summary>
        internal static string PayConfirm16 {
            get {
                return ResourceManager.GetString("PayConfirm16", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 您訂購 “自行訂購商店”，再次提醒您要自行打電話訂購或將訂單列印傳真給商家做確認。謝謝！.
        /// </summary>
        internal static string PayConfirm17 {
            get {
                return ResourceManager.GetString("PayConfirm17", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 我們已保留您本次欲訂購的商品數量，若您選擇轉帳或匯款
        ///煩請於&lt;span class=&quot;font_f27c2f&quot;&gt;3個工作天&lt;/span&gt;內將款項匯至指定帳戶。.
        /// </summary>
        internal static string PayConfirm18 {
            get {
                return ResourceManager.GetString("PayConfirm18", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 您所選擇付款方式為貨到付款，再次提醒您到 &lt;a href=&quot;../user&quot; class=&quot;ablack2&quot; &gt;會員專區&lt;a&gt; 確認
        ///地址電話是否正確。.
        /// </summary>
        internal static string PayConfirm19 {
            get {
                return ResourceManager.GetString("PayConfirm19", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 如果您對上列商品有任何特別的要求，請於備註說明中填寫！.
        /// </summary>
        internal static string PayConfirm2 {
            get {
                return ResourceManager.GetString("PayConfirm2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 您所選擇付款方式為信用卡付款。.
        /// </summary>
        internal static string PayConfirm20 {
            get {
                return ResourceManager.GetString("PayConfirm20", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 我們將直接通知商店出貨到您指定的地點，謝謝!.
        /// </summary>
        internal static string PayConfirm21 {
            get {
                return ResourceManager.GetString("PayConfirm21", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to (請提供收據給我!).
        /// </summary>
        internal static string PayConfirm3 {
            get {
                return ResourceManager.GetString("PayConfirm3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 本次紅利折抵使用.
        /// </summary>
        internal static string PayConfirm4 {
            get {
                return ResourceManager.GetString("PayConfirm4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ＊注意： 每次使用以100點為單位，每100點可以直接折抵10元應付金額！.
        /// </summary>
        internal static string PayConfirm5 {
            get {
                return ResourceManager.GetString("PayConfirm5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 請選擇配送時段!.
        /// </summary>
        internal static string PayConfirm6 {
            get {
                return ResourceManager.GetString("PayConfirm6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 【注　意】\n\n　謝謝您的訂購,您所選擇的配送時間為『.
        /// </summary>
        internal static string PayConfirm7 {
            get {
                return ResourceManager.GetString("PayConfirm7", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 』\n\n☆由於商家配送狀況極有可能因外在因素(天氣,交通)所影響，請以商家與您確認的配送時間為主☆.
        /// </summary>
        internal static string PayConfirm8 {
            get {
                return ResourceManager.GetString("PayConfirm8", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 並請您在享受完餐點之後，給予賣家您的評價，共同創造更好的賣家服務品質及餐點品質，謝謝!.
        /// </summary>
        internal static string PayConfirm9 {
            get {
                return ResourceManager.GetString("PayConfirm9", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 此訂單尚未達到最低額度 {0} 元.
        /// </summary>
        internal static string PayError1 {
            get {
                return ResourceManager.GetString("PayError1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 此商家目前無法接受訂單.
        /// </summary>
        internal static string PayError2 {
            get {
                return ResourceManager.GetString("PayError2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 此訂單尚未達到最低訂購數量 {0} 項.
        /// </summary>
        internal static string PayError3 {
            get {
                return ResourceManager.GetString("PayError3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 以下列表是您所訂購的商品.
        /// </summary>
        internal static string PleaseCheck {
            get {
                return ResourceManager.GetString("PleaseCheck", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 我們已保留您本次欲訂購的商品數量，煩請於&lt;strong&gt; 3 &lt;/strong&gt;個工作天內將款項匯至指定帳戶&lt;br /&gt;
        ///==================================&lt;br /&gt;
        ///匯款帳戶：台新銀行(812) 營業部&lt;br /&gt;
        ///匯款戶名：康迅數位整合股份有限公司&lt;br /&gt;
        ///匯款帳號：001-01-072340-7-00&lt;br /&gt;
        ///==================================&lt;br /&gt;
        ///在您完成匯款後，敬請主動來信至&lt;a href=&quot;mailto://buymore_payeasy@payeasy.com.tw&quot;&gt;buymore_payeasy@payeasy.com.tw&lt;/a&gt;「團購客服信箱」
        ///註明：匯款日期、匯款金額、匯款戶名、匯款帳號後五碼等資料，以利匯款確認。&lt;br /&gt;
        ///待收到全額款項並確認無誤後，我們即會安排出貨。 &lt;br /&gt;
        ///若您有任何疑問，請來信至&lt;a href=&quot;mailto://buymore_payeasy@payeasy.com.tw&quot;&gt;buymore_payeasy@payeasy.com.tw&lt;/a&gt;「團購 [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string PzPaymentInfo {
            get {
                return ResourceManager.GetString("PzPaymentInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 讓我們一起加入17Life來一起生活吧！
        ///我們可以一起團購各地的美食，
        ///一起團購讓我們美美的保養品，
        ///每天也可以一起訂午餐晚餐，一起吃雞排喝下午茶！
        ///每天每天都可以一起生活，省錢團購哦！.
        /// </summary>
        internal static string RecLnkMsg {
            get {
                return ResourceManager.GetString("RecLnkMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 讓我們一起加入＂17Life＂，一起省錢團購去！.
        /// </summary>
        internal static string RecLnkTitle {
            get {
                return ResourceManager.GetString("RecLnkTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 看看我的區域內有哪些商家可供選擇.
        /// </summary>
        internal static string SeeStoreInArea {
            get {
                return ResourceManager.GetString("SeeStoreInArea", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 賣家已不接受訂購, 請選擇其他訂購方式或是&lt;a href=&quot;default.aspx&quot;&gt;其他商家&lt;/a&gt;, 感謝您的光臨.
        /// </summary>
        internal static string SellerClosed {
            get {
                return ResourceManager.GetString("SellerClosed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ☆請您將以下的文字與連結寄送給您的同仁，其他同仁即可透過連結進入您所選擇的團體訂購頁面開始訂購午餐/下午茶/晚餐。.
        /// </summary>
        internal static string SimpGrpOrd1 {
            get {
                return ResourceManager.GetString("SimpGrpOrd1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 請填入您的姓名與分機, 後按下送出按鈕即可結束訂購.
        /// </summary>
        internal static string SimpGrpOrderReminder {
            get {
                return ResourceManager.GetString("SimpGrpOrderReminder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 抱歉, 您無法在此商家開啟團購 ,有以下幾種可能：&lt;br /&gt;1. 您仍未選擇您的所在地&lt;br /&gt;2. 您仍未登入會員狀能。&lt;br /&gt;3. 此商家無法配送到您所在區域.
        /// </summary>
        internal static string SimpGrpOrdError1 {
            get {
                return ResourceManager.GetString("SimpGrpOrdError1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 為必填項目，請務必填寫確實以免遞送錯誤.
        /// </summary>
        internal static string UserRegister1 {
            get {
                return ResourceManager.GetString("UserRegister1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 部分商家不懂英文，所以請用中文姓名註冊.
        /// </summary>
        internal static string UserRegister2 {
            get {
                return ResourceManager.GetString("UserRegister2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 請確實填寫公司名稱以利商家配送.
        /// </summary>
        internal static string UserRegister3 {
            get {
                return ResourceManager.GetString("UserRegister3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 請確實填寫部門別以利商家配送.
        /// </summary>
        internal static string UserRegister4 {
            get {
                return ResourceManager.GetString("UserRegister4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 服務條款 
        ///Lunch King 午餐王係依據本服務條款提供Lunch King 午餐王（http://www.lunchking.com.tw）服務 (以下簡稱「本服務」)。本網站由日太科技股份有限公司所有。當您開始使用本網站時，即表示您已同意接受本網站的服務條款。
        ///當您違反本網站所聲明之服務條款時，本網站有權力終止您使用本網站的所有服務。
        ///
        ///資料連結 
        ///Lunch King 午餐王提供之服務，目的在於便利您可以快速的享受到外送餐點的服務。您可能會因本網站頁面上的連結而因此連結至其他業者經營的網站，但不表示Lunch King 午餐王與該等業者有任何關係。
        ///Lunch King 午餐王隨時會與其他公司(餐飲公司)、廠商等第三人（「內容提供者」）合作，由其提供內容供Lunch King 午餐王刊登，Lunch King 午餐王於刊登時均將註明內容提供者。基於尊重內容提供者之智慧財產權，Lunch King 午餐王對其所提供之內容並不做實質之審查或修改，對該等內容之正確真偽亦不負任何責任。對該等內容之正確真偽，您宜自行判斷之。您若認為某些內容涉及侵權或有所不實，請逕向該內容提供者反應意見。
        ///當您透 [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string UserRegister5 {
            get {
                return ResourceManager.GetString("UserRegister5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 歡迎您加入Lunch King午餐王會員的行列，以後您只要登入會員，就可以直接到您所屬的路段首頁去選購您所需要的餐點，每天享受用滑鼠訂餐的便利哦。.
        /// </summary>
        internal static string UserRegister6 {
            get {
                return ResourceManager.GetString("UserRegister6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 親愛的訪客您好，餐點的配送由於時間以及距離的考量，賣家無法配送餐點到每一個地區或大樓&lt;br&gt;
        ///─ 如果您目前所在的區域並非我們可以配送餐點的地方，我們在此向您致歉，我們將會致力於增加配送的地點，希望在日後能夠有榮幸為您服務。&lt;br&gt;
        ///─如果您可以在下方的選項找到您所在的大樓，請您花一分鐘的時間完成會員註冊程序，以後您將可以每天享受用滑鼠訂餐的便利。.
        /// </summary>
        internal static string UserRegister7 {
            get {
                return ResourceManager.GetString("UserRegister7", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 17Life服務條款
        ///
        ///歡迎您來使用17Life！在您開始使用17Life前，您必須閱讀並同意下列17Life服務條款(以下稱「本條款」)，遵守下列條款、細則和政策的規定，亦同時包含任何未來新增或是修改的項目。17Life係依據本服務條款提供17Life（https://www.17Life.com.tw）服務 (以下簡稱「本服務」)。本網站由日太科技股份有限公司所有。當您開始使用本網站時，即表示您已同意接受本網站的服務條款。當您違反本網站所聲明之服務條款時，本網站有權力終止您使用本網站的所有服務。
        ///
        ///	PayEasy.com會員約定條款https://www.payeasy.com.tw/MyPayeasy/member/MemberGuide.jsp
        ///
        ///因本網站與PayEasy.com有深度合作關係合約，而PayEasy.com有權隨時增訂或修改「PayEasy.com會員約定條款」，您亦同意受到這增訂或修改之約束。若您不願意接受或遵守「PayEasy.com會員約定條款」，則您不得使用17Life所提供之服務。當「本條款」與「PayEasy.com會員約定條款」有相互抵觸時，則以「本條款」 [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string UserRegister8 {
            get {
                return ResourceManager.GetString("UserRegister8", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 我要推薦商家.
        /// </summary>
        internal static string WantToRecommend {
            get {
                return ResourceManager.GetString("WantToRecommend", resourceCulture);
            }
        }
    }
}

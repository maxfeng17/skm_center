﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web
{
    public partial class ATMNotify : System.Web.UI.Page
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (AccessCheck())
            {
                if (Request.RequestType == "POST")
                {
                    NameValueCollection nvc = Request.Form;
                    int returncode = 543;
                    using (TransactionScope ts = new TransactionScope())
                    {
                        returncode = PaymentFacade.CtAtmLogSave(nvc["TransactionNo"], nvc["MsgID"], nvc["body"]);
                        if (returncode != 543)
                            ts.Complete();
                    }
                    Response.StatusCode = returncode;
                    Label1.Text += @"<br />StatusCode=" + returncode.ToString();
                    Label1.Text += @"<br />TransactionNo=" + nvc["TransactionNo"];
                    Label1.Text += @"<br />MsgID=" + nvc["MsgID"];
                    Label1.Text += @"<br />body=" + nvc["body"];
                }        
            }
            else
            {
                Label1.Text += Request.UserHostAddress.ToString();
            }
        }

        protected bool AccessCheck()
        {
            string[] allow = new string[] { "127.0.0.1", "61.63.22.6", "203.66.181.146", "192.168.1.1", 
                "175.184.241.6", "175.184.241.146", "175.184.243.6", "175.184.243.146", "175.184.245.141", config.AtmTestPostIp };
            string ip=Request.UserHostAddress.ToString();
            for (int i = 0; i < allow.Length;i++ )
            {
                if(ip==allow[i])
                    return true;
            }
            return false; 
        }
    }
}
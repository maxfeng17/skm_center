﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="LunchKingSite.Web.Error" EnableViewState="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>17Life</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link href="<%=ResolveUrl("~/Themes/PCweb/css/MasterPage.css") %>" rel="stylesheet" type="text/css" />
	<link href="<%=ResolveUrl("~/Themes/PCweb/css/ppon.css") %>" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="errpage-bg">
		<div class="errpage-note">
			<h1>
				<asp:Literal ID="litTitle" runat="server" Text="噢噢！好像出了點小狀況。" /></h1>
			<p>
				<asp:Literal ID="lMsg" runat="server" Text="<%$Resources:Message, MsgException%>" /></p>
			<%
				if (ErrorCode != "404")
				{
					Response.Write("<p class='errpage-code'>錯誤代碼：" + ErrorCode + "</p>");
				}
				else
				{
					Response.Write("<div id='rp' style='padding: 28px 0px 28px 0px; font-size: 28px'>5 秒鐘後回到首頁</div>");
					Response.Write(@"
	<script type='text/javascript'>
		window.countdown = 5;
		window.setInterval(function () {
			if (window.countdown > 0) window.countdown--;
			document.getElementById('rp').innerText = window.countdown.toString() + ' 秒鐘後回到首頁';
			if (window.countdown == 0) {
				location.href = '/';
			}
		}, 1000);
	</script>
");
				}
			%>
			<p class="link">
				<a href="javascript:history.back();">返回上一頁</a> │
				<a href="<%=ResolveUrl("/")%>">17Life 首頁</a> │
				<a href="<%=ResolveUrl("~/piinlife/default.aspx")%>">PiinLife 首頁</a> │
				<a href="<%=SystemConfig.SiteUrl + SystemConfig.SiteServiceUrl%>">客服中心</a>
			</p>
		</div>
	</div>

	<div class="errpage-footer">
		<div class="footer_content">
			<p class="copyright">版權所有 ©
				<script language="javascript"> var Today = new Date(); document.write(Today.getFullYear()); </script>
				康太數位整合股份有限公司</p>
			<div class="all-logo">17Life &amp; PiinLife</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!--footer-->
</body>
</html>

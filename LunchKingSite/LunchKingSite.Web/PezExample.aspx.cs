﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web
{

    public partial class PezExample : System.Web.UI.Page
    {
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        protected void Page_Load(object sender, EventArgs e)
        {
            merchantContent.Text = Request.QueryString["m"];
            memNum.Text = Request.QueryString["mn"] ?? "";
        }
    }
}
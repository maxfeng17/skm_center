﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;

namespace LunchKingSite.Web.Service
{
    /// <summary>
    /// Summary description for einvoiceaccount
    /// </summary>
    public class einvoiceaccount : IHttpHandler
    {
        const string CardType = "EG0071";
        const string ComUniqueId = "24317014";
        public void ProcessRequest(HttpContext context)
        {
            string username = string.Empty;
            if (!string.IsNullOrEmpty(username = context.Request.QueryString["username"]) && username == context.User.Identity.Name)
            {
                int uniqueid = MemberFacade.GetUniqueId(username);
                EasyDigitCipher cipher = new EasyDigitCipher();
                string token = cipher.Encrypt(string.Format("{0}{1}{2}", DateTime.Now.Month, uniqueid, DateTime.Now.Day));
                context.Response.ContentType = "text/html";
                StringBuilder builder = new StringBuilder();
                builder.AppendLine("<!DOCTYPE html><html><head><meta charset=\"utf-8\"></head>");
                builder.AppendLine("<body><form name=\"einvoice\" method=\"post\" action=\"https://www.einvoice.nat.gov.tw/APMEMBERVAN/membercardlogin\">");
                builder.AppendLine("<input type=\"hidden\" name=\"card_ban\" value=\"" + ComUniqueId + "\">");
                builder.AppendLine("<input type=\"hidden\" name=\"card_type\" value=\"" + GetBase64String(CardType) + "\">");
                builder.AppendLine("<input type=\"hidden\" name=\"card_no1\" value=\"" + GetBase64String(uniqueid.ToString()) + "\">");
                builder.AppendLine("<input type=\"hidden\" name=\"card_no2\" value=\"" + GetBase64String(uniqueid.ToString()) + "\">");
                builder.AppendLine("<input type=\"hidden\" name=\"back_url\" value=\"\">");
                builder.AppendLine("<input type=\"hidden\" name=\"token\" value=\"" + token + "\">");
                builder.AppendLine("</form><script>document.einvoice.submit();</script></body></html>");
                context.Response.Write(builder.ToString());
            }
            else
            {
                context.Response.StatusCode = 404;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public string GetBase64String(string input)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(input));
        }
    }
}
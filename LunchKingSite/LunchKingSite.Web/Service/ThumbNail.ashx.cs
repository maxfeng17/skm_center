﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using LunchKingSite.BizLogic.Component;
using System.Net.Mail;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.Service
{
    /// <summary>
    /// Summary description for ThumbNail
    /// </summary>
    public class ThumbNail : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            Guid bid;
            string querystring;
            context.Response.ContentType = "text/jpeg";
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();
            Guid.TryParse(string.IsNullOrEmpty(querystring = context.Request.QueryString["bid"]) ? string.Empty : querystring, out bid);
            try
            {
                // 要有圖檔最後修改時間的依據，暫用dp的修改時間
                DateTime modifedTime = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true).EventModifyTime;
                if (!String.IsNullOrEmpty(context.Request.Headers["If-Modified-Since"]))
                {
                    CultureInfo provider = CultureInfo.InvariantCulture;
                    var lastMod = DateTime.ParseExact(context.Request.Headers["If-Modified-Since"], "r", provider).ToLocalTime();
                    //if (lastMod == modifedTime) { 我己前是這樣寫的，但現在卻會有小小的誤差讓相等不成立。原因尚待了解。
                    if (new TimeSpan(modifedTime.Ticks - lastMod.Ticks).Seconds < 3)
                    {
                        context.Response.StatusCode = 304;
                        context.Response.StatusDescription = "Not Modified";
                        return;
                    }
                }
                context.Response.Cache.SetCacheability(HttpCacheability.Public);
                context.Response.Cache.SetLastModified(modifedTime);
                ViewPponDealManager.DefaultManager.ThumbNailImageGetByBid(bid).ShowThumbNailImage(context.Response.OutputStream);
            }
            catch (Exception error)
            {
                MailMessage msg = new MailMessage();
                msg.To.Add(config.AdminEmail);
                msg.Subject = "ThumbNail Service Error";
                msg.From = new MailAddress(config.AdminEmail);
                msg.IsBodyHtml = true;
                msg.Body = error.Message;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
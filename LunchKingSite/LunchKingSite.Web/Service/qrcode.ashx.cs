﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using LunchKingSite.Core.Component;
using com.google.zxing;
using com.google.zxing.common;

namespace LunchKingSite.Web.Service
{
    /// <summary>
    /// Summary description for qrcode
    /// </summary>
    public class qrcode : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            EasyDigitCipher cipher = new EasyDigitCipher();
            string encodedCode = context.Request["code"];
            string code;
            int dQRHeight = 100;
            int dQRWeight = 100;
            if (string.IsNullOrEmpty(encodedCode))
            {
                Return404(context);
                return;
            }
            else
            {
                string[] encodeedcodelist = encodedCode.Split(new string[] { "-" }, StringSplitOptions.None);
                string encypt_code = encodeedcodelist.Skip(2).Take(1).DefaultIfEmpty(string.Empty).First();
                if (!cipher.Decrypt(encypt_code, encodedCode, out code))
                {
                    Return404(context);
                    return;
                }
                else
                {
                    List<string> code_source = new List<string>();
                    code_source.Add(encodeedcodelist.Skip(0).Take(1).DefaultIfEmpty(string.Empty).First());
                    code_source.Add(encodeedcodelist.Skip(1).Take(1).DefaultIfEmpty(string.Empty).First());
                    code_source.Add(code);
                    IEnumerable<string> actual_codes = code_source.Where(x => !string.IsNullOrEmpty(x));
                    Image img = CreateQRCode(actual_codes.Count() > 0 ? actual_codes.Aggregate((current, next) => current + "-" + next) : string.Empty, dQRHeight, dQRWeight);
                    context.Response.ContentType = "image/png";
                    byte[] b = GetImageBytes(img);
                    context.Response.OutputStream.Write(b, 0, b.Length);
                    img.Dispose();
                }
            }

        }

        private void Return404(HttpContext context)
        {
            context.Response.StatusCode = 404;
        }

        private Bitmap CreateQRCode(string code, int dQRHeight, int dQRWeight)
        {
            ByteMatrix byteMatrix = new MultiFormatWriter().encode(code, BarcodeFormat.QR_CODE, dQRHeight, dQRWeight);
            int width = byteMatrix.Width;
            int height = byteMatrix.Height;

            Bitmap bmap = new Bitmap(width, height, PixelFormat.Format32bppArgb);

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    bmap.SetPixel(x, y, byteMatrix.get_Renamed(x, y) != -1 ? ColorTranslator.FromHtml("Black") : ColorTranslator.FromHtml("White"));
                }
            }
            return bmap.Clone(new Rectangle(16, 16, 68, 68), PixelFormat.Format32bppArgb);
        }

        private byte[] GetImageBytes(Image image)
        {
            ImageCodecInfo codec = null;

            foreach (ImageCodecInfo e in ImageCodecInfo.GetImageEncoders())
            {
                if (e.MimeType == "image/png")
                {
                    codec = e;
                    break;
                }
            }

            using (EncoderParameters ep = new EncoderParameters())
            {
                ep.Param[0] = new EncoderParameter(Encoder.Quality, 100L);

                using (MemoryStream ms = new MemoryStream())
                {
                    image.Save(ms, codec, ep);
                    return ms.ToArray();
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using Winnovative.WnvHtmlConvert;
using System.Web.Security;
namespace LunchKingSite.Web.Service
{
    public class refundform : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            if (string.IsNullOrEmpty(context.User.Identity.Name))
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }
            Guid orderGuid;
            if (!string.IsNullOrEmpty(context.Request.QueryString["oid"]))
            {
                if (!Guid.TryParse(context.Request.QueryString["oid"], out orderGuid))
                {
                    context.Response.StatusCode = 404;
                    return;
                }
                else
                {
                    string fileName = "退貨申請書";
                    string userName;
                    if (string.IsNullOrEmpty(context.Request.QueryString["u"]))
                    {
                        userName = context.User.Identity.Name;
                    }
                    else
                    {
                        userName = context.Request.QueryString["u"];
                    }
                    string requestUrl = "/user/refund_form.aspx?oid=" + orderGuid.ToString() + "&u=" + userName;
                    PdfConverter pdfc = new PdfConverter();
                    pdfc.LicenseKey = @"YklTQlNCVFZCVExSQlFTTFNQTFtbW1s=";
                    pdfc.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                    pdfc.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                    pdfc.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;

                    // disable unnecessary features to enhance performance
                    pdfc.ScriptsEnabled = pdfc.ScriptsEnabledInImage = false;
                    pdfc.ActiveXEnabled = pdfc.ActiveXEnabledInImage = false;
                    pdfc.PdfDocumentOptions.JpegCompressionEnabled = false;

                    byte[] b = pdfc.GetPdfFromUrlBytes(WebUtility.GetSiteRoot() + requestUrl);

                    context.Response.Buffer = false;
                    context.Response.ClearHeaders();
                    context.Response.ClearContent();
                    context.Response.ContentType = "application/pdf";
                    context.Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(fileName) + ".pdf");
                    context.Response.AddHeader("Content-Length", b.Length.ToString());
                    context.Response.BinaryWrite(b);
                    context.Response.Flush();
                }
            }
        }
        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}
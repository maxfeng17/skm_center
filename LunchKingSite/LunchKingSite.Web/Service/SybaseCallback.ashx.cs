﻿using System;
using System.Web;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.Service
{
    public class SybaseCallback : IHttpHandler
    {
        private ILog logger = LogManager.GetLogger(typeof(SybaseCallback).Name);
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        public void ProcessRequest(HttpContext context)
        {
            //失敗
            //CUSTOMERID=26529&ORDERID=1946851430&STATUS=0xDB65+Message+2+of+orderid+1946851430+for+number+%2b886999999999+rejected+by+Operator&NBMESSAGE=2&SUBJECT=N%2fA&MESSAGEID=2&MSISDN=%2b886999999999&DATE=04-12-2014&TIME=09%3a32%3a04&MOBILEACK=1
            //成功
            //CUSTOMERID=26529&ORDERID=1010680165&STATUS=Message+1+of+the+order+1010680165+at+destination+of+%2b886918638954+has+been+received+at+04%3a51%3a23+(CET)+on+the+05-12-2014.&NBMESSAGE=1&SUBJECT=N%2fA&MESSAGEID=1&MSISDN=%2b886918638954&DATE=05-12-2014&TIME=04%3a51%3a23&MOBILEACK=1
            HttpRequest Request = context.Request;
            bool isMobileAck = Request.QueryString["MOBILEACK"] == "1";
            
            if (isMobileAck)
            {
                string smsOrderId = Request.QueryString["ORDERID"];
                bool success = Request.QueryString["STATUS"].Contains("received");
                string phoneNumber = GetLocalPhoneNumber(Request.QueryString["MSISDN"]);
                DateTime? resultTime = GetDateTimeFromCenturalEurope(Request.QueryString["DATE"], Request.QueryString["TIME"]);
                if (resultTime != null)
                {
                    SmsStatus status = SmsStatus.Success;
                    SmsFailReason reason = SmsFailReason.None;
                    if (success == false)
                    {
                        status = SmsStatus.Fail;
                        reason = SmsFailReason.Rejected;
                    }
                    try
                    {
                        SmsLog smsLog = pp.SmsLogGet(smsOrderId, phoneNumber);
                        if (smsLog != null)
                        {
                            smsLog.Status = (int)status;
                            smsLog.FailReason = (int)reason;
                            smsLog.ResultTime = resultTime;
                            pp.SMSLogSet(smsLog);
                        }
                        //logger.Info("更新簡訊狀態完成，參數: " + Request.QueryString);
                    }
                    catch (Exception ex)
                    {
                        logger.Info("更新簡訊狀態失敗，異動失敗，參數: " + Request.QueryString, ex);
                    }
                }
                else
                {
                    logger.Info("更新簡訊狀態失敗，參數不完整，參數: " + Request.QueryString);
                }
            }
        }

        private string GetLocalPhoneNumber(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber))
            {
                return string.Empty;
            }
            return phoneNumber.Replace("+886", "0");
        }

        public bool IsReusable
        {
            get { return false; }
        }

        private DateTime? GetDateTimeFromCenturalEurope(string date, string time)
        {
            if (string.IsNullOrEmpty(date) || string.IsNullOrEmpty(time))
            {
                return null;
            }
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("Central European Standard Time");
            string str = date + " " + HttpUtility.UrlDecode(time);
            try
            {
                DateTime dt = DateTime.ParseExact(str, "dd-MM-yyyy HH:mm:ss", null);
                DateTime result = TimeZoneInfo.ConvertTimeToUtc(dt, tzi).ToLocalTime();
                return result;
            }
            catch
            {
                logger.Info("GetDateTimeFromCenturalEurope，DateTime dt Parse error，time: " + time + ",str:" + str);
                return null;
            }    
        }

    }
}
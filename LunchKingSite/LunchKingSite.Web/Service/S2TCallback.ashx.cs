﻿using System;
using System.Web;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.Service
{
    public class S2TCallback : IHttpHandler
    {
        private ILog logger = LogManager.GetLogger(typeof(S2TCallback).Name);
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        public void ProcessRequest(HttpContext context)
        {
            HttpRequest Request = context.Request;
            try
            {
                string smsOrderId = Request.Params["msgID"];
                bool success = Request.Params["status"].Equals("O"); // O = sucess F = false R = sending
                string phoneNumber = GetLocalPhoneNumber(Request.Params["phone"]);
                string format = "yyyyMMddHHmmss";
                DateTime? resultTime = DateTime.ParseExact(Request.Params["acktime"], format, System.Globalization.CultureInfo.InvariantCulture);
                if (resultTime != null)
                {
                    SmsStatus status = SmsStatus.Success;
                    SmsFailReason reason = SmsFailReason.None;
                    if (success == false)
                    {
                        string response_status = Request.Params["status"];
                        switch (response_status)
                        {
                            case "F":
                                status = SmsStatus.Fail;
                                reason = SmsFailReason.Rejected;
                                break;
                            case "R":
                                status = SmsStatus.Sending;
                                reason = SmsFailReason.None;
                                break;
                        }
                    }
                    try
                    {
                        SmsLog smsLog = pp.SmsLogGet(smsOrderId, phoneNumber);
                        if (smsLog != null)
                        {
                            smsLog.Status = (int)status;
                            smsLog.FailReason = (int)reason;
                            smsLog.ResultTime = resultTime;
                            pp.SMSLogSet(smsLog);
                        }
                        //logger.Info("更新簡訊狀態完成，參數: " + Request.QueryString);
                    }
                    catch (Exception ex)
                    {
                        logger.Info("更新簡訊狀態失敗，異動失敗，參數: " + Request.QueryString, ex);
                    }
                    context.Response.Write(SmsStatus.Success.ToString());
                }
                else
                {
                    logger.Info("更新簡訊狀態失敗，參數不完整，參數: " + Request.QueryString);
                    context.Response.Write(SmsStatus.Fail.ToString());
                }
            }
            catch (Exception ex)
            {
                string keys = string.Empty;
                for (int i = 0; i < Request.Form.AllKeys.Length; i++)
                {
                    keys += (Request.Form.AllKeys[i] + "<br>");
                }
                logger.Info("不明錯誤，參數: " + Request.QueryString + "來源:" + HttpContext.Current.Request.UserHostAddress + "POST keys:" + keys, ex);
                context.Response.Write(SmsStatus.Fail.ToString());
            }
        }

        private string GetLocalPhoneNumber(string phoneNumber)
        {
            if (String.IsNullOrEmpty(phoneNumber))
            {
                return string.Empty;
            }
            return phoneNumber.Replace("886", "0");
        }
        public bool IsReusable
        {
            get { return false; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.Service
{
    /// <summary>
    /// barcodeEAN13 的摘要描述
    /// </summary>
    public class barcodeEAN13 : IHttpHandler
    {
        private string _code;
        private float _size;
        private int _imageWidth;
        private int _imageHigh;
        private float _barcodeHight;

        public void ProcessRequest(HttpContext context)
        {
            if (GetRequest(context))
            {
                System.Drawing.Bitmap barCodeBitmap = new Core.Component.BarCodeEAN13(_code).Paint(_size, _imageWidth, _imageHigh, _barcodeHight);

                MemoryStream oStream = new MemoryStream();
                barCodeBitmap.Save(oStream, ImageFormat.Png);

                context.Response.ClearContent();
                context.Response.ContentType = "image/png";

                context.Response.BinaryWrite(oStream.ToArray());
            }
            else 
            {
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private bool GetRequest(HttpContext context)
        {
            if (context.Request["code"].IsEmpty())
            {
                return false;
            }
            else 
            {

                float fSize = 1f, barcodeHeight = 0f;
                int imageWidth = 545, imageHight = 357;
                
                float.TryParse(context.Request["size"].IsEmpty() ? "1" : context.Request["size"], out fSize);
                int.TryParse(context.Request["width"].IsEmpty() ? "545" : context.Request["width"], out imageWidth);
                int.TryParse(context.Request["hight"].IsEmpty() ? "357" : context.Request["hight"], out imageHight);
                float.TryParse(context.Request["barcodehight"].IsEmpty() ? "0" : context.Request["barcodehight"], out barcodeHeight);

                _code = context.Request["code"];
                _size = fSize;
                _imageWidth = imageWidth;
                _imageHigh = imageHight;
                _barcodeHight = barcodeHeight;

                // barcode ean13 長度不可小於 12
                return _code.Length >= 12 ? true : false;
            }
        }
    }
}
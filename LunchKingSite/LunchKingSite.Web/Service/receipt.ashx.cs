﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using LunchKingSite.WebLib;
using Winnovative.WnvHtmlConvert;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.Web.Service
{
    /// <summary>
    /// Summary description for receipt
    /// </summary>
    public class receipt : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string requestUrl;
            if (context.Request.QueryString["mode"] == "piin")
            {
                requestUrl = string.Format("/piinlife/ReceiptPrint.aspx?ogid={0}&mode=piin", context.Request.QueryString["ogid"]);
            }
            else
            {
                requestUrl = string.Format("/Ppon/ReceiptPrint.aspx?oid={0}&user={1}", context.Request.QueryString["oid"], context.User.Identity.Name);
            }
            PdfConverter pdfc = new PdfConverter();
            pdfc.LicenseKey = @"YklTQlNCVFZCVExSQlFTTFNQTFtbW1s=";
            pdfc.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfc.PdfDocumentOptions.TopMargin = 50;
            pdfc.PdfDocumentOptions.LeftMargin = 0;
            pdfc.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfc.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            pdfc.ScriptsEnabled = true;
            if (context.Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                pdfc.HttpRequestHeaders = string.Format("Cookie : {0}={1}",
                        FormsAuthentication.FormsCookieName, context.Request.Cookies[FormsAuthentication.FormsCookieName].Value);

            byte[] b = pdfc.GetPdfFromUrlBytes(WebUtility.GetSiteRoot() + requestUrl);

            context.Response.Buffer = false;
            context.Response.ClearHeaders();
            context.Response.ClearContent();
            context.Response.ContentType = "application/pdf";
            string fileName = "收據.pdf";
            context.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            context.Response.AddHeader("Content-Length", b.Length.ToString());
            context.Response.BinaryWrite(b);
            context.Response.Flush();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
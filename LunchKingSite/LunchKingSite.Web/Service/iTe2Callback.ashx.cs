﻿using System;
using System.Web;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace LunchKingSite.Web.Service
{
    /// <summary>
    /// iTe2Callback 的摘要描述
    /// </summary>
    public class iTe2Callback : IHttpHandler
    {
        private ILog logger = LogManager.GetLogger(typeof(iTe2Callback).Name);
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        public void ProcessRequest(HttpContext context)
        {
            StreamReader reader = new StreamReader(context.Request.InputStream, Encoding.UTF8);
            string xmlRequest = reader.ReadToEnd();
            try
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(xmlRequest);
                XmlNodeList nodeList = xdoc.SelectNodes("//SmsStatus");
                foreach(XmlNode elm in nodeList)
                {
                    string smsOrderId = elm["SEQ"].InnerText;
                    bool success = elm["RES"].InnerText == "OK";

                    string phoneNumber = elm["DestAddress"].InnerText;
                    string format = "yyyyMMddHHmm";
                    DateTime? resultTime = DateTime.ParseExact(elm["CheckDate"].InnerText, format, System.Globalization.CultureInfo.InvariantCulture);
                    if (resultTime != null)
                    {
                        SmsStatus status = SmsStatus.Success;
                        SmsFailReason reason = SmsFailReason.None;
                        if (success == false)
                        {
                            string response_status = elm["RES"].InnerText;
                            switch (response_status)
                            {
                                case "FD":
                                    status = SmsStatus.Sending;
                                    reason = SmsFailReason.None;
                                    break;
                                case "CK":
                                    status = SmsStatus.Sending;
                                    reason = SmsFailReason.None;
                                    break;
                                case "ST":
                                    status = SmsStatus.Sending;
                                    reason = SmsFailReason.None;
                                    break;
                                case "RJ":
                                    status = SmsStatus.Sending;
                                    reason = SmsFailReason.None;
                                    break;
                                default:
                                    status = SmsStatus.Fail;
                                    reason = SmsFailReason.Rejected;
                                    break;
                            }
                        }
                        try
                        {
                            SmsLog smsLog = pp.SmsLogGet(smsOrderId, phoneNumber);
                            if (smsLog != null)
                            {
                                smsLog.Status = (int)status;
                                smsLog.FailReason = (int)reason;
                                smsLog.ResultTime = resultTime;
                                pp.SMSLogSet(smsLog);
                                context.Response.Write(SmsStatus.Success.ToString());
                            }
                            else
                            {
                                logger.Info("更新簡訊狀態失敗，無法找到此筆簡訊，smsOrderId:" + smsOrderId + "，phoneNumber:" + phoneNumber);
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Info("更新簡訊狀態失敗: " + ex.ToString(), ex);
                        }
                    }
                    else
                    {
                        logger.Info("更新resultTime失敗: " + xmlRequest.ToString());
                        context.Response.Write(SmsStatus.Fail.ToString());
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Info("不明錯誤，來源:" + HttpContext.Current.Request.UserHostAddress + ";" + context.Request.HttpMethod + ":" + xmlRequest.ToString() + ";" + ex.Message.ToString(), ex);
                context.Response.Write(SmsStatus.Fail.ToString());
            }
        }

        private string GetLocalPhoneNumber(string phoneNumber)
        {
            if (String.IsNullOrEmpty(phoneNumber))
            {
                return string.Empty;
            }
            return phoneNumber.Replace("886", "0");
        }

        public bool IsReusable
        {
            get { return false; }
        }
    }
}
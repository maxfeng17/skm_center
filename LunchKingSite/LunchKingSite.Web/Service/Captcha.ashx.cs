﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace LunchKingSite.Web.Service
{
    /// <summary>
    /// Summary description for Captcha
    /// </summary>
    public class Captcha : IHttpHandler, IReadOnlySessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            //move seesion to here
            Random r = new Random();
            context.Session["Captcha"] = r.Next(1000, 10000).ToString();

            if (context.Session["Captcha"] != null)
            {
                Bitmap bmpOut = new Bitmap(55, 25);
                Graphics g = Graphics.FromImage(bmpOut);
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.FillRectangle(Brushes.Black, 0, 0, 55, 25);
                g.DrawString(context.Session["Captcha"].ToString(), new Font("Verdana", 14), new SolidBrush(Color.White), 0, 0);
                MemoryStream ms = new MemoryStream();
                bmpOut.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                byte[] bmpBytes = ms.GetBuffer();
                bmpOut.Dispose();
                ms.Close();
                context.Response.BinaryWrite(bmpBytes);
            }
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
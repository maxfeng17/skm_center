﻿using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Winnovative.WnvHtmlConvert;

namespace LunchKingSite.Web.Service
{
    /// <summary>
    /// GetCreditNotePdf 的摘要描述
    /// </summary>
    public class GetCreditNotePdf : IHttpHandler
    {
        private static ILog logger = LogManager.GetLogger("GetCreditNotePdf");
        private static IItemProvider ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        public void ProcessRequest(HttpContext context)
        {
            string fileName = DateTime.Now.ToString("yyyyMMdd");

            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            Stream inputStream = context.Request.InputStream;
            Encoding encoding = context.Request.ContentEncoding;
            StreamReader streamReader = new StreamReader(inputStream, encoding);
            string strJson = streamReader.ReadToEnd();
            GetCreditNoteInput p = JsonConvert.DeserializeObject<GetCreditNoteInput>(strJson);
            string html = p.Html;
            string accessToken = p.AccessToken;

            OauthToken token = CommonFacade.OauthTokenGet(accessToken);
            if(token.IsLoaded == false)
            {
                context.Response.StatusCode = 404;
                return;
            }



            PdfConverter pdfc = new PdfConverter();
            pdfc.LicenseKey = @"YklTQlNCVFZCVExSQlFTTFNQTFtbW1s=";
            pdfc.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfc.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfc.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;

            pdfc.ScriptsEnabled = pdfc.ScriptsEnabledInImage = false;
            pdfc.ActiveXEnabled = pdfc.ActiveXEnabledInImage = false;
            pdfc.PdfDocumentOptions.JpegCompressionEnabled = false;


            //byte[] b = pdfc.GetPdfFromUrlBytes( requestUrl);

            var converter = new NReco.PdfGenerator.HtmlToPdfConverter();
            converter.PdfToolPath = System.IO.Path.GetTempPath();
            byte[] b;
            try
            {
                b = converter.GeneratePdf(html);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }

            context.Response.Buffer = false;
            context.Response.ClearHeaders();
            context.Response.ClearContent();
            context.Response.ContentType = "application/pdf";
            context.Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(fileName) + ".pdf");
            context.Response.AddHeader("Content-Length", b.Length.ToString());
            context.Response.BinaryWrite(b);
            context.Response.Flush();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

    public class GetCreditNoteInput
    {
        public string AccessToken { get; set; }
        public string Html { get; set; }
    }
}
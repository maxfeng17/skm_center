﻿using System;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using Winnovative.WnvHtmlConvert;

namespace LunchKingSite.Web.Service
{
    /// <summary>
    /// Summary description for HidealCoupon
    /// </summary>
    public class HidealCouponDownload : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            int? couponId;
            int dealId = 0;

            int i;
            couponId = string.IsNullOrEmpty(context.Request.QueryString["cid"]) ? null : int.TryParse(context.Request.QueryString["cid"], out i) ? new int?(i) : null;

            if (!string.IsNullOrEmpty(context.Request.QueryString["did"]))
            {
                try
                {
                    int did;
                    dealId = int.TryParse(context.Request.QueryString["did"], out did) ? did : 0;
                }
                catch { dealId = 0; }
            }

            if (couponId == null && dealId.Equals(0))
            {
                Return404(context);
                return;
            }

            string fileName = "coupon";
            string requestUrl = "/piinlife/member/CouponPrint.aspx?s=1&";
            if (couponId != null)
            {
                // if coupon doesn't exist or it's someone else's coupon, return 404
                ViewHiDealCoupon coupon = ProviderFactory.Instance().GetProvider<IHiDealProvider>().ViewHiDealCouponGet(couponId ?? 0);
                string username = GetUserName(context);
                if (coupon == null || !coupon.IsLoaded || coupon.UserName != username)
                {
                    Return404(context);
                    return;
                }
                fileName = (coupon.SellerName + "_" + coupon.Prefix + coupon.Sequence).Replace(" ", "_");
                requestUrl += "cid=" + couponId + "&u=" + username;
            }
            else
                requestUrl += "did=" + dealId;

            PdfConverter pdfc = new PdfConverter();
            pdfc.LicenseKey = @"YklTQlNCVFZCVExSQlFTTFNQTFtbW1s=";
            pdfc.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfc.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfc.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;

            // disable unnecessary features to enhance performance
            pdfc.ScriptsEnabled = pdfc.ScriptsEnabledInImage = false;
            pdfc.ActiveXEnabled = pdfc.ActiveXEnabledInImage = false;
            pdfc.PdfDocumentOptions.JpegCompressionEnabled = false;

            byte[] b = pdfc.GetPdfFromUrlBytes(WebUtility.GetSiteRoot() + requestUrl);

            context.Response.Buffer = false;
            context.Response.ClearHeaders();
            context.Response.ClearContent();
            context.Response.ContentType = "application/pdf";
            context.Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(fileName) + ".pdf");
            context.Response.AddHeader("Content-Length", b.Length.ToString());
            context.Response.BinaryWrite(b);
            context.Response.Flush();
        }

        private void Return404(HttpContext context)
        {
            context.Response.StatusCode = 404;
        }

        private string GetUserName(HttpContext context)
        {
            string username = context.User.Identity.Name;
            // if there exists parameter "byoc" and its value is "affirm", we override username to value of parameter "u" as long as its not empty
            if (!string.IsNullOrWhiteSpace(context.Request["byoc"]) && context.Request["byoc"] == "affirm" && !string.IsNullOrWhiteSpace(context.Request["u"]))
                username = context.Request["u"];

            return username;
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}
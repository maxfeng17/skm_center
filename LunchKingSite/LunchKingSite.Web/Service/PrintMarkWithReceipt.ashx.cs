﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using LunchKingSite.WebLib;
using Winnovative.WnvHtmlConvert;
using LunchKingSite.BizLogic.Facade;
using log4net;

namespace LunchKingSite.Web.Service
{
    /// <summary>
    /// Summary description for receipt
    /// </summary>
    public class PrintMarkWithReceipt : IHttpHandler
    {
        ILog logger = LogManager.GetLogger(typeof(PrintMarkWithReceipt));
        public void ProcessRequest(HttpContext context)
        {
            string requestUrl;
            if (string.IsNullOrEmpty(context.Request.QueryString["url"]))
            {
                throw new Exception("PrintMarkWithReceipt fail, url not found.");
            }            
            requestUrl = context.Request.QueryString["url"];

            //擬轉pdf元件，無法讀取 https://, 改成使用http:// ，看是否可行
            requestUrl = requestUrl.Replace("https://", "http://");
            logger.Info("PrintMarkWithReceipt url=" + requestUrl);

            string queryString = new Uri(requestUrl).Query;
            var queryDictionary = HttpUtility.ParseQueryString(queryString);
            string id = queryDictionary["id"];
            if (string.IsNullOrEmpty(id))
            {
                throw new Exception("PrintMarkWithReceipt fail, param id not found.");
            }
            logger.Info("PrintMarkWithReceipt id=" + id);

            //TODO 這邊欠權限檢查
            try
            {

                PdfConverter pdfc = new PdfConverter();
                pdfc.LicenseKey = @"YklTQlNCVFZCVExSQlFTTFNQTFtbW1s=";
                pdfc.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                pdfc.PdfDocumentOptions.TopMargin = 50;
                pdfc.PdfDocumentOptions.LeftMargin = 0;
                pdfc.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                pdfc.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
                pdfc.ScriptsEnabled = true;
                
                byte[] b = pdfc.GetPdfFromUrlBytes(requestUrl);

                context.Response.Buffer = false;
                context.Response.ClearHeaders();
                context.Response.ClearContent();
                context.Response.ContentType = "application/pdf";
                string fileName = id + ".pdf";
                context.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                context.Response.AddHeader("Content-Length", b.Length.ToString());
                context.Response.BinaryWrite(b);
                context.Response.Flush();
            }
            catch (Exception ex)
            {
                logger.Info("ProcessRequest fail", ex);
                throw;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
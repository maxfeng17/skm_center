﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using LunchKingSite.WebLib;
using Winnovative.WnvHtmlConvert;
using LunchKingSite.BizLogic.Facade;
using log4net;
using System.Net;
using System.IO;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.Service
{
    /// <summary>
    /// Summary description for receipt
    /// </summary>
    public class GetWmsFee : IHttpHandler
    {
        ILog logger = LogManager.GetLogger(typeof(GetWmsFee));
        static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public void ProcessRequest(HttpContext context)
        {
            string requestUrl;
            if (string.IsNullOrEmpty(context.Request.QueryString["url"]))
            {
                throw new Exception("");
            }            
            requestUrl = context.Request.QueryString["url"];
            string feeName = context.Request.QueryString["feeName"];
            //List<string> items = new JsonSerializer().Deserialize<List<string>>(data);

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                Stream responseStream = response.GetResponseStream();
                //string baseDir = string.IsNullOrEmpty(config.WmsFeeFileDir) ? HttpContext.Current.Server.MapPath("~/cache/WmsFeeFile/") : config.WmsFeeFileDir;
                string baseDir = string.IsNullOrEmpty(config.WebAppDataPath) ? HttpContext.Current.Server.MapPath("~/cache/WmsFeeFile/") : Path.Combine(config.WebAppDataPath, "WmsFeeFile");
                Stream stream = new FileStream(Path.Combine(baseDir,feeName + ".csv"), FileMode.Create);

                byte[] bArr = new byte[1024];
                int size = responseStream.Read(bArr, 0, bArr.Length);
                while (size > 0)
                {
                    stream.Write(bArr, 0, size);
                    size = responseStream.Read(bArr, 0, bArr.Length);
                }
                stream.Close();
                responseStream.Close();




            }
            catch (Exception ex)
            {
                logger.Info("ProcessRequest fail", ex);
                throw;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib;
using Winnovative.WnvHtmlConvert;

namespace LunchKingSite.Web.Service
{
    /// <summary>
    /// Summary description for returndiscount
    /// </summary>
    public class returndiscount : IHttpHandler
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(returndiscount));
        public void ProcessRequest(HttpContext context)
        {
            if (string.IsNullOrEmpty(context.User.Identity.Name))
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }
            Guid orderGuid;
            if (!string.IsNullOrEmpty(context.Request.QueryString["oid"]))
            {
                if (!Guid.TryParse(context.Request.QueryString["oid"], out orderGuid))
                {
                    context.Response.StatusCode = 404;
                    return;
                }
                else
                {
                    string fileName = "退回或折讓證明單";
                    string userName;
                    if (string.IsNullOrEmpty(context.Request.QueryString["u"]))
                    {
                        userName = context.User.Identity.Name;
                    }
                    else
                    {
                        userName = context.Request.QueryString["u"];
                    }


                    string requestUrl = Helper.CombineUrl(config.SiteUrl, "user/ReturnDiscount.aspx?oid=")
                        + orderGuid.ToString() + "&u=" + HttpUtility.UrlEncode(userName);

                    #region old version
                    PdfConverter pdfc = new PdfConverter();
                    pdfc.LicenseKey = @"YklTQlNCVFZCVExSQlFTTFNQTFtbW1s=";
                    pdfc.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                    pdfc.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                    pdfc.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;

                    // disable unnecessary features to enhance performance
                    pdfc.ScriptsEnabled = pdfc.ScriptsEnabledInImage = false;
                    pdfc.ActiveXEnabled = pdfc.ActiveXEnabledInImage = false;
                    pdfc.PdfDocumentOptions.JpegCompressionEnabled = false;


                    //byte[] b = pdfc.GetPdfFromUrlBytes( requestUrl);
                    #endregion

                    var converter = new NReco.PdfGenerator.HtmlToPdfConverter();
                    converter.PdfToolPath = System.IO.Path.GetTempPath();
                    byte[] b;
                    try
                    {
                        b = converter.GeneratePdfFromFile(requestUrl, null);
                    }
                    catch (Exception ex)
                    {
                        logger.WarnFormat("request {0} error. ex={1}", requestUrl, ex);
                        throw;
                    }

                    context.Response.Buffer = false;
                    context.Response.ClearHeaders();
                    context.Response.ClearContent();
                    context.Response.ContentType = "application/pdf";
                    context.Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(fileName) + ".pdf");
                    context.Response.AddHeader("Content-Length", b.Length.ToString());
                    context.Response.BinaryWrite(b);
                    context.Response.Flush();
                }
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
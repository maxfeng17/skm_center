﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.Service
{
    /// <summary>
    /// Summary description for $codebehindclassname$
    /// </summary>
    public class barcode : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            EasyDigitCipher cipher = new EasyDigitCipher();
            string encodedCode = context.Request["code"];
            string codeType = context.Request["type"];
            string code;
            Image img;
            if (string.IsNullOrEmpty(encodedCode))
            {
                Return404(context);
                return;
            }
            else
            {
                if (string.IsNullOrEmpty(codeType))
                {
                    string[] encodeedcodelist = encodedCode.Split(new string[] { "-" }, StringSplitOptions.None);
                    string encypt_code = encodeedcodelist.Skip(2).Take(1).DefaultIfEmpty(string.Empty).First();
                    if (!cipher.Decrypt(encypt_code, encodedCode, out code))
                    {
                        Return404(context);
                        return;
                    }
                    else
                    {
                        List<string> code_source = new List<string>();
                        code_source.Add(encodeedcodelist.Skip(0).Take(1).DefaultIfEmpty(string.Empty).First());
                        code_source.Add(encodeedcodelist.Skip(1).Take(1).DefaultIfEmpty(string.Empty).First());
                        code_source.Add(code);
                        IEnumerable<string> actual_codes = code_source.Where(x => !string.IsNullOrEmpty(x));
                        img = new BarCode128(actual_codes.Count() > 0 ? actual_codes.Aggregate((current, next) => current + "-" + next) : string.Empty, BarCode128.BarCode128Types.A).Paint();
                    }
                }
                else
                {
                    if (string.Compare(codeType, "orderid", true) == 0)
                    {
                        BarCodeSettings barCodeSettings = new BarCodeSettings(true);
                        barCodeSettings.DrawText = false;
                        img = new BarCode128(encodedCode, BarCode128.BarCode128Types.A, barCodeSettings).Paint();
                    }
                    else
                    {
                        Return404(context);
                        return;
                    }

                }
                context.Response.ContentType = "image/png";
                byte[] b = GetImageBytes(img);
                context.Response.OutputStream.Write(b, 0, b.Length);
                img.Dispose();
            }

        }

        private void Return404(HttpContext context)
        {
            context.Response.StatusCode = 404;
        }

        private byte[] GetImageBytes(Image image)
        {
            ImageCodecInfo codec = null;

            foreach (ImageCodecInfo e in ImageCodecInfo.GetImageEncoders())
            {
                if (e.MimeType == "image/png")
                {
                    codec = e;
                    break;
                }
            }

            using (EncoderParameters ep = new EncoderParameters())
            {
                ep.Param[0] = new EncoderParameter(Encoder.Quality, 100L);

                using (MemoryStream ms = new MemoryStream())
                {
                    image.Save(ms, codec, ep);
                    return ms.ToArray();
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}

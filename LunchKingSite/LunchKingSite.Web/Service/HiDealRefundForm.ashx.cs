﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib;
using Winnovative.WnvHtmlConvert;

namespace LunchKingSite.Web.Service
{
    /// <summary>
    /// Summary description for HiDealRefundForm
    /// </summary>
    public class HiDealRefundForm : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (string.IsNullOrEmpty(context.User.Identity.Name))
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }

            if (!string.IsNullOrEmpty(context.Request.QueryString["id"]))
            {
                var theId = context.Request.QueryString["id"];
                var formType = context.Request.QueryString["FormType"];
                var couponid = context.Request.QueryString["cid"];
                if (!HiDealReturnedManager.IsRefundFormPageId(theId))
                {
                    context.Response.StatusCode = 404;
                    return;
                }
                else
                {
                    string fileName = !(string.Compare(formType, "RAF", true) == 0) ? "退回或折讓證明單" : "退貨申請書";
                    string requestUrl = "/piinlife/member/refundform.aspx?id=" + theId + "&FormType=" + formType;
                    if (!string.IsNullOrEmpty(couponid))
                    {
                        requestUrl += "&cid=" + couponid;
                    }
                    PdfConverter pdfc = new PdfConverter();
                    pdfc.LicenseKey = @"YklTQlNCVFZCVExSQlFTTFNQTFtbW1s=";
                    pdfc.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                    pdfc.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
                    pdfc.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;

                    // disable unnecessary features to enhance performance
                    pdfc.ScriptsEnabled = pdfc.ScriptsEnabledInImage = false;
                    pdfc.ActiveXEnabled = pdfc.ActiveXEnabledInImage = false;
                    pdfc.PdfDocumentOptions.JpegCompressionEnabled = false;

                    byte[] b = pdfc.GetPdfFromUrlBytes((WebUtility.GetSiteRoot() + requestUrl));

                    context.Response.Buffer = false;
                    context.Response.ClearHeaders();
                    context.Response.ClearContent();
                    context.Response.ContentType = "application/pdf";
                    context.Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(fileName) + ".pdf");
                    context.Response.AddHeader("Content-Length", b.Length.ToString());
                    context.Response.BinaryWrite(b);
                    context.Response.Flush();
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}
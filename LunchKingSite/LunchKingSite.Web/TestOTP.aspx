﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestOTP.aspx.cs" Inherits="LunchKingSite.Web.TestOTP" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:button runat="server" text="下訂單" OnClick="MakeOrder_Click" />
        <br />
        <asp:button runat="server" text="ResultUrl" OnClick="ResultUrl_Click" />

        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        
        <br />
        <asp:TextBox ID="userName" runat="server"></asp:TextBox>
        <asp:TextBox ID="password" runat="server" TextMode="Password"></asp:TextBox>
        <br />
        <asp:RadioButtonList ID="RadioButtonList1" runat="server">
            <asp:ListItem>非3D卡號</asp:ListItem>
            <asp:ListItem>3D正常卡號</asp:ListItem>
            <asp:ListItem>3D錯誤卡號</asp:ListItem>
            <asp:ListItem>3D錯誤到期日</asp:ListItem>
            <asp:ListItem>3D錯誤後三碼</asp:ListItem>
            <asp:ListItem>3D記憶卡號</asp:ListItem>
            <asp:ListItem>其他測試</asp:ListItem>
        </asp:RadioButtonList>
    </div>
    </form>
</body>
</html>


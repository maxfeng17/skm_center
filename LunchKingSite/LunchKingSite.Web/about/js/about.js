// detect the width of mob or web size
let w = $(window).width();

// common scroll till end and show the button

let showBackToTop = function () {
    let userWindowInnerHeight = window.innerHeight;
    let timelineSec =document.getElementById('sec__timeline');
    let timelineSecTop = timelineSec.getBoundingClientRect().top;
    if ( userWindowInnerHeight > timelineSecTop ) {
        $(".backtop").css("display", "block");
    } else if (userWindowInnerHeight <= timelineSecTop) {
        $(".backtop").css("display", "none");
    }
}
window.addEventListener("scroll", showBackToTop);

// common ABOUT scroll and change nav to have gray bg

    let changeTransparentNavScrollFunc = function () {
        let y = window.scrollY;
        let t = $(window).scrollTop();
        // When user scrolled will remove transparent and logo disappear
        if (y >= 1  && t >= 1 && w > 768) {
            $("#nav__container").removeClass("transparent__background");
            $(".centerlogo").css("display", "none");
        } else {
            $("#nav__container").addClass("transparent__background");
            $(".centerlogo").css("display", "block");
        }
    };

// ABOUT page scroll to each block change nav colors

    let changeColorNavScrollFunc = function () {
        let timelineSec =document.getElementById('sec__timeline');
        let timelineSecTop = timelineSec.getBoundingClientRect().top;
        let timelineSecBottom = timelineSec.getBoundingClientRect().bottom;
        if (timelineSecTop < 60 && timelineSecBottom > 60) {
            $("#nav__container").addClass("pink__background");
        } else {
            $("#nav__container").removeClass("pink__background");
        }

        let lifeSec =document.getElementById('sec__life');
        let lifeSecTop = lifeSec.getBoundingClientRect().top;
        let lifeSecBottom = lifeSec.getBoundingClientRect().bottom;
        if (lifeSecTop < 60 && lifeSecBottom > 60) {
            $("#nav__container").addClass("green__background");
        } else {
            $("#nav__container").removeClass("green__background");
        }

        let workSec =document.getElementById('sec__work');
        let workSecTop = workSec.getBoundingClientRect().top;
        let workSecBottom = workSec.getBoundingClientRect().bottom;
        if (workSecTop < 60 && workSecBottom > 60) {
            $("#nav__container").addClass("blue__background");
        } else {
            $("#nav__container").removeClass("blue__background");
        }

        let partnerSec =document.getElementById('sec__partner');
        let partnerSecTop = partnerSec.getBoundingClientRect().top;
        let partnerSecBottom = partnerSec.getBoundingClientRect().bottom;
        if (partnerSecTop < 60 && partnerSecBottom > 60) {
            $("#nav__container").addClass("orange__background");
        } else {
            $("#nav__container").removeClass("orange__background");
        }
    }

// ABOUT page scroll and content fade in effect

    $(window).scroll(function(){
        // Check the location of each desired element 
        $('.fadein').each( function(i){
            let bottom_of_object = $(this).offset().top + $(this).outerHeight();
            let bottom_of_window = $(window).scrollTop() + $(window).height();
            // If the object is completely visible in the window, fade in it 
            if( bottom_of_window > bottom_of_object ){
                $(this).animate({'opacity':'1'},500);
            }
        }); 
        
        $('.fadeinleft').each( function(i){      
            let bottom_of_element = $(this).offset().top + $(this).outerHeight();
            let bottom_of_window = $(window).scrollTop() + $(window).height();
            if( bottom_of_window > bottom_of_element ){
                $(this).animate({'opacity':'1','margin':'0px 15px'},500);
            }
        }); 

        $('.fadeinlefttime').each( function(i){      
            let bottom_of_element = $(this).offset().top + $(this).outerHeight();
            let bottom_of_window = $(window).scrollTop() + $(window).height();
            if( bottom_of_window > bottom_of_element ){
                $(this).animate({'opacity':'1','margin':'0px 15px'},i*500);
            }
        }); 
    });

    if ( w < 768 ) {
        $("#main__content").removeClass("fadein");
    }

// ABOUT page number counter when scroll to there

    // set a variable to check if it works for once then no need for second time
    let isInit = false;

    $(window).scroll(function(){
        $('.app__logo').each( function(i){
            let bottom_of_object = $(this).offset().top + $(this).outerHeight();
            let bottom_of_window = $(window).scrollTop() + $(window).height();
            if(isInit == false && bottom_of_window > bottom_of_object ){
                    isInit = true;
                    $('.num__count').each(function () {
                        $(this).css("color","#F04086");
                        $(this).prop('Counter',0).animate({
                            Counter: $(this).text()
                        }, {
                            duration: 500,
                            easing: 'swing',
                            step: function (now) {
                                $(this).text(Math.ceil(now));
                            }
                        });
                    });
            }
        }); 
    });
   

// ABOUT page slider

    let slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
    showSlides(slideIndex += n);
    }

    // use onclick to get current elem and show the slide (if no hover situation)
    function currentSlide(n) {
    showSlides(slideIndex = n);
    }

    // use onmouseover to get current elem and run hover effect 
    function currentHover(n){
        showHoverEffect(slideIndex = n);
    }

    //  hover elem and move car
    function showHoverEffect(n) {
        // according to slide number change the car place
        if ( slideIndex == 1) {
            $("#car__icon").animate({left:"6%"});
            if (w < 1000){
                $("#car__icon").animate({left:"4%"});
            }
        } else if ( slideIndex == 2) {
            $("#car__icon").animate({left:"25%"},"fast");
            if (w < 1000){
                $("#car__icon").animate({left:"23%"},"fast");
            }
        } else if ( slideIndex == 3 ) {
            $("#car__icon").animate({left:"46%"},"fast");
            if (w < 1000){
                $("#car__icon").animate({left:"44%"},"fast");
            }
        } else if ( slideIndex == 4 ) {
            $("#car__icon").animate({left:"69%"},"fast");
            if (w < 1000){
                $("#car__icon").animate({left:"63%"},"fast");
            }
        } else if ( slideIndex == 5 ) {
            $("#car__icon").animate({left:"88%"},"fast");
            if (w < 1000){
                $("#car__icon").animate({left:"85%"},"fast");
            }
        }
        // add this in here will have hover & change effect
        showSlides(n);
    }

    function showSlides(n) {
        let i;
        let slides = document.getElementsByClassName("building__content");
        let dots = document.getElementsByClassName("building__icon");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        if (w > 768){
            slides[slideIndex-1].style.display = "flex";
        } else {
            slides[slideIndex-1].style.display = "block";
        }
        dots[slideIndex-1].className += " active";
    }

// ABOUT lottie for work part

    lottie.loadAnimation({
        container: document.getElementById('work__happy'), // the dom element that will contain the animation
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: './json/newhappydata.json' 
    });

    lottie.loadAnimation({
        container: document.getElementById('work__data'), 
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: './json/combinedata.json' 
    });

    lottie.loadAnimation({
        container: document.getElementById('work__pro'), 
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: './json/prodata.json' 
    });

// ABOUT lottie for header logo

    lottie.loadAnimation({
        container: document.getElementById('logo__data'), 
        renderer: 'svg',
        loop: false,
        autoplay: true,
        path: './json/logodata.json' 
    });



// ABOUT media query detect and change 

function mediaFunction(x) {
    if (x.matches) { // If media query matches
        $("#nav__container").removeClass("transparent__background");
        window.removeEventListener("scroll", changeTransparentNavScrollFunc);
        window.removeEventListener("scroll", changeColorNavScrollFunc);
    } else {
        window.addEventListener("scroll", changeTransparentNavScrollFunc);
        window.addEventListener("scroll", changeColorNavScrollFunc);
    }
}

let x = window.matchMedia("(max-width: 768px)")
mediaFunction(x) // Call listener function at run time
x.addListener(mediaFunction) // Attach listener function on state changes

// common scroll till end and show the button

let showBackToTop = function () {
    let userWindowInnerHeight = window.innerHeight;
    let timeSec =document.getElementById('timeline__box');
    let timeSecTop = timeSec.getBoundingClientRect().top;
    if ( userWindowInnerHeight > timeSecTop ) {
        $(".backtop").css("display", "block");
    } else if (userWindowInnerHeight <= timeSecTop) {
        $(".backtop").css("display", "none");
    }
}
window.addEventListener("scroll", showBackToTop);


function loadFirst () {
    $("#timebox__firstload").css("display", "block");
    $("#loadmore__first").css("display", "none");
}

// common backtop button click event 

    $(".backtop").click(function() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    });

// common click mob nav show menu and change to X mark

$("#nav__mob").click(function(){
    if($('.nav__mobmenu').css('display') == 'none') {
        $(".nav__mob").css("background-image","url(./image/cancel.svg)");
        $(".nav__mobmenu").css("display", "block");
    } else {
        $(".nav__mob").css("background-image","url(./image/menu.svg)");
        $(".nav__mobmenu").css("display", "none");
    }
});

// common click nav search icon show search bar

    $("#nav__opensearch").click(function(){
        if($(".nav__searchbar").css('display') == 'none') {
            $("#nav__opensearch").removeClass("normal__search");
            $("#nav__opensearch").addClass("clicked__search");
            $(".nav__searchbar").css("display", "flex");
        } else {
            $("#nav__opensearch").removeClass("clicked__search");
            $("#nav__opensearch").addClass("normal__search");
            $(".nav__searchbar").css("display", "none");
        }
    });


    // search function for web

    function gotosearch() {
        if (window.searchEngine != null) {
            location.href = "https://www.17life.com" + "/ppon/pponsearch.aspx?search=" + encodeURI($('#querystring').val()) + "&searchEngine=" + window.searchEngine;
        } else {
            location.href = "https://www.17life.com" + "/ppon/pponsearch.aspx?search=" + encodeURI($('#querystring').val());
        }
    }

    // search function for mob
    
    function search() {
        if (window.searchEngine != null) {
            location.href = "https://www.17life.com" + "/m/SearchResult?keywords=" + encodeURI($('#mobquerystring').val()) + "&searchEngine=" + window.searchEngine;
        } else {
            location.href = "https://www.17life.com" + "/m/SearchResult?keywords=" + encodeURI($('#mobquerystring').val());
        }
    }


    $('#nav__opensearch').click(function() {
        $('#querystring').focus();
    });
    
    $('#querystring').keypress(function(evt) {
        if (evt.which == 13) {
            gotosearch();
        }
    });

    $('#mobquerystring').keypress(function(evt) {
        if (evt.which == 13) {
            gotosearch();
        }
    });
$(function () {
    /***
	*
	*正常選單(預設:關)
	*
	***/
    $('.header_wrap_normal .fa-angle-up').hide();
    $('.header_wrap_normal .nav').hide();
    $('.header_wrap_normal').css('border-bottom', '0px');

    /***
	*
	*跟屁蟲選單(預設)
	*
	***/
    $('.header_wrap_slider').hide();
    $('.header_wrap_slider .nav').hide();
    $('.header_wrap_slider .fa-angle-down').css('display', 'inline-block');
    $('.header_wrap_slider .fa-angle-up').css('display', 'none');
    $('.header_wrap_slider').css('border-bottom', '0px');

    /***
	*
	*index 預設(展開)
	*
	***/
    $('.open_header_nav .fa-angle-up').show();
    $('.open_header_nav .fa-angle-down').hide();
    $('.open_header_nav .nav').show();
    $('.open_header_nav ').css('border-bottom', '1px solid #ddd');

    /***
	*
	*選單 上下滑開關
	*
	***/
    $('.header_wrap .slider_btn').click(function () {
        var head = $(this).closest('.header_wrap');
        var nav = head.find('.nav');
        if (nav.is(':visible')) {
            nav.slideUp({ duration: 500, easing: "easeOutExpo" });
            head.find('.fa-angle-up').hide();
            head.find('.fa-angle-down').show();
            head.css('border-bottom', '0px');
        } else {
            nav.slideDown({ duration: 500, easing: "easeOutExpo" });
            head.find('.fa-angle-up').show();
            head.find('.fa-angle-down').hide();
            head.css('border-bottom', '1px solid #ddd');
        }
    });

    /***
	*
	*跟屁蟲選單 超過一定高度自己跑出來(目前設定為1136:iphone5的螢幕高)
	*
	***/
    $(window).scroll(function () {
        $('.header_wrap_slider .fa-angle-up').hide();
        $('.header_wrap_slider .fa-angle-down').show();
	    var triggerHeight = $('.header_wrap_normal .header').height();
	    if ($('.header_wrap_normal .nav').is(':visible')) {
	        triggerHeight += $('.header_wrap_normal .nav').height();
	    }
		//console.log('triggerHeight: ' + triggerHeight);
        if ($(this).scrollTop() > triggerHeight) {
            $('.header_wrap_slider').slideDown({ duration: 500, easing: "easeOutExpo" });
            $('.header_wrap_slider .nav').hide();
        } else {
            $('.header_wrap_slider').slideUp({ duration: 0, easing: "easeOutExpo" });
        }
    });

    /***
    *
    *商品頁:頁面捲動到詳細內容nav以下時，nav置頂
    *
    ***/
    var maniNav_height=$('.detail_mainNav').height();  //商品頁的詳細內容nav高度

    /*錨點*/
    // var mainNav_1_pos= $('.description').position().top - maniNav_height;   //mainNav_1對應的位置
    // var mainNav_2_pos= $('.explain').position().top - maniNav_height;       //mainNav_2對應的位置
    // var mainNav_3_pos= $('.related').position().top - maniNav_height;       //mainNav_3對應的位置
    // var flag_mainNav_1=0;
    // var flag_mainNav_2=0;
    // var flag_mainNav_3=0;

    /*隨著捲軸移動nav會自動顯示正確顏色(位置)*/
    $(window).scroll(function () {                                
        var scrollTop_now=$(window).scrollTop()+ maniNav_height ;         
        if (scrollTop_now >= mainNav_1_pos && scrollTop_now < mainNav_2_pos) {
            $('.mainNav').removeClass('active');
            $('.mainNav_1').addClass('active');
            flag_mainNav_1=1;  
            flag_mainNav_2=0;  
            flag_mainNav_3=0;
        }
        else if(scrollTop_now >= mainNav_2_pos && scrollTop_now <mainNav_3_pos) {
            $('.mainNav').removeClass('active');
            $('.mainNav_2').addClass('active');
            flag_mainNav_1=0;  
            flag_mainNav_2=1;  
            flag_mainNav_3=0;
        }
        else if(scrollTop_now >= mainNav_3_pos) {
            $('.mainNav').removeClass('active');
            $('.mainNav_3').addClass('active');
            flag_mainNav_1=0;  
            flag_mainNav_2=0;  
            flag_mainNav_3=1;
        }               
    });
    
    /*點擊mainNav 畫面自動捲到對應位置*/  
    $('.detail_mainNav').find('.mainNav_1').click(function () {   
        if(flag_mainNav_1==0){
            $("body").animate({
                scrollTop: ( $(".detail_mainIntro").position().top - 0 )
            }, 'slow');
            $('.mainNav').removeClass('active');
            $(this).addClass('active');
            stop(true,false); 
            flag_mainNav_1=1;  
            flag_mainNav_2=0;  
            flag_mainNav_3=0;   
        }                             
    });
    $('.detail_mainNav').find('.mainNav_2').click(function () {
        if(flag_mainNav_2==0){
            stop(false,false); 
            $("body").animate({
                scrollTop: ( $(".explain").position().top - maniNav_height )
            }, 'slow');
            $('.mainNav').removeClass('active');
            $(this).addClass('active');
            stop(true,false); 
            flag_mainNav_1=0;  
            flag_mainNav_2=1;  
            flag_mainNav_3=0;  
        }
    });
    $('.detail_mainNav').find('.mainNav_3').click(function () {
        if(flag_mainNav_3==0){
            stop(false,false); 
            $("body").animate({
                scrollTop: ( $(".related").position().top - maniNav_height )
            }, 'slow');
            $('.mainNav').removeClass('active');
            $(this).addClass('active');
            stop(true,false); 
            flag_mainNav_1=0;  
            flag_mainNav_2=0;  
            flag_mainNav_3=1;  
        }
    });
                        
    /*頁面捲動到相關商品以下時，buyAction_wrap回歸原始位置*/
    $(window).scroll(function () {
        var scrollBottom = $(window).scrollTop() + $(window).height();
        if (scrollBottom > $(".related").offset().top + $(".related").height() ) {
            $('.buyAction_wrap').removeClass("fixedBuyAction");
        } else {
            $('.buyAction_wrap').addClass("fixedBuyAction");
        }
    });  

    /*收藏功能*/
    var flag_fav=0; //預設尚未加入最愛
    $('.fav').click(function () { 
        if(flag_fav==0){
            //加入最愛
            $(this).find('i').removeClass('fa-heart-o');
            $(this).find('i').addClass('active');     
            $('.text_fav').html('已收藏');
            flag_fav=1; 
        }else{
            //取消加入
            $(this).find('i').addClass('fa-heart-o');
            $(this).find('i').removeClass('active');
            $('.text_fav').html('收藏');
            flag_fav=0; 
        }
    });    

    /*overHeight 高度太高的滑動開關*/
    //如果overHeight高度大於400px
    //限制高度並出現按鈕            
    var overHeight_height=$('.overHeight').height();
    if(overHeight_height>=400){
        var flag_overHeight=0; //0收合;1展開
        $(this).find('.overHeight_sliderBtn a').click(function () {
            if(flag_overHeight==0){
                $(this).parent().parent().addClass('overHeight_SlideDown');
                $(this).find('.fa-angle-down').css('display','none');
                $(this).find('.fa-angle-up').css('display','inline-block');
                $(this).find('span').html('收合');
                flag_overHeight=1;
            }else{
                $(this).parent().parent().removeClass('overHeight_SlideDown');
                $(this).find('.fa-angle-down').css('display','inline-block');
                $(this).find('span').html('展開看詳細');
                $(this).find('.fa-angle-up').css('display','none');
                flag_overHeight=0;
            }                   
        })
    }else{
        $('.overHeight_sliderBtn').css('display','none');
    }

    /*soldout 頁籤切換*/
    var flag_soldout_1=0; //為你推薦
    var flag_soldout_2=0; //猜你喜歡
    $('.soldout_tab').find('.soldoutTab_1').click(function () {   
        if(flag_soldout_1==0){                    
            $('.soldoutTab_2').removeClass('active');
            $(this).addClass('active');
            $('.soldot_similar').css('display','block');
            $('.soldot_youlike').css('display','none');
            stop(true,false); 
            flag_soldout_1=1;  
            flag_soldout_2=0;  
        }                             
    });
    $('.soldout_tab').find('.soldoutTab_2').click(function () {   
        if(flag_soldout_2==0){                    
            $('.soldoutTab_1').removeClass('active');
            $(this).addClass('active');
            $('.soldot_similar').css('display','none');
            $('.soldot_youlike').css('display','block');
            stop(true,false); 
            flag_soldout_1=0;  
            flag_soldout_2=1;  
        }                             
    });


    /*alert 測試用：請工程師大大忽略*/
    $('.dealList li').click(function(){
        alert('我知道你會點，還沒寫好先等等');
    });
    $('.buyAction_wrap li.buy_app').click(function(){
        alert('還沒寫好等等等等');
    });         

});



﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="order_return.aspx.cs" Inherits="LunchKingSite.Web.order_return" %>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, user-scalable=no" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>感謝您的回覆</title>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js" type="text/javascript"></script>
<!--資料呈現選擇 供內部溝通使用-->
  <script type="text/javascript">
    $(document).ready(function () {  
        $(".jsut_test .normal").click(function() {
              $(".jsut_test").css('display','none');  
              $(".section_normal").css('display','block');
              $(".section_expired").css('display','none');
              $(".section_repeat").css('display','none');
        });
        $(".jsut_test .expired").click(function() {
              $(".jsut_test").css('display','none');  
              $(".section_normal").css('display','none');
              $(".section_expired").css('display','block');
              $(".section_repeat").css('display','none');
        });
        $(".jsut_test .repeat").click(function() {
              $(".jsut_test").css('display','none');  
              $(".section_normal").css('display','none');
              $(".section_expired").css('display','none');
              $(".section_repeat").css('display','block');
        });
    }); 
  </script>
<!--資料呈現選擇 供內部溝通使用 end-->

<style>
/*--------- reset ---------*/
html,body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,button,textarea,p,blockquote,th,td,figure{margin:0;padding:0;
}
table{border-collapse:collapse;
border-spacing:0;
font-size:inherit;
font:100%;
}
fieldset,img{border:none;
}
address,caption,cite,code,dfn,em,th,var{font-style:normal;
font-weight:normal;
}
img{vertical-align:middle;
}
abbr,acronym{border:none;
font-variant:normal;
}
sup{vertical-align:text-top;
}
sub{vertical-align:text-bottom;
}
input,button,textarea,select{*font-size:100%;
}
input{vertical-align:middle;
}
select,input,button,textarea{font:100% Verdana, Arial, Helvetica, sans-serif;
}
pre,code,kbd,samp,tt{font-family:Verdana, Arial, Helvetica, sans-serif;
}
small{font-size:100%;
}
ins{text-decoration:none;
}
li{
	list-style: none;
}
/*--------- style ---------*/
html{text-align:left;
}
html,legend{color:#333;
}
body{
font-size:16px;
line-height:1.7;
font-family:\5FAE\8EDF\6B63\9ED1\9AD4;
background-color:#eee;
}
input,button,textarea,select{font-family:inherit;
font-size:inherit;
font-weight:inherit;
color:#333;
padding:1px;
}
p{margin:0 0 10px 0;
}
a{text-decoration:none;
}
a:hover{text-decoration:underline;
}
.clear{
clear:both;
}
.center{
	text-align:center;
}
/*--------- layout start ---------*/
.newyear .container{
	margin:0 auto;
}
.newyear .header{
	float:left;
	width:100%;
	margin-bottom:12px;
	background-color:#FFF;
	border-bottom:1px solid #ddd;
}
.newyear .header img{
	width: 80px;
}
.header_topic{
	margin:5px auto;
	width:84px;
}
.link_wrap_web .logo{
	float: left;
}
.link_wrap_web ul{
	margin-top: 50px;
	float: right;
}
.link_wrap_web ul li{
	float: left;
	width: 160px;
	height: 40px; 
	background: #555; 
	margin-right: 10px;
	margin-bottom: 10px; 	
}

.section{
	float:left;
	width:100%;
	margin-bottom:12px;
	background-color:#FFF;
	border-bottom:1px solid #ddd;
	position: relative;
}
.section .result_img{
	position: absolute;
	right: 0;
	top: 0;
}
.section .topic{
	float:left;
	width:92%;
	margin-bottom:12px;
	padding:20px 4%;
	border-bottom:1px solid #ddd;	
	text-align: center;
}
.section .topic span{
	float:left;
	margin-top:2px;
	margin-right:4px;
}
.section .topic strong{
	font-size:20px;
	line-height:24px;
	color:#090;
}
.section .dec{
	float:left;
	width:92%;
	line-height:16px;
	margin-bottom:16px;
	padding:0 4%;
	color:#666;
}
.section p{
	line-height: 1.2;
}
.section .link_17life {
	clear: both;
	margin: 0 auto;	display: block;
	text-align: center;
	margin-bottom: 20px; 
}
.section .link_17life>a{
	display: inline-block;
	padding: 5px 10px;
	background: #E74C3C;
	color: #fff;
	border-radius: 5px;
	text-decoration: none;
}
.section .link_17life>a:hover{
	background: #f05949;
}

.footer{
	float:left;
	width:100%;
	line-height:1.4;
	padding:10px 0 12px 0;
	text-align:center;
	color:#999;
	font-size:14px;
	background: #31312C;
	margin-top: 50px;
}
.footer img{
	width: 90px;
}


/*L*/
@media only screen and (min-width:961px) {
	.link_wrap_web{
		width: 960px;
		margin: 0 auto;
	}
	.link_wrap_web{
		margin-top: 50px;
	}
	.section{
		border-radius: 10px;
		box-shadow: 0px 0px 5px rgba(0,0,0,0.4);
	}
	.section .topic strong{
		float: left;
	}
	.newyear .container{
		width:960px;
	}
	.link_wrap{
		width: 480px;
		margin: 0 auto;
	}
	.header{
		display: none;
	}
	.link_wrap_web{
		display: block;
	}
	.link_wrap{
		display: none;
	}
}

/*M*/
@media screen and (min-width: 640px) and (max-width: 960px) {
	.newyear .container{
		width:100%;
	}
	.link_wrap{
		width: 480px;
		margin: 0 auto;
	}
	.link_wrap_web{
		display: none;
	}
	.link_wrap{
		display: block;
	}
}

/*S*/
@media only screen and (max-width:640px) {
	.newyear .container{
		width:100%;
	}
	.header{
		display: block;
	}
	.link_wrap_web{
		display: none;
	}
	.link_wrap{
		display: block;
	}
}

</style>
</head>

<body>

<div class="newyear">
	<div class="header">
	  <div class="header_topic">
	  	<a href="https://www.17life.com" target="_blank">
	  		<img src="https://www.17life.com/images/17P/active/20170317_elin/images/logo_17life.png"/>
	  	</a>
	  </div>
	</div>

	<div class="link_wrap_web">
		<div class="logo"><a href="https://www.17life.com" target="_blank"><img src="https://www.17life.com/images/17P/active/20170317_elin/images/logo_17life.png"/></a></div>
	</div>
	<div class="container">
	  <div class="section">	  	
	    <div class="topic"><strong>感謝您的回覆！</strong></div>
	    <!--正常勾稽-->
		<div id="section_normal" class="section_normal" runat="server">
		    <div class="result_img"><img src="https://www.17life.com/images/17P/active/20170317_elin/images/payment_success.png"></div>
		    <div class="dec">
			    <p>我們將儘速為您處理發票作廢及銷貨折讓單事宜，並完成退貨。</p>
			    <p>您將在完成後收到電子折讓開立通知信件，並在帳戶中看到完整退款項目</p><br/>
			    <p>如有任何問題，歡迎您隨時與我們聯繫，也期待您常回來逛逛17Life喔。</p><br/>
			</div>
		</div><!--正常勾稽 end-->

		<!--連結過期-->
		<div id="section_expired" class="section_expired" runat="server">
		    <div class="result_img"><img src="https://www.17life.com/images/17P/active/20170317_elin/images/payment_failed.png"></div>
		    <div class="dec">
			    <p>您的連結已失效，請與我們客服人員聯繫。</p>
			    <p>(02)2521-9131，服務時間：09:00 ~ 18:00(平日)</p><br/>
			</div>
		</div><!--連結過期 end-->

		<!--重覆點擊-->
		<div id="section_repeat" class="section_repeat" runat="server">
		    <div class="result_img"><img src="https://www.17life.com/images/17P/active/20170317_elin/images/payment_success.png"></div>
		    <div class="dec">
			    <p>訂單已進入退貨程序，我們將儘速為您處理。</p>
			</div>
		</div><!--重覆點擊 end-->

		<div class="link_17life">
			<a href="https://www.17life.com" target="_blank">到 17Life 逛逛</a>
		</div>
	  </div>
	</div>

	<div class="footer clear">
		<a href="https://www.17life.com"><img src="https://www.17life.com/images/17P/active/20170317_elin/images/footer_150x80.png"></a><br/>
		<p>版權所有 © 2017 康太數位整合股份有限公司<br>
        Taiwan (R.O.C) 104 台北市中山北路一段11號13樓</p>
	</div>
</div>
</body>

</html>

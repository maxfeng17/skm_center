﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Optimization;

namespace LunchKingSite.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //BundleTable.EnableOptimizations = true;//debug模式下要壓縮的話，將此行註解拿掉

            bundles.UseCdn = true;
            CssRewriteUrlTransform cssUrlRewriter = new CssRewriteUrlTransform();

            #region Scripts
            //[JS] jquery
            bundles.Add(new ScriptBundle("~/bundles/js/jquery").Include(
                        "~/Tools/js/jquery-1.9.1.min.js",
                        "~/Tools/js/jquery-migrate-1.2.1.min.js"));
            
            //[JS] site.master
            bundles.Add(new ScriptBundle("~/bundles/siteMasterJS").Include(
                        "~/Tools/js/jquery-1.9.1.min.js",
                        "~/Tools/js/jquery-migrate-1.2.1.min.js",
                        "~/Tools/js/jquery-ui-1.9.2.min.js",
                        "~/Tools/js/jquery.blockUI.js",
                        "~/Tools/js/jquery.cycle.all.min.js",
                        "~/Tools/js/effects/nivoslider.js",
                        "~/Tools/js/mrel.js",
                        "~/Tools/js/jquery.Guid.js",
                        "~/Tools/js/jquery.lazyload.min.js",
                        "~/Tools/js/jquery.numeric.js",
                        "~/Tools/js/jquery.mousewheel.min.js",
                        "~/Tools/js/jquery.kinetic.min.js",
                        "~/Tools/js/jquery.smoothdivscroll-1.3-min.js",
                        "~/Tools/js/overlay-channel/modernizr.custom.js",
                        "~/Tools/js/jquery.cookie.js"));

            //[JS] Default.aspx
            bundles.Add(new ScriptBundle("~/bundles/defaultJS").Include(
                        "~/Tools/js/jquery.smartbanner.js",
                        "~/Tools/js/homecook.js",
                        "~/Tools/js/osm/OpenLayers.js",
                        "~/Tools/js/jquery-sliding-menu.js",
                        "~/Tools/js/TouchSlide.1.1.source.js",
                        "~/Tools/js/jquery.SuperSlide.2.1.1.js",
                        "~/Tools/js/ppon/pponDealList.js"));

            //also can by directory
            //bundles.Add(new StyleBundle("~/allStyles").IncludeDirectory("~/Styles", "*.css"));
            #endregion

            #region Styles
            //[CSS] Ppon.Master
            bundles.Add(new StyleBundle("~/bundles/pponMasterCSS")
                                .Include("~/Themes/PCweb/css/MasterPage.css", cssUrlRewriter)
                                .Include("~/Themes/PCweb/css/ppon.css", cssUrlRewriter)
                                .Include("~/Themes/PCweb/plugin/font-awesome/font-awesome.css", cssUrlRewriter)
                                .Include("~/Themes/PCweb/css/PPONEDM.css", cssUrlRewriter)
                                .Include("~/Themes/default/images/17Life/Gactivities/EMS/EventMailSub.css", cssUrlRewriter)
                                .Include("~/Tools/js/jquery.smartbanner.css", cssUrlRewriter)
                                .Include("~/Themes/mobile/css/appbn.css", cssUrlRewriter)
                                .Include("~/Themes/default/images/17Life/sk/swiper.css", cssUrlRewriter));
            //[CSS] Default
            bundles.Add(new StyleBundle("~/bundles/defaultCSS")
                                .Include("~/themes/PCweb/css/Rightside.css", cssUrlRewriter)
                                .Include("~/themes/PCweb/css/RDL-L.css", cssUrlRewriter)
                                .Include("~/themes/PCweb/css/RDL-M.css", cssUrlRewriter)
                                .Include("~/themes/PCweb/css/RDL-S.css", cssUrlRewriter)
                                .Include("~/Themes/default/style/pponDefault.css", cssUrlRewriter)
                                .Include("~/Themes/PCweb/css/ppon_item.min.css", cssUrlRewriter));
            #endregion

            //BundleAndMinifyEachJS(bundles);
        }

        private static void BundleAndMinifyEachJS(BundleCollection bundles) {
            //針對某個目錄內，一支js bundle一份
            //為了讓沒有設定bundle的js也能使用Script.Render()來引入
            //並且取得v=?{hashkey}來當作版號與壓縮

            //↓可能可以改成 "多個資料夾"且"遞迴"
            string rootPath = HostingEnvironment.MapPath("~/Tools/js");//@"C:\Workspace\Cognac\Main\Source\LunchKingSite\LunchKingSite.Web\Tools\js";
            string[] routes = Directory.GetFiles(rootPath).Where(x => x.EndsWith(".js")).ToArray();
            
            foreach (var route in routes)
            {
                string file = Path.GetFileName(route);
                string fileName = Path.GetFileNameWithoutExtension(route).Replace(".", "_");
                bundles.Add(new ScriptBundle(string.Format("~/bundles/{0}", fileName)).Include(string.Format("~/Tools/js/{0}", file)));
            }
        }
    }
}
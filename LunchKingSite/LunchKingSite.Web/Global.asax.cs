﻿using System;
using System.Configuration;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac.Integration.Web;
using Elmah;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.RocketMQTrans;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.WebApi;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Component.MemberActions;
using LunchKingSite.Web.Base;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Optimization;
using Autofac;
using log4net;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.Web
{
    public class Global : HttpApplication, IContainerProviderAccessor
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("Elmah");
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger("global");

        private List<string> sensitive_datalist = new List<string>() { "ExpMonth", "ExpYear", "SecurityCode", "CardNo" };

        protected void Application_Start(object sender, EventArgs e)
        {
            System.IO.FileInfo file = new System.IO.FileInfo(Server.MapPath("~") + "\\" + ConfigurationManager.AppSettings["log4net-config"].Replace('/', '\\'));
            log4net.Config.XmlConfigurator.ConfigureAndWatch(file);
            //SqlServerUtility.LoadNativeAssemblies(Server.MapPath("~/bin"));

            DateTime now = DateTime.Now;
            ContainerProvider = LoaderConfig.Register();

            LogManager.GetLogger("start").Info("啟用:" + (DateTime.Now - now).TotalSeconds.ToString("0.##"));

            ProviderFactory.ReInit(ContainerProvider.RequestLifetime.BeginLifetimeScope());

            TemplateFactory.Instance().Init(Server.MapPath(ConfigurationManager.AppSettings["TemplateDirectory"]),
                bool.Parse(ConfigurationManager.AppSettings["TemplateFactoryDebug"]));
            Jobs.Instance.Start();
            OopsClient.Instance.Start(
                ProviderFactory.Instance().GetConfig().OopsHost,
                ProviderFactory.Instance().GetConfig().OopsTopic);
            this.Error += Application_Error;

            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            RegisterRoutes(RouteTable.Routes);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            ModelMetadataProviders.Current = new LkModelMetadataProvider();
            ModelBinders.Binders.Add(typeof(bool), new BooleanBinder());
            PayEvents.Register();
            MemberEvents.Register();
            FileWaterers.Register();
            GameFacade.Register();
            //SwaggerConfig.Register(GlobalConfiguration.Configuration);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //初始 中台RocketMQTrans
            SKMCenterRocketMQTrans.Start();

            StopMonitorWebRoot();
            MvcHandler.DisableMvcResponseHeader = true;
            GlobalConfiguration.Configuration.EnsureInitialized();
        }

        private void Application_Error(object sender, EventArgs e)
        {
            if (ProviderFactory.Instance().GetConfig().OopsEnabled == false)
            {
                return;
            }
            try
            {
                HttpContext context = HttpContext.Current;
                Exception ex = Server.GetLastError();
                OopsClient.Instance.Write(ex, context);
            } 
            catch (Exception ex)
            {
                logger.Info(ex);
            }
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            //這個設定是為了讓aspx頁面也進Routes
            //如果MAP不到才會去到舊的實體路徑
            //MVC的ROUTE請小心，舊的ASPX是不是會先被ROUTE至CONTROLLER
            //過濾.ASPX請在MapRoute第四個參數加上
            //new { action = @"^(?!.*\.aspx).*$" },
            //or
            //new { action = @"^(?!.*\.(?:aspx|asmx)).*" },
            routes.RouteExistingFiles = true;

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}", new { resource = @"^((?!oops)).*$" });
            routes.IgnoreRoute("store/{*pathInfo}");    //避免意外, 在特選店家整合到目前專案前, 先明確忽略掉
            routes.IgnoreRoute("vbs/webservice/Verificationservice.asmx/{*pathInfo}");

            const string catPattern = @"\d{1,6}";

            routes.MapMvcAttributeRoutes();

            string rsrcConstraint = "[a-zA-Z0-9]+_[a-zA-Z0-9_]+";
            string bidConstraint = @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$";
            string cidConstraint = @"\d{3}";

            routes.MapPageRoute("ErrorHandle", "error-{statusCode}.aspx", "~/Error.aspx", false);

            //2018年1111活動，活動結束後可移除。
            routes.MapRoute(
                "event1111",
                "event1111",
                new { controller = "Event", action = "Event1111" },
                new string[] { "LunchKingSite.WebLib.Controllers" });
            routes.MapRoute(
                "event1212",
                "event1212",
                new { controller = "Event", action = "Event1212" },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            routes.MapRoute(
                "event20190102",
                "event20190102",
                new { controller = "Event", action = "Event20190102" },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            routes.MapRoute(
                "event20190130",
                "event20190130",
                new { controller = "Event", action = "Event20190130" },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            #region 商家後台 - 提案單
            routes.MapRoute(
                name: "HouseProposal",
                url: "sal/proposal/house/proposalcontent",
               defaults: new
               {
                   controller = "Proposal",
                   action = "HouseProposalContent"
               },
               constraints: new { controller = "Proposal", action = "HouseProposalContent" }
            );
            routes.MapRoute(
                name: "VbsHouseProposal",
                url: "vbs/proposal/house/proposalcontent",
               defaults: new
               {
                   controller = "VbsProposal",
                   action = "HouseProposalContent"

               },
               constraints: new { controller = "VbsProposal", action = "HouseProposalContent" }
            );
            routes.MapRoute(
                name: "VbsProductOption",
                url: "vbs/productoption",
               defaults: new
               {
                   controller = "VbsProposal",
                   action = "ProductOption"
               },
               constraints: new { controller = "VbsProposal", action = "ProductOption" }
            );
            routes.MapRoute(
                name: "VbsProductOptionContent",
                url: "vbs/productoptioncontent",
               defaults: new
               {
                   controller = "VbsProposal",
                   action = "ProductOptionContent"
               },
               constraints: new { controller = "VbsProposal", action = "ProductOptionContent" }
            );
            routes.MapRoute(
                name: "SalesProductOption",
                url: "sal/productoption",
               defaults: new
               {
                   controller = "Proposal",
                   action = "ProductOption"
               },
               constraints: new { controller = "Proposal", action = "ProductOption" }
            );
            routes.MapRoute(
                name: "SalesProductOptionContent",
                url: "sal/productoptioncontent",
               defaults: new
               {
                   controller = "Proposal",
                   action = "ProductOptionContent"
               },
               constraints: new { controller = "Proposal", action = "ProductOptionContent" }
            );
            routes.MapRoute(
                "SalesProposal",
                "sal/proposal/{action}",
                new { controller = "Proposal", action = "Index" },
                new string[] { "LunchKingSite.WebLib.Controllers" });
            routes.MapRoute(
                "VbsProposal",
                "vbs/proposal/{action}",
                new { controller = "VbsProposal", action = "Index" },
                new string[] { "LunchKingSite.WebLib.Controllers" });
            #endregion 商家後台 - 提案單

            #region MGM

            routes.MapRoute(
                "User",
                "User/{action}",
                new { controller = "User" },
                new { action = @"^(?!.*\.(?:aspx|asmx)).*" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "ContactDetails",
                "User/Contact/Datails/{id}",
                new { Controller = "User", action = "ContactDetails", id = UrlParameter.Optional },
               constraints: new
               {
                   id = @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"
               }
            );

            //領取禮物，導到中繼頁
            routes.MapRoute(
                "AccessCodeGet",
                "g/{accessCode}",
                new { Controller = "User", action = "GiftInFo", accessCode = UrlParameter.Optional },
               constraints: new
               {
                   accessCode = @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"
               }
            );

            #endregion


            #region 策展相關

            routes.MapPageRouteWithName("BrandEvent2", "Event/brandevent/{u}", "~/Event/brandevent.aspx", false, null, new RouteValueDictionary { });
            routes.MapPageRouteWithName("BrandMobile2", "Event/brandmobile/{u}", "~/Event/brandmobile.aspx", false, null, new RouteValueDictionary { });

            routes.MapRoute(
                "Event",
                "Event/{action}",
                new { controller = "Event" },
                new { action = @"^(?!.*\.(?:aspx|asmx)).*" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            //策展商品頁
            routes.MapRoute("brandevent", "event/brandevent/{u}", new { controller = "Event", action = "BrandEvent", u = new RouteValueDictionary { { "u", @"[^.]+" } } }, new string[] { "LunchKingSite.WebLib.Controllers" });
            routes.MapRoute("brandmobile", "event/brandmobile/{u}", new { controller = "Event", action = "BrandMobile", u = new RouteValueDictionary { { "u", @"[^.]+" } } }, new string[] { "LunchKingSite.WebLib.Controllers" });


            #endregion


            #region 商家後臺 - 出貨管理

            routes.MapRoute(
                "VendorBillingSystemShipActionIdTypeProcess",
                "vbs/ship/{action}/{id}/{type}",
                new { controller = "VendorBillingSystemShip" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "VendorBillingSystemShip",
                "vbs/ship/{action}",
                new { controller = "VendorBillingSystemShip" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "VendorBillingSystemActionIdTypeStoreIdProcess",
                "vbs/{action}/{id}/{type}/{detailId}",
                new { controller = "VendorBillingSystem", detailId = UrlParameter.Optional },
                new { action = @"^(?!.*\.(?:aspx|asmx)).*" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "VendorBillingSystemDefault",
                "vbs/{action}",
                new { controller = "VendorBillingSystem" },
                new { action = @"^(?!.*\.(?:aspx|asmx)).*" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            #endregion

            #region 商家後臺 - 預約管理

            routes.MapRoute(
                "BookingSystem",
                "vbs/BookingSystem/{action}",
                new { controller = "BookingSystem" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "ReserveLock",
                "vbs/ReserveLock/{action}",
                new { controller = "ReserveLock" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            #endregion 商家後臺 - 預約管理

            #region 商家後台 - 評價系統

            routes.MapRoute(
                "UserEvaluation",
                "vbs/UserEvaluation/{action}",
                new { controller = "UserEvaluation" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            #endregion

            #region 商家後台 - 購物車
            routes.MapRoute(
                "VendorBillingSystemShoppingCart",
                "vbs/sc/{action}",
                new { controller = "VendorBillingSystemShoppingCart" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );
            #endregion 商家後台 - 購物車

            #region 紅利Pin碼系統

            routes.MapRoute("FamiportPincode", "FamiportPincode/{action}",
                new { controller = "FamiportPincode" },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            #endregion

            #region Blog
            routes.MapRoute(
                name: "Blog",
                url: "blog/{blogid}",
               defaults: new
               {
                   controller = "Blog",
                   action = "Index"
               }
            );
            #endregion Blog

            #region 砍價遊戲

            routes.MapRoute(
                "GameHome",
                "game/{campaignName}/home",
                new { controller = "Game", action = "Home" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );
            routes.MapRoute(
                "Game",
                "game/{action}",
                new { controller = "Game" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );


            routes.MapRoute(
                 "GameRoom",
                 "ControlRoom/Game/{action}",
                 new { controller = "GameRoom", action = "Home" },
                 new string[] { "LunchKingSite.WebLib.Controllers.ControlRoom" }
             );

            #endregion 砍價遊戲

            routes.MapRoute(
                "SkmEvent",
                "SkmEvent/{action}",
                new { controller = "SkmEvent" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "SkmEventActivity",
                "SkmEvent/Activity/{token}",
                new { controller = "SkmEvent", action = "Activity", token = UrlParameter.Optional },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "SkmEventPrizeDrawEvent",
                "SkmEvent/PrizeDrawEvent/{token}",
                new { controller = "SkmEvent", action = "PrizeDrawEvent", token = UrlParameter.Optional },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "SkmEventPrizeDrawResult",
                "SkmEvent/PrizeDrawResult/{token}",
                new { controller = "SkmEvent", action = "PrizeDrawResult", token = UrlParameter.Optional },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );


            routes.MapRoute(
                "SKMPrizeList",
                "SKMDeal/PrizeList",
                new { controller = "SKMDeal", action = "SKMList", dealType = "PrizeDeals" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "SKMPrizeDraw",
                "SKMDeal/SKMBurningDealInfo/{dealType}",
                new { controller = "SKMDeal", action = "SKMBurningDealInfo", dealType = UrlParameter.Optional },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute("SKMDeal", "SKMDeal/{action}",
              new { controller = "SKMDeal" },
              new string[] { "LunchKingSite.WebLib.Controllers" });

            routes.MapRoute("MasterPass", "MasterPass/{action}",
              new { controller = "MasterPass" },
              new string[] { "LunchKingSite.WebLib.Controllers" });

            routes.MapRoute("ISP", "ISP/{action}",
                new { controller = "ISP" },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            #region ControlRoom
            routes.MapRoute(
                "WebEdit",
                "ControlRoom/WebEdit/{action}",
                new { controller = "WebEdit" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );
            #region 折價券
            routes.MapRoute(
                    "DiscountCode",
                    "ControlRoom/DiscountCode/{action}",
                    new { controller = "DiscountCode" },
                    new string[] { "LunchKingSite.WebLib.Controllers" }
                );
            #endregion

            #region EDM
            routes.MapRoute(
                "Edm",
                "ControlRoom/Edm/{action}",
                new { controller = "Edm" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );
            #endregion

            //商家核銷對帳系統 - 帳號管理 and 權限管理
            routes.MapRoute(
                "VbsCompanyAccountManagement",
                "ControlRoom/vbs/membership/{action}",
                new { controller = "VendorBillingSystemMembership" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );


            //沒特定分類的 VBS 後台功能
            routes.MapRoute(
                "VbsBackend",
                "ControlRoom/vbs/{action}",
                new { controller = "VendorBillingSystemBackend" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "BalanceSheetGeneration",
                "ControlRoom/simulator/{action}",
                new { controller = "VendorBillingSystemSimulator" },
                new { action = @"^(?!.*\.aspx).*$" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "HiDealVerification",
                "ControlRoom/HiDealVerification/{action}",
                new { controller = "HiDealVerification" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "OAuthControlroom",
                "ControlRoom/OAuth/{action}",
                new { controller = "OAuth" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "SampleControlroom",
                "ControlRoom/Sample/{action}",
                new { controller = "Sample" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            //商家後台-物流商
            routes.MapRoute(
                "VendorBillingSystemShipCompanyManagement",
                "ControlRoom/vbs/membership/{action}",
                new { controller = "MembershipBackend" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            //服務商通報系統
            routes.MapRoute(
                "SystemPartner",
                "PartnerReportingSystem/{action}",
                new { controller = "PartnerReportingSystem" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            //客服系統
            routes.MapRoute(
                "ServicetManagement",
                "ControlRoom/api/customerservice/{action}",
                new { controller = "Customerservice" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );
            #region Ppon後台新分類設定

            routes.MapRoute("CategoryControlroom", "ControlRoom/Category/{action}",
                            new { controller = "Category" },
                            new string[] { "LunchKingSite.WebLib.Controllers" });

            #endregion Ppon後台新分類設定


            #region 資策會後台
            routes.MapRoute(
                "IDEASControlroom",
                "ControlRoom/IDEAS/{action}",
                new { controller = "IDEAS" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );
            #endregion 資策會後台

            routes.MapRoute(
                "MemberControlroom",
                "ControlRoom/Member/{action}",
                new { controller = "Member" },
                new { action = @"^(?!.*\.aspx).*$" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "SellerControlroom",
                "ControlRoom/Seller/{action}",
                new { controller = "Seller" },
                new { action = @"^(?!.*\.aspx).*$" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "OrderControlroom",
                "ControlRoom/Order/{action}",
                new { controller = "Order" },
                new { action = @"^(?!.*\.aspx).*$" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "FinanceControlroom",
                "ControlRoom/Finance/{action}",
                new { controller = "Finance" },
                new { action = @"^(?!.*\.aspx).*$" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "SystemControlroom",
                "ControlRoom/System/{action}",
                new { controller = "System" },
                new { action = @"^(?!.*\.aspx).*$" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "PponSetup",
                "ControlRoom/PponSetup/{action}",
                new { controller = "PponSetup" },
                new { action = @"^(?!.*\.aspx).*$" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "Maintainance",
                "ControlRoom/Maintainance/{action}",
                new { controller = "Maintainance" },
                new string[] { "LunchKingSite.WebLib.Controllers.ControlRoom" }
            );

            routes.MapRoute(
                "ThemeCuration",
                "ControlRoom/ThemeCuration/{action}",
                new { controller = "ThemeCuration" },
                new { action = @"^(?!.*\.aspx).*$" },
                new string[] { "LunchKingSite.WebLib.Controllers.ControlRoom" }
            );

            routes.MapRoute(
                "ChannelControlroom",
                "ControlRoom/Channel/{action}",
                new { controller = "Channel" },
                new { action = @"^(?!.*\.aspx).*$" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "PponControlroom",
                "ControlRoom/Ppon/{action}",
                new { controller = "PponControlRoom" },
                new { action = @"^(?!.*\.aspx).*$" },
                new string[] { "LunchKingSite.WebLib.Controllers.ControlRoom" }
            );

            routes.MapRoute(
                "DealTypeRoom",
                "ControlRoom/DealType/{action}",
                new { controller = "DealTypeRoom", action = "Index" },
                new { action = @"^(?!.*\.aspx).*$" },
                new string[] { "LunchKingSite.WebLib.Controllers.ControlRoom" }
            );

            #region 新業務系統
            routes.MapRoute(
                "SalesSystemController",
                "Sal/{action}",
                new { controller = "SalesSystem" },
                new { action = @"^(?!.*\.(?:aspx|ashx)).*" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "SalesSystemBackend",
                "ControlRoom/Sal/{action}",
                new { controller = "SalesSystemBackend" },
                new { action = @"^(?!.*\.(?:aspx|ashx)).*" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );
            #endregion 新業務系統

            #region 17Pay

            routes.MapRoute(
                "MoblieWalletAuth",
                "MobilePayment/{action}",
                new { controller = "MobilePayment", action = "Index" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "WalletPayment",
                "ControlRoom/WalletPayment/{action}",
                new { controller = "WalletPayment" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            #endregion 17Pay

            #endregion ControlRoom

            routes.MapRoute(
                "WebViewVendorSystemDefault",
                "m/vbs/{action}",
                new { controller = "WebViewVendorSystem" },
                new { action = @"^(?!.*\.(?:aspx|asmx)).*" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
             );

            routes.MapRoute(
                "WebViewOrderDefault",
                "m/order/{action}",
                new { controller = "WebViewOrder" },
                new { action = @"^(?!.*\.(?:aspx|asmx)).*" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
             );

            #region mbrowser

            routes.MapRoute(
                "m_17Life_Delivery",
                "delivery/{cid}/{rsrc}",
                new { controller = "Mobile", action = "Index", cid = UrlParameter.Optional, rsrc = rsrcConstraint },
                new { cid = @"\d{3}", mobileWidth = new MobileWidthConstraint() },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            routes.MapRoute(
                "m_17Life_Index_Cid",
                "m/{cid}/{cat}",
                new { controller = "Mobile", action = "Index", cid = UrlParameter.Optional, cat = UrlParameter.Optional },
                new { cid = @"\d{3}", cat = @"^[0-9]*$" },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            routes.MapRoute(
                "m_17Life_Index",
                "m/Index",
                new { controller = "Mobile", action = "Home" },
                new { },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            routes.MapRoute(
                "m_17Life_food",
                "m/Food",
                new { controller = "Mobile", action = "Index" },
                new { },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            routes.MapRoute(
                "m_17Life_Home",
                "m/Home",
                new { controller = "Mobile", action = "Home" },
                new { },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            routes.MapRoute(
                "m_17Life_Home2",
                "m",
                new { controller = "Mobile", action = "Home" },
                new { },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            #region Mobile Deal Detail Page 產品頁/商品頁

            routes.MapRoute(
                "17Life_MobileDeal1of5",
                "m/deal/{bid}",
                new { controller = "Mobile", action = "deal" },
                new RouteValueDictionary { { "bid", bidConstraint } });
            routes.MapRoute("17Life_MobileDeal2of5", "deal/{bid}",
                new { controller = "Mobile", action = "deal" },
                new RouteValueDictionary
                {
                    {
                        "controller" , new MobilePageConstraint()
                    },
                    {
                        "bid", bidConstraint
                    }
                });
            routes.MapRoute("17Life_MobileDeal3of5", "{bid}",
                new { controller = "Mobile", action = "deal" },
                new RouteValueDictionary
                {
                    {
                        "controller" , new MobilePageConstraint()
                    },
                    {
                        "bid", bidConstraint
                    }
                });
            routes.MapRoute("17Life_MobileDeal4of5", "{cid}/{bid}",
                new { controller = "Mobile", action = "deal" },
                new RouteValueDictionary
                {
                    {
                        "controller" , new MobilePageConstraint()
                    },
                    {
                        "bid", bidConstraint
                    },
                    {
                        "cid", cidConstraint
                    }
                });
            routes.MapRoute("17Life_MobileDeal5of5", "{bid}/{rsrc}",
                new { controller = "Mobile", action = "deal" },
                new RouteValueDictionary
                {
                    {
                        "controller" , new MobilePageConstraint()
                    },
                    {
                        "bid", bidConstraint
                    },
                    {
                        "rsrc", rsrcConstraint
                    }
                });

            #endregion

            routes.MapRoute(
                "m_17Life_action",
                "m/{action}",
                new { controller = "Mobile", action = "Index" },
                new { action = @"^(?!.*\.aspx).*$" },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            #endregion mbrowser

            routes.MapRoute("BeaconTrigger", "Beacon/{action}",
                new { controller = "Beacon" },
                new { action = "Trigger" },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            routes.MapRoute("Beacon", "Beacon/{action}/{functionName}",
                new { controller = "Beacon" },
                new { },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            #region 問卷調查

            routes.MapRoute(
                "Questionnaire",
                "Questionnaire/{action}",
                new { controller = "Questionnaire" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
                "Questionnaire_Event",
                "Questionnaire/Event/{token}",
                new { controller = "Questionnaire", action = "Event", token = UrlParameter.Optional },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            #endregion

            #region 限時優惠

            routes.MapRoute(
                "LimitedTimeSelectionRoom",
                "ControlRoom/LimitedTimeSelection/{action}",
                new { controller = "LimitedTimeSelectionRoom", action = "Index" },
                new { action = @"^(?!.*\.aspx).*$" },
                new string[] { "LunchKingSite.WebLib.Controllers.ControlRoom" }
            );

            routes.MapRoute(
                "RushBuy",
                "rushbuy/{id}",
                new { controller = "Event", action = "RushBuy" },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            routes.MapRoute(
                "RushBuyLoader",
                "rushbuyLoader",
                new { controller = "Event", action = "RushBuyLoader" },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            #endregion

            #region line綁定

            routes.MapRoute(
                "NewMemberConfirmMember",
                "NewMember/ConfirmMember",
                new { controller = "NewMember", action = "ConfirmMember" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
               "NewMemberMemberBinding",
               "NewMember/MemberBinding",
               new { controller = "NewMember", action = "MemberBinding" },
               new string[] { "LunchKingSite.WebLib.Controllers" }
           );

            routes.MapRoute(
                "NewMemberRegister",
                "NewMember/Register",
                new { controller = "NewMember", action = "Register" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
               "NewMemberRegisterEmailValidation",
               "NewMember/RegisterEmailValidation",
               new { controller = "NewMember", action = "RegisterEmailValidation" },
               new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            routes.MapRoute(
              "NewMemberRegisterMobileValidation",
              "NewMember/RegisterMobileValidation",
              new { controller = "NewMember", action = "RegisterMobileValidation" },
              new string[] { "LunchKingSite.WebLib.Controllers" }
           );

            #endregion 

            routes.MapRoute(
                "mvc",
                "mvc/{controller}/{action}",
                new { },
                new { },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            routes.MapRoute(
                "RegularsSystemDefault",
                "RegularsSystem/{action}",
                new { controller = "RegularsSystem" },
                new { action = @"^(?!.*\.(?:aspx|asmx)).*" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
            );

            //android先進來量尺寸
            routes.MapPageRouteWithName(
                "Android_Take_Width",
                "ppon/default.aspx",
                "~/Ppon/GetClientInfo.aspx",
                false, null, new RouteValueDictionary(new { controller = new AndroidConstraint() }));

            //將M版攔走
            routes.MapRoute(
                "m_17Life_Default_dir",
                "ppon/default.aspx"
                , new { controller = "Mobile", action = "Home" },
                new { mobileWidth = new MobileWidthConstraint(), bid = new DealListConstraint() });

            #region ppon

            routes.MapPageRouteWithName("17Life_Event", "ppon/Event/{url}", "~/ppon/event.aspx", false, null, new RouteValueDictionary { { "url", @"[^.]+" } });
            routes.MapPageRouteWithName("17Life_TodayDeal_cid", "TodayDeals/{cid}", "~/ppon/todaydeal.aspx", false, null, new RouteValueDictionary { { "cid", @"\d{3}" } });
            routes.MapPageRouteWithName("17Life_TodayDeal", "{locate}", "~/ppon/todaydeal.aspx", false, null, new RouteValueDictionary { { "locate", "TodayDeals" } });
            routes.MapPageRouteWithName("17Life_Default_CidBidCpa", "{cid}/{bid}/{cpa}", "~/ppon/detail.aspx", false, null, new RouteValueDictionary { { "bid", @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$" }, { "cid", @"\d{3}" }, { "cpa", @"\A(cpa|CPA)-\w*.\w*" } });

            routes.MapPageRouteWithName("17Life_Default_CidBidRsrc", "{cid}/{bid}/{rsrc}", "~/ppon/detail.aspx", false, null,
                new RouteValueDictionary
                {
                    {"bid", @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"},
                    {"cid", @"\d{3}"},
                    {"rsrc", rsrcConstraint}
                });
            routes.MapPageRouteWithName("17Life_Default_BidRsrcCpa", "{bid}/{rsrc}/{cpa}", "~/ppon/detail.aspx", false, null,
                new RouteValueDictionary
                {
                    {"bid", @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"},
                    {"rsrc", rsrcConstraint},
                    {"cpa", @"\A(cpa|CPA)-\w*.\w*"}
                });
            //商品頁 比重高 編號 1 of 4 
            routes.MapPageRouteWithName("17Life_Default_CidBid",
                "{cid}/{bid}", "~/ppon/detail.aspx", false, null,
                new RouteValueDictionary
                {
                    { "bid", @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$" }, { "cid", @"\d{3}" }
                });

            routes.MapRoute("17Life_M_Search",
                "ppon/pponsearch.aspx"
                , new { controller = "Mobile", action = "SearchResult" },
                new { mobileWidth = new MobileWidthConstraint() });


            routes.MapPageRouteWithName("17Life_Default_CidAidCat", "{cid}/{aid}/{cat}", "~/ppon/default.aspx", false, null, new RouteValueDictionary { { "cid", @"\d{3}" }, { "aid", @"\d{1,4}" }, { "cat", catPattern } });
            routes.MapPageRouteWithName("17Life_Default_CidCat", "{cid}/{cat}", "~/ppon/default.aspx", false, null, new RouteValueDictionary { { "cid", @"\d{3}" }, { "cat", catPattern } });
            routes.MapPageRouteWithName("17Life_DeliveryCidAidCpa", "delivery/{cid}/{aid}/{cpa}", "~/ppon/default.aspx", false, null, new RouteValueDictionary { { "cid", @"\d{3}" }, { "aid", @"\d{3}" }, { "cpa", @"\A(cpa|CPA)-\w*.\w*" } });
            routes.MapPageRouteWithName("17Life_DeliveryCidCpa", "delivery/{cid}/{cpa}", "~/ppon/default.aspx", false, null, new RouteValueDictionary { { "cid", @"\d{3}" }, { "cpa", @"\A(cpa|CPA)-\w*.\w*" } });
            routes.MapPageRouteWithName("17Life_DeliveryCidAidCat", "delivery/{cid}/{aid}/{cat}", "~/ppon/default.aspx", false, null, new RouteValueDictionary { { "cid", @"\d{3}" }, { "aid", @"\d{3}" }, { "cat", @"(-1|\d{1,6})" } });
            routes.MapPageRouteWithName("17Life_DeliveryCidCat", "delivery/{cid}/{cat}", "~/ppon/default.aspx", false, null, new RouteValueDictionary { { "cid", @"\d{3}" }, { "cat", @"(-1|\d{1,6})" } });
            routes.MapPageRouteWithName("17Life_DeliveryCid", "delivery/{cid}", "~/ppon/default.aspx", false, null, new RouteValueDictionary { { "cid", @"\d{3}" } });
            routes.MapPageRouteWithName("17Life_DeliveryCidRsrc", "delivery/{cid}/{rsrc}", "~/ppon/default.aspx", false, null,
                new RouteValueDictionary { { "cid", @"\d{3}" }, { "rsrc", rsrcConstraint } });
            routes.MapPageRouteWithName("17Life_Default_BidCpa", "{bid}/{cpa}", "~/ppon/detail.aspx", false, null, new RouteValueDictionary { { "bid", @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$" }, { "cpa", @"\A(cpa|CPA)-\w*.\w*" } });
            //商品頁 比重高 編號 2 of 4 
            routes.MapPageRouteWithName("17Life_Default_BidRsrc", "{bid}/{rsrc}", "~/ppon/detail.aspx", false, null,
                new RouteValueDictionary
                {
                    {"bid", @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"},
                    {"rsrc", rsrcConstraint}
                });
            routes.MapPageRouteWithName("17Life_Default_CidRsrc", "{cid}/{rsrc}", "~/ppon/default.aspx", false, null,
                new RouteValueDictionary { { "cid", @"\d{3}" }, { "rsrc", rsrcConstraint } });
            routes.MapPageRouteWithName("17Life_Default_CidCpa", "{cid}/{cpa}", "~/ppon/default.aspx", false, null, new RouteValueDictionary { { "cid", @"\d{3}" }, { "cpa", @"\A(cpa|CPA)-\w*.\w*" } });
            //商品頁 比重高 編號 3 of 4 
            routes.MapPageRouteWithName("17Life_Default_Bid", "{bid}", "~/ppon/detail.aspx", false, null,
                new RouteValueDictionary {
                {
                    "bid", @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"
                } });

            //全家頁面使用 /490?rsrc=17_SBN
            routes.MapPageRouteWithName("17Life_Default_Cid", "{cid}", "~/ppon/default.aspx", false, null, new RouteValueDictionary { { "cid", @"\d{3}" } });

            //隱私權相關頁面轉由ppon移置NewMembr下，安全機制
            routes.MapPageRouteWithName("17Life_Policy", "{locate}/{page}", "~/newmember/privacypolicy.aspx", false, null, new RouteValueDictionary { { "locate", "ppon" }, { "page", "policy.aspx" } });
            routes.MapPageRouteWithName("17Life_Privacy", "{locate}/{page}", "~/newmember/privacy.aspx", false, null, new RouteValueDictionary { { "locate", "ppon" }, { "page", "privacy.aspx" } });
            routes.MapPageRouteWithName("17Life_PrivacyGoogle", "{locate}/{page}", "~/newmember/privacy.aspx", false, null, new RouteValueDictionary { { "locate", "ppon" }, { "page", "privacygoogle.aspx" } });
            routes.MapPageRouteWithName("17Life_PrivacyPolicy", "{locate}/{page}", "~/newmember/privacypolicy.aspx", false, null, new RouteValueDictionary { { "locate", "ppon" }, { "page", "privacypolicy.aspx" } });



            routes.MapPageRouteWithName(
                "m_default_Android_Take_Width",
                "default.aspx",
                "~/Ppon/GetClientInfo.aspx",
                false, null, new RouteValueDictionary(new { controller = new AndroidConstraint() }));
            routes.MapRoute(
                "m_Default",
                "default.aspx"
                , new { controller = "Mobile", action = "Home" },
                new { mobileWidth = new MobileWidthConstraint(), bid = new DealListConstraint() });
            routes.MapPageRouteWithName("Default", "default.aspx", "~/ppon/default.aspx", false, null, new RouteValueDictionary { });

            routes.MapRoute(
                "Ppon_ContactUs",
                "Ppon/ContactUs",
                new { controller = "Ppon", action = "ContactUs" },
                new string[] { "LunchKingSite.WebLib.Controllers" });


            #region deeplink

            routes.MapRoute("17Life_MobileChannel", "channel/{ch}",
                new { controller = "Mobile", action = "index" },
                new RouteValueDictionary
                {
                    {
                        "controller" , new MobilePageConstraint()
                    }
                });
            routes.MapPageRouteWithName("17Life_Channel", "channel/{ch}", "~/ppon/default.aspx", false, null, new RouteValueDictionary { { "ch", @"\d{2,8}" } });

            routes.MapPageRouteWithName("17Life_Channel_muti", "channel/{ch},{aid},{cat}", "~/ppon/default.aspx", false, null, new RouteValueDictionary { { "ch", @"\d{2,8}" }, { "aid", @"\d{1,4}" }, { "cat", catPattern } });
            //商品頁 比重高 編號 4 of 4 (雖然預定用這個取代其它所有的商品頁route設定，2017/09/30統計時仍然僅占30%
            routes.MapPageRouteWithName("17Life_Deal", "deal/{bid}", "~/ppon/detail.aspx", false, null,
                new RouteValueDictionary
                {
                    { "bid", @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$" }
                });

            #endregion

            #region Partial View

            routes.MapRoute(
                "Ppon_SideDeals",
                "ppon/partial/sidedeals",
                new { controller = "Ppon", action = "LoadSideDeals", workCityId = 476, takeCount = 20 },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            routes.MapRoute(
                "Ppon_TodayDeals",
                "ppon/partial/todaydeals",
                new { controller = "Ppon", action = "LoadTodayDeals", days = UrlParameter.Optional },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            routes.MapRoute(
                "Ppon_SearchTodayDeals",
                "ppon/partial/searchtodaydeals",
                new { controller = "Ppon", action = "LoadSearchTodayDeals", days = UrlParameter.Optional, cityid = UrlParameter.Optional },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            routes.MapRoute(
                "Ppon_SearchDeals",
                "ppon/partial/searchdeals",
                new { controller = "Ppon", action = "LoadSearchDeals" },
                new string[] { "LunchKingSite.WebLib.Controllers" });

            routes.MapRoute(
                name: "PponPartial",
                url: "ppon/partial/{action}/{id}",
                defaults: new { controller = "Ppon", action = "Index", id = UrlParameter.Optional }
            );

            #endregion Partial View

            #endregion ppon

            #region Piinlife

            routes.MapPageRouteWithName("17Life_PiinlifeBuy", "{locate}/{page}", "~/ppon/buy.aspx", false, null, new RouteValueDictionary { { "locate", "piinlife" }, { "page", "buy.aspx" } });
            routes.MapPageRouteWithName("17Life_PiinlifeBid", "piinlife/{bid}", "~/piinlife/deal.aspx", false, null, new RouteValueDictionary { { "bid", @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$" } });
            routes.MapPageRouteWithName("17Life_PiinlifeBidRsrc", "piinlife/{bid}/{rsrc}", "~/piinlife/deal.aspx", false, null,
                new RouteValueDictionary
                {
                    {"bid", @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"},
                    {"rsrc", rsrcConstraint}
                });
            routes.MapPageRouteWithName("17Life_PiinlifeCatBid", "piinlife/{cat}/{bid}", "~/piinlife/deal.aspx", false, null, new RouteValueDictionary { { "cat", catPattern }, { "bid", @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$" } });
            routes.MapPageRouteWithName("17Life_PiinlifeCatBidRsrc", "piinlife/{cat}/{bid}/{rsrc}", "~/piinlife/deal.aspx", false, null,
                new RouteValueDictionary
                {
                    {"cat", catPattern},
                    {"bid", @"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"},
                    {"rsrc", rsrcConstraint}
                });

            #endregion

            #region APP跳轉網頁用

            routes.MapRoute(
                "AppVBS",
                "VBS_R/{action}",
                new { controller = "AppVBS" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
                );

            routes.MapRoute(
                "App",
                "APP/{action}",
                new { controller = "App" },
                new string[] { "LunchKingSite.WebLib.Controllers" }
                );

            #endregion APP跳轉網頁用

            #region public app api

            routes.MapRoute(
                "PublicAppApi",
                "V1/APP/{action}",
                new { controller = "App" },
                new string[] { "LunchKingSite.WebLib.Controllers.V1" }
                );

            #endregion

            #region 安全性考量 Elmah鎖ip
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["elmah-ips"]))
            {
                string[] ips = ConfigurationManager.AppSettings["elmah-ips"].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                routes.Add("elmah", new Route("oops.axd/{*pathInfo}", null, new RouteValueDictionary { { "", new IpConstraint(ips) } }, new ElmahRouteHandler()));
            }
            #endregion
        }

        protected void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown
            string errorMessage = "Application ending";
            HttpRuntime runtime = (HttpRuntime)typeof(HttpRuntime).InvokeMember("_theRuntime", BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.GetField, null, null, null);
            if (runtime != null)
            {
                string shutDownMessage = (string)runtime.GetType().InvokeMember("_shutDownMessage", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField, null, runtime, null);
                string shutDownStack = (string)runtime.GetType().InvokeMember("_shutDownStack", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField, null, runtime, null);
                errorMessage = string.Format("{0}\r\n\r\n_shutDownMessage={1}\r\n\r\n_shutDownStack={2}", errorMessage, shutDownMessage, shutDownStack);
            }
            log4net.LogManager.GetLogger("Global").Warn(errorMessage);

            ProviderFactory.Instance().GetProvider<IMessageQueueProvider>().Disconnect();

            //關閉 新光中台RocketMQ
            SKMCenterRocketMQTrans.ShutdownProducer();

            Jobs.Instance.Stop();
            PostMan.Instance().Execute(null);
        }

        protected void Application_PreSendRequestHeaders()
        {
            Response.Headers.Remove("Server");
            Response.Headers.Remove("X-AspNet-Version");
            Response.Headers.Remove("X-Powered-By");
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        protected void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

        void ErrorLog_Logged(object sender, ErrorLoggedEventArgs args)
        {
            if (Request["log"] == "1")
            {
                var lastErr = Server.GetLastError();
                log.Error("[ErrorLog_Logged]", lastErr);
                Response.Write(lastErr);
                Response.End();
                return;
            }

            HttpCookie errorid = CookieManager.NewCookie("ErrorId");
            errorid.Value = args.Entry.Id;
            if (HttpContext.Current.Response.Cookies["ErrorId"] == null)
            {
                HttpContext.Current.Response.Cookies.Add(errorid);
            }
            else
            {
                HttpContext.Current.Response.Cookies.Set(errorid);
            }

        }

        void ErrorLog_Filtering(object sender, ExceptionFilterEventArgs e)
        {
            //log.Info("Elmah error log filter start!");
            if (e.Exception.GetBaseException() is HttpRequestValidationException)
            {
                log.Info("Elmah error message is : " + e.Exception.Message);
                if (e.Exception.Message.IndexOf("logoutRequest=") >= 0)
                {
                    e.Dismiss();
                    return;
                }
            }
            //配合 DefensModules，BAN掉的不另外記錄，節省資源
            var ex = e.Exception as HttpException;
            if (ex != null && ex.GetHttpCode() == 403)
            {
                e.Dismiss();
                return;
            }

            //付款頁將刷卡資訊隱藏
            if (e.Context is HttpContext)
            {
                HttpContext httpcontext = (HttpContext)e.Context;
                HttpRequest request = httpcontext.Request;
                if (request != null && request.Form.AllKeys.Length > 0
                    && request.Path.Contains("buy.aspx", StringComparison.OrdinalIgnoreCase))
                {
                    var new_error = new Elmah.Error(e.Exception, httpcontext);
                    foreach (var item in request.Form.OfType<string>().Where(x => sensitive_datalist.Any(y => x.IndexOf(y, StringComparison.OrdinalIgnoreCase) > 0)))
                    {
                        new_error.Form.Set(item, "XXXX");
                    }
                    Elmah.ErrorLog.GetDefault(httpcontext).Log(new_error);
                    e.Dismiss();
                }
            }
        }

        static IContainerProvider _containerProvider;
        public IContainerProvider ContainerProvider
        {
            get
            {
                return _containerProvider;
            }
            private set
            {
                _containerProvider = value;
            }
        }

        /// <summary>
        /// _dirMonSubdirs 停掉，
        /// 仍然有監控其它主要特別路徑，譬如 bin,app_code,  web.config 檔等
        /// </summary>
        private void StopMonitorWebRoot()
        {
            PropertyInfo pi = typeof(HttpRuntime).GetProperty("FileChangesMonitor",
                BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
            object o = pi.GetValue(null, null);
            FieldInfo f = o.GetType().GetField("_dirMonSubdirs", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.IgnoreCase);
            object monitor = f.GetValue(o);
            MethodInfo m = monitor.GetType().GetMethod("StopMonitoring", BindingFlags.Instance | BindingFlags.NonPublic);
            m.Invoke(monitor, new object[] { });
        }
    }

    public class LoaderConfig
    {
        public static ContainerProvider Register()
        {
            ContainerBuilder builder = new ContainerBuilder();
            new LunchKingSite.Core.CoreModule().RegisterWithContainer(builder);
            new LunchKingSite.BizLogic.BizLogicModule().RegisterWithContainer(builder);
            new LunchKingSite.Mongo.MongoModule().RegisterWithContainer(builder);
            new LunchKingSite.SsBLL.SsBllModule().RegisterWithContainer(builder);
            new LunchKingSite.Web.LkWebModule().RegisterWithContainer(builder);
            new LunchKingSite.WebLib.WebLibModule().RegisterWithContainer(builder);

            IContainer container = builder.Build();

            new LunchKingSite.Core.CoreModule().Initialize(container);
            new LunchKingSite.BizLogic.BizLogicModule().Initialize(container);
            new LunchKingSite.Mongo.MongoModule().Initialize(container);
            new LunchKingSite.SsBLL.SsBllModule().Initialize(container);
            new LunchKingSite.Web.LkWebModule().Initialize(container);
            new LunchKingSite.WebLib.WebLibModule().Initialize(container);

            return new ContainerProvider(container);
        }
    }

    public class AndroidConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            return LunchKingSite.BizLogic.Facade.CommonFacade.AndroidCheckSize();
        }
    }
    public class MobileWidthConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            return LunchKingSite.BizLogic.Facade.CommonFacade.ToMobileVersion();
        }
    }
    public class MobilePageConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();

            if (config.MDealEnabled)
            {
                return CommonFacade.IsMobileVersion();
            }
            return false;
        }
    }
    public class DealListConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            return httpContext.Request["bid"] == null;
        }
    }

}

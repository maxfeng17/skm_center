﻿using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Verification
{
    public partial class banks : RolePage, IVerificationBanks
    {
        #region event

        public event EventHandler<DataEventArgs<string>> SearchLog = null;

        #endregion event

        #region props

        private VerificationBanksPresenter _presenter;

        public VerificationBanksPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get
            {
                return this.Page.User.Identity.Name;
            }
        }

        public TrustBankType TrustType
        {
            get
            {
                return TrustBankType.Trust;
            }
        }

        public TrustLogType LogType
        {
            get
            {
                return (TrustLogType)(int.Parse(ddl_Type.SelectedValue));
            }
        }

        public TrustProvider Provider
        {
            get
            {
                TrustProvider provider;
                if (!string.IsNullOrEmpty(hif_Bank.Value))
                {
                    return Enum.TryParse<TrustProvider>(hif_Bank.Value, out provider) ? provider : TrustProvider.Initial;
                }
                else
                {
                    return TrustProvider.Initial;
                }
            }
            set
            {
                hif_Bank.Value = value.ToString();
            }
        }

        #endregion props

        #region interface

        public void SetAmountSum(int sumamount, int totalcount, DateTime basedate)
        {
            lab_RemainAmount.Text = sumamount.ToString("F0");
            lab_TotalCount.Text = totalcount.ToString("F0") + "筆";
            lab_BaseDate.Text = basedate.ToString("yyyy/MM/dd"); ;
        }

        public void SetAmountSum(int sumamount, int orderAmount, int verifyAmount, int refundAmount)
        {

        }

        #endregion interface

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }

            if (!Page.IsPostBack)
            {
                Presenter.OnViewInitialized();
                ddl_Type.Items.Clear();
                ListItem[] listItem = WebUtility.GetListItemFromEnum(TrustLogType.CouponCashTrust);
                lab_Com.Text = "康太數位整合股份有限公司";
                pan_Invalid.Visible = pan_Valid.Visible = false;

                // 多銀行角色需帶querystring以判斷欲觀看的銀行報表
                MemberRoles role = Request.QueryString["bank"] != null ? MemberRoles.TryParse(Request.QueryString["bank"].ToString(), out role) ? role : MemberRoles.RegisteredUser : MemberRoles.RegisteredUser;
                if (User.IsInRole(MemberRoles.TaiShin.ToString()) && (!role.Equals(MemberRoles.RegisteredUser) ? role.Equals(MemberRoles.TaiShin) : true))
                {
                    lab_Bank.Text = "台新銀行";
                    Provider = TrustProvider.TaiShin;
                    ddl_Type.Items.AddRange(listItem);
                    pan_Valid.Visible = true;
                }
                else if (User.IsInRole(MemberRoles.ysbank.ToString()) && (!role.Equals(MemberRoles.RegisteredUser) ? role.Equals(MemberRoles.ysbank) : true))
                {
                    lab_Bank.Text = "陽信商銀";
                    Provider = TrustProvider.Sunny;
                    ddl_Type.Items.AddRange(listItem);
                    pan_Valid.Visible = true;
                }
                else if (User.IsInRole(MemberRoles.Hwatai.ToString()) && (!role.Equals(MemberRoles.RegisteredUser) ? role.Equals(MemberRoles.Hwatai) : true))
                {
                    lab_Bank.Text = "華泰銀行";
                    Provider = TrustProvider.Hwatai;
                    ddl_Type.Items.Add(listItem.Where(x => x.Value.Equals(((int)TrustLogType.CouponCashTrust).ToString())).First());
                    lab_Com.Text = "康太數位整合股份有限公司";
                    pan_Valid.Visible = true;
                }
                else
                {
                    pan_Invalid.Visible = true;
                    ddl_Type.Enabled = false;
                    btn_Search.Enabled = false;
                }
            }
            Presenter.OnViewLoaded();
        }

        protected void TypeChanged(object sender, EventArgs e)
        {
            lab_UserAmountSum.Text = lab_QueryTime.Text = lab_Message.Text = string.Empty;
            pan_CouponTrust.Visible = pan_UserTrust.Visible = false;
            gv_CouponCashTrustLog.DataSource = gv_UserCashTrustLog.DataSource = null;
            gv_CouponCashTrustLog.DataBind();
            gv_UserCashTrustLog.DataBind();
            switch (LogType)
            {
                case TrustLogType.CouponCashTrust:
                    pan_CouponTrust.Visible = true;
                    break;

                case TrustLogType.UserCashTrust:
                    pan_UserTrust.Visible = true;
                    break;
            }
        }

        public void SetCouponTrustLog(CashTrustLogCollection cashlogs, TrustVerificationReport report)
        {
            if (cashlogs == null || cashlogs.Count == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "final", "alert('查無項目!')", true);
            }
            else
            {
                DateTime start_date = new DateTime(report.ReportIntervalEnd.Year - 1, report.ReportIntervalEnd.Month, 11);
                //排除滿一年以上的信託紀錄
                gv_CouponCashTrustLog.DataSource = cashlogs.Where(x => x.TrustedBankTime !=null && x.TrustedBankTime.Value >= start_date);
                gv_CouponCashTrustLog.DataBind();
            }
        }

        public void SetUserTrustLog(UserCashTrustLogCollection userlogs, TrustVerificationReport report)
        {
            if (userlogs == null || userlogs.Count == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "final", "alert('查無項目!')", true);
            }
            else
            {
                gv_UserCashTrustLog.DataSource = userlogs.OrderBy(x => x.Id); ;
                lab_UserAmountSum.Text = "帳戶總額:$" + userlogs.Sum(x => x.Amount);
                gv_UserCashTrustLog.DataBind();
            }
        }

        protected void gv_Bound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem != null)
            {
                if ((GridView)sender == gv_CouponCashTrustLog)
                {
                    CashTrustLog log = (CashTrustLog)e.Row.DataItem;
                    if (log != null)
                    {
                        ((Label)e.Row.FindControl("lab_TrustedTime")).Text = DateCheck(log.TrustedBankTime, log.BankStatus, 1);
                        ((Label)e.Row.FindControl("lab_VerifiededTime")).Text = DateCheck(log.VerifiedBankTime, log.BankStatus, 2);
                    }
                }
                else
                {
                    UserCashTrustLog log = (UserCashTrustLog)e.Row.DataItem;
                    if (log != null)
                    {
                        ((Label)e.Row.FindControl("lab_TrustedTime")).Text = DateCheck(log.TrustedBankTime, log.BankStatus, 1);
                        ((Label)e.Row.FindControl("lab_VerifiededTime")).Text = DateCheck(log.VerifiedBankTime, log.BankStatus, 2);
                    }
                }
            }
        }

        //type:1 trust type:2 verify
        protected string DateCheck(DateTime? date, int bankstatus, int type)
        {
            if (date == null)
            {
                if (bankstatus == (int)TrustBankStatus.Trusted && type == 1)
                {
                    return "本次信託，待銀行確認後更新信託日";
                }
                else if (bankstatus == ((int)TrustBankStatus.Trusted + (int)TrustBankStatus.Verified) && type == 2)
                {
                    return "本次核銷，待銀行確認後更新核銷日";
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return date.Value.ToString("yyyy/MM/dd");
            }
        }

        protected void SearchDetail(object sender, EventArgs e)
        {
            lab_QueryTime.Text = "查詢時間: " + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            gv_CouponCashTrustLog.DataSource = gv_UserCashTrustLog.DataSource = null;
            gv_CouponCashTrustLog.DataBind();
            gv_UserCashTrustLog.DataBind();
            lab_UserAmountSum.Text = lab_Message.Text = string.Empty;
            if (this.SearchLog != null)
            {
                if (LogType == TrustLogType.CouponCashTrust)
                {
                    if (string.IsNullOrEmpty(tbx_Coupon_1.Text) && string.IsNullOrEmpty(tbx_Coupon_2.Text))
                    {
                        if (string.IsNullOrEmpty(tbx_Coupon_Long.Text))
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "final", "alert('請輸入序號!!');", true);
                        }
                        else
                        {
                            this.SearchLog(this, new DataEventArgs<string>(tbx_Coupon_Long.Text));
                        }
                    }
                    else
                    {
                        this.SearchLog(this, new DataEventArgs<string>(tbx_Coupon_1.Text + "-" + tbx_Coupon_2.Text));
                    }

                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "final", "$(document).ready(function(){invisiblerow1('coupontable');});", true);
                }
                else
                {
                    int memberid;
                    if (int.TryParse(tbx_ScashAccount.Text, out memberid))
                    {
                        this.SearchLog(this, new DataEventArgs<string>(tbx_ScashAccount.Text));
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "final", "$(document).ready(function(){invisiblerow1('usertable');});", true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "final", "alert('請輸入數字!!');", true);
                    }
                }
            }
        }

        private void NonAuthorize()
        {
            pan_Invalid.Visible = true;
            ddl_Type.Enabled = false;
            btn_Search.Enabled = false;
        }
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="banks.aspx.cs" Inherits="LunchKingSite.Web.Verification.banks" Title="銀行核銷" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MC" runat="server">
    <script type="text/javascript">
        //        $(document).ready(function () {
        //            $('#naviBar').removeClass('MenuBarItemSubmenu').addClass('MenuBarHoverIndex');
        //        });
        function jump(obj) {
            if (obj == 0) {
                $('#<%=tbx_Coupon_Long.ClientID %>').val('');
                if ($('#<%=tbx_Coupon_1.ClientID %>').val().length == 4) {
                    $('#<%=tbx_Coupon_2.ClientID %>').focus();
                    $('#<%=tbx_Coupon_2.ClientID %>').val('');
                }
            }
            else if (obj == 1) {
                if ($('#<%=tbx_Coupon_2.ClientID %>').val().length == 4) {
                    $('#<%=btn_Search.ClientID %>').focus();
                }
            }
            else if (obj == 2) {
                $('#<%=tbx_Coupon_1.ClientID %>').val('');
                $('#<%=tbx_Coupon_2.ClientID %>').val('');
            }
}
function invisiblerow1(obj) {
    //            $('#' + obj + ' tr:gt(2)').each(function () { $(this).find('td:first').css('visibility', 'hidden'); });
    $('#' + obj + ' tr:gt(2)').each(function () { $(this).find('td:first').html(''); });
}
    </script>
    <style>
        .mainhead {
            background-color: #4F81BD;
            font-size: x-large;
            color: White;
            font-weight: bold;
            margin-left: 5px;
        }

        .mainrow {
            background-color: #D0D8E8;
            border: 1px solid white;
            margin-left: 5px;
        }

        .mainrow2 {
            background-color: #E9EDF4;
            border: 1px solid white;
            margin-left: 5px;
        }

        .head {
            background-color: #688E45;
            font-weight: bolder;
            color: White;
            border: 1px solid white;
            text-align: center;
        }

        .head2 {
            background-color: #F0AF13;
            color: #688E45;
            font-weight: bolder;
            border: thin solid gray;
            margin-left: 5px;
        }

        .row {
            background-color: #B3D5F0;
            border: thin solid gray;
            margin-left: 5px;
        }

        #navimain2 {
            display: none;
        }
    </style>
    <asp:Panel ID="pan_Invalid" runat="server">
        <div style="color: Red; height: 500px">
            你尚無權限使用!!請洽資訊人員!!
        </div>
    </asp:Panel>
    <asp:Panel ID="pan_Valid" runat="server">
        <asp:UpdatePanel ID="upan_All" runat="server">
            <ContentTemplate>
                <div>
                    <table style="margin-top: 20px; margin-bottom: 500px;  width:500px;">
                        <tr>
                            <td colspan="3" class="mainhead">信託/核銷查詢系統
                            </td>
                        </tr>
                        <tr>
                            <td class="mainrow">信託單位
                            </td>
                            <td colspan="2" class="mainrow">
                                <asp:Label ID="lab_Bank" runat="server"></asp:Label>
                                <asp:HiddenField ID="hif_Bank" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="mainrow2">公司名稱
                            </td>
                            <td colspan="2" class="mainrow2">
                                <asp:Label ID="lab_Com" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="mainrow">基準日
                            </td>
                            <td colspan="2" class="mainrow">
                                <asp:Label ID="lab_BaseDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="mainrow2">信託總餘額
                            </td>
                            <td colspan="2" class="mainrow2">$<asp:Label ID="lab_RemainAmount" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="mainrow">信託總筆數
                            </td>
                            <td colspan="2" class="mainrow">
                                <asp:Label ID="lab_TotalCount" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="head" style="width: 20%">
                                <asp:DropDownList ID="ddl_Type" runat="server" OnSelectedIndexChanged="TypeChanged"
                                    AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="head" style="width: 60%">
                                <asp:Panel ID="pan_CouponTrust" runat="server">
                                    <asp:TextBox ID="tbx_Coupon_1" runat="server" onkeyup="jump(0);" MaxLength="4" Width="55"></asp:TextBox>
                                    -<asp:TextBox ID="tbx_Coupon_2" runat="server" onkeyup="jump(1);" MaxLength="4" Width="55"></asp:TextBox>
                                    <br />
                                   or &nbsp;
                                    <asp:TextBox ID="tbx_Coupon_Long" runat="server" Width="90" onkeyup="jump(2);"></asp:TextBox>
                                </asp:Panel>
                                <asp:Panel ID="pan_UserTrust" runat="server" Visible="false">
                                    <asp:TextBox ID="tbx_ScashAccount" runat="server" MaxLength="10"></asp:TextBox>
                                </asp:Panel>
                            </td>
                            <td class="head" style="width: 20%">
                                <asp:Button ID="btn_Search" runat="server" Text="查詢" OnClick="SearchDetail" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align:center">
                                <asp:Label ID="lab_Message" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:GridView ID="gv_CouponCashTrustLog" runat="server" OnRowDataBound="gv_Bound"
                                    AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <table id="coupontable" style="width: 500px">
                                                    <tr>
                                                        <td class="head2">憑證編號
                                                        </td>
                                                        <td class="head2">金額
                                                        </td>
                                                        <td class="head2">信託日
                                                        </td>
                                                        <td class="head2">核銷日
                                                        </td>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="row">
                                                        <%# ((CashTrustLog)Container.DataItem).TrustSequenceNumber %>
                                                    </td>
                                                    <td class="row">$<%# ((((CashTrustLog)Container.DataItem).CreditCard) + (((CashTrustLog)Container.DataItem).Scash) + (((CashTrustLog)Container.DataItem).Atm)+(((CashTrustLog)Container.DataItem).Lcash)).ToString("F0")%>
                                                    </td>
                                                    <td class="row">
                                                        <asp:Label ID="lab_TrustedTime" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="row">
                                                        <asp:Label ID="lab_VerifiededTime" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:GridView ID="gv_UserCashTrustLog" runat="server" AutoGenerateColumns="false"
                                    OnRowDataBound="gv_Bound">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <table id="usertable">
                                                    <tr>
                                                        <td class="head2">購物金帳戶編號
                                                        </td>
                                                        <td class="head2">金額
                                                        </td>
                                                        <td class="head2">信託日
                                                        </td>
                                                        <td class="head2">核銷日
                                                        </td>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="row">
                                                        <%# ((UserCashTrustLog)Container.DataItem).UserId%>
                                                    </td>
                                                    <td class="row">$<%# ((UserCashTrustLog)Container.DataItem).Amount%>
                                                    </td>
                                                    <td class="row">
                                                        <asp:Label ID="lab_TrustedTime" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="row">
                                                        <asp:Label ID="lab_VerifiededTime" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <asp:Label ID="lab_UserAmountSum" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Label ID="lab_QueryTime" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

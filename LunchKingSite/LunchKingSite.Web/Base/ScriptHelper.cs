﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Optimization;
using System.Web.UI;

namespace LunchKingSite.Core
{
    public static class ScriptHelper
    {
        public static IHtmlString Render(params string[] paths)
        {
            for (int i = 0; i < paths.Length; i++)
            {
                if (paths[i].Contains(@"~/bundles"))
                {
                    paths[i] = paths[i].TrimEnd(".js").Replace(".", "_");
                }
                else //if(paths[i].Contains(@"~"))
                {
                    //paths[i] = VirtualPathUtility.ToAppRelative(paths[i]);
                    paths[i] = (HttpContext.Current.Handler as Page).ResolveUrl(paths[i]);
                }
            }

            return Scripts.Render(paths);
        }
    }
}

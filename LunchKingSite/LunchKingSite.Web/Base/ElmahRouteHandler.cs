﻿using LunchKingSite.Core;
using LunchKingSite.WebLib.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace LunchKingSite.Web.Base
{
    public class ElmahRouteHandler : IRouteHandler
    {
        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return new Elmah.ErrorLogPageFactory().GetHandler(HttpContext.Current, null, null, null);
        }
    }

    public class IpConstraint : IRouteConstraint
    {
        public string[] ip_list;
        public IpConstraint(string[] ip_list)
        {
            this.ip_list = ip_list;
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            if (httpContext.Request.Url.AbsolutePath.Contains("oops.axd", StringComparison.InvariantCultureIgnoreCase))
            {
                string client_ip = Helper.GetClientIP();
                if (!ip_list.Contains(client_ip))
                {
                    throw new UnauthorizedAccessException(client_ip + "/ohoh無權限進入");
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }

        }
    }
}
using System.Resources;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Core.UI
{
    public abstract class LocalizedBaseUserControl : BaseUserControl, ILocalizedView
    {
        public ResourceManager Localization
        {
            get { return I18N.Phrase.ResourceManager; }
        }

        public ResourceManager Message
        {
            get { return I18N.Message.ResourceManager; }
        }
    }
}

using System;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.UI;

namespace LunchKingSite.Web.Base
{
    public class ViewManager
    {
        public static UserControl InitPageAndControl(string controlPath, out Page pageHolder)
        {
            /*
            pageHolder = new Page();
            HtmlForm f = new HtmlForm();
            HtmlGenericControl div = new HtmlGenericControl("dynamicusercontrol");
            UserControl ctrl = (UserControl)pageHolder.LoadControl(controlPath);
            div.Controls.Add(ctrl);
            f.Controls.Add(div);
            pageHolder.Controls.Add(f);
            */
            pageHolder = new Page();
            UserControl ctrl = (UserControl)pageHolder.LoadControl(controlPath);
            pageHolder.Controls.Add(ctrl);
            return ctrl;
        }

        public static string RenderView(Page pageHolder)
        {
            using (StringWriter output = new StringWriter())
            {
                HttpContext.Current.Server.Execute(pageHolder, output, false);
                /*
                System.Xml.XmlDocument xDoc = new System.Xml.XmlDocument();
                xDoc.LoadXml(output.ToString());
                return xDoc.SelectSingleNode("//dynamicusercontrol").InnerXml;
                */
                return output.ToString();
            }
        }

        public static string RenderView(string path)
        {
            return RenderView(path, null);
        }

        public static string RenderView(string path, object data)
        {
            Page pageHolder = new Page();
            UserControl viewControl = (UserControl)pageHolder.LoadControl(path);

            if (data != null)
            {
                Type viewControlType = viewControl.GetType();
                PropertyInfo field = viewControlType.GetProperty("Data");

                if (field != null)
                {
                    field.SetValue(viewControlType, data, null);
                }
                else
                {
                    throw new Exception("View file: " + path + " does not have a public Data property");
                }
            }

            pageHolder.Controls.Add(viewControl);

            StringWriter output = new StringWriter();
            HttpContext.Current.Server.Execute(pageHolder, output, false);

            return output.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;


namespace LunchKingSite.Web
{
    public partial class order_return : System.Web.UI.Page
    {
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected void Page_Load(object sender, EventArgs e)
        {
          
            DateTime ExpireDate = DateTime.Parse("2017/03/27");
            if (DateTime.Now >= ExpireDate)
            {
                section_expired.Visible = true;
                section_normal.Visible = false;
                section_repeat.Visible = false;
            }
            else
            {
                string order_id = Request.QueryString["oid"];

                if (!string.IsNullOrEmpty(order_id))
                {
                    //先取出einvoice的資料 & allowance_status=1 的資料
                    var einvoice = mp.GetEinvoiceMainByAllowanceStatusAndOrderId(order_id, 2);

                    if (einvoice.Count() > 0)
                    {
                        //如果有資料進行Update
                        foreach (var item in einvoice)
                        {
                            item.AllowanceStatus = (int)AllowanceStatus.ElecAllowance;
                            mp.EinvoiceMainSet(item);
                        }
                        section_normal.Visible = true;
                        section_expired.Visible = false;
                        section_repeat.Visible = false;
                    }
                    else
                    {
                        //如果找不到資料時，取是否已經電子確認過了
                        var einvoiceAlready = mp.GetEinvoiceMainByAllowanceStatusAndOrderId(order_id, 1);
                        if (einvoiceAlready.Count() > 0)
                        {
                            section_repeat.Visible = true;
                            section_normal.Visible = false;
                            section_expired.Visible = false;
                        }
                        else
                        {
                            section_expired.Visible = true;
                            section_repeat.Visible = false;
                            section_normal.Visible = false;                          
                        }
                    }

                }
                else
                {
                    section_expired.Visible = true;
                    section_repeat.Visible = false;
                    section_normal.Visible = false;
                }
            }
        }
    }
}
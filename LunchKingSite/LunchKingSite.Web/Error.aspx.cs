﻿using System;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web
{
    public partial class Error : LocalizedBasePage, IErrorView
    {
        public ISysConfProvider SystemConfig
        {
            get { return ProviderFactory.Instance().GetConfig(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                _presenter.OnViewLoaded();
                if (!IsPostBack) 
                {
                    _presenter.OnViewInitialized();
                    if (RequestMode == "404") 
                    {
                        this.Response.StatusCode = 404;
                        ErrorCode = "404";
                    } 
                    else if (RequestMode == "403" || RequestMode == "403.6") //403.2 給 Defense Module 會員多次嘗次登入，顯示暫時封鎖的訊息
                    {
                        this.Response.StatusCode = 403;
                        ErrorCode = "403";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("{0}-{1}", ex.Message, ex.StackTrace);
            }
        }

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(Error));

        #region props
        private ErrorPresenter _presenter;
        public ErrorPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string RequestMode
        {
            get
            {
                string mode = (string)Request.RequestContext.RouteData.Values["statusCode"] ??  Request.QueryString["action"];
                if (!string.IsNullOrEmpty(mode))
                    mode = mode.Trim();
                return mode;
            }
        }

        public string ErrorPath
        {
            get { return Request.QueryString["aspxerrorpath"]; }
        }

        public string ErrorTitle
        {
            get { return litTitle.Text; }
            set { litTitle.Text = value; }
        }

        public string ErrorMessage
        {
            get { return lMsg.Text; }
            set { lMsg.Text = value; }
        }

        private string _errorCode;
        public string ErrorCode
        {
            get
            {
                if (string.IsNullOrEmpty(_errorCode)) 
                {
                    _errorCode = Request.Cookies["ErrorId"] != null ? Request.Cookies["ErrorId"].Value : "";
                }
                return _errorCode;
            }
            set 
            {
                _errorCode = value;
            }
        }
        #endregion
    }
}

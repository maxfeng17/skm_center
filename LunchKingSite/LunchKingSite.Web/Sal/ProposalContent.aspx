﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="ProposalContent.aspx.cs" Inherits="LunchKingSite.Web.Sal.ProposalContent"  %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Model" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/fancybox/jquery.fancybox.css") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Tools/js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="../Tools/js/jquery.nav.js"></script>
    <script type="text/javascript" src="../Tools/js/fancybox/jquery.fancybox.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.maxlength-min.js"></script>


    <style type="text/css">
        .edit{
            display:none;
        }
        .CPC {
            display:none;
        }
        .travel {
            display:none;
        }
         /*側邊anchor*/
        .nav-anchor {
            position: fixed;
            top: 125px;
            margin-left: 970px;
            z-index: 99;
        }

        .nav-anchor li {
            margin-bottom: 2px;
            text-align: center;
        }

        .nav-anchor a {
            width: 100px;
            height: 30px;
            line-height: 30px;
            background: #ededed;
            color: #666;
            display: block;
            font-size: 11px;
            text-decoration: none;
            text-transform: uppercase;
        }

        .nav-anchor a:hover {
            background: #dedede;
        }

        .nav-anchor .current a {
            background: #3F3F3F;
            color: #e5e5e5;
        }

        .nav-anchor li:first-child a {
            -webkit-border-radius: 3px 3px 0 0;
            -moz-border-radius: 3px 3px 0 0;
            border-radius: 3px 3px 0 0;
        }

        .nav-anchor li:last-child a {
            -webkit-border-radius: 0 0 3px 3px;
            -moz-border-radius: 0 0 3px 3px;
            border-radius: 0 0 3px 3px;
        }

        /*目前位置*/
        .breadcrumb {
	        /*centering*/
	        display: inline-block;
	        box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.35);
	        overflow: hidden;
	        border-radius: 5px;
	        /*Lets add the numbers for each link using CSS counters. flag is the name of the counter. to be defined using counter-reset in the parent element of the links*/
	        counter-reset: flag; 
        }

        .breadcrumb a {
	        text-decoration: none;
	        outline: none;
	        display: block;
	        float: left;
	        font-size: 12px;
	        line-height: 36px;
	        color: white;
	        /*need more margin on the left of links to accomodate the numbers*/
	        padding: 0 10px 0 60px;
	        background: #666;
	        background: linear-gradient(#666, #333);
	        position: relative;
        }


        /*adding the arrows for the breadcrumbs using rotated pseudo elements*/
        .breadcrumb a:after {
	        content: '';
	        position: absolute;
	        top: 0; 
	        right: -18px; /*half of square's length*/
	        /*same dimension as the line-height of .breadcrumb a */
	        width: 36px; 
	        height: 36px;
	        /*as you see the rotated square takes a larger height. which makes it tough to position it properly. So we are going to scale it down so that the diagonals become equal to the line-height of the link. We scale it to 70.7% because if square's: 
	        length = 1; diagonal = (1^2 + 1^2)^0.5 = 1.414 (pythagoras theorem)
	        if diagonal required = 1; length = 1/1.414 = 0.707*/
	        transform: scale(0.707) rotate(45deg);
	        /*we need to prevent the arrows from getting buried under the next link*/
	        z-index: 1;
	        /*background same as links but the gradient will be rotated to compensate with the transform applied*/
	        background: #666;
	        background: linear-gradient(135deg, #666, #333);
	        /*stylish arrow design using box shadow*/
	        box-shadow: 
		        2px -2px 0 2px rgba(0, 0, 0, 0.4), 
		        3px -3px 0 2px rgba(255, 255, 255, 0.1);
	        /*
		        5px - for rounded arrows and 
		        50px - to prevent hover glitches on the border created using shadows*/
	        border-radius: 0 5px 0 50px;
        }
       
        .flat a, .flat a:after {
	        background: white;
	        color: black;
	        transition: all 0.5s;
        }

        .flat a.active, 
        .flat a.active:after{
            color:#fff;
	        background: #AF0000;
        }

        div.pager {
            text-align: center;
            margin: 1em 0;
        }

        div.pager span {
            display: inline-block;
            width: 1.8em;
            height: 1.8em;
            line-height: 1.8;
            text-align: center;
            cursor: pointer;
            background: #ededed;
            color: #000;
            margin-right: 0.5em;
            border:1px;

        }

        div.pager span.active {
            background: #c00;
            color:#fff;
        }

        .inputStyle {
            display: inline-block;
            height: 20px;
            padding: 3px 5px;
            margin-bottom: 10px;
            font-size: 12px;
            line-height: 20px;
            color: #555555;
            vertical-align: middle;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }
        .inputStyleP {
            display: inline-block;
            height: 20px;
            padding: 3px 5px;
            margin-bottom: 10px;
            font-size: 12px;
            line-height: 20px;
            color: #555555;
            vertical-align: middle;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }
        .selectReadonly{
            pointer-events: none; 
            cursor: default;
        }

        /*上檔頻道顯示*/
        .grui-form .form-unit .data-input [class*=pCommercial] {
            display: block;
        }
        .grui-form .form-unit .data-input p.pCategory {
            display: block;
            background: #f2f2f0;
        }
        /*營業據點highlight*/
        .showtr tr:hover td
        {
            background-color: #EDBD3E;
            cursor:move;
        }
        #dragandrophandler {
            border: 2px dotted #0B85A1;
            width: 100%;
            text-align: center;
            color: #92AAB0;
            vertical-align: middle;
            padding: 10px 10px 10 10px;
            margin-bottom: 10px;
            font-size: 200%;
        }
        /*約拍小幫手-儲存*/
        #btn-menu {
            display: block;
            top: 666px;
            margin-left: -90px;
            position: fixed;
        }
        .btn-circle {
            display: block;
            border-radius: 5px;
            width: 80px;
            height: 40px;
            text-decoration: none;
            color: #ffffff;
            font-family: Arial;
            font-size: 17px;
            padding: 2px 2px;
            text-decoration: none;
            text-shadow: 1px 1px 0px #810e05;
        }

        /*商家帳號提示*/
        .tooltip {
            position: relative;
            display: inline-block;
        }

        .tooltip .tooltiptext {
            visibility: hidden;
            background-color: white;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            /* Position the tooltip */
            position: absolute;
            z-index: 1;
            border-style:solid;
            border-color:gray;
            border-width: 1px;
            width:200px;
        }

        .tooltip:hover .tooltiptext {
            visibility: visible;
        }

        .divDisabled
         { 
            position:absolute;
            left:0; 
            top:0; 
            background: #F9F9F9; /* div的顏色 */
            width:100%; 
            height:100%;
            opacity: 0.5; /* 透明度 */
            filter:Alpha(Opacity=50, Style=0) /* 透明度 */
         } 
    </style>
    <script type="text/javascript">
        var Deals = [];
        var onlySaveSate; //約拍小幫手-儲存狀態
        $(document).ready(function () {
            $("#divdealstar textarea").css("resize", "vertical");
            $("#divDealCharacterToHouse textarea").css("resize", "vertical");
            $("#divDealCharacterToShop textarea").css("resize", "vertical");
            $("#txtBrandName").attr("placeholder", "商家名稱 - 方案名稱");
            CheckSpecialFlag($('#chkPhotographers'), 'Photographers');
            CheckSpecialFlag($('#chkInvoiceFounder'), 'InvoiceFounder');
            CheckSpecialFlag($('#chkFTPPhoto'), 'FTPPhoto');
            //CheckPhotographFlag($('#chkPhotographers'));
            
            CheckSpecialFlag2($('#chkBeauty'), 'Beauty');
            CheckSpecialFlag2($('#chkExpiringProduct'), 'ExpiringProduct');
            CheckSpecialFlag2($('#chkInspectRule'), 'InspectRule');
            CheckSpecialFlag2($('#chkStoreStory'), 'StoreStory');
            CheckSpecialFlag2($('#chkRecommendFood'), 'RecommendFood');
            CheckSpecialFlag2($('#chkEnvironmentIntroduction'), 'EnvironmentIntroduction');
            CheckSpecialFlag2($('#chkAroundScenery'), 'AroundScenery');
            CheckSpecialFlag2($('#chkSeasonActivity'), 'SeasonActivity');
            CheckSpecialFlag2($("#chkMediaReport"), 'MediaReport')
            CheckSpecialFlag2($('#chkTravel'), 'Travel');
            if ($("#hidSellerFlag").val() != "0"){
                Enabletxt($('#chkDealStar'), 'DealStar');
            }
            
            Enabletxt($("#chkDealCharacterToShop1"), 'DealCharacterToShop1');
            Enabletxt($("#chkDealCharacterToShop2"), 'DealCharacterToShop2');
            Enabletxt($("#chkDealCharacterToShop3"), 'DealCharacterToShop3');
            Enabletxt($("#chkDealCharacterToShop4"), 'DealCharacterToShop4');
            if ($("#hidSellerFlag").val() != "0") {
                Enabletxt($("#chkDealCharacterToHouse"), 'DealCharacterToHouse')
            }
            
            if(!$("#chkMediaReport").is(':disabled')) {
                Enabletxt($("#chkMediaReportPic"), 'MediaReport')
            }
            else
            {
                $("[id *= imgMediaReport]").attr("onclick", '');
            }


            BindDealSubType($('#ddlDealType'));

            deliveryTypeChange($('#ddlDeliveryType'));
            ShipTypeChange($('#ddlShipType'));

            

            $("#chkGroupCoupon").click(function(){
                CPCShowOrHide();
            });

            $("#chkMohist").click(function () {
                if ($(this).attr("checked") == "checked") {
                    $("#chkGroupCoupon").attr('checked', false);
                    $("#chkGroupCoupon").attr('disabled', 'disabled');
                    $("#ddlGroupCouponType").val("0").change();
                    $(".GroupCoupon.CPC").hide();
                } else {
                    $("#chkGroupCoupon").removeAttr("disabled");
                    
                }
            });

            //隱藏活動使用有效時間範圍&小日曆
            if($('#ddlStartUseUnit').val() != '<%= (int)ProposalStartUseUnitType.BeforeDay %>')
            {
                $(".range1").show();
                $(".range2").hide();
            }
            else
            {
                $(".range1").hide();
                $(".range2").show();
                setupdate('txtStartUse2', 'txtStartUse');
            }
            //優惠活動拖曳排序
            if ($("#txtBrandName").attr("disabled") != "disabled") {
                $("#proposalContentTable tbody").sortable({
                    stop: function (event, ui) {
                        sortTableIndx("#proposalContentTable")
                    }
                });
            }

            $("#ddlDealType").change(function () {
                if ($(this).val() == "<%=((int)ProposalDealType.Travel)%>") {
                    alert("不得建立旅遊類別，請從商家頁面建立。");
                    $("#ddlDealType").val("1");
                }
            });


            if ($("#ddlDealType").val() == "<%=((int)ProposalDealType.Travel)%>") {
                $("#ddlDealType").addClass("selectReadonly");
                $(".travel").show();
            } else {
                $(".travel").hide();
            }

            if ($("#ddlDeliveryType").val() == "<%=((int)DeliveryType.ToHouse)%>" && $("#ddlDealType").val() == "<%=((int)ProposalDealType.Piinlife)%>") {
                $("#ddlDealType").addClass("selectReadonly");
            } else {
            }

            // 設定顯示的tab
            var tab = '<%= Tab %>';
            if (tab != '') {
                SelectTab($('.mc-navbar').find('[tab=' + tab + ']'));
            }

            

            //即時顯示約拍小幫手與結果回填
            if ($("#chkPhotographers").is(":checked")) { //初始化
                $("#btnPhotoHelper").show();
                ProposalSpecialTextGet();
            } else {
                $("#btnPhotoHelper").hide();
            }

            $("#chkPhotographers").click(function () {
                if ($("#chkPhotographers").is(":checked")) {
                    $("#btnPhotoHelper").show();
                } else {
                    $("#btnPhotoHelper").hide();
                    $("#divPhotoResult").hide();
                }
            });

            /*
            * 通知攝影師
            */
            $("#btnPhotographerAppointMail").click(function () {
                if (!confirm("確定要發信通知攝影師?")) {
                    return false;
                }
                var PhotographerAppointer = $("#ddlPhotographerAppointer").val();

                $.ajax({
                    type: "POST",
                    url: "ProposalContent.aspx/MailToPhotographer",
                    data: "{'UserName': '<%= UserName %>','PhotographerName':'" + PhotographerAppointer + "','ProposalId':'<%= ProposalId %>' }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var c = response.d;
                        if (c) {
                            $("#hidPhotographerAppointCheck").attr("checked", "checked");
                            alert("發送成功!");
                        } else {
                            alert("發送失敗!");
                        }
                        
                    }, error: function (xhr) {
                        console.log(xhr.responseText);
                    }
                });
            });

            IntialControls();
            appendSlide();

            $('#subMenu').onePageNav();

            $('#txtSellerCheck').attr('placeHolder', '填寫商家編號');
            $('#txtdelastar').attr('placeHolder', '限1000字內');
            $('#txtdealcharacter').attr('placeHolder', '連結');
            $('#txtEmp').attr('placeHolder', '輸入e-mail');

            $(".Restrictions").fancybox({
                maxWidth: 850,
                maxHeight: 600,
                fitToView: false,
                width: '70%',
                height: '70%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                helpers: {
                    overlay: { closeClick: false }
                },
                afterClose: function () {
                    $.ajax({
                        type: "POST",
                        url: "ProposalContent.aspx/BindRestrictions",
                        data: "{'pid': '<%=ProposalId%>'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var c = response.d;
                            if (c != undefined) {
                                var _json = JSON.parse(c);
                                $("#liRestrictions").html(_json.Restrictions.replace(/\r\n/g, "<br />"));
                            }
                        }, error: function (response) {
                            alert(response.message);
                            console.log(response.message);
                        }
                    });
                }
            });

 

            /*
            * 分頁
            */
            $("#tblProposalChangeLog").each(function () {
                var currentPage = 0;
                var numPerPage = 10;
                var $table = $(this);
                $table.bind('repaginate', function () {
                    $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
                });
                $table.trigger('repaginate');
                var numRows = $table.find('tbody tr').length;
                var numPages = Math.ceil(numRows / numPerPage);
                var $pager = $('<div class="pager"></div>');
                for (var page = 0; page < numPages; page++) {
                    $('<span class="page-number"></span>').text(page + 1).bind('click', {
                        newPage: page
                    }, function (event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');
                    }).appendTo($pager).addClass('clickable');
                }
                $pager.insertAfter($table).find('span.page-number:first').addClass('active');
            });

            //初始化狀態
            $('#btnPhotoHelper').click(function () {
                onlySaveSate = false;

                //約拍小幫手儲存功能，有攝影狀態時就隱藏
                $('input[id*="btnFloatSave"]').show();

                if ($("#hidPhotographerAppoint").is(":checked") || $("#hidPhotographerAppointCheck").is(":checked") || 
                    $("#hidPhotographerAppointClose").is(":checked") || $("#hidPhotographerAppointCancel").is(":checked")) { //約拍中
                    $('input[id*="btnFloatSave"]').hide();
                }
            });

            $("#btnPhotoHelper").fancybox({
                helpers: {
                    overlay: { closeClick: false }                  
                },
                afterClose: function () {
                    //fancybox關閉後...
                    if ($("#btnPhotographerAppoint").is(':visible') == false) {
                        ProposalSpecialTextGet();
                        return;
                    }                    
                }
            });

            /*
            * 抽單
            */
            $("#btnProposalDraw").fancybox({
                helpers: {
                    overlay: { closeClick: false }
                },
                afterClose: function () {
                    //fancybox關閉後...
                  
                }
            });

            /*
            * 提前頁確
            */
            $("#chkEarlierPageCheck").click(function () {
                if ($(this).is(":checked")) {
                    $("#txtEarlierPageCheckDay").removeAttr("disabled");
                } else {
                    $("#txtEarlierPageCheckDay").val("");
                    $("#txtEarlierPageCheckDay").attr("disabled", "disabled");
                }
            });


            //同意合約未回先上傳
            $('[id*=btnFlag]').each(function()
            {
                $('.if-error').hide();
                $('.error').removeClass('error');

                var pic = $(this).attr('src').indexOf('x.png');
                var flag   = $(this).parent().html().indexOf('<%=Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalListingFlag.PaperContractNotCheck)%>');
                if (flag != -1 && pic != -1) {
                    $(this).fancybox({
                        width: '300px',
                        height: '500px',
                        href: '#divPaperContractNotCheckReason',
                        helpers: {
                            overlay: { closeClick: false }
                        },
                        afterClose: function () {

                            return;
                        }
                    });
                }
                

            });

            //業務轉件
            $('[id*=ReferralEmail]').autocomplete({
                source: function (request, response) {
                    $.ajax(
                    {
                        type: "POST",
                        url: "SellerContent.aspx/GetSalesmanNameArray",
                        data: JSON.stringify({
                            userName: request.term
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.Label,
                                    value: item.Value
                                }
                            }))
                        }, error: function (res) {
                            console.log(res);
                        }
                    });
                },
            });



            //約拍中
            if ($("#hidPhotographerAppoint").is(":checked")) {

                //需有攝影權限的人才看的到
                if ($("#hidPhotographerPri").val() == "1") {
                    $("#photographerAppoint").show();
                }

                //不同類型顯示不同攝影表格
                if ($("#ddlDeliveryType").val() == "<%=(int)DeliveryType.ToShop%>") {
                    //憑證
                    $("#photoToShopRes").show();
                    $("#photoToHouseRes").hide();

                } else {

                    if ($("#ddlDealType").val() == "<%=((int)ProposalDealType.Travel)%>") {
                        //旅遊宅配
                        $("#photoToShopRes").show();
                        $("#photoToHouseRes").hide();
                    } else {
                        //一般宅配
                        $("#photoToShopRes").hide();
                        $("#photoToHouseRes").show();
                    }

                }
            }


            //顯示不同檔次特色
            if ($("#ddlDeliveryType").val() == "<%=(int)DeliveryType.ToHouse%>" || $("#ddlDealType").val() == "<%=(int)ProposalDealType.Travel%>") {
                //宅配+旅遊
                $("#divDealCharacterToShop").hide();
                $("#divDealCharacterToHouse").show();
            }
            else
            {
                //非旅遊憑證
                $("#divDealCharacterToShop").show();
                $("#divDealCharacterToHouse").hide();
            }

            //宅配顯示項目
            if ($("#ddlDeliveryType").val() == "<%=(int)DeliveryType.ToHouse%>" && $("#ddlDealType").val() != "<%=(int)ProposalDealType.Travel%>")
                $(".ToHouse").show();
            else
                $(".ToHouse").hide();

            //成套票券檔次不可使用折價券
            $('#chkGroupCoupon').click(function () {
                $('#chkNotAllowedDiscount').attr('checked', $('#chkGroupCoupon').is(':checked'));
                $('#chkNotAllowedDiscount').attr('disabled', $('#chkGroupCoupon').is(':checked'));
                $('#chkLowGrossMarginAllowedDiscount').attr('disabled', $('#chkGroupCoupon').is(':checked'));
            });

            //遊戲檔次不可使用折價券
            $('#chkGame').click(function () {
                $('#chkNotAllowedDiscount').attr('checked', $('#chkGame').is(':checked'));
                $('#chkNotAllowedDiscount').attr('disabled', $('#chkGame').is(':checked'));
                $('#chkLowGrossMarginAllowedDiscount').attr('disabled', $('#chkGame').is(':checked'));
            });


            <%--當不可用折價券被打勾，disabled低毛利率可用折價券--%>
            SwitchSpecialRequirementOnChange();

            //指派創編
            $('#btnAssign').click(function () {
                var AssignFlagList = [];
                $('.AssignFlag').each(function () {
                    var check = $(this);
                    if (check.is(':checked')) {
                        AssignFlagList.push(check.val());
                    }
                });

                $('#<%=hidAssignFlag.ClientID%>').val(AssignFlagList);
            });

            //創編指派人員
            $('#txtEmp').autocomplete({
                source: function (request, response) {
                    $.ajax(
                    {
                        type: "POST",
                        url: "ProductionBusinessList.aspx/GetProductionNameArray",
                        data: JSON.stringify({
                            userName: request.term
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.Label,
                                    value: item.Value
                                }
                            }))
                        }, error: function (res) {
                            console.log(res);
                        }
                    });
                },
                select: function () {
                    //選擇項目時
                    $(this).trigger('change');
                }
            });

            //檢查是否為創意部人員
            $('#txtEmp').blur(function () {
                $.ajax(
                    {
                        type: "POST",
                        url: "ProductionBusinessList.aspx/IsProduction",
                        data: "{ 'userName': '" + $(this).val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d == false) {
                                $('#NoProductionEmp').show();
                                $(".AssignFlag").prop('disabled', 'disabled');
                            }
                            else {
                                $('#NoProductionEmp').hide();
                                $(".AssignFlag").prop('disabled', '');
                            }

                        }, error: function (res) {
                            console.log(res);
                        }
                    });
            });

            
            //熟客17來、優惠券、即買即用、最後一天、新光三越 預設disabled (前二頻道for APP，新光for新光APP)
            $('[id*=hidChannel]').each(function () {
                if($(this).val() == 2160 || $(this).val() == 2161 || $(this).val() == 100001 || $(this).val() == 100002 || $(this).val() == 185)
                    $(this).next().attr('disabled', 'disabled');
            });


            //顯示上檔頻道
            //頻道
            $('.pChannel').has('[id*=chkChannel]:checked').each(function () {
                $(this).next('.pArea').show();

                //後台不顯示
                var categoryList = $(this).next('.pArea').find("input[id*='chkCategory']");
                $.each(categoryList, function(cidx, cobj){
                    var isShow = $(cobj).parent().find("input[id*='hidIsShowBackEnd']").val();
                    if(isShow != undefined){
                        if(isShow.toLowerCase() == "false"){
                            $(cobj).attr("disabled", "disabled");
                            //需同時把子項目也disabled
                            $(cobj).parent('.pB').next('.pCategory').find("input[id*='chkSubDealCategory']").attr("disabled","disabled");
                        }
                    }
                });
            });

            //商圈
            $('.pA').has('[id*=chkArea]:checked').each(function () {
                var id = $(this).find('[id*=hidArea]').val();
                $(this).parents('ul').find('.pCommercial' + id).show();
            });

            //商圈(旅遊)-地區
            $('[id*=chkCommercial]:checked').each(function () {
                var id = $(this).prev().val();
                $(this).parents('tr').next().find('.pSpecialRegion' + id).show();
            });

            //分類
            $('.pB').has('[id*=chkCategory]:checked').each(function () {
                var controls = $(this).next('.pCategory').find('input[type=checkbox]');
                if (controls.length > 0) {
                    $(this).next('.pCategory').show();

                    //後台不顯示
                    var subCategoryList = $(this).next('.pCategory').find("input[id*='chkSubDealCategory']");
                    $.each(subCategoryList, function(cidx, cobj){
                        var isShow = $(cobj).parent().find("input[id*='hidSubIsShowBackEnd']").val();
                        if(isShow != undefined){
                            if(isShow.toLowerCase() == "false"){
                                $(cobj).attr("disabled", "disabled");
                            }
                        }
                    });
                }

            });

            //檔次名稱帶入關鍵字
            $('#txtBrandName').change(function(){
                $('#txtKeyword').val($(this).val());

            });

            //顯示店家單據開立方式-其他
            $('.record').change(function(){
                ShowAccountingMessage();
            });

            $('.record').change(function(){
                IsNoTax();
            });

            IsNoTax();

            

            //店家檢核全選
            CheckAll2('Check');
            CheckAll2('VerifyShop');
            CheckAll2('Verify');
            CheckAll2('Accouting');
            CheckAll2('ViewBalanceSheet');

            //營業據點勾選
            $('[id$=Check]').change(function () {
                if ($(this).is(':checked'))
                {
                    var id = $(this).attr('id').replace('rptPponStore', 'rptVbs').replace('chkCheck', 'chkVerifyShop');
                    if (!$('#' + id).is(':disabled'))
                    {
                        if(($("#ddlDeliveryType").val() == "<%=(int)DeliveryType.ToShop%>" && $('#chkNoRestrictedStore').is(':checked')) ||
                          ($("#ddlDeliveryType").val() == "<%=(int)DeliveryType.ToHouse%>"))
                        {
                            //not doing
                        }
                        else
                        {
                            $('#' + id).attr('checked', 'checked');
                        }
                        
                    }
                    id = $(this).attr('id').replace('rptPponStore', 'rptVbs').replace('chkCheck', 'chkVerify');
                    if (!$('#' + id).is(':disabled'))
                    {
                        $('#' + id).attr('checked', 'checked');
                    }
                        
                }
                CheckAll2('Check');
            })

            //銷售店舖勾選
            $('[id$=VerifyShop]').change(function () {
                if (($("#ddlDeliveryType").val() == "<%=(int)DeliveryType.ToShop%>" && $('#chkNoRestrictedStore').is(':checked')) ||
                    ($("#ddlDeliveryType").val() == "<%=(int)DeliveryType.ToHouse%>")) {
                    alert('宅配檔次/通用券檔次無須設定[銷售店鋪]賣家');
                    $('[id$=VerifyShop]').removeAttr("checked");
                }
                else
                {
                    CheckAll2('VerifyShop');
                }
            })

            //憑證核實勾選
            $('[id$=Verify]').change(function () {
                CheckAll2('Verify');
            })

            //匯款對象勾選
            $('[id$=Accouting]').change(function () {
                CheckAll2('Accouting');
            })

            //檢視對帳單
            $('[id$=ViewBalanceSheet]').change(function () {
                CheckAll2('ViewBalanceSheet');
            })



            //對帳權限勾選
            $(".hideBalanceSheet").click(function () {
                var src = $(this).attr("src");
                var index = $(".hideBalanceSheet").index(this);
                if ($('.checkViewBalanceSheet > :checkbox:eq(' + index + ')').is(':disabled') ||
                    src.indexOf('disabled') > 0) {
                    return false;
                }
                if (!$('.checkViewBalanceSheet > :checkbox:eq(' + index + ')').is(':checked')) {
                    alert("僅限已勾選檢視對帳單權限之賣家使用");
                    return false;
                }
                if ($(':hidden[id$=hidIsHideBalanceSheet]:eq(' + index + ')').val() === "True") {
                    $(':hidden[id$=hidIsHideBalanceSheet]:eq(' + index + ')').val('False');
                    src = src.replace("lock", "unlock");
                }
                else {
                    $(':hidden[id$=hidIsHideBalanceSheet]:eq(' + index + ')').val('True');
                    src = src.replace("unlock", "lock");
                }

                $(this).attr("src", src);
            });

            if ($("#hidFileFlag").val() == "1") {
                //檔次過期後無法上傳檔案
                $("#dragandrophandler").css("display", "none");
                $("img[id='deleteFile']").css("display", "none");
            }



            //檢查字數
            $$('txtMarketAnalysis').maxlength({
                maxCharacters: 1000,
                slider: true
            });
            $$('txtBrandName').maxlength({
                maxCharacters: 100,
                slider: true
            });
            //$$('txtMarketingResource').maxlength({
            //    maxCharacters: 500,
            //    slider: true
            //});
            $$('txtKeyword').maxlength({
                maxCharacters: 120,
                slider: true
            });
            $$('txtMemo').maxlength({
                maxCharacters: 1000,
                slider: true
            });
            $$('txtProductSpec').maxlength({
                maxCharacters: 4000,
                slider: true
            });


            //拖曳上傳檔案
            var obj = $("#dragandrophandler");
            obj.on('dragenter', function (e) {
                e.stopPropagation();
                e.preventDefault();
                $(this).css('border', '2px solid #0B85A1');
            });
            obj.on('dragover', function (e) {
                e.stopPropagation();
                e.preventDefault();
            });
            obj.on('drop', function (e) {

                $(this).css('border', '2px dotted #0B85A1');
                e.preventDefault();
                var files = e.originalEvent.dataTransfer.files;

                //上傳檔案到Server
                handleFileUpload(files, obj);
            });
            $(document).on('dragenter', function (e) {
                e.stopPropagation();
                e.preventDefault();
            });
            $(document).on('dragover', function (e) {
                e.stopPropagation();
                e.preventDefault();
                obj.css('border', '2px dotted #0B85A1');
            });
            $(document).on('drop', function (e) {
                e.stopPropagation();
                e.preventDefault();
            });

            DataModifyOnChecked();


            //顯示商家帳號勾選
            $('#chkIsShowVbsAccountID').change(function () {
                if ($(this).is(':checked')) {
                    $('[id=IsShowVbsAccountID]').show();
                }
                else {
                    $('[id=IsShowVbsAccountID]').hide();
                }
            });


            //fix chrome disabled cannot copy
            $('textarea:disabled').removeAttr("disabled").prop("readonly", true);
	
            $('input[type=text]:disabled').not("input[id='txtStartUse2']").not("input[id='txtEarlierPageCheckDay']")
                                          .not("input[id='txtStartUse']").removeAttr("disabled").prop("readonly", true);
            
            
            

            //營業據點可排序
            $('#pponstorebody').sortable({
                update: function (event, ui) {
                    UpdatePponStoreSort();
                }
            });

        });

        function UpdatePponStoreSort() {
            var array = [];
            $('#pponstorebody tr').each(function (i, obj) {
                array[i] = $(obj).find('input[name$="hidStoreGuid"]').val();
            });

            data = {
                proposalId: <%=ProposalId%>,
                guids: array
            }

            $.ajax({
                type: "POST",
                url: "ProposalContent.aspx/SetSellerSort",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8"
            });
        }

        function handleFileUpload(files, obj) {
            var formData = new FormData();
            formData.append("ProposalId", "<%=ProposalId%>");
            for (var i = 0; i < files.length; i++) {
                formData.append('file', files[i]);
                //var status = new createStatusbar(obj); //產生 Progress.
                //status.setFileNameSize(files[i].name, files[i].size);
            }
            sendFileToServer(formData, status);
        }

        function sendFileToServer(formData, status) {
            var uploadURL = "ContractUpload.ashx";
            
            formData.append("UploadType", "SellerFile");
            $.blockUI({ message: '<h1>上傳中，請稍後....</h1>' });

            var jqXHR = $.ajax({
                url: uploadURL,
                type: "POST",
                contentType: false,
                processData: false,
                cache: false,
                data: formData,
                success: function (data) {
                    ReloadFileData(JSON.parse(JSON.parse(data).Message));
                    //alert("上傳完畢");
                    $.unblockUI();
                }, error: function (xhr) {
                    console.log(xhr.responseText);
                    $.unblockUI();
                }
            });
        }


        /*
        * 刪除檔案
        */
        function deleteSellerFile(id) {
            if (!confirm("確認刪除檔案?")) {
                return false;
            }
            $.ajax({
                url: "ProposalContent.aspx/ProposalSellerFilesDelete",
                type: "POST",
                data: "{ 'id': " + id + ", 'UserName': '<%= UserName %>'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    ReloadFileData(data.d);
                }, error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        }

        function ReloadFileData(data) {
            $("#fileResutl").empty();
            var _html = "<table style='width:100%' border='0' cellspacing='0' cellpadding='0' class='rd-table' id='tabFiles'>";
            _html += "<thead>";
            _html += "<tr style='background-color:#444;color:#fff;height:30px'>";
            _html += "<th class='OrderDate'><input type='checkbox' value='true' onclick='checkAllFile(this)' id='checkAll' /></th>";
            _html += "<th class='OrderDate'>檔名</th>";
            _html += "<th class='OrderDate'>檔案大小</th>";
            _html += "<th class='OrderDate'>上傳日期</th>";
            _html += "<th class='OrderDate'>刪除</th>";
            _html += "</tr>";
            _html += "</thead>";
            $.each(data, function (idx, d) {
                var fileSize = "0";
                if (d.FileSize < 1024) {
                    fileSize = roundX(d.FileSize, 2);
                } else if ((d.FileSize / 10240) > 0) {
                    fileSize = roundX(d.FileSize / 1024, 0) + " KB";
                } else {
                    fileSize = roundX(d.FileSize / 10240 / 1024, 0) + " MB";
                }
                _html += "<tr style='height:30px'>";
                _html += '<td style="text-align: left"><input type="checkbox" value="' + d.Id + '" /></td>';
                _html += "<td><a href='#' onclick='DownLoadSellerFile(" + d.Id + ")'>" + d.OriFileName + "</a></td>";
                _html += "<td>" + fileSize + "</td>";
                _html += "<td>" + d.CreateTime + "</td>";
                _html += "<td><img src='../Themes/default/images/17Life/G2/trash.png' alt='刪除' title='刪除' style='padding-left: 5px' class='view' onclick='deleteSellerFile(" + d.Id + ");'></td>";
                _html += "</tr>";
            });
            _html += "</table>";
            _html += '<div style="width:100%;text-align:center">';
            _html += '<input type="button" value="下載" onclick="DownloadFiles()" class="btn rd-mcacstbtn" />';
            _html += '</div>';
            $("#fileResutl").append(_html);
        }

        function parseJsonDate(jsonDateString) {
            return new Date(parseInt(jsonDateString.replace('/Date(', '')));
        }

        function setupdate(from, to) {
            var dates = $('#' + from + ', #' + to).datepicker({
                changeMonth: true,
                dateFormat: 'yy/mm/dd',
                onSelect: function (selectedDate) {
                    var option = this.id == from ? "minDate" : "maxDate";
                    var instance = $(this).data("datepicker");
                    var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                }
            });
        }

        function SelectTab(obj) {
            if ($(obj).find('.mc-navbtn').hasClass('on')) {
                return;
            } else {
                var div = $(obj).attr('tab');
                $('.mc-navbtn').each(function () {
                    $(this).removeClass('on');
                });
                $(obj).find('.mc-navbtn').addClass('on');
                $('.mc-content').hide();
                $('#' + div).show();
            }
        }

        function LocationSaveCheck() {
            // 憑證儲存必須至少勾選一個分店
            if ($("#ddlDeliveryType").val() == "<%=(int)DeliveryType.ToShop%>") {
                if ($('#pponstore').find('[id*=chkCheck]:checked').length == 0) {
                    alert('至少需勾選一個營業據點。');
                    return false;
                }
            }

            UpdatePponStoreSort();
            return true;
        }

        function VbsSaveCheck() {
            // 憑證儲存必須至少各勾選一個銷售店鋪、憑證核實與匯款對象
            if ($('#vbs').find('[id*=Verify]:checked').length == 0 || $('#vbs').find('[id*=Accouting]:checked').length == 0) {
                alert('至少需各勾選一項憑證核實與匯款對象。');
                return false;
            }
            else
            {
                if ($('#ddlDeliveryType').val() == '<%= (int)DeliveryType.ToShop %>' && !$('#chkNoRestrictedStore').is(':checked')) {
                    if ($('#vbs').find('[id*=VerifyShop]:checked').length == 0) {
                        alert('憑證類型至少需勾選一個銷售店鋪。');
                        return false;
                    }
                }
            }
            return true;
        }

        function sortTableIndx(tableID) {
            var sortData = [];
            $(tableID + " tr:not(:has(th))").each(function (idx, value) {

                var DealId = $(this).closest('tr').find("input.DealId");
                var Sort = $(this).closest('tr').find("input.Sort");
                var itemPrice = $(this).closest('tr').find('input#txtItemPrice');

                Sort.val(idx + 1);

                $.each(Deals, function (idx, deal) {
                    if (deal.Id == DealId.val()) {
                        deal.Sort = Sort.val();
                    }
                });
            });

            $.each(Deals, function (idx, deal) {
                var ProposalMultiDealSort = {
                    ItemPrice: deal.ItemPrice,
                    Sort: deal.Sort,
                    ID: deal.Id
                };
                sortData.push(ProposalMultiDealSort);
            });



            $.ajax({
                type: "POST",
                url: "ProposalContent.aspx/ProposalMiltiDealsSort",
                data: "{'ProposalId':'<%= ProposalId %>','UserName':'<%= UserName %>','sortData': '" + JSON.stringify(sortData) + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var res = response.d;
                    
                }, error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        }

        function IntialControls() {
            ProposalMiltiDealsGet();
            CPCShowOrHide();
            ShowAccountingMessage();

            //if ($("#txtBrandName").attr("disabled") != "disabled") {
            if (($("#hidApplyFlag").val() == "0" || $("#hidCreatedFlag").val() == "0") && $("#hidMultiDealsUpdate").val() == "1") {
                $(".DealEditStyle").show();
            } else {
                $(".DealEditStyle").hide();
            }

            if ($("#hidCreatedFlag").val() >= "1") {
                //已建檔的資料不能再新增Deal
                $("#addDeals").css("display", "none");
            }

            if ($("#ddlDeliveryType").val() == "<%=(int)DeliveryType.ToShop%>") {
                $(".consumertime").show();
                $("#photoToShop").show();
                $("#photoToHouse").hide();
            } else {
                if ($("#ddlDealType").val() == "<%=((int)ProposalDealType.Travel)%>") {
                    $(".consumertime").show();
                    $("#photoToShop").show();
                    $("#photoToHouse").hide();
                } else {
                    $("#photoToShop").hide();
                    $("#photoToHouse").show();
                    $(".consumertime").hide();
                }
                
            }

            $('#txtPhotographerAppointDate').datepicker({ dateFormat: 'yy/mm/dd' });
            $('#txtBusinessHourOrderTimeS').datepicker({ dateFormat: 'yy/mm/dd' });
            $('#txtContractDueDate').datepicker({ dateFormat: 'yy/mm/dd' });

            if ('<%=(int)Status%>' == '<%=(int)ProposalStatus.Created%>')
                $('#divReocrd *').prop('disabled', 'disabled');
            //else
            //    $('#divReocrd *').prop('disabled', '');


            HideDelete();
        

            initTravel();
        }

        function CheckAll(obj) {

            var id = $(obj).attr('id');

            $('#pponstore').find('[id*=chk' + id + ']').each(function () {
                if ($(obj).is(':checked') && $(this).parent().next().next().find('[id*=pAddress]').text() != '') {
                    $(this).attr('checked', 'checked');
                } else {
                    $(this).removeAttr('checked');
                }
            });

            $('#vbs').find('[id$=chk' + id + ']').each(function () {
                if ($(obj).is(':checked') && !$(this).is(':disabled')) {
                    $(this).attr('checked', 'checked');
                } else {
                    $(this).removeAttr('checked');
                }
            });
        }

        function CheckAll2(id)
        {
            //檢核checkAll
            var TotalCount = 0;
            var CheckCount = 0;
            $('[id$=chk' + id + ']').each(function () {

                if (!$(this).is(':disabled'))
                    TotalCount++;
                if ($(this).is(':checked'))
                    CheckCount++;
            });
            if (TotalCount == CheckCount && CheckCount != 0 && TotalCount != 0)
                $('#' + id).attr('checked', true);
            else
                $('#' + id).attr('checked', false);

        }

        //取得優惠方案資料
        function ProposalMiltiDealsGet() {
            $.ajax({
                type: "POST",
                url: "ProposalContent.aspx/ProposalMiltiDealsGet",
                data: "{'ProposalId':'<%= ProposalId %>' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var res = response.d;
                    if (res != null) {
                        multiDealBind(res);
                    } else {
                        addChanges();
                    }


                }, error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        }

        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                  .toString(16)
                  .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
              s4() + '-' + s4() + s4() + s4();
        }

        function appendSlide() {
            if ($("#deal").length > 0) {
                $("#subMenu").append('<li><a href="#deal">提案內容</a></li>');
            }
            if ($("#<%=divMultiDeals.ClientID%>").length > 0) {
                $("#subMenu").append('<li class="current"><a href="#<%=divMultiDeals.ClientID%>">優惠內容</a></li>');
            }
            
            if ($("#dealDate").length > 0) {
                $("#subMenu").append('<li><a href="#dealDate">時程資訊</a></li>');
            }
            if ($("#dealInfo").length > 0) {
                $("#subMenu").append('<li><a href="#dealInfo">商品資訊</a></li>');
            }
            if ($("#<%=divAssign.ClientID%>").length > 0) {
                $("#subMenu").append('<li><a href="#<%=divAssign.ClientID%>">工作指派</a></li>');
            }
            if ($("#<%=divHistory.ClientID%>").length > 0) {
                $("#subMenu").append('<li><a href="#<%=divHistory.ClientID%>">注意事項</a></li>');
            }            
        }

        function ReturnedCheckLog() {
            var cf = false;
            $.each($("#returnReason input[type=checkbox]"), function () {
                if ($(this).attr("checked") == "checked") {
                    cf = true;
                }
            });
            if (!cf) {
                alert('請選取退件類別');
                return false;
            }

            if ($.trim($('#txtReturnedLog').val()) == '') {
                alert('請填寫退件原因');
                $('#txtReturnedLog').focus();
                return false;
            }
            return true;
        }
        function SellerReturnedCheckLog() {
            if ($.trim($('#txtSellerReturnedLog').val()) == '') {
                alert('請填寫退件原因');
                $('#txtSellerReturnedLog').focus();
                return false;
            }
            return true;
        }

        function CheckLog() {
            if ($.trim($('#txtChangeLog').val()) == '') {
                $('#pChangeLog').show();
                return false;
            }
            return true;
        }
        function chkSellerProposalCheckWaitting() {
            if (!confirm("確定要請求商家覆核?")) {
                return false;
            }
        }


        function openOptionsPopup(id) {
            $.fancybox({
                'padding': 0,
                'width': 300,
                'height': 300,
                'type': 'iframe',
                content: $('#divSellerOptions').show(),
                afterLoad: function () {
                    $.ajax({
                        type: "POST",
                        url: "ProposalContent.aspx/ProposalMiltiDealsGet",
                        data: "{'ProposalId':'<%= ProposalId %>'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var data = eval(response.d);
                            $.each(data, function (idx, obj) {
                                if (obj.Id == id) {
                                    $('#divSellerOptions').html(obj.SellerOptions);
                                }
                            });
                        }, error: function (xhr) {
                            console.log(xhr.responseText);
                        }
                    });
                }
            });
        }

        function SaveCheck() {

            var check = true;
            $('.if-error').hide();
            $('.error').removeClass('error');

            $("#txtMultiDeals").val("");

            if ($.trim($('#txtBrandName').val()) == '') {
                $('#pBrandName').show();
                $('#pBrandName').parent().parent().addClass('error');
                $('#txtBrandName').focus();
                check = false;
            }

            

           <%-- if ($('#chkPhotographers').is(':checked')) {
                if ($("#ddlDeliveryType").val() == "<%=(int)DeliveryType.ToShop%>") {
                    if ($.trim($('#txtPhotoAddress').val()) == '') {
                        $('#pPhotoAddress').show();
                        $('#pPhotoAddress').parent().parent().addClass('error');
                        $('#txtPhotoAddress').focus();
                        check = false;
                    }
                    if ($.trim($('#txtPhotoContact').val()) == '') {
                        $('#pPhotoContact').show();
                        $('#pPhotoContact').parent().parent().addClass('error');
                        $('#txtPhotoContact').focus();
                        check = false;
                    }
                    if ($.trim($('#txtPhotoPhone').val()) == '') {
                        $('#pPhotoPhone').show();
                        $('#pPhotoPhone').parent().parent().addClass('error');
                        $('#txtPhotoPhone').focus();
                        check = false;
                    }
                    if ($.trim($('#txtPhotoMemo').val()) == '') {
                        $('#pPhotoMemo').show();
                        $('#pPhotoMemo').parent().parent().addClass('error');
                        $('#txtPhotoMemo').focus();
                        check = false;
                    }
                }
            }--%>

            //if ($('#chkFTPPhoto').is(':checked') && $.trim($('#txtFTPPhoto').val()) == '') {
            //    $('#pFTPPhoto').show();
            //    $('#pFTPPhoto').parent().parent().addClass('error');
            //    $('#txtFTPPhoto').focus();
            //    check = false;
            //}


            if ($('#chkInvoiceFounder').is(':checked') && $.trim($('#txtInvoiceFounder').val()) == '') {
                $('#pInvoiceFounder').show();
                $('#pInvoiceFounder').parent().parent().addClass('error');
                $('#txtInvoiceFounder').focus();
                check = false;
            }

            if ($('#chkEarlierPageCheck').is(':checked')) {
                if ($.trim($('#txtEarlierPageCheckDay').val()) == "" || isNaN($('#txtEarlierPageCheckDay').val())) {
                    $('#pEarlierPageCheck').show();
                    $('#pEarlierPageCheck').parent().parent().addClass('error');
                    $('#txtEarlierPageCheckDay').focus();
                    check = false;
                }                
            }

            if ($('#ddlDeliveryType').val() == '<%= (int)DeliveryType.ToShop %>') {
                if ($.trim($('#txtStartUse').val()) == '') {
                    $('#pStartUse').show();
                    $('#pStartUse').parent().parent().addClass('error');
                    $('#txtStartUse').focus();
                    check = false;
                }
                if ($('#ddlStartUseUnit').val() != '<%= (int)ProposalStartUseUnitType.BeforeDay %>' && (isNaN(parseInt($('#txtStartUse').val())) || parseInt($('#txtStartUse').val()) > 500)) {
                    $('#pStartUseError').show();
                    $('#pStartUseError').parent().parent().addClass('error');
                    $('#txtStartUse').focus();
                    check = false;
                }
                if ($('#ddlStartUseUnit').val() == '<%= (int)ProposalStartUseUnitType.BeforeDay %>' && (isNaN(parseInt($('#txtStartUse').val())))) {
                    $('#pStartUseError').show();
                    $('#pStartUseError').parent().parent().addClass('error');
                    $('#txtStartUse').focus();
                    check = false;
                }
                if ($('#ddlStartUseUnit').val() == '<%= (int)ProposalStartUseUnitType.BeforeDay %>' && (isNaN(parseInt($('#txtStartUse2').val())) && $.trim($('#txtStartUse2').val()) != '')) {
                    $('#pStartUseError').show();
                    $('#pStartUseError').parent().parent().addClass('error');
                    $('#txtStartUse2').focus();
                    check = false;
                }
                if ($('#ddlStartUseUnit').val() == '<%= (int)ProposalStartUseUnitType.BeforeDay %>' && (!isValidDate($('#txtStartUse').val()))) {
                    $('#pStartUseDateError').show();
                    $('#pStartUseDateError').parent().parent().addClass('error');
                    $('#txtStartUse').focus();
                    check = false;
                }
                if ($('#ddlStartUseUnit').val() == '<%= (int)ProposalStartUseUnitType.BeforeDay %>' && (!isValidDate($('#txtStartUse2').val()) && $.trim($('#txtStartUse2').val()) != '')) {
                    $('#pStartUseDateError').show();
                    $('#pStartUseDateError').parent().parent().addClass('error');
                    $('#txtStartUse2').focus();
                    check = false;
                }
            } else if ($('#ddlDeliveryType').val() == '<%= (int)DeliveryType.ToHouse %>') {
                if ($.trim($('#txtProductSpec').val()) == '') {
                    $('#pProductSpec').show();
                    $('#pProductSpec').parent().parent().addClass('error');
                    $('#txtProductSpec').focus();
                    check = false;
                }

                if ($('#ddlShipType').val() == '<%= (int)DealShipType.Normal %>') {
                    if ($("#<%=rboUniShipDay.ClientID%>").attr("checked") == "checked")
                    {
                        if ($.trim($('#txtShipText1').val()) == '' || $.trim($('#txtShipText2').val()) == '')
                        {
                            $('#pShipTypeError').show();
                            $('#pShipTypeError').parent().parent().addClass('error');
                            $('#txtShipText1').focus();
                            check = false;
                        }
                        else if (($.trim($('#txtShipText1').val()) != '' && isNaN($.trim($('#txtShipText1').val()))) || ($.trim($('#txtShipText2').val()) != '' && isNaN($.trim($('#txtShipText2').val()))))
                        {
                            $('#pShipTypeError').show();
                            $('#pShipTypeError').parent().parent().addClass('error');
                            $('#txtShipText1').focus();
                            check = false;
                        }
                    }
                    else
                    {
                        if ($.trim($('#txtShipText3').val()) == '') {
                            $('#pShipTypeError').show();
                            $('#pShipTypeError').parent().parent().addClass('error');
                            $('#txtShipText1').focus();
                            check = false;
                        }
                        else if ($.trim($('#txtShipText3').val()) != '' && isNaN($.trim($('#txtShipText3').val())))
                        {
                            $('#pShipTypeError').show();
                            $('#pShipTypeError').parent().parent().addClass('error');
                            $('#txtShipText1').focus();
                            check = false;
                        }
                    }
                }
            }

            if ($('#chkSelfOptional').is(':checked')) {
                if ($.trim($("#txtBusinessPrice").val()) == "" || isNaN($.trim($("#txtBusinessPrice").val()))) {
                    $('#pBusinessPrice').show();
                    $('#pBusinessPrice').parent().parent().addClass('error');
                    check = false;
                }
            }

            $("#txtMultiDeals").val(JSON.stringify(Deals));

            if ($("#ddlDeliveryType").val() == "<%=(int)DeliveryType.ToShop%>" || $("#ddlDealType").val() == "<%=((int)ProposalDealType.Travel)%>") {
                var cf = false;
                $.each($(".consumertime input[type=checkbox]"), function () {
                    if ($(this).attr("checked")=="checked") {
                        cf = true;
                    }
                });
                if (!cf) {
                    alert("請至少選取一項時段與消費");
                    check = false;
                }
            }


            if (Deals.length == 0)
            {
                alert("請至少填寫一項優惠內容");
                check = false;
            }



            //本檔主打星
            if (!$("#chkDealStar").is(":checked") && $("#txtDealStar").val() == '')
            {
                $("#pDealStar").parent().parent().addClass('error');
                $("#pDealStar").show();
                $("#txtDealStar").focus();
                check = false;
            }

            //檔次特色
            if ($("#ddlDeliveryType").val() == "<%=(int)DeliveryType.ToShop%>" && $("#ddlDealType").val() != "<%=((int)ProposalDealType.Travel)%>")
            {
                if ((!$("#chkDealCharacterToShop1").is(":checked") && $("#txtDealCharacterToShop1").val() == '') || (!$("#chkDealCharacterToShop2").is(":checked") && $("#txtDealCharacterToShop2").val() == '') ||
                    (!$("#chkDealCharacterToShop3").is(":checked") && $("#txtDealCharacterToShop3").val() == '') || (!$("#chkDealCharacterToShop4").is(":checked") && $("#txtDealCharacterToShop4").val() == '')) {
                    $("#pDealCharacterToShop").parent().parent().addClass('error');
                    $("#pDealCharacterToShop").show();
                    $("#divDealCharacterToShop").attr("tabindex", -1).focus();
                    check = false;
                }
                
            }
            else
            {
                if (!$("#chkDealCharacterToHouse").is(":checked") && ($("#txtDealCharacterToHouse1").val() == "" || $("#txtDealCharacterToHouse2").val() == "" || $("#txtDealCharacterToHouse3").val() == "")) {
                    $("#pDealCharacterToHouse").parent().parent().addClass('error');
                    $("#pDealCharacterToHouse").show();
                    $("#divDealCharacterToHouse").attr("tabindex", -1).focus();
                    check = false;
                }
            }

            //店家單據開立方式

            if (('<%=SellerId%>'!='')&&('<%=(int)Status%>'=='<%=(int)ProposalStatus.Initial%>')&&(!($('#<%=rbRecordInvoice.ClientID%>').is(':checked') || $('#<%=rbRecordNoTaxInvoice.ClientID%>').is(':checked') || $('#<%=rbRecordReceipt.ClientID%>').is(':checked') || $('#<%=rbRecordOthers.ClientID%>').is(':checked'))))
            {
                check = false;
                $('#pRecordOthers').show();
                $('#pRecordOthers').parent().parent().addClass('error');
                $('#<%=rbRecordOthers.ClientID%>').focus();
            }


            if ($('#<%=rbRecordOthers.ClientID%>').is(':checked') && $('#<%=txtAccountingMessage.ClientID%>').val()=="") {
                check = false;
                $('#pRecordOthers').show();
                $('#pRecordOthers').parent().parent().addClass('error');
                $('#<%=rbRecordOthers.ClientID%>').focus();                
            }


            if (!ChannelSaveCheck()) {
                return false;
            }

            if (!CheckEveryDayNewDeal())
            {
                alert('【特殊標記下->每日一物】及【宅配->每日下殺】需要設定一致，請調整後再送出。');
                return false;
            }

            //為了讓免稅商品在disable的狀態下還能背後端取到值
            $('#<%=chkFreeTax.ClientID %>').attr('disabled', false);
            $('[id*=rdbMediaReportIn]').attr("disabled", false);
            $('[id*=rdbMediaReportOut]').attr("disabled", false);

            $("#divChannelList input[type='checkbox']").attr("disabled", false);

            return check;

    }

    //div隱藏
    function CheckSpecialFlag(obj, id) {
        if ($(obj).is(':checked')) {
            $('#div' + id).show();
        } else {
            $('#div' + id).hide();
            $('#txt' + id).val('');
        }
    }

    //txt隱藏
    function CheckSpecialFlag2(obj, id) {

        if ($(obj).is(':checked')) {
            $("#txt" + id).show();

            $("#div" + id + "Pic").show();
            $("#div" + id + "Link").show();
        } else {
            $("#txt" + id).hide();
            $('#txt' + id).val('');

            $("#div" + id + "Pic").hide();
            $("#div" + id + "Link").hide();
        }

        //用disabled後端抓不到資料
        if ($(obj).is(':disabled')) {
            $("#txt" + id).prop("readOnly", true);

            $("#txt" + id + "1").prop("readOnly", true);
            $("#txt" + id + "2").prop("readOnly", true);
            $("#txt" + id + "3").prop("readOnly", true);
            $("#txt" + id + "4").prop("readOnly", true);
            $("#txt" + id + "5").prop("readOnly", true);
            $("[id*=rdb" + id + "In]").prop("disabled", true);
            $("[id*=rdb" + id + "Out]").prop("disabled", true);
            $('#imgMediaReportAdd').hide();
        }

    }

    //鎖住底下控制項
    function Enabletxt(obj, id) {
        if ($(obj).is(':checked') || $(obj).is(':disabled')) {
            //若有勾起或disable,則text要readonly

            $("[id *= txt" + id + "]").prop("readOnly", true);
            if (!$(obj).is(':disabled'))
                $("[id *= txt" + id + "]").val('');//勾起但是disable,則text要清空
            $("[id *= rdb" + id + "]").prop("disabled", true);
            $("[id *= img" + id + "Add]").attr("onclick", '');
            $("[id *= img" + id + "Delete]").attr("onclick", '');
            if (id == "MediaReport") {
                $("[id *= div" + id + "Link2]").hide();
                $("[id *= div" + id + "Link3]").hide();
                $("[id *= div" + id + "Link4]").hide();
                $("[id *= div" + id + "Link5]").hide();
            }
        }
        else {
            $("[id *= txt" + id + "]").prop("readOnly", false);
            $("[id *= rdb" + id + "]").prop("disabled", false);
            $("[id *= img" + id + "Add]").attr("onclick", 'addTextbox();');
            $("[id *= img" + id + "Delete]").attr("onclick", 'DeleteTextbox(this);');
        }
    }
    


    function initTravel() {
        if ($("#ddlDeliveryType").val() == '<%= (int)DeliveryType.ToShop %>') {

        } else {
            if ($("#ddlDealType").val() == "<%=((int)ProposalDealType.Travel)%>") {
                $(".travelview").show();
            } else {
                $(".travelview").hide();
            }
        }
        
        if ($("#ddlDealType").val() == "<%=((int)ProposalDealType.Travel)%>") {
            $(".consumertime").show();
        }
    }

    <%--function CheckPhotographFlag(obj) {
        if ($(obj).is(':checked')) {
            if ($("#ddlDeliveryType").val() == "<%=(int)DeliveryType.ToShop%>") {
                $('#divPhotographers').show();
                $('#divDeliveryPhotographers').hide();
            } else {
                $('#divPhotographers').hide();
                $('#divDeliveryPhotographers').show();
            }
        } else {
            $('#divPhotographers').hide();
            $('#divDeliveryPhotographers').hide();
            $('#txtPhotoAddress').val('');
            $('#txtPhotoTel').val('');
            $('#txtPhotoPhone').val('');
            $('#txtPhotoMemo').val('');
            $('#txtPhotoDate').val('');
            $('#txtPhotoUrl').val('');
            $('#txtPhohoNeeds').val('');
            $('#txtRefrenceData').val('');
            $('#txtPhotoContact').val('');
        }
    }--%>

    function BindDealSubType(obj) {
        $.ajax({
            type: "POST",
            url: "ProposalContent.aspx/GetDealSubType",
            data: "{'id': '" + $(obj).val() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var subType = $('#ddlDealSubType');
                $(subType).find('option').remove();
                $.each(response.d, function (index, item) {
                    $(subType).append($('<option></option>').attr('value', item.Key).text(item.Value));
                });

                $(subType).val($('#hidDealSubType').val());
                SetDealSubType(subType);

                if ($(obj).val() == "<%=(int)ProposalDealType.Product%>") {
                    $("#dealMarket").show();
                } else {
                    $("#dealMarket").hide();
                }
            }
        });
    }

        function SetDealSubType(obj) {

            $('#hidDealSubType').val($(obj).val());

            //先暫存
            $('#hidOtherShop1').val($("#ddlOtherShop1").val());
            $('#hidOtherShop2').val($("#ddlOtherShop2").val());
            $('#hidOtherShop3').val($("#ddlOtherShop3").val());

            if ($(obj).val() == "<%=(int)ProposalDealSubType.Child%>"
            || $(obj).val() == "<%=(int)ProposalDealSubType.Popular%>"
            || $(obj).val() == "<%=(int)ProposalDealSubType.Living%>"
            || $(obj).val() == "<%=(int)ProposalDealSubType.ConsumerElectronic%>") {
                // 流行/婦幼/居家生活/3C
                $("#ddlOtherShop1").empty();
                $("#ddlOtherShop1").append($('<option></option>').attr('value', "-1").text("請選擇"));
                $("#ddlOtherShop1").append($('<option></option>').attr('value', "yahoo").text("Yahoo!奇摩超級商城"));
                $("#ddlOtherShop1").append($('<option></option>').attr('value', "pchome").text("PChome 商店街"));
                $("#ddlOtherShop1").append($('<option></option>').attr('value', "taobao").text("淘寶"));

                $("#ddlOtherShop2").empty();
                $("#ddlOtherShop2").append($('<option></option>').attr('value', "-1").text("請選擇"));
                $("#ddlOtherShop2").append($('<option></option>').attr('value', "yahoo").text("Yahoo!奇摩超級商城"));
                $("#ddlOtherShop2").append($('<option></option>').attr('value', "pchome").text("PChome 商店街"));
                $("#ddlOtherShop2").append($('<option></option>').attr('value', "taobao").text("淘寶"));

                $("#ddlOtherShop3").empty();
                $("#ddlOtherShop3").append($('<option></option>').attr('value', "-1").text("請選擇"));
                $("#ddlOtherShop3").append($('<option></option>').attr('value', "yahoo").text("Yahoo!奇摩超級商城"));
                $("#ddlOtherShop3").append($('<option></option>').attr('value', "pchome").text("PChome 商店街"));
                $("#ddlOtherShop3").append($('<option></option>').attr('value', "taobao").text("淘寶"));
            } else {
                $("#ddlOtherShop1").empty();
                $("#ddlOtherShop1").append($('<option></option>').attr('value', "-1").text("請選擇"));
                $("#ddlOtherShop1").append($('<option></option>').attr('value', "yahoo").text("Yahoo!奇摩超級商城"));
                $("#ddlOtherShop1").append($('<option></option>').attr('value', "pchome").text("PChome 商店街"));

                $("#ddlOtherShop2").empty();
                $("#ddlOtherShop2").append($('<option></option>').attr('value', "-1").text("請選擇"));
                $("#ddlOtherShop2").append($('<option></option>').attr('value', "yahoo").text("Yahoo!奇摩超級商城"));
                $("#ddlOtherShop2").append($('<option></option>').attr('value', "pchome").text("PChome 商店街"));

                $("#ddlOtherShop3").empty();
                $("#ddlOtherShop3").append($('<option></option>').attr('value', "-1").text("請選擇"));
                $("#ddlOtherShop3").append($('<option></option>').attr('value', "yahoo").text("Yahoo!奇摩超級商城"));
                $("#ddlOtherShop3").append($('<option></option>').attr('value', "pchome").text("PChome 商店街"));
            }

            $('#ddlOtherShop1').val($('#hidOtherShop1').val());
            $('#ddlOtherShop2').val($('#hidOtherShop2').val());
            $('#ddlOtherShop3').val($('#hidOtherShop3').val());
        }

        function checkAllFile(chk) {
            var $this = $(chk);
            $.each($("#tabFiles input[type='checkbox']"), function (idx, obj) {
                if ($(obj).attr("id") != "checkAll") {
                    $(obj).prop("checked", $this.prop("checked"));
                }
            });
        }


    function StartUseChange(obj) {
        if ($(obj).val() == '<%= (int)ProposalStartUseUnitType.BeforeDay %>') {
            $(obj).prev('#txtStartUse').attr('placeHolder', 'yyyy/MM/dd');
            $('#txtStartUse2').attr('placeHolder', 'yyyy/MM/dd');
            $('#txtStartUse').val('');
            setupdate('txtStartUse2', 'txtStartUse');
            $(".range1").hide();
            $(".range2").show();
            } else {
            $(obj).prev('#txtStartUse').removeAttr('placeHolder');
            $('#txtStartUse').val('');
            $('#txtStartUse2').val('');
            $('#txtStartUse').datepicker("destroy");
            $('#txtStartUse2').datepicker("destroy");
            $(".range1").show();
            $(".range2").hide();
            }
        }

        function ShipTypeChange(obj) {
            if ($(obj).val() == '<%= (int)DealShipType.Normal %>') {
                $('#pShipType').show();
                $('#pShipTypeOther').hide();
                $("#txtShipTypeOther").val("");
            } else if ($(obj).val() == '<%= (int)DealShipType.Other %>') {
                $('#pShipType').hide();
                $('#pShipTypeOther').show();
                $('#txtShipText1').val('');
                $('#txtShipText2').val('');
            }else{
                $('#pShipType').hide();
                $('#txtShipText1').val('');
                $('#txtShipText2').val('');
            }
        }

        function deliveryTypeChange(obj) {
            if ($(obj).val() == '<%= (int)DeliveryType.ToShop %>') {
                //憑證
                $('.ppon').show();
                $('.product').hide();
                $(".GroupCoupon").show();
            } else {
                //宅配
                $('.ppon').hide();
                $('.product').show();
                $(".GroupCoupon").hide();
                $("#chkGroupCoupon").removeAttr("checked");

                if ($("#hidShoppingCartV2Enabled").val() == "1") {
                    //購物車啟用
                    $(".shoppingCartCfgHide").hide();
                    $(".shoppingCartCfgShow").show();
                } else {
                    $(".shoppingCartCfgHide").show();
                    $(".shoppingCartCfgShow").hide();
                }
            }
        }

        function ValidateNumber(e, pnumber) {
            if (!/^\d+$/.test(pnumber)) {
                $(e).val(/^\d+/.exec($(e).val()));
            }
            return false;
        }

        function ValidateNumber(pnumber) {
            if (!/^\d+$/.test(pnumber)) {
                return false;
            }
            else
            {
                return true;
            }
            
        }

        //優惠內容進入編輯模式
        function changeToEditMode(ele) {
            var tr = $(ele).closest('tr');
            tr.find('td').addClass('editStyle');
            tr.find('.edit').show();
            tr.find('.view').hide();

            var itemPrice = tr.find('input#txtItemPrice');
            var grossMargin = tr.find("input#txtGrossMargin");
            var cost = tr.find('input#txtCost');
            var slottingFeeQuantity = tr.find('input#txtSlottingFeeQuantity');
            var orderTotalLimit = tr.find('input#txtOrderTotalLimit');
            var cpcBuy = tr.find('input#txtCPCBuy');
            var cpcPresent = tr.find('input#txtCPCPresent');

            //毛利率
            var _gross = GetGrossMargin(itemPrice.val(), cost.val(), slottingFeeQuantity.val(), orderTotalLimit.val(), cpcBuy.val(), cpcPresent.val());
            grossMargin.val(_gross);


            if ($("#hidApplyFlag").val() == "1" && $("#hidCreatedFlag").val() == "0" && $("#hidMultiDealsUpdate").val() == "1") {
                //QC之後，建檔之前
                //最大購買、ATM保留、多重選項要可以編輯
                tr.find('.qcedit').show();
                tr.find('.qcview').hide();
            }
        }
        /*
        * 取消編輯模式
        */
        function cancelChanges(ele) {
            finishEditing(ele);
            ProposalMiltiDealsGet();
        }
        /*
        * 回復瀏覽模式
        */
        function finishEditing(ele) {
            var tr = $(ele).closest('tr');
            tr.find('td').removeClass('editStyle');

            tr.find('.edit').hide();
            tr.find('.view').show();
        }
        /*
        * 刪除
        */
        function deleteChanges(ele) {
            if (!confirm("確定要刪除此筆資料?")) {
                return false;
            }
            saveChanges(ele, true, false);
            var tr = $(ele).closest('tr');
            tr.remove();
        }
        /*
        * 複製
        */
        function copyChanges(ele) {
            if (!confirm("確定要複製此筆資料?")) {
                return false;
            }
            saveChanges(ele, false, true);
        }

        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                  .toString(16)
                  .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
              s4() + '-' + s4() + s4() + s4();
        }


        /*
        * 儲存資料
        */
        function saveChanges(ele, isDelete, isCopy) {
            var tr = $(ele).closest('tr');

            var id = tr.find('input.DealId');
            var uniqueKey = tr.find('input.UniqueKey');

            var itemNameTravel = tr.find('textarea#txtItemNameTravel');
            var itemName = tr.find('textarea#txtItemName');
            itemNameTravel.val(itemNameTravel.val().replace("\"", "").replace("\u000B", "").replace("\u0003", "").replace("\u0008", "").replace("\\", "\\\\").replace("'", "\\u0027").replace("\u2028", ""));
            itemName.val(itemName.val().replace("\"", "").replace("\u000B", "").replace("\u0003", "").replace("\u0008", "").replace("\\", "\\\\").replace("'", "\\u0027").replace("\u2028", ""));

            var origPrice = tr.find('input#txtOrigPrice');
            var itemPrice = tr.find('input#txtItemPrice');
            var cpcBuy = tr.find('input#txtCPCBuy');
            var cpcPresent = tr.find('input#txtCPCPresent');
            var grossMargin = tr.find("input#txtGrossMargin");
            var cost = tr.find('input#txtCost');
            var costpercent = tr.find('input#txtCostPercent');
            var slottingFeeQuantity = tr.find('input#txtSlottingFeeQuantity');
            var orderTotalLimit = tr.find('input#txtOrderTotalLimit');
            var orderMaxPersonal = tr.find('input#txtOrderMaxPersonal');
            var atmMaximum = tr.find('input#txtAtmMaximum');
            var freights = tr.find('input#txtFreights');
            var noFreightLimit = tr.find('input#txtNoFreightLimit');
            var options = tr.find('textarea#txtOptions');
            var selleroptions = tr.find('textarea#txtSellerOptions');
            var CPCData = tr.find('span.CPCData');
            var cpc = tr.find('td.CPC');
            var sort = tr.find('input.Sort');
            var freightsId = "";
            var tmpFreightsId = tr.find('input[name^="FreightsId"]');
            $.each(tmpFreightsId, function (fIdx, fVal) {
                if ($(fVal).is(":checked")) {
                    freightsId = $(fVal).val();
                }
            });

            var message = checkDealData(false, itemName.val(), itemNameTravel.val(), itemPrice.val(), origPrice.val(), origPrice.val(), cost.val(), costpercent.val(), orderTotalLimit.val(), orderMaxPersonal.val(), cpcBuy.val(), cpcPresent.val(), slottingFeeQuantity.val(), atmMaximum.val());
            if (message != "") {
                alert(message);
                return false;
            }

            itemName.addClass("edit");
            origPrice.addClass("edit");
            itemPrice.addClass("edit");
            cpcBuy.addClass("edit");
            cpcPresent.addClass("edit");
            grossMargin.addClass("edit");
            cost.addClass("edit");
            slottingFeeQuantity.addClass("edit");
            orderTotalLimit.addClass("edit");
            orderMaxPersonal.addClass("edit");
            atmMaximum.addClass("edit");
            freights.addClass("edit");
            noFreightLimit.addClass("edit");
            options.addClass("edit");

            //憑證組數是否顯示
            CPCShowOrHide();

            tr.find('span.DealId').html(id.val());
            tr.find('span.ItemName').html(itemName.val().replace("\"", "").replace("<", "").replace(">", ""));
            tr.find('span.ItemNameTravel').html(itemNameTravel.val().replace("\"", "").replace("<", "").replace(">", ""));
            tr.find('span.OrigPrice').html(origPrice.val());
            tr.find('span.ItemPrice').html(itemPrice.val());
            tr.find('span.CPCBuy').html(cpcBuy.val());
            tr.find('span.CPCPresent').html(cpcPresent.val());
            tr.find('span.CPCData').html("買" + cpcBuy.val() + "送" + cpcPresent.val());
            tr.find('span.GrossMargin').html(grossMargin.val());
            tr.find('span.Cost').html(cost.val());
            tr.find('span.SlottingFeeQuantity').html(slottingFeeQuantity.val());
            tr.find('span.OrderTotalLimit').html(orderTotalLimit.val());
            tr.find('span.OrderMaxPersonal').html(orderMaxPersonal.val());
            tr.find('span.AtmMaximum').html(atmMaximum.val());
            tr.find('span.Freights').html(freights.val());
            tr.find('span.NoFreightLimit').html(noFreightLimit);
            tr.find('span.Options').html(options);
            tr.find('span.SellerOptions').html(selleroptions);
            


            //毛利率
            var _gross = GetGrossMargin(itemPrice.val(), cost.val(), slottingFeeQuantity.val(), orderTotalLimit.val(),cpcBuy.val(),cpcPresent.val());
            grossMargin.val(_gross);

            //完成編輯
            finishEditing(ele);

            var __id = 0;
            if (!isCopy) {
                __id = id.val();
            }
            var __uniqueKey = guid();
            if (!isCopy) {
                __uniqueKey = uniqueKey.val();
            } else {
                uniqueKey.val(__uniqueKey);
            }

            var _selleroptions = "";
            if (selleroptions.val() != undefined) {
                _selleroptions = selleroptions.val().replace("\"", "");
            }

            var ProposalMultiDealModel = addDeals(__uniqueKey, __id, sort.val(), itemName.val(), itemNameTravel.val(), origPrice.val(), itemPrice.val(),
                cpcBuy.val(), cpcPresent.val(), cost.val(), slottingFeeQuantity.val(), orderTotalLimit.val(), orderMaxPersonal.val(),
                atmMaximum.val(), freights.val(), noFreightLimit.val(), options.val().replace("\"", ""), _selleroptions, freightsId);

            saveProposalMultiDeal(ele, ProposalMultiDealModel, isDelete, isCopy, itemName.val(), itemPrice.val());
        }

        //異動售價or進貨價重算獲利毛利率+洽談毛利率
        function changeGross(ele) {
            var tr = $(ele).closest('tr');
            var itemPrice = tr.find('input#txtItemPrice');
            var grossMargin = tr.find("input#txtGrossMargin");
            var cost = tr.find('input#txtCost');
            var costpercent = tr.find('input#txtCostPercent');
            var slottingFeeQuantity = tr.find('input#txtSlottingFeeQuantity');
            var orderTotalLimit = tr.find('input#txtOrderTotalLimit');
            var cpcBuy = tr.find('input#txtCPCBuy');
            var cpcPresent = tr.find('input#txtCPCPresent');

            

            var _gross = "";
            if (isNaN($.trim(itemPrice.val())) || $.trim(itemPrice.val()) == 0 || $.trim(itemPrice.val()) == "") {

            } else if (isNaN($.trim(cost.val())) || $.trim(cost.val()) == 0 || $.trim(cost.val()) == "") {

            } else {
                _gross = GetGrossMargin(itemPrice.val(), cost.val(), slottingFeeQuantity.val(), orderTotalLimit.val(), cpcBuy.val(), cpcPresent.val());
            }


            //獲利毛利率
            grossMargin.val(_gross);

            //洽談毛利率
            var _costpercent = '';
            _costpercent = GetCostPercent(itemPrice.val(), cost.val());
            costpercent.val(_costpercent);
        }
        
        //計算獲利毛利率(ProposaFacede也有，要改起一併調整)
        function GetGrossMargin(itemPrice, cost, slottingFeeQuantity, orderTotalLimit, cpcBuy, cpcPresent)
        {
            var value = 0;


            if ("<%= Helper.IsFlagSet(Convert.ToInt32(hidCreatedFlag.Value),(int)ProposalBusinessCreateFlag.Created)%>" == "False")
            {
                //還在提案單(尚未建檔)

                ///1.05後先捨去到小數點第四位
                //售價都要/1.05
                itemPrice = Math.floor((itemPrice / 1.05) * Math.pow(10, (4 || 0))) / Math.pow(10, (4 || 0));;

                //統一發票(含稅)，才除以1.05
                if ($('#<%=rbRecordInvoice.ClientID%>').is(':checked') || $('#<%=rbRecordOthers.ClientID%>').is(':checked'))
                {
                    cost = Math.floor((cost / 1.05) * Math.pow(10, (4 || 0))) / Math.pow(10, (4 || 0));
                }
            }
            else if ("<%= Helper.IsFlagSet(Convert.ToInt32(hidCreatedFlag.Value),(int)ProposalBusinessCreateFlag.Created)%>" == "True")
            {
                //已建檔

                //店家發票免稅影響進價
                if (($('#hidRecord').val() == "2" && $('#hidInputTax').val() == "True") || $('#hidRecord').val() == "0")
                {
                    cost = Math.floor((cost / 1.05) * Math.pow(10, (4 || 0))) / Math.pow(10, (4 || 0));
                }
                //開立免稅發票給消費者影響售價
                if ("<%= Helper.IsFlagSet(Convert.ToInt32(hidGroupOrder.Value),(int)GroupOrderStatus.NoTax)%>" == "False")
                {
                    itemPrice = Math.floor((itemPrice / 1.05) * Math.pow(10, (4 || 0))) / Math.pow(10, (4 || 0));;;
                }
            }
            


            if (slottingFeeQuantity != 0)
            {
                //上架費算法
                value = (itemPrice == 0 || orderTotalLimit == 0 ? 0 : ((itemPrice) * orderTotalLimit - (cost * (orderTotalLimit - slottingFeeQuantity))) / ((itemPrice) * orderTotalLimit) * 100);
            }
            else
            {
                //一般算法 or 成套票券算法
                value = (itemPrice == 0 ? 0 : (itemPrice - cost) / itemPrice * 100);
            }
                
            return Math.floor(value.toPrecision(12) * Math.pow(10, (2 || 0))) / Math.pow(10, (2 || 0));
        }

        //從洽談毛利率反算進貨價
        function GetCost(ele)
        {
            var tr = $(ele).closest('tr');
            var itemPrice = tr.find('input#txtItemPrice');
            var costpercent = tr.find('input#txtCostPercent');
            var cost = tr.find('input#txtCost');
            var _cost = '';
            var _costpercent = '';

            if (costpercent.val() != '')
            {
                //計算進價
                _cost = itemPrice.val() * (costpercent.val() / 100);
                _cost = itemPrice.val() - _cost;
                cost.val(Math.round(_cost));

                //重算毛利率
                changeGross(ele);

                //重算進貨價百分比
                var _costpercent = '';
                _costpercent = GetCostPercent(itemPrice.val(), cost.val());
                costpercent.val(_costpercent);

            }
            
        }

        //計算洽談毛利率(ProposaFacede也有，要改起一併調整)
        function GetCostPercent(itemPrice, cost)
        {
            var value = 0;
            value = ((itemPrice - cost) / itemPrice) * 100;
            value = Math.floor(value.toPrecision(12) * Math.pow(10, (2 || 0))) / Math.pow(10, (2 || 0));
            return value;
        }
        

        //從毛利率回推進貨價
        <%--function changeFromGross(ele) {
            var tr = $(ele).closest('tr');
            var itemPrice = tr.find('input#txtItemPrice');
            var _itemPrice = itemPrice.val();
            var grossMargin = tr.find("input#txtGrossMargin");
            var cost = tr.find('input#txtCost');
            var slottingFeeQuantity = tr.find('input#txtSlottingFeeQuantity');
            var orderTotalLimit = tr.find('input#txtOrderTotalLimit');
            var cpcBuy = tr.find('input#txtCPCBuy');
            var cpcPresent = tr.find('input#txtCPCPresent');

            var _cost = "";
            if (isNaN($.trim(itemPrice.val())) || $.trim(itemPrice.val()) == 0) {

            } else if (isNaN($.trim(grossMargin.val())) || $.trim(grossMargin.val()) == 0) {

            } else {


                //成套票券要算上買、送份數
                if ($('#chkGroupCoupon').is(':checked'))
                {
                    _itemPrice = _itemPrice * cpcBuy.val();
                }

                if ("<%= Helper.IsFlagSet(Convert.ToInt32(hidCreatedFlag.Value),(int)ProposalBusinessCreateFlag.Created)%>" == "False") {

                    //還在提案單(尚未建檔)

                    //售價都要/1.05
                    _itemPrice = _itemPrice / 1.05;

                }
                else if ("<%= Helper.IsFlagSet(Convert.ToInt32(hidCreatedFlag.Value),(int)ProposalBusinessCreateFlag.Created)%>" == "True") {

                    //已建檔

                    //開立免稅發票給消費者影響售價
                    if ("<%= Helper.IsFlagSet(Convert.ToInt32(hidGroupOrder.Value),(int)GroupOrderStatus.NoTax)%>" == "False")
                    {
                        _itemPrice = _itemPrice / 1.05;
                    }

                }


                if (slottingFeeQuantity.val() != 0)
                {
	                //上架費算法
                    _cost = _itemPrice == 0 ? 0 : ((grossMargin.val() / 100 * (_itemPrice * orderTotalLimit.val())) - (_itemPrice * orderTotalLimit.val())) / (orderTotalLimit.val() - slottingFeeQuantity.val());
                }
                else
                {
	                //一般算法 or 成套票券算法
                    _cost = _itemPrice == 0 ? 0 : (grossMargin.val() / 100 * _itemPrice) - _itemPrice;

                    
                }


                //成套票券要算上買、送份數
                if ($('#chkGroupCoupon').is(':checked')) {
                    _cost = _cost / (parseFloat(cpcBuy.val()) + parseFloat(cpcPresent.val()));
                }


                if ("<%= Helper.IsFlagSet(Convert.ToInt32(hidCreatedFlag.Value),(int)ProposalBusinessCreateFlag.Created)%>" == "False")
                {
                    //還在提案單(尚未建檔)

                    //統一發票(含稅)，才除以1.05
                    if ($('#<%=rbRecordInvoice.ClientID%>').is(':checked') || $('#<%=rbRecordOthers.ClientID%>').is(':checked'))
                    {
                        _cost = _cost * 1.05;
                    }
                }
                else if ("<%= Helper.IsFlagSet(Convert.ToInt32(hidCreatedFlag.Value),(int)ProposalBusinessCreateFlag.Created)%>" == "True") 
                {
                    //已建檔

                    //店家發票免稅影響進價
                    if (($('#hidRecord').val() == "2" && $('#hidInputTax').val() == "True") || $('#hidRecord').val() == "0")
                    {
                        _cost = _cost * 1.05;
                    }

                }

                _cost = Math.abs(_cost);
            }

            cost.val(Math.round(_cost));
            cost.css({ 'background-color': '' });
            grossMargin.css({ 'background-color': '' });
            if (cost != "") {
                cost.css({ 'background-color': '#DFD8D1' });
            }            
        }--%>

        /*
        * 檢查優惠資料輸入是否正確
        */
        function checkDealData(isDelete, itemName, itemNameTravel, itemPrice, origPrice, origPrice, cost, costpercent, orderTotalLimit, orderMaxPersonal, CPCBuy, CPCPresent, slottingFeeQuantity, atmMaximum) {
            var message = "";
            if (isDelete) {
                if ($.trim(itemName) == '') {
                }
            }

            if ($("#ddlDealType").val() == "<%=((int)ProposalDealType.Travel)%>") {
                //旅遊
                if ($.trim(itemNameTravel) == '') {
                    message += "請輸入專案內容\r\n";
                }
            }

            if ($.trim(itemName) == '' && $.trim(itemNameTravel) == '') {
                message += "請輸入專案內容\r\n";
            }

            if (itemName.length >= 400 || itemNameTravel.length >=400) {
                message += "專案內容字數請勿超過400字元\r\n";
            }

            if ($.trim(itemPrice) == '' || $.trim(itemPrice) == '0' || isNaN($.trim(itemPrice)) || !ValidateNumber(itemPrice)) {
                message += "請輸入售價或不允許輸入非整數\r\n";
            }

            if ($.trim(origPrice) == '' || $.trim(origPrice) == '0' || isNaN($.trim(origPrice)) || !ValidateNumber(origPrice)) {
                message += "請輸入原價或不允許輸入非整數\r\n";
            }

            if ($.trim(slottingFeeQuantity) == '' || isNaN($.trim(slottingFeeQuantity)) || !ValidateNumber(slottingFeeQuantity)) {
                message += "請輸入上架費份數或不允許輸入非整數\r\n";
            }

            if ($.trim(cost) == '' || isNaN($.trim(cost)) || !ValidateNumber(cost)) {
                message += "請輸入進貨價或不允許輸入非整數\r\n";
            }

            if ($.trim(costpercent) != '' && isNaN($.trim(costpercent))) {
                message += "進貨價百分比不允許輸入非整數\r\n";
            }

            if (($.trim(slottingFeeQuantity) != '' && $.trim(slottingFeeQuantity) != '0') && ($.trim(cost) == '' || $.trim(cost) == '0')) {
                message += "有上架費份數，進貨價不可為0\r\n";
            }

            if ($.trim(orderTotalLimit) == '' || isNaN($.trim(orderTotalLimit)) || !ValidateNumber(orderTotalLimit)) {
                message += "請輸入最大購買量或不允許輸入非整數\r\n";
            }

            if ($.trim(atmMaximum) == '' || isNaN($.trim(atmMaximum)) || !ValidateNumber(atmMaximum)) {
                message += "請輸入ATM保留份數或不允許輸入非整數\r\n";
            }
            

            if ($.trim(orderMaxPersonal) == '' || isNaN($.trim(orderMaxPersonal)) || !ValidateNumber(orderMaxPersonal)) {
                message += "請輸入每人限購數量或不允許輸入非整數\r\n";
            }

            
            if ($("#chkGroupCoupon").attr("checked") == "checked") {
                if ($('#ddlGroupCouponType option:selected').val() == '0') {
                    message += "成套票券檔次沒有選擇模式，請再次確認後進行送件。\r\n";
                }
                if ($.trim(CPCBuy) == '' || $.trim(CPCPresent) == ''
                    || $.trim(CPCBuy) == '0' || isNaN($.trim(CPCBuy))
                    || isNaN($.trim(CPCPresent))
                    ) {
                    message += "請輸入憑證組數，不可等於0\r\n";
                }
                if ($.trim(cost) % (parseInt($.trim(CPCBuy)) + parseInt($.trim(CPCPresent))) > 0) {
                    message += "成套票券檔次進貨價(" + $.trim(cost) + ")必須能整除組數(" + (parseInt($.trim(CPCBuy)) + parseInt($.trim(CPCPresent))) + ")";
                }
                if ($('#ddlGroupCouponType option:selected').val() == '1' && parseInt($.trim(itemPrice)) % parseInt($.trim(CPCBuy)) > 0) {
                    message += "成套票券A型售價(" + $.trim(itemPrice) + ")必須能整除買數(" + $.trim(CPCBuy) + ")";
                }
                if ($('#ddlGroupCouponType option:selected').val() == '2' && parseInt($.trim(origPrice)) % (parseInt($.trim(CPCBuy)) + parseInt($.trim(CPCPresent))) > 0) {
                    message += "成套票券B型原價(" + $.trim(origPrice) + ")必須能整除組數(" + (parseInt($.trim(CPCBuy)) + parseInt($.trim(CPCPresent))) + ")";
                }
                if ($('#ddlGroupCouponType option:selected').val() == '2' && parseInt($.trim(itemPrice)) / parseInt($.trim(CPCBuy)) > parseInt($.trim(origPrice)) / (parseInt($.trim(CPCBuy)) + parseInt($.trim(CPCPresent)))) {
                    message += "成套票券B型單張售價不可高於原價";
                }
            }
            return message;
        }
        function saveProposalMultiDeal(ele, dealData, isDelete, isCopy, itemName, itemPrice) {
            $.ajax({
                type: "POST",
                url: "ProposalContent.aspx/ProposalMiltiDealsSave",
                data: "{'ProposalId':'<%= ProposalId %>' , 'UserName':'<%= UserName %>' ,'data': '" + JSON.stringify(dealData) + "','isDelete': '" + isDelete + "','isCopy': '" + isCopy + "','itemName':'" + itemName + "','itemPrice':'" + itemPrice + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var res = response.d;
                    multiDealBind(res);

                }, error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        }


        //Bind優惠內容
        function multiDealBind(res) {
            var table = $("#proposalContentTable");
            $('#proposalContentTable tr:not(:first)').remove();
            var _html = "";
            Deals = [];
            $.each(res, function (idx, value) {

                idx = idx + 1 ;
                _html += '<tr>';
                _html += '<td style="max-width: 300px;">';

                //方案內容
                _html += '<span class="view">' + (value.OrigPrice > 0 ? roundX(value.ItemPrice / value.OrigPrice * 10, 1) + '折' : "") + '</span>';
                _html += '   <span class="ItemName view qcedit">' + value.ItemName + '</span>';
                if (value.ItemName != "") {
                    _html += "<br />";
                }

                //方案說明
                if ($("#ddlDealType").val() == "<%=((int)ProposalDealType.Travel)%>") {
                    _html += '   <span class="ItemNameTravel view qcedit">' + value.ItemNameTravel + '</span>';
                } else {
                    _html += '   <span class="ItemNameTravel view qcedit">' + value.ItemNameTravel + '</span>';
                }
                if (value.ItemNameTravel != "") {
                    _html += "<br />";
                }

                _html += '   <input class="DealId" type="hidden" value="' + value.Id + '" /><input class="Sort" type="hidden" value="' + idx + '" />';
                if (value.UniqueKey == "") {
                    value.UniqueKey = guid();
                }
                _html += '   <input class="UniqueKey" type="hidden" value="' + value.UniqueKey + '" />';

                //方案名稱
                if ($("#ddlDealType").val() == "<%=((int)ProposalDealType.Travel)%>") {
                    _html += '   <textarea rows="2" cols="15" id="txtItemName" class="input-half edit qcedit" placeholder="方案名稱： 商家名稱 - 方案名稱">' + value.ItemName + '</textarea>';
                } else {
                    _html += '   <textarea rows="2" cols="15" id="txtItemName" class="input-half edit qcview" placeholder="方案名稱： 商家名稱 - 方案名稱">' + value.ItemName + '</textarea>';
                }


                //方案說明
                if ($("#ddlDealType").val() == "<%=((int)ProposalDealType.Travel)%>" || $("#ddlDealType").val() == "<%=((int)ProposalDealType.LocalDeal)%>" || $("#ddlDealType").val() == "<%=((int)ProposalDealType.PBeauty)%>") {
                    _html += '   <textarea rows="2" cols="15" id="txtItemNameTravel" class="input-half edit qcview" placeholder="方案說明">' + value.ItemNameTravel + '</textarea>';
                } else {
                    _html += '   <textarea rows="2" cols="15" id="txtItemNameTravel" class="input-half " placeholder="方案說明" style="display:none">' + value.ItemNameTravel + '</textarea>';
                }

                _html += '   </td>';

                //原價
                _html += '   <td><span class="OrigPrice view qcedit">' + value.OrigPrice + '</span><input type="text" id="txtOrigPrice" class="input-half edit qcview" size="3" maxlength="6" value="' + value.OrigPrice + '"/></td>';

                //售價
                _html += '   <td><span class="ItemPrice view qcedit">' + value.ItemPrice + '</span><input type="text" id="txtItemPrice" class="input-small edit qcview" size="3" maxlength="6" value="' + value.ItemPrice + '" onkeyup="changeGross(this)" /></td>';
                

                //有無勾選成套票券
                if ($("#chkGroupCoupon").attr("checked") == "checked") {
                    _html += '   <td class=""><span class="CPCData view qcedit"><span class="CPCBuy view">買' + value.GroupCouponBuy + '送' + value.GroupCouponPresent + '</span></span><span class="CPCData edit">買<input type="text" id="txtCPCBuy" class="input-small qcview" size="2" maxlength="6" value="' + (value.GroupCouponBuy == "" ? "0" : value.GroupCouponBuy) + '" onkeyup="changeGross(this)" /><br/>送<input type="text" id="txtCPCPresent" class="input-small " size="2" maxlength="6" value="' + (value.GroupCouponPresent == "" ? "0" : value.GroupCouponPresent) + '" onkeyup="changeGross(this)" /></span></td>';
                } else {
                    _html += '   <td class="CPC"><span class="CPCData edit">買<input type="text" id="txtCPCBuy" class="input-small " size="2" maxlength="6" value="0" onkeyup="changeGross(this)"/><br/>送<input type="text" id="txtCPCPresent" class="input-small qcview" size="2" maxlength="6" value="0" onkeyup="changeGross(this)" /></span></td>';
                }

                //獲利毛利率
                _html += '   <td style="color: #BF0000"><span class="GrossMargin view qcedit">' + GetGrossMargin(value.ItemPrice, value.Cost, value.SlottingFeeQuantity, value.OrderTotalLimit, value.GroupCouponBuy, value.GroupCouponPresent) + '</span>' +
                                                        '<input type="text" id="txtGrossMargin" class="input-half edit qcview" size="3" maxlength="6" value="" onkeyup="changeFromGross(this)" value="" disabled style="background-color:#DFD8D1"/></td>';

                //洽談毛利率
                //進貨價
                _html += '   <td>';
                
                _html += '<span class="CostPercent view qcedit">' + GetCostPercent(value.ItemPrice, value.Cost) + '%</span><br />';
                _html +='<span class="Cost view qcedit">' + value.Cost + '</span>' +
                    '<input type="text" id="txtCostPercent" class="input-small edit qcview" size="2" maxlength="6" value="' + GetCostPercent(value.ItemPrice, value.Cost) + '" onblur="GetCost(this)" /><span class="input-small edit qcview">%</span><br />' +
                    '<input type="text" id="txtCost" class="input-small edit qcview" size="4" maxlength="6" value="' + value.Cost + '" onkeyup="changeGross(this)" /></td>';
                
                //上架費
                _html += '   <td><span class="SlottingFeeQuantity view qcedit">' + value.SlottingFeeQuantity + '</span><input type="text" id="txtSlottingFeeQuantity" class="input-small edit qcview" size="3" maxlength="6" value="' + (value.SlottingFeeQuantity == "" ? "0" : value.SlottingFeeQuantity) + '" onkeyup="changeGross(this)" /></td>';
                
                //最大購買
                if ($("#ddlDeliveryType").val() == '<%= (int)DeliveryType.ToHouse %>') {
                    _html += '   <td><span class="OrderTotalLimit view qcview">' + value.OrderTotalLimit + '</span><input type="text" id="txtOrderTotalLimit" class="input-small edit qcedit" size="3" maxlength="6" value="' + value.OrderTotalLimit + '" onkeyup="calculateAtmMaximun(this)"   /></td>';
                }else{
                    _html += '   <td><span class="OrderTotalLimit view qcedit">' + value.OrderTotalLimit + '</span><input type="text" id="txtOrderTotalLimit" class="input-small edit qcview" size="3" maxlength="6" value="' + value.OrderTotalLimit + '" onkeyup="calculateAtmMaximun(this)"   /></td>';
                }

                //每人限購
                _html += '   <td><span class="OrderMaxPersonal view qcview">' + value.OrderMaxPersonal + '</span><input type="text" id="txtOrderMaxPersonal" class="input-small edit qcedit" size="3" maxlength="6" value="' + value.OrderMaxPersonal + '"  /></td>';

                //ATM保留
                _html += '   <td><span class="AtmMaximum view qcview">' + value.AtmMaximum + '</span><input type="text" id="txtAtmMaximum" class="input-small edit qcedit" size="4" maxlength="6" value="' + value.AtmMaximum + '"  /></td>';

                if ($("#ddlDeliveryType").val() == '<%= (int)DeliveryType.ToHouse %>') {
                    //宅配
                    if ($("#hidShoppingCartV2Enabled").val() == "1") {
                        //購物車啟用
                        _html += '   <td class="product shoppingCartCfgHide" style="display:none"><span class="Freights view qcview">' + value.Freights + '</span><input type="text" id="txtFreights" class="input-small edit qcedit" size="3" maxlength="6" value="' + (value.Freights == "" ? "0" : value.Freights) + '" onblur="calcNoFreightAmt(this)" /></td>';
                        _html += '   <td class="product shoppingCartCfgHide" style="display:none"><span class="NoFreightLimit view qcview">' + value.NoFreightLimit + '</span><input type="text" id="txtNoFreightLimit" class="input-small edit qcedit" size="3" maxlength="6" value="' + (value.NoFreightLimit == "" ? "0" : value.NoFreightLimit) + '" onblur="calcNoFreightAmt(this)" /></td>';
                        _html += '   <td class="product shoppingCartCfgShow" style="text-align:left"><span class="FreightsId view qcview" style="display:none">' + value.FreightsId + '</span>';
                    }
                    else {
                        _html += '   <td class="product shoppingCartCfgHide"><span class="Freights view qcview">' + value.Freights + '</span><input type="text" id="txtFreights" class="input-small edit qcedit" size="3" maxlength="6" value="' + (value.Freights == "" ? "0" : value.Freights) + '" onblur="calcNoFreightAmt(this)" /></td>';
                        _html += '   <td class="product shoppingCartCfgHide"><span class="NoFreightLimit view qcview">' + value.NoFreightLimit + '</span><input type="text" id="txtNoFreightLimit" class="input-small edit qcedit" size="3" maxlength="6" value="' + (value.NoFreightLimit == "" ? "0" : value.NoFreightLimit) + '" onblur="calcNoFreightAmt(this)" /></td>';
                        _html += '   <td class="product shoppingCartCfgShow" style="display:none"><span class="FreightsId view qcview">' + value.FreightsId + '</span>';
                    }
                    
                    var items = eval($("#hidShoppingCartFreightsItem").val());
                    _html += "<span class='edit'>";
                    $.each(items, function (itemId, itemVal) {
                        if (value.FreightsId == itemVal.FreightId) {
                            _html += "<input type='radio' id='FreightsId' name='FreightsId" + value.Id + "' value='" + itemVal.FreightId + "' checked='checked' />" + itemVal.FreightName + "<br />";
                        } else {
                            _html += "<input type='radio' id='FreightsId' name='FreightsId" + value.Id + "' value='" + itemVal.FreightId + "' />" + itemVal.FreightName + "<br />";
                        }                        
                    });
                    _html += "</span>";
                    $.each(items, function (itemId, itemVal) {
                        if (value.FreightsId == itemVal.FreightId) {
                            _html += "<span class='view'>" + itemVal.FreightName + "</span>";
                        }
                    });
                    
                    _html += '   </td>';
                } else {
                    //憑證
                    _html += '   <td class="product shoppingCartCfgHide" style="display:none"><span class="Freights view qcview"></span><input type="text" id="txtFreights" class="input-small edit qcedit" size="3" maxlength="6" value="0" onblur="calcNoFreightAmt()" /></td>';
                    _html += '   <td class="product shoppingCartCfgHide" style="display:none"><span class="NoFreightLimit view qcview"></span><input type="text" id="txtNoFreightLimit edit" class="input-small edit qcedit" size="3" maxlength="6" value="0" onblur="calcNoFreightAmt()" /></td>';
                    _html += '   <td class="product shoppingCartCfgShow" style="display:none"><span class="FreightsId view qcview">' + value.FreightsId + '</span>';
                    var items = eval($("#hidShoppingCartFreightsItem").val());
                    $.each(items, function (itemId, itemVal) {
                        _html += "<input type='radio' id='FreightsId' name='FreightsId' value='" + itemVal.FreightId + "' />" + itemVal.FreightName;
                    });
                    _html += '   </td>';
                }

                //多重選項
                _html += '<td  style="max-width: 150px;">';
                _html += '<span class="Options view qcview">' + value.Options + '</span><textarea rows="2" cols="15" id="txtOptions" class="edit qcedit">' + value.Options + '</textarea>';

                //功能鈕
                if (value.SellerOptions != "") {
                    _html += '<img src="/Themes/default/images/17Life/B5/b_search.jpg" onclick="openOptionsPopup(' + value.Id + ')" /><textarea rows="2" cols="15" id="txtSellerOptions" style="display:none">' + value.SellerOptions + '</textarea>';
                }
                _html += '</td>';
                // if ($("#txtBrandName").attr("disabled") != "disabled") {
                if (($("#hidApplyFlag").val() == "0" || $("#hidCreatedFlag").val() == "0") && $("#hidMultiDealsUpdate").val() == "1" && ( $("#hidSellerCheckFlag").val() == "1")) {
                    _html += '   <td class="DealEditStyle" style="width:100px">';
                    _html += '   <img src="/Themes/PCweb/images/Tick.png" alt="儲存" title="儲存" style="padding-left:5px" class="edit" onclick="saveChanges(this,false,false);"  />';
                    _html += '   <img src="../Themes/default/images/17Life/G2/undo.png" alt="取消編輯" title="取消編輯" style="padding-left:5px;" class="edit" onclick="cancelChanges(this);"  />';
                    _html += '   <img src="../Themes/default/images/17Life/G2/edit.png" alt="編輯" title="編輯" style="padding-left:5px;" class="view" onclick="changeToEditMode(this);"  />';
                    _html += '   <img src="../Themes/default/images/17Life/G2/delete.png" alt="刪除" title="刪除" style="padding-left:5px;" class="view" onclick="deleteChanges(this);"  />';
                    _html += '   <img src="../Themes/default/images/17Life/G2/copy_icon.png" alt="複製" title="複製" style="padding-left:5px;" class="view" onclick="copyChanges(this);"  />';
                    if ($('#chkGroupCoupon').attr('checked') == 'checked') {
                        _html += '   <img src="../Themes/default/images/17Life/G2/safely_code_icon.png" alt="檢視" title="檢視" style="padding-left:5px;" class="view" onclick="previewCPC(this);"  />';
                    }
                    _html += '   </td>';
                }
                _html += '</tr>';

                addDeals(value.UniqueKey, value.Id, idx, value.ItemName, value.ItemNameTravel, value.OrigPrice, value.ItemPrice, value.GroupCouponBuy, value.GroupCouponPresent, value.Cost, value.SlottingFeeQuantity,
                    value.OrderTotalLimit, value.OrderMaxPersonal, value.AtmMaximum, value.Freights, value.NoFreightLimit, value.Options, value.SellerOptions, value.FreightsId);
                
            });
            table.append(_html);

            SwitchSpecialRequirement(res.length);
        }

        function calcNoFreightAmt() {

        }

        function roundX(val, precision) {
            var precision = precision || 0; //預設0位數         
            return (Math.floor((val * Math.pow(10, (precision || 0)) + 0.5))) / Math.pow(10, (precision || 0));
        }


        function addDeals(UniqueKey, Id, Sort, ItemName, ItemNameTravel, OrigPrice, ItemPrice, GroupCouponBuy, GroupCouponPresent, Cost, SlottingFeeQuantity, OrderTotalLimit, OrderMaxPersonal, AtmMaximum, Freights, NoFreightLimit, Options, SellerOptions, FreightsId) {
            var ProposalMultiDealModel = {
                Id: Id,
                Sort: Sort,
                ItemName: ItemName,
                ItemNameTravel: ItemNameTravel,
                OrigPrice: OrigPrice,
                ItemPrice: ItemPrice,
                CPCBuy: GroupCouponBuy,
                CPCPresent: GroupCouponPresent,
                Cost: Cost,
                SlottingFeeQuantity: SlottingFeeQuantity,
                OrderTotalLimit: OrderTotalLimit,
                OrderMaxPersonal: OrderMaxPersonal,
                AtmMaximum: AtmMaximum,
                Freights: Freights,
                NoFreightLimit: NoFreightLimit,
                Options: Options,
                SellerOptions: SellerOptions,
                FreightsId: FreightsId,
                UniqueKey: UniqueKey                
            }
            Deals.push(ProposalMultiDealModel);

            return ProposalMultiDealModel;
        }

        

        function addTextbox()
        {
            var cobject = $("#hidMediaReport");
            var c = cobject.val();
            $("#divMediaReportLink" + c).show();
            
            var count = Number(c) + 1;
            cobject.val(count);

            if (c == 5)
                $("#imgMediaReportAdd").hide();
            else
                $("#imgMediaReportAdd").show();
        }

        function DeleteTextbox(img)
        {
            var cobject = $("#hidMediaReport");
            var c = cobject.val();
            $("#divMediaReportLink" + (c - 1)).hide();
            var count = Number(c) -1;
            cobject.val(count);

            if (count == 6)
                $("#imgMediaReportAdd").hide();
            else
                $("#imgMediaReportAdd").show();
        }

        //新增一列優惠內容
        function addChanges() {
            var __id = $('#proposalContentTable tr:not(":has(th)")').length + 1;
            var table = $("#proposalContentTable");
            var _html = '<tr>';
            _html += '<td><span class="ItemName view"></span>';
            _html += '       <input class="DealId" type="hidden" value="0" /><input class="Sort" type="hidden" value="' + __id + '" />';
            _html += '   <input class="UniqueKey" type="hidden" value="' + guid() + '" />';
            _html += '       <textarea rows="2" cols="15" id="txtItemName" class="input-half" placeholder="方案名稱： 商家名稱 - 方案名稱"></textarea>';
            if ($("#ddlDealType").val() == "<%=((int)ProposalDealType.Travel)%>" || $("#ddlDealType").val() == "<%=((int)ProposalDealType.LocalDeal)%>" || $("#ddlDealType").val() == "<%=((int)ProposalDealType.PBeauty)%>") {
                //旅遊
                _html += '       <textarea rows="2" cols="15" id="txtItemNameTravel" class="input-half" placeholder="方案說明"></textarea>';
            } else {
                _html += '       <textarea rows="2" cols="15" id="txtItemNameTravel" class="input-half travel" placeholder="方案說明" style="display:none"></textarea>';
            }
            
            
            _html += '   </td>';
            _html += '   <td><span class="OrigPrice view"></span><input type="text" id="txtOrigPrice" class="input-half " size="3" maxlength="6" value="" /></td>';
            _html += '   <td><span class="ItemPrice view"></span><input type="text" id="txtItemPrice" class="input-small " size="3" maxlength="6" value="" onkeyup="changeGross(this)"  /></td>';
            _html += '   <td class="CPC"><span class="CPCData view"></span>買<input type="text" id="txtCPCBuy" class="input-small " size="2" maxlength="6" value="0" onkeyup="changeGross(this)" /><br/>送<input type="text" id="txtCPCPresent" class="input-small " size="2" maxlength="6" value="0" onkeyup="changeGross(this)" /></span></td>';
            _html += '   <td><span class="GrossMargin view"></span><input type="text" id="txtGrossMargin" class="input-half " size="3" maxlength="6" value="" onkeyup="changeFromGross(this)" disabled style="background-color:#DFD8D1" /></td>';
            _html += '   <td><span class="Cost view"></span><input type="text" id="txtCostPercent" class="input-small " size="2" maxlength="6" value=""  onblur="GetCost(this)" />%<br />' +
                                                           '<input type="text" id="txtCost" class="input-small " size="4" maxlength="6" value=""  onkeyup="changeGross(this)" /></td>';
            _html += '   <td><span class="SlottingFeeQuantity view"></span><input type="text" id="txtSlottingFeeQuantity" class="input-small " size="3" maxlength="6" value="0" onkeyup="changeGross(this)" /></td>';
            _html += '   <td><span class="OrderTotalLimit view"></span><input type="text" id="txtOrderTotalLimit" class="input-small " size="3" maxlength="6" value="" onkeyup="calculateAtmMaximun(this)"  /></td>';
            _html += '   <td><span class="OrderMaxPersonal view"></span><input type="text" id="txtOrderMaxPersonal" class="input-small " size="3" maxlength="6" value="" /></td>';//每人限購
            _html += '   <td><span class="AtmMaximum view"></span><input type="text" id="txtAtmMaximum" class="input-small " size="3" maxlength="6" value=""  /></td>';
            if ($("#ddlDeliveryType").val() == '<%= (int)DeliveryType.ToHouse %>') {
                //宅配
                if ($("#hidShoppingCartV2Enabled").val() != "1") {
                    //購物車未啟用
                    _html += '   <td class="product shoppingCartCfgHide"><span class="Freights view"></span><input type="text" id="txtFreights" class="input-small " size="3" maxlength="6" value="0" onblur="calcNoFreightAmt(this)" /></td>';
                    _html += '   <td class="product shoppingCartCfgHide"><span class="NoFreightLimit view"></span><input type="text" id="txtNoFreightLimit" class="input-small " size="3" maxlength="6" value="0" /></td>';
                } else {
                    //購物車啟用
                    _html += '   <td class="product shoppingCartCfgShow" style="text-align:left"><span class="FreightsId view qcview"></span>';
                    var items = eval($("#hidShoppingCartFreightsItem").val());
                    var fidx = 0;
                    $.each(items, function (itemId, itemVal) {
                        if (fidx == 0) {
                            _html += "<input type='radio' id='FreightsId' name='FreightsId" + __id + "' value='" + itemVal.FreightId + "' checked='checked' />" + itemVal.FreightName + "<br />";
                        } else {
                            _html += "<input type='radio' id='FreightsId' name='FreightsId" + __id + "' value='" + itemVal.FreightId + "' />" + itemVal.FreightName + "<br />";
                        }                        
                    });
                    _html += ' </td>';
                }                                
            } else {
                //憑證
                _html += '   <td class="product shoppingCartCfgHide" style="display:none"><span class="Freights view"></span><input type="text" id="txtFreights" class="input-small " size="3" maxlength="6" value="0" onblur="calcNoFreightAmt(this)" /></td>';
                _html += '   <td class="product shoppingCartCfgHide" style="display:none"><span class="NoFreightLimit view"></span><input type="text" id="txtNoFreightLimit" class="input-small " size="3" maxlength="6" value="0" /></td>';
                _html += '   <td class="product shoppingCartCfgShow" style="display:none"><span class="FreightsId view qcview"></span>';
                var items = eval($("#hidShoppingCartFreightsItem").val());
                $.each(items, function (itemId, itemVal) {
                    _html += "<input type='radio' id='FreightsId' name='FreightsId' value='" + itemVal.FreightId + "' />" + itemVal.FreightName;
                });
                _html += '   </td>';
            }
            _html += '   <td><span class="Options view"></span><textarea rows="2" cols="15" id="txtOptions" class=""></textarea></td>';
            _html += '   <td style="width:100px">';
            _html += '   <span><img id="imgSave" src="/Themes/PCweb/images/Tick.png" alt="儲存" title="儲存" style="padding-left:5px" class="view" onclick="saveChanges(this,false,false);"  /></span>';
            //_html += '   <span><img id="imgCancel" src="../Images/icons/undo21.png" alt="取消編輯" title="取消編輯" style="padding-left:5px;" class="edit" onclick="cancelChanges(this);"  /></span>';
            //_html += '   <span><img id="imgEdit" src="../Images/icons/pens5.png" alt="編輯" title="編輯" style="padding-left:5px;" class="edit" onclick="changeToEditMode(this);"  /></span>';
            _html += '   <span><img id="imgDelete" src="../Themes/default/images/17Life/G2/delete.png" alt="刪除" title="刪除" style="padding-left:5px;" class="view" onclick="deleteChanges(this);"  /></span>';
            _html += '</td>';
            _html += '</tr>';

            table.append(_html);

            CPCShowOrHide();
        }

        function CPCShowOrHide() {
            if ($("#chkGroupCoupon").attr("checked") == "checked") {
                $(".CPC").show();
            } else {
                $(".CPC").hide();
            }
        }

        /*
        * 計算ATM最大購買數量
        */
        function calculateAtmMaximun(obj) {
            var tr = $(obj).closest('tr');
            var txtAtmMaximum = tr.find('input#txtAtmMaximum');
            var c = parseInt($(obj).val());
            if (c != 0) {
                txtAtmMaximum.val(Math.floor(c / 5));//20%
            }

            //順便調整毛利率
            changeGross(obj);
            
        }

        function chkCreateBusinessHour() {
            if ($("#chkPhotographers").is(":checked") == false && $("#chkSellerPhoto").is(":checked") == false && $("#chkFTPPhoto").is(":checked") == false && $("#chkSystemPic").is(":checked") == false) {
                alert("至少選擇一項照片來源");
                return false;
            }
           
        }

        

        function savePaperContractNotCheckReason() {

            var reason = '';
            $('.chkReason').each(function(){
                if ($(this).find('input').is(':checked'))
                {
                    if ($(this).find('label').text() == '其他：')
                        reason = $(this).next().val();
                    else
                        reason = $(this).find('label').text();
                }
            });

            if (reason == '')
            {
                $('#pPaperContractNotCheckReason').show()
                $('#pPaperContractNotCheckReason').parent().parent().addClass('error');
                return false;
            }

                
            $.ajax({
                type: "POST",
                url: "ProposalContent.aspx/PaperContractNotCheckReasonSave",
                data: "{'proposalid':'<%=ProposalId%>','username':'<%=UserName%>','Reason':'" + reason + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    window.parent.$.fancybox.close();
                    location.href = location.protocol + "//" + location.host + location.pathname + "?pid=<%=ProposalId%>";
                }, error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        }

        //約拍小幫手-儲存
        function onlySavePhotographer() {
            onlySaveSate = true;
            savePhotographer();
        }

        //約拍小幫手-儲存+送出
        function savePhotographer() {
            var txtPhotoAddress = $("#txtPhotoAddress").val();  //拍攝地點
            var txtPhotoContact = $("#txtPhotoContact").val();  //現場聯絡人姓名
            var radMale = $("#radMale").is(":checked");         //先生
            var radFemale = $("#radFemale").is(":checked");     //小姐
            var txtPhotoTel = $("#txtPhotoTel").val();          //現場聯絡人電話
            var txtPhotoPhone = $("#txtPhotoPhone").val();      //現場聯絡人手機
            var txtPhotoMemo = $("#txtPhotoMemo").val();        //建議重點拍攝項目
            var txtPhotoDate = $("#txtPhotoDate").val();        //店家希望拍照時間
            var txtPhotoUrl = $("#txtPhotoUrl").val();          //參考網址 / 資料

            txtPhotoMemo = txtPhotoMemo.replace("\"", "").replace("<", "").replace(">", "");
            txtPhotoUrl = txtPhotoUrl.replace("\"", "").replace("<", "").replace(">", "");

            var txtPhohoNeeds = $("#txtPhohoNeeds").val();      //拍攝需求
            var txtRefrenceData = $("#txtRefrenceData").val();  //參考資料


            if ($("#ddlDeliveryType").val() == '<%= (int)DeliveryType.ToShop %>' || ($("#ddlDeliveryType").val() == '<%= (int)DeliveryType.ToHouse %>' && $("#ddlDealType").val() == '<%= (int)ProposalDealType.Travel %>')) {
                
                //憑證+旅遊宅配
                if ($.trim(txtPhotoAddress) == "") {
                    alert("請輸入拍攝地點!");
                    return false;
                }
                if ($.trim(txtPhotoContact) == "") {
                    alert("請輸入現場聯絡人姓名!");
                    return false;
                }
                if ((!radMale && !radFemale)) {
                    alert("請選擇性別!");
                    return false;
                }
                if ($.trim(txtPhotoPhone) == "") {
                    alert("請輸入現場聯絡人手機!");
                    return false;
                }
                if ($.trim(txtPhotoMemo) == "") {
                    alert("請輸入建議重點拍攝項目!");
                    return false;
                }
            }
            
            
            var Sex = radMale ? "M" : "F";
            
            var postData = {};
            postData.ProposalId =  "<%= ProposalId %>";
            postData.deliveryType = $("#ddlDeliveryType").val();
            postData.dealType = $("#ddlDealType").val();
            postData.PhotoAddress = txtPhotoAddress;
            postData.PhotoContact = txtPhotoContact;
            postData.Sex = Sex;
            postData.PhotoTel = txtPhotoTel;
            postData.PhotoPhone = txtPhotoPhone;
            postData.PhotoMemo = txtPhotoMemo;
            postData.PhotoDate = txtPhotoDate;
            postData.PhotoUrl = txtPhotoUrl;
            postData.PhohoNeeds = txtPhohoNeeds;
            postData.RefrenceData = txtRefrenceData;
            postData.UserName = "<%= UserName%>";
            postData.onlySaveSate = onlySaveSate;
            postDataJsonFormat = JSON.stringify(postData);

            $.ajax({
                type: "POST",
                url: "ProposalContent.aspx/photoSave",
                <%-- data: "{'ProposalId':'<%= ProposalId %>' ,'deliveryType':'" + $("#ddlDeliveryType").val() + "','dealType':'" + $("#ddlDealType").val() + "'," +
                        "'PhotoAddress':'" + txtPhotoAddress + "','PhotoContact':'" + txtPhotoContact + "'," +
                        "'Sex':'" + Sex + "','PhotoTel':'" + txtPhotoTel + "','PhotoPhone':'" + txtPhotoPhone + "'," +
                        "'PhotoMemo':'" + txtPhotoMemo + "','PhotoDate':'" + txtPhotoDate + "','PhotoUrl':'" + txtPhotoUrl + "'," +
                        "'PhohoNeeds':'" + txtPhohoNeeds + "','RefrenceData':'" + txtRefrenceData  + "','onlySaveSate':'" + onlySaveSate + "', 'UserName':'<%= UserName %>'" +
                        "}",--%>
                data: postDataJsonFormat,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var res = response.d;
                    if (res != null) {
                        if (res) {
                            if (onlySaveSate) {
                                alert("儲存成功!");
                            } else {
                                alert("約拍成功!");
                                $("#hidPhotographerAppoint").attr("checked", "checked");

                                //需有攝影權限的人才看的到 且狀態不為儲存
                                if ($("#hidPhotographerPri").val() == "1") {
                                    $("#photographerAppoint").show();
                                }
                            }  
                        }
                    } 
                    window.parent.$.fancybox.close();
                }, error: function (xhr) {
                    console.log(xhr.responseText);
                }       
            });
        }

        function chkChangeSellerConfirm() {
            if (!confirm("確定要變更賣家嗎?")) {
                return false;
            }
        }


        function editPhotographer() {
            enabledPhoto(true);
        }
        function cancelPhotographer() {
            ProposalSpecialTextGet();
        }

        function enabledPhoto(f) {

            var txtPhotographerAppointDate = $("#txtPhotographerAppointDate").val();
            if ($.trim(txtPhotographerAppointDate) != "") 
            {
                //拍攝結案不得再修改
                if ($("#hidPhotographerAppointClose").is(":checked")) {
                    $("#btnPhotographerEdit").hide();    //我要修改資料
                    $("#btnClosePhotographer").hide();    //取消
                    $("#btnPhotographerSave").hide();    //確認修改
                    $("#btnPhotographerCancel").hide();    //取消修改

                    $(".inputStyle").attr("readonly", "readonly");
                    //$("#radMale").attr("disabled", "disabled");
                    //$("#radFemale").attr("disabled", "disabled");
                    $('#divGender').addClass('divDisabled');
                    return false;
                }

                if ($("#hidPhotographerPri").val() == "0") {
                    //業務(已有拍攝日期，業務不得再修改)
                    $("#btnPhotographerEdit").hide();    //我要修改資料
                    $("#btnClosePhotographer").hide();    //取消
                    $("#btnPhotographerSave").hide();    //確認修改
                    $("#btnPhotographerCancel").hide();    //取消修改

                    $(".inputStyle").attr("readonly", "readonly");
                    //$("#radMale").attr("disabled", "disabled");
                    //$("#radFemale").attr("disabled", "disabled");
                    $('#divGender').addClass('divDisabled');
                    return false;
                }
            }


            // 當開始有狀態時，才開始進入修改資料的流程
            if (!$("#hidPhotographerAppoint").is(":checked")) {
                return false;
            }

            if (f) {
                $("#btnPhotographerEdit").hide();    //我要修改資料
                $("#btnClosePhotographer").hide();    //取消
                $("#btnPhotographerSave").show();    //確認修改
                $("#btnPhotographerCancel").show();    //取消修改
                $(".inputStyle").removeAttr("readonly");
                //$("#radMale").removeAttr("disabled");
                //$("#radFemale").removeAttr("disabled");
                $('#divGender').removeClass('divDisabled');
            } else {
                $("#btnPhotographerEdit").show();    //我要修改資料
                $("#btnClosePhotographer").show();    //取消
                $("#btnPhotographerSave").hide();    //確認修改
                $("#btnPhotographerCancel").hide();    //取消修改
                $(".inputStyle").attr("readonly", "readonly");
                //$("#radMale").attr("disabled", "disabled");
                //$("#radFemale").attr("disabled", "disabled");
                $('#divGender').addClass('divDisabled');
            }
        }

        function savePhotographerAppoint() {
            var check = checkPhotographerData($("#ddlPhotographerAppointer").val());
            var photographerAppointDate = $("#txtPhotographerAppointDate").val();
            var photographerAppointMemoDate = $("#txtPhotographerAppointMemo").val();
            var chkPhotographerAppointCancelDate = $("#chkPhotographerAppointCancel").is(":checked");
            var PhotographerAppointHour = $("#<%=ddlPhotographerAppointHour.ClientID%>").val();
            var PhotographerAppointMinute = $("#<%=ddlPhotographerAppointMinute.ClientID%>").val();
            var PhotographerAppointer = $("#ddlPhotographerAppointer").val();
            
            if (chkPhotographerAppointCancelDate)
            {
                if (photographerAppointMemoDate == "") {
                    alert("若要取消拍攝，必需填入原因於備註欄中!");
                    return false;
                } else {
                    if (!confirm("注意! 如要取消拍攝，請確認是否已在備註欄中填寫取消原因。")) {
                        return false;
                    }
                }

                //取消拍攝相關資料清除
                photographerAppointDate = "";
                PhotographerAppointHour = "01";
                PhotographerAppointMinute = "00";
                PhotographerAppointer = "";
            }
            else
            {
                //沒勾取消拍攝才要檢核
                if (!check) {
                    alert("請輸入正確的攝影師Email");
                    return false;
                }
                if (!isValidDate(photographerAppointDate)) {
                    alert("輸入日期格式不正確");
                    return false;
                }
            }
            $.ajax({
                type: "POST",
                url: "ProposalContent.aspx/photoAppointSave",
                data: "{'proposalId':'<%= ProposalId %>' ,'photographerAppointDate':'" + photographerAppointDate + "'," +
                        "'photographerAppointHour':'" + PhotographerAppointHour + "','photographerAppointMinute':'" + PhotographerAppointMinute + "'," +
                        "'oriPhotographerAppointer':'" + $("#ddlPhotographerAppointer").val() + "','photographerAppointer':'" + PhotographerAppointer + "'," + 
                        "'photographerAppointMemo':'" + $("#txtPhotographerAppointMemo").val() + "'," + "'photographerAppointCancel':'" + $("#chkPhotographerAppointCancel").is(":checked") + "'," + 
                        "'userName':'<%= UserName %>'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var res = response.d;
                    if (res != null) {
                        if (res) {

                            var message = "";
                            if (new Date($('#txtBusinessHourOrderTimeS').val()).toString() != "Invalid Date")
                            {
                                var PhotographerAppointDate = new Date($('#txtPhotographerAppointDate').val());
                                var BusinessHourOrderTimeS = new Date($('#txtBusinessHourOrderTimeS').val());
                                var Diff = (BusinessHourOrderTimeS - PhotographerAppointDate) / 86400000;
                                if (Diff <= 4)
                                    message = "\n【攝影日期、上檔日期，二個日期差距不到4天，請注意！】";
                            }

                            alert("確認成功!" + message);
                            if ($("#chkPhotographerAppointCancel").is(":checked")) {
                                $("#hidPhotographerAppointCancel").attr("checked", "checked");
                            } else {
                                $("#hidPhotographerAppointCancel").removeAttr("checked");
                            }
                            //ProposalSpecialTextGet();
                        }
                    }
                    window.parent.$.fancybox.close();
                }, error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        }

        function editPhotographerAppoint() {
            enabledPhotographerAppoint(true);
        }

        function cancelPhotographerAppoint() {
            ProposalSpecialTextGet();
        }

        function enabledPhotographerAppoint(f) {

            var txtPhotographerAppointDate = $("#txtPhotographerAppointDate").val();
            if ($.trim(txtPhotographerAppointDate) != "")
            {
                //拍攝結案不得再修改
                if ($("#hidPhotographerAppointClose").is(":checked"))
                {
                    $("#btnPhotographerAppointMail").hide();    //通知攝影師
                    $("#btnPhotographerAppointClose").hide();   //取消
                    $("#btnPhotographerAppointEdit").hide();    //我要修改資料
                    $("#btnPhotographerAppointSave").hide();    //確認修改
                    $("#btnPhotographerAppointCancel").hide();  //取消修改
                

                    $(".inputStyleP").attr("readonly", "readonly");
                    $("#txtPhotographerAppointDate").attr("disabled", "disabled");
                    $("#chkPhotographerAppointCancel").attr("disabled", "disabled");
                    $("#<%=ddlPhotographerAppointHour.ClientID%>").attr("disabled", "disabled");
                    $("#<%=ddlPhotographerAppointMinute.ClientID%>").attr("disabled", "disabled");
                    $("#ddlPhotographerAppointer").attr("disabled", "disabled");
                
                    return false;
                }
            }
            
            if (f)
            {
                $("#btnPhotographerAppointMail").hide();
                $("#btnPhotographerAppointClose").hide();
                $("#btnPhotographerAppointEdit").hide();
                $("#btnPhotographerAppointSave").show();
                $("#btnPhotographerAppointCancel").show();

                $(".inputStyleP").removeAttr("readonly");
                $("#txtPhotographerAppointDate").removeAttr("disabled");
                $("#chkPhotographerAppointCancel").removeAttr("disabled");
                $("#<%=ddlPhotographerAppointHour.ClientID%>").removeAttr("disabled");
                $("#<%=ddlPhotographerAppointMinute.ClientID%>").removeAttr("disabled");
                $("#ddlPhotographerAppointer").removeAttr("disabled");


            }
            else
            {
                $("#btnPhotographerAppointMail").show();
                $("#btnPhotographerAppointClose").show();
                $("#btnPhotographerAppointEdit").show();
                $("#btnPhotographerAppointSave").hide();
                $("#btnPhotographerAppointCancel").hide();

                $(".inputStyleP").attr("readonly", "readonly");
                $("#txtPhotographerAppointDate").attr("disabled", "disabled");
                $("#chkPhotographerAppointCancel").attr("disabled", "disabled");
                $("#<%=ddlPhotographerAppointHour.ClientID%>").attr("disabled", "disabled");
                $("#<%=ddlPhotographerAppointMinute.ClientID%>").attr("disabled", "disabled");
                $("#ddlPhotographerAppointer").attr("disabled", "disabled");

            }        
        }

        function closePhotographer() {
            if (!confirm("確定離開?\r\n視窗將關閉，請確認資料都已儲存")) {
                return false;
            }
            window.parent.$.fancybox.close();
        }

        //撈攝影資料
        function ProposalSpecialTextGet() {

            var txtPhotoAddress = $("#txtPhotoAddress");    //拍攝地點
            var txtPhotoContact = $("#txtPhotoContact");    //現場聯絡人姓名
            var radMale = $("#radMale");                    //先生
            var radFemale = $("#radFemale");                //小姐
            var txtPhotoTel = $("#txtPhotoTel");            //現場聯絡人電話
            var txtPhotoPhone = $("#txtPhotoPhone");        //現場聯絡人手機
            var txtPhotoMemo = $("#txtPhotoMemo");          //建議重點拍攝項目
            var txtPhotoDate = $("#txtPhotoDate");          //店家希望拍照時間
            var txtPhotoUrl = $("#txtPhotoUrl");            //參考網址 / 資料
            var txtPhohoNeeds = $("#txtPhohoNeeds");        //拍攝需求
            var txtRefrenceData = $("#txtRefrenceData");    //參考資料
            var txtPhotographerAppointDate = $("#txtPhotographerAppointDate");                //約定拍攝日期
            var chkPhotographerAppointCancel = $("#chkPhotographerAppointCancel");            //取消拍攝
            var ddlPhotographerAppointHour = $("#<%=ddlPhotographerAppointHour.ClientID%>");  //約定拍攝時間
            var ddlPhotographerAppointMinute = $("#<%=ddlPhotographerAppointMinute%>");
            var ddlPhotographerAppointer = $("#ddlPhotographerAppointer");                    //指派攝影師
            var txtPhotographerAppointMemo = $("#txtPhotographerAppointMemo");                //備註


            var spanPhotoStatus = $(".spanPhotoStatus");    //攝影狀態
            var spanPhotoAddress = $("#spanPhotoAddress");  //拍攝地點
            var spanPhotoContact = $("#spanPhotoContact");  //現場聯絡人姓名
            var spanPhotoSex = $("#spanPhotoSex");          //性別
            var spanPhotoTel = $("#spanPhotoTel");          //現場聯絡人電話
            var spanPhotoPhone = $("#spanPhotoPhone");      //現場聯絡人手機
            var spanPhotoMemo = $("#spanPhotoMemo");        //建議重點拍攝項目
            var spanPhotoDate = $("#spanPhotoDate");        //店家希望拍照時間
            var spanPhotoUrl = $("#spanPhotoUrl");          //參考網址 / 資料
            var spanPhohoNeeds = $("#spanPhohoNeeds");      //拍攝需求
            var spanRefrenceData = $("#spanRefrenceData");  //參考資料
            var spanPhotographerAppointDate = $("#spanPhotographerAppointDate");    //約定拍攝日期
            var spanPhotographerAppointTime = $("#spanPhotographerAppointTime");    //約定拍攝時間
            var spanPhotographerAppointMemo = $("#spanPhotographerAppointMemo");    //備註

            
            //下方資料
            $.ajax({
                type: "POST",
                url: "ProposalContent.aspx/ProposalSpecialAppointTextGet",
                data: "{'ProposalId':'<%= ProposalId %>'}",
                contentType: "application/json; charset=utf-8",
                async: false,
                dataType: "json",
                success: function (response) {
                    var res = response.d;
                    if (res != null && res.replace(/\|/g, '') != "") {
                        txtPhotographerAppointDate.val(res.split("|")[0]);
                        spanPhotographerAppointDate.html(res.split("|")[0]);

                        if (res.split("|").length > 1) {
                            ddlPhotographerAppointHour.val(res.split("|")[1]);
                        }
                        if (res.split("|").length > 2) {
                            ddlPhotographerAppointMinute.val(res.split("|")[2]);
                            spanPhotographerAppointTime.html(res.split("|")[1] + ":" + res.split("|")[2]);
                        }
                        if (res.split("|").length > 3) {
                            ddlPhotographerAppointer.val(res.split("|")[3]);
                        }
                        if (res.split("|").length > 4) {
                            txtPhotographerAppointMemo.val(res.split("|")[4]);
                            spanPhotographerAppointMemo.html(res.split("|")[4]);
                        }
                        if (res.split("|").length > 5) {
                            if (res.split("|")[5] == "true") {
                                chkPhotographerAppointCancel.attr("checked", true);
                            }
                        }


                        //需有攝影權限或業務才看的到
                        if ($("#hidPhotographerRes").val() == "1") {
                            $("#photographerAppointRes").show();
                        }
                        

                        $("#btnPhotographerAppointCheck").hide();    //約拍確認
                        enabledPhotographerAppoint(false);
                    } else {
                        $("#photographerAppointRes").hide();
                    }
                }, error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });

            //上方資料
            $.ajax({
                type: "POST",
                url: "ProposalContent.aspx/ProposalSpecialTextGet",
                data: "{'ProposalId':'<%= ProposalId %>'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var res = response.d;
                    if (res != null && res.replace(/\|/g, '') != "") {

                        enabledPhoto(false);

                        $("#btnPhotographerAppoint").hide(); //我要約拍
                        $("#divPhotoResult").show();//拍攝結果

                        if ($("#hidPhotographerAppointClose").is(":checked")) {
                            $(".spanPhotoStatus").html("<%=Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalPhotographer.PhotographerAppointClose)%>");
                        } else if ($("#hidPhotographerAppointCancel").is(":checked")) {
                            $(".spanPhotoStatus").html("<%=Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalPhotographer.PhotographerAppointCancel)%>");
                        } else if ($("#hidPhotographerAppointCheck").is(":checked")) {
                            $(".spanPhotoStatus").html("<%=Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalPhotographer.PhotographerAppointCheck)%>");
                        } else if ($("#hidPhotographerAppoint").is(":checked")) {
                            $(".spanPhotoStatus").html("<%=Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalPhotographer.PhotographerAppoint)%>");
                        } else {
                            $("#btnPhotographerAppoint").show(); //我要約拍 
                            $("#divPhotoResult").hide();
                        }

                        if ($("#ddlDeliveryType").val() == '<%= (int)DeliveryType.ToShop %>' || ($("#ddlDeliveryType").val() == '<%= (int)DeliveryType.ToHouse %>' && $("#ddlDealType").val() == '<%= (int)ProposalDealType.Travel %>')) {
                            //憑證+旅遊宅配
                            $("#photoToShopRes").show();
                            $("#photoToHouseRes").hide();

                            txtPhotoAddress.val(res.split("|")[0]);
                            spanPhotoAddress.html(res.split("|")[0]);
                            if (res.split("|").length > 1) {
                                txtPhotoTel.val(res.split("|")[1]);
                                spanPhotoTel.html(res.split("|")[1]);
                            }
                            if (res.split("|").length > 2) {
                                txtPhotoPhone.val(res.split("|")[2]);
                                spanPhotoPhone.html(res.split("|")[2]);
                            }
                            if (res.split("|").length > 3) {
                                txtPhotoMemo.val(res.split("|")[3]);
                                spanPhotoMemo.html(res.split("|")[3]);
                            }
                            if (res.split("|").length > 4) {
                                txtPhotoDate.val(res.split("|")[4]);
                                spanPhotoDate.html(res.split("|")[4]);
                            }
                            if (res.split("|").length > 5) {
                                txtPhotoUrl.val(res.split("|")[5]);
                                spanPhotoUrl.html(res.split("|")[5]);
                            }
                            if (res.split("|").length > 6) {
                                txtPhotoContact.val(res.split("|")[6]);
                                spanPhotoContact.html(res.split("|")[6]);
                            }
                            if (res.split("|").length > 7) {
                                if (res.split("|")[7] == "M") {
                                    radMale.attr("checked", true);
                                    spanPhotoSex.html("先生");
                                }
                                else {
                                    radFemale.attr("checked", true);
                                    spanPhotoSex.html("小姐");
                                }
                            }
                        } else {
                            //宅配
                            $("#photoToShopRes").hide();
                            $("#photoToHouseRes").show();


                            if (res.split("|").length > 0) {
                                txtPhohoNeeds.val(res.split("|")[0]);
                                spanPhohoNeeds.html(res.split("|")[0]);
                            }
                            if (res.split("|").length > 1) {
                                txtRefrenceData.val(res.split("|")[1]);
                                spanRefrenceData.html(res.split("|")[1]);
                            }
                        }
                    }
                }, error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        }
        

        

        /*
        * 檢查是否為員工的mail
        */
        function checkPhotographerData(userName) {
            var flag = false;
            $.ajax({
                type: "POST",
                url: "ProposalContent.aspx/CheckIsEmployee",
                data: "{'userName':'" + userName + "'}",
                async: false,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    flag= response.d;
                }, error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });

            return flag;
        }


        <%--如果有設定折價券權限的話，根據最小的毛利率，決定是否要顯示"特殊需求checkbox"--%>
        function SwitchSpecialRequirement(dealsQuantity) {
            if ('<%=HasUseDiscountSetPrivilege%>' == 'True') {
                var allowed = $('#<%= chkLowGrossMarginAllowedDiscount.ClientID %>');
                var notAllowed = $('#<%= chkNotAllowedDiscount.ClientID %>');
                var lbAllowed = $('label[for=<%= chkLowGrossMarginAllowedDiscount.ClientID %>]');
                var lbNotAllowed = $('label[for=<%= chkNotAllowedDiscount.ClientID %>]');

                <%--優惠內容必須大於0 且 不能為正在編輯--%>
                if (dealsQuantity > 0 && $('.editStyle').length == 0) {
                        notAllowed.removeAttr('disabled');
                        lbNotAllowed.css('color', '');
                        
                    <%--不管毛利率 只要勾選不可用折價券一律不可用--%>
                    if (notAllowed.is(':checked')) {
                        allowed.removeAttr('checked');
                        allowed.prop('disabled', 'disabled');
                        lbAllowed.css('color', 'silver');
                    } else {
                        allowed.removeAttr('disabled');
                        lbAllowed.css('color', '');
                    }

                }
                else {
                    <%--0個優惠的時候，控制項全部disabled--%>
                    allowed.prop('disabled', 'disabled');
                    lbAllowed.css('color', 'silver');
                    notAllowed.prop('disabled', 'disabled');
                    lbNotAllowed.css('color', 'silver');
                }
            }
        }

        function SwitchSpecialRequirementOnChange() {
            var notAllowed = $('#<%= chkNotAllowedDiscount.ClientID %>');

            notAllowed.on('change', function () {
                <%--數量>0，勾選本檔不可使用折價券的時候，低毛利率可用折價券才會disabled--%>
                SwitchSpecialRequirement(99);
            });
        }


        //上傳合約
        function ContractUpload() {
            var proposalId = "<%= ProposalId %>";//$("#pSellerId").html().replace(/\r?\n/g, "").replace(/ /g, "");
            var sellerId = "<%= SellerId %>";
            var contractFileType = $('[id*=ddlContactType]').val();
            var contractFileMemo = $("#txtContractFileMemo").val();
            var contractDueDate = $("#txtContractDueDate").val();
            if (contractDueDate != "") {
                if (!isValidDate(contractDueDate)) {
                    alert("輸入日期格式不正確");
                    return false;
                }
            }
            var contractPartyBContact = $("#txtContractPartyBContact").val();
            var contractVersion = $("#txtContractVersion").val();
            var files = $('#fileContract')[0].files;
            $('.if-error').hide();
            $('.error').removeClass('error');
            if (contractFileType == undefined) {
                $('#pContractFile').show();
                $('[id*=ddlContactType]').parent().parent().addClass('error');
                return false;
            }
            if (proposalId == "" || contractFileType.length == 0) {
                $('#pContractFile').show();
                $('[id*=ddlContactType]').parent().parent().addClass('error');
                return false;
            }
            if ($.trim(contractPartyBContact) == "") {
                $('#pContractPartyBContact').show();
                $("#txtContractPartyBContact").parent().parent().parent().addClass('error');
                return false;
            }
            if (files.length == 0) {
                alert("請選擇檔案");
                return false;
            }

            var formdata = new FormData();
            $.each(files, function (i, file) {
                formdata.append(file.name, file);
            });

            formdata.append("UploadType", "Proposal");
            formdata.append("SellerId", sellerId);
            formdata.append("ProposalId", proposalId);
            formdata.append("ContractFileType", contractFileType);
            formdata.append("ContractFileMemo", contractFileMemo);
            formdata.append("ContractDueDate", contractDueDate);
            formdata.append("ContractPartyBContact", contractPartyBContact);
            formdata.append("ContractVersion", contractVersion);
            

            $.ajax({
                type: "POST",
                url: "ContractUpload.ashx",
                data: formdata,
                processData: false,
                contentType: false,
                success: function (data) {
                    var c = JSON.parse(data);
                    if (c != undefined) {
                        if (c.IsSuccess) {
                            alert("上傳成功");
                        } else {
                            alert(c.Message);
                        }
                        location.href = location.protocol + "//" + location.host + location.pathname + "?pid=<%=ProposalId%>&tab=ContractSet";
                        HideDelete();
                    }
                },
                error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        }

        function HideDelete()
        {
            //排檔編輯,刪除鈕隱藏
            if ("<%= Helper.IsFlagSet(Convert.ToInt32(hidBusinessCheckFlag.Value),(int)ProposalBusinessFlag.ScheduleCheck)%>" == "True")
                $(".HideDelete").hide();
        }

        /*
        * 刪除合約檔案
        */
        function deleteContractFile(guid, ProposalId, ContractType) {
            if (!confirm("是否刪除檔案?")) {
                return false;
            }
            $.ajax({
                type: "POST",
                url: "ProposalContent.aspx/DeleteContractFile",
                data: "{'guid': '" + guid + "','proposalid': '" + ProposalId + "','contracttype': '" + ContractType + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //location.reload();
                    location.href = location.protocol + "//" + location.host + location.pathname + "?pid=<%=ProposalId%>&tab=ContractSet";
                },
                error: function (response) {
                    console.log(response.responseText);
                }
            });
        }

        function selectChannel(obj) {

            if ($(obj).is(':checked')) {
                $(obj).parent('.pChannel').next('.pArea').show();
                //後台不顯示
                var categoryList = $(obj).parent('.pChannel').next('.pArea').find("input[id*='chkCategory']");
                $.each(categoryList, function(cidx, cobj){
                    var isShow = $(cobj).parent().find("input[id*='hidIsShowBackEnd']").val();
                    if(isShow != undefined){
                        if(isShow.toLowerCase() == "false"){
                            $(cobj).attr("disabled", "disabled");
                        }
                    }
                });
            } else {
                $(obj).parent('.pChannel').next('.pArea').hide();
                $(obj).parent('.pChannel').next('.pArea').find('.pCommercial').hide();

                $(obj).parent('.pChannel').next('.pArea').find('input[type=checkbox]').each(function () {
                    $(this).next('label').css('color', '#333');
                    $(this).attr('checked', false);
                });
            }
        }

        function selectArea(obj) {

            var id = $(obj).prev().val();
            if ($(obj).is(':checked')) {
                $(obj).parent().parent().parent().find('.pCommercial' + id).show();
            } else {
                $(obj).parent().parent().parent().find('.pCommercial' + id).hide();

                $(obj).$(obj).parent().parent().parent().find('.pCommercial' + id).find('input[type=checkbox]').each(function () {
                    $(this).next('label').css('color', '#333');
                    $(this).attr('checked', false);
                });
            }
        }
        
        function selectCommercial(obj) {

            var id = $(obj).prev().val();

            if ($(obj).is(':checked')) {
                $(obj).parent().parent().parent().parent().parent().find('.pSpecialRegion' + id).show();
            } else {
                $(obj).parent().parent().parent().parent().parent().find('.pSpecialRegion' + id).hide();

                $(obj).parent().parent().parent().parent().parent().find('.pSpecialRegion' + id).find('input[type=checkbox]').each(function () {
                    $(this).next('label').css('color', '#333');
                    $(this).attr('checked', false);
                });
            }
        }


        

        function selectCategory(obj) {

            if ($(obj).is(':checked')) {
                var controls = $(obj).parent().next('.pCategory').find('input[type=checkbox]');
                if (controls.length > 0) {
                    $(obj).parent().next('.pCategory').show();
                    //後台不顯示
                    var subCategoryList = $(obj).parent('.pB').next('.pCategory').find("input[id*='chkSubDealCategory']");
                    $.each(subCategoryList, function(cidx, cobj){
                        var isShow = $(cobj).parent().find("input[id*='hidSubIsShowBackEnd']").val();
                        if(isShow != undefined){
                            if(isShow.toLowerCase() == "false"){
                                $(cobj).attr("disabled", "disabled");
                            }
                        }
                    });
                }
            } else {
                $(obj).parent().next('.pCategory').hide();

                $(obj).parent().next('.pCategory').find('input[type=checkbox]').each(function () {
                    $(this).next('label').css('color', '#333');
                    $(this).attr('checked', false);
                });
            }
        }

        function ChannelSaveCheck() {
            var isHasArea = false;
            var isHasCommercial = false;
            var isHasCategory = false;
            var isHasSubDealCategory = false;
           
            var checkAreaNumber = 0;
            var checkCommercialNumber = 0;
            var checkCategoryNumber = 0;
            var checkSubDealCategoryNumber = 0;
            
            $('.pA').each(function () {
                if ($(this).is(":visible")) {
                    isHasArea = true;
                    checkAreaNumber += $(this).find('input[type="checkbox"][id*="chkArea"]:checked').length;
                }
            });

            $('[class*=pCommercial]').each(function () {
                if ($(this).is(":visible")) {
                    isHasCommercial = true;
                    checkCommercialNumber += $(this).find('input[type="checkbox"][id*="chkCommercial"]:checked').length;
                }
            });

            $('.pB').each(function () {
                if ($(this).is(":visible")) {
                    isHasCategory = true;
                    checkCategoryNumber += $(this).find('input[type="checkbox"][id*="chkCategory"]:checked').length;
                }
            });

            $('.pCategory').each(function () {
                if ($(this).is(":visible")) {
                    isHasSubDealCategory = true;
                    checkSubDealCategoryNumber += $(this).find('input[type="checkbox"][id*="chkSubDealCategory"]:checked').length;
                }
            });

            
            if (isHasArea && checkAreaNumber <= 0) {
                if (!alert("請勾選區域!")) {
                    return false;
                }
            }
            else if (isHasCategory && checkCategoryNumber <= 0) {
                if (!alert("請勾選分類!")) {
                    return false;
                }
            }
            else if (isHasSubDealCategory && checkSubDealCategoryNumber <= 0) {
                if (!alert("請勾選子分類!")) {
                    return false;
                }
            }
            else if (isHasCommercial && checkCommercialNumber <= 0) {
                
                if (!confirm("此檔未勾選商圈喔!確定要存檔?")) {
                    return false;
                }
                else
                    return true;
                
            }
            else
                return true;
        }

        function ShowAccountingMessage()
        {
            if($('#<%=rbRecordOthers.ClientID%>').is(':checked'))
                $('#<%=txtAccountingMessage.ClientID %>').show();
            else
            {
                $('#<%=txtAccountingMessage.ClientID %>').hide();
                $('#<%=txtAccountingMessage.ClientID %>').val('');
            }
                
        }
        function isValidDate(dateStr) {

            if (dateStr == null || dateStr == '') return false;
            var parts = dateStr.split('/');
            if (parts.length != 3) return false;
            if (parts[0] > 9999 || parts[0] < 1753)
                return false;
            var accDate = new Date(dateStr);
            if (parseFloat(parts[0]) == accDate.getFullYear()
                && parseFloat(parts[1]) == accDate.getMonth() + 1
                && parseFloat(parts[2]) == accDate.getDate()) {
                return true;
            }
            else {
                return false;
            }
        }

        <%--#region退件因素細項數據統計 相關--%>
        <%--註冊勾選【資料錯誤修正】事件--%>
        function DataModifyOnChecked() {
            $('#<%=chkDataModify.ClientID%>').on('click', function () {
                if ($(this).is(':checked')) {
                    OpenReturnDetailDialog();
                }
                else {
                    $('#<%=hidReturnDetailDataModifyValue.ClientID%>').val("[]");
                    $('#detailDiv_DataModify').remove();
                }
            });
        }
        <%--開啟細項編輯dialog--%>
        function OpenReturnDetailDialog() {
            var detailIds = {};
            var currentIds = JSON.parse($('#<%=hidReturnDetailDataModifyValue.ClientID%>').val());
            $.each(currentIds, function (i, obj) {
                detailIds[i] = parseInt(this);
            });

            var codeGroup = "";
            if ($("#ddlDeliveryType").val() == '<%= (int)DeliveryType.ToShop %>') {
                codeGroup = "<%=SystemCodeGroup.ProposalReturnDetailDataModifyOfPpon%>";
            }
            else {
                codeGroup = "<%=SystemCodeGroup.ProposaReturnDetailDataModifyOfDelivery%>";
            }
            $.fancybox.open({
                closeBtn: true,
                href: "/Sal/ProposalReturnDetailDialog",
                type: "ajax",
                title: "<%=Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.DataModify)%>細目選擇(可複選)",
                maxWidth: '45%',
                ajax: {
                    data: {
                        codeGroup: codeGroup,
                        detailIds: detailIds
                    }
                },
                beforeClose: AutoUncheckDataModify,
                helpers: {
                    title: {
                        type: 'inside',
                        position: 'top'
                    },
                    overlay: {
                        closeClick: false // prevents closing when clicking OUTSIDE fancybox
                    }
                }
            });
        }
        <%--更新細目勾選隱藏欄位資料--%>
        function UpdateDetailItems(detailIds, detailTxts) {
            $('#detailDiv_DataModify').remove();
            var title = '<%=Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.DataModify)%>' + '細目：';
            var btnEdit = '<input type="button" onclick="OpenReturnDetailDialog();" value="編輯" class="btn btn-small" style="margin-left:15px;" />';
            var showTxt = title + detailTxts.join(' / ');

            if (detailIds.length > 0) {
                var detailDiv = '<div id="detailDiv_DataModify" style="background-color:#eee;">' + showTxt + btnEdit + '</div>';
                $('#returnReason').after(detailDiv);
            }
            $('#<%=hidReturnDetailDataModifyValue.ClientID%>').val(JSON.stringify(detailIds));
            $('#<%=hidReturnDetailDataModifyTxt.ClientID%>').val(showTxt);
            CloseFancyBox();
        }
        <%--細目無勾選時，自動取消資料錯誤修正選項--%>
        function AutoUncheckDataModify() {
            var currentIds = JSON.parse($('#<%=hidReturnDetailDataModifyValue.ClientID%>').val());
            if (currentIds.length == 0) {
                $('#<%=chkDataModify.ClientID%>').prop('checked', false);
                $('#<%=hidReturnDetailDataModifyTxt.ClientID%>').val("");
            }
        }
        function CloseFancyBox(count) {
            $.fancybox.close();
        }
        <%--#endregion退件因素細項數據統計--%>


        /*
        * 下載檔案
        */
        function DownloadFiles() {
            var chks = [];
            $.each($("#tabFiles input[type='checkbox']"), function (idx, obj) {
                if ($(obj).attr("id") != "checkAll") {
                    if ($(obj).prop("checked")) {
                        chks.push($(obj).val());
                    }                    
                }
            });
            if (chks.length == 0) {
                alert("請先選擇要下載的檔案");
            } else {
                $.ajax(
                {
                    type: "POST",
                    url: "SellerContent.aspx/GenTicketId",
                    data: "{ 'UserName': '<%=UserName%>'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    dataFilter: function (data) { return data; },
                    success: function (data) {
                        DownLoadRealFile(chks, data.d);
                    }, error: function (res) {
                        console.log(res);
                    }
                });

                
            }
        }

        function DownLoadRealFile(chks, ticketId) {
            var formData = new FormData();
            formData.append("UploadType", "DownloadMultiFile");
            formData.append("id", JSON.stringify(chks));
            formData.append("ProposalId", "<%= ProposalId %>");
            formData.append("ticketId", ticketId);
            $.ajax({
                type: "POST",
                url: "ContractDownload.ashx",
                data: formData,
                contentType: false,
                processData: false,
                cache: false,
                success: function (response) {
                    location.href = response;
                }, error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        }
        function DownLoadProposal(guid) {
            $.ajax(
            {
                type: "POST",
                url: "SellerContent.aspx/GenTicketId",
                data: "{ 'UserName': '<%=UserName%>'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    location.href = "ContractDownload.ashx?guid=" + guid + "&UploadType=Proposal&ticketId=" + data.d;
                }, error: function (res) {
                    console.log(res);
                }
            });
        }
        function DownLoadSellerFile(id) {
            $.ajax(
            {
                type: "POST",
                url: "SellerContent.aspx/GenTicketId",
                data: "{ 'UserName': '<%=UserName%>'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    location.href = "ContractDownload.ashx?id=" + id + "&UploadType=SellerFile&ticketId=" + data.d;
                }, error: function (res) {
                    console.log(res);
                }
            });
        }
        function ProposalDraw() {
            if (!confirm("確定要抽單?")) {
                return false;
            }
            if ($.trim($("#txtProposalDrawReason").val()) == "" || $.trim($("#txtProposalDrawReason").val()).length > 250) {
                alert("請輸入250字內的抽件原因。");
                return false;
            } else {
                $.ajax({
                    type: "POST",
                    url: "ProposalContent.aspx/ProposalDraw",
                    data: "{'ProposalId':'<%= ProposalId %>', 'ProposalDrawReason':'" + $("#txtProposalDrawReason").val() + "', 'UserName': '<%= UserName %>'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d) {
                            location.href = "ProposalContent.aspx?pid=<%= ProposalId %>";
                        } else {
                            alert("很抱歉！提案已被QC業務人員QC完成，無法自行抽回，若確認需要退單，請洽相關單位協助。");
                        }                        
                    }, error: function (xhr) {
                        console.log(xhr.responseText);
                    }
                });
            }
        }

        function IsNoTax() {

            if ('<%=(int)Status%>' == '<%=(int)ProposalStatus.Apply%>')
            {
                $('#<%=chkFreeTax.ClientID %>').attr("disabled", true);
            }
            else
            {
                if ($('#<%=rbRecordNoTaxInvoice.ClientID%>').is(':enabled')) {
                    if ($('#<%=rbRecordNoTaxInvoice.ClientID%>').is(':checked'))
                    {
                        $('#<%=chkFreeTax.ClientID %>').attr('checked', true)
                        $('#<%=chkFreeTax.ClientID %>').attr('disabled', true);
                    }
                    else
                        $('#<%=chkFreeTax.ClientID %>').prop('disabled', false);
                }
            }
            
        }

        function previewCPC(ele) {
            var tr = $(ele).closest('tr');
            var origPrice = tr.find('input#txtOrigPrice').val();
            var itemPrice = tr.find('input#txtItemPrice').val();
            var cpcBuy = parseInt(tr.find('input#txtCPCBuy').val());
            var cpcPresent = parseInt(tr.find('input#txtCPCPresent').val());
            var groupType = $('#ddlGroupCouponType option:selected').val();
            var item = new Array();
            if (cpcBuy + cpcPresent > 45 || cpcBuy + cpcPresent <= 0) {
                alert('數量超過45或等於0，不提供計算');
                return;
            }
            switch (groupType) {
                case "1":
                    var avgp = Math.floor(itemPrice / cpcBuy);
                    var rem = itemPrice % cpcBuy;
                    for (var i = 0; i < (cpcBuy + cpcPresent) ; i++) {
                        if (i < cpcBuy) {
                            if (rem > 0) {
                                item[i] = avgp + 1;
                                rem = rem - 1;
                            } else {
                                item[i] = avgp
                            }
                        } else {
                            item[i] = 0;
                        }
                    }
                    break;
                case "2":
                    var cost = Math.ceil(origPrice / cpcBuy);
                    var count = Math.floor(itemPrice / cost);
                    var sp = itemPrice - (cost * count);
                    if (sp < 0 || sp > cost) {
                        alert('無法計算')
                        return;
                    }
                    for (var i = 0; i < (cpcBuy + cpcPresent) ; i++) {
                        if (i < count) {
                            item[i] = cost;
                        } else if (i == count) {
                            item[i] = sp;
                        }
                        else {
                            item[i] = 0;
                        }
                    }
                    break;
                default:
                    return;
            }

            $(item).each(function (index, value) {
                createDiv(index, value);
            })
            var canvas = $('#canvas');
            canvas.show();
            $.blockUI({ message: canvas, css: { top:10+"%", left:25+"%", margin: 0, width: 800 + "px" } });
        }

        function createDiv(i,p) {
            var canvas = $('#canvas');
            var obj = '<div onclick="closeCanvas();" style="text-align:center;height:70px;width:150px;border-style:solid;solid;border-width:1px;float:left;margin-left:5px;margin-bottom:5px;opacity:1;line-height:70px;"></div>';
            canvas.append(obj);
            obj = $(canvas).find("div").eq(i);
            $(obj).html("<p>面額:" + p + "</p>");
        }

        function closeCanvas() {
            //clearInterval(oTimer);
            var obj = $('#canvas');
            $(obj).empty();
            obj.hide();
        }

        function CheckEveryDayNewDeal()
        {
            var check = null;
            $('label').each(function()
            {
                if ($(this).text()=="每日下殺")
                {
                    check = $(this).prev();
                    return false;
                }
            })
            
            if ($('#chkEveryDayNewDeal').is(':checked')!=$(check).is(':checked'))
            {
                return false;
            }

            return true;
        }

        function CheckPromoDeal(obj) {
            if ($(obj).attr("id") == "chkPromotionDeal") {
                if ($(obj).is(":checked")) {
                    var chkPromotionDeal = $("#chkExhibitionDeal").is(":checked");
                    if (chkPromotionDeal) {
                        event.preventDefault();
                        alert("【展演類型檔次】及【展覽類型檔次】只能擇一勾選");
                    }
                }
            }
            if ($(obj).attr("id") == "chkExhibitionDeal") {
                if ($(obj).is(":checked")) {
                    var chkPromotionDeal = $("#chkPromotionDeal").is(":checked");
                    if (chkPromotionDeal) {
                        event.preventDefault();
                        alert("【展演類型檔次】及【展覽類型檔次】只能擇一勾選");
                    }
                }
            }
        }
                
        function openOptionsNote() {
            $.fancybox({
                'padding': 0,
                'width': 752,
                'height': 597,
                'type': 'iframe',
                content: $('#divSalesOptionImage').show()
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <asp:Panel ID="divMain" runat="server" Visible="false" DefaultButton="btnSave">
        <div class="mc-navbar" style="display: block;">
            <a onclick="SelectTab(this);" tab="MainSet">
                <div class="mc-navbtn on">
                    提案單
                </div>
            </a>
            <a onclick="SelectTab(this);" tab="PponStoreSet" class="ppon travelview" id="aPponStore" runat="server">
                <div class="mc-navbtn" id="divStore" runat="server">
                    營業據點
                </div>
            </a>
            <a onclick="SelectTab(this);" tab="VbsSet" id="aVbs" runat="server">
                <div class="mc-navbtn" id="divVbsStore" runat="server">
                    核銷相關
                </div>
            </a>
            <a onclick="SelectTab(this);" tab="ContractSet">
                <div class="mc-navbtn">
                    合約上傳
        </div>
            </a>
        </div>
        <div id="MainSet" class="mc-content">
            <div id="basic">
                <h1 class="rd-smll">
                    <span class="breadcrumb flat">
                        <a href="javascript:void(0);">提案單 No.<%= ProposalId %></a><a href="javascript:void(0);" class="active">提案管理</a>
                    </span>
                    <a href="ProposalList.aspx" style="color: #08C;">回列表</a>
                </h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <asp:PlaceHolder ID="divSeller" runat="server" Visible="true">
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            商家資訊</label>
                        <div class="data-input rd-data-input">

                            <p>
                                <asp:Literal ID="liSellerName" runat="server"></asp:Literal>
                            </p>
                            <p>
                                <asp:HyperLink ID="hkSellerId" runat="server" Target="_blank"></asp:HyperLink>
                            </p>
                            <p style="color: #BF0000;">
                                <asp:Image ID="imgSellerStatus" runat="server" />
                                <asp:Literal ID="liSellerStatus" runat="server"></asp:Literal>
                            </p>
                            <p>
                                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="刪除提案" OnClick="btnDelete_Click" OnClientClick="return confirm('確定要義無反顧地刪除這張提案單?');" Visible="false" />
                                <asp:Button ID="btnCloneProposal" runat="server" CssClass="btn btn-small form-inline-btn" Text="複製提案單" OnClick="btnCloneProposal_Click"/>
                            </p>
                        </div>
                    </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="phdChangeSeller" runat="server" Visible="false">
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            更換賣家</label>
                        <div class="data-input rd-data-input">
                            <p>
                                <asp:TextBox ID="txtChangeSeller" runat="server" CssClass="input-small" Style="width: 200px" ClientIDMode="Static" MaxLength="30"></asp:TextBox>
                                <asp:Button ID="btnChangeSeller" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="移轉" OnClick="btnChangeSeller_Click" ClientIDMode="Static" OnClientClick="return chkChangeSellerConfirm();" />
                            </p>
                        </div>
                    </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="divSellerCheck" runat="server" Visible="true">
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                            系統商家資訊</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="txtSellerCheck" CssClass="input-small" Style="width: 200px" runat="server" ClientIDMode="Static"></asp:TextBox>
                                <asp:Button ID="btnSellerCheck" CssClass="btn btn-small btn-primary form-inline-btn" runat="server" Text="確認商家" OnClick="btnSellerCheck_Click" />
                                <a style="color: #08c" href="SellerList.aspx" target="_blank">商家管理</a>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="divBusiness" runat="server" Visible="false">
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                串接資訊</label>
                            <div class="data-input rd-data-input">
                                <asp:PlaceHolder ID="divBusinessHour" runat="server" Visible="false">
                                    <p>Bid:</p>
                                    <p>
                                        <asp:HyperLink ID="hkBusinessGuid" runat="server" Target="_blank" ClientIDMode="Static"></asp:HyperLink>
                                    </p>
                                    <p>
                                        <asp:HyperLink ID="hkSetup" runat="server" Target="_blank" ClientIDMode="Static" Text="後台" Visible="false"></asp:HyperLink>
                                    </p>
                                    <p class="note">
                                        <asp:Literal ID="liBusinessOrderTime" runat="server"></asp:Literal>
                                    </p>
                                    <asp:Button ID="btnReCreateProposal" runat="server" CssClass="btn btn-small form-inline-btn" Text="複製提案單" OnClick="btnReCreateProposal_Click" Visible="false" />
                                    <%--<asp:Button ID="btnReCreateSameProposal" runat="server" CssClass="btn btn-small  btn-primary form-inline-btn" Text="完全複製檔" OnClick="btnReCreateSameProposal_Click" Visible="false" />--%>
                                    <br />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="divReferrerBusinessHour" runat="server" Visible="false">
                                    <p style="color: blue;">
                                        <asp:Literal ID="liProposalCopyType" runat="server"></asp:Literal>:
                                    </p>
                                    <p style="color: blue;">
                                        <asp:HyperLink ID="hkReferrerBusinessHour" runat="server" Target="_blank"></asp:HyperLink>
                                    </p>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="divBuseinssHourOrderTimeSet" runat="server" Visible="false">
                                    <br />
                                    <p>
                                        上檔日期
                                    </p>
                                    <p>
                                        <asp:TextBox ID="txtBusinessHourOrderTimeS" runat="server" ClientIDMode="Static" Columns="8"></asp:TextBox>
                                    </p>
                                    <p>
                                        <asp:Button ID="btnOrderTimeSave" runat="server" CssClass="btn btn-small" Text="排檔" OnClick="btnOrderTimeSave_Click" />
                                        <asp:Button ID="btnCancelOrderTimeSave" runat="server" CssClass="btn btn-small" Text="取消上檔" OnClick="btnCancelOrderTimeSave_Click" Visible="false" />
                                    </p>
                                    <p>
                                        <asp:Button ID="btnUrgent" runat="server" CssClass="btn btn-small btn-primary" Text="快速上檔" OnClick="btnUrgent_Click" Visible="false" />
                                        <asp:Label ID="lblUrgent" runat="server" Visible="false" Text="快速上檔" ForeColor="#BF0000"></asp:Label>
                                        <asp:Button ID="btnCancelUrgent" runat="server" CssClass="btn btn-small btn-primary" Text="取消快速上檔" OnClick="btnCancelUrgent_Click" Visible="false" />
                                    </p>
                                </asp:PlaceHolder>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="divVendorAccount" runat="server" Visible="false">
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                商家提案帳號</label>
                            <div class="data-input rd-data-input">
                                <p>
                                    <asp:Literal ID="litVendorAccount" runat="server"></asp:Literal>
                                </p>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            負責業務</label>
                        <div class="data-input rd-data-input">
                            <table>
                                <tr>
                                    <td style="width: 80px">
                                        <asp:Literal ID="liDevelopeSalesName" runat="server"></asp:Literal>

                                    </td>
                                    <td style="width: 250px">
                                        <asp:Literal ID="liDevelopeSalesEmail" runat="server"></asp:Literal>

                                    </td>
                                    <td>
                                        <asp:PlaceHolder ID="divDevelopeReferral" runat="server" Visible="false">
                                            <p>
                                                <asp:TextBox ID="txtDevelopeReferralEmail" runat="server" CssClass="input-small" Style="width: 200px" ClientIDMode="Static"></asp:TextBox>
                                                <asp:Button ID="btnDevelopeReferral" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="轉件" OnClick="btnDevelopeReferral_Click" />
                                            </p>
                                        </asp:PlaceHolder>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 80px">
                                        <asp:Literal ID="liOperationSalesName" runat="server"></asp:Literal>

                                    </td>
                                    <td style="width: 250px">
                                        <asp:Literal ID="liOperationSalesEmail" runat="server"></asp:Literal>

                                    </td>
                                    <td>
                                        <asp:PlaceHolder ID="divOperationReferral" runat="server" Visible="false">
                                            <p>
                                                <asp:TextBox ID="txtOperationReferralEmail" runat="server" CssClass="input-small" Style="width: 200px" ClientIDMode="Static"></asp:TextBox>
                                                <asp:Button ID="btnOperationReferral" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="轉件" OnClick="btnOperationReferral_Click" />
                                            </p>
                                        </asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <asp:PlaceHolder ID="divReferralSale" runat="server" Visible="false">
                        <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            轉介人員</label>
                        <div class="data-input rd-data-input">
                            <p>
                                A. <asp:Literal ID="liReferralOtherName" runat="server"></asp:Literal>
                            </p>
                            <p>
                                <asp:Literal ID="liReferralOther" runat="server"></asp:Literal>
                            </p>
                            <asp:PlaceHolder ID="divReferralA" runat="server" Visible="true">
                                <p>
                                    <asp:DropDownList ID="ddlReferralOther" runat="server" Width="200"></asp:DropDownList>
                                </p>
                            </asp:PlaceHolder>
                        </div>
                        <div class="data-input rd-data-input">
                            <p>
                                B. <asp:Literal ID="liReferralSaleName" runat="server"></asp:Literal>
                            </p>
                            <p>
                                <asp:Literal ID="liReferralSale" runat="server"></asp:Literal>
                            </p>
                            <asp:PlaceHolder ID="divReferralB" runat="server" Visible="true">
                                <p>
                                    <asp:DropDownList ID="ddlReferralSale" runat="server" Width="200"></asp:DropDownList>
                                </p>
                            </asp:PlaceHolder>
                        </div>
                    </div>
                    </asp:PlaceHolder>
                    
                    <asp:Panel ID="divDownload" runat="server" class="form-unit" Visible="false">
                        <label class="unit-label rd-unit-label">
                            合約列印</label>
                        <div class="data-input rd-data-input">
                            <asp:DropDownList ID="ddlStoreList" runat="server" ClientIDMode="Static" Width="150px" Visible="false"></asp:DropDownList>
                            <asp:Button ID="btnDownload" runat="server" CssClass="btn btn-primary btn-small form-inline-btn" Text="下載" OnClick="btnDownload_Click" />
                            <asp:CheckBox ID="chkIsListCheck" runat="server" Text="列印上架確認單" Visible="false" />
                        </div>
                    </asp:Panel>
                    <div class="form-unit" style="border-bottom: none;">
                        <label class="unit-label rd-unit-label">
                            檢核狀態</label>
                        <div class="data-input rd-data-input">
                            <asp:HiddenField ID="hidStatus" runat="server" />
                            <asp:HiddenField ID="hidApplyFlag" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="hidApproveFlag" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="hidCreatedFlag" runat="server" ClientIDMode="Static"  Value="0"/>
                            <asp:HiddenField ID="hidBusinessCheckFlag" runat="server" ClientIDMode="Static" Value="0"/>
                            <asp:HiddenField ID="hidEditorFlag" runat="server" ClientIDMode="Static"  />
                            <asp:HiddenField ID="hidEditorPassFlag" runat="server" ClientIDMode="Static"  />
                            <asp:HiddenField ID="hidListingFlag" runat="server"  ClientIDMode="Static" />
                            <asp:HiddenField ID="hidMultiDealsUpdate" runat="server"  ClientIDMode="Static" />
                            <asp:HiddenField ID="hidPhotographerPri" runat="server"  ClientIDMode="Static" Value="0" />
                            <asp:HiddenField ID="hidPhotographerRes" runat="server"  ClientIDMode="Static" Value="0" />
                            <asp:HiddenField ID="hidSellerFlag" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="hidSellerCheckFlag" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="hidFileFlag" runat="server" ClientIDMode="Static" Value="0" />
                            <asp:HiddenField ID="hidReturnDetailDataModifyValue" runat="server" ClientIDMode="Static" Value="[]" />
                            <asp:HiddenField ID="hidReturnDetailDataModifyTxt" runat="server" ClientIDMode="Static" Value="" />
                            <asp:HiddenField ID="hidShoppingCartV2Enabled" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="hidShoppingCartFreightsItem" runat="server" ClientIDMode="Static" />

                            <div id="returnReason">
                                <asp:PlaceHolder ID="phReturnPpon" runat="server">
                                    <asp:CheckBox ID="chkCommissionInCompatible" runat="server" ClientIDMode="Static" />
                                    <asp:CheckBox ID="chkFoldInCompatible" runat="server" ClientIDMode="Static"  />
                                    <asp:CheckBox ID="chkCopiesInCompatible" runat="server" ClientIDMode="Static"/>
                                    <asp:CheckBox ID="chkBusinessPriceLower" runat="server" ClientIDMode="Static"/><br />
                                    <asp:CheckBox ID="chkNeedOptimization" runat="server" ClientIDMode="Static"/>
                                    <asp:CheckBox ID="chkPoorThanBusinessStrife" runat="server" ClientIDMode="Static" />
                                    <asp:CheckBox ID="chkDataModifyContractInComplete" runat="server" ClientIDMode="Static" /><br />
                                    <asp:CheckBox ID="chkCouponEventContentModify" runat="server" ClientIDMode="Static" />
                                </asp:PlaceHolder>    
                                <asp:PlaceHolder ID="phReturnHouse" runat="server">
                                    <asp:CheckBox ID="chkOptimizationPrice" runat="server" ClientIDMode="Static" />
                                    <asp:CheckBox ID="chkInformationError" runat="server" ClientIDMode="Static"  />
                                    <asp:CheckBox ID="chkPercentLower" runat="server" ClientIDMode="Static" />
                                    <asp:CheckBox ID="chkExistProposal" runat="server" ClientIDMode="Static" /><br />
                                    <asp:CheckBox ID="chkSaleOverProposal" runat="server" ClientIDMode="Static" />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="phReturnAll" runat="server">
                                    <asp:CheckBox ID="chkDataModify" runat="server" ClientIDMode="Static" />
                                    <asp:CheckBox ID="chkContractError" runat="server" ClientIDMode="Static" />
                                    <asp:CheckBox ID="chkCancel" runat="server" ClientIDMode="Static" />
                                    <asp:CheckBox ID="chkCheckError" runat="server" ClientIDMode="Static" />
                                    <asp:CheckBox ID="chkOther" runat="server" ClientIDMode="Static" />
                                </asp:PlaceHolder>
                            </div>
                            <ul class="login-way check-flag">
                                <li style="width: 100%;">
                                    <p style="width: 80%;">
                                        <asp:Button ID="btnSellerProposalSaleEditor" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="編輯商家提案" OnClick="btnSellerProposalSaleEditor_Click" Visible="false" />
                                        <asp:Button ID="btnSellerProposalCheckWaitting" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="請求商家覆核" OnClick="btnSellerProposalCheckWaitting_Click" Visible="false" OnClientClick="return chkSellerProposalCheckWaitting()" />
                                        <asp:Button ID="btnProposalSend" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="送件QC" OnClick="btnProposalSend_Click" Visible="false" />
                                        
                                        <asp:PlaceHolder ID="divSellerProposalReturned" runat="server" Visible="false">
                                            <asp:TextBox ID="txtSellerReturnedLog" runat="server" CssClass="input-half" TextMode="MultiLine" Height="60px" Width="100%" ClientIDMode="Static" placeholder="請填寫400字內的退件原因。提案退回後，將會以Mail方式通知商家，同時告知退件原因。"></asp:TextBox>

                                            <asp:Button ID="btnSellerProposalReturned" runat="server" CssClass="btn btn-small form-inline-btn" Text="提案單退回商家" OnClick="btnSellerProposalReturned_Click" OnClientClick="return SellerReturnedCheckLog();" />
                                        </asp:PlaceHolder>
                                        <asp:PlaceHolder ID="divProposalReturned" runat="server" Visible="false">
                                            <asp:TextBox ID="txtReturnedLog" runat="server" CssClass="input-half" TextMode="MultiLine" Height="60px" Width="100%" ClientIDMode="Static"></asp:TextBox>

                                            <asp:Button ID="btnProposalReturned" runat="server" CssClass="btn btn-small form-inline-btn" Text="退件" OnClick="btnProposalReturned_Click" OnClientClick="return ReturnedCheckLog();" />
                                        </asp:PlaceHolder>
                                        <asp:Button ID="btnCreateBusinessHour" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="建檔" OnClick="btnCreateBusinessHour_Click" OnClientClick="return chkCreateBusinessHour()" Visible="false" />
                                        <asp:HyperLink ID="btnProposalDraw" runat="server" ClientIDMode="Static" class="btn btn-primary rd-mcacstbtn form-inline-btn" style="color:#fff" Visible="false">抽單</asp:HyperLink>
                                    </p>
                                </li>
                                <asp:Repeater ID="rptStatus" runat="server" OnItemDataBound="rptStatus_ItemDataBound">
                                    <ItemTemplate>
                                        <li style="width: 100%;">
                                            <table>
                                                <tr>
                                                    <td style="width:15%">
                                            <asp:Image ID="imgStatus" runat="server" Width="35px" Style="margin-right: 7px" />
                                            <p>
                                                <asp:Literal ID="liStatus" runat="server"></asp:Literal>
                                            </p>
                                                    </td>
                                                    <td style="width:80%">
                                            <asp:Repeater ID="rptFlag" runat="server" OnItemDataBound="rptFlag_ItemDataBound" OnItemCommand="rptFlag_ItemCommand">
                                                <ItemTemplate>
                                                    <p>
                                                        <asp:ImageButton ID="btnFlag" runat="server" />
                                                        <asp:ImageButton ID="btnFlagPass" runat="server" Visible="false"/>
                                                        <asp:Literal ID="liFlag" runat="server"></asp:Literal>
                                                    </p>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                                    </td>
                                                </tr>
                                            </table>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div id="divPaperContractNotCheckReason" class="form-unit" style="display: none; height: 300px; width: 500px;">
                <label class="unit-label rd-unit-label">
                    填寫原因</label>
                <div class="data-input rd-data-input">
                    <asp:CheckBox ID="chkReason1" CssClass="chkReason" runat="server" Text="附約確實尚未收到或交回，需加附證明文件" /><br />
                    <asp:CheckBox ID="chkReason2" CssClass="chkReason" runat="server" Text="附約曾回來過，但需再修改簽章" /><br />
                    <asp:CheckBox ID="chkReason3" CssClass="chkReason" runat="server" Text="附約曾回來過，但需再加蓋騎縫章" /><br />
                    <asp:CheckBox ID="chkReason4" CssClass="chkReason" runat="server" Text="其他：" />
                    <asp:TextBox ID="txtPaperContractNotCheckReason" runat="server" ClientIDMode="Static"></asp:TextBox>
                    <br />
                    <br />
                    注意！合約未回先上檔的原因將與QC、QA退件一併進行相關比對，尚請各位主管務必協助確實填寫，謝謝。
                    <br />
                    <br />
                    <asp:Button ID="btnPaperContractNotCheckReason" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="儲存" OnClientClick="savePaperContractNotCheckReason()" ClientIDMode="Static" />
                    <p id="pPaperContractNotCheckReason" class="if-error" style="display: none;">請輸入同意合約未回先上檔之原因。</p>
                </div>
                
            </div>
            <div id="deal">
                <h1 class="rd-smll">提案內容</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;消費方式</label>
                        <div class="data-input rd-data-input">
                            <p>
                                <asp:DropDownList ID="ddlDeliveryType" ClientIDMode="Static" runat="server" Width="100px" onchange="deliveryTypeChange(this);"></asp:DropDownList><br />
                                <span class="GroupCoupon"><asp:CheckBox ID="chkNoRestrictedStore" runat="server" ClientIDMode="Static" Text="通用券"/></span>
                                <span><asp:CheckBox ID="chkConsignment" runat="server" ClientIDMode="Static" Visible="false" Text="商品寄倉"/></span>
                                <span class="GroupCoupon"><asp:CheckBox ID="chkGroupCoupon" runat="server" ClientIDMode="Static" Text="成套票券"/></span>
                                <span class="GroupCoupon CPC"><asp:DropDownList ID="ddlGroupCouponType" runat="server" ClientIDMode="Static" style="width:100px;" /></span>
                                <span><asp:CheckBox ID="cbxBankDeal" runat="server" ClientIDMode="Static"/></span><br />
                                <span><asp:CheckBox ID="chkPromotionDeal" runat="server" ClientIDMode="Static" onclick="CheckPromoDeal(this);" Text="展演檔次-鎖定功能(逾期自動核銷)" /></span>
                                <span><asp:CheckBox ID="chkExhibitionDeal" runat="server" ClientIDMode="Static" onclick="CheckPromoDeal(this);" Text="展覽類型檔次(逾期自動核銷)" /></span>
                                <span><asp:CheckBox ID="chkGame" runat="server" ClientIDMode="Static" Text="遊戲活動檔" /></span>
                                <span><asp:CheckBox ID="chkChannelGift" runat="server" ClientIDMode="Static" Text="代銷贈品檔" Enabled="false"/></span>
                            </p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;代銷通路</label>
                        <div class="data-input rd-data-input">
                            <asp:CheckBoxList ID="chkAgentChannels" runat="server" RepeatDirection="Horizontal"></asp:CheckBoxList>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;檔次名稱</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtBrandName" runat="server" Width="80%" ClientIDMode="Static"></asp:TextBox>
                            <p id="pBrandName" class="if-error" style="display: none;">請輸入檔次名稱。</p>
                            <p class="note">* 註：部分宅配商品若無品牌請輸入商品名稱，如：專業高強度防震運動無鋼圈背心、可溶水拋棄式馬桶坐墊紙隨手包</p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;提案類型</label>
                        <div class="data-input rd-data-input">
                            <p>
                                <asp:DropDownList ID="ddlDealType" ClientIDMode="Static" runat="server" Width="100px" onchange="BindDealSubType(this);"></asp:DropDownList>
                            </p>
                            <p>
                                <asp:DropDownList ID="ddlDealSubType" runat="server" ClientIDMode="Static" Width="100px" onchange="SetDealSubType(this);"></asp:DropDownList>
                                <asp:HiddenField ID="hidDealSubType" runat="server" ClientIDMode="Static" />
                                <asp:CheckBox ID="chkSelfOptional" runat="server" ClientIDMode="Static"/>自選檔，預估營業額
                                <asp:TextBox ID="txtBusinessPrice" runat="server" ClientIDMode="Static" MaxLength="7"></asp:TextBox>
                                <p id="pBusinessPrice" class="if-error" style="display: none;">請輸入預估營業額。</p>
                                <p>
                                </p>
                                <p>
                                </p>
                                <p>
                                </p>                       
                            </p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;店家單據開立方式</label>
                        <div class="data-input rd-data-input" id="divReocrd">
                                <asp:RadioButton ID="rbRecordInvoice" CssClass="record" GroupName="record" runat="server" />統一發票
                                <asp:RadioButton ID="rbRecordNoTaxInvoice" CssClass="record" GroupName="record" runat="server" />免稅統一發票
                                <asp:RadioButton ID="rbRecordReceipt" CssClass="record" GroupName="record" runat="server" />免用統一發票收據
                                <asp:RadioButton ID="rbRecordOthers" CssClass="record" GroupName="record" runat="server" />其他
                                <asp:TextBox ID="txtAccountingMessage" runat="server" />
                                <asp:HiddenField ID="hidRecord" runat="server" ClientIDMode="Static" Value="" />
                                <asp:HiddenField ID="hidInputTax" runat="server" ClientIDMode="Static" Value="" />
                                <asp:HiddenField ID="hidGroupOrder" runat="server" ClientIDMode="Static" Value="0" />
                                
                            <p id="pRecordOthers" class="if-error" style="display: none;">請輸入店家單據開立方式。</p>
                        </div>
                    </div>
                    <asp:Panel id="chkMohistPanel" runat="server" class="form-unit" Visible="false">
                        <label class="unit-label rd-unit-label">墨攻</label>
                        <div class="data-input rd-data-input">
                            <asp:CheckBox ID="chkMohist" runat="server" ClientIDMode="Static"></asp:CheckBox>
                        </div>
                    </asp:Panel>
                    <div class="form-unit product" style="display: none;">
                        <label class="unit-label rd-unit-label">
                            出貨類型</label>
                        <div class="data-input rd-data-input">
                            <asp:DropDownList ID="ddlShipType" runat="server" ClientIDMode="Static" Width="100px" onchange="ShipTypeChange(this);"></asp:DropDownList>
                            <span id="pShipType" style="display: none;">
                                <br />
                                <asp:RadioButton ID="rboUniShipDay" runat="server" GroupName="SubShipType" />
                                <p>最早出貨日：上檔後+</p>
                                <asp:TextBox ID="txtShipText1" runat="server" ClientIDMode="Static" CssClass="input-small" Width="50px" onkeyup="return ValidateNumber($(this),value);" MaxLength="3"></asp:TextBox>
                                <p>個工作日</p>
                                <p>，最晚出貨日：統一結檔後+</p>
                                <asp:TextBox ID="txtShipText2" runat="server" ClientIDMode="Static" CssClass="input-small" Width="50px" onkeyup="return ValidateNumber($(this),value);" MaxLength="3"></asp:TextBox>
                                <p>個工作日</p>
                                <br />
                                <asp:RadioButton ID="rboNormalShipDay" runat="server" GroupName="SubShipType" />
                                <p>訂單成立後</p>
                                <asp:TextBox ID="txtShipText3" runat="server" ClientIDMode="Static" CssClass="input-small" Width="50px" onkeyup="return ValidateNumber($(this),value);" MaxLength="3"></asp:TextBox>
                                <p>個工作日出貨完畢</p>
                            </span>
                            <span id="pShipTypeOther" style="display: none;">
                                <br />
                                <p></p>
                                <asp:TextBox ID="txtShipTypeOther" runat="server" TextMode="MultiLine" Height="60px" Width="80%" ClientIDMode="Static"></asp:TextBox>
                                <p></p>
                            </span>
                            <p id="pShipTypeError" class="if-error" style="display: none;">請輸入出貨與退貨資訊，且僅能輸入數字。</p>
                        </div>
                    </div>
                    <div class="form-unit product" style="display:none">
                        <label class="unit-label rd-unit-label">
                            配送方式</label>
                        <div class="data-input rd-data-input">
                            <asp:DropDownList ID="ddlDeliveryMethod" runat="server" Width="180px">
                                <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-unit product" style="display:none">
                        <label class="unit-label rd-unit-label">
                            鑑賞期</label>
                        <div class="data-input rd-data-input">
                            <asp:CheckBox ID="chkTrialPeriod" runat="server" />不適用
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            市場分析</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtMarketAnalysis" runat="server" TextMode="MultiLine" Height="60px" Width="80%"></asp:TextBox>
                            <p class="note">如: 最近一次銷售狀況、競爭對手銷售情況、店家優勢賣點、他網訪價、競網比價</p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            行銷/策展專案</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtMarketingResource" runat="server" Width="60%"></asp:TextBox><br />
                            <p class="note">要參與的現有策展ＩＤ，請以「/」分隔(例：758/752/753)</p>
                        </div>
                    </div>
            </div>
            </div>
            <asp:Panel ID="divMultiDeals" runat="server">
                <h1 class="rd-smll">優惠內容
                <asp:HyperLink ID="hkMultiDealEdit" runat="server" ClientIDMode="Static" ForeColor="#0088cc" Font-Size="Medium" Text="編輯" style="display:none"></asp:HyperLink>
                </h1>
                <hr class="header_hr">
                <div id="mc-table">
                <asp:Repeater ID="rptMultiDeal" runat="server" OnItemDataBound="rptMultiDeal_ItemDataBound">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table" id="proposalContentTable">
                            <thead>
                            <tr class="rd-C-Hide">
                                <th class="OrderDate">專案內容
                                </th>
                                <th class="OrderDate">原價
                                </th>
                                <th class="OrderSerial">售價
                                </th>
                                <th class="OrderSerial CPC">憑證組數
                                </th>
                                <th class="OrderSerial">獲利毛利率
                                </th>
                                <th class="OrderName">洽談毛利率<br />進貨價
                                </th>
                                <th class="OrderDate">上架費<br />(份)
                                </th>
                                <th class="OrderDate">最大<br />購買
                                </th>
                                <th class="OrderDate">每人<br />限購
                                </th>
                                <th class="OrderDate">ATM<br />保留
                                </th>
                                <th class="OrderDate product shoppingCartCfgHide">運費
                                </th>
                                <th class="OrderDate product shoppingCartCfgHide">免運<br />份數
                                </th>
                                <th class="OrderDate product shoppingCartCfgShow">購物車
                                </th>
                                <th class="OrderDate">多重選項</th>
                                <th class="OrderDate DealEditStyle">
                                    <%if (IsEnableVbsNewShipFile) { %>
                                    <input type="button" class="btn btn-primary rd-mcacstbtn" style="padding:3px 5px 3px 5px;" value="撰寫範例教學" onclick="openOptionsNote();"/>
                                    <%} %>
                                </th>
                            </tr>
                            </thead>
                    </HeaderTemplate>
                    <ItemTemplate>
                        
                    </ItemTemplate>
                    <FooterTemplate>
                        <tr>
                            <td>
                                 <asp:Label ID="lblEmptyData" Text="" runat="server" Visible="false">
                                 </asp:Label>
                             </td>
                         </tr>
                        </table>
                    </FooterTemplate>
                </asp:Repeater> 
                </div>
                <div id="canvas" onclick="$.unblockUI();" style="display:none;"></div>
               <%-- <img src="../Images/icons/add182.png" alt="新增優惠" title="新增優惠" style="padding-top:10px;float:right" onclick="addChanges();"  />--%>
                <asp:Image runat="server" ImageUrl="../Themes/default/images/17Life/G2/add.png" ID="addDeals" ClientIDMode="Static" ToolTip="新增優惠" style="padding-top:10px;float:right" onclick="addChanges();"></asp:Image>
            </asp:Panel>
            <br />
            <div id="dealMarket" style="display:none">
                <h1 class="rd-smll">巿場分析比價</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;Gomaji</label>
                        <div class="data-input rd-data-input">
                            <p class="range1">價格</p>
                            <p>
                                <asp:TextBox ID="txtGomajiPrice" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 70px" MaxLength="10"></asp:TextBox>
                            </p>
                            <p>
                                連結
                            </p>
                            <p>
                                <asp:TextBox ID="txtGomajiLink" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 250px"></asp:TextBox>
                            </p>
                        </div>
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;創業家兄弟</label>
                        <div class="data-input rd-data-input">
                            <p class="range1">價格</p>
                            <p>
                                <asp:TextBox ID="txtKuobrothersPrice" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 70px" MaxLength="10"></asp:TextBox>
                            </p>
                            <p>
                                連結
                            </p>
                            <p>
                                <asp:TextBox ID="txtKuobrothersLink" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 250px"></asp:TextBox>
                            </p>
                        </div>
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;瘋狂賣客</label>
                        <div class="data-input rd-data-input">
                            <p class="range1">價格</p>
                            <p>
                                <asp:TextBox ID="txtCrazymikePrice" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 70px" MaxLength="10"></asp:TextBox>
                            </p>
                            <p>
                                連結
                            </p>
                            <p>
                                <asp:TextBox ID="txtCrazymikeLink" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 250px"></asp:TextBox>
                            </p>
                        </div>
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;EzPrice比價網</label>
                        <div class="data-input rd-data-input">
                            <p class="range1">價格</p>
                            <p>
                                <asp:TextBox ID="txtEzPricePrice1" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 70px" MaxLength="10"></asp:TextBox>
                            </p>
                            <p>
                                連結
                            </p>
                            <p>
                                <asp:TextBox ID="txtEzPriceLink1" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 250px"></asp:TextBox>
                            </p>
                            <br />
                            <p class="range1">價格</p>
                            <p>
                                <asp:TextBox ID="txtEzPricePrice2" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 70px" MaxLength="10"></asp:TextBox>
                            </p>
                            <p>
                                連結
                            </p>
                            <p>
                                <asp:TextBox ID="txtEzPriceLink2" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 250px"></asp:TextBox>
                            </p>
                            <br />
                            <p class="range1">價格</p>
                            <p>
                                <asp:TextBox ID="txtEzPricePrice3" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 70px" MaxLength="10"></asp:TextBox>
                            </p>
                            <p>
                                連結
                            </p>
                            <p>
                                <asp:TextBox ID="txtEzPriceLink3" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 250px"></asp:TextBox>
                            </p>
                        </div>
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;品購</label>
                        <div class="data-input rd-data-input">
                            <p class="range1">價格</p>
                            <p>
                                <asp:TextBox ID="txtPinglePrice1" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 70px" MaxLength="10"></asp:TextBox>
                            </p>
                            <p>
                                連結
                            </p>
                            <p>
                                <asp:TextBox ID="txtPingleLink1" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 250px"></asp:TextBox>
                            </p>
                            <br />
                            <p class="range1">價格</p>
                            <p>
                                <asp:TextBox ID="txtPinglePrice2" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 70px" MaxLength="10"></asp:TextBox>
                            </p>
                            <p>
                                連結
                            </p>
                            <p>
                                <asp:TextBox ID="txtPingleLink2" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 250px"></asp:TextBox>
                            </p>
                            <br />
                            <p class="range1">價格</p>
                            <p>
                                <asp:TextBox ID="txtPinglePrice3" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 70px" MaxLength="10"></asp:TextBox>
                            </p>
                            <p>
                                連結
                            </p>
                            <p>
                                <asp:TextBox ID="txtPingleLink3" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 250px"></asp:TextBox>
                            </p>
                        </div>
                        <label class="unit-label rd-unit-label">
                            類似商品</label>
                        <div class="data-input rd-data-input">
                            <p class="range1">價格</p>
                            <p>
                                <asp:TextBox ID="txtSimilarPrice" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 70px" MaxLength="10"></asp:TextBox>
                            </p>
                            <p>
                                連結
                            </p>
                            <p>
                                <asp:TextBox ID="txtSimilarLink" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 250px"></asp:TextBox>
                            </p>
                        </div>
                        <label class="unit-label rd-unit-label">
                            其他</label>
                        <div class="data-input rd-data-input">

                            <p>
                                <asp:DropDownList ID="ddlOtherShop1" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:HiddenField ID="hidOtherShop1" runat="server" ClientIDMode="Static" />
                                <p class="range1">價格</p>
                                <asp:TextBox ID="txtOtherPrice1" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 70px" MaxLength="10"></asp:TextBox>
                            </p>
                            <p>
                                連結
                            </p>
                            <p>
                                <asp:TextBox ID="txtOtherLink1" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 250px"></asp:TextBox>
                            </p>
                            <br />

                            <p>
                                <asp:DropDownList ID="ddlOtherShop2" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:HiddenField ID="hidOtherShop2" runat="server" ClientIDMode="Static" />
                                <p class="range1">價格</p>
                                <asp:TextBox ID="txtOtherPrice2" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 70px" MaxLength="10"></asp:TextBox>
                            </p>
                            <p>
                                連結
                            </p>
                            <p>
                                <asp:TextBox ID="txtOtherLink2" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 250px"></asp:TextBox>
                            </p>
                            <br />

                            <p>
                                <asp:DropDownList ID="ddlOtherShop3" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:HiddenField ID="hidOtherShop3" runat="server" ClientIDMode="Static" />
                                <p class="range1">價格</p>
                                <asp:TextBox ID="txtOtherPrice3" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 70px" MaxLength="10"></asp:TextBox>
                            </p>
                            <p>
                                連結
                            </p>
                            <p>
                                <asp:TextBox ID="txtOtherLink3" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 250px"></asp:TextBox>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div id="TreviFountain" style="display: none">
                <h1 class="rd-smll">頁面許願池</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <span style="color: blue; font-weight: bold;">想要大聲告訴大家的商品特色和優點，消費者們可以從這檔裡得到什麼呢？請告訴我們，讓我們來為你說出來！</span>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*本檔主打星</span></label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtDealFeature" runat="server"  TextMode="MultiLine" Height="80px" Width="80%" placeholder="為了保險，請告訴我們這檔的男女主角是誰，別得大家誤會一場！你可以寫強打的商品名稱、顏色、口味、菜名.....你想讓消費者第一眼就注意到、被吸引的。注意！是唯一、獨特，而不是每檔裡面什麼都是主打星，這樣太沒特色了！"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <span style="color: #BF0000; font-weight: bold;">*這檔的特色、商品的優點，你想要怎麼說服大家購買你的產品呢？</span>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>1&nbsp;</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtDealFeature1" runat="server" CssClass="input-small" Style="width: 400px" placeholder="例如：牛肉麵，湯濃、肉厚、麵Q，吃飽飽"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>2&nbsp;</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtDealFeature2" runat="server" CssClass="input-small" Style="width: 400px" placeholder="例如：SPA，技術好、環境優、美人多，養身養心也養眼"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>3&nbsp;</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtDealFeature3" runat="server" CssClass="input-small" Style="width: 400px" placeholder="例如：燜燒鍋，快速、方便、好清洗，省時美味不打折"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            4&nbsp;</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtDealFeature4" runat="server"  TextMode="MultiLine" Height="80px" Width="80%" placeholder="我的商品太好了，優點說不完，三個必填不夠用，全部的好處、優點全都寫出來！"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <span style="color: blue; font-weight: bold;">那大圖呢？你想怎麼做呢？有什麼樣的建議呢？</span>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>壓字&nbsp;</label>
                        <div class="data-input rd-data-input">
                            <asp:CheckBox ID="chkPicARTDecide" runat="server" />尊重專業，由ART決定<br />
                            <asp:CheckBox ID="chkPicHotDeal" runat="server" />熱賣商品
                            <asp:CheckBox ID="chkPicAvgPrice" runat="server" />平均XXX元/單位
                            <asp:CheckBox ID="chkPicBuyOneGetOne" runat="server" />買一送一
                            <asp:CheckBox ID="chkPicSeasonsLimited" runat="server" />季節限定<br />
                            <asp:CheckBox ID="chkPicExclusive" runat="server" />限量商品
                            <asp:CheckBox ID="chkPicHotSelling" runat="server" />暢銷熱賣
                            <asp:CheckBox ID="chkPicMakeAvailable" runat="server" />買到賺到<br />
                            <asp:CheckBox ID="chkPicOther" runat="server" />其他：<asp:TextBox ID="TextBox33" runat="server" CssClass="input-small" Style="width: 400px" placeholder="為使大圖好看，字數請勿超過10個字！請注意錯字！"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        關於大圖與內文圖的其他想法與建議：<span style="color: #ccc; font-weight: bold;">(本欄送出之後即會被鎖住)</span>
                    </div>
                    <div class="form-unit">
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtPic" runat="server"  TextMode="MultiLine" Height="80px" Width="80%" placeholder="有寫有機會，沒寫就讓專業的來！怎麼寫？例如：說明圖的顏色請盡量使用暖色系，要有小孩....營造出溫馨的樣子；大圖請使用XXXX.jpg....."></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div id="dealDate">
                <h1 class="rd-smll">時程資訊</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <asp:PlaceHolder ID="phdEarlierPageCheck" runat="server">
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                <asp:CheckBox ID="chkEarlierPageCheck" runat="server" ClientIDMode="Static" />申請提前</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="txtEarlierPageCheckDay" runat="server" CssClass="input-small" Style="width: 50px" ClientIDMode="Static" MaxLength="3"></asp:TextBox>天頁確
                                <p id="pEarlierPageCheck" class="if-error" style="display: none;">請輸入正確數字。</p>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            建議的上檔時間</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtScheduleExpect" runat="server" CssClass="input-small" Style="width: 200px"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit ppon travelview" style="display: none;">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;活動使用有效時間</label>
                        <div class="data-input rd-data-input">
                            <p class="range1">活動開始兌換後</p>
                            <p class="range2">
                                <asp:TextBox ID="txtStartUse2" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 130px" MaxLength="30"></asp:TextBox>
                                ~ </p>
                            <p>
                                <asp:TextBox ID="txtStartUse" runat="server" ClientIDMode="Static" CssClass="input-small" Style="width: 130px" MaxLength="30"></asp:TextBox>
                                <asp:DropDownList ID="ddlStartUseUnit" ClientIDMode="Static" runat="server" Width="50px" onchange="StartUseChange(this);"></asp:DropDownList>
                            </p>
                            <p>
                                皆可使用
                            </p>
                            <p id="pStartUse" class="if-error" style="display: none;">請輸入活動使用有效時間。</p>
                            <p id="pStartUseError" class="if-error" style="display: none;">活動使用有效時間輸入錯誤，僅限輸入數字。</p>
                            <p id="pStartUseDateError" class="if-error" style="display: none;">活動使用有效時間數字格式錯誤(ex:yyyy/mm/dd)。</p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="dealInfo">
                <h1 class="rd-smll">商品資訊</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit consumertime" style="display:none">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;時段與消費</label>
                        <div class="data-input rd-data-input">
                            <asp:CheckBox ID="chkWeekdays" runat="server" ClientIDMode="Static" class="consumer" />
                            <asp:CheckBox ID="chkHoliday" runat="server" ClientIDMode="Static" class="consumer" />
                            <asp:CheckBox ID="chkSpecialHoliday" runat="server"  ClientIDMode="Static" class="consumer" />
                            <asp:CheckBox ID="chkBreakfast" runat="server"  ClientIDMode="Static" class="consumer" />
                            <asp:CheckBox ID="chkLunch" runat="server"  ClientIDMode="Static" class="consumer" />
                            <asp:CheckBox ID="chkDinner" runat="server" ClientIDMode="Static" class="consumer" />
                            <asp:CheckBox ID="chkAfternoonTea" runat="server" ClientIDMode="Static" class="consumer" />
                            <asp:CheckBox ID="chkNightSnack" runat="server" ClientIDMode="Static" class="consumer" />
                            <asp:CheckBox ID="chkHolidayFare" runat="server" ClientIDMode="Static" class="consumer" />
                            <br />
                            <asp:CheckBox ID="chkMinimumCharge" runat="server" ClientIDMode="Static" class="consumer" />
                            <asp:CheckBox ID="chkServiceCharge" runat="server" ClientIDMode="Static" class="consumer" />
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            特殊標記</label>
                        <div class="data-input rd-data-input">
                            <asp:CheckBox ID="chkFirstDeal" runat="server" ClientIDMode="Static" Enabled="false" />
                            <asp:CheckBox ID="chkUrgent" runat="server" ClientIDMode="Static" Enabled="false" />
                            <asp:CheckBox ID="chkFreeTax" runat="server" />
                            <asp:CheckBox ID="chkParallelImportation" runat="server" />
                             <asp:CheckBox ID="chkYearContract" runat="server" ClientIDMode="Static" />
                            <asp:CheckBox ID="chkNewItem" runat="server" ClientIDMode="Static" />
                            <asp:CheckBox ID="chkOnlineUse" runat="server" />
                            <asp:CheckBox ID="chkExclusive" runat="server" ClientIDMode="Static" />
                            <asp:CheckBox ID="chkEveryDayNewDeal" runat="server" ClientIDMode="Static" />
                            <br />
                            <asp:CheckBox ID="chkSpecialPrice" runat="server" ClientIDMode="Static" />
                            <asp:CheckBox ID="hidPhotographerAppoint" runat="server" ClientIDMode="Static" style="display:none" />
                            <asp:CheckBox ID="hidPhotographerAppointCheck" runat="server" ClientIDMode="Static" style="display:none" />
                            <asp:CheckBox ID="hidPhotographerAppointCancel" runat="server" ClientIDMode="Static" style="display:none" />
                            <asp:CheckBox ID="hidPhotographerAppointClose" runat="server" ClientIDMode="Static" style="display:none" />
                            <br />
                            <%--<asp:CheckBox ID="chkTravel" runat="server" ClientIDMode="Static" onclick="CheckSpecialFlag(this, 'Travel');" />--%>
                            <%--<asp:CheckBox ID="chkBeauty" runat="server" ClientIDMode="Static" onclick="CheckSpecialFlag(this, 'Beauty');" />
                            <asp:CheckBox ID="chkExpiringProduct" runat="server" ClientIDMode="Static" onclick="CheckSpecialFlag(this, 'ExpiringProduct');" />
                            <asp:CheckBox ID="chkInspectRule" runat="server" ClientIDMode="Static" onclick="CheckSpecialFlag(this, 'InspectRule');" />--%>
                            <asp:CheckBox ID="chkInvoiceFounder" runat="server" ClientIDMode="Static" onclick="CheckSpecialFlag(this, 'InvoiceFounder');" />
                            <asp:CheckBox ID="chkTaiShinChosen" runat="server"/>

                        </div>
                    </div>
                    <asp:Panel ID="divSpecialRequirement" CssClass="form-unit" runat="server">
                        <label class="unit-label rd-unit-label">特殊需求</label>
                        <div class="data-input rd-data-input">
                            <asp:CheckBox ID="chkLowGrossMarginAllowedDiscount" runat="server" ClientIDMode="Static" Visible="false" />
                            <asp:CheckBox ID="chkNotAllowedDiscount" runat="server" ClientIDMode="Static" />
                        </div>
                    </asp:Panel>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;照片來源</label>
                        <div class="data-input rd-data-input">
                            <asp:CheckBox ID="chkPhotographers" runat="server" ClientIDMode="Static"  />
                            <asp:CheckBox ID="chkSellerPhoto" runat="server" ClientIDMode="Static" />
                            <asp:CheckBox ID="chkFTPPhoto" runat="server" ClientIDMode="Static" onclick="CheckSpecialFlag(this, 'FTPPhoto');" />
                            <asp:CheckBox ID="chkSystemPic" runat="server" ClientIDMode="Static" />
                            <asp:HyperLink ID="btnPhotoHelper" runat="server" ClientIDMode="Static" class="btn btn-primary rd-mcacstbtn form-inline-btn" style="color:#fff">約拍小幫手</asp:HyperLink>
                        </div>
                    </div>
                    <div id="divPhotoResult" class="form-unit" style="display:none">
                  
                        <div id="photoToShopRes" class="data-input rd-data-input">
                            <p style="color:red">約定拍攝狀態</p>
                            <span style="color:red" class="spanPhotoStatus"></span>
                            <br />
                            <p>拍攝地點</p>
                            <span id="spanPhotoAddress"></span>
                            <br />
                            <p>現場聯絡人姓名</p>
                            <span id="spanPhotoContact"></span><span id="spanPhotoSex"></span>
                            <br />
                            <p>現場聯絡人電話</p>
                            <span id="spanPhotoTel"></span>
                            <br />
                            <p>現場聯絡人手機</p>
                            <span id="spanPhotoPhone"></span>
                            <br />
                            <p>建議重點拍攝項目</p>
                            <span id="spanPhotoMemo"></span>
                            <br />
                            <p>店家希望拍照時間</p>
                            <span id="spanPhotoDate"></span>
                            <br />
                            <p>參考網址</p>
                            <span id="spanPhotoUrl"></span>
                        </div>
                        <div id="photoToHouseRes" class="data-input rd-data-input">
                            <p style="color:red">約定拍攝狀態</p>
                            <span style="color:red" class="spanPhotoStatus"></span>
                            <br />
                            <p>拍攝需求</p>
                            <span id="spanPhohoNeeds"></span>
                            <br />
                            <p>參考資料</p>
                            <span id="spanRefrenceData"></span>
                            <br />
                        </div>
                        <hr class="header_hr">
                        <div id="photographerAppointRes" class="data-input rd-data-input" style="display:none">
                            <p>約定拍攝日期</p>
                            <span id="spanPhotographerAppointDate"></span>
                            <br />
                            <p>約定拍攝時間</p>
                            <span id="spanPhotographerAppointTime"></span>
                            <br />
                            <p>備註</p>
                            <span id="spanPhotographerAppointMemo"></span>
                            <br />
                        </div>
                    </div>
                    <div id="divPhotographersNew" class="form-unit" style="display: none; height: 500px; width: 700px;">
                        <div id="btn-menu">
                            <asp:Button ID="btnFloatSave" runat="server" CssClass="btn btn-primary rd-mcacstbtn btn-circle" Text="儲存" OnClientClick="onlySavePhotographer()" ClientIDMode="Static" />
                        </div>
                        <label class="unit-label rd-unit-label">
                            攝影註記</label>
                        <div class="data-input rd-data-input">
                            <div id="photoToShop">
                                <table style="width:80%">
                                    <tr>
                                        <td><span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;拍攝地點</td>
                                        <td><asp:TextBox ID="txtPhotoAddress" runat="server" ClientIDMode="Static" Width="60%" class="inputStyle"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;現場聯絡人姓名</td>
                                        <td><asp:TextBox ID="txtPhotoContact" runat="server" ClientIDMode="Static" Width="40%" class="inputStyle"></asp:TextBox>
                                            <div style="position:relative;">
                                                <div class="" id="divGender"></div>
                                                <asp:RadioButton ID="radMale" runat="server"  ClientIDMode="Static" GroupName="PhotoSex" class="inputStyle" />先生
                                                <asp:RadioButton ID="radFemale" runat="server"  ClientIDMode="Static" GroupName="PhotoSex" class="inputStyle" />小姐
                                            </div>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>現場聯絡人電話</td>
                                        <td><asp:TextBox ID="txtPhotoTel" runat="server" ClientIDMode="Static" placeholder="(02)2521-9131" Width="60%" class="inputStyle"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td><span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;現場聯絡人手機</td>
                                        <td><asp:TextBox ID="txtPhotoPhone" runat="server" ClientIDMode="Static" placeholder="0987-654-321" Width="60%" class="inputStyle"></asp:TextBox>
                                            <p id="pPhotoPhone" class="if-error" style="display: none;">請輸入現場聯絡人手機。</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;建議重點拍攝項目</td>
                                        <td><asp:TextBox ID="txtPhotoMemo" runat="server" ClientIDMode="Static" class="inputStyle" TextMode="MultiLine" style="height:80px;width:100%;"></asp:TextBox>
                                            <p id="pPhotoMemo" class="if-error" style="display: none;">請輸入建議重點拍攝項目。</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>店家希望拍照時間</td>
                                        <td><asp:TextBox ID="txtPhotoDate" runat="server" ClientIDMode="Static" Width="60%" class="inputStyle"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>參考網址 / 資料</td>
                                        <td><asp:TextBox ID="txtPhotoUrl" runat="server" ClientIDMode="Static" class="inputStyle" TextMode="MultiLine" style="height:80px;width:100%;"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="photoToHouse">
                                <table style="width:80%">
                                    <tr>
                                        <td>拍攝需求</td>
                                        <td><asp:TextBox ID="txtPhohoNeeds" runat="server" ClientIDMode="Static" TextMode="MultiLine" Height="60px" Width="80%" class="inputStyle"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>參考資料</td>
                                        <td><asp:TextBox ID="txtRefrenceData" runat="server" ClientIDMode="Static" TextMode="MultiLine" Height="60px" Width="80%" class="inputStyle"></asp:TextBox></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="form-unit end-unit">
                                <div class="data-input rd-data-input">
                                    <asp:Button ID="btnPhotographerAppoint" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="我要約拍" OnClientClick="savePhotographer()" ClientIDMode="Static"  />
                                    <asp:Button ID="btnPhotographerEdit" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="我要修改資料" OnClientClick="editPhotographer()" ClientIDMode="Static" style="display:none"  />
                                    <asp:Button ID="btnPhotographerSave" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="確認修改" OnClientClick="savePhotographer()" ClientIDMode="Static" style="display:none"  />
                                    <asp:Button ID="btnPhotographerCancel" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="取消修改" OnClientClick="cancelPhotographer()" ClientIDMode="Static" style="display:none"  />
                                    <asp:Button ID="btnClosePhotographer" runat="server" CssClass="btn btn-default rd-mcacstbtn" Text="取消" OnClientClick="closePhotographer()" ClientIDMode="Static"  />
                                </div>
                            </div>
                            <hr class="header_hr">
                            <div id="photographerAppoint" style="display:none">
                                <table style="width:80%">
                                    <tr>
                                        <td>約定拍攝日期</td>
                                        <td><asp:TextBox ID="txtPhotographerAppointDate" runat="server" ClientIDMode="Static" Width="60%" class="inputStyleP" ></asp:TextBox>
                                            <input type="checkbox" id="chkPhotographerAppointCancel" />取消拍攝
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>約定拍攝時間</td>
                                        <td>
                                            <asp:DropDownList ID="ddlPhotographerAppointHour" runat="server"></asp:DropDownList>
                                            <asp:DropDownList ID="ddlPhotographerAppointMinute" runat="server"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>指派攝影師</td>
                                        <td><asp:DropDownList ID="ddlPhotographerAppointer" runat="server" ClientIDMode="Static" Width="60%" ></asp:DropDownList>
                                            <asp:Button ID="btnPhotographerAppointMail" runat="server" CssClass="btn btn-default rd-mcacstbtn" Text="通知攝影師" ClientIDMode="Static"  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>備註</td>
                                        <td><asp:TextBox ID="txtPhotographerAppointMemo" runat="server" ClientIDMode="Static"  TextMode="MultiLine" style="height:80px;width:100%;" class="inputStyleP"></asp:TextBox></td>
                                    </tr>
                                </table>
                                <div class="form-unit end-unit">
                                    <div class="data-input rd-data-input">
                                        <asp:Button ID="btnPhotographerAppointCheck" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="約拍確認" OnClientClick="savePhotographerAppoint()" ClientIDMode="Static"  />
                                        <asp:Button ID="btnPhotographerAppointEdit" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="我要修改資料" OnClientClick="editPhotographerAppoint()" ClientIDMode="Static" style="display:none"  />
                                        <asp:Button ID="btnPhotographerAppointSave" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="儲存" OnClientClick="savePhotographerAppoint()" ClientIDMode="Static" style="display:none"  />
                                        <asp:Button ID="btnPhotographerAppointCancel" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="取消修改" OnClientClick="cancelPhotographerAppoint()" ClientIDMode="Static" style="display:none"  />
                                        <asp:Button ID="btnPhotographerAppointClose" runat="server" CssClass="btn btn-default rd-mcacstbtn" Text="取消" OnClientClick="closePhotographer()" ClientIDMode="Static"  />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="divProposalDraw" class="form-unit" style="display: none; height: 300px; width: 700px;">
                        抽件原因：<br />
                        <asp:TextBox ID="txtProposalDrawReason" runat="server" CssClass="input-half" TextMode="MultiLine" Height="60px" Width="80%" ClientIDMode="Static" placeholder="限250字內。"></asp:TextBox><br />
                        <input type="button" class="btn btn-default rd-mcacstbtn" value="取消" onclick="closePhotographer()" />
                        <asp:Button ID="btnRealProposalDraw" runat="server" CssClass="btn btn-default rd-mcacstbtn" Text="抽!" OnClientClick="return ProposalDraw()" ClientIDMode="Static" />
                    </div>
                    <div id="divFTPPhoto" class="form-unit" style="display: none;">
                        <label class="unit-label rd-unit-label">
                            舊照片的檔次BID</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtFTPPhoto" runat="server" ClientIDMode="Static" Width="200px"></asp:TextBox>
                            <p class="note">需填入指定檔次BID</p>
                            <br />
                            <p id="pFTPPhoto" class="if-error" style="display: none;">請輸入FTP舊照片BID。</p>
                        </div>
                    </div>
                    <div id="divdealstar" class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;本檔主打星</label>
                        <div class="data-input rd-data-input">
                            <p class="note">強打的商品名稱、顏色、口味、菜名.....</p>
                            <br />
                            <asp:CheckBox ID="chkDealStar" runat="server" ClientIDMode="Static" onclick="Enabletxt(this, 'DealStar');"/>無
                            <br />
                            <asp:TextBox ID="txtDealStar" ClientIDMode="Static" runat="server" TextMode="MultiLine" Height="60px" Width="100%" MaxLength="1000"></asp:TextBox>
                            <br />
                            <p id="pDealStar" class="if-error" style="display: none;">請輸入本檔主打星。</p>
                        </div>
                    </div>
                    <div id="divDealCharacterToHouse" class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;檔次特色</label>
                        <div class="data-input rd-data-input">
                            <p class="note">例如：燜燒鍋快速方便好清洗....，還有可以註明文案、大圖想要呈現的重點.....</p>
                            <br />
                            <asp:CheckBox ID="chkDealCharacterToHouse" runat="server" ClientIDMode="Static" onclick="Enabletxt(this, 'DealCharacterToHouse');"/>無
                            <br /> 
                        </div>
                        <table style="width: 100%">
                            <tr>
                                <td class="unit-label rd-unit-label"><span style="color: #BF0000; font-weight: bold;">*</span>1</td>
                                <td style="width: 100%;padding-bottom:5px" align="right">
                                    <asp:TextBox ID="txtDealCharacterToHouse1" ClientIDMode="Static" runat="server" TextMode="MultiLine" Width="95%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="unit-label rd-unit-label"><span style="color: #BF0000; font-weight: bold;">*</span>2</td>
                                <td style="width: 100%;padding-bottom:5px" align="right">
                                    <asp:TextBox ID="txtDealCharacterToHouse2" ClientIDMode="Static" runat="server" TextMode="MultiLine" Width="95%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="unit-label rd-unit-label"><span style="color: #BF0000; font-weight: bold;">*</span>3</td>
                                <td style="width: 100%;padding-bottom:5px" align="right">
                                    <asp:TextBox ID="txtDealCharacterToHouse3" ClientIDMode="Static" runat="server" TextMode="MultiLine" Width="95%"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="unit-label rd-unit-label">4</td>
                                <td style="width: 100%;padding-bottom:5px" align="right">
                                    <asp:TextBox ID="txtDealCharacterToHouse4" ClientIDMode="Static" runat="server" TextMode="MultiLine" Width="95%"></asp:TextBox></td>
                            </tr>
                        </table>
                        <div class="data-input rd-data-input">
                            <p id="pDealCharacterToHouse" class="if-error" style="display: none;">請輸入檔次特色。</p>
                        </div>
                    </div>
                    <div id="divDealCharacterToShop" class="form-unit">
                        <label class="unit-label rd-unit-label">
                            檔次特色</label><br />
                       <table style="width:100%">
                           <tr>
                               <td class="unit-label rd-unit-label">
                                   <span style="color: #BF0000; font-weight: bold">*</span><label id="lbldealcharacterToShop1" runat="server">1.</label>
                               </td>
                               <td style="width:90%" align="right">
                                   <asp:CheckBox ID="chkDealCharacterToShop1" runat="server" ClientIDMode="Static" onclick="Enabletxt(this, 'DealCharacterToShop1');"/>無
                                   <asp:TextBox ID="txtDealCharacterToShop1" ClientIDMode="Static" runat="server" TextMode="MultiLine" width="90%"></asp:TextBox>
                               </td>
                           </tr>
                           <tr>
                               <td class="unit-label rd-unit-label">
                                   <span style="color: #BF0000; font-weight: bold">*</span><label id="lbldealcharacterToShop2" runat="server">2.</label>
                               </td>
                               <td style="width:90%" align="right">
                                   <asp:CheckBox ID="chkDealCharacterToShop2" runat="server" ClientIDMode="Static" onclick="Enabletxt(this, 'DealCharacterToShop2');"/>無
                                   <asp:TextBox ID="txtDealCharacterToShop2" ClientIDMode="Static" runat="server" TextMode="MultiLine" width="90%"></asp:TextBox>
                               </td>
                           </tr>
                           <tr>
                               <td class="unit-label rd-unit-label">
                                   <span style="color: #BF0000; font-weight: bold">*</span><label id="lbldealcharacterToShop3" runat="server">3.</label>
                               </td>
                               <td style="width:90%" align="right">
                                   <asp:CheckBox ID="chkDealCharacterToShop3" runat="server" ClientIDMode="Static" onclick="Enabletxt(this, 'DealCharacterToShop3');"/>無
                                   <asp:TextBox ID="txtDealCharacterToShop3" ClientIDMode="Static" runat="server" TextMode="MultiLine" width="90%"></asp:TextBox>
                               </td>
                           </tr>
                           <tr>
                               <td class="unit-label rd-unit-label">
                                   <span style="color: #BF0000; font-weight: bold">*</span><label id="lbldealcharacterToShop4" runat="server">4.</label>
                               </td>
                               <td style="width:90%" align="right">
                                   <asp:CheckBox ID="chkDealCharacterToShop4" runat="server" ClientIDMode="Static" onclick="Enabletxt(this, 'DealCharacterToShop4');"/>無
                                   <asp:TextBox ID="txtDealCharacterToShop4" ClientIDMode="Static" runat="server" TextMode="MultiLine" width="90%"></asp:TextBox>
                               </td>
                           </tr>
                           <tr>
                               <td class="unit-label rd-unit-label">
                                   <label id="lbldealcharacterToShop5"><label>5.其他</label></label>
                               </td>
                               <td style="width:90%" align="right">
                                   <asp:TextBox ID="txtDealCharacterToShop5" ClientIDMode="Static" runat="server" TextMode="MultiLine" width="95%" Height="60px"></asp:TextBox>
                               </td>
                           </tr>
                       </table>
                        <div class="data-input rd-data-input">
                            <p id="pDealCharacterToShop" class="if-error" style="display: none;">請輸入檔次特色。</p>
                        </div>
                        
                    </div>
                    <div class="form-unit product" style="display: none;">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;規格功能/主要成份</label>
                        <div class="data-input rd-data-input">
                            <p class="note">尺寸、顏色、保存期限、產地、成份、使用方式、保固期限、注意事項....</p>
                            <asp:TextBox ID="txtProductSpec" ClientIDMode="Static" runat="server" TextMode="MultiLine" Height="80px" Width="80%"></asp:TextBox>
                            <br />
                            <p id="pProductSpec" class="if-error" style="display: none;">請輸入商品規格/功能。</p>
                        </div>
                    </div>
                    

                    <div id="divBeauty" class="form-unit ToHouse">
                        <label class="unit-label rd-unit-label">
                            <asp:CheckBox ID="chkBeauty" runat="server" ClientIDMode="Static" Text="醫療 / 妝廣字號" onclick="CheckSpecialFlag2(this, 'Beauty');" /></label>
                        <div id="CheckSpecialFlag2Beauty" class="data-input rd-data-input">
                            <p class="note" style="display:none">如: 主要成分、核准字號、主要成分、核准字號、主要成分、核准字號</p>
                            <asp:TextBox ID="txtBeauty" runat="server" ClientIDMode="Static" TextMode="MultiLine" Width="80%"></asp:TextBox>
                            
                            <br />
                            <p id="pBeauty" class="if-error" style="display: none;">請輸入主要成分/核准字號。</p>
                        </div>
                    </div>
                    <div id="divExpiringProduct" class="form-unit ToHouse">
                        <label class="unit-label rd-unit-label">
                        <asp:CheckBox ID="chkExpiringProduct" runat="server" ClientIDMode="Static" Text="即期品" onclick="CheckSpecialFlag2(this, 'ExpiringProduct');" /></label>
                        <div id="CheckSpecialFlag2ExpiringProduct" class="data-input rd-data-input">
                            <p class="note">小於保存期限的三分之一</p>
                            <asp:TextBox ID="txtExpiringProduct" runat="server" ClientIDMode="Static" TextMode="MultiLine" Width="80%"></asp:TextBox>
                            <br />
                            <p id="pExpiringProduct" class="if-error" style="display: none;">請輸入即期品說明。</p>
                        </div>
                    </div>
                    <div id="divInspectRule" class="form-unit ToHouse">
                        <label class="unit-label rd-unit-label">
                        <asp:CheckBox ID="chkInspectRule" runat="server" ClientIDMode="Static" Text="檢驗規定" onclick="CheckSpecialFlag2(this, 'InspectRule')" /></label>
                        <div id="CheckSpecialFlag2InspectRule" class="data-input rd-data-input">
                            <p class="note">例如：BSMI、SGS、NCC證號</p>
                            <asp:TextBox ID="txtInspectRule" runat="server" ClientIDMode="Static" TextMode="MultiLine" Width="80%"></asp:TextBox>
                            <br />
                            <p id="pInspectRule" class="if-error" style="display: none;">請輸入檢驗規定。</p>
                        </div>
                    </div>
                    <div id="divRecommendFood" class="form-unit travel">
                        <label class="unit-label rd-unit-label">
                            <asp:CheckBox ID="chkRecommendFood" runat="server" ClientIDMode="Static" onclick="CheckSpecialFlag2(this, 'RecommendFood');"/>推薦菜色/食材</label>
                        <div id="CheckSpecialFlag2RecommendFood" class="data-input rd-data-input">
                            <p class="note">特殊食材、烹調方式、商品的背後故事</p>
                            <asp:TextBox ID="txtRecommendFood" runat="server" ClientIDMode="Static" TextMode="MultiLine" Height="60px" Width="80%"></asp:TextBox>
                            <br />
                            <p id="pRecommendFood" class="if-error" style="display: none;">請輸入檢驗規定。</p>
                        </div>
                    </div>
                    <div id="divEnvironmentIntroduction" class="form-unit travel">
                        <label class="unit-label rd-unit-label">
                            <asp:CheckBox ID="chkEnvironmentIntroduction" runat="server" ClientIDMode="Static" onclick="CheckSpecialFlag2(this, 'EnvironmentIntroduction');"/>環境介紹</label>
                        <div id="CheckSpecialFlag2EnvironmentIntroduction" class="data-input rd-data-input">
                            <p class="note">主題風格、裝潢佈置、特有空間</p>
                            <asp:TextBox ID="txtEnvironmentIntroduction" runat="server" ClientIDMode="Static" TextMode="MultiLine" Height="60px" Width="80%"></asp:TextBox>
                            <br />
                            <p id="pEnvironmentIntroduction" class="if-error" style="display: none;">請輸入檢驗規定。</p>
                        </div>
                    </div>
                    <div id="divAroundScenery" class="form-unit travel">
                        <label class="unit-label rd-unit-label">
                            <asp:CheckBox ID="chkAroundScenery" runat="server" ClientIDMode="Static" onclick="CheckSpecialFlag2(this, 'AroundScenery');"/>周邊景點</label>
                        <div id="CheckSpecialFlag2AroundScenery" class="data-input rd-data-input">
                            <p class="note">指定需放上之周邊景點</p>
                            <asp:TextBox ID="txtAroundScenery" runat="server" ClientIDMode="Static" TextMode="MultiLine" Height="60px" Width="80%"></asp:TextBox>
                            <br />
                            <p id="pAroundScenery" class="if-error" style="display: none;">請輸入檢驗規定。</p>
                        </div>
                    </div>
                    <div id="divSeasonActivity" class="form-unit travel">
                        <label class="unit-label rd-unit-label">
                            <asp:CheckBox ID="chkSeasonActivity" runat="server" ClientIDMode="Static" onclick="CheckSpecialFlag2(this, 'SeasonActivity');"/>季節/配合活動</label>
                        <div id="CheckSpecialFlag2SeasonActivity" class="data-input rd-data-input">
                            <p class="note">例如：日月潭檔次須置入『2015日月潭國際花火音樂暨自行車嘉年華』，活動期間和官方網站</p>
                            <asp:TextBox ID="txtSeasonActivity" runat="server" ClientIDMode="Static" TextMode="MultiLine" Height="60px" Width="80%"></asp:TextBox>
                            <br />
                            <p id="pSeasonActivity" class="if-error" style="display: none;">請輸入檢驗規定。</p>
                        </div>
                    </div>
                    <div id="divStoreStory" class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <asp:CheckBox ID="chkStoreStory" runat="server" ClientIDMode="Static" onclick="CheckSpecialFlag2(this, 'StoreStory');"/>店家/品牌故事</label>
                        <div id="CheckSpecialFlag2StoreStory" class="data-input rd-data-input">
                            <asp:TextBox ID="txtStoreStory" runat="server" ClientIDMode="Static" TextMode="MultiLine" Height="60px" Width="80%"></asp:TextBox>
                            <br />
                            <p id="pStoreStory" class="if-error" style="display: none;">請輸入檢驗規定。</p>
                        </div>
                    </div>
                    <div id="divMediaReport" class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <asp:CheckBox ID="chkMediaReport" runat="server" ClientIDMode="Static" onclick="CheckSpecialFlag2(this, 'MediaReport');"/>媒體/報導</label>
                        <div class="data-input rd-data-input">
                            <p class="note">媒體連結請給Youtube連結；報章媒體的報導圖檔請直接勾選下方選項，並將圖檔存放於指定的FTP</p><br />
                            <div id="divMediaReportPic" style="display:none">
                                <asp:CheckBox ID="chkMediaReportPic" runat="server" ClientIDMode="Static" onclick="Enabletxt(this, 'MediaReport');"/>報導圖檔：依FTP
                            </div>
                            <div id="divMediaReportLink" style="display:none" runat="server" ClientIDMode="Static">
                                <div id="TextValueDiv"> 
                                    <div id="divMediaReportLink1" runat="server">
                                        <asp:TextBox ID="txtMediaReport1" style="width:70%" runat="server" ClientIDMode="Static" ></asp:TextBox>
                                        <asp:RadioButton ID="rdbMediaReportIn1" GroupName="rdbMediaReport1" Text="崁入" Checked="true" runat="server" ClientIDMode="Static" />
                                        <asp:RadioButton ID="rdbMediaReportOut1" GroupName="rdbMediaReport1" Text="外連" runat="server" ClientIDMode="Static" />
                                    </div>
                                    <div id="divMediaReportLink2" runat="server" style="display:none" ClientIDMode="Static">
                                        <asp:TextBox ID="txtMediaReport2" style="width:70%" runat="server" ClientIDMode="Static" ></asp:TextBox>
                                        <asp:RadioButton ID="rdbMediaReportIn2" GroupName="rdbMediaReport2" Text="崁入" Checked="true" runat="server" ClientIDMode="Static" />
                                        <asp:RadioButton ID="rdbMediaReportOut2" GroupName="rdbMediaReport2" Text="外連" runat="server" ClientIDMode="Static" />
                                        <img id='imgMediaReportDelete2' src='../Themes/default/images/17Life/G2/delete.png' alt='刪除' title='刪除' class='view' onclick='DeleteTextbox(this);'  />
                                    </div>
                                    <div id="divMediaReportLink3" runat="server" style="display:none" ClientIDMode="Static">
                                        <asp:TextBox ID="txtMediaReport3" style="width:70%" runat="server" ClientIDMode="Static" ></asp:TextBox>
                                        <asp:RadioButton ID="rdbMediaReportIn3" GroupName="rdbMediaReport3" Text="崁入" Checked="true" runat="server" ClientIDMode="Static" />
                                        <asp:RadioButton ID="rdbMediaReportOut3" GroupName="rdbMediaReport3" Text="外連" runat="server" ClientIDMode="Static" />
                                        <img id='imgMediaReportDelete3' src='../Themes/default/images/17Life/G2/delete.png' alt='刪除' title='刪除' class='view' onclick='DeleteTextbox(this);'  />
                                    </div>
                                    <div id="divMediaReportLink4" runat="server" style="display:none" ClientIDMode="Static">
                                        <asp:TextBox ID="txtMediaReport4" style="width:70%" runat="server" ClientIDMode="Static" ></asp:TextBox>
                                        <asp:RadioButton ID="rdbMediaReportIn4" GroupName="rdbMediaReport4" Text="崁入" Checked="true" runat="server" ClientIDMode="Static" />
                                        <asp:RadioButton ID="rdbMediaReportOut4" GroupName="rdbMediaReport4" Text="外連" runat="server" ClientIDMode="Static" />
                                        <img id='imgMediaReportDelete4' src='../Themes/default/images/17Life/G2/delete.png' alt='刪除' title='刪除' class='view' onclick='DeleteTextbox(this);'  />
                                    </div>
                                    <div id="divMediaReportLink5" runat="server" style="display:none" ClientIDMode="Static">
                                        <asp:TextBox ID="txtMediaReport5" style="width:70%" runat="server" ClientIDMode="Static" ></asp:TextBox>
                                        <asp:RadioButton ID="rdbMediaReportIn5" GroupName="rdbMediaReport5" Text="崁入" Checked="true" runat="server" ClientIDMode="Static" />
                                        <asp:RadioButton ID="rdbMediaReportOut5" GroupName="rdbMediaReport5" Text="外連" runat="server" ClientIDMode="Static" />
                                        <img id='imgMediaReportDelete5' src='../Themes/default/images/17Life/G2/delete.png' alt='刪除' title='刪除' class='view' onclick='DeleteTextbox(this);'  />
                                    </div>
                                </div>
                                <div id="testdiv" runat="server">

                                </div>
                            
                                <input id="hidMediaReport" type="hidden" value="2" runat="server" clientidmode="Static"/>
                                <br />
                                <img id="imgMediaReportAdd" src="../Themes/default/images/17Life/G2/add.png" alt="新增" title="新增" style="padding-bottom:15px;"  class="view" onclick="addTextbox();" runat="server" clientidmode="Static"  />
                                <br />
                                <p id="pMediaReport" class="if-error" style="display: none;">請輸入檢驗規定。</p>
                            </div>
                        </div>
                    </div>
                    <div id="divTravel" class="form-unit travel">
                        <label class="unit-label rd-unit-label">
                            <asp:CheckBox ID="chkTravel" runat="server" ClientIDMode="Static" Text="旅遊業登記" onclick="CheckSpecialFlag2(this, 'Travel');"/></label>
                        <div id="CheckSpecialFlag2Travel" class="data-input rd-data-input">
                            <asp:TextBox ID="txtTravel" runat="server" ClientIDMode="Static" TextMode="MultiLine" Height="60px" Width="80%"></asp:TextBox>
                            <br />
                            <p id="pTravel" class="if-error" style="display: none;">請輸入旅遊登記字號。</p>
                        </div>
                    </div>
                    <div id="divInvoiceFounder" class="form-unit" style="display: none;">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;發票開立人</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtInvoiceFounder" runat="server" ClientIDMode="Static" TextMode="MultiLine" Height="60px" Width="80%"></asp:TextBox>
                            <p class="note">如: 須填寫公司名稱、統編 / ID、負責人；同受款人則需填寫負責人；同簽約公司免填。</p>
                            <br />
                            <p id="pInvoiceFounder" class="if-error" style="display: none;">請輸入發票開立人。</p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            合約附註說明</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtContractMemo" runat="server" TextMode="MultiLine" Height="80px" Width="80%" MaxLength="80"></asp:TextBox>
                            <p class="note" style="color:#BF0000">需列印於合約說明內容者</p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            備註說明</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtMemo" runat="server" TextMode="MultiLine" Height="80px" Width="80%"></asp:TextBox>
                            <p class="note" style="color:#BF0000">此欄位註記不會列印在合約裡</p>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div id="Keyword">
                <h1 class="rd-smll">上檔頻道 & 搜尋關鍵字</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div id="divChannelList" class="form-unit" style="margin-left:55px">
                        <asp:Repeater ID="rptChannel" runat="server" OnItemDataBound="rptChannel_ItemDataBound">
                            <ItemTemplate>
                                <div class="form-unit">
                                    <label class="unit-label rd-unit-label pChannel" style="float: none">
                                        <asp:HiddenField ID="hidChannel" runat="server" />
                                        <asp:CheckBox ID="chkChannel" runat="server" onclick="selectChannel(this);" />
                                    </label>
                                    <div class="data-input rd-data-input pArea" style="display: none;">
                                        <ul>
                                            <asp:Repeater ID="rptArea" runat="server" OnItemDataBound="rptArea_ItemDataBound">
                                                <HeaderTemplate>
                                                    <li style="border-bottom: 1px solid #e4e4e4;">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <p class="pA">
                                                        <asp:HiddenField ID="hidArea" runat="server" />
                                                        <asp:CheckBox ID="chkArea" runat="server" onclick="selectArea(this);" />
                                                    </p>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </li>
                                                </FooterTemplate>
                                            </asp:Repeater>

                                            <asp:Repeater ID="rptArea2" runat="server" OnItemDataBound="rptArea2_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr style="background-color: #f2f2f0" >
                                                        <td colspan="2">
                                                            <p class="pCommercial<%#((CategoryNode)Container.DataItem).CategoryId %>" style="display: none;">
                                                                <label><%# ((CategoryNode)Container.DataItem).NodeDatas.Count > 0 ? ((CategoryNode)Container.DataItem).NodeDatas.FirstOrDefault().CategoryNodes.FirstOrDefault().CategoryName : "商圈‧景點：" %></label>
                                                                <asp:Repeater ID="rptCommercial" runat="server" OnItemDataBound="rptCommercial_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hidCommercial" runat="server" />
                                                                        <asp:CheckBox ID="chkCommercial" runat="server" onclick="selectCommercial(this);" />
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td style="width:90%">
                                                            <asp:Repeater ID="rptSPRegion" runat="server" OnItemDataBound="rptSPRegion_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <p class="pSpecialRegion<%#((CategoryNode)Container.DataItem).CategoryId %>" style="display: none;">
                                                                        <asp:Repeater ID="rptSpecialRegion" runat="server" OnItemDataBound="rptSpecialRegion_ItemDataBound">
                                                                            <ItemTemplate>
                                                                                <asp:HiddenField ID="hidSpecialRegion" runat="server" />
                                                                                <asp:CheckBox ID="chkSpecialRegion" runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </p>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            

                                            <asp:Repeater ID="rptCategory" runat="server" OnItemDataBound="rptCategory_ItemDataBound">
                                                <HeaderTemplate>
                                                    <li style="border-bottom: 1px solid #e4e4e4;">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <p class="pB">
                                                        <asp:HiddenField ID="hidCategory" runat="server" />
                                                        <asp:CheckBox ID="chkCategory" runat="server" onclick="selectCategory(this);" />
                                                        <asp:HiddenField ID="hidIsShowBackEnd" runat="server" />
                                                    </p>
                                                    <p class="pCategory" style="display: none;">
                                                        <asp:Repeater runat="server" ID="rptSubDealCategoryArea" OnItemDataBound="rptSubDealCategoryArea_ItemDataBound">
                                                            <ItemTemplate>
                                                                <span>
                                                                <asp:HiddenField ID="hidSubIsShowBackEnd" runat="server" />
                                                                <asp:HiddenField ID="hidSubDealCategory" runat="server" />
                                                                <asp:CheckBox ID="chkSubDealCategory" runat="server" />                                                                
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </p>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </li>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            搜尋關鍵字</label>
                        <div class="data-input rd-data-input">
                            <p>
                                <asp:TextBox ID="txtKeyword" runat="server" CssClass="input-half" Width="200px" ClientIDMode="Static"></asp:TextBox>
                            </p>
                            <p id="pKeyword" class="if-error" style="display: none; margin-top: 8px">請輸入搜尋關鍵字。</p>
                            <p class="note" style="line-height: 20px;">
                                ●  建議輸入內容 (請以 " / " 分隔)：<br />
                                1. 檔次名稱：建議針對品牌名稱輸入，且可拆開輸入會更容易搜尋的到，如義大天悅可輸入：義大/天悅<br />
                                2. 拆字分解：盡量地將字詞拆開，如義美巧克力酥片，可以拆分為 義美/巧克力/巧克力酥片<br />
                                3. 相關連結：若此檔次與特殊情境相關，可額外輸入，如宜蘭住宿檔次，可輸入＂童玩節＂等類似字詞<br />
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="AncestorBid">
                <h1 class="rd-smll">檔次續接設定</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            續接數量檔次</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtAncestorBid" runat="server" Width="250px"></asp:TextBox> * 請輸入上一檔次的Bid
                            
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            續接憑證檔次</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtAncestorSeqBid" runat="server" Width="250px"></asp:TextBox> * 請輸入上一檔次的Bid
                        </div>
                    </div>
                    <div class="form-unit end-unit">
                        <div class="data-input rd-data-input">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="儲存" OnClick="btnSave_Click" OnClientClick="return SaveCheck();" ClientIDMode="Static" />
                        </div>
                    </div>
                </div>
            </div>
            <div id="RestrictionsSet" runat="server">
                <h1 class="rd-smll">權益說明
                    <asp:HyperLink ID="hkRestrictionsEdit" runat="server" ClientIDMode="Static" ForeColor="#0088cc" Font-Size="Medium" Text="編輯" CssClass="Restrictions fancybox.iframe"></asp:HyperLink>
                </h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <div class="data-input rd-data-input" style="margin-left: 0px">
                            <p style="font-size: small; line-height: 20px;">
                                <asp:Label ID="liRestrictions" runat="server" ClientIDMode="Static"></asp:Label>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Panel ID="divAssign" runat="server" Visible="false">
                <h1 class="rd-smll" id="assign">工作指派</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <asp:Panel ID="divAssignText" runat="server" CssClass="data-input rd-data-input" Style="margin-left: 70px" Visible="false">
                            <p>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="float: left; margin-top: 6px;">
                                    <ContentTemplate>
                                        <br />
                                        <asp:TextBox ID="txtEmp" runat="server" ClientIDMode="Static" CssClass="input-half" Style="width: 200px"></asp:TextBox><span style="color:white">@17life.com</span>
                                        <input id="chkAssignSetting" type="checkbox" class="AssignFlag" value="<%=(int)ProposalEditorFlag.Setting %>" /><%= LunchKingSite.I18N.Phrase.ProposalEditorFlagSetting %>
                                        <input id="chkAssignCopyWriter" type="checkbox" class="AssignFlag" value="<%=(int)ProposalEditorFlag.CopyWriter %>" /><%= LunchKingSite.I18N.Phrase.ProposalEditorFlagCopyWriter %>
                                        <input id="chkAssignImageDesign" type="checkbox" class="AssignFlag" value="<%=(int)ProposalEditorFlag.ImageDesign %>" /><%= LunchKingSite.I18N.Phrase.ProposalEditorFlagImageDesign %>
                                        <input id="chkAssignART" type="checkbox" class="AssignFlag" value="<%=(int)ProposalEditorFlag.ART %>" /><%= LunchKingSite.I18N.Phrase.ProposalEditorFlagART %>
                                        <input id="chkAssignListing" type="checkbox" class="AssignFlag" value="<%=(int)ProposalEditorFlag.Listing %>" /><%= LunchKingSite.I18N.Phrase.ProposalEditorFlagListing %>
                                        <asp:HiddenField ID="hidAssignFlag" runat="server" ClientIDMode="Static" />
                                        <br />
                                        <span style="color: #BF0000; font-weight: bold;display:none" id="NoProductionEmp">查無此人</span>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <p>
                                </p>
                                <p>
                                    <asp:Button ID="btnAssign" runat="server" CssClass="btn btn-primary rd-mcacstbtn form-inline-btn AssignFlag" OnClick="btnAssign_Click" Text="指派" ClientIDMode="Static" />
                                </p>
                                <p>
                                </p>
                                <p>
                                </p>                                                       
                            </p>
                        </asp:Panel>
                        <asp:Repeater ID="rptAssignLogs" runat="server">
                            <HeaderTemplate>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li>
                                    <label class="unit-label rd-unit-label">
                                        <%# ((ProposalLog)Container.DataItem).CreateId.Replace("@17life.com.tw", string.Empty).Replace("@17life.com", string.Empty) %>&nbsp;&nbsp;:
                                    </label>
                                    <div class="data-input rd-data-input">
                                        <p>
                                            <%# ((ProposalLog)Container.DataItem).ChangeLog.Replace("|", "<br />") %>
                                        </p>
                                        <p class="note" style="float: right;"><%# ((ProposalLog)Container.DataItem).CreateTime.ToString("yyyy/MM/dd HH:mm") %></p>
                                        <p class="note" style="float: right; color: #BF0000;"><%# ((ProposalLog)Container.DataItem).Dept %></p>
                                    </div>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panSellerFile" runat="server" DefaultButton="btnChangeLog" Visible="false">
                <h1 class="rd-smll">檔案上傳</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <div id="dragandrophandler">請將檔案拖曳至此</div>
                        <asp:Repeater ID="rptSellerFile" runat="server"  OnItemDataBound="rptSellerFile_ItemDataBound">
                            <HeaderTemplate>
                                <div id="fileResutl" style="max-height:250px;overflow:auto;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table" id="tabFiles">
                                        <thead>
                                            <tr style="background-color:#444;color:#fff;height:30px">
                                                <th class="OrderDate"><input type="checkbox" value="true" onclick="checkAllFile(this)" id="checkAll" /></th>
                                                <th class="OrderDate">檔名</th>
                                                <th class="OrderDate">檔案大小</th>
                                                <th class="OrderDate">上傳日期</th>
                                                <th class="OrderDate">刪除</th>
                                            </tr>
                                        </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: left">
                                        <input type="checkbox" value="<%# ((ProposalSellerFile)Container.DataItem).Id %>" />
                                    </td>
                                    <td style="text-align: left">
                                        <a href="#" onclick="DownLoadSellerFile('<%# ((ProposalSellerFile)Container.DataItem).Id %>')"><%# ((ProposalSellerFile)Container.DataItem).OriFileName %></a>
                                    </td>
                                    <td style="text-align: left; word-break: break-all; max-width: 250px">
                                        <asp:Literal ID="liFileSize" runat="server"></asp:Literal>
                                    </td>
                                    <td style="text-align: left;">
                                        <%# ((ProposalSellerFile)Container.DataItem).CreateTime.ToString("yyyy/MM/dd HH:mm") %>
                                    </td>
                                    <td style="text-align: left;">
                                        <img id="deleteFile" src="../Themes/default/images/17Life/G2/trash.png" alt="刪除" title="刪除" style="padding-left: 5px" class="view" onclick="deleteSellerFile('<%# ((ProposalSellerFile)Container.DataItem).Id %>');">
                                    </td>
                                </tr>

                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                                
                                
                                <div style="width:100%;text-align:center">
                                    <input type="button" value="下載" onclick="DownloadFiles()" class="btn rd-mcacstbtn" />
                                </div>
                                
                                </div>
                            </FooterTemplate>
                        </asp:Repeater>
                        <div style="width: 100%; text-align: center">
                            <asp:CheckBox ID="chkFileDone" runat="server" Text="圖檔文件已上傳完整" />
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="divHistory" runat="server" DefaultButton="btnChangeLog">
                <h1 class="rd-smll">歷史紀錄&留言</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <div class="data-input rd-data-input" style="margin-left: 70px">
                            <asp:TextBox ID="txtChangeLog" runat="server" CssClass="input-half" ClientIDMode="Static" Style="margin-top: 10px"></asp:TextBox>
                            <asp:Button ID="btnChangeLog" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="留言" OnClick="btnChangeLog_Click" OnClientClick="return CheckLog();" />
                            <p id="pChangeLog" class="if-error" style="display: none; margin-top: 8px">請輸入內容。</p>
                        </div>
                        <asp:Repeater ID="rptLog" runat="server">
                            <HeaderTemplate>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table" id="tblProposalChangeLog">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="mc-tableContentITEM">
                                    <td style="text-align:left">
                                        <%# ((ProposalLog)Container.DataItem).CreateId.Replace("@17life.com.tw", string.Empty).Replace("@17life.com", string.Empty) %>
                                    </td>
                                    <td style="text-align:left;word-break: break-all;max-width:250px">
                                       <%# ((ProposalLog)Container.DataItem).ChangeLog.Replace("|", "<br />") %>
                                    </td>
                                    <td style="text-align:left;">
                                        <%# ((ProposalLog)Container.DataItem).CreateTime.ToString("yyyy/MM/dd HH:mm") %>
                                    </td>
                                    <td style="text-align:left;">
                                        <%# ((ProposalLog)Container.DataItem).Dept %>
                                    </td>
                                </tr>
                                
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </asp:Panel>
        </div>

        <div id="PponStoreSet" class="mc-content" style="display: none;">
            <asp:PlaceHolder ID="divPponStore" runat="server" Visible="false">
                <h1 class="rd-smll"><span class="breadcrumb flat">
                    <a href="#">營業據點</a>
                    <a href="#" class="active">提案管理</a>
                </span>
                    <a href="ProposalList.aspx" style="color: #08C;">回列表</a>
                </h1>
                <hr class="header_hr">
                <div id="mc-table" class="showtr">
                    <asp:Repeater ID="rptPponStore" runat="server" OnItemDataBound="rptPponStore_ItemDataBound">
                        <HeaderTemplate>
                            <table id="pponstore" width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                                <tr class="rd-C-Hide">
                                    <th class="OrderDate">
                                        <input type="checkbox" onclick="CheckAll(this);" id="Check" />
                                    </th>
                                    <th class="OrderDate">營業據點名稱
                                    </th>
                                    <th class="OrderSerial">地址
                                    </th>
                                    <th class="OrderName">營業時間
                                    </th>
                                    <th class="OrderDate">公休日
                                    </th>
                                </tr>
                                <tbody id="pponstorebody">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="mc-tableContentITEM">
                                <td class="OrderDate">
                                    <asp:HiddenField ID="hidStoreGuid" runat="server" />
                                    <asp:CheckBox ID="chkCheck" runat="server" />
                                </td>
                                <td class="OrderDate storename" style="text-align:left">
                                    <asp:Literal ID="liStoreName" runat="server"></asp:Literal>
                                </td>
                                <td class="OrderSerial" style="text-align:left">
                                    <p id="pAddress">
                                        <asp:Literal ID="liAddress" runat="server"></asp:Literal>
                                    </p>
                                    
                                </td>
                                <td class="OrderName" style="text-align:left">
                                    <span class="rd-Detailtitle">營業時間：</span>
                                    <asp:Literal ID="liOpenTime" runat="server"></asp:Literal>
                                </td>
                                <td class="OrderDate" style="text-align:left">
                                    <span class="rd-Detailtitle">公休日：</span>
                                    <asp:Literal ID="liCloseDate" runat="server"></asp:Literal>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                                </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="form-unit">
                    <div class="grui-form">
                        <label class="unit-label rd-unit-label"></label>
                        <div class="form-unit end-unit">
                            <div class="data-input rd-data-input">
                                <asp:Button ID="btnPponStoreSave" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="儲存" OnClick="btnPponStoreSave_Click" OnClientClick="return LocationSaveCheck();" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>

        <div id="VbsSet" class="mc-content" style="display: none;">
            <asp:PlaceHolder ID="divVbs" runat="server" Visible="false">
                <h1 class="rd-smll"><span class="breadcrumb flat">
                    <a href="#">核銷相關</a>
                    <a href="#" class="active">提案管理</a>
                </span>
                    <asp:CheckBox ID="chkIsShowVbsAccountID" runat="server" Text="顯示商家帳號" ClientIDMode="Static" />
                    <a href="ProposalList.aspx" style="color: #08C;">回列表</a>
                </h1>
                <hr class="header_hr">
                <div id="mc-table">
                    <asp:Repeater ID="rptVbs" runat="server" OnItemDataBound="rptVbs_ItemDataBound">
                        <HeaderTemplate>
                            <table id="vbs" width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                                <tr class="rd-C-Hide">
                                    <th class="OrderDate">
                                    </th>
                                    <th class="OrderSerial">已售/上限
                                    </th>
                                    <th class="OrderName">
                                        <input type="checkbox" onclick="CheckAll(this);" id="VerifyShop" />
                                        銷售店舖
                                    </th>
                                    <th class="OrderName">
                                        <input type="checkbox" onclick="CheckAll(this);" id="Verify" />
                                        憑證核實
                                    </th>
                                    <th class="OrderDate">
                                        <input type="checkbox" onclick="CheckAll(this);" id="Accouting" />
                                        匯款對象
                                    </th>
                                    <th class="OrderDate">
                                        <input type="checkbox" onclick="CheckAll(this);" id="ViewBalanceSheet" />
                                        檢視對帳單
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="mc-tableContentITEM">
                                <td style="text-align:left">
                                    <asp:Literal ID="liStoreName" runat="server"></asp:Literal>
                                    <br />
                                    <span id="IsShowVbsAccountID" style="display: none">
                                       <asp:Literal ID="liVbsAccountID" runat="server"></asp:Literal>
                                        <asp:Panel ID="divtooltip" runat="server" CssClass="tooltip" Visible="false">
                                            ...
                                        <asp:Literal ID="liMoreVbsAccountID" runat="server"></asp:Literal>
                                        </asp:Panel>
                                    </span>
                                    <asp:HiddenField ID="hidStoreGuid" runat="server" />
                                </td>
                                <td class="OrderDate">
                                    <asp:TextBox ID="txtQuantity" runat="server" Width="50px" onkeyup="return ValidateNumber($(this),value);" MaxLength="5"></asp:TextBox>
                                </td>
                                <td class="OrderDate">
                                    <span class="rd-Detailtitle">銷售店舖</span>
                                    <asp:CheckBox ID="chkVerifyShop" runat="server" />
                                </td>
                                <td class="OrderDate">
                                    <span class="rd-Detailtitle">憑證核實</span>
                                    <asp:CheckBox ID="chkVerify" runat="server" />
                                </td>
                                <td class="OrderDate">
                                    <span class="rd-Detailtitle">匯款對象</span>
                                    <asp:CheckBox ID="chkAccouting" runat="server" />
                                </td>
                                <td class="OrderDate">
                                    <span class="rd-Detailtitle">檢視對帳單</span>
                                    <asp:CheckBox ID="chkViewBalanceSheet" runat="server" CssClass="checkViewBalanceSheet" />
                                    <asp:Image ID="btnHideBalanceSheet" runat="server" CssClass="hideBalanceSheet" />
                                    <asp:HiddenField ID="hidIsHideBalanceSheet" runat="server"/>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="form-unit">
                    <div class="grui-form">
                        <label class="unit-label rd-unit-label"></label>
                        <div class="form-unit end-unit">
                            <div class="data-input rd-data-input">
                                <asp:Button ID="btnVbsSave" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="儲存" OnClick="btnVbsSave_Click" OnClientClick="return VbsSaveCheck();" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>

        <div id="ContractSet" class="mc-content" style="display: none;">
            <asp:PlaceHolder ID="divContract" runat="server" Visible="false">
                <h1 class="rd-smll"><span class="breadcrumb flat">
                    <a href="javascript:void(0);">合約上傳</a>
                    <a href="javascript:void(0);" class="active">提案管理</a>
                </span></h1>
                <hr class="header_hr">
                <div class="grui-form" id="contractfile-form">
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            類型</label>
                        <div class="data-input rd-data-input">
                            <asp:DropDownList ID="ddlContactType" runat="server" Width="180px">
                                    <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <p id="pContractFile" class="if-error" style="display: none;">請選擇合約類型。</p>
                            <p>
                                <label class="unit-label rd-unit-label">版號</label>
                                <asp:TextBox ID="txtContractVersion" runat="server" CssClass="input-half" ClientIDMode="Static" Style="width: 200px" MaxLength="20"></asp:TextBox>
                            </p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            備註/關鍵字</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtContractFileMemo" runat="server" CssClass="input-half" ClientIDMode="Static" Style="width: 200px" MaxLength="80"></asp:TextBox>
                            <p>
                                <label class="unit-label rd-unit-label">合約到期日</label>
                                <asp:TextBox ID="txtContractDueDate" runat="server" CssClass="input-half" ClientIDMode="Static" Style="width: 200px"></asp:TextBox>
                            </p>
                        </div>
                        
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            選擇檔案</label>
                        <div class="data-input rd-data-input">
                            <ul id="fileList">
                            </ul>
                            <input type="file" id="fileContract" />
                            <input type="button" id="btnFileUpload" class="btn btn-primary rd-mcacstbtn" value="上傳" onclick="ContractUpload()" />
                            <p>
                                <label class="unit-label rd-unit-label">
                                    <span style="color: #BF0000; font-weight: bold;">*</span>乙方聯絡人</label>
                                <asp:TextBox ID="txtContractPartyBContact" runat="server" CssClass="input-half" ClientIDMode="Static" Style="width: 200px"  MaxLength="50"></asp:TextBox>
                            </p>
                            <p id="pContractPartyBContact" class="if-error" style="display: none;">請輸入乙方聯絡人。</p>
                        </div>
                        
                    </div>
                </div>
                <div class="form-unit">
                    <div id="mc-table">
                        <asp:Repeater ID="rptContractFiles" runat="server">
                            <HeaderTemplate>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                                    <tr class="rd-C-Hide">
                                        <th class="OrderDate" style="width:250px">檔名
                                        </th>
                                        <th class="OrderDate">版號
                                        </th>
                                        <th class="OrderSerial">備註/關鍵字
                                        </th>
                                        <th class="OrderSerial">合約到期日
                                        </th>
                                        <th class="OrderSerial">乙方聯絡人
                                        </th>
                                        <th class="OrderName">類型
                                        </th>
                                        <th class="OrderDate">建立人員
                                        </th>
                                        <th class="OrderCouponState">建立時間
                                        </th>
                                        <th class="OrderCouponState"></th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="mc-tableContentITEM">
                                    <td style="text-align: left">
                                        <a href="#" onclick="DownLoadProposal('<%# ((ProposalContractFile)Container.DataItem).Guid %>')"><%# ((ProposalContractFile)Container.DataItem).FileName %></a>
                                    </td>
                                    <td style="text-align: left">
                                        <%# ((ProposalContractFile)Container.DataItem).Version %>
                                    </td>
                                    <td style="text-align: left">
                                        <%# ((ProposalContractFile)Container.DataItem).Memo.Replace("|", "<br />") %>
                                    </td>
                                    <td style="text-align: left">
                                        <%# ((ProposalContractFile)Container.DataItem).DueDate == null ? "" : ((ProposalContractFile)Container.DataItem).DueDate.Value.ToString("yyyy/MM/dd") %>    
                                    </td>
                                    <td style="text-align: left">
                                        <%# ((ProposalContractFile)Container.DataItem).PartyBContact.Replace("|", "<br />") %>
                                    </td>
                                    <td style="text-align: left">
                                        <%# Helper.GetDevelopStatus<SellerContractType>(((ProposalContractFile)Container.DataItem).ContractType) %>
                                    </td>
                                    <td style="text-align: left">
                                        <%# ((ProposalContractFile)Container.DataItem).CreateId.Replace("@17life.com.tw", string.Empty).Replace("@17life.com", string.Empty) %>
                                    </td>
                                    <td style="text-align: left">
                                        <%# ((ProposalContractFile)Container.DataItem).CreateTime.ToString("yyyy/MM/dd HH:mm") %>
                                    </td>
                                    <td style="text-align: left" class="HideDelete">
                                        <img src="../Themes/default/images/17Life/G2/delete.png" alt="刪除" title="刪除" style="padding-left: 5px" class="view" onclick="deleteContractFile('<%# ((ProposalContractFile)Container.DataItem).Guid %>','<%# ((ProposalContractFile)Container.DataItem).ProposalId %>','<%# ((ProposalContractFile)Container.DataItem).ContractType %>');">
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>
    </asp:Panel>
    <span style="display:none">
        <asp:TextBox ID="txtMultiDeals" runat="server" Text="" ClientIDMode="Static"></asp:TextBox>
        <asp:TextBox ID="txtEnabled" runat="server" Text="" ClientIDMode="Static"></asp:TextBox>
    </span>
    <div id="divSellerOptions" style="width:300px;height:200px;padding:20px;display:none">

    </div>
    <!--側邊anchor Start-->
        <ul id="subMenu" class="nav-anchor" style="top: 150px">
            
        </ul>

        <!--側邊anchor End-->
    <div id="divSalesOptionImage" style="width:752px;height:597px;padding:20px;display:none;text-align:center;">
        <img src="/Themes/OWriteOff/MutipleOptionSample.png" alt="多重選項範本" />
        <input type="button" name="optionSample" id="donwloadSample" style="align-items:center" value="點此下載教學範本" class="btn btn-primary rd-mcacstbtn" onclick="location.href = '/template/SalesDocs/MutipleOptionSample.docx'" />
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

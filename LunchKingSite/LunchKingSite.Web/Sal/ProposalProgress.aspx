﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="ProposalProgress.aspx.cs" Inherits="LunchKingSite.Web.Sal.ProposalProgress" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Src="~/Sal/ProposalMenu.ascx" TagName="ProposalMenu" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <style>
        .mc-navbar .mc-navbtn {
            width: 10%;
        }
    </style>
    <uc2:ProposalMenu ID="ProposalMenu" runat="server"></uc2:ProposalMenu>
    <div class="mc-content">

        <p class="note" style="font-size: small; color: #666; float: right;">
            <asp:DropDownList ID="ddlMonth" runat="server" OnSelectedIndexChanged="ddlMonth_OnSelectedIndexChanged" AutoPostBack="true">
                <asp:ListItem Value="1" Text="1個月"></asp:ListItem>
                <asp:ListItem Value="2" Text="2個月"></asp:ListItem>
                <asp:ListItem Value="3" Text="3個月"></asp:ListItem>
                <asp:ListItem Value="4" Text="4個月"></asp:ListItem>
                <asp:ListItem Value="5" Text="5個月"></asp:ListItem>
                <asp:ListItem Value="6" Text="6個月"></asp:ListItem>
            </asp:DropDownList>
            * 僅限查詢半年內提案的檔次資訊
        </p>
        <asp:Repeater ID="rptMonthProposal" runat="server" OnItemDataBound="rptMonthProposal_ItemDataBound">
            <HeaderTemplate>
                <div class="mc-content">
            </HeaderTemplate>
            <ItemTemplate>
                <h1 class="rd-smll">
                    <asp:Literal ID="liMonth" runat="server"></asp:Literal>
                </h1>
                <hr class="header_hr">
                <div id="mc-table">
                    <asp:Repeater ID="rptProposal" runat="server" OnItemDataBound="rptProposal_ItemDataBound">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                                <tr class="rd-C-Hide">
                                    <th class="OrderName">上檔日
                                    </th>
                                    <th class="OrderDate">單號
                                    </th>
                                    <th class="OrderName">商家名稱 / 檔次
                                    </th>
                                    <th class="OrderName">商家
                                    </th>
                                    <th class="OrderName">核准
                                    </th>
                                    <th class="OrderName">建檔
                                    </th>
                                    <th class="OrderName">檔次
                                    </th>
                                    <th class="OrderName">編輯
                                    </th>
                                    <th class="OrderName">上架
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="mc-tableContentITEM">
                                <td class="OrderName" style="text-align: left; vertical-align: top">
                                    <span class="rd-Detailtitle">上檔日：</span>
                                    <asp:Literal ID="liOrderTimeS" runat="server"></asp:Literal>
                                </td>
                                <td class="OrderDate" style="text-align: left; vertical-align: top">
                                    <span class="rd-Detailtitle">單號：</span>
                                    <asp:HyperLink ID="hkProposalId" runat="server" Target="_blank"></asp:HyperLink>
                                    <br />
                                </td>
                                <td class="OrderName" style="text-align: left; vertical-align: top; max-width: 300px">
                                    <span class="rd-Detailtitle">商家名稱 / 檔次：</span>
                                    <asp:HyperLink ID="hkSellerName" runat="server" Target="_blank"></asp:HyperLink>
                                    <br />
                                    <asp:HyperLink ID="hkBusinessHour" runat="server" Target="_blank"></asp:HyperLink>
                                </td>
                                <td class="OrderName" style="vertical-align: top">
                                    <span class="rd-Detailtitle">商家：</span>
                                    <asp:Image ID="imgFlag" runat="server" Style="vertical-align: top;" />
                                    <asp:Literal ID="liSellerTempStatus" runat="server"></asp:Literal>
                                </td>
                                <asp:Repeater ID="rptStatus" runat="server" OnItemDataBound="rptStatus_ItemDataBound">
                                    <ItemTemplate>
                                        <td class="OrderName" style="vertical-align: top">
                                            <span class="rd-Detailtitle" style="font-weight: bold; border: solid 1px;">
                                                <asp:Literal ID="liFlagTitle" runat="server"></asp:Literal></span></span>
                                        <asp:Repeater ID="rptFlag" runat="server" OnItemDataBound="rptFlag_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:Image ID="imgFlag" runat="server" Style="vertical-align: top;" />
                                                <span>
                                                    <asp:Literal ID="liFlag" runat="server"></asp:Literal></span>
                                                <br />
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>

                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <br />
            </ItemTemplate>
            <FooterTemplate>
                </div>
            </FooterTemplate>
        </asp:Repeater>
        <asp:TextBox ID="txtMonth" runat="server" Text="1" Visible="false"></asp:TextBox>
    </div>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

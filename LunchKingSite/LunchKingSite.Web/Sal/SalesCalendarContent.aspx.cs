﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Sal
{
    public partial class SalesCalendarContent : RolePage, ISalesCalendarContentView
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        #region props
        private SalesCalendarContentPresenter _presenter;
        public SalesCalendarContentPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public int Id
        {
            get
            {
                int id = 0;
                if (Request.QueryString["Id"] != null && int.TryParse(Request.QueryString["Id"].ToString(), out id))
                {
                    int.TryParse(Request.QueryString["Id"].ToString(), out id);
                }
                hidId.Value = id.ToString();
                return id;
            }
        }
        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }
        public short DevelopStatus
        {
            get { return short.Parse(ddlDevelopStatus.SelectedValue); }
        }
        public short SellerGrade
        {
            get { return short.Parse(ddlSellerGrade.SelectedValue); }
        }
        public string SaleGoogleCalendarAPIKey
        {
            get
            {
                return config.SaleGoogleCalendarAPIKey;
            }
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IntialControls();
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }


        #endregion

        #region method
        private void IntialControls()
        {
            // 活動類型
            Dictionary<int, string> flags = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(SellerEventType)))
            {
                flags[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerEventType)item);
            }
            flags.Add(-1, "請選擇");
            ddlSellerEventType.DataSource = flags;
            ddlSellerEventType.DataTextField = "Value";
            ddlSellerEventType.DataValueField = "Key";
            ddlSellerEventType.DataBind();

            //顯示間隔
            Dictionary<int, string> interval = new Dictionary<int, string>();
            for (int idx = 1; idx <= 30; idx++)
            {
                interval[idx] = idx.ToString();
            }
            ddlInterval.DataSource = interval;
            ddlInterval.DataTextField = "Value";
            ddlInterval.DataValueField = "Key";
            ddlInterval.DataBind();

            //時間
            Dictionary<int, string> intervalTimes = new Dictionary<int, string>();
            DateTime StartTime = DateTime.ParseExact("01:00", "HH:mm", null);
            DateTime EndTime = DateTime.ParseExact("23:59", "HH:mm", null);
            TimeSpan Interval = new TimeSpan(0, 30, 0);
            int tidx = 0;
            while (StartTime < EndTime)
            {
                intervalTimes[tidx] = StartTime.ToString("HH:mm");
                StartTime = StartTime.Add(Interval);
                tidx++;
            }
            ddlBeginTime.DataSource = intervalTimes;
            ddlBeginTime.DataTextField = "Value";
            ddlBeginTime.DataValueField = "Value";
            ddlBeginTime.DataBind();

            ddlEndTime.DataSource = intervalTimes;
            ddlEndTime.DataTextField = "Value";
            ddlEndTime.DataValueField = "Value";
            ddlEndTime.DataBind();


            //開發狀態
            Dictionary<int, string> developStatus = new Dictionary<int, string>();
            foreach (DevelopStatus item in Enum.GetValues(typeof(DevelopStatus)))
            {
                developStatus[(int)item] = Helper.GetDescription(item);
            }
            ddlDevelopStatus.DataSource = developStatus;
            ddlDevelopStatus.DataTextField = "Value";
            ddlDevelopStatus.DataValueField = "Key";
            ddlDevelopStatus.DataBind();

            //客戶等級
            Dictionary<int, string> sellerGrade = new Dictionary<int, string>();
            foreach (SellerGrade item in Enum.GetValues(typeof(SellerGrade)))
            {
                sellerGrade[(int)item] = Helper.GetDescription(item);
            }
            ddlSellerGrade.DataSource = sellerGrade;
            ddlSellerGrade.DataTextField = "Value";
            ddlSellerGrade.DataValueField = "Key";
            ddlSellerGrade.DataBind();

            txtUserName.Text = UserName.Replace(".tw", ""); ;

        }
        public void SetCalendarContent(SalesCalendarEvent cal)
        {
            txtCalendarContent.Text = cal.EventContent;
            if (cal.EventDateS != null && cal.EventDateE != null)
            {
                txtBeginDate.Text = cal.EventDateS.Value.ToString("yyyy/MM/dd");
                txtEndDate.Text = cal.EventDateE.Value.ToString("yyyy/MM/dd");
                ddlBeginTime.SelectedValue = cal.EventDateS.Value.ToString("HH:mm");
                ddlEndTime.SelectedValue = cal.EventDateE.Value.ToString("HH:mm");
            }
            chkWholeDay.Checked = cal.IsWholeDay;
            txtLocation.Text = cal.Location;
            txtSellerName.Text = cal.SellerName;
            txtContact.Text = cal.Contact;
            ddlSellerEventType.SelectedValue = Convert.ToString(cal.EventType);
            chkWakeUp.Checked = cal.IsWakeup;
            ddlCycle.SelectedValue = Convert.ToString(cal.WakeupCycle);
            txtMemo.Text = cal.Memo;

        }
        #endregion

        #region webmethod
        [WebMethod]
        public static SalesCalendarEvent SalesCalendarSave(int id, string gid, string content, string dateS, string dateE,
            bool isWholeDay, bool isRepeat, string location, string sellerName, int eventType, string contact, bool isWakeUp, int wakeupMinutes, int wakeupCycle,
            string memo, int freq, int interval, string week, int endson, string endson_data,
            string UserName, bool isSyncSellerNote, string DevelopStatus, string SellerGrade, string SellerId)
        {
            SalesCalendarEvent calendar = PponFacade.SalesCalendarEventGet(id);
            if (!calendar.IsLoaded)
            {
                calendar = new SalesCalendarEvent();
                calendar.Gid = gid;
            }
            DateTime dtS = DateTime.MinValue;
            DateTime.TryParse(dateS, out dtS);
            DateTime dtE = DateTime.Now;
            DateTime.TryParse(dateE, out dtE);
            if (dtS == DateTime.MinValue || dtE == DateTime.MinValue)
            {
                calendar.Type = (int)SalesCalendarType.Todo;
            }
            else
            {
                calendar.Type = (int)SalesCalendarType.Schedlule;
            }
            calendar.EventContent = content;
           
            
            if (dtS != DateTime.MinValue)
            {
                calendar.EventDateS = DateTime.Parse(dateS);
            }
            if (dtE != DateTime.MinValue)
            {
                calendar.EventDateE = DateTime.Parse(dateE);
            }
            calendar.IsWholeDay = isWholeDay;
            calendar.IsRepeat = isRepeat;
            calendar.Location = location;
            calendar.SellerName = sellerName;
            calendar.Contact = contact;
            calendar.EventType = eventType;
            calendar.IsWakeup = isWakeUp;
            calendar.WakeupMinutes = wakeupMinutes;
            calendar.WakeupCycle = wakeupCycle;
            calendar.Memo = memo;
            calendar.CreateTime = DateTime.Now;
            calendar.CreateUser = UserName;
            calendar.Frequency = freq;
            calendar.Interval = interval;
            calendar.Week = week;
            //calendar.WakeupDateS = wakeup_date_s;
            calendar.Endson = endson;
            calendar.EndsonData = endson_data;

            PponFacade.SalesCalendarEventSave(calendar);

            if (isSyncSellerNote)
            {
                Seller seller = SellerFacade.SellerGetById(SellerId);
                short developStatus = 0;
                short sellerGrade = 0;
                short.TryParse(DevelopStatus, out developStatus);
                short.TryParse(SellerGrade, out sellerGrade);


                SellerFacade.SellerManageLogSet(seller.Guid, developStatus, sellerGrade, sellerName + "," + contact + "," + memo, UserName);
            }

            return calendar;
        }

        [WebMethod]
        public static SalesCalendarEvent SalesCalendarUpdateGid(int Id, string Gid)
        {
            SalesCalendarEvent calendar = PponFacade.SalesCalendarEventGet(Id);
            calendar.Gid = Gid;
            PponFacade.SalesCalendarEventSave(calendar);

            return calendar;
        }
        [WebMethod]
        public static string CreateGoogleCalendar(string content, string loc, string dateS, string dateE, bool isWakeUp,
            int minutes, int cycle, string sellerName, string contact, string memo,
            string interval, string freq, string week, string endson, string endson_data)
        {
            SalesGoogleCalendarModel model = new SalesGoogleCalendarModel();
            model.summary = content;
            model.location = loc;
            model.description = sellerName + "," + contact + "," + memo;
            model.start = new GoogleCalendarDate
            {
                dateTime = dateS,
                timeZone = "Asia/Taipei"
            };

            model.end = new GoogleCalendarDate
            {
                dateTime = dateE,
                timeZone = "Asia/Taipei"
            };

            string Recurrence = "";
            string Frequency = "";
            switch (freq)
            {
                case "0":
                    //每天
                    Frequency = "FREQ=DAILY;";
                    if (!string.IsNullOrEmpty(interval))
                    {
                        Frequency += "INTERVAL=" + interval + ";";
                    }
                    break;
                case "1":
                    //每週(一到五)
                    Frequency = "FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR;";
                    if (string.IsNullOrEmpty(week))
                    {
                        
                        Frequency += "BYDAY=MO,TU,WE,TH,FR;";
                    }
                    break;
                case "2":
                    //每週一三五
                    Frequency = "FREQ=WEEKLY;BYDAY=MO,WE,FR;";
                    break;
                case "3":
                    //每週二四
                    Frequency = "FREQ=WEEKLY;BYDAY=TU,TH;";
                    break;
                case "4":
                    //每週
                    Frequency = "FREQ=WEEKLY;";
                    if (!string.IsNullOrEmpty(week))
                    {
                        string _week = WeeksGet(week);
                        Frequency += "BYDAY=" + _week + ";";
                    }
                    if (!string.IsNullOrEmpty(interval))
                    {
                        Frequency += "INTERVAL=" + interval + ";";
                    }
                    break;
                case "5":
                    //每月
                    Frequency = "FREQ=MONTHLY;";
                    if (!string.IsNullOrEmpty(interval))
                    {
                        Frequency += "INTERVAL=" + interval + ";";
                    }
                    break;
                case "6":
                    //每年
                    Frequency = "FREQ=YEARLY;";
                    if (!string.IsNullOrEmpty(interval))
                    {
                        Frequency += "INTERVAL=" + interval + ";";
                    }
                    break;
                default:
                    Frequency = "FREQ=DAILY;";
                    if (!string.IsNullOrEmpty(interval))
                    {
                        Frequency += "INTERVAL=" + interval + ";";
                    }
                    break;
            }

            //WKST

            string Endson_count = "";
            switch (endson)
            {
                case "0":
                    break;
                case "1":
                    Endson_count = "COUNT=" + endson_data + ";";
                    break;
                case "2":
                    DateTime dt = DateTime.Now;
                    DateTime.TryParse(endson_data, out dt);
                    Endson_count = "UNTIL=" + dt.ToString("yyyyMMdd") + ";";
                    break;
            }

            Recurrence = Frequency + Endson_count;

            if (Recurrence.EndsWith(";"))
            {
                Recurrence = Recurrence.Substring(0, Recurrence.Length - 1);
            }

            model.recurrence = new List<string>()
            {
                "RRULE:" + Recurrence
                //"RRULE:FREQ=WEEKLY;INTERVAL=1"
                //"RRULE:FREQ=WEEKLY;INTERVAL=2;COUNT=8;WKST=SU;BYDAY=TU,TH"
            };

            if (isWakeUp)
            {
                model.reminders = new GoogleCalendarReminder
                {
                    useDefault = false,
                    overrides = new List<GoogleCalendarOverrides>
                {
                    new GoogleCalendarOverrides {
                        method = "popup",
                        minutes = minutes * cycle
                    }
                }
                };
            }

            return new JsonSerializer().Serialize(model);
        }

        [WebMethod]
        public static List<AjaxResponseResult> GetSellerName(string sellerName, string UserName)
        {

            List<AjaxResponseResult> res = new List<AjaxResponseResult>();
            if (!string.IsNullOrEmpty(sellerName) && sellerName.Length >= 2 && sellerName.Length < 6)
            {
                var sellers = SellerFacade.GetSellersByName(sellerName, UserName);
                foreach (var e in sellers)
                {
                    res.Add(new AjaxResponseResult
                    {
                        Value = e.Key.ToString(),
                        Label = e.Value
                    });
                }
            }

            return res;
        }


        [WebMethod]
        public static List<AjaxResponseResult> GetContactame(string sellerId, string ContactName)
        {
            List<AjaxResponseResult> res = new List<AjaxResponseResult>();

            if (!string.IsNullOrEmpty(ContactName) && ContactName.Length >= 1 && ContactName.Length < 6)
            {
                Seller seller = SellerFacade.SellerGetById(sellerId);

                if (seller.IsLoaded && seller != null)
                {
                    if (!string.IsNullOrEmpty(seller.Contacts))
                    {
                        List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
                        foreach (Seller.MultiContracts c in contacts)
                        {
                            if (int.Parse(c.Type) == (int)ProposalContact.Normal)
                            {
                                res.Add(new AjaxResponseResult
                                {
                                    Value = c.ContactPersonName,
                                    Label = c.ContactPersonName
                                });
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(seller.SellerContactPerson))
                        {
                            res.Add(new AjaxResponseResult
                            {
                                Value = seller.SellerContactPerson,
                                Label = seller.SellerContactPerson
                            });
                        }
                    }
                }
            }

            return res;
        }


        [WebMethod]
        public static string GetSellerContactName(string sellerId)
        {
            string contactName = "";
            Seller seller = SellerFacade.SellerGetById(sellerId);
            if(seller.IsLoaded && seller != null)
            {
                if (!string.IsNullOrEmpty(seller.SellerContactPerson))
                {
                    contactName = seller.SellerContactPerson;
                }
                else
                {
                    if (!string.IsNullOrEmpty(seller.Contacts))
                    {
                        List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
                        foreach (Seller.MultiContracts c in contacts)
                        {
                            if (int.Parse(c.Type) == (int)ProposalContact.Normal)
                            {
                                contactName = c.ContactPersonName;
                                break;
                            }
                        }
                    }
                }               
            }

            return contactName;
        }

        private static string WeeksGet(string week)
        {
            string[] weeks = week.Split(",");
            string _Week = "";
            if (weeks.Length >= 6)
            {
                if (weeks[0] == "Y")
                {
                    _Week += "SU,";
                }
                if (weeks[1] == "Y")
                {
                    _Week += "MO,";
                }
                if (weeks[2] == "Y")
                {
                    _Week += "TU,";
                }
                if (weeks[3] == "Y")
                {
                    _Week += "WE,";
                }
                if (weeks[4] == "Y")
                {
                    _Week += "TH,";
                }
                if (weeks[5] == "Y")
                {
                    _Week += "FR,";
                }
                if (weeks[6] == "Y")
                {
                    _Week += "SA,";
                }
            }
            if (_Week.EndsWith(","))
            {
                _Week = _Week.Substring(0, _Week.Length - 1);
            }
            return _Week;
        }
        #endregion
    }
}
﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Sal
{
    public partial class PponOptionContnet : RolePage, IPponOptionContnetView
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        #region props
        private PponOptionContnetPresenter _presenter;
        public PponOptionContnetPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public Guid BusinessHourGuid
        {
            get
            {
                Guid guid = Guid.Empty;
                Guid.TryParse(txtBusinessHourGuid.Text, out guid);
                return guid;
            }
        }
        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }
        #endregion

        #region event
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Presenter.OnViewInitialized();
                IntialControls();
                EnabledSettings(UserName);
            }
            _presenter.OnViewLoaded();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //if (this.Search != null)
            //{
            //    this.Search(this, e);
            //}
        }
        protected void rptStore_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is Store)
            {
                Store dataItem = (Store)e.Item.DataItem;
                Literal litStoreGuid = (Literal)e.Item.FindControl("litStoreGuid");
                litStoreGuid.Text = dataItem.Guid.ToString();
            }
        }
        #endregion

        #region method
        private void IntialControls()
        {
           
        }
        private void EnabledSettings(string username)
        {
            ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(username));
            if (emp.IsLoaded)
            {
                if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.ReadAll))
                {
                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.CrossDeptTeam))
                {
                    if (string.IsNullOrEmpty(emp.CrossDeptTeam) && emp.TeamNo == null)
                    {
                        ShowMessage("身份權限未明，無法查看資料，請洽產品或技術人員協助");
                    }
                }
                else
                {
                }
            }
            else
            {
                ShowMessage("查無員工資料，請重新檢視員工資料維護設定。");
            }

        }
        public void SetPponOptionContent(PponOptionItems poi)
        {
            //if (poi != null )
            //{
            //    phResult.Visible = true;

            //    //檔次
            //    rptDeals.DataSource = poi.PponDeals;
            //    rptDeals.DataBind();

            //    //分店
            //    rptStore.DataSource = poi.store;
            //    rptStore.DataBind();

            //    //多重選項
            //    rptPponOption.DataSource = poi.pponOption;
            //    rptPponOption.DataBind();

            //    rptOrderTotalLimit.DataSource = poi.OrderTotalLimit;
            //    rptOrderTotalLimit.DataBind();

            //    foreach (RepeaterItem item in rptStore.Items)
            //    {
            //        Literal litQuantity = (Literal)item.FindControl("litQuantity");
            //        Literal litStoreGuid = (Literal)item.FindControl("litStoreGuid");
            //        Label litStoreCount = (Label)item.FindControl("litStoreCount");
            //        TextBox txtStoreCount = (TextBox)item.FindControl("txtStoreCount");
            //        var findPponStore = poi.pponStores.FirstOrDefault(x => x.StoreGuid.ToString() == litStoreGuid.Text.Trim());

            //        if (findPponStore != null)
            //        {
            //            litStoreCount.Text = findPponStore.TotalQuantity == null ? "" : findPponStore.TotalQuantity.Value.ToString();
            //            txtStoreCount.Text = litStoreCount.Text;
            //        }                    
            //    }
            //}
            //else
            //{
            //    phResult.Visible = false;
            //}

        }

        public void ShowMessage(string msg)
        {
            string script = string.Empty;
            if (!string.IsNullOrWhiteSpace(msg))
            {
                script = string.Format("alert('{0}');", msg);
            }
            script += string.Format("location.href='{1}';", msg, config.SiteUrl);

            if (!string.IsNullOrWhiteSpace(script))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "alert", script, true);
            }
        }
        #endregion

        #region webmethod
        [WebMethod]
        public static string DataGet(string bid, string assignBid, string UserName, string Url)
        {
            PponOptionItems poi = null;
            bool IsNegative = false;
            bool IsUpdate = false;
            bool IsSelf = true;
            bool IsPrivilege = true;
            string NoPrivilege = "";
            Guid _bid = Guid.Empty;
            Guid.TryParse(bid, out _bid);
            Guid _assignBid = Guid.Empty;
            Guid.TryParse(assignBid, out _assignBid);

            ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(UserName));

            #region 回傳查詢權限
            if (FunctionPrivilegeManager.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReadAll, Url) || FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.ReadAll, Url).Contains(UserName))
            {
                //全區
                poi = PponFacade.PponOptionItemsGet(_bid, _assignBid);
            }
            else
            {
                if (emp.IsLoaded)
                {
                    

                    //跨區
                    if (FunctionPrivilegeManager.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.CrossDeptTeam, Url) || FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.CrossDeptTeam, Url).Contains(UserName))
                    {
                        poi = PponFacade.PponOptionItemsGet(_bid, _assignBid);

                        if (poi.PponDeals != null)
                        {
                            ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(Convert.ToInt32(poi.PponDeals.FirstOrDefault().DevelopeSalesId));
                            ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(Convert.ToInt32(poi.PponDeals.FirstOrDefault().OperationSalesId));

                            string message1 = "";
                            string message2 = "";
                            bool checkDeSales = ProposalFacade.GetCrossPrivilege(3, emp, deSalesEmp.DeptId, deSalesEmp.TeamNo, deSalesEmp.DeptName, null, ref message1);

                            if (!checkDeSales)
                            {
                                if (opSalesEmp.IsLoaded)
                                {
                                    bool checkOpSales = ProposalFacade.GetCrossPrivilege(3, emp, opSalesEmp.DeptId, opSalesEmp.TeamNo, opSalesEmp.DeptName, null, ref message2);

                                    if (!checkOpSales)
                                    {
                                        IsPrivilege = false;
                                        NoPrivilege = message2;
                                    }

                                }
                            }
                        }
                    }
                    else
                    {
                        //僅瀏覽
                        poi = PponFacade.PponOptionItemsGet(_bid, _assignBid);

                        if (poi.PponDeals != null)
                        {
                            ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(Convert.ToInt32(poi.PponDeals.FirstOrDefault().DevelopeSalesId));
                            ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(Convert.ToInt32(poi.PponDeals.FirstOrDefault().OperationSalesId));

                            if (emp.UserId != deSalesEmp.UserId && emp.UserId != opSalesEmp.UserId)
                            {
                                IsSelf = false;
                            }
                            
                        }
                    }
                }
            }
            #endregion


            #region 回傳更新權限
            if (poi != null)
            {
                if (poi.PponDeals != null)
                {
                    var data = poi.PponDeals.FirstOrDefault();
                    if (data != null)
                    {
                        if (FunctionPrivilegeManager.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.UpdateAll, Url) || FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.UpdateAll, Url).Contains(UserName))
                        {
                            //全區更新
                            IsUpdate = true;
                        }
                        else
                        {
                            if (FunctionPrivilegeManager.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.UpdateCrossDeptTeam, Url) || 
                                FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.UpdateCrossDeptTeam, Url).Contains(UserName))
                            {
                                //僅能更新跨區設定的檔次
                                ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(Convert.ToInt32(poi.PponDeals.FirstOrDefault().DevelopeSalesId));
                                ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(Convert.ToInt32(poi.PponDeals.FirstOrDefault().OperationSalesId));

                                string message1 = "";
                                string message2 = "";

                                bool checkDeSales = ProposalFacade.GetCrossPrivilege(3, emp, deSalesEmp.DeptId, deSalesEmp.TeamNo, deSalesEmp.DeptName, null, ref message1);
                                if (checkDeSales)
                                {
                                    IsUpdate = true;
                                }
                                else
                                {
                                    if (opSalesEmp.IsLoaded)
                                    {
                                        bool checkOpSales = ProposalFacade.GetCrossPrivilege(3, emp, opSalesEmp.DeptId, opSalesEmp.TeamNo, opSalesEmp.DeptName, null, ref message2);

                                        if (checkOpSales)
                                        {
                                            IsUpdate = true;
                                        }

                                    }
                                }
                            }
                            else if (FunctionPrivilegeManager.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Update, Url) || FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.Update, Url).Contains(UserName))
                            {
                                ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(Convert.ToInt32(poi.PponDeals.FirstOrDefault().DevelopeSalesId));
                                ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(Convert.ToInt32(poi.PponDeals.FirstOrDefault().OperationSalesId));

                                if (emp.UserId == deSalesEmp.UserId || emp.UserId == opSalesEmp.UserId)
                                {
                                    //僅能更新自己的檔次
                                    IsUpdate = true;
                                }
                            }
                        }
                    }
                }
                poi.NoPrivilege = NoPrivilege;
                poi.IsPrivilege = IsPrivilege;//是否有權限
                poi.IsSelf = IsSelf;
                poi.IsUpdate = IsUpdate;    //是否允許更新
                poi.IsNegative = IsNegative;//是否允許輸入負數
            }
            #endregion

            //業務不能更改商品寄倉檔次
            Department department = HumanFacade.GetDepartmentByDeptId(emp.DeptId);
            if (department.ParentDeptId != EmployeeDept.S000.ToString() && poi.IsConsignment)
                poi.IsConsignment = false;

            return new JsonSerializer().Serialize(poi);
        }
        [WebMethod]
        public static string DataSave(string bid, string storeList, string QuantityList, string OrderTotalLimitList, string UserName, string Url)
        {
            bool IsUpdate = false;

            var _storeList = new JsonSerializer().Deserialize<List<storeList>>(storeList);   //分店
            var _QuantityList = new JsonSerializer().Deserialize<List<QuantityList>>(QuantityList); //多重選項
            var _OrderTotalLimitList = new JsonSerializer().Deserialize<List<OrderTotalLimitList>>(OrderTotalLimitList);   //最大購買數量


            ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(UserName));

            if (emp.DeptId == null)
            {
                return new JsonSerializer().Serialize(
                    new { IsSuccess = false, Message = "您尚未設定部門！" }
                );
            }


            Guid _bid = Guid.Empty;
            Guid.TryParse(bid, out _bid);

            PponOptionItems poi = PponFacade.PponOptionItemsGet(_bid, _bid);
            var data = poi.PponDeals.FirstOrDefault();

            //var pels = Enum.GetValues(typeof(PponOptionEdotLevel));
            if (!FunctionPrivilegeManager.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Setting, Url) && !FunctionPrivilegeManager.GetPrivilegesByFunctionType(Url, SystemFunctionType.Setting).Contains(UserName))
            {
                var checkStore = _storeList.Where(x => int.Parse(x.TotalQuantity) < 0).FirstOrDefault();
                var checkQuantity = _QuantityList.Where(x => int.Parse(x.ModifyCatgSeq) <= 0).FirstOrDefault();
                var checkOrderTotalLimit = _OrderTotalLimitList.Where(x => int.Parse(x.ModifyOrderTotalLimit) <= 0).FirstOrDefault();

                if (checkStore != null || checkQuantity != null || checkOrderTotalLimit != null)
                {
                    return new JsonSerializer().Serialize(
                                        new { IsSuccess = false, Message = "不允許輸入負數" }
                                    );
                }
            }

            if (FunctionPrivilegeManager.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.UpdateAll, Url) || FunctionPrivilegeManager.GetPrivilegesByFunctionType(Url, SystemFunctionType.UpdateAll).Contains(UserName))
            {
                //全區更新
                IsUpdate = true;
            }
            else
            {
                if (FunctionPrivilegeManager.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.UpdateCrossDeptTeam, Url) || FunctionPrivilegeManager.GetPrivilegesByFunctionType(Url, SystemFunctionType.UpdateCrossDeptTeam).Contains(UserName))
                {
                    //僅能更新跨區設定的檔次
                    ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(Convert.ToInt32(poi.PponDeals.FirstOrDefault().DevelopeSalesId));
                    ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(Convert.ToInt32(poi.PponDeals.FirstOrDefault().OperationSalesId));

                    string message1 = "";
                    string message2 = "";

                    bool checkDeSales = ProposalFacade.GetCrossPrivilege(3, emp, deSalesEmp.DeptId, deSalesEmp.TeamNo, deSalesEmp.DeptName, null, ref message1);
                    if (checkDeSales)
                    {
                        IsUpdate = true;
                    }
                    else
                    {
                        if (opSalesEmp.IsLoaded)
                        {
                            bool checkOpSales = ProposalFacade.GetCrossPrivilege(3, emp, opSalesEmp.DeptId, opSalesEmp.TeamNo, opSalesEmp.DeptName, null, ref message2);

                            if (checkOpSales)
                            {
                                IsUpdate = true;
                            }

                        }
                    }
                }
                else if (FunctionPrivilegeManager.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Update, Url) || FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.Update, Url).Contains(UserName))
                {
                    ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(Convert.ToInt32(poi.PponDeals.FirstOrDefault().DevelopeSalesId));
                    ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(Convert.ToInt32(poi.PponDeals.FirstOrDefault().OperationSalesId));

                    if (emp.UserId == deSalesEmp.UserId || emp.UserId == opSalesEmp.UserId)
                    {
                        //僅能更新自己的檔次
                        IsUpdate = true;
                    }
                }
            }
            if (!IsUpdate)
            {
                return new JsonSerializer().Serialize(
                    new { IsSuccess = false, Message = "您無權限修改該檔資訊！" }
                );
            }


            PponFacade.PponOptionItemsSet(bid, _storeList, _QuantityList, _OrderTotalLimitList, UserName);

            return new JsonSerializer().Serialize(
                    new { IsSuccess = true, Message = "儲存成功！" }
                );
        }
        #endregion
    }
}
﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Sal
{
    public partial class SellerList : RolePage, ISalesSellerListView
    {
        public ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        #region props
        private SalesSellerListPresenter _presenter;
        public SalesSellerListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public string BossName
        {
            get
            {
                return txtBossName.Text;
            }
        }

        public string SellerName
        {
            get { return txtSellerName.Text; }
        }

        public string CompanyName
        {
            get { return txtCompanyName.Text; }
        }

        public string SignCompanyId
        {
            get { return txtSignCompanyId.Text; }
        }

        public int? SalesId
        {
            get
            {
                if (!string.IsNullOrEmpty(txtSalesEmail.Text))
                {
                    ViewEmployee emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, txtSalesEmail.Text);
                    return emp.UserId;
                }
                else
                {
                    return null;
                }
                
            }
        }

        public SellerTempStatus? TempStaus
        {
            get
            {
                if (ddlTempStatus.SelectedValue != string.Empty)
                {
                    SellerTempStatus status = SellerTempStatus.Applied;
                    SellerTempStatus.TryParse(ddlTempStatus.SelectedValue, out status);
                    return status;
                }
                else
                {
                    return null;
                }
            }
        }
        public int RowCount {
            get {
                int _RowCount = 0;
                int.TryParse(hidRowCount.Value, out _RowCount);
                return _RowCount;
            }
            set
            {
                hidRowCount.Value = value.ToString();
            }
        }

        public int PageSize
        {
            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }
        }

        public int CurrentPage
        {
            get
            {
                return ucPager.CurrentPage;
            }
        }
        /// <summary>
        /// [搜尋條件]自己的
        /// </summary>
        public bool IsPrivate
        {
            get
            {
                return chkIsPrivate.Checked;
            }
        }
        /// <summary>
        /// [搜尋條件]公池
        /// </summary>
        public bool IsPublic
        {
            get
            {
                return chkIsPublic.Checked;
            }
        }

        public int EmpUserId
        {
            get
            {
                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReadAll))
                {
                    return 0;
                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.CrossDeptTeam))
                {
                    return 0;
                }
                else
                {
                    return MemberFacade.GetUniqueId(UserName);
                }

            }
        }

        public string CrossDeptTeam
        {
            get
            {
                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReadAll))
                {
                    return "";
                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.CrossDeptTeam))
                {

                    ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(UserName));
                    if (!string.IsNullOrEmpty(emp.CrossDeptTeam))
                        return emp.CrossDeptTeam;
                    else if (emp.TeamNo == null)
                        return emp.DeptId + "[0]";
                    else
                        return emp.DeptId + "[" + emp.TeamNo + "]";
                }
                else
                {
                    return "";
                }

            }

        }

        public string Dept
        {
            get { return ddlDept.SelectedValue; }
        }

        public int CityId
        {
            get
            {
                int _CityId = 0;
                int.TryParse(hidTownshipId.Value, out _CityId);
                return _CityId;
            }


        }

        public string SellerLevelList
        {
            get
            {
                return hidSellerLevel.Value;
            }
        }

        public string SellerPorperty
        {
            get
            {
                return hidSellerPorperty.Value;
            }
        }

        public int SellerManagLogNumber
        {
            get
            {
                int number = 0;
                int.TryParse(hidNumber.Value, out number);
                return number;
            }
        }

        public string SellerManagLogDatepart
        {
            get
            {
                return ddlDatepart.SelectedValue;
            }
        }

        public string ExportSellerList
        {
            get
            {
                return hidExportSellerList.Value;
            }
        }

        public FileUpload FUExport
        {
            get
            {
                return FUImport;
            }
        }



        #endregion

        #region event
        public event EventHandler Search;
        public event EventHandler<DataEventArgs<int>> OnGetCount;
        public event EventHandler<DataEventArgs<int>> PageChanged;
        public event EventHandler Export;
        public event EventHandler Import;
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["comid"] != null)
                {
                    txtSignCompanyId.Text = Request.QueryString["comid"].ToString();
                }
                InitialControls();
                Presenter.OnViewInitialized();
                EnabledSettings(UserName);
            }
            _presenter.OnViewLoaded();

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            PageSize = Convert.ToInt32(ddlChangePage.SelectedValue);

            if (this.Search != null)
            {
                this.Search(this, e);
            }
            ucPager.ResolvePagerView(CurrentPage, true);

        }
        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChanged != null)
            {
                this.PageChanged(this, new DataEventArgs<int>(pageNumber));
            }
        }

        protected void btnSellerAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect(ResolveUrl("~/sal/SellerContent.aspx"));
        }

        protected void btnSellerBatch_Click(object sender, EventArgs e)
        {
            Response.Redirect(ResolveUrl("~/sal/SellerBatchContent.aspx"));
        }

        protected int GetCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetCount != null)
            {
                OnGetCount(this, e);
            }
            return e.Data;
        }

        protected void rptSeller_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is Seller)
            {
                Seller dataItem = (Seller)e.Item.DataItem;
                Guid SellerGuid = dataItem.Guid;
                Literal liStatusDetail = (Literal)e.Item.FindControl("liStatusDetail");
                Literal liSellerSales = (Literal)e.Item.FindControl("liSellerSales");
                liStatusDetail.Text = SellerFacade.GetSellerPorperty(SellerGuid) + " / " + SellerFacade.GetSellerManageLogDiff(SellerGuid);
                liSellerSales.Text = SellerFacade.GetSellerSales(SellerGuid);

                var SellerTree = sp.SellerTreeGetListBySellerGuid(SellerGuid).FirstOrDefault();
                if (SellerTree != null)
                {
                    HyperLink parentSellerLink = (HyperLink)e.Item.FindControl("parentSellerLink");
                    ImageButton btnSellerTree = (ImageButton)e.Item.FindControl("btnSellerTree");
                    Seller ParentSeller = sp.SellerGet(SellerTree.ParentSellerGuid);

                    parentSellerLink.Text = ParentSeller.SellerId + " " + ParentSeller.SellerName;
                    parentSellerLink.NavigateUrl = ResolveUrl("~/sal/SellerContent.aspx?sid=" + SellerTree.ParentSellerGuid);
                    btnSellerTree.Visible = true;


                }

            }
        }


        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (this.Export != null)
            {
                this.Export(this, e);
            }
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (this.Import != null)
            {
                this.Import(this, e);
            }
        }


        #endregion

        #region method
        private void InitialControls()
        {
            Dictionary<int, string> flags = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(SellerTempStatus)))
            {
                flags[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerTempStatus)item);
            }
            ddlTempStatus.DataSource = flags;
            ddlTempStatus.DataTextField = "Value";
            ddlTempStatus.DataValueField = "Key";
            ddlTempStatus.DataBind();

            // 業務部門
            ddlDept.DataSource = HumanFacade.GetSalesDepartment();
            ddlDept.DataTextField = "DeptName";
            ddlDept.DataValueField = "DeptId";
            ddlDept.DataBind();

            // 地址(城市)
            IEnumerable<City> citys = CityManager.Citys.Where(x => x.Code != "SYS");
            ddlCompanyCityId.DataSource = citys;
            ddlCompanyCityId.DataTextField = "CityName";
            ddlCompanyCityId.DataValueField = "Id";
            ddlCompanyCityId.DataBind();

            





        }

        private void EnabledSettings(string username)
        {
            ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(username));
            if (emp.IsLoaded)
            {
                if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.ReadAll))
                {
                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.CrossDeptTeam))
                {
                    if (string.IsNullOrEmpty(emp.CrossDeptTeam) && emp.TeamNo == null)
                    {
                        ShowMessage("身份權限未明，無法查看資料，請洽產品或技術人員協助");
                    }
                }
                else
                {
                }


                // 新增商家
                if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.Create))
                {
                    btnSellerAdd.Visible = true;
                }
                
                //商家批次處理
                if (FunctionPrivilegeManager.IsInSystemFunctionPrivilege(username, SystemFunctionType.Read, "/sal/SellerBatchContent.aspx"))
                {
                    btnSellerBatch.Visible = true;
                }
            }
            else
            {
                ShowMessage("查無員工資料，請重新檢視員工資料維護設定。");
            }

            if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.PatchChangeSeller))
            {
                hidPatchChangeSeller.Value = "1";
            }

        }

        public void SetSellerList(List<Seller> dataList)
        {
            divSellerList.Visible = true;
            rptSeller.DataSource = dataList;
            rptSeller.DataBind();
        }
        public void ShowMessage(string msg)
        {
            string script = string.Format("alert('{0}');", msg);
            Page.ClientScript.RegisterStartupScript(GetType(), "alert", script, true);
        }


        #endregion

        [WebMethod]
        public static List<string> GetSellerNameAutoCompelte(string partName)
        {
            return SellerFacade.SellerGetColumnListByLike(Seller.Columns.SellerName, partName);
        }

        [WebMethod]
        public static List<string> GetCompanyNameAutoCompelte(string partName)
        {
            return SellerFacade.SellerGetColumnListByLike(Seller.Columns.CompanyName, partName);
        }

        [WebMethod]
        public static List<string> GetSalesEmailAutoCompelte(string partEmail)
        {
            return HumanFacade.ViewEmployeeGetColumnListByLike(ViewEmployee.Columns.Email, partEmail);
        }

        [WebMethod]
        public static string ExportSeller(string[] array)
        {
            return "";
        }

        /// <summary>
        /// 商家類型
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static List<AjaxResponseResult> GetSellerLevelList()
        {
            List<SellerLevel> dataList = new List<SellerLevel>();
            dataList.Add(SellerLevel.A);
            dataList.Add(SellerLevel.B);
            dataList.Add(SellerLevel.C);
            dataList.Add(SellerLevel.Big);
            dataList.Add(SellerLevel.Medium);
            dataList.Add(SellerLevel.Small);
            dataList.Add(SellerLevel.Delivery);

            List<AjaxResponseResult> res = new List<AjaxResponseResult>();
            
                
            foreach (SellerLevel e in dataList)
            {
               
                res.Add(new AjaxResponseResult
                {
                    Value = ((int)e).ToString(),
                    Label = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, e).Split('：')[0]
                });
            }
            

            return res;

        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="SalesManagement.aspx.cs" Inherits="LunchKingSite.Web.Sal.SalesManagement" %>

<%@ Register Src="~/Ppon/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link type="text/css" href="../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <script src="../Tools/js/jquery.ui.datepicker-zh-TW.js" type="text/javascript"></script>
    <style type="text/css">
        .GridViewStyle th
        {
            text-align: center; 
        }

        .GridViewStyle td
        {
            text-align: center;
        }

        .General label
        {
            font-weight: normal;
        }

        .Title
        {
            width: 90px;
        }

        table
        {
            border-collapse: separate;
        }

        input.ui-datepicker
        {
            width: 80px;
        }

        .mc-menu-btn
        {
            display: none;
        }

        #navi2 {
            display: none;
        }
    </style>
    <script type="text/javascript">
        function setupdatepicker(from, to) {
            var dates = $('#' + from + ', #' + to).datepicker({
                changeMonth: true,
                numberOfMonths: 3,
                onSelect: function (selectedDate) {
                    var option = this.id == from ? "minDate" : "maxDate";
                    var instance = $(this).data("datepicker");
                    var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                }
            });
        }
        $(document).ready(function () {
            setupdatepicker('<%=txtCreateTimeS.ClientID%>', '<%=txtCreateTimeE.ClientID%>');
            setupdatepicker('<%=txtBusinessStartTimeS.ClientID%>', '<%=txtBusinessStartTimeE.ClientID%>');

            var availableSalesNameTags = [<%=SalesNameArray%>];
            $('#<%=txtSalesName.ClientID %>').autocomplete({
                source: availableSalesNameTags
            });
        });

        function checkStoreList(obj) {
            var storeList = $(obj).prev('[id*=ddlStoreList]');
            if (storeList != undefined && storeList.css('display') == 'none') {
                storeList.css('display', '');
                return false;
            }
            return true;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <asp:HiddenField ID="hidEmpName" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hidEmpDeptId" runat="server"></asp:HiddenField>
    <asp:Panel ID="divSearch" runat="server" DefaultButton="btnSearch">
        <table style="padding: 5px 5px 5px 5px;">
            <tr>
                <td style="vertical-align: top">
                    <table style="padding: 10px 10px 15px 20px; border-style: groove; border-width: 1px;
                        background-color: White; letter-spacing: 2px; line-height: 30px;">
                        <tr>
                            <td>
                                <table style="width: 900px;">
                                    <tr>
                                        <td colspan="4" style="font-size: large; font-weight: bold;">業務工單管理
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Title">店家名稱
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSellerName" runat="server" Width="200px"></asp:TextBox>
                                        </td>
                                        <td class="Title">工單編號
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtBusinessOrderId" runat="server" Width="100px"></asp:TextBox>
                                            <asp:Button ID="btnAddBussinessOrder" Text="新增工單" PostBackUrl="~/Sal/BusinessOrderDetail.aspx"
                                                runat="server" Visible="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Title">簽約公司
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCompanyName" runat="server" Width="200px"></asp:TextBox>
                                        </td>
                                        <td class="Title">建立日期
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCreateTimeS" runat="server" Columns="8" CssClass="ui-datepicker" />
                                            ～
                                            <asp:TextBox ID="txtCreateTimeE" runat="server" Columns="8" CssClass="ui-datepicker" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Title" id="SearchSalesTitle" runat="server">負責業務
                                        </td>
                                        <td id="SearchSales" runat="server">
                                            <asp:TextBox ID="txtSalesName" runat="server" Width="80px"></asp:TextBox>
                                            &nbsp;&nbsp;<span class="Title">建立人員</span>
                                            <asp:TextBox ID="txtCreateId" runat="server"></asp:TextBox>
                                        </td>
                                        <td class="Title">檔次類型
                                        </td>
                                        <td class="General">
                                            <asp:DropDownList ID="ddlBusinessType" runat="server">
                                                <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                                                <asp:ListItem Text="好康" Value="Ppon"></asp:ListItem>
                                                <asp:ListItem Text="宅配" Value="Product"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:CheckBox ID="chkIsTravelType" runat="server" Text="查旅遊" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Title">上檔日期
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtBusinessStartTimeS" runat="server" Columns="8" CssClass="ui-datepicker" />
                                            ～
                                            <asp:TextBox ID="txtBusinessStartTimeE" runat="server" Columns="8" CssClass="ui-datepicker" />
                                        </td>
                                        <td class="Title General">免稅查詢
                                        </td>
                                        <td class="General">
                                            <asp:RadioButtonList ID="rdlInvoiceType" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="" Text="忽略" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="DuplicateUniformInvoice" Text="免稅"></asp:ListItem>
                                                <asp:ListItem Value="Other" Text="其他"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Title General">送審狀態
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlStatus" runat="server">
                                                <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                                                <asp:ListItem Text="草稿（業務）" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="送審（業務主管）" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="核准（各區營管）" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="已複查（營管）" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="已排檔（創意編輯）" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="已建檔（完成）" Value="5"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td colspan="2" class="General">
                                            <asp:CheckBox ID="chkIsMohistVerify" runat="server" Text="查墨攻" />
                                            <asp:CheckBox ID="chkIsTrustHwatai" runat="server" Text="查華泰" />
                                            <asp:CheckBox ID="chkIsNotContractSend" runat="server" Text="查合約未回" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Title" id="SearchSalesPlaceTitle" runat="server">審核區域
                                        </td>
                                        <td id="SearchSalesPlace" runat="server">
                                            <asp:HiddenField ID="hidSalesLevel" runat="server" />
                                            <asp:DropDownList ID="ddlSalesDept" runat="server" DataTextField="DeptName" DataValueField="DeptId"
                                                AppendDataBoundItems="true">
                                                <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td colspan="2">
                                            <a href="../Vourcher/vourcherseller.aspx" target="_blank" style="color: Blue">優惠券系統連結</a>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="../sal/BackendRecentDeals.aspx" target="_blank" style="color: Blue">查詢過往好康</a>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="../sal/MembershipCardQuery.aspx" target="_blank" style="color: Blue">熟客卡查詢</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="Title" id="tdDeptSetPlace" runat="server" visible="false">區域管理設定
                                            <asp:Button ID="btnDeptSave" runat="server" Text="設定" OnClick="btnDeptSave_Click" /><br />
                                            <asp:HiddenField ID="hidSalesArea" runat="server" />
                                            <asp:Repeater ID="repSalesDept" runat="server" OnItemDataBound="repSalesDept_ItemDataBound">
                                                <HeaderTemplate>
                                                    <ul style="list-style-type: circle; font-size: small; padding-left: 40px">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <li>
                                                        <asp:HiddenField ID="hidLeaderId" runat="server" />
                                                        <asp:Label ID="lblLeaderId" runat="server" Text=""></asp:Label>
                                                        <asp:CheckBoxList ID="chklLeaderArea" Font-Bold="false" runat="server" RepeatDirection="Horizontal"
                                                            RepeatLayout="Flow" DataTextField="Name" DataValueField="Id">
                                                        </asp:CheckBoxList>
                                                    </li>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </ul>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="搜尋" />&nbsp;
                                        </td>
                                        <td colspan="2">
                                            下載微調:<asp:TextBox ID="txtDonwloadBottomMarginFix" runat="server" Width="20px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="padding-top: 10px; text-align: center">
                                <asp:GridView ID="gvBusinessList" runat="server" OnRowDataBound="gvBusinessList_RowDataBound"
                                    OnRowCommand="gvBusinessList_RowCommand" GridLines="Horizontal" ForeColor="Black"
                                    CellPadding="4" BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE" BackColor="White"
                                    EmptyDataText="無符合的資料" AutoGenerateColumns="false" Font-Size="Small" Width="900px"
                                    CssClass="GridViewStyle">
                                    <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                                        CssClass="AlignCenter"></HeaderStyle>
                                    <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                    <RowStyle BackColor="#F7F7DE" Height="20px" CssClass="AlignCenter"></RowStyle>
                                    <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" CssClass="AlignCenter"></PagerStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="類型">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBusinessType" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="AlignLeft" Width="3%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="工單編號">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lkBusinessOrderID" runat="server" ForeColor="Blue" CommandName="SHOW"></asp:LinkButton>
                                                <asp:HyperLink ID="hkBusinessOrderID" runat="server" ForeColor="Blue" Target="_blank"></asp:HyperLink>
                                            </ItemTemplate>
                                            <ItemStyle Width="7%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="店家名稱" ItemStyle-CssClass="AlignCenter">
                                            <ItemTemplate>
                                                <asp:Label ID="lblMohistVerify" runat="server" Text="(墨)" ForeColor="Red"></asp:Label>
                                                <asp:Label ID="lblTrustHwatai" runat="server" Text="(華)" ForeColor="DarkOrange"></asp:Label>
                                                <asp:HyperLink ID="hlSellerName" runat="server" ForeColor="Blue" Target="_blank"></asp:HyperLink>
                                            </ItemTemplate>
                                            <ItemStyle Width="14%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="上檔日期" ItemStyle-CssClass="AlignCenter">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBusinessHourTimeS" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="8%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="業務" ItemStyle-CssClass="AlignCenter">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSalesName" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="3%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="建立人員" ItemStyle-CssClass="AlignCenter">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCreateId" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="5%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="異動日期" ItemStyle-CssClass="AlignCenter">
                                            <ItemTemplate>
                                                <asp:Label ID="lblModifyTime" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="10%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="部門" ItemStyle-CssClass="AlignCenter" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDept" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="4%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="狀態" ItemStyle-CssClass="AlignCenter">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="3%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="複製" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCopyType" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="4%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="備註" ItemStyle-CssClass="AlignCenter" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblImportantMemo" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="9%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="下載" ItemStyle-CssClass="AlignCenter" Visible="false">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlStoreList" runat="server" AppendDataBoundItems="true" Style="display: none">
                                                    <asp:ListItem Text="總店" Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:LinkButton ID="lkDownload" runat="server" ForeColor="Blue" Text="下載" CommandName="DOWNLOAD"
                                                    OnClientClick="return checkStoreList(this);"></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle Width="3%" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <uc1:Pager ID="ucPager" runat="server" PageSize="15" ongetcount="GetCount" onupdate="UpdateHandler">
                                </uc1:Pager>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <asp:Panel ID="divBusinessOrderShow" runat="server" Visible="false">
        <asp:LinkButton ID="liBackToList" runat="server" Text="<< 返回列表" OnClick="liBackToList_Click"></asp:LinkButton>
        <div style="border-style: groove; border-width: 1px; padding: 10px 10px 15px 20px;
            background-image: url(../Themes/default/images/17Life/G2/preview.png); background-repeat: no-repeat;
            background-position: center;">
            <asp:Literal ID="liBusinessOrder" runat="server"></asp:Literal>
        </div>
    </asp:Panel>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StoreAdd.aspx.cs" Inherits="LunchKingSite.Web.Sal.StoreAdd" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="../Tools/js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" src="../Tools/js/homecook.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#selCity').change(function () { updateCity($('#selCity').val()); });
            if ($('[id*=txtAccountingBankDesc]').val() == '') {
                BindBankInfo();
            }
            if ($('[id*=txtAccountingBranchDesc]').val() == '') {
                BindBranchInfo();
            }
        });

        var citys = <%= CityJSonData %>;
        function updateCity(cid) {
            $.each(citys, function(i,item) {
                if(item.id == cid) {
                    var el = $('#selTownship');
                    el.children().remove();
                    el.addItems(eval(item.Townships), function (t) { return t.name; }, function (t) { return t.id; });
                    el.get(0).selectedIndex = 0;
                    return false;
                }
            });
        }

        function CloseWindow(){
            window.close();
        }
        function BindBankInfo() {
            var obj = $('[id*=ddlAccountingBank]');
            var id = obj.val();
            if (id != '') {
                $('[id*=txtAccountingBankId]').val(id);
                $('[id*=txtAccountingBankDesc]').val($.trim($(obj).find(':selected').text().replace(id,'')));
            }
        }

        function BindBranchInfo() {
            var obj = $('[id*=ddlAccountingBranch]');
            var id = obj.val();
            if (id != '' && id != null) {
                $('[id*=txtAccountingBranchId]').val(id);
                $('[id*=txtAccountingBranchDesc]').val($.trim($(obj).find(':selected').text().replace(id,'')));
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" EnablePartialRendering="true" ScriptMode="Release"
            EnableScriptGlobalization="false" EnableScriptLocalization="false"
            ID="sm">
            <CompositeScript ScriptMode="Release">
                <Scripts>
                    <asp:ScriptReference Name="MicrosoftAjax.js" />
                    <asp:ScriptReference Name="MicrosoftAjaxWebForms.js" />
                    <asp:ScriptReference Name="AjaxControlToolkit.Compat.Timer.Timer.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="AjaxControlToolkit.Common.Common.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="AjaxControlToolkit.Compat.DragDrop.DragDropScripts.js"
                        Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="AjaxControlToolkit.ExtenderBase.BaseScripts.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="AjaxControlToolkit.DragPanel.FloatingBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="AjaxControlToolkit.DynamicPopulate.DynamicPopulateBehavior.js"
                        Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="AjaxControlToolkit.RoundedCorners.RoundedCornersBehavior.js"
                        Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="AjaxControlToolkit.DropShadow.DropShadowBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Name="AjaxControlToolkit.ModalPopup.ModalPopupBehavior.js" Assembly="AjaxControlToolkit" />
                    <asp:ScriptReference Path="~/Tools/js/header.js" />
                    <asp:ScriptReference Path="~/Tools/js/homecook.js" />
                </Scripts>
            </CompositeScript>
        </asp:ScriptManager>
        <asp:Button ID="btnAdd" runat="server" Text="新增分店" OnClick="btnAdd_Click" />
        <asp:Panel ID="divEdit" runat="server" Visible="false">
            <table id="tbStore" style="font-size: smaller; width: 600px">
                <tr>
                    <td>
                        <asp:HiddenField ID="hidCityJSonData" runat="server" />
                        <fieldset style="background-color: #fff0d4">
                            <legend style="color: #4F8AFF">店鋪資料</legend>
                            <table style="padding: 10px 10px 10px 10px">
                                <tr>
                                    <td>分店名稱
                                    </td>
                                    <td>
                                        <asp:HiddenField ID="hidStoreId" runat="server" />
                                        <asp:TextBox ID="txtBranchName" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>電話
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStoreTel" runat="server" Width="300" placeholder="(區碼)12345678"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>地址
                                    </td>
                                    <td>
                                        <asp:HiddenField ID="hidAddressCityId" runat="server" />
                                        <asp:HiddenField ID="hidAddressTownshipId" runat="server" />
                                        <asp:Repeater ID="repCity" runat="server">
                                            <HeaderTemplate>
                                                <select name="selCity" id="selCity">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <option value="<%#Eval("Id")%>" <%# ((City)Container.DataItem).Id == AddressCityId ? "selected='selected'" : "" %>>
                                                    <%#Eval("CityName")%></option>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </select>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <asp:Repeater ID="repTown" runat="server">
                                            <HeaderTemplate>
                                                <select name="selTownship" id="selTownship">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <option value="<%#Eval("Id")%>" <%#((City)Container.DataItem).Id == AddressTownshipId ? "selected='selected'" : "" %>>
                                                    <%#Eval("CityName")%></option>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </select>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <asp:TextBox ID="txtStoreAddress" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>營業時間
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtOpeningTime" TextMode="MultiLine" runat="server" Width="300"></asp:TextBox><br />
                                        範例：週一~週五09:00~24:00；週六10:00~03:00
                                    </td>
                                </tr>
                                <tr>
                                    <td>公休日
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCloseDate" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>數量
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtQuantity" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>交通資訊
                                    </td>
                                    <td>捷運:&nbsp;<asp:TextBox ID="txtMrt" runat="server" Width="300"></asp:TextBox><br />
                                        開車:&nbsp;<asp:TextBox ID="txtCar" runat="server" Width="300"></asp:TextBox><br />
                                        公車:&nbsp;<asp:TextBox ID="txtBus" runat="server" Width="300"></asp:TextBox><br />
                                        其他:&nbsp;<asp:TextBox ID="txtOV" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>官網網址
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtWebUrl" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>FaceBook網址
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFBUrl" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Plurk網址
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPlurkUrl" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>部落格網址
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtBlogUrl" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>其他網址
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtOtherUrl" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>備註
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtRemark" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <fieldset style="background-color: #fff0d4">
                            <legend style="color: #4F8AFF">分店匯款資料</legend>
                            <table style="padding: 10px 10px 10px 10px;">
                                <tr>
                                    <td>簽約公司名稱
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStoreName" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>簽約公司ID/統一編號
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStoreSignCompanyID" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>簽約公司地址
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStoreCompanyAddress" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>負責人名稱
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStoreBossName" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>受款人ID/統編
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStoreID" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>受款人銀行/分行 資訊
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="upBankInfo" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="ddlAccountingBank" runat="server" AppendDataBoundItems="true"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlAccountingBank_SelectedIndexChanged"
                                                    onchange="BindBankInfo();">
                                                    <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                                                </asp:DropDownList>
                                                <br />
                                                <asp:DropDownList ID="ddlAccountingBranch" runat="server" onchange="BindBranchInfo();">
                                                </asp:DropDownList>
                                                <br />
                                                匯款銀行：
                                            <asp:TextBox ID="txtAccountingBankId" runat="server" Width="50px"></asp:TextBox>
                                                <asp:TextBox ID="txtAccountingBankDesc" runat="server" Width="200px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtAccountingBankId"
                                                    ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator><br />
                                                分 行：
                                            <asp:TextBox ID="txtAccountingBranchId" runat="server" Width="50px"></asp:TextBox>
                                                <asp:TextBox ID="txtAccountingBranchDesc" runat="server" Width="260px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="txtAccountingBranchId"
                                                    ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>受款人帳號
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStoreAccount" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>受款人戶名
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStoreAccountName" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>帳務聯絡人
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStoreAccountingName" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>帳務聯絡人電話
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStoreAccountingTel" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>帳務聯絡人信箱
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStoreEmail" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>店家備註
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStoreNotice" runat="server" Width="300"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="儲存" OnClick="btnSave_Click" Visible="false" />
                        <asp:Button ID="btnDelete" runat="server" Text="刪除" OnClick="btnDelete_Click" ForeColor="Red" OnClientClick="return confirm('確定刪除此筆分店資料?')" />
                        <asp:Button ID="btnClose" runat="server" Text="關閉" OnClick="btnClose_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="divStoreList" runat="server">
            <asp:GridView ID="gvStoreList" runat="server" OnRowDataBound="gvStoreList_RowDataBound"
                OnRowCommand="gvStoreList_RowCommand" GridLines="Horizontal" ForeColor="Black"
                CellPadding="4" BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE" BackColor="White"
                EmptyDataText="無符合的資料" AutoGenerateColumns="false" Font-Size="Small" EnableViewState="true"
                Width="800px">
                <FooterStyle BackColor="#CCCC99"></FooterStyle>
                <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
                <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
                <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                    HorizontalAlign="Center"></HeaderStyle>
                <Columns>
                    <asp:TemplateField HeaderText="分店名稱">
                        <ItemTemplate>
                            <asp:Label ID="lblStoreName" runat="server"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="200px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="數量">
                        <ItemTemplate>
                            <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="分店電話" DataField="StoreTel" ItemStyle-HorizontalAlign="Center"
                        ItemStyle-Width="170px" />
                    <asp:TemplateField HeaderText="地址">
                        <ItemTemplate>
                            <asp:Label ID="lblAddress" runat="server"></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="370px" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button ID="btnEdit" runat="server" Text="編輯" CommandArgument="<%# ((LunchKingSite.BizLogic.Model.Sales.SalesStore)Container.DataItem).Id %>"
                                CommandName="UPD" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button ID="btnDelete" runat="server" Text="刪除" ForeColor="Red" CommandArgument="<%# ((LunchKingSite.BizLogic.Model.Sales.SalesStore)Container.DataItem).Id %>"
                                CommandName="DEL" OnClientClick="return confirm('確定刪除?')" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </form>
</body>
</html>

﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.SellerSales;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Sal
{
    public partial class ProposalContent : RolePage, ISalesProposalContentView
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        public IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        public IHumanProvider hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        public IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        #region props
        private SalesProposalContentPresenter _presenter;
        public SalesProposalContentPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public int ProposalId
        {
            get
            {
                int id = 0;
                if (Request.QueryString["pid"] != null && int.TryParse(Request.QueryString["pid"].ToString(), out id))
                {
                    int.TryParse(Request.QueryString["pid"].ToString(), out id);
                }
                return id;
            }
        }

        private string _SellerId = "";
        public string SellerId
        {
            get
            {
                return _SellerId;
            }
            set
            {
                _SellerId = value;
            }
        }

        public string SalesManNameList = string.Empty;
        public string[] SalesmanNameArray
        {
            set
            {
                var rtn = string.Empty;
                foreach (var s in value)
                {
                    if (rtn == string.Empty)
                    {
                        rtn = string.Format("'{0}'", s);
                    }
                    else
                    {
                        rtn = string.Format("{0},'{1}'", rtn, s);
                    }
                }
                SalesManNameList = rtn;
            }
        }

        public Guid BusinessHourGuid
        {
            get
            {
                Guid bid = Guid.Empty;
                if (!string.IsNullOrEmpty(hkBusinessGuid.Text))
                {
                    Guid.TryParse(hkBusinessGuid.Text, out bid);
                }
                return bid;
            }
        }

        public Guid SellerGuid
        {
            get
            {
                Guid sid = Guid.Empty;
                if (Request.QueryString["sid"] != null)
                {
                    Guid.TryParse(Request.QueryString["sid"].ToString(), out sid);
                }
                return sid;
            }
        }

        public string AncestorBid
        {
            get
            {
                return txtAncestorBid.Text;
            }
        }
        public string AncestorSeqBid
        {
            get
            {
                return txtAncestorSeqBid.Text;
            }
        }

        private Guid _ProSellerGuid;
        public Guid ProSellerGuid
        {
            get
            {
                return _ProSellerGuid;
            }
            set
            {
                _ProSellerGuid = value;
            }
        }

        public ProposalStatus Status
        {
            get
            {
                ProposalStatus status = ProposalStatus.Initial;
                ProposalStatus.TryParse(hidStatus.Value, out status);
                return status;
            }
        }

        public List<int> AgentChannels
        {
            get
            {
                List<int> list = new List<int>();
                for (int idx = 0; idx < chkAgentChannels.Items.Count; idx++)
                {
                    if (chkAgentChannels.Items[idx].Selected)
                    {
                        string v = chkAgentChannels.Items[idx].Value;
                        int s = 0;
                        if (int.TryParse(v, out s))
                        {
                            list.Add(s);
                        }
                    }
                }

                return list;
            }
        }


        public string Tab
        {
            get
            {
                if (Request.QueryString["tab"] != null)
                {
                    return Request.QueryString["tab"].ToString();
                }
                return string.Empty;
            }
        }

        public bool IsPromotionDeal
        {
            get
            {
                return chkPromotionDeal.Checked;
            }
        }

        public bool IsExhibitionDeal
        {
            get
            {
                return chkExhibitionDeal.Checked;
            }
        }

        public bool IsGame
        {
            get
            {
                return chkGame.Checked;
            }
        }

        public bool IsChannelGift
        {
            get
            {
                return chkChannelGift.Checked;
            }
        }

        public Dictionary<int, string> ReturnReason
        {
            get
            {
                Dictionary<int, string> list = new Dictionary<int, string>();
                if (ddlDeliveryType.SelectedValue == ((int)DeliveryType.ToShop).ToString())
                {
                    if (chkCommissionInCompatible.Checked)
                    {
                        list.Add((int)ProposalReturnPpon.CommissionInCompatible, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.CommissionInCompatible));
                    }
                    if (chkFoldInCompatible.Checked)
                    {
                        list.Add((int)ProposalReturnPpon.FoldInCompatible, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.FoldInCompatible));
                    }
                    if (chkCopiesInCompatible.Checked)
                    {
                        list.Add((int)ProposalReturnPpon.CopiesInCompatible, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.CopiesInCompatible));
                    }
                    if (chkBusinessPriceLower.Checked)
                    {
                        list.Add((int)ProposalReturnPpon.BusinessPriceLower, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.BusinessPriceLower));
                    }
                    if (chkNeedOptimization.Checked)
                    {
                        list.Add((int)ProposalReturnPpon.NeedOptimization, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.NeedOptimization));
                    }
                    if (chkPoorThanBusinessStrife.Checked)
                    {
                        list.Add((int)ProposalReturnPpon.PoorThanBusinessStrife, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.PoorThanBusinessStrife));
                    }
                    if (chkDataModifyContractInComplete.Checked)
                    {
                        list.Add((int)ProposalReturnPpon.DataModifyContractInComplete, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.DataModifyContractInComplete));
                    }
                    if (chkCouponEventContentModify.Checked)
                    {
                        list.Add((int)ProposalReturnPpon.CouponEventContentModify, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.CouponEventContentModify));
                    }
                    if (chkDataModify.Checked)
                    {
                        list.Add((int)ProposalReturnPpon.DataModify, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.DataModify));
                    }
                    if (chkContractError.Checked)
                    {
                        list.Add((int)ProposalReturnPpon.ContractError, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.ContractError));
                    }
                    if (chkCancel.Checked)
                    {
                        list.Add((int)ProposalReturnPpon.CancelOrderTime, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.CancelOrderTime));
                    }
                    if (chkCheckError.Checked)
                    {
                        list.Add((int)ProposalReturnPpon.CheckError, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.CheckError));
                    }
                    if (chkOther.Checked)
                    {
                        list.Add((int)ProposalReturnPpon.Other, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.Other));
                    }
                }
                else
                {
                    if (chkOptimizationPrice.Checked)
                    {
                        list.Add((int)ProposalReturnHouse.OptimizationPrice, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.OptimizationPrice));
                    }
                    if (chkInformationError.Checked)
                    {
                        list.Add((int)ProposalReturnHouse.InformationError, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.InformationError));
                    }
                    if (chkPercentLower.Checked)
                    {
                        list.Add((int)ProposalReturnHouse.PercentLower, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.PercentLower));
                    }
                    if (chkExistProposal.Checked)
                    {
                        list.Add((int)ProposalReturnHouse.ExistProposal, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.ExistProposal));
                    }
                    if (chkSaleOverProposal.Checked)
                    {
                        list.Add((int)ProposalReturnHouse.SaleOverProposal, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.SaleOverProposal));
                    }
                    if (chkDataModify.Checked)
                    {
                        list.Add((int)ProposalReturnHouse.DataModify, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.DataModify));
                    }
                    if (chkContractError.Checked)
                    {
                        list.Add((int)ProposalReturnHouse.ContractError, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.ContractError));
                    }
                    if (chkCancel.Checked)
                    {
                        list.Add((int)ProposalReturnHouse.CancelOrderTime, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.CancelOrderTime));
                    }
                    if (chkCheckError.Checked)
                    {
                        list.Add((int)ProposalReturnHouse.CheckError, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.CheckError));
                    }
                    if (chkOther.Checked)
                    {
                        list.Add((int)ProposalReturnHouse.Other, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.Other));
                    }
                }

                return list;
            }
        }

        private bool _IsUrgent = false;
        public bool IsUrgent
        {
            get { return _IsUrgent; }
            set
            {
                _IsUrgent = value;
            }
        }

        private bool _IsOrderTimeSave = false;
        public bool IsOrderTimeSave
        {
            get { return _IsOrderTimeSave; }
            set
            {
                _IsOrderTimeSave = value;
            }
        }

        private Guid _CheckSellerGuid;
        public Guid CheckSellerGuid
        {
            get { return _CheckSellerGuid; }
        }

        public decimal MinGrossMargin
        {
            get { return config.GrossMargin; }
        }

        /// <summary>
        /// 是否具有權限設定[低毛利率可使用折價券]、[本檔不可使用折價券]
        /// </summary>
        public bool HasUseDiscountSetPrivilege
        {
            get
            {
                return CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.UserDiscount);
            }
        }

        /// <summary>
        /// 出貨日期格式
        /// </summary>
        public DealShippingDateType OrderShippingDateType
        {
            get
            {
                return (rboUniShipDay.Checked) ? DealShippingDateType.Special : DealShippingDateType.Normal;
            }
            set
            {
                switch (value)
                {
                    case DealShippingDateType.Normal:
                        rboNormalShipDay.Checked = true;
                        break;

                    case DealShippingDateType.Special:
                        rboUniShipDay.Checked = true;
                        break;

                    default:
                        break;
                }
            }
        }

        public int ReceiptType
        {
            get
            {
                if (rbRecordInvoice.Checked)
                {
                    return (int)VendorReceiptType.Invoice;
                }
                else if (rbRecordReceipt.Checked)
                {
                    return (int)VendorReceiptType.Receipt;
                }
                else if (rbRecordNoTaxInvoice.Checked)
                {
                    return (int)VendorReceiptType.NoTaxInvoice;
                }
                else if (rbRecordOthers.Checked)
                {
                    return (int)VendorReceiptType.Other;
                }
                else
                    return 99;

            }
        }

        public bool IsListCheck
        {
            get
            {
                return chkIsListCheck.Checked;
            }
        }

        /// <summary>
        /// 退件-資料修正細項Id的陣列字串
        /// </summary>
        /// <example>[1, 2, 3]</example>
        public string ReturnDetailDataModifyValue
        {
            get
            {
                return hidReturnDetailDataModifyValue.Value;
            }
        }
        /// <summary>
        /// 退件-資料修正細項名稱的字串 (chang_log用)
        /// </summary>
        /// <example>資料錯誤修正：原價 / 售價 / 店家電話</example>
        public string ReturnDetailDataModifyTxt
        {
            get
            {
                return hidReturnDetailDataModifyTxt.Value;
            }
        }

        public bool IsEnableVbsNewShipFile
        {
            get { return config.IsEnableVbsNewShipFile; }
        }
        #endregion

        #region event
        public event EventHandler<EventArgs> Save;
        public event EventHandler<DataEventArgs<DateTime>> OrderTimeSave;
        public event EventHandler<EventArgs> Urgent;
        public event EventHandler<EventArgs> Delete;
        public event EventHandler<EventArgs> CreateBusinessHour;
        public event EventHandler<DataEventArgs<string>> ChangeSeller;
        public event EventHandler<DataEventArgs<KeyValuePair<string, SellerSalesType>>> Referral;
        public event EventHandler<EventArgs> ProposalSend;
        public event EventHandler<DataEventArgs<string>> ProposalReturned;
        public event EventHandler<DataEventArgs<KeyValuePair<int, List<string>>>> Assign;
        public event EventHandler<DataEventArgs<string>> SellerProposalReturned;
        public event EventHandler<DataEventArgs<int>> SellerProposalCheckWaitting;
        public event EventHandler<DataEventArgs<int>> SellerProposalSaleEditor;
        public event EventHandler<DataEventArgs<string>> ChangeLog;
        public event EventHandler<DataEventArgs<string>> CheckFlag;
        public event EventHandler<DataEventArgs<KeyValuePair<Guid, ProposalCopyType>>> ReCreateProposal;
        public event EventHandler<DataEventArgs<KeyValuePair<int, ProposalCopyType>>> CloneProposal;
        public event EventHandler<EventArgs> PponStoreSave;
        public event EventHandler<DataEventArgs<int>> VbsSave;
        public event EventHandler<DataEventArgs<Guid>> Download;
        public event EventHandler<EventArgs> UpdateSellerGuid;
        #endregion

        #region method
        public void SetProposalContent(SalesBusinessModel model, ViewProposalSeller pro, ProposalLogCollection logs, ProposalLogCollection assignLogs,
            SellerCollection tcsc, ProposalStoreCollection psc, ProposalCouponEventContent pcec, ProposalContractFileCollection pcfc, ProposalCategoryDealCollection pcdc,
            Seller seller, ProposalSellerFileCollection sellerFiles, List<ShoppingCartFreight> scfs)
        {
            #region 商家資訊

            if (pro.ProposalCreatedType == (int)ProposalCreatedType.Seller && !Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.Send))
            {
                ShowMessage("查無此提案單。", ProposalContentMode.BackToList);
            }

            liSellerName.Text = pro.SellerName;
            _SellerId = pro.SellerId;
            _ProSellerGuid = pro.SellerGuid;
            hkSellerId.Text = pro.SellerId;
            txtChangeSeller.Text = pro.SellerId;
            hkSellerId.NavigateUrl = ResolveUrl("~/sal/SellerContent.aspx?sid=" + pro.SellerGuid);
            if (pro.TempStatus == (int)SellerTempStatus.Completed)
            {
                imgSellerStatus.ImageUrl = ResolveUrl("~/Themes/PCweb/images/Tick.png");
            }
            else
            {
                imgSellerStatus.ImageUrl = ResolveUrl("~/Themes/PCweb/images/x.png");
            }
            liSellerStatus.Text = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (SellerTempStatus)pro.TempStatus);

            #endregion

            #region 串接資訊

            if (pro.BusinessHourGuid.HasValue || pro.ReferrerBusinessHourGuid.HasValue)
            {
                divBusiness.Visible = true;
                if (pro.BusinessHourGuid != null)
                {
                    divBusinessHour.Visible = true;
                    hkBusinessGuid.Text = pro.BusinessHourGuid.ToString();
                    hkBusinessGuid.NavigateUrl = ResolveUrl("~/sal/BusinessContent.aspx?bid=" + pro.BusinessHourGuid);
                    if (Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.SettingCheck))
                    {
                        hkSetup.NavigateUrl = ResolveUrl("~/controlroom/ppon/setup.aspx?bid=" + pro.BusinessHourGuid);
                        hkSetup.Visible = true;
                    }

                    if (pro.OrderTimeS.HasValue)
                    {
                        liBusinessOrderTime.Text = txtBusinessHourOrderTimeS.Text = pro.OrderTimeS.Value.ToString("yyyy/MM/dd");
                    }

                    //建檔後以同步行銷文案到後台，不可再修正
                    txtMarketingResource.Enabled = false;
                }
                if (pro.ReferrerBusinessHourGuid != null)
                {
                    divReferrerBusinessHour.Visible = true;
                    liProposalCopyType.Text = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (ProposalCopyType)pro.CopyType);
                    hkReferrerBusinessHour.Text = pro.ReferrerBusinessHourGuid.ToString();
                    hkReferrerBusinessHour.NavigateUrl = ResolveUrl("~/sal/BusinessContent.aspx?bid=" + pro.ReferrerBusinessHourGuid);
                }
            }

            #endregion

            #region 業務資訊

            ProposalSalesModel proposalSalesModel = ProposalFacade.ProposalSaleGetByPid(pro.Id);
            liDevelopeSalesName.Text = proposalSalesModel.DevelopeSalesEmpName;
            liDevelopeSalesEmail.Text = proposalSalesModel.DevelopeSalesEmail;

            liOperationSalesName.Text = proposalSalesModel.OperationSalesEmpName;
            liOperationSalesEmail.Text = proposalSalesModel.OperationSalesEmail;

            #endregion

            #region 購物車
            hidShoppingCartFreightsItem.Value = new JsonSerializer().Serialize(scfs.Select(x =>
                                                    new
                                                    {
                                                        FreightId = x.Id,
                                                        FreightName = (x.Freights == 0 ? "免運" : string.Format("{0}(滿${1}免運，未滿收${2})", x.FreightsName, x.NoFreightLimit, x.Freights))
                                                    }
                                                    ));
            #endregion

            #region 提案內容

            ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);

            txtBrandName.Text = pro.BrandName;
            if (ddlDeliveryType.Items.FindByValue(pro.DeliveryType.ToString()) != null)
            {
                ddlDeliveryType.SelectedValue = pro.DeliveryType.ToString();
            }
            else
            {
                #region 設定消費方式預設值

                if (emp.DeptId == EmployeeChildDept.S010.ToString())
                {
                    ddlDeliveryType.SelectedValue = ((int)DeliveryType.ToHouse).ToString();
                }

                #endregion
            }
            if (ddlDealType.Items.FindByValue(pro.DealType.ToString()) != null)
            {
                ddlDealType.SelectedValue = pro.DealType.ToString();
            }
            else
            {
                #region 設定提案類型預設值

                EmployeeChildDept dept = EmployeeChildDept.TryParse(emp.DeptId, out dept) ? dept : EmployeeChildDept.S001;
                switch (dept)
                {
                    case EmployeeChildDept.S001:
                    case EmployeeChildDept.S002:
                    case EmployeeChildDept.S003:
                    case EmployeeChildDept.S004:
                    case EmployeeChildDept.S005:
                    case EmployeeChildDept.S006:
                    case EmployeeChildDept.S013:
                    case EmployeeChildDept.S014:
                        ddlDealType.SelectedValue = ((int)ProposalDealType.LocalDeal).ToString();
                        break;
                    case EmployeeChildDept.S007:
                        ddlDealType.SelectedValue = ((int)ProposalDealType.PBeauty).ToString();
                        break;
                    case EmployeeChildDept.S008:
                        //ddlDealType.SelectedValue = ((int)ProposalDealType.Travel).ToString();
                        break;
                    case EmployeeChildDept.S010:
                        ddlDealType.SelectedValue = ((int)ProposalDealType.Product).ToString();
                        break;
                    case EmployeeChildDept.S011:
                    case EmployeeChildDept.S012:
                        ddlDealType.SelectedValue = ((int)ProposalDealType.Piinlife).ToString();
                        break;
                    case EmployeeChildDept.S009:
                    default:
                        break;
                }

                #endregion
            }
            if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.GroupCoupon))
            {
                ddlGroupCouponType.SelectedValue = (pro.GroupCouponDealType ?? 0).ToString();
            }
            hidDealSubType.Value = pro.DealSubType.ToString();
            txtProductSpec.Text = pro.ProductSpec;
            txtBusinessPrice.Text = (pro.BusinessPrice != null ? pro.BusinessPrice.ToString() : "");

            //店家單據開立方式
            if (pro.VendorReceiptType == (int)VendorReceiptType.Invoice)
                rbRecordInvoice.Checked = true;
            else if (pro.VendorReceiptType == (int)VendorReceiptType.NoTaxInvoice)
                rbRecordNoTaxInvoice.Checked = true;
            else if (pro.VendorReceiptType == (int)VendorReceiptType.Receipt)
                rbRecordReceipt.Checked = true;
            else if (pro.VendorReceiptType == (int)VendorReceiptType.Other)
                rbRecordOthers.Checked = true;


            if (Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.Created) && model != null)
            {
                hidRecord.Value = model.Accounting.VendorReceiptType.ToString();
                hidInputTax.Value = model.Accounting.IsInputTaxRequired.ToString();
                hidGroupOrder.Value = model.Group.Status.ToString();

            }

            txtAccountingMessage.Text = pro.Othermessage;

            //配送方式
            if (pro.DeliveryMethod != null)
                ddlDeliveryMethod.SelectedValue = pro.DeliveryMethod.ToString();

            //鑑賞期
            if (pro.TrialPeriod != null)
            {
                chkTrialPeriod.Checked = pro.TrialPeriod ?? false;
            }

            //墨攻
            chkMohist.Enabled = (!Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.SettingCheck)); //送出排檔後鎖定選擇

            if (pro.DealType == (int)ProposalDealType.Travel)
            {
                chkMohistPanel.Visible = true;
                chkMohist.Checked = pro.Mohist ?? false;
            }
            else
            {
                chkMohist.Checked = false;
            }

            chkWeekdays.Checked = Helper.IsFlagSet(pro.ConsumerTime, ProposalConsumerTime.Weekdays);
            chkHoliday.Checked = Helper.IsFlagSet(pro.ConsumerTime, ProposalConsumerTime.Holiday);
            chkSpecialHoliday.Checked = Helper.IsFlagSet(pro.ConsumerTime, ProposalConsumerTime.SpecialHoliday);
            chkBreakfast.Checked = Helper.IsFlagSet(pro.ConsumerTime, ProposalConsumerTime.Breakfast);
            chkLunch.Checked = Helper.IsFlagSet(pro.ConsumerTime, ProposalConsumerTime.Lunch);
            chkDinner.Checked = Helper.IsFlagSet(pro.ConsumerTime, ProposalConsumerTime.Dinner);
            chkAfternoonTea.Checked = Helper.IsFlagSet(pro.ConsumerTime, ProposalConsumerTime.AfternoonTea);
            chkNightSnack.Checked = Helper.IsFlagSet(pro.ConsumerTime, ProposalConsumerTime.NightSnack);
            chkHolidayFare.Checked = Helper.IsFlagSet(pro.ConsumerTime, ProposalConsumerTime.HolidayFare);
            chkMinimumCharge.Checked = Helper.IsFlagSet(pro.ConsumerTime, ProposalConsumerTime.MinimumCharge);
            chkServiceCharge.Checked = Helper.IsFlagSet(pro.ConsumerTime, ProposalConsumerTime.ServiceCharge);


            chkSelfOptional.Checked = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.SelfOptional);
            chkGroupCoupon.Checked = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.GroupCoupon);
            chkNoRestrictedStore.Checked = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.NoRestrictedStore);
            chkFreeTax.Checked = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.FreeTax);
            chkParallelImportation.Checked = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.ParallelImportation);
            chkOnlineUse.Checked = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.OnlineUse);
            chkYearContract.Checked = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.YearContract);
            chkExclusive.Checked = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.Exclusive);
            chkFirstDeal.Checked = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.FirstDeal);
            chkUrgent.Checked = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.Urgent);
            chkNewItem.Checked = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.NewItem);
            chkEveryDayNewDeal.Checked = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.EveryDayNewDeal);
            chkSpecialPrice.Checked = pro.SpecialPrice;
            hidPhotographerAppoint.Checked = Helper.IsFlagSet(pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppoint);
            hidPhotographerAppointCheck.Checked = Helper.IsFlagSet(pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppointCheck);
            hidPhotographerAppointCancel.Checked = Helper.IsFlagSet(pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppointCancel);
            hidPhotographerAppointClose.Checked = Helper.IsFlagSet(pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppointClose);
            chkSystemPic.Checked = pro.SystemPic;
            cbxBankDeal.Checked = pro.IsBankDeal;
            chkPromotionDeal.Checked = pro.IsPromotionDeal;
            chkExhibitionDeal.Checked = pro.IsExhibitionDeal;
            chkGame.Checked = pro.IsGame;
            chkChannelGift.Checked = pro.IsChannelGift;
            chkTaiShinChosen.Checked = pro.IsTaishinChosen;

            List<int> agentChannels = ProposalFacade.GetAgentChannels(pro.AgentChannels);
            foreach (ListItem item in chkAgentChannels.Items)
            {
                if (agentChannels.Contains(Convert.ToInt32(item.Value)))
                {
                    item.Selected = true;
                }
            }

            //特殊需求
            chkLowGrossMarginAllowedDiscount.Checked = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.LowGrossMarginAllowedDiscount);
            chkNotAllowedDiscount.Checked = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.NotAllowedDiscount);

            #region 特殊標記

            Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(pro.SpecialFlagText);
            foreach (var item in Enum.GetValues(typeof(ProposalSpecialFlag)))
            {
                if (Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)item))
                {
                    CheckBox chk = null;
                    TextBox txt = null;
                    switch ((ProposalSpecialFlag)item)
                    {
                        case ProposalSpecialFlag.Photographers:
                            chk = chkPhotographers;
                            break;
                        case ProposalSpecialFlag.SellerPhoto:
                            chk = chkSellerPhoto;
                            break;
                        case ProposalSpecialFlag.FTPPhoto:
                            chk = chkFTPPhoto;
                            txt = txtFTPPhoto;
                            break;
                        case ProposalSpecialFlag.Beauty:
                            chk = chkBeauty;
                            txt = txtBeauty;
                            break;
                        case ProposalSpecialFlag.ExpiringProduct:
                            chk = chkExpiringProduct;
                            txt = txtExpiringProduct;
                            break;
                        case ProposalSpecialFlag.InspectRule:
                            chk = chkInspectRule;
                            txt = txtInspectRule;
                            break;
                        case ProposalSpecialFlag.InvoiceFounder:
                            chk = chkInvoiceFounder;
                            txt = txtInvoiceFounder;
                            break;
                        case ProposalSpecialFlag.LowGrossMarginAllowedDiscount:
                            chk = chkLowGrossMarginAllowedDiscount;
                            break;
                        case ProposalSpecialFlag.NotAllowedDiscount:
                            chk = chkNotAllowedDiscount;
                            break;
                        case ProposalSpecialFlag.StoreStory:
                            chk = chkStoreStory;
                            txt = txtStoreStory;
                            break;
                        case ProposalSpecialFlag.RecommendFood:
                            chk = chkRecommendFood;
                            txt = txtRecommendFood;
                            break;
                        case ProposalSpecialFlag.EnvironmentIntroduction:
                            chk = chkEnvironmentIntroduction;
                            txt = txtEnvironmentIntroduction;
                            break;
                        case ProposalSpecialFlag.AroundScenery:
                            chk = chkAroundScenery;
                            txt = txtAroundScenery;
                            break;
                        case ProposalSpecialFlag.SeasonActivity:
                            chk = chkSeasonActivity;
                            txt = txtSeasonActivity;
                            break;
                        case ProposalSpecialFlag.Travel:
                            chk = chkTravel;
                            txt = txtTravel;
                            break;
                        case ProposalSpecialFlag.DealStar:
                            txt = txtDealStar;
                            break;
                        case ProposalSpecialFlag.None:
                        default:
                            break;
                    }

                    if (chk != null)
                    {
                        chk.Checked = true;
                    }
                    if (txt != null)
                    {
                        txt.Text = specialText[(ProposalSpecialFlag)item];
                    }
                    else
                    {
                        //if ((ProposalSpecialFlag)item == ProposalSpecialFlag.Photographers)
                        //{
                        //    if (specialText != null)
                        //    {
                        //        string photoString = specialText[(ProposalSpecialFlag)item];
                        //        if (ddlDealType.SelectedValue == Convert.ToString((int)DeliveryType.ToShop))
                        //        {
                        //            if (photoString.Split('|').Length > 5)
                        //            {
                        //                txtPhotoAddress.Text = photoString.Split('|')[0];
                        //                if (photoString.Split('|').Length > 1)
                        //                {
                        //                    txtPhotoTel.Text = photoString.Split('|')[1];
                        //                }
                        //                if (photoString.Split('|').Length > 2)
                        //                {
                        //                    txtPhotoPhone.Text = photoString.Split('|')[2];
                        //                }
                        //                if (photoString.Split('|').Length > 3)
                        //                {
                        //                    txtPhotoMemo.Text = photoString.Split('|')[3];
                        //                }
                        //                if (photoString.Split('|').Length > 4)
                        //                {
                        //                    txtPhotoDate.Text = photoString.Split('|')[4];
                        //                }
                        //                if (photoString.Split('|').Length > 5)
                        //                {
                        //                    txtPhotoUrl.Text = photoString.Split('|')[5];
                        //                }
                        //                if (photoString.Split('|').Length > 6)
                        //                {
                        //                    txtPhotoContact.Text = photoString.Split('|')[6];
                        //                }
                        //                if (photoString.Split('|').Length > 7)
                        //                {
                        //                    if (photoString.Split('|')[7] == "M")
                        //                    {
                        //                        radMale.Checked = true;
                        //                    }
                        //                    else
                        //                    {
                        //                        radFemale.Checked = true;
                        //                    }
                        //                }
                        //            }
                        //        }
                        //        else
                        //        {
                        //            if (photoString.Split('|').Length > 2)
                        //            {
                        //                txtPhohoNeeds.Text = photoString.Split('|')[0];
                        //                if (photoString.Split('|').Length > 1)
                        //                {
                        //                    txtRefrenceData.Text = photoString.Split('|')[1];
                        //                }
                        //            }
                        //        }
                        //    }
                        //}
                    }
                }
                else
                {
                    CheckBox chk = null;
                    TextBox txt = null;
                    switch ((ProposalSpecialFlag)item)
                    {
                        case ProposalSpecialFlag.DealStar:
                            chk = chkDealStar;
                            txt = txtDealStar;
                            break;
                    }

                    //預設不勾
                    if (chk != null && pro.SpecialFlagText != "")
                    {
                        chk.Checked = true;
                    }

                }
            }

            #endregion

            #region 檔次特色
            Dictionary<ProposalDealCharacter, string> dealcharacterText = new JsonSerializer().Deserialize<Dictionary<ProposalDealCharacter, string>>(pro.DealCharacterFlagText);
            foreach (var item in Enum.GetValues(typeof(ProposalDealCharacter)))
            {
                if (Helper.IsFlagSet(pro.DealCharacterFlag, (ProposalDealCharacter)item))
                {
                    TextBox txt = null;
                    if (pro.DeliveryType == (int)DeliveryType.ToShop && pro.DealType == (int)ProposalDealType.PBeauty)
                    {
                        switch ((ProposalDealCharacter)item)
                        {
                            case ProposalDealCharacter.PBeautyDevice:
                                txt = txtDealCharacterToShop1;
                                break;
                            case ProposalDealCharacter.PBeautyProduct:
                                txt = txtDealCharacterToShop2;
                                break;
                            case ProposalDealCharacter.PBeautyLocation:
                                txt = txtDealCharacterToShop3;
                                break;
                            case ProposalDealCharacter.PBeautyEnvironment:
                                txt = txtDealCharacterToShop4;
                                break;
                            case ProposalDealCharacter.PBeautyOther:
                                txt = txtDealCharacterToShop5;
                                break;
                        }
                    }
                    else if (pro.DeliveryType == (int)DeliveryType.ToShop && pro.DealType != (int)ProposalDealType.PBeauty && pro.DealType != (int)ProposalDealType.Travel)
                    {
                        switch ((ProposalDealCharacter)item)
                        {
                            case ProposalDealCharacter.ToShopFood:
                                txt = txtDealCharacterToShop1;
                                break;
                            case ProposalDealCharacter.ToShopSpecialFood:
                                txt = txtDealCharacterToShop2;
                                break;
                            case ProposalDealCharacter.ToShopCook:
                                txt = txtDealCharacterToShop3;
                                break;
                            case ProposalDealCharacter.ToShopEnvironment:
                                txt = txtDealCharacterToShop4;
                                break;
                            case ProposalDealCharacter.ToShopOther:
                                txt = txtDealCharacterToShop5;
                                break;
                        }
                    }
                    else
                    {
                        switch ((ProposalDealCharacter)item)
                        {
                            case ProposalDealCharacter.ToHouse1:
                                txt = txtDealCharacterToHouse1;
                                break;
                            case ProposalDealCharacter.ToHouse2:
                                txt = txtDealCharacterToHouse2;
                                break;
                            case ProposalDealCharacter.ToHouse3:
                                txt = txtDealCharacterToHouse3;
                                break;
                            case ProposalDealCharacter.ToHouse4:
                                txt = txtDealCharacterToHouse4;
                                break;
                        }
                    }

                    if (txt != null)
                    {
                        if (dealcharacterText.ContainsKey((ProposalDealCharacter)item))
                        {
                            txt.Text = dealcharacterText[(ProposalDealCharacter)item];
                        }
                    }
                }
                else
                {
                    CheckBox chk = null;
                    if (pro.DeliveryType == (int)DeliveryType.ToShop && pro.DealType == (int)ProposalDealType.PBeauty)
                    {
                        switch ((ProposalDealCharacter)item)
                        {
                            case ProposalDealCharacter.PBeautyDevice:
                                chk = chkDealCharacterToShop1;
                                break;
                            case ProposalDealCharacter.PBeautyProduct:
                                chk = chkDealCharacterToShop2;
                                break;
                            case ProposalDealCharacter.PBeautyLocation:
                                chk = chkDealCharacterToShop3;
                                break;
                            case ProposalDealCharacter.PBeautyEnvironment:
                                chk = chkDealCharacterToShop4;
                                break;
                        }
                    }
                    else if (pro.DeliveryType == (int)DeliveryType.ToShop && pro.DealType != (int)ProposalDealType.PBeauty && pro.DealType != (int)ProposalDealType.Travel)
                    {
                        switch ((ProposalDealCharacter)item)
                        {
                            case ProposalDealCharacter.ToShopFood:
                                chk = chkDealCharacterToShop1;
                                break;
                            case ProposalDealCharacter.ToShopSpecialFood:
                                chk = chkDealCharacterToShop2;
                                break;
                            case ProposalDealCharacter.ToShopCook:
                                chk = chkDealCharacterToShop3;
                                break;
                            case ProposalDealCharacter.ToShopEnvironment:
                                chk = chkDealCharacterToShop4;
                                break;
                        }
                    }
                    else
                    {
                        switch ((ProposalDealCharacter)item)
                        {
                            case ProposalDealCharacter.ToHouse1:
                            case ProposalDealCharacter.ToHouse2:
                            case ProposalDealCharacter.ToHouse3:
                                chk = chkDealCharacterToHouse;
                                break;
                        }
                    }

                    //預設不勾
                    if (chk != null && pro.SpecialFlagText != "")
                    {
                        chk.Checked = true;
                    }
                }
            }



            //檔次特色
            if (pro.DeliveryType == (int)DeliveryType.ToShop && pro.DealType == (int)ProposalDealType.PBeauty)
            {
                lbldealcharacterToShop1.InnerText = "1." + Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalDealCharacter.PBeautyDevice);
                lbldealcharacterToShop2.InnerText = "2." + Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalDealCharacter.PBeautyProduct);
                lbldealcharacterToShop3.InnerText = "3." + Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalDealCharacter.PBeautyLocation);
                lbldealcharacterToShop4.InnerText = "4." + Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalDealCharacter.PBeautyEnvironment);
            }
            else if (pro.DeliveryType == (int)DeliveryType.ToShop && pro.DealType != (int)ProposalDealType.PBeauty)
            {
                lbldealcharacterToShop1.InnerText = "1." + Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalDealCharacter.ToShopFood);
                lbldealcharacterToShop2.InnerText = "2." + Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalDealCharacter.ToShopSpecialFood);
                lbldealcharacterToShop3.InnerText = "3." + Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalDealCharacter.ToShopCook);
                lbldealcharacterToShop4.InnerText = "4." + Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalDealCharacter.ToShopEnvironment);
            }
            #endregion

            #region 媒體報導
            List<ProposalMediaReportLink> mediareportText = new JsonSerializer().Deserialize<List<ProposalMediaReportLink>>(pro.MediaReportFlagText);
            if (mediareportText == null)
                hidMediaReport.Value = "2";
            else if (mediareportText.Count == 0)
                hidMediaReport.Value = "2";
            else
                hidMediaReport.Value = (mediareportText.Count() + 1).ToString();


            foreach (var item in Enum.GetValues(typeof(ProposalMediaReport)))
            {
                if (Helper.IsFlagSet(pro.MediaReportFlag, (ProposalMediaReport)item))
                {
                    CheckBox chk = null;
                    switch ((ProposalMediaReport)item)
                    {
                        case ProposalMediaReport.Pic:
                            chk = chkMediaReport;
                            chk.Checked = true;
                            chk = chkMediaReportPic;
                            chk.Checked = true;
                            break;
                        case ProposalMediaReport.Link:
                            chk = chkMediaReport;
                            chk.Checked = true;
                            for (int i = 0; i < mediareportText.Count; i++)
                            {

                                switch (i + 1)
                                {
                                    case 1:
                                        divMediaReportLink1.Style.Add("display", "");
                                        txtMediaReport1.Text = mediareportText[i].Link;
                                        if (mediareportText[i].Type == (int)ProposalMediaReport.LinkIn)
                                            rdbMediaReportIn1.Checked = true;
                                        else if (mediareportText[i].Type == (int)ProposalMediaReport.LinkOut)
                                            rdbMediaReportOut1.Checked = true;
                                        break;
                                    case 2:
                                        divMediaReportLink2.Style.Add("display", "");
                                        txtMediaReport2.Text = mediareportText[i].Link;
                                        if (mediareportText[i].Type == (int)ProposalMediaReport.LinkIn)
                                            rdbMediaReportIn2.Checked = true;
                                        else if (mediareportText[i].Type == (int)ProposalMediaReport.LinkOut)
                                            rdbMediaReportOut2.Checked = true;

                                        break;
                                    case 3:
                                        divMediaReportLink3.Style.Add("display", "");
                                        txtMediaReport3.Text = mediareportText[i].Link;
                                        if (mediareportText[i].Type == (int)ProposalMediaReport.LinkIn)
                                            rdbMediaReportIn3.Checked = true;
                                        else if (mediareportText[i].Type == (int)ProposalMediaReport.LinkOut)
                                            rdbMediaReportOut3.Checked = true;

                                        break;
                                    case 4:
                                        divMediaReportLink4.Style.Add("display", "");
                                        txtMediaReport4.Text = mediareportText[i].Link;
                                        if (mediareportText[i].Type == (int)ProposalMediaReport.LinkIn)
                                            rdbMediaReportIn4.Checked = true;
                                        else if (mediareportText[i].Type == (int)ProposalMediaReport.LinkOut)
                                            rdbMediaReportOut4.Checked = true;

                                        break;
                                    case 5:
                                        divMediaReportLink5.Style.Add("display", "");
                                        txtMediaReport5.Text = mediareportText[i].Link;
                                        if (mediareportText[i].Type == (int)ProposalMediaReport.LinkIn)
                                            rdbMediaReportIn5.Checked = true;
                                        else if (mediareportText[i].Type == (int)ProposalMediaReport.LinkOut)
                                            rdbMediaReportOut5.Checked = true;

                                        break;

                                }
                            }
                            break;
                    }
                }
            }
            #endregion

            txtMarketAnalysis.Text = pro.MarketAnalysis;
            txtMarketingResource.Text = pro.MarketingResource;
            txtScheduleExpect.Text = pro.ScheduleExpect;
            ddlStartUseUnit.SelectedValue = pro.StartUseUnit.ToString();
            txtStartUse.Text = pro.StartUseText;
            txtStartUse2.Text = pro.StartUseText2;
            ddlShipType.SelectedValue = pro.ShipType.ToString();
            if (pro.ShippingdateType == (int)DealShippingDateType.Normal)
            {
                //訂單成立後
                rboUniShipDay.Checked = false;
                rboNormalShipDay.Checked = true;
                txtShipText1.Text = "";
                txtShipText2.Text = "";
                txtShipText3.Text = pro.ShipText3;
            }
            else
            {
                //最早出貨日
                rboUniShipDay.Checked = true;
                rboNormalShipDay.Checked = false;
                txtShipText1.Text = pro.ShipText1;
                txtShipText2.Text = pro.ShipText2;
                txtShipText3.Text = "";
            }

            txtShipTypeOther.Text = pro.ShipOther;

            txtContractMemo.Text = pro.ContractMemo;
            txtMemo.Text = pro.Memo;

            #endregion

            #region 檢核狀態

            hidStatus.Value = pro.Status.ToString();
            hidApplyFlag.Value = pro.ApplyFlag.ToString();
            hidApproveFlag.Value = pro.ApproveFlag.ToString();
            hidCreatedFlag.Value = pro.BusinessCreateFlag.ToString();
            hidBusinessCheckFlag.Value = pro.BusinessFlag.ToString();
            hidEditorFlag.Value = pro.EditFlag.ToString();
            hidEditorPassFlag.Value = pro.EditPassFlag.ToString();
            hidListingFlag.Value = pro.ListingFlag.ToString();
            rptStatus.DataSource = SellerFacade.GetProposalFlags(pro);
            rptStatus.DataBind();
            rptStatus.Visible = Helper.IsFlagSet(pro.ApplyFlag, ProposalApplyFlag.Apply);
            hidShoppingCartV2Enabled.Value = config.ShoppingCartV2Enabled ? "1" : "0";

            #endregion

            #region 多檔次

            if (pro.DeliveryType != 0)
            {
                divMultiDeals.Visible = true;

                ProposalMultiDealCollection multiDeals = new ProposalMultiDealCollection();
                rptMultiDeal.DataSource = multiDeals;
                rptMultiDeal.DataBind();
            }

            #endregion

            #region 指派

            rptAssignLogs.DataSource = assignLogs.OrderByDescending(x => x.CreateTime);
            rptAssignLogs.DataBind();

            #endregion

            #region 歷史紀錄&留言

            rptLog.DataSource = logs.OrderByDescending(x => x.CreateTime);
            rptLog.DataBind();

            #endregion

            #region 檔案上傳

            if (pro.DeliveryType == (int)DeliveryType.ToHouse)
            {
                panSellerFile.Visible = true;
                rptSellerFile.DataSource = sellerFiles.OrderByDescending(x => x.CreateTime);
                rptSellerFile.DataBind();
            }
            #endregion


            #region 分店

            if (!Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.Created))
            {
                chkIsShowVbsAccountID.Visible = false;
            }



            if (tcsc.Count() > 0)
            {
                if ((pro.DeliveryType == (int)DeliveryType.ToShop) || (pro.DeliveryType == (int)DeliveryType.ToHouse && (pro.DealType == (int)ProposalDealType.Travel || pro.DealType == (int)ProposalDealType.Piinlife)))
                {
                    //宅配不須營業據點
                    aPponStore.Visible = true;
                    divPponStore.Visible = true;

                    rptPponStore.DataSource = tcsc.GroupJoin(psc, xs => xs.Guid, xpsc => xpsc.StoreGuid, (xs, xpsc) => new { seller = xs, proposalStores = xpsc.DefaultIfEmpty(new ProposalStore()) })
                                                  .SelectMany(a => a.proposalStores.Select(b => new { seller = a.seller, proposalStore = b }))
                                                  .OrderByDescending(x => Helper.IsFlagSet(x.proposalStore.VbsRight, VbsRightFlag.Location)).ThenBy(x => x.proposalStore.SortOrder).ThenBy(x => x.seller.CreateTime)
                                                  .Select(x => x.seller)
                                                  .ToDictionary(x => x, y => psc.Where(s => s.ProposalId == ProposalId && s.StoreGuid == y.Guid).FirstOrDefault());
                    rptPponStore.DataBind();

                    var ppon = psc.Where(s => Helper.IsFlagSet(s.VbsRight, VbsRightFlag.Verify)).Select(x => x.StoreGuid).ToArray();
                    ddlStoreList.Visible = Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck) ? true : false;
                    ddlStoreList.DataSource = tcsc.Where(x => x.Guid.EqualsAny(ppon));
                    ddlStoreList.DataTextField = "SellerName";
                    ddlStoreList.DataValueField = "Guid";
                    ddlStoreList.DataBind();

                    if (Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck) && ((pro.DeliveryType == (int)DeliveryType.ToShop) || (pro.DeliveryType == (int)DeliveryType.ToHouse && (pro.DealType == (int)ProposalDealType.Travel || pro.DealType == (int)ProposalDealType.Piinlife))))
                        ddlStoreList.Items.Insert(0, new ListItem("整份含各店資料全印", Guid.Empty.ToString()));

                }

                //核銷頁簽都要顯示
                aVbs.Visible = true;
                divVbs.Visible = true;
                rptVbs.DataSource = tcsc.OrderBy(x => x.CreateTime).ToDictionary(x => x, y => psc.Where(s => s.ProposalId == ProposalId && s.StoreGuid == y.Guid).FirstOrDefault());
                rptVbs.DataBind();
            }

            chkIsListCheck.Visible = Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck) ? true : false;


            #endregion

            #region 接續份數
            if (pro.AncestorBusinessHourGuid.HasValue)
            {
                txtAncestorBid.Text = pro.AncestorBusinessHourGuid.Value.ToString();
            }
            if (pro.AncestorSequenceBusinessHourGuid.HasValue)
            {
                txtAncestorSeqBid.Text = pro.AncestorSequenceBusinessHourGuid.Value.ToString();
            }
            #endregion


            #region 權益說明

            int _DeliveryType = (int)DeliveryType.ToShop;
            switch (Convert.ToInt16(ddlDealType.SelectedValue))
            {
                case (int)DeliveryType.ToHouse:
                    _DeliveryType = (int)ProvisionDepartmentType.Product;
                    break;
                case (int)DeliveryType.ToShop:
                    if (pro.DealType == (int)ProposalDealType.Travel)
                    {
                        _DeliveryType = (int)ProvisionDepartmentType.Travel;
                    }
                    else
                    {
                        _DeliveryType = (int)ProvisionDepartmentType.Ppon;
                    }
                    break;
                default:
                    _DeliveryType = (int)ProvisionDepartmentType.Ppon;
                    break;
            }

            hkRestrictionsEdit.NavigateUrl = ResolveUrl("~/sal/RestrictionsGenerator.aspx?pid=" + pro.Id + "&type=" + _DeliveryType.ToString());
            if (pcec.Restrictions != null)
            {
                liRestrictions.Text = pcec.Restrictions.Replace("\n", "<br />");
            }

            #endregion



            #region 巿場分析比價
            if (pro.DealType == (int)ProposalDealType.Product)
            {
                if (!string.IsNullOrEmpty(pro.SaleMarketAnalysis))
                {
                    var ma = new JsonSerializer().Deserialize<ProposalMarketAnalysis>(pro.SaleMarketAnalysis);
                    if (ma != null)
                    {
                        txtGomajiPrice.Text = ma.GomajiPrice;
                        txtGomajiLink.Text = ma.GomajiLink;
                        txtKuobrothersPrice.Text = ma.KuobrothersPrice;
                        txtKuobrothersLink.Text = ma.KuobrothersLink;
                        txtCrazymikePrice.Text = ma.CrazymikePrice;
                        txtCrazymikeLink.Text = ma.CrazymikeLink;
                        txtEzPricePrice1.Text = ma.EzPricePrice1;
                        txtEzPriceLink1.Text = ma.EzPriceLink1;
                        txtEzPricePrice2.Text = ma.EzPricePrice2;
                        txtEzPriceLink2.Text = ma.EzPriceLink2;
                        txtEzPricePrice3.Text = ma.EzPricePrice3;
                        txtEzPriceLink3.Text = ma.EzPriceLink3;
                        txtPinglePrice1.Text = ma.PinglePrice1;
                        txtPingleLink1.Text = ma.PingleLink1;
                        txtPinglePrice2.Text = ma.PinglePrice2;
                        txtPingleLink2.Text = ma.PingleLink2;
                        txtPinglePrice3.Text = ma.PinglePrice3;
                        txtPingleLink3.Text = ma.PingleLink3;
                        txtSimilarPrice.Text = ma.SimilarPrice;
                        txtSimilarLink.Text = ma.SimilarLink;
                        if (!string.IsNullOrEmpty(ma.OtherShop1))
                        {
                            ddlOtherShop1.SelectedValue = ma.OtherShop1;
                        }
                        txtOtherPrice1.Text = ma.OtherPrice1;
                        txtOtherLink1.Text = ma.OtherLink1;
                        if (!string.IsNullOrEmpty(ma.OtherShop2))
                        {
                            ddlOtherShop2.SelectedValue = ma.OtherShop2;
                        }
                        txtOtherPrice2.Text = ma.OtherPrice2;
                        txtOtherLink2.Text = ma.OtherLink2;
                        if (!string.IsNullOrEmpty(ma.OtherShop3))
                        {
                            ddlOtherShop3.SelectedValue = ma.OtherShop3;
                        }
                        txtOtherPrice3.Text = ma.OtherPrice3;
                        txtOtherLink3.Text = ma.OtherLink3;
                    }
                }
            }


            #endregion

            #region 上檔頻道

            if (pcdc.Count > 0)
            {
                foreach (RepeaterItem channelItem in rptChannel.Items)
                {
                    HiddenField hidChannel = (HiddenField)channelItem.FindControl("hidChannel");
                    CheckBox chkChannel = (CheckBox)channelItem.FindControl("chkChannel");
                    int channel = int.TryParse(hidChannel.Value, out channel) ? channel : 0;
                    if (channel != 0)
                    {
                        if (pcdc.Any(x => x.Cid == channel))
                        {
                            chkChannel.Checked = true;

                            // 區域
                            Repeater rptArea = (Repeater)channelItem.FindControl("rptArea");
                            foreach (RepeaterItem areaItem in rptArea.Items)
                            {
                                HiddenField hidArea = (HiddenField)areaItem.FindControl("hidArea");
                                CheckBox chkArea = (CheckBox)areaItem.FindControl("chkArea");
                                int area = int.TryParse(hidArea.Value, out area) ? area : 0;
                                if (area != 0)
                                {
                                    if (pcdc.Any(x => x.Cid == area))
                                    {
                                        chkArea.Checked = true;


                                    }
                                }
                            }

                            Repeater rptArea2 = (Repeater)channelItem.FindControl("rptArea2");
                            foreach (RepeaterItem areaItem in rptArea2.Items)
                            {
                                // 商圈
                                Repeater rptCommercial = (Repeater)areaItem.FindControl("rptCommercial");
                                foreach (RepeaterItem commItem in rptCommercial.Items)
                                {
                                    HiddenField hidCommercial = (HiddenField)commItem.FindControl("hidCommercial");
                                    CheckBox chkCommercial = (CheckBox)commItem.FindControl("chkCommercial");
                                    int id = int.TryParse(hidCommercial.Value, out id) ? id : 0;
                                    if (id != 0)
                                    {
                                        if (pcdc.Any(x => x.Cid == id))
                                        {
                                            chkCommercial.Checked = true;
                                        }
                                    }
                                }

                                //旅遊-商圈‧景點
                                Repeater rptSPRegion = (Repeater)areaItem.FindControl("rptSPRegion");
                                foreach (RepeaterItem spregion in rptSPRegion.Items)
                                {
                                    Repeater rptSpecialRegion = (Repeater)spregion.FindControl("rptSpecialRegion");
                                    foreach (RepeaterItem specialregion in rptSpecialRegion.Items)
                                    {
                                        HiddenField hidSpecialRegion = (HiddenField)specialregion.FindControl("hidSpecialRegion");
                                        CheckBox chkSpecialRegion = (CheckBox)specialregion.FindControl("chkSpecialRegion");
                                        int id = int.TryParse(hidSpecialRegion.Value, out id) ? id : 0;
                                        if (id != 0)
                                        {
                                            if (pcdc.Any(x => x.Cid == id))
                                            {
                                                chkSpecialRegion.Checked = true;
                                            }
                                        }
                                    }
                                }
                            }



                            // 分類
                            Repeater rptCategory = (Repeater)channelItem.FindControl("rptCategory");
                            foreach (RepeaterItem areaItem in rptCategory.Items)
                            {
                                HiddenField hidCategory = (HiddenField)areaItem.FindControl("hidCategory");
                                CheckBox chkCategory = (CheckBox)areaItem.FindControl("chkCategory");
                                int id = int.TryParse(hidCategory.Value, out id) ? id : 0;
                                if (id != 0)
                                {
                                    if (pcdc.Any(x => x.Cid == id))
                                    {
                                        chkCategory.Checked = true;
                                    }
                                }


                                // 子分類
                                Repeater rptSubDealCategoryArea = (Repeater)areaItem.FindControl("rptSubDealCategoryArea");
                                foreach (RepeaterItem subDealItem in rptSubDealCategoryArea.Items)
                                {
                                    HiddenField hidSubDealCategory = (HiddenField)subDealItem.FindControl("hidSubDealCategory");
                                    CheckBox chkSubDealCategory = (CheckBox)subDealItem.FindControl("chkSubDealCategory");
                                    int subid = int.TryParse(hidSubDealCategory.Value, out subid) ? subid : 0;
                                    if (subid != 0)
                                    {
                                        if (pcdc.Any(x => x.Cid == subid))
                                        {
                                            chkSubDealCategory.Checked = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            #endregion

            txtKeyword.Text = pro.PicAlt;

            EnabledSettings(model, pro);

            if (pro.SellerGuid == Guid.Empty)
            {
                divSellerCheck.Visible = true;
                divSeller.Visible = false;
                divStore.Visible = false;
                divVbsStore.Visible = false;
                btnCreateBusinessHour.Visible = false;
                btnProposalSend.Text = "送進QC諮詢";
            }
            else
            {
                divSellerCheck.Visible = false;
                divSeller.Visible = true;
                divStore.Visible = true;
                divVbsStore.Visible = true;
                btnProposalSend.Text = "送件QC";
            }

            chkFileDone.Checked = pro.FilesDone;

            #region 合約上傳
            rptContractFiles.DataSource = pcfc;
            rptContractFiles.DataBind();
            divContract.Visible = true;

            //預設代賣家聯絡人
            txtContractPartyBContact.Text = pro.SellerBossName;
            List<Seller.MultiContracts> contactscontact = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(pro.Contacts);
            if (contactscontact != null)
            {
                foreach (var c in contactscontact)
                {
                    txtContractPartyBContact.Text += "/" + c.ContactPersonName;
                }
            }

            if (!string.IsNullOrEmpty(txtContractPartyBContact.Text) && txtContractPartyBContact.Text.Length > 50)
                txtContractPartyBContact.Text = txtContractPartyBContact.Text.Substring(0, 50);

            //合約類型
            ddlContactType.Items.Clear();
            foreach (var item in Enum.GetValues(typeof(SellerContractType)))
            {
                if ((SellerContractType)item == SellerContractType.Flat || (SellerContractType)item == SellerContractType.PairActivity || (SellerContractType)item == SellerContractType.Pair || (SellerContractType)item == SellerContractType.Draft)
                    ddlContactType.Items.Add(new ListItem(Helper.GetEnumDescription((SellerContractType)item), ((int)item).ToString()));
            }
            if (pro.ProposalCreatedType == (int)ProposalCreatedType.Seller)
            {
                divVendorAccount.Visible = true;
                litVendorAccount.Text = pro.CreateId;
            }

            #endregion

            #region 提前頁確
            chkEarlierPageCheck.Checked = pro.IsEarlierPageCheck;
            if (pro.IsEarlierPageCheck)
            {
                txtEarlierPageCheckDay.Text = pro.IsEarlierPageCheck ? pro.EarlierPageCheckDay.ToString() : "";
            }
            else
            {
                txtEarlierPageCheckDay.Text = "";
                txtEarlierPageCheckDay.Enabled = false;
            }
            #endregion

            #region 商品寄倉

            if (config.IsConsignment)
            {
                if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    chkConsignment.Visible = true;
                    chkConsignment.Checked = pro.Consignment;
                }
            }

            #endregion

            if (model != null)
            {
                if (model.Business.BusinessHourOrderTimeE != null && model.Business.BusinessHourOrderTimeE.Year != DateTime.MaxValue.Year
                    && model.Business.BusinessHourOrderTimeE.Year != 1900)
                {
                    if (model.Business.BusinessHourOrderTimeE <= DateTime.Now)
                    {
                        hidFileFlag.Value = "1";
                    }
                }
            }

        }

        public Proposal GetProposalData(Proposal pro)
        {
            pro.Id = ProposalId;
            pro.BrandName = ProposalFacade.RemoveSpecialCharacter(txtBrandName.Text.Trim());
            pro.DeliveryType = Convert.ToInt32(ddlDeliveryType.SelectedValue);
            ProposalDealType dealType = ProposalDealType.TryParse(ddlDealType.SelectedValue, out dealType) ? dealType : ProposalDealType.None;
            pro.DealType = (int)dealType;
            if (!string.IsNullOrEmpty(txtBusinessPrice.Text))
            {
                //預估營業額
                int businessPrice = 0;
                int.TryParse(txtBusinessPrice.Text, out businessPrice);
                pro.BusinessPrice = businessPrice;
            }
            ProposalDealSubType dealSubType = ProposalDealSubType.TryParse(hidDealSubType.Value, out dealSubType) ? dealSubType : ProposalDealSubType.None;
            pro.DealSubType = (int)dealSubType;
            pro.ProductSpec = ProposalFacade.RemoveSpecialCharacter(txtProductSpec.Text.Trim());
            pro.GroupCouponDealType = chkGroupCoupon.Checked ? Convert.ToInt32(ddlGroupCouponType.SelectedValue) : (int)GroupCouponDealType.None;


            //特殊標記
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkFreeTax.Checked, pro.SpecialFlag, ProposalSpecialFlag.FreeTax));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkParallelImportation.Checked, pro.SpecialFlag, ProposalSpecialFlag.ParallelImportation));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkOnlineUse.Checked, pro.SpecialFlag, ProposalSpecialFlag.OnlineUse));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkTravel.Checked, pro.SpecialFlag, ProposalSpecialFlag.Travel));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkYearContract.Checked, pro.SpecialFlag, ProposalSpecialFlag.YearContract));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkExclusive.Checked, pro.SpecialFlag, ProposalSpecialFlag.Exclusive));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkNewItem.Checked, pro.SpecialFlag, ProposalSpecialFlag.NewItem));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkBeauty.Checked, pro.SpecialFlag, ProposalSpecialFlag.Beauty));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkExpiringProduct.Checked, pro.SpecialFlag, ProposalSpecialFlag.ExpiringProduct));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkInspectRule.Checked, pro.SpecialFlag, ProposalSpecialFlag.InspectRule));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkInvoiceFounder.Checked, pro.SpecialFlag, ProposalSpecialFlag.InvoiceFounder));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkPhotographers.Checked, pro.SpecialFlag, ProposalSpecialFlag.Photographers));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkSellerPhoto.Checked, pro.SpecialFlag, ProposalSpecialFlag.SellerPhoto));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkFTPPhoto.Checked, pro.SpecialFlag, ProposalSpecialFlag.FTPPhoto));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkSelfOptional.Checked, pro.SpecialFlag, ProposalSpecialFlag.SelfOptional));//自選檔
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkEveryDayNewDeal.Checked, pro.SpecialFlag, ProposalSpecialFlag.EveryDayNewDeal));//每日一物
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(!chkDealStar.Checked, pro.SpecialFlag, ProposalSpecialFlag.DealStar));//本檔主打星

            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkRecommendFood.Checked, pro.SpecialFlag, ProposalSpecialFlag.RecommendFood));//推薦菜色/食材
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkEnvironmentIntroduction.Checked, pro.SpecialFlag, ProposalSpecialFlag.EnvironmentIntroduction));//環境介紹
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkAroundScenery.Checked, pro.SpecialFlag, ProposalSpecialFlag.AroundScenery));//周邊景點
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkSeasonActivity.Checked, pro.SpecialFlag, ProposalSpecialFlag.SeasonActivity));//季節/配合活動
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkStoreStory.Checked, pro.SpecialFlag, ProposalSpecialFlag.StoreStory));//店家故事、品牌故事
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkTravel.Checked, pro.SpecialFlag, ProposalSpecialFlag.Travel));//旅遊業登記
            pro.IsTaishinChosen = chkTaiShinChosen.Checked;

            //特殊標記QC
            pro.SpecialPrice = chkSpecialPrice.Checked;
            //系統圖庫
            pro.SystemPic = chkSystemPic.Checked;
            //銀行合作
            pro.IsBankDeal = cbxBankDeal.Checked;

            //特殊需求
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkLowGrossMarginAllowedDiscount.Checked, pro.SpecialFlag, ProposalSpecialFlag.LowGrossMarginAllowedDiscount));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkNotAllowedDiscount.Checked, pro.SpecialFlag, ProposalSpecialFlag.NotAllowedDiscount));

            //檔次特色
            if (pro.DeliveryType == (int)DeliveryType.ToShop && pro.DealType == (int)ProposalDealType.PBeauty)//玩美憑證
            {
                pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(!chkDealCharacterToShop1.Checked, pro.DealCharacterFlag, ProposalDealCharacter.PBeautyDevice));//設備/器材
                pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(!chkDealCharacterToShop2.Checked, pro.DealCharacterFlag, ProposalDealCharacter.PBeautyProduct));//商品
                pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(!chkDealCharacterToShop3.Checked, pro.DealCharacterFlag, ProposalDealCharacter.PBeautyLocation));//地點
                pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(!chkDealCharacterToShop4.Checked, pro.DealCharacterFlag, ProposalDealCharacter.PBeautyEnvironment));//環境
                pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(txtDealCharacterToShop5.Text == "" ? false : true, pro.DealCharacterFlag, ProposalDealCharacter.PBeautyOther));//其他
            }
            else if (pro.DeliveryType == (int)DeliveryType.ToShop && pro.DealType != (int)ProposalDealType.PBeauty && pro.DealType != (int)ProposalDealType.Travel)//一般憑證
            {
                pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(!chkDealCharacterToShop1.Checked, pro.DealCharacterFlag, ProposalDealCharacter.ToShopFood));//菜色
                pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(!chkDealCharacterToShop2.Checked, pro.DealCharacterFlag, ProposalDealCharacter.ToShopSpecialFood));//特殊食材
                pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(!chkDealCharacterToShop3.Checked, pro.DealCharacterFlag, ProposalDealCharacter.ToShopCook));//烹調方式
                pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(!chkDealCharacterToShop4.Checked, pro.DealCharacterFlag, ProposalDealCharacter.ToShopEnvironment));//環境
                pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(txtDealCharacterToShop5.Text == "" ? false : true, pro.DealCharacterFlag, ProposalDealCharacter.ToShopOther));//其他
            }
            else if (pro.DeliveryType == (int)DeliveryType.ToHouse || pro.DealType == (int)ProposalDealType.Travel)//宅配+旅遊
            {
                pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(txtDealCharacterToHouse1.Text == "" ? false : true, pro.DealCharacterFlag, ProposalDealCharacter.ToHouse1));//檔次特色
                pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(txtDealCharacterToHouse2.Text == "" ? false : true, pro.DealCharacterFlag, ProposalDealCharacter.ToHouse2));
                pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(txtDealCharacterToHouse3.Text == "" ? false : true, pro.DealCharacterFlag, ProposalDealCharacter.ToHouse3));
                pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(txtDealCharacterToHouse4.Text == "" ? false : true, pro.DealCharacterFlag, ProposalDealCharacter.ToHouse4));
            }

            //媒體報導
            pro.MediaReportFlag = chkMediaReport.Checked ? chkMediaReportPic.Checked ? (int)ProposalMediaReport.Pic : (int)ProposalMediaReport.Link : (int)ProposalMediaReport.None;



            //時段與消費
            pro.ConsumerTime = Convert.ToInt32(Helper.SetFlag(chkWeekdays.Checked, pro.ConsumerTime, ProposalConsumerTime.Weekdays));
            pro.ConsumerTime = Convert.ToInt32(Helper.SetFlag(chkHoliday.Checked, pro.ConsumerTime, ProposalConsumerTime.Holiday));
            pro.ConsumerTime = Convert.ToInt32(Helper.SetFlag(chkSpecialHoliday.Checked, pro.ConsumerTime, ProposalConsumerTime.SpecialHoliday));
            pro.ConsumerTime = Convert.ToInt32(Helper.SetFlag(chkBreakfast.Checked, pro.ConsumerTime, ProposalConsumerTime.Breakfast));
            pro.ConsumerTime = Convert.ToInt32(Helper.SetFlag(chkLunch.Checked, pro.ConsumerTime, ProposalConsumerTime.Lunch));
            pro.ConsumerTime = Convert.ToInt32(Helper.SetFlag(chkDinner.Checked, pro.ConsumerTime, ProposalConsumerTime.Dinner));
            pro.ConsumerTime = Convert.ToInt32(Helper.SetFlag(chkAfternoonTea.Checked, pro.ConsumerTime, ProposalConsumerTime.AfternoonTea));
            pro.ConsumerTime = Convert.ToInt32(Helper.SetFlag(chkNightSnack.Checked, pro.ConsumerTime, ProposalConsumerTime.NightSnack));
            pro.ConsumerTime = Convert.ToInt32(Helper.SetFlag(chkHolidayFare.Checked, pro.ConsumerTime, ProposalConsumerTime.HolidayFare));
            pro.ConsumerTime = Convert.ToInt32(Helper.SetFlag(chkMinimumCharge.Checked, pro.ConsumerTime, ProposalConsumerTime.MinimumCharge));
            pro.ConsumerTime = Convert.ToInt32(Helper.SetFlag(chkServiceCharge.Checked, pro.ConsumerTime, ProposalConsumerTime.ServiceCharge));

            if (Convert.ToInt32(ddlDeliveryType.SelectedValue) == (int)DeliveryType.ToShop)
            {
                //宅配不會有成套票券
                pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkGroupCoupon.Checked, pro.SpecialFlag, ProposalSpecialFlag.GroupCoupon));//成套票券

                if (chkGroupCoupon.Checked || chkGame.Checked)
                    pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SpecialFlag, ProposalSpecialFlag.NotAllowedDiscount));
            }
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(chkNoRestrictedStore.Checked, pro.SpecialFlag, ProposalSpecialFlag.NoRestrictedStore));

            if (chkNoRestrictedStore.Checked)
            {
                ProposalStoreCollection psc = pp.ProposalStoreGetListByProposalId(pro.Id);
                ProposalStoreCollection savePponStore = new ProposalStoreCollection();
                foreach (var ps in psc)
                {
                    ps.VbsRight = Convert.ToInt32(Helper.SetFlag(false, ps.VbsRight, VbsRightFlag.VerifyShop));
                    savePponStore.Add(ps);
                }
                pp.ProposalStoreSetList(savePponStore);
            }
            Dictionary<ProposalSpecialFlag, string> specialText = new Dictionary<ProposalSpecialFlag, string>();
            Dictionary<ProposalDealCharacter, string> dealcharacterText = new Dictionary<ProposalDealCharacter, string>();
            Dictionary<ProposalMediaReport, string> mediareportText = new Dictionary<ProposalMediaReport, string>();


            if (chkPhotographers.Checked)
            {
                if (ddlDeliveryType.SelectedValue == Convert.ToString((int)DeliveryType.ToShop) || (ddlDeliveryType.SelectedValue == Convert.ToString((int)DeliveryType.ToHouse) && ddlDealType.SelectedValue == Convert.ToString((int)ProposalDealType.Travel)))
                {
                    //憑證+旅遊宅配
                    List<string> photoString = new List<string>();
                    photoString.Add(txtPhotoAddress.Text.Trim());
                    photoString.Add(txtPhotoTel.Text.Trim());
                    photoString.Add(txtPhotoPhone.Text.Trim());
                    photoString.Add(txtPhotoMemo.Text.Trim());
                    photoString.Add(txtPhotoDate.Text.Trim());
                    photoString.Add(txtPhotoUrl.Text.Trim());
                    photoString.Add(txtPhotoContact.Text.Trim());
                    photoString.Add(radMale.Checked ? "M" : "F");
                    specialText.Add(ProposalSpecialFlag.Photographers, string.Join("|", photoString));
                }
                else
                {
                    //宅配
                    List<string> photoString = new List<string>();
                    photoString.Add(txtPhohoNeeds.Text.Trim());
                    photoString.Add(txtRefrenceData.Text.Trim());
                    specialText.Add(ProposalSpecialFlag.Photographers, string.Join("|", photoString));
                }

            }
            else
            {
                specialText.Remove(ProposalSpecialFlag.Photographers);
                pro.SpecialAppointFlagText = null;
                pro.PhotographerAppointFlag = 0;
            }
            if (chkFTPPhoto.Checked)
            {
                specialText.Add(ProposalSpecialFlag.FTPPhoto, txtFTPPhoto.Text.Trim());
            }
            if (chkBeauty.Checked)
            {
                specialText.Add(ProposalSpecialFlag.Beauty, ProposalFacade.RemoveSpecialCharacter(txtBeauty.Text.Trim()));
            }
            if (chkExpiringProduct.Checked)
            {
                specialText.Add(ProposalSpecialFlag.ExpiringProduct, ProposalFacade.RemoveSpecialCharacter(txtExpiringProduct.Text.Trim()));
            }
            if (chkInspectRule.Checked)
            {
                specialText.Add(ProposalSpecialFlag.InspectRule, ProposalFacade.RemoveSpecialCharacter(txtInspectRule.Text.Trim()));
            }
            if (chkInvoiceFounder.Checked)
            {
                specialText.Add(ProposalSpecialFlag.InvoiceFounder, txtInvoiceFounder.Text.Trim());
            }
            if (!chkDealStar.Checked)
            {
                specialText.Add(ProposalSpecialFlag.DealStar, txtDealStar.Text.Trim());
            }

            if (chkRecommendFood.Checked)
            {
                specialText.Add(ProposalSpecialFlag.RecommendFood, txtRecommendFood.Text.Trim());
            }
            if (chkEnvironmentIntroduction.Checked)
            {
                specialText.Add(ProposalSpecialFlag.EnvironmentIntroduction, txtEnvironmentIntroduction.Text.Trim());
            }
            if (chkAroundScenery.Checked)
            {
                specialText.Add(ProposalSpecialFlag.AroundScenery, txtAroundScenery.Text.Trim());
            }
            if (chkSeasonActivity.Checked)
            {
                specialText.Add(ProposalSpecialFlag.SeasonActivity, txtSeasonActivity.Text.Trim());
            }
            if (chkStoreStory.Checked)
            {
                specialText.Add(ProposalSpecialFlag.StoreStory, txtStoreStory.Text.Trim());
            }
            if (chkTravel.Checked)
            {
                specialText.Add(ProposalSpecialFlag.Travel, txtTravel.Text);
            }
            if (pro.DeliveryType == (int)DeliveryType.ToShop && pro.DealType == (int)ProposalDealType.PBeauty)//玩美憑證
            {
                if (!chkDealCharacterToShop1.Checked)
                {
                    dealcharacterText.Add(ProposalDealCharacter.PBeautyDevice, txtDealCharacterToShop1.Text.Trim());
                }
                if (!chkDealCharacterToShop2.Checked)
                {
                    dealcharacterText.Add(ProposalDealCharacter.PBeautyProduct, txtDealCharacterToShop2.Text.Trim());
                }
                if (!chkDealCharacterToShop3.Checked)
                {
                    dealcharacterText.Add(ProposalDealCharacter.PBeautyLocation, txtDealCharacterToShop3.Text.Trim());
                }
                if (!chkDealCharacterToShop4.Checked)
                {
                    dealcharacterText.Add(ProposalDealCharacter.PBeautyEnvironment, txtDealCharacterToShop4.Text.Trim());
                }
                if (txtDealCharacterToShop5.Text != "")
                {
                    dealcharacterText.Add(ProposalDealCharacter.PBeautyOther, txtDealCharacterToShop5.Text.Trim());
                }
            }
            else if (pro.DeliveryType == (int)DeliveryType.ToShop && pro.DealType != (int)ProposalDealType.PBeauty && pro.DealType != (int)ProposalDealType.Travel)//一般憑證
            {
                if (!chkDealCharacterToShop1.Checked)
                {
                    dealcharacterText.Add(ProposalDealCharacter.ToShopFood, txtDealCharacterToShop1.Text.Trim());
                }
                if (!chkDealCharacterToShop2.Checked)
                {
                    dealcharacterText.Add(ProposalDealCharacter.ToShopSpecialFood, txtDealCharacterToShop2.Text.Trim());
                }
                if (!chkDealCharacterToShop3.Checked)
                {
                    dealcharacterText.Add(ProposalDealCharacter.ToShopCook, txtDealCharacterToShop3.Text.Trim());
                }
                if (!chkDealCharacterToShop4.Checked)
                {
                    dealcharacterText.Add(ProposalDealCharacter.ToShopEnvironment, txtDealCharacterToShop4.Text.Trim());
                }
                if (txtDealCharacterToShop5.Text != "")
                {
                    dealcharacterText.Add(ProposalDealCharacter.ToShopOther, txtDealCharacterToShop5.Text.Trim());
                }
            }
            else if (pro.DeliveryType == (int)DeliveryType.ToHouse || pro.DealType == (int)ProposalDealType.Travel)//宅配+旅遊
            {
                if (!chkDealCharacterToHouse.Checked)
                {
                    dealcharacterText.Add(ProposalDealCharacter.ToHouse1, txtDealCharacterToHouse1.Text.Trim());
                    dealcharacterText.Add(ProposalDealCharacter.ToHouse2, txtDealCharacterToHouse2.Text.Trim());
                    dealcharacterText.Add(ProposalDealCharacter.ToHouse3, txtDealCharacterToHouse3.Text.Trim());
                    dealcharacterText.Add(ProposalDealCharacter.ToHouse4, txtDealCharacterToHouse4.Text.Trim());
                }
            }


            ProposalMediaReportLink MediaReportLink = null;
            List<ProposalMediaReportLink> ProposalMediaReportLinkList = new List<ProposalMediaReportLink>();
            if (chkMediaReport.Checked)
            {
                if (!chkMediaReportPic.Checked)
                {
                    for (int i = 1; i <= 5; i++)
                    {
                        MediaReportLink = new ProposalMediaReportLink();
                        switch (i)
                        {
                            case 1:
                                if (txtMediaReport1.Text != "")
                                {
                                    MediaReportLink.Type = rdbMediaReportIn1.Checked ? (int)ProposalMediaReport.LinkIn : (int)ProposalMediaReport.LinkOut;
                                    MediaReportLink.Link = txtMediaReport1.Text;
                                    ProposalMediaReportLinkList.Add(MediaReportLink);
                                }
                                break;
                            case 2:
                                if (txtMediaReport2.Text != "")
                                {
                                    MediaReportLink.Type = rdbMediaReportIn2.Checked ? (int)ProposalMediaReport.LinkIn : (int)ProposalMediaReport.LinkOut;
                                    MediaReportLink.Link = txtMediaReport2.Text;
                                    ProposalMediaReportLinkList.Add(MediaReportLink);
                                }
                                break;
                            case 3:
                                if (txtMediaReport3.Text != "")
                                {
                                    MediaReportLink.Type = rdbMediaReportIn3.Checked ? (int)ProposalMediaReport.LinkIn : (int)ProposalMediaReport.LinkOut;
                                    MediaReportLink.Link = txtMediaReport3.Text;
                                    ProposalMediaReportLinkList.Add(MediaReportLink);
                                }
                                break;
                            case 4:
                                if (txtMediaReport4.Text != "")
                                {
                                    MediaReportLink.Type = rdbMediaReportIn4.Checked ? (int)ProposalMediaReport.LinkIn : (int)ProposalMediaReport.LinkOut;
                                    MediaReportLink.Link = txtMediaReport4.Text;
                                    ProposalMediaReportLinkList.Add(MediaReportLink);
                                }
                                break;
                            case 5:
                                if (txtMediaReport5.Text != "")
                                {
                                    MediaReportLink.Type = rdbMediaReportIn5.Checked ? (int)ProposalMediaReport.LinkIn : (int)ProposalMediaReport.LinkOut;
                                    MediaReportLink.Link = txtMediaReport5.Text;
                                    ProposalMediaReportLinkList.Add(MediaReportLink);
                                }
                                break;

                        }
                    }
                }

            }
            pro.MediaReportFlagText = new JsonSerializer().Serialize(ProposalMediaReportLinkList);
            pro.DealCharacterFlagText = new JsonSerializer().Serialize(dealcharacterText);
            pro.SpecialFlagText = new JsonSerializer().Serialize(specialText);
            pro.MarketAnalysis = txtMarketAnalysis.Text.Trim();
            pro.MarketingResource = txtMarketingResource.Text.Trim();
            pro.ScheduleExpect = txtScheduleExpect.Text.Trim();
            ProposalStartUseUnitType startUnit = ProposalStartUseUnitType.TryParse(ddlStartUseUnit.SelectedValue, out startUnit) ? startUnit : ProposalStartUseUnitType.Day;
            pro.StartUseUnit = (int)startUnit;
            pro.StartUseText = txtStartUse.Text;
            pro.StartUseText2 = txtStartUse2.Text;
            DealShipType shipType = DealShipType.TryParse(ddlShipType.SelectedValue, out shipType) ? shipType : DealShipType.Normal;
            pro.ShipType = (int)shipType;
            if (shipType == DealShipType.Normal)
            {
                pro.ShippingdateType = (int)OrderShippingDateType;
                //一般出貨
                if (OrderShippingDateType == DealShippingDateType.Normal)
                {
                    //訂單成立後X個工作日內出貨完畢
                    pro.ShipText3 = txtShipText3.Text.Trim();
                    pro.ShipText1 = "";
                    pro.ShipText2 = "";
                    pro.ShipOther = "";
                }
                else if (OrderShippingDateType == DealShippingDateType.Special)
                {
                    //最早出貨日
                    pro.ShipText1 = txtShipText1.Text.Trim();
                    //最晚出貨日
                    pro.ShipText2 = txtShipText2.Text.Trim();
                    pro.ShipText3 = "";
                    pro.ShipOther = "";
                }
            }
            else if (shipType == DealShipType.Other)
            {
                pro.ShipOther = txtShipTypeOther.Text.Trim();
            }

            pro.ContractMemo = ProposalFacade.RemoveSpecialCharacter(txtContractMemo.Text.Trim(), true);
            pro.Memo = txtMemo.Text.Trim();

            //接續份數
            Guid _AncestorBusinessHourGuid = Guid.Empty;
            if (txtAncestorBid.Text.Trim() != "")
            {
                Guid.TryParse(txtAncestorBid.Text.Trim(), out _AncestorBusinessHourGuid);
                pro.AncestorBusinessHourGuid = _AncestorBusinessHourGuid;
            }
            else
            {
                pro.AncestorBusinessHourGuid = null;
            }

            Guid _AncestorSequenceBusinessHourGuid = Guid.Empty;
            if (txtAncestorSeqBid.Text.Trim() != "")
            {
                Guid.TryParse(txtAncestorSeqBid.Text.Trim(), out _AncestorSequenceBusinessHourGuid);
                pro.AncestorSequenceBusinessHourGuid = _AncestorSequenceBusinessHourGuid;
            }
            else
            {
                pro.AncestorSequenceBusinessHourGuid = null;
            }

            //巿場分析比價
            if (pro.DealType == (int)ProposalDealType.Product)
            {
                var SaleMarketAnalysis = new ProposalMarketAnalysis
                {
                    GomajiPrice = txtGomajiPrice.Text.Trim(),
                    GomajiLink = txtGomajiLink.Text.Trim(),
                    KuobrothersPrice = txtKuobrothersPrice.Text.Trim(),
                    KuobrothersLink = txtKuobrothersLink.Text.Trim(),
                    CrazymikePrice = txtCrazymikePrice.Text.Trim(),
                    CrazymikeLink = txtCrazymikeLink.Text.Trim(),
                    EzPricePrice1 = txtEzPricePrice1.Text.Trim(),
                    EzPriceLink1 = txtEzPriceLink1.Text.Trim(),
                    EzPricePrice2 = txtEzPricePrice2.Text.Trim(),
                    EzPriceLink2 = txtEzPriceLink2.Text.Trim(),
                    EzPricePrice3 = txtEzPricePrice3.Text.Trim(),
                    EzPriceLink3 = txtEzPriceLink3.Text.Trim(),
                    PinglePrice1 = txtPinglePrice1.Text.Trim(),
                    PingleLink1 = txtPingleLink1.Text.Trim(),
                    PinglePrice2 = txtPinglePrice2.Text.Trim(),
                    PingleLink2 = txtPingleLink2.Text.Trim(),
                    PinglePrice3 = txtPinglePrice3.Text.Trim(),
                    PingleLink3 = txtPingleLink3.Text.Trim(),
                    SimilarPrice = txtSimilarPrice.Text.Trim(),
                    SimilarLink = txtSimilarLink.Text.Trim(),
                    OtherShop1 = ddlOtherShop1.SelectedValue,
                    OtherPrice1 = txtOtherPrice1.Text.Trim(),
                    OtherLink1 = txtOtherLink1.Text.Trim(),
                    OtherShop2 = ddlOtherShop2.SelectedValue,
                    OtherPrice2 = txtOtherPrice2.Text.Trim(),
                    OtherLink2 = txtOtherLink2.Text.Trim(),
                    OtherShop3 = ddlOtherShop3.SelectedValue,
                    OtherPrice3 = txtOtherPrice3.Text.Trim(),
                    OtherLink3 = txtOtherLink3.Text.Trim()
                };

                pro.SaleMarketAnalysis = new JsonSerializer().Serialize(SaleMarketAnalysis);
            }
            else
                pro.SaleMarketAnalysis = string.Empty;

            pro.PicAlt = txtKeyword.Text.Trim();

            //發票來源及內容
            if (ReceiptType != 99)
                pro.VendorReceiptType = ReceiptType;
            pro.Othermessage = txtAccountingMessage.Text;

            //配送方式
            int? deliverymethod = null;
            pro.DeliveryMethod = ddlDeliveryMethod.SelectedValue != "" ? Convert.ToInt32(ddlDeliveryMethod.SelectedValue) : deliverymethod;

            //鑑賞期
            pro.TrialPeriod = chkTrialPeriod.Checked;

            //墨攻        
            pro.Mohist = chkMohist.Checked;

            //提前頁確
            pro.IsEarlierPageCheck = chkEarlierPageCheck.Checked;
            if (chkEarlierPageCheck.Checked)
            {
                int EarlierPageCheckDay = 0;
                int.TryParse(txtEarlierPageCheckDay.Text, out EarlierPageCheckDay);
                pro.EarlierPageCheckDay = EarlierPageCheckDay;
            }

            //商品寄倉
            if (config.IsConsignment)
            {
                pro.Consignment = chkConsignment.Checked;
            }

            return pro;
        }

        public ProposalStoreCollection GetPponStores()
        {
            ProposalStoreCollection rtn = new ProposalStoreCollection();
            foreach (RepeaterItem store in rptPponStore.Items)
            {
                CheckBox chkCheck = (CheckBox)store.FindControl("chkCheck");
                if (chkCheck.Checked && chkCheck.Enabled)
                {
                    HiddenField hidStoreGuid = (HiddenField)store.FindControl("hidStoreGuid");
                    Guid stid = Guid.TryParse(hidStoreGuid.Value, out stid) ? stid : Guid.Empty;
                    if (stid != Guid.Empty)
                    {
                        rtn.Add(new ProposalStore()
                        {
                            ProposalId = ProposalId,
                            StoreGuid = stid,
                            VbsRight = (int)VbsRightFlag.Location,
                            ResourceGuid = Guid.NewGuid(),
                            CreateId = UserName,
                            CreateTime = DateTime.Now
                        });
                    }
                }
            }
            return rtn;
        }

        public ProposalStoreCollection GetVbs()
        {
            ProposalStoreCollection rtn = new ProposalStoreCollection();
            foreach (RepeaterItem store in rptVbs.Items)
            {
                TextBox txtQuantity = (TextBox)store.FindControl("txtQuantity");
                CheckBox chkVerifyShop = (CheckBox)store.FindControl("chkVerifyShop");
                CheckBox chkVerify = (CheckBox)store.FindControl("chkVerify");
                CheckBox chkAccouting = (CheckBox)store.FindControl("chkAccouting");
                CheckBox chkViewBalanceSheet = (CheckBox)store.FindControl("chkViewBalanceSheet");
                Image btnHideBalanceSheet = (Image)store.FindControl("btnHideBalanceSheet");
                bool isHideBalanceSheet = bool.Parse(((HiddenField)store.FindControl("hidIsHideBalanceSheet")).Value ?? false.ToString());

                if ((chkVerifyShop.Checked && chkVerifyShop.Enabled) || (chkVerify.Checked && chkVerify.Enabled) || (chkAccouting.Checked && chkAccouting.Enabled) || (chkViewBalanceSheet.Checked && chkViewBalanceSheet.Enabled))
                {
                    int VerifyShop = 0, Verify = 0, Accouting = 0, BalanceSheet = 0, BalanceSheetHideFromDealSeller = 0;
                    if (chkVerifyShop.Checked)
                        VerifyShop = (int)VbsRightFlag.VerifyShop;
                    if (chkVerify.Checked)
                        Verify = (int)VbsRightFlag.Verify;
                    if (chkAccouting.Checked)
                        Accouting = (int)VbsRightFlag.Accouting;
                    if (chkViewBalanceSheet.Checked)
                        BalanceSheet = (int)VbsRightFlag.ViewBalanceSheet;
                    if (chkViewBalanceSheet.Checked && isHideBalanceSheet)
                        BalanceSheetHideFromDealSeller = (int)VbsRightFlag.BalanceSheetHideFromDealSeller;

                    HiddenField hidStoreGuid = (HiddenField)store.FindControl("hidStoreGuid");
                    Guid stid = Guid.TryParse(hidStoreGuid.Value, out stid) ? stid : Guid.Empty;
                    if (stid != Guid.Empty)
                    {

                        int? qty;
                        if (!string.IsNullOrWhiteSpace(txtQuantity.Text.Trim()))
                        {
                            qty = int.Parse(txtQuantity.Text.Trim());
                        }
                        else
                        {
                            qty = null;
                        }

                        rtn.Add(new ProposalStore()
                        {
                            ProposalId = ProposalId,
                            StoreGuid = stid,
                            TotalQuantity = qty,
                            VbsRight = VerifyShop + Verify + Accouting + BalanceSheet + BalanceSheetHideFromDealSeller,
                            ResourceGuid = Guid.NewGuid(),
                            CreateId = UserName,
                            CreateTime = DateTime.Now

                        });
                    }
                }
            }
            return rtn;
        }

        public void RedirectProposal(int id, string anchor = "")
        {
            Response.Redirect(ResolveUrl("~/sal/ProposalContent.aspx?pid=" + id + anchor));
        }

        public void RedirectHouseProposal(int id)
        {
            Response.Redirect(ResolveUrl("~/sal/proposal/house/proposalcontent?pid=" + id));
        }

        public void RedirectSellerList()
        {
            Response.Redirect(ResolveUrl("~/sal/SellerList.aspx"));
        }

        public void RedirectBusinessHour(Guid bid)
        {
            Response.Redirect(ResolveUrl("~/sal/BusinessContent.aspx?bid=" + bid));
        }

        public void ShowMessage(string msg, ProposalContentMode mode, string parms = "")
        {
            string script = string.Empty;
            switch (mode)
            {
                case ProposalContentMode.BackToList:
                case ProposalContentMode.ProposalNotFound:
                    script = string.Format("alert('{0}'); location.href='{1}/sal/ProposalList.aspx';", msg, config.SiteUrl);
                    break;
                case ProposalContentMode.DataError:
                    script = string.Format("alert('{0}');", msg);
                    break;
                case ProposalContentMode.GoToProposal:
                    script += string.Format("location.href='{0}/sal/ProposalContent.aspx?pid={1}';", config.SiteUrl, parms);
                    break;
                case ProposalContentMode.AlertGoToProposal:
                    script = string.Format("alert('{0}'); location.href='{1}/sal/ProposalContent.aspx?pid={2}';", msg, config.SiteUrl, parms);
                    break;
                case ProposalContentMode.GotoHouseProposal:
                    script = string.Format("location.href='{0}/sal/proposal/house/proposalcontent?pid={1}';", config.SiteUrl, parms);
                    break;
                default:
                    break;
            }

            if (!string.IsNullOrWhiteSpace(script))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "alert", script, true);
            }
        }

        public ProposalCategoryDealCollection GetCategoryDeals()
        {
            List<int> categories = new List<int>();
            foreach (RepeaterItem channel in rptChannel.Items)
            {
                // 頻道
                int channelId = 0;
                CheckBox chkChannel = (CheckBox)channel.FindControl("chkChannel");
                if (chkChannel.Checked)
                {
                    HiddenField hidChannel = (HiddenField)channel.FindControl("hidChannel");
                    if (int.TryParse(hidChannel.Value, out channelId) && channelId != 0)
                    {
                        categories.Add(channelId);

                        // 區域
                        Repeater rptArea = (Repeater)channel.FindControl("rptArea");
                        foreach (RepeaterItem area in rptArea.Items)
                        {
                            CheckBox chkArea = (CheckBox)area.FindControl("chkArea");
                            if (chkArea.Checked)
                            {
                                HiddenField hidArea = (HiddenField)area.FindControl("hidArea");
                                int id = int.TryParse(hidArea.Value, out id) ? id : 0;
                                if (id != 0)
                                {
                                    categories.Add(id);
                                }
                            }
                        }

                        // 商圈
                        Repeater rptArea2 = (Repeater)channel.FindControl("rptArea2");
                        foreach (RepeaterItem areaItem in rptArea2.Items)
                        {
                            Repeater rptCommercial = (Repeater)areaItem.FindControl("rptCommercial");
                            foreach (RepeaterItem commercial in rptCommercial.Items)
                            {
                                CheckBox chkCommercial = (CheckBox)commercial.FindControl("chkCommercial");
                                if (chkCommercial.Checked)
                                {
                                    HiddenField hidCommercial = (HiddenField)commercial.FindControl("hidCommercial");
                                    int cid = int.TryParse(hidCommercial.Value, out cid) ? cid : 0;
                                    if (cid != 0)
                                    {
                                        categories.Add(cid);
                                    }
                                }
                            }

                            //旅遊-商圈‧景點
                            Repeater rptSPRegion = (Repeater)areaItem.FindControl("rptSPRegion");
                            foreach (RepeaterItem spregion in rptSPRegion.Items)
                            {
                                Repeater rptSpecialRegion = (Repeater)spregion.FindControl("rptSpecialRegion");
                                foreach (RepeaterItem specialregion in rptSpecialRegion.Items)
                                {
                                    CheckBox chkSpecialRegion = (CheckBox)specialregion.FindControl("chkSpecialRegion");
                                    if (chkSpecialRegion.Checked)
                                    {
                                        HiddenField hidSpecialRegion = (HiddenField)specialregion.FindControl("hidSpecialRegion");
                                        int cid = int.TryParse(hidSpecialRegion.Value, out cid) ? cid : 0;
                                        if (cid != 0)
                                        {
                                            categories.Add(cid);
                                        }
                                    }
                                }

                            }
                        }


                        // 分類
                        Repeater rptCategory = (Repeater)channel.FindControl("rptCategory");
                        foreach (RepeaterItem commercial in rptCategory.Items)
                        {
                            CheckBox chkCategory = (CheckBox)commercial.FindControl("chkCategory");
                            if (chkCategory.Checked)
                            {
                                HiddenField hidCategory = (HiddenField)commercial.FindControl("hidCategory");
                                int id = int.TryParse(hidCategory.Value, out id) ? id : 0;
                                if (id != 0)
                                {
                                    categories.Add(id);
                                }

                                // 子分類
                                Repeater rptSubDealCategoryArea = (Repeater)commercial.FindControl("rptSubDealCategoryArea");
                                foreach (RepeaterItem subDeal in rptSubDealCategoryArea.Items)
                                {
                                    CheckBox chkSubDealCategory = (CheckBox)subDeal.FindControl("chkSubDealCategory");
                                    if (chkSubDealCategory.Checked)
                                    {
                                        HiddenField hidSubDealCategory = (HiddenField)subDeal.FindControl("hidSubDealCategory");
                                        int subid = int.TryParse(hidSubDealCategory.Value, out subid) ? subid : 0;
                                        if (subid != 0)
                                        {
                                            categories.Add(subid);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            ProposalCategoryDealCollection rtn = new ProposalCategoryDealCollection();
            foreach (int cid in categories)
            {
                rtn.Add(new ProposalCategoryDeal()
                {
                    Pid = ProposalId,
                    Cid = cid
                });
            }

            return rtn;
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Proposal pro = ProposalFacade.ProposalGet(ProposalId);
                if(pro.ProposalSourceType == (int)ProposalSourceType.House)
                {
                    RedirectHouseProposal(pro.Id);
                }
                IntialControls();
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnOrderTimeSave_Click(object sender, EventArgs e)
        {
            IsOrderTimeSave = true;
            if (this.OrderTimeSave != null)
            {
                DateTime date = DateTime.TryParse(txtBusinessHourOrderTimeS.Text, out date) ? date : DateTime.MaxValue;
                if (date != DateTime.MaxValue)
                {
                    this.OrderTimeSave(this, new DataEventArgs<DateTime>(date));
                }
            }
        }
        protected void btnChangeSeller_Click(object sender, EventArgs e)
        {
            if (this.ChangeSeller != null)
            {
                this.ChangeSeller(this, new DataEventArgs<string>(txtChangeSeller.Text));
            }
        }
        protected void btnCancelOrderTimeSave_Click(object sender, EventArgs e)
        {
            IsOrderTimeSave = false;
            if (this.OrderTimeSave != null)
            {
                DateTime date = DateTime.TryParse(txtBusinessHourOrderTimeS.Text, out date) ? date : DateTime.MaxValue;
                if (date != DateTime.MaxValue)
                {
                    this.OrderTimeSave(this, new DataEventArgs<DateTime>(date));
                }
            }
        }

        protected void btnCloneProposal_Click(object sender, EventArgs e)
        {
            //複製提案單(尚未有檔次)
            Proposal pro = ProposalFacade.ProposalGet(ProposalId);
            Seller s = sp.SellerGet(pro.SellerGuid);

            if (s.StoreStatus == (int)StoreStatus.Cancel)
            {
                ShowMessage("隱藏之店家無法複製提案單", ProposalContentMode.DataError);
            }
            else if (config.IsRemittanceFortnightly && !SellerFacade.IsAgreeNewContractSeller(s.Guid, (int)DeliveryType.ToHouse) && (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType == (int)ProposalDealType.Product))
            {
                ShowMessage("商家尚未同意新的條款內容，無法複製提案。", ProposalContentMode.DataError);
            }
            else
            {
                Guid bid = Guid.Empty;
                Guid.TryParse(pro.BusinessHourGuid.ToString(), out bid);
                if (this.CloneProposal != null)
                {
                    this.CloneProposal(this, new DataEventArgs<KeyValuePair<int, ProposalCopyType>>(new KeyValuePair<int, ProposalCopyType>(ProposalId, ProposalCopyType.Similar)));
                }
            }
        }

        protected void btnReCreateProposal_Click(object sender, EventArgs e)
        {
            //複製提案單(有檔次)
            Proposal pro = ProposalFacade.ProposalGet(ProposalId);
            Seller s = sp.SellerGet(pro.SellerGuid);
            if (s.StoreStatus == (int)StoreStatus.Cancel)
            {
                ShowMessage("隱藏之店家無法複製提案單", ProposalContentMode.DataError);
            }
            else if (config.IsRemittanceFortnightly && !SellerFacade.IsAgreeNewContractSeller(s.Guid, (int)DeliveryType.ToHouse) && (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType == (int)ProposalDealType.Product))
            {
                ShowMessage("商家尚未同意新的條款內容，無法複製提案。", ProposalContentMode.DataError);
            }
            else
            {
                Guid bid = Guid.Empty;
                Guid.TryParse(pro.BusinessHourGuid.ToString(), out bid);
                if (bid != Guid.Empty)
                {
                    if (this.ReCreateProposal != null)
                    {
                        this.ReCreateProposal(this, new DataEventArgs<KeyValuePair<Guid, ProposalCopyType>>(new KeyValuePair<Guid, ProposalCopyType>(bid, ProposalCopyType.Similar)));
                    }
                }
            }
        }

        //protected void btnReCreateSameProposal_Click(object sender, EventArgs e)
        //{
        //    Proposal pro = ProposalFacade.ProposalGet(ProposalId);
        //    Guid bid = Guid.Empty;
        //    Guid.TryParse(pro.BusinessHourGuid.ToString(), out bid);
        //    if (bid != Guid.Empty)
        //    {
        //        if (this.ReCreateProposal != null)
        //        {
        //            this.ReCreateProposal(this, new DataEventArgs<KeyValuePair<Guid, ProposalCopyType>>(new KeyValuePair<Guid, ProposalCopyType>(bid, ProposalCopyType.Same)));
        //        }
        //    }
        //}

        protected void btnUrgent_Click(object sender, EventArgs e)
        {
            IsUrgent = true;
            if (this.Urgent != null)
            {
                this.Urgent(this, e);
            }
        }

        protected void btnCancelUrgent_Click(object sender, EventArgs e)
        {
            IsUrgent = false;
            if (this.Urgent != null)
            {
                this.Urgent(this, e);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Save != null)
            {
                this.Save(this, e);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.Delete != null)
            {
                this.Delete(this, e);
            }
        }

        protected void btnCreateBusinessHour_Click(object sender, EventArgs e)
        {
            if (this.CreateBusinessHour != null)
            {
                this.CreateBusinessHour(this, e);
            }
        }

        protected void btnDevelopeReferral_Click(object sender, EventArgs e)
        {
            if (this.Referral != null)
            {
                this.Referral(this, new DataEventArgs<KeyValuePair<string, SellerSalesType>>(new KeyValuePair<string, SellerSalesType>(txtDevelopeReferralEmail.Text, SellerSalesType.Develope)));
            }
        }

        protected void btnOperationReferral_Click(object sender, EventArgs e)
        {
            if (this.Referral != null)
            {
                this.Referral(this, new DataEventArgs<KeyValuePair<string, SellerSalesType>>(new KeyValuePair<string, SellerSalesType>(txtOperationReferralEmail.Text, SellerSalesType.Operation)));
            }
        }

        protected void btnProposalSend_Click(object sender, EventArgs e)
        {
            if (this.ProposalSend != null)
            {
                this.ProposalSend(this, e);
                ShowMessage("", ProposalContentMode.GoToProposal, ProposalId.ToString());
            }
        }
        protected void btnSellerProposalSaleEditor_Click(object sender, EventArgs e)
        {
            if (this.SellerProposalSaleEditor != null)
            {
                this.SellerProposalSaleEditor(this, new DataEventArgs<int>(ProposalId));
            }
        }
        protected void btnSellerProposalReturned_Click(object sender, EventArgs e)
        {
            if (this.SellerProposalReturned != null)
            {
                this.SellerProposalReturned(this, new DataEventArgs<string>(txtSellerReturnedLog.Text));
            }
        }
        protected void btnSellerProposalCheckWaitting_Click(object sender, EventArgs e)
        {
            if (this.SellerProposalCheckWaitting != null)
            {
                this.SellerProposalCheckWaitting(this, new DataEventArgs<int>(ProposalId));
            }
        }

        protected void btnProposalReturned_Click(object sender, EventArgs e)
        {
            if (this.ProposalReturned != null)
            {
                this.ProposalReturned(this, new DataEventArgs<string>(txtReturnedLog.Text));
                txtReturnedLog.Text = string.Empty;
                chkCommissionInCompatible.Checked = false;
                chkFoldInCompatible.Checked = false;
                chkCopiesInCompatible.Checked = false;
                chkBusinessPriceLower.Checked = false;
                chkNeedOptimization.Checked = false;
                chkPoorThanBusinessStrife.Checked = false;
                chkDataModifyContractInComplete.Checked = false;
                chkCouponEventContentModify.Checked = false;
                chkOptimizationPrice.Checked = false;
                chkInformationError.Checked = false;
                chkPercentLower.Checked = false;
                chkExistProposal.Checked = false;
                chkSaleOverProposal.Checked = false;
                chkDataModify.Checked = false;
                chkContractError.Checked = false;
                chkCancel.Checked = false;
                chkCheckError.Checked = false;
                chkOther.Checked = false;
                hidReturnDetailDataModifyValue.Value = "[]";
                hidReturnDetailDataModifyTxt.Value = "";
            }
        }
        protected void btnPponStoreSave_Click(object sender, EventArgs e)
        {
            if (this.PponStoreSave != null)
            {
                this.PponStoreSave(this, e);
            }

            if (this.VbsSave != null)
            {
                this.VbsSave(this, new DataEventArgs<int>(1));
            }
        }

        protected void btnVbsSave_Click(object sender, EventArgs e)
        {
            if (this.VbsSave != null)
            {
                this.VbsSave(this, new DataEventArgs<int>(0));
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            if (this.Download != null)
            {
                Guid stid = Guid.Empty;
                Guid.TryParse(ddlStoreList.SelectedValue, out stid);
                this.Download(this, new DataEventArgs<Guid>(stid));
            }
        }

        protected void rptSellerFile_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ProposalSellerFile)
            {
                ProposalSellerFile dataItem = (ProposalSellerFile)e.Item.DataItem;
                Literal liFileSize = (Literal)e.Item.FindControl("liFileSize");
                string size = "0";
                if (dataItem.FileSize < 1024)
                {
                    size = dataItem.FileSize.ToString();
                }
                else if ((dataItem.FileSize / 1024) > 0)
                {
                    size = Convert.ToInt32(Math.Round(Convert.ToDecimal(dataItem.FileSize / 1024), 0)).ToString() + " KB";
                }
                else if ((dataItem.FileSize / 1024 / 1024) > 0)
                {
                    size = Convert.ToInt32(Math.Round(Convert.ToDecimal(dataItem.FileSize / 1024 / 1024), 0)).ToString() + " MB";
                }
                liFileSize.Text = size;
            }
        }

        protected void rptStatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<ProposalStatus, Dictionary<Enum, bool>>)
            {
                KeyValuePair<ProposalStatus, Dictionary<Enum, bool>> dataItem = (KeyValuePair<ProposalStatus, Dictionary<Enum, bool>>)e.Item.DataItem;

                Image imgStatus = (Image)e.Item.FindControl("imgStatus");
                Literal liStatus = (Literal)e.Item.FindControl("liStatus");
                Repeater rptFlag = (Repeater)e.Item.FindControl("rptFlag");

                if (dataItem.Key <= Status)
                {
                    imgStatus.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/skm/skm-arrow-R.png");
                }
                else
                {
                    imgStatus.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/silder_arrows.png");
                }
                liStatus.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, dataItem.Key);
                rptFlag.DataSource = dataItem.Value.ToDictionary(x => x, y => dataItem.Key);
                rptFlag.DataBind();
            }
        }

        protected void rptMultiDeal_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (rptMultiDeal.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    Label lblFooter = (Label)e.Item.FindControl("lblEmptyData");
                    lblFooter.Visible = true;
                }
            }
        }

        protected void rptFlag_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<KeyValuePair<Enum, bool>, ProposalStatus>)
            {
                KeyValuePair<KeyValuePair<Enum, bool>, ProposalStatus> dataItem = (KeyValuePair<KeyValuePair<Enum, bool>, ProposalStatus>)e.Item.DataItem;
                ImageButton btnFlag = (ImageButton)e.Item.FindControl("btnFlag");
                ImageButton btnFlagPass = (ImageButton)e.Item.FindControl("btnFlagPass");
                Literal liFlag = (Literal)e.Item.FindControl("liFlag");
                KeyValuePair<Type, string> flagType = SellerFacade.GetProposalFlagsTypeByStatus(dataItem.Value);
                if (flagType.Key != null)
                {
                    bool enabled = false;
                    if (flagType.Key == typeof(ProposalApplyFlag))
                    {
                        enabled = false;
                    }
                    else
                    {
                        // 檢核狀態權限檢查
                        SystemFunctionType funcType = SystemFunctionType.All;
                        if (SystemFunctionType.TryParse(dataItem.Key.Key.ToString(), out funcType) && funcType != SystemFunctionType.All)
                        {
                            if (funcType == SystemFunctionType.Created || funcType == SystemFunctionType.ScheduleCheck || funcType == SystemFunctionType.SettingCheck || funcType == SystemFunctionType.FinanceCheck)
                            {
                                enabled = false; // 建檔確認、排檔編輯、送出排檔、財務資料，需透過系統執行或操作流程變更，非手動更新
                            }
                            else if (funcType == SystemFunctionType.PaperContractCheck &&
                                    (ddlDeliveryType.SelectedValue == Convert.ToString((int)DeliveryType.ToHouse) && ddlDealType.SelectedValue != Convert.ToString((int)ProposalDealType.Travel)))
                            {
                                enabled = false;//附約已回在一般宅配自動勾選，其餘都手動勾選
                            }
                            else if (funcType == SystemFunctionType.Setting || funcType == SystemFunctionType.CopyWriter || funcType == SystemFunctionType.ImageDesign ||
                                     funcType == SystemFunctionType.ART || funcType == SystemFunctionType.Listing)
                            {
                                //編輯檢核要有被指派且是自己
                                ViewEmployee emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, UserName);

                                ProposalEditorFlag flag = (ProposalEditorFlag)Enum.Parse(typeof(ProposalEditorFlag), funcType.ToString());
                                ProposalAssignLog palc = ProposalFacade.ProposalAssignLog(ProposalId).Where(x => x.AssignFlag == (int)flag && x.AssignEmail == emp.EmpName).FirstOrDefault();
                                if (palc != null)
                                    enabled = true;
                            }
                            else
                            {
                                //同意合約未回先上檔需再送出排檔前或是合約已回未勾選，合約檢核需在送出排檔後，合約檢核一定要有勾選合約已回
                                if (funcType == SystemFunctionType.PaperContractNotCheck && (Helper.IsFlagSet(Convert.ToInt32(hidCreatedFlag.Value), ProposalBusinessCreateFlag.SettingCheck) || Helper.IsFlagSet(Convert.ToInt32(hidListingFlag.Value), ProposalListingFlag.PaperContractCheck)))
                                    enabled = false;
                                else if (funcType == SystemFunctionType.PaperContractInspect && !Helper.IsFlagSet(Convert.ToInt32(hidCreatedFlag.Value), ProposalBusinessCreateFlag.SettingCheck))
                                    enabled = false;
                                else if (funcType == SystemFunctionType.PaperContractInspect && !Helper.IsFlagSet(Convert.ToInt32(hidListingFlag.Value), ProposalListingFlag.PaperContractCheck))
                                    enabled = false;
                                else
                                    enabled = CommonFacade.IsInSystemFunctionPrivilege(UserName, funcType);
                            }
                        }
                    }
                    btnFlag.OnClientClick = enabled ? string.Empty : "alert('您無此權限。'); return false;";
                    btnFlagPass.OnClientClick = enabled ? string.Empty : "alert('您無此權限。'); return false;";

                    string text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, dataItem.Key.Key);
                    liFlag.Text = text;
                    btnFlag.CommandArgument = Convert.ToInt32(dataItem.Key.Key).ToString() + "|" + flagType.Value + "|" + text + "|" + dataItem.Key.Value.ToString();

                    if (flagType.Key == typeof(ProposalEditorFlag))
                    {
                        //編輯檢核的藍勾勾
                        if (ProposalFacade.ProposalEditorPassFlagGet((ProposalEditorFlag)Convert.ToInt32(hidEditorPassFlag.Value), Convert.ToInt32(dataItem.Key.Key)))
                        {
                            btnFlagPass.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/blueTick.png");
                            btnFlagPass.Width = 20;
                            btnFlagPass.CommandArgument = Convert.ToInt32(dataItem.Key.Key).ToString() + "|" + Proposal.Columns.EditPassFlag + "|" + text + "|" + (true).ToString();
                            if (enabled)
                            {
                                btnFlagPass.OnClientClick = "return confirm('確定要取消檢核?');";
                            }
                        }
                        else
                        {
                            btnFlagPass.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G3/Question_mark.png");
                            btnFlagPass.AlternateText = "Pass";
                            btnFlagPass.CommandArgument = Convert.ToInt32(dataItem.Key.Key).ToString() + "|" + Proposal.Columns.EditPassFlag + "|" + text + "|" + (false).ToString();
                        }
                        btnFlagPass.Visible = true;
                    }

                    if (dataItem.Key.Value)
                    {
                        btnFlag.ImageUrl = ResolveUrl("~/Themes/PCweb/images/Tick.png");
                        if (enabled)
                        {
                            btnFlag.OnClientClick = "return confirm('確定要取消檢核?');";
                        }
                    }
                    else
                    {
                        btnFlag.ImageUrl = ResolveUrl("~/Themes/PCweb/images/x.png");
                    }
                }
            }
        }

        protected void rptFlag_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (this.CheckFlag != null)
            {
                this.CheckFlag(this, new DataEventArgs<string>(e.CommandArgument.ToString()));
                ShowMessage("", ProposalContentMode.GoToProposal, ProposalId.ToString());
            }
        }

        protected void rptPponStore_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<Seller, ProposalStore>)
            {
                KeyValuePair<Seller, ProposalStore> dataItem = (KeyValuePair<Seller, ProposalStore>)e.Item.DataItem;
                HiddenField hidStoreGuid = (HiddenField)e.Item.FindControl("hidStoreGuid");
                CheckBox chkCheck = (CheckBox)e.Item.FindControl("chkCheck");
                Literal liStoreName = (Literal)e.Item.FindControl("liStoreName");
                Literal liAddress = (Literal)e.Item.FindControl("liAddress");
                Literal liOpenTime = (Literal)e.Item.FindControl("liOpenTime");
                Literal liCloseDate = (Literal)e.Item.FindControl("liCloseDate");
                hidStoreGuid.Value = dataItem.Key.Guid.ToString();
                liStoreName.Text = dataItem.Key.SellerName;
                if (dataItem.Value != null && Helper.IsFlagSet(dataItem.Value.VbsRight, VbsRightFlag.Location))
                {
                    chkCheck.Checked = true;
                }
                if (dataItem.Key.StoreTownshipId.HasValue)
                {
                    liAddress.Text = CityManager.CityTownShopStringGet(dataItem.Key.StoreTownshipId.Value) + dataItem.Key.StoreAddress;
                }
                liOpenTime.Text = dataItem.Key.OpenTime;
                liCloseDate.Text = dataItem.Key.CloseDate;
            }
        }

        protected void rptVbs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<Seller, ProposalStore>)
            {
                KeyValuePair<Seller, ProposalStore> dataItem = (KeyValuePair<Seller, ProposalStore>)e.Item.DataItem;
                HiddenField hidStoreGuid = (HiddenField)e.Item.FindControl("hidStoreGuid");
                Literal liStoreName = (Literal)e.Item.FindControl("liStoreName");
                Literal liVbsAccountID = (Literal)e.Item.FindControl("liVbsAccountID");
                Panel divtooltip = (Panel)e.Item.FindControl("divtooltip");
                Literal liMoreVbsAccountID = (Literal)e.Item.FindControl("liMoreVbsAccountID");
                TextBox txtQuantity = (TextBox)e.Item.FindControl("txtQuantity");
                CheckBox chkVerifyShop = (CheckBox)e.Item.FindControl("chkVerifyShop");
                CheckBox chkVerify = (CheckBox)e.Item.FindControl("chkVerify");
                CheckBox chkAccouting = (CheckBox)e.Item.FindControl("chkAccouting");
                CheckBox chkViewBalanceSheet = (CheckBox)e.Item.FindControl("chkViewBalanceSheet");
                Image btnHideBalanceSheet = (Image)e.Item.FindControl("btnHideBalanceSheet");
                HiddenField hidIsHideBalanceSheet = (HiddenField)e.Item.FindControl("hidIsHideBalanceSheet");

                hidStoreGuid.Value = dataItem.Key.Guid.ToString();
                hidIsHideBalanceSheet.Value = false.ToString();

                //顯示階層
                Guid root = SellerFacade.GetRootSellerGuid(dataItem.Key.Guid);
                int level = (int)SellerFacade.GetSellerTreeLevel(dataItem.Key.Guid, root);
                string space = "";
                for (int i = 1; i < level; i++)
                    space += "&nbsp;&nbsp;&nbsp;";

                liStoreName.Text = space + dataItem.Key.SellerName;

                //沒有proposal_store就不勾
                if (dataItem.Value != null)
                {

                    #region 顯示商家帳號
                    string strVbsAccountID = "";
                    string strMoreVbsAccountID = "";
                    bool IsShowToolTip = false;
                    ProposalFacade.ShowVbsAccount(dataItem.Value.VbsRight, root, dataItem.Key.Guid.ToString(), BusinessHourGuid, out strVbsAccountID, out strMoreVbsAccountID, out IsShowToolTip);
                    liVbsAccountID.Text = strVbsAccountID.Equals("") ? "" : space + strVbsAccountID;
                    liMoreVbsAccountID.Text = strMoreVbsAccountID.Equals("") ? "" : space + strMoreVbsAccountID;
                    divtooltip.Visible = IsShowToolTip;

                    #endregion

                    txtQuantity.Text = dataItem.Value.TotalQuantity.HasValue ? dataItem.Value.TotalQuantity.Value.ToString() : string.Empty;
                    if (Helper.IsFlagSet(dataItem.Value.VbsRight, VbsRightFlag.VerifyShop))
                    {
                        chkVerifyShop.Checked = true;
                    }
                    if (Helper.IsFlagSet(dataItem.Value.VbsRight, VbsRightFlag.Verify))
                    {
                        chkVerify.Checked = true;
                    }
                    if (Helper.IsFlagSet(dataItem.Value.VbsRight, VbsRightFlag.Accouting))
                    {
                        chkAccouting.Checked = true;
                    }
                    if (Helper.IsFlagSet(dataItem.Value.VbsRight, VbsRightFlag.ViewBalanceSheet))
                    {
                        chkViewBalanceSheet.Checked = true;

                    }
                    if (Helper.IsFlagSet(dataItem.Value.VbsRight, VbsRightFlag.BalanceSheetHideFromDealSeller))
                    {
                        btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/lock.png");
                        hidIsHideBalanceSheet.Value = true.ToString();
                    }
                    else
                    {
                        btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock.png");
                        hidIsHideBalanceSheet.Value = false.ToString();
                    }
                }
                else
                {
                    btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock.png");
                    hidIsHideBalanceSheet.Value = false.ToString();
                }

                if (hidStoreGuid.Value == _ProSellerGuid.ToString())
                {
                    btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock_disabled.png");
                    hidIsHideBalanceSheet.Value = false.ToString();
                }
            }
        }


        protected void btnAssign_Click(object sender, EventArgs e)
        {
            if (this.Assign != null)
            {
                List<string> AssignFlagList = hidAssignFlag.Value.Split(',').ToList();

                if (AssignFlagList.Count > 0)
                {
                    //ViewEmployee loginUser = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(UserName));
                    ViewEmployee EmpUser = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, txtEmp.Text);
                    int userId = EmpUser.UserId;
                    if (userId != 0)
                    {
                        this.Assign(this, new DataEventArgs<KeyValuePair<int, List<string>>>(new KeyValuePair<int, List<string>>(userId, AssignFlagList)));
                    }
                }


            }
        }

        protected void btnChangeLog_Click(object sender, EventArgs e)
        {
            if (this.ChangeLog != null)
            {
                this.ChangeLog(this, new DataEventArgs<string>(txtChangeLog.Text));
            }
        }

        protected void btnSellerCheck_Click(object sender, EventArgs e)
        {
            if (txtSellerCheck.Text != "")
            {
                Seller s = SellerFacade.SellerGetById(txtSellerCheck.Text);

                if (s.IsLoaded)
                {
                    if (s.StoreStatus == (int)StoreStatus.Cancel)
                    {
                        ShowMessage("無法設定隱藏之店家", ProposalContentMode.DataError);
                    }
                    else
                    {
                        _CheckSellerGuid = s.Guid;

                        if (this.UpdateSellerGuid != null)
                        {
                            this.UpdateSellerGuid(this, e);
                        }
                        ShowMessage("商家確認完成", ProposalContentMode.AlertGoToProposal, ProposalId.ToString());
                    }
                }

                else
                    ShowMessage("無該商家編號", ProposalContentMode.DataError);
            }
            else
                ShowMessage("請輸入商家編號", ProposalContentMode.DataError);
        }

        protected void rptChannel_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;

                // 頻道
                CheckBox chkChannel = (CheckBox)e.Item.FindControl("chkChannel");
                HiddenField hidChannel = (HiddenField)e.Item.FindControl("hidChannel");
                chkChannel.Text = dataItem.CategoryName;
                hidChannel.Value = dataItem.CategoryId.ToString();

                // 區域
                Repeater rptArea = (Repeater)e.Item.FindControl("rptArea");
                Repeater rptArea2 = (Repeater)e.Item.FindControl("rptArea2");
                List<CategoryTypeNode> area = dataItem.NodeDatas.Where(x => x.NodeType == CategoryType.PponChannelArea).ToList();
                if (area.Count > 0)
                {
                    CategoryTypeNode areaNode = area.First();
                    rptArea.DataSource = areaNode.CategoryNodes;
                    rptArea.DataBind();

                    rptArea2.DataSource = rptArea.DataSource;
                    rptArea2.DataBind();
                }

                // 分類
                Repeater rptCategory = (Repeater)e.Item.FindControl("rptCategory");
                List<CategoryTypeNode> categories = dataItem.NodeDatas.Where(x => x.NodeType == CategoryType.DealCategory).ToList();
                if (categories.Count > 0)
                {
                    CategoryTypeNode categoryNode = categories.First();
                    rptCategory.DataSource = categoryNode.CategoryNodes;
                    rptCategory.DataBind();
                }
            }
        }

        protected void rptArea_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                CheckBox chkArea = (CheckBox)e.Item.FindControl("chkArea");
                HiddenField hidArea = (HiddenField)e.Item.FindControl("hidArea");
                chkArea.Text = dataItem.CategoryName;
                hidArea.Value = dataItem.CategoryId.ToString();


            }
        }

        protected void rptArea2_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                // 商圈
                List<CategoryNode> specialRegionCategories = dataItem.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                if ((specialRegionCategories.Any(x => x.CategoryName == "商圈‧景點")))
                {
                    var fatherCategoryNode = specialRegionCategories.Where(x => x.CategoryName == "商圈‧景點").First();
                    Repeater rptCommercial = (Repeater)e.Item.FindControl("rptCommercial");
                    List<CategoryNode> commercials = fatherCategoryNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                    if (commercials.Count > 0)
                    {
                        rptCommercial.DataSource = commercials;
                        rptCommercial.DataBind();
                    }
                }

                if ((specialRegionCategories.Any(x => x.CategoryName.Contains("旅遊商圈‧景點"))))
                {
                    var fatherCategoryNode = specialRegionCategories.Where(x => x.CategoryName.Contains("旅遊商圈‧景點")).First();
                    Repeater rptCommercial = (Repeater)e.Item.FindControl("rptCommercial");
                    List<CategoryNode> commercials = fatherCategoryNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                    if (commercials.Count > 0)
                    {
                        rptCommercial.DataSource = commercials;
                        rptCommercial.DataBind();

                        //商圈景點子區域
                        if (fatherCategoryNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea).Any())
                        {
                            Repeater rptSPRegion = (Repeater)e.Item.FindControl("rptSPRegion");
                            rptSPRegion.DataSource = fatherCategoryNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                            rptSPRegion.DataBind();
                        }
                    }
                }
            }
        }

        protected void rptCommercial_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                CheckBox chkCommercial = (CheckBox)e.Item.FindControl("chkCommercial");
                HiddenField hidCommercial = (HiddenField)e.Item.FindControl("hidCommercial");
                chkCommercial.Text = dataItem.CategoryName;
                hidCommercial.Value = dataItem.CategoryId.ToString();
            }
        }

        protected void rptCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                CheckBox chkCategory = (CheckBox)e.Item.FindControl("chkCategory");
                HiddenField hidCategory = (HiddenField)e.Item.FindControl("hidCategory");
                HiddenField hidIsShowBackEnd = (HiddenField)e.Item.FindControl("hidIsShowBackEnd");
                chkCategory.Text = dataItem.CategoryName;
                hidCategory.Value = dataItem.CategoryId.ToString();
                hidIsShowBackEnd.Value = dataItem.IsShowBackEnd.ToString();

                //子分類
                Repeater rptSubDealCategoryArea = (Repeater)e.Item.FindControl("rptSubDealCategoryArea");

                //List<ViewCategoryDependency> subCategoryCol = PponFacade.ViewCategoryDependencyGetByParentId(dataItem.CategoryId);

                //List<Category> subDealCategories = new List<Category>();

                //foreach (var subCategory in subCategoryCol.Where(x => x.CategoryStatus == 1))
                //{
                //    Category subnode = PponFacade.SubDealCategoryGet(subCategory.CategoryId);
                //    if (subnode.IsLoaded)
                //    {
                //        subDealCategories.Add(subnode);
                //    }
                //}


                //if (subDealCategories.Count > 0)
                //{
                //    rptSubDealCategoryArea.DataSource = subDealCategories;
                //    rptSubDealCategoryArea.DataBind();
                //}
                if (dataItem.NodeDatas.Count() > 0)
                {
                    rptSubDealCategoryArea.DataSource = dataItem.NodeDatas.First().CategoryNodes;
                    rptSubDealCategoryArea.DataBind();
                }

            }
        }

        protected void rptSubDealCategoryArea_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //if (e.Item.DataItem is Category)
            //{
            //    Category dataItem = (Category)e.Item.DataItem;
            //    CheckBox chkSubDealCategory = (CheckBox)e.Item.FindControl("chkSubDealCategory");
            //    HiddenField hidSubDealCategory = (HiddenField)e.Item.FindControl("hidSubDealCategory");
            //    chkSubDealCategory.Text = dataItem.Name;
            //    hidSubDealCategory.Value = dataItem.Id.ToString();
            //}
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                CheckBox chkSubDealCategory = (CheckBox)e.Item.FindControl("chkSubDealCategory");
                HiddenField hidSubDealCategory = (HiddenField)e.Item.FindControl("hidSubDealCategory");
                HiddenField hidSubIsShowBackEnd = (HiddenField)e.Item.FindControl("hidSubIsShowBackEnd");
                chkSubDealCategory.Text = dataItem.CategoryName;
                hidSubDealCategory.Value = dataItem.CategoryId.ToString();
                hidSubIsShowBackEnd.Value = dataItem.IsShowBackEnd.ToString();
            }
        }

        protected void rptSPRegion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                Repeater rptSpecialRegion = (Repeater)e.Item.FindControl("rptSpecialRegion");
                List<CategoryNode> specialRegionCategories = dataItem.GetSubCategoryTypeNode(CategoryType.PponChannelArea);

                if (specialRegionCategories.Any())
                {
                    rptSpecialRegion.DataSource = specialRegionCategories;
                    rptSpecialRegion.DataBind();
                }
            }
        }

        protected void rptSpecialRegion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                CheckBox chkSpecialRegion = (CheckBox)e.Item.FindControl("chkSpecialRegion");
                HiddenField hidSpecialRegion = (HiddenField)e.Item.FindControl("hidSpecialRegion");
                chkSpecialRegion.Text = dataItem.CategoryName;
                hidSpecialRegion.Value = dataItem.CategoryId.ToString();
            }
        }

        #endregion

        #region private method
        private void IntialControls()
        {
            // 類型
            Dictionary<int, string> flags = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(DeliveryType)))
            {
                flags[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (DeliveryType)item);
            }
            ddlDeliveryType.DataSource = flags;
            ddlDeliveryType.DataTextField = "Value";
            ddlDeliveryType.DataValueField = "Key";
            ddlDeliveryType.DataBind();

            //通路類型
            Dictionary<int, string> flagAgentChannels = ProposalFacade.GetAgentChannels();
            chkAgentChannels.DataSource = flagAgentChannels;
            chkAgentChannels.DataTextField = "Value";
            chkAgentChannels.DataValueField = "Key";
            chkAgentChannels.DataBind();


            //成套票券類型
            ddlGroupCouponType.Items.Add(new ListItem("=請選擇=", ((int)GroupCouponDealType.None).ToString()));
            foreach (GroupCouponDealType type in Enum.GetValues(typeof(GroupCouponDealType)))
            {
                if (!string.IsNullOrEmpty(Helper.GetDescription(type)))
                {
                    ddlGroupCouponType.Items.Add(new ListItem(Helper.GetDescription(type), ((int)type).ToString()));
                }
                if (!config.EnabledGroupCouponTypeB && type == GroupCouponDealType.CostAssign)
                {
                    ddlGroupCouponType.Items[ddlGroupCouponType.Items.Count - 1].Attributes.Add("disabled", "disabled");
                }
            }

            // 時段與消費
            chkWeekdays.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalConsumerTime.Weekdays);
            chkHoliday.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalConsumerTime.Holiday);
            chkSpecialHoliday.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalConsumerTime.SpecialHoliday);
            chkBreakfast.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalConsumerTime.Breakfast);
            chkLunch.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalConsumerTime.Lunch);
            chkDinner.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalConsumerTime.Dinner);
            chkAfternoonTea.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalConsumerTime.AfternoonTea);
            chkNightSnack.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalConsumerTime.NightSnack);
            chkHolidayFare.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalConsumerTime.HolidayFare);
            chkMinimumCharge.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalConsumerTime.MinimumCharge);
            chkServiceCharge.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalConsumerTime.ServiceCharge);


            // 特殊標記
            chkFreeTax.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.FreeTax);
            chkParallelImportation.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.ParallelImportation);
            chkOnlineUse.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.OnlineUse);
            chkTravel.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.Travel);
            chkYearContract.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.YearContract);
            chkExclusive.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.Exclusive);
            chkFirstDeal.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.FirstDeal);
            chkUrgent.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.Urgent);
            chkNewItem.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.NewItem);
            chkBeauty.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.Beauty);
            chkExpiringProduct.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.ExpiringProduct);
            chkInspectRule.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.InspectRule);
            chkInvoiceFounder.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.InvoiceFounder);
            chkPhotographers.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.Photographers);
            chkSellerPhoto.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.SellerPhoto);
            chkFTPPhoto.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.FTPPhoto);
            chkEveryDayNewDeal.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.EveryDayNewDeal);
            chkSpecialPrice.Text = "特殊價格QC";
            chkSystemPic.Text = "系統圖庫";
            cbxBankDeal.Text = "銀行合作";
            chkTaiShinChosen.Text = "台新特談商品";

            //特殊需求
            chkLowGrossMarginAllowedDiscount.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.LowGrossMarginAllowedDiscount);
            chkNotAllowedDiscount.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalSpecialFlag.NotAllowedDiscount);
            if (!HasUseDiscountSetPrivilege)
            {
                divSpecialRequirement.Visible = false;
                //沒權限的話不可設定
                chkLowGrossMarginAllowedDiscount.Enabled = false;
                chkNotAllowedDiscount.Enabled = false;
            }

            //退件原因
            chkCommissionInCompatible.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.CommissionInCompatible);
            chkFoldInCompatible.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.FoldInCompatible);
            chkCopiesInCompatible.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.CopiesInCompatible);
            chkBusinessPriceLower.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.BusinessPriceLower);
            chkNeedOptimization.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.NeedOptimization);
            chkPoorThanBusinessStrife.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.PoorThanBusinessStrife);
            chkDataModifyContractInComplete.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.DataModifyContractInComplete);
            chkCouponEventContentModify.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnPpon.CouponEventContentModify);

            chkOptimizationPrice.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.OptimizationPrice);
            chkInformationError.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.InformationError);
            chkPercentLower.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.PercentLower);
            chkExistProposal.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.ExistProposal);
            chkSaleOverProposal.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.SaleOverProposal);
            chkDataModify.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.DataModify);
            chkContractError.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.ContractError);
            chkCancel.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.CancelOrderTime);
            chkCheckError.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.CheckError);
            chkOther.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalReturnHouse.Other);

            btnPhotoHelper.NavigateUrl = "#divPhotographersNew";
            btnProposalDraw.NavigateUrl = "#divProposalDraw";


            // 提案類型
            Dictionary<int, string> dealType = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(ProposalDealType)))
            {
                dealType[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDealType)item);
            }
            ddlDealType.DataSource = dealType.Where(x => x.Key != (int)ProposalDealType.None);
            ddlDealType.DataTextField = "Value";
            ddlDealType.DataValueField = "Key";
            ddlDealType.DataBind();

            // 出貨類型
            Dictionary<int, string> shipType = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(DealShipType)))
            {
                if ((DealShipType)item != DealShipType.Wms)
                    shipType[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (DealShipType)item);
            }
            ddlShipType.DataSource = shipType;
            ddlShipType.DataTextField = "Value";
            ddlShipType.DataValueField = "Key";
            ddlShipType.DataBind();

            // 活動使用有效時間
            Dictionary<int, string> startUseUnitType = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(ProposalStartUseUnitType)))
            {
                startUseUnitType[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalStartUseUnitType)item);
            }
            ddlStartUseUnit.DataSource = startUseUnitType;
            ddlStartUseUnit.DataTextField = "Value";
            ddlStartUseUnit.DataValueField = "Key";
            ddlStartUseUnit.DataBind();



            //約拍時間
            Dictionary<string, string> PhotographerHour = new Dictionary<string, string>();
            for (int i = 1; i <= 24; i++)
            {
                PhotographerHour.Add(i.ToString().PadLeft(2, '0'), i.ToString().PadLeft(2, '0'));
            }
            ddlPhotographerAppointHour.DataTextField = "Key";
            ddlPhotographerAppointHour.DataValueField = "Value";
            ddlPhotographerAppointHour.DataSource = PhotographerHour;
            ddlPhotographerAppointHour.DataBind();

            Dictionary<string, string> PhotographerMinute = new Dictionary<string, string>();
            PhotographerMinute.Add("00", "00");
            PhotographerMinute.Add("30", "30");
            ddlPhotographerAppointMinute.DataTextField = "Key";
            ddlPhotographerAppointMinute.DataValueField = "Value";
            ddlPhotographerAppointMinute.DataSource = PhotographerMinute;
            ddlPhotographerAppointMinute.DataBind();

            Dictionary<string, string> SaleOtherShop = new Dictionary<string, string>();

            SaleOtherShop.Add("-1", "請選擇");
            SaleOtherShop.Add("yahoo", "Yahoo!奇摩超級商城");
            SaleOtherShop.Add("pchome", "PChome 商店街");
            SaleOtherShop.Add("taobao", "淘寶");

            ddlOtherShop1.DataSource = SaleOtherShop;
            ddlOtherShop1.DataValueField = "Key";
            ddlOtherShop1.DataTextField = "Value";
            ddlOtherShop1.DataBind();

            ddlOtherShop2.DataSource = SaleOtherShop;
            ddlOtherShop2.DataValueField = "Key";
            ddlOtherShop2.DataTextField = "Value";
            ddlOtherShop2.DataBind();

            ddlOtherShop3.DataSource = SaleOtherShop;
            ddlOtherShop3.DataValueField = "Key";
            ddlOtherShop3.DataTextField = "Value";
            ddlOtherShop3.DataBind();

            //上檔頻道
            List<CategoryNode> nodeData = CategoryManager.PponChannelCategoryTree.CategoryNodes.OrderBy(x => x.Seq).ToList();
            rptChannel.DataSource = nodeData.Where(x => x.CategoryId.EqualsNone(91, 92, 93));
            rptChannel.DataBind();

            //配送方式
            Dictionary<int, string> deliverymethod = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(ProposalDeliveryMethod)))
            {
                ddlDeliveryMethod.Items.Add(new ListItem(Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDeliveryMethod)item), ((int)item).ToString()));
            }

            //指派攝影師
            var empCollection = hp.ViewEmployeeCollectionGetAll().Where(x => !x.IsInvisible);

            Dictionary<string, string> photographerAppointer = new Dictionary<string, string>();

            List<string> mailToUser = FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.Photographer, "/sal/ProposalContent.aspx");

            foreach (var item in mailToUser)
            {
                if (mailToUser.IndexOf(item) == 0)
                {
                    photographerAppointer.Add(mailToUser.IndexOf(item).ToString(), "請選擇");
                    photographerAppointer.Add(mailToUser.IndexOf(item) + 1.ToString(), item);
                }
                else
                {
                    photographerAppointer.Add(mailToUser.IndexOf(item) + 1.ToString(), item);
                }
            }

            ddlPhotographerAppointer.DataSource = photographerAppointer;
            ddlPhotographerAppointer.DataTextField = "Value";
            ddlPhotographerAppointer.DataValueField = "Value";
            ddlPhotographerAppointer.DataBind();
        }

        /// <summary>
        /// 欄位是否顯示
        /// </summary>
        /// <param name="model"></param>
        /// <param name="pro"></param>
        private void EnabledSettings(SalesBusinessModel model, ViewProposalSeller pro)
        {
            #region 瀏覽權限檢查 (無全區瀏覽權限，會判斷商家是否有綁定業務，有的話只能檢視自己的提案單)


            //登入者
            ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(UserName));
            //ViewEmployee emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, UserName);//之後可測試這樣是否比較快?
            if (emp.IsLoaded)
            {
                if (!CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReadAll))
                {

                    //提案單業務
                    ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
                    ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.OperationSalesId ?? 0);

                    if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.CrossDeptTeam))
                    {
                        if (string.IsNullOrEmpty(emp.CrossDeptTeam) && emp.TeamNo == null)
                        {
                            ShowMessage("身份權限未明，無法查看資料，請洽產品或技術人員協助。", ProposalContentMode.BackToList);
                        }
                        else
                        {
                            #region 本區檢視
                            string message1 = "";
                            string message2 = "";
                            bool checkDeSales = ProposalFacade.GetCrossPrivilege(2, emp, deSalesEmp.DeptId, deSalesEmp.TeamNo, deSalesEmp.DeptName, null, ref message1);


                            if (!checkDeSales)
                            {
                                if (opSalesEmp.IsLoaded)
                                {
                                    bool checkOpSales = ProposalFacade.GetCrossPrivilege(2, emp, opSalesEmp.DeptId, opSalesEmp.TeamNo, opSalesEmp.DeptName, null, ref message2);

                                    if (!checkOpSales)
                                    {
                                        ShowMessage((message1 == message2) ? message1 : (message1 + "\\n" + message2), ProposalContentMode.BackToList);
                                        return;
                                    }
                                }
                            }

                            #endregion
                        }
                    }

                    else
                    {
                        #region 無檢視權限(僅能查看負責業務是自己的提案單)

                        if ((deSalesEmp.IsLoaded && deSalesEmp.Email != UserName) && ((!opSalesEmp.IsLoaded) || opSalesEmp.IsLoaded && opSalesEmp.Email != UserName))
                        {
                            ShowMessage(string.Format("無法查看負責業務 {0} {1} 的提案單資訊。", deSalesEmp.EmpName, opSalesEmp.EmpName), ProposalContentMode.BackToList);
                            return;
                        }

                        #endregion
                    }
                }
            }
            else
            {
                ShowMessage("查無員工資料，請重新檢視員工資料維護設定。", ProposalContentMode.BackToList);
                return;
            }



            #region 合約下載權限檢查 (商家必須通過檢核才可下載合約)

            if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Download))
            {
                if (!string.IsNullOrEmpty(pro.BrandName))
                {
                    if (Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                    {
                        btnDownload.Text = "下載";
                    }
                    else
                    {
                        btnDownload.Text = "草稿下載";
                    }
                    divDownload.Visible = true;
                }
            }

            #endregion

            #region 轉介人員
            if (Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.SettingCheck))
            {
                ddlReferralOther.Enabled = false;
                ddlReferralSale.Enabled = false;
            }
            else
            {
                ddlReferralOther.Enabled = true;
                ddlReferralSale.Enabled = true;
            }
            #endregion

            #region 分店設定
            bool sellerenable;
            bool ToHouseenable;
            if (Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.SettingCheck))
            {
                sellerenable = false;
                btnPponStoreSave.Visible = false;
                btnVbsSave.Visible = false;
            }
            else
                sellerenable = true;

            //原生賣家以下的樹(包含自己)
            List<Guid> pponstore = new List<Guid>();
            var ChildrenList = SellerFacade.GetChildrenSellers(pro.SellerGuid).ToList();
            if ((StoreStatus)sp.SellerGetByBid(pro.SellerGuid).StoreStatus == StoreStatus.Available)
                pponstore.Add(pro.SellerGuid);
            foreach (var Children in ChildrenList)
                pponstore.Add(Children.Guid);

            foreach (RepeaterItem item in rptPponStore.Items)
            {
                CheckBox chkCheck = (CheckBox)item.FindControl("chkCheck");
                Literal liAddress = (Literal)item.FindControl("liAddress");
                HiddenField StoreGuid = (HiddenField)item.FindControl("hidStoreGuid");
                Guid pponstid = Guid.TryParse(StoreGuid.Value, out pponstid) ? pponstid : Guid.Empty;
                chkCheck.Enabled = liAddress.Text == "" || (StoreStatus)sp.SellerGet(pponstid).StoreStatus == StoreStatus.Cancel ? false : sellerenable;

            }
            foreach (RepeaterItem item in rptVbs.Items)
            {
                TextBox txtQuantity = (TextBox)item.FindControl("txtQuantity");
                CheckBox chkVerifyShop = (CheckBox)item.FindControl("chkVerifyShop");
                CheckBox chkVerify = (CheckBox)item.FindControl("chkVerify");
                CheckBox chkAccouting = (CheckBox)item.FindControl("chkAccouting");
                CheckBox chkViewBalanceSheet = (CheckBox)item.FindControl("chkViewBalanceSheet");
                Image btnHideBalanceSheet = (Image)item.FindControl("btnHideBalanceSheet");
                HiddenField hidIsHideBalanceSheet = (HiddenField)item.FindControl("hidIsHideBalanceSheet");
                HiddenField StoreGuid = (HiddenField)item.FindControl("hidStoreGuid");

                Guid stid = Guid.TryParse(StoreGuid.Value, out stid) ? stid : Guid.Empty;
                chkVerifyShop.Enabled = chkVerify.Enabled = chkAccouting.Enabled = chkViewBalanceSheet.Enabled = txtQuantity.Enabled = pponstore.Contains(stid);

                //宅配只能勾自己
                if (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType != (int)ProposalDealType.Travel && pro.SellerGuid.ToString() != StoreGuid.Value)
                {
                    ToHouseenable = false;
                }
                else
                {
                    ToHouseenable = true;
                }



                #region 檢視對帳單圖片設定
                if (sellerenable)
                {
                    if (pro.SellerGuid == stid)
                        btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock_disabled.png");
                    else
                    {
                        if (!pponstore.Contains(stid))
                            btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock_disabled.png");
                        else
                        {
                            if (hidIsHideBalanceSheet.Value == true.ToString())
                                btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/lock.png");
                            else
                                btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock.png");
                        }
                    }
                }
                else
                {
                    if (!pponstore.Contains(stid))
                        btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock_disabled.png");
                    else
                    {
                        if (hidIsHideBalanceSheet.Value == true.ToString())
                            btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/lock_disabled.png");
                        else
                            btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock_disabled.png");
                    }
                }
                #endregion

                txtQuantity.Enabled = sellerenable ? ToHouseenable ? txtQuantity.Enabled : false : false;
                chkVerifyShop.Enabled = sellerenable ? ToHouseenable ? chkVerifyShop.Enabled : false : false;
                chkVerify.Enabled = sellerenable ? ToHouseenable ? chkVerify.Enabled : false : false;
                chkAccouting.Enabled = sellerenable ? ToHouseenable ? chkAccouting.Enabled : false : false;
                chkViewBalanceSheet.Enabled = sellerenable ? ToHouseenable ? chkViewBalanceSheet.Enabled : false : false;
            }
            #endregion

            #endregion

            if (pro.IsLoaded)
            {
                #region 更新權限檢查 (尚未提案申請、非快速上檔的提案單，才可變更資料)

                EnableControls(false, false, false, false, pro);

                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Update))
                {
                    //已提案申請
                    if (Helper.IsFlagSet(pro.ApplyFlag, ProposalApplyFlag.Apply))
                    {
                        if (ddlDeliveryType.SelectedValue == Convert.ToString((int)DeliveryType.ToShop))
                        {
                            //憑證規則不變
                            EnableControls(false, false, true, false, pro);
                        }
                        else
                        {
                            #region 宅配
                            EnableControls(false, false, true, false, pro);
                            if (!Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                            {
                                //尚未QC
                                if (Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.SaleEditor))
                                {
                                    if (Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.CheckWaitting) && !Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.Check))
                                    
{
                                        //等待商家審核
                                        EnableControls(false, false, true, false, pro);
                                        btnProposalSend.Visible = false;
                                    }
                                    else
                                    {
                                        //業務編輯中
                                        btnSellerProposalCheckWaitting.Visible = true;  //請求商家覆核
                                        if (pro.ProposalCreatedType == (int)ProposalCreatedType.Seller)
                                        {
                                            divSellerProposalReturned.Visible = true;   //提案單退回商家
                                        }
                                        EnableControls(false, false, true, true, pro);
                                    }
                                }
                                else
                                {
                                    //非業務編輯中不能編輯
                                    btnSellerProposalSaleEditor.Visible = false;//編輯商家提案
                                    divSellerProposalReturned.Visible = false;   //提案單退回商家

                                    EnableControls(false, false, true, false, pro);
                                }
                            }
                            else
                            {
                                //QC後不能編輯
                                btnProposalSend.Visible = false;
                                btnSellerProposalCheckWaitting.Visible = false;  //請求商家覆核
                                divSellerProposalReturned.Visible = false;   //提案單退回商家
                                EnableControls(false, false, true, false, pro);
                            }
                            #endregion
                        }
                    }
                    //草稿
                    else
                    {
                        if (pro.CopyType == (int)ProposalCopyType.Similar || pro.CopyType == (int)ProposalCopyType.Same)
                        {
                            #region 複製檔
                            if (ddlDeliveryType.SelectedValue == Convert.ToString((int)DeliveryType.ToShop))
                            {
                                //憑證規則不變
                                if (pro.CopyType == (int)ProposalCopyType.Same)
                                {
                                    EnableControls(false, false, false, false, pro);
                                }
                                else
                                {
                                    EnableControls(true, false, true, true, pro);
                                }
                            }
                            else
                            {
                                #region 宅配
                                if (Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.SaleEditor))
                                {
                                    if (Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.CheckWaitting) && !Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.Check))
                                    {
                                        //等待商家審核
                                        EnableControls(true, true, true, false, pro);
                                        btnProposalSend.Visible = false;
                                    }
                                    else
                                    {
                                        //業務編輯中
                                        btnSellerProposalCheckWaitting.Visible = true;  //請求商家覆核
                                        if (pro.ProposalCreatedType == (int)ProposalCreatedType.Seller)
                                        {
                                            divSellerProposalReturned.Visible = true;   //提案單退回商家
                                        }

                                        EnableControls(true, true, true, true, pro);
                                    }
                                }
                                else
                                {
                                    //非業務編輯中不能編輯
                                    if (!Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                                    {
                                        btnProposalSend.Visible = true;

                                        btnSellerProposalSaleEditor.Visible = true;//編輯商家提案
                                        if (pro.ProposalCreatedType == (int)ProposalCreatedType.Seller)
                                        {
                                            divSellerProposalReturned.Visible = true;   //提案單退回商家
                                        }
                                    }
                                    else
                                    {
                                        btnSellerProposalSaleEditor.Visible = false;//編輯商家提案
                                        divSellerProposalReturned.Visible = false;   //提案單退回商家
                                    }


                                    EnableControls(true, true, true, false, pro);
                                }
                                #endregion
                            }
                            #endregion
                        }
                        else
                        {
                            #region 新檔
                            if (ddlDeliveryType.SelectedValue == Convert.ToString((int)DeliveryType.ToShop))
                            {
                                //憑證規則不變
                                EnableControls(true, true, true, true, pro);
                            }
                            else
                            {
                                #region 宅配
                                if (Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.SaleEditor))
                                {
                                    if (Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.CheckWaitting) && !Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.Check))
                                    {
                                        //等待商家審核
                                        EnableControls(true, true, true, false, pro);
                                        btnProposalSend.Visible = false;
                                    }
                                    else
                                    {
                                        //業務編輯中
                                        btnSellerProposalCheckWaitting.Visible = true;  //請求商家覆核
                                        if (pro.ProposalCreatedType == (int)ProposalCreatedType.Seller)
                                        {
                                            divSellerProposalReturned.Visible = true;   //提案單退回商家
                                        }

                                        EnableControls(true, true, true, true, pro);
                                    }
                                }
                                else
                                {
                                    //非業務編輯中不能編輯
                                    if (!Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                                    {
                                        btnProposalSend.Visible = true;

                                        btnSellerProposalSaleEditor.Visible = true;//編輯商家提案
                                        if (pro.ProposalCreatedType == (int)ProposalCreatedType.Seller)
                                        {
                                            divSellerProposalReturned.Visible = true;   //提案單退回商家
                                        }
                                    }
                                    else
                                    {
                                        btnSellerProposalSaleEditor.Visible = false;//編輯商家提案
                                        divSellerProposalReturned.Visible = false;   //提案單退回商家
                                    }


                                    EnableControls(true, true, true, false, pro);
                                }
                                #endregion
                            }

                            if (txtBrandName.Text.Trim() == "")
                            {
                                btnSellerProposalCheckWaitting.Visible = false;  //請求商家覆核
                                btnSellerProposalSaleEditor.Visible = false;//編輯商家提案
                                divSellerProposalReturned.Visible = false;   //提案單退回商家
                            }
                            #endregion
                        }


                    }
                }


                #endregion


                #region 提案申請權限檢查

                if (Helper.IsFlagSet(pro.ApplyFlag, ProposalApplyFlag.Apply))
                {
                    //是否已送件
                    btnProposalSend.Visible = false;
                }
                else
                {
                    if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Apply))
                    {
                        //有無送件權限
                        if (ddlDeliveryType.SelectedValue == Convert.ToString((int)DeliveryType.ToHouse))
                        {
                            if (Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.CheckWaitting) && !Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.Check))
                            {
                                //等待商家覆核，應該不能再送QC
                                btnProposalSend.Visible = false;
                            }
                            else
                            {
                                btnProposalSend.Visible = true;
                            }
                        }
                        else
                        {
                            btnProposalSend.Visible = true;
                        }
                    }
                    else
                    {
                        btnProposalSend.Visible = false;
                    }
                }

                if (string.IsNullOrEmpty(pro.BrandName))
                {
                    btnProposalSend.Visible = false;
                }

                #endregion

                #region 刪除權限檢查 (尚未建檔的提案，才可刪除資料)

                if (pro.BusinessHourGuid == null && CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Delete))
                {
                    btnDelete.Visible = true;
                }

                #endregion

                #region 複製提案單(建檔後)權限

                bool vReCreateProposal = false;
                if (model != null)
                {

                    if (model.Business.BusinessHourOrderTimeS < DateTime.Now)
                    {
                        if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReCreateProposal))
                        {
                            btnReCreateProposal.Visible = true;
                            vReCreateProposal = true;
                        }
                        //if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReCreateSameProposal))
                        //{
                        //    //btnReCreateSameProposal.Visible = true;
                        //}
                    }
                    else
                    {
                        if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReCreateProposal))
                        {
                            btnReCreateProposal.Visible = true;
                            vReCreateProposal = true;
                        }
                        else
                        {
                            btnReCreateProposal.Visible = false;
                        }
                        //btnReCreateSameProposal.Visible = false;
                    }
                }
                else
                {
                    btnReCreateProposal.Visible = false;
                    //btnReCreateSameProposal.Visible = false;
                }

                #endregion

                #region 複製提案單(建檔前)權限

                btnCloneProposal.Visible = false;
                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReCreateProposal))
                {
                    if (string.IsNullOrEmpty(pro.BrandName))
                    {
                        //尚未輸入輸入資料
                        btnCloneProposal.Visible = false;
                    }
                    else
                    {
                        //建檔前/後複製提案單只會出現一個
                        btnCloneProposal.Visible = !vReCreateProposal;
                    }
                }
                else
                {
                    //沒權限 
                    btnCloneProposal.Visible = false;
                }

                #endregion

                #region 搬移賣家權限檢查 (尚未建檔且非提案諮詢的提案，才可搬移賣家)

                if (pro.BusinessHourGuid == null && CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ProposalChangeSeller) && pro.SellerGuid != Guid.Empty)
                {
                    phdChangeSeller.Visible = true;
                }

                #endregion

                #region 退件權限檢查

                if (Helper.IsFlagSet(pro.ApplyFlag, ProposalApplyFlag.Apply) && CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ProposalReturned))
                {
                    //在排檔編輯完成後只能由商審退件
                    bool bReturn = false;
                    if (Helper.IsFlagSet(pro.BusinessFlag, ProposalBusinessFlag.ScheduleCheck))
                    {
                        if (SellerFacade.IsDepartment(EmployeeDept.Q000, UserName))
                        {
                            bReturn = true;
                        }
                        else
                        {
                            bReturn = false;
                        }
                    }
                    else
                    {
                        bReturn = true;
                    }
                    if (bReturn)
                    {
                        divProposalReturned.Visible = true;
                        phReturnAll.Visible = true;

                        #region 退件原因
                        if (ddlDeliveryType.SelectedValue == Convert.ToString((int)DeliveryType.ToShop))
                        {
                            phReturnPpon.Visible = true;
                            phReturnHouse.Visible = false;
                        }
                        else
                        {
                            phReturnPpon.Visible = false;
                            phReturnHouse.Visible = true;
                        }

                    }
                    else
                    {
                        divProposalReturned.Visible = false;
                        phReturnAll.Visible = false;
                        phReturnPpon.Visible = false;
                        phReturnHouse.Visible = false;
                    }

                    #endregion
                }
                else
                {
                    divProposalReturned.Visible = false;
                    phReturnAll.Visible = false;
                    phReturnPpon.Visible = false;
                    phReturnHouse.Visible = false;

                }

                #endregion

                #region 抽單權限
                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Apply))
                {
                    if (Helper.IsFlagSet(pro.ApplyFlag, ProposalApplyFlag.Apply))
                    {
                        if (!Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                        {
                            //只有檢核狀態《提案申請》V &《QC確認》X 的情況下，才會出現出功能按鈕
                            btnProposalDraw.Visible = true;
                        }
                    }
                }
                #endregion 抽單權限

                #region 轉件權限檢查

                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Referral))
                {
                    divDevelopeReferral.Visible = true;
                    divOperationReferral.Visible = true;
                }
                else
                {
                    divDevelopeReferral.Visible = false;
                    divOperationReferral.Visible = false;
                }

                #endregion

                #region 建檔權限檢查
                if (pro.Status >= (int)ProposalStatus.Approve && pro.BusinessHourGuid == null &&
                    CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.BusinessCreated))
                {
                    btnCreateBusinessHour.Visible = true;
                }
                else
                {
                    btnCreateBusinessHour.Visible = false;
                }
                #endregion

                #region 編輯商家提案單權限檢查
                if (!CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ProposalSellerEdit))
                {
                    btnSellerProposalSaleEditor.Visible = false;
                    btnSellerProposalCheckWaitting.Visible = false;
                }
                #endregion

                #region 排檔權限檢查(再調整)
                if (Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.SettingCheck))
                {
                    //排檔後不可在修改照片來源
                    chkPhotographers.Enabled = false;
                    chkSellerPhoto.Enabled = false;
                    chkFTPPhoto.Enabled = false;
                    chkSystemPic.Enabled = false;

                    txtKeyword.Enabled = false;
                    ddlDeliveryMethod.Enabled = false;
                    txtProductSpec.Enabled = false;
                    txtFTPPhoto.Enabled = false;

                    chkDealStar.Enabled = false;
                    txtDealStar.Enabled = false;
                    chkDealCharacterToShop1.Enabled = false;
                    chkDealCharacterToShop2.Enabled = false;
                    chkDealCharacterToShop3.Enabled = false;
                    chkDealCharacterToShop4.Enabled = false;
                    txtDealCharacterToShop1.Enabled = false;
                    txtDealCharacterToShop2.Enabled = false;
                    txtDealCharacterToShop3.Enabled = false;
                    txtDealCharacterToShop4.Enabled = false;
                    txtDealCharacterToShop5.Enabled = false;
                    chkDealCharacterToHouse.Enabled = false;
                    txtDealCharacterToHouse1.Enabled = false;
                    txtDealCharacterToHouse2.Enabled = false;
                    txtDealCharacterToHouse3.Enabled = false;
                    txtDealCharacterToHouse4.Enabled = false;
                    chkRecommendFood.Enabled = false;
                    chkEnvironmentIntroduction.Enabled = false;
                    chkAroundScenery.Enabled = false;
                    chkSeasonActivity.Enabled = false;
                    chkStoreStory.Enabled = false;
                    txtStoreStory.Enabled = false;
                    chkBeauty.Enabled = false;
                    chkExpiringProduct.Enabled = false;
                    chkInspectRule.Enabled = false;
                    chkMediaReport.Enabled = false;
                    chkMediaReportPic.Enabled = false;
                    chkTravel.Enabled = false;

                    txtContractMemo.Enabled = false;

                    txtAncestorBid.Enabled = false;
                    txtAncestorSeqBid.Enabled = false;

                    //出貨類型
                    ddlShipType.Enabled = false;
                    //鑑賞期
                    chkTrialPeriod.Enabled = false;
                    //巿場分析
                    txtMarketAnalysis.Enabled = false;
                    //建議的上檔時間欄位
                    txtScheduleExpect.Enabled = false;

                    #region 上檔頻道

                    foreach (RepeaterItem channelItem in rptChannel.Items)
                    {
                        CheckBox chkChannel = (CheckBox)channelItem.FindControl("chkChannel");
                        chkChannel.Enabled = false;

                        // 區域
                        Repeater rptArea = (Repeater)channelItem.FindControl("rptArea");
                        foreach (RepeaterItem areaItem in rptArea.Items)
                        {
                            CheckBox chkArea = (CheckBox)areaItem.FindControl("chkArea");
                            chkArea.Enabled = false;
                        }

                        Repeater rptArea2 = (Repeater)channelItem.FindControl("rptArea2");
                        foreach (RepeaterItem areaItem in rptArea2.Items)
                        {
                            // 商圈
                            Repeater rptCommercial = (Repeater)areaItem.FindControl("rptCommercial");
                            foreach (RepeaterItem commItem in rptCommercial.Items)
                            {
                                CheckBox chkCommercial = (CheckBox)commItem.FindControl("chkCommercial");
                                chkCommercial.Enabled = false;
                            }

                            //旅遊-商圈‧景點
                            Repeater rptSPRegion = (Repeater)areaItem.FindControl("rptSPRegion");
                            foreach (RepeaterItem spregion in rptSPRegion.Items)
                            {
                                Repeater rptSpecialRegion = (Repeater)spregion.FindControl("rptSpecialRegion");
                                foreach (RepeaterItem specialregion in rptSpecialRegion.Items)
                                {
                                    CheckBox chkSpecialRegion = (CheckBox)specialregion.FindControl("chkSpecialRegion");
                                    chkSpecialRegion.Enabled = false;
                                }
                            }
                        }


                        // 分類
                        Repeater rptCategory = (Repeater)channelItem.FindControl("rptCategory");
                        foreach (RepeaterItem areaItem in rptCategory.Items)
                        {
                            CheckBox chkCategory = (CheckBox)areaItem.FindControl("chkCategory");
                            chkCategory.Enabled = false;

                            // 子分類
                            Repeater rptSubDealCategoryArea = (Repeater)areaItem.FindControl("rptSubDealCategoryArea");
                            foreach (RepeaterItem subDealItem in rptSubDealCategoryArea.Items)
                            {
                                CheckBox chkSubCategory = (CheckBox)subDealItem.FindControl("chkSubDealCategory");
                                chkSubCategory.Enabled = false;
                            }
                        }


                    }
                    #endregion
                }


                #endregion

                #region 上檔權限檢查
                if (Helper.IsFlagSet(pro.BusinessFlag, ProposalBusinessFlag.ProposalAudit) && CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ScheduleCheck))
                {
                    divBuseinssHourOrderTimeSet.Visible = true;
                    if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.Urgent))
                    {
                        btnUrgent.Visible = false;
                        lblUrgent.Visible = true;
                        btnCancelUrgent.Visible = true;
                    }
                    else
                    {
                        btnUrgent.Visible = true;
                        lblUrgent.Visible = false;
                        btnCancelUrgent.Visible = false;
                    }
                    if (Helper.IsFlagSet(pro.BusinessFlag, ProposalBusinessFlag.ScheduleCheck))
                    {
                        btnCancelOrderTimeSave.Visible = true;
                    }
                    else
                    {
                        btnCancelOrderTimeSave.Visible = false;
                    }
                }
                else
                {
                    divBuseinssHourOrderTimeSet.Visible = false;
                }
                #endregion

                #region 修改優惠內容權限
                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Update))
                {
                    hidMultiDealsUpdate.Value = "1";
                }
                else
                {
                    hidMultiDealsUpdate.Value = "-1";
                }
                #endregion

                #region 提前頁確權限 
                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.EarlierPageCheck))
                {
                    chkEarlierPageCheck.Enabled = true;
                    txtEarlierPageCheckDay.Enabled = true;
                }
                else
                {
                    //業務在送件QC前可以調整頁確
                    if (!Helper.IsFlagSet(pro.ApplyFlag, ProposalApplyFlag.Apply))
                    {
                        ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
                        ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.OperationSalesId ?? 0);
                        if (deSalesEmp.Email == UserName || opSalesEmp.Email == UserName)
                        {
                            chkEarlierPageCheck.Enabled = true;
                            txtEarlierPageCheckDay.Enabled = true;
                        }
                        else
                        {
                            chkEarlierPageCheck.Enabled = false;
                            txtEarlierPageCheckDay.Enabled = false;
                        }
                    }
                    else
                    {
                        chkEarlierPageCheck.Enabled = false;
                        txtEarlierPageCheckDay.Enabled = false;
                    }
                }

                if (Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                {
                    hkRestrictionsEdit.Visible = false;
                    chkEarlierPageCheck.Enabled = false;
                    txtEarlierPageCheckDay.Enabled = false;
                }
                #endregion

                #region 攝影權限檢查

                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.PhotographerCheck))
                {
                    //若無權限僅能查詢拍攝註記
                    hidPhotographerPri.Value = "1";
                }

                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.PhotographerCheck) || (User.IsInRole(MemberRoles.Sales.ToString()) && Helper.IsFlagSet(pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppointCheck)))
                {
                    hidPhotographerRes.Value = "1";
                }

                //作用移到前台即時反應並調整為草稿時，就可以使用 
                if (pro.SellerGuid != Guid.Empty)
                {
                    btnPhotoHelper.Visible = true;
                }
                else
                {
                    btnPhotoHelper.Visible = false;
                }

                #endregion

                #region 備註說明權限
                if (Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck) && Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.Created) &&
                    Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.SettingCheck) && !CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Memo))
                {
                    txtMemo.Enabled = false;
                }

                #endregion

                #region 工作指派權限檢查

                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Assign))
                {
                    divAssign.Visible = true;

                    int ProposalEditorFlag = 0;
                    foreach (var item in Enum.GetValues(typeof(ProposalEditorFlag)))
                    {
                        ProposalEditorFlag += (int)(ProposalEditorFlag)item;
                    }
                    if (pro.EditFlag != ProposalEditorFlag)
                    {
                        divAssignText.Visible = true;
                    }
                    else
                    {
                        divAssignText.Visible = false;
                    }
                }

                #endregion

            }

            divMain.Visible = true;
        }

        /// <summary>
        /// 欄位是否可編輯
        /// </summary>
        /// <param name="enabled">送件後是否可異動</param>
        /// <param name="copy_enabled"></param>
        /// <param name="editor_enabled"></param>
        /// <param name="seller_enabled">商家提案(宅配)欄位是否可以異動</param>
        /// <param name="pro"></param>
        private void EnableControls(bool enabled, bool copy_enabled, bool editor_enabled, bool seller_enabled, ViewProposalSeller pro)
        {

            if (ddlDeliveryType.SelectedValue == Convert.ToString((int)DeliveryType.ToHouse) && pro.ProposalCreatedType == (int)ProposalCreatedType.Seller)
            {
                //商家建的宅配

                txtBrandName.Enabled = seller_enabled;
                ddlDealType.Enabled = seller_enabled;
                ddlDealSubType.Enabled = seller_enabled;
                ddlDeliveryMethod.Enabled = seller_enabled; //配送方式

                rbRecordInvoice.Enabled = seller_enabled;   //店家單據開立方式
                rbRecordNoTaxInvoice.Enabled = seller_enabled;
                rbRecordReceipt.Enabled = seller_enabled;
                rbRecordOthers.Enabled = seller_enabled;
                txtAccountingMessage.Enabled = seller_enabled;

                chkParallelImportation.Enabled = seller_enabled;

                ddlShipType.Enabled = seller_enabled;       //出貨類型
                txtShipText1.Enabled = seller_enabled;      //上檔後X個工作日
                txtShipText2.Enabled = seller_enabled;      //統一結檔後X個工作日
                txtShipText3.Enabled = seller_enabled;      //訂單成立後X個工作日出貨完畢
                txtShipTypeOther.Enabled = seller_enabled;
                rboUniShipDay.Enabled = seller_enabled;     //最早出貨日
                rboNormalShipDay.Enabled = seller_enabled;  //訂單成立後

                chkPhotographers.Enabled = seller_enabled;  //照片來源
                chkSellerPhoto.Enabled = seller_enabled;
                chkFTPPhoto.Enabled = seller_enabled;
                chkSystemPic.Enabled = seller_enabled;

                txtProductSpec.Enabled = seller_enabled;
                chkBeauty.Enabled = seller_enabled;         //醫療 / 妝廣字號
                txtBeauty.Enabled = seller_enabled;
                chkExpiringProduct.Enabled = seller_enabled;//即期品
                txtExpiringProduct.Enabled = seller_enabled;
                chkInspectRule.Enabled = seller_enabled;    //檢驗規定
                txtInspectRule.Enabled = seller_enabled;
                txtContractMemo.Enabled = seller_enabled;   //合約附註說明

                chkDealStar.Enabled = seller_enabled;               //本檔主打星
                txtDealStar.Enabled = seller_enabled;
                chkDealCharacterToHouse.Enabled = seller_enabled;   //檔次特色
                txtDealCharacterToHouse1.Enabled = seller_enabled;
                txtDealCharacterToHouse2.Enabled = seller_enabled;
                txtDealCharacterToHouse3.Enabled = seller_enabled;
                txtDealCharacterToHouse4.Enabled = seller_enabled;

                chkStoreStory.Enabled = seller_enabled;             //品牌故事
                txtStoreStory.Enabled = seller_enabled;

                chkMediaReport.Enabled = seller_enabled;            //媒體
                chkMediaReportPic.Enabled = seller_enabled;




                bool eflag = false;
                if (!Helper.IsFlagSet(pro.ApplyFlag, ProposalApplyFlag.Apply))
                {
                    if (pro.PassSeller == 1)
                    {
                        if (Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.Send))
                        {
                            //商家送件
                            eflag = false;
                            if (Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.CheckWaitting))
                            {
                                //請求商家覆核

                            }
                            else if (Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.SaleEditor))
                            {
                                //業務編輯中
                                eflag = true;
                            }
                        }
                    }
                    else if (Helper.IsFlagSet(pro.ApplyFlag, ProposalApplyFlag.Apply) && !Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.Created))
                    {
                        if (Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.Check))
                        {
                            //商家已覆核
                            eflag = true;
                        }
                        else
                        {
                            eflag = false;
                        }
                    }
                    else
                    {
                        eflag = true;
                    }
                }
                else
                {
                    eflag = true;
                }

                hidSellerFlag.Value = seller_enabled ? "1" : "0";
                hidSellerCheckFlag.Value = eflag ? "1" : "0";

            }
            else
            {
                //enable:送件後不能異動，editor_enabled送件仍可異動
                //憑證+業務建的宅配
                txtBrandName.Enabled = enabled;
                ddlDealType.Enabled = enabled;
                ddlDealSubType.Enabled = enabled;

                chkParallelImportation.Enabled = enabled;    //平行輸入

                chkPhotographers.Enabled = editor_enabled;   //照片來源
                chkSellerPhoto.Enabled = editor_enabled;
                chkFTPPhoto.Enabled = editor_enabled;
                chkSystemPic.Enabled = editor_enabled;

                txtContractMemo.Enabled = editor_enabled;    //合約附註說明
                chkStoreStory.Enabled = editor_enabled;      //品牌故事
                txtStoreStory.Enabled = editor_enabled;

                hidSellerFlag.Value = "1";
                hidSellerCheckFlag.Value = "1";

                if (ddlDeliveryType.SelectedValue == Convert.ToString((int)DeliveryType.ToHouse))
                {
                    #region 只有宅配才有
                    ddlDeliveryMethod.Enabled = enabled;//配送方式

                    ddlShipType.Enabled = enabled;      //出貨類型
                    txtShipText1.Enabled = enabled;     //上檔後X個工作日
                    txtShipText2.Enabled = enabled;     //統一結檔後X個工作日
                    txtShipText3.Enabled = enabled;     //訂單成立後X個工作日出貨完畢
                    txtShipTypeOther.Enabled = enabled;
                    rboUniShipDay.Enabled = enabled;    //最早出貨日
                    rboNormalShipDay.Enabled = enabled; //訂單成立後

                    txtProductSpec.Enabled = editor_enabled;     //規格功能/主要成份

                    //FAB
                    chkBeauty.Enabled = editor_enabled;          //醫療 / 妝廣字號
                    txtBeauty.Enabled = editor_enabled;
                    chkExpiringProduct.Enabled = editor_enabled; //即期品
                    txtExpiringProduct.Enabled = editor_enabled;
                    chkInspectRule.Enabled = editor_enabled;     //檢驗規定
                    txtInspectRule.Enabled = editor_enabled;

                    //巿場分析比價
                    txtGomajiPrice.Enabled = enabled;
                    txtGomajiLink.Enabled = enabled;
                    txtKuobrothersPrice.Enabled = enabled;
                    txtKuobrothersLink.Enabled = enabled;
                    txtCrazymikePrice.Enabled = enabled;
                    txtCrazymikeLink.Enabled = enabled;
                    txtEzPricePrice1.Enabled = enabled;
                    txtEzPriceLink1.Enabled = enabled;
                    txtEzPricePrice2.Enabled = enabled;
                    txtEzPriceLink2.Enabled = enabled;
                    txtEzPricePrice3.Enabled = enabled;
                    txtEzPriceLink3.Enabled = enabled;
                    txtPinglePrice1.Enabled = enabled;
                    txtPingleLink1.Enabled = enabled;
                    txtPinglePrice2.Enabled = enabled;
                    txtPingleLink2.Enabled = enabled;
                    txtPinglePrice3.Enabled = enabled;
                    txtPingleLink3.Enabled = enabled;
                    txtSimilarPrice.Enabled = enabled;
                    txtSimilarLink.Enabled = enabled;
                    ddlOtherShop1.Enabled = enabled;
                    txtOtherPrice1.Enabled = enabled;
                    txtOtherLink1.Enabled = enabled;
                    ddlOtherShop2.Enabled = enabled;
                    txtOtherPrice2.Enabled = enabled;
                    txtOtherLink2.Enabled = enabled;
                    ddlOtherShop3.Enabled = enabled;
                    txtOtherPrice3.Enabled = enabled;
                    txtOtherLink3.Enabled = enabled;

                    //商品寄倉
                    if (config.IsConsignment)
                    {
                        chkConsignment.Enabled = enabled;
                    }
                    #endregion
                }
            }



            //提案內容
            ddlDeliveryType.Enabled = false;    //消費方式皆不能異動
            chkSelfOptional.Enabled = enabled;  //自選檔
            txtBusinessPrice.Enabled = enabled; //預估營業額
            chkGroupCoupon.Enabled = enabled;   //成套票券
            cbxBankDeal.Enabled = enabled;   //銀行合作
            ddlGroupCouponType.Enabled = enabled; //成套票券選項
            chkNoRestrictedStore.Enabled = enabled;   //通用券
            chkPromotionDeal.Enabled = enabled; //展演類型檔次
            chkPromotionDeal.Visible = (ddlDeliveryType.SelectedValue != Convert.ToString((int)DeliveryType.ToHouse));
            chkExhibitionDeal.Enabled = enabled;    //展覽類型檔次
            chkExhibitionDeal.Visible = (ddlDeliveryType.SelectedValue != Convert.ToString((int)DeliveryType.ToHouse));
            chkGame.Enabled = enabled;    //遊戲檔
            chkGame.Visible = (ddlDealType.SelectedValue == Convert.ToString((int)ProposalDealType.Piinlife)) ? false : true;
            //chkChannelGift.Enabled = enabled;    //遊戲檔
            txtMarketAnalysis.Enabled = enabled;//市場分析

            //時程資訊
            txtScheduleExpect.Enabled = editor_enabled;//建議的上檔時間
            ddlStartUseUnit.Enabled = enabled;//活動使用有效時間
            txtStartUse.Enabled = enabled;
            txtStartUse2.Enabled = enabled;

            //商品資訊
            //特殊標記
            chkOnlineUse.Enabled = enabled;//即買即用
            chkOnlineUse.Visible = (ddlDeliveryType.SelectedValue == Convert.ToString((int)DeliveryType.ToHouse) && ddlDealType.SelectedValue != Convert.ToString((int)ProposalDealType.Travel) ? false : true);
            chkYearContract.Enabled = enabled;//長期上架
            chkExclusive.Enabled = enabled;//獨家
            chkExclusive.Visible = (ddlDeliveryType.SelectedValue == Convert.ToString((int)DeliveryType.ToHouse) && ddlDealType.SelectedValue != Convert.ToString((int)ProposalDealType.Travel) ? false : true);
            chkNewItem.Enabled = enabled;//新商品
            chkInvoiceFounder.Enabled = enabled;//發票開立人
            chkInvoiceFounder.Visible = (ddlDeliveryType.SelectedValue == Convert.ToString((int)DeliveryType.ToHouse) && ddlDealType.SelectedValue != Convert.ToString((int)ProposalDealType.Travel) ? false : true);
            chkEveryDayNewDeal.Enabled = enabled;   //每日一物
            chkEveryDayNewDeal.Visible = (ddlDeliveryType.SelectedValue == Convert.ToString((int)DeliveryType.ToHouse) && ddlDealType.SelectedValue != Convert.ToString((int)ProposalDealType.Travel) ? true : false);
            chkSpecialPrice.Enabled = enabled;   //特殊價格QC
            chkSpecialPrice.Visible = (ddlDeliveryType.SelectedValue == Convert.ToString((int)DeliveryType.ToHouse) && ddlDealType.SelectedValue != Convert.ToString((int)ProposalDealType.Travel) ? true : false);
            //免稅商品於前端控制enable

            //時段與消費
            chkWeekdays.Enabled = enabled;
            chkHoliday.Enabled = enabled;
            chkSpecialHoliday.Enabled = enabled;
            chkBreakfast.Enabled = enabled;
            chkLunch.Enabled = enabled;
            chkDinner.Enabled = enabled;
            chkAfternoonTea.Enabled = enabled;
            chkNightSnack.Enabled = enabled;
            chkHolidayFare.Enabled = enabled;
            chkMinimumCharge.Enabled = enabled;
            chkServiceCharge.Enabled = enabled;


            txtMemo.Enabled = editor_enabled;

            //FAB
            chkRecommendFood.Enabled = editor_enabled;
            txtRecommendFood.Enabled = editor_enabled;
            chkEnvironmentIntroduction.Enabled = editor_enabled;
            txtEnvironmentIntroduction.Enabled = editor_enabled;
            chkAroundScenery.Enabled = editor_enabled;
            txtAroundScenery.Enabled = editor_enabled;
            chkSeasonActivity.Enabled = editor_enabled;
            txtSeasonActivity.Enabled = editor_enabled;
            chkTravel.Enabled = editor_enabled;
            txtTravel.Enabled = editor_enabled;

            //只有商家可以勾
            chkFileDone.Enabled = false;

            //拍攝資料
            txtPhohoNeeds.Enabled = editor_enabled;
            txtRefrenceData.Enabled = editor_enabled;

            //儲存
            btnSave.Visible = enabled | editor_enabled;


        }

        /// <summary>
        /// 根據word範本下載word
        /// </summary>
        /// <param name="pro">提案單</param>
        /// <param name="type">提案類型</param>
        /// <param name="dataList">提案單<相關資訊</param>
        /// <param name="fileName">檔名</param>
        /// <param name="contacts">聯絡人</param>
        /// <param name="LocationstoreList">營業據點</param>
        /// <param name="AccountingstoreList">核銷相關</param>
        public void DownloadWord(Proposal pro, DeliveryType type, Dictionary<string, string> dataList, string fileName, string contacts, List<Seller> LocationstoreList, List<Seller> AccountingstoreList)
        {
            #region get template file

            string wordFile = string.Empty;
            string footer = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderFooter.docx");//蓋章
            string ProposalContractContact = HttpContext.Current.Server.MapPath("~/template/SalesDocs/ProposalContractContact.docx");//聯絡人
            string ProposalContractContentPpon = HttpContext.Current.Server.MapPath("~/template/SalesDocs/ProposalContractContentPpon.docx");//好康優惠內容
            string ProposalContractContentProductTravel = HttpContext.Current.Server.MapPath("~/template/SalesDocs/ProposalContractContentProductTravel.docx");//旅遊宅配優惠內容

            #region 列印一般合約範本
            if (!IsListCheck)
            {
                if (Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                {
                    if (type == DeliveryType.ToHouse && pro.DealType != (int)ProposalDealType.Piinlife && pro.DealType != (int)ProposalDealType.Travel)
                        wordFile = HttpContext.Current.Server.MapPath("~/template/SalesDocs/ProposalContractProduct.docx");
                    else
                        wordFile = HttpContext.Current.Server.MapPath("~/template/SalesDocs/ProposalContractEmpty.docx");

                }
                else
                {
                    //草稿
                    if (type == DeliveryType.ToHouse && pro.DealType != (int)ProposalDealType.Piinlife && pro.DealType != (int)ProposalDealType.Travel)
                        wordFile = HttpContext.Current.Server.MapPath("~/template/SalesDocs/ProposalContractProductDraft.docx");
                    else
                        wordFile = HttpContext.Current.Server.MapPath("~/template/SalesDocs/ProposalContractDraftEmpty.docx");
                }
            }
            #endregion

            #region 列印上架確認單範本
            if (IsListCheck)
            {
                switch (type)
                {
                    case DeliveryType.ToShop:
                        //憑證(包含旅遊)
                        wordFile = HttpContext.Current.Server.MapPath("~/template/SalesDocs/ProposalContractPponListCheck.docx");
                        break;
                    case DeliveryType.ToHouse:
                        if (pro.DealType == (int)ProposalDealType.Travel)
                        {
                            //旅遊宅配
                            wordFile = HttpContext.Current.Server.MapPath("~/template/SalesDocs/ProposalContractPponListCheck.docx");
                        }
                        else
                        {
                            //一般宅配
                            wordFile = HttpContext.Current.Server.MapPath("~/template/SalesDocs/ProposalContractProductListCheck.docx");
                        }
                        break;
                    default:
                        break;
                }
            }
            #endregion

            #endregion get template file

            fileName = fileName.Replace("{", "{{").Replace("}", "}}");
            // Load the document.
            using (Novacode.DocX document = Novacode.DocX.Load(wordFile))
            {


                if (!IsListCheck)
                {
                    if (type == DeliveryType.ToHouse && pro.DealType != (int)ProposalDealType.Piinlife && pro.DealType != (int)ProposalDealType.Travel)
                    {

                    }
                    else
                    {
                        #region 聯絡人資料
                        List<Seller.MultiContracts> contactsList = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(contacts);

                        document.InsertParagraph().Append("壹、聯絡人資料").Bold().FontSize(16);

                        if (contactsList != null)
                        {
                            foreach (var c in contactsList)
                            {
                                document.InsertDocument(Novacode.DocX.Load(ProposalContractContact));

                                document.ReplaceText("ContactType", Helper.GetDescription((ProposalContact)Convert.ToInt32(c.Type)));
                                document.ReplaceText("ContactPersonName", c.ContactPersonName);
                                document.ReplaceText("SellerMobile", c.SellerMobile);
                                document.ReplaceText("SellerTel", c.SellerTel);
                                document.ReplaceText("SellerFax", c.SellerFax);
                                document.ReplaceText("ContactPersonEmail", c.ContactPersonEmail);
                            }
                        }
                        else
                            document.InsertParagraph().Append("無");


                        #endregion


                        if (type == DeliveryType.ToShop)
                            document.InsertDocument(Novacode.DocX.Load(ProposalContractContentPpon));
                        else if (type == DeliveryType.ToHouse && (pro.DealType == (int)ProposalDealType.Piinlife || pro.DealType == (int)ProposalDealType.Travel))
                        {
                            document.InsertDocument(Novacode.DocX.Load(ProposalContractContentProductTravel));

                            if (type == DeliveryType.ToHouse && pro.DealType == (int)ProposalDealType.Piinlife)
                            {
                                //只有憑證和旅遊才要"活動有效使用時間"
                                List<Novacode.Table> tcount = document.Tables;
                                Novacode.Table table = document.Tables[tcount.Count() - 1];
                                Novacode.Row row = table.Rows[2];
                                row.Remove();


                            }

                        }

                    }

                    //以下是否要勾稽下拉選擇的分店!!
                    #region 營業據點
                    if (LocationstoreList.Count() > 0)
                    {
                        //標題
                        document.InsertParagraph().Append("參、營業據點資訊").Bold().FontSize(16).Append("(有勾選店鋪顯示的商家才會出現)").Color(System.Drawing.Color.Red).FontSize(8);

                        foreach (var l in LocationstoreList)
                        {
                            //建立表格
                            Novacode.Table LocationTable = document.AddTable(8, 1);
                            //表格寬度
                            LocationTable.AutoFit = Novacode.AutoFit.Window;
                            //表格border
                            Novacode.Border border = new Novacode.Border();
                            border.Tcbs = Novacode.BorderStyle.Tcbs_double;
                            LocationTable.SetBorder(Novacode.TableBorderType.Top, border);
                            LocationTable.SetBorder(Novacode.TableBorderType.Bottom, border);
                            LocationTable.SetBorder(Novacode.TableBorderType.Left, border);
                            LocationTable.SetBorder(Novacode.TableBorderType.Right, border);


                            //寫內容
                            string vehicleInfo = string.Empty;
                            vehicleInfo += !string.IsNullOrEmpty(l.Mrt) ? "●捷運：" + l.Mrt + "\n" : string.Empty;
                            vehicleInfo += !string.IsNullOrEmpty(l.Car) ? "●開車：" + l.Car + "\n" : string.Empty;
                            vehicleInfo += !string.IsNullOrEmpty(l.Bus) ? "●公車：" + l.Bus + "\n" : string.Empty;
                            vehicleInfo += !string.IsNullOrEmpty(l.OtherVehicles) ? "●其他：" + l.OtherVehicles : string.Empty;


                            string webUrl = string.Empty;
                            webUrl += !string.IsNullOrEmpty(l.WebUrl) ? "●官網：" + l.WebUrl + "\n" : string.Empty;
                            webUrl += !string.IsNullOrEmpty(l.FacebookUrl) ? "●FaceBook：" + l.FacebookUrl + "\n" : string.Empty;
                            webUrl += !string.IsNullOrEmpty(l.BlogUrl) ? "●部落格：" + l.BlogUrl + "\n" : string.Empty;
                            webUrl += !string.IsNullOrEmpty(l.OtherUrl) ? "●其他：" + l.OtherUrl : string.Empty;

                            LocationTable.Rows[0].Cells[0].Paragraphs.First().Append(l.SellerName);
                            LocationTable.Rows[1].Cells[0].Paragraphs.First().Append(l.StoreTownshipId.HasValue ? "地址：" + CityManager.CityTownShopStringGet(l.StoreTownshipId.Value) + l.StoreAddress : string.Empty);
                            LocationTable.Rows[2].Cells[0].Paragraphs.First().Append("營業電話：" + l.StoreTel);
                            LocationTable.Rows[3].Cells[0].Paragraphs.First().Append("交通資訊：" + "\n" + vehicleInfo.TrimEnd("\n"));
                            LocationTable.Rows[4].Cells[0].Paragraphs.First().Append("相關網站/網址：" + "\n" + webUrl.TrimEnd("\n"));
                            LocationTable.Rows[5].Cells[0].Paragraphs.First().Append("營業時間：" + l.OpenTime ?? string.Empty);
                            LocationTable.Rows[6].Cells[0].Paragraphs.First().Append("公休日：" + l.CloseDate ?? string.Empty);
                            LocationTable.Rows[7].Cells[0].Paragraphs.First().Append("備註：" + l.StoreRemark);

                            //塞入表格
                            document.InsertTable(LocationTable);
                            document.InsertParagraph().Append("");
                        }

                    }
                    #endregion

                    #region 帳務資料
                    if (AccountingstoreList.Count() > 0)
                    {
                        //標題
                        if (LocationstoreList.Count() > 0)
                            document.InsertParagraph().Append("肆、帳務資料").Bold().FontSize(16).Append("(匯款對象)").Color(System.Drawing.Color.Red).FontSize(8);
                        else
                            document.InsertParagraph().Append("參、帳務資料").Bold().FontSize(16).Append("(匯款對象)").Color(System.Drawing.Color.Red).FontSize(8);

                        document.InsertParagraph().Append("●各店匯款 / 各店簽約應確實簽定授權書或正式合約，本文件帳務資料相關欄位僅供檢視之用，請勿在此直接修改或通知，若有異動，請依正式流程申請變更").FontSize(8);
                        document.InsertParagraph().Append("●匯款銀行戶名需與簽約公司名稱相同，若由個人與我們簽約請提供簽約人的帳戶").FontSize(8);



                        foreach (var v in AccountingstoreList)
                        {
                            #region 可提供憑證
                            string invoice = string.Empty;
                            if (v.VendorReceiptType != null)
                            {
                                switch ((VendorReceiptType)v.VendorReceiptType)
                                {
                                    case VendorReceiptType.Other:
                                        invoice = v.Othermessage;
                                        break;
                                    case VendorReceiptType.Receipt:
                                    case VendorReceiptType.Invoice:
                                    case VendorReceiptType.NoTaxInvoice:
                                        invoice = Helper.GetLocalizedEnum((VendorReceiptType)v.VendorReceiptType);
                                        break;
                                    default:
                                        break;
                                }
                            }
                            #endregion

                            //建立表格
                            Novacode.Table AccountingTable = document.AddTable(11, 2);
                            //表格寬度
                            AccountingTable.AutoFit = Novacode.AutoFit.Window;
                            //表格border
                            Novacode.Border border = new Novacode.Border();
                            border.Tcbs = Novacode.BorderStyle.Tcbs_double;
                            AccountingTable.SetBorder(Novacode.TableBorderType.Top, border);
                            AccountingTable.SetBorder(Novacode.TableBorderType.Bottom, border);
                            AccountingTable.SetBorder(Novacode.TableBorderType.Left, border);
                            AccountingTable.SetBorder(Novacode.TableBorderType.Right, border);
                            AccountingTable.SetBorder(Novacode.TableBorderType.InsideV, border);//內側

                            //寫內容
                            Novacode.Row row = AccountingTable.Rows[0];
                            row.MergeCells(0, 1);
                            row.Cells[0].Paragraphs.First().Append(v.SellerName);
                            if (row.Cells[0].Paragraphs.Count > 1)
                                row.Cells[0].Paragraphs.Last().Remove(false);//合併後會變兩個段落
                            row = AccountingTable.Rows[1];
                            row.MergeCells(0, 1);
                            row.Cells[0].Paragraphs.First().Append("簽約公司名稱：" + v.CompanyName);
                            if (row.Cells[0].Paragraphs.Count > 1)
                                row.Cells[0].Paragraphs.Last().Remove(false);
                            row = AccountingTable.Rows[2];
                            row.MergeCells(0, 1);
                            row.Cells[0].Paragraphs.First().Append("簽約公司負責人：" + v.CompanyBossName);
                            if (row.Cells[0].Paragraphs.Count > 1)
                                row.Cells[0].Paragraphs.Last().Remove(false);
                            row = AccountingTable.Rows[3];
                            row.MergeCells(0, 1);
                            row.Cells[0].Paragraphs.First().Append("簽約公司地址：" + CityManager.CityTownShopStringGet(v.CityId) + v.SellerAddress);
                            if (row.Cells[0].Paragraphs.Count > 1)
                                row.Cells[0].Paragraphs.Last().Remove(false);
                            row = AccountingTable.Rows[4];
                            row.Cells[0].Paragraphs.First().Append("銀行代碼：" + v.CompanyBankCode);
                            row.Cells[1].Paragraphs.First().Append("分行代碼：" + v.CompanyBranchCode);
                            row = AccountingTable.Rows[5];
                            row.Cells[0].Paragraphs.First().Append("受款人ID：" + v.CompanyID);
                            row.Cells[1].Paragraphs.First().Append("帳號：" + v.CompanyAccount);
                            row = AccountingTable.Rows[6];
                            row.Cells[0].Paragraphs.First().Append("戶名：" + v.CompanyAccountName);
                            row.Cells[1].Paragraphs.First().Append("出帳方式：").Append(v.RemittanceType == null ? "" : Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (RemittanceType)v.RemittanceType));
                            row = AccountingTable.Rows[7];
                            row.MergeCells(0, 1);
                            row.Cells[0].Paragraphs.First().Append("可提供憑證：" + invoice);
                            if (row.Cells[0].Paragraphs.Count > 1)
                                row.Cells[0].Paragraphs.Last().Remove(false);
                            row = AccountingTable.Rows[8];
                            row.MergeCells(0, 1);
                            row.Cells[0].Paragraphs.First().Append("【帳務聯絡人資料】");
                            row.Cells[0].Paragraphs.First().Alignment = Novacode.Alignment.center;
                            if (row.Cells[0].Paragraphs.Count > 1)
                                row.Cells[0].Paragraphs.Last().Remove(false);
                            row = AccountingTable.Rows[9];
                            row.Cells[0].Paragraphs.First().Append("帳務聯絡人：" + v.AccountantName);
                            row.Cells[1].Paragraphs.First().Append("帳務聯絡人電話：" + v.AccountantTel);
                            row = AccountingTable.Rows[10];
                            row.MergeCells(0, 1);
                            row.Cells[0].Paragraphs.First().Append("帳務聯絡人電子信箱：").Append(v.CompanyEmail == null ? "" : v.CompanyEmail);
                            if (row.Cells[0].Paragraphs.Count > 1)
                                row.Cells[0].Paragraphs.Last().Remove(false);



                            //有發票開立人才加入顯示
                            Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(pro.SpecialFlagText);
                            if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.InvoiceFounder) && specialText.ContainsKey(ProposalSpecialFlag.InvoiceFounder))
                            {

                                AccountingTable.InsertRow(8);
                                row = AccountingTable.Rows[8];
                                row.MergeCells(0, 1);
                                row.Cells[0].Paragraphs.First().Append("發票開立人：" + specialText[ProposalSpecialFlag.InvoiceFounder]);

                                AccountingTable.Rows.Add(row);
                            }


                            //塞入表格
                            document.InsertTable(AccountingTable);
                            document.InsertParagraph().Append("");
                        }
                    }
                    #endregion


                    if (Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                    {
                        //草稿階段不列印「用印」
                        document.InsertDocument(Novacode.DocX.Load(footer));
                    }
                }
                else
                {
                    fileName = fileName + "_上架確認單";
                }

                // Replace text in this document.
                foreach (KeyValuePair<string, string> item in dataList)
                {
                    document.ReplaceText(item.Key, !string.IsNullOrEmpty(item.Value) ? item.Value : string.Empty);
                }





                // Save changes made to this document.
                MemoryStream memoryStream = new MemoryStream();
                document.SaveAs(memoryStream);

                // print word
                Response.ContentType = "application/msword";
                Response.AddHeader("content-disposition", "attachment; filename=\"" + string.Format(fileName + "_{0}", DateTime.Now.ToString("yyyy_MM_dd")) + ".doc\"");
                memoryStream.WriteTo(Response.OutputStream);
                Response.End();
            } // Release this document from memory.
        }

        public static bool MailToPhotographerForCancel(string UserName, string PhotographerAppointer, int ProposalId)
        {
            return ProposalFacade.MailToPhotographerForCancel(UserName, PhotographerAppointer, ProposalId);
        }

        #endregion

        #region WS
        [WebMethod]
        public static dynamic GetDealSubType(string id)
        {
            Dictionary<int, string> rtn = new Dictionary<int, string>();
            ProposalDealType type = ProposalDealType.TryParse(id, out type) ? type : ProposalDealType.None;
            if (type != ProposalDealType.None)
            {
                switch (type)
                {
                    case ProposalDealType.LocalDeal:
                        rtn.Add((int)ProposalDealSubType.DealSet, Helper.GetLocalizedEnum(ProposalDealSubType.DealSet));
                        rtn.Add((int)ProposalDealSubType.Offsetting, Helper.GetLocalizedEnum(ProposalDealSubType.Offsetting));
                        rtn.Add((int)ProposalDealSubType.Propoduct, Helper.GetLocalizedEnum(ProposalDealSubType.Propoduct));
                        rtn.Add((int)ProposalDealSubType.EatToDie, Helper.GetLocalizedEnum(ProposalDealSubType.EatToDie));
                        rtn.Add((int)ProposalDealSubType.Tickets, Helper.GetLocalizedEnum(ProposalDealSubType.Tickets));
                        rtn.Add((int)ProposalDealSubType.Others, Helper.GetLocalizedEnum(ProposalDealSubType.Others));
                        break;
                    case ProposalDealType.PBeauty:
                        rtn.Add((int)ProposalDealSubType.SPA, Helper.GetLocalizedEnum(ProposalDealSubType.SPA));
                        rtn.Add((int)ProposalDealSubType.HairSalon, Helper.GetLocalizedEnum(ProposalDealSubType.HairSalon));
                        rtn.Add((int)ProposalDealSubType.NailSalon, Helper.GetLocalizedEnum(ProposalDealSubType.NailSalon));
                        rtn.Add((int)ProposalDealSubType.Massage, Helper.GetLocalizedEnum(ProposalDealSubType.Massage));
                        rtn.Add((int)ProposalDealSubType.Others, Helper.GetLocalizedEnum(ProposalDealSubType.Others));
                        break;
                    case ProposalDealType.Piinlife:
                        rtn.Add((int)ProposalDealSubType.DealSet, Helper.GetLocalizedEnum(ProposalDealSubType.DealSet));
                        rtn.Add((int)ProposalDealSubType.Offsetting, Helper.GetLocalizedEnum(ProposalDealSubType.Offsetting));
                        rtn.Add((int)ProposalDealSubType.Propoduct, Helper.GetLocalizedEnum(ProposalDealSubType.Propoduct));
                        rtn.Add((int)ProposalDealSubType.EatToDie, Helper.GetLocalizedEnum(ProposalDealSubType.EatToDie));
                        rtn.Add((int)ProposalDealSubType.Course, Helper.GetLocalizedEnum(ProposalDealSubType.Course));
                        rtn.Add((int)ProposalDealSubType.SPA, Helper.GetLocalizedEnum(ProposalDealSubType.SPA));
                        rtn.Add((int)ProposalDealSubType.HairSalon, Helper.GetLocalizedEnum(ProposalDealSubType.HairSalon));
                        rtn.Add((int)ProposalDealSubType.NailSalon, Helper.GetLocalizedEnum(ProposalDealSubType.NailSalon));
                        rtn.Add((int)ProposalDealSubType.Massage, Helper.GetLocalizedEnum(ProposalDealSubType.Massage));
                        rtn.Add((int)ProposalDealSubType.Others, Helper.GetLocalizedEnum(ProposalDealSubType.Others));
                        break;
                    case ProposalDealType.Travel:
                        rtn.Add((int)ProposalDealSubType.Hotel, Helper.GetLocalizedEnum(ProposalDealSubType.Hotel));
                        rtn.Add((int)ProposalDealSubType.BedAndBreakfast, Helper.GetLocalizedEnum(ProposalDealSubType.BedAndBreakfast));
                        rtn.Add((int)ProposalDealSubType.HotelRest, Helper.GetLocalizedEnum(ProposalDealSubType.HotelRest));
                        rtn.Add((int)ProposalDealSubType.Motel, Helper.GetLocalizedEnum(ProposalDealSubType.Motel));
                        rtn.Add((int)ProposalDealSubType.Travel, Helper.GetLocalizedEnum(ProposalDealSubType.Travel));
                        rtn.Add((int)ProposalDealSubType.Tickets, Helper.GetLocalizedEnum(ProposalDealSubType.Tickets));
                        rtn.Add((int)ProposalDealSubType.HotSpring, Helper.GetLocalizedEnum(ProposalDealSubType.HotSpring));
                        rtn.Add((int)ProposalDealSubType.Others, Helper.GetLocalizedEnum(ProposalDealSubType.Others));
                        break;
                    case ProposalDealType.Product:
                        rtn.Add((int)ProposalDealSubType.Food, Helper.GetLocalizedEnum(ProposalDealSubType.Food));
                        rtn.Add((int)ProposalDealSubType.Living, Helper.GetLocalizedEnum(ProposalDealSubType.Living));
                        rtn.Add((int)ProposalDealSubType.Furniture, Helper.GetLocalizedEnum(ProposalDealSubType.Furniture));
                        rtn.Add((int)ProposalDealSubType.Popular, Helper.GetLocalizedEnum(ProposalDealSubType.Popular));
                        rtn.Add((int)ProposalDealSubType.ConsumerElectronic, Helper.GetLocalizedEnum(ProposalDealSubType.ConsumerElectronic));
                        rtn.Add((int)ProposalDealSubType.HomeAppliances, Helper.GetLocalizedEnum(ProposalDealSubType.HomeAppliances));
                        rtn.Add((int)ProposalDealSubType.Beauty, Helper.GetLocalizedEnum(ProposalDealSubType.Beauty));
                        rtn.Add((int)ProposalDealSubType.Child, Helper.GetLocalizedEnum(ProposalDealSubType.Child));
                        break;
                    case ProposalDealType.None:
                    default:
                        break;
                }
            }
            return rtn.ToArray();
        }

        /// <summary>
        /// 提案單優惠內容儲存
        /// </summary>
        /// <param name="ProposalId"></param>
        /// <param name="UserName"></param>
        /// <param name="data"></param>
        /// <param name="isDelete"></param>
        /// <param name="isCopy"></param>
        /// <param name="itemName"></param>
        /// <param name="itemPrice"></param>
        /// <returns></returns>
        [WebMethod]
        public static ProposalMultiDealCollection ProposalMiltiDealsSave(int ProposalId, string UserName, string data, bool isDelete, bool isCopy, string itemName, string itemPrice)
        {
            ProposalMultiDealModel model = new JsonSerializer().Deserialize<ProposalMultiDealModel>(data);
            ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealSave(model, ProposalId, UserName, isDelete, isCopy, itemName, itemPrice);
            return multiDeals;
        }

        /// <summary>
        /// 提案單調整順序
        /// </summary>
        /// <param name="ProposalId"></param>
        /// <param name="UserName"></param>
        /// <param name="sortData"></param>
        /// <returns></returns>
        [WebMethod]
        public static bool ProposalMiltiDealsSort(int ProposalId, string UserName, string sortData)
        {
            ProposalFacade.ProposalMultiDealSort(ProposalId, UserName, sortData);
            return true;
        }

        /// <summary>
        /// 取得優惠方案資料
        /// </summary>
        /// <param name="ProposalId"></param>
        /// <returns></returns>
        [WebMethod]
        public static ProposalMultiDealCollection ProposalMiltiDealsGet(int ProposalId)
        {
            ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealGetByPid(ProposalId);
            if (multiDeals.Count() > 0)
            {
                return multiDeals;
            }
            else
            {
                return null;
            }
        }

        //[WebMethod]
        //public static IEnumerable<ProposalMultiDealModel> ProposalMiltiDealsGet(int ProposalId)
        //{
        //    Proposal proposals = ProposalFacade.ProposalGet(ProposalId);
        //    List<ProposalMultiDealModel> model = new JsonSerializer().Deserialize<List<ProposalMultiDealModel>>(proposals.MultiDeals);
        //    if (model != null)
        //    {
        //        return model.OrderBy(x => x.Sort).ThenBy(x => x.ItemPrice);
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}
        [WebMethod]
        public static List<ProposalSellerFile> ProposalSellerFilesDelete(int id, string UserName)
        {
            List<ProposalSellerFile> fileList = ProposalFacade.ProposalSellerFileDelete(id, UserName, ProposalLogType.Initial);

            return fileList;
        }


        [WebMethod]
        public static string ProposalSpecialTextGet(int ProposalId)
        {
            Proposal pro = ProposalFacade.ProposalGet(ProposalId);
            string photoString = "";
            if (Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.Photographers))
            {
                Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(pro.SpecialFlagText);
                if (specialText != null && specialText.Count > 0)
                {
                    photoString = specialText[(ProposalSpecialFlag)ProposalSpecialFlag.Photographers];
                }
            }
            return photoString;
        }
        [WebMethod]
        public static string ProposalSpecialAssignTextGet(int ProposalId)
        {
            Proposal pro = ProposalFacade.ProposalGet(ProposalId);
            string photoString = "";
            if (Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.Photographers))
            {
                Dictionary<ProposalPhotographer, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalPhotographer, string>>(pro.SpecialAppointFlagText);
                if (specialText != null && specialText.Count > 0)
                {
                    photoString = specialText[(ProposalPhotographer)ProposalPhotographer.PhotographerAppointCheck];
                }
            }
            return photoString;
        }

        [WebMethod]
        public static string ProposalSpecialAppointTextGet(int ProposalId)
        {
            Proposal pro = ProposalFacade.ProposalGet(ProposalId);
            string photoString = "";
            if (Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.Photographers))
            {
                Dictionary<ProposalPhotographer, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalPhotographer, string>>(pro.SpecialAppointFlagText);
                if (specialText != null && specialText.Count > 0)
                {
                    photoString = specialText[(ProposalPhotographer)ProposalPhotographer.PhotographerAppointCheck];
                }
            }
            return photoString;
        }

        [WebMethod]
        public static bool photoAppointSave(int proposalId, string photographerAppointDate, string photographerAppointHour, string photographerAppointMinute,
            string oriPhotographerAppointer, string photographerAppointer, string photographerAppointMemo, string photographerAppointCancel, string userName)
        {
            //約定拍攝相關資料儲存

            Proposal pro = ProposalFacade.ProposalGet(proposalId);
            Dictionary<ProposalPhotographer, string> specialText = new Dictionary<ProposalPhotographer, string>();
            List<string> photoString = new List<string>();
            photoString.Add(photographerAppointDate);
            photoString.Add(photographerAppointHour);
            photoString.Add(photographerAppointMinute);
            photoString.Add(photographerAppointer);
            photoString.Add(photographerAppointMemo);
            photoString.Add(photographerAppointCancel);
            specialText.Add(ProposalPhotographer.PhotographerAppointCheck, string.Join("|", photoString));

            pro.SpecialAppointFlagText = new JsonSerializer().Serialize(specialText);
            bool cancel = false;
            bool.TryParse(photographerAppointCancel, out cancel);


            if (!Helper.IsFlagSet(pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppointCancel) && cancel)
            {
                ProposalFacade.ProposalLog(proposalId, "變更 攝影狀態 為 " + Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalPhotographer.PhotographerAppointCancel), userName);
            }

            pro.PhotographerAppointFlag = Convert.ToInt32(Helper.SetFlag(cancel, pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppointCancel));



            bool flag = ProposalFacade.ProposalPhotoSet(pro, userName);

            //取消約拍時，發送取消通知信
            if (photographerAppointCancel == "true")
            {
                MailToPhotographerForCancel(userName, oriPhotographerAppointer, proposalId);
            }

            return flag;

        }
        [WebMethod]
        public static bool photoSave(int ProposalId, string deliveryType, string dealType, string PhotoAddress, string PhotoContact, string Sex, string PhotoTel,
            string PhotoPhone, string PhotoMemo, string PhotoDate, string PhotoUrl, string PhohoNeeds, string RefrenceData, string UserName, bool onlySaveSate)
        {
            //基本資料儲存/我要約拍
            Proposal pro = ProposalFacade.ProposalGet(ProposalId);

            Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(pro.SpecialFlagText) ??
                                                                  new Dictionary<ProposalSpecialFlag, string>();

            specialText.Remove(ProposalSpecialFlag.Photographers);

            if (deliveryType == Convert.ToString((int)DeliveryType.ToShop) || (deliveryType == Convert.ToString((int)DeliveryType.ToHouse) && dealType == Convert.ToString((int)ProposalDealType.Travel)))
            {
                //憑證+旅遊宅配
                List<string> photoString = new List<string>();
                photoString.Add(PhotoAddress);
                photoString.Add(PhotoTel);
                photoString.Add(PhotoPhone);
                photoString.Add(PhotoMemo);
                photoString.Add(PhotoDate);
                photoString.Add(PhotoUrl);
                photoString.Add(PhotoContact);
                photoString.Add(Sex);
                specialText.Add(ProposalSpecialFlag.Photographers, string.Join("|", photoString));
            }
            else
            {
                //宅配
                List<string> photoString = new List<string>();
                photoString.Add(PhohoNeeds);
                photoString.Add(RefrenceData);
                specialText.Add(ProposalSpecialFlag.Photographers, string.Join("|", photoString));
            }

            pro.SpecialFlagText = new JsonSerializer().Serialize(specialText);

            //只儲存時，不改變攝影狀態
            if (!onlySaveSate)
            {
                if (!Helper.IsFlagSet(pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppoint))
                {
                    ProposalFacade.ProposalLog(ProposalId, "變更 攝影狀態 為 " + Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalPhotographer.PhotographerAppoint), UserName);
                    pro.PhotographerAppointFlag = Convert.ToInt32(Helper.SetFlag(true, pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppoint));
                }
            }

            bool flag = ProposalFacade.ProposalPhotoSet(pro, UserName);

            return flag;
        }

        [WebMethod]
        public static List<AjaxResponseResult> GetPhotographerNameArray(string empName)
        {
            List<AjaxResponseResult> res = new List<AjaxResponseResult>();
            if (!string.IsNullOrEmpty(empName) && empName.Length > 2)
            {
                List<string> emps = ProposalFacade.GetPhotographerNameArray(empName);
                foreach (string e in emps)
                {
                    res.Add(new AjaxResponseResult
                    {
                        Value = e,
                        Label = e
                    });
                }
            }

            return res;
        }

        [WebMethod]
        public static bool MailToPhotographer(string UserName, string PhotographerName, int ProposalId)
        {
            //通知攝影師
            return ProposalFacade.MailToPhotographer(UserName, PhotographerName, ProposalId);
        }



        [WebMethod]
        public static bool CheckIsEmployee(string userName)
        {
            return ProposalFacade.CheckIsEmployee(userName);
        }

        [WebMethod]
        public static string BindRestrictions(int pid)
        {
            int _pid = 0;
            int.TryParse(pid.ToString(), out _pid);
            ProposalCouponEventContent pcec = ProposalFacade.RestrictionsGet(_pid);

            return new JsonSerializer().Serialize(pcec);
        }

        [WebMethod]
        public static bool DeleteContractFile(string guid, int proposalid, int contracttype)
        {
            ProposalContractFileCollection pcf = ProposalFacade.ProposalContractFileGetByType(proposalid, (int)SellerContractType.Pair);
            if (pcf.Count == 1 && contracttype == (int)SellerContractType.Pair)
            {
                //合約已回及合約檢核取消
                Proposal pro = ProposalFacade.ProposalGet(proposalid);
                pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(false, pro.ListingFlag, ProposalListingFlag.PaperContractCheck));
                pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(false, pro.ListingFlag, ProposalListingFlag.PaperContractInspect));
                ProposalFacade.ProposalSet(pro);
            }

            //刪除合約
            Guid _guid = Guid.Empty;
            Guid.TryParse(guid, out _guid);
            ProposalFacade.DeleteContractFile(_guid);

            return true;
        }

        [WebMethod]
        public static bool PaperContractNotCheckReasonSave(int proposalid, string username, string Reason)
        {
            Proposal pro = ProposalFacade.ProposalGet(proposalid);
            pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(true, pro.ListingFlag, ProposalListingFlag.PaperContractNotCheck));
            ProposalFacade.ProposalSet(pro);
            ProposalFacade.ProposalLog(proposalid, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalListingFlag.PaperContractNotCheck) + " ： " + Reason, username);
            return true;
        }

        [WebMethod]
        public static bool ProposalDraw(int ProposalId, string ProposalDrawReason, string UserName)
        {
            return ProposalFacade.ProposalDraw(ProposalId, ProposalDrawReason, UserName);
        }


        [WebMethod]
        public static bool SetSellerSort(int proposalId, string[] guids)
        {
            IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            Guid stroeGuid;
            for (int i = 0; i < guids.Length; i++)
            {
                if (!Guid.TryParse(guids[i], out stroeGuid))
                {
                    continue;
                }

                var proposalStore = pp.ProposalStoreGet(proposalId, stroeGuid);
                if (proposalStore.IsLoaded)
                {
                    proposalStore.SortOrder = i;
                    pp.ProposalStoreSet(proposalStore);
                }
            }

            return true;
        }
        #endregion
    }
}
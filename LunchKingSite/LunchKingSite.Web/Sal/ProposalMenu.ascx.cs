﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Sal
{
    public partial class ProposalMenu : System.Web.UI.UserControl
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        #region props
        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }
        public string CurrentUrl
        {
            get
            {
                return HttpContext.Current.Request.FilePath.ToLower();
            }
        }
        public string SellerList
        {
            get
            {
                return CurrentUrl.IndexOf("sellerlist") >= 0 ? "on" : "";
            }
        }
        public string ProposalList
        {
            get
            {
                return CurrentUrl.IndexOf("proposallist") >= 0 ? "on" : "";
            }
        }
        public string BusinessList
        {
            get
            {
                return CurrentUrl.IndexOf("businesslist") >= 0 ? "on" : "";
            }
        }
        public string ProposalProgress
        {
            get
            {
                return CurrentUrl.IndexOf("proposalprogress") >= 0 ? "on" : "";
            }
        }
        public string PponDealCalendar
        {
            get
            {
                return CurrentUrl.IndexOf("ppondealcalendar") >= 0 ? "on" : "";
            }
        }
        public string BackendRecentDeals
        {
            get
            {
                return CurrentUrl.IndexOf("backendrecentdeals") >= 0 ? "on" : "";
            }
        }
        public string PponOptionContnet
        {
            get
            {
                return CurrentUrl.IndexOf("pponoptioncontnet") >= 0 ? "on" : "";
            }
        }
        public string SalesCalendar
        {
            get
            {
                return CurrentUrl.IndexOf("salescalendar") >= 0 ? "on" : "";
            }
        }
        public string ShoppingCartManage
        {
            get
            {
                return CurrentUrl.IndexOf("shoppingcartmanage") >= 0 ? "on" : "";
            }
        }
        public string StoreQrCodeManager
        {
            get
            {
                return CurrentUrl.IndexOf("storeqrcodemanage") >= 0 ? "on" : "";
            }
        }
        public string ISPManage
        {
            get
            {
                return CurrentUrl.IndexOf("ispmanage") >= 0 ? "on" : "";
            }
        }
        public string WmsManage
        {
            get
            {
                return CurrentUrl.IndexOf("wmsmanage") >= 0 ? "on" : "";
            }
        }

        public string WmsReturn
        {
            get
            {
                return CurrentUrl.IndexOf("wmsreturn") >= 0 ? "on" : "";
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EnabledControls();
            }
        }

        private void EnabledControls()
        {
            List<string> userSCList = FunctionPrivilegeManager.GetPrivilegesByFunctionType("/Sal/ShoppingCartManage.aspx", SystemFunctionType.Read);
            if (config.ShoppingCartEnabled)
            {
                if (userSCList.Contains(UserName))
                {
                    phdShoppingCartManage.Visible = true;
                }
            }
        }
    }
}
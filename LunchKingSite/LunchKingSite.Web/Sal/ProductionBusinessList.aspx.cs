﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.SellerSales;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Sal
{
    public partial class ProductionBusinessList : RolePage, ISalesProductionBusinessListView
    {
        public static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        #region prop
        private SalesProductionBusinessListPresenter _presenter;
        public SalesProductionBusinessListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public int CurrentPage
        {
            get
            {
                return ucPager.CurrentPage;
            }
        }

        public int PageSize
        {
            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }
        }

        public string City
        {
            get
            {
                return hidCheckCity.Value;
            }
        }

        public DateTime DateStart
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtDateStart.Text, out date);
                return date;
            }
        }

        public DateTime DateEnd
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtDateEnd.Text, out date);
                return date;
            }
        }

        public int DateType
        {
            get
            {
                int datetype;
                Int32.TryParse(rbDate.SelectedValue, out datetype);
                return datetype;
            }
        }

        public int StatusType
        {
            get
            {
                int datetype;
                Int32.TryParse(rbAssign.SelectedValue, out datetype);
                return datetype;
            }
        }

        public int Status
        {
            get
            {
                int status;
                Int32.TryParse(hidStatus.Value, out status);
                return status;
            }
        }

        public string OrderField
        {
            get
            {
                //ViewPponDeal view = new ViewPponDeal();   
                return hdOrderField.Value;
            }
        }
        public string OrderBy
        {
            get
            {
                string _order = hdOrderBy.Value;

                return _order;
            }
        }

        public bool ChkProduction
        {
            get
            {
                return chkProduction.Checked;
            }
        }

        public string Emp
        {
            get
            {
                if (!string.IsNullOrEmpty(txtEmployee.Text))
                {
                    if (!string.IsNullOrEmpty(UserName))
                    {
                        ViewEmployee ve = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, txtEmployee.Text + UserName.Substring(UserName.IndexOf('@')));

                        if (ve.IsLoaded)
                            return ve.EmpName;
                        else
                            return "";
                    }
                    else
                        return "";
                }
                else
                {
                    return null;
                }

            }
        }

        public int DealProperty
        {
            get
            {
                int datetype;
                Int32.TryParse(rbDealProperty.SelectedValue, out datetype);
                return datetype;
            }
        }

        public int PicType
        {
            get
            {
                int pic;
                Int32.TryParse(hidPic.Value, out pic);
                return pic;
            }
        }

        public string AssignPro
        {
            get
            {
                return hidAssignPro.Value;
            }
        }

        public string AssignFlag
        {
            get
            {
                return hidAssignFlag.Value;
            }
        }

        public int DealType
        {
            get
            {
                int flag = int.TryParse(hidDealType.Value, out flag) ? flag : 0;
                return flag;
            }
        }
        public int DealSubType
        {
            get
            {
                int flag = int.TryParse(hidDealSubType.Value, out flag) ? flag : 0;
                return flag;
            }
        }

        public bool MarketingResource
        {
            get
            {
                return chkMarketingResource.Checked;
            }
        }



        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IntialControls();
                Presenter.OnViewInitialized();
                EnabledSettings(UserName);
            }
            _presenter.OnViewLoaded();
        }


        #region event
        public event EventHandler Search;
        public event EventHandler<DataEventArgs<int>> OnGetCount;
        public event EventHandler<DataEventArgs<int>> PageChanged;
        #endregion

        #region page

        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChanged != null)
            {
                this.PageChanged(this, new DataEventArgs<int>(pageNumber));
            }
        }

        protected int GetCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetCount != null)
            {
                OnGetCount(this, e);
            }
            return e.Data;
        }

        protected void rptChannel_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;

                // 頻道
                CheckBox chkChannel = (CheckBox)e.Item.FindControl("chkChannel");
                HiddenField hidChannel = (HiddenField)e.Item.FindControl("hidChannel");
                chkChannel.Text = dataItem.CategoryName;
                hidChannel.Value = dataItem.CategoryId.ToString();

                // 區域
                Repeater rptArea = (Repeater)e.Item.FindControl("rptArea");
                List<CategoryTypeNode> area = dataItem.NodeDatas.Where(x => x.NodeType == CategoryType.PponChannelArea).ToList();
                if (area.Count > 0)
                {
                    CategoryTypeNode areaNode = area.First();
                    rptArea.DataSource = areaNode.CategoryNodes;
                    rptArea.DataBind();
                }
            }
        }

        protected void rptArea_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                CheckBox chkArea = (CheckBox)e.Item.FindControl("chkArea");
                HiddenField hidArea = (HiddenField)e.Item.FindControl("hidArea");
                chkArea.Text = dataItem.CategoryName;
                hidArea.Value = dataItem.CategoryId.ToString();

            }
        }

        protected void rptBusiness_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<ViewProposalSeller, ProposalAssignLogCollection>)
            {
                KeyValuePair<ViewProposalSeller, ProposalAssignLogCollection> dataItem = (KeyValuePair<ViewProposalSeller, ProposalAssignLogCollection>)e.Item.DataItem;
                //DealAccounting da = pp.DealAccountingGet((Guid)dataItem.Key.BusinessHourGuid);

                ViewProposalSeller pro = dataItem.Key;
                ProposalAssignLogCollection logs = dataItem.Value;
                HyperLink hkProposalId = (HyperLink)e.Item.FindControl("hkProposalId");
                PlaceHolder divFirstDeal = (PlaceHolder)e.Item.FindControl("divFirstDeal");
                Literal liFirstDeal = (Literal)e.Item.FindControl("liFirstDeal");
                Literal liSellerLevel = (Literal)e.Item.FindControl("liSellerLevel");
                HyperLink hkSellerName = (HyperLink)e.Item.FindControl("hkSellerName");
                Label lblDeliveryType = (Label)e.Item.FindControl("lblDeliveryType");
                Literal liDealType = (Literal)e.Item.FindControl("liDealType");
                Literal liDealSubType = (Literal)e.Item.FindControl("liDealSubType");
                Literal liProposalContent = (Literal)e.Item.FindControl("liProposalContent");
                Literal liMarketAnalysis = (Literal)e.Item.FindControl("liMarketAnalysis");
                Repeater rptSpecialFlag = (Repeater)e.Item.FindControl("rptSpecialFlag");
                Literal liOrderTimeS = (Literal)e.Item.FindControl("liOrderTimeS");
                Literal liSalesName = (Literal)e.Item.FindControl("liSalesName");
                Repeater rptAssign = (Repeater)e.Item.FindControl("rptAssign");

                Image imgFlagPhoto = (Image)e.Item.FindControl("imgFlagPhoto");
                Literal liAssignPhoto = (Literal)e.Item.FindControl("liAssignPhoto");

                //單號
                if (pro.ProposalSourceType == (int)ProposalSourceType.Original)
                {
                    hkProposalId.NavigateUrl = ResolveUrl("~/sal/ProposalContent.aspx?pid=" + pro.Id);
                    hkProposalId.Text = pro.Id.ToString();
                }
                else
                {
                    hkProposalId.NavigateUrl = ResolveUrl("~/sal/proposal/house/proposalcontent?pid=" + pro.Id);
                    hkProposalId.Text = pro.Id.ToString();
                }
                
                

                //規模
                liSellerLevel.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, pro.SellerLevel.HasValue ? (SellerLevel)pro.SellerLevel : SellerLevel.None).Split('：')[0];

                //類型
                lblDeliveryType.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (DeliveryType)pro.DeliveryType);

                switch ((DeliveryType)pro.DeliveryType)
                {
                    case DeliveryType.ToShop:
                        lblDeliveryType.BackColor = System.Drawing.Color.Pink;
                        break;
                    case DeliveryType.ToHouse:
                        lblDeliveryType.BackColor = System.Drawing.Color.Orange;
                        break;
                    default:
                        break;
                }
                liDealType.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDealType)pro.DealType);
                //liDealSubType.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDealSubType)pro.DealSubType);
                if (pro.ProposalSourceType == (int)ProposalSourceType.House)
                {
                    List<SystemCode> dealType = BizLogic.Component.SystemCodeManager.GetDealType().Where(x => x.ParentCodeId == null && x.CodeId != 1999).ToList();
                    var typeName = dealType.Where(x => x.CodeId == pro.DealType1).FirstOrDefault();
                    if (typeName != null)
                    {
                        liDealSubType.Text = typeName.CodeName;
                    }
                    else
                    {
                        liDealSubType.Text = string.Empty;
                    }
                }
                else
                {
                    liDealSubType.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDealSubType)pro.DealSubType);
                }

                //商家名稱
                if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.FirstDeal))
                {
                    divFirstDeal.Visible = true;
                    liFirstDeal.Text = Helper.GetLocalizedEnum(ProposalSpecialFlag.FirstDeal);
                }
                hkSellerName.NavigateUrl = ResolveUrl("~/sal/SellerContent.aspx?sid=" + pro.SellerGuid);
                if (pro.SellerGuid == Guid.Empty)
                    hkSellerName.Text = "";
                else
                    hkSellerName.Text = pro.SellerName;

                //提案內容
                ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealGetByPid(pro.Id);


                string NoSellerGuid = "";
                if (pro.SellerGuid == Guid.Empty)
                    NoSellerGuid = "<提案諮詢>";

                if (multiDeals != null && multiDeals.Count > 0)
                {
                    List<string> content = new List<string>();
                    if (multiDeals.Count > 1)
                    {
                        //多檔次只取母檔
                        var _filtermulti = multiDeals.OrderBy(x => x.ItemPrice);


                        foreach (var item in _filtermulti)
                        {

                            string name = NoSellerGuid + string.Format("{0}折！只要{1}元，即可購得原價{2}元【{3}】{4}", item.OrigPrice.Equals(0) ? 0 : Math.Round((item.ItemPrice / item.OrigPrice) * 10, 1), item.ItemPrice.ToString("F0"), item.OrigPrice.ToString("F0"), pro.BrandName, item.ItemName.Length > 10 ? item.ItemName.Substring(0, 10) + "..." : item.ItemName);
                            string grossMargin = item.ItemPrice.Equals(0) ? "0%" : Math.Round((item.ItemPrice - item.Cost) / item.ItemPrice * 100, 2) + "%";
                            if (pro.BusinessHourGuid != null)
                                content.Add(string.Format("<a target='_blank' href='{0}'>{1}</a>&nbsp;&nbsp;<span style='color: #BF0000'>{2}</span>", ResolveUrl("~/ppon/default.aspx?bid=" + pro.BusinessHourGuid), name, grossMargin));
                            else
                                content.Add(string.Format("{0}&nbsp;&nbsp;<span style='color: #BF0000'>{1}</span>", name, grossMargin));
                            break;
                        }
                    }
                    else
                    {
                        foreach (var item in multiDeals)
                        {
                            string name = NoSellerGuid + string.Format("{0}折！只要{1}元，即可購得原價{2}元【{3}】{4}", item.OrigPrice.Equals(0) ? 0 : Math.Round((item.ItemPrice / item.OrigPrice) * 10, 1), item.ItemPrice.ToString("F0"), item.OrigPrice.ToString("F0"), pro.BrandName, item.ItemName.Length > 10 ? item.ItemName.Substring(0, 10) + "..." : item.ItemName);
                            string grossMargin = item.ItemPrice.Equals(0) ? "0%" : Math.Round((item.ItemPrice - item.Cost) / item.ItemPrice * 100, 2) + "%";
                            if (pro.BusinessHourGuid != null)
                                content.Add(string.Format("<a target='_blank' href='{0}'>{1}</a>&nbsp;&nbsp;<span style='color: #BF0000'>{2}</span>", ResolveUrl("~/ppon/default.aspx?bid=" + pro.BusinessHourGuid), name, grossMargin));
                            else
                                content.Add(string.Format("{0}&nbsp;&nbsp;<span style='color: #BF0000'>{1}</span>", name, grossMargin));
                        }
                    }
                    liProposalContent.Text = string.Join("<br />", content);
                }
                else
                {
                    if (pro.BusinessHourGuid != null)
                        liProposalContent.Text = string.Format("<a target='_blank' href='{0}'>" + NoSellerGuid + "(無)</a>", ResolveUrl("~/ppon/default.aspx?bid=" + pro.BusinessHourGuid));
                    else
                        liProposalContent.Text = NoSellerGuid + "(無)";
                }
                liMarketAnalysis.Text = pro.MarketAnalysis.Length > 100 ? pro.MarketAnalysis.Substring(0, 100) + "..." : pro.MarketAnalysis;
                Dictionary<string, System.Drawing.Color> flags = new Dictionary<string, System.Drawing.Color>();
                foreach (var flag in Enum.GetValues(typeof(ProposalSpecialFlag)))
                {
                    if ((ProposalSpecialFlag)flag == ProposalSpecialFlag.FirstDeal)
                    {
                        continue;
                    }
                    if (Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)flag))
                    {
                        if ((ProposalSpecialFlag)flag == ProposalSpecialFlag.Urgent)
                        {
                            if (!flags.Keys.Contains(Helper.GetLocalizedEnum((ProposalSpecialFlag)flag)))
                            {
                                flags.Add(Helper.GetLocalizedEnum((ProposalSpecialFlag)flag), System.Drawing.Color.Pink);
                            }

                        }
                        else
                        {
                            if (!flags.Keys.Contains(Helper.GetLocalizedEnum((ProposalSpecialFlag)flag)))
                            {
                                flags.Add(Helper.GetLocalizedEnum((ProposalSpecialFlag)flag), System.Drawing.Color.Transparent);
                            }
                        }
                    }
                }

                if (pro.IsEarlierPageCheck)
                {
                    flags.Add("頁", System.Drawing.Color.Orange);
                }

                if (!string.IsNullOrEmpty(pro.MarketingResource))
                {
                    flags.Add("行銷", System.Drawing.Color.Pink);
                }
                if (pro.ProposalSourceType == (int)ProposalSourceType.House)
                {
                    flags.Add("新宅配", System.Drawing.Color.YellowGreen);
                }
                if (pro.CopyType != (int)ProposalCopyType.None)
                {
                    switch ((ProposalCopyType)pro.CopyType)
                    {
                        case ProposalCopyType.Similar:
                            if (!flags.Keys.Contains(Helper.GetLocalizedEnum((ProposalCopyType)pro.CopyType)))
                            {
                                flags.Add(Helper.GetLocalizedEnum((ProposalCopyType)pro.CopyType), System.Drawing.Color.Chartreuse);
                            }
                            break;
                        case ProposalCopyType.Same:
                            if (!flags.Keys.Contains(Helper.GetLocalizedEnum((ProposalCopyType)pro.CopyType)))
                            {
                                flags.Add(Helper.GetLocalizedEnum((ProposalCopyType)pro.CopyType), System.Drawing.Color.NavajoWhite);
                            }
                            break;
                        case ProposalCopyType.None:
                        default:
                            break;
                    }
                }
                if (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.ShipType == (int)DealShipType.Ship72Hrs)
                {
                    flags.Add(Helper.GetLocalizedEnum(DealShipType.Ship72Hrs), System.Drawing.Color.Yellow);
                }
                rptSpecialFlag.DataSource = flags;
                rptSpecialFlag.DataBind();

                //上檔日期
                liOrderTimeS.Text = pro.OrderTimeS.HasValue ? pro.OrderTimeS.Value.ToString("yyyy/MM/dd") : string.Empty;

                //業務
                ProposalSalesModel model = ProposalFacade.ProposalSaleGetByPid(pro.Id);
                liSalesName.Text = model.DevelopeSalesEmpName + model.DevelopeSalesDeptName.Replace("業務部(", "(").Replace("系統業務部", "(系統業務部)").Replace("通路發展部", "(通路發展部)") + "<br>" +
                                   model.OperationSalesEmpName + model.OperationSalesDeptName.Replace("業務部(", "(").Replace("系統業務部", "(系統業務部)").Replace("通路發展部", "(通路發展部)");


                //指派
                Dictionary<string, int> data = new Dictionary<string, int>();
                var assignlist = logs.OrderBy(x => x.AssignFlag);
                int Isflag = 0;
                string AssignContent = "";
                foreach (var item in assignlist)
                {
                    AssignContent = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalEditorFlag)item.AssignFlag) + " " + item.AssignEmail;


                    if (AssignContent.Split(' ')[0] == Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalEditorFlag.Setting))
                        Isflag = Helper.IsFlagSet(pro.EditFlag, ProposalEditorFlag.Setting) ? 1 : Helper.IsFlagSet(pro.EditPassFlag, ProposalEditorFlag.Setting) ? 2 : 0;
                    else if (AssignContent.Split(' ')[0] == Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalEditorFlag.CopyWriter))
                        Isflag = Helper.IsFlagSet(pro.EditFlag, ProposalEditorFlag.CopyWriter) ? 1 : Helper.IsFlagSet(pro.EditPassFlag, ProposalEditorFlag.CopyWriter) ? 2 : 0;
                    else if (AssignContent.Split(' ')[0] == Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalEditorFlag.ImageDesign))
                        Isflag = Helper.IsFlagSet(pro.EditFlag, ProposalEditorFlag.ImageDesign) ? 1 : Helper.IsFlagSet(pro.EditPassFlag, ProposalEditorFlag.ImageDesign) ? 2 : 0;
                    else if (AssignContent.Split(' ')[0] == Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalEditorFlag.ART))
                        Isflag = Helper.IsFlagSet(pro.EditFlag, ProposalEditorFlag.ART) ? 1 : Helper.IsFlagSet(pro.EditPassFlag, ProposalEditorFlag.ART) ? 2 : 0;
                    else if (AssignContent.Split(' ')[0] == Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalEditorFlag.Listing))
                        Isflag = Helper.IsFlagSet(pro.EditFlag, ProposalEditorFlag.Listing) ? 1 : Helper.IsFlagSet(pro.EditPassFlag, ProposalEditorFlag.Listing) ? 2 : 0;

                    data.Add(AssignContent, Isflag);

                }

                //攝影指派
                string photoString = "";
                Dictionary<ProposalPhotographer, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalPhotographer, string>>(pro.SpecialAppointFlagText);
                if (specialText != null && specialText.Count > 0)
                {
                    photoString = specialText[(ProposalPhotographer)ProposalPhotographer.PhotographerAppointCheck];

                    liAssignPhoto.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalBusinessFlag.PhotographerCheck) + " " +
                                         string.Format("{0}", photoString.Split("|").Length > 3 ? MemberFacade.GetFullName(photoString.Split('|')[3]) : "");
                    imgFlagPhoto.ImageUrl = Helper.IsFlagSet(pro.BusinessFlag, ProposalBusinessFlag.PhotographerCheck) ? ResolveUrl("~/Themes/PCweb/images/Tick.png") : ResolveUrl("~/Themes/PCweb/images/x.png");
                }

                rptAssign.DataSource = data;
                rptAssign.DataBind();


            }

        }

        protected void rptSpecialFlag_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<string, System.Drawing.Color>)
            {
                KeyValuePair<string, System.Drawing.Color> dataItem = (KeyValuePair<string, System.Drawing.Color>)e.Item.DataItem;
                Label lblFlag = (Label)e.Item.FindControl("lblFlag");
                lblFlag.Text = dataItem.Key;
                lblFlag.BackColor = dataItem.Value;
            }
        }

        protected void rptAssign_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<string, int>)
            {
                KeyValuePair<string, int> dataItem = (KeyValuePair<string, int>)e.Item.DataItem;
                Image imgFlag = (Image)e.Item.FindControl("imgFlag");
                Literal liAssign = (Literal)e.Item.FindControl("liAssign");

                liAssign.Text = dataItem.Key;
                if (dataItem.Value == 1)
                    imgFlag.ImageUrl = ResolveUrl("~/Themes/PCweb/images/Tick.png");
                else if (dataItem.Value == 2)
                {
                    imgFlag.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/blueTick.png");
                    imgFlag.Width = 20;
                }
                else if (dataItem.Value == 0)
                    imgFlag.ImageUrl = ResolveUrl("~/Themes/PCweb/images/x.png");

            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            PageSize = Convert.ToInt32(ddlChangePage.SelectedValue);

            if (this.Search != null)
            {
                hdOrderBy.Value = "asc";
                hdOrderField.Value = "";

                this.Search(this, e);
            }
            ucPager.ResolvePagerView(CurrentPage, true);
        }
        protected void hidSortSearch_Click(object sender, EventArgs e)
        {
            if (Search != null)
            {
                if (hdOrderBy.Value == "desc")
                {
                    hdOrderBy.Value = "asc";
                }
                else
                {
                    hdOrderBy.Value = "desc";
                }


                this.Search(sender, e);
                ucPager.ResolvePagerView(1, true);
            }
        }
        protected void btnAssign_Click(object sender, EventArgs e)
        {
            PageSize = Convert.ToInt32(ddlChangePage.SelectedValue);

            if (this.Search != null)
            {
                this.Search(this, e);
            }
            ucPager.ResolvePagerView(CurrentPage, true);

            txtAssignEmp.Text = "";
        }


        #endregion

        #region private method
        private void IntialControls()
        {
            // 上檔頻道
            List<CategoryNode> nodeData = CategoryManager.PponChannelCategoryTree.CategoryNodes.OrderBy(x => x.Seq).ToList();
            rptChannel.DataSource = nodeData.Where(x => x.CategoryId.EqualsNone(91, 92, 93, 185, 186, 2160, 2161, 100001, 100002));
            rptChannel.DataBind();

            // 提案類型
            Dictionary<int, string> dealType = new Dictionary<int, string>();

            foreach (var item in Enum.GetValues(typeof(ProposalDealType)))
            {
                if (item.ToString() == ProposalDealType.None.ToString())
                {
                    dealType[(int)item] = "請選擇";
                }
                else
                {
                    dealType[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDealType)item);
                }
            }
            ddlDealType.DataSource = dealType;//.Where(x => x.Key != (int)ProposalDealType.None);
            ddlDealType.DataTextField = "Value";
            ddlDealType.DataValueField = "Key";
            ddlDealType.DataBind();

        }
        private void EnabledSettings(string username)
        {
            if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.Assign))
                btnBatchAssign.Visible = true;
        }

        #endregion

        #region method
        public void SetBusinessList(Dictionary<ViewProposalSeller, ProposalAssignLogCollection> dataList)
        {

            DateTime start = DateTime.Now;
            divBusinessList.Visible = true;
            rptBusiness.DataSource = dataList;
            rptBusiness.DataBind();
            DateTime end = DateTime.Now;
            TimeSpan sp = end - start;
            ProposalFacade.ProposalPerformanceLogSet("/sal/SalesProductionBusinessList.aspx", "Start：" + start.ToString("yyyy-MM-dd hh:mm:ss fff") + " End：" + end.ToString("yyyy-MM-dd hh:mm:ss fff") + " Time：" + sp.Milliseconds + "毫秒 ", UserName);
        }

        public void ShowMessage(string msg)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + msg + "');", true);
        }
        #endregion

        #region WebMethod
        [WebMethod]
        public static List<AjaxResponseResult> GetProductionNameArray(string userName)
        {
            List<AjaxResponseResult> res = new List<AjaxResponseResult>();
            if (!string.IsNullOrEmpty(userName) && userName.Length >= 2)
            {
                List<string> emps = SellerFacade.GetProductionNameArray(userName);
                foreach (string emp in emps)
                {
                    res.Add(new AjaxResponseResult
                    {
                        Value = emp,
                        Label = emp
                    });
                }
            }

            return res;
        }

        [WebMethod]
        public static bool IsProduction(string userName)
        {
            return SellerFacade.IsDepartment(EmployeeDept.P000, userName) | SellerFacade.IsDepartment(EmployeeDept.Q000, userName);
        }

        [WebMethod]
        public static bool AssignProduction(string AssignPro, string AssignFlag, string AssignEmp, string UserName)
        {
            return ProposalFacade.AssignProduction(AssignPro, AssignFlag, AssignEmp, UserName);
        }


        #endregion

    }
}
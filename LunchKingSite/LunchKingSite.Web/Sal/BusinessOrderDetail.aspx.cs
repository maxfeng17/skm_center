﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using MongoDB.Bson;
using Novacode;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Winnovative.WnvHtmlConvert;
using LunchKingSite.Mongo.Facade;

namespace LunchKingSite.Web.Sal
{
    public partial class BusinessOrderDetail : RolePage, ISalesBusinessOrderDetailView
    {
        private ISysConfProvider _config = ProviderFactory.Instance().GetConfig();

        public const int productUseStartAddDays = 3; // 預設宅配檔一般出貨設定 最早出貨日(開始使用)=上檔後+3個工作日
        public const int productUseEndAddDays = 7; // 預設宅配檔一般出貨設定 最晚出貨日=結檔後+7個工作日
        public const int ShippingDate = 7;

        #region props

        private SalesBusinessOrderDetailPresenter _presenter;

        public SalesBusinessOrderDetailPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public ObjectId BusinessOrderObjectId
        {
            get
            {
                ObjectId id = ObjectId.Empty;
                if (Request["boid"] != null)
                {
                    ObjectId.TryParse(Request["boid"], out id);
                }
                return id;
            }
        }

        public Guid BusinessHourGuid
        {
            get
            {
                if (string.IsNullOrEmpty(txtBid.Text))
                {
                    return Guid.Empty;
                }

                try
                {
                    return new Guid(txtBid.Text);
                }
                catch
                {
                    return Guid.Empty;
                }
            }
        }

        public Guid AncestorSequenceBid
        {
            get
            {
                if (string.IsNullOrEmpty(txtAncestorSequenceBid.Text))
                {
                    return Guid.Empty;
                }

                try
                {
                    return new Guid(txtAncestorSequenceBid.Text);
                }
                catch
                {
                    return Guid.Empty;
                }
            }
        }

        public Guid AncestorQuantityBid
        {
            get
            {
                if (string.IsNullOrEmpty(txtAncestorQuantityBid.Text))
                {
                    return Guid.Empty;
                }

                try
                {
                    return new Guid(txtAncestorQuantityBid.Text);
                }
                catch
                {
                    return Guid.Empty;
                }
            }
        }

        public string BusinessOrderId
        {
            get
            {
                return lblBusinessOrderId.Text;
            }
        }

        private string _ShippingDateType;
        public string RbShippingDateType
        {
            get
            {
                if (_ShippingDateType != null)
                {
                    return _ShippingDateType;
                }
                else
                {
                    int type = int.TryParse(ShippingDateType.Normal.ToString(), out type) ? type : 0;
                    return type.ToString();
                }
            }
            set
            {
                _ShippingDateType = value;
            }
        }

        public string SellerId
        {
            get
            {
                return Request.QueryString["sid"] != null ? Request.QueryString["sid"].ToString() : string.Empty;
            }
        }

        public string SalesId
        {
            get
            {
                return lblCreateId.Text.ToLower();
            }
        }

        public string UserId
        {
            get
            {
                return Page.User.Identity.Name.ToLower();
            }
        }

        public string AccountBankNo
        {
            get
            {
                return txtAccountingBankId.Text;
            }
        }

        public string AuthorizationGroup
        {
            get
            {
                return rdlAuthorizationGroup.SelectedValue;
            }
            set
            {
                if (rdlAuthorizationGroup.Items.FindByValue(value) != null)
                {
                    rdlAuthorizationGroup.SelectedValue = value;
                }
            }
        }

        public Dictionary<int, List<int>> SelectedChannelCategories
        {
            get
            {
                Dictionary<int, List<int>> selected = new Dictionary<int, List<int>>();
                List<int> selectedList = new JsonSerializer().Deserialize<List<int>>(hiddenChannelChecked.Value);
                List<List<int>> selectedSubList = new JsonSerializer().Deserialize<List<List<int>>>(hiddenSubChannelChecked.Value);
                if (selectedList == null || selectedList.Count == 0 || selectedSubList == null || selectedSubList.Count == 0)
                {
                    return selected;
                }
                for (int i = 0; i < selectedList.Count; i++)
                {
                    selected.Add(selectedList[i], selectedSubList[i]);
                }
                return selected;
            }

            set
            {
                List<int> selectChannelIds = new List<int>();
                List<List<int>> selectSubChannelIds = new List<List<int>>();
                foreach (var data in value)
                {
                    selectChannelIds.Add(data.Key);
                    selectSubChannelIds.Add(data.Value);
                }
                hiddenChannelChecked.Value = new JsonSerializer().Serialize(selectChannelIds);
                hiddenSubChannelChecked.Value = new JsonSerializer().Serialize(selectSubChannelIds);
            }
        }

        public Dictionary<int, List<int>> SelectedChannelSpecialRegionCategories
        {
            get
            {
                Dictionary<int, List<int>> selected = new Dictionary<int, List<int>>();
                List<List<int>> selectedList = new JsonSerializer().Deserialize<List<List<int>>>(hiddenSubChannelChecked.Value);
                List<List<int>> selectedSubList = new JsonSerializer().Deserialize<List<List<int>>>(hiddenSubSpecialRegionChecked.Value);

                if (selectedList == null || selectedList.Count == 0 || selectedSubList == null || selectedSubList.Count == 0)
                {
                    return selected;
                }

                List<int> subchannel = new List<int>();

                foreach (var channelset in selectedList)
                {
                    subchannel.AddRange(channelset.Select(x => x));
                }

                for (int i = 0; i < subchannel.Count; i++)
                {
                    selected.Add(subchannel[i], selectedSubList[i]);
                }


                return selected;
            }

            set
            {
                List<int> selectChannelIds = new List<int>();
                List<List<int>> selectSubChannelIds = new List<List<int>>();
                foreach (var data in value)
                {
                    selectChannelIds.Add(data.Key);
                    selectSubChannelIds.Add(data.Value);
                }
                hiddenSubSpecialRegionChecked.Value = new JsonSerializer().Serialize(selectSubChannelIds);
            }
        }

        public List<int> SelectedDealCategories
        {
            get { return new JsonSerializer().Deserialize<List<int>>(hiddenDealCategory.Value); }
            set { hiddenDealCategory.Value = new JsonSerializer().Serialize(value); }
        }

        public int[] SelectedTravelCategoryId
        {
            get
            {
                List<int> selected = new List<int>();
                for (int i = 0; i < chklTravelCategory.Items.Count; i++)
                {
                    if (chklTravelCategory.Items[i].Selected)
                    {
                        selected.Add(int.Parse(chklTravelCategory.Items[i].Value));
                    }
                }
                return selected.ToArray();
            }

            set
            {
                if (value != null && value.Length > 0)
                {
                    for (int i = 0; i < chklTravelCategory.Items.Count; i++)
                    {
                        chklTravelCategory.Items[i].Selected = value != null && value.Any(x => x == int.Parse(chklTravelCategory.Items[i].Value));
                    }
                    chklTravelCategory.Items[chklTravelCategory.Items.Count - 1].Attributes["style"] = "display:none";
                    chklTravelCategory.Items[chklTravelCategory.Items.Count - 1].Selected = true;
                }
            }
        }

        public Dictionary<int, int[]> SelectedCommercialCategoryId
        {
            get
            {
                Dictionary<int, int[]> selected = new Dictionary<int, int[]>();
                for (int i = 0; i < rptCommercialCategory.Items.Count; i++)
                {
                    HiddenField hidCommercialCategoryId = (HiddenField)rptCommercialCategory.Items[i].FindControl("hidCommercialCategoryId");
                    CheckBoxList chklCommercialCategory = (CheckBoxList)rptCommercialCategory.Items[i].FindControl("chklCommercialCategory");
                    int cid = int.TryParse(hidCommercialCategoryId.Value, out cid) ? cid : 0;
                    int[] categorys = chklCommercialCategory.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => Convert.ToInt32(item.Value)).ToArray();
                    if (!cid.Equals(0) && categorys.Length > 0)
                    {
                        selected.Add(cid, categorys);
                    }
                }
                return selected;
            }
            set
            {
                if (value.Count > 0)
                {
                    for (int i = 0; i < rptCommercialCategory.Items.Count; i++)
                    {
                        HiddenField hidCommercialCategoryId = (HiddenField)rptCommercialCategory.Items[i].FindControl("hidCommercialCategoryId");
                        CheckBoxList chklCommercialCategory = (CheckBoxList)rptCommercialCategory.Items[i].FindControl("chklCommercialCategory");
                        int cid = int.TryParse(hidCommercialCategoryId.Value, out cid) ? cid : 0;
                        if (value.ContainsKey(cid))
                        {
                            string separator = ",";
                            SetCheckBoxListSelectedValue(chklCommercialCategory, string.Join(separator, value[cid]));
                        }
                    }
                }
            }
        }

        public string SalesDept
        {
            get
            {
                return rdlSalesDept.SelectedValue;
            }
            set
            {
                if (rdlSalesDept.Items.FindByValue(value) != null)
                {
                    rdlSalesDept.SelectedValue = value;
                }
                else
                {
                    ShowMessage("您尚未設定歸屬部門，請洽該區特助人員!", _config.SiteUrl + "/sal/salesmanagement.aspx");
                }
            }
        }

        public string SalesMemberArea
        {
            get
            {
                return hidSalesMemberArea.Value;
            }
            set
            {
                hidSalesMemberArea.Value = value;
            }
        }

        public Level SalesLevel
        {
            get
            {
                int value;
                int.TryParse(hidSalesLevel.Value, out value);
                return (Level)value;
            }
            set
            {
                hidSalesLevel.Value = Convert.ToInt32(value).ToString();
            }
        }

        public int AddressCityId
        {
            get
            {
                if ((Request["selCity"] == null) || (Request["selCity"] == ""))
                {
                    int cityId;
                    return int.TryParse(hidAddressCityId.Value, out cityId) ? cityId : -1;
                }
                else
                {
                    return int.Parse(Request["selCity"]);
                }
            }
            set
            {
                hidAddressCityId.Value = value.ToString();
            }
        }

        public int AddressTownshipId
        {
            get
            {
                if ((Request["selTownship"] == null) || (Request["selTownship"] == ""))
                {
                    int townshipId;
                    return int.TryParse(hidAddressTownshipId.Value, out townshipId) ? townshipId : -1;
                }
                else
                {
                    return int.Parse(Request["selTownship"]);
                }
            }
            set
            {
                hidAddressTownshipId.Value = value.ToString();
            }
        }

        public string CityJSonData
        {
            get
            {
                return hidCityJSonData.Value;
            }
            set
            {
                hidCityJSonData.Value = value;
            }
        }

        public string SalesName
        {
            get
            {
                return txtSalesName.Text;
            }
            set
            {
                txtSalesName.Text = value;
            }
        }

        public string SalesPhone
        {
            get
            {
                return txtSalesPhone.Text;
            }
            set
            {
                txtSalesPhone.Text = value;
            }
        }

        public RemittanceType PaidType
        {
            get
            {
                if (rbAchWeekly.Checked)
                {
                    return RemittanceType.AchWeekly;
                }

                if (rbPartially.Checked)
                {
                    return RemittanceType.ManualPartially;
                }

                if (rbManualWeekly.Checked)
                {
                    return RemittanceType.ManualWeekly;
                }

                if (rbManualMonthly.Checked)
                {
                    return RemittanceType.ManualMonthly;
                }

                if (rbFlexible.Checked)
                {
                    return RemittanceType.Flexible;
                }

                if (rbMonthly.Checked)
                {
                    return RemittanceType.Monthly;
                }

                if (rbWeekly.Checked)
                {
                    return RemittanceType.Weekly;
                }

                return RemittanceType.Others;
            }
            set
            {
                rbAchWeekly.Checked = rbPartially.Checked = rbManualWeekly.Checked = rbManualMonthly.Checked = rbOthers.Checked
                     = rbFlexible.Checked = rbMonthly.Checked = rbWeekly.Checked = false;

                switch (value)
                {
                    case RemittanceType.AchWeekly:
                        rbAchWeekly.Checked = true;
                        break;

                    case RemittanceType.ManualPartially:
                        rbPartially.Checked = true;
                        break;

                    case RemittanceType.ManualWeekly:
                        rbManualWeekly.Checked = true;
                        break;

                    case RemittanceType.ManualMonthly:
                        rbManualMonthly.Checked = true;
                        break;

                    case RemittanceType.Flexible:
                        rbFlexible.Checked = true;
                        break;

                    case RemittanceType.AchMonthly:
                    case RemittanceType.Monthly:
                        rbMonthly.Checked = true;
                        break;

                    case RemittanceType.Weekly:
                        rbWeekly.Checked = true;
                        break;

                    default:
                        rbOthers.Checked = true;
                        break;
                }
            }
        }

        public PponVerifyType VerifyType
        {
            get
            {
                if (rdlOnlineUse.Checked)
                {
                    return PponVerifyType.OnlineUse;
                }
                else if (rdlInventoryOnly.Checked)
                {
                    return PponVerifyType.InventoryOnly;
                }
                else
                {
                    return PponVerifyType.OnlineOnly;
                }
            }
            set
            {
                rdlOnlineUse.Checked = rdlOnlineOnly.Checked = rdlInventoryOnly.Checked = false;

                switch (value)
                {
                    case PponVerifyType.OnlineUse:
                        rdlOnlineUse.Checked = true;
                        break;

                    case PponVerifyType.InventoryOnly:
                        rdlInventoryOnly.Checked = true;
                        break;

                    case PponVerifyType.OnlineOnly:
                    default:
                        rdlOnlineOnly.Checked = true;
                        break;
                }
            }
        }

        public bool IsContinuedSequence
        {
            get
            {
                return chkIsContinuedSequence.Checked;
            }
        }

        public bool IsContinuedQuantity
        {
            get
            {
                return chkIsContinuedQuantity.Checked;
            }
        }

        public AdditionalContracts Contracts
        {
            get
            {
                AdditionalContracts contracts = new AdditionalContracts();
                contracts.IsProductRefund = chkProductRefund.Checked;
                return contracts;
            }
            set
            {
                chkProductRefund.Checked = value.IsProductRefund;
            }
        }

        private string _salesEmailArray;
        public string SalesEmailArray
        {
            get { return _salesEmailArray; }
            set { _salesEmailArray = value; }
        }

        private string _salesNameArray;
        public string SalesNameArray
        {
            get { return _salesNameArray; }
            set { _salesNameArray = value; }
        }

        private string _salesInfoArray = string.Empty;
        public string SalesInfoArray
        {
            get
            {
                return _salesInfoArray;
            }
            set
            {
                _salesInfoArray = value;
            }
        }

        public bool IsSalesAssistant
        {
            get
            {
                return User.IsInRole(MemberRoles.SalesAssistant.ToString());
            }
        }

        public bool IsSales
        {
            get
            {
                return User.IsInRole(MemberRoles.Sales.ToString());
            }
        }

        private BusinessType _businessType;
        public BusinessType BusinessType
        {
            get
            {
                if (_businessType != BusinessType.None)
                {
                    return _businessType;
                }
                else
                {
                    BusinessType type = BusinessType.None;
                    BusinessType.TryParse(rdlBusinessType.SelectedValue, out type);
                    return type;
                }
            }
            set
            {
                _businessType = value;
                rdlBusinessType.SelectedValue = value.ToString();
            }
        }

        private ActiveControlParameters _activeControls;
        public ActiveControlParameters ActiveControls
        {
            get
            {
                return _activeControls;
            }
            set
            {
                _activeControls = value;
            }
        }

        private AuthorizationControlParameters _authorizationControls;
        public AuthorizationControlParameters AuthorizationControls
        {
            get
            {
                return _authorizationControls;
            }
            set
            {
                _authorizationControls = value;
            }
        }

        private string[] chineseNumber = { "一", "二", "三", "四", "五", "六", "七", "八", "九", "十" };

        private string[] paragraph = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N"
                                         , "O", "P", "Q", "R", "S", "T", "U", "V" ,"W" ,"X" ,"Y" ,"Z"};

        private string newLine
        {
            get
            {
                return chkIsTravelType.Checked ? "\n" : @"<br />";
            }
        }

        private string indentation
        {
            get
            {
                return chkIsTravelType.Checked ? "\t" : @"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            }
        }

        public int DealType1
        {
            get
            {
                try
                {
                    return int.Parse(ddlDealType1.SelectedValue);
                }
                catch
                {
                    return -1;
                }
            }
            set { ddlDealType1.SelectedValue = value.ToString(); }
        }

        public int DealType2
        {
            get
            {
                try
                {
                    return int.Parse(hdDealType2.Value);
                }
                catch
                {
                    return -1;
                }
            }
            set { hdDealType2.Value = value.ToString(); }

        }

        public bool IsLongContract
        {
            set { chkIsLongContract.Checked = value; }
            get { return chkIsLongContract.Checked; }
        }

        #endregion props

        #region event

        public event EventHandler<DataEventArgs<string>> SaveClicked;

        public event EventHandler<EventArgs> DeleteClicked;

        public event EventHandler<DataEventArgs<ObjectId>> StoreDeleteClicked;

        public event EventHandler<DataEventArgs<BusinessCopyType>> CopyClicked;

        public event EventHandler<EventArgs> DownloadStoreClicked;

        public event EventHandler<DataEventArgs<List<SalesStore>>> ImportClicked;

        public event EventHandler<EventArgs> RefreshStoreClicked;

        public event EventHandler<EventArgs> DownloadClicked;

        public event EventHandler<DataEventArgs<string>> ReferredClicked;

        #endregion event

        #region method

        public void SetDealLabel(DealLabelCollection dealLabels)
        {
            foreach (DealLabel dealLabel in dealLabels.Where(x => x.SystemCodeId != (int)DealLabelSystemCode.Tickets))
            {
                if (GetDealLabelShowType(dealLabel) != DealLabelShowType.icon)
                {
                    CheckBoxList cbl = GetDealLabelCheckBoxList(dealLabel);
                    ListItem item = GetDealLabelListItem(dealLabel);

                    if (cbl != null && item.Enabled == true)
                    {
                        cbl.Items.Add(GetDealLabelListItem(dealLabel));
                    }
                }
            }
        }

        public void SetDealType(int parentId, int codeId, List<SystemCode> dealTypes)
        {
            #region 檔次母分類

            ddlDealType1.Items.Clear();
            ddlDealType1.Items.Add(new ListItem("=請選擇母分類=", "0"));

            List<SystemCode> dealTypes1 = dealTypes.Where(x => x.ParentCodeId == null).OrderBy(x => x.Seq).ToList();
            foreach (var code in dealTypes1)
            {
                ddlDealType1.Items.Add(new ListItem(code.CodeName, code.CodeId.ToString()));
                if (code.CodeId == 1999)
                {
                    ddlDealType1.Items.FindByValue("1999").Attributes.Add("Disabled", "Disabled");
                }
            }
            ddlDealType1.SelectedValue = parentId.ToString();


            ddlDealType3.DataSource = dealTypes.Select(x => new { CodeId = x.CodeId, CodeNmae = x.CodeName });
            ddlDealType3.DataValueField = "CodeId";
            ddlDealType3.DataTextField = "CodeNmae";
            ddlDealType3.DataBind();
            ddlDealType3.SelectedIndex = codeId;
            #endregion

            #region 檔次子分類

            //ddlDealType2.Items.Clear();
            //ddlDealType2.Items.Add(new ListItem("=請選擇子分類=", "0"));

            //List<SystemCode> dealTypes2 = dealTypes.Where(x => x.ParentCodeId == parentId).OrderBy(x => x.Seq).ToList();
            //foreach (var code in dealTypes2)
            //{
            //    ddlDealType2.Items.Add(new ListItem(code.CodeName, code.CodeId.ToString()));
            //}
            //ddlDealType2.SelectedValue = codeId.ToString();
            hdDealType2.Value = codeId.ToString();

            #endregion
        }

        public void GetLocationDropDownList(List<City> citys, List<City> locations)
        {
            repCity.DataSource = citys;
            repCity.DataBind();

            repTown.DataSource = locations;
            repTown.DataBind();
        }

        public void SetSelectableZone(List<PponCity> cities)
        {
            chklPponCity.Items.Clear();
            chklPponCity.Items.AddRange(cities.Select(x => new ListItem(x.CityName, x.CityId.ToString())).ToArray());
            chklPponCity.Items.Add(new ListItem() { Text = "舊品生活", Value = "Piinlife" });
            chklPponCity.Items.Add(new ListItem() { Text = "其它", Value = "Others" });

            chklNPponCity.Items.Clear();
            chklNPponCity.Items.Add(new ListItem() { Text = "舊品生活", Value = "Piinlife" });
            chklNPponCity.Items.Add(new ListItem() { Text = "其它", Value = "Others" });

            foreach (ListItem item in chklNPponCity.Items)
            {
                if (item.Value == "Piinlife")
                {
                    item.Attributes.Add("onclick", "setHidPiinLifeForNPponCity();");
                }
            }

        }

        public void SetSelectableChannel(CategoryTypeNode channelTypeNode)
        {
            JsonSerializer json = new JsonSerializer();
            //將頻道的資料儲存於隱藏的欄位
            //排除92福利網 93天貓
            List<int> channelList = channelTypeNode.CategoryNodes.Where(p => p.CategoryId != 92 && p.CategoryId != 93).Select(x => x.CategoryId).ToList();
            hiddenChannelJSon.Value = json.Serialize(channelList);
            //將頻道所屬的分類之資料儲存於隱藏的欄位
            List<List<int>> channelCategoryList = new List<List<int>>();
            foreach (var node in channelTypeNode.CategoryNodes.Where(p => p.CategoryId != 92 && p.CategoryId != 93))
            {
                List<int> dealCategories = new List<int>();
                List<CategoryTypeNode> typeNodes = node.NodeDatas.Where(x => x.NodeType == CategoryType.DealCategory).ToList();
                if (typeNodes.Count > 0)
                {
                    dealCategories = typeNodes.First().CategoryNodes.Select(x => x.CategoryId).ToList();
                }
                channelCategoryList.Add(dealCategories);
            }
            hiddenChannelCategoryJSon.Value = json.Serialize(channelCategoryList);


            channelCheckArea.DataSource = channelTypeNode.CategoryNodes.Where(p => p.CategoryId != 92 && p.CategoryId != 93).OrderBy(x => x.Seq);
            channelCheckArea.DataBind();




            //dealCategoryArea.DataSource = CategoryManager.CategoryGetListByType(CategoryType.DealCategory);
            dealCategoryArea.DataSource = GetCategoryGetListWithoutSubDealCategory(CategoryManager.CategoryGetListByType(CategoryType.DealCategory));
            dealCategoryArea.DataBind();
        }


        public void SetSelectableTravelCategory(List<Category> categories)
        {
            chklTravelCategory.Items.AddRange(categories.Select(x => new ListItem(x.Name, x.Id.ToString()) { Selected = x.ParentCode == null, Enabled = x.ParentCode != null }).OrderBy(x => x.Selected).ThenBy(x => x.Value).ToArray());
            chklTravelCategory.Items[chklTravelCategory.Items.Count - 1].Attributes["style"] = "display:none";
            chklTravelCategory.Items[chklTravelCategory.Items.Count - 1].Selected = true;
        }

        public void SetSelectableCommercialCategory(Dictionary<Category, List<Category>> categories)
        {
            rptCommercialCategory.DataSource = categories;
            rptCommercialCategory.DataBind();
        }

        public void SetAccountingBankInfo(BankInfoCollection bankList)
        {
            ddlAccountingBank.DataSource = bankList.Select(x => new { No = x.BankNo, Name = x.BankNo + " " + x.BankName });
            ddlAccountingBank.DataValueField = "No";
            ddlAccountingBank.DataTextField = "Name";
            ddlAccountingBank.DataBind();
        }

        public void SetAccountingBranchInfo(BankInfoCollection branchList)
        {
            ddlAccountingBranch.DataSource = branchList.Select(x => new { No = x.BranchNo, Name = x.BranchNo + " " + x.BranchName });
            ddlAccountingBranch.DataValueField = "No";
            ddlAccountingBranch.DataTextField = "Name";
            ddlAccountingBranch.DataBind();
        }

        public void GetSalesDepartment(List<Department> deptc)
        {
            rdlSalesDept.DataSource = deptc.Select(x => new { Id = x.DeptId, Name = x.DeptName });
            rdlSalesDept.DataBind();

            rdlAuthorizationGroup.DataSource = deptc.Select(x => new { Id = x.DeptId, Name = x.DeptName.Replace("業務", string.Empty).Replace("部", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty) });
            rdlAuthorizationGroup.DataBind();
        }

        public void GetAccBusinessGroupGetList(AccBusinessGroupCollection abgc)
        {
            rdlAccBusinessGroup.DataSource = abgc;
            rdlAccBusinessGroup.DataBind();

            rdlAccBusinessGroup.SelectedIndex = 0;
        }

        public void SetBusinessOrderDetail(BusinessOrder item)
        {
            if (item != null)
            {
                btnStoreAdd.Enabled = true;

                #region 共同基本資料

                if (item.BusinessHourGuid != Guid.Empty)
                {
                    txtBid.Text = item.BusinessHourGuid.ToString();
                }
                else if (!item.DealId.Equals(0))
                {
                    txtBid.Text = item.DealId.ToString();
                }

                if (item.AncestorQuantityBusinessHourGuid != Guid.Empty)
                {
                    txtAncestorQuantityBid.Text = item.AncestorQuantityBusinessHourGuid.ToString();
                }
                if (item.AncestorSequenceBusinessHourGuid != Guid.Empty)
                {
                    txtAncestorSequenceBid.Text = item.AncestorSequenceBusinessHourGuid.ToString();
                }

                txtUniqueId.Text = item.UniqueId.ToString();
                txtSellerId.Text = item.SellerId;
                hidSellerGuid.Value = item.SellerGuid.ToString();
                lblBusinessOrderId.Text = item.BusinessOrderId;
                if (!item.CopyType.Equals(BusinessCopyType.None))
                {
                    hlCopyBusinessOrderId.Text = item.CopyBusinessOrderId.ToString();
                    hlCopyBusinessOrderId.NavigateUrl = ResolveUrl("~/Sal/BusinessOrderDetail.aspx?boid=" + item.CopyBusinessHourGuid);
                    switch (item.CopyType)
                    {
                        case BusinessCopyType.General:
                            lblBusinessCopyType.Text = "<複製>";
                            lblBusinessCopyType.ForeColor = System.Drawing.Color.Green;
                            break;

                        case BusinessCopyType.Again:
                            lblBusinessCopyType.Text = "<再次上檔>";
                            lblBusinessCopyType.ForeColor = System.Drawing.Color.Red;
                            if (item.IsContinuedQuantity)
                            {
                                lblBusinessCopyType.Text += "(數量+)";
                            }
                            break;

                        default:
                            break;
                    }
                }
                else
                {
                    lblBusinessCopyType.ForeColor = System.Drawing.Color.Gray;
                }
                if (item.SellerLevel != BusinessOrderSellerLevel.None)
                {
                    rdlSellerLevel.SelectedValue = item.SellerLevel.ToString();
                }
                rdlSalesDept.SelectedValue = item.SalesDeptId;
                SetCheckBoxListSelectedValue(chklPponCity, item.PponCity);
                SetCheckBoxListSelectedValue(chklNPponCity, item.PponCity);
                if (item.PponCity.Split(',').ToList().Contains("Piinlife"))
                {
                    hidPiinLifeForNPponCity.Value = "1";
                }
                txtOtherPponCity.Text = txtNOtherPponCity.Text = item.OtherPponCity;



                if (item.SelectedChannelCategories.Count > 0)
                {
                    SelectedChannelCategories = item.SelectedChannelCategories;
                    SelectedChannelSpecialRegionCategories = item.SelectedChannelSpecialRegionCategories;
                }
                else
                {
                    #region 舊City分類轉換SelectedChannelCategories

                    //處理城市的選取
                    List<PponCity> pponCities = new List<PponCity>();
                    foreach (PponCity pponCity in PponCityGroup.DefaultPponCityGroup)
                    {
                        if (item.PponCity.Contains(pponCity.CityId.ToString()))
                        {
                            pponCities.Add(pponCity);
                        }
                    }

                    Dictionary<int, List<int>> selectedChannelCategories = new Dictionary<int, List<int>>();
                    foreach (CategoryNode node in CategoryManager.PponChannelCategoryTree.CategoryNodes)
                    {
                        //有子項目
                        List<CategoryNode> subList = node.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                        if (subList != null && subList.Count > 0)
                        {
                            //旅遊度假需另外處理
                            if (node.CategoryId == PponCityGroup.DefaultPponCityGroup.Travel.CategoryId)
                            {
                                if (item.PponCity.Split(',').Contains(PponCityGroup.DefaultPponCityGroup.Travel.CityId.ToString()))
                                {
                                    //有勾選城市
                                    List<int> areaList = new List<int>();
                                    //檢查有無小區域的部分
                                    foreach (Category c in ViewPponDealManager.DefaultManager.TravelCategories)
                                    {
                                        if (item.TravelCategory != null && item.TravelCategory.Contains(Convert.ToInt32(c.Id)))
                                        {
                                            Category newTravelCategory = CategoryManager.GetPponChannelAreaByTravel(c.Id);
                                            if (newTravelCategory != null)
                                            {
                                                areaList.Add(newTravelCategory.Id);
                                            }
                                        }
                                    }
                                    selectedChannelCategories.Add(node.CategoryId, areaList);
                                }
                            }
                            else if (node.CategoryId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CategoryId)
                            {
                                if (pponCities.Any(x => x.CategoryId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CategoryId))
                                {
                                    selectedChannelCategories.Add(node.CategoryId, new List<int>());
                                }
                            }
                            else
                            {
                                //有子項目，判斷城市是否有勾選子項目的區塊，若有新增此區塊於新分類顯示
                                List<int> areaList = new List<int>();
                                foreach (var categoryNode in subList)
                                {
                                    if (pponCities.Any(x => x.CategoryId == categoryNode.CategoryId))
                                    {
                                        areaList.Add(categoryNode.CategoryId);
                                    }
                                }
                                if (areaList.Count > 0)
                                {
                                    selectedChannelCategories.Add(node.CategoryId, areaList);
                                }
                            }
                        }
                        else
                        {
                            //無子項目，直接判斷主區塊是否選取
                            if (pponCities.Any(x => x.CategoryId == node.CategoryId))
                            {
                                selectedChannelCategories.Add(node.CategoryId, new List<int>());
                            }
                        }
                    }
                    SelectedChannelCategories = selectedChannelCategories;

                    #endregion 舊City分類轉換SelectedChannelCategories
                }

                //DealCategory
                SelectedDealCategories = item.SelectedDealCategories;

                AuthorizationGroup = item.AuthorizationGroup;
                hidAuthorizationGroup.Value = item.AuthorizationGroup;
                rdlAccBusinessGroup.SelectedValue = item.AccBusinessGroupId;
                hidAccBusinessGroup.Value = item.AccBusinessGroupId;
                this.SelectedTravelCategoryId = item.TravelCategory;
                this.SelectedCommercialCategoryId = item.CommercialCategory;
                chkIsContractSend.Checked = item.IsContractSend;
                if (!item.IsContractSend)
                {
                    if (ddlNoContractReason.Items.FindByText(item.NoContractReason) != null)
                    {
                        ddlNoContractReason.Items.FindByText(item.NoContractReason).Selected = true;
                    }
                    else
                    {
                        ddlNoContractReason.SelectedValue = "5";
                    }
                }
                txtNoContractReason.Text = item.NoContractReason;
                txtSalesName.Text = item.SalesName;
                txtSalesPhone.Text = item.SalesPhone;
                txtKeywords.Text = item.Keywords;
                txtImportant1.Text = item.Important1;
                txtImportant2.Text = item.Important2;
                txtImportant3.Text = item.Important3;
                txtImportant4.Text = item.Important4;
                SetCheckBoxListSelectedValue(chklPictureSource, item.PictureSource);
                txtMarketEvent.Text = item.MarketEvent;
                SetCheckBoxListSelectedValue(chklSourceType, item.SourceType);
                BusinessType = item.BusinessType;
                rdlIsApplyNewPromotion.SelectedValue = item.IsApplyNewPromotion.ToString();
                rdlApplyNewPromotion.SelectedValue = item.IsApplyNewPromotionConform.ToString();
                hidStatus.Value = Convert.ToInt32(item.Status).ToString();
                ddlStatus.SelectedValue = Convert.ToInt32(item.Status).ToString();
                txtSellerName.Text = item.SellerName;
                lblTitle.Text = item.SellerName;
                txtCompanyName.Text = item.CompanyName;
                rdlPrivateContract.SelectedValue = item.IsPrivateContract.ToString();
                txtGuiNumber.Text = item.GuiNumber;
                txtBossName.Text = item.BossName;
                txtPersonalId.Text = item.PersonalId;
                txtContactPerson.Text = item.ContactPerson;
                txtBossTel.Text = item.BossTel;
                txtBossPhone.Text = item.BossPhone;
                txtBossFax.Text = item.BossFax;
                txtBossEmail.Text = item.BossEmail;
                txtBossAddress.Text = item.BossAddress;
                txtAccountingName.Text = item.AccountingName;
                txtAccountingTel.Text = item.AccountingTel;
                txtAccountingEmail.Text = item.AccountingEmail;

                hdDealType2.Value = (item.DealType == null) ? "0" : item.DealType.ToString();
                //ddlDealType2.SelectedValue = item.DealType;

                if (ddlAccountingBank.Items.FindByValue(item.AccountingBankId) != null)
                {
                    ddlAccountingBank.SelectedValue = item.AccountingBankId;
                }
                else
                {
                    ddlAccountingBank.SelectedIndex = -1;
                    ddlAccountingBranch.SelectedIndex = -1;
                }
                txtAccountingBankId.Text = item.AccountingBankId;
                txtAccountingBankDesc.Text = item.AccountingBank;
                txtAccountingBranchId.Text = item.AccountingBranchId;
                txtAccountingBranchDesc.Text = item.AccountingBranch;

                txtAccountingId.Text = item.AccountingId;
                txtAccounting.Text = item.Accounting;
                txtAccount.Text = item.Account;
                chkIsEntrustSell.Checked = item.IsEntrustSell;
                rbIsMohist.Checked = lblMohistVerify.Visible = item.IsMohistVerify;
                rbHwatai.Checked = lblHwatai.Visible = item.IsTrustHwatai;
                rdlDealAccountingPayType.SelectedValue = ((int)item.PayToCompany).ToString();
                PaidType = item.RemittancePaidType;
                lblRemittanceTypeDesc.Text = Helper.GetLocalizedEnum(item.RemittancePaidType);
                switch (item.Invoice.Type)
                {
                    case InvoiceType.General:
                        rdlGeneral.Checked = true;
                        break;

                    case InvoiceType.DuplicateUniformInvoice:
                        rdlDuplicate.Checked = true;
                        rdlInTax.SelectedValue = item.Invoice.InTax;
                        break;

                    case InvoiceType.NonInvoice:
                        rdlNonInvoice.Checked = true;
                        break;

                    case InvoiceType.Other:
                        rdlInvoiceOther.Checked = true;
                        txtInvoiceOtherDesc.Text = item.Invoice.Name;
                        break;

                    case InvoiceType.EntertainmentTax:
                        rdlEntertainmentTax.Checked = true;
                        break;

                    default:
                        break;
                }
                rdlInvoiceFounder.SelectedValue = ((int)item.InvoiceFounderType).ToString();
                txtInvoiceFounderCompanyName.Text = item.InvoiceFounderCompanyName;
                txtInvoiceFounderCompanyID.Text = item.InvoiceFounderCompanyID;
                txtInvoiceFounderBossName.Text = item.InvoiceFounderBossName;
                ddlMultiDeal.SelectedValue = item.MultiDeal;
                txtWebUrl.Text = item.WebUrl;
                txtFBUrl.Text = item.FBUrl;
                txtPlurkUrl.Text = item.PlurkUrl;
                txtBlogUrl.Text = item.BlogUrl;
                txtOtherUrl.Text = item.OtherUrl;
                txtStoreName.Text = item.StoreName;
                var township = CityManager.TownShipGetById(item.AddressTownshipId);
                AddressCityId = township == null ? -1 : township.ParentId.Value;
                AddressTownshipId = township == null ? -1 : township.Id;
                txtSellerAddress.Text = item.SellerAddress;

                txtMrt.Text = item.Mrt;
                txtCar.Text = item.Car;
                txtBus.Text = item.Bus;
                txtVehicleInfo.Text = item.VehicleInfo;
                chkIsShowSpecailStore.Checked = item.IsShowSpecailStore;
                rdlReturnType.SelectedValue = ((int)item.ReturnType).ToString();
                chkIsNotAccessSms.Checked = item.IsNotAccessSms;
                txtOtherMemo.Text = item.OtherMemo;
                Contracts = item.AdditionalContracts;
                lblCreateId.Text = item.CreateId;
                lblModifyId.Text = item.ModifyId;
                lblModifyTime.Text = item.ModifyTime != null ? item.ModifyTime.Value.ToLocalTime().ToString() : string.Empty;
                lblBusinessHourTimeS.Text = txtBusinessHourTimeS.Text = item.BusinessHourTimeS != null ? item.BusinessHourTimeS.Value.ToLocalTime().ToString("yyyy/MM/dd") : string.Empty;

                #region 好康小提示

                hidProvisionResult.Value = new JsonSerializer().Serialize(
                    item.ProvisionList.Select(x => new
                    {
                        contentId = x.Key[0].ToString(),
                        descId = x.Key[1].ToString(),
                        desc = x.Value
                    })
                );

                #endregion 好康小提示

                #endregion 共同基本資料

                #region 檔次優惠內容

                #region 優惠內容

                repDeal.DataSource = item.Deals;
                repDeal.DataBind();

                #endregion 優惠內容

                switch (BusinessType)
                {
                    case BusinessType.Ppon:

                        #region 基本資料

                        // 僅限p好康設定:核銷方式、刷卡機、p好康優惠內容、訂位電話、營業時間、公休日、p好康憑證使用方式
                        VerifyType = item.PponVerifyType;
                        rdlIsEDC.SelectedValue = item.IsEDC ? "1" : "0";
                        chkIsKownEDC.Checked = item.IsKownEDC;
                        chkIsTaiShinEDC.Checked = item.IsKownTaiShinEDC;
                        txtEdcDesc1.Text = item.EdcDesc1;
                        txtEdcDesc2.Text = item.EdcDesc2;
                        txtEdcDesc3.Text = item.EdcDesc3;
                        txtEdcDesc4.Text = item.EdcDesc4;
                        txtEdcDesc5.Text = item.EdcDesc5;
                        txtReservationTel.Text = item.ReservationTel;
                        txtOpeningTime.Text = item.OpeningTime;
                        txtCloseDate.Text = item.CloseDate;

                        #endregion 基本資料

                        break;

                    case BusinessType.Product:

                        #region 基本資料

                        // 僅限p商品設定:退換貨聯絡人、退換貨聯絡人電話、運費代收、p商品優惠內容、商品配送資訊
                        txtReturnedPerson.Text = item.ReturnedPerson;
                        txtReturnedTel.Text = item.ReturnedTel;
                        txtReturnedEmail.Text = item.ReturnedEmail;
                        if (!string.IsNullOrEmpty(item.FreightType))
                        {
                            rdlFreightType.SelectedValue = item.FreightType;
                        }

                        if (item.FreightType.Equals("1"))
                        {
                            SetCheckBoxListSelectedValue(chklFreightCompany, item.FreightCompany);
                        }
                        txtProductFreightOtherCompany.Text = item.FreightOtherCompany;
                        chkIslandsFreight.Checked = item.IslandsFreight;
                        SetCheckBoxListSelectedValue(chklFreightMode, item.FreightMode);
                        rdlIsTakePicture.SelectedValue = item.IsTakePicture ? "1" : "0";
                        chkIsParallelProduct.Checked = item.IsParallelProduct;
                        if (item.IsExpiringItems)
                        {
                            chkIsExpiringItems.Checked = item.IsExpiringItems;
                            txtExpiringItemsDateStart.Text = item.ExpiringItemsDateStart;
                            txtExpiringItemsDateEnd.Text = item.ExpiringItemsDateEnd;
                        }
                        txtProductPreservation.Text = item.ProductPreservation;
                        txtProductUse.Text = item.ProductUse;
                        txtProductSpec.Text = item.ProductSpec;
                        txtProductIngredients.Text = item.ProductIngredients;
                        txtSanctionNumber.Text = item.SanctionNumber;
                        SetCheckBoxListSelectedValue(chklInspectRule, item.InspectRule); 
                        txtUseTimeStart.Text = item.ProductUseDateStartSet.ToString();
                        txtUseTimeEnd.Text = item.ProductUseDateEndSet.ToString();

                        RbShippingDateType = Convert.ToString((int)item.ShippingDateType);
                        txtShippingDate.Text = item.ShippingDate.ToString();

                        #endregion 基本資料

                        break;

                    case BusinessType.None:
                    default:
                        break;
                }

                if (item.IsTravelType)
                {
                    chkIsTravelType.Checked = true;
                    // 僅限p旅遊設定:民宿登記證字號、所在地警察局電話
                    txtHotelId.Text = item.HotelId;
                    txtPoliceTel.Text = item.PoliceTel;
                    txtHotelReservationTel.Text = item.HotelReservationTel;
                }

                #endregion 檔次優惠內容

                #region 特店

                txtPezStores.Text = item.PezStore;
                txtPezStoreNotice.Text = item.PezStoreNotice;
                txtCreateTimeS.Text = item.PezStoreDateStart != null ? item.PezStoreDateStart.Value.ToLocalTime().ToString("yyyy/MM/dd") : string.Empty;
                txtCreateTimeE.Text = item.PezStoreDateEnd != null ? item.PezStoreDateEnd.Value.ToLocalTime().ToString("yyyy/MM/dd") : string.Empty;

                #endregion 特店

                #region 歷史紀錄

                gvHistoryList.DataSource = item.HistoryList.OrderByDescending(x => x.ModifyTime);
                gvHistoryList.DataBind();

                #endregion 歷史紀錄

                #region 檔次標籤

                SetCheckBoxListSelectedValue(cbl_Icons, item.LabelIconList);
                switch (BusinessType)
                {
                    case BusinessType.Ppon:
                        SetCheckBoxListSelectedValue(cbl_couponDealTag, item.LabelTagList);
                        SetCheckBoxListSelectedValue(cbl_couponTravelDealTag, item.LabelTagList);
                        break;
                    case BusinessType.Product:
                        rdoShipType.SelectedValue = Convert.ToString((int)item.ShipType == 0 ? 1 : (int)item.ShipType);
                        SetCheckBoxListSelectedValue(cbl_deliveryTravelDealTag, item.LabelTagList);
                        break;
                }

                #endregion 檔次標籤

                #region APP限定檔次

                cbxAppLimitedEdition.Checked = item.IsAppLimitedEdition;

                #endregion

                #region 長期合約

                IsLongContract = item.IsLongContract;

                #endregion

                btnDelete.Visible = true;
            }
        }

        public void SetStoreList(IEnumerable<SalesStore> storeList)
        {
            gvStoreList.DataSource = storeList;
            gvStoreList.DataBind();

            ddlStoreList.Items.Clear();
            ddlStoreList.Items.Add(new ListItem("總店", string.Empty));
            foreach (SalesStore store in storeList)
            {
                ddlStoreList.Items.Add(new ListItem(store.BranchName, store.BranchName));
            }
        }

        public BusinessOrder GetBusinessOrderData(BusinessOrder previousItem, bool isCopy, string historyMemo)
        {
            BusinessOrder item = new BusinessOrder();

            // 若資料庫內的資料狀態已不在欲儲存的狀態(代表頁面未更新)，則應重新reload畫面
            if (!isCopy && !AuthorizationControls.ListItemStatus.Contains(previousItem.Status))
            {
                Response.Redirect("~/sal/BusinessOrderDetail.aspx?boid=" + BusinessOrderObjectId.ToString());
            }

            // 編輯控制項
            if (ActiveControls.IsEnableControls && !isCopy)
            {
                #region 資料取自畫面

                #region 共同資訊

                item.IsTravelType = chkIsTravelType.Checked;
                Guid sid = Guid.TryParse(hidSellerGuid.Value, out sid) ? sid : Guid.Empty;
                if (!sid.Equals(Guid.Empty))
                {
                    item.SellerGuid = sid;
                }

                item.PponCity = GetCheckBoxListSelectedValue(chklPponCity);

                if (item.PponCity.Contains(PponCityGroup.DefaultPponCityGroup.Travel.CityId.ToString()))
                {
                    item.TravelCategory = this.SelectedTravelCategoryId;
                }

                if (SelectedChannelCategories.Count > 0)
                {
                    item.SelectedChannelCategories = SelectedChannelCategories;
                    item.SelectedChannelSpecialRegionCategories = SelectedChannelSpecialRegionCategories;
                }
                else
                {
                    #region 舊City分類轉換SelectedChannelCategories

                    //處理城市的選取
                    List<PponCity> pponCities = new List<PponCity>();
                    foreach (PponCity pponCity in PponCityGroup.DefaultPponCityGroup)
                    {
                        if (item.PponCity.Contains(pponCity.CityId.ToString()))
                        {
                            pponCities.Add(pponCity);
                        }
                    }

                    Dictionary<int, List<int>> selectedChannelCategories = new Dictionary<int, List<int>>();
                    foreach (CategoryNode node in CategoryManager.PponChannelCategoryTree.CategoryNodes)
                    {
                        //有子項目
                        List<CategoryNode> subList = node.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                        if (subList != null && subList.Count > 0)
                        {
                            //旅遊度假需另外處理
                            if (node.CategoryId == PponCityGroup.DefaultPponCityGroup.Travel.CategoryId)
                            {
                                if (item.PponCity.Split(',').Contains(PponCityGroup.DefaultPponCityGroup.Travel.CityId.ToString()))
                                {
                                    //有勾選城市
                                    List<int> areaList = new List<int>();
                                    //檢查有無小區域的部分
                                    foreach (Category c in ViewPponDealManager.DefaultManager.TravelCategories)
                                    {
                                        if (item.TravelCategory != null && item.TravelCategory.Contains(Convert.ToInt32(c.Id)))
                                        {
                                            Category newTravelCategory = CategoryManager.GetPponChannelAreaByTravel(c.Id);
                                            if (newTravelCategory != null)
                                            {
                                                areaList.Add(newTravelCategory.Id);
                                            }
                                        }
                                    }
                                    selectedChannelCategories.Add(node.CategoryId, areaList);
                                }
                            }
                            else if (node.CategoryId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CategoryId)
                            {
                                if (pponCities.Any(x => x.CategoryId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CategoryId))
                                {
                                    selectedChannelCategories.Add(node.CategoryId, new List<int>());
                                }
                            }
                            else
                            {
                                //有子項目，判斷城市是否有勾選子項目的區塊，若有新增此區塊於新分類顯示
                                List<int> areaList = new List<int>();
                                foreach (var categoryNode in subList)
                                {
                                    if (pponCities.Any(x => x.CategoryId == categoryNode.CategoryId))
                                    {
                                        areaList.Add(categoryNode.CategoryId);
                                    }
                                }
                                if (areaList.Count > 0)
                                {
                                    selectedChannelCategories.Add(node.CategoryId, areaList);
                                }
                            }
                        }
                        else
                        {
                            //無子項目，直接判斷主區塊是否選取
                            if (pponCities.Any(x => x.CategoryId == node.CategoryId))
                            {
                                selectedChannelCategories.Add(node.CategoryId, new List<int>());
                            }
                        }
                    }
                    item.SelectedChannelCategories = selectedChannelCategories;

                    #endregion 舊City分類轉換SelectedChannelCategories
                }

                //DealCategory
                item.SelectedDealCategories = SelectedDealCategories;

                item.CommercialCategory = this.SelectedCommercialCategoryId;
                item.OtherPponCity = txtOtherPponCity.Text;
                item.AccBusinessGroupId = rdlAccBusinessGroup.SelectedValue;
                item.SalesName = txtSalesName.Text;
                item.SalesPhone = txtSalesPhone.Text;
                if (rdlSalesDept.SelectedItem != null)
                {
                    item.SalesDeptId = rdlSalesDept.SelectedValue;
                }
                if (rdlSalesDept.SelectedItem != null)
                {
                    item.SalesDeptName = rdlSalesDept.SelectedItem.Text;
                }
                item.Keywords = txtKeywords.Text;
                item.Important1 = txtImportant1.Text;
                item.Important2 = txtImportant2.Text;
                item.Important3 = txtImportant3.Text;
                item.Important4 = txtImportant4.Text;
                item.PictureSource = GetCheckBoxListSelectedValue(chklPictureSource);
                item.MarketEvent = txtMarketEvent.Text;
                item.SourceType = GetCheckBoxListSelectedValue(chklSourceType);
                item.BusinessType = BusinessType;
                bool isApplyNewPromotion = bool.TryParse(rdlIsApplyNewPromotion.SelectedValue, out isApplyNewPromotion) ? isApplyNewPromotion : false;
                item.IsApplyNewPromotion = isApplyNewPromotion;
                if (isApplyNewPromotion)
                {
                    bool isApplyNewPromotionConform = bool.TryParse(rdlApplyNewPromotion.SelectedValue, out isApplyNewPromotionConform) ? isApplyNewPromotionConform : false;
                    item.IsApplyNewPromotionConform = isApplyNewPromotionConform;
                }
                item.SellerName = txtSellerName.Text;
                item.CompanyName = txtCompanyName.Text;
                bool isPrivateContract = false;
                bool.TryParse(rdlPrivateContract.SelectedValue, out isPrivateContract);
                item.IsPrivateContract = isPrivateContract;
                item.GuiNumber = txtGuiNumber.Text;
                item.BossName = txtBossName.Text;
                item.PersonalId = txtPersonalId.Text;
                item.ContactPerson = txtContactPerson.Text;
                item.BossTel = txtBossTel.Text;
                item.BossPhone = txtBossPhone.Text;
                item.BossFax = txtBossFax.Text;
                item.BossEmail = txtBossEmail.Text;
                item.BossAddress = txtBossAddress.Text;
                item.AccountingName = txtAccountingName.Text;
                item.AccountingTel = txtAccountingTel.Text;
                item.AccountingEmail = txtAccountingEmail.Text;
                item.AccountingBankId = txtAccountingBankId.Text;
                item.AccountingBank = txtAccountingBankDesc.Text;
                item.AccountingBranchId = txtAccountingBranchId.Text;
                item.AccountingBranch = txtAccountingBranchDesc.Text;
                item.AccountingId = txtAccountingId.Text;
                item.Accounting = txtAccounting.Text;
                item.Account = txtAccount.Text;
                item.IsEntrustSell = chkIsEntrustSell.Checked;
                item.IsMohistVerify = rbIsMohist.Checked;
                item.IsTrustHwatai = rbHwatai.Checked;
                DealAccountingPayType payType = DealAccountingPayType.PayToCompany;
                DealAccountingPayType.TryParse(rdlDealAccountingPayType.SelectedValue, out payType);
                item.RemittancePaidType = PaidType;
                item.PayToCompany = payType;

                if (rdlDuplicate.Checked)
                {
                    item.Invoice.Type = InvoiceType.DuplicateUniformInvoice;
                    item.Invoice.Name = rdlDuplicate.Text;
                    item.Invoice.InTax = rdlInTax.SelectedValue;
                }
                else if (rdlGeneral.Checked)
                {
                    item.Invoice.Type = InvoiceType.General;
                    item.Invoice.Name = rdlGeneral.Text;
                }
                else if (rdlNonInvoice.Checked)
                {
                    item.Invoice.Type = InvoiceType.NonInvoice;
                    item.Invoice.Name = rdlNonInvoice.Text;
                }
                else if (rdlInvoiceOther.Checked)
                {
                    item.Invoice.Type = InvoiceType.Other;
                    item.Invoice.Name = txtInvoiceOtherDesc.Text;
                }
                else if (rdlEntertainmentTax.Checked)
                {
                    item.Invoice.Type = InvoiceType.EntertainmentTax;
                    item.Invoice.Name = rdlEntertainmentTax.Text;
                }

                InvoiceFounderType founderType = InvoiceFounderType.Company;
                InvoiceFounderType.TryParse(rdlInvoiceFounder.SelectedValue, out founderType);
                item.InvoiceFounderType = founderType;
                item.InvoiceFounderCompanyName = txtInvoiceFounderCompanyName.Text;
                item.InvoiceFounderCompanyID = txtInvoiceFounderCompanyID.Text;
                item.InvoiceFounderBossName = txtInvoiceFounderBossName.Text;
                item.MultiDeal = ddlMultiDeal.SelectedValue;
                item.WebUrl = txtWebUrl.Text;
                item.FBUrl = txtFBUrl.Text;
                item.PlurkUrl = txtPlurkUrl.Text;
                item.BlogUrl = txtBlogUrl.Text;
                item.OtherUrl = txtOtherUrl.Text;
                item.StoreName = txtStoreName.Text;
                item.AddressCityId = AddressCityId;
                item.AddressTownshipId = AddressTownshipId;
                item.SellerAddress = txtSellerAddress.Text;
                item.Mrt = txtMrt.Text;
                item.Car = txtCar.Text;
                item.Bus = txtBus.Text;
                item.VehicleInfo = txtVehicleInfo.Text;
                item.IsShowSpecailStore = chkIsShowSpecailStore.Checked;

                item.DealType = Int32.Parse(hdDealType2.Value);
                ReturnType returnType = ReturnType.Access;
                ReturnType.TryParse(rdlReturnType.SelectedValue, out returnType);
                item.ReturnType = returnType;
                item.IsNotAccessSms = chkIsNotAccessSms.Checked;
                item.OtherMemo = txtOtherMemo.Text;
                item.AdditionalContracts = Contracts;

                #region 好康小提示

                Dictionary<ObjectId[], string> provisionList = new Dictionary<ObjectId[], string>();
                var provisions = new JsonSerializer().Deserialize<dynamic>(hidProvisionResult.Value);
                if (provisions != null)
                {
                    foreach (var provi in provisions)
                    {
                        ObjectId contentId;
                        ObjectId.TryParse(provi.contentId.ToString(), out contentId);
                        ObjectId descId;
                        ObjectId.TryParse(provi.descId.ToString(), out descId);
                        if (!contentId.Equals(ObjectId.Empty) && !descId.Equals(ObjectId.Empty))
                        {
                            provisionList.Add(new ObjectId[] { contentId, descId }, provi.desc.ToString());
                        }
                    }
                    if (provisions.Count > 0 && provisionList.Count > 0)
                    {
                        item.ProvisionList = provisionList;
                    }
                    else
                    {
                        item.ProvisionList = previousItem.ProvisionList;
                    }
                }
                else
                {
                    item.ProvisionList = previousItem.ProvisionList;
                }

                #endregion 好康小提示

                #endregion 共同資訊

                #region 優惠內容

                var jsonDeals = new JsonSerializer().Deserialize<List<Deal>>(hdDeals.Value);
                if (jsonDeals != null)
                {
                    item.Deals = jsonDeals;
                }

                #endregion 優惠內容

                #region 檔次標籤

                item.LabelIconList = GetCheckBoxListSelectedValue(cbl_Icons);

                switch (BusinessType)
                {
                    case BusinessType.Ppon:
                        List<string> tagList = new List<string>();

                        if (!string.IsNullOrEmpty(cbl_couponDealTag.SelectedValue))
                        {
                            tagList.AddRange(GetCheckBoxListSelectedValue(cbl_couponDealTag).Split(','));
                        }
                        if (!string.IsNullOrEmpty(cbl_couponTravelDealTag.SelectedValue))
                        {
                            tagList.AddRange(GetCheckBoxListSelectedValue(cbl_couponTravelDealTag).Split(','));
                        }

                        if (tagList.Count > 0)
                        {
                            item.LabelTagList = string.Join(",", tagList);
                        }
                        break;
                    case BusinessType.Product:
                        item.ShipType = (ShipType)int.Parse(rdoShipType.SelectedValue);
                        item.LabelTagList = GetCheckBoxListSelectedValue(cbl_deliveryTravelDealTag);
                        break;
                }


                #endregion 檔次標籤

                #region 檔次優惠內容

                switch (BusinessType)
                {
                    case BusinessType.Ppon:

                        #region 基本資訊

                        // 好康設定(核銷方式、刷卡機、訂位電話、營業時間、公休日)
                        item.PponVerifyType = VerifyType;
                        item.IsEDC = rdlIsEDC.SelectedValue.Equals("1");
                        if (!item.IsEDC)
                        {
                            item.IsKownEDC = chkIsKownEDC.Checked;
                            if (item.IsKownEDC)
                            {
                                item.IsKownTaiShinEDC = chkIsTaiShinEDC.Checked;
                                if (item.IsKownTaiShinEDC)
                                {
                                    item.EdcDesc1 = txtEdcDesc1.Text;
                                    item.EdcDesc2 = txtEdcDesc2.Text;
                                    item.EdcDesc3 = txtEdcDesc3.Text;
                                    item.EdcDesc4 = txtEdcDesc4.Text;
                                    item.EdcDesc5 = txtEdcDesc5.Text;
                                }
                            }
                        }
                        item.ReservationTel = txtReservationTel.Text;
                        item.OpeningTime = txtOpeningTime.Text;
                        item.CloseDate = txtCloseDate.Text;

                        #endregion 基本資訊

                        break;

                    case BusinessType.Product:

                        #region 基本資訊

                        // 僅限p商品設定:退換貨聯絡人、退換貨聯絡人電話、運費代收、p商品優惠內容、商品配送資訊
                        item.ReturnedPerson = txtReturnedPerson.Text;
                        item.ReturnedTel = txtReturnedTel.Text;
                        item.ReturnedEmail = txtReturnedEmail.Text;
                        item.FreightType = rdlFreightType.SelectedValue;
                        item.FreightCompany = GetCheckBoxListSelectedValue(chklFreightCompany);

                        #endregion 基本資訊

                        #region 使用方式

                        item.FreightOtherCompany = txtProductFreightOtherCompany.Text;
                        item.IslandsFreight = chkIslandsFreight.Checked;
                        item.FreightMode = GetCheckBoxListSelectedValue(chklFreightMode);
                        item.IsTakePicture = rdlIsTakePicture.SelectedValue.Equals("1");
                        item.IsParallelProduct = chkIsParallelProduct.Checked;
                        item.IsExpiringItems = chkIsExpiringItems.Checked;
                        if (item.IsExpiringItems)
                        {
                            item.ExpiringItemsDateStart = txtExpiringItemsDateStart.Text;
                            item.ExpiringItemsDateEnd = txtExpiringItemsDateEnd.Text;
                        }
                        item.ProductPreservation = txtProductPreservation.Text;
                        item.ProductUse = txtProductUse.Text;
                        item.ProductSpec = txtProductSpec.Text;
                        item.ProductIngredients = txtProductIngredients.Text;
                        item.SanctionNumber = txtSanctionNumber.Text;
                        item.InspectRule = GetCheckBoxListSelectedValue(chklInspectRule);
                        int useTimeStart = int.TryParse(txtUseTimeStart.Text, out useTimeStart) ? useTimeStart : productUseStartAddDays;
                        item.ProductUseDateStartSet = useTimeStart;   
                        int useTimeEnd = int.TryParse(txtUseTimeEnd.Text, out useTimeEnd) ? useTimeEnd : productUseEndAddDays;
                        item.ProductUseDateEndSet = useTimeEnd;

                        ShippingDateType tmpdate = ShippingDateType.TryParse(Request["ShippingDateType"], out tmpdate) ? tmpdate : ShippingDateType.Normal;
                        item.ShippingDateType = tmpdate;
                        int useShippingDate = int.TryParse(txtShippingDate.Text, out useShippingDate) ? useShippingDate : ShippingDate;
                        item.ShippingDate = useShippingDate;

                        #endregion 使用方式

                        break;

                    case BusinessType.None:
                    default:
                        break;
                }
                if (chkIsTravelType.Checked)
                {
                    // 旅遊設定(民宿登記證字號、所在地警察局電話)
                    item.HotelId = txtHotelId.Text;
                    item.PoliceTel = txtPoliceTel.Text;
                    item.HotelReservationTel = txtHotelReservationTel.Text;
                }

                #endregion 檔次優惠內容

                #region 特店

                if (!string.IsNullOrEmpty(txtPezStores.Text.Trim()))
                {
                    item.PezStore = txtPezStores.Text;
                    item.PezStoreNotice = txtPezStoreNotice.Text;
                    DateTime dateStart;
                    DateTime.TryParse(txtCreateTimeS.Text, out dateStart);
                    item.PezStoreDateStart = dateStart;
                    DateTime dateEnd;
                    DateTime.TryParse(txtCreateTimeE.Text, out dateEnd);
                    item.PezStoreDateEnd = dateEnd;
                }

                #endregion 特店

                #region APP限定購買

                item.IsAppLimitedEdition = cbxAppLimitedEdition.Checked;

                #endregion

                #region 長期合約

                item.IsLongContract = IsLongContract;

                #endregion

                #endregion
            }
            else
            {
                #region 資料取自原來的資料(不具權限不應可異動控制項資料)

                #region 共同資訊

                item.IsTravelType = previousItem.IsTravelType;
                item.SellerGuid = previousItem.SellerGuid;

                item.PponCity = previousItem.PponCity;
                item.TravelCategory = previousItem.TravelCategory;
                if (previousItem.SelectedChannelCategories.Count > 0)
                {
                    item.SelectedChannelCategories = previousItem.SelectedChannelCategories;
                    item.SelectedChannelSpecialRegionCategories = previousItem.SelectedChannelSpecialRegionCategories;
                    item.SelectedChannelStandardRegionCategories = previousItem.SelectedChannelStandardRegionCategories;
                }
                else
                {
                    #region 舊City分類轉換SelectedChannelCategories

                    //處理城市的選取
                    List<PponCity> pponCities = new List<PponCity>();
                    foreach (PponCity pponCity in PponCityGroup.DefaultPponCityGroup)
                    {
                        if (previousItem.PponCity.Contains(pponCity.CityId.ToString()))
                        {
                            pponCities.Add(pponCity);
                        }
                    }

                    Dictionary<int, List<int>> selectedChannelCategories = new Dictionary<int, List<int>>();
                    foreach (CategoryNode node in CategoryManager.PponChannelCategoryTree.CategoryNodes)
                    {
                        //有子項目
                        List<CategoryNode> subList = node.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                        if (subList != null && subList.Count > 0)
                        {
                            //旅遊度假需另外處理
                            if (node.CategoryId == PponCityGroup.DefaultPponCityGroup.Travel.CategoryId)
                            {
                                if (previousItem.PponCity.Split(',').Contains(PponCityGroup.DefaultPponCityGroup.Travel.CityId.ToString()))
                                {
                                    //有勾選城市
                                    List<int> areaList = new List<int>();
                                    //檢查有無小區域的部分
                                    foreach (Category c in ViewPponDealManager.DefaultManager.TravelCategories)
                                    {
                                        if (previousItem.TravelCategory != null && previousItem.TravelCategory.Contains(Convert.ToInt32(c.Id)))
                                        {
                                            Category newTravelCategory = CategoryManager.GetPponChannelAreaByTravel(c.Id);
                                            if (newTravelCategory != null)
                                            {
                                                areaList.Add(newTravelCategory.Id);
                                            }
                                        }
                                    }
                                    selectedChannelCategories.Add(node.CategoryId, areaList);
                                }
                            }
                            else if (node.CategoryId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CategoryId)
                            {
                                if (pponCities.Any(x => x.CategoryId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CategoryId))
                                {
                                    selectedChannelCategories.Add(node.CategoryId, new List<int>());
                                }
                            }
                            else
                            {
                                //有子項目，判斷城市是否有勾選子項目的區塊，若有新增此區塊於新分類顯示
                                List<int> areaList = new List<int>();
                                foreach (var categoryNode in subList)
                                {
                                    if (pponCities.Any(x => x.CategoryId == categoryNode.CategoryId))
                                    {
                                        areaList.Add(categoryNode.CategoryId);
                                    }
                                }
                                if (areaList.Count > 0)
                                {
                                    selectedChannelCategories.Add(node.CategoryId, areaList);
                                }
                            }
                        }
                        else
                        {
                            //無子項目，直接判斷主區塊是否選取
                            if (pponCities.Any(x => x.CategoryId == node.CategoryId))
                            {
                                selectedChannelCategories.Add(node.CategoryId, new List<int>());
                            }
                        }
                    }
                    item.SelectedChannelCategories = selectedChannelCategories;

                    #endregion 舊City分類轉換SelectedChannelCategories
                }

                //DealCategory
                item.SelectedDealCategories = previousItem.SelectedDealCategories;

                item.CommercialCategory = previousItem.CommercialCategory;
                item.OtherPponCity = previousItem.OtherPponCity;
                item.AccBusinessGroupId = previousItem.AccBusinessGroupId;
                item.SalesName = previousItem.SalesName;
                item.SalesPhone = previousItem.SalesPhone;
                item.SalesDeptId = previousItem.SalesDeptId;
                item.SalesDeptName = previousItem.SalesDeptName;
                item.Keywords = previousItem.Keywords;
                item.Important1 = previousItem.Important1;
                item.Important2 = previousItem.Important2;
                item.Important3 = previousItem.Important3;
                item.Important4 = previousItem.Important4;
                item.SourceType = previousItem.SourceType;
                item.PictureSource = previousItem.PictureSource;
                item.MarketEvent = previousItem.MarketEvent;
                item.BusinessType = previousItem.BusinessType;
                item.IsApplyNewPromotion = previousItem.IsApplyNewPromotion; ;
                item.IsApplyNewPromotionConform = previousItem.IsApplyNewPromotionConform;
                item.SellerName = previousItem.SellerName;
                item.CompanyName = previousItem.CompanyName;
                item.IsPrivateContract = previousItem.IsPrivateContract;
                item.GuiNumber = previousItem.GuiNumber;
                item.BossName = previousItem.BossName;
                item.PersonalId = previousItem.PersonalId;
                item.ContactPerson = previousItem.ContactPerson;
                item.BossTel = previousItem.BossTel;
                item.BossPhone = previousItem.BossPhone;
                item.BossFax = previousItem.BossFax;
                item.BossEmail = previousItem.BossEmail;
                item.BossAddress = previousItem.BossAddress;
                item.AccountingName = previousItem.AccountingName;
                item.AccountingTel = previousItem.AccountingTel;
                item.AccountingEmail = previousItem.AccountingEmail;
                item.AccountingBankId = previousItem.AccountingBankId;
                item.AccountingBank = previousItem.AccountingBank;
                item.AccountingBranchId = previousItem.AccountingBranchId;
                item.AccountingBranch = previousItem.AccountingBranch;
                item.AccountingId = previousItem.AccountingId;
                item.Accounting = previousItem.Accounting;
                item.Account = previousItem.Account;
                item.IsEntrustSell = previousItem.IsEntrustSell;
                item.IsMohistVerify = previousItem.IsMohistVerify;
                item.IsTrustHwatai = previousItem.IsTrustHwatai;
                item.RemittancePaidType = previousItem.RemittancePaidType;
                item.PayToCompany = previousItem.PayToCompany;
                item.Invoice = previousItem.Invoice;
                item.InvoiceFounderCompanyName = previousItem.InvoiceFounderCompanyName;
                item.InvoiceFounderType = previousItem.InvoiceFounderType;
                item.InvoiceFounderCompanyID = previousItem.InvoiceFounderCompanyID;
                item.InvoiceFounderBossName = previousItem.InvoiceFounderBossName;
                item.MultiDeal = previousItem.MultiDeal;
                item.WebUrl = previousItem.WebUrl;
                item.FBUrl = previousItem.FBUrl;
                item.PlurkUrl = previousItem.PlurkUrl;
                item.BlogUrl = previousItem.BlogUrl;
                item.OtherUrl = previousItem.OtherUrl;
                item.StoreName = previousItem.StoreName;
                item.AddressCityId = previousItem.AddressCityId;
                item.AddressTownshipId = previousItem.AddressTownshipId;
                item.SellerAddress = previousItem.SellerAddress;
                item.Mrt = previousItem.Mrt;
                item.Car = previousItem.Car;
                item.Bus = previousItem.Bus;
                item.VehicleInfo = previousItem.VehicleInfo;
                item.IsShowSpecailStore = previousItem.IsShowSpecailStore;

                item.DealType = Int32.Parse(hdDealType2.Value);
                item.ReturnType = previousItem.ReturnType;
                item.IsNotAccessSms = previousItem.IsNotAccessSms;
                item.OtherMemo = previousItem.OtherMemo;
                item.AdditionalContracts = previousItem.AdditionalContracts;

                #region 好康小提示

                item.ProvisionList = previousItem.ProvisionList;

                #endregion 好康小提示

                #endregion 共同資訊

                #region 優惠內容

                item.Deals = previousItem.Deals;

                #endregion 優惠內容

                #region 檔次標籤

                item.LabelIconList = previousItem.LabelIconList;
                item.LabelTagList = previousItem.LabelTagList;
                item.ShipType = previousItem.ShipType;

                #endregion 檔次標籤

                #region 檔次優惠內容

                switch (BusinessType)
                {
                    case BusinessType.Ppon:

                        #region 基本資訊

                        // 好康設定(核銷方式、刷卡機、訂位電話、營業時間、公休日)
                        item.PponVerifyType = previousItem.PponVerifyType;
                        item.IsEDC = previousItem.IsEDC;
                        item.IsKownEDC = previousItem.IsKownEDC;
                        item.IsKownTaiShinEDC = previousItem.IsKownTaiShinEDC;
                        item.EdcDesc1 = previousItem.EdcDesc1;
                        item.EdcDesc2 = previousItem.EdcDesc2;
                        item.EdcDesc3 = previousItem.EdcDesc3;
                        item.EdcDesc4 = previousItem.EdcDesc4;
                        item.EdcDesc5 = previousItem.EdcDesc5;
                        item.ReservationTel = previousItem.ReservationTel;
                        item.OpeningTime = previousItem.OpeningTime;
                        item.CloseDate = previousItem.CloseDate;

                        #endregion 基本資訊

                        break;

                    case BusinessType.Product:

                        #region 基本資訊

                        // 僅限p商品設定:退換貨聯絡人、退換貨聯絡人電話、運費代收、p商品優惠內容、商品配送資訊
                        item.ReturnedPerson = previousItem.ReturnedPerson;
                        item.ReturnedTel = previousItem.ReturnedTel;
                        item.ReturnedEmail = previousItem.ReturnedEmail;
                        item.FreightType = previousItem.FreightType;
                        item.FreightCompany = previousItem.FreightCompany;

                        #endregion 基本資訊

                        #region 使用方式

                        item.FreightOtherCompany = previousItem.FreightOtherCompany;
                        item.IslandsFreight = previousItem.IslandsFreight;
                        item.FreightMode = previousItem.FreightMode;
                        item.IsTakePicture = previousItem.IsTakePicture;
                        item.IsParallelProduct = previousItem.IsParallelProduct;
                        item.IsExpiringItems = previousItem.IsExpiringItems;
                        item.ExpiringItemsDateStart = previousItem.ExpiringItemsDateStart;
                        item.ExpiringItemsDateEnd = previousItem.ExpiringItemsDateEnd;
                        item.ProductPreservation = previousItem.ProductPreservation;
                        item.ProductUse = previousItem.ProductUse;
                        item.ProductSpec = previousItem.ProductSpec;
                        item.ProductIngredients = previousItem.ProductIngredients;
                        item.SanctionNumber = previousItem.SanctionNumber;
                        item.InspectRule = previousItem.InspectRule;
                        item.ProductUseDateStartSet = previousItem.ProductUseDateStartSet;
                        item.ProductUseDateEndSet = previousItem.ProductUseDateEndSet;

                        item.ShippingDateType = previousItem.ShippingDateType;
                        item.ShippingDate = previousItem.ShippingDate;

                        #endregion 使用方式

                        break;

                    case BusinessType.None:
                    default:
                        break;
                }
                if (chkIsTravelType.Checked)
                {
                    // 旅遊設定(民宿登記證字號、所在地警察局電話)
                    item.HotelId = previousItem.HotelId;
                    item.PoliceTel = previousItem.PoliceTel;
                    item.HotelReservationTel = previousItem.HotelReservationTel;
                }

                #endregion 檔次優惠內容

                #region 雜誌

                item.CreditCardReaderId = previousItem.CreditCardReaderId;
                item.PerCustomerTrans = previousItem.PerCustomerTrans;
                item.PerCustomerTransOthers = previousItem.PerCustomerTransOthers;
                item.PerCustomerTransDesc = previousItem.PerCustomerTransDesc;
                item.SellerTemplateDesc = previousItem.SellerTemplateDesc;
                item.SellerTemplate = previousItem.SellerTemplate;
                item.SellerDescription = previousItem.SellerDescription;
                item.MagazineNotice1 = previousItem.MagazineNotice1;
                item.MagazineNotice2 = previousItem.MagazineNotice2;
                item.PreferentialItems = previousItem.PreferentialItems;

                item.Preferential = previousItem.Preferential;
                item.IsMagazineInfo = previousItem.IsMagazineInfo;

                #endregion 雜誌

                #region 特店

                item.PezStore = previousItem.PezStore;
                item.PezStoreNotice = previousItem.PezStoreNotice;
                item.PezStoreDateStart = previousItem.PezStoreDateStart;
                item.PezStoreDateEnd = previousItem.PezStoreDateEnd;

                #endregion 特店

                #region APP限定購買

                item.IsAppLimitedEdition = previousItem.IsAppLimitedEdition;

                #endregion

                #region 長期合約

                item.IsLongContract = previousItem.IsLongContract;

                #endregion

                #endregion
            }

            // 審核區域權限
            if (AuthorizationControls.IsAuthorizationGroup || BusinessOrderObjectId == ObjectId.Empty)
            {
                item.AuthorizationGroup = rdlAuthorizationGroup.SelectedValue;
            }
            else
            {
                item.AuthorizationGroup = previousItem.AuthorizationGroup;
            }

            // 賣家規模
            if (AuthorizationControls.isSellerLevelEnabled)
            {
                BusinessOrderSellerLevel level = BusinessOrderSellerLevel.TryParse(rdlSellerLevel.SelectedValue, out level) ? level : BusinessOrderSellerLevel.None;
                item.SellerLevel = level;
            }
            else
            {
                item.SellerLevel = previousItem.SellerLevel;
            }

            // 狀態 (異動狀態必須在該登入人員具有的狀態列表內，且當業務主管過審時，若審核區域沒有落該業務主管具有的權限區域內，則不能更新為別的狀態)
            BusinessOrderStatus status = BusinessOrderStatus.TryParse(ddlStatus.SelectedValue, out status) ? status : BusinessOrderStatus.Temp;
            if (ActiveControls.IsUpdateStautsEnabled && AuthorizationControls.ListItemStatus.Contains(status) &&
                (string.IsNullOrWhiteSpace(SalesMemberArea) || SalesMemberArea.Split(',').Contains(item.AuthorizationGroup)))
            {
                item.Status = status;
            }
            else
            {
                item.Status = previousItem.Status;
            }

            // 一律紀錄log
            item.IsNoteHistory = true;

            // 檔號
            if (ActiveControls.IsBidTextEnabled)
            {
                if (!string.IsNullOrEmpty(txtBid.Text.Trim()))
                {
                    Guid bid;
                    Guid.TryParse(txtBid.Text, out bid);
                    if (!bid.Equals(Guid.Empty))
                    {
                        item.BusinessHourGuid = bid;
                    }
                    else
                    {
                        int did;
                        int.TryParse(txtBid.Text, out did);
                        item.DealId = did;
                    }
                }
                int uniqueId = 0;
                int.TryParse(txtUniqueId.Text, out uniqueId);
                item.UniqueId = uniqueId;
            }
            else
            {
                item.BusinessHourGuid = previousItem.BusinessHourGuid;
                item.DealId = previousItem.DealId;
                item.UniqueId = previousItem.UniqueId;
            }

            // 賣家編號
            if (ActiveControls.IsSellerInfoEnabled)
            {
                item.SellerId = txtSellerId.Text;
            }
            else
            {
                item.SellerId = previousItem.SellerId;
            }

            // 合約送交
            if (ActiveControls.IsContractSendEnabled)
            {
                item.IsContractSend = chkIsContractSend.Checked;
                if (!chkIsContractSend.Checked)
                {
                    item.NoContractReason = txtNoContractReason.Text;
                }
            }
            else
            {
                item.IsContractSend = previousItem.IsContractSend;
                item.NoContractReason = previousItem.NoContractReason;
            }

            // 上檔日期
            if (AuthorizationControls.IsBusinessHourTimeSVisible)
            {
                DateTime businessHourTimeS;
                DateTime.TryParse(txtBusinessHourTimeS.Text, out businessHourTimeS);
                if (!businessHourTimeS.Equals(DateTime.MinValue))
                {
                    item.BusinessHourTimeS = businessHourTimeS;
                }
            }
            else
            {
                item.BusinessHourTimeS = previousItem.BusinessHourTimeS;
            }

            // 若狀態為已建檔，須更新最新狀態為已建檔
            if (previousItem.Status.Equals(BusinessOrderStatus.Create))
            {
                item.Status = BusinessOrderStatus.Create;
            }

            //接續憑證BID
            if (!string.IsNullOrEmpty(txtAncestorSequenceBid.Text.Trim()))
            {
                Guid sbid;
                Guid.TryParse(txtAncestorSequenceBid.Text, out sbid);
                if (!sbid.Equals(Guid.Empty))
                {
                    item.AncestorSequenceBusinessHourGuid = sbid;
                }
            }
            //接續數量BID
            if (!string.IsNullOrEmpty(txtAncestorQuantityBid.Text.Trim()))
            {
                Guid qbid;
                Guid.TryParse(txtAncestorQuantityBid.Text, out qbid);
                if (!qbid.Equals(Guid.Empty))
                {
                    item.AncestorQuantityBusinessHourGuid = qbid;
                }
            }


            if (!isCopy)
            {
                #region 歷史紀錄

                item.HistoryList = previousItem.HistoryList;
                Dictionary<string, string> list = new Dictionary<string, string>();

                // 狀態異動註記
                if (previousItem.Status != item.Status)
                {
                    list.Add("狀態", ddlStatus.SelectedItem.Text);
                }

                // 狀態"曾"送審過後則紀錄異動資訊
                if (previousItem.IsNoteHistory)
                {
                    #region 優惠內容

                    var deals = new JsonSerializer().Deserialize<List<Deal>>(hdDeals.Value);
                    if (deals != null)
                    {
                        item.Deals = deals;
                        for (int i = 0; i < item.Deals.Count; i++)
                        {
                            // 有檔次紀錄 且 有進貨價 或 有最高/最低購買數量者 皆儲存資料
                            if (previousItem.Deals.Count > i && ((!item.Deals[i].PurchasePrice.Equals("0") && !string.IsNullOrEmpty(item.Deals[i].PurchasePrice)) || (!string.IsNullOrEmpty(item.Deals[i].MinQuentity) && !item.Deals[i].MinQuentity.Equals("0")) || (!string.IsNullOrEmpty(item.Deals[i].MaxQuentity) && !item.Deals[i].MaxQuentity.Equals("0"))))
                            {
                                if (previousItem.Deals[i].ItemPrice != item.Deals[i].ItemPrice)
                                {
                                    list.Add("賣價" + paragraph[i], item.Deals[i].ItemPrice);
                                }

                                if (previousItem.Deals[i].OrignPrice != item.Deals[i].OrignPrice)
                                {
                                    list.Add("原價" + paragraph[i], item.Deals[i].OrignPrice);
                                }

                                if (previousItem.Deals[i].ItemName != item.Deals[i].ItemName)
                                {
                                    list.Add("優惠活動" + paragraph[i], item.Deals[i].ItemName);
                                }

                                if (previousItem.Deals[i].MultOption != item.Deals[i].MultOption)
                                {
                                    list.Add("款式選項" + paragraph[i], item.Deals[i].MultOption);
                                }

                                if (previousItem.Deals[i].IsReferenceAttachment != item.Deals[i].IsReferenceAttachment)
                                {
                                    list.Add("款式選項參見附件" + paragraph[i], item.Deals[i].IsReferenceAttachment ? "是" : "否");
                                }

                                if (previousItem.Deals[i].MinQuentity != item.Deals[i].MinQuentity)
                                {
                                    list.Add("最低門檻數量" + paragraph[i], item.Deals[i].MinQuentity);
                                }

                                if (previousItem.Deals[i].MaxQuentity != item.Deals[i].MaxQuentity)
                                {
                                    list.Add("最高購買數量" + paragraph[i], item.Deals[i].MaxQuentity);
                                }

                                if (previousItem.Deals[i].LimitPerQuentity != item.Deals[i].LimitPerQuentity)
                                {
                                    list.Add("每人限購" + paragraph[i], item.Deals[i].LimitPerQuentity);
                                }

                                if (previousItem.Deals[i].StartMonth != item.Deals[i].StartMonth)
                                {
                                    string startUseUnit = string.Empty;
                                    switch (item.Deals[i].StartUseUnit)
                                    {
                                        case StartUseUnitType.Day:
                                            startUseUnit = "天";
                                            break;

                                        case StartUseUnitType.Month:
                                            startUseUnit = "月";
                                            break;

                                        case StartUseUnitType.BeforeDay:
                                            startUseUnit = "前";
                                            break;

                                        default:
                                            break;
                                    }
                                    list.Add("活動使用有效時間" + paragraph[i], item.Deals[i].StartMonth + startUseUnit);
                                }

                                if (previousItem.Deals[i].IsRestrictionRule != item.Deals[i].IsRestrictionRule)
                                {
                                    list.Add("特殊使用日期限制" + paragraph[i], item.Deals[i].IsRestrictionRule ? "是" : "否");
                                }

                                if (previousItem.Deals[i].RestrictionRuleDesc != item.Deals[i].RestrictionRuleDesc)
                                {
                                    list.Add("特殊使用日期限制描述" + paragraph[i], item.Deals[i].RestrictionRuleDesc);
                                }

                                if (previousItem.Deals[i].Freight != item.Deals[i].Freight)
                                {
                                    list.Add("運費" + paragraph[i], item.Deals[i].Freight);
                                }

                                if (previousItem.Deals[i].NonFreightLimit != item.Deals[i].NonFreightLimit)
                                {
                                    list.Add("免運門檻" + paragraph[i], item.Deals[i].NonFreightLimit);
                                }

                                if (previousItem.Deals[i].IsIncludeFreight != item.Deals[i].IsIncludeFreight)
                                {
                                    list.Add("含運" + paragraph[i], item.Deals[i].IsIncludeFreight ? "是" : "否");
                                }

                                if (previousItem.Deals[i].IsFreightToCollect != item.Deals[i].IsFreightToCollect)
                                {
                                    list.Add("貨到付運費" + paragraph[i], item.Deals[i].IsFreightToCollect ? "是" : "否");
                                }

                                if (previousItem.Deals[i].IsNotDeliveryIslands != item.Deals[i].IsNotDeliveryIslands)
                                {
                                    list.Add("配送外島" + paragraph[i], item.Deals[i].IsNotDeliveryIslands ? "否" : "是");
                                }

                                if (previousItem.Deals[i].IsATM != item.Deals[i].IsATM)
                                {
                                    list.Add("是否使用ATM" + paragraph[i], item.Deals[i].IsATM ? "是" : "否");
                                }

                                if (previousItem.Deals[i].AtmQuantity != item.Deals[i].AtmQuantity)
                                {
                                    list.Add("ATM保留份數" + paragraph[i], item.Deals[i].AtmQuantity);
                                }

                                if (previousItem.Deals[i].Commission != item.Deals[i].Commission)
                                {
                                    list.Add("發票對開" + paragraph[i], item.Deals[i].Commission.Equals(0) ? "否" : "是，佣金為$" + item.Deals[i].Commission);
                                }

                                if (previousItem.Deals[i].PurchasePrice != item.Deals[i].PurchasePrice)
                                {
                                    list.Add("進貨價格" + paragraph[i], item.Deals[i].PurchasePrice);
                                }

                                if (previousItem.Deals[i].BookingSystemType != item.Deals[i].BookingSystemType)
                                {
                                    list.Add("配合預約管理" + paragraph[i], GetBookingSystemType((BookingType)item.Deals[i].BookingSystemType));
                                }

                                if (previousItem.Deals[i].AdvanceReservationDays != item.Deals[i].AdvanceReservationDays)
                                {
                                    list.Add("預約管理-消費者提早預約天數" + paragraph[i], item.Deals[i].AdvanceReservationDays.ToString());
                                }

                                if (previousItem.Deals[i].CouponUsers != item.Deals[i].CouponUsers)
                                {
                                    if (item.Deals[i].CouponUsers == 1)
                                    {
                                        list.Add("預約管理-優惠人數" + paragraph[i], "單人優惠");
                                    }
                                    else
                                    {
                                        list.Add("預約管理-優惠人數" + paragraph[i], "多人優惠(" + item.Deals[i].CouponUsers + "人享用一張憑證)");
                                    }
                                }

                                if (previousItem.Deals[i].IsReserveLock != item.Deals[i].IsReserveLock)
                                {
                                    list.Add("住宿類，商家系統提供「鎖定憑證」功能" + paragraph[i], (item.Deals[i].IsReserveLock) ? "是" : "否");
                                }

                                

                            }
                        }
                    }

                    #endregion 優惠內容

                    switch (BusinessType)
                    {
                        case BusinessType.Ppon:

                            #region 基本資訊

                            // 僅限p好康設定:核銷方式、刷卡機、p好康優惠內容、訂位電話、營業時間、公休日、p好康憑證使用方式
                            if (previousItem.PponVerifyType != item.PponVerifyType)
                            {
                                list.Add("核銷方式", Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, item.PponVerifyType));
                            }

                            if (previousItem.IsEDC != item.IsEDC)
                            {
                                list.Add("店內是否有刷卡機", rdlIsEDC.SelectedItem.Text);
                            }

                            if (previousItem.IsKownEDC != item.IsKownEDC)
                            {
                                list.Add("有裝設刷卡機意願", chkIsKownEDC.Checked ? "是" : "否");
                            }

                            if (previousItem.IsKownTaiShinEDC != item.IsKownTaiShinEDC)
                            {
                                list.Add("是否想了解台新銀行刷卡機之優惠", chkIsTaiShinEDC.Checked ? "是" : "否");
                            }

                            if (previousItem.EdcDesc1 != item.EdcDesc1)
                            {
                                list.Add("每月平均營業額", txtEdcDesc1.Text);
                            }

                            if (previousItem.EdcDesc2 != item.EdcDesc2)
                            {
                                list.Add("每月平均刷卡量", txtEdcDesc2.Text);
                            }

                            if (previousItem.EdcDesc3 != item.EdcDesc3)
                            {
                                list.Add("原配合銀行", txtEdcDesc3.Text);
                            }

                            if (previousItem.EdcDesc4 != item.EdcDesc4)
                            {
                                list.Add("原配合銀行手續費", txtEdcDesc4.Text);
                            }

                            if (previousItem.EdcDesc5 != item.EdcDesc5)
                            {
                                list.Add("方便聯絡時間/聯絡人", txtEdcDesc5.Text);
                            }

                            if (previousItem.ReservationTel != item.ReservationTel)
                            {
                                list.Add("訂位電話", txtReservationTel.Text);
                            }

                            if (previousItem.OpeningTime != item.OpeningTime)
                            {
                                list.Add("營業時間", txtOpeningTime.Text);
                            }

                            if (previousItem.CloseDate != item.CloseDate)
                            {
                                list.Add("公休日", txtCloseDate.Text);
                            }

                            #endregion 基本資訊

                            break;

                        case BusinessType.Product:

                            #region 基本資訊

                            // 僅限p商品設定:退換貨聯絡人、退換貨聯絡人電話、運費代收、p商品優惠內容、商品配送資訊
                            if (previousItem.ReturnedPerson != item.ReturnedPerson)
                            {
                                list.Add("退換貨連絡人", txtReturnedPerson.Text);
                            }

                            if (previousItem.ReturnedTel != item.ReturnedTel)
                            {
                                list.Add("退換貨連絡人電話", txtReturnedTel.Text);
                            }

                            if (previousItem.ReturnedEmail != item.ReturnedEmail)
                            {
                                list.Add("退換貨連絡電子信箱", txtReturnedEmail.Text);
                            }

                            if (previousItem.FreightType != item.FreightType)
                            {
                                list.Add("運費代收單位", rdlFreightType.SelectedItem.Text);
                            }

                            if (previousItem.FreightCompany != item.FreightCompany)
                            {
                                list.Add("公司配合之貨運公司", GetCheckBoxListSelectedText(chklFreightCompany));
                            }

                            #endregion 基本資訊

                            #region 使用方式

                            if (previousItem.FreightOtherCompany != item.FreightOtherCompany)
                            {
                                list.Add("貨運公司", txtProductFreightOtherCompany.Text);
                            }

                            if (previousItem.IslandsFreight != item.IslandsFreight)
                            {
                                list.Add("離島地區(金/馬/澎)是否配送", chkIslandsFreight.Checked ? "是" : "否");
                            }

                            if (previousItem.FreightMode != item.FreightMode)
                            {
                                list.Add("配送方式", GetCheckBoxListSelectedText(chklFreightMode));
                            }

                            if (previousItem.IsTakePicture != item.IsTakePicture)
                            {
                                list.Add("商品瑕疵證明", rdlIsTakePicture.SelectedItem.Text);
                            }

                            if (previousItem.IsParallelProduct != item.IsParallelProduct)
                            {
                                list.Add("平行輸入", chkIsParallelProduct.Checked ? "是" : "否");
                            }

                            if (previousItem.IsExpiringItems != item.IsExpiringItems)
                            {
                                list.Add("即期品", chkIsExpiringItems.Checked ? "是" : "否");
                            }

                            if (previousItem.ExpiringItemsDateStart != item.ExpiringItemsDateStart)
                            {
                                list.Add("有效期限(起)", txtExpiringItemsDateStart.Text);
                            }

                            if (previousItem.ExpiringItemsDateEnd != item.ExpiringItemsDateEnd)
                            {
                                list.Add("有效期限(迄)", txtExpiringItemsDateEnd.Text);
                            }

                            if (previousItem.ProductPreservation != item.ProductPreservation)
                            {
                                list.Add("商品保存方式 / 期限", txtProductPreservation.Text);
                            }

                            if (previousItem.ProductUse != item.ProductUse)
                            {
                                list.Add("功能 / 使用方法", txtProductUse.Text);
                            }

                            if (previousItem.ProductSpec != item.ProductSpec)
                            {
                                list.Add("商品規格", txtProductSpec.Text);
                            }

                            if (previousItem.ProductIngredients != item.ProductIngredients)
                            {
                                list.Add("商品主要成份", txtProductIngredients.Text);
                            }

                            if (previousItem.SanctionNumber != item.SanctionNumber)
                            {
                                list.Add("商品核準字號", txtSanctionNumber.Text);
                            }

                            if (previousItem.InspectRule != item.InspectRule)
                            {
                                list.Add("商品檢驗規定", GetCheckBoxListSelectedText(chklInspectRule));
                            }

                            if (previousItem.ProductUseDateStartSet != item.ProductUseDateStartSet || previousItem.ProductUseDateEndSet != item.ProductUseDateEndSet)
                            {
                                list.Add("出貨日設定", string.Format("最早出貨日：上檔後+{0}個工作日；最晚出貨日：統一結檔後+{1}個工作日。", txtUseTimeStart.Text, txtUseTimeEnd.Text));
                            }

                            if (previousItem.ShippingDate != item.ShippingDate)
                            {
                                list.Add("出貨日設定", string.Format("訂單成立後{0}個工作日內出貨完畢", txtShippingDate.Text));
                            }


                            #endregion 使用方式

                            break;

                        case BusinessType.None:
                        default:
                            break;
                    }

                    if (item.IsTravelType)
                    {
                        // 僅限p旅遊設定:民宿登記證字號、所在地警察局電話
                        if (previousItem.HotelId != item.HotelId)
                        {
                            list.Add("民宿登記證字號", txtHotelId.Text);
                        }

                        if (previousItem.PoliceTel != item.PoliceTel)
                        {
                            list.Add("所在地警察局電話", txtPoliceTel.Text);
                        }

                        if (previousItem.HotelReservationTel != item.HotelReservationTel)
                        {
                            list.Add("訂房電話", txtHotelReservationTel.Text);
                        }
                    }

                    #region 其他共同資訊

                    if (previousItem.SellerId != item.SellerId)
                    {
                        list.Add("賣家編號", txtSellerId.Text);
                    }

                    if ((previousItem.BusinessHourTimeS != null ? previousItem.BusinessHourTimeS.Value.ToLocalTime().Date : previousItem.BusinessHourTimeS) !=
                        (item.BusinessHourTimeS != null ? item.BusinessHourTimeS.Value.ToLocalTime().Date : item.BusinessHourTimeS))
                    {
                        list.Add("上檔日期", txtBusinessHourTimeS.Text);
                    }

                    if (previousItem.IsContractSend != item.IsContractSend)
                    {
                        list.Add("合約已送交", chkIsContractSend.Checked ? "是" : "否");
                    }

                    if (!chkIsContractSend.Checked)
                    {
                        if (previousItem.NoContractReason != item.NoContractReason)
                        {
                            list.Add("合約未回理由", txtNoContractReason.Text);
                        }
                    }

                    if (previousItem.BusinessType != item.BusinessType)
                    {
                        list.Add("檔次類型", BusinessType.ToString());
                    }

                    if (previousItem.IsTravelType != item.IsTravelType)
                    {
                        list.Add("旅遊類型", chkIsTravelType.Checked ? "是" : "否");
                    }

                    if (previousItem.PponCity != item.PponCity)
                    {
                        list.Add("活動區域", GetCheckBoxListSelectedText(chklPponCity));
                    }

                    if (previousItem.TravelCategory != item.TravelCategory)
                    {
                        if (previousItem.TravelCategory == null || item.TravelCategory == null ||
                            !previousItem.TravelCategory.SequenceEqual(item.TravelCategory))
                        {
                            list.Add("旅遊區域", GetCheckBoxListSelectedText(chklTravelCategory));
                        }
                    }

                    string changeCommercialCategory = string.Empty;
                    if (previousItem.CommercialCategory != item.CommercialCategory)
                    {
                        for (int i = 0; i < rptCommercialCategory.Items.Count; i++)
                        {
                            CheckBoxList chklCommercialCategory = (CheckBoxList)rptCommercialCategory.Items[i].FindControl("chklCommercialCategory");
                            changeCommercialCategory += "、" + GetCheckBoxListSelectedText(chklCommercialCategory);
                        }
                    }

                    if (previousItem.OtherPponCity != item.OtherPponCity)
                    {
                        list.Add("其他活動區域", txtOtherPponCity.Text);
                    }

                    if (previousItem.AuthorizationGroup != item.AuthorizationGroup)
                    {
                        list.Add("審核權限設定", rdlAuthorizationGroup.SelectedItem.Text);
                    }

                    if (previousItem.SellerLevel != item.SellerLevel && rdlSellerLevel.SelectedItem != null)
                    {
                        list.Add("賣家規模", rdlSellerLevel.SelectedItem.Text);
                    }

                    if (previousItem.AccBusinessGroupId != item.AccBusinessGroupId)
                    {
                        list.Add("館別", rdlAccBusinessGroup.SelectedItem.Text);
                    }

                    if (previousItem.SalesName != item.SalesName)
                    {
                        list.Add("負責業務", txtSalesName.Text);
                    }

                    if (previousItem.SalesPhone != item.SalesPhone)
                    {
                        list.Add("業務電話", txtSalesPhone.Text);
                    }

                    if (previousItem.Keywords != item.Keywords)
                    {
                        list.Add("搜尋關鍵字", txtKeywords.Text);
                    }

                    if (previousItem.Important1 != item.Important1)
                    {
                        list.Add("建議文案重點1", txtImportant1.Text);
                    }

                    if (previousItem.Important2 != item.Important2)
                    {
                        list.Add("建議文案重點2", txtImportant2.Text);
                    }

                    if (previousItem.Important3 != item.Important3)
                    {
                        list.Add("建議文案重點3", txtImportant3.Text);
                    }

                    if (previousItem.Important4 != item.Important4)
                    {
                        list.Add("建議文案重點4", txtImportant4.Text);
                    }

                    if (previousItem.PictureSource != item.PictureSource)
                    {
                        list.Add("照片來源", GetCheckBoxListSelectedText(chklPictureSource));
                    }

                    if (previousItem.MarketEvent != item.MarketEvent)
                    {
                        list.Add("行銷活動", txtMarketEvent.Text);
                    }

                    if (previousItem.SourceType != item.SourceType)
                    {
                        list.Add("名單來源", GetCheckBoxListSelectedText(chklSourceType));
                    }

                    if (previousItem.IsApplyNewPromotion != item.IsApplyNewPromotion)
                    {
                        list.Add("行銷資源申請", rdlIsApplyNewPromotion.SelectedItem.Text);
                    }

                    if (item.IsApplyNewPromotion)
                    {
                        if (previousItem.IsApplyNewPromotionConform != item.IsApplyNewPromotionConform)
                        {
                            list.Add("行銷資源申請資格", rdlApplyNewPromotion.SelectedItem.Text);
                        }
                    }

                    if (previousItem.SellerName != item.SellerName)
                    {
                        list.Add("品牌名稱", txtSellerName.Text);
                    }

                    if (previousItem.CompanyName != item.CompanyName)
                    {
                        list.Add("簽約公司名稱", txtCompanyName.Text);
                    }

                    if (previousItem.GuiNumber != item.GuiNumber)
                    {
                        list.Add("統一編號", txtGuiNumber.Text);
                    }

                    if (previousItem.BossName != item.BossName)
                    {
                        list.Add("負責人", txtBossName.Text);
                    }

                    if (previousItem.PersonalId != item.PersonalId)
                    {
                        list.Add("身分證字號", txtPersonalId.Text);
                    }

                    if (previousItem.ContactPerson != item.ContactPerson)
                    {
                        list.Add("聯絡人", txtContactPerson.Text);
                    }

                    if (previousItem.BossTel != item.BossTel)
                    {
                        list.Add("行動電話", txtBossTel.Text);
                    }

                    if (previousItem.BossPhone != item.BossPhone)
                    {
                        list.Add("連絡電話", txtBossPhone.Text);
                    }

                    if (previousItem.BossFax != item.BossFax)
                    {
                        list.Add("傳真電話", txtBossFax.Text);
                    }

                    if (previousItem.BossEmail != item.BossEmail)
                    {
                        list.Add("電子信箱", txtBossEmail.Text);
                    }

                    if (previousItem.BossAddress != item.BossAddress)
                    {
                        list.Add("簽約公司地址", txtBossAddress.Text);
                    }

                    if (previousItem.AccountingName != item.AccountingName)
                    {
                        list.Add("帳務連絡人", txtAccountingName.Text);
                    }

                    if (previousItem.AccountingTel != item.AccountingTel)
                    {
                        list.Add("帳務連絡人電話", txtAccountingTel.Text);
                    }

                    if (previousItem.AccountingEmail != item.AccountingEmail)
                    {
                        list.Add("帳務聯絡人電子信箱", txtAccountingEmail.Text);
                    }

                    if (previousItem.AccountingBank != item.AccountingBank)
                    {
                        list.Add("匯款銀行", txtAccountingBankId.Text + " " + txtAccountingBankDesc.Text);
                    }

                    if (previousItem.AccountingBranch != item.AccountingBranch)
                    {
                        list.Add("分 行", txtAccountingBranchId.Text + " " + txtAccountingBranchDesc.Text);
                    }

                    if (previousItem.AccountingId != item.AccountingId)
                    {
                        list.Add("統一編號／身份證字號", txtAccountingId.Text);
                    }

                    if (previousItem.Accounting != item.Accounting)
                    {
                        list.Add("戶 名", txtAccounting.Text);
                    }

                    if (previousItem.Account != item.Account)
                    {
                        list.Add("帳 號", txtAccount.Text);
                    }

                    if (previousItem.ShipType != item.ShipType)
                    {
                        list.Add("出 貨", Helper.GetLocalizedEnum(item.ShipType));
                    }

                    if (previousItem.LabelIconList != item.LabelIconList)
                    {
                        list.Add("Icon標籤", GetCheckBoxListSelectedText(cbl_Icons));
                    }

                    if (previousItem.IsAppLimitedEdition != item.IsAppLimitedEdition)
                    {
                        list.Add("APP限定購買", cbxAppLimitedEdition.Checked ? "是" : "否");
                    }

                    if (previousItem.IsLongContract != item.IsLongContract)
                    {
                        list.Add("長期合約", IsLongContract ? "是" : "否");
                    }

                    if (previousItem.LabelTagList != item.LabelTagList)
                    {
                        List<string> dealTagList = new List<string>();

                        if (!string.IsNullOrEmpty(cbl_deliveryTravelDealTag.SelectedValue))
                        {
                            dealTagList.AddRange(GetCheckBoxListSelectedText(cbl_deliveryTravelDealTag).Split('、'));
                        }
                        if (!string.IsNullOrEmpty(cbl_couponDealTag.SelectedValue))
                        {
                            dealTagList.AddRange(GetCheckBoxListSelectedText(cbl_couponDealTag).Split('、'));
                        }
                        if (!string.IsNullOrEmpty(cbl_couponTravelDealTag.SelectedValue))
                        {
                            dealTagList.AddRange(GetCheckBoxListSelectedText(cbl_couponTravelDealTag).Split('、'));
                        }

                        list.Add("Tag標籤", string.Join("、", dealTagList));
                    }

                    if (previousItem.IsEntrustSell != item.IsEntrustSell)
                    {
                        list.Add("設定為代收轉付", chkIsEntrustSell.Checked ? "是" : "否");
                    }

                    if (previousItem.IsMohistVerify != item.IsMohistVerify)
                    {
                        list.Add("配合墨攻核銷", rbIsMohist.Checked ? "是" : "否");
                    }

                    if (previousItem.IsTrustHwatai != item.IsTrustHwatai)
                    {
                        list.Add("信託華泰銀行", rbHwatai.Checked ? "是" : "否");
                    }

                    if (previousItem.PayToCompany != item.PayToCompany)
                    {
                        list.Add("匯款資料", rdlDealAccountingPayType.SelectedItem.Text);
                    }

                    if (previousItem.InvoiceFounderType != item.InvoiceFounderType)
                    {
                        list.Add("發票開立人-類型", Helper.GetLocalizedEnum(item.InvoiceFounderType));
                    }

                    if (previousItem.InvoiceFounderCompanyName != item.InvoiceFounderCompanyName)
                    {
                        list.Add("發票開立人-公司名稱", txtInvoiceFounderCompanyName.Text);
                    }

                    if (previousItem.InvoiceFounderCompanyID != item.InvoiceFounderCompanyID)
                    {
                        list.Add("發票開立人-統編 / ID", txtInvoiceFounderCompanyID.Text);
                    }

                    if (previousItem.InvoiceFounderBossName != item.InvoiceFounderBossName)
                    {
                        list.Add("發票開立人-負責人", txtInvoiceFounderBossName.Text);
                    }

                    if (previousItem.RemittancePaidType != item.RemittancePaidType)
                    {
                        list.Add("出帳方式", Helper.GetLocalizedEnum(item.RemittancePaidType));
                    }

                    if (previousItem.Invoice.Type != item.Invoice.Type || previousItem.Invoice.InTax != item.Invoice.InTax)
                    {
                        switch (item.Invoice.Type)
                        {
                            case InvoiceType.General:
                                list.Add("可提供憑證", rdlGeneral.Text);
                                break;

                            case InvoiceType.DuplicateUniformInvoice:
                                list.Add("可提供憑證", rdlDuplicate.Text);
                                if (previousItem.Invoice.InTax != item.Invoice.InTax)
                                {
                                    list.Add("稅額", rdlInTax.SelectedItem.Text);
                                }
                                break;

                            case InvoiceType.NonInvoice:
                                list.Add("可提供憑證", rdlNonInvoice.Text);
                                break;

                            case InvoiceType.Other:
                                list.Add("可提供憑證", rdlInvoiceOther.Text);
                                break;

                            case InvoiceType.EntertainmentTax:
                                list.Add("可提供憑證", rdlEntertainmentTax.Text);
                                break;

                            default:
                                break;
                        }
                    }

                    if (previousItem.MultiDeal != item.MultiDeal)
                    {
                        list.Add("檔次狀況", ddlMultiDeal.SelectedItem.Text);
                    }

                    if (previousItem.WebUrl != item.WebUrl)
                    {
                        list.Add("網站-官網", txtWebUrl.Text);
                    }

                    if (previousItem.FBUrl != item.FBUrl)
                    {
                        list.Add("網站-FaceBook網址", txtFBUrl.Text);
                    }

                    if (previousItem.PlurkUrl != item.PlurkUrl)
                    {
                        list.Add("網站-Plurk網址", txtPlurkUrl.Text);
                    }

                    if (previousItem.BlogUrl != item.BlogUrl)
                    {
                        list.Add("網站-部落格網址", txtBlogUrl.Text);
                    }

                    if (previousItem.OtherUrl != item.OtherUrl)
                    {
                        list.Add("網站-其他網址", txtOtherUrl.Text);
                    }

                    if (previousItem.StoreName != item.StoreName)
                    {
                        list.Add("店名", txtStoreName.Text);
                    }

                    if (previousItem.SellerAddress != item.SellerAddress || previousItem.AddressCityId != item.AddressCityId || previousItem.AddressTownshipId != item.AddressTownshipId)
                    {
                        list.Add("店址", CityManager.CityTownShopStringGet(item.AddressTownshipId) + txtSellerAddress.Text);
                    }

                    if (previousItem.Mrt != item.Mrt)
                    {
                        list.Add("交通資訊-捷運", txtMrt.Text);
                    }

                    if (previousItem.Car != item.Car)
                    {
                        list.Add("交通資訊-開車", txtCar.Text);
                    }

                    if (previousItem.Bus != item.Bus)
                    {
                        list.Add("交通資訊-公車", txtBus.Text);
                    }

                    if (previousItem.VehicleInfo != item.VehicleInfo)
                    {
                        list.Add("交通資訊-其他", txtVehicleInfo.Text);
                    }

                    if (previousItem.IsShowSpecailStore != item.IsShowSpecailStore)
                    {
                        list.Add("特選店家不露出", chkIsShowSpecailStore.Checked ? "是" : "否");
                    }

                    if (string.Join(",", previousItem.ProvisionList.Values) != string.Join(",", item.ProvisionList.Values))
                    {
                        string provision = string.Empty;
                        List<string> itemList = item.ProvisionList.Values.ToList();
                        for (int i = 0; i < itemList.Count; i++)
                        {
                            provision += "(" + (i + 1) + "). " + itemList[i] + " ";
                        }
                        list.Add("好康條款", provision);
                    }

                    if (previousItem.DealType != item.DealType)
                    {
                        list.Add("銷售類別分析", hdDealType2.Value);
                    }

                    if (previousItem.ReturnType != item.ReturnType)
                    {
                        list.Add("檔次退貨方式", rdlReturnType.SelectedItem.Text);
                    }

                    if (previousItem.IsNotAccessSms != item.IsNotAccessSms)
                    {
                        list.Add("憑證使用方式", chkIsNotAccessSms.Checked ? "是" : "否");
                    }

                    if (previousItem.OtherMemo != item.OtherMemo)
                    {
                        list.Add("其他附註說明", txtOtherMemo.Text);
                    }

                    if (previousItem.PezStore != item.PezStore)
                    {
                        list.Add("特店折扣", txtPezStores.Text);
                    }

                    if (previousItem.PezStoreNotice != item.PezStoreNotice)
                    {
                        list.Add("特店折扣注意事項", txtPezStoreNotice.Text);
                    }

                    if (item.PezStoreDateStart != null)
                    {
                        DateTime previousDateS = DateTime.MinValue;
                        if (previousItem.PezStoreDateStart != null)
                        {
                            DateTime.TryParse(previousItem.PezStoreDateStart.Value.ToLocalTime().ToString(), out previousDateS);
                        }

                        DateTime previousDateE = DateTime.MinValue;
                        if (previousItem.PezStoreDateStart != null)
                        {
                            DateTime.TryParse(previousItem.PezStoreDateEnd.Value.ToLocalTime().ToString(), out previousDateE);
                        }

                        if (previousDateS != item.PezStoreDateStart || previousDateE != item.PezStoreDateEnd)
                        {
                            list.Add("特店折扣合約起日", txtCreateTimeS.Text + "~" + txtCreateTimeE.Text);
                        }
                    }

                    if (!string.IsNullOrEmpty(txtHistory.Text.Trim()))
                    {
                        // 免稅註記 || 出帳方式註記
                        if (CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.TaxFreeNotice)
                            || CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.RemittanceNotice))
                        {
                            item.HistoryList.Add(new History() { Changeset = "[財會]" + txtHistory.Text, ModifyId = UserId, ModifyTime = DateTime.Now, IsImportant = true });
                        }
                        else
                        {
                            list.Add("其他異動附註", txtHistory.Text);
                        }
                    }

                    var previousItemNotInItem = previousItem.SelectedDealCategories.Except(item.SelectedDealCategories).ToList();
                    var itemNotInPreviousItem = item.SelectedDealCategories.Except(previousItem.SelectedDealCategories).ToList();
                    if (previousItemNotInItem.Any() || itemNotInPreviousItem.Any())
                    {
                        var jsonS = new JsonSerializer();
                        list.Add("檔次分類設定", "Before:" + jsonS.Serialize(previousItem.SelectedDealCategories) + "=> After:" + jsonS.Serialize(item.SelectedDealCategories));
                    }


                    #endregion 其他共同資訊
                }

                // 歷史資料
                History history = GetHistoryList(list, historyMemo);
                if (history != null)
                {
                    item.HistoryList.Add(history);
                }

                #endregion 歷史紀錄
            }
            return item;
        }

        public void SetSellerData(Seller seller)
        {
            txtSellerName.Text = seller.SellerName.Replace("17P好康-", string.Empty);
            txtCompanyName.Text = seller.CompanyName;
            txtSellerId.Text = seller.SellerId;
            hidSellerGuid.Value = seller.Guid.ToString();
            txtBossName.Text = seller.SellerBossName;
            txtContactPerson.Text = seller.SellerContactPerson;
            txtBossTel.Text = seller.SellerMobile;
            txtBossEmail.Text = seller.SellerEmail;
            txtBossFax.Text = seller.SellerFax;
            txtReturnedPerson.Text = seller.ReturnedPersonName;
            txtReturnedTel.Text = seller.ReturnedPersonTel;
            txtReturnedEmail.Text = seller.ReturnedPersonEmail;
            txtGuiNumber.Text = seller.SignCompanyID;
            txtAccountingBankId.Text = seller.CompanyBankCode;
            txtAccountingBranchId.Text = seller.CompanyBranchCode;
            txtAccount.Text = seller.CompanyAccount;
            txtAccounting.Text = seller.CompanyAccountName;
            txtAccountingId.Text = seller.CompanyID;
            txtAccountingName.Text = seller.AccountantName;
            txtAccountingTel.Text = seller.AccountantTel;
            txtAccountingEmail.Text = seller.CompanyEmail;
        }

        public void RedirectSalesManagementPage()
        {
            Response.Redirect("~/sal/salesmanagement.aspx");
        }

        public void SetProvisionDepartments(IEnumerable<ProvisionDepartment> departments)
        {
            repDepartment.DataSource = departments;
            repDepartment.DataBind();

            repDataList.DataSource = departments;
            repDataList.DataBind();
        }

        public void SetProvisionDepartmentsModel(IEnumerable<ProvisionDepartmentModel> departments)
        {
            repDepartment.DataSource = departments;
            repDepartment.DataBind();

            repDataList.DataSource = departments;
            repDataList.DataBind();
        }

        public void ImportStoreList(List<SalesStore> storeList)
        {
            string fileName = string.Format("{0}_{1}.xls", txtSellerName.Text, DateTime.Now.ToString("yyyy_MM_dd"));
            Export(fileName, storeList);
        }

        public void ShowMessage(string msg, string redirect = "")
        {
            string script = string.Format("alert('{0}');", msg);
            if (!string.IsNullOrWhiteSpace(redirect))
            {
                script += "location.href='" + redirect + "';";
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", script, true);
        }

        public void Download(BusinessOrder businessOrder)
        {
            // 如果非選擇總店，則不需列印分店資訊
            SalesStore salesStore = businessOrder.Store.Where(x => x.BranchName.Equals(hidStoreSelectedValue.Value)).FirstOrDefault();

            if (User.IsInRole(MemberRoles.Auditor.ToString("g"))) // 營稽人員一律列印word
            {
                DownloadWordByWordTemplate(businessOrder.BusinessType, SalesFacade.GetBusinessOrderDictionary(businessOrder, salesStore, BusinessOrderDownloadType.Word), salesStore != null ? new List<SalesStore>() : businessOrder.Store);
            }
            else
            {
                if (chkIsTravelType.Checked)
                {
                    DownloadWordByWordTemplate(businessOrder.BusinessType, SalesFacade.GetBusinessOrderDictionary(businessOrder, salesStore, BusinessOrderDownloadType.Word), salesStore != null ? new List<SalesStore>() : businessOrder.Store);
                }
                else
                {
                    DownloadPdfByHtmlTemplate(businessOrder.BusinessType, SalesFacade.GetBusinessOrderDictionary(businessOrder, salesStore, BusinessOrderDownloadType.Pdf), salesStore != null ? new List<SalesStore>() : businessOrder.Store);
                }
            }
        }

        public void ReloadPage(ObjectId boid)
        {
            Response.Redirect("~/sal/BusinessOrderDetail.aspx?boid=" + boid.ToString());
        }

        #endregion method

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                    return;
                }
                InitialControls();
                Presenter.OnViewInitialized();

                ((Ppon.Ppon)base.Master).ViewPortEnabled = false;
            }
            if (!AuthorizationControl())
            {
                RedirectSalesManagementPage();
            }

            BusinessOrderStatus value;
            BusinessOrderStatus.TryParse(ddlStatus.SelectedValue, out value);
            btnUpdateStatus.CausesValidation = !value.Equals(BusinessOrderStatus.Temp);
            // 草稿狀態下開放自動更新功能
            if (_config.BusinessOrderUpdateTimerInterval > 0)
            {
                UpdateTimer.Interval = _config.BusinessOrderUpdateTimerInterval;
                UpdateTimer.Enabled = lblCreateId.Text.ToLower().Equals(UserId) && value.Equals(BusinessOrderStatus.Temp);
            }
            Addjavascript();
            SetSalesmanNameArray();
            _presenter.OnViewLoaded();
        }

        protected void SaveCheck(object sender, EventArgs e)
        {
            bool isValid = true;
            if (btnUpdateStatus.CausesValidation)
            {
                //isValid = Page.IsValid && CheckData();
            }

            if (isValid)
            {
                if (this.SaveClicked != null)
                {
                    this.SaveClicked(this, new DataEventArgs<string>(string.Empty));
                }

                if (!AuthorizationControl())
                {
                    RedirectSalesManagementPage();
                }

                lblSuccessMsg.Text = string.Format("工單資訊已更新!! {0}", DateTime.Now.ToString("yyyy/MM/dd HH:mm"));
                lblSuccessMsg.Visible = true;
                lblErrorMsg.Visible = false;
            }
            else
            {
                lblErrorMsg.Visible = true;
                lblSuccessMsg.Visible = false;
            }
        }

        protected void Save(object sender, EventArgs e)
        {
            if (this.SaveClicked != null)
            {
                this.SaveClicked(this, new DataEventArgs<string>("auto"));
            }

            lblSuccessMsg.Text = string.Format("工單資訊已更新!! {0}", DateTime.Now.ToString("yyyy/MM/dd HH:mm"));
            lblSuccessMsg.Visible = true;
            lblErrorMsg.Visible = false;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.DeleteClicked != null)
            {
                this.DeleteClicked(this, e);
            }

            Response.Redirect("~/Sal/SalesManagement.aspx");
        }

        protected void btnCopy_Click(object sender, EventArgs e)
        {
            if (this.CopyClicked != null)
            {
                BusinessCopyType type = BusinessCopyType.None;
                BusinessCopyType.TryParse(rdlBusinessCopyType.SelectedValue, out type);

                this.CopyClicked(this, new DataEventArgs<BusinessCopyType>(type));
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            if (this.DownloadClicked != null)
            {
                this.DownloadClicked(this, e);
            }
        }

        protected void btnDownloadStore_Click(object sender, EventArgs e)
        {
            if (this.DownloadStoreClicked != null)
            {
                this.DownloadStoreClicked(this, e);
            }
        }

        protected void lkStoreExampleDownload_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.ContentType = "application/vnd.xls";
            Response.AddHeader("Content-Disposition", "attachment;filename=SalesStore.xls");
            Response.WriteFile(HttpContext.Current.Server.MapPath("~/template/SalesDocs/SalesStore.xls"));
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (this.ImportClicked != null)
            {
                lblStoreAddressMsg.Visible = true;
                if (FileUpload1.PostedFile != null && FileUpload1.PostedFile.ContentLength > 0)
                {
                    if (FileUpload1.PostedFile.ContentType == "application/vnd.ms-excel")
                    {
                        List<SalesStore> listStore = GetSalesStoreDataList(FileUpload1.PostedFile);
                        if (listStore != null && listStore.Count > 0)
                        {
                            this.ImportClicked(this, new DataEventArgs<List<SalesStore>>(listStore));
                            lblStoreAddressMsg.Text = "分店資料匯入成功!!請確認。";
                        }
                    }
                    else
                    {
                        lblStoreAddressMsg.Text = "匯入分店Excel檔案格式錯誤!!!請匯入副檔名為.xls檔案";
                    }
                }
                else
                {
                    lblStoreAddressMsg.Text = "請選擇欲匯入的Excel檔案!!";
                }
            }
        }

        protected void UpdatePanel1_Load(object sender, EventArgs e)
        {
            hidStoreSelectedValue.Value = ddlStoreList.SelectedValue;
            if (this.RefreshStoreClicked != null)
            {
                this.RefreshStoreClicked(this, e);
            }
        }

        protected void gvStoreList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is SalesStore)
            {
                SalesStore item = (SalesStore)e.Row.DataItem;
                Label lblStoreName = ((Label)e.Row.FindControl("lblStoreName"));
                Label lblQuantity = ((Label)e.Row.FindControl("lblQuantity"));
                Button btnDelete = ((Button)e.Row.FindControl("btnDelete"));

                bool enabled = txtSellerName.Enabled;
                lblStoreName.Text = !string.IsNullOrWhiteSpace(item.BranchName) ? item.BranchName : "(無分店名稱)";
                lblQuantity.Text = item.Quantity.ToString();

                Label lblAddress = ((Label)e.Row.FindControl("lblAddress"));
                lblAddress.Text = CityManager.CityTownShopStringGet(item.AddressTownshipId) + item.StoreAddress;
                btnDelete.Enabled = enabled;
            }
        }

        protected void gvStoreList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DEL")
            {
                if (this.StoreDeleteClicked != null)
                {
                    ObjectId id;
                    ObjectId.TryParse(e.CommandArgument.ToString(), out id);
                    if (!id.Equals(ObjectId.Empty))
                    {
                        this.StoreDeleteClicked(this, new DataEventArgs<ObjectId>(id));
                    }
                }
            }
        }

        protected void gvHistoryList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                History dataItem = (History)e.Row.DataItem;

                Label lblChangeId = (Label)e.Row.FindControl("lblChangeId");
                lblChangeId.Text = e.Row.RowIndex.Equals(0) ? "New!!" : string.Empty;

                Label lblModifyTime = (Label)e.Row.FindControl("lblModifyTime");
                lblModifyTime.Text = dataItem.ModifyTime.ToLocalTime().ToString("yyyy/MM/dd HH:mm");

                if (dataItem.IsImportant.Equals(true))
                {
                    e.Row.BackColor = System.Drawing.Color.Pink;
                }
            }
        }

        protected void btnReferred_Click(object sender, EventArgs e)
        {
            if (this.ReferredClicked != null)
            {
                this.ReferredClicked(this, new DataEventArgs<string>(txtReferredUser.Text.ToLower().Trim()));
            }
        }

        #region 好康小提示

        protected void repDataList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ProvisionDepartment)
            {
                ProvisionDepartment dataItem = (ProvisionDepartment)e.Item.DataItem;

                HiddenField hidListDepartment = (HiddenField)e.Item.FindControl("hidListDepartment");
                DataList dlCategory = (DataList)e.Item.FindControl("dlCategory");

                hidListDepartment.Value = dataItem.Id.ToString();
                string cssClass = string.Empty;
                switch (dataItem.Name)
                {
                    case "P好康":
                        cssClass = "Ppon NoTravel";
                        break;

                    case "P商品":
                        cssClass = "Product NoTravel";
                        break;

                    case "P旅遊":
                        cssClass = "Travel";
                        break;

                    default:
                        break;
                }
                dlCategory.DataSource = dataItem.Categorys.OrderBy(x => x.Rank);
                dlCategory.DataBind();
                dlCategory.CssClass = cssClass;
            }
        }

        protected void dlCategory_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if (e.Item.DataItem is ProvisionCategory)
            {
                ProvisionCategory dataItem = (ProvisionCategory)e.Item.DataItem;

                HiddenField hidListCategory = (HiddenField)e.Item.FindControl("hidListCategory");
                Label lblListCategory = (Label)e.Item.FindControl("lblListCategory");

                lblListCategory.Text = dataItem.Name;
                hidListCategory.Value = dataItem.Id.ToString();
            }
        }

        protected void repDepartment_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ProvisionDepartment)
            {
                ProvisionDepartment dataItem = (ProvisionDepartment)e.Item.DataItem;

                HiddenField hidDepartmentId = (HiddenField)e.Item.FindControl("hidDepartmentId");
                Repeater repCategory = (Repeater)e.Item.FindControl("repCategory");

                hidDepartmentId.Value = dataItem.Id.ToString();
                repCategory.DataSource = dataItem.Categorys.OrderBy(x => x.Rank);
                repCategory.DataBind();
            }
        }

        protected void repCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ProvisionCategory)
            {
                ProvisionCategory dataItem = (ProvisionCategory)e.Item.DataItem;

                HiddenField hidCategoryId = (HiddenField)e.Item.FindControl("hidCategoryId");
                Repeater repItem = (Repeater)e.Item.FindControl("repItem");

                hidCategoryId.Value = dataItem.Id.ToString();
                repItem.DataSource = dataItem.Items.OrderBy(x => x.Rank);
                repItem.DataBind();
            }
        }

        protected void repItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ProvisionItem)
            {
                ProvisionItem dataItem = (ProvisionItem)e.Item.DataItem;

                Label lblItem = (Label)e.Item.FindControl("lblItem");
                HiddenField hidItemId = (HiddenField)e.Item.FindControl("hidItemId");
                Repeater repContent = (Repeater)e.Item.FindControl("repContent");

                lblItem.Text = dataItem.Name;
                hidItemId.Value = dataItem.Id.ToString();
                repContent.DataSource = dataItem.Contents.OrderBy(x => x.Rank);
                repContent.DataBind();
            }
        }

        protected void repContent_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ProvisionContent)
            {
                ProvisionContent dataItem = (ProvisionContent)e.Item.DataItem;

                Label lblContent = (Label)e.Item.FindControl("lblContent");
                HiddenField hidContentId = (HiddenField)e.Item.FindControl("hidContentId");
                HiddenField hidDescription = (HiddenField)e.Item.FindControl("hidDescription");
                HiddenField hidDescId = (HiddenField)e.Item.FindControl("hidDescId");

                lblContent.Text = dataItem.Name;
                hidContentId.Value = dataItem.Id.ToString();
                if (dataItem.ProvisionDescs.Count > 0)
                {
                    hidDescId.Value = dataItem.ProvisionDescs[0].Id.ToString();
                    hidDescription.Value = dataItem.ProvisionDescs[0].Description;
                }
            }
        }

        #endregion 好康小提示

        protected void rptCommercialCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<Category, List<Category>>)
            {
                KeyValuePair<Category, List<Category>> dataItem = (KeyValuePair<Category, List<Category>>)e.Item.DataItem;

                HiddenField hidCommercialCategoryId = (HiddenField)e.Item.FindControl("hidCommercialCategoryId");
                CheckBox chkCommercialArea = (CheckBox)e.Item.FindControl("chkCommercialArea");
                CheckBoxList chklCommercialCategory = (CheckBoxList)e.Item.FindControl("chklCommercialCategory");

                hidCommercialCategoryId.Value = dataItem.Key.Id.ToString();
                chkCommercialArea.Text = dataItem.Key.Name;
                chklCommercialCategory.DataSource = dataItem.Value;
                chklCommercialCategory.DataValueField = "Id";
                chklCommercialCategory.DataTextField = "Name";
                chklCommercialCategory.DataBind();
            }
        }

        protected void channelCheckArea_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode node = (CategoryNode)e.Item.DataItem;
                //CheckBoxList subChannelCheck = (CheckBoxList)e.Item.FindControl("subChannelCheck");
                HiddenField channelId = (HiddenField)e.Item.FindControl("channelCheckId");
                Repeater subChannelCheckArea = (Repeater)e.Item.FindControl("subChannelCheckArea");

                channelId.Value = node.CategoryId.ToString();
                List<CategoryTypeNode> areaCategories = node.NodeDatas.Where(x => x.NodeType == CategoryType.PponChannelArea).ToList();
                if (areaCategories.Count > 0)
                {
                    CategoryTypeNode areaCategory = areaCategories.First();
                    subChannelCheckArea.DataSource = areaCategory.CategoryNodes;
                    subChannelCheckArea.DataBind();

                }
                Repeater subChannelSPRegionCheckArea = (Repeater)e.Item.FindControl("subChannelSPRegionCheckArea");
                if (areaCategories.Count > 0)
                {
                    CategoryTypeNode areaCategory = areaCategories.First();
                    subChannelSPRegionCheckArea.DataSource = areaCategory.CategoryNodes;
                    subChannelSPRegionCheckArea.DataBind();

                }

            }
        }

        protected void subChannelSPRegionCheckArea_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode node = (CategoryNode)e.Item.DataItem;
                Repeater subChannelSpecialRegionCheckArea = (Repeater)e.Item.FindControl("subChannelSpecialRegionCheckArea");

                Literal llSpecialRegion = (Literal)e.Item.FindControl("llSpecialRegion");

                List<CategoryNode> specialRegionCategories = node.GetSubCategoryTypeNode(CategoryType.PponChannelArea);

                //List<CategoryTypeNode> specialRegionCategories = node.NodeDatas.Where(x => x.NodeType == CategoryType.SpecialRegion).ToList();

                if ((specialRegionCategories.Any(x => x.CategoryName == "商圈‧景點")))
                {
                    var fatherCategoryNode = specialRegionCategories.Where(x => x.CategoryName == "商圈‧景點").First();
                    subChannelSpecialRegionCheckArea.DataSource = fatherCategoryNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                    subChannelSpecialRegionCheckArea.DataBind();
                }

                //List<CategoryTypeNode> standardRegionCategories = node.NodeDatas.Where(x => x.NodeType == CategoryType.StandardRegion).ToList();

                //if (standardRegionCategories.Count > 0)
                //{
                //    CategoryTypeNode standardRegion = standardRegionCategories.First();

                //    List<CategoryNode> allsubStandardRegion = new List<CategoryNode>();
                //    foreach (var categoryNode in standardRegion.CategoryNodes)
                //    {
                //       allsubStandardRegion.AddRange(categoryNode.NodeDatas.Where(x => x.NodeType == CategoryType.StandardRegion).First().CategoryNodes);
                //    }

                //}

                //llSpecialRegion.Text =(specialRegionCategories.Count > 0 && standardRegionCategories.Count > 0)?"<br />":null;
            }
        }

        protected void dealCategoryArea_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is Category)
            {
                Category category = (Category)e.Item.DataItem;
                Repeater subDealCategoryArea = (Repeater)e.Item.FindControl("subDealCategoryArea");

                List<ViewCategoryDependency> subCategoryCol = PponFacade.ViewCategoryDependencyGetByParentId(category.Id);

                List<Category> subDealCategories = new List<Category>();

                foreach (var subCategory in subCategoryCol.Where(x => x.CategoryStatus == 1))
                {
                    Category subnode = PponFacade.SubDealCategoryGet(subCategory.CategoryId);
                    if (subnode.IsLoaded)
                    {
                        subDealCategories.Add(subnode);
                    }
                }

                if (subDealCategories.Count > 0)
                {
                    subDealCategoryArea.DataSource = subDealCategories;
                    subDealCategoryArea.DataBind();
                }

            }
        }

        private List<Category> GetCategoryGetListWithoutSubDealCategory(List<Category> categories)
        {
            List<int> subCategorylist = PponFacade.ViewCategoryDependencyGetByParentType(CategoryType.DealCategory);
            return categories.Where(x => !subCategorylist.Contains(x.Id)).ToList();
        }

        [WebMethod]
        public static void DoPostBack()
        {
        }

        [WebMethod]
        public static dynamic BankInfoGetBranchList(string id)
        {
            BankInfoCollection branchList = VourcherFacade.BankInfoGetBranchList(id);
            return branchList.Select(x => new { No = x.BranchNo, Name = x.BranchNo + " " + x.BranchName });
        }

        [WebMethod]
        public static string GetDealType2(int parentId)
        {
            var scs = PponFacade.DealType2GetList(parentId);
            return new JsonSerializer().Serialize(scs);
        }

        #endregion page

        #region private method

        private CheckBoxList GetDealLabelCheckBoxList(DealLabel dealLabel)
        {
            string checkBoxListId = string.Empty;

            switch ((DealLabelType)Enum.ToObject(typeof(DealLabelType), dealLabel.DealType))
            {
                case DealLabelType.Coupon:
                    switch (GetDealLabelShowType(dealLabel))
                    {
                        case DealLabelShowType.tag:
                            checkBoxListId = cbl_couponDealTag.UniqueID;
                            break;
                        case DealLabelShowType.travel:
                            checkBoxListId = cbl_couponTravelDealTag.UniqueID;
                            break;
                    }
                    break;
                case DealLabelType.Delivery:
                    switch (GetDealLabelShowType(dealLabel))
                    {
                        case DealLabelShowType.travel:
                            checkBoxListId = cbl_deliveryTravelDealTag.UniqueID;
                            break;
                    }
                    break;
            }

            return (CheckBoxList)Page.FindControl(checkBoxListId);
        }

        private DealLabelShowType GetDealLabelShowType(DealLabel dealLabel)
        {
            return (DealLabelShowType)Enum.Parse(typeof(DealLabelShowType), dealLabel.ShowType);
        }

        private ListItem GetDealLabelListItem(DealLabel dealLabel)
        {
            ListItem dealLabelListItem = new ListItem();
            dealLabelListItem.Text = SystemCodeManager.GetDealLabelCodeName((int)dealLabel.SystemCodeId);
            dealLabelListItem.Value = dealLabel.SystemCodeId.ToString();

            //預設需要打勾的檔次標籤
            switch ((DealLabelSystemCode)Enum.ToObject(typeof(DealLabelSystemCode), dealLabel.SystemCodeId))
            {
                case DealLabelSystemCode.NAOutlyingIslands:
                case DealLabelSystemCode.Tickets:
                    dealLabelListItem.Selected = true;
                    break;
            }

            //檔次標籤與其他舊有選項連動者預設反灰不讓使用者勾選, 與其他選項一併連動
            switch ((DealLabelSystemCode)Enum.ToObject(typeof(DealLabelSystemCode), dealLabel.SystemCodeId))
            {
                case DealLabelSystemCode.NAOutlyingIslands:
                case DealLabelSystemCode.Tickets:
                case DealLabelSystemCode.SellOverOneThousand:
                case DealLabelSystemCode.OnlyPaperFormatCoupon:
                case DealLabelSystemCode.PayOnDelivery:
                    dealLabelListItem.Enabled = false;
                    break;
            }

            return dealLabelListItem;
        }

        private void InitialControls()
        {
            // 依照指定順序排列
            List<BusinessOrderSellerLevel> dataList = new List<BusinessOrderSellerLevel>();
            dataList.Add(BusinessOrderSellerLevel.A);
            dataList.Add(BusinessOrderSellerLevel.B);
            dataList.Add(BusinessOrderSellerLevel.C);
            dataList.Add(BusinessOrderSellerLevel.Big);
            dataList.Add(BusinessOrderSellerLevel.Medium);
            dataList.Add(BusinessOrderSellerLevel.Small);
            foreach (BusinessOrderSellerLevel item in dataList)
            {
                rdlSellerLevel.Items.Add(new ListItem(Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, item), item.ToString()));
            }

            chklPictureSource.DataSource = Enum.GetValues(typeof(PictureSourceType)).Cast<PictureSourceType>().Select(x => new ListItem { Value = ((int)x).ToString(), Text = Helper.GetLocalizedEnum(x) });
            chklPictureSource.DataTextField = "Text";
            chklPictureSource.DataValueField = "Value";
            chklPictureSource.DataBind();

            ddlStoreList.Items.Add(new ListItem("總店", string.Empty));

            pProductRefund.InnerHtml = string.Format(Helper.GetLocalizedEnum(I18N.Message.ResourceManager, AdditionalContractsType.ProductRefund), "<br />");

            rdlDealAccountingPayType.DataSource = Enum.GetValues(typeof(DealAccountingPayType)).Cast<DealAccountingPayType>().Select(x => new ListItem { Value = ((int)x).ToString(), Text = Helper.GetLocalizedEnum(x) });
            rdlDealAccountingPayType.DataTextField = "Text";
            rdlDealAccountingPayType.DataValueField = "Value";
            rdlDealAccountingPayType.DataBind();
            rdlDealAccountingPayType.SelectedValue = ((int)DealAccountingPayType.PayToCompany).ToString();

            rdlInTax.DataSource = Enum.GetValues(typeof(BusinessOrderInvoiceTax)).Cast<BusinessOrderInvoiceTax>().Select(x => new ListItem { Value = ((int)x).ToString(), Text = Helper.GetLocalizedEnum(x) });
            rdlInTax.DataTextField = "Text";
            rdlInTax.DataValueField = "Value";
            rdlInTax.DataBind();
            rdlInTax.Items.FindByValue(((int)BusinessOrderInvoiceTax.In18Tax).ToString()).Attributes.Add("style", "display: none");

            ddlMultiDeal.DataSource = Enum.GetValues(typeof(BusinessOrderMultiDeal)).Cast<BusinessOrderMultiDeal>().Select(x => new ListItem { Value = ((int)x).ToString(), Text = Helper.GetLocalizedEnum(x) });
            ddlMultiDeal.DataTextField = "Text";
            ddlMultiDeal.DataValueField = "Value";
            ddlMultiDeal.DataBind();

            chklInspectRule.DataSource = Enum.GetValues(typeof(BusinessOrderInspectRule)).Cast<BusinessOrderInspectRule>().Select(x => new ListItem { Value = ((int)x).ToString(), Text = Helper.GetLocalizedEnum(x) });
            chklInspectRule.DataTextField = "Text";
            chklInspectRule.DataValueField = "Value";
            chklInspectRule.DataBind();

            rdlInvoiceFounder.DataSource = Enum.GetValues(typeof(InvoiceFounderType)).Cast<InvoiceFounderType>().Select(x => new ListItem { Value = ((int)x).ToString(), Text = Helper.GetLocalizedEnum(x) });
            rdlInvoiceFounder.DataTextField = "Text";
            rdlInvoiceFounder.DataValueField = "Value";
            rdlInvoiceFounder.DataBind();

        }

        private bool AuthorizationControl()
        {
            bool authorize = false;

            int value;
            int.TryParse(hidStatus.Value, out value);
            BusinessOrderStatus status = (BusinessOrderStatus)value;
            ActiveControls = new ActiveControlParameters();
            AuthorizationControls = new AuthorizationControlParameters();
            string[] auditPrivileges = FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.Audit).ToArray();

            #region Get the roles

            List<MemberRoles> roles = new List<MemberRoles>();
            if (User.IsInRole(MemberRoles.Sales.ToString("g")))
            {
                roles.Add(MemberRoles.Sales);
            }
            if (User.IsInRole(MemberRoles.SalesAssistant.ToString("g")))
            {
                roles.Add(MemberRoles.SalesAssistant);
            }
            if (User.IsInRole(MemberRoles.SalesManager.ToString("g")))
            {
                roles.Add(MemberRoles.SalesManager);
            }
            if (User.IsInRole(MemberRoles.Auditor.ToString("g")))
            {
                roles.Add(MemberRoles.Auditor);
            }
            if (User.IsInRole(MemberRoles.Production.ToString("g")))
            {
                roles.Add(MemberRoles.Production);
            }
            if (User.IsInRole(MemberRoles.BusinessOrderViewer.ToString("g")))
            {
                roles.Add(MemberRoles.BusinessOrderViewer);
            }

            #endregion

            if (!roles.Contains(MemberRoles.RegisteredUser))
            {

                if (BusinessOrderObjectId == ObjectId.Empty)
                {
                    if (roles.Contains(MemberRoles.Sales) || roles.Contains(MemberRoles.SalesAssistant) || roles.Contains(MemberRoles.SalesManager))
                    {
                        #region 新增工單

                        // 業務/業務助理/業務主管 皆可新增工單
                        ActiveControls.IsBusinessTypeEnable = true;
                        ActiveControls.IsEnableControls = true;
                        ActiveControls.IsUpdateStautsVisible = true;
                        ActiveControls.IsUpdateStautsEnabled = true;
                        ActiveControls.IsRemittanceTypeDescVisible = true;

                        AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp };

                        if (roles.Contains(MemberRoles.SalesAssistant))
                        {
                            // 業務助理可異動賣家規模
                            AuthorizationControls.isSellerLevelEnabled = true;
                        }

                        authorize = true;

                        #endregion
                    }
                }
                else
                {
                    #region 編輯/審核工單

                    bool isLeader = (SalesLevel.Equals(Level.Leader) || SalesLevel.Equals(Level.Manager)) && SalesMemberArea.Contains(hidAuthorizationGroup.Value);

                    switch (status)
                    {
                        case BusinessOrderStatus.Temp:

                            #region 草稿

                            if ((roles.Contains(MemberRoles.Sales) || roles.Contains(MemberRoles.SalesManager)) && string.Equals(SalesId, UserId, StringComparison.OrdinalIgnoreCase))
                            {
                                // 業務/業務主管 可編輯自己建立的工單
                                ActiveControls.IsStoreAdd = true;
                                ActiveControls.IsEnableControls = true;
                                ActiveControls.IsDelete = true;
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsUpdateStautsEnabled = true;
                                ActiveControls.IsDownload = true;
                                ActiveControls.IsReferredVisible = true;
                                ActiveControls.IsRemittanceTypeDescVisible = true;
                                ActiveControls.CopyType = new List<BusinessCopyType>() { BusinessCopyType.General };
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send };

                                authorize = true;
                            }

                            if (roles.Contains(MemberRoles.SalesAssistant) || roles.Contains(MemberRoles.Sales))
                            {
                                // 業務助理可編輯所有業務的工單
                                ActiveControls.IsStoreAdd = true;
                                ActiveControls.IsEnableControls = true;
                                ActiveControls.IsDelete = true;
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsUpdateStautsEnabled = true;
                                ActiveControls.IsDownload = true;
                                ActiveControls.IsReferredVisible = true;
                                ActiveControls.CopyType = new List<BusinessCopyType>() { BusinessCopyType.General };
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send };

                                // 業務助理可異動合約狀態&出帳方式
                                ActiveControls.IsContractSendEnabled = true;
                                ActiveControls.IsRemittanceTypeVisible = true;
                                ActiveControls.IsRemittanceTypeDescVisible = false;

                                // 業務助理可異動賣家規模
                                AuthorizationControls.isSellerLevelEnabled = true;

                                authorize = true;
                            }

                            #endregion

                            break;
                        case BusinessOrderStatus.Send:

                            #region 送審

                            if (roles.Contains(MemberRoles.Sales) || roles.Contains(MemberRoles.SalesManager))
                            {
                                // 業務查看自己的工單，並可取回
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsUpdateStautsEnabled = true;
                                ActiveControls.IsDownload = true;
                                ActiveControls.IsReferredVisible = true;
                                ActiveControls.IsRemittanceTypeDescVisible = true;
                                ActiveControls.CopyType = new List<BusinessCopyType>() { BusinessCopyType.General };

                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send };

                                authorize = true;
                            }

                            if (roles.Contains(MemberRoles.SalesAssistant))
                            {
                                // 業務助理可查看所有業務的工單，並可取回
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsUpdateStautsEnabled = true;
                                ActiveControls.IsDownload = true;
                                ActiveControls.IsReferredVisible = true;
                                ActiveControls.IsRemittanceTypeDescVisible = true;
                                ActiveControls.CopyType = new List<BusinessCopyType>() { BusinessCopyType.General };
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send };

                                // 業務助理協助查看工單，並可異動合約狀態
                                ActiveControls.IsContractSendEnabled = true;

                                // 業務助理可異動賣家規模
                                AuthorizationControls.isSellerLevelEnabled = true;

                                authorize = true;
                            }

                            if (roles.Contains(MemberRoles.SalesManager) && isLeader)
                            {
                                // 業務主管可 轉件／審核 該區工單
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsUpdateStautsEnabled = true;
                                ActiveControls.IsHistoryMemoVisible = true;
                                ActiveControls.IsRemittanceTypeDescVisible = true;
                                AuthorizationControls.IsAuthorizationGroup = true;
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve };

                                authorize = true;
                            }

                            #endregion

                            break;
                        case BusinessOrderStatus.Approve:
                        case BusinessOrderStatus.Review:

                            #region 核准/複查(關卡合併)

                            if (roles.Contains(MemberRoles.Sales) || roles.Contains(MemberRoles.SalesManager))
                            {
                                // 業務查看自己的工單
                                ActiveControls.IsDownload = true;
                                ActiveControls.IsReferredVisible = true;
                                ActiveControls.IsRemittanceTypeDescVisible = true;
                                ActiveControls.CopyType = new List<BusinessCopyType>() { BusinessCopyType.General };
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review };

                                authorize = true;
                            }

                            if (roles.Contains(MemberRoles.SalesAssistant))
                            {
                                // 業務助理可查看所有業務的工單
                                ActiveControls.IsDownload = true;
                                ActiveControls.IsReferredVisible = true;
                                ActiveControls.IsRemittanceTypeDescVisible = true;
                                ActiveControls.CopyType = new List<BusinessCopyType>() { BusinessCopyType.General };
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review };

                                // 業務助理協助查看工單，並可異動合約狀態
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsContractSendEnabled = true;

                                // 業務助理可異動賣家規模
                                AuthorizationControls.isSellerLevelEnabled = true;

                                authorize = true;
                            }

                            if (roles.Contains(MemberRoles.SalesManager) && isLeader)
                            {
                                // 業務主管可查看工單
                                ActiveControls.IsRemittanceTypeDescVisible = true;
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review };

                                authorize = true;
                            }

                            if (roles.Contains(MemberRoles.Auditor))
                            {
                                // 稽核人員可更新狀態為已排檔，並設定上檔日期
                                ActiveControls.IsStoreAdd = true;
                                ActiveControls.IsEnableControls = true;
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsUpdateStautsEnabled = true;
                                ActiveControls.IsDownload = true;
                                ActiveControls.IsHistoryMemoVisible = true;
                                ActiveControls.IsSellerInfoEnabled = true;
                                ActiveControls.IsContractSendEnabled = true;
                                ActiveControls.IsRemittanceTypeVisible = true;
                                ActiveControls.IsRemittanceTypeDescVisible = false;

                                // 稽核人員可異動賣家規模
                                AuthorizationControls.isSellerLevelEnabled = true;

                                authorize = true;
                            }

                            if (roles.Contains(MemberRoles.BusinessOrderViewer))
                            {
                                // 工單瀏覽者可查看工單
                                ActiveControls.IsRemittanceTypeDescVisible = true;
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review };

                                authorize = true;
                            }

                            if (CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.TaxFreeNotice))
                            {
                                // 具免稅通知權限人員可異動歷史紀錄備註
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsHistoryMemoVisible = true;

                                authorize = true;
                            }

                            if (CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.RemittanceNotice))
                            {
                                // 具出帳方式通知權限人員可異動歷史紀錄備註
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsHistoryMemoVisible = true;

                                authorize = true;
                            }

                            // 具稽核(排檔)權限人員可編輯上檔日期及變更為已排檔狀態
                            if (auditPrivileges.Contains(UserId, StringComparer.OrdinalIgnoreCase))
                            {
                                AuthorizationControls.IsBusinessHourTimeSVisible = true;
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await };
                            }
                            else
                            {
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review };
                            }

                            #endregion

                            break;
                        case BusinessOrderStatus.Await:

                            #region 已排檔

                            if (roles.Contains(MemberRoles.Sales) || roles.Contains(MemberRoles.SalesManager))
                            {
                                // 業務查看自己的工單
                                ActiveControls.IsDownload = true;
                                ActiveControls.IsReferredVisible = true;
                                ActiveControls.IsRemittanceTypeDescVisible = true;
                                ActiveControls.CopyType = new List<BusinessCopyType>() { BusinessCopyType.General };
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await };

                                authorize = true;
                            }

                            if (roles.Contains(MemberRoles.SalesAssistant))
                            {
                                // 業務助理可查看所有業務的工單
                                ActiveControls.IsDownload = true;
                                ActiveControls.IsReferredVisible = true;
                                ActiveControls.IsRemittanceTypeDescVisible = true;
                                ActiveControls.CopyType = new List<BusinessCopyType>() { BusinessCopyType.General };
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await };

                                // 業務助理協助查看工單，並可異動合約狀態
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsContractSendEnabled = true;

                                // 業務助理可異動賣家規模
                                AuthorizationControls.isSellerLevelEnabled = true;

                                authorize = true;
                            }

                            if (roles.Contains(MemberRoles.SalesManager) && isLeader)
                            {
                                // 業務主管可查看工單
                                ActiveControls.IsRemittanceTypeDescVisible = true;
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await };

                                authorize = true;
                            }

                            if (roles.Contains(MemberRoles.BusinessOrderViewer))
                            {
                                // 工單瀏覽者可查看工單
                                ActiveControls.IsRemittanceTypeDescVisible = true;
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await };

                                authorize = true;
                            }

                            if (roles.Contains(MemberRoles.Auditor))
                            {
                                // 營管人員可變更賣家編號及合約狀態
                                ActiveControls.IsUpdateStautsEnabled = true;
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsDownload = true;
                                ActiveControls.IsHistoryMemoVisible = true;
                                ActiveControls.IsSellerInfoEnabled = true;
                                ActiveControls.IsContractSendEnabled = true;

                                // 稽核人員可異動賣家規模
                                AuthorizationControls.isSellerLevelEnabled = true;

                                // 具修改權限人員可編輯工單內容
                                if (CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.Update))
                                {
                                    ActiveControls.IsEnableControls = true;
                                    ActiveControls.IsRemittanceTypeVisible = true;
                                    ActiveControls.IsRemittanceTypeDescVisible = false;
                                }

                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await };

                                authorize = true;
                            }

                            if (roles.Contains(MemberRoles.Production))
                            {
                                // 已排檔後，可編輯內容
                                ActiveControls.IsStoreAdd = true;
                                ActiveControls.IsEnableControls = true;
                                ActiveControls.IsUpdateStautsEnabled = true;
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsDownload = true;
                                ActiveControls.IsHistoryMemoVisible = true;
                                ActiveControls.IsBidTextEnabled = true;
                                ActiveControls.IsSellerInfoEnabled = true;
                                ActiveControls.IsRemittanceTypeVisible = true;
                                ActiveControls.IsRemittanceTypeDescVisible = false;
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await, BusinessOrderStatus.Create };

                                authorize = true;
                            }

                            if (CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.TaxFreeNotice))
                            {
                                // 具免稅通知權限人員可異動歷史紀錄備註
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsHistoryMemoVisible = true;

                                authorize = true;
                            }

                            if (CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.RemittanceNotice))
                            {
                                // 具出帳方式通知權限人員可異動歷史紀錄備註
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsHistoryMemoVisible = true;

                                authorize = true;
                            }

                            // 具稽核(排檔)權限人員可編輯上檔日期
                            if (auditPrivileges.Contains(UserId, StringComparer.OrdinalIgnoreCase))
                            {
                                AuthorizationControls.IsBusinessHourTimeSVisible = true;
                            }

                            #endregion

                            break;
                        case BusinessOrderStatus.Create:

                            #region 已建檔

                            if (roles.Contains(MemberRoles.Sales) || roles.Contains(MemberRoles.SalesManager))
                            {
                                // 業務查看自己的工單
                                ActiveControls.IsDownload = true;
                                ActiveControls.IsReferredVisible = true;
                                ActiveControls.IsRemittanceTypeDescVisible = true;
                                ActiveControls.CopyType = new List<BusinessCopyType>() { BusinessCopyType.General, BusinessCopyType.Again };
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await, BusinessOrderStatus.Create };

                                authorize = true;
                            }

                            if (roles.Contains(MemberRoles.SalesAssistant))
                            {
                                // 業務助理可查看所有業務的工單
                                ActiveControls.IsDownload = true;
                                ActiveControls.IsReferredVisible = true;
                                ActiveControls.IsRemittanceTypeDescVisible = true;
                                ActiveControls.CopyType = new List<BusinessCopyType>() { BusinessCopyType.General, BusinessCopyType.Again };
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await, BusinessOrderStatus.Create };

                                // 業務助理協助查看工單，並可異動合約狀態
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsContractSendEnabled = true;

                                // 業務助理可異動賣家規模
                                AuthorizationControls.isSellerLevelEnabled = true;

                                authorize = true;
                            }

                            if (roles.Contains(MemberRoles.SalesManager) && isLeader)
                            {
                                // 業務主管可查看工單
                                ActiveControls.IsRemittanceTypeDescVisible = true;
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await, BusinessOrderStatus.Create };

                                authorize = true;
                            }

                            if (roles.Contains(MemberRoles.BusinessOrderViewer))
                            {
                                // 工單瀏覽者可查看工單
                                ActiveControls.IsRemittanceTypeDescVisible = true;
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await, BusinessOrderStatus.Create };

                                authorize = true;
                            }

                            if (roles.Contains(MemberRoles.Auditor))
                            {
                                // 營管人員可變更合約送交狀態及賣家編號
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsDownload = true;
                                ActiveControls.IsSellerInfoEnabled = true;
                                ActiveControls.IsContractSendEnabled = true;

                                // 稽核人員可異動賣家規模
                                AuthorizationControls.isSellerLevelEnabled = true;

                                // 具修改權限人員可編輯工單內容
                                if (CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.Update))
                                {
                                    ActiveControls.IsRemittanceTypeVisible = true;
                                    ActiveControls.IsRemittanceTypeDescVisible = false;
                                    ActiveControls.IsEnableControls = true;
                                }

                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await, BusinessOrderStatus.Create };

                                authorize = true;
                            }

                            if (roles.Contains(MemberRoles.Production))
                            {
                                // 創編人員可編輯工單，並註記檔次編號
                                ActiveControls.IsStoreAdd = true;
                                ActiveControls.IsEnableControls = true;
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsDownload = true;
                                ActiveControls.IsHistoryMemoVisible = true;
                                ActiveControls.IsBidTextEnabled = true;
                                ActiveControls.IsSellerInfoEnabled = true;
                                ActiveControls.IsRemittanceTypeVisible = true;
                                ActiveControls.IsRemittanceTypeDescVisible = false;
                                AuthorizationControls.ListItemStatus = new List<BusinessOrderStatus>() { BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await, BusinessOrderStatus.Create };

                                authorize = true;
                            }

                            if (CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.TaxFreeNotice))
                            {
                                // 具免稅通知權限人員可異動歷史紀錄備註
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsHistoryMemoVisible = true;

                                authorize = true;
                            }

                            if (CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.RemittanceNotice))
                            {
                                // 具出帳方式通知權限人員可異動歷史紀錄備註
                                ActiveControls.IsUpdateStautsVisible = true;
                                ActiveControls.IsHistoryMemoVisible = true;

                                authorize = true;
                            }

                            // 具稽核(排檔)權限人員才可編輯上檔日期
                            if (auditPrivileges.Contains(UserId, StringComparer.OrdinalIgnoreCase))
                            {
                                AuthorizationControls.IsBusinessHourTimeSVisible = true;
                            }

                            #endregion

                            break;
                        default:
                            break;
                    }

                    #endregion
                }

                if (authorize)
                {
                    EnableActiveControl(ActiveControls);
                    EnableAuthorizationControl(AuthorizationControls);
                    return true;
                }
            }
            return false;
        }

        private string GetCheckBoxListSelectedValue(CheckBoxList chkBoxList)
        {
            string separator = ",";
            return string.Join(separator, chkBoxList.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value));
        }

        private string GetCheckBoxListSelectedText(CheckBoxList chkBoxList)
        {
            string separator = "、";
            return string.Join(separator, chkBoxList.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Text));
        }

        private void SetCheckBoxListSelectedValue(CheckBoxList chkBoxList, string checkItems)
        {
            for (int i = 0; i < chkBoxList.Items.Count; i++)
            {
                if (checkItems != null)
                {
                    chkBoxList.Items[i].Selected = checkItems.Split(',').Contains(chkBoxList.Items[i].Value);
                }
            }
        }

        /// <summary>
        /// 工單內容中所有控制項是否啟用
        /// </summary>
        /// <param name="enabled"></param>
        private void EnableControls(bool enabled)
        {
            #region 新增業務工單

            rdlSalesDept.Enabled = enabled;
            rdlAccBusinessGroup.Enabled = enabled;
            chkIsContractSend.Enabled = enabled;
            ddlNoContractReason.Enabled = enabled;
            txtNoContractReason.Enabled = enabled;
            chklPponCity.Enabled = enabled;
            chklTravelCategory.Enabled = enabled;
            for (int i = 0; i < rptCommercialCategory.Items.Count; i++)
            {
                CheckBox chkCommercialArea = (CheckBox)rptCommercialCategory.Items[i].FindControl("chkCommercialArea");
                chkCommercialArea.Enabled = enabled;
                CheckBoxList chklCommercialCategory = (CheckBoxList)rptCommercialCategory.Items[i].FindControl("chklCommercialCategory");
                chklCommercialCategory.Enabled = enabled;
            }
            txtOtherPponCity.Enabled = enabled;
            txtSalesName.Enabled = enabled;
            txtSalesPhone.Enabled = enabled;
            txtKeywords.Enabled = enabled;
            txtImportant1.Enabled = enabled;
            txtImportant2.Enabled = enabled;
            txtImportant3.Enabled = enabled;
            txtImportant4.Enabled = enabled;
            chklPictureSource.Enabled = enabled;
            txtMarketEvent.Enabled = enabled;
            chklSourceType.Enabled = enabled;
            rdlOnlineUse.Enabled = enabled;
            rdlOnlineOnly.Enabled = enabled;
            //rdlInventoryOnly.Enabled = enabled;
            rdlIsEDC.Enabled = enabled;
            rdlIsApplyNewPromotion.Enabled = enabled;
            rdlApplyNewPromotion.Enabled = enabled;
            chkIsKownEDC.Enabled = enabled;
            chkIsTaiShinEDC.Enabled = enabled;
            txtEdcDesc1.Enabled = enabled;
            txtEdcDesc2.Enabled = enabled;
            txtEdcDesc3.Enabled = enabled;
            txtEdcDesc4.Enabled = enabled;
            txtEdcDesc5.Enabled = enabled;

            #endregion 新增業務工單

            #region Part1 店家資料

            txtSellerName.Enabled = enabled;
            txtCompanyName.Enabled = enabled;
            rdlPrivateContract.Enabled = enabled;
            txtGuiNumber.Enabled = enabled;
            txtBossName.Enabled = enabled;
            txtPersonalId.Enabled = enabled;
            txtContactPerson.Enabled = enabled;
            txtBossTel.Enabled = enabled;
            txtBossPhone.Enabled = enabled;
            txtBossFax.Enabled = enabled;
            txtBossEmail.Enabled = enabled;
            txtBossAddress.Enabled = enabled;
            txtReturnedPerson.Enabled = enabled;
            txtReturnedTel.Enabled = enabled;
            txtReturnedEmail.Enabled = enabled;
            chkSyncContactPerson.Enabled = enabled;
            txtHotelId.Enabled = enabled;
            txtPoliceTel.Enabled = enabled;
            txtAccountingName.Enabled = enabled;
            txtAccountingTel.Enabled = enabled;
            btnStoreAdd.Enabled = enabled;
            chkSyncUser.Visible = enabled;
            txtAccountingEmail.Enabled = enabled;
            ddlAccountingBank.Enabled = enabled;
            txtAccountingBankId.Enabled = enabled;
            txtAccountingBankDesc.Enabled = enabled;
            if (!enabled)
            {
                ddlAccountingBranch.Attributes["disabled"] = "disabled";
            }
            else
            {
                ddlAccountingBranch.Attributes.Remove("disabled");
            }
            txtAccountingBranchId.Enabled = enabled;
            txtAccountingBranchDesc.Enabled = enabled;
            txtAccountingId.Enabled = enabled;
            txtAccounting.Enabled = enabled;
            txtAccount.Enabled = enabled;
            chkIsEntrustSell.Enabled = enabled;
            rbIsMohist.Enabled = enabled;
            rbHwatai.Enabled = enabled;
            rdlDealAccountingPayType.Enabled = enabled;
            rdlDuplicate.Enabled = enabled;
            rdlInTax.Enabled = enabled;
            rdlEntertainmentTax.Enabled = enabled;
            rdlGeneral.Enabled = enabled;
            rdlNonInvoice.Enabled = enabled;
            rdlInvoiceOther.Enabled = enabled;
            txtInvoiceOtherDesc.Enabled = enabled;
            rdlFreightType.Enabled = enabled;
            chklFreightCompany.Enabled = enabled;
            rdlInvoiceFounder.Enabled = enabled;
            txtInvoiceFounderCompanyName.Enabled = enabled;
            txtInvoiceFounderCompanyID.Enabled = enabled;
            txtInvoiceFounderBossName.Enabled = enabled;

            #endregion Part1 店家資料

            #region Part2 好康優惠

            ddlMultiDeal.Enabled = enabled;

            txtWebUrl.Enabled = enabled;
            txtFBUrl.Enabled = enabled;
            txtPlurkUrl.Enabled = enabled;
            txtBlogUrl.Enabled = enabled;
            txtOtherUrl.Enabled = enabled;
            txtStoreName.Enabled = enabled;
            txtSellerAddress.Enabled = enabled;

            txtReservationTel.Enabled = enabled;
            txtHotelReservationTel.Enabled = enabled;
            txtOpeningTime.Enabled = enabled;
            txtCloseDate.Enabled = enabled;
            txtMrt.Enabled = enabled;
            txtCar.Enabled = enabled;
            txtBus.Enabled = enabled;
            txtVehicleInfo.Enabled = enabled;
            chkIsShowSpecailStore.Enabled = enabled;

            txtProductFreightOtherCompany.Enabled = enabled;
            chkIslandsFreight.Enabled = enabled;
            chklFreightMode.Enabled = enabled;
            rdlIsTakePicture.Enabled = enabled;
            chkIsParallelProduct.Enabled = enabled;
            chkIsExpiringItems.Enabled = enabled;
            txtExpiringItemsDateStart.Enabled = enabled;
            txtExpiringItemsDateEnd.Enabled = enabled;
            txtProductPreservation.Enabled = enabled;
            txtProductUse.Enabled = enabled;
            txtProductSpec.Enabled = enabled;
            txtProductIngredients.Enabled = enabled;
            txtSanctionNumber.Enabled = enabled;
            chklInspectRule.Enabled = enabled;
            txtUseTimeStart.Enabled = enabled;
            txtUseTimeEnd.Enabled = enabled;
            rdoShipType.Enabled = enabled;
            cbl_Icons.Enabled = enabled;
            cbl_deliveryTravelDealTag.Enabled = enabled;
            cbl_couponDealTag.Enabled = enabled;
            cbl_couponTravelDealTag.Enabled = enabled;

            rdlReturnType.Enabled = enabled;
            chkIsNotAccessSms.Enabled = enabled;
            txtOtherMemo.Enabled = enabled;
            chkProductRefund.Enabled = enabled;

            cbxAppLimitedEdition.Enabled = enabled;

            #endregion Part2 好康優惠

            txtPezStores.Enabled = enabled;
            txtPezStoreNotice.Enabled = enabled;
            txtCreateTimeS.Enabled = enabled;
            txtCreateTimeE.Enabled = enabled;
        }

        /// <summary>
        /// 活動控制項是否啟用
        /// </summary>
        /// <param name="parameters"></param>
        private void EnableActiveControl(ActiveControlParameters parameters)
        {
            rdlBusinessType.Enabled = chkIsTravelType.Enabled = parameters.IsBusinessTypeEnable;
            divStoreAdd1.Visible = divStoreAdd2.Visible = parameters.IsStoreAdd;
            EnableControls(parameters.IsEnableControls);
            btnDelete.Visible = parameters.IsDelete;
            btnUpdateStatus.Visible = parameters.IsUpdateStautsVisible;
            divStatus.Visible = parameters.IsUpdateStautsVisible;
            ddlStatus.Enabled = parameters.IsUpdateStautsEnabled;
            divDonwload.Visible = parameters.IsDownload;
            divHistoryMemo.Visible = parameters.IsHistoryMemoVisible;
            divReferred.Visible = parameters.IsReferredVisible;
            txtBid.Enabled = txtUniqueId.Enabled = parameters.IsBidTextEnabled;
            txtSellerId.Enabled = parameters.IsSellerInfoEnabled;
            chkIsContractSend.Enabled = ddlNoContractReason.Enabled = txtNoContractReason.Enabled = parameters.IsContractSendEnabled;

            if (parameters.IsRemittanceTypeVisible)
            {
                lblRemittanceTypeDesc.Visible = false;
                spanRemittanceType.Style.Remove("display");
            }
            else
            {
                lblRemittanceTypeDesc.Visible = true;
                spanRemittanceType.Style.Add("display", "none");
            }

            divCopyBusiness.Visible = parameters.CopyType.Count() > 0;
            foreach (ListItem item in rdlBusinessCopyType.Items)
            {
                BusinessCopyType type = BusinessCopyType.None;
                BusinessCopyType.TryParse(item.Value, out type);
                item.Enabled = parameters.CopyType.Contains(type);
            }
        }

        /// <summary>
        /// 權限狀態顯示控制
        /// </summary>
        /// <param name="parameters"></param>
        private void EnableAuthorizationControl(AuthorizationControlParameters parameters)
        {
            rdlAuthorizationGroup.Enabled = parameters.IsAuthorizationGroup;
            divBusinessHourTimeS.Visible = parameters.IsBusinessHourTimeSVisible;
            rdlSellerLevel.Enabled = parameters.isSellerLevelEnabled;
            foreach (ListItem item in ddlStatus.Items)
            {
                BusinessOrderStatus status = BusinessOrderStatus.Temp;
                BusinessOrderStatus.TryParse(item.Value, out status);
                //item.Enabled = parameters.ListItemStatus.Contains(status);
            }
        }

        private void Addjavascript()
        {
            btnStoreAdd.Attributes.Add("onclick", "var returnValue = window.showModalDialog('StoreAdd.aspx?boid=" + BusinessOrderObjectId + "&enable=" + txtSellerName.Enabled + "',window,'dialogHeight:600px;dialogWidth:630px;dialogLeft:350px;');__doPostBack('UpdatePanel1', '');");
        }

        private History GetHistoryList(Dictionary<string, string> item, string memo)
        {
            History record = new History();
            record.ModifyId = UserId;
            record.ModifyTime = DateTime.Now;

            foreach (KeyValuePair<string, string> history in item)
            {
                record.Changeset += string.Format("更改{0}為\"{1}\", ", history.Key, history.Value);
            }
            if (!string.IsNullOrEmpty(record.Changeset))
            {
                if (!string.IsNullOrWhiteSpace(memo))
                {
                    record.Changeset += memo;
                }
                return record;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 取得匯入excel的分店資料
        /// </summary>
        private List<SalesStore> GetSalesStoreDataList(HttpPostedFile pFile)
        {
            HSSFWorkbook hssfworkbook = new HSSFWorkbook(pFile.InputStream);
            Sheet currentSheet = hssfworkbook.GetSheetAt(0);
            List<SalesStore> storeList = new List<SalesStore>();
            for (int i = 1; i <= currentSheet.LastRowNum; i++)
            {
                NPOI.SS.UserModel.Row row = currentSheet.GetRow(i);
                if (row != null && row.GetCell(0) != null)
                {
                    SalesStore store = new SalesStore();
                    store.BusinessOrderId = BusinessOrderObjectId;
                    store.BranchName = row.GetCell(0) != null ? row.GetCell(0).StringCellValue : string.Empty;
                    int qty = int.TryParse(CommonFacade.GetSheetCellValueString(row.GetCell(1)), out qty) ? qty : 0;
                    store.Quantity = qty;
                    store.StoreTel = CommonFacade.GetSheetCellValueString(row.GetCell(2));
                    string addressValue = row.GetCell(3) != null ? row.GetCell(3).StringCellValue : string.Empty;
                    // 解析地址城市ID、鄉鎮市區ID
                    City city = CityManager.CityGetByStringAddress(addressValue);
                    if (!string.IsNullOrEmpty(addressValue) && city != null)
                    {
                        City township = CityManager.TownshipIdGetByStringAddress(city.Id, addressValue);
                        if (township != null)
                        {
                            store.AddressCityId = city.Id;
                            store.AddressTownshipId = township.Id;
                            store.StoreAddress = addressValue.Replace(city.CityName, string.Empty).Replace(township.CityName, string.Empty);
                        }
                        else
                        {
                            lblStoreAddressMsg.Text = string.Format("匯入分店地址：{0} \"鄉鎮市區\" 資料錯誤!! 請重新匯入。", addressValue);
                            return null;
                        }
                    }
                    else
                    {
                        lblStoreAddressMsg.Text = string.Format("匯入分店地址：{0} \"城市\" 資料錯誤!! 請重新匯入。", addressValue);
                        return null;
                    }
                    store.OpeningTime = row.GetCell(4) != null ? row.GetCell(4).StringCellValue : string.Empty;
                    store.CloseDate = row.GetCell(5) != null ? row.GetCell(5).StringCellValue : string.Empty;
                    store.Mrt = row.GetCell(6) != null ? row.GetCell(6).StringCellValue : string.Empty;
                    store.Car = row.GetCell(7) != null ? row.GetCell(7).StringCellValue : string.Empty;
                    store.Bus = row.GetCell(8) != null ? row.GetCell(8).StringCellValue : string.Empty;
                    store.OV = row.GetCell(9) != null ? row.GetCell(9).StringCellValue : string.Empty;
                    store.WebUrl = row.GetCell(10) != null ? row.GetCell(10).StringCellValue : string.Empty;
                    store.FBUrl = row.GetCell(11) != null ? row.GetCell(11).StringCellValue : string.Empty;
                    store.PlurkUrl = row.GetCell(12) != null ? row.GetCell(12).StringCellValue : string.Empty;
                    store.BlogUrl = row.GetCell(13) != null ? row.GetCell(13).StringCellValue : string.Empty;
                    store.OtherUrl = row.GetCell(14) != null ? row.GetCell(14).StringCellValue : string.Empty;
                    store.Remark = row.GetCell(15) != null ? row.GetCell(15).StringCellValue : string.Empty;
                    store.StoreName = row.GetCell(16) != null ? row.GetCell(16).StringCellValue : string.Empty;
                    store.StoreCompanyAddress = row.GetCell(17) != null ? row.GetCell(17).StringCellValue : string.Empty;
                    store.StoreBossName = row.GetCell(18) != null ? row.GetCell(18).StringCellValue : string.Empty;
                    store.StoreID = CommonFacade.GetSheetCellValueString(row.GetCell(19));
                    store.StoreSignCompanyID = CommonFacade.GetSheetCellValueString(row.GetCell(20));

                    // 銀行/分行代碼
                    if (row.GetCell(21) != null)
                    {
                        if (row.GetCell(21).CellType == CellType.NUMERIC)
                        {
                            int bankCode = int.TryParse(CommonFacade.GetSheetCellValueString(row.GetCell(21)), out bankCode) ? bankCode : 0;
                            store.StoreBankCode = !bankCode.Equals(0) ? string.Format("{0:000}", bankCode) : string.Empty;
                        }
                        else
                        {
                            store.StoreBankCode = row.GetCell(21).StringCellValue;
                        }

                        BankInfoCollection branchs = VourcherFacade.BankInfoGetBranchList(store.StoreBankCode);
                        if (branchs.FirstOrDefault() != null)
                        {
                            store.StoreBankDesc = branchs[0].BankName;
                        }

                        if (row.GetCell(22) != null)
                        {
                            if (row.GetCell(22).CellType == CellType.NUMERIC)
                            {
                                int branchCode = int.TryParse(CommonFacade.GetSheetCellValueString(row.GetCell(22)), out branchCode) ? branchCode : 0;
                                store.StoreBranchCode = !branchCode.Equals(0) ? string.Format("{0:0000}", branchCode) : string.Empty;
                            }
                            else
                            {
                                store.StoreBranchCode = row.GetCell(22).StringCellValue;
                            }

                            BankInfo branch = branchs.Where(x => x.BranchNo == store.StoreBranchCode).FirstOrDefault();
                            if (branch != null)
                            {
                                store.StoreBranchDesc = branch.BranchName;
                            }
                        }
                        else
                        {
                            store.StoreBranchCode = string.Empty;
                        }
                    }
                    else
                    {
                        store.StoreBankCode = string.Empty;
                    }

                    store.StoreAccount = CommonFacade.GetSheetCellValueString(row.GetCell(23));
                    store.StoreAccountName = row.GetCell(24) != null ? row.GetCell(24).StringCellValue : string.Empty;
                    store.StoreAccountingName = row.GetCell(25) != null ? row.GetCell(25).StringCellValue : string.Empty;
                    store.StoreAccountingTel = row.GetCell(26) != null ? row.GetCell(26).StringCellValue : string.Empty;
                    store.StoreEmail = row.GetCell(27) != null ? row.GetCell(27).StringCellValue : string.Empty;
                    store.StoreNotice = row.GetCell(28) != null ? row.GetCell(28).StringCellValue : string.Empty;
                    storeList.Add(store);
                }
            }
            return storeList;
        }

        private string GetCheckBoxCheckStringFormat(string format, CheckBox chkbox, params string[] desc)
        {
            if (chkbox.Checked)
            {
                return chkbox.Text + (!string.IsNullOrEmpty(format) ? string.Format(format, desc) : string.Empty);
            }
            else
            {
                return string.Empty;
            }
        }


        //private bool CheckCopyData()
        //{
        //    string msg = string.Empty;

        //    if ((txtAncestorSequenceBid.Text.Trim() != "") && (!chkIsContinuedSequence.Checked))
        //    {
        //        chkIsContinuedSequence.Checked = true;
        //    }
        //    if ((txtAncestorSequenceBid.Text.Trim() != "") && (!chkIsContinuedQuantity.Checked))
        //    {
        //        msg += "您勾選了「接續銷售數量」，請輸入接續數量BID!";
        //    }
        //    if ((txtAncestorSequenceBid.Text.Trim() == "") && (chkIsContinuedQuantity.Checked))
        //    {
        //        msg += "您未勾選「接續銷售數量」，但是輸入了「接續數量BID」，請確認兩者需一致!";
        //    }
        //    if (msg != "")
        //    {
        //        ShowMessage(msg);
        //        return false;
        //    }

        //    return true;
        //}

        /// <summary>
        /// 公司統一編號／身分證字號 驗證
        /// </summary>
        /// <returns></returns>
        private bool CheckData()
        {
            string msg = string.Empty;
            string personalIdErrorMsg = string.Empty;
            string personalIdErrorMsgBasic = string.Empty;
            string personalIdErrorMsgAccount = string.Empty;
            string companyNoErrorMsg = string.Empty;

            // 若為私人簽約，則需填寫身分證字號
            bool isPrivateContract = false;
            bool.TryParse(rdlPrivateContract.SelectedValue, out isPrivateContract);

            if (isPrivateContract)
            {
                if (!string.IsNullOrEmpty(txtGuiNumber.Text.Trim()))
                {
                    // 統一編號驗證
                    string companyMsg = RegExRules.CompanyNoCheck(txtGuiNumber.Text.Trim());
                    companyNoErrorMsg += !string.IsNullOrEmpty(companyMsg) ? string.Format("【基本資料】{0}\\n", companyMsg) : string.Empty;
                }

                // 身分證字號驗證
                personalIdErrorMsgBasic = RegExRules.PersonalIdCheck(txtPersonalId.Text.Trim());
                personalIdErrorMsgAccount = RegExRules.PersonalIdCheck(txtAccountingId.Text.Trim());
                personalIdErrorMsg += !string.IsNullOrEmpty(personalIdErrorMsgBasic) ? string.Format("【基本資料】{0}\\n", personalIdErrorMsgBasic) : string.Empty;
                personalIdErrorMsg += !string.IsNullOrEmpty(personalIdErrorMsgAccount) ? string.Format("【財務資料】{0}\\n", personalIdErrorMsgAccount) : string.Empty;
                personalIdErrorMsg = !string.IsNullOrEmpty(personalIdErrorMsg) ? string.Format("私人簽約{0}\\n", personalIdErrorMsg) : string.Empty;
            }
            else
            {
                if (string.IsNullOrEmpty(txtGuiNumber.Text.Trim()))
                {
                    msg += "【基本資料】沒有輸入統編!";
                    ShowMessage(msg);
                    return false;
                }

                // 統一編號驗證
                string companyMsg = RegExRules.CompanyNoCheck(txtGuiNumber.Text.Trim());
                companyNoErrorMsg += !string.IsNullOrEmpty(companyMsg) ? string.Format("【基本資料】{0}\\n", companyMsg) : string.Empty;

                // 統一編號驗證
                string accountingMsg = RegExRules.CompanyNoCheck(txtAccountingId.Text.Trim());
                companyNoErrorMsg += !string.IsNullOrEmpty(accountingMsg) ? string.Format("公司行號簽約【財務資料】{0}\\n", accountingMsg) : string.Empty;
            }
            msg += personalIdErrorMsg + companyNoErrorMsg;

            // 僅限身分證字號必須通過驗證才能送審
            if (!string.IsNullOrEmpty(personalIdErrorMsgBasic + personalIdErrorMsgAccount))
            {
                ShowMessage(msg);
                return false;
            }

            if (ActiveControls.IsEnableControls)
            {
                // 發票開立人檢查
                string invoiceFounderCheck = string.Empty;
                if (string.IsNullOrWhiteSpace(txtInvoiceFounderCompanyName.Text))
                {
                    invoiceFounderCheck += "公司名稱、";
                }
                if (string.IsNullOrWhiteSpace(txtInvoiceFounderCompanyID.Text))
                {
                    invoiceFounderCheck += "統編 / ID、";
                }
                if (string.IsNullOrWhiteSpace(txtInvoiceFounderBossName.Text))
                {
                    invoiceFounderCheck += "負責人、";
                }
                msg += string.Format("發票開立人-{0}未填!!", invoiceFounderCheck.TrimEnd('、'));
                if (!string.IsNullOrWhiteSpace(invoiceFounderCheck))
                {
                    ShowMessage(msg);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 根據word範本下載word或pdf
        /// </summary>
        /// <param name="wordFile">Part1、Part2</param>
        /// <param name="wordFile3">Part3</param>
        /// <param name="wordFile4">Part4</param>
        /// <param name="dataList">取代參數</param>
        private void DownloadWordByWordTemplate(BusinessType type, Dictionary<string, string> dataList, List<SalesStore> storeList)
        {
            #region get template file

            string wordFile = string.Empty;
            string footer = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderFooter.docx");
            string storelist = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderStorelist.docx");
            string storelistAccount = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderStorelistAccount.docx");

            switch (type)
            {
                case BusinessType.Ppon:
                    wordFile = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderPponTravel.docx");
                    break;

                case BusinessType.Product:
                    wordFile = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderProductTravel.docx");
                    break;

                case BusinessType.None:
                default:
                    break;
            }

            #endregion get template file

            // Load the document.
            using (DocX document = DocX.Load(wordFile))
            {
                // Replace text in this document.
                foreach (KeyValuePair<string, string> item in dataList)
                {
                    document.ReplaceText(item.Key, !string.IsNullOrEmpty(item.Value) ? item.Value : string.Empty);
                }

                #region store list

                if (storeList.Count > 0)
                {
                    document.InsertParagraph().Append("分店資訊").Bold().FontSize(16);
                }

                foreach (SalesStore store in storeList)
                {
                    if (!string.IsNullOrEmpty(store.StoreAccount))
                    {
                        document.InsertDocument(DocX.Load(storelistAccount));
                    }
                    else
                    {
                        document.InsertDocument(DocX.Load(storelist));
                    }

                    document.ReplaceText("BranchName", store.BranchName);
                    document.ReplaceText("StoreTel", store.StoreTel);
                    document.ReplaceText("OpeningTime", store.OpeningTime);
                    document.ReplaceText("Address", CityManager.CityTownShopStringGet(store.AddressTownshipId) + store.StoreAddress);
                    document.ReplaceText("CloseDate", store.CloseDate);
                    document.ReplaceText("StoreEmail", store.StoreEmail);
                    document.ReplaceText("StoreNotice", store.StoreNotice);

                    if (!string.IsNullOrEmpty(store.StoreAccount))
                    {
                        document.ReplaceText("StoreName", store.StoreName);
                        document.ReplaceText("StoreBossName", store.StoreBossName);
                        document.ReplaceText("StoreCompanyAdd", store.StoreCompanyAddress ?? string.Empty);
                        document.ReplaceText("StoreSignCompID", store.StoreSignCompanyID ?? string.Empty);
                        document.ReplaceText("StoreID", store.StoreID);
                        document.ReplaceText("StoreBranchCode", store.StoreBranchCode);
                        document.ReplaceText("StoreBankCode", store.StoreBankCode);
                        document.ReplaceText("StoreAccountName", store.StoreAccountName);
                        document.ReplaceText("StoreAccountingName", store.StoreAccountingName ?? string.Empty);
                        document.ReplaceText("StoreAccountingTel", store.StoreAccountingTel ?? string.Empty);
                        document.ReplaceText("StoreAccount", store.StoreAccount);
                    }
                }

                #endregion store list

                document.InsertDocument(DocX.Load(footer));

                // Save changes made to this document.
                MemoryStream memoryStream = new MemoryStream();
                document.SaveAs(memoryStream);

                // print word
                Response.ContentType = "application/msword";
                Response.AddHeader("content-disposition", "attachment; filename=\"" + string.Format(dataList["SellerName"] + "_{0}", DateTime.Now.ToString("yyyy_MM_dd")) + ".doc\"");
                memoryStream.WriteTo(Response.OutputStream);
                Response.End();
            } // Release this document from memory.
        }

        /// <summary>
        /// 根據htm範本下載pdf
        /// </summary>
        /// <param name="wordFile">Part1、Part2</param>
        /// <param name="wordFile3">Part3</param>
        /// <param name="wordFile4">Part4</param>
        /// <param name="dataList">取代參數</param>
        private void DownloadPdfByHtmlTemplate(BusinessType type, Dictionary<string, string> dataList, List<SalesStore> storeList)
        {
            #region get template file

            string wordFile = string.Empty;
            string magazine = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderMagazine.htm");
            string pezStore = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderPezStore.htm");
            string footer = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderFooter.htm");
            string storelist = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderStorelist.htm");
            string storelistAccount = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderStorelistAccount.htm");

            switch (type)
            {
                case BusinessType.Ppon:
                    wordFile = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderPpon.htm");
                    break;

                case BusinessType.Product:
                    wordFile = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderProduct.htm");
                    break;

                case BusinessType.None:
                default:
                    break;
            }

            #endregion get template file

            string nextPage = @"<br clear=all style='page-break-before:always'>";
            System.Text.Encoding encode = Encoding.UTF8;

            StreamReader reader = new StreamReader(wordFile, encode);
            string content = reader.ReadToEnd();
            reader.Close();
            //if (!string.IsNullOrEmpty(txtPezStores.Text.Trim()))
            //{
            //    content += nextPage;
            //    StreamReader reader4 = new StreamReader(pezStore, encode);
            //    content += reader4.ReadToEnd();
            //    reader4.Close();
            //}

            // replace
            foreach (KeyValuePair<string, string> item in dataList)
            {
                content = Regex.Replace(content, item.Key, !string.IsNullOrEmpty(item.Value) ? item.Value : string.Empty);
            }

            #region store list

            string storeContent = string.Empty;
            if (storeList.Count > 0)
            {
                storeContent += nextPage;
                storeContent += "<h2>分店資訊</h2>";
            }

            foreach (SalesStore store in storeList)
            {
                StreamReader readerStore = new StreamReader(storelist, encode);
                if (!string.IsNullOrEmpty(store.StoreAccount))
                {
                    readerStore = new StreamReader(storelistAccount, encode);
                }

                storeContent += readerStore.ReadToEnd();
                readerStore.Close();

                storeContent = Regex.Replace(storeContent, "BranchName", store.BranchName);
                storeContent = Regex.Replace(storeContent, "StoreTel", store.StoreTel);
                storeContent = Regex.Replace(storeContent, "OpeningTime", store.OpeningTime);
                storeContent = Regex.Replace(storeContent, "Address", CityManager.CityTownShopStringGet(store.AddressTownshipId) + store.StoreAddress);
                storeContent = Regex.Replace(storeContent, "CloseDate", store.CloseDate);
                storeContent = Regex.Replace(storeContent, "StoreEmail", store.StoreEmail);
                storeContent = Regex.Replace(storeContent, "StoreNotice", store.StoreNotice);

                if (!string.IsNullOrEmpty(store.StoreAccount))
                {
                    storeContent = Regex.Replace(storeContent, "StoreName", store.StoreName);
                    storeContent = Regex.Replace(storeContent, "StoreBossName", store.StoreBossName);
                    storeContent = Regex.Replace(storeContent, "StoreCompanyAdd", store.StoreCompanyAddress ?? string.Empty);
                    storeContent = Regex.Replace(storeContent, "StoreSignCompID", store.StoreSignCompanyID ?? string.Empty);
                    storeContent = Regex.Replace(storeContent, "StoreID", store.StoreID);
                    storeContent = Regex.Replace(storeContent, "StoreBranchCode", store.StoreBranchCode);
                    storeContent = Regex.Replace(storeContent, "StoreBankCode", store.StoreBankCode);
                    storeContent = Regex.Replace(storeContent, "StoreAccountName", store.StoreAccountName);
                    storeContent = Regex.Replace(storeContent, "StoreAccountingName", store.StoreAccountingName ?? string.Empty);
                    storeContent = Regex.Replace(storeContent, "StoreAccountingTel", store.StoreAccountingTel ?? string.Empty);
                    storeContent = Regex.Replace(storeContent, "StoreAccount", store.StoreAccount);
                }
            }
            content += storeContent;

            #endregion store list

            StreamReader readerFooter = new StreamReader(footer, encode);
            content += readerFooter.ReadToEnd();
            readerFooter.Close();

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment; filename=\"" + string.Format(dataList["SellerName"] + "_{0}", DateTime.Now.ToString("yyyy_MM_dd")) + ".pdf\"");
            PdfConverter pdfc = new PdfConverter();
            pdfc.LicenseKey = @"YklTQlNCVFZCVExSQlFTTFNQTFtbW1s=";
            pdfc.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfc.PdfDocumentOptions.TopMargin = 50;
            pdfc.PdfDocumentOptions.LeftMargin = 40;
            int buttonMargin = int.TryParse(txtDonwloadBottomMarginFix.Text, out buttonMargin) ? buttonMargin : 0;
            pdfc.PdfDocumentOptions.BottomMargin = 50 + buttonMargin;
            pdfc.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfc.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(pdfc.GetPdfBytesFromHtmlString(content));
            Response.End();
        }

        private void Export(string fileName, List<SalesStore> storeList)
        {
            if (storeList.Count > 0)
            {
                Workbook workbook = new HSSFWorkbook();

                #region 連鎖分店

                // 新增試算表
                Sheet store = workbook.CreateSheet("分店資訊");
                NPOI.SS.UserModel.Row storeHeaderRow = store.CreateRow(0);
                storeHeaderRow.CreateCell(0).SetCellValue("分店名稱");
                storeHeaderRow.CreateCell(1).SetCellValue("電話");
                storeHeaderRow.CreateCell(2).SetCellValue("地址");
                storeHeaderRow.CreateCell(3).SetCellValue("營業時間");
                storeHeaderRow.CreateCell(4).SetCellValue("公休日");
                storeHeaderRow.CreateCell(5).SetCellValue("捷運");
                storeHeaderRow.CreateCell(6).SetCellValue("開車");
                storeHeaderRow.CreateCell(7).SetCellValue("公車");
                storeHeaderRow.CreateCell(8).SetCellValue("其他交通資訊");
                storeHeaderRow.CreateCell(9).SetCellValue("官網網址");
                storeHeaderRow.CreateCell(10).SetCellValue("FaceBook網址");
                storeHeaderRow.CreateCell(11).SetCellValue("Plurk網址");
                storeHeaderRow.CreateCell(12).SetCellValue("部落格網址");
                storeHeaderRow.CreateCell(13).SetCellValue("其他網址");
                storeHeaderRow.CreateCell(14).SetCellValue("備註");
                storeHeaderRow.CreateCell(15).SetCellValue("簽約公司名稱");
                storeHeaderRow.CreateCell(16).SetCellValue("簽約公司地址");
                storeHeaderRow.CreateCell(17).SetCellValue("負責人名稱");
                storeHeaderRow.CreateCell(18).SetCellValue("受款人ID/統編");
                storeHeaderRow.CreateCell(19).SetCellValue("簽約公司ID/統一編號");
                storeHeaderRow.CreateCell(20).SetCellValue("受款人銀行代號");
                storeHeaderRow.CreateCell(21).SetCellValue("受款人分行代號");
                storeHeaderRow.CreateCell(22).SetCellValue("受款人帳號");
                storeHeaderRow.CreateCell(23).SetCellValue("受款人戶名");
                storeHeaderRow.CreateCell(24).SetCellValue("帳務聯絡人");
                storeHeaderRow.CreateCell(25).SetCellValue("帳務聯絡人電話");
                storeHeaderRow.CreateCell(26).SetCellValue("店家email");
                storeHeaderRow.CreateCell(27).SetCellValue("店家備註");

                NPOI.SS.UserModel.Cell storeCell = null;
                int storeRowCount = 0;
                int storeRowIndex = 1;
                foreach (SalesStore salesStore in storeList)
                {
                    NPOI.SS.UserModel.Row dataRow = store.CreateRow(storeRowIndex);

                    storeCell = dataRow.CreateCell(0); storeCell.SetCellValue(salesStore.BranchName);
                    storeCell = dataRow.CreateCell(1); storeCell.SetCellValue(salesStore.StoreTel);
                    storeCell = dataRow.CreateCell(2); storeCell.SetCellValue(CityManager.CityTownShopStringGet(salesStore.AddressTownshipId) + salesStore.StoreAddress);
                    storeCell = dataRow.CreateCell(3); storeCell.SetCellValue(salesStore.OpeningTime);
                    storeCell = dataRow.CreateCell(4); storeCell.SetCellValue(salesStore.CloseDate);
                    storeCell = dataRow.CreateCell(5); storeCell.SetCellValue(salesStore.Mrt);
                    storeCell = dataRow.CreateCell(6); storeCell.SetCellValue(salesStore.Car);
                    storeCell = dataRow.CreateCell(7); storeCell.SetCellValue(salesStore.Bus);
                    storeCell = dataRow.CreateCell(8); storeCell.SetCellValue(salesStore.OV);
                    storeCell = dataRow.CreateCell(9); storeCell.SetCellValue(salesStore.WebUrl);
                    storeCell = dataRow.CreateCell(10); storeCell.SetCellValue(salesStore.FBUrl);
                    storeCell = dataRow.CreateCell(11); storeCell.SetCellValue(salesStore.PlurkUrl);
                    storeCell = dataRow.CreateCell(12); storeCell.SetCellValue(salesStore.BlogUrl);
                    storeCell = dataRow.CreateCell(13); storeCell.SetCellValue(salesStore.OtherUrl);
                    storeCell = dataRow.CreateCell(14); storeCell.SetCellValue(salesStore.Remark);
                    storeCell = dataRow.CreateCell(15); storeCell.SetCellValue(salesStore.StoreName);
                    storeCell = dataRow.CreateCell(16); storeCell.SetCellValue(salesStore.StoreCompanyAddress);
                    storeCell = dataRow.CreateCell(17); storeCell.SetCellValue(salesStore.StoreBossName);
                    storeCell = dataRow.CreateCell(18); storeCell.SetCellValue(salesStore.StoreID);
                    storeCell = dataRow.CreateCell(19); storeCell.SetCellValue(salesStore.StoreSignCompanyID);
                    storeCell = dataRow.CreateCell(20); storeCell.SetCellValue(salesStore.StoreBankCode);
                    storeCell = dataRow.CreateCell(21); storeCell.SetCellValue(salesStore.StoreBranchCode);
                    storeCell = dataRow.CreateCell(22); storeCell.SetCellValue(salesStore.StoreAccount);
                    storeCell = dataRow.CreateCell(23); storeCell.SetCellValue(salesStore.StoreAccountName);
                    storeCell = dataRow.CreateCell(24); storeCell.SetCellValue(salesStore.StoreAccountingName);
                    storeCell = dataRow.CreateCell(25); storeCell.SetCellValue(salesStore.StoreAccountingTel);
                    storeCell = dataRow.CreateCell(26); storeCell.SetCellValue(salesStore.StoreEmail);
                    storeCell = dataRow.CreateCell(27); storeCell.SetCellValue(salesStore.StoreNotice);

                    storeRowIndex++;
                    storeRowCount++;
                }

                #endregion 連鎖分店

                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode(fileName)));
                workbook.Write(HttpContext.Current.Response.OutputStream);
            }
        }

        private string GetBookingSystemType(BookingType type)
        {
            if (type == BookingType.Coupon)
            {
                return "配合預約服務(訂位)";
            }
            else if (type == BookingType.Travel)
            {
                return "配合預約服務(訂房)";
            }
            else
            {
                return "不配合預約服務";
            }
        }

        private void SetSalesmanNameArray()
        {
            IEnumerable<ViewEmployee> viewEmpCol = HumanFacade.ViewEmployeeGetAll();
            string emails = string.Empty;
            string names = string.Empty;
            foreach (ViewEmployee emp in viewEmpCol)
            {
                if (emails == string.Empty)
                {
                    emails = string.Format("'{0}'", emp.Email);
                }
                else
                {
                    emails = string.Format("{0},'{1}'", emails, emp.Email);
                }
            }
            SalesEmailArray = "'" + string.Join("','", viewEmpCol.Select(x => x.Email)) + "'";

            int value;
            int.TryParse(hidStatus.Value, out value);
            BusinessOrderStatus status = (BusinessOrderStatus)value;

            dynamic salesInfo = status != BusinessOrderStatus.Temp ? viewEmpCol.Select(x => new { value = x.EmpName, label = x.EmpName, desc = x.Mobile }) : viewEmpCol.Where(x => x.DepartureDate == null && !x.IsInvisible).Select(x => new { value = x.EmpName, label = x.EmpName, desc = x.Mobile });
            SalesInfoArray = new JsonSerializer().Serialize(salesInfo);
        }

        #endregion private method
    }

    #region parameters

    public class ActiveControlParameters
    {
        /// <summary>
        /// 工單類型是否啟用
        /// </summary>
        public bool IsBusinessTypeEnable;

        /// <summary>
        /// 是否可新增分店
        /// </summary>
        public bool IsStoreAdd;

        /// <summary>
        /// 是否可編輯工單內容
        /// </summary>
        public bool IsEnableControls;

        /// <summary>
        /// 是否可刪除工單
        /// </summary>
        public bool IsDelete;

        /// <summary>
        /// 是否可儲存工單
        /// </summary>
        public bool IsUpdateStautsVisible;

        /// <summary>
        /// 是否可變更工單狀態
        /// </summary>
        public bool IsUpdateStautsEnabled;

        /// <summary>
        /// 是否可下載工單
        /// </summary>
        public bool IsDownload;

        /// <summary>
        /// 是否可註記歷史紀錄備註
        /// </summary>
        public bool IsHistoryMemoVisible;

        /// <summary>
        /// 好康條款異動是否可見
        /// </summary>
        public bool IsProvisionEditVisible;

        /// <summary>
        /// 是否可轉件工單
        /// </summary>
        public bool IsReferredVisible;

        /// <summary>
        /// 可變更檔次編號
        /// </summary>
        public bool IsBidTextEnabled;

        /// <summary>
        /// 可變更賣家編號
        /// </summary>
        public bool IsSellerInfoEnabled;

        /// <summary>
        /// 是否可變更合約狀態
        /// </summary>
        public bool IsContractSendEnabled;

        /// <summary>
        /// 是否顯示出帳方式
        /// </summary>
        public bool IsRemittanceTypeDescVisible;

        /// <summary>
        /// 是否可變更出帳方式
        /// </summary>
        public bool IsRemittanceTypeVisible;

        /// <summary>
        /// 複製類型
        /// </summary>
        public List<BusinessCopyType> CopyType;

        public ActiveControlParameters()
        {
            CopyType = new List<BusinessCopyType>();
        }
    }

    public class AuthorizationControlParameters
    {
        /// <summary>
        /// 是否可變更審核區域
        /// </summary>
        public bool IsAuthorizationGroup;

        /// <summary>
        /// 是否可變更上檔日期
        /// </summary>
        public bool IsBusinessHourTimeSVisible;

        /// <summary>
        /// 是否可變更賣家規模
        /// </summary>
        public bool isSellerLevelEnabled;

        /// <summary>
        /// 可見狀態項目
        /// </summary>
        public List<BusinessOrderStatus> ListItemStatus;

        public AuthorizationControlParameters()
        {
            ListItemStatus = new List<BusinessOrderStatus>();
        }
    }

    #endregion parameters
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="SellerContent.aspx.cs" Inherits="LunchKingSite.Web.Sal.SellerContent" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Register Src="../User/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <%--<link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/ui-lightness/jquery-ui.min.css" rel="stylesheet" type="text/css" />--%>
    <link href="<%=ResolveUrl("~/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Tools/js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="../Tools/js/jquery.nav.js"></script>
    <link href="<%=ResolveUrl("~/Tools/js/fancybox/jquery.fancybox.css") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Tools/js/fancybox/jquery.fancybox.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../Tools/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../Themes/OWriteOff/plugin/bpopup/jquery.bpopup.js"></script>
    <%--<link rel="stylesheet" href="<%=ResolveUrl("~/Themes/default/images/17Life/Regulars/css/bootstrap.css") %>"  type="text/css">--%>


    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="../../Themes/OWriteOff/plugin/bpopup/jquery.bpopup.js"></script>



    <style type="text/css">
        .mc-navbtn {
            cursor: pointer;
        }
        /*側邊anchor*/
        .nav-anchor {
            position: fixed;
            top: 125px;
            margin-left: 970px;
            z-index: 99;
        }

        .nav-anchor li {
            margin-bottom: 2px;
            text-align: center;
        }

        .nav-anchor a {
            width: 100px;
            height: 30px;
            line-height: 30px;
            background: #ededed;
            color: #666;
            display: block;
            font-size: 11px;
            text-decoration: none;
            text-transform: uppercase;
        }

            .nav-anchor a:hover {
                background: #dedede;
            }

        .nav-anchor .current a {
            background: #3F3F3F;
            color: #e5e5e5;
        }

        .nav-anchor li:first-child a {
            -webkit-border-radius: 3px 3px 0 0;
            -moz-border-radius: 3px 3px 0 0;
            border-radius: 3px 3px 0 0;
        }

        .nav-anchor li:last-child a {
            -webkit-border-radius: 0 0 3px 3px;
            -moz-border-radius: 0 0 3px 3px;
            border-radius: 0 0 3px 3px;
        }

        #fileHolder { 
            border: 10px dashed #ccc; 
            width: 80%; 
            min-height: 200px; 
            margin: 20px auto;
            text-align:center;
            line-height: 200px;          
        }
        div.pager {
            text-align: center;
            margin: 1em 0;
        }

        div.pager span {
            display: inline-block;
            width: 1.8em;
            height: 1.8em;
            line-height: 1.8;
            text-align: center;
            cursor: pointer;
            background: #ededed;
            color: #000;
            margin-right: 0.5em;
            border:1px;

        }

        div.pager span.active {
            background: #c00;
            color:#fff;
        }

        #fileHolder.hover { border: 10px dashed #0c0; }
        #fileHolder img { display: block; margin: 10px auto; }
        #fileHolder p { margin: 10px; font-size: 14px; }


        .breadcrumb {
	        /*centering*/
	        display: inline-block;
	        box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.35);
	        overflow: hidden;
	        border-radius: 5px;
	        /*Lets add the numbers for each link using CSS counters. flag is the name of the counter. to be defined using counter-reset in the parent element of the links*/
	        counter-reset: flag; 
        }

        .breadcrumb a {
	        text-decoration: none;
	        outline: none;
	        display: block;
	        float: left;
	        font-size: 12px;
	        line-height: 36px;
	        color: white;
	        /*need more margin on the left of links to accomodate the numbers*/
	        padding: 0 10px 0 60px;
	        background: #666;
	        background: linear-gradient(#666, #333);
	        position: relative;
        }


        /*adding the arrows for the breadcrumbs using rotated pseudo elements*/
        .breadcrumb a:after {
	        content: '';
	        position: absolute;
	        top: 0; 
	        right: -18px; /*half of square's length*/
	        /*same dimension as the line-height of .breadcrumb a */
	        width: 36px; 
	        height: 36px;
	        /*as you see the rotated square takes a larger height. which makes it tough to position it properly. So we are going to scale it down so that the diagonals become equal to the line-height of the link. We scale it to 70.7% because if square's: 
	        length = 1; diagonal = (1^2 + 1^2)^0.5 = 1.414 (pythagoras theorem)
	        if diagonal required = 1; length = 1/1.414 = 0.707*/
	        transform: scale(0.707) rotate(45deg);
	        /*we need to prevent the arrows from getting buried under the next link*/
	        z-index: 1;
	        /*background same as links but the gradient will be rotated to compensate with the transform applied*/
	        background: #666;
	        background: linear-gradient(135deg, #666, #333);
	        /*stylish arrow design using box shadow*/
	        box-shadow: 
		        2px -2px 0 2px rgba(0, 0, 0, 0.4), 
		        3px -3px 0 2px rgba(255, 255, 255, 0.1);
	        /*
		        5px - for rounded arrows and 
		        50px - to prevent hover glitches on the border created using shadows*/
	        border-radius: 0 5px 0 50px;
        }
       
        .flat a, .flat a:after {
	        background: white;
	        color: black;
	        transition: all 0.5s;
        }

        .flat a.active, 
        .flat a.active:after{
            color:#fff;
	        background: #AF0000;
        }

        /*營業時間、公休日X鈕*/
        .remove a {
            padding-right:30px;
            background: url(/Themes/PCweb/images/x.png) no-repeat right center;
        }

        .well {
          min-height: 20px;
          padding: 19px;
          margin-bottom: 20px;
          background-color: #f5f5f5;
          border: 1px solid #e3e3e3;
          border-radius: 4px;
          -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
                  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
        }
        .well blockquote {
          border-color: #ddd;
          border-color: rgba(0, 0, 0, .15);
        }
        .well-lg {
          padding: 24px;
          border-radius: 6px;
        }
        .well-sm {
          padding: 9px;
          border-radius: 3px;
        }
        fieldset {
          min-width: 0;
          padding: 0;
          margin: 0;
          border: 0;
        }
        legend {
          display: block;
          width: 100%;
          padding: 0;
          margin-bottom: 20px;
          font-size: 21px;
          line-height: inherit;
          color: #333;
          border: 0;
          border-bottom: 1px solid #e5e5e5;
        }

        .btnAdd {
            float: right;
            width: 100%;
            padding: 15px 0;
            text-align: center;
            border-radius: 99em;
            width: 20px;
            height: 20px;
            padding: 0px 0px;
            background-image: linear-gradient(to bottom, #c10000, #c10000);

            color: #ffffff;
            font-weight: bold;
            cursor: pointer;
            text-decoration:none
                
        }

        /*負責業務提示*/
        .tooltip {
            position: relative;
            display: inline-block;
        }

        .title {
            background-color: #f2f2f2;
            border-collapse: collapse;
            padding: 15px;
        }  
        .bpopup-modalV2 {
        /*彈窗預設為隱藏，直到jquery調用才顯示*/
        display: none;
        padding:30px;
        background: #fff;
    } 

        /* 用bpopup.js製作的彈窗 以及 其內UI Start */
        .btn-default {
            width: 100px;
            height: 20px;
            background: #ccc;
            padding: 10px;
            text-align: center;
            border-radius: 5px;
            color: white;
            font-size:medium;
        }
        .btn-warn {
            width: 100px;
            height: 20px;
            background: #bf0000;
            padding: 10px;
            text-align: center;
            border-radius: 5px;
            color: white;
            font-size:medium;
        }
        .bpopup-modal {
            /*彈窗預設為隱藏，直到jquery調用才顯示*/
            display: none;
            /*margin-left: 250px;*/
            width: calc(100vw - 575px);
            background: #fff;
        }

        .bpopup-modal .row {
            margin-bottom: 0;
        }

        .bpopup-modal.bpopup-modal-toast {
            width: 400px;
            color: #000;
            border-radius: 10px;
        }


        .bpopup-modal.bpopup-modal-toast .bpopup-modal-title {
            text-align: left;
            margin: 20px;
            margin-top:50px;
            margin-bottom:50px;
            font-size: 18px;
        }

        .bpopup-modal-footer {
            margin-top: 20px;
            margin-left:20px;
            margin-bottom:50px;
            text-align:center;
        }

        .bpopup-modal a {
            cursor: pointer;
        }

        /* 用bpopup.js製作的彈窗 以及 其內UI End */
    </style>
    <script type="text/javascript">
        var frequency = {
            weekly: 1,
            monthly: 2,
            special: 3,
            alldayround: 99
        }
        $(document).ready(function () {

            $('#txtCompanyAddress').attr('autocomplete', 'off');

            var city = $('#ddlCompanyCityId');
            if ($(city).val() != '-1') {
                BindTownship(city);
            }

            var city = $('#ddlLocationCity');
            if ($(city).val() != '-1') {
                BindLocationTownship(city);
            }

            var bank = $('#ddlBankCode');
            if ($(bank).val() != '-1') {
                BindBranchCode(bank);
            }

            appendSlide();
            $('#subMenu').onePageNav();

            $('#txtContractDueDate').datepicker();
            $("#btnCreateTravelProposal").fancybox();

            $("#btnSellerLvDetail").fancybox({
                helpers: {

                    //overlay: { closeClick: false }
                },
                afterClose: function () {
                }
            });


            //超取申請駁回框
            $('#btnRejectPopupFamily').fancybox({
                'parent': "form:first",
                'href': '#divRejectFamily'
            })
            $('#btnRejectPopupFamily').attr('disabled', $('#btnPass').attr('disabled'));
            $('#btnRejectPopupFamily').attr('style', "visibility:'" + $('#btnRejectPopupFamily').attr('visibility') + "'");


            $('#btnRejectPopupSeven').fancybox({
                'parent': "form:first",
                'href': '#divRejectSeven'
            })
            $('#btnRejectPopupSeven').attr('disabled', $('#btnPass').attr('disabled'));
            $('#btnRejectPopupSeven').attr('style', "visibility:'" + $('#btnRejectPopupSeven').attr('visibility') + "'");

            //結業營業日
            $('#formCloseDownDate').datepicker();
            $('#closeDownContainer input[type="radio"]').change(closeDownDateChange);
            if ($('#closeDownContainer input[type="radio"]').eq(0).filter(':checked').length === 1) {   //營業狀態若是 "正常營業" => 結束營業日期不可輸入東西
                $('#formCloseDownDate').attr('disabled', 'disabled');
            }

            //設置賣家規模
            var d = $('#hdSellerLevelDetail').val();
            SetSellerLevelDetail(d);


            $('#txtParentSellerName').autocomplete({
                source: function (request, response) {
                    $.ajax(
                        {
                            type: "POST",
                            url: "SellerContent.aspx/GetSellerNameArray",
                            data: JSON.stringify({
                                sellerName: request.term
                            }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            dataFilter: function (data) { return data; },
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.Label,
                                        value: item.Value
                                    }
                                }))
                            }, error: function (res) {
                                console.log(res);
                            }
                        });
                },
                focus: function (event, ui) {
                    $("#txtParentSellerName").val(ui.item.label.substring(ui.item.label.indexOf(' ') + 1));
                    $('#liParentSellerId').html(ui.item.label.substring(0, ui.item.label.indexOf(' ')));

                    return false;
                },
                select: function (event, ui) {
                    $("#txtParentSellerName").val(ui.item.label.substring(ui.item.label.indexOf(' ') + 1));
                    $('#liParentSellerId').html(ui.item.label.substring(0, ui.item.label.indexOf(' ')));
                    $("#hidParentSellerGuid").val(ui.item.value);
                    //選擇項目時
                    $(this).trigger('change');
                    return false;
                }
            });
            $('#txtParentSellerName').blur(function () {
                if ($.trim($(this).val()) == "") {
                    $('#hidParentSellerGuid').val("");
                }
            });

            //檢查是否有該公司&子孫公司
            $('#txtParentSellerName').change(function () {


                $.ajax(
                    {
                        type: "POST",
                        url: "SellerContent.aspx/IsCompany",
                        data: JSON.stringify({
                            CompanyName: $(this).val(),
                            SellerGuid: $('#hidParentSellerGuid').val()
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d == 'NoCompany') {
                                $('#NoCompany').show();
                                $('#NotParentCompany').hide();
                                $('#hidParentSellerGuid').val('');
                            }
                            else if (data.d != '') {
                                $('#NoCompany').hide();
                                $('#NotParentCompany').hide();
                                $('#hidParentSellerGuid').val(data.d);
                            }
                            else if (data.d == '') {
                                $('#NoCompany').hide();
                                $('#NotParentCompany').hide();
                                $('#hidParentSellerGuid').val('');
                            }
                            if ($('#hidParentSellerGuid').val() != '') {
                                $.ajax(
                                    {
                                        type: "POST",
                                        url: "SellerContent.aspx/IsChildrenCompany",
                                        data: "{ 'SellerGuid': '<%=SellerGuid%>','CompanyID': '" + $('#hidParentSellerGuid').val() + "'}",
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        success: function (data) {
                                            if (data.d == false) {
                                                $('#NotParentCompany').show();
                                                $('#NoCompany').hide();
                                            }
                                            else {
                                                $('#NotParentCompany').hide();
                                                $('#NoCompany').hide();
                                            }

                                        }, error: function (res) {
                                            console.log(res);
                                        }
                                    });
                            }


                        }, error: function (res) {
                            console.log(res);
                        }
                    });


            });

            // 設定顯示的tab
            var tab = '<%= Tab %>';
            if (tab != '') {
                SelectTab($('.mc-navbar').find('[tab=' + tab + ']'));
            }


            $("#tblSellerChangeLog").each(function () {
                var currentPage = 0;
                var numPerPage = 6;
                var $table = $(this);
                $table.bind('repaginate', function () {
                    $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
                });
                $table.trigger('repaginate');
                var numRows = $table.find('tbody tr').length;
                var numPages = Math.ceil(numRows / numPerPage);
                var $pager = $('<div class="pager"></div>');
                for (var page = 0; page < numPages; page++) {
                    $('<span class="page-number"></span>').text(page + 1).bind('click', {
                        newPage: page
                    }, function (event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');
                    }).appendTo($pager).addClass('clickable');
                }
                $pager.insertAfter($table).find('span.page-number:first').addClass('active');
            });


            $("#tblSellerManageLog").each(function () {
                var currentPage = 0;
                var numPerPage = 6;
                var $table = $(this);
                $table.bind('repaginate', function () {
                    $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
                });
                $table.trigger('repaginate');
                var numRows = $table.find('tbody tr').length;
                var numPages = Math.ceil(numRows / numPerPage);
                var $pager = $('<div class="pager"></div>');
                for (var page = 0; page < numPages; page++) {
                    $('<span class="page-number"></span>').text(page + 1).bind('click', {
                        newPage: page
                    }, function (event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');
                    }).appendTo($pager).addClass('clickable');
                }
                $pager.insertAfter($table).find('span.page-number:first').addClass('active');
            });

            //財務資料、聯絡人預設閉合
            CollapseBlock('span-fiance', 'finance-form');
            CollapseBlock('span-contact', 'contact-form');


            if ($("#hdEnabled").val() == "false") {
                $("#spanCloneContact input").attr("disabled", "disabled");
                $("#spanCloneContact textarea").attr("readonly", "readonly");
            }

            if ($("#ddlSellerLevel").val() == "<%=(int)SellerLevel.Delivery%>") {
                $("#storelist").hide();
            } else {
                $("#storelist").show();
            }
            $("#ddlSellerLevel").change(function () {
                if ($(this).val() == "<%=(int)SellerLevel.Delivery%>") {
                    $("#storelist").hide();
                } else {
                    $("#storelist").show();
                }
            });




            $('#txtReferralSale').autocomplete({
                source: function (request, response) {
                    $.ajax(
                        {
                            type: "POST",
                            url: "SellerContent.aspx/GetSalesmanNameArray",
                            data: JSON.stringify({
                                userName: request.term
                            }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            dataFilter: function (data) { return data; },
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.Label,
                                        value: item.Value
                                    }
                                }))
                            }, error: function (res) {
                                console.log(res);
                            }
                        });
                },
            });

            $('#txtReferralOther').autocomplete({
                source: function (request, response) {
                    $.ajax(
                        {
                            type: "POST",
                            url: "SellerContent.aspx/GetEmployeeNameArray",
                            data: "{ 'userName': '" + request.term + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            dataFilter: function (data) { return data; },
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.Label,
                                        value: item.Value
                                    }
                                }))
                            }, error: function (res) {
                                console.log(res);
                            }
                        });
                },
            });

            //顯示文字
            $("#ddlTownshipId").change(function () {
                ShowText("Address");
            });

            $("#ddlLocationTownshipId").change(function () {
                ShowText("LocationAddress");
            });

            $("#txtCompanyAddress").change(function () {
                ShowText("Address");
            });

            $("#txtLocationAddress").change(function () {
                ShowText("LocationAddress");
            });

            $("#ddlBankCode").change(function () {
                ShowText("Bank");
            });

            $("#ddlBranchCode").change(function () {
                ShowText("BankBranch");
            });

            //顯示店家單據開立方式-其他
            ShowAccountingMessage();
            $('.record').change(function () {
                ShowAccountingMessage();
            });

            /*
            * 營業時間全選
            */
            $("#chk_AllBusinessWeekly").click(function () {
                if ($(this).attr("checked")) {
                    $("input[name*=chk_BusinessWeekly]").attr("checked", true);
                } else {
                    $("input[name*=chk_BusinessWeekly]").attr("checked", false);
                }
            });

            $("input[name*=chk_BusinessWeekly]").click(function () {
                var flag = true;
                $.each($("input[name*=chk_BusinessWeekly]"), function () {
                    if (!$(this).attr("checked")) {
                        flag = false;
                    }
                });

                if (!flag) {
                    $("#chk_AllBusinessWeekly").attr("checked", false);
                } else {
                    $("#chk_AllBusinessWeekly").attr("checked", true);
                }
            });



            /*
            * 公休時間全選
            */
            $("#chk_close_AllBusinessWeekly").click(function () {
                if ($(this).attr("checked")) {
                    $("input[name*=chk_close_BusinessWeekly]").attr("checked", true);
                } else {
                    $("input[name*=chk_close_BusinessWeekly]").attr("checked", false);
                }
            });

            $("input[name*=chk_close_BusinessWeekly]").click(function () {
                var flag = true;
                $.each($("input[name*=chk_close_BusinessWeekly]"), function () {
                    if (!$(this).attr("checked")) {
                        flag = false;
                    }
                });
                if (!flag) {
                    $("#chk_close_AllBusinessWeekly").attr("checked", false);
                } else {
                    $("#chk_close_AllBusinessWeekly").attr("checked", true);
                }
            });

            $("#btnVbsProposalItemPrice").click(function () {
                var selectedVal = [];
                var radDealType = $("input[name*='radDealType']:checked");
                $.each(radDealType, function (i, o) {
                    selectedVal.push($(o).val());
                });
                var data = {
                    sellerGuid: '<%=SellerGuid%>',
                    content: selectedVal,
                    userName: '<%=UserName%>',
                };
                if (selectedVal.length == 0) {
                    alert("尚未選擇任何項目");
                    return false;
                }
                $.ajax(
                    {
                        async: false,
                        type: "POST",
                        url: "SellerContent.aspx/SaveProposalItemPriceContent",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            window.parent.$.fancybox.close();
                        }, error: function (res) {
                            console.log(res);
                        }
                    });
            });


            BindOpenCloseDate();

            /*
            * 公休日
            */
            HideOrShowCloseSpan();
            $("#ddlCloseDate").change(function () {
                HideOrShowCloseSpan();
            });

            //公休日(特殊日期)
            setupdatepicker('txtCloseBeginDate', 'txtCloseEndDate');


            //預約系統設定


            $('#<%=chkIsOpenBooking.ClientID%>').change(function () {
                var rdlBooking = $('#<%=chkIsOpenBooking.ClientID%>');
                var rdlRservationSetting = $('#<%=rdlIsOpenBooking.ClientID%> input:radio:checked');

                if (rdlBooking.is(':checked')) {
                    $('#<%=rdlIsOpenBooking.ClientID%>').css("display", "");
                    $('#<%=rdlIsOpenBooking.ClientID%>_1').attr("checked", true);
                } else {
                    $('#<%=rdlIsOpenBooking.ClientID%>').css("display", "none");
                }

            });

            if ($('#<%=chkIsOpenBooking.ClientID%>').is(':checked')) {
                $('#<%=rdlIsOpenBooking.ClientID%>').css("display", "");
            } else {
                $('#<%=rdlIsOpenBooking.ClientID%>').css("display", "none");
            }


            //營業據點使否鎖住
            $("#chkNoLocation").change(function () {
                IsLocation($(this));
            });
            IsLocation($('#chkNoLocation'));


            $("#syncBusinessHour").fancybox();
            $("#syncCloseDate").fancybox();


            //店家圖片
            $('.dI').button();
            $('.dI').click(function () {
                var cont = confirm('Are you sure you want to delete it?');
                if (cont) {
                    $(this).parent().remove();
                }
            });


            $('#images').sortable();
            $('#images').disableSelection();

            $('#btnUploadPic').click(function () {
                var sellerid = $("#pSellerId").html().replace(/\r?\n/g, "").replace(/ /g, "");
                var push = [];
                $('.PicName').each(function () {
                    var a = sellerid + "," + $(this).html().replace(/\r?\n/g, "").replace(/ /g, "");
                    push.push(a);
                });
                $('#hi').val(push.join('|'));
            });


            $('.b-area').change(function () {
                $('#btnSetData').hide();
            })


            //fix chrome disabled cannot copy
            $('#MainSet :text:disabled').removeAttr("disabled").prop("readonly", true);
            if ($('#chkNoLocation').is(':disabled') && $('#chkNoLocation').is(':checked')) {
                $('#divLocation :text:disabled').removeAttr("disabled").prop("readonly", true);
            }


            //新增業務
            $('#btnAdd').click(function () {
                var salesCount = $('[id*=tdDevelopeSales]').length + 1;

                var html = '';
                html += '<tr>';
                html += '<td id="tdDevelopeSales"' + salesCount + '"><input id="txtMultipleDevelopeSales' + salesCount + '" type="text"  CssClass="input-small" Style="width: 200px"/></td>';
                html += '<td id="tdOperationSales"' + salesCount + '"><input id="txtMultipleOperationSales' + salesCount + '" type="text" CssClass="input-small" Style="width: 200px"/></td>';
                html += '</tr>';

                salesCount++;
                $('#multipleSales').append(html);

                SalesAutoComplete();

            })

            //取得業務
            GetMultipleSales();


            //商家轉件管理fancybox
            $("#btnSellerSalesReferral").fancybox({
                closeBtn: true,
                href: "/Sal/SellerSalesReferral",
                type: "ajax",
                title: "商家轉件管理",
                maxWidth: '45%',
                ajax: {
                    data: {
                        sellerGuid: '<%=SellerGuid%>',
                    }
                },
                helpers: {
                    title: {
                        type: 'inside',
                        position: 'top'
                    },
                    overlay: {
                        closeClick: false // prevents closing when clicking OUTSIDE fancybox
                    }
                }

            });

            //商家提案售價顯示管理fancybox
            $("#btnVbsItemPriceVisible").click(function () {
                $.fancybox({
                    'padding': 0,
                    'width': 800,
                    'height': 600,
                    'type': 'iframe',
                    content: $('#divVbsProposalItemPriceContent').show()
                });
            });


            //宅配商家才顯示商品管理頁簽
            var is_house = false;
            var seller_property = $("input[name*='chkSellerPorperty']:checked").map(function () {
                if ($(this).next("label").html() == "團購宅配") {
                    $('#cbHourse').attr('checked', 'checked');
                    return "<%=(int)ProposalSellerPorperty.HomeDelivery%>";
                }
            });

            if ($.inArray("<%=(int)ProposalSellerPorperty.HomeDelivery%>", seller_property) < 0)
                $("#a1").hide();

            if ('<%=config.IsRemittanceFortnightly%>' == "True" && '<%=IsAgreePponNewContractSeller%>' == 'True')
            {
                ShowRemittanceType($('[id*=chkSellerPorperty][value=1]'));
                $('[id*=chkSellerPorperty][value=1]').click(function () {
                    ShowRemittanceType($(this))
                })
            }

            if ('<%=config.IsRemittanceFortnightly%>' == "True" && '<%=IsAgreeHouseNewContractSeller%>' == 'True' && '<%=IsAgreePponNewContractSeller%>' == 'False')
            {
                $('#divRemittanceType').hide();
            }
            
            //isp
            $('#hdReturnCycleFamily').val($('#returnCycleFamily').val());
            $('#hdReturnTypeFamily').val($('#returnTypeFamily').val());
            $('#hdReturnOtherFamily').val($('#returnOtherFamily').val());

            $('#hdReturnCycleSeven').val($('#returnCycleSeven').val());
            $('#hdReturnTypeSeven').val($('#returnTypeSeven').val());
            $('#hdReturnOtherSeven').val($('#returnOtherSeven').val());

            
            $('#hdReturnAddress').val($('#returnAddress').val());
            $('#hdContactName').val($('#contactName').val());
            $('#hdContactTel').val($('#contactTel1').val() + '-' + $('#contactTel2').val() + ($('#contactTel3').val() != '' ? '#' + $('#contactTel3').val() : ""));
            $('#hdContactEmail').val($('#contactEmail').val());
            $('#hdContactMobile').val($('#contactMobile').val());

            $('#returnCycleFamily,#returnTypeFamily,#returnOtherFamily').change(function () {
                $('#hdReturnCycleFamily').val($('#returnCycleFamily').val());
                $('#hdReturnTypeFamily').val($('#returnTypeFamily').val());
                $('#hdReturnOtherFamily').val($('#returnOtherFamily').val());
            })

            $('#returnCycleSeven,#returnTypeSeven,#returnOtherSeven').change(function () {
                $('#hdReturnCycleSeven').val($('#returnCycleSeven').val());
                $('#hdReturnTypeSeven').val($('#returnTypeSeven').val());
                $('#hdReturnOtherSeven').val($('#returnOtherSeven').val());
            })

            $('#returnAddress,#contactName,#contactTel1,#contactTel2,#contactTel3,#contactEmail,#contactMobile').change(function () {
                $('#hdReturnAddress').val($('#returnAddress').val());
                $('#hdContactName').val($('#contactName').val());
                $('#hdContactTel').val($('#contactTel1').val() + '-' + $('#contactTel2').val() + ($('#contactTel3').val() != '' ? '#' + $('#contactTel3').val() : ""));
                $('#hdContactEmail').val($('#contactEmail').val());
                $('#hdContactMobile').val($('#contactMobile').val());
            })



            if (window.location.hash) {
                var obj = location.hash;
                SelectTab(obj);
            }

        });

        var closeDownDateChange = function (s, e) {
            var isCloseDown = false;          

            if ($("input[id*=rdlcloseDownStatus]:checked").val() == '1') {
                $('#formCloseDownDate').attr('disabled', false);
                isCloseDown = true;
            }
            if ($("input[id*=rdlcloseDownStatus]:checked").val() == '0') {
                isCloseDown = false;
                $('#formCloseDownDate').attr('disabled', 'disabled');
            }
            if (!isCloseDown) {
                $('#formCloseDownDate').val('');
            }
        };

        var validateCloseDownInfo = function () {
            var result = true;
            if ($("input[id*=rdlcloseDownStatus]:checked").val() == '1') {
                result = $('#formCloseDownDate').val().length > 0;    //close down must have date

                
                if (!result) {
                    alert('請設定結束營業日期!');
                    return false;
                }
                else
                {
                    if (!isValidDate($('#formCloseDownDate').val())) {
                        alert("輸入日期格式不正確");
                        return false;
                    }
                }
                if (!window.confirm("您已調整營業狀態，請問是否儲存？")) {
                    return false;
                }
            }
            return result;

        };

        var validateBusinessClose = function () {

            var result = true;
            $('.if-error').hide();
            $('.error').removeClass('error');

            //營業時間
            var _settingTimes = "";
            $.each($("#panBusinessHour div"), function (idx, value) {
                _settingTimes += value.firstChild.data + "；";
            });
            if (_settingTimes.length > 0) {
                _settingTimes = _settingTimes.substring(0, _settingTimes.length - 1);
            }
            $("#tbBusinessHour").val(_settingTimes);
            /*
            CKEDITOR.instances.tbBusinessHour.setData(_settingTimes, function()
            {
                this.checkDirty();
            });
            */
            //公休日
            var _settingCloseTimes = "";
            $.each($("#panCloseDate div"), function (idx, value) {
                _settingCloseTimes += value.firstChild.data + "；";
            });
            if (_settingCloseTimes.length > 0) {
                _settingCloseTimes = _settingCloseTimes.substring(0, _settingCloseTimes.length - 1);
            }
            $("#<%=tbCloseDate.ClientID%>").val(_settingCloseTimes);



            //營業據點必填
            if (!$('#chkNoLocation').is(':checked')) {

                if ($('#txtStoreTel').val() == "") {
                    $('#pStoreTel').show();
                    $('#pStoreTel').parent().addClass('error');
                    $('#txtStoreTel').focus();
                    result = false;
                }

                if ($('#ddlLocationCity').val() == -1 || $('#txtLocationAddress').val() == "") {
                    $('#pLocationAddress').show();
                    $('#pLocationAddress').parent().addClass('error');
                    $('#ddlLocationCity').focus();
                    result = false;
                }
            }

            return result;
        }

        function checkApplyProposal(d,btn)
        {
            if (d.indexOf('「宅配」') > 0 && $('#txtParentSellerName').val() != '')
                d = "此賣家不為最上層賣家，是否確定要申請「宅配」提案?";


            var btn = $(btn).attr('id');

            if ('<%=config.IsRemittanceFortnightly%>' == 'True' && d.indexOf('「宅配」') > 0 && '<%=IsAgreeHouseNewContractSeller%>' == 'False')
            {
                alert('商家尚未同意新的條款內容，無法建立提案。');
                return false;
            }
            else if (!confirm(d))
            {
                return false;
            }
            else
            {
                LoadNextStep(d);
            }
        }


        function checkDeliveryProposal(d,btn)
        {
            if (d.indexOf('「宅配」') > 0 && $('#txtParentSellerName').val() != '')
                d = "此賣家不為最上層賣家，是否確定要申請「宅配」提案?";


            var btn = $(btn).attr('id');

            if ('<%=config.IsRemittanceFortnightly%>' == 'True' && d.indexOf('「宅配」') > 0 && '<%=IsAgreeHouseNewContractSeller%>' == 'False')
            {
                alert('商家尚未同意新的條款內容，無法建立提案。');
                return false;
            }
            else if (!show_message(d))
            {
                return false;
            }
        }


        function LoadNextStep(d) {
            var isMultiple = false;
            $.ajax(
                {
                    async: false,
                    type: "POST",
                    url: "SellerContent.aspx/CheckMultipleSales",
                    data: JSON.stringify({
                        sellerGuid: '<%=SellerGuid%>',
                        userName: '<%=UserName%>',
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        
                        
                        if (data.d)
                        {
                            isMultiple = true;

                            var IsTravelProposal = false;
                            var IsPBeautyProposal = false;
                            var ProposalDeliveryType = '<%=(int)DeliveryType.ToShop%>';


                            if (btn.indexOf('Travel') > 0)
                                IsTravelProposal = true;
                            if (btn.indexOf('PBeauty') > 0)
                                IsPBeautyProposal = true;
                            if (btn.indexOf('Delivery') > 0)
                                ProposalDeliveryType = '<%=(int)DeliveryType.ToHouse%>'


                            $('#hidSelectSalesIsTravelProposal').val(IsTravelProposal);
                            $('#hidSelectSalesIsPBeautyProposal').val(IsPBeautyProposal);
                            $('#hidSelectSalesProposalDeliveryType').val(ProposalDeliveryType);
                        }
                        else
                        {
                            //$(this).trigger('click');
                            //return true;
                        }

                        
                    }, error: function (res) {
                        console.log(res);
                    }
                });

                if (isMultiple)
                {
                    
                    
                    $.ajax(
                    {
                        async: false,
                        type: "POST",
                        url: "SellerContent.aspx/GetMultipleSales",
                        data: JSON.stringify({
                            sellerGuid: '<%=SellerGuid%>',
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            var result = data.d;
                            var _html = '';
                            _html += "<tr>";
                            _html += "<td></td>";
                            _html += "<td>";
                            _html += "負責業務1";
                            _html += "</td>";
                            _html += "<td>";
                            _html += "負責業務2";
                            _html += "</td>";
                            _html += "</tr>";

                            $.each(result, function (idx, obj) {
                                _html += "<tr>";
                                _html += "<td><input id='rdSalesGroup" + obj.SalesGroup + "' type = 'radio' name='selectMultiplerSellerSales' /></td> ";
                                _html += "<td id=tdSelectDevelopeSales" + obj.SalesGroup + ">";
                                _html += obj.DevelopeSalesEmail;
                                _html += "</td>";
                                _html += "<td id=tdSelectOperationSales" + obj.SalesGroup + ">";
                                _html += obj.OperationSalesEmail;
                                _html += "</td>";
                                _html += "</tr>";

                            });


                            $('#selectmultipleSales').html(_html);
                        
                        }, error: function (res) {
                            console.log(res);
                        }
                    });
                    

                    //取得哪組業務
                    $('#btnSelectSalesSend').click(function ()
                    {
                        var id = $('input:radio:checked[name="selectMultiplerSellerSales"]').attr('id').replace('rdSalesGroup', '');
                        $('#hidSelectSalesDevelope').val($('#tdSelectDevelopeSales' + id).text())
                        $('#hidSelectSalesOperation').val($('#tdSelectOperationSales' + id).text())
                    })

                    $('#btnSelectSalesCancel').click(function () {
                        $('#hidSelectSalesDevelope').val('<%=UserName%>');
                        $('#hidSelectSalesOperation').val('')
                    })


                   //選擇業務fancybox
                    $.fancybox([
                        {
                            closeBtn: true,    
                            parent: "form:first",
                            href: '#divCheckSales',
                            title: "提案人員組別確認",
                            maxWidth: '45%',
                            helpers: {
                                title: {
                                    type: 'inside',
                                    position: 'top'
                                },
                                overlay: {
                                    closeClick: false // prevents closing when clicking OUTSIDE fancybox
                                }
                            }
                        }
                    ]);
                    return false;
                }

                
                if (d.indexOf('「宅配」') > 0) {
                    //popup
                    $('div#bpopup-warehouseType').bPopup({
                            modalClose: true,
                        });
                    return false;
                }
        }

        function ContractUpload(){
            var sellerId = $("#pSellerId").html().replace(/\r?\n/g, "").replace(/ /g,"");
            var contractFileType = $('[id*=ddlContactType]').val();
            var contractFileMemo = $("#txtContractFileMemo").val();
            var contractDueDate = $("#txtContractDueDate").val();
            if (contractDueDate != "") {
                if (!isValidDate(contractDueDate)) {
                    alert("輸入日期格式不正確");
                    return false;
                }
            }
            var contractPartyBContact = $("#txtContractPartyBContact").val();
            var contractVersion = $("#txtContractVersion").val();
            var files = $('#fileContract')[0].files;
            $('.if-error').hide();
            $('.error').removeClass('error');
            if(contractFileType == undefined){
                $('#pContractFile').show();
                $('[id*=ddlContactType]').parent().parent().addClass('error');
                return false;
            }
            if(sellerId == "" || contractFileType.length == 0){
                $('#pContractFile').show();
                $('[id*=ddlContactType]').parent().parent().addClass('error');
                return false;
            }
            if ($.trim(contractPartyBContact) == "") {
                $('#pContractPartyBContact').show();
                $("#txtContractPartyBContact").parent().parent().parent().addClass('error');
                return false;
            }
            if ($.trim(contractVersion) == "") {
                $('#pContractVersion').show();
                $("#txtContractVersion").parent().parent().parent().addClass('error');
                return false;
            }
            if(files.length ==0){
                alert("請選擇檔案");
                return false;
            }
            
            var formdata = new FormData();
            $.each(files, function (i, file) {
                formdata.append(file.name, file);
            });

            formdata.append("UploadType", "Accessory");
            formdata.append("SellerId",sellerId);
            formdata.append("ContractFileType",contractFileType);
            formdata.append("SellerGuid",'<%= SellerGuid %>');
            formdata.append("ContractFileMemo", contractFileMemo);
            formdata.append("ContractDueDate", contractDueDate);
            formdata.append("ContractPartyBContact", contractPartyBContact);
            formdata.append("ContractVersion", contractVersion);
            
            $.ajax({
                type: "POST",
                url: "ContractUpload.ashx",
                data: formdata,
                processData: false,
                contentType: false,
                success: function (data) {
                    var c = JSON.parse(data);
                    if(c != undefined){
                        if(c.IsSuccess){
                            alert("上傳成功");
                        }else{
                            alert(c.Message);
                        }
                        //location.href = location.href + "&tab=divContract";
                        location.href = location.protocol + "//" + location.host + location.pathname + "?sid=<%=SellerGuid%>&tab=divContract";
                    }
                },
                error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        }


        function appendSlide() {
            if ($("#sell").length > 0) {
                $("#subMenu").append('<li class="current"><a href="#sell">基本資料</a></li>');
            }
            if ($("#<%=divContact.ClientID%>").length > 0) {
                $("#subMenu").append('<li><a href="#<%=divContact.ClientID%>">聯絡人</a></li>');
            }
            if ($("#finance").length > 0) {
                $("#subMenu").append('<li><a href="#finance">財務資料</a></li>');
            }
            if ($("#storelist").length > 0) {
                $("#subMenu").append('<li><a href="#storelist">店鋪資訊</a></li>');
            }
            if ($("#tracklog").length > 0) {
                $("#subMenu").append('<li><a href="#tracklog">變更記錄</a></li>');
            }
        }

        function BindTownship(obj) {
            $.ajax({
                type: "POST",
                url: "<%= config.SSLSiteUrl %>/api/locationservice/GetTownships",
                data: "{'cid': '" + $(obj).val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var township = $('#ddlTownshipId');
                    $(township).find('option').remove();
                    $.each(response.Data, function (index, item) {
                        $(township).append($('<option></option>').attr('value', item.id).text(item.name));
                    });
                    $(township).val($('#hidTownshipId').val());
                    SetTownshipId(township);
                    ShowText("Address");
                },
                error: function (xhr) {
                    console.log(xhr.responseText);
                }
            }); 
        }

        function SetTownshipId(obj) {
            $('#hidTownshipId').val($(obj).val()); 
        }

        function BindLocationTownship(obj) {
            $.ajax({
                type: "POST",
                url: "<%= config.SSLSiteUrl %>/api/locationservice/GetTownships",
                data: "{'cid': '" + $(obj).val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var township = $('#ddlLocationTownshipId');
                    $(township).find('option').remove();
                    $.each(response.Data, function (index, item) {
                        $(township).append($('<option></option>').attr('value', item.id).text(item.name));
                    });
                    $(township).val($('#hidLocationTownshipId').val());
                    SetLocationTownshipId(township);
                    ShowText("LocationAddress");
                }
            }); 
        }

        function SetLocationTownshipId(obj) {
            $('#hidLocationTownshipId').val($(obj).val()); 
        }

        function BindBranchCode(obj) {
            $.ajax({
                type: "POST",
                url: "SellerContent.aspx/BankInfoGetBranchList",
                data: "{'id': '" + $(obj).val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var branch = $('#ddlBranchCode');
                    $(branch).find('option').remove();
                    $.each(response.d, function (index, item) {
                        $(branch).append($('<option></option>').attr('value', item.BranchNo).text(item.BranchName));
                    });
                    $(branch).val($('#hidBranchCode').val());
                    SetBranchId(branch);
                    ShowText("Bank");
                    ShowText("BankBranch");
                }
            });
        }

        function SetBranchId(obj) {
            $('#hidBranchCode').val($(obj).val());
        }

        function CheckLog() {
            if ($.trim($('#txtChangeLog').val()) == '') {
                $('#pChangeLog').show();
                return false;
            }
            $.ajax({
                type: "POST",
                url: "SellerContent.aspx/SaveLog",
                data: "{'sid': '<%=SellerGuid%>','changeLog': '" + $.trim($('#txtChangeLog').val()) + "','userName': '<%=User.Identity.Name%>'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    location.reload();
                }
            });

            return true;
        }

        function CheckManageLog() {
            if ($.trim($('#txtManageLog').val()) == '' || $("#ddlDevelopStatus").val() == -1 || $("#ddlSellerGrade").val() == -1) {
                $('#pManageLog').show();
                return false;
            }
            $.ajax({
                type: "POST",
                url: "SellerContent.aspx/SaveManageLog",
                data: "{'sid': '<%=SellerGuid%>','developStatus': '" + $("#ddlDevelopStatus").val() + "','sellerGrade': '" + $("#ddlSellerGrade").val() + "','changeLog': '" + $.trim($('#txtManageLog').val()) + "','userName': '<%=User.Identity.Name%>'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $('#txtManageLog').val("");
                    $('#ddlDevelopStatus').val("-1");
                    $('#ddlSellerGrade').val("-1");
                    //alert(location.protocol);
                    location.href = location.protocol + "//" + location.host + location.pathname + "?sid=<%=SellerGuid%>&tab=divManageNote";
                }
            });

            return true;
        }


        function syncBossName(obj, id) {
            var control = $(obj).parent().find("#" + id);
            if ($(obj).is(':checked')) {
                control.val($('#txtSellerBossName').val());
            } else {
                control.val('');
            }
        }

        function syncContactName(obj){
            $.each($("div#spanCloneContact"),function(idx,value){
                if($(this).find('[id=ddlProposalContact]').val() == "<%=(int)ProposalContact.Normal%>"){
                    var ddlProposalContact = $.trim($(this).find('[id=ddlProposalContact]').val());             //聯絡人類別
                    var txtProposalContactName = $.trim($(this).find('[id=txtProposalContactName]').val());     //姓名
                    var txtProposalContactPhone = $.trim($(this).find('[id=txtProposalContactPhone]').val());   //聯絡電話
                    var txtProposalContactMobile = $.trim($(this).find('[id=txtProposalContactMobile]').val()); //行動電話
                    var txtProposalContactFax = $.trim($(this).find('[id=txtProposalContactFax]').val());       //傳真
                    var txtProposalContactEmail = $.trim($(this).find('[id=txtProposalContactEmail]').val());   //Email
                    var txtProposalContactOther = $.trim($(this).find('[id=txtProposalContactOther]').val());   //其他

                    var parent = obj.closest("#spanCloneContact");
                    

                    if ($(obj).is(':checked')) {
                        $("#ddlProposalContact",parent).val(ddlProposalContact);
                        $("#txtProposalContactName",parent).val(txtProposalContactName);
                        $("#txtProposalContactPhone",parent).val(txtProposalContactPhone);
                        $("#txtProposalContactMobile",parent).val(txtProposalContactMobile);
                        $("#txtProposalContactFax",parent).val(txtProposalContactFax);
                        $("#txtProposalContactEmail",parent).val(txtProposalContactEmail);
                        $("#txtProposalContactOther",parent).val(txtProposalContactOther);
                    } else {
                        $("#ddlProposalContact",parent).val("");
                        $("#txtProposalContactName",parent).val("");
                        $("#txtProposalContactPhone",parent).val("");
                        $("#txtProposalContactMobile",parent).val("");
                        $("#txtProposalContactFax",parent).val("");
                        $("#txtProposalContactEmail",parent).val("");
                        $("#txtProposalContactOther",parent).val("");
                    }
                    return;
                }
            });

            
        }

        function SaveCheck() {
            $('.if-error').hide();
            $('.error').removeClass('error');

            var check = true;
            if ($.trim($('#hidTownshipId').val()) == '' || $.trim($('#txtCompanyAddress').val()) == '') {
                //$('#pAddress').show();
                //$('#pAddress').parent().parent().addClass('error');
                //check = false;
                //$('#txtCompanyAddress').focus();
            }
            if ($.trim($('#txtSellerName').val()) == '') {
                check = false;
                $('#pSellerName').show();
                $('#pSellerName').parent().parent().addClass('error');
                $('#txtSellerName').focus();
            }
            if ($('#hdSellerLevel').val() == '') {
                check = false;
                $('#pSellerLevel').show();
                $('#pSellerLevel').parent().parent().addClass('error');
                $('#ddlSellerLevel').focus();
            }
            if ($.trim($('#txtSellerBossName').val()) == '') {
                //check = false;
                //$('#pBossName').show();
                //$('#pBossName').parent().parent().addClass('error');
                //$('#txtSellerBossName').focus();
            }
            if ($.trim($('#txtSignCompanyID').val()) == '') {
                //check = false;
                //$('#pCompanyID').show();
                //$('#pCompanyID').parent().parent().addClass('error');
                //$('#txtSignCompanyID').focus();
            } else {
                if (!$("#chkNoCompanyIDCheck").is(":checked")) {
                    if ($.trim($('#txtSignCompanyID').val()).length < 8) {
                        check = false;
                        $('#pCompanyIDLengthError').show();
                        $('#pCompanyIDLengthError').parent().parent().addClass('error');
                        $('#txtSignCompanyID').focus();
                    }
                }
            }
            //店家來源
            if($("#ddlSellerFrom").val() == "-1"){
                check = false;
                $('#pSellerFrom').show();
                $('#pSellerFrom').parent().parent().addClass('error');
                $('#ddlSellerFrom').focus();
            }
            //店家屬性
            var objSellerPorperty = $("input:checkbox[name*='chkSellerPorperty']");
            var pChecked = false;
            $.each(objSellerPorperty,function(idx,value){
                if(value.checked){
                    pChecked = true;
                }
            });
            if(!pChecked){
                check = false;
                $('#pSellerPorperty').show();
                $('#pSellerPorperty').parent().parent().addClass('error');
            }
            //帳務聯絡人
            var FinanceName = $.trim($("#txtFinanceName").val());
            var FinanceTel = $.trim($("#txtFinanceTel").val());
            var FinanceEmail = $.trim($("#txtFinanceEmail").val());

            //if(FinanceName=="" || FinanceTel ==""){
            //    check = false;
            //    $('#pFinanceName').show();
            //    $('#pFinanceName').parent().parent().addClass('error');
            //}
            //if(FinanceTel=="" ){
            //    check = false;
            //    $('#pFinanceTel').show();
            //    $('#pFinanceTel').parent().parent().addClass('error');
            //}
            //if(FinanceEmail=="" ){
            //    check = false;
            //    $('#pFinanceEmail').show();
            //    $('#pFinanceEmail').parent().parent().addClass('error');
            //}


            //處理聯絡人資料
            var hdJson = [];
            $("#hdContacts").val("");
            $.each($("div#spanCloneContact"),function(idx,value){
                var ddlProposalContact = $.trim($(this).find('[id=ddlProposalContact]').val());             //聯絡人類別
                var txtProposalContactName = $.trim($(this).find('[id=txtProposalContactName]').val());     //姓名
                var txtProposalContactPhone = $.trim($(this).find('[id=txtProposalContactPhone]').val());   //聯絡電話
                var txtProposalContactMobile = $.trim($(this).find('[id=txtProposalContactMobile]').val()); //行動電話
                var txtProposalContactFax = $.trim($(this).find('[id=txtProposalContactFax]').val());       //傳真
                var txtProposalContactEmail = $.trim($(this).find('[id=txtProposalContactEmail]').val());   //Email
                var txtProposalContactOther = $.trim($(this).find('[id=txtProposalContactOther]').val());   //其他
                
                if (ddlProposalContact == "-1" && txtProposalContactName == "" && txtProposalContactPhone == "" && txtProposalContactMobile == "" &&
                    txtProposalContactFax == "" && txtProposalContactEmail == "" && txtProposalContactOther == "") {
                                     
                }else{
                    var contact = {
                        Type : $(this).find('[id=ddlProposalContact]').val(),
                        ContactPersonName : $(this).find('[id=txtProposalContactName]').val(),
                        SellerTel : $(this).find('[id=txtProposalContactPhone]').val(),
                        SellerMobile : $(this).find('[id=txtProposalContactMobile]').val(),
                        SellerFax : $(this).find('[id=txtProposalContactFax]').val(),
                        ContactPersonEmail :$(this).find('[id=txtProposalContactEmail]').val(),
                        Others : $(this).find('[id=txtProposalContactOther]').val()
                    };
                    hdJson.push(contact);
                }
            });
            if (hdJson.length > 0) {
                $("#hdContacts").val(JSON.stringify(hdJson));
            }

            //店家單據開立方式-其他
            if ($('#<%=rbRecordOthers.ClientID%>').is(':checked') && $('#<%=txtAccountingMessage.ClientID%>').val()=="") {
                check = false;
                $('#pRecordOthers').show();
                $('#pRecordOthers').parent().parent().addClass('error');
                $('#<%=rbRecordOthers.ClientID%>').focus();                
            }

            if ($('#NoCompany').is(':visible')) {
                check = false;
                $('#NoCompany').focus();
            }

            if ($('#NotParentCompany').is(':visible')) {
                check = false;
                $('#NotParentCompany').focus();
            }

            //儲存多業務
            var listDevelopeSales = [];
            var listOperationSales = [];
            $.each($("[id*=txtMultipleDevelopeSales]"), function (idx, value) {              

                var salesGroup = $(this).attr('id').replace('txtMultipleDevelopeSales', '')
                $.ajax({
                    url: 'SellerContent.aspx/CheckSales',
                    async: false,
                    type: 'POST',
                    data: JSON.stringify({
                        type: 1,
                        developeSales: value.value,
                        operationSales: $('#txtMultipleOperationSales' + salesGroup).val()
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    error: function (xhr) {
                        var error = xhr;
                    },
                    success: function (reqObj) {
                        if (reqObj.d!='')
                        {
                            alert(reqObj.d);
                            check = false;
                        }
                        else
                        {
                            var sellerSales = {};
                            sellerSales.SalesGroup = salesGroup;
                            sellerSales.SalesMail = $('#txtMultipleDevelopeSales' + salesGroup).val();

                            listDevelopeSales.push(sellerSales);
                            
                        }
                    }
                });

                //有資料不足的即跳開
                if (!check)
                    return false;

            });

            $.each($("[id*=txtMultipleOperationSales]"), function (idx, value) {

                if ($(this).val() != '') {
                    var salesGroup = $(this).attr('id').replace('txtMultipleOperationSales', '')

                    var sellerSales = {};
                    sellerSales.SalesGroup = salesGroup;
                    sellerSales.SalesMail = $('#txtMultipleOperationSales' + salesGroup).val();

                    listOperationSales.push(sellerSales);
                }
            });

            


            if (check) {

                var SellerName = $('#txtSellerName').val();
                var SignCompanyID = $('#txtSignCompanyID').val();
                var Value;
                var Result;
                var Guid;
                if (SignCompanyID != "") {
                    $.ajax({
                        url: 'SellerContent.aspx/SellerGetByCompany',
                        async: false,
                        type: 'POST',
                        data: '{"SellerGuid":"' + "<%= SellerGuid %>" + '","SellerName":"' + SellerName + '","SignCompanyID":"' + SignCompanyID + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        error: function (xhr) {
                        },
                        success: function (reqObj) {
                            Result = $.parseJSON(reqObj.d);
                            Value = Result.d;
                            Guid = Result.Guid;
                        }
                    });

                    if (Value == "True") {
                        if (Guid != "<%= SellerGuid %>")
                        {
                            if (confirm('錯誤！\n您輸入的資料已在系統中存在，\n請重新輸入，或直接到搜尋結果頁。') == true) {
                                $('#HiddenField1').val('True1');
                            }
                            else {
                                $('#HiddenField1').val('False1');
                            }
                        }
                        else
                            $('#HiddenField1').val('');
                    
                    }
                    else
                    {
                        $.ajax({
                            url: 'SellerContent.aspx/SellerGet',
                            async: false,
                            type: 'POST',
                            data: '{"SellerGuid":"' + "<%= SellerGuid %>" + '","SignCompanyID":"' + SignCompanyID + '"}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            error: function (xhr) {
                            },
                            success: function (reqObj) {
                                Result = $.parseJSON(reqObj.d);
                                Value = Result.d;
                                Guid = Result.Guid;
                            }
                        });

                        if (Value == "True") {
                            if (Guid != "<%= SellerGuid %>")
                            {
                                if (confirm('注意！\n您輸入的統編已在系統中存在，\n請確認是否要儲存或修改。') == true) {
                                    $('#HiddenField1').val('True2');
                                }
                                else {
                                    $('#HiddenField1').val('False2');
                                }
                            }
                            else
                                $('#HiddenField1').val('');
                        }
                    }  
                }

                
                //存業務資料
                $('#hidDevelopeSales').val(JSON.stringify(listDevelopeSales));
                $('#hidOperationSales').val(JSON.stringify(listOperationSales));
            }
            
            if ($("#hidDealStatus").val() != "") {
                if ($("#hidReferralSale").val() != "" || $("#txtReferralSale").val() != "") {
                    if ($("#hidReferralSale").val() != $("#txtReferralSale").val()) {
                        if (!confirm("注意！轉介人員的所屬檔次已開檔，且已進入計算區間，若有變更轉介人員則可能發生檔次、數值的影響，確認是否要進行變更？")) {
                            check = false;
                        } else {
                            $("#hidDealMail").val("1");
                        }
                    }
                }
            }

            return check;
        }

        function ValidateNumber(e, pnumber) {
            if (!/^\d+$/.test(pnumber)) {
                $(e).val(/^\d+/.exec($(e).val()));
            }
            return false;
        }

        function ValidateText(e, pnumber) {
            if (!/^[\d|a-zA-Z]+$/.test(pnumber)) {
                $(e).val(/^\d+/.exec($(e).val()));
            }
            return false;
        }

        function CollapseBlock(spanid,id) {
            var obj = $("#" + id);
            var objSpan = $("#" + spanid);
            if (obj.css("display") == "none") {
                obj.show();
                objSpan.text("▲");
                
            } else {
                obj.hide();
                objSpan.text("▼");
                
            }
        }

        function cloneContact(){
            var newContact = $("div#spanCloneContact").size();
            if(newContact > 2){
                alert("聯絡人至多新增三個。");
                return false;
            }
            var _html = "";
            _html += '<div id="spanCloneContact">';
            _html += '<label class="unit-label rd-unit-label">';
            _html += '	聯絡人類別</label>';
            _html += '<div class="data-input rd-data-input">';
            _html += '	<select id="ddlProposalContact" style="width:150px;">';
            <%foreach (var item in proposalContact)
            {
            %>
            _html += '		<option value="<%=item.Key%>"><%=item.Value%></option>';
            <%
            } 
            %>                                       
            _html += '	</select>';            
            _html += '</div>';
            _html += '<label class="unit-label rd-unit-label">';
            _html += '	姓名</label>';
            _html += '<div class="data-input rd-data-input">';
            _html += '	<input type="text" id="txtProposalContactName" class="input-half" />';
            _html += '	<input id="chkProposalContactType" type="checkbox" onclick="syncBossName(this, \'txtProposalContactName\');" />同負責人';
            _html += '	<input id="chkProposalContactCopy" type="checkbox" onclick="syncContactName(this);" />同一般(頁確)聯絡人';
            _html += '  <p id="pProposalContact" class="if-error" style="display: none;">請輸入聯絡人類別/姓名/電話。</p>';
            _html += '</div>';
            _html += '<label class="unit-label rd-unit-label">';
            _html += '	聯絡電話</label>';
            _html += '<div class="data-input rd-data-input">';
            _html += '	<input type="text" id="txtProposalContactPhone" class="input-half" />';
            _html += '</div>';
            _html += '<label class="unit-label rd-unit-label">';
            _html += '	行動電話</label>';
            _html += '<div class="data-input rd-data-input">';
            _html += '	<input type="text" id="txtProposalContactMobile" class="input-half" />';
            _html += '</div>';
            _html += '<label class="unit-label rd-unit-label">';
            _html += '	傳真</label>';
            _html += '<div class="data-input rd-data-input">';
            _html += '	<input type="text" id="txtProposalContactFax" class="input-half" />';
            _html += '</div>';
            _html += '<label class="unit-label rd-unit-label">';
            _html += '	Email</label>';
            _html += '<div class="data-input rd-data-input">';
            _html += '	<input type="text" id="txtProposalContactEmail" class="input-half" />';
            _html += '</div>';
            _html += '<label class="unit-label rd-unit-label">';
            _html += '	其他</label>';
            _html += '<div class="data-input rd-data-input">';
            _html += '	<textarea name="txtProposalContactOther" rows="2" cols="20" id="txtProposalContactOther" style="height:60px;width:80%;"></textarea>';
            _html += '</div>  ';
            _html += '<br />';
            _html += '<p style="float:right;">';
            //_html += '<img src="../Themes/default/images/17Life/G2/delete.png" alt="刪除" onclick="deleteContact(this);" />';
            _html += '<input type="button" id="btnDeleteContact"  class="btn rd-mcacstbtn" value="刪除聯絡人" onclick="deleteContact(this);" />';
            _html += '</p>';
            _html += '<p style="padding-top:20px"></p>';
            _html += '</div>';


            $("#spanCloneContact").after(_html);        
        }

        /*
        * 刪除聯絡人
        */
        function deleteContact(obj){
            var parent = obj.closest("#spanCloneContact");
            parent.remove();
        }
        /*
        * 刪除合約檔案
        */
        function deleteContractFile(guid){
            if(!confirm("是否刪除檔案?")){
                return false;
            }
            $.ajax({
                type: "POST",
                url: "SellerContent.aspx/DeleteContractFile",
                data: "{'guid': '" + guid + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //location.reload();
                    location.href = location.protocol + "//" + location.host + location.pathname + "?sid=<%=SellerGuid%>&tab=divContract";
                }
            });
        }

        function SelectTab(obj) {
            if ($(obj).find('.mc-navbtn').hasClass('on')) {
                return;
            } else {
                var div = $(obj).attr('tab');
                $('.mc-navbtn').each(function () {
                    $(this).removeClass('on');
                });
                $(obj).find('.mc-navbtn').addClass('on');
                $('.mc-content').hide();
                $('#' + div).show();

                if($(obj).context.innerText == "基本資料"){
                    $('#subMenu').show();
                } else {
                    $('#subMenu').hide();
                }
            }
        }

        function ShowText(type)
        {
            if (type == "Address")
            {
                $("label[id='lblAddress']").html($("#ddlCompanyCityId option:selected").text() + $("#ddlTownshipId option:selected").text() + $('#txtCompanyAddress').val());
            }
            if (type == "LocationAddress")
            {
                $("label[id='lblAddress2']").html($("#ddlLocationCity option:selected").text() + $("#ddlLocationTownshipId option:selected").text() + $('#txtLocationAddress').val());
            }
            else if (type=="Bank")
            {
                $("label[id='lblBank']").html($("#ddlBankCode option:selected").text());
            }
            else if (type=="BankBranch")
            {
                $("label[id='lblBankBranch']").html($("#ddlBranchCode option:selected").text());
            }  
        }

        function ShowAccountingMessage() {
            if ($('#<%=rbRecordOthers.ClientID%>').is(':checked'))
                $('#<%=txtAccountingMessage.ClientID %>').show();
            else
            {
                $('#<%=txtAccountingMessage.ClientID %>').hide();
                $('#<%=txtAccountingMessage.ClientID %>').val('');
            }
                
        }


        function BindOpenCloseDate() {
            //營業時間
            var _settingTime = "<%=BusinessHour%>";
            var _settingTimes = _settingTime.split("；");
            $.each(_settingTimes, function (idx, value) {
                if (value != "") {
                    appendSettingTime(value, "panBusinessHour");
                }
            });

			//公休日
            var _settingCloseTime = "<%=CloseDateInformation%>";
            var _settingCloseTimes = _settingCloseTime.split("；");
            $.each(_settingCloseTimes, function (idx, value) {
                if (value != "") {
                    appendSettingTime(value, "panCloseDate");
                }
            });
        }

        /*
        * 同步營業時間到其他店鋪
        */
        function syncBusinessHour() {
            var storeGuid = [];
            $.each($("#divBusinessHour input:checked").not("#chkAllSeller"), function () {
                var obj = $(this).val();
                storeGuid.push(obj);
            });

            var _settingTimes = "";
            $.each($("#panBusinessHour div"), function (idx, value) {
                _settingTimes += value.innerText + "；";
            });
            if (_settingTimes.length > 0) {
                _settingTimes = _settingTimes.substring(0, _settingTimes.length - 1);
            }
            if (storeGuid.length > 0) {
                $.ajax({
                    type: "POST",
                    url: "SellerContent.aspx/SyncBusinessHour",
                    data: "{'sellerGuid': '" + JSON.stringify(storeGuid) + "','opentime' : '" + _settingTimes + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        alert("資料已同步");
                        window.parent.$.fancybox.close();
                    }
                });
            }
        }

        /*
        * 同步公休時間到其他店鋪
        */
        function syncCloseDate() {
            var storeGuid = [];
            $.each($("#divCloseDate input:checked").not("#chkAllSeller"), function () {
                var obj = $(this).val();
                storeGuid.push(obj);
            });

            var _settingTimes = "";
            $.each($("#panCloseDate div"), function (idx, value) {
                _settingTimes += value.innerText + "；";
            });
            if (_settingTimes.length > 0) {
                _settingTimes = _settingTimes.substring(0, _settingTimes.length - 1);
            }
            if (storeGuid.length > 0) {
                $.ajax({
                    type: "POST",
                    url: "SellerContent.aspx/SyncCloseDate",
                    data: "{'storeGuid': '" + JSON.stringify(storeGuid) + "','closedate' : '" + _settingTimes + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        alert("資料已同步");
                        window.parent.$.fancybox.close();
                    }
                });
            }
        }


        function addBusinessHour(){
            var chk_BusinessWeekly = $("input[name*=chk_BusinessWeekly]:checked");
            var ddl_WeeklyBeginHour = $("#ddl_WeeklyBeginHour").val();
            var ddl_WeeklyBeginMinute = $("#ddl_WeeklyBeginMinute").val();
            var ddl_WeeklyEndHour = $("#ddl_WeeklyEndHour").val();
            var ddl_WeeklyEndMinute = $("#ddl_WeeklyEndMinute").val();

            var _html = ddl_WeeklyBeginHour + "~" + ddl_WeeklyEndHour;

            var weeklys = chk_BusinessWeekly.map(function() {
                return this.value;
            }).get().join();


            if (weeklys == "" && !$('#chk_24hr').is(':checked') && !$('#chk_WebInfo').is(':checked')) {
                alert("請先選擇一項");
                return false;
            }
            else
            {
                if (weeklys != "")
                    getWeeklyContinue("", weeklys, ddl_WeeklyBeginHour + ":" + ddl_WeeklyBeginMinute, ddl_WeeklyEndHour + ":" + ddl_WeeklyEndMinute, false, "panBusinessHour");
                if ($('#chk_24hr').is(':checked'))
                    appendSettingTime('24小時無休', "panBusinessHour");
                if ($('#chk_WebInfo').is(':checked'))
                    appendSettingTime('依網站公告', "panBusinessHour");

            }

            $('#chk_AllBusinessWeekly').attr("checked", false);
            $('#chk_24hr').attr("checked", false);
            $('#chk_WebInfo').attr("checked", false);
            $("input[name*=chk_BusinessWeekly]").attr("checked", false);
            $("#ddl_WeeklyBeginHour").val("00");
            $("#ddl_WeeklyBeginMinute").val("00");
            $("#ddl_WeeklyEndHour").val("00");
            $("#ddl_WeeklyEndMinute").val("00");
   
        }

        function addCloseHour(){
            var ddlCloseDate = $("#ddlCloseDate");  //頻率
            var ddlCloseDateText = $('#ddlCloseDate :selected').text();
            var chk_close_BusinessWeekly = $("input[name*=chk_close_BusinessWeekly]:checked");  //每周
            var txtCloseMonthlyDay = $("#txtCloseMonthlyDay");  //每月
            var txtCloseBeginDate = $("#txtCloseBeginDate");  //特殊
            var txtCloseEndDate = $("#txtCloseEndDate");  //特殊
            var chk_close_BusinessWeekly = $("input[name*=chk_close_BusinessWeekly]:checked");
            var ddl_close_WeeklyBeginHour = $("#ddl_close_WeeklyBeginHour").val();
            var ddl_close_WeeklyBeginMinute = $("#ddl_close_WeeklyBeginMinute").val();
            var ddl_close_WeeklyEndHour = $("#ddl_close_WeeklyEndHour").val();
            var ddl_close_WeeklyEndMinute = $("#ddl_close_WeeklyEndMinute").val();
            var chk_close_allday = $("#chk_close_allday").is(':checked');

            var _frequency = GetCloseFrequency();
            switch(_frequency){
                case frequency.weekly:
                    //週
                    var weeklys = chk_close_BusinessWeekly.map(function() {
                        return this.value;
                    }).get().join();

                    getWeeklyContinue(ddlCloseDateText, weeklys,ddl_close_WeeklyBeginHour + ":" + ddl_close_WeeklyBeginMinute, ddl_close_WeeklyEndHour + ":" + ddl_close_WeeklyEndMinute, chk_close_allday, "panCloseDate");
                    break;
                case frequency.monthly:
                    //月
                    var beginTime = ddl_close_WeeklyBeginHour + ":" + ddl_close_WeeklyBeginMinute;
                    var endTime = ddl_close_WeeklyEndHour + ":" + ddl_close_WeeklyEndMinute;
                    $("#panCloseDate").append('<div class="remove">' + ddlCloseDateText + " "  + txtCloseMonthlyDay.val() + '日' + (chk_close_allday ? "全天" : beginTime + "~" + endTime )  + '<a onclick="removeSettingTime(this);"></a></div>');
                    break;
                case frequency.special:
                    //特殊
                    var beginDate = txtCloseBeginDate.val();
                    var endDate = txtCloseEndDate.val();
                    var beginTime = ddl_close_WeeklyBeginHour + ":" + ddl_close_WeeklyBeginMinute;
                    var endTime = ddl_close_WeeklyEndHour + ":" + ddl_close_WeeklyEndMinute;
                    if ($.trim(beginDate) == ""){
                        alert("請輸入至少一個日期");
                        return false;
                    }
                    if($.trim(beginDate) != ""){
                        if (!isValidDate($.trim(beginDate))) {
                            alert("起始日期格式不正確");
                            return false;
                        }
                    }
                    if($.trim(endDate) != ""){
                        if (!isValidDate($.trim(endDate))) {
                            alert("結束日期格式不正確");
                            return false;
                        }
                    }
                    if(!CompareDate(beginDate,endDate)){
                        alert("結束日期需晚於起始日期");
                        return false;
                    }
                    var paddingDatetime = "";
                    if($.trim(endDate) != ""){
                        paddingDatetime = beginDate + "~" + endDate;
                    }else{
                        paddingDatetime = beginDate;
                    }
                    $("#panCloseDate").append('<div class="remove">' + paddingDatetime + " " +  (chk_close_allday ? "全天" : beginTime + "~" + endTime )  + '<a onclick="removeSettingTime(this);"></a></div>');
                    break;
                case frequency.alldayround:
                    //全年無休
                    $("#panCloseDate").append('<div class="remove"><%=LunchKingSite.Core.Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (LunchKingSite.Core.StoreCloseDate)LunchKingSite.Core.StoreCloseDate.AllYearRound)%><a onclick="removeSettingTime(this);"></a></div>');
                    break;
            }

            $("#chk_close_AllBusinessWeekly").attr("checked", false);
            $("input[name*=chk_close_BusinessWeekly]").attr('checked', false);
            $("#ddlCloseDate").val("1");
            $("#txtCloseMonthlyDay").val("");
            $("#txtCloseBeginDate").val("");
            $("#txtCloseEndDate").val("");
            $("#ddl_close_WeeklyBeginHour").val("00");
            $("#ddl_close_WeeklyBeginMinute").val("00");
            $("#ddl_close_WeeklyEndHour").val("00");
            $("#ddl_close_WeeklyEndMinute").val("00");
            $("#chk_close_allday").removeAttr('checked');
            

            HideOrShowCloseSpan();
        }

        /*
        *營業時間
        */
        function getWeeklyContinue(ofrenquency, weeklys, beginTime, endTime, allDay, obj) {
            if (weeklys == "") {
                alert("請先選擇週幾");
                return false;
            }
            $.ajax({
                type: "POST",
                url: "SellerContent.aspx/GetWeeklyName",
                data: "{'frenquency':'" + ofrenquency + "','weeklys':'" + weeklys + "','beginTime':'" + beginTime + "','endTime':'" + endTime + "','allDay': '" + allDay + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var c = response.d;
                    if (c != undefined) {
                        appendSettingTime(c, obj);
                    }
                }, error: function (error) {
                    console.log(error);
                }
            });
        }

        function appendSettingTime(c, obj) {
            $("#" + obj + "").append('<div class="remove">' + c + '<a onclick="removeSettingTime(this);"></a></div>');
        }
        /*
        * 移除營業時間div
        */
        function removeSettingTime(obj) {
            obj.closest("div").remove();
        }

        /*
        * 公休日欄位顯示或隱藏
        */
        function HideOrShowCloseSpan() {
            var _weekly = false;
            var _monthly = false;
            var _special = false;
            var _time = false;

            var _frequency = GetCloseFrequency();
            switch (_frequency) {
                case frequency.weekly:
                    _weekly = true;
                    _monthly = false;
                    _special = false;
                    _time = true;
                    break;
                case frequency.monthly:
                    _weekly = false;
                    _monthly = true;
                    _special = false;
                    _time = true;
                    break;
                case frequency.special:
                    _weekly = false;
                    _monthly = false;
                    _special = true;
                    _time = true;
                    break;
                case frequency.alldayround:
                    _weekly = false;
                    _monthly = false;
                    _special = false;
                    _time = false;
                    break;
            }

            if (_weekly) {
                $("#panCloseWeekly").show();
            } else {
                $("#panCloseWeekly").hide();
            }
            if (_monthly) {
                $("#panCloseMonthly").show();
            } else {
                $("#panCloseMonthly").hide();
            }
            if (_special) {
                $("#panCloseSpecial").show();
            } else {
                $("#panCloseSpecial").hide();
            }
            if (_time) {
                $("#panCloseTime").show();
            } else {
                $("#panCloseTime").hide();
            }
        }

        function GetCloseFrequency() {
            var _frequency = "";
            switch ($("#ddlCloseDate").val()) {
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.EveryWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.SingleWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.BiWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.FirstWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.SecondWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.ThirdWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.ForthWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.FifthWeekly%>":
                    _frequency = frequency.weekly;
                    break;
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.EveryMonthly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.SingleMonthly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.BiMonthly%>":
                    _frequency = frequency.monthly;
                    break;
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.SpecialDate%>":
                    _frequency = frequency.special;
                    break;
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.AllYearRound%>":
                    _frequency = frequency.alldayround;
                    break;
            }
            return _frequency;
        }

        function isValidDate(dateStr) {
            if (dateStr == null || dateStr == '') return false;
            var parts = dateStr.split('/');
            if (parts.length != 3) return false;
            if (parts[0] > 9999 || parts[0] < 1753)
                return false;
            var accDate = new Date(dateStr);
            if (parseFloat(parts[0]) == accDate.getFullYear()
                && parseFloat(parts[1]) == accDate.getMonth() + 1
                && parseFloat(parts[2]) == accDate.getDate()) {
                return true;
            }
            else {
                return false;
            }
        }


        function setupdatepicker(from, to) {
            var dates = $('#' + from + ', #' + to).datepicker({
                changeMonth: true,
                numberOfMonths: 3,
                onSelect: function (selectedDate) {
                    var option = this.id == from ? "minDate" : "maxDate";
                    var instance = $(this).data("datepicker");
                    var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                },
                onClose: function (selectedDate) {

                }
            });
        }

        function CompareDate(sd, ed) {
            var d1 = new Date(sd);
            var d2 = new Date(ed);
            if (d2 <= d1) {
                return false;
            }
            return true;
        }

        function checkAll(obj, div) {
            if ($(obj).is(":checked")) {
                $("#" + div + " > input[type=checkbox]").attr("checked", "checked");
            } else {
                $("#" + div + " > input[type=checkbox]").removeAttr("checked");
            }
        }

        function IsLocation(obj,type)
        {
            if ($(obj).is(':checked') || $(obj).is(':disabled')) {

                //鎖
                if ($(obj).is(':disabled'))
                {
                    //審核通過
                    $('#divLocation :text').prop("readonly", true);
                    $('#divLocation *').not($('#divLocation :text')).prop('disabled', true);
                } 
                else
                {
                    //申請建立
                    //排除勾選,儲存鈕及提供服務區塊
                    $('#divLocation :text').not('#chkNoLocation').not('#btnSaveLocation').not(".NoLockLocation *").prop("readonly", true);
                    $('#divLocation *').not($('#divLocation :text')).not('#chkNoLocation').not('#btnSaveLocation').not(".NoLockLocation *").prop('disabled', true);
                }
                $('#syncCloseDate,#syncBusinessHour').hide();
                $('.remove a').hide();
            }

            else {
                //不鎖
                $('#syncCloseDate,#syncBusinessHour').show();
                $('.remove a').show();
                $('#divLocation :text').prop("readonly", false);
                $('#divLocation *').prop('disabled', false);
            }
        }

        function openSellerRefferContent() {
            $.fancybox({
                'padding': 0,
                'width': 800,
                'height': 600,
                'type': 'iframe',
                content: $('#divSellerRefferContent').show()
            });
        }


        function CheckSellerApprove()
        {
            var check = true;

            $.each($("div#spanCloneContact"), function (idx, value) {
                var ddlProposalContact = $.trim($(this).find('[id=ddlProposalContact]').val());             //聯絡人類別
                var txtProposalContactName = $.trim($(this).find('[id=txtProposalContactName]').val());     //姓名
                var txtProposalContactPhone = $.trim($(this).find('[id=txtProposalContactPhone]').val());   //聯絡電話
                var txtProposalContactMobile = $.trim($(this).find('[id=txtProposalContactMobile]').val()); //行動電話
                var txtProposalContactFax = $.trim($(this).find('[id=txtProposalContactFax]').val());       //傳真
                var txtProposalContactEmail = $.trim($(this).find('[id=txtProposalContactEmail]').val());   //Email
                var txtProposalContactOther = $.trim($(this).find('[id=txtProposalContactOther]').val());   //其他

                if (ddlProposalContact == "-1" || txtProposalContactName == "" || txtProposalContactPhone == "" || txtProposalContactEmail == "") {
                    
                    check =  false;
                }
            });

            if (check == false)
                alert("聯絡人資料「類別/姓名/聯絡電話/Email」未填寫完整，請完成填寫，並確實完成【申請核准】");

            return check;
        }

        function DownLoadContract(guid) {
            $.ajax(
            {
                type: "POST",
                url: "SellerContent.aspx/GenTicketId",
                data: "{ 'UserName': '<%=UserName%>'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    location.href = "ContractDownload.ashx?guid=" + guid + "&UploadType=Accessory&ticketId=" + data.d;
                }, error: function (res) {
                    console.log(res);
                }
            });
            
        }

        function SetSellerLevelDetail(d) {
            if (d != '') {
                var detail = $.parseJSON(d);
                $('#ddlSellerLevel option[value="' + detail.NoCaterLv + '"]').attr("selected", "selected");
                $('#txtSellers').val(detail.Sellers);
                $('#txtAvgPrice').val(detail.AvgPrice);
                $('#txtAvgSeat').val(detail.AvgSeat);
                $('#txtTurnoverRate').val(detail.TurnoverRate);
                $('#txtTurnoverFreq').val(detail.TurnoverFreq);
                $('#txtCrowdsFlow').val(detail.CrowdsFlow);
                SetTurnover();
            } 
            SetSellerLevel();
        }

        function SetCrowdsFlow() {
            var cf = $('#txtAvgSeat').val() * $('#txtTurnoverRate').val() * 0.01 * $('#txtTurnoverFreq').val();
            if (!isNaN(cf) && cf != 0) {
                $('#txtCrowdsFlow').val(cf);
                return true;
            } else {
                alert('含有非數字或0，請檢查');
            }
            return false;
        }

        function SetTurnover() {
            var t = $('#txtSellers').val() * $('#txtAvgPrice').val() * $('#txtCrowdsFlow').val();
            if (!isNaN(t) && t !=0) {
                $('#lbTurnover').text('$' + parseFloat(t, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
                t = t / 10000;
                var isSingle = $('#liParentSellerId').text() == '';
                if (!isSingle) {
                    if (t >= 10000) {
                        $('#lbLevel').text('SA');
                        $('#hdSellerLevel').val('<%=(int)SellerLevel.SA %>');
                    } else if (t >= 3000) {
                        $('#lbLevel').text('A');
                        $('#hdSellerLevel').val('<%=(int)SellerLevel.A %>');
                    } else if (t >= 1000) {
                        $('#lbLevel').text('B');
                        $('#hdSellerLevel').val('<%=(int)SellerLevel.B %>');
                    } else if (t >= 500) {
                        $('#lbLevel').text('C');
                        $('#hdSellerLevel').val('<%=(int)SellerLevel.C %>');
                    } else{
                        $('#lbLevel').text('D');
                        $('#hdSellerLevel').val('<%=(int)SellerLevel.D %>');
                    }
                } else {
                    if (t >= 250) {
                        $('#lbLevel').text('大');
                        $('#hdSellerLevel').val('<%=(int)SellerLevel.Big %>');
                    } else if (t >= 100) {
                        $('#lbLevel').text('中');
                        $('#hdSellerLevel').val('<%=(int)SellerLevel.Medium %>');
                    } else if (t < 100) {
                        $('#lbLevel').text('小');
                        $('#hdSellerLevel').val('<%=(int)SellerLevel.Small %>');
                    }
                }
                $('#btnSetData').show();
                return true;
            } else if ($('#txtSellers').val() == $('#txtAvgPrice').val() == $('#txtCrowdsFlow').val() == '') {
                var lv = $('#ddlSellerLevel option:selected').val();
                if (lv != "-1") {
                    $('#hdSellerLevel').val(lv);
                }
                return true;
            } else {
                alert('含有非數字或0，請檢查');
            }
            return false;
        }

        function SetData() {
            if (SetTurnover()) {
                var data = {
                    "NoCaterLv": $('#ddlSellerLevel option:selected').val(),
                    "Sellers": $('#txtSellers').val(),
                    "AvgSeat": $('#txtAvgSeat').val(),
                    "AvgPrice": $('#txtAvgPrice').val(),
                    "TurnoverRate": $('#txtTurnoverRate').val(),
                    "TurnoverFreq": $('#txtTurnoverFreq').val(),
                    "CrowdsFlow": $('#txtCrowdsFlow').val(),
                }
                data = JSON.stringify(data);
                $('#hdSellerLevelDetail').val(data);
                SetSellerLevel();
            };
            window.parent.$.fancybox.close();
        }

        function SetSellerLevel() {
            var lv = $('#lbLevel').text();
            if (lv != '') {
                $('#lbSellerLevel').text(lv);
            } else {
                var obj = $('#ddlSellerLevel option:selected');
                if (obj.val() != "-1") {
                    lv = obj.text().split('：')[0];
                    $('#lbSellerLevel').text(lv);
                } else {
                    $('#lbSellerLevel').text('');
                    $('#hdSellerLevel').val('');
                    $('#hdSellerLevelDetail').val('');
                }
            }
            
        }

        function ClearDataB() {
            $('.b-area').val('');
            $('.b-area-text').text('');
            $('#btnSetData').show();
            SetSellerLevel();
        }

        function GetMultipleSales()
        {
            $.ajax(
            {
                type: "POST",
                url: "SellerContent.aspx/GetMultipleSales",
                data: JSON.stringify({
                    sellerGuid: '<%=SellerGuid%>',
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {


                    var result = data.d;
                    var _html = '';
                    _html += "<tr>";
                    _html += "<td>業務1 <img id='imgDevelopeSales' title='業務 1：這個欄位填寫的是開發業務' src='../Themes/default/images/17Life/G2/ProposalQu.png' style='width: 20px; '></td>";
                    _html += "<td>業務2 <img id='imgOperationSales' title='業務 2：這個欄位填寫的是經營業務' src='../Themes/default/images/17Life/G2/ProposalQu.png' style='width: 20px; '></td>";
                    _html += "</tr>";

                    $.each(result, function (idx, obj) {
                        _html += "<tr>";
                        _html += "<td id=tdDevelopeSales" + obj.SalesGroup + ">";
                        _html += obj.DevelopeSalesEmpName + obj.DevelopeSalesEmail + obj.DevelopeSalesDeptName.replace("業務部(", "(").replace("系統業務部", "(系統業務部)").replace("通路發展部", "(通路發展部)");
                        _html += "</td>";
                        _html += "<td id=tdOperationSales" + obj.SalesGroup + ">";
                        if (obj.OperationSalesEmail == '')
                            _html += "<input id='txtMultipleOperationSales" + obj.SalesGroup + "' type ='text' cssclass='input-small' style='width: 200px'>";
                        else
                            _html += obj.OperationSalesEmpName + obj.OperationSalesEmail + obj.OperationSalesDeptName.replace("業務部(", "(").replace("系統業務部", "(系統業務部)").replace("通路發展部", "(通路發展部)");
                        _html += "</td>";
                        _html += "</tr>";

                    });

                    $('#multipleSales').html(_html);
                    SalesAutoComplete();
                }, error: function (res) {
                    console.log(res);
                }
            });

            
        }

        function CloseFancyBox() {
            $.fancybox.close();
        }

        function SalesAutoComplete()
        {
            $('[id*=txtMultiple]').autocomplete({
                source: function (request, response) {
                    $.ajax(
                    {
                        type: "POST",
                        url: "SellerContent.aspx/GetSalesmanNameArray",
                        data: JSON.stringify({
                            userName: request.term
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.Label,
                                    value: item.Value
                                }
                            }))
                        }, error: function (res) {
                            console.log(res);
                        }
                    });
                },
            });
        }


        function ShowRemittanceType(obj)
        {
            if (obj.is(':checked'))
                $('#divRemittanceType').show();
            else
                $('#divRemittanceType').hide();
        }

        function show_message(txt) {
            $("#bpopup-toast-message .alert_message").html(txt);
            $('#bpopup-toast-message').bPopup({
                modalClose: false,
                onOpen: function () {
                    $('.bpopup-step_alert-confirm').click(function () {
                        LoadNextStep(txt);
                    });
                },
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <div id="bpopup-toast-message" class="card-panel bpopup-modal bpopup-modal-toast">
        <div class="row">
            <div class="col s12 bpopup-modal-title alert_message">

            </div>
            <div class="col s12 center-align bpopup-modal-footer">
                <a class="waves-effect waves-light btn-warn b-close bpopup-step_alert-confirm">確定</a>
                <a class="waves-effect waves-light btn-default b-close">取消</a>
            </div>
        </div>
    </div>
    <asp:PlaceHolder ID="divMain" runat="server" Visible="false">
        <div class="mc-navbar" style="display: block;">
            <a onclick="SelectTab(this);" tab="MainSet">
                <div class="mc-navbtn on">
                    基本資料
                </div>
            </a><a onclick="SelectTab(this);" tab="divLocation" id="aLocation" runat="server" visible="false">
                <div class="mc-navbtn">
                    營業據點
                </div>
            </a><a onclick="SelectTab(this);" tab="divManageNote" id="aManageNote" runat="server" visible="false">
                <div class="mc-navbtn">
                    經營管理
                </div>
            </a><a onclick="SelectTab(this);" tab="divContract" class="ppon" id="aContract" runat="server" visible="false">
                <div class="mc-navbtn">
                    合約上傳
                </div>
            </a><a onclick="SelectTab(this);" tab="divShoppingCartManage" class="ppon" id="aShoppingCartManage" runat="server" visible="false">
                <div class="mc-navbtn">
                    購物車
                </div>
            </a>
            <%if (LunchKingSite.BizLogic.Facade.ProposalFacade.IsVbsProposalNewVersion())
                {%>
            <a href="/sal/productoption?sid=<%=SellerGuid%>" target="_blank" tab="divProduct" class="ppon" id="a1">
                <div class="mc-navbtn">
                    商品與庫存管理
                </div>
            </a>
            <%} %>
            <a onclick="SelectTab(this);" tab="divISPManage" class="ppon" id="aISPManage" runat="server" visible="false" clientidmode="Static">
                <div class="mc-navbtn">
                    超商作業管理
                </div>
            </a>
        </div>
        <div id="MainSet" class="mc-content">
            <asp:PlaceHolder ID="divBasic" runat="server" Visible="false">
                <div id="basic">
                    <h1 class="rd-smll">
                        <span class="breadcrumb flat">
	                        <a href="javascript:void(0);"><asp:Literal ID="liTitle" runat="server"></asp:Literal></a>
	                        <a href="javascript:void(0);" class="active">商家管理</a>
                        </span>
                        <a href="SellerList.aspx" style="color: #08C;">回列表</a>
                    </h1>
                    <hr class="header_hr">
                    <div class="grui-form">
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                狀態</label>
                            <div class="data-input rd-data-input">
                                <p style="color: #BF0000">
                                    <asp:Literal ID="liTempStatus" runat="server"></asp:Literal>
                                </p>
                                <p>
                                    <asp:Button ID="btnSellerApprove" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="申請核准" OnClick="btnSellerApprove_Click" Visible="false" ClientIDMode="Static" OnClientClick="return CheckSellerApprove();" />
                                    <asp:Button ID="btnSellerReturned" runat="server" CssClass="btn btn-small form-inline-btn" Text="退回申請" OnClick="btnSellerReturned_Click" Visible="false" />
                                    <asp:Button ID="btnSellerApply" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="申請變更" OnClick="btnSellerApply_Click" Visible="false" />
                                    <p class="if-error">
                                        <asp:Literal ID="liMessage" runat="server"></asp:Literal>
                                    </p>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                商家資訊</label>
                            <div class="data-input rd-data-input">
                                <p>
                                    <asp:Literal ID="liSellerName" runat="server"></asp:Literal>
                                </p>
                                <p id="pSellerId">
                                    <asp:Literal ID="liSellerId" runat="server"></asp:Literal>
                                </p>
                                <p>
                                    <a href="ProposalList.aspx?sid=<%= SellerGuid %>" target="_blank" style="color: #08C;">查看提案清單</a>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit" id="divCreateProposal" runat="server" visible="false">
                            <label class="unit-label rd-unit-label">
                                提案申請</label>
                            <div class="data-input rd-data-input">
                                <p>
                                    <asp:Button ID="btnCreateProposal" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="憑證提案" OnClick="btnCreateProposal_Click" Visible="false" OnClientClick="return checkApplyProposal('確定要申請「憑證」提案?',this);" />
                                    <asp:Button ID="btnCreatePBeautyProposal" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="玩美憑證提案" OnClick="btnCreatePBeautyProposal_Click" Visible="false" OnClientClick="return checkApplyProposal('確定要申請「玩美憑證」提案?',this);" />
                                    <asp:Button ID="btnCreateDeliveryProposal" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="宅配提案" Visible="false" OnClientClick="return checkDeliveryProposal('確定要申請「宅配」提案?',this);" />
                                    <asp:TextBox ID="hdProposalDeliveryType" runat="server" Visible="false"></asp:TextBox>

                                    <asp:HyperLink ID="btnCreateTravelProposal" runat="server" Visible="false" ClientIDMode="Static" class="btn btn-small btn-primary form-inline-btn" Style="color: #fff">旅遊提案</asp:HyperLink>

                                    <asp:Button ID="btnCreateTravelPponProposal" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="旅遊憑證提案" Visible="false" OnClick="btnCreateTravelPponProposal_Click" OnClientClick="return checkApplyProposal('確定要申請「旅遊憑證」提案?',this);" />
                                    <asp:Button ID="btnCreateTravelDeliveryProposal" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="旅遊宅配提案" Visible="false" OnClick="btnCreateTravelDeliveryProposal_Click" OnClientClick="return checkApplyProposal('確定要申請「旅遊宅配」提案?',this);" />
                                    <asp:Button ID="btnCreatePiinLifeDeliveryProposal" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="品生活宅配提案" Visible="false" OnClick="btnCreatePiinLifeDeliveryProposal_Click" OnClientClick="return checkApplyProposal('確定要申請「品生活宅配」提案?',this);" />

                                </p>
                                <div id="divCheckSales" class="form-unit" style="display: none; height: 500px; width: 700px;">
                                    <br />
                                    由於您的帳號出現在多組設定中，請協助確認要以哪組人員建立提案單。
                                    <br />
                                    <br />
                                    <table id="selectmultipleSales" style="width:700px">

                                    </table>
                                    <asp:Button ID="btnSelectSalesCancel" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="取消"  OnClick="btnMultipleSalesCreateProposal_Click" ClientIDMode="static"/>
                                    <asp:Button ID="btnSelectSalesSend" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="確認建立" OnClick="btnMultipleSalesCreateProposal_Click" ClientIDMode="Static" />
                                    <input id="hidSelectSalesIsTravelProposal" type="hidden" runat="server" ClientIDMode="Static"/>
                                    <input id="hidSelectSalesIsPBeautyProposal" type="hidden" runat="server" ClientIDMode="Static"/>
                                    <input id="hidSelectSalesProposalDeliveryType" type="hidden" runat="server" ClientIDMode="Static"/>
                                    <input id="hidSelectSalesDevelope" type="hidden" runat="server" ClientIDMode="Static"/>
                                    <input id="hidSelectSalesOperation" type="hidden" runat="server" ClientIDMode="Static"/>
                                </div>
                            </div>
                        </div>
                        <asp:Panel ID="divSalesInfo" runat="server" CssClass="form-unit">
                            
                            <div class="data-input rd-data-input">

                                <table id="multipleSales" style="width:700px">
                                    <tr>
                                        <td>
                                            負責業務1&nbsp;&nbsp;<asp:Image ID="btnVerifyShop" runat="server" ImageUrl="../Themes/default/images/17Life/G2/ProposalQu.png" width="20px" ToolTip="這個欄位填寫的是開發業務" />
                                        </td>
                                        <td>
                                            負責業務2&nbsp;&nbsp;<asp:Image ID="Image1" runat="server" ImageUrl="../Themes/default/images/17Life/G2/ProposalQu.png" width="20px" ToolTip="這個欄位填寫的是經營業務" />
                                        </td>
                                    </tr>
                                </table>
                                
                                <asp:PlaceHolder ID="divReferral" runat="server" Visible="false">
                                    <div id="btnAdd" class="btnAdd" clientidmode="Static">+</div>
                                </asp:PlaceHolder>
                                <asp:HiddenField ID="hidDevelopeSales" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="hidOperationSales" runat="server" ClientIDMode="Static" />
                                
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="divIntroduct" runat="server" CssClass="form-unit" Visible="false">
                            <label class="unit-label rd-unit-label">
                                轉介人員</label>
                            <div class="data-input rd-data-input">
                                <p>
                                    <asp:Literal ID="liReferralSaleName" runat="server"></asp:Literal>
                                    <asp:HiddenField ID="hidDealStatus" runat="server" ClientIDMode="Static" />
                                    <asp:HiddenField ID="hidDealMail" runat="server" ClientIDMode="Static" />
                                </p>
                                <asp:TextBox ID="txtReferralSale" runat="server" CssClass="input-small" Style="width: 200px" ClientIDMode="Static"></asp:TextBox>
                            </div>
                            
                        </asp:Panel>
                        <asp:Panel ID="divIntroductPercent" runat="server" CssClass="form-unit" Visible="false">
                            <label class="unit-label rd-unit-label">
                                趴數選擇</label>
                            <div class="data-input rd-data-input">
                                <p>
                                    <img alt="說明" src="../Themes/default/images/17Life/G2/ProposalQu.png" width="20px" onclick="openSellerRefferContent()" />
                                    <asp:DropDownList ID="ddlReferralPercent" runat="server">
                                        <asp:ListItem Value="0" Text="請選擇"></asp:ListItem>
                                        <asp:ListItem Value="5" Text="5%：沒有人脈介紹，屬於在地業務和系統業務業務內部間的協助"></asp:ListItem>
                                        <asp:ListItem Value="10" Text="10%：B介紹給A / C介紹給A"></asp:ListItem>
                                        <asp:ListItem Value="15" Text="15%：B介紹給A / C介紹給A"></asp:ListItem>
                                        <asp:ListItem Value="20" Text="20%：沒有人脈介紹，屬於在地業務和系統業務業務內部間的協助"></asp:ListItem>
                                        <asp:ListItem Value="30" Text="30%：B介紹給A"></asp:ListItem>
                                        <asp:ListItem Value="35" Text="35%：B介紹給A"></asp:ListItem>
                                        <asp:ListItem Value="40" Text="40%：B介紹給A"></asp:ListItem>
                                    </asp:DropDownList>
                                </p>
                                <asp:HiddenField ID="hidReferralSale" runat="server" ClientIDMode="Static" />
                                <p>
                                    <asp:Literal ID="liReferralSaleBeginTime" runat="server"></asp:Literal> ~
                                </p>
                                <p>
                                    <asp:Literal ID="liReferralSaleEndTime" runat="server"></asp:Literal>
                                </p>
                            </div>
                        </asp:Panel>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                第二線客服</label>
                            <div class="data-input rd-data-input">
                                 <asp:DropDownList runat="server" ID="ddlUserId" Width="300px">
                                 </asp:DropDownList>

                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                工具列</label>
                            <div class="data-input rd-data-input">
                                <asp:Button ID="btnCopySeller" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="複製為新商家" OnClick="btnCopySeller_Click"/>
                                <asp:Button ID="btnCopyChildSeller" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="複製為新的下層商家" OnClick="btnCopyChildSeller_Click" />
                                <asp:HyperLink ID="btnSellerSalesReferral" runat="server" ClientIDMode="Static" Visible="false" class="btn btn-small btn-primary form-inline-btn" style="color:#fff">商家轉件管理</asp:HyperLink>
                                <asp:HyperLink ID="btnVbsItemPriceVisible" runat="server" ClientIDMode="Static" Visible="true" class="btn btn-small btn-primary form-inline-btn" style="color:#fff">商家提案售價顯示管理</asp:HyperLink>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
            <asp:Panel ID="divSeller" runat="server" DefaultButton="btnSave">
                <div><h1 class="rd-smll"><a href="SellerList.aspx" style="color: #08C;">回列表</a></h1></div>
                
                <div id="sell">
                    <h1 class="rd-smll">基本資料
                        <span style="color: red; font-size:small">建立商家資料時，品牌名稱、店家來源、店家屬性、規模為必填欄位，會在存檔時檢查，若無，則無法建立。</span>
                        <span id="span-sell" style="float:right" onclick="CollapseBlock('span-sell','sell-form');">▲</span>
                    </h1>
                    <hr class="header_hr">
                    
                    <div class="grui-form" id="sell-form">
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;品牌/店家名稱</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="txtSellerName" runat="server" CssClass="input-half" ClientIDMode="Static"></asp:TextBox>
                                <p id="pSellerName" class="if-error" style="display: none;">請輸入品牌名稱。</p>
                                <p>
                                    <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;規模
                                </p>
                                <label id="lbSellerLevel"></label>
                                <%--<asp:Button runat="server" ID="btnSellerLvDetail" CssClass="btn btn-small btn-primary form-inline-btn" Text="規則資料設定" OnClientClick="return false;" />--%>
                                <asp:HyperLink ID="btnSellerLvDetail" CssClass="btn btn-small btn-primary form-inline-btn" runat="server" NavigateUrl="#myModal" ClientIDMode="Static" style="color:white">規則資料設定</asp:HyperLink>
                                <p id="pSellerLevel" class="if-error" style="display: none;">請選擇商家規模。</p>
                                <asp:HiddenField runat="server" ID="hdSellerLevel" ClientIDMode="Static" />
                                <asp:HiddenField runat="server" ID="hdSellerLevelDetail" ClientIDMode="Static" />
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;店家來源</label>
                            <div class="data-input rd-data-input">
                                <asp:DropDownList ID="ddlSellerFrom" runat="server" Width="100px" ClientIDMode="Static" AppendDataBoundItems="true">
                                        <asp:ListItem Text="請選擇" Value="-1"></asp:ListItem>
                                    </asp:DropDownList>
                                <p id="pSellerFrom" class="if-error" style="display: none;">請選擇店家來源。</p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;店家屬性</label>
                            <div class="data-input rd-data-input">
                                <asp:CheckBoxList ID="chkSellerPorperty" runat="server" RepeatColumns="10"></asp:CheckBoxList>
                                <p id="pSellerPorperty" class="if-error" style="display: none;">請選擇店家屬性。</p>
                            </div>
                        </div>
                        <div  class="form-unit">
                            <label class="unit-label rd-unit-label">
                                <span style="color: #BF0000; font-weight: bold;"></span>&nbsp;物流方式</label>
                            <div class="data-input rd-data-input">
                                <asp:CheckBox ID="cbHourse" runat="server" Text="宅配" Enabled="false" ClientIDMode="Static" ></asp:CheckBox>
                                <asp:CheckBox ID="cbISPEnabled" runat="server" Text="超商B2C物流交寄" Enabled="false" ></asp:CheckBox>
                                <asp:CheckBox ID="cbWmsEnabled" runat="server" Text="統倉(PChome)" Enabled="false" ></asp:CheckBox>
                            </div>
                        </div>
                        <asp:PlaceHolder ID="phdFreightManage" runat="server" Visible="false">
                            <div class="form-unit">
                                <label class="unit-label rd-unit-label">
                                    購物車運費</label>
                                <div class="data-input rd-data-input">
                                    <div>
                                        系統預設免運(可至<font color="blue">商家系統->購物車管理</font>進行運費設定)
                                    </div>
                                </div>
                            </div>
                        </asp:PlaceHolder>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                上層公司</label>
                            <div class="data-input rd-data-input">
                                <asp:Label ID="liParentSellerId" runat="server" Text="" ClientIDMode="Static"></asp:Label>
                                <asp:TextBox ID="txtParentSellerName" runat="server" ClientIDMode="Static" CssClass="input-half" MaxLength="50"></asp:TextBox>
                                <asp:HyperLink ID="hkSellerTree" runat="server" ClientIDMode="Static" Style="color: blue;text-decoration: underline;" Visible="false" Target="_blank">組織樹狀圖</asp:HyperLink>
                                <asp:HiddenField ID="hidParentSellerGuid" runat="server" ClientIDMode="Static" />
                                <span style="color: #BF0000; font-weight: bold;display:none" id="NoCompany">無此公司</span>
                                <span style="color: #BF0000; font-weight: bold;display:none" id="NotParentCompany">不得將子孫公司(包含自己)做為上層公司</span>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                統一編號/ID</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="txtSignCompanyID" runat="server" ClientIDMode="Static" CssClass="input-half" onkeyup="return ValidateText($(this),value);" MaxLength="10"></asp:TextBox>
                                <asp:CheckBox ID="chkNoCompanyIDCheck" runat="server" Text="忽略驗證" ClientIDMode="Static" />
                                <p id="pCompanyID" class="if-error" style="display: none;">請輸入統一編號/ID。</p>
                                <p id="pCompanyIDLengthError" class="if-error" style="display: none;">統一編號請輸入8碼，身分證字號請輸入10碼。</p>
                                <span><a href="https://www.etax.nat.gov.tw/etwmain//front/ETW113W1_2" target="_blank" style="color:blue;text-decoration: underline;" ><img src="../Themes/default/images/question_icon.gif" alt="公司登記資料查詢"/>公司登記資料查詢</a></span>
                                <asp:HiddenField ID="HiddenField1" runat="server" ClientIDMode="Static"  />
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                公司地址</label>
                            <div class="data-input rd-data-input">
                                <span class="rd-c-br">
                                    <asp:DropDownList ID="ddlCompanyCityId" runat="server" onchange="BindTownship(this);" Width="100px" ClientIDMode="Static" AppendDataBoundItems="true">
                                        <asp:ListItem Text="請選擇" Value="-1"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hidTownshipId" runat="server" ClientIDMode="Static" />
                                    <asp:DropDownList ID="ddlTownshipId" runat="server" Width="100px" onchange="SetTownshipId(this);" ClientIDMode="Static">
                                    </asp:DropDownList>
                                </span>
                                <asp:TextBox ID="txtCompanyAddress" runat="server" CssClass="input-half" ClientIDMode="Static"></asp:TextBox>
                                <label id="lblAddress"></label>
                                <p id="pAddress" class="if-error" style="display: none;">請選擇，並輸入完整的地址。</p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                負責人</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="txtSellerBossName" runat="server" CssClass="input-small" Style="width: 100px" ClientIDMode="Static"></asp:TextBox>
                                <p id="pBossName" class="if-error" style="display: none;">請輸入負責人。</p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                顯示狀態</label>
                            <div class="data-input rd-data-input">
                                <asp:RadioButtonList ID="rdlStatus" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="顯示" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="隱藏" Value="1"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                營業狀態</label>
                            <div class="data-input rd-data-input">
                                <div id="closeDownContainer">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:RadioButtonList ID="rdlcloseDownStatus" runat="server" RepeatDirection="Horizontal" ClientIDMode="Static">
                                                    <asp:ListItem Text="正常營業" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="結束營業" Value="1"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>結業營業日<asp:TextBox runat="server" ID="formCloseDownDate" ClientIDMode="Static" />
                                            </td>
                                        </tr>
                                    </table>
                                    
                                    
                                </div>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                其他</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="txtOthers" runat="server" TextMode="MultiLine" Width="80%" Height="60px"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-unit" style="display:none">
                            <label class="unit-label rd-unit-label">
                                商家圖檔設定</label>
                            <div class="data-input rd-data-input">
                                <asp:Repeater ID="ri" runat="server">
                                    <HeaderTemplate>
                                        <ul id="images">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li>
                                            <asp:Image ID="i" runat="server" ImageUrl='<%#Eval("Text")%>' />
                                            <p class="PicName">
                                                <asp:Literal ID="l" runat="server" Text='<%# System.IO.Path.GetFileName(Eval("Text").ToString())%>' ClientIDMode="Static"></asp:Literal>
                                            </p>
                                            <a href="#" class="dI">X</a><hr />
                                        </li>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ul>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:HiddenField ID="hi" runat="server" ClientIDMode="Static" />
                                <table width="100%">
                                    <tr>
                                        <td>上傳照片
                                            <asp:FileUpload ID="photoUpload" runat="server" />
                                            *jpeg&amp;gif
                                        </td>
                                        <td>
                                            <asp:Button ID="btnUploadPic" runat="server" OnClick="btnUploadPic_Save" Text="儲存圖片" ClientIDMode="Static" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal Start-->
                <div class="container" style="display: none">

                    <div class="mc-content" id="myModal">

                        <h1 class="rd-smll">A區 不是在地餐飲類型的商家請填此區</h1>
                        <div class="form-unit">
                            <div class="data-input rd-data-input" style="margin-left: 0px">
                                <p style="font-size: small; line-height: 20px;">
                                    玩美、旅遊、宅配...等沒有餐飲類型檔次合作的商家，請在這裡設定即可，不用填寫下方區塊 餐飲類型檔次商家，請填下面區塊，此處不填 
                                </p>
                            </div>
                        </div>
                        <div class="grui-form">
                            <div class="form-unit">
                                <label class="unit-label rd-unit-label">
                                    <span>規模</span></label>
                                <div class="data-input rd-data-input">
                                    <asp:DropDownList ID="ddlSellerLevel" runat="server" ClientIDMode="Static">
                                        <asp:ListItem Text="請選擇" Value="-1"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <h1 class="rd-smll">B區 在地餐飲類型的商家請填此區</h1>
                        <div class="form-unit">
                            <div class="data-input rd-data-input" style="margin-left: 0px">
                                <p style="font-size: small; line-height: 20px;">
                                    餐飲類型檔次商家，請填這個區塊，若上方已有選擇，則會依照這裡計算出來的規模為主 
                                </p>
                            </div>
                        </div>
                        <div class="grui-form">
                            <div class="form-unit">
                                <label class="unit-label rd-unit-label">
                                    <span>A 店家數</span></label>
                                <div class="data-input rd-data-input">
                                    <input id="txtSellers" type="text" class="input-small b-area" style="width: 50px" maxlength="5" onkeypress="return event.charCode >= 48 && event.charCode <= 57" />如果是單店，請填 1 (限整數，不允許為 0 )
                                </div>
                            </div>
                            <div class="form-unit">
                                <label class="unit-label rd-unit-label">
                                    <span>B 平均客單價</span></label>
                                <div class="data-input rd-data-input">
                                    <input id="txtAvgPrice" type="text" class="input-small b-area" style="width: 50px" maxlength="5" onkeypress="return event.charCode >= 48 && event.charCode <= 57" />元/人 (限整數，不允許為 0 )
                                </div>
                            </div>
                        </div>

                        <fieldset class="well">
                            <legend>F 人潮流量計算區</legend>
                            <div class="grui-form">
                                <div class="form-unit">
                                    <label class="unit-label rd-unit-label">
                                        <span>C 平均座位數</span></label>
                                    <div class="data-input rd-data-input">
                                        <input id="txtAvgSeat" type="text" class="input-small b-area" style="width: 50px" maxlength="5" onkeypress="return event.charCode >= 48 && event.charCode <= 57" />單店/個 (限整數，不允許為 0 )
                                    </div>
                                </div>
                                <div class="form-unit">
                                    <label class="unit-label rd-unit-label">
                                        <span>D 滿桌率(%)</span></label>
                                    <div class="data-input rd-data-input">
                                        <input id="txtTurnoverRate" type="text" class="input-small b-area" style="width: 50px" maxlength="5" onkeypress="return event.charCode >= 48 && event.charCode <= 57" />% (限整數，不允許為 0 )
                                    </div>
                                </div>
                                <div class="form-unit">
                                    <label class="unit-label rd-unit-label">
                                        <span>E 翻桌次</span></label>
                                    <div class="data-input rd-data-input">
                                        <input id="txtTurnoverFreq" type="text" class="input-small b-area" style="width: 50px" maxlength="5" onkeypress="return event.charCode >= 48 && event.charCode <= 57" />次/月 (限整數，不允許為 0 )
                                    </div>
                                </div>
                                <div class="form-unit">
                                    <div class="data-input rd-data-input" style="margin-left: 0px">
                                        <p style="font-size: large; line-height: 20px; color: blue">
                                            按【計算人潮流量】可以自動計算結果到 F 欄
                                    
                                    <input type="button" value="計算人潮流量" onclick="SetCrowdsFlow();" class="btn btn-small btn-primary form-inline-btn" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <div class="grui-form">
                            <div class="form-unit">
                                <label class="unit-label rd-unit-label">
                                    <span style="font-size: large; line-height: 20px;">F 人潮流量</span></label>
                                <div class="data-input rd-data-input">
                                    <input id="txtCrowdsFlow" type="text" class="input-small b-area" style="width: 70px" maxlength="5" onkeypress="return event.charCode >= 48 && event.charCode <= 57" />
                                    人/月  F人潮流量 = C × D × E
                                </div>
                            </div>

                            <div class="form-unit">
                                <div class="data-input rd-data-input" style="margin-left: 0px; color: blue; font-size: large">

                                    <p>營業額 = A × B × F</p>
                                    計算結果與規模，請按【計算營業額】按鈕進行
                             <input type="button" value="計算營業額" onclick="SetTurnover();" class="btn btn-small btn-primary form-inline-btn" />
                                </div>
                            </div>
                        </div>
                        <div class="grui-form">
                            <h1 class="rd-smll">
                                        G月營業額
                                    <label id="lbTurnover" class="b-area-text"  style="font-size: large;"></label>
                                    計算結果：規模<label id="lbLevel" class="b-area-text"></label>
                            </h1>
 
                        </div>

                        <div class="modal-footer">
                            <input type="button" class="btn btn-default" value="B區資料清空" onclick="ClearDataB();" />
                            <button id="btnSetData" type="button" class="btn btn-default" data-dismiss="modal" onclick="SetData();">確定</button>
                        </div>
                    </div>
                </div>
                <!--Modal End-->
            </asp:Panel>
            <asp:Panel ID="divContact" runat="server">
                <div id="contactlist">
                    <h1 class="rd-smll">聯絡人
                        <span style="color: red; font-size:small">儲存時不是必填欄位，但在建檔時會檢查，依提案單勾選匯款對象的商家是否填寫完整，若無，則無法進行建檔。</span>
                        <span id="span-contact" style="float:right" onclick="CollapseBlock('span-contact','contact-form');">▲</span></h1>
                    <hr class="header_hr">
                    <div class="grui-form" id="contact-form">
                        <div class="form-unit">
                            <asp:Repeater ID="rptMultiContacts" runat="server" OnItemDataBound="rptMultiContacts_ItemDataBound">
                                <ItemTemplate>
                                <div id="spanCloneContact">
                                    <label class="unit-label rd-unit-label">
                                       聯絡人類別</label>
                                    <div class="data-input rd-data-input">
                                        <asp:DropDownList ID="ddlProposalContact" runat="server" ClientIDMode="Static" style="width:150px"></asp:DropDownList>
                                        <p style="color: #BF0000; font-size: small;">* 宅配商家需填寫「<%=Helper.GetDescription(ProposalContact.ReturnPerson) %>」</p>                                    
                                    </div>
                                    <label class="unit-label rd-unit-label">
                                        姓名</label>
                                    <div class="data-input rd-data-input">
                                        <input type="text" id="txtProposalContactName" class="input-half" value="<%# ((Seller.MultiContracts)Container.DataItem).ContactPersonName %>" />
                                        <input id="chkProposalContactType" type="checkbox" onclick="syncBossName(this, 'txtProposalContactName');" />同負責人
                                        <p id="pProposalContact" class="if-error" style="display: none;">請輸入聯絡人類別/姓名/電話。</p>
                                    </div>
                                    <label class="unit-label rd-unit-label">
                                        聯絡電話</label>
                                    <div class="data-input rd-data-input">
                                        <input type="text" id="txtProposalContactPhone" class="input-half" value="<%# ((Seller.MultiContracts)Container.DataItem).SellerTel %>" />
                                    </div>
                                    <label class="unit-label rd-unit-label">
                                        行動電話</label>
                                    <div class="data-input rd-data-input">
                                        <input type="text" id="txtProposalContactMobile" class="input-half" value="<%# ((Seller.MultiContracts)Container.DataItem).SellerMobile %>" />
                                    </div>
                                    <label class="unit-label rd-unit-label">
                                        傳真</label>
                                    <div class="data-input rd-data-input">
                                        <input type="text" id="txtProposalContactFax" class="input-half" value="<%# ((Seller.MultiContracts)Container.DataItem).SellerFax %>" maxlength="20" />
                                    </div>
                                    <label class="unit-label rd-unit-label">
                                        Email</label>
                                    <div class="data-input rd-data-input">
                                        <input type="text" id="txtProposalContactEmail" class="input-half" value="<%# ((Seller.MultiContracts)Container.DataItem).ContactPersonEmail %>" />
                                    </div>
                                    <label class="unit-label rd-unit-label">
                                        其他</label>
                                    <div class="data-input rd-data-input">
                                        <textarea name="txtProposalContactOther" rows="2" cols="20" id="txtProposalContactOther" style="height:60px;width:80%;" ><%# ((Seller.MultiContracts)Container.DataItem).Others %></textarea>
                                    </div>  
                                    <br />
                                    <p style="float:right;">
                                        <%# (Container.ItemIndex == 0 ? "<input type='button' id='btnAddContact'  class='btn rd-mcacstbtn' value='新增聯絡人' onclick='cloneContact()' />" : "<input type='button' id='btnDeleteContact'  class='btn rd-mcacstbtn' value='刪除聯絡人' onclick='deleteContact(this);' />") %>
                                    </p>
                                    <p style="padding-top:20px"></p>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>  
                        </div><!--//<div class="form-unit">-->
                    </div>
                </div>

                <div id="finance">
                    <h1 class="rd-smll">財務資料
                        <span style="color: red; font-size:small">儲存時不是必填欄位，但在建檔時會檢查提案單所屬商家的欄位是否填寫完整，若無，則無法完成建檔。<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            憑證商家至少有一個一般聯絡人；宅配商家則至少要有一個一般聯絡人和一個退換貨聯絡人</span>
                        <span id="span-fiance" style="float:right" onclick="CollapseBlock('span-fiance','finance-form');">▲</span></h1>
                    <hr class="header_hr">
                    <div class="grui-form" id="finance-form">
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                簽約公司名稱</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="txtCompanyName" runat="server" CssClass="input-half"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                簽約公司負責人</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="txtCompanyBossName" runat="server" CssClass="input-half"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                銀行代碼</label>
                            <div class="data-input rd-data-input">
                                <asp:DropDownList ID="ddlBankCode" ClientIDMode="Static" runat="server" Width="150px" onchange="BindBranchCode(this);" AppendDataBoundItems="true">
                                    <asp:ListItem Text="請選擇" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                                <label id="lblBank"></label>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                分行代碼</label>
                            <div class="data-input rd-data-input">
                                <asp:HiddenField ID="hidBranchCode" runat="server" ClientIDMode="Static" />
                                <asp:DropDownList ID="ddlBranchCode" ClientIDMode="Static" runat="server" Width="200px" onchange="SetBranchId(this);">
                                </asp:DropDownList>
                                <label id="lblBankBranch"></label>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                受款人ID</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="txtCompanyID" runat="server" CssClass="input-half" onkeyup="return ValidateText($(this),value);" MaxLength="10"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                帳號</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="txtAccountId" runat="server" CssClass="input-half" onkeyup="return ValidateNumber($(this),value);" MaxLength="14"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                戶名</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="txtAccount" runat="server" CssClass="input-half"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-unit" id="divRemittanceType" runat="server" clientidmode="Static">
                            <% if (config.IsRemittanceFortnightly && IsAgreePponNewContractSeller)
                               { %>
                            <label class="unit-label rd-unit-label">
                                憑證出帳方式</label>
                            <%}else { %>
                            <label class="unit-label rd-unit-label">
                                出帳方式</label>
                            <%} %>
                            <div class="data-input rd-data-input">
                                <asp:DropDownList ID="ddlRemittanceType" runat="server" Width="100px" ClientIDMode="Static" AppendDataBoundItems="true">
                                    <asp:ListItem Text="請選擇" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                店家單據開立方式</label>
                            <div class="data-input rd-data-input">
                                <asp:RadioButton ID="rbRecordInvoice" CssClass="record" GroupName="record" runat="server"  />統一發票
                                <asp:RadioButton ID="rbRecordNoTaxInvoice" CssClass="record" GroupName="record" runat="server" />免稅統一發票
                                <asp:RadioButton ID="rbRecordReceipt" CssClass="record" GroupName="record" runat="server" />免用統一發票收據
                                <asp:RadioButton ID="rbRecordOthers" CssClass="record" GroupName="record" runat="server" ClientIDMode="Static" />其他
                                <asp:TextBox ID="txtAccountingMessage" runat="server" ClientIDMode="Static" />
                                <p id="pRecordOthers" class="if-error" style="display: none;">請輸入店家單據開立方式。</p>
                            </div>
                        </div>
                        <div class="form-unit" id="contact-finance-from">
                            <label class="unit-label rd-unit-label">
                                帳務聯絡人</label>
                            <div class="data-input rd-data-input">
                                <p>姓名</p>
                                <asp:TextBox ID="txtFinanceName" ClientIDMode="Static" runat="server" CssClass="input-small" Style="width: 100px"></asp:TextBox>
                                <asp:CheckBox ID="FinancePersonSync" runat="server" onclick="syncBossName(this, 'txtFinanceName');" Text="同負責人" ClientIDMode="Static" />
                                <p id="pFinanceName" class="if-error" style="display: none;">請輸入帳務聯絡人姓名。</p>
                                <br />
                                <p>電話</p>
                                <asp:TextBox ID="txtFinanceTel" runat="server" CssClass="input-half" ClientIDMode="Static"></asp:TextBox>
                                <p id="pFinanceTel" class="if-error" style="display: none;">請輸入帳務聯絡人電話。</p>
                                <br />
                                <p>Email</p>
                                <asp:TextBox ID="txtFinanceEmail" runat="server" CssClass="input-half" ClientIDMode="Static"></asp:TextBox>
                                <p id="pFinanceEmail" class="if-error" style="display: none;">請輸入帳務聯絡人Email。</p>
                                <br />
                            </div>
                        </div>
                    </div>
                    
                </div>
            </asp:Panel>
            <asp:PlaceHolder ID="SaveHolder" runat="server" >
                <div style="padding-bottom:50px">
                    <div class="form-unit end-unit">
                        <div class="data-input rd-data-input" style="background-color:#f3d58c;text-align:center;padding:5px">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="儲存" OnClick="btnSave_Click" OnClientClick="if( !validateCloseDownInfo() ) { return false; } else {return SaveCheck();}" />
                        </div>
                    </div>
                </div> 
            </asp:PlaceHolder>
            <br />
            <asp:Panel ID="divChangeLog" runat="server" Visible="false" DefaultButton="btnChangeLog">
                <div id="tracklog">
                    <h1 class="rd-smll">變更記錄<span id="span-tracklog" style="float:right" onclick="CollapseBlock('span-tracklog','tracklog-form');">▲</span></h1>
                    <hr class="header_hr">
                    <div class="grui-form" id="tracklog-form">
                        <div class="form-unit">
                            <div class="data-input rd-data-input" style="margin-left: 70px">
                                <asp:TextBox ID="txtChangeLog" runat="server" CssClass="input-half" ClientIDMode="Static" Style="margin-top: 10px"></asp:TextBox>
                                <asp:Button ID="btnChangeLog" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="送出" OnClientClick="return CheckLog();" style="display:none" />
                                <input type="button" class="btn btn-primary rd-mcacstbtn" value="送出" onclick="CheckLog();" />
                                <p id="pChangeLog" class="if-error" style="display: none; margin-top: 8px">請輸入內容。</p>
                            </div>
                            <asp:Repeater ID="rptSellerChangeLog" runat="server">
                                <HeaderTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table" id="tblSellerChangeLog">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="mc-tableContentITEM">
                                        <td style="text-align:left">
                                            <%# ((SellerChangeLog)Container.DataItem).CreateId.Replace("@17life.com.tw", string.Empty).Replace("@17life.com", string.Empty) %>&nbsp;&nbsp;:
                                        </td>
                                        <td style="text-align:left;word-break: break-all;">
                                            <%# ((SellerChangeLog)Container.DataItem).ChangeLog.Replace("|", "<br />") %>
                                        </td>
                                        <td style="text-align:left;width:150px">
                                            <%# ((SellerChangeLog)Container.DataItem).CreateTime.ToString("yyyy/MM/dd HH:mm") %>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            

        </div>

        <div id="divLocation" class="mc-content" style="display: none;">
            <asp:CheckBox ID="chkNoLocation" runat="server" Text="沒有營業據點" ClientIDMode="Static" />
            <div id="location">
                <h1 class="rd-smll">營業據點<span id="span-location" style="float:right" onclick="CollapseBlock('span-location','location-form');">▲</span></h1>
                <hr class="header_hr">                        
                    <div class="grui-form" id="location-form">
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;訂位電話</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="txtStoreTel" runat="server" CssClass="input-half" ClientIDMode="Static"></asp:TextBox>
                                <p id="pStoreTel" class="if-error" style="display: none; margin-top: 8px">請輸入訂位電話。</p>
                            </div>
                            <label class="unit-label rd-unit-label">
                                <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;地址</label>
                            <div class="data-input rd-data-input">
                                <%--<asp:DropDownList ID="ddlCity" runat="server" Width="100px"></asp:DropDownList>
                                <asp:DropDownList ID="ddlTownship" runat="server" Width="100px"></asp:DropDownList>--%>
                                <asp:DropDownList ID="ddlLocationCity" runat="server" onchange="BindLocationTownship(this);" Width="100px" ClientIDMode="Static" AppendDataBoundItems="true">
                                    <asp:ListItem Text="請選擇" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HiddenField ID="hidLocationTownshipId" runat="server" ClientIDMode="Static" />
                                <asp:DropDownList ID="ddlLocationTownshipId" runat="server" Width="100px" onchange="SetLocationTownshipId(this);" ClientIDMode="Static">
                                </asp:DropDownList>
                                <asp:TextBox ID="txtLocationAddress" runat="server" CssClass="input-half" ClientIDMode="Static" placeholder="請填標準地址，不要加其他資訊"></asp:TextBox>
                                <br />
                                <label id="lblAddress2"></label>
                                <p id="pLocationAddress" class="if-error" style="display: none;">請選擇，並輸入完整的地址。</p>
                            </div>
                            <label class="unit-label rd-unit-label">
                                交通資訊</label>
                            <div class="data-input rd-data-input">
                                捷運<asp:TextBox ID="txtMrt" runat="server" CssClass="input-half" MaxLength="500"></asp:TextBox><br />
                                開車<asp:TextBox ID="txtCar" runat="server" CssClass="input-half" MaxLength="500"></asp:TextBox><br />
                                公車<asp:TextBox ID="txtBus" runat="server" CssClass="input-half" MaxLength="500"></asp:TextBox><br />
                                其他<asp:TextBox ID="txtOV" runat="server" CssClass="input-half" MaxLength="500"></asp:TextBox><br />
                            </div>
                            <label class="unit-label rd-unit-label">
                                官網</label>
                            <div class="data-input rd-data-input">
                                官網<asp:TextBox ID="txtWebUrl" runat="server" CssClass="input-full"></asp:TextBox><br />
                                Facebook<asp:TextBox ID="txtFBUrl" runat="server" CssClass="input-full"></asp:TextBox><br />
                                部落格<asp:TextBox ID="txtBlogUrl" runat="server" CssClass="input-full"></asp:TextBox><br />
                                其他<asp:TextBox ID="tbOtherUrl" runat="server" CssClass="input-full"></asp:TextBox><br />
                            </div>
                            <label class="unit-label rd-unit-label">
                                營業時間</label>
                            <div class="data-input rd-data-input">
                                <asp:CheckBox ID="chk_AllBusinessWeekly" runat="server" ClientIDMode="Static" />全選
                                <input type="checkbox" id="chk_24hr"  />24小時無休
                                <input type="checkbox" id="chk_WebInfo"  />依網站公告
                                <br />
                                <input type="checkbox" id="chk_BusinessWeekly0" name="chk_BusinessWeekly" value="0" />週一
                                <input type="checkbox" id="chk_BusinessWeekly1" name="chk_BusinessWeekly" value="1" />週二
                                <input type="checkbox" id="chk_BusinessWeekly2" name="chk_BusinessWeekly" value="2" />週三
                                <input type="checkbox" id="chk_BusinessWeekly3" name="chk_BusinessWeekly" value="3" />週四
                                <input type="checkbox" id="chk_BusinessWeekly4" name="chk_BusinessWeekly" value="4" />週五
                                <input type="checkbox" id="chk_BusinessWeekly5" name="chk_BusinessWeekly" value="5" />週六
                                <input type="checkbox" id="chk_BusinessWeekly6" name="chk_BusinessWeekly" value="6" />週日
                                <br />
                                時間:<asp:DropDownList ID="ddl_WeeklyBeginHour" runat="server" ClientIDMode="Static" style="width:100px;"></asp:DropDownList>:
                                <asp:DropDownList ID="ddl_WeeklyBeginMinute" runat="server" ClientIDMode="Static" style="width:100px;"></asp:DropDownList>～
                                <asp:DropDownList ID="ddl_WeeklyEndHour" runat="server" ClientIDMode="Static" style="width:100px;"></asp:DropDownList>:
                                <asp:DropDownList ID="ddl_WeeklyEndMinute" runat="server" ClientIDMode="Static" style="width:100px;"></asp:DropDownList>
                                <div>
                                    <input type="button" value="新增" onclick="addBusinessHour();" />
                                </div>
                                <asp:Panel ID="panBusinessHour" runat="server" ClientIDMode="Static" style="color:red">
                                   
                                </asp:Panel>
                                <span style="display:none">
                                <asp:TextBox ID="tbBusinessHour" TextMode="MultiLine" runat="server" Width="300" ClientIDMode="Static"></asp:TextBox>
                                </span>
                                <span
                                    style="color: Gray;">
                                    <asp:Literal ID="lit_Store_OpenTime" runat="server"></asp:Literal></span>
                                <div id="divBusinessHour" style="display:none;width:500px;height:300px">
                                    <asp:PlaceHolder ID="phBusinessSellers" runat="server">
                                        <asp:Repeater ID="rptBSeller" runat="server" OnItemDataBound="rptBSeller_ItemDataBound">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAllSeller" runat="server" ClientIDMode="Static" onclick="checkAll(this,'divBusinessSeller')" />全選<br />
                                                <hr class="header_hr">
								            </HeaderTemplate>
								            <ItemTemplate>
                                                <div id="divBusinessSeller">
                                                <asp:CheckBox ID="chkSeller" runat="server" /> <%# ((Seller)Container.DataItem).SellerName %>
									            <br />
                                                </div>
								            </ItemTemplate>
								            <FooterTemplate>
                                                <input type="button" id="btnSyncBusinessHour" value="確認同步" class="btn btn-small btn-primary form-inline-btn" onclick="syncBusinessHour()" />
								            </FooterTemplate>
                                        </asp:Repeater>
                                    </asp:PlaceHolder>
                                </div>
                                <span>
                                <asp:HyperLink ID="syncBusinessHour" runat="server" ClientIDMode="Static" class="btn btn-small btn-primary form-inline-btn openclose" style="color:#fff">將營業時間同步到其他店鋪</asp:HyperLink>
                                <hr />
                            </div>
                            <label class="unit-label rd-unit-label">
                                公休日</label>
                            <div class="data-input rd-data-input">
                                頻率：<asp:DropDownList ID="ddlCloseDate" runat="server" ClientIDMode="Static" Width="100px"></asp:DropDownList><br />
                                <span id="panCloseWeekly">
                                    <asp:CheckBox ID="chk_close_AllBusinessWeekly" runat="server" ClientIDMode="Static" />全選
                                    <br />
                                    <input type="checkbox" id="chk_close_BusinessWeekly0" name="chk_close_BusinessWeekly" value="0" />週一
                                    <input type="checkbox" id="chk_close_BusinessWeekly1" name="chk_close_BusinessWeekly" value="1" />週二
                                    <input type="checkbox" id="chk_close_BusinessWeekly2" name="chk_close_BusinessWeekly" value="2" />週三
                                    <input type="checkbox" id="chk_close_BusinessWeekly3" name="chk_close_BusinessWeekly" value="3" />週四
                                    <input type="checkbox" id="chk_close_BusinessWeekly4" name="chk_close_BusinessWeekly" value="4" />週五
                                    <input type="checkbox" id="chk_close_BusinessWeekly5" name="chk_close_BusinessWeekly" value="5" />週六
                                    <input type="checkbox" id="chk_close_BusinessWeekly6" name="chk_close_BusinessWeekly" value="6" />週日
                                    <br />
                                </span>
                                <span id="panCloseMonthly">
                                    日子：<asp:TextBox ID="txtCloseMonthlyDay" runat="server" placeholder="填入特定日(只寫數字，以、號隔開，如 「1、15」)" Width="300px" ClientIDMode="Static"></asp:TextBox>日<br />
                                </span>
                                <span id="panCloseSpecial">
                                    日期：<asp:TextBox ID="txtCloseBeginDate" runat="server" Width="100px" ClientIDMode="Static" ></asp:TextBox>～<asp:TextBox ID="txtCloseEndDate" runat="server"  Width="100px" ClientIDMode="Static" ></asp:TextBox><br />
                                </span>
                                <span id="panCloseTime">
                                時間:<asp:DropDownList ID="ddl_close_WeeklyBeginHour" runat="server" ClientIDMode="Static" style="width:100px;"></asp:DropDownList>:
                                    <asp:DropDownList ID="ddl_close_WeeklyBeginMinute" runat="server" ClientIDMode="Static" style="width:100px;"></asp:DropDownList>～
                                    <asp:DropDownList ID="ddl_close_WeeklyEndHour" runat="server" ClientIDMode="Static" style="width:100px;"></asp:DropDownList>:
                                    <asp:DropDownList ID="ddl_close_WeeklyEndMinute" runat="server" ClientIDMode="Static" style="width:100px;"></asp:DropDownList>
                                    <asp:CheckBox ID="chk_close_allday" runat="server" ClientIDMode="Static" />全天<br />
                                    <br />
                                </span>
                                <input type="button" value="新增" onclick="addCloseHour();" /><br />
                                <asp:Panel ID="panCloseDate" runat="server" ClientIDMode="Static" style="color:red">
                                   
                                </asp:Panel>
                                <div style="display:none">
                                    <asp:TextBox ID="tbCloseDate" runat="server" Width="300"></asp:TextBox>
                                </div>
                                <span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_CloseDate" runat="server"></asp:Literal></span>
                                <div id="divCloseDate" style="display:none;width:500px;height:300px">
                                    <asp:PlaceHolder ID="phCloseSellers" runat="server">
                                        <asp:Repeater ID="rptCSeller" runat="server" OnItemDataBound="rptCSeller_ItemDataBound">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAllSeller" runat="server" ClientIDMode="Static" onclick="checkAll(this,'divCloseSeller')"  />全選<br />
                                                <hr class="header_hr">
								            </HeaderTemplate>
								            <ItemTemplate>
                                                <div id="divCloseSeller">
                                                <asp:CheckBox ID="chkSeller" runat="server" /> <%# ((Seller)Container.DataItem).SellerName %>
									            <br />
                                                </div>
								            </ItemTemplate>
								            <FooterTemplate>
                                                <input type="button" id="btnSyncCloseDate" value="確認同步" class="btn btn-small btn-primary form-inline-btn" onclick="syncCloseDate()" />
								            </FooterTemplate>
                                        </asp:Repeater>
                                    </asp:PlaceHolder>
                                </div>
                                <span >
                                <asp:HyperLink ID="syncCloseDate" runat="server" ClientIDMode="Static" class="btn btn-small btn-primary form-inline-btn openclose" style="color:#fff">將公休日同步到其他店鋪</asp:HyperLink></span>
                            </div>
                            <label class="unit-label rd-unit-label">
                                備註</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="txtRemark" runat="server" CssClass="input-half" ClientIDMode="Static" Style="margin-top: 10px" TextMode="MultiLine" Width="80%" Height="60px" MaxLength="500"></asp:TextBox>
                            </div>                      
                        </div>
                    </div>
            </div>
            <div id="serivce">
                <h1 class="rd-smll">提供服務<span id="span-service" style="float: right" onclick="CollapseBlock('span-service','service-form');">▲</span></h1>
                <div class="grui-form" id="service-form">
                    <div class="form-unit">
                        <div class="data-input rd-data-input NoLockLocation">
                            <asp:CheckBox ID="chkCreditcardAvailable" runat="server" Text="刷卡服務" /><br />
                            <asp:CheckBox ID="chkIsOpenBooking" runat="server" Text="預約管理" /><br />
                            開啟預約服務連結
                            <asp:RadioButtonList ID="rdlIsOpenBooking" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <asp:ListItem Text="關閉" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="開啟" Value="1"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="data-input rd-data-input">
                            <asp:Button ID="btnSaveLocation" runat="server" Text="儲存" CssClass="btn btn-small btn-primary form-inline-btn" OnClick="btnSaveLocation_Click" OnClientClick="if( !validateBusinessClose() ) { return false; } " ClientIDMode="Static" />

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="divManageNote" class="mc-content" style="display: none;">
                <div id="managenote">
                    <h1 class="rd-smll"><span class="breadcrumb flat">
	                        <a href="javascript:void(0);">經營筆記</a>
	                        <a href="javascript:void(0);" class="active">商家管理</a>
                        </span></h1>
                    <hr class="header_hr">
                    <div class="grui-form" id="manage-form">
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                開發狀態</label>
                            <div class="data-input rd-data-input">
                                <asp:DropDownList ID="ddlDevelopStatus" runat="server" Width="150px" ClientIDMode="Static" AppendDataBoundItems="true">
                                    <asp:ListItem Text="請選擇" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <label class="unit-label rd-unit-label">
                                客戶等級</label>
                            <div class="data-input rd-data-input">
                                <asp:DropDownList ID="ddlSellerGrade" runat="server" Width="150px" ClientIDMode="Static" AppendDataBoundItems="true">
                                    <asp:ListItem Text="請選擇" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="txtManageLog" runat="server" CssClass="input-half" ClientIDMode="Static" Style="margin-top: 10px" TextMode="MultiLine"  Width="80%" Height="60px"></asp:TextBox>
                            </div>
                            <div class="data-input rd-data-input">
                                <%--<asp:Button ID="btnManageLog" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="送出" OnClick="btnManageLog_Click" OnClientClick="return CheckManageLog();" />    --%>
                                <input type="button" class="btn btn-primary rd-mcacstbtn" value="送出" onclick="CheckManageLog();" />
                                <p id="pManageLog" class="if-error" style="display: none; margin-top: 8px">請輸入內容。</p>
                            </div>                            
                        </div>
                    </div>
                </div>

                <div id="managelog">
                    <h1 class="rd-smll">經營記錄<span id="span-managelog" style="float:right" onclick="CollapseBlock('span-managelog','managelog-form');">▲</span></h1>
                    <hr class="header_hr">
                    <div class="grui-form" id="managelog-form">
                        <div class="form-unit">
                            <asp:Repeater ID="rptSellerManageLog" runat="server">
                                <HeaderTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table" id="tblSellerManageLog">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="mc-tableContentITEM">
                                        <td style="text-align:left">
                                            <%# ((SellerManageLog)Container.DataItem).CreateId.Replace("@17life.com.tw", string.Empty).Replace("@17life.com", string.Empty) %>&nbsp;&nbsp;
                                        </td>
                                        <td style="text-align:left">
                                            <%# Helper.GetDevelopStatus<DevelopStatus>(((SellerManageLog)Container.DataItem).DevelopStatus) %>&nbsp;&nbsp;
                                        </td>
                                        <td style="text-align:left">
                                            <%# Helper.GetDevelopStatus<SellerGrade>(((SellerManageLog)Container.DataItem).SellerGrade) %>&nbsp;&nbsp;
                                        </td>
                                        <td style="text-align:left;width:350px;word-break: break-all;">
                                            <%# ((SellerManageLog)Container.DataItem).ChangeLog.Replace("|", "<br />") %>
                                        </td>
                                        <td style="text-align:left;width:150px">
                                            <%# ((SellerManageLog)Container.DataItem).CreateTime.ToString("yyyy/MM/dd HH:mm") %>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>


            <div id="divShoppingCartManage" class="mc-content" style="display: none;">
              
                <div id="shoppingCartlog">
                    <h1 class="rd-smll"><span class="breadcrumb flat">
	                        <a href="javascript:void(0);">購物車</a>
	                        <a href="javascript:void(0);" class="active">商家管理</a>
                        </span></h1>
                    <hr class="header_hr">
                    <div class="grui-form" id="shoppingCartlog-form">
                        <div id="mc-table">
                            <asp:Repeater ID="rptShoppingCart" runat="server">
                                <HeaderTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table" id="tblShoppingCart">
                                        <thead>
                                            <tr class="rd-C-Hide">
                                                <th class="OrderDate">狀態</th>
                                                <th class="OrderDate">賣家</th>
                                                <th class="OrderDate">序號</th>
                                                <th class="OrderDate">購物車名稱</th>
                                                <th class="OrderDate">免運門檻</th>
                                                <th class="OrderDate">未滿門檻收取運費</th>
                                                <th class="OrderDate">新增時間</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="mc-tableContentITEM">
                                        <td style="text-align:left">
                                            <%# ((ShoppingCartManageList)Container.DataItem).FreightsStatusName %>&nbsp;&nbsp;
                                        </td>
                                        <td style="text-align:left;word-break: break-all;">
                                            <%# ((ShoppingCartManageList)Container.DataItem).SellerName.Replace("|", "<br />") %>
                                        </td>
                                        <td style="text-align:left;">
                                            <%# ((ShoppingCartManageList)Container.DataItem).UniqueId %>
                                        </td>
                                        <td style="text-align:left;">
                                            <%# ((ShoppingCartManageList)Container.DataItem).FreightsName %>
                                        </td>
                                        <td style="text-align:center;">
                                            <%# ((ShoppingCartManageList)Container.DataItem).NoFreightLimit %>
                                        </td>
                                        <td style="text-align:center;">
                                            <%# ((ShoppingCartManageList)Container.DataItem).Freights %>
                                        </td>
                                        <td style="text-align:center;">
                                            <%# ((ShoppingCartManageList)Container.DataItem).CreateTime.ToString("yyyy/MM/dd HH:mm") %>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <br />
                        <h1 class="rd-smll">購物車異動紀錄</h1>
                        <hr class="header_hr">
                        <div class="form-unit">
                            <asp:Repeater ID="rptShoppingCartLog" runat="server">
                                <HeaderTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table" id="tblShoppingCartLog">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="mc-tableContentITEM">
                                        <td style="text-align: left">
                                            <%# ((ShoppingCartFreightsLog)Container.DataItem).CreateUser.Replace("@17life.com.tw", string.Empty).Replace("@17life.com", string.Empty) %>&nbsp;&nbsp;
                                        </td>
                                        <td style="text-align: left; width: 350px; word-break: break-all;">
                                            <%# ((ShoppingCartFreightsLog)Container.DataItem).ContentLog.Replace("|", "<br />") %>
                                        </td>
                                        <td style="text-align: left; width: 150px">
                                            <%# ((ShoppingCartFreightsLog)Container.DataItem).CreateTime.ToString("yyyy/MM/dd HH:mm") %>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>

            <div id="divContract" class="mc-content" style="display: none;">
                <div id="contract">
                    <h1 class="rd-smll"><span class="breadcrumb flat">
	                        <a href="javascript:void(0);">合約上傳</a>
	                        <a href="javascript:void(0);" class="active">商家管理</a>
                        </span></h1>
                    <hr class="header_hr">
                    <div class="grui-form" id="contractfile-form">
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                類型</label>
                            <div class="data-input rd-data-input">
                                <asp:DropDownList ID="ddlContactType" runat="server" Width="180px">
                                <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <p id="pContractFile" class="if-error" style="display: none;">請選擇合約類型。</p>
                                <p>
                                    <label class="unit-label rd-unit-label">版號</label>
                                    <asp:TextBox ID="txtContractVersion" runat="server" CssClass="input-half" ClientIDMode="Static" Style="width: 200px" MaxLength="20"></asp:TextBox>
                                    <p id="pContractVersion" class="if-error" style="display: none;">必填。</p>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                備註/關鍵字</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="txtContractFileMemo" runat="server" CssClass="input-half" ClientIDMode="Static" Style="width: 200px" MaxLength="80"></asp:TextBox>
                                <p>
                                    <label class="unit-label rd-unit-label">合約到期日</label>
                                    <asp:TextBox ID="txtContractDueDate" runat="server" CssClass="input-half" ClientIDMode="Static" Style="width: 200px"></asp:TextBox>
                                </p>
                            </div>

                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                選擇檔案</label>
                            <div class="data-input rd-data-input">
                                <ul id="fileList">
                                </ul>
                                <input type="file" id="fileContract" />
                                <input type="button" id="btnFileUpload" class="btn btn-primary rd-mcacstbtn" value="上傳" onclick="ContractUpload()" />
                                <p>
                                    <label class="unit-label rd-unit-label">
                                        <span style="color: #BF0000; font-weight: bold;">*</span>乙方聯絡人</label>
                                    <asp:TextBox ID="txtContractPartyBContact" runat="server" CssClass="input-half" ClientIDMode="Static" Style="width: 200px" MaxLength="50"></asp:TextBox>
                                </p>
                                <p id="pContractPartyBContact" class="if-error" style="display: none;">請輸入乙方聯絡人。</p>
                            </div>
                        </div>
                    </div>
                    <div class="form-unit">
                            <div id="mc-table">
                                <asp:Repeater ID="rptContractFiles" runat="server">
                                    <HeaderTemplate>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                                            <tr class="rd-C-Hide">
                                                <th class="OrderDate" style="width:250px">檔名
                                                </th>
                                                <th class="OrderDate">版號
                                                </th>
                                                <th class="OrderSerial">備註/關鍵字
                                                </th>
                                                <th class="OrderSerial">合約到期日
                                                </th>
                                                <th class="OrderSerial">乙方聯絡人
                                                </th>
                                                <th class="OrderName">類型
                                                </th>
                                                <th class="OrderDate">建立人員
                                                </th>
                                                <th class="OrderCouponState">建立時間
                                                </th>
                                                <th class="OrderCouponState">
                                                </th>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr class="mc-tableContentITEM">
                                            <td style="text-align:left">                                                
                                                <a href="#" onclick="DownLoadContract('<%# ((SellerContractFile)Container.DataItem).Guid %>')"><%# ((SellerContractFile)Container.DataItem).FileName %></a>                                            
                                            </td>
                                            <td style="text-align:left">
                                                <%# ((SellerContractFile)Container.DataItem).Version %>
                                            </td>
                                            <td style="text-align:left">
                                                <%# ((SellerContractFile)Container.DataItem).Memo.Replace("|", "<br />") %>
                                            </td>
                                            <td style="text-align: left">
                                                <%# ((SellerContractFile)Container.DataItem).DueDate.Equals(DateTime.MinValue) ? "" : ((SellerContractFile)Container.DataItem).DueDate.ToString("yyyy/MM/dd") %>    
                                            </td>
                                            <td style="text-align: left">
                                                <%# ((SellerContractFile)Container.DataItem).PartyBContact.Replace("|", "<br />") %>
                                            </td>
                                            <td style="text-align:left">
                                                <%# Helper.GetDevelopStatus<SellerContractType>(((SellerContractFile)Container.DataItem).ContractType) %>
                                            </td>
                                            <td style="text-align:left">
                                                <%# ((SellerContractFile)Container.DataItem).CreateId.Replace("@17life.com.tw", string.Empty).Replace("@17life.com", string.Empty) %>
                                            </td>
                                            <td style="text-align:left">
                                                <%# ((SellerContractFile)Container.DataItem).CreateTime.ToString("yyyy/MM/dd HH:mm") %>
                                            </td>
                                            <td style="text-align:left">
                                                <img src="../Themes/default/images/17Life/G2/delete.png" alt="刪除" title="刪除" style="padding-left:5px" class="view" onclick="deleteContractFile('<%# ((SellerContractFile)Container.DataItem).Guid %>');">
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                </div>
            </div>
        <!--側邊anchor Start-->
        <ul id="subMenu" class="nav-anchor" style="top: 150px">
            
        </ul>

        <!--側邊anchor End-->
    </asp:PlaceHolder>
    <div style="display:none">
        <asp:TextBox ID="hdContacts" runat="server" ClientIDMode="Static"  Visible="true"></asp:TextBox>
        <asp:TextBox ID="hdEnabled" runat="server" ClientIDMode="Static"  Visible="true"></asp:TextBox>
        
    </div>
    <div id="divSellerRefferContent" style="width: 800px; height: 600px; display:none">
        <div style="margin: 10px">
            <table border="1">
                <thead>
                    <tr>
                        <th>名詞</th>
                        <th>代表意義</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>A</td>
                        <td>在地業務、系統業務</td>
                    </tr>
                    <tr>
                        <td>B</td>
                        <td>17Life其他同仁</td>
                    </tr>
                    <tr>
                        <td>C</td>
                        <td>外部人員(非17Life同仁)</td>
                    </tr>
                    <tr>
                        <td>L1</td>
                        <td>客戶主要負責人、老闆</td>
                    </tr>
                    <tr>
                        <td>L2</td>
                        <td>客戶主要負責人下一層主管</td>
                    </tr>
                </tbody>
            </table>
            <br />
            <font color="#f86207"><strong>A 17Life其他同仁 介紹給17Life的業務</strong></font>
            <table border="1">
                <thead>
                    <tr>
                        <th>轉介程度</th>
                        <th>關係</th>
                        <th>動作</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>10%</td>
                        <td>介紹負責人員(非L1&L2，但是為相關核心主管級人員)</td>
                        <td>B需要協助安排會議，但不需要親自出席會議，後續由A自行洽談合作。<br />舉例-同事B認識小蒙牛行銷主管，介紹給業務後，且安排會議，後續由業務A跟行銷主管洽談合作案。</td>
                    </tr>
                    <tr>
                        <td>15%</td>
                        <td>介紹負責人員(非L1&L2，但是為相關核心主管級人員)</td>
                        <td>由A主導洽談，B參與第1次面對面會議，由A完成後續簽約。</td>
                    </tr>
                    <tr>
                        <td>30%</td>
                        <td>介紹Key man(L1&L2)(老闆或負責人或有決定權的人-總經理、副總經理、營運長等)</td>
                        <td>由A主導洽談，B全程參與洽談合作(如果有三次會議，B也陪同三次)，由A完成後續簽約。</td>
                    </tr>
                    <tr>
                        <td>35%</td>
                        <td>介紹Key man(L1&L2)(老闆或負責人或有決定權的人-總經理、副總經理、營運長等)</td>
                        <td>由B洽談合作(包含所有的條件)，A完成後續簽約<br />毛利率規範不得低於公司規範標準，若毛利低於公司規範，則適用前一階的獎金分配條件(30%)。</td>
                    </tr>
                    <tr>
                        <td>40%</td>
                        <td>介紹Key man(L1&L2)(老闆或負責人或有決定權的人-總經理、副總經理、營運長等)</td>
                        <td>由B洽談合作(包含所有條件)，A完成後續簽約<br />＊必需在公司毛利規範下高於條件2%。</td>
                    </tr>
                </tbody>
            </table>

            <br />
            <font color="#f86207"><strong>B 外部人員(非17Life同仁) 介紹給17Life的業務</strong></font>
            <table border="1">
                <thead>
                    <tr>
                        <th>轉介程度</th>
                        <th>關係</th>
                        <th>動作</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>20%</td>
                        <td>介紹負責人員(非L1&L2，但是為相關核心主管級人員)</td>
                        <td>B需要協助安排會議，但不需要親自出席會議，後續由A自行洽談合作。<br />舉例-同事B認識小蒙牛行銷主管，介紹給業務後，且安排會議，後續由業務A跟行銷主管洽談合作案。</td>
                    </tr>
                    <tr>
                        <td>30%</td>
                        <td>介紹負責人員(非L1&L2，但是為相關核心主管級人員)</td>
                        <td>由A主導洽談，B參與第1次面對面會議，由A完成後續簽約。</td>
                    </tr>
                    
                </tbody>
            </table>

            <br />
            <font color="#f86207"><strong>C 沒有人脈介紹 屬於在地業務和系統業務業務內部間的協助。</strong></font>
            <table border="1">
                <thead>
                    <tr>
                        <th>轉介程度</th>
                        <th>關係</th>
                        <th>動作</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>5%</td>
                        <td>沒有人脈介紹</td>
                        <td>透過業務技巧約到店家。</td>
                    </tr>
                    <tr>
                        <td>20%</td>
                        <td>沒有人脈介紹</td>
                        <td>由B洽談合作(包含所有的條件)，A完成後續簽約<br />＊必需在公司毛利規範下高於條件2%。</td>
                    </tr>
                    
                </tbody>
            </table>
        </div>
    </div>

    <div id="divVbsProposalItemPriceContent" style="display:none;width:400px;height:400px">
        <asp:CheckBoxList ID="radDealType" runat="server"></asp:CheckBoxList>
        <input type="button" id="btnVbsProposalItemPrice" class="btn btn-small btn-primary form-inline-btn" value="儲存" />
    </div>

    <div id="divISPManage" class="mc-content"  style="display:none;">
        <h1 class="rd-smll">
            <span class="breadcrumb flat">
	            <a href="javascript:void(0);">超商作業管理</a>
	            <a href="javascript:void(0);" class="active">商家管理</a>
            </span>
            <a href="SellerList.aspx" style="color: #08C;">回列表</a>
        </h1>
        <hr />
        <div>
            <% LunchKingSite.WebLib.Component.WebFormMVCUtil.RenderAction("~/Views/Proposal/ISPOptionContent.cshtml", GetModel()); %>
        </div>
        <br />
        <asp:HiddenField ID="hdReturnCycleFamily" runat="server" ClientIDMode="Static"  />
        <asp:HiddenField ID="hdReturnTypeFamily" runat="server" ClientIDMode="Static"  />
        <asp:HiddenField ID="hdReturnOtherFamily" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdReturnCycleSeven" runat="server" ClientIDMode="Static"  />
        <asp:HiddenField ID="hdReturnTypeSeven" runat="server" ClientIDMode="Static"  />
        <asp:HiddenField ID="hdReturnOtherSeven" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdReturnAddress" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdContactName" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdContactTel" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdContactEmail" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdContactMobile" runat="server" ClientIDMode="Static" />

        全家
        <div id="divIspVerifyFamily" runat="server" visible="false">
            <input id="btnRejectPopupFamily" type="button" value="駁回" />
            <asp:Button runat="server" ID="btnPassFamily" Text="審核通過" OnClick="btnPassFamily_Click" ClientIDMode="Static" OnClientClick="return check_data_Family();"/>
            <div id="divRejectFamily"  style="width:500px;height:200px;display:none">
                <asp:TextBox Width="450" Height="100" ID="tbRejectReasonFamily" runat="server" TextMode="MultiLine" placeholder="請輸入駁回原因" ></asp:TextBox>
                <br /><br />
                <asp:Button runat="server" ID="btnRejectFamily" Text="送出" OnClick="btnRejectFamily_Click" />
            </div>
        </div>
        <div id="divIspUpdateFamily" runat="server" visible="false">
            <%--<asp:Button runat="server" ID="btnUndoFamily" Text="取消修改" OnClick="btnUndoFamily_Click" />--%>
            <asp:Button runat="server" ID="btnUpdateFamily" Text="確認修改" OnClick="btnUpdateFamily_Click" OnClientClick="return check_data_Family();"/>
        </div>

        7-11
        <div id="divIspVerifySeven" runat="server" visible="false">
            <input id="btnRejectPopupSeven" type="button" value="駁回" />
            <asp:Button runat="server" ID="btnPassSeven" Text="審核通過" OnClick="btnPassSeven_Click" ClientIDMode="Static" OnClientClick="return check_data_Seven();"/>
            <div id="divRejectSeven"  style="width:500px;height:200px;display:none">
                <asp:TextBox Width="450" Height="100" ID="tbRejectReasonSeven" runat="server" TextMode="MultiLine" placeholder="請輸入駁回原因" ></asp:TextBox>
                <br /><br />
                <asp:Button runat="server" ID="btnRejectSeven" Text="送出" OnClick="btnRejectSeven_Click" />
            </div>
        </div>
        <div id="divIspUpdateSeven" runat="server" visible="false">
            <%--<asp:Button runat="server" ID="btnUndoSeven" Text="取消修改" OnClick="btnUndoSeven_Click" />--%>
            <asp:Button runat="server" ID="btnUpdateSeven" Text="確認修改" OnClick="btnUpdateSeven_Click" OnClientClick="return check_data_Seven();"/>
        </div>
    </div>

    <div id="bpopup-warehouseType" class="card-panel bpopup-modalV2" style="left: 62.5px; position: absolute; top: 45.75px; z-index: 9999; opacity: 1; display: none;">
        請選擇配宅提案類型<br />
       <asp:LinkButton ID="btnCreateDeliveryNormalProposal" runat="server" class="btn btn-small btn-primary form-inline-btn fix_btn_height" style="color:#fff" Text="一般宅配 (自出含快出與超取)" OnClick="btnCreateDeliveryNormalProposal_Click" OnClientClick="return true"/><br />
       

         <% if (IsConfirmWms)
            { %>
                <asp:LinkButton ID="btnCreateDeliveryWmsProposal" runat="server" class="btn btn-small btn-primary form-inline-btn fix_btn_height" style="color:#fff;" Text="24H到貨 (倉儲+物流)" OnClick="btnCreateDeliveryWmsProposal_Click" OnClientClick="return true"/><br />
         <%}%>


        提醒您：一但選擇提案類型後即無法變更喔
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="StoreContent.aspx.cs" Inherits="LunchKingSite.Web.Sal.StoreContent" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/fancybox/jquery.fancybox.css") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Tools/js/fancybox/jquery.fancybox.js"></script>
    <style type="text/css">
        .remove a {
            padding-right:30px;
            background: url(/Themes/PCweb/images/x.png) no-repeat right center;
        }
        .breadcrumb {
	        /*centering*/
	        display: inline-block;
	        box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.35);
	        overflow: hidden;
	        border-radius: 5px;
	        /*Lets add the numbers for each link using CSS counters. flag is the name of the counter. to be defined using counter-reset in the parent element of the links*/
	        counter-reset: flag; 
        }

        .breadcrumb a {
	        text-decoration: none;
	        outline: none;
	        display: block;
	        float: left;
	        font-size: 12px;
	        line-height: 36px;
	        color: white;
	        /*need more margin on the left of links to accomodate the numbers*/
	        padding: 0 10px 0 60px;
	        background: #666;
	        background: linear-gradient(#666, #333);
	        position: relative;
        }


        /*adding the arrows for the breadcrumbs using rotated pseudo elements*/
        .breadcrumb a:after {
	        content: '';
	        position: absolute;
	        top: 0; 
	        right: -18px; /*half of square's length*/
	        /*same dimension as the line-height of .breadcrumb a */
	        width: 36px; 
	        height: 36px;
	        /*as you see the rotated square takes a larger height. which makes it tough to position it properly. So we are going to scale it down so that the diagonals become equal to the line-height of the link. We scale it to 70.7% because if square's: 
	        length = 1; diagonal = (1^2 + 1^2)^0.5 = 1.414 (pythagoras theorem)
	        if diagonal required = 1; length = 1/1.414 = 0.707*/
	        transform: scale(0.707) rotate(45deg);
	        /*we need to prevent the arrows from getting buried under the next link*/
	        z-index: 1;
	        /*background same as links but the gradient will be rotated to compensate with the transform applied*/
	        background: #666;
	        background: linear-gradient(135deg, #666, #333);
	        /*stylish arrow design using box shadow*/
	        box-shadow: 
		        2px -2px 0 2px rgba(0, 0, 0, 0.4), 
		        3px -3px 0 2px rgba(255, 255, 255, 0.1);
	        /*
		        5px - for rounded arrows and 
		        50px - to prevent hover glitches on the border created using shadows*/
	        border-radius: 0 5px 0 50px;
        }
       
        .flat a, .flat a:after {
	        background: white;
	        color: black;
	        transition: all 0.5s;
        }

        .flat a.active, 
        .flat a.active:after{
            color:#fff;
	        background: #AF0000;
        }
    </style>
    <script type="text/javascript">
        var frequency = {
            weekly: 1,
            monthly: 2,
            special: 3,
            alldayround: 99
        }
        $(document).ready(function () {

            var city = $('#ddlCompanyCityId');
            if ($(city).val() != '-1') {
                BindTownship(city);
            }

            var bank = $('#ddlBankCode');
            if ($(bank).val() != '-1') {
                BindBranchCode(bank);
            }


            var _settingTime = $("#txtOpenTime").val();
            var _settingTimes = _settingTime.split(";");
            $.each(_settingTimes, function (idx, value) {
                if (value != "") {
                    appendSettingTime(value);
                }
            });

            /*
            * 公休日
            */
            HideOrShowCloseSpan();
            $("#ddlCloseDate").change(function () {
                HideOrShowCloseSpan();
            });

            //公休日(特殊日期)
            $('#txtCloseBeginDate').datepicker({ dateFormat: 'yy/mm/dd' });
            $('#txtCloseEndDate').datepicker({ dateFormat: 'yy/mm/dd' });


            if ($("#txtStoreName").attr("disabled") == "disabled") {
                $(".openclose").css("display","none");
            }


            /*
           * 營業時間全選
           */
            $("#chk_AllBusinessWeekly").click(function () {
                if ($(this).attr("checked")) {
                    $("input[name*=chk_BusinessWeekly]").attr("checked", true);
                } else {
                    $("input[name*=chk_BusinessWeekly]").attr("checked", false);
                }
            });

            $("input[name*=chk_BusinessWeekly]").click(function () {
                var flag = true;
                $.each($("input[name*=chk_BusinessWeekly]"), function () {
                    if (!$(this).attr("checked")) {
                        flag = false;
                    }
                });
                if (!flag) {
                    $("#chk_AllBusinessWeekly").attr("checked", false);
                } else {
                    $("#chk_AllBusinessWeekly").attr("checked", true);
                }
            });

            /*
            * 公休時間全選
            */
            $("#chk_close_AllBusinessWeekly").click(function () {
                if ($(this).attr("checked")) {
                    $("input[name*=chk_close_BusinessWeekly]").attr("checked", true);
                } else {
                    $("input[name*=chk_close_BusinessWeekly]").attr("checked", false);
                }
            });

            $("input[name*=chk_close_BusinessWeekly]").click(function () {
                var flag = true;
                $.each($("input[name*=chk_close_BusinessWeekly]"), function () {
                    if (!$(this).attr("checked")) {
                        flag = false;
                    }
                });
                if (!flag) {
                    $("#chk_close_AllBusinessWeekly").attr("checked", false);
                } else {
                    $("#chk_close_AllBusinessWeekly").attr("checked", true);
                }
            });
            
            BindOpenCloseDate();

            $("#syncBusinessHour").fancybox();
            $("#syncCloseDate").fancybox();


            //顯示文字
            $("#ddlTownshipId").change(function () {
                ShowText("Address");
            });

            $("#txtCompanyAddress").change(function () {
                ShowText("Address");
            });

            $("#ddlBankCode").change(function () {
                ShowText("Bank");
            });

            $("#ddlBranchCode").change(function () {
                ShowText("BankBranch");
            });
        });

        /*
        * 同步營業時間到其他店鋪
        */
        function syncBusinessHour() {
            var storeGuid = [];
            $.each($("#divBusinessHour input:checked").not("#chkAllStore"), function () {
                var obj = $(this).val();
                storeGuid.push(obj);
            });

            var _settingTimes = "";
            $.each($("#panBusinessHour div"), function (idx, value) {
                _settingTimes += value.innerText + "；";
            });
            if (_settingTimes.length > 0) {
                _settingTimes = _settingTimes.substring(0, _settingTimes.length - 1);
            }
            if (storeGuid.length > 0) {
                $.ajax({
                    type: "POST",
                    url: "StoreContent.aspx/SyncBusinessHour",
                    data: "{'storeGuid': '" + JSON.stringify(storeGuid) + "','opentime' : '" + _settingTimes + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        alert("資料已同步");
                        window.parent.$.fancybox.close();
                    }
                });
            }
        }

        /*
        * 同步公休時間到其他店鋪
        */
        function syncCloseDate() {
            var storeGuid = [];
            $.each($("#divCloseDate input:checked").not("#chkAllStore"), function () {
                var obj = $(this).val();
                storeGuid.push(obj);
            });

            var _settingTimes = "";
            $.each($("#panCloseDate div"), function (idx, value) {
                _settingTimes += value.innerText + "；";
            });
            if (_settingTimes.length > 0) {
                _settingTimes = _settingTimes.substring(0, _settingTimes.length - 1);
            }
            if (storeGuid.length > 0) {
                $.ajax({
                    type: "POST",
                    url: "StoreContent.aspx/SyncCloseDate",
                    data: "{'storeGuid': '" + JSON.stringify(storeGuid) + "','closedate' : '" + _settingTimes + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        alert("資料已同步");
                        window.parent.$.fancybox.close();
                    }
                });
            }
        }

        function BindOpenCloseDate() {
            //營業時間
            var _settingTime = $("#txtOpenTime").val();
            var _settingTimes = _settingTime.split("；");
            $.each(_settingTimes, function (idx, value) {
                if (value != "") {
                    appendSettingTime(value, "panBusinessHour");
                }
            });

            //公休日
            var _settingCloseTime = $("#txtCloseDate").val();
            var _settingCloseTimes = _settingCloseTime.split("；");
            $.each(_settingCloseTimes, function (idx, value) {
                if (value != "") {
                    appendSettingTime(value, "panCloseDate");
                }
            });
        }

        function BindTownship(obj) {
            $.ajax({
                type: "POST",
                url: "<%= config.SiteUrl %>/api/locationservice/GetTownships",
                data: "{'cid': '" + $(obj).val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var township = $('#ddlTownshipId');
                    $(township).find('option').remove();
                    $.each(response.Data, function (index, item) {
                        $(township).append($('<option></option>').attr('value', item.id).text(item.name));
                    });
                    $(township).val($('#hidTownshipId').val());
                    SetTownshipId(township);
                    ShowText("Address");
                }
            });
        }

        function SetTownshipId(obj) {
            $('#hidTownshipId').val($(obj).val());
        }

        function BindBranchCode(obj) {
            $.ajax({
                type: "POST",
                url: "SellerContent.aspx/BankInfoGetBranchList",
                data: "{'id': '" + $(obj).val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var branch = $('#ddlBranchCode');
                    $(branch).find('option').remove();
                    $.each(response.d, function (index, item) {
                        $(branch).append($('<option></option>').attr('value', item.BranchNo).text(item.BranchName));
                    });
                    if ($('#hidBranchCode').val() != '') {
                        $(branch).val($('#hidBranchCode').val());
                    } else {
                        SetBranchId(branch);
                    }
                    ShowText("Bank");
                    ShowText("BankBranch");
                }
            });
        }

        function SetBranchId(obj) {
            $('#hidBranchCode').val($(obj).val());
        }

        function CheckLog() {
            if ($.trim($('#txtChangeLog').val()) == '') {
                $('#pChangeLog').show();
                return false;
            }
            return true;
        }

        function syncBossName(obj) {
            if ($(obj).is(':checked')) {
                $('#txtFinanceName').val($('#txtBossName').val());
            } else {
                $('#txtFinanceName').val('');
            }
        }

        function SaveCheck() {
            $('.if-error').hide();
            $('.error').removeClass('error');

            var check = true;
            if ($.trim($('#hidTownshipId').val()) == '' || $.trim($('#txtCompanyAddress').val()) == '') {
                $('#pAddress').show();
                $('#pAddress').parent().parent().addClass('error');
                check = false;
            }
            if ($.trim($('#txtStoreName').val()) == '') {
                check = false;
                $('#pStoreName').show();
                $('#pStoreName').parent().parent().addClass('error');
            }
            if ($.trim($('#txtBossName').val()) == '') {
                check = false;
                $('#pBossName').show();
                $('#pBossName').parent().parent().addClass('error');
            }
            if ($.trim($('#txtPhone').val()) == '') {
                check = false;
                $('#pPhone').show();
                $('#pPhone').parent().parent().addClass('error');
            }
            if (!check) {
                $('#txtStoreName').focus();
            }

            var _settingTimes = "";
            $.each($("#panBusinessHour div"), function (idx, value) {
                _settingTimes += value.innerText + "；";
            });
            if (_settingTimes.length > 0) {
                _settingTimes = _settingTimes.substring(0, _settingTimes.length - 1);
            }
            $("#txtOpenTime").val(_settingTimes);

            var _settingCloseTimes = "";
            $.each($("#panCloseDate div"), function (idx, value) {
                _settingCloseTimes += value.innerText + "；";
            });
            if (_settingCloseTimes.length > 0) {
                _settingCloseTimes = _settingCloseTimes.substring(0, _settingCloseTimes.length - 1);
            }
            $("#txtCloseDate").val(_settingCloseTimes);

            return check;
        }

        function ValidateNumber(e, pnumber) {
            if (!/^\d+$/.test(pnumber)) {
                $(e).val(/^\d+/.exec($(e).val()));
            }
            return false;
        }

        function ValidateText(e, pnumber) {
            if (!/^[\d|a-zA-Z]+$/.test(pnumber)) {
                $(e).val(/^\d+/.exec($(e).val()));
            }
            return false;
        }
        function addBusinessHour() {
            var chk_BusinessWeekly = $("input[name*=chk_BusinessWeekly]:checked");
            var ddl_WeeklyBeginHour = $("#ddl_WeeklyBeginHour").val();
            var ddl_WeeklyBeginMinute = $("#ddl_WeeklyBeginMinute").val();
            var ddl_WeeklyEndHour = $("#ddl_WeeklyEndHour").val();
            var ddl_WeeklyEndMinute = $("#ddl_WeeklyEndMinute").val();
            var _html = ddl_WeeklyBeginHour + "~" + ddl_WeeklyEndHour;

            var weeklys = chk_BusinessWeekly.map(function () {
                return this.value;
            }).get().join();

            getWeeklyContinue("", weeklys, ddl_WeeklyBeginHour + ":" + ddl_WeeklyBeginMinute, ddl_WeeklyEndHour + ":" + ddl_WeeklyEndMinute, false, "panBusinessHour");

            $("input[name*=chk_BusinessWeekly]").prop('checked', false);
            $("#ddl_WeeklyBeginHour").val("01");
            $("#ddl_WeeklyBeginMinute").val("00");
            $("#ddl_WeeklyEndHour").val("01");
            $("#ddl_WeeklyEndMinute").val("00");

            $("#chk_AllBusinessWeekly").attr("checked", false);

        }

        function addCloseDate() {
            var ddlCloseDate = $("#ddlCloseDate");  //頻率
            var ddlCloseDateText = $('#ddlCloseDate :selected').text();
            var chk_close_BusinessWeekly = $("input[name*=chk_close_BusinessWeekly]:checked");  //每周
            var txtCloseMonthlyDay = $("#txtCloseMonthlyDay");  //每月
            var txtCloseBeginDate = $("#txtCloseBeginDate");  //特殊
            var txtCloseEndDate = $("#txtCloseEndDate");  //特殊
            var chk_close_BusinessWeekly = $("input[name*=chk_close_BusinessWeekly]:checked");
            var ddl_close_WeeklyBeginHour = $("#ddl_close_WeeklyBeginHour").val();
            var ddl_close_WeeklyBeginMinute = $("#ddl_close_WeeklyBeginMinute").val();
            var ddl_close_WeeklyEndHour = $("#ddl_close_WeeklyEndHour").val();
            var ddl_close_WeeklyEndMinute = $("#ddl_close_WeeklyEndMinute").val();
            var chk_close_allday = $("#chk_close_allday").is(':checked');

            var _frequency = GetCloseFrequency();
            switch (_frequency) {
                case frequency.weekly:
                    //週
                    var weeklys = chk_close_BusinessWeekly.map(function () {
                        return this.value;
                    }).get().join();

                    getWeeklyContinue(ddlCloseDateText, weeklys, ddl_close_WeeklyBeginHour + ":" + ddl_close_WeeklyBeginMinute, ddl_close_WeeklyEndHour + ":" + ddl_close_WeeklyEndMinute, chk_close_allday, "panCloseDate");
                    break;
                case frequency.monthly:
                    //月
                    var beginTime = ddl_close_WeeklyBeginHour + ":" + ddl_close_WeeklyBeginMinute;
                    var endTime = ddl_close_WeeklyEndHour + ":" + ddl_close_WeeklyEndMinute;
                    $("#panCloseDate").append('<div class="remove">' + ddlCloseDateText + " " + txtCloseMonthlyDay.val() + '日' + (chk_close_allday ? "全天" : beginTime + "~" + endTime) + '<a class="openclose" onclick="removeSettingTime(this);"></a></div>');
                    break;
                case frequency.special:
                    //特殊
                    var beginDate = txtCloseBeginDate.val();
                    var endDate = txtCloseEndDate.val();
                    var beginTime = ddl_close_WeeklyBeginHour + ":" + ddl_close_WeeklyBeginMinute;
                    var endTime = ddl_close_WeeklyEndHour + ":" + ddl_close_WeeklyEndMinute;
                    if ($.trim(beginDate) == "") {
                        alert("請輸入至少一個日期");
                        return false;
                    }
                    if ($.trim(beginDate) != "") {
                        if (!ValidateDate($.trim(beginDate))) {
                            alert("起始日期格式不正確");
                            return false;
                        }
                    }
                    if ($.trim(endDate) != "") {
                        if (!ValidateDate($.trim(endDate))) {
                            alert("結束日期格式不正確");
                            return false;
                        }
                    }
                    if (!CompareDate(beginDate, endDate)) {
                        alert("結束日期需晚於起始日期");
                        return false;
                    }
                    var paddingDatetime = "";
                    if ($.trim(endDate) != "") {
                        paddingDatetime = beginDate + "~" + endDate;
                    } else {
                        paddingDatetime = beginDate;
                    }
                    $("#panCloseDate").append('<div class="remove">' + paddingDatetime + " " + (chk_close_allday ? "全天" : beginTime + "~" + endTime) + '<a class="openclose" onclick="removeSettingTime(this);"></a></div>');
                    break;
                case frequency.alldayround:
                    //全年無休
                    $("#panCloseDate").append('<div class="remove"><%=LunchKingSite.Core.Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (LunchKingSite.Core.StoreCloseDate)LunchKingSite.Core.StoreCloseDate.AllYearRound)%><a class="openclose" onclick="removeSettingTime(this);"></a></div>');
                    break;
            }

            $("input[name*=chk_close_BusinessWeekly]:checked").removeAttr('checked');
            $("#ddlCloseDate").val("1");
            $("#txtCloseMonthlyDay").val("");
            $("#txtCloseBeginDate").val("");
            $("#txtCloseEndDate").val("");
            $("input[name*=chk_close_BusinessWeekly]:checked").removeAttr('checked');
            $("#ddl_close_WeeklyBeginHour").val("01");
            $("#ddl_close_WeeklyBeginMinute").val("00");
            $("#ddl_close_WeeklyEndHour").val("01");
            $("#ddl_close_WeeklyEndMinute").val("00");
            $("#chk_close_allday").removeAttr('checked');
            $("#ddl_close_WeeklyBeginHour").attr('disabled', false);
            $("#ddl_close_WeeklyBeginMinute").attr('disabled', false);
            $("#ddl_close_WeeklyEndHour").attr('disabled', false);
            $("#ddl_close_WeeklyEndMinute").attr('disabled', false);

            $("#chk_close_AllBusinessWeekly").attr("checked", false);

            HideOrShowCloseSpan();
        }

        function ValidateDate(dtValue) {
            var dtRegex = new RegExp(/\b(\d{4})([/])(0[1-9]|1[012])([/])(0[1-9]|[12][0-9]|3[01])\b/);
            return dtRegex.test(dtValue);
        }

        function CompareDate(sd, ed) {
            var d1 = new Date(sd);
            var d2 = new Date(ed);
            if (d2 <= d1) {
                return false;
            }
            return true;
        }

        function getWeeklyContinue(ofrenquency, weeklys, beginTime, endTime, allDay, obj) {
            if (weeklys == "") {
                alert("請先選擇週幾");
                return false;
            }
            $.ajax({
                type: "POST",
                url: "StoreContent.aspx/GetWeeklyName",
                data: "{'frenquency':'" + ofrenquency + "','weeklys':'" + weeklys + "','beginTime':'" + beginTime + "','endTime':'" + endTime + "','allDay':'" + allDay + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var c = response.d;
                    if (c != undefined) {
                        appendSettingTime(c, obj);
                    }
                }, error: function (error) {
                    console.log(error);
                }
            });
        }
        function appendSettingTime(c, obj) {
            $("#" + obj + "").append('<div class="remove">' + c + '<a class="openclose" onclick="removeSettingTime(this);"></a></div>');
        }

        function removeSettingTime(obj) {
            if ($("#txtStoreName").attr("disabled") == "disabled") {
                alert("無法刪除");
                return false;
            }
            obj.closest("div").remove();
        }

        /*
        * 公休日欄位顯示或隱藏
        */
        function HideOrShowCloseSpan() {
            var _weekly = false;
            var _monthly = false;
            var _special = false;
            var _time = false;

            var _frequency = GetCloseFrequency();
            switch (_frequency) {
                case frequency.weekly:
                    _weekly = true;
                    _monthly = false;
                    _special = false;
                    _time = true;
                    break;
                case frequency.monthly:
                    _weekly = false;
                    _monthly = true;
                    _special = false;
                    _time = true;
                    break;
                case frequency.special:
                    _weekly = false;
                    _monthly = false;
                    _special = true;
                    _time = true;
                    break;
                case frequency.alldayround:
                    _weekly = false;
                    _monthly = false;
                    _special = false;
                    _time = false;
                    break;
            }

            if (_weekly) {
                $("#panCloseWeekly").show();
            } else {
                $("#panCloseWeekly").hide();
            }
            if (_monthly) {
                $("#panCloseMonthly").show();
            } else {
                $("#panCloseMonthly").hide();
            }
            if (_special) {
                $("#panCloseSpecial").show();
            } else {
                $("#panCloseSpecial").hide();
            }
            if (_time) {
                $("#panCloseTime").show();
            } else {
                $("#panCloseTime").hide();
            }
        }


        function GetCloseFrequency() {
            var _frequency = "";
            switch ($("#ddlCloseDate").val()) {
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.EveryWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.SingleWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.BiWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.FirstWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.SecondWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.ThirdWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.ForthWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.FifthWeekly%>":
                    _frequency = frequency.weekly;
                    break;
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.EveryMonthly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.SingleMonthly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.BiMonthly%>":
                    _frequency = frequency.monthly;
                    break;
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.SpecialDate%>":
                    _frequency = frequency.special;
                    break;
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.AllYearRound%>":
                    _frequency = frequency.alldayround;
                    break;
            }
            return _frequency;
        }
        function checkAll(obj, div) {
            if ($(obj).is(":checked")) {
                $("#" + div + " > input[type=checkbox]").attr("checked", "checked");
            } else {
                $("#" + div + " > input[type=checkbox]").removeAttr("checked");
            }
        }

        function ShowText(type) {
            if (type == "Address") {
                $("label[id='lblAddress']").html($("#ddlCompanyCityId option:selected").text() + $("#ddlTownshipId option:selected").text() + $('#txtCompanyAddress').val());
            }
            else if (type == "Bank") {
                $("label[id='lblBank']").html($("#ddlBankCode option:selected").text());
            }
            else if (type == "BankBranch") {
                $("label[id='lblBankBranch']").html($("#ddlBranchCode option:selected").text());
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <div class="mc-content">
        <div id="basic">
            <h1 class="rd-smll">
                <span class="breadcrumb flat">
	                        <a href="javascript:void(0);"><asp:Literal ID="liTitle" runat="server"></asp:Literal></a>
	                        <a href="javascript:void(0);" >店鋪資料</a>
                            <a href="javascript:void(0);" >商家資料</a>
                            <a href="javascript:void(0);" class="active">商家管理</a>
                        </span>
                <asp:HyperLink ID="hkBackToSeller" runat="server" Text="回商家資訊" Style="color: #08C"></asp:HyperLink>
            </h1>
            <hr class="header_hr">
            <div class="grui-form">
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        狀態</label>
                    <div class="data-input rd-data-input">
                        <p style="color: #BF0000">
                            <asp:Literal ID="liTempStatus" runat="server"></asp:Literal>
                        </p>
                        <p>
                            <asp:Button ID="btnStoreApprove" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="申請核准" OnClick="btnStoreApprove_Click" Visible="false" />
                            <asp:Button ID="btnStoreReturned" runat="server" CssClass="btn btn-small form-inline-btn" Text="退回申請" OnClick="btnStoreReturned_Click" Visible="false" />
                            <asp:Button ID="btnStoreApply" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="申請變更" OnClick="btnStoreApply_Click" Visible="false" />
                        </p>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        商家資訊</label>
                    <div class="data-input rd-data-input">
                        <p>
                            <asp:Literal ID="liStoreName" runat="server"></asp:Literal>
                        </p>
                        <p>
                            <asp:HyperLink ID="hkSellerId" runat="server" Target="_blank"></asp:HyperLink>
                        </p>
                    </div>
                </div>
                <asp:Panel ID="divSalesInfo" runat="server" CssClass="form-unit" Visible="false">
                    <label class="unit-label rd-unit-label">
                        負責業務</label>
                    <div class="data-input rd-data-input">
                        <p>
                            <asp:Literal ID="liSalesName" runat="server"></asp:Literal>
                        </p>
                        <p>
                            <asp:Literal ID="liSalesEmail" runat="server"></asp:Literal>
                        </p>
                        <p>
                            <asp:Literal ID="liSalesDeptId" runat="server"></asp:Literal>
                        </p>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <asp:Panel ID="divMain" runat="server" DefaultButton="btnSave">
            <div id="sell">
                <h1 class="rd-smll">店鋪資訊</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;店名</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtStoreName" runat="server" CssClass="input-half" ClientIDMode="Static"></asp:TextBox>
                            <p id="pStoreName" class="if-error" style="display: none;">請輸入店名。</p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;負責人</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtBossName" runat="server" CssClass="input-small" Style="width: 100px" ClientIDMode="Static"></asp:TextBox>
                            <p id="pBossName" class="if-error" style="display: none;">請輸入負責人。</p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;訂位電話</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtPhone" runat="server" CssClass="input-half" ClientIDMode="Static"></asp:TextBox>
                            <p id="pPhone" class="if-error" style="display: none;">請輸入聯絡人電話。</p>
                            <p>
                                <asp:CheckBox ID="chkCreditCardAvailable" runat="server" Text="刷卡服務" />
                            </p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;公司地址</label>
                        <div class="data-input rd-data-input">
                            <span class="rd-c-br">
                                <asp:DropDownList ID="ddlCompanyCityId" runat="server" onchange="BindTownship(this);" Width="100px" ClientIDMode="Static" AppendDataBoundItems="true">
                                    <asp:ListItem Text="請選擇" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HiddenField ID="hidTownshipId" runat="server" ClientIDMode="Static" />
                                <asp:DropDownList ID="ddlTownshipId" runat="server" Width="100px" onchange="SetTownshipId(this);" ClientIDMode="Static">
                                </asp:DropDownList>
                            </span>
                            <asp:TextBox ID="txtCompanyAddress" runat="server" CssClass="input-half" ClientIDMode="Static"></asp:TextBox>
                            <label id="lblAddress"></label>
                            <p id="pAddress" class="if-error" style="display: none;">請選擇，並輸入完整的地址。</p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            交通資訊</label>
                        <div class="data-input rd-data-input">
                            <p>捷運</p>
                            <asp:TextBox ID="txtMrt" runat="server" CssClass="input-half"></asp:TextBox>
                            <br />
                            <p>開車</p>
                            <asp:TextBox ID="txtCar" runat="server" CssClass="input-half"></asp:TextBox>
                            <br />
                            <p>公車</p>
                            <asp:TextBox ID="txtBus" runat="server" CssClass="input-half"></asp:TextBox>
                            <br />
                            <p>其他</p>
                            <asp:TextBox ID="txtOV" runat="server" CssClass="input-half"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            網站</label>
                        <div class="data-input rd-data-input">
                            <p>官網</p>
                            <asp:TextBox ID="txtWebUrl" runat="server" CssClass="input-half"></asp:TextBox>
                            <br />
                            <p>FaceBook</p>
                            <asp:TextBox ID="txtFacebook" runat="server" CssClass="input-half"></asp:TextBox>
                            <br />
                            <p>部落格</p>
                            <asp:TextBox ID="txtBlog" runat="server" CssClass="input-half"></asp:TextBox>
                            <br />
                            <p>其他</p>
                            <asp:TextBox ID="txtOtherUrl" runat="server" CssClass="input-half"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            營業時間</label>
                        <div class="data-input rd-data-input">
                            <asp:CheckBox ID="chk_AllBusinessWeekly" runat="server" ClientIDMode="Static" />全選
                            <%--<asp:CheckBoxList ID="chk_BusinessWeekly" runat="server" RepeatColumns="7"></asp:CheckBoxList>--%>
                            <br />
                                <input type="checkbox" id="chk_BusinessWeekly0" name="chk_BusinessWeekly" value="0" />週一
                                <input type="checkbox" id="chk_BusinessWeekly1" name="chk_BusinessWeekly" value="1" />週二
                                <input type="checkbox" id="chk_BusinessWeekly2" name="chk_BusinessWeekly" value="2" />週三
                                <input type="checkbox" id="chk_BusinessWeekly3" name="chk_BusinessWeekly" value="3" />週四
                                <input type="checkbox" id="chk_BusinessWeekly4" name="chk_BusinessWeekly" value="4" />週五
                                <input type="checkbox" id="chk_BusinessWeekly5" name="chk_BusinessWeekly" value="5" />週六
                                <input type="checkbox" id="chk_BusinessWeekly6" name="chk_BusinessWeekly" value="6" />週日
                                <%--<asp:CheckBoxList ID="chk_BusinessWeekly" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="週一" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="週二" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="週三" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="週四" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="週五" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="週六" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="週日" Value="6"></asp:ListItem>
                                </asp:CheckBoxList>--%>
                                <br />


                            時間:<asp:DropDownList ID="ddl_WeeklyBeginHour" runat="server" ClientIDMode="Static" Width="70px"></asp:DropDownList>：
                            <asp:DropDownList ID="ddl_WeeklyBeginMinute" runat="server" ClientIDMode="Static" Width="70px"></asp:DropDownList>～
                            <asp:DropDownList ID="ddl_WeeklyEndHour" runat="server" ClientIDMode="Static" Width="70px"></asp:DropDownList>：
                            <asp:DropDownList ID="ddl_WeeklyEndMinute" runat="server" ClientIDMode="Static" Width="70px"></asp:DropDownList>
                            <div>
                                <input type="button" value="新增" onclick="addBusinessHour();" class="btn rd-mcacstbtn openclose" />
                            </div>
                            <div id="panBusinessHour" style="padding-top:5px;color:#BF0000">

                            </div>

                            <div id="divBusinessHour" style="display:none;width:500px;height:300px">
                                <asp:PlaceHolder ID="phBusinessStores" runat="server">
                                    <asp:Repeater ID="rptBStore" runat="server" OnItemDataBound="rptBStore_ItemDataBound">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAllStore" runat="server" ClientIDMode="Static" onclick="checkAll(this,'divBusinessStore')" />全選<br />
                                            <hr class="header_hr">
								        </HeaderTemplate>
								        <ItemTemplate>
                                            <div id="divBusinessStore">
                                            <asp:CheckBox ID="chkStore" runat="server" /> <%# ((Store)Container.DataItem).StoreName %>
									        <br />
                                            </div>
								        </ItemTemplate>
								        <FooterTemplate>
                                            <input type="button" id="btnSyncBusinessHour" value="確認同步" class="btn btn-small btn-primary form-inline-btn" onclick="syncBusinessHour()" />
								        </FooterTemplate>
                                    </asp:Repeater>
                                </asp:PlaceHolder>
                            </div>
                            <asp:HyperLink ID="syncBusinessHour" runat="server" ClientIDMode="Static" class="btn btn-small btn-primary form-inline-btn openclose" style="color:#fff">將營業時間同步到其他店鋪</asp:HyperLink>
                            <asp:TextBox ID="txtOpenTime" runat="server" CssClass="input-half" ClientIDMode="Static" TextMode="MultiLine" Width="80%" Height="60px"  style="display:none"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            公休日</label>
                        <div class="data-input rd-data-input">
                            頻率：<asp:DropDownList ID="ddlCloseDate" runat="server" ClientIDMode="Static"  Width="100px"></asp:DropDownList>
                            <div id="panCloseWeekly">
                                <asp:CheckBox ID="chk_close_AllBusinessWeekly" runat="server" ClientIDMode="Static" />全選
       
                                <br />
                                <input type="checkbox" id="chk_close_BusinessWeekly0" name="chk_close_BusinessWeekly" value="0" />週一
                                <input type="checkbox" id="chk_close_BusinessWeekly1" name="chk_close_BusinessWeekly" value="1" />週二
                                <input type="checkbox" id="chk_close_BusinessWeekly2" name="chk_close_BusinessWeekly" value="2" />週三
                                <input type="checkbox" id="chk_close_BusinessWeekly3" name="chk_close_BusinessWeekly" value="3" />週四
                                <input type="checkbox" id="chk_close_BusinessWeekly4" name="chk_close_BusinessWeekly" value="4" />週五
                                <input type="checkbox" id="chk_close_BusinessWeekly5" name="chk_close_BusinessWeekly" value="5" />週六
                                <input type="checkbox" id="chk_close_BusinessWeekly6" name="chk_close_BusinessWeekly" value="6" />週日
                                <%--<asp:CheckBoxList ID="chk_close_BusinessWeekly" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="週一" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="週二" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="週三" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="週四" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="週五" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="週六" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="週日" Value="6"></asp:ListItem>
                                </asp:CheckBoxList>--%>
                                <br />


                            </div>
                            <div id="panCloseMonthly">
                                日子：<asp:TextBox ID="txtCloseMonthlyDay" runat="server" placeholder="填入特定日(只寫數字，以、號隔開，如 「1、15」)" Width="300px" ClientIDMode="Static"></asp:TextBox>日
                            </div>
                            <div id="panCloseSpecial">
                                日期：<asp:TextBox ID="txtCloseBeginDate" runat="server" Width="100px" ClientIDMode="Static"></asp:TextBox>～<asp:TextBox ID="txtCloseEndDate" runat="server"  Width="100px" ClientIDMode="Static" ></asp:TextBox>
                            </div>
                            <div id="panCloseTime">
                            時間:<asp:DropDownList ID="ddl_close_WeeklyBeginHour" runat="server" ClientIDMode="Static" Width="70px"></asp:DropDownList>:
                                <asp:DropDownList ID="ddl_close_WeeklyBeginMinute" runat="server" ClientIDMode="Static" Width="70px"></asp:DropDownList>～
                                <asp:DropDownList ID="ddl_close_WeeklyEndHour" runat="server" ClientIDMode="Static" Width="70px"></asp:DropDownList>:
                                <asp:DropDownList ID="ddl_close_WeeklyEndMinute" runat="server" ClientIDMode="Static" Width="70px"></asp:DropDownList>
                                <asp:CheckBox ID="chk_close_allday" runat="server" ClientIDMode="Static" />全天<br />
                            </div>
                            <div>
                                <input type="button" value="新增" onclick="addCloseDate();" class="btn rd-mcacstbtn openclose" />
                            </div>
                            <div ID="panCloseDate" style="padding-top:5px;color:#BF0000">
                                   
                            </div>
                            <div id="divCloseDate" style="display:none;width:500px;height:300px">
                                <asp:PlaceHolder ID="phCloseStores" runat="server">
                                    <asp:Repeater ID="rptCStore" runat="server" OnItemDataBound="rptCStore_ItemDataBound">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAllStore" runat="server" ClientIDMode="Static" onclick="checkAll(this,'divCloseStore')"  />全選<br />
                                            <hr class="header_hr">
								        </HeaderTemplate>
								        <ItemTemplate>
                                            <div id="divCloseStore">
                                            <asp:CheckBox ID="chkStore" runat="server" /> <%# ((Store)Container.DataItem).StoreName %>
									        <br />
                                            </div>
								        </ItemTemplate>
								        <FooterTemplate>
                                            <input type="button" id="btnSyncCloseDate" value="確認同步" class="btn btn-small btn-primary form-inline-btn" onclick="syncCloseDate()" />
								        </FooterTemplate>
                                    </asp:Repeater>
                                </asp:PlaceHolder>
                            </div>
                            <asp:HyperLink ID="syncCloseDate" runat="server" ClientIDMode="Static" class="btn btn-small btn-primary form-inline-btn openclose" style="color:#fff">將公休日同步到其他店鋪</asp:HyperLink>
                            <asp:TextBox ID="txtCloseDate" runat="server" CssClass="input-half"  ClientIDMode="Static" style="display:none"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            備註</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtOthers" runat="server" TextMode="MultiLine" Width="80%" Height="60px"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div id="finance">
                <h1 class="rd-smll">匯款資訊</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            簽約公司名稱</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtCompanyName" runat="server" CssClass="input-half"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            統一編號/ID</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtSignCompanyID" runat="server" CssClass="input-half" onkeyup="return ValidateText($(this),value);" MaxLength="12"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            銀行代碼</label>
                        <div class="data-input rd-data-input">
                            <asp:DropDownList ID="ddlBankCode" ClientIDMode="Static" runat="server" Width="150px" onchange="BindBranchCode(this);" AppendDataBoundItems="true">
                                <asp:ListItem Text="請選擇" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                            <label id="lblBank"></label>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            分行代碼</label>
                        <div class="data-input rd-data-input">
                            <asp:HiddenField ID="hidBranchCode" runat="server" ClientIDMode="Static" />
                            <asp:DropDownList ID="ddlBranchCode" ClientIDMode="Static" runat="server" Width="200px" onchange="SetBranchId(this);">
                            </asp:DropDownList>
                            <label id="lblBankBranch"></label>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            受款人ID</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtCompanyID" runat="server" CssClass="input-half" onkeyup="return ValidateText($(this),value);" MaxLength="12"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            帳號</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtAccountId" runat="server" CssClass="input-half" onkeyup="return ValidateNumber($(this),value);" MaxLength="14"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            戶名</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtAccount" runat="server" CssClass="input-half"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            帳務聯絡人</label>
                        <div class="data-input rd-data-input">
                            <p>姓名</p>
                            <asp:TextBox ID="txtFinanceName" runat="server" CssClass="input-small" Style="width: 100px" ClientIDMode="Static"></asp:TextBox>
                            <asp:CheckBox ID="financeSync" runat="server" onclick="syncBossName(this);" Text="同負責人" ClientIDMode="Static" />
                            <br />
                            <p>電話</p>
                            <asp:TextBox ID="txtFinanceTel" runat="server" CssClass="input-half"></asp:TextBox>
                            <br />
                            <p>Email</p>
                            <asp:TextBox ID="txtFinanceEmail" runat="server" CssClass="input-half"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit end-unit">
                        <div class="data-input rd-data-input">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="儲存" OnClick="btnSave_Click" OnClientClick="return SaveCheck();" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <br />
        <asp:PlaceHolder ID="divChangeLog" runat="server">
            <div id="tracklog">
                <h1 class="rd-smll">變更紀錄</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <div class="data-input rd-data-input" style="margin-left: 70px">
                            <asp:TextBox ID="txtChangeLog" runat="server" CssClass="input-half" ClientIDMode="Static" Style="margin-top: 10px"></asp:TextBox>
                            <asp:Button ID="btnChangeLog" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="送出" OnClick="btnChangeLog_Click" OnClientClick="return CheckLog();" />
                            <p id="pChangeLog" class="if-error" style="display: none; margin-top: 8px">請輸入內容。</p>
                        </div>
                        <asp:Repeater ID="rptStoreChangeLog" runat="server">
                            <HeaderTemplate>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li>
                                    <label class="unit-label rd-unit-label">
                                        <%# ((StoreChangeLog)Container.DataItem).CreateId.Replace("@17life.com.tw", string.Empty).Replace("@17life.com", string.Empty) %>&nbsp;&nbsp;:
                                    </label>
                                    <div class="data-input rd-data-input">
                                        <p style="max-width: 520px;">
                                            <%# ((StoreChangeLog)Container.DataItem).ChangeLog.Replace("|", "<br />") %>
                                        </p>
                                        <p class="note" style="float: right;"><%# ((StoreChangeLog)Container.DataItem).CreateTime.ToString("yyyy/MM/dd HH:mm") %></p>
                                    </div>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="ProposalList.aspx.cs" Inherits="LunchKingSite.Web.Sal.ProposalList" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>

<%@ Register Src="../User/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Src="~/Sal/ProposalMenu.ascx" TagName="ProposalMenu" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css") %>" rel="stylesheet" type="text/css" />
    <style>
        .mc-navbar .mc-navbtn{
            width:10%;
        }
    </style>
    <script type="text/javascript" src="../Tools/js/jquery.blockUI.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            $('#txtOrderTimeS').datepicker({ dateFormat: 'yy/mm/dd' });
            $('#txtOrderTimeE').datepicker({ dateFormat: 'yy/mm/dd' });

            SalesEmailAutoComplete();

            var status = $('#ddlProposalStatus');
            if ($(status).val() != '0') {
                BindFlagList(status);
            }

            SetchkOrderTime('#ddlDealType')
            BindDealSubType($('#ddlDealType'));


            $("#dialog-CreateProposal").dialog({
                resizable: false,
                modal: true,
                autoOpen: false,
                title: "提示視窗",
                height: 200,
                width: 750,
                buttons: {
                    "旅遊宅配": function () {
                        $.ajax({
                            url: "ProposalList.aspx/CreateProposal",
                            data: "{'UserName':'<%=UserName%>','IsTravelProposal':'<%=true%>','IsPBeautyProposal':'<%=false%>','ProposalDeliveryType':'<%=(int)DeliveryType.ToHouse%>'}",
                            type: "POST",
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            error: function (e) {

                            },
                            success: function (s) {
                                document.location.href = "ProposalContent.aspx?pid=" + s.d;
                            }
                        });
                    },
                    "旅遊憑證": function () {
                        $.ajax({
                            url: "ProposalList.aspx/CreateProposal",
                            data: "{'UserName':'<%=UserName%>','IsTravelProposal':'<%=true%>','IsPBeautyProposal':'<%=false%>','ProposalDeliveryType':'<%=(int)DeliveryType.ToShop%>'}",
                            type: "POST",
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            error: function (e) {

                            },
                            success: function (s) {
                                document.location.href = "ProposalContent.aspx?pid=" + s.d;
                            }
                        });
                    },
                    "一般宅配": function () {
                        alert("此功能停用");
                    },
                    "玩美憑證": function () {
                        $.ajax({
                            url: "ProposalList.aspx/CreateProposal",
                            data: "{'UserName':'<%=UserName%>','IsTravelProposal':'<%=false%>','IsPBeautyProposal':'<%=true%>','ProposalDeliveryType':'<%=(int)DeliveryType.ToShop%>'}",
                            type: "POST",
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            error: function (e) {

                            },
                            success: function (s) {
                                document.location.href = "ProposalContent.aspx?pid=" + s.d;
                            }
                        });
                    },
                    "一般憑證": function () {
                        $.ajax({
                            url: "ProposalList.aspx/CreateProposal",
                            data: "{'UserName':'<%=UserName%>','IsTravelProposal':'<%=false%>','IsPBeautyProposal':'<%=false%>','ProposalDeliveryType':'<%=(int)DeliveryType.ToShop%>'}",
                            type: "POST",
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            error: function (e) {

                            },
                            success: function (s) {
                                document.location.href = "ProposalContent.aspx?pid=" + s.d;
                            }
                        });
                    }
                }
            });

            $("#<%=btnCreateProposal.ClientID%>").click(
                function () {
                    $("#dialog-CreateProposal").dialog('open');
                    return false;
                }
            );

            //勾選Status->flag全勾選
            $(".chkStatus").change(function () {
                var chkStatus = $(this);
                var chkFlag = $(this).parent().next().children();


                $(chkFlag).find('input:checkbox').each(function () {
                    if (chkStatus.is(":checked"))
                        $(this).prop("checked", "checked")
                    else
                        $(this).prop("checked", "")
                });

                $(".chkFlag").trigger("change");
            });

            //單勾選flag
            $(".chkFlag").change(function () {
                
                $("#hidStatusFlagResult").val("");
                $("#hidSavechkFlagResult").val("");

                if ($("#AdvanceSearch").is(':visible') == true) {


                    var ApplyFlag = 0, ApproveFlag = 0, CreatedFlag = 0, BusinessCheckFlag = 0, EditorFlag = 0, ListingFlag = 0;//紀錄勾選的flag
                    var ApplyTotal = 0, ApproveTotal = 0, CreatedTotal = 0, BusinessCheckTotal = 0, EditorTotal = 0, ListingTotal = 0;//紀錄有勾選的項目數量
                    var NoApplyTotal = 0, NoApproveTotal = 0, NoCreatedTotal = 0, NoBusinessCheckTotal = 0, NoEditorTotal = 0, NoListingTotal = 0;//紀錄沒有勾選的項目數量
                    $(".chkFlag").each(function () {

                        var status = $(this).parent().parent().prev().children().next().val();
                        var flag = $(this).next().val();

                        if ($(this).is(":checked")) {
                            
                            //為了postback後有所紀錄
                            $("#hidSavechkFlagResult").val($("#hidSavechkFlagResult").val() + "|" + status + ":" + flag);

                            //勾選flag連動status
                            if (status == '<%=(int)ProposalStatus.Apply%>') {
                                ApplyFlag += parseInt(flag);
                                ApplyTotal++;
                                //若flag已全勾則status要自動勾起
                                if (ApplyTotal == '<%=Enum.GetNames(typeof(ProposalApplyFlag)).Length  - 1%>')
                                    $(this).parent().parent().prev().children().eq(0).prop("checked", "checked");
                            }
                            else if (status == '<%=(int)ProposalStatus.Approve%>') {
                                ApproveFlag += parseInt(flag);
                                ApproveTotal++;
                                if (ApproveTotal == '<%=Enum.GetNames(typeof(ProposalApproveFlag)).Length  - 1%>')
                                    $(this).parent().parent().prev().children().eq(0).prop("checked", "checked");
                            }
                            else if (status == '<%=(int)ProposalStatus.Created%>') {
                                CreatedFlag += parseInt(flag);
                                CreatedTotal++;
                                if (CreatedTotal == '<%=Enum.GetNames(typeof(ProposalBusinessCreateFlag)).Length  - 1%>')
                                    $(this).parent().parent().prev().children().eq(0).prop("checked", "checked");
                            }
                            else if (status == '<%=(int)ProposalStatus.BusinessCheck%>') {
                                BusinessCheckFlag += parseInt(flag);
                                BusinessCheckTotal++;
                                if (BusinessCheckTotal == '<%=Enum.GetNames(typeof(ProposalBusinessFlag)).Length  - 1%>')
                                    $(this).parent().parent().prev().children().eq(0).prop("checked", "checked");
                            }
                            else if (status == '<%=(int)ProposalStatus.Editor%>') {
                                EditorFlag += parseInt(flag);
                                EditorTotal++;
                                if (EditorTotal == '<%=Enum.GetNames(typeof(ProposalEditorFlag)).Length  - 1%>')
                                    $(this).parent().parent().prev().children().eq(0).prop("checked", "checked");
                            }
                            else if (status == '<%=(int)ProposalStatus.Listing%>') {
                                ListingFlag += parseInt(flag);
                                ListingTotal++;
                                if (ListingTotal == '<%=Enum.GetNames(typeof(ProposalListingFlag)).Length  - 1%>')
                                    $(this).parent().parent().prev().children().eq(0).prop("checked", "checked");
                            }
                        }
                        else {

                            if (status == '<%=(int)ProposalStatus.Apply%>') {
                                NoApplyTotal++;
                                //若flag沒有全勾則status自動取消勾選
                                if (parseInt('<%=Enum.GetNames(typeof(ProposalApplyFlag)).Length  - 1%>') - NoApplyTotal != '<%=Enum.GetNames(typeof(ProposalApplyFlag)).Length  - 1%>')
                                    $(this).parent().parent().prev().children().eq(0).prop("checked", "");
                            }
                            else if (status == '<%=(int)ProposalStatus.Approve%>') {
                                NoApproveTotal++;
                                if (parseInt('<%=Enum.GetNames(typeof(ProposalApproveFlag)).Length  - 1%>') - NoApproveTotal != '<%=Enum.GetNames(typeof(ProposalApproveFlag)).Length  - 1%>')
                                    $(this).parent().parent().prev().children().eq(0).prop("checked", "");
                            }
                            else if (status == '<%=(int)ProposalStatus.Created%>') {
                                NoCreatedTotal++;
                                if (parseInt('<%=Enum.GetNames(typeof(ProposalBusinessCreateFlag)).Length  - 1%>') - NoCreatedTotal != '<%=Enum.GetNames(typeof(ProposalBusinessCreateFlag)).Length  - 1%>')
                                    $(this).parent().parent().prev().children().eq(0).prop("checked", "");
                            }
                            else if (status == '<%=(int)ProposalStatus.BusinessCheck%>') {
                                NoBusinessCheckTotal++;
                                if (parseInt('<%=Enum.GetNames(typeof(ProposalBusinessFlag)).Length  - 1%>') - NoBusinessCheckTotal != '<%=Enum.GetNames(typeof(ProposalBusinessFlag)).Length  - 1%>')
                                    $(this).parent().parent().prev().children().eq(0).prop("checked", "");
                            }
                            else if (status == '<%=(int)ProposalStatus.Editor%>') {
                                NoEditorTotal++;
                                if (parseInt('<%=Enum.GetNames(typeof(ProposalEditorFlag)).Length  - 1%>') - NoEditorTotal != '<%=Enum.GetNames(typeof(ProposalEditorFlag)).Length  - 1%>')
                                    $(this).parent().parent().prev().children().eq(0).prop("checked", "");

                            }
                            else if (status == '<%=(int)ProposalStatus.Listing%>') {
                                NoListingTotal++;
                                if (parseInt('<%=Enum.GetNames(typeof(ProposalListingFlag)).Length  - 1%>') - NoListingTotal != '<%=Enum.GetNames(typeof(ProposalListingFlag)).Length  - 1%>')
                                    $(this).parent().parent().prev().children().eq(0).prop("checked", "");
                            }
                        }
                    });

                    //記錄所有勾選flag的狀態,要帶入sql
                    var count = '<%=Enum.GetNames(typeof(ProposalStatus)).Length%>'
                    for (i = 0; i < count; i++) {
                        if (i == '<%=(int)ProposalStatus.Apply%>')
                            $("#hidStatusFlagResult").val($("#hidStatusFlagResult").val() + "|" + i + ":" + ApplyFlag);
                        else if (i == '<%=(int)ProposalStatus.Approve%>')
                            $("#hidStatusFlagResult").val($("#hidStatusFlagResult").val() + "|" + i + ":" + ApproveFlag);
                        else if (i == '<%=(int)ProposalStatus.Created%>')
                            $("#hidStatusFlagResult").val($("#hidStatusFlagResult").val() + "|" + i + ":" + CreatedFlag);
                        else if (i == '<%=(int)ProposalStatus.BusinessCheck%>')
                            $("#hidStatusFlagResult").val($("#hidStatusFlagResult").val() + "|" + i + ":" + BusinessCheckFlag);
                        else if (i == '<%=(int)ProposalStatus.Editor%>')
                            $("#hidStatusFlagResult").val($("#hidStatusFlagResult").val() + "|" + i + ":" + EditorFlag);
                        else if (i == '<%=(int)ProposalStatus.Listing%>')
                            $("#hidStatusFlagResult").val($("#hidStatusFlagResult").val() + "|" + i + ":" + ListingFlag);
                    }
                }
  
            });


            //顯示原先的展開、勾選狀態
            Maintainimgbtn();
            if (document.getElementById("<%=hidMaintainchkFlag.ClientID %>").value != "")
                MaintainchkFlag(document.getElementById("<%=hidMaintainchkFlag.ClientID %>").value);



        });

        <%--[負責業務]提示輸入選單--%>
        function SalesEmailAutoComplete() {
            $('#<%=txtSalesEmail.ClientID%>').autocomplete({
                source: []
            });

            $('#<%=txtSalesEmail.ClientID%>').on('input', function () {
                var theInput = $(this);
                if (theInput.val().length > 0) {
                    $.ajax({
                        url: "SellerList.aspx/GetSalesEmailAutoCompelte",
                        method: "POST",//1.9.0
                        contentType: "application/json",
                        data: JSON.stringify({
                            partEmail: $(this).val()
                        }),
                        success: function (data) {
                            theInput.autocomplete("option", "source", data.d);
                        }
                    });
                }
            });
        }

        //檢查負責業務Email
        function SalesEmailExist(obj) {
            $.ajax({
                url: "ProposalList.aspx/SalesEmailExist",
                method: "POST",//1.9.0
                contentType: "application/json",
                data: JSON.stringify({
                    email: $(obj).val()
                }),
                success: function (response) {
                    if (!response.d) 
                        alert("請輸入正確的負責業務Email！");
                }
            });
        }

        function SaveControl()
        {
            //紀錄進階搜尋展開狀態
            var hidMaintainimgbtn = document.getElementById("<%=hidMaintainimgbtn.ClientID %>");
            hidMaintainimgbtn.value = $("#imgbtnHideContent").attr("src");

            //紀錄勾選flag狀態
            var hidMaintainchkFlag = document.getElementById("<%=hidMaintainchkFlag.ClientID %>");
            hidMaintainchkFlag.value = $("#hidSavechkFlagResult").val();

        }

        function Maintainimgbtn() {
            
            var hidMaintainimgbtn = document.getElementById("<%=hidMaintainimgbtn.ClientID %>");
            if (hidMaintainimgbtn.value != "") {
                $("#imgbtnHideContent").attr('src', hidMaintainimgbtn.value);

                if (hidMaintainimgbtn.value == '../Themes/default/images/17Life/G2/ArrowUp.png') {
                    $("#AdvanceSearch").css('display', 'none');
                    $("#imgbtnHideContent").attr('src', '../Themes/default/images/17Life/G2/ArrowUp.png');
                    $("#OriginalSearch *").prop('disabled', '');
                }
                else {
                    $("#AdvanceSearch").css('display', 'block');
                    $("#imgbtnHideContent").attr('src', '../Themes/default/images/17Life/G2/ArrowDown.png');

                    $("#OriginalSearch *").prop('disabled', 'disabled');
                }
            }

        }

        function MaintainchkFlag(value)
        {
            $(".chkFlag").each(function () {
                var Nowstatus = $(this).parent().parent().prev().children().next().val();
                var Nowflag = $(this).next().val();

                var status = value.split("|");
                for (i = 0; i < status.length ; i++)
                {
                    var flag = status[i].split(":");
                    for (j = 0; j < flag.length; j++)
                    {
                        //若目前的flag有在value裡就勾選
                        if (Nowstatus == flag[0] && Nowflag == flag[1]) {
                            $(this).prop("checked", "checked");
                        }
                    }
                }
            });

            $(".chkFlag").trigger("change");

        }

        function BindFlagList(obj) {
            $.ajax({
                type: "POST",
                url: "ProposalList.aspx/GetFlagList",
                data: "{'p': '" + $(obj).val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var json = $.parseJSON(response.d);
                    var flags = $('#rdlFlags');
                    $(flags).html('');
                    $.each(json, function (index, item) {
                        $(flags).append('<input id="rdFlag' + index + '" type="radio" value="' + index + '" onchange="SetFlag(this);" name="flags"><label for="rdFlag' + index + '">' + item + '</label>');
                    });
                    if ($('#hidCheckFlag').val() == '') {
                        $('#hidCheckFlag').val($(flags).find('[type=radio]').eq(0).val());
                    }
                    $(flags).find('[type=radio]').each(function () {
                        if ($(this).val() == $('#hidCheckFlag').val()) {
                            $(this).attr('checked', 'checked');
                        }
                    });
                    if ($(flags).find('[type=radio]:checked').length == 0) {
                        var firstObj = $(flags).find('[type=radio]').eq(0);
                        firstObj.attr('checked', 'checked');
                        $('#hidCheckFlag').val(firstObj.val());
                    }

                    SetchkOrderTime(obj);
                        
                }
            });
        }

        function SetFlag(obj) {
            $('#hidCheckFlag').val($(obj).val());
        }

        function BindDealSubType(obj) {
            $.ajax({
                type: "POST",
                url: "ProposalContent.aspx/GetDealSubType",
                data: "{'id': '" + $(obj).val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var subType = $('#ddlDealSubType');
                    $(subType).find('option').remove();
                    $(subType).append($('<option></option>').attr('value', "").text("請選擇"));
                    $.each(response.d, function (index, item) {
                        $(subType).append($('<option></option>').attr('value', item.Key).text(item.Value));
                    });
                    $('#hidDealType').val($(obj).val());
                   

                    $(subType).val($('#hidDealSubType').val());
                    SetDealSubType(subType);
                }
            });
        }
        function SetDealSubType(obj) {
            $('#hidDealSubType').val($(obj).val());
        }

        function SetchkOrderTime(obj) {
            var a = $(obj).val();
            if ($(obj).val() == 3) {
                $('.chkNoOrderTime').show();
            }
            else {
                $('.chkNoOrderTime').hide();
            }
        }

        function HideContent(obj) {
            
            if ($("#AdvanceSearch").is(':visible') == true) {
                $("#AdvanceSearch").css('display', 'none');
                $(obj).attr('src', '../Themes/default/images/17Life/G2/ArrowUp.png');
                $("#OriginalSearch *").prop('disabled', '');

                //關閉時全部勾選取消
                $(".chkFlag").each(function () {
                    $(this).prop("checked", "").trigger("change");
                });
                $(".chkStatus").each(function () {
                    $(this).prop("checked", "");
                });
            }
            else {
                $("#AdvanceSearch").css('display', 'block');
                $(obj).attr('src', '../Themes/default/images/17Life/G2/ArrowDown.png');
                
                $("#OriginalSearch *").prop('disabled', 'disabled');

                //預設財務資料勾選
                $(".chkFlag").each(function () {
                    var status = $(this).parent().parent().prev().children().next().val();
                    var flag = $(this).next().val();
                    if(status == "<%=(int)ProposalStatus.Listing%>" && flag == "<%=(int)ProposalListingFlag.FinanceCheck%>")
                        $(this).prop("checked", "checked").trigger("change");
                });
            }
        }
        function confirmTransferProposal() {
            if (!confirm("確定要整批移轉提案單?")) {
                return false;
            }

            $.blockUI({
                message: "匯入中請稍後...",
                css: {
                    width: '20%',
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });

            return true;
        }
        function openBatchProposal() {
            window.open("/sal/proposal/batchcreateproposal");
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <uc2:ProposalMenu ID="ProposalMenu" runat="server" ></uc2:ProposalMenu>
    <div class="mc-content">
        <h1 class="rd-smll">提案單列表</h1>
        <hr class="header_hr">
        <asp:Panel ID="divSearch" runat="server" DefaultButton="btnSearch">
            <div class="grui-form">
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        單號</label>
                    <div class="data-input rd-data-input">
                        <asp:TextBox ID="txtProposalId" runat="server" CssClass="input-small" Width="200px"></asp:TextBox>
                        <p>
                            <label class="unit-label rd-unit-label">
                                Bid</label>
                            <asp:TextBox ID="txtBid" runat="server" CssClass="input-half" Width="200px"></asp:TextBox>
                        </p>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        品牌名稱</label>
                    <div class="data-input rd-data-input">
                        <asp:TextBox ID="txtBrandName" runat="server" CssClass="input-small" Width="200px"></asp:TextBox>
                        <p>
                            <label class="unit-label rd-unit-label">
                                商家名稱</label>
                            <asp:TextBox ID="txtSellerName" runat="server" CssClass="input-small" Width="200px"></asp:TextBox>
                        </p>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        提案內容</label>
                    <div class="data-input rd-data-input">
                        <asp:TextBox ID="txtDealContent" runat="server" CssClass="input-half" Width="200px"></asp:TextBox>
                        <p>
                            <label class="unit-label rd-unit-label">
                                標記</label>
                            <asp:DropDownList ID="ddlSpecialFlag" runat="server" CssClass="select-wauto" Width="200px"></asp:DropDownList>
                        </p>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        類型</label>
                    <div class="data-input rd-data-input">
                        <asp:DropDownList ID="ddlDeliveryType" runat="server" Width="230px" AppendDataBoundItems="true">
                            <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <p style="display:none">
                            <label class="unit-label rd-unit-label">
                                複製</label>
                            <asp:DropDownList ID="ddlProposalCopyType" runat="server" Width="200px" AppendDataBoundItems="true">
                                <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </p>
                        <p>
                            <label class="unit-label rd-unit-label">
                                提案來源</label>
                            <asp:DropDownList ID="ddlProposalCreatedType" runat="server" CssClass="select-wauto" Width="200px" AppendDataBoundItems="true">
                                <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </p>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        上檔日期</label>
                    <div class="data-input rd-data-input">
                        <asp:TextBox ID="txtOrderTimeS" ClientIDMode="Static" runat="server" Columns="8" />
                        ～
                        <asp:TextBox ID="txtOrderTimeE" ClientIDMode="Static" runat="server" Columns="8" />
                        <asp:CheckBox ID="chkIsOrderTimeSet" runat="server" Text="僅顯示已排檔" />
                        <p>
                            行銷/策展專案
                        </p>
                        <p>
                            <asp:TextBox ID="txtMarketingResource" runat="server"></asp:TextBox>
                        </p>
                    </div>
                </div>
               
                <asp:Panel ID="divSales" runat="server" CssClass="form-unit" Visible="false">
                    <label class="unit-label rd-unit-label">
                        負責業務</label>
                    <div class="data-input rd-data-input">
                        <asp:TextBox ID="txtSalesEmail" runat="server" CssClass="input-small" Width="200px" placeholder="請輸入Email" onchange="SalesEmailExist(this)"></asp:TextBox>
                        <asp:HiddenField ID="hidSalesId" runat="server" Visible="false"></asp:HiddenField>
                        <asp:PlaceHolder ID="divSalesDept" runat="server" Visible="false">
                            <p>
                                <label class="unit-label rd-unit-label">
                                    業務部門</label>
                                <asp:DropDownList ID="ddlDept" runat="server" CssClass="select-wauto" Width="200px" AppendDataBoundItems="true">
                                    <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </p>
                        </asp:PlaceHolder>
                    </div>
                </asp:Panel>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        提案類型</label>
                    <div class="data-input rd-data-input">
                         
                        <asp:DropDownList ID="ddlDealType" ClientIDMode="Static" runat="server" Width="100px" onchange="BindDealSubType(this);"></asp:DropDownList>
                          
                        <asp:DropDownList ID="ddlDealSubType" runat="server" ClientIDMode="Static" Width="100px" onchange="SetDealSubType(this);"></asp:DropDownList>
                        <asp:HiddenField ID="hidDealType" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hidDealSubType" runat="server" ClientIDMode="Static" />
                            
                        <p></p>
                        <p></p>
                        <p>
                            <label class="unit-label rd-unit-label">
                                檔次類型</label>
                            <asp:DropDownList ID="ddlDealType1" runat="server" CssClass="select-wauto" Width="200px" AppendDataBoundItems="true">
                                <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </p>
                    </div>
                </div>
                <div class="form-unit" style="display:none">
                    <label class="unit-label rd-unit-label">
                        母檔檔號</label>
                    <div class="data-input rd-data-input">
                        <asp:TextBox ID="txtUniqueId" runat="server" CssClass="input-small" Width="200px"></asp:TextBox>
  
                        <p>
                            <label class="unit-label rd-unit-label">
                                約拍狀態</label>
                            <asp:DropDownList ID="ddlProposalPhotographer" runat="server" CssClass="select-wauto" Width="100px" AppendDataBoundItems="true">
                                <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:CheckBox ID="chkFTPPhoto" runat="server" Text="舊照片"/>
                            <asp:CheckBox ID="chkSellerPhoto" runat="server" Text="店家提供照片"/>
                        </p>
                    </div>
                     
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        未檢核項目</label>
                    <div class="data-input rd-data-input" id="OriginalSearch">
                        <asp:DropDownList ID="ddlProposalStatus" runat="server" CssClass="select-wauto" Width="200px" AppendDataBoundItems="true" ClientIDMode="Static" onchange="BindFlagList(this);">
                            <asp:ListItem Text="請選擇" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <p style="padding-left: 20px;">
                            <asp:CheckBox ID="chkShowAppley" runat="server" Text="僅顯示已申請" Checked="false" Visible="false"/>
                            <asp:CheckBox ID="chkShowChecked" runat="server" Text="反查已完成的檢核項目" Checked="true"/>
                            <asp:CheckBox ID="chkNoOrderTime" runat="server" Text="尚未排檔的提案單" Checked="true" CssClass="chkNoOrderTime"/>
                        </p>
                        <asp:HiddenField ID="hidCheckFlag" runat="server" ClientIDMode="Static" />
                        <div id="rdlFlags">
                        </div>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        新宅配狀態</label>
                    <div class="data-input rd-data-input">
                        <asp:DropDownList ID="ddlNewHouseStatus" runat="server" CssClass="select-wauto" Width="200px" AppendDataBoundItems="true" ClientIDMode="Static" onchange="BindFlagList(this);">
                            <asp:ListItem Text="請選擇" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        <p style="padding-left: 20px;">
                            <asp:CheckBox ID="chkShowHouseCheck" runat="server" Text="反查已完成的檢核項目" Checked="true"/>
                        </p>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        上檔情況</label>
                    <div class="data-input rd-data-input">
                        <asp:DropDownList ID="ddlDealStatus" runat="server" CssClass="select-wauto" Width="200px" AppendDataBoundItems="true" ClientIDMode="Static">
                            <asp:ListItem Text="請選擇" Value="0"></asp:ListItem>
                            <asp:ListItem Text="上架銷售" Value="O"></asp:ListItem>
                            <asp:ListItem Text="暫停銷售" Value="S"></asp:ListItem>
                            <asp:ListItem Text="已結檔" Value="C"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <asp:HiddenField ID="hidMaintainimgbtn" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hidMaintainchkFlag" runat="server" ClientIDMode="Static" />
                <div class="form-unit">
                    <div class="data-input rd-data-input" style="margin-left:650px">

                        <asp:DropDownList ID="ddlChangePage" runat="server" OnSelectedIndexChanged="btnSearch_Click" ClientIDMode="Static" AutoPostBack="true" Width="100px">
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>30</asp:ListItem>
                            <asp:ListItem>40</asp:ListItem>
                            <asp:ListItem>50</asp:ListItem>
                            <asp:ListItem>100</asp:ListItem>
                            <asp:ListItem>200</asp:ListItem>
                            <asp:ListItem>500</asp:ListItem>
                        </asp:DropDownList>

                        <span style="color: #BF0000; font-weight: bold;">進階搜尋</span>
                            <asp:ImageButton ID="imgbtnHideContent" runat="server" ImageUrl="~/Themes/default/images/17Life/G2/ArrowUp.png" OnClientClick="HideContent(this); return false;" ClientIDMode="Static" />
              
                    </div>
                </div>
                <div class="form-unit">
                    <div class="data-input rd-data-input" id="AdvanceSearch" style="margin-left:50px;width:1000px;display:none">
                        <asp:Repeater ID="rptStatus" runat="server" OnItemDataBound="rptStatus_ItemDataBound">
                            <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <input id="chkStatus" type="checkbox" class="chkStatus" />
                                                <asp:Literal ID="liStatus" runat="server"></asp:Literal>
                                                <asp:HiddenField ID="hidStatus" runat="server" />
                                                
                                            </td>
                                            <td>
                                                <asp:Repeater ID="rptFlag" runat="server" OnItemDataBound="rptFlag_ItemDataBound">
                                                    <ItemTemplate>
                                                        <p>
                                                            <input id="chkFlag" type="checkbox" class="chkFlag" />
                                                            <asp:Literal ID="liFlag" runat="server"></asp:Literal>
                                                            <asp:HiddenField ID="hidFlag" runat="server" />
                                                        </p>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </table>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:HiddenField ID="hidStatusFlagResult" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="hidSavechkFlagResult" runat="server" ClientIDMode="Static" />
                        
                    </div>
                    <div class="form-unit end-unit">
                        <div class="data-input rd-data-input">
                            <asp:Button ID="btnSearch" runat="server" Text="搜尋" CssClass="btn rd-mcacstbtn" OnClick="btnSearch_Click" OnClientClick="SaveControl()" />                            
                            <asp:Button ID="btnCreateProposal" runat="server" Text="提案諮詢" CssClass="btn rd-mcacstbtn" Visible="false"/>
                            <asp:Button ID="btnBatchCreateProposal" runat="server" Text="增補宅配提案單" CssClass="btn rd-mcacstbtn" Visible="false" OnClientClick="openBatchProposal()" />
                            <div id="dialog-CreateProposal">請選擇您要諮詢的提案類型</div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </asp:Panel>
        <asp:PlaceHolder ID="divTransferProposal" runat="server" Visible="false">
            <div class="data-input rd-data-input">
                <label class="unit-label rd-unit-label">提案單批次轉換業務功能</label>
                <asp:Button ID="btnExport" CssClass="btn rd-mcacstbtn" runat="server" Text="匯出" OnClick="btnExport_Click" ClientIDMode="Static" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:FileUpload ID="FUImport" runat="server" />
                <asp:Button ID="btnImport" CssClass="btn rd-mcacstbtn" runat="server" Text="匯入" OnClick="btnImport_Click" OnClientClick="return confirmTransferProposal();" />
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="divProposalList" runat="server" Visible="false">
            <div id="mc-table">
                <asp:Repeater ID="rptProposal" runat="server" OnItemDataBound="rptProposal_ItemDataBound">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                            <tr class="rd-C-Hide">
                                <th class="OrderDate">單號
                                </th>
                                <th class="OrderSerial">規模
                                </th>
                                <th class="OrderSerial">商家名稱
                                </th>
                                <th class="OrderDate">類型
                                </th>
                                <th class="OrderName" style="width: 320px">提案內容
                                </th>
                                <th class="OrderExp">上檔日
                                </th>
                                <th class="OrderExp" style="width:10%">(母檔)檔號
                                </th>
                                <th class="OrderExp">業務
                                </th>
                                <th class="OrderCouponState">檢核狀態
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="mc-tableContentITEM">
                            <td class="OrderDate">
                                <span class="rd-Detailtitle">單號：</span>
                                <asp:HyperLink ID="hkProposalId" runat="server" Target="_blank"></asp:HyperLink>
                            </td>
                            <td class="OrderSerial">
                                <span class="rd-Detailtitle">規模：</span>
                                <asp:Literal ID="liSellerLevel" runat="server"></asp:Literal>
                            </td>
                            <td class="OrderSerial" style="max-width: 80px;text-align:left">
                                <asp:PlaceHolder ID="divFirstDeal" runat="server" Visible="false">
                                    <span style="color: #BF0000; border: solid 1px; padding: 5px 5px 5px 5px;">
                                        <asp:Literal ID="liFirstDeal" runat="server"></asp:Literal>
                                    </span>
                                    <br />
                                </asp:PlaceHolder>
                                <asp:HyperLink ID="hkSellerName" runat="server" Target="_blank"></asp:HyperLink>
                            </td>
                            <td class="OrderDate" style="text-align:left">
                                <asp:Label ID="lblDeliveryType" runat="server"></asp:Label><br />
                                <asp:Literal ID="liDealType" runat="server"></asp:Literal><br />
                                <asp:Literal ID="liDealSubType" runat="server"></asp:Literal>
                            </td>
                            <td class="OrderName" style="text-align:left;word-wrap: break-word;max-width:300px">
                                <asp:Literal ID="liProposalContent" runat="server"></asp:Literal>
                                <span style="color: #666; font-size: 12px;">
                                    <asp:Literal ID="liMarketAnalysis" runat="server"></asp:Literal>
                                </span>
                                <asp:Repeater ID="rptSpecialFlag" runat="server" OnItemDataBound="rptSpecialFlag_ItemDataBound">
                                    <HeaderTemplate>
                                        <span>
                                            <br />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblFlag" runat="server" Style="color: #BF0000; border: solid 1px; white-space: nowrap; margin: 2px; padding: 5px 5px 5px 5px; line-height: 30px;"></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </span>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                            <td class="OrderDate" style="text-align:left">
                                <span class="rd-Detailtitle">上檔日：</span>
                                <asp:Literal ID="liOrderTimeS" runat="server"></asp:Literal>
                            </td>
                            <td class="OrderDate" style="text-align:left">
                                <span class="rd-Detailtitle">(母檔)檔號：</span>
                                <asp:Literal ID="libUniqueId" runat="server"></asp:Literal>
                            </td>
                            <td class="OrderDate" style="text-align:left">
                                <span class="rd-Detailtitle">業務：</span>
                                <asp:Literal ID="liSalesName" runat="server"></asp:Literal>
                            </td>
                            <td class="OrderDate" style="text-align:left">
                                <span class="rd-Detailtitle">狀態：</span>
                                <asp:Label ID="lblStatus" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <uc1:Pager ID="ucPager" runat="server" PageSize="10" ongetcount="GetCount" onupdate="UpdateHandler"></uc1:Pager>
        </asp:PlaceHolder>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="BackendRecentDeals.aspx.cs" Inherits="LunchKingSite.Web.Sal.BackendRecentDeals" %>

<%@ Register Src="~/User/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Src="~/Sal/ProposalMenu.ascx" TagName="ProposalMenu" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link type="text/css" href="../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <link type="text/css" href="../Themes/default/images/17Life/G2/safelycode.css"
        rel="stylesheet" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <script src="../Tools/js/jquery.ui.datepicker-zh-TW.js" type="text/javascript"></script>
    <script type="text/javascript">
        var divSearch = {
            onChange: function (e) {
                if ($('#ddlChannelType').val() == '0') {
                    $('#ddlCityId').show();
                    $('#txtSearchDealName').attr('placeholder', '請輸入部分商品黑標，如：燒肉 吃到飽');
                    $('#<%=txtSearchDealName.ClientID%>').attr('placeholder', '請輸入部分商品黑標，如：燒肉 吃到飽');
                }
                else {
                    $('#ddlCityId').hide();
                    $('#txtSearchDealName').attr('placeholder', '輸入部分行銷標題內容，如：下午茶 輕食');
                    $('#<%=txtSearchDealName.ClientID%>').attr('placeholder', '輸入部分行銷標題內容，如：下午茶 輕食');
                }
                this.Channel = $('#ddlChannelType').val();
            },
            init: function () {
                $('#<%=txtBusinessStartTimeS.ClientID%>').datepicker({ dateFormat: 'yy/mm/dd' });
                $('#<%=txtBusinessStartTimeE.ClientID%>').datepicker({ dateFormat: 'yy/mm/dd' });
                $('#<%=txtBusinessDeliverTimeS.ClientID%>').datepicker({ dateFormat: 'yy/mm/dd' });
                $('#<%=txtBusinessDeliverTimeE.ClientID%>').datepicker({ dateFormat: 'yy/mm/dd' });
                $('#ddlChannelType').bind('change', $.proxy(this.onChange, this));
                $('#<%=txtSearchStoreName.ClientID%>').attr('placeholder', '可輸入部分店家名稱，如：王品');
                $('#<%=txtSearchDealName.ClientID%>').attr('placeholder', '請輸入部分商品黑標，如：燒肉 吃到飽');
                $('#<%=txtSalesName.ClientID%>').attr('placeholder', '請輸入中文姓名');
            }
        }

        var tableSort = {
            init: function () {
                $("#imgRecentDealDate").click(function () {
                    $("#<%=hdOrderField.ClientID%>").val("business_hour_order_time_s");
                    QueryData();
                });
                $("#imgDeliverDate").click(function () {
                    $("#<%=hdOrderField.ClientID%>").val("business_hour_deliver_time_s");
                    QueryData();
                });
                $("#imgPrice").click(function () {
                    $("#<%=hdOrderField.ClientID%>").val("business_price");
                    QueryData();
                });
                $("#imgContinuedQuantity").click(function () {
                    $("#<%=hdOrderField.ClientID%>").val("continued_quantity");
                    QueryData();
                });
                $("#imgQuantity").click(function () {
                    $("#<%=hdOrderField.ClientID%>").val("quantity");
                    QueryData();
                });
                $("#imgDealStartTime").click(function () {
                    $("#<%=hdOrderField.ClientID%>").val("deal_start_time");
                    QueryData();
                });


            },
            image: function () {
                var __orderby = $("#<%=hdOrderBy.ClientID%>").val();
                var __orderField = $("#<%=hdOrderField.ClientID%>").val();
                var __objId = "";
                var sortArray = [];
                var __img = "sort-down.png";

                if (__orderby == "asc") {
                    __img = "sort-up.png";
                } else {
                    __img = "sort-down.png";
                }

                switch (__orderField) {
                    case "business_hour_order_time_s":
                        __objId = "imgRecentDealDate";
                        break;
                    case "business_hour_deliver_time_s":
                        __objId = "imgDeliverDate";
                        break;
                    case "business_price":
                        __objId = "imgPrice";
                        break;
                    case "continued_quantity":
                        __objId = "imgContinuedQuantity";
                        break;
                    case "quantity":
                        __objId = "imgQuantity";
                        break;
                    case "deal_start_time":
                        __objId = "imgDealStartTime";
                        break;
                }

                sortArray.push("imgRecentDealDate");
                sortArray.push("imgDeliverDate");
                sortArray.push("imgPrice");
                sortArray.push("imgContinuedQuantity");
                sortArray.push("imgQuantity");
                sortArray.push("imgDealStartTime");

                //先把所有的圖示都換回預設圖示
                $.each(sortArray, function (index, value) {
                    $("#" + value + "").attr("src", "../Themes/default/images/17Life/G2/sort-initial.png");
                });
                //將要排序的圖示單獨換掉                
                $("#" + __objId + "").attr("src", "../Themes/default/images/17Life/G2/" + __img);
            }
        }
        $(document).ready(function () {
            if (document.getElementById('DivSearch')) {
                divSearch.init();

                $("#<%=hdDealType1.ClientID%>").val($("#<%=ddlDealType1.ClientID%>").val());
            }

            tableSort.init();
            //Table排序後要更換image
            tableSort.image();

            if ($('#ddlChannelType').val() == '0') {
                $('#ddlCityId').show();
            } else {
                $('#ddlCityId').hide();
            }
            //畫面重載時，要選取到「銷售分析類別」之前選取的項目
            SetDealType2();


        });

        function QueryData() {
            $("#<%=hidSortSearch.ClientID%>").click();
        }


        function ExpandSubDeals(obj) {
            var subDeals = $(obj).parent().parent().next('#SubDeals');
            while (subDeals.length != 0) {
                if (subDeals.css('display') == 'none') {
                    $(obj).removeClass('safelycode_m').addClass('safelycode_mp');
                    subDeals.show();
                } else {
                    subDeals.hide();
                    $(obj).removeClass('safelycode_mp').addClass('safelycode_m');
                }
                subDeals = $(subDeals).next('#SubDeals');
            }
        }

        function SetDealType2() {
            var ddlFirstNode = "請選擇子分類";
            var ddlMain = $('#<%=ddlDealType1.ClientID%>');
            var ddlSub = $('#<%=ddlDealType2.ClientID%>');
            var hdSub = $('#<%=hdDealType2.ClientID%>');

            $.ajax({
                type: "POST",
                url: "BackendRecentDeals.aspx/GetDealType2",
                data: "{parentId: " + ddlMain.val() + "}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    ddlSub.empty();
                    ddlSub.append($('<option></option>').val('0').html(ddlFirstNode));
                    $.each(cates, function (i, item) {
                        ddlSub.append($('<option></option>').val(item.CodeId).html(item.CodeName));
                    });


                    if (hdSub.val() != "") {
                        $('#<%=ddlDealType2.ClientID%> option[value=' + hdSub.val() + ']').attr('selected', 'selected');
                }
                    if ($("#<%=hdDealType1.ClientID%>").val() != $("#<%=ddlDealType1.ClientID%>").val()) {
                        hdSub.val('0');
                    }

                },
                error: function (xhr) {
                    alert(xhr.responseText);
                    ddlSub.empty();
                    ddlSub.append($('<option></option>').val('0').html(ddlFirstNode));
                }
            });
        }

        function SetDealType2Value() {
            $('#<%=hdDealType2.ClientID%>').val($('#<%=ddlDealType2.ClientID%> option:selected').val());
        }

        function ShowOrderID(show)
        {
            //var ShowOrderID = document.getElementsByClassName("ShowOrderID");
            //var AdhustWidth = document.getElementsByClassName("AdhustWidth");
            //for (var i = 0, length = ShowOrderID.length; i < length; i++) {
            //    if (show == "True") {
            //        ShowOrderID[i].style.display = '';
            //    }
            //    else {
            //        ShowOrderID[i].style.display = 'none';
            //    }
            //}

            //for (var i = 0, length = AdhustWidth.length; i < length; i++) {
            //    if (show == "True") {
            //        AdhustWidth[i].style.Width = '100px';
            //    }
            //    else {
            //        AdhustWidth[i].style.Width = 'auto';
            //    }
            //}
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <style>
        .mc-navbar .mc-navbtn{
            width:10%;
        }
    </style>
    <uc2:ProposalMenu ID="ProposalMenu" runat="server" ></uc2:ProposalMenu>
    <div class="mc-content">
        <asp:Panel ID="divMain" runat="server" DefaultButton="Search">
            <div id="DivSearch">
                <h1 class="rd-smll">過往好康查詢</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            上檔頻道</label>
                        <div class="data-input rd-data-input">
                            <asp:DropDownList ID="ddlChannelType" runat="server" ClientIDMode="Static" Width="100px"></asp:DropDownList>
                            <asp:DropDownList ID="ddlCityId" ClientIDMode="Static" runat="server" Width="100px"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            銷售分析類別</label>
                        <div class="data-input rd-data-input">
                            <asp:DropDownList ID="ddlDealType1" runat="server" onchange="SetDealType2()" Width="130px"></asp:DropDownList>
                            <asp:HiddenField ID="hdDealType1" runat="server" Value="" />
                            <asp:DropDownList ID="ddlDealType2" runat="server" onchange="SetDealType2Value()" Width="200px"></asp:DropDownList>
                            <asp:HiddenField ID="hdDealType2" runat="server" Value="" />
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            上檔日期</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtBusinessStartTimeS" runat="server"></asp:TextBox>
                            ～
                    <asp:TextBox ID="txtBusinessStartTimeE" runat="server"></asp:TextBox>
                            <asp:RadioButtonList ID="chkDealTime" runat="server">
                                <asp:ListItem Text="僅出現已結檔好康" Value="0"  />
                                <asp:ListItem Text="僅出現在線好康" Value="1"/>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            兌換/配送期間</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtBusinessDeliverTimeS" runat="server"></asp:TextBox>
                            ～<asp:TextBox ID="txtBusinessDeliverTimeE" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            店家名稱</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtSearchStoreName" runat="server" CssClass="searchtext input-half"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            業務姓名</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtSalesName" runat="server" CssClass="searchtext input-half"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            優惠內容</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtSearchDealName" runat="server" CssClass="searchtext input-half"></asp:TextBox>
                            <asp:HiddenField ID="hdOrderField" runat="server" Value="" />
                            <span style="display: none">
                                <asp:Button ID="hidSortSearch" runat="server" Text="Button" OnClick="hidSortSearch_Click" /></span>
                            <asp:HiddenField ID="hdOrderBy" runat="server" Value="asc" />
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                               檔號/Bid
                            </label>
                        <div class="data-input rd-data-input">
                            <asp:DropDownList ID="ddlBidFilter" runat="server" ClientIDMode="Static" Width="100px">
                                <asp:ListItem Text="檔號" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Bid" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtBid" runat="server" CssClass="searchtext input-half"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                               出貨類型
                            </label>
                        <div class="data-input rd-data-input">
                            <asp:DropDownList ID="ddlWms" runat="server" ClientIDMode="Static" Width="200px">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-unit end-unit">
                        <div class="data-input rd-data-input">
                            <asp:Button ID="Search" runat="server" Text="搜好康" OnClick="Search_Click" CssClass="btn rd-mcacstbtn" />
                            <p class="note">貼心小提示： 搜尋多個關鍵詞，請以「半形空白鍵」隔開，例如：胡同 燒肉</p>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="divResult" runat="server" Visible="false">
            <div id="mc-table">
                <asp:Repeater ID="repPponRecentDeal" runat="server" OnItemDataBound="RepPponRecentDeal_ItemDataBound"
                    Visible="False">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table" id="tbOrder">
                            <tr class="rd-C-Hide">
                                <th class="OrderName ShowOrderID" id="OrderIDTitle">單號</th>
                                <th class="OrderDate AdhustWidth" style="width:100px">好康期間<a href="javascript:void(0);"><img id="imgRecentDealDate" src="../Themes/default/images/17Life/G2/sort-initial.png" alt="排序" /></a></th>
                                <th class="OrderDate AdhustWidth" style="width:100px">兌換/配送<a href="javascript:void(0);"><img id="imgDeliverDate" src="../Themes/default/images/17Life/G2/sort-initial.png" alt="排序" /></a></th>
                                <th class="OrderSerial">店家名稱</th>
                                <th class="OrderName">優惠內容</th>
                                <th class="OrderDate">前台數量<a href="javascript:void(0);"><img id="imgContinuedQuantity" src="../Themes/default/images/17Life/G2/sort-initial.png" alt="排序" /></a></th>
                                <th class="OrderExp">實際銷售<a href="javascript:void(0);"><img id="imgQuantity" src="../Themes/default/images/17Life/G2/sort-initial.png" alt="排序" /></a></th>
                                <th class="OrderCouponState">營業額<a href="javascript:void(0);"><img id="imgPrice" src="../Themes/default/images/17Life/G2/sort-initial.png" alt="排序" /></a></th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="mc-tableContentITEM">
                            <td class="OrderName ShowOrderID" style="width: 100px" id="OrderIDContent">
                                <asp:HyperLink ID="hypOrderID" runat="server" Target="_blank"></asp:HyperLink>
                                <asp:ImageButton ID="btnOrderID" runat="server" ImageUrl="~/Themes/default/images/17Life/B5/b_search.jpg" Visible="false"/>
                            </td>
                            <td class="OrderDate">
                                <span class="rd-Detailtitle">好康期間：</span>
                                <div id="Expand" runat="server" class="safelycode_m" style="margin: -10px 0px -5px -25px; cursor: pointer;"
                                    onclick="ExpandSubDeals(this);">
                                </div>
                                <asp:Label ID="lblRecentDealDate" runat="server"></asp:Label></td>
                            <td class="OrderDate">
                                <span class="rd-Detailtitle">兌換/配送：</span>
                                <asp:Label ID="lblDeliverDate" runat="server"></asp:Label>
                            </td>
                            <td class="OrderSerial" style="width: 150px">
                                <asp:Label ID="lblSellerName" runat="server"></asp:Label>
                            </td>
                            <td class="OrderName" style="width: 250px">
                                <asp:HyperLink ID="hypItemName" runat="server" Target="_blank"></asp:HyperLink>
                            </td>
                            <td class="OrderDate">
                                <span class="rd-Detailtitle">前台數量：</span>
                                <asp:Label ID="lblContinuedQuantity" runat="server"></asp:Label>
                            </td>
                            <td class="OrderExp">
                                <span class="rd-Detailtitle">實際銷售：</span>
                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                            </td>
                            <td class="OrderDate">
                                <span class="rd-Detailtitle">營業額：</span>
                                <asp:Label ID="lblPrice" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <asp:Repeater ID="rptSubDeals" runat="server" OnItemDataBound="rptSubDeals_ItemDataBound">
                            <ItemTemplate>
                                <tr id="SubDeals" style="background-color: #FFFABF; display: none;" class="mc-tableContentITEM">
                                    <td class="OrderName ShowOrderID" id="OrderIDSubContent">
                                        <%--<asp:HyperLink ID="hypOrderID" runat="server" Target="_blank"></asp:HyperLink>--%>
                                        <asp:ImageButton ID="btnOrderID" runat="server" ImageUrl="~/Themes/default/images/17Life/B5/b_search.jpg" Visible="false"/>
                                    </td>
                                    <td class="OrderDate">
                                        <span class="rd-Detailtitle">好康期間：</span>
                                        -&nbsp;
                                    <asp:Label ID="lblRecentDealDate" runat="server"></asp:Label></td>
                                    <td class="OrderDate">
                                        <span class="rd-Detailtitle">兌換/配送：</span>
                                        <asp:Label ID="lblDeliverDate" runat="server"></asp:Label>
                                    </td>
                                    <td class="OrderSerial">
                                        <asp:Label ID="lblSellerName" runat="server"></asp:Label>
                                    </td>
                                    <td class="OrderName">
                                        <asp:HyperLink ID="hypEventName" runat="server" Target="_blank"></asp:HyperLink>
                                    </td>
                                    <td class="OrderDate">
                                        <span class="rd-Detailtitle">前台數量：</span>
                                        -
                                    </td>
                                    <td class="OrderExp">
                                        <span class="rd-Detailtitle">實際銷售：</span>
                                        <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                    </td>
                                    <td class="OrderDate">
                                        <span class="rd-Detailtitle">營業額：</span>
                                        <asp:Label ID="lblPrice" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <asp:Repeater ID="repPiinlifeRecentDeal" runat="server" OnItemDataBound="RepPiinlifeRecentDeal_ItemDataBound"
                    Visible="False">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                            <tr class="rd-C-Hide">
                                <th class="OrderDate">上檔日期<a href="javascript:void(0);"><img id="imgDealStartTime" src="../Themes/default/images/17Life/G2/sort-initial.png" alt="排序" /></a></th>
                                <th class="OrderSerial">店家名稱</th>
                                <th class="OrderName">優惠內容</th>
                                <th class="OrderName">商品名稱</th>
                                <th class="OrderExp">銷售數量</th>
                            </tr>
                        </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="mc-tableContentITEM">
                            <td class="OrderDate">
                                <span class="rd-Detailtitle">上檔日期：</span>
                                <asp:Label ID="lblRecentDealDate" runat="server"></asp:Label></td>
                            <td class="OrderSerial" style="width: 150px">
                                <asp:Label ID="lblSellerName" runat="server"></asp:Label>
                            </td>
                            <td class="OrderName" style="width: 250px">
                                <asp:HyperLink ID="hypEventName" runat="server" Target="_blank"></asp:HyperLink>
                            </td>
                            <td class="OrderName">
                                <asp:Label ID="lblProductName" runat="server"></asp:Label></td>
                            <td class="OrderExp">
                                <span class="rd-Detailtitle">銷售數量：</span>
                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </asp:Panel>
        <uc1:Pager ID="ucPager" runat="server" OnGetCount="RecentDealGetCount" OnUpdate="UpdateHandler"
            PageSize="15" Visible="false" />
    </div>
</asp:Content>

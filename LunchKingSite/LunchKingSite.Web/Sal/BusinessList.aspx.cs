﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.SellerSales;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Sal
{
    public partial class BusinessList : RolePage, ISalesBusinessListView
    {
        #region props
        private SalesBusinessListPresenter _presenter;
        public SalesBusinessListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public int? SalesId
        {
            get
            {
                if (!string.IsNullOrEmpty(txtSalesEmail.Text))
                {
                    ViewEmployee emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, txtSalesEmail.Text);
                    return emp.UserId;
                }
                else
                {
                    return null;
                }
            }
        }
        public string ItemName
        {
            get
            {
                return txtItemName.Text;
            }
        }

        public Guid Bid
        {
            get
            {
                Guid id = Guid.TryParse(txtBid.Text, out id) ? id : Guid.Empty;
                return id;
            }
        }

        public string SellerName
        {
            get { return txtSellerName.Text; }
        }

        public DateTime OrderTimeS
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtOrderTimeS.Text, out date);
                return date;
            }
        }

        public DateTime OrderTimeE
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtOrderTimeE.Text, out date);
                return date;
            }
        }

        public bool IsOrderTimeSet
        {
            get
            {
                return chkIsOrderTimeSet.Checked;
            }
        }

        public int PageSize
        {
            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }
        }

        public int CurrentPage
        {
            get
            {
                return ucPager.CurrentPage;
            }
        }
        public FileUpload FUExport
        {
            get
            {
                return FUImport;
            }
        }


        public int EmpUserId
        {
            get
            {
                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReadAll))
                {
                    return 0;
                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.CrossDeptTeam))
                {
                    return 0;
                }
                else
                {
                    return MemberFacade.GetUniqueId(UserName);
                }

            }
        }

        public string CrossDeptTeam
        {
            get
            {
                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReadAll))
                {
                    return "";
                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.CrossDeptTeam))
                {

                    ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(UserName));
                    if (!string.IsNullOrEmpty(emp.CrossDeptTeam))
                        return emp.CrossDeptTeam;
                    else if (emp.TeamNo == null)
                        return emp.DeptId + "[0]";
                    else
                        return emp.DeptId + "[" + emp.TeamNo + "]";
                }
                else
                {
                    return "";
                }

            }

        }
        #endregion

        #region event
        public event EventHandler Search;
        public event EventHandler Export;
        public event EventHandler Import;
        public event EventHandler<DataEventArgs<int>> OnGetCount;
        public event EventHandler<DataEventArgs<int>> PageChanged;
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Presenter.OnViewInitialized();
                EnabledSettings(UserName);
            }
            _presenter.OnViewLoaded();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.Search != null)
            {
                this.Search(this, e);
            }
            ucPager.ResolvePagerView(CurrentPage, true);
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (this.Export != null)
            {
                this.Export(this, e);
            }
        }
        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (this.Import != null)
            {
                this.Import(this, e);
            }
        }

        protected void rptBusiness_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<ViewPponDealCalendar, Proposal>)
            {
                KeyValuePair<ViewPponDealCalendar, Proposal> dataItem = (KeyValuePair<ViewPponDealCalendar, Proposal>)e.Item.DataItem;

                HyperLink hkProposalId = (HyperLink)e.Item.FindControl("hkProposalId");
                HyperLink hkSellerName = (HyperLink)e.Item.FindControl("hkSellerName");
                HyperLink hkItemName = (HyperLink)e.Item.FindControl("hkItemName");
                Literal liSalesName = (Literal)e.Item.FindControl("liSalesName");
                Label lblMultiDeal = (Label)e.Item.FindControl("lblMultiDeal");
                Literal liOrderTime = (Literal)e.Item.FindControl("liOrderTime");
                Literal liDealCatgegories = (Literal)e.Item.FindControl("liDealCatgegories");
                Label lblStatus = (Label)e.Item.FindControl("lblStatus");

                if (dataItem.Value.IsLoaded)
                {
                    if (dataItem.Value.ProposalSourceType == (int)ProposalSourceType.Original)
                    {
                        hkProposalId.NavigateUrl = ResolveUrl("~/sal/ProposalContent.aspx?pid=" + dataItem.Value.Id);
                    }
                    else
                    {
                        hkProposalId.NavigateUrl = ResolveUrl("~/sal/proposal/house/proposalcontent?pid=" + dataItem.Value.Id);
                    }
                    hkProposalId.Text = dataItem.Value.Id.ToString();
                }

                hkSellerName.NavigateUrl = ResolveUrl("~/sal/SellerContent.aspx?sid=" + dataItem.Key.SellerGuid);
                hkSellerName.Text = dataItem.Key.SellerName;

                hkItemName.Text = !string.IsNullOrWhiteSpace(dataItem.Key.ItemName) ? (dataItem.Key.ItemName.Length < 30 ? dataItem.Key.ItemName.Substring(0, dataItem.Key.ItemName.Length) : dataItem.Key.ItemName.Substring(0, 30) + "...") : "(無)";
                hkItemName.NavigateUrl = ResolveUrl("~/ppon/default.aspx?bid=" + dataItem.Key.BusinessHourGuid);

                ProposalSalesModel model = ProposalFacade.DealGetSalesByBid(dataItem.Key.BusinessHourGuid);
                liSalesName.Text = model.DevelopeSalesEmpName + "<br>" + model.OperationSalesEmpName;

                if (dataItem.Key.BusinessHourOrderTimeS.Date != DateTime.MaxValue.Date)
                {
                    liOrderTime.Text = dataItem.Key.BusinessHourOrderTimeS.ToString("yyyy/MM/dd ~ ") + dataItem.Key.BusinessHourOrderTimeE.ToString("yyyy/MM/dd");
                }

                List<string> rtn = PponFacade.CategoryNameGetListByBid(dataItem.Key.BusinessHourGuid, CategoryType.PponChannel);
                liDealCatgegories.Text = string.Join("/", rtn);

                if (dataItem.Key.BusinessHourOrderTimeS > DateTime.Now)
                {
                    if (dataItem.Value.IsLoaded && !Helper.IsFlagSet(dataItem.Value.BusinessCreateFlag, ProposalBusinessCreateFlag.SettingCheck))
                    {
                        lblStatus.Text = "未確認";
                        lblStatus.ForeColor = System.Drawing.Color.Orange;
                    }
                    else
                    {
                        if (dataItem.Key.BusinessHourOrderTimeS < DateTime.MaxValue.Date)
                        {
                            lblStatus.Text = "已排檔";
                            lblStatus.ForeColor = System.Drawing.Color.Brown;
                        }
                        else
                        {
                            lblStatus.Text = "待排檔";
                            lblStatus.ForeColor = System.Drawing.Color.Green;
                        }
                    }
                }
                else
                {
                    if (dataItem.Key.BusinessHourOrderTimeE < DateTime.Now)
                    {
                        lblStatus.Text = "已結檔";
                        lblStatus.ForeColor = System.Drawing.Color.Gray;
                    }
                    else
                    {
                        lblStatus.Text = "搶購中";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                    }
                }
            }
        }

        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChanged != null)
            {
                this.PageChanged(this, new DataEventArgs<int>(pageNumber));
            }
        }

        protected int GetCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetCount != null)
            {
                OnGetCount(this, e);
            }
            return e.Data;
        }
        #endregion

        #region method
        public void SetBusinessList(Dictionary<ViewPponDealCalendar, Proposal> dataList)
        {
            //批次匯入匯出提案單
            if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.TransferDeal))
            {
                divTransferDeal.Visible = true;
            }
            divBusinessList.Visible = true;
            rptBusiness.DataSource = dataList;
            rptBusiness.DataBind();
        }

        private void EnabledSettings(string username)
        {
            ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(username));
            if (emp.IsLoaded)
            {
                if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.ReadAll))
                {
                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.CrossDeptTeam))
                {
                    #region 本區檢視
                    if (string.IsNullOrEmpty(emp.CrossDeptTeam) && emp.TeamNo == null)
                    {
                        ShowMessage("身份權限未明，無法查看資料，請洽產品或技術人員協助");
                    }
                    else
                    {

                    }

                    #endregion
                }
                else
                {
                }
            }
            else
            {
                ShowMessage("查無員工資料，請重新檢視員工資料維護設定。");
            }

        }

        public void ShowMessage(string msg)
        {
            string script = string.Format("alert('{0}');", msg);
            Page.ClientScript.RegisterStartupScript(GetType(), "alert", script, true);
        }
        #endregion

    }
}
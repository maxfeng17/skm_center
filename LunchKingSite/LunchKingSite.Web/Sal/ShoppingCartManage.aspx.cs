﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Sal
{
    public partial class ShoppingCartManage : RolePage, IShoppingCartManageView
    {
        #region props
        private ShoppingCartManagePresenter _presenter;
        public ShoppingCartManagePresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string SearchName
        {
            get
            {
                return txtName.Text.Trim();
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }
        #endregion

        #region event
        public event EventHandler Search;
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.Search != null)
            {
                this.Search(this, e);
            }
        }
        protected void rptShoppingCart_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ShoppingCartManageList)
            {
                ShoppingCartManageList dataItem = (ShoppingCartManageList)e.Item.DataItem;
                CheckBox chkFreight = (CheckBox)e.Item.FindControl("chkFreight");
                Label litId = (Label)e.Item.FindControl("litId");
                chkFreight.InputAttributes.Add("value", litId.Text);
            }
        }
        #endregion

        #region method
        public void SetShoppintCartData(List<ShoppingCartManageList> items)
        {
            rptShoppingCart.DataSource = items;
            rptShoppingCart.DataBind();
        }
        #endregion

        #region webmethod
        [WebMethod]
        public static dynamic ShoppingCartFreightsCheck(string ids, string UserName)
        {
            bool IsSuccess = false;
            string Message = "";
            try
            {
                List<int> data = new JsonSerializer().Deserialize<List<int>>(ids);
                foreach (int id in data)
                {
                    ShoppingCartFreight scfi = SellerFacade.ShoppingCartFreightsGet(id);
                    if (scfi.IsLoaded)
                    {
                        scfi.FreightsStatus = (int)ShoppingCartFreightStatus.Approve;
                        SellerFacade.ShoppingCartFreightsSet(scfi);
                        SellerFacade.ShoppingCartFreightsLogSet(UserName, scfi.Id, "【購物車審核確認】" + scfi.Id);

                        SellerFacade.MailShoppingCartToSeller(scfi.SellerGuid, scfi.FreightsName);
                    }
                }
                IsSuccess = true;
                Message = "已成功審核" + data.Count + "台購物車!";
            }
            catch
            {
                IsSuccess = false;
                Message = "已成功審核0台購物車!";
            }

            return new JsonSerializer().Serialize(new { IsSuccess = IsSuccess, Message = Message });
        }
        #endregion
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="SalesCalendarContent.aspx.cs" Inherits="LunchKingSite.Web.Sal.SalesCalendarContent" %>


<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>

<%@ Register Src="../User/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/fancybox/jquery.fancybox.css") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Tools/js/fancybox/jquery.fancybox.js"></script>
    <script type="text/javascript" src="../Tools/js/moment.js"></script>
    <link href="<%=ResolveUrl("~/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css") %>" rel="stylesheet" type="text/css" />
    <style>
        .fancybox-inner {
            overflow: visible !important;
        }
    </style>

    <script type="text/javascript" src="<%=ResolveUrl("~/Tools/js/sales.calendar.js") %>"></script>

    <script type="text/javascript">
        var Sales;
        var CLIENT_ID = '<%=SaleGoogleCalendarAPIKey%>';
        $(function () {
            $("#btnCreateCalendar").fancybox();

            $('#txtBeginDate').datepicker({
                dateFormat: 'yy/mm/dd',
                onSelect: function (date) {
                    $("#txtWakeUpDateS").attr('value', date);
                }
            });
            $('#txtBeginDate').blur(function () {
                $("#txtWakeUpDateS").attr('value', $(this).val());
            });
            $('#txtEndDate').datepicker({ dateFormat: 'yy/mm/dd' });
            $('#txtEndson_count_input').datepicker({ dateFormat: 'yy/mm/dd' });
            $("div.fancybox-inner input#txtEndson_until_input").datepicker({ dateFormat: 'yy/mm/dd' });



            $("#PopupLink").fancybox();

            $("#btnSavePopup").live("click", function () {
                var message = updatePopupData();
                if (message != "") {
                    alert(message);
                    return false;
                }
                $.fancybox.close();
            });

            $('#txtSellerName').keyup(function () {
                $("#txtSellerId").val("");
            });

            $('#txtSellerName').autocomplete({
                source: function (request, response) {
                    $.ajax(
                    {
                        type: "POST",
                        url: "SalesCalendarContent.aspx/GetSellerName",
                        data: "{ 'sellerName': '" + request.term + "','UserName':'<%=UserName%>'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.Label,
                                    value: item.Value
                                }
                            }))
                        }, error: function (res) {
                            console.log(res);
                        }
                    });
                },
                select: function (event, ui) {
                    event.preventDefault();
                    $("#txtSellerName").val(ui.item.label);
                    $("#txtSellerId").val(ui.item.value);
                    SellerContactGet(ui.item.value);
                }
            });

            $('#txtContact').autocomplete({
                source: function (request, response) {
                    $.ajax(
                    {
                        type: "POST",
                        url: "SalesCalendarContent.aspx/GetContactame",
                        data: "{ 'sellerId': '" + $("#txtSellerId").val() + "','ContactName':'" + request.term + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.Label,
                                    value: item.Value
                                }
                            }))
                        }, error: function (res) {
                            console.log(res);
                        }
                    });
                },
                select: function (event, ui) {
                    event.preventDefault();
                    $("#txtContact").val(ui.item.label);
                }
            });
        });

        jQuery(window).load(function () {
            Sales.Calendar.GetSalesCalenderID();
        });

        function SellerContactGet(sellerId) {
            $.ajax(
                    {
                        type: "POST",
                        url: "SalesCalendarContent.aspx/GetSellerContactName",
                        data: "{ 'sellerId': '" + sellerId + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            $("#txtContact").val(data.d);
                        }, error: function (res) {
                            console.log(res);
                        }
                    });
        }


        function Save() {
            
            var SalesCalenderId = $("#CalenderId").val();
            if (SalesCalenderId == undefined || $.trim(SalesCalenderId) == "") {
                alert("取得日曆資料失敗，請聯絡管理員!");
                return false;
            }
            var content = $("#txtCalendarContent").val();
            var loc = $("#txtLocation").val();

            var chkWholeDay = $("#chkWholeDay").is(":checked"); //全天

            if (($("#txtBeginDate").val() == "" && $("#txtEndDate").val() != "") || ($("#txtBeginDate").val() != "" && $("#txtEndDate").val() == "")) {
                alert("請填寫完整起訖日期!");
                return false;
            }
            if ($("#txtBeginDate").val() != "") {
                if (!isValidDate($("#txtBeginDate").val())) {
                    alert("日期格式不正確!");
                    return false;
                }
            }
            if ($("#txtEndDate").val() != "") {
                if (!isValidDate($("#txtEndDate").val())) {
                    alert("日期格式不正確!");
                    return false;
                }
            }

            var dateS = formatLocalDate($("#txtBeginDate").val() + ' ' + (chkWholeDay ? "00:00" : $("#ddlBeginTime").val()));
            var dateE = formatLocalDate($("#txtEndDate").val() + ' ' + (chkWholeDay ? "24:00" : $("#ddlEndTime").val()));

            var isWholeDay = $("#chkWholeDay").is(":checked");
            var isRepeat = $("#chkRepeat").is(":checked");

            var sellerName = $("#txtSellerName").val();
            var contact = $("#txtContact").val();

            var eventType = $("#ddlSellerEventType").val();

            var IsWakeUp = $("#chkWakeUp").is(":checked");
            var Minutes = $("#txtMinutes").val();
            var Cycle = $("#ddlCycle").val();

            var memo = $("#txtMemo").val();


            var frequencyData = $("#hidRepeat").val();
            var fData = {
                freq: 1,
                interval: 1,
                week: 1,
                wakeup_date_s: dateS,
                endson: 1,
                endson_data: 1
            };

            if (frequencyData != "") {
                fData = JSON.parse(frequencyData);
            }

            if (!IsWakeUp) {
                Minutes = 0;
                Cycle = 0;
            }

            if ($.trim(content) == "") {
                alert("請輸入內容");
                return false;
            }
            //if ($.trim($("#txtBeginDate").val()) == "") {
            //    alert("請輸入起始日");
            //    return false;
            //}
            //if ($.trim($("#txtEndDate").val()) == "") {
            //    alert("請輸入結束");
            //    return false;
            //}
            if ($.trim(sellerName) == "") {
                alert("請輸入商家名稱");
                return false;
            }
            if ($.trim(contact) == "") {
                alert("請輸入聯絡人");
                return false;
            }

            if (IsWakeUp) {
                if ($.trim(Minutes) == "" || isNaN(Minutes)) {
                    alert("請輸入提醒時間");
                    return false;
                }
            }
            
            var isSyncSellerNote = ($("input[name$='radSyncSellerNote']:checked").val() == "Y" ? true : false);
            var DevelopStatus = $("#ddlDevelopStatus").val();
            var SellerGrade = $("#ddlSellerGrade").val();
            var SellerId = $("#txtSellerId").val();

            if ($("#txtSellerId").val() == "" && isSyncSellerNote) {
                alert("「商家名稱」透過下拉選單選擇，才能寫入經營紀錄。");
                return false;
            }

            var dbData = {
                'id': $("#hidId").val(),
                'content': content,
                'dateS': dateS,
                'dateE': dateE,
                'isWholeDay': isWholeDay,
                'isRepeat': isRepeat,
                'location': loc,
                'sellerName': sellerName,
                'eventType': eventType,
                'contact': contact,
                'isWakeUp': IsWakeUp,
                'wakeupMinutes': Minutes,
                'wakeupCycle': Cycle,
                'memo': memo,
                'freq': fData.freq,
                'interval': fData.interval,
                'week': fData.week,
                'wakeup_date_s': fData.wakeup_date_s,
                'endson': fData.endson,
                'endson_data': fData.endson_data,
                'UserName': '<%=UserName%>',
                'isSyncSellerNote': isSyncSellerNote,
                'DevelopStatus': DevelopStatus,
                'SellerGrade': SellerGrade,
                'SellerId': SellerId
            };

            var cid = Sales.Calendar.EventDBSave(SalesCalenderId, "<%=Guid.NewGuid()%>", dbData);
        }

        function endsonEvnet(obj) {
            switch ($(obj).val()) {
                case "0":
                    $("input#txtdhEndson_count_input").attr("readonly", "readonly");
                    $("input#txtEndson_until_input").attr("readonly", "readonly");
                    $("input#txtdhEndson_count_input").val("");
                    $("input#txtEndson_until_input").val("");
                    break;
                case "1":
                    $("input#txtdhEndson_count_input").removeProp("readonly");
                    $("input#txtEndson_until_input").attr("readonly", "readonly");
                    $("input#txtEndson_until_input").val("");
                    break;
                case "2":
                    $("input#txtdhEndson_count_input").attr("readonly", "readonly");
                    $("input#txtdhEndson_count_input").val("");
                    $("input#txtEndson_until_input").prop("readonly", false);
                    $('input#txtEndson_until_input').val((new Date()).format("yyyy/MM/dd"));
                    break;
            }
        }

        /*
        * 格式化日期
        */
        function formatLocalDate(date) {
            var d = moment(date);
            return d.format();
        }

        function WholeDay(obj) {
            if ($(obj).is(":checked")) {
                $("#ddlBeginTime").hide();
                $("#ddlEndTime").hide();
            } else {
                $("#ddlBeginTime").show();
                $("#ddlEndTime").show();
            }
        }

        function openPopup() {
            var isChk = $("#chkRepeat").is(":checked");
            var repeat = $("#hidRepeat").val();
            if (isChk) {
                $.fancybox($("#divCreateCalendar").html(), {
                    minWidth: 650,
                    minHeight: 400,
                    autoScale: true,
                    transitionIn: 'none',
                    transitionOut: 'none',
                    modal: true,
                    hideOnContentClick: false,
                    beforeLoad: function () {
                    },
                    afterShow: function () {
                        if (repeat != "") {
                            var data = JSON.parse(repeat);
                            $("div.fancybox-inner input#hidFrequency").val(data.freq);
                            $("div.fancybox-inner select#ddlFrequency").val(data.freq);
                            $("div.fancybox-inner input#hidInterval").val(data.interval);
                            $("div.fancybox-inner select#ddlInterval").val(data.interval);

                            var weekData = data.week.split(",");
                            if (weekData[0] == "Y") {
                                $("input#chkWeek0").attr("checked", "checked");
                            }
                            if (weekData[1] == "Y") {
                                $("input#chkWeek1").attr("checked", "checked");
                            }
                            if (weekData[2] == "Y") {
                                $("input#chkWeek2").attr("checked", "checked");
                            }
                            if (weekData[3] == "Y") {
                                $("input#chkWeek3").attr("checked", "checked");
                            }
                            if (weekData[4] == "Y") {
                                $("input#chkWeek4").attr("checked", "checked");
                            }
                            if (weekData[5] == "Y") {
                                $("input#chkWeek5").attr("checked", "checked");
                            }
                            if (weekData[6] == "Y") {
                                $("input#chkWeek6").attr("checked", "checked");
                            }
                            if (weekData[7] == "Y") {
                                $("input#chkWeek7").attr("checked", "checked");
                            }

                            var endson = data.endson;
                            $("input[name='endson'][value='" + endson + "']").attr('checked', true);

                            switch (endson) {
                                case "0":
                                    break;
                                case "1":
                                    $("div.fancybox-inner input#txtdhEndson_count_input").val(data.endson_data);
                                    break;
                                case "2":
                                    $("div.fancybox-inner input#txtEndson_until_input").val(data.endson_data);
                                    break;
                            }


                            //endson
                            //endson_data
                        }
                    }, afterClose: function () {
                        if ($.trim($("#hidRepeat").val()) != "") {
                            $("#spanRepeat").html("<a href='javascript:void(0)' onclick='openPopup()'>編輯</a>");
                        }
                    }
                });
            }
        }

        /*
        * Save FancyBox data
        */
        function savePopup() {
            var message = updatePopupData();
            if (message != "") {
                alert(message);
                return false;
            }
            $.fancybox.close();
        }
        /*
        * Close Fancybox
        */
        function closePopup() {
            $.fancybox.close();
        }

        function updatePopupData() {
            var message = "";

            if ($("input#hidFrequency").val() == "") {
                $("input#hidFrequency").val($("#ddlFrequency").val());
            }
            if ($("input#hidInterval").val() == "") {
                $("input#hidInterval").val($("#ddlInterval").val());
            }

            var freq = $("input#hidFrequency").val();
            var interval = $("input#hidInterval").val();

            var Week0 = $("input#chkWeek0").is(":checked");
            var Week1 = $("input#chkWeek1").is(":checked");
            var Week2 = $("input#chkWeek2").is(":checked");
            var Week3 = $("input#chkWeek3").is(":checked");
            var Week4 = $("input#chkWeek4").is(":checked");
            var Week5 = $("input#chkWeek5").is(":checked");
            var Week6 = $("input#chkWeek6").is(":checked");

            var week = (Week0 ? 'Y' : 'N') + "," + (Week1 ? 'Y' : 'N') + "," + (Week2 ? 'Y' : 'N') + "," + (Week3 ? 'Y' : 'N') + "," + (Week4 ? 'Y' : 'N') + "," + (Week5 ? 'Y' : 'N') + "," + (Week6 ? 'Y' : 'N');

            var wakeup_date_s = $("input#txtWakeUpDateS").val();
            var endson = $("div.fancybox-inner input[name='endson']:checked").val();
            if (endson == undefined) {
                message = "請選擇「結束日期」選項";
            }
            var endson_data = "";
            switch (endson) {
                case "0":
                    break;
                case "1":
                    endson_data = $("div.fancybox-inner input#txtdhEndson_count_input").val();
                    break;
                case "2":
                    endson_data = $("div.fancybox-inner input#txtEndson_until_input").val();
                    break;
            }

            var repeatData = "";

            var repeatJson = {
                freq: freq,
                interval: interval,
                week: week,
                wakeup_date_s: wakeup_date_s,
                endson: endson,
                endson_data: endson_data
            };
            $("#hidRepeat").val(JSON.stringify(repeatJson));

            return message;
        }

        /*
        * 變更顯示頻率
        */
        function changeWeekFrequency(obj) {
            var $this = $(obj);
            $(".Week").show();
            $(".interval").show();
            $(".domrepeat").hide();
            $("input#hidFrequency").val($this.val());
            switch ($this.val()) {
                case "0":
                    //每天
                case "6":
                    //每年
                    document.getElementById("spanInterval").textContent = "天";
                    $(".Week").hide();
                    break;
                case "1":
                case "2":
                case "3":
                    $(".Week").hide();
                    $(".interval").hide();
                    break;
                case "4":
                    //每週
                    $("#spanInterval").html("週");
                    break;
                case "5":
                    //每月
                    $(".domrepeat").show();
                    $(".Week").hide();
                    break;
            }
        }

        function changeInterval(obj) {
            var $this = $(obj);
            $("input#hidInterval").val($this.val());
        }

        /*
        * 檢核日期格式
        */
        function isValidDate(dateStr) {

            if (dateStr == null || dateStr == '') return false;
            var parts = dateStr.split('/');
            if (parts.length != 3) return false;
            if (parts[0] > 9999 || parts[0] < 1753)
                return false;
            var accDate = new Date(dateStr);
            if (parseFloat(parts[0]) == accDate.getFullYear()
                && parseFloat(parts[1]) == accDate.getMonth() + 1
                && parseFloat(parts[2]) == accDate.getDate()) {
                return true;
            }
            else {
                return false;
            }
        };
    </script>
    <script type="text/javascript" src="https://apis.google.com/js/client.js?onload=checkAuth"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">

    <pre id="output"></pre>
    <style>
        .mc-navbar .mc-navbtn {
            width: 10%;
        }
    </style>
    <div class="mc-navbar" style="display: block;">
        <a href="SellerList.aspx">
            <div class="mc-navbtn">
                商家管理
            </div>
        </a><a href="ProposalList.aspx">
            <div class="mc-navbtn">
                提案管理
            </div>
        </a><a href="BusinessList.aspx">
            <div class="mc-navbtn">
                檔次管理
            </div>
        </a><a href="ProposalProgress.aspx">
            <div class="mc-navbtn">
                進度條
            </div>
        </a><a href="PponDealCalendar.aspx">
            <div class="mc-navbtn">
                檔期表
            </div>
        </a><a href="BackendRecentDeals.aspx">
            <div class="mc-navbtn">
                過往檔次查詢
            </div>
        </a><a href="PponOptionContnet.aspx">
            <div class="mc-navbtn">
                數量管理
            </div>
        </a>
        </a><a href="SalesCalendar.aspx">
            <div class="mc-navbtn on">
                個人行事曆
            </div>
        </a>
    </div>

    <div class="mc-content">
        <h1 class="rd-smll"><span class="breadcrumb flat">建立活動
        </span><a href="SalesCalendar.aspx" style="color: #08C;">回列表</a>
        </h1>
        <hr class="header_hr">
        <div id="maincontent" class="clearfix"></div>
        <div class="grui-form" id="authorize-div">
            <button id="authorize-button" onclick="handleAuthClick(event)">
                請求認證
            </button>
        </div>
        <div id="cal-content">
            <div class="grui-form" id="finance-form">
                <%--<div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        類型</label>
                    <div class="data-input rd-data-input">
                        <input type="radio" name="rdoType" value="1" checked="checked" />活動行程
                    <input type="radio" name="rdoType" value="2" onclick="javascript: location.href = 'SalesTaskContent.aspx'" />待辦事項
                    </div>
                </div>--%>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;內容</label>
                    <div class="data-input rd-data-input">
                        <asp:TextBox ID="txtCalendarContent" runat="server" ClientIDMode="Static" class="aspNetDisabled input-half" MaxLength="50" placeholder="最多輸入50個字"></asp:TextBox>
                        <asp:HiddenField ID="hidId" runat="server" Value="" ClientIDMode="Static" />
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        時間</label>
                    <div class="data-input rd-data-input">
                        <asp:TextBox ID="txtBeginDate" runat="server" ClientIDMode="Static" class="aspNetDisabled input-half" Style="width: 70px"></asp:TextBox>
                        <asp:DropDownList ID="ddlBeginTime" runat="server" ClientIDMode="Static" class="aspNetDisabled" Style="width: 100px;"></asp:DropDownList>
                        到
                        <asp:TextBox ID="txtEndDate" runat="server" ClientIDMode="Static" class="aspNetDisabled input-half" Style="width: 70px"></asp:TextBox>
                        <asp:DropDownList ID="ddlEndTime" runat="server" ClientIDMode="Static" class="aspNetDisabled" Style="width: 100px;"></asp:DropDownList>
                        <br />
                        <asp:CheckBox ID="chkWholeDay" runat="server" ClientIDMode="Static" onclick="WholeDay(this)" />全天
                        <asp:CheckBox ID="chkRepeat" runat="server" ClientIDMode="Static" onclick="openPopup()" />重複
                        <span id="spanRepeat"></span>
                        <input type="hidden" id="hidRepeat" />
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        地點</label>
                    <div class="data-input rd-data-input">
                        <asp:TextBox ID="txtLocation" runat="server" ClientIDMode="Static" class="aspNetDisabled input-half" Style="width: 200px"></asp:TextBox>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;商家名稱</label>
                    <div class="data-input rd-data-input">
                        <asp:TextBox ID="txtSellerName" runat="server" ClientIDMode="Static" class="aspNetDisabled input-half" Style="width: 200px"></asp:TextBox>
                        <asp:TextBox ID="txtSellerId" runat="server" ClientIDMode="Static" class="aspNetDisabled input-half" Style="width: 200px;display:none"></asp:TextBox>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;聯絡人</label>
                    <div class="data-input rd-data-input">
                        <asp:TextBox ID="txtContact" runat="server" ClientIDMode="Static" class="aspNetDisabled input-half" Style="width: 200px"></asp:TextBox>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        活動類型</label>
                    <div class="data-input rd-data-input">
                        <asp:DropDownList ID="ddlSellerEventType" runat="server" ClientIDMode="Static" Width="200"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        提醒</label>
                    <div class="data-input rd-data-input">
                        <asp:CheckBox ID="chkWakeUp" runat="server" ClientIDMode="Static" />開啟
                    <asp:TextBox ID="txtMinutes" runat="server" Width="30px" MaxLength="3" ClientIDMode="Static"></asp:TextBox>
                        <asp:DropDownList ID="ddlCycle" runat="server" ClientIDMode="Static" Style="width: 100px;">
                            <asp:ListItem Value="1">分鐘</asp:ListItem>
                            <asp:ListItem Value="60">小時</asp:ListItem>
                            <asp:ListItem Value="3600">天</asp:ListItem>
                            <asp:ListItem Value="86400">週</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        備註</label>
                    <div class="data-input rd-data-input">
                        <asp:TextBox ID="txtMemo" runat="server" ClientIDMode="Static" Width="80%" Height="60px" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        寫入經營紀錄</label>
                    <div class="data-input rd-data-input">
                        <asp:RadioButtonList ID="radSyncSellerNote" runat="server" ClientIDMode="Static">
                            <asp:ListItem Value="Y">是</asp:ListItem>
                            <asp:ListItem Value="N" Selected>否</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        </label>
                    <div class="data-input rd-data-input">
                        開發狀態：<asp:DropDownList ID="ddlDevelopStatus" runat="server"  ClientIDMode="Static" class="aspNetDisabled" Style="width: 100px;"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        </label>
                    <div class="data-input rd-data-input">
                        客戶等級：<asp:DropDownList ID="ddlSellerGrade" runat="server"  ClientIDMode="Static" class="aspNetDisabled" Style="width: 100px;"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-unit">
                    <div class="data-input rd-data-input">
                        <input type="button" class="btn btn-small btn-primary form-inline-btn openclose" value="儲存" onclick="Save()" />
                        <input type="text" id="CalenderId" value="" style="display: none" />
                        <input type="text" id="SalesCalenderName" value="業務系統" style="display: none" />
                        <asp:TextBox ID="txtUserName" runat="server" ClientIDMode="Static" style="display: none"></asp:TextBox>
                    </div>
                </div>
                
                
            </div>
        </div>
    </div>
    <div id="divCreateCalendar" style="display: none;">
        <h1 class="rd-smll">提醒方式</h1>
        <hr class="header_hr">
        <div class="grui-form" id="finance-form">
            <div class="form-unit">
                <label class="unit-label rd-unit-label">
                    顯示頻率</label>
                <div class="data-input rd-data-input">
                    <select id="ddlFrequency" style="width: 150px" onchange="changeWeekFrequency(this)">
                        <option value="0">每天</option>
                        <option value="1">每週 (週一至週五)</option>
                        <option value="2">每週一、週三和週五</option>
                        <option value="3">每週二和週四</option>
                        <option value="4" selected="selected">每週</option>
                        <option value="5">每月</option>
                        <option value="6">每年</option>
                    </select>
                    <input type="text" id="hidFrequency" value="" style="display: none" />
                </div>
            </div>
            <div class="form-unit interval">
                <label class="unit-label rd-unit-label">
                    顯示間隔</label>
                <div class="data-input rd-data-input">
                    <asp:DropDownList ID="ddlInterval" runat="server" Width="150px" ClientIDMode="Static" onchange="changeInterval(this)"></asp:DropDownList><span id="spanInterval"></span>
                    <input type="text" id="hidInterval" value="" style="display: none" />
                </div>
            </div>
            <div style="display: none" class="domrepeat">
                <div class="form-unit ">
                    <label class="unit-label rd-unit-label">
                        重複方式</label>
                    <div class="data-input rd-data-input">
                        <input id="radDomrepeat" name="repeatby" type="radio" checked="checked">每月第幾天
                    <input id="radDowrepeat" name="repeatby" type="radio">星期幾
                    </div>
                </div>
            </div>
            <div class="form-unit Week">
                <label class="unit-label rd-unit-label">
                    顯示時間</label>
                <div class="data-input rd-data-input">
                    <div>
                        <input id="chkWeek0" name="SU" type="checkbox">日
                        <input id="chkWeek1" name="MO" type="checkbox">一
                        <input id="chkWeek2" name="TU" type="checkbox">二
                        <input id="chkWeek3" name="WE" type="checkbox">三
                        <input id="chkWeek4" name="TH" type="checkbox">四
                        <input id="chkWeek5" name="FR" type="checkbox">五
                        <input id="chkWeek6" name="SA" type="checkbox">六
                    </div>
                </div>
            </div>
            <%--<div class="form-unit">
                <label class="unit-label rd-unit-label">
                    開始日期</label>
                <div class="data-input rd-data-input">
                    <input type="text" id="txtWakeUpDateS" name="txtWakeUpDateS" value="" />
                </div>
            </div>--%>
            <div class="form-unit">
                <label class="unit-label rd-unit-label">
                    結束日期</label>
                <div class="data-input rd-data-input">
                    <input id="chkEndson_never" name="endson" type="radio" value="0" onclick="endsonEvnet(this)" checked="checked">永遠不要<br />
                    <input id="chkEndson_count" name="endson" type="radio" value="1" onclick="endsonEvnet(this)">重複<input id="txtdhEndson_count_input" readonly="readonly" size="3" value="" maxlength="2">次後<br />
                    <input id="chkEndson_until" name="endson" type="radio" value="2" onclick="endsonEvnet(this)"><input id="txtEndson_until_input" readonly="readonly" size="10" value="" />
                </div>
            </div>
            <div class="form-unit">
                <div class="data-input rd-data-input">
                    <%--<input type="button" onclick="savePopup()" class="btn btn-small btn-primary form-inline-btn openclose" value="完成" />--%>
                    <input type="button" id="btnSavePopup" class="btn btn-small btn-primary form-inline-btn openclose" value="完成" />
                    <input type="button" onclick="closePopup()" class="btn btn-small btn-primary form-inline-btn openclose" value="取消" />

                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

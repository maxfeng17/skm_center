﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.SellerSales;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Sal
{
    public partial class PponDealCalendar : RolePage, ISalesPponDealCalendarView
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region props
        private SalesPponDealCalendarPresenter _presenter;
        public SalesPponDealCalendarPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public int MonthCount
        {
            get
            {
                return 1;
            }
        }
        public string Dept
        {
            get
            {
                return ddlDept.SelectedValue;
            }
        }

        public string EmpDept
        {
            get
            {
                ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(UserName));
                return emp.DeptId;
            }
        }

        public int EmpUserId
        {
            get
            {
                ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(UserName));
                return emp.UserId;
            }
        }
        public int DealType1
        {
            get {
                int v = -1;
                int.TryParse(ddlDealType1.SelectedValue,out v);
                return v;
            }
        }
        public int DealType2
        {
            get
            {
                int v = -1;
                int.TryParse(ddlDealType2.SelectedValue, out v);
                return v;
            }
        }

        private int _Year;
        public int Year
        {
            get
            {
                if (_Year == 0)
                {
                    if (rptMonth.Items.Count == 0)
                        _Year = DateTime.Now.Year;
                    else
                    {
                        Literal liMonth = (Literal)rptMonth.Items[0].FindControl("liMonth");
                        _Year = Convert.ToInt32(liMonth.Text.Substring(0, 4));
                    }

                }

                return _Year;
            }
        }

        private int _Month;
        public int Month
        {
            get
            {
                if (_Month == 0)
                {
                    if (rptMonth.Items.Count == 0)
                        _Month = DateTime.Now.Month;
                    else
                    {
                        Literal liMonth = (Literal)rptMonth.Items[0].FindControl("liMonth");
                        _Month = Convert.ToInt32(liMonth.Text.Substring(7, 2));
                    }

                }
                return _Month;
            }
        }

        public string CrossDeptTeam
        {
            get
            {
                ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(UserName));
                if (!string.IsNullOrEmpty(emp.CrossDeptTeam))
                    return emp.CrossDeptTeam;
                else if (emp.TeamNo == null)
                    return emp.DeptId + "[0]";
                else
                    return emp.DeptId + "[" + emp.TeamNo + "]";
            }
        }
        #endregion

        #region event
        public event EventHandler<DataEventArgs<string>> DeptChanged;
        public event EventHandler<DataEventArgs<string>> DealTypeChanged;
        public event EventHandler PreviousSearch;
        public event EventHandler NextSearch;
        #endregion

        #region method
        public void SetPponDealCalendar(Dictionary<ViewPponDealCalendar, Proposal> vpdc)
        {
            DateTime now = DateTime.Now;
            Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>> data = new Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>();
            List<Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>> dataList = new List<Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>>();
            Dictionary<DateTime, List<Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>>> rtn = new Dictionary<DateTime, List<Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>>>();
            for (int m = 0; m < MonthCount; m++)
            {
                DateTime firstDay = new DateTime(_Year, _Month, 1);
                if (_Month + m > 12)
                {
                    firstDay = new DateTime(_Year + 1, 1, 1);
                }
                else
                {
                    firstDay = new DateTime(_Year, _Month + m, 1);
                }
                for (int i = 0; i < (int)firstDay.DayOfWeek ; i++)
                {
                    data.Add(DateTime.MinValue.AddSeconds(i), new Dictionary<ViewPponDealCalendar, Proposal>());
                }
                for (int i = 0; i < firstDay.GetCountDaysOfMonth(); i++)
                {
                    DateTime thedate = firstDay.AddDays(i);
                    Dictionary<ViewPponDealCalendar, Proposal> vpds = vpdc.Where(x => x.Key.BusinessHourOrderTimeS.Date == thedate).ToDictionary(x => x.Key, y => y.Value);
                    if (vpds.Count == 0)
                    {
                        vpds.Add(new ViewPponDealCalendar(), new Proposal());
                    }
                    data.Add(thedate, vpds);
                    if (thedate.DayOfWeek == DayOfWeek.Saturday || firstDay.GetLastDayOfMonth() == thedate)
                    {
                        if (firstDay.GetLastDayOfMonth() == thedate)
                        {
                            for (int l = 0; l < (int)DayOfWeek.Saturday - (int)thedate.DayOfWeek; l++)
                            {
                                if (thedate.DayOfWeek != DayOfWeek.Friday)
                                {
                                    data.Add(DateTime.MinValue.AddSeconds(l), new Dictionary<ViewPponDealCalendar, Proposal>());
                                }
                            }
                        }
                        dataList.Add(new Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>(data));
                        data.Clear();
                    }
                }
                rtn.Add(firstDay, new List<Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>>(dataList));
                dataList.Clear();
            }
            rptMonth.DataSource = rtn;
            rptMonth.DataBind();
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _Year = (int)DateTime.Now.Year;
                _Month = (int)DateTime.Now.Month;
                InitialControl();
                EnabledSettings(UserName);
                Presenter.OnViewInitialized();
            }

            _presenter.OnViewLoaded();
        }


        protected void rptMonth_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<DateTime, List<Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>>>)
            {
                KeyValuePair<DateTime, List<Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>>> dataItem = (KeyValuePair<DateTime, List<Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>>>)e.Item.DataItem;
                Literal liMonth = (Literal)e.Item.FindControl("liMonth");
                Literal liTotalDealCount = (Literal)e.Item.FindControl("liTotalDealCount");
                Repeater rptWeek = (Repeater)e.Item.FindControl("rptWeek");
                liMonth.Text = dataItem.Key.ToString("yyyy - MM");
                liTotalDealCount.Text = string.Format("(本月檔次數共 {0} 筆)", dataItem.Value.Sum(x => x.Sum(y => y.Value.Count(z => z.Key.BusinessHourGuid != Guid.Empty))));
                rptWeek.DataSource = dataItem.Value;
                rptWeek.DataBind();
            }
        }

        protected void rptWeek_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>)
            {
                Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>> dataItem = (Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>)e.Item.DataItem;
                Repeater rptDate = (Repeater)e.Item.FindControl("rptDate");
                rptDate.DataSource = dataItem;// dataItem.Where(x => x.Key.DayOfWeek.EqualsNone(DayOfWeek.Saturday, DayOfWeek.Sunday));
                rptDate.DataBind();
            }
        }

        protected void rptDate_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>)
            {
                KeyValuePair<DateTime, Dictionary<ViewPponDealCalendar, Proposal>> dataItem = (KeyValuePair<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>)e.Item.DataItem;
                Literal liWeekName = (Literal)e.Item.FindControl("liWeekName");
                PlaceHolder divDate = (PlaceHolder)e.Item.FindControl("divDate");
                Label lblDate = (Label)e.Item.FindControl("lblDate");
                Repeater rptDeal = (Repeater)e.Item.FindControl("rptDeal");
                ImageButton btnHideContent = (ImageButton)e.Item.FindControl("btnHideContent");
                switch (dataItem.Key.DayOfWeek)
                {
                    case DayOfWeek.Friday:
                        liWeekName.Text = "星期五";
                        break;
                    case DayOfWeek.Monday:
                        liWeekName.Text = "星期一";
                        break;
                    case DayOfWeek.Saturday:
                        liWeekName.Text = "星期六";
                        break;
                    case DayOfWeek.Sunday:
                        liWeekName.Text = "星期日";
                        break;
                    case DayOfWeek.Thursday:
                        liWeekName.Text = "星期四";
                        break;
                    case DayOfWeek.Tuesday:
                        liWeekName.Text = "星期二";
                        break;
                    case DayOfWeek.Wednesday:
                        liWeekName.Text = "星期三";
                        break;
                    default:
                        break;
                }
                if (dataItem.Key.Date != DateTime.MinValue.Date)
                {
                    lblDate.Text = dataItem.Key.Day.ToString();
                    divDate.Visible = true;
                    btnHideContent.Visible = true;
                    if (dataItem.Key.Date == DateTime.Now.Date)
                    {
                        lblDate.ForeColor = System.Drawing.Color.Red;
                        lblDate.Font.Bold = true;
                        lblDate.Font.Underline = true;
                    }
                }
                else
                {
                    liWeekName.Text = string.Empty;
                }
                rptDeal.DataSource = dataItem.Value;
                rptDeal.DataBind();
            }
        }

        protected void rptDeal_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<ViewPponDealCalendar, Proposal>)
            {
                KeyValuePair<ViewPponDealCalendar, Proposal> dataItem = (KeyValuePair<ViewPponDealCalendar, Proposal>)e.Item.DataItem;
                HyperLink hkPid = (HyperLink)e.Item.FindControl("hkPid");
                HyperLink hkItemName = (HyperLink)e.Item.FindControl("hkItemName");
                Label lblOrderTimeE = (Label)e.Item.FindControl("lblOrderTimeE");
                Literal liWarning = (Literal)e.Item.FindControl("liWarning");
                Literal liSalesName = (Literal)e.Item.FindControl("liSalesName");
                if (dataItem.Key.BusinessHourGuid != Guid.Empty)
                {
                    if (dataItem.Value.Id != 0)
                    {
                        hkPid.Text = "<font color='red'>(" + (e.Item.ItemIndex + 1).ToString() + ")</font>" + dataItem.Value.Id;
                        if (dataItem.Value.ProposalSourceType == (int)ProposalSourceType.Original)
                        {
                            hkPid.NavigateUrl = ResolveUrl("~/sal/ProposalContent.aspx?pid=" + dataItem.Value.Id);
                        }
                        else
                        {
                            hkPid.NavigateUrl = ResolveUrl("~/sal/proposal/house/proposalcontent?pid=" + dataItem.Value.Id);
                        }
                            
                    }
                    else
                        hkPid.Text = "<font color='red'>(" + (e.Item.ItemIndex + 1).ToString() + ")</font>";


                    hkItemName.Text = dataItem.Key.ItemName;
                    hkItemName.NavigateUrl = ResolveUrl("~/ppon/default.aspx?bid=" + dataItem.Key.BusinessHourGuid);
                    //img.ImageUrl = ImageFacade.GetMediaPathsFromRawData(dataItem.Key.EventImagePath, MediaType.PponDealPhoto).FirstOrDefault();
                    lblOrderTimeE.Text = string.Format("上檔天數: {0} 天", (dataItem.Key.BusinessHourOrderTimeE - dataItem.Key.BusinessHourOrderTimeS).Days.ToString());

                    //業務
                    ProposalSalesModel model = ProposalFacade.DealGetSalesByBid(dataItem.Key.BusinessHourGuid);

                    liSalesName.Text = string.Format("<i class='fa fa-user fa-fw {0}'></i>{1}", 
                        hidEmpUserId.Value.Equals(dataItem.Key.DevelopeSalesId.ToString()) || hidEmpUserId.Value.Equals(dataItem.Key.OperationSalesId == null ? "0" : dataItem.Key.OperationSalesId.ToString() ) ? string.Empty : "otherSales", model.DevelopeSalesEmpName + (string.IsNullOrEmpty(model.OperationSalesEmpName) ? "" : "/" + model.OperationSalesEmpName));

                    if (dataItem.Value.IsLoaded)
                    {
                        string fontFormat = "<i class='fa {0} fa-fw' style='color: #BF0000;' title='{1}'></i>";
                        if (!Helper.IsFlagSet(dataItem.Value.ListingFlag, ProposalListingFlag.PaperContractCheck))
                        {
                            liWarning.Text += string.Format(fontFormat, "fa-file-text-o", "紙本合約未回");
                        }
                        if (!Helper.IsFlagSet(dataItem.Value.BusinessFlag, ProposalBusinessFlag.PhotographerCheck))
                        {
                            liWarning.Text += string.Format(fontFormat, "fa-camera", "尚未攝影確認");
                        }
                        if (!Helper.IsFlagSet(dataItem.Value.ListingFlag, ProposalListingFlag.PageCheck))
                        {
                            liWarning.Text += string.Format(fontFormat, "fa-eye", "尚未頁面確認");
                        }
                        if (!Helper.IsFlagSet(dataItem.Value.ListingFlag, ProposalListingFlag.FinanceCheck))
                        {
                            liWarning.Text += string.Format(fontFormat, "fa-usd", "財務資料尚未確認");
                        }
                    }
                }
            }
        }

        protected void ddlDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.DeptChanged != null)
            {
                this.DeptChanged(sender, new DataEventArgs<string>(((DropDownList)sender).SelectedValue));
            }
        }

        protected void ddlDealType1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string id = ((DropDownList)sender).SelectedValue;
            Dictionary<int, string> rtn = new Dictionary<int, string>();
            ProposalDealType type = ProposalDealType.TryParse(id, out type) ? type : ProposalDealType.None;
            if (type != ProposalDealType.None)
            {
                rtn = ProposalFacade.ProposalSubDealTypeGet(type,true);
            }

            ddlDealType2.DataSource = rtn;
            ddlDealType2.DataTextField = "Value";
            ddlDealType2.DataValueField = "Key";
            ddlDealType2.DataBind();

            if (this.DealTypeChanged != null)
            {
                this.DealTypeChanged(sender, new DataEventArgs<string>(((DropDownList)sender).SelectedValue));
            }
        }
        protected void ddlDealType2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.DealTypeChanged != null)
            {
                this.DealTypeChanged(sender, new DataEventArgs<string>(((DropDownList)sender).SelectedValue));
            }
        }

        protected void btnPrevious_Click(object sender, ImageClickEventArgs e)
        {
            Literal liMonth = (Literal)rptMonth.Items[0].FindControl("liMonth");
            _Year = Convert.ToInt32(liMonth.Text.Substring(0, 4));
            _Month = Convert.ToInt32(liMonth.Text.Substring(7, 2));

            if (_Month == 1)
            {
                _Year = _Year - 1;
                _Month = 12;
            }
            else
                _Month = _Month - 1;


            if (this.PreviousSearch != null)
            {
                this.PreviousSearch(this, e);
            }
        }

        protected void btnNext_Click(object sender, ImageClickEventArgs e)
        {
            Literal liMonth = (Literal)rptMonth.Items[0].FindControl("liMonth");
            _Year = Convert.ToInt32(liMonth.Text.Substring(0, 4));
            _Month = Convert.ToInt32(liMonth.Text.Substring(7, 2));

            if (_Month == 12)
            {
                _Year = _Year + 1;
                _Month = 1;
            }
            else
                _Month = _Month + 1;


            if (this.NextSearch != null)
            {
                this.NextSearch(this, e);
            }
        }
        #endregion

        #region private method

        private void InitialControl()
        {
            // 業務部門
            List<Department> Depts = HumanFacade.GetSalesDepartment();
            Depts.Insert(0, new Department
            {
                DeptName = "請選擇",
                DeptId = ""
            });
            ddlDept.DataSource = Depts;
            ddlDept.DataTextField = "DeptName";
            ddlDept.DataValueField = "DeptId";
            ddlDept.DataBind();


            Dictionary<int, string> dealType = new Dictionary<int, string>();
            dealType.Add(-1, "請選擇");

            foreach (var item in Enum.GetValues(typeof(ProposalDealType)))
            {
                dealType[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDealType)item);
            }

            ddlDealType1.DataSource = dealType.Where(x => x.Key != (int)ProposalDealType.None);
            ddlDealType1.DataTextField = "Value";
            ddlDealType1.DataValueField = "Key";
            ddlDealType1.DataBind();

        }

        private void EnabledSettings(string username)
        {
            ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(username));
            if (emp.IsLoaded)
            {
                hidEmpUserId.Value = emp.UserId.ToString();

                if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.ReadAll))
                {
                    divDeptList.Visible = true;
                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.CrossDeptTeam))
                {
                    if (string.IsNullOrEmpty(emp.CrossDeptTeam) && emp.TeamNo == null)
                    {
                        ShowMessage("身份權限未明，無法查看資料，請洽產品或技術人員協助");
                    }
                    else
                    {
                        divShowSelf.Visible = true;
                    }
                    
                }
            }
            else
            {
                ShowMessage("查無員工資料，請重新檢視員工資料維護設定。");
            }
        }

        public void ShowMessage(string msg)
        {
            string script = string.Format("alert('{0}');", msg);
            Page.ClientScript.RegisterStartupScript(GetType(), "alert", script, true);
        }

        #endregion

    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="SellerBatchContent.aspx.cs" Inherits="LunchKingSite.Web.Sal.SellerBatchContent" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>

<%@ Register Src="../User/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/fancybox/jquery.fancybox.css") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Tools/js/jquery.blockUI.js"></script>    
    <script type="text/javascript">
        function DownloadSample()
        {
            location.href = "../template/SalesDocs/SellerBatchContentSample.xls";
            return true;
        }

        function WaitingBlock() {
            $.blockUI({
                message: "上傳中請稍後...",
                css: {
                    width: '20%',
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <asp:PlaceHolder ID="divMain" runat="server">
        <div class="mc-navbar">
            <a>
                <div class="mc-navbtn on">
                    商家批次處理
                </div>
            </a>
        </div>
        <div id="MainSet" class="mc-content">
            <asp:Panel ID="divSeller" runat="server">
                <div><h1 class="rd-smll"><a href="SellerList.aspx" style="color: #08C;">回列表</a></h1></div>
                
                <div id="sell">
                    <h1 class="rd-smll">商家批次處理
                        <br/>
                        <span style="color: red; font-size:small">處理商家資料時，品牌名稱、店家數、平均客單價、平均座位數、滿桌率(%)、翻桌次、店家來源、店家屬性、為必填欄位，會在處理時檢查，若無，則無法建立/修改。</span>
                    </h1>
                    <hr class="header_hr">
                    
                    <div class="grui-form" id="sell-form">
                        <div class="form-unit">                            
                            <div class="data-input rd-data-input">
                                <input type="button" name="btnSellerBatchContentSample" id="btnSellerBatchContentSample" class="btn btn-small btn-primary form-inline-btn" OnClick="return DownloadSample();" value="下載批次範本" />
                            </div>
                        </div>
                        <div class="form-unit">                            
                            <div class="data-input rd-data-input">
                                <asp:FileUpload ID="FUImport" runat="server" />
                                <asp:Button runat="server" ID="btnUpload" CssClass="btn btn-small btn-primary form-inline-btn" Text="上傳商家資料" OnClick="btnUpload_Click" OnClientClick="WaitingBlock();" />
                            </div>
                        </div> 
                    </div>
                    <asp:Panel ID="divErrorList" runat="server" Visible="false">
                        <label class="unit-label rd-unit-label">錯誤列表</label>
                        <div id="mc-table">
                            <asp:Repeater ID="rptErrorList" runat="server">
                                <HeaderTemplate>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                                        <tr class="rd-C-Hide">
                                            <th class="Export OrderSerial" style="display:none">
                                                <input id="chkChangeAll" type="checkbox" />
                                            </th>
                                            <th class="ProcessMode">處理模式
                                            </th>
                                            <th class="SellerID">編號
                                            </th>
                                            <th class="SellerName">品牌名稱
                                            </th>
                                            <th class="ParentSellerID">上層公司
                                            </th>
                                            <th class="ErrorMessage">錯誤訊息
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="mc-tableContentITEM">
                                        <td class="ProcessMode" style="width:8%">
                                            <%# ((SellerBatchErrorData)Container.DataItem).ProcessMode %>
                                        </td>
                                        <td class="SellerID" style="text-align:center;width:10%">
                                            <%# ((SellerBatchErrorData)Container.DataItem).SellerID %>
                                        </td>
                                        <td class="SellerName" style="text-align:left;width:30%">
                                            <%# ((SellerBatchErrorData)Container.DataItem).SellerName %>
                                        </td>
                                        <td class="ParentSellerID" style="text-align:center;width:10%">
                                            <%# ((SellerBatchErrorData)Container.DataItem).ParentSellerID %>
                                        </td>
                                        <td class="ErrorMessage" style="text-align:left">
                                            <%# ((SellerBatchErrorData)Container.DataItem).ErrorMessage %>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <uc1:Pager ID="ucPager" runat="server" PageSize="10" ongetcount="GetCount" onupdate="UpdateHandler"></uc1:Pager>
                    </asp:Panel>
                </div>
            </asp:Panel>
        </div>
    </asp:PlaceHolder>
</asp:Content>
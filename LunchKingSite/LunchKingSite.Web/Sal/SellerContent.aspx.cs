﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.SellerSales;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Sal
{
    public partial class SellerContent : RolePage, ISalesSellerContentView
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        public IHumanProvider hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        public Dictionary<int, string> proposalContact = new Dictionary<int, string>()
        {
            {-1, "請選擇"}
        };

        #region props
        private SalesSellerContentPresenter _presenter;
        public SalesSellerContentPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public Guid SellerGuid
        {
            get
            {
                Guid id = Guid.Empty;
                if (Request.QueryString["sid"] != null && Guid.TryParse(Request.QueryString["sid"].ToString(), out id))
                {
                    Guid.TryParse(Request.QueryString["sid"].ToString(), out id);
                }
                return id;
            }
        }

        public bool NoCompanyIDCheck
        {
            get
            {
                return chkNoCompanyIDCheck.Checked;
            }
        }
        public short DevelopStatus
        {
            get { return short.Parse(ddlDevelopStatus.SelectedValue); }
        }
        public short SellerGrade
        {
            get { return short.Parse(ddlSellerGrade.SelectedValue); }
        }

        private string _SellerList = "";
        public string SellerList
        {
            set
            {
                _SellerList = value;
            }
            get
            {
                return _SellerList;
            }
        }

        public int ProposalDeliveryType
        {
            set
            {
                hdProposalDeliveryType.Text = Convert.ToString(value);
            }
            get
            {
                int _value = (int)DeliveryType.ToHouse;
                int.TryParse(hdProposalDeliveryType.Text, out _value);
                return _value;
            }
        }

        private bool _IsTravelProposal;
        public bool IsTravelProposal
        {
            set
            {
                _IsTravelProposal = value;
            }
            get
            {
                return _IsTravelProposal;
            }
        }

        private bool _IsPiinLifeProposal;
        public bool IsPiinLifeProposal
        {
            set
            {
                _IsPiinLifeProposal = value;
            }
            get
            {
                return _IsPiinLifeProposal;
            }
        }

        private bool _IsPBeautyProposal;
        public bool IsPBeautyProposal
        {
            set
            {
                _IsPBeautyProposal = value;
            }
            get
            {
                return _IsPBeautyProposal;
            }
        }

        private bool _IsWms = false;
        public bool IsWms
        {
            set
            {
                _IsWms = value;
            }
            get
            {
                return _IsWms;
            }
        }

        public bool IsConfirmWms
        {
            get
            {
                return WmsFacade.isConfirmWms(SellerGuid, string.Empty);
            }
        }

        public string Tab
        {
            get
            {
                if (Request.QueryString["tab"] != null)
                {
                    return Request.QueryString["tab"].ToString();
                }
                return string.Empty;
            }
        }

        public string SalesManNameList = string.Empty;
        public string[] SalesmanNameArray
        {
            set
            {
                var rtn = string.Empty;
                foreach (var s in value)
                {
                    if (rtn == string.Empty)
                    {
                        rtn = string.Format("'{0}'", s);
                    }
                    else
                    {
                        rtn = string.Format("{0},'{1}'", rtn, s);
                    }
                }
                SalesManNameList = rtn;
            }
        }

        private string _hid;
        public string hid
        {
            set
            {
                _hid = HiddenField1.Value;
            }
            get
            {
                return HiddenField1.Value;
            }
        }

        public int ReceiptType
        {
            get
            {
                if (rbRecordInvoice.Checked)
                {
                    return (int)VendorReceiptType.Invoice;
                }
                else if (rbRecordReceipt.Checked)
                {
                    return (int)VendorReceiptType.Receipt;
                }
                else if (rbRecordNoTaxInvoice.Checked)
                {
                    return (int)VendorReceiptType.NoTaxInvoice;
                }
                else if (rbRecordOthers.Checked)
                {
                    return (int)VendorReceiptType.Other;
                }
                else
                    return 99;
            }
        }

        public string AccountingMessage
        {
            get
            {
                return txtAccountingMessage.Text;
            }
        }

        /// <summary>
        /// 狀態
        /// </summary>
        public StoreStatus Status
        {
            get
            {
                int selected;
                if (int.TryParse(rdlStatus.SelectedValue, out selected))
                {
                    return (StoreStatus)selected;
                }
                else
                {
                    return StoreStatus.Available;
                }
            }
            set
            {
                rdlStatus.SelectedValue = ((int)value).ToString();
            }
        }

        public bool IsCloseDown
        {
            get
            {
                return rdlcloseDownStatus.SelectedValue == "1" ? true : false;
            }
            set
            {
                rdlcloseDownStatus.SelectedValue = value ? "1" : "0";
            }
        }

        /// <summary>
        /// 轉介人員(業務)
        /// </summary>
        public string ReferralSale
        {
            get
            {
                return txtReferralSale.Text.Trim();
            }
        }
        public int ReferralPercent
        {
            get
            {
                int percent = 0;
                int.TryParse(ddlReferralPercent.SelectedValue, out percent);
                return percent;
            }
        }

        public bool IsReferralModifyMail
        {
            get
            {
                return hidDealMail.Value != "";
            }
        }

        public DateTime? CloseDownDate
        {
            get
            {
                DateTime dt = DateTime.MinValue;
                DateTime.TryParse(formCloseDownDate.Text, out dt);
                if (dt == DateTime.MinValue)
                {
                    return null;
                }
                else
                {
                    return dt;
                }
            }
            set
            {
                if (value.HasValue)
                {
                    formCloseDownDate.Text = value.Value.ToString("yyyy/MM/dd");
                }
            }
        }


        /// <summary>
        /// 營業時間
        /// </summary>
        public string BusinessHour
        {
            get { return HttpUtility.HtmlDecode(tbBusinessHour.Text.Trim()); }
            set { tbBusinessHour.Text = value; }
        }

        /// <summary>
        /// 公休日
        /// </summary>
        public string CloseDateInformation
        {
            get { return tbCloseDate.Text.Trim(); }
            set { tbCloseDate.Text = value; }
        }

        private Guid _ParentGuid;
        public Guid ParentGuid
        {
            get { return _ParentGuid; }
            set { _ParentGuid = value; }
        }

        public string DevelopeSales
        {
            get { return hidDevelopeSales.Value; }
            set { hidDevelopeSales.Value = value; }
        }

        public string OperationSales
        {
            get { return hidOperationSales.Value; }
            set { hidOperationSales.Value = value; }
        }

        public string SelectDevelopeSales
        {
            get { return hidSelectSalesDevelope.Value; }
            set { hidSelectSalesDevelope.Value = value; }
        }

        public string SelectOperationSales
        {
            get { return hidSelectSalesOperation.Value; }
            set { hidSelectSalesOperation.Value = value; }
        }

        public string SecondService
        {
            get { return ddlUserId.SelectedValue; }
            set { ddlUserId.SelectedValue = value; }
        }

        public bool IsAgreeHouseNewContractSeller
        {
            get
            {
                return SellerFacade.IsAgreeNewContractSeller(SellerGuid, (int)DeliveryType.ToHouse);
            }
        }



        public bool IsAgreePponNewContractSeller
        {
            get
            {
                return SellerFacade.IsAgreeNewContractSeller(SellerGuid, (int)DeliveryType.ToShop);
            }
        }

        public ViewVbsInstorePickupCollection ViewVbsInstorePickupViewData
        {
            get; set;
        }

        public ViewVbsInstorePickup ViewVbsInstorePickupFamily
        {
            get; set;
        }

        public ViewVbsInstorePickup ViewVbsInstorePickupSeven
        {
            get; set;
        }

        public bool IsISPVerifyFamily { get; set; }

        public bool IsISPEnabledFamily { get; set; }

        public bool IsISPVerifySeven { get; set; }

        public bool IsISPEnabledSeven { get; set; }

        public bool IsWmsEnabled { get; set; }

        public int ReturnCycleFamily
        {
            get
            {
                return int.Parse(hdReturnCycleFamily.Value);
            }
        }

        public int ReturnTypeFamily
        {
            get
            {
                return int.Parse(hdReturnTypeFamily.Value);
            }
        }

        public string ReturnOtherFamily
        {
            get
            {
                return hdReturnOtherFamily.Value;
            }
        }
        public int ReturnCycleSeven
        {
            get
            {
                if (hdReturnCycleSeven.Value == "")
                    return -1;
                else
                    return int.Parse(hdReturnCycleSeven.Value);
            }
        }

        public int ReturnTypeSeven
        {
            get
            {
                if (hdReturnTypeSeven.Value == "")
                    return -1;
                else
                    return int.Parse(hdReturnTypeSeven.Value);
            }
        }

        public string ReturnOtherSeven
        {
            get
            {
                return hdReturnOtherSeven.Value;
            }
        }

        public string ReturnAddress
        {
            get
            {
                return hdReturnAddress.Value;
            }
        }

        public string ContactName
        {
            get
            {
                return hdContactName.Value;
            }
        }
        public string ContactTel
        {
            get
            {
                return hdContactTel.Value;
            }
        }
        public string ContactEmail
        {
            get
            {
                return hdContactEmail.Value;
            }
        }

        public string ContactMobile
        {
            get
            {
                return hdContactMobile.Value;
            }
        }


        

        #endregion

        #region event
        public event EventHandler<EventArgs> Save;
        public event EventHandler<DataEventArgs<SellerTempStatus>> SellerApprove;
        public event EventHandler<DataEventArgs<Guid>> CreateProposal;
        public event EventHandler<DataEventArgs<int>> OnGetCount;
        public event EventHandler<DataEventArgs<int>> OnGetMCount;
        public event EventHandler<DataEventArgs<int>> PageChanged;
        public event EventHandler<DataEventArgs<int>> PageMChanged;
        public event EventHandler<EventArgs> SaveLocation;
        public event EventHandler<DataEventArgs<PhotoInfo>> UpdatePhoto;
        public event EventHandler<DataEventArgs<string>> ImageListChanged;
        public event EventHandler<DataEventArgs<KeyValuePair<bool, ServiceChannel>>> ISPApply;
        public event EventHandler<DataEventArgs<KeyValuePair<string, ServiceChannel>>> ISPReject;

        #endregion

        #region method
        public void SetSellerContent(Seller s, ViewEmployee emp,
                                    SellerChangeLogCollection changeLogs,
                                    SellerManageLogCollection manageLogs,
                                    SellerContractFileCollection scfs,
                                    Dictionary<Seller, Seller> ds,
                                    List<ShoppingCartManageList> freiList,
                                    ShoppingCartFreightsLogCollection scLogs)
        {
            if (s.Guid != Guid.Empty)
            {
                // 審核狀態
                liTempStatus.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerTempStatus)s.TempStatus);

                // 商家編號/名稱
                liSellerId.Text = s.SellerId;
                liTitle.Text = liSellerName.Text = s.SellerName;

                //上層公司
                //var sellertree = stc.FirstOrDefault(x => x.IsEnable);

                if (ds.Count > 0 && !string.IsNullOrEmpty(ds.FirstOrDefault().Value.ToString()))
                {
                    liParentSellerId.Text = ds.FirstOrDefault().Value.SellerId;
                    txtParentSellerName.Text = ds.FirstOrDefault().Value.SellerName;
                    hidParentSellerGuid.Value = ds.FirstOrDefault().Value.Guid.ToString();
                }
                else
                {
                    liParentSellerId.Text = "";
                    txtParentSellerName.Text = "";
                }


                //顯示狀態
                Status = (StoreStatus)s.StoreStatus;

                //營業狀態
                IsCloseDown = s.IsCloseDown;
                CloseDownDate = s.CloseDownDate;

                cbISPEnabled.Checked = IsISPEnabledFamily || IsISPEnabledSeven;

                //是否啟用統倉
                cbWmsEnabled.Checked = IsWmsEnabled;

                // 聯絡資訊
                txtSellerName.Text = s.SellerName;
                if (s.SellerLevel.HasValue && string.IsNullOrEmpty(s.SellerLevelDetail))
                {
                    ddlSellerLevel.SelectedValue = ((int)s.SellerLevel).ToString();
                    hdSellerLevel.Value = ((int)s.SellerLevel).ToString();
                }
                btnSellerLvDetail.Attributes.Add("onclick", "return false;");
                hdSellerLevelDetail.Value = s.SellerLevelDetail;
                City township = CityManager.TownShipGetById(s.CityId);
                if (township != null)
                {
                    ddlCompanyCityId.SelectedValue = township.ParentId.ToString();
                    hidTownshipId.Value = township.Id.ToString();
                }

                txtCompanyAddress.Text = s.SellerAddress;
                txtSellerBossName.Text = s.SellerBossName;
                //txtContactPersonName.Text = s.SellerContactPerson;
                //txtSellerTel.Text = s.SellerTel;
                //txtSellerMobile.Text = s.SellerMobile;
                //txtSellerFax.Text = s.SellerFax;
                //txtContactPersonEmail.Text = s.SellerEmail;
                //txtReturnedPersonName.Text = s.ReturnedPersonName;
                //txtReturnedPersonTel.Text = s.ReturnedPersonTel;
                //txtReturnedPersonEmail.Text = s.ReturnedPersonEmail;
                txtOthers.Text = s.SellerDescription;
                ddlSellerFrom.SelectedValue = (s.SellerFrom ?? -1).ToString();
                string _SellerPorperty = s.SellerPorperty;
                if (!string.IsNullOrEmpty(_SellerPorperty))
                {
                    string[] pros = _SellerPorperty.Split(",");
                    foreach (string pro in pros)
                    {
                        var listProperty = from i in chkSellerPorperty.Items.Cast<ListItem>()
                                           where pro.Equals(((ListItem)i).Value)
                                           select i;

                        listProperty.First().Selected = true;
                    }

                }

                #region 轉介人員
                liReferralSaleName.Text = "";
                if (!string.IsNullOrEmpty(s.ItemPriceCondition))
                {
                    //原有轉介功能廢除(先借放這個欄位)
                    List<string> pros = SellerFacade.GetProposalItemPriceContent(s.Guid);
                    foreach (string pro in pros)
                    {
                        var listDealType = from i in radDealType.Items.Cast<ListItem>()
                                            where pro.Equals(((ListItem)i).Value)
                                            select i;

                        listDealType.First().Selected = true;
                    }

                    //txtReferralSale.Text = s.ReferralSale;
                    //hidReferralSale.Value = s.ReferralSale;
                    //if (s.ReferralSaleBeginTime != null && s.ReferralSaleEndTime != null)
                    //{
                    //    liReferralSaleBeginTime.Text = (s.ReferralSaleBeginTime ?? DateTime.MinValue).ToString("yyyy/MM/dd");
                    //    liReferralSaleEndTime.Text = (s.ReferralSaleEndTime ?? DateTime.MinValue).ToString("yyyy/MM/dd");
                    //}
                    //if (s.ReferralSalePercent != null && s.ReferralSalePercent != 0)
                    //{
                    //    ddlReferralPercent.SelectedValue = s.ReferralSalePercent.ToString();
                    //}
                    //ViewEmployee empRefferSale = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, s.ReferralSale);
                    //liReferralSaleName.Text = empRefferSale.EmpName;
                }
                #endregion

                // 匯款資訊
                txtCompanyName.Text = s.CompanyName;
                txtCompanyBossName.Text = s.CompanyBossName;
                txtCompanyID.Text = s.CompanyID;
                if (!string.IsNullOrWhiteSpace(s.CompanyBankCode))
                {
                    try
                    {
                        ddlBankCode.SelectedValue = s.CompanyBankCode;
                        hidBranchCode.Value = s.CompanyBranchCode;
                    }
                    catch { }

                }
                txtSignCompanyID.Text = s.SignCompanyID;
                txtAccountId.Text = s.CompanyAccount;
                txtAccount.Text = s.CompanyAccountName;
                txtFinanceName.Text = s.AccountantName;
                txtFinanceTel.Text = s.AccountantTel;
                txtFinanceEmail.Text = s.CompanyEmail;
                ddlRemittanceType.SelectedValue = (s.RemittanceType ?? -1).ToString();

                //聯絡人資料
                List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(s.Contacts);
                if (contacts == null)
                {
                    contacts = new List<Seller.MultiContracts>();
                    bool flag = false;
                    //一般聯絡人
                    if (!string.IsNullOrEmpty(s.SellerContactPerson))
                    {
                        Seller.MultiContracts NContract = new Seller.MultiContracts()
                        {
                            Type = ((int)ProposalContact.Normal).ToString(),
                            ContactPersonName = s.SellerContactPerson,
                            SellerTel = s.SellerTel,
                            SellerMobile = s.SellerMobile,
                            SellerFax = s.SellerFax,
                            ContactPersonEmail = s.SellerEmail,
                            Others = ""
                        };
                        contacts.Add(NContract);
                        flag = true;
                    }
                    //退換貨聯絡人
                    if (!string.IsNullOrEmpty(s.ReturnedPersonName))
                    {
                        Seller.MultiContracts RContract = new Seller.MultiContracts()
                        {
                            Type = ((int)ProposalContact.ReturnPerson).ToString(),
                            ContactPersonName = s.ReturnedPersonName,
                            SellerTel = s.ReturnedPersonTel,
                            SellerMobile = "",
                            SellerFax = "",
                            ContactPersonEmail = s.ReturnedPersonEmail,
                            Others = ""
                        };
                        contacts.Add(RContract);
                        flag = true;
                    }
                    if (!flag)
                    {
                        Seller.MultiContracts MultiContract = new Seller.MultiContracts()
                        {
                            Type = "-1",
                            ContactPersonName = "",
                            SellerTel = "",
                            SellerMobile = "",
                            SellerFax = "",
                            ContactPersonEmail = "",
                            Others = ""
                        };
                        contacts.Add(MultiContract);
                    }
                }
                rptMultiContacts.DataSource = contacts;
                rptMultiContacts.DataBind();


                // 追蹤紀錄
                var logData = changeLogs.OrderByDescending(x => x.CreateTime);
                rptSellerChangeLog.DataSource = logData;
                rptSellerChangeLog.DataBind();

                // 經營筆記
                var mlogData = manageLogs.OrderByDescending(x => x.CreateTime);
                rptSellerManageLog.DataSource = mlogData;
                rptSellerManageLog.DataBind();

                //合約上傳
                var scfsData = scfs.OrderByDescending(x => x.CreateTime);
                rptContractFiles.DataSource = scfsData;
                rptContractFiles.DataBind();

                //購物車
                rptShoppingCart.DataSource = freiList.OrderByDescending(x => x.Id);
                rptShoppingCart.DataBind();

                //購物車異動紀錄
                rptShoppingCartLog.DataSource = scLogs.OrderByDescending(x => x.CreateTime);
                rptShoppingCartLog.DataBind();


                //預設代賣家聯絡人
                txtContractPartyBContact.Text = s.SellerBossName;
                List<Seller.MultiContracts> contactscontact = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(s.Contacts);
                if (contactscontact != null)
                {
                    foreach (var c in contactscontact)
                    {
                        txtContractPartyBContact.Text += "/" + c.ContactPersonName;
                    }
                }

                if (!string.IsNullOrEmpty(txtContractPartyBContact.Text) && txtContractPartyBContact.Text.Length > 50)
                    txtContractPartyBContact.Text = txtContractPartyBContact.Text.Substring(0, 50);

                //合約類型
                Dictionary<int, string> contacttype = new Dictionary<int, string>();
                ddlContactType.Items.Clear();
                ddlContactType.Items.Add(new ListItem(Helper.GetEnumDescription(SellerContractType.PCPMain), ((int)SellerContractType.PCPMain).ToString()));
                foreach (var item in Enum.GetValues(typeof(SellerContractType)))
                {
                    if((int)item >= (int)SellerContractType.Delivery)
                    {
                        ddlContactType.Items.Add(new ListItem(Helper.GetEnumDescription((SellerContractType)item), ((int)item).ToString()));
                    }
                }


                // 已建立的賣家才可顯示的資訊
                divBasic.Visible = true;
                //divStore.Visible = true;
                divChangeLog.Visible = true;
                //divManageNote.Visible = true;
                //divManageLog.Visible = true;
                //divContract.Visible = true; 


                //頁籤
                aManageNote.Visible = true;
                aContract.Visible = true;
                aLocation.Visible = true;
                aISPManage.Visible = true;
                if (config.ShoppingCartEnabled)
                {
                    aShoppingCartManage.Visible = true;
                }

                //店家單據開立方式
                if (s.VendorReceiptType == (int)VendorReceiptType.Invoice)
                    rbRecordInvoice.Checked = true;
                else if (s.VendorReceiptType == (int)VendorReceiptType.NoTaxInvoice)
                    rbRecordNoTaxInvoice.Checked = true;
                else if (s.VendorReceiptType == (int)VendorReceiptType.Receipt)
                    rbRecordReceipt.Checked = true;
                else if (s.VendorReceiptType == (int)VendorReceiptType.Other)
                    rbRecordOthers.Checked = true;

                txtAccountingMessage.Text = s.Othermessage;


                //營業據點
                chkNoLocation.Checked = string.IsNullOrEmpty(s.StoreTel);
                txtStoreTel.Text = s.StoreTel;
                //地址
                if (s.StoreTownshipId != null)
                {
                    City locationtownship = CityManager.TownShipGetById((int)s.StoreTownshipId);
                    if (locationtownship != null)
                    {
                        ddlLocationCity.SelectedValue = locationtownship.ParentId.ToString();
                        hidLocationTownshipId.Value = locationtownship.Id.ToString();
                    }
                }

                txtLocationAddress.Text = s.StoreAddress;
                //交通資訊
                txtMrt.Text = s.Mrt;
                txtCar.Text = s.Car;
                txtBus.Text = s.Bus;
                txtOV.Text = s.OtherVehicles;
                //網站
                txtWebUrl.Text = s.WebUrl;
                txtFBUrl.Text = s.FacebookUrl;
                txtBlogUrl.Text = s.BlogUrl;
                tbOtherUrl.Text = s.OtherUrl;
                //營業時間
                BusinessHour = s.OpenTime;
                //公休日
                CloseDateInformation = s.CloseDate;
                //備註
                txtRemark.Text = s.StoreRemark;
                //刷卡
                chkCreditcardAvailable.Checked = s.CreditcardAvailable;
                //預約
                chkIsOpenBooking.Checked = s.IsOpenBooking;
                rdlIsOpenBooking.SelectedValue = s.IsOpenReservationSetting == true ? "1" : "0";

                //所有分店
                rptBSeller.DataSource = ds.Keys;
                rptBSeller.DataBind();

                rptCSeller.DataSource = ds.Keys;
                rptCSeller.DataBind();


                hi.Value = s.SellerLogoimgPath;
                SetImageGrid(s.SellerLogoimgPath);

                //抓取ddl
                var entity = hp.ViewEmployeeCollectionGetByFilter("dept_id", "C002", false);
                ddlUserId.Items.Clear();
                ddlUserId.Items.Add(new ListItem("請選擇", ""));
                foreach (var item in entity)
                {
                    ddlUserId.Items.Add(new ListItem("(" + item.EmpName + ")" + item.Email, item.EmpId));
                }

                SellerMappingEmployee sme = hp.SellerMappingEmployeeGet(SellerGuid);
                if (sme.IsLoaded)
                {
                    try { 
                        ddlUserId.SelectedValue = sme.EmpId;
                    }
                    catch { }               
                }

            }

            if (config.ShoppingCartV2Enabled)
            {
                phdFreightManage.Visible = true;
            }

            EnabledSettings(s);
        }

        public void RedirectProposal(Proposal pro)
        {
            if (ProposalFacade.IsVbsProposalNewVersion())
            {
                if(pro.ProposalSourceType == (int)ProposalSourceType.House)
                {
                    Response.Redirect(ResolveUrl("~/sal/proposal/house/proposalcontent?pid=" + pro.Id));
                }                
            }
            Response.Redirect(ResolveUrl("~/sal/ProposalContent.aspx?pid=" + pro.Id));
        }

        public void RedirectSeller(Guid sid)
        {
            Response.Redirect(ResolveUrl("~/sal/SellerContent.aspx?sid=" + sid));
        }

        public void ShowMessage(string msg, SellerContentMode mode, string parms = "")
        {
            string script = string.Empty;
            switch (mode)
            {
                case SellerContentMode.GenericError:
                    liMessage.Text = msg;
                    break;
                case SellerContentMode.PrivilegeError:
                case SellerContentMode.SellerNotFound:
                    script = string.Format("alert('{0}'); location.href='{1}/sal/SellerList.aspx';", msg, config.SiteUrl);
                    break;
                case SellerContentMode.CompanyIdCheck:
                case SellerContentMode.SalesError:
                    script = string.Format("alert('{0}');", msg);
                    break;
                case SellerContentMode.CompanyIdExist:
                    script = string.Format("alert('{0}'); location.href='{1}/sal/SellerList.aspx?comid={2}';", msg, config.SiteUrl, parms);
                    break;
                case SellerContentMode.TurnUrl:
                    script = string.Format("location.href='{0}/sal/SellerList.aspx?comid={1}';", config.SiteUrl, parms);
                    break;
                case SellerContentMode.CopyTurnUrl:
                    script = string.Format("location.href='{0}/sal/SellerContent.aspx?csid={1}';", config.SiteUrl, parms);
                    break;
                default:
                    break;
            }

            if (!string.IsNullOrWhiteSpace(script))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "alert", script, true);
            }
        }

        public void SetImageGrid(string rawDataPath)
        {
            ListItemCollection lic = new ListItemCollection();
            string[] paths = ImageFacade.GetMediaPathsFromRawData(rawDataPath, MediaType.SellerPhotoLarge);
            foreach (string p in paths)
            {
                lic.Add(new ListItem(p, p));
            }
            ri.DataSource = lic;
            ri.DataBind();
        }

        public void SetImageFile(PhotoInfo pi)
        {
            ImageUtility.UploadFileWithResize(pi.PFile.ToAdapter(), pi.Type, pi.DestFilePath, pi.DestFileName, 157, 86);
        }


        [WebMethod]
        public static string SellerGetByCompany(string SellerGuid, string SellerName, string SignCompanyID)
        {
            SellerCollection cs = ProviderFactory.Instance().GetProvider<ISellerProvider>().SellerGetByCompanyList(SellerName, SignCompanyID);
            var result = cs.Any(p => p.Guid.ToString() == SellerGuid);
            if (cs.Count > 0)
            {
                if (result)
                    return new JsonSerializer().Serialize(new { d = "True", Guid = SellerGuid });
                else
                    return new JsonSerializer().Serialize(new { d = "True", Guid = cs[0].Guid });
            }
            else
                return new JsonSerializer().Serialize(new { d = "False", Guid = "" });
        }

        [WebMethod]
        public static string SellerGet(string SellerGuid, string SignCompanyID)
        {
            SellerCollection cs = ProviderFactory.Instance().GetProvider<ISellerProvider>().SellerGetList(Seller.Columns.SignCompanyID, SignCompanyID);
            var result = cs.Any(p => p.Guid.ToString() == SellerGuid);

            if (cs.Count > 0)
            {
                if (result)
                    return new JsonSerializer().Serialize(new { d = "True", Guid = SellerGuid });
                else
                    return new JsonSerializer().Serialize(new { d = "True", Guid = cs[0].Guid });
            }
            else
                return new JsonSerializer().Serialize(new { d = "False", Guid = "" });
        }


        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetWeekly();
                IntialControls();
                Presenter.OnViewInitialized();

            }
            Page.MaintainScrollPositionOnPostBack = true;
            liMessage.Text = string.Empty;
            _presenter.OnViewLoaded();
        }

        protected void btnSellerApprove_Click(object sender, EventArgs e)
        {
            if (this.SellerApprove != null)
            {
                this.SellerApprove(this, new DataEventArgs<SellerTempStatus>(SellerTempStatus.Completed));
            }
        }


        protected void btnSellerReturned_Click(object sender, EventArgs e)
        {
            if (this.SellerApprove != null)
            {
                this.SellerApprove(this, new DataEventArgs<SellerTempStatus>(SellerTempStatus.Returned));
            }
        }

        protected void btnSellerApply_Click(object sender, EventArgs e)
        {
            if (this.SellerApprove != null)
            {
                this.SellerApprove(this, new DataEventArgs<SellerTempStatus>(SellerTempStatus.Applied));
            }
        }

        protected void btnCreateProposal_Click(object sender, EventArgs e)
        {
            if (this.CreateProposal != null)
            {
                IsTravelProposal = false;
                _IsPBeautyProposal = false;
                ProposalDeliveryType = (int)DeliveryType.ToShop;
                SelectDevelopeSales = UserName;
                this.CreateProposal(this, new DataEventArgs<Guid>(SellerGuid));
            }
        }

        protected void btnCreatePBeautyProposal_Click(object sender, EventArgs e)
        {
            if (this.CreateProposal != null)
            {
                IsTravelProposal = false;
                IsPBeautyProposal = true;
                ProposalDeliveryType = (int)DeliveryType.ToShop;
                SelectDevelopeSales = UserName;
                this.CreateProposal(this, new DataEventArgs<Guid>(SellerGuid));
            }
        }

        protected void btnCreateDeliveryProposal_Click(object sender, EventArgs e)
        {
            if (this.CreateProposal != null)
            {
                IsTravelProposal = false;
                IsPBeautyProposal = false;
                ProposalDeliveryType = (int)DeliveryType.ToHouse;
                SelectDevelopeSales = UserName;
                this.CreateProposal(this, new DataEventArgs<Guid>(SellerGuid));
            }
        }

        protected void btnCreateDeliveryNormalProposal_Click(object sender, EventArgs e)
        {
            if (this.CreateProposal != null)
            {
                IsTravelProposal = false;
                IsPBeautyProposal = false;
                ProposalDeliveryType = (int)DeliveryType.ToHouse;
                SelectDevelopeSales = UserName;
                this.CreateProposal(this, new DataEventArgs<Guid>(SellerGuid));
            }
        }

        protected void btnCreateDeliveryWmsProposal_Click(object sender, EventArgs e)
        {
            if (this.CreateProposal != null)
            {
                IsTravelProposal = false;
                IsPBeautyProposal = false;
                ProposalDeliveryType = (int)DeliveryType.ToHouse;
                SelectDevelopeSales = UserName;
                IsWms = true;
                this.CreateProposal(this, new DataEventArgs<Guid>(SellerGuid));
            }
        }



        protected void btnCreateTravelPponProposal_Click(object sender, EventArgs e)
        {
            if (this.CreateProposal != null)
            {
                IsTravelProposal = true;
                IsPBeautyProposal = false;
                ProposalDeliveryType = (int)DeliveryType.ToShop;
                SelectDevelopeSales = UserName;
                this.CreateProposal(this, new DataEventArgs<Guid>(SellerGuid));
            }
        }

        protected void btnCreateTravelDeliveryProposal_Click(object sender, EventArgs e)
        {
            if (this.CreateProposal != null)
            {
                IsTravelProposal = true;
                IsPBeautyProposal = false;
                ProposalDeliveryType = (int)DeliveryType.ToHouse;
                SelectDevelopeSales = UserName;
                this.CreateProposal(this, new DataEventArgs<Guid>(SellerGuid));
            }
        }

        protected void btnCreatePiinLifeDeliveryProposal_Click(object sender, EventArgs e)
        {
            if (this.CreateProposal != null)
            {
                IsTravelProposal = false;
                IsPBeautyProposal = false;
                IsPiinLifeProposal = true;
                ProposalDeliveryType = (int)DeliveryType.ToHouse;
                SelectDevelopeSales = UserName;

                this.CreateProposal(this, new DataEventArgs<Guid>(SellerGuid));
            }
        }

        protected void btnMultipleSalesCreateProposal_Click(object sender, EventArgs e)
        {
            if (this.CreateProposal != null)
            {
                IsTravelProposal = Convert.ToBoolean(hidSelectSalesIsTravelProposal.Value);
                _IsPBeautyProposal = Convert.ToBoolean(hidSelectSalesIsPBeautyProposal.Value);
                ProposalDeliveryType = Convert.ToInt32(hidSelectSalesProposalDeliveryType.Value);
                SelectDevelopeSales = hidSelectSalesDevelope.Value;
                SelectOperationSales = hidSelectSalesOperation.Value;

                this.CreateProposal(this, new DataEventArgs<Guid>(SellerGuid));
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Save != null)
            {
                this.Save(this, e);
            }
        }

        protected void btnFileUpload_Click(object sender, EventArgs e)
        {

        }

        protected void btnSaveLocation_Click(object sender, EventArgs e)
        {

            if (SaveLocation != null)
            {
                SaveLocation(this, null);
            }

        }

        protected int GetCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetCount != null)
            {
                OnGetCount(this, e);
            }
            return e.Data;
        }
        protected int GetMCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetMCount != null)
            {
                OnGetMCount(this, e);
            }
            return e.Data;
        }

        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChanged != null)
            {
                this.PageChanged(this, new DataEventArgs<int>(pageNumber));
            }
        }
        protected void UpdateMHandler(int pageNumber)
        {
            if (this.PageMChanged != null)
            {
                this.PageMChanged(this, new DataEventArgs<int>(pageNumber));
            }
        }
        protected void rptMultiContacts_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                foreach (ProposalContact item in Enum.GetValues(typeof(ProposalContact)))
                {
                    proposalContact[(int)item] = Helper.GetDescription(item);
                }
                string selectedValue = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Type"));
                DropDownList ddlProposalContact = (DropDownList)e.Item.FindControl("ddlProposalContact");
                ddlProposalContact.DataSource = proposalContact;
                ddlProposalContact.SelectedValue = selectedValue;
                ddlProposalContact.DataTextField = "Value";
                ddlProposalContact.DataValueField = "Key";
                ddlProposalContact.DataBind();
            }
        }

        protected void rptBSeller_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is LunchKingSite.DataOrm.Seller)
            {
                LunchKingSite.DataOrm.Seller dataItem = (LunchKingSite.DataOrm.Seller)e.Item.DataItem;
                Panel panSellerName = (Panel)e.Item.FindControl("panSellerName");
                CheckBox chkCheck = (CheckBox)e.Item.FindControl("chkSeller");

                chkCheck.InputAttributes["value"] = dataItem.Guid.ToString();
            }
        }

        protected void rptCSeller_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is LunchKingSite.DataOrm.Seller)
            {
                LunchKingSite.DataOrm.Seller dataItem = (LunchKingSite.DataOrm.Seller)e.Item.DataItem;
                Panel panSellerName = (Panel)e.Item.FindControl("panSellerName");
                CheckBox chkCheck = (CheckBox)e.Item.FindControl("chkSeller");

                chkCheck.InputAttributes["value"] = dataItem.Guid.ToString();
            }
        }

        protected void btnCopySeller_Click(object sender, EventArgs e)
        {
            ShowMessage("", SellerContentMode.CopyTurnUrl, SellerGuid.ToString() + "-0");
        }

        protected void btnCopyChildSeller_Click(object sender, EventArgs e)
        {
            ShowMessage("", SellerContentMode.CopyTurnUrl, SellerGuid.ToString() + "-1");
        }


        protected void btnUploadPic_Save(object sender, EventArgs e)
        {
            string rawData = hi.Value;
            if (!String.IsNullOrEmpty(photoUpload.PostedFile.FileName))
            {
                string tempFile = Path.GetTempFileName();
                HttpPostedFile pFile = photoUpload.PostedFile;
                string destFileName = "logo-" + DateTime.Now.ToString("yyMMddHHmmss");
                if (pFile.ContentType == "image/pjpeg" || pFile.ContentType == "image/jpeg" || pFile.ContentType == "image/x-png" || pFile.ContentType == "image/gif")
                {
                    PhotoInfo pi = new PhotoInfo();
                    pi.PFile = pFile;
                    pi.Type = UploadFileType.SellerPhoto;
                    pi.DestFilePath = liSellerId.Text;
                    pi.DestFileName = destFileName;

                    UpdatePhoto(this, new DataEventArgs<PhotoInfo>(pi));

                    if (!string.IsNullOrEmpty(rawData))
                    {
                        rawData = Helper.GenerateRawDataFromRawPaths(rawData, ImageFacade.GenerateMediaPath(liSellerId.Text, destFileName + "." + Helper.GetExtensionByContentType(pFile.ContentType)));
                    }
                    else
                    {
                        rawData = ImageFacade.GenerateMediaPath(liSellerId.Text, destFileName + "." + Helper.GetExtensionByContentType(pFile.ContentType));
                    }
                }
            }

            if (this.ImageListChanged != null)
            {
                this.ImageListChanged(this, new DataEventArgs<string>(rawData));
            }
        }



        #endregion

        #region private method
        private void IntialControls()
        {
            // 地址(城市)
            IEnumerable<City> citys = CityManager.Citys.Where(x => x.Code != "SYS");
            ddlCompanyCityId.DataSource = citys;
            ddlCompanyCityId.DataTextField = "CityName";
            ddlCompanyCityId.DataValueField = "Id";
            ddlCompanyCityId.DataBind();

            ddlLocationCity.DataSource = citys;
            ddlLocationCity.DataTextField = "CityName";
            ddlLocationCity.DataValueField = "Id";
            ddlLocationCity.DataBind();

            //店家來源
            Dictionary<int, string> sellerFrom = new Dictionary<int, string>();
            foreach (ProposalSellerFrom item in Enum.GetValues(typeof(ProposalSellerFrom)))
            {
                sellerFrom[(int)item] = Helper.GetDescription(item);
            }
            ddlSellerFrom.DataSource = sellerFrom;
            ddlSellerFrom.DataTextField = "Value";
            ddlSellerFrom.DataValueField = "Key";
            ddlSellerFrom.DataBind();

            List<SystemCode> dealType = SystemCodeManager.GetDealType().Where(x => x.ParentCodeId == null).ToList();
            radDealType.DataValueField = "CodeId";
            radDealType.DataTextField = "CodeName";
            radDealType.DataSource = dealType;
            radDealType.DataBind();

            //店家屬性
            Dictionary<int, string> sellerProperty = new Dictionary<int, string>();
            List<ProposalSellerPorperty> sellerPropertyList = new List<ProposalSellerPorperty>();
            //foreach (ProposalSellerPorperty item in Enum.GetValues(typeof(ProposalSellerPorperty)))
            //{
            //    sellerProperty[(int)item] = Helper.GetDescription(item);
            //}
            sellerPropertyList.Add(ProposalSellerPorperty.Ppon);
            sellerPropertyList.Add(ProposalSellerPorperty.HomeDelivery);
            sellerPropertyList.Add(ProposalSellerPorperty.GroupCoupon);
            sellerPropertyList.Add(ProposalSellerPorperty.RegularsCard);
            sellerPropertyList.Add(ProposalSellerPorperty.LionTravel);
            sellerPropertyList.Add(ProposalSellerPorperty.Skm);
            sellerPropertyList.Add(ProposalSellerPorperty.Taishin);

            foreach (ProposalSellerPorperty item in sellerPropertyList)
            {
                chkSellerPorperty.Items.Add(new ListItem(Helper.GetDescription(item), ((int)item).ToString()));
            }

            //chkSellerPorperty.DataSource = sellerProperty
            //chkSellerPorperty.DataTextField = "Value";
            //chkSellerPorperty.DataValueField = "Key";
            //chkSellerPorperty.DataBind();


            // 賣家規模(依照指定順序排列)
            List<SellerLevel> dataList = new List<SellerLevel>();
            dataList.Add(SellerLevel.SA);
            dataList.Add(SellerLevel.A);
            dataList.Add(SellerLevel.B);
            dataList.Add(SellerLevel.C);
            dataList.Add(SellerLevel.D);
            dataList.Add(SellerLevel.Big);
            dataList.Add(SellerLevel.Medium);
            dataList.Add(SellerLevel.Small);
            dataList.Add(SellerLevel.Delivery);
            foreach (SellerLevel item in dataList)
            {
                ddlSellerLevel.Items.Add(new ListItem(Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, item), ((int)item).ToString()));
            }

            // 銀行代碼
            var bankS = VourcherFacade.BankInfoGetMainList().Select(x => new { BankNo = x.BankNo, BankName = x.BankNo + " " + x.BankName });
            ddlBankCode.DataSource = bankS;
            ddlBankCode.DataTextField = "BankName";
            ddlBankCode.DataValueField = "BankNo";
            ddlBankCode.DataBind();

            //出帳方式
            Dictionary<int, string> achList = new Dictionary<int, string>();
            if (config.IsRemittanceFortnightly && IsAgreePponNewContractSeller)
            {
                achList.Add((int)RemittanceType.Monthly, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Monthly));
                achList.Add((int)RemittanceType.Flexible, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Flexible));
                achList.Add((int)RemittanceType.Others, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Others));
            }
            else
            {
                achList.Add((int)RemittanceType.Weekly, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Weekly));
                achList.Add((int)RemittanceType.Monthly, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Monthly));
                achList.Add((int)RemittanceType.Flexible, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Flexible));
                achList.Add((int)RemittanceType.Others, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Others));
            }

            ddlRemittanceType.DataTextField = "Value";
            ddlRemittanceType.DataValueField = "Key";
            ddlRemittanceType.DataSource = achList;
            ddlRemittanceType.DataBind();

            //開發狀態
            Dictionary<int, string> developStatus = new Dictionary<int, string>();
            foreach (DevelopStatus item in Enum.GetValues(typeof(DevelopStatus)))
            {
                developStatus[(int)item] = Helper.GetDescription(item);
            }
            ddlDevelopStatus.DataSource = developStatus;
            ddlDevelopStatus.DataTextField = "Value";
            ddlDevelopStatus.DataValueField = "Key";
            ddlDevelopStatus.DataBind();

            //客戶等級
            Dictionary<int, string> sellerGrade = new Dictionary<int, string>();
            foreach (SellerGrade item in Enum.GetValues(typeof(SellerGrade)))
            {
                sellerGrade[(int)item] = Helper.GetDescription(item);
            }
            ddlSellerGrade.DataSource = sellerGrade;
            ddlSellerGrade.DataTextField = "Value";
            ddlSellerGrade.DataValueField = "Key";
            ddlSellerGrade.DataBind();

            //聯絡人
            foreach (ProposalContact item in Enum.GetValues(typeof(ProposalContact)))
            {
                proposalContact[(int)item] = Helper.GetDescription(item);
            }

            List<Seller.MultiContracts> contacts = new List<Seller.MultiContracts>();
            contacts.Add(new Seller.MultiContracts()
            {
                Type = "-1",
                ContactPersonName = "",
                SellerTel = "",
                SellerMobile = "",
                SellerFax = "",
                ContactPersonEmail = "",
                Others = ""
            });
            rptMultiContacts.DataSource = contacts;
            rptMultiContacts.DataBind();

            btnCreateTravelProposal.NavigateUrl = "#divCreateTravelProposal";

            Seller s = new Seller();
            //複製商家
            if (Request.QueryString["csid"] != null)
            {
                string sellerid = Request.QueryString["csid"].ToString().Substring(0, Request.QueryString["csid"].ToString().Length - 2);
                Guid seller_id = Guid.Empty;
                Guid.TryParse(sellerid, out seller_id);
                s = sp.SellerGet(seller_id);

                bool HaveParent = Request.QueryString["csid"].ToString().Split('-')[5].ToString() == "0" ? false : true;

                if (s.SellerLevel.HasValue)
                {
                    ddlSellerLevel.SelectedValue = ((int)s.SellerLevel).ToString();
                }
                ddlSellerFrom.SelectedValue = (s.SellerFrom ?? -1).ToString();

                string _SellerPorperty = s.SellerPorperty;
                if (!string.IsNullOrEmpty(_SellerPorperty))
                {
                    string[] pros = _SellerPorperty.Split(",");
                    foreach (string pro in pros)
                    {
                        var listProperty = from i in chkSellerPorperty.Items.Cast<ListItem>()
                                           where pro.Equals(((ListItem)i).Value)
                                           select i;

                        listProperty.First().Selected = true;

                    }

                }

                //上層
                if (HaveParent)
                {
                    //複製為新的下層公司
                    liParentSellerId.Text = s.SellerId;
                    hidParentSellerGuid.Value = s.Guid.ToString();
                    txtParentSellerName.Text = s.SellerName;
                }
                else
                {
                    //複製為新商家
                    SellerTree ParentTree = sp.SellerTreeGetListBySellerGuid(s.Guid).FirstOrDefault();
                    if (ParentTree != null)
                    {
                        Seller ParentSeller = sp.SellerGet(ParentTree.ParentSellerGuid);
                        liParentSellerId.Text = ParentSeller.SellerId;
                        hidParentSellerGuid.Value = ParentSeller.Guid.ToString();
                        txtParentSellerName.Text = ParentSeller.SellerName;
                    }

                }



                txtSignCompanyID.Text = s.SignCompanyID;

                City township = CityManager.TownShipGetById(s.CityId);
                if (township != null)
                {
                    ddlCompanyCityId.SelectedValue = township.ParentId.ToString();
                    hidTownshipId.Value = township.Id.ToString();
                }

                txtCompanyAddress.Text = s.SellerAddress;

                txtSellerBossName.Text = s.SellerBossName;

                Status = (StoreStatus)s.StoreStatus;

                IsCloseDown = s.IsCloseDown;
                CloseDownDate = s.CloseDownDate;

                txtOthers.Text = s.SellerDescription;

                txtCompanyName.Text = s.CompanyName;

                txtCompanyBossName.Text = s.CompanyBossName;


                if (!string.IsNullOrWhiteSpace(s.CompanyBankCode))
                {
                    try
                    {
                        ddlBankCode.SelectedValue = s.CompanyBankCode;
                        hidBranchCode.Value = s.CompanyBranchCode;
                    }
                    catch { }

                }
                txtCompanyID.Text = s.CompanyID;

                txtAccountId.Text = s.CompanyAccount;
                txtAccount.Text = s.CompanyAccountName;
                ddlRemittanceType.SelectedValue = (s.RemittanceType ?? -1).ToString();

                if (s.VendorReceiptType == (int)VendorReceiptType.Invoice)
                    rbRecordInvoice.Checked = true;
                else if (s.VendorReceiptType == (int)VendorReceiptType.NoTaxInvoice)
                    rbRecordNoTaxInvoice.Checked = true;
                else if (s.VendorReceiptType == (int)VendorReceiptType.Receipt)
                    rbRecordReceipt.Checked = true;
                else if (s.VendorReceiptType == (int)VendorReceiptType.Other)
                    rbRecordOthers.Checked = true;

                txtAccountingMessage.Text = s.Othermessage;

                txtFinanceName.Text = s.AccountantName;
                txtFinanceTel.Text = s.AccountantTel;
                txtFinanceEmail.Text = s.CompanyEmail;


                //聯絡人資料
                List<Seller.MultiContracts> contacts2 = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(s.Contacts);
                if (contacts2 == null)
                {
                    contacts2 = new List<Seller.MultiContracts>();
                    bool flag = false;
                    //一般聯絡人
                    if (!string.IsNullOrEmpty(s.SellerContactPerson))
                    {
                        Seller.MultiContracts NContract = new Seller.MultiContracts()
                        {
                            Type = ((int)ProposalContact.Normal).ToString(),
                            ContactPersonName = s.SellerContactPerson,
                            SellerTel = s.SellerTel,
                            SellerMobile = s.SellerMobile,
                            SellerFax = s.SellerFax,
                            ContactPersonEmail = s.SellerEmail,
                            Others = ""
                        };
                        contacts2.Add(NContract);
                        flag = true;
                    }
                    //退換貨聯絡人
                    if (!string.IsNullOrEmpty(s.ReturnedPersonName))
                    {
                        Seller.MultiContracts RContract = new Seller.MultiContracts()
                        {
                            Type = ((int)ProposalContact.ReturnPerson).ToString(),
                            ContactPersonName = s.ReturnedPersonName,
                            SellerTel = s.ReturnedPersonTel,
                            SellerMobile = "",
                            SellerFax = "",
                            ContactPersonEmail = s.ReturnedPersonEmail,
                            Others = ""
                        };
                        contacts2.Add(RContract);
                        flag = true;
                    }
                    if (!flag)
                    {
                        Seller.MultiContracts MultiContract = new Seller.MultiContracts()
                        {
                            Type = "-1",
                            ContactPersonName = "",
                            SellerTel = "",
                            SellerMobile = "",
                            SellerFax = "",
                            ContactPersonEmail = "",
                            Others = ""
                        };
                        contacts2.Add(MultiContract);
                    }
                }
                rptMultiContacts.DataSource = contacts2;
                rptMultiContacts.DataBind();

            }

            //組織樹狀圖
            if (SellerGuid != Guid.Empty)
            {
                hkSellerTree.Visible = true;
                hkSellerTree.NavigateUrl = ResolveUrl("../Sal/SellerTreeView?sid=" + SellerGuid + "&type=proposal");
            }

            syncBusinessHour.NavigateUrl = "#divBusinessHour";
            syncCloseDate.NavigateUrl = "#divCloseDate";

        }

        private void SetWeekly()
        {
            Dictionary<string, string> weekly = new Dictionary<string, string>();
            weekly.Add("0", "週一");
            weekly.Add("1", "週二");
            weekly.Add("2", "週三");
            weekly.Add("3", "週四");
            weekly.Add("4", "週五");
            weekly.Add("5", "週六");
            weekly.Add("6", "週日");

            //營業時間
            //chk_BusinessWeekly.DataValueField = "Key";
            //chk_BusinessWeekly.DataTextField = "Value";
            //chk_BusinessWeekly.DataSource = weekly;
            //chk_BusinessWeekly.DataBind();

            ////公休日
            //chk_close_BusinessWeekly.DataValueField = "Key";
            //chk_close_BusinessWeekly.DataTextField = "Value";
            //chk_close_BusinessWeekly.DataSource = weekly;
            //chk_close_BusinessWeekly.DataBind();


            Dictionary<string, string> weeklyHour = new Dictionary<string, string>();
            for (int iHour = 0; iHour <= 24; iHour++)
            {
                weeklyHour.Add(iHour.ToString().PadLeft(2, '0'), iHour.ToString().PadLeft(2, '0'));
            }
            //營業時間
            ddl_WeeklyBeginHour.DataValueField = "Key";
            ddl_WeeklyBeginHour.DataTextField = "Value";
            ddl_WeeklyBeginHour.DataSource = weeklyHour;
            ddl_WeeklyBeginHour.DataBind();

            ddl_WeeklyEndHour.DataValueField = "Key";
            ddl_WeeklyEndHour.DataTextField = "Value";
            ddl_WeeklyEndHour.DataSource = weeklyHour;
            ddl_WeeklyEndHour.DataBind();

            //公休日
            ddl_close_WeeklyBeginHour.DataValueField = "Key";
            ddl_close_WeeklyBeginHour.DataTextField = "Value";
            ddl_close_WeeklyBeginHour.DataSource = weeklyHour;
            ddl_close_WeeklyBeginHour.DataBind();

            ddl_close_WeeklyEndHour.DataValueField = "Key";
            ddl_close_WeeklyEndHour.DataTextField = "Value";
            ddl_close_WeeklyEndHour.DataSource = weeklyHour;
            ddl_close_WeeklyEndHour.DataBind();

            Dictionary<string, string> weeklyMinute = new Dictionary<string, string>();
            for (int iMinute = 0; iMinute < 60; iMinute += 5)
            {
                weeklyMinute.Add(iMinute.ToString().PadLeft(2, '0'), iMinute.ToString().PadLeft(2, '0'));
            }
            //營業時間
            ddl_WeeklyBeginMinute.DataValueField = "Key";
            ddl_WeeklyBeginMinute.DataTextField = "Value";
            ddl_WeeklyBeginMinute.DataSource = weeklyMinute;
            ddl_WeeklyBeginMinute.DataBind();

            ddl_WeeklyEndMinute.DataValueField = "Key";
            ddl_WeeklyEndMinute.DataTextField = "Value";
            ddl_WeeklyEndMinute.DataSource = weeklyMinute;
            ddl_WeeklyEndMinute.DataBind();

            ddl_close_WeeklyBeginMinute.DataValueField = "Key";
            ddl_close_WeeklyBeginMinute.DataTextField = "Value";
            ddl_close_WeeklyBeginMinute.DataSource = weeklyMinute;
            ddl_close_WeeklyBeginMinute.DataBind();

            ddl_close_WeeklyEndMinute.DataValueField = "Key";
            ddl_close_WeeklyEndMinute.DataTextField = "Value";
            ddl_close_WeeklyEndMinute.DataSource = weeklyMinute;
            ddl_close_WeeklyEndMinute.DataBind();

            //公休時間
            Dictionary<int, string> flags = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(StoreCloseDate)))
            {
                flags[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (StoreCloseDate)item);
            }
            ddlCloseDate.DataSource = flags;
            ddlCloseDate.DataTextField = "Value";
            ddlCloseDate.DataValueField = "Key";
            ddlCloseDate.DataBind();



        }

        private void EnabledSettings(Seller s)
        {
            #region 瀏覽權限檢查 (無全區瀏覽權限，會判斷商家是否有綁定業務，有的話只能檢視自己的商家)

            //登入者
            ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(UserName));
            if (emp.IsLoaded)
            {
                
                if (!CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReadAll))
                {
                    List<MultipleSalesModel> model = SellerFacade.SellerSaleGetBySellerGuid(s.Guid);
                    //新增or公池不檢查
                    if (model.Count() != 0)
                    {
                        //商家業務
                        //ViewEmployee selleremp = HumanFacade.ViewEmployeeGetByUserId(s.SalesId.Value);

                        if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.CrossDeptTeam))
                        {
                            if (string.IsNullOrEmpty(emp.CrossDeptTeam) && emp.TeamNo == null)
                            {
                                ShowMessage("身份權限未明，無法查看資料，請洽產品或技術人員協助。", SellerContentMode.PrivilegeError);
                            }
                            else
                            {
                                #region 本區檢視
                                string message = "";
                                bool check = ProposalFacade.GetCrossPrivilege(1, emp, null, null, null, model, ref message);
                                if (!check)
                                {
                                    ShowMessage(message, SellerContentMode.PrivilegeError);
                                    return;
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            #region 無檢視權限(僅能查看負責業務是自己的商家)
                            var sales = model.Where(x => x.DevelopeSalesEmail == UserName || x.OperationSalesEmail == UserName).FirstOrDefault();
                            if (sales == null)
                            {
                                ShowMessage(string.Format("無法查看該商家資訊。"), SellerContentMode.PrivilegeError);
                                return;
                            }

                            #endregion
                        }
                    }
                }
            }
            else
            {
                ShowMessage("查無員工資料，請重新檢視員工資料維護設定。", SellerContentMode.PrivilegeError);
                return;
            }



            #endregion

            #region 轉介人員權限檢查
            divIntroduct.Visible = false;// CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReferralAdd);
            divIntroductPercent.Visible = false; //CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReferralAdd);
            #endregion

            if (s.Guid != Guid.Empty)
            {
                #region 更新權限檢查 (尚未審核通過的商家，才可變更資料；尚在on檔/兌換中的商家，必須具有財務變更權限才可以變更財務資料)

                if (s.TempStatus != (int)SellerTempStatus.Completed && CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Update))
                {
                    if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.FinanceApplied))
                    {
                        EnabledControls(true, true);
                    }
                    else
                    {
                        ViewPponDealCollection vpdc = PponFacade.GetOnUseViewPponDealListBySellerGuid(s.Guid);
                        bool isOnUse = vpdc.Count > 0;
                        EnabledControls(true, !isOnUse);
                    }
                }
                else
                {
                    EnabledControls(false, false);
                }

                #endregion

                #region 審核權限檢查

                // 申請核准
                btnSellerApprove.Visible = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Completed) && s.TempStatus == (int)SellerTempStatus.Applied;

                // 申請退回
                btnSellerReturned.Visible = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Completed) && s.TempStatus == (int)SellerTempStatus.Applied;

                // 申請變更
                btnSellerApply.Visible = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Applied) && s.TempStatus != (int)SellerTempStatus.Applied;

                #endregion

                #region 增加業務權限檢查

                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Referral))
                {
                    divReferral.Visible = true;
                    btnSellerSalesReferral.Visible = true;
                }

                #endregion

                #region 提案申請權限檢查

                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Apply))
                {
                    btnCreateProposal.Visible = true;
                    btnCreatePBeautyProposal.Visible = true;
                    btnCreateDeliveryProposal.Visible = true;
                    btnCreateTravelPponProposal.Visible = true;
                    btnCreateTravelDeliveryProposal.Visible = true;
                    btnCreatePiinLifeDeliveryProposal.Visible = true;
                    hdProposalDeliveryType.Visible = false;
                }

                #endregion
            }

            divMain.Visible = true;

            //隱藏店家不能建提案單
            if (s.StoreStatus == (int)StoreStatus.Available)
                divCreateProposal.Visible = true;
            else
                divCreateProposal.Visible = false;
        }

        private void EnabledControls(bool enabled, bool finance_enabled)
        {
            // 聯絡資訊
            txtSellerName.Enabled = enabled;
            btnSellerLvDetail.Visible = enabled;
            ddlSellerLevel.Enabled = enabled;
            ddlCompanyCityId.Enabled = enabled;
            ddlTownshipId.Enabled = enabled;
            txtCompanyAddress.Enabled = enabled;
            txtSellerBossName.Enabled = enabled;
            //txtContactPersonName.Enabled = enabled;
            //txtSellerTel.Enabled = enabled;
            //txtSellerMobile.Enabled = enabled;
            //txtSellerFax.Enabled = enabled;
            //txtContactPersonEmail.Enabled = enabled;
            //txtReturnedPersonName.Enabled = enabled;
            //txtReturnedPersonTel.Enabled = enabled;
            //txtReturnedPersonEmail.Enabled = enabled;
            txtOthers.Enabled = enabled;
            //ContactPersonSync.Visible = enabled;
            //ReturnedPersonSync.Visible = enabled;
            ddlSellerFrom.Enabled = enabled;
            chkSellerPorperty.Enabled = enabled;
            //txtParentSellerID.Enabled = enabled;
            txtParentSellerName.Enabled = enabled;
            hdEnabled.Text = enabled.ToString().ToLower();

            txtReferralSale.Enabled = enabled;
            ddlReferralPercent.Enabled = enabled;

            hidDealStatus.Value = "";

            //檢查是否有已開檔的檔次
            ViewPponDealCollection vpdc = PponFacade.GetOnUseViewPponDealListBySellerGuid(SellerGuid);
            if (vpdc.Count > 0)
            {

                if (!string.IsNullOrEmpty(liReferralSaleBeginTime.Text))
                {
                    DateTime edt = DateTime.MaxValue;
                    DateTime.TryParse(liReferralSaleEndTime.Text, out edt);
                    if (edt < DateTime.Now)
                    {
                        if (DateTime.Now.Subtract(edt).Days > 30)
                        {
                            //結束日滿13個月後，可以再度編輯(主要是為了獎金可以計算到上個月的資料)
                            txtReferralSale.Enabled = enabled;
                            ddlReferralPercent.Enabled = enabled;
                        }
                        else
                        {
                            txtReferralSale.Enabled = false;
                            ddlReferralPercent.Enabled = false;
                        }
                    }
                    else
                    {
                        //若 B 欄資料寫入後，且 E (獎金計算區間)  已至開檔日期(含)，則無法再變更
                        txtReferralSale.Enabled = false;
                        ddlReferralPercent.Enabled = false;
                    }
                }

            }

            // 匯款資訊
            txtCompanyName.Enabled = finance_enabled;
            txtCompanyBossName.Enabled = finance_enabled;
            txtCompanyID.Enabled = finance_enabled;
            chkNoCompanyIDCheck.Visible = finance_enabled;
            ddlBankCode.Enabled = finance_enabled;
            ddlBranchCode.Enabled = finance_enabled;
            txtSignCompanyID.Enabled = finance_enabled;
            txtAccountId.Enabled = finance_enabled;
            txtAccount.Enabled = finance_enabled;
            txtFinanceName.Enabled = finance_enabled;
            txtFinanceTel.Enabled = finance_enabled;
            txtFinanceEmail.Enabled = finance_enabled;
            FinancePersonSync.Visible = finance_enabled;
            ddlRemittanceType.Enabled = finance_enabled;

            rbRecordInvoice.Enabled = finance_enabled;
            rbRecordNoTaxInvoice.Enabled = finance_enabled;
            rbRecordReceipt.Enabled = finance_enabled;
            rbRecordOthers.Enabled = finance_enabled;

            btnSave.Visible = enabled | finance_enabled;
            SaveHolder.Visible = enabled | finance_enabled;

            rdlStatus.Enabled = enabled;
            rdlcloseDownStatus.Enabled = enabled;
            formCloseDownDate.Enabled = enabled;
            chkNoLocation.Enabled = enabled;
            btnSaveLocation.Visible = enabled;
        }

        private string CheckUrlFormat(string url)
        {
            url = HttpUtility.UrlDecode(url);
            if (!string.IsNullOrWhiteSpace(url))
            {
                if (url.StartsWith("http"))
                {
                    return url.Trim();
                }
                else
                {
                    return "http://" + url.Trim();
                }
            }

            return url;
        }

        public Seller GetSellerData(Seller s)
        {
            // 聯絡資訊
            s.SellerName = ProposalFacade.RemoveSpecialCharacter(txtSellerName.Text);
            if (!string.IsNullOrEmpty(ddlSellerLevel.SelectedValue))
            {
                SellerLevel level = SellerLevel.TryParse(hdSellerLevel.Value, out level) ? level : SellerLevel.None;
                s.SellerLevel = (int)level;
            }
            s.SellerLevelDetail = hdSellerLevelDetail.Value;
            int township = int.TryParse(hidTownshipId.Value, out township) ? township : 200;
            s.CityId = township;
            s.SellerAddress = txtCompanyAddress.Text.Trim();
            s.SellerBossName = txtSellerBossName.Text.Trim();
            //s.SellerContactPerson = txtContactPersonName.Text;
            //s.SellerTel = txtSellerTel.Text;
            //s.SellerMobile = txtSellerMobile.Text;
            //s.SellerFax = txtSellerFax.Text;
            //s.SellerEmail = txtContactPersonEmail.Text;
            //s.ReturnedPersonName = txtReturnedPersonName.Text;
            //s.ReturnedPersonTel = txtReturnedPersonTel.Text;
            //s.ReturnedPersonEmail = txtReturnedPersonEmail.Text;
            s.SellerDescription = txtOthers.Text.Trim();

            //店家來源
            s.SellerFrom = short.Parse(ddlSellerFrom.SelectedValue);
            //店家屬性
            var listProperty = from i in chkSellerPorperty.Items.Cast<ListItem>()
                               where ((ListItem)i).Selected
                               select i.Value;


            s.SellerPorperty = string.Join(",", listProperty);
            //上層公司
            Guid parentSellerId = Guid.Empty;
            Guid.TryParse(hidParentSellerGuid.Value, out parentSellerId);
            _ParentGuid = parentSellerId;

            //顯示狀態
            //營業狀態
            s.IsCloseDown = IsCloseDown;

            if (!IsCloseDown)
            {
                s.StoreStatus = (int)Status;
                s.CloseDownDate = null;
            }
            else
            {
                s.StoreStatus = 1; //倒店 => 隱藏
                s.CloseDownDate = CloseDownDate;
            }

            //轉介人員
            if (!string.IsNullOrEmpty(txtReferralSale.Text.Trim()))
            {
                if (string.IsNullOrEmpty(s.ReferralSale))
                {
                    s.ReferralSaleCreateTime = DateTime.Now;
                }
                s.ReferralSale = txtReferralSale.Text.Trim();
            }
            else
            {
                s.ReferralSaleCreateTime = null;
                s.ReferralSale = null;
                s.ReferralSaleBeginTime = null;
                s.ReferralSaleEndTime = null;
                hidReferralSale.Value = "";
            }


            s.ReferralSalePercent = Convert.ToInt32(ddlReferralPercent.SelectedValue);


            // 匯款資訊
            s.CompanyName = txtCompanyName.Text.Trim();
            s.CompanyBossName = txtCompanyBossName.Text.Trim();
            s.CompanyID = txtCompanyID.Text.Trim();
            if (!string.IsNullOrWhiteSpace(ddlBankCode.SelectedValue))
            {
                s.CompanyBankCode = ddlBankCode.SelectedValue;
                s.CompanyBranchCode = hidBranchCode.Value;
            }
            s.SignCompanyID = txtSignCompanyID.Text.Trim();
            s.CompanyAccount = txtAccountId.Text.Trim();
            s.CompanyAccountName = txtAccount.Text.Trim();
            s.AccountantName = txtFinanceName.Text.Trim();
            s.AccountantTel = txtFinanceTel.Text.Trim();
            s.CompanyEmail = txtFinanceEmail.Text.Trim();
            if (ddlRemittanceType.SelectedValue != "-1")
            {
                s.RemittanceType = int.Parse(ddlRemittanceType.SelectedValue);
            }
            else
            {
                s.RemittanceType = null;
            }
            if (!string.IsNullOrEmpty(hdContacts.Text))
            {
                s.Contacts = hdContacts.Text;
                List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(s.Contacts);
                if (contacts != null)
                {
                    List<string> listContact = new List<string>();
                    foreach (Seller.MultiContracts c in contacts)
                    {
                        if (int.Parse(c.Type) == (int)ProposalContact.Normal)
                        {
                            //一般
                            if (!listContact.Contains(ProposalContact.Normal.ToString()))
                            {
                                s.SellerContactPerson = c.ContactPersonName;
                                s.SellerTel = c.SellerTel;
                                s.SellerMobile = c.SellerMobile;
                                s.SellerFax = c.SellerFax;
                                s.SellerEmail = c.ContactPersonEmail;

                                listContact.Add(ProposalContact.Normal.ToString());
                            }
                        }
                        if (int.Parse(c.Type) == (int)ProposalContact.ReturnPerson)
                        {
                            //退貨
                            if (!listContact.Contains(ProposalContact.ReturnPerson.ToString()))
                            {
                                s.ReturnedPersonName = c.ContactPersonName;
                                s.ReturnedPersonTel = c.SellerTel;
                                s.ReturnedPersonEmail = c.ContactPersonEmail;

                                listContact.Add(ProposalContact.ReturnPerson.ToString());
                            }
                        }
                    }

                    //檢查是否資料有刪除
                    if (!listContact.Contains(ProposalContact.Normal.ToString()))
                    {
                        //沒有一般
                        s.SellerContactPerson = "";
                        s.SellerTel = "";
                        s.SellerMobile = "";
                        s.SellerFax = "";
                        s.SellerEmail = "";
                    }
                    if (!listContact.Contains(ProposalContact.ReturnPerson.ToString()))
                    {
                        //沒有退貨
                        s.ReturnedPersonName = "";
                        s.ReturnedPersonTel = "";
                        s.ReturnedPersonEmail = "";
                    }
                }
            }
            else
            {
                s.Contacts = hdContacts.Text;
                //一般
                s.SellerContactPerson = "";
                s.SellerTel = "";
                s.SellerMobile = "";
                s.SellerFax = "";
                s.SellerEmail = "";
                s.SellerDescription = "";
                //退貨
                s.ReturnedPersonName = "";
                s.ReturnedPersonTel = "";
                s.ReturnedPersonEmail = "";
            }

            //發票來源及內容
            if (ReceiptType != 99)
                s.VendorReceiptType = ReceiptType;
            s.Othermessage = txtAccountingMessage.Text;

            return s;
        }

        public Seller GetSellerLocationData(Seller s)
        {
            s.StoreTel = chkNoLocation.Checked == true ? null : txtStoreTel.Text;
            //地址
            int? value = 0;
            if (value == 0)
                value = null;

            int cityship = int.TryParse(ddlLocationCity.SelectedValue, out cityship) ? cityship : 0;
            s.StoreCityId = chkNoLocation.Checked == true ? value : cityship;
            int township = int.TryParse(hidLocationTownshipId.Value, out township) ? township : 0;
            s.StoreTownshipId = chkNoLocation.Checked == true ? value : township;
            s.StoreAddress = chkNoLocation.Checked == true ? null : txtLocationAddress.Text;
            //交通資訊
            s.Mrt = chkNoLocation.Checked == true ? null : txtMrt.Text;
            s.Car = chkNoLocation.Checked == true ? null : txtCar.Text;
            s.Bus = chkNoLocation.Checked == true ? null : txtBus.Text;
            s.OtherVehicles = chkNoLocation.Checked == true ? null : txtOV.Text;
            //網站
            s.WebUrl = chkNoLocation.Checked == true ? null : CheckUrlFormat(txtWebUrl.Text);
            s.FacebookUrl = chkNoLocation.Checked == true ? null : CheckUrlFormat(txtFBUrl.Text);
            s.BlogUrl = chkNoLocation.Checked == true ? null : CheckUrlFormat(txtBlogUrl.Text);
            s.OtherUrl = chkNoLocation.Checked == true ? null : CheckUrlFormat(tbOtherUrl.Text);
            //營業時間
            s.OpenTime = chkNoLocation.Checked == true ? null : BusinessHour;
            //公休日
            s.CloseDate = chkNoLocation.Checked == true ? null : CloseDateInformation;
            //備註
            s.StoreRemark = chkNoLocation.Checked == true ? null : txtRemark.Text;
            //刷卡
            s.CreditcardAvailable = chkCreditcardAvailable.Checked;
            //預約
            s.IsOpenBooking = chkIsOpenBooking.Checked;
            s.IsOpenReservationSetting = chkIsOpenBooking.Checked == false ? false : rdlIsOpenBooking.SelectedValue == "0" ? false : true;

            //填入經緯度
            if (!chkNoLocation.Checked)
            {
                if (township > 0 && !string.IsNullOrEmpty(s.StoreAddress))
                {
                    KeyValuePair<string, string> latitude_longitude = new KeyValuePair<string, string>();
                    latitude_longitude = LocationFacade.GetArea(CityManager.CityTownShopStringGet(township) + s.StoreAddress);
                    s.Coordinate = LocationFacade.GetGeographyWKT(latitude_longitude.Key, latitude_longitude.Value);
                }
            }
            return s;
        }
        #endregion

        #region public method
        public string GetContactType(string value, string source)
        {
            if (value == source)
            {
                return "selected";
            }
            return "";
        }
        #endregion

        [WebMethod]
        public static dynamic BankInfoGetBranchList(string id)
        {
            return VourcherFacade.BankInfoGetBranchList(id).Select(x => new { BranchNo = x.BranchNo, BranchName = x.BranchNo + " " + x.BranchName });
        }
        [WebMethod]
        public static bool DeleteContractFile(string guid)
        {
            Guid _guid = Guid.Empty;
            Guid.TryParse(guid, out _guid);
            SellerFacade.DeleteContractFile(_guid);
            return true;
        }

        [WebMethod]
        public static bool SaveLog(Guid sid, string changeLog, string userName)
        {
            SellerFacade.SellerLogSet(sid, changeLog, userName);
            return true;
        }

        [WebMethod]
        public static bool SaveManageLog(Guid sid, short developStatus, short sellerGrade, string changeLog, string userName)
        {
            SellerFacade.SellerManageLogSet(sid, developStatus, sellerGrade, changeLog, userName);
            return true;
        }

        [WebMethod]
        public static List<AjaxResponseResult> GetSalesmanNameArray(string userName)
        {
            List<AjaxResponseResult> res = new List<AjaxResponseResult>();
            if (!string.IsNullOrEmpty(userName) && userName.Length >= 2)
            {
                List<string> emps = SellerFacade.GetSalesmanNameArray(userName);
                foreach (string emp in emps)
                {
                    res.Add(new AjaxResponseResult
                    {
                        Value = emp,
                        Label = emp
                    });
                }
            }

            return res;
        }
        [WebMethod]
        public static List<AjaxResponseResult> GetEmployeeNameArray(string userName)
        {
            List<AjaxResponseResult> res = new List<AjaxResponseResult>();
            if (!string.IsNullOrEmpty(userName) && userName.Length >= 2)
            {
                List<string> emps = SellerFacade.GetEmployeeNameArray(userName);
                foreach (string emp in emps)
                {
                    res.Add(new AjaxResponseResult
                    {
                        Value = emp,
                        Label = emp
                    });
                }
            }

            return res;
        }
        [WebMethod]
        public static List<AjaxResponseResult> GetSellerNameArray(string sellerName)
        {

            List<AjaxResponseResult> res = new List<AjaxResponseResult>();
            res = SellerFacade.GetSellerNameArray(sellerName);

            return res;
        }


        [WebMethod]
        public static string IsCompany(string CompanyName, string SellerGuid)
        {
            if (!string.IsNullOrEmpty(CompanyName))
            {
                Seller seller = SellerFacade.IsCompany(CompanyName, SellerGuid);
                if (seller.IsLoaded)
                    return seller.Guid.ToString();
                else
                    return "NoCompany";
            }
            else
                return "";

        }

        [WebMethod]
        public static bool IsChildrenCompany(Guid SellerGuid, Guid CompanyID)
        {
            List<Guid> list = new List<Guid>();
            list = SellerFacade.GetChildrenSellerGuid(SellerGuid).ToList();
            list.Add(SellerGuid);
            if (!list.Contains(CompanyID))
                return true;
            else
                return false;
        }

        [WebMethod]
        public static string GetWeeklyName(string frenquency, string weeklys, string beginTime, string endTime, bool allDay)
        {
            return SellerFacade.GetWeeklyNames(frenquency, weeklys, beginTime, endTime, allDay);
        }

        [WebMethod]
        public static bool SyncBusinessHour(string sellerGuid, string opentime)
        {
            List<Guid> seGuid = new JsonSerializer().Deserialize<List<Guid>>(sellerGuid);
            return SellerFacade.StoreOpenTimeUpdate(seGuid, opentime);
        }
        [WebMethod]
        public static bool SyncCloseDate(string storeGuid, string closedate)
        {
            List<Guid> stGuid = new JsonSerializer().Deserialize<List<Guid>>(storeGuid);
            return SellerFacade.StoreCloseDateUpdate(stGuid, closedate);
        }
        [WebMethod]
        public static string GenTicketId(string UserName)
        {
            string TicketId = Security.MD5Hash(Guid.NewGuid().ToString() + DateTime.Now);
            ProposalFacade.ProposalFileOauthSet(TicketId, UserName);
            return TicketId;
        }

        /// <summary>
        /// 顯示業務資料
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        [WebMethod]
        public static List<MultipleSalesModel> GetMultipleSales(Guid sellerGuid)
        {
            string html = string.Empty;
            List<MultipleSalesModel> model = SellerFacade.SellerSaleGetBySellerGuid(sellerGuid);
            return model;
        }

        /// <summary>
        /// 檢核業務填寫是否正確
        /// </summary>
        /// <param name="sales"></param>
        /// <returns></returns>
        [WebMethod]
        public static string CheckSales(int type, string developeSales, string operationSales)
        {
            return SellerFacade.CheckSales(type, developeSales, operationSales);
        }

        /// <summary>
        /// 商家轉件
        /// </summary>
        /// <param name="sales"></param>
        /// <returns></returns>
        [WebMethod]
        public static void SaveSellerSales(string multipleSalesList, Guid sellerGuid, string userName,bool resetSalesGroup)
        {
            ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();

            List<MultipleSalesModel> multipleSales = new List<MultipleSalesModel>();
            multipleSales = new JsonSerializer().Deserialize<List<MultipleSalesModel>>(multipleSalesList);

            List<string> deSalesName = new List<string>();
            List<string> opSalesName = new List<string>();

            Seller seller = sp.SellerGet(sellerGuid);
            if (!seller.IsLoaded)
            {
                return;
            }

            if (multipleSales.Count() > 0)
            {
                sp.SellerSaleDelete(sellerGuid);
                foreach (var sales in multipleSales)
                {
                    ViewEmployee salesEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, sales.DevelopeSalesEmail);

                    SellerSale s = new SellerSale();
                    s.SellerGuid = sellerGuid;
                    s.SellerSalesId = salesEmp.UserId;
                    s.SalesGroup = sales.SalesGroup;
                    s.SalesType = (int)SellerSalesType.Develope;
                    s.CreateId = userName;
                    s.CreateTime = DateTime.Now;
                    sp.SellerSaleSet(s);
                    deSalesName.Add(salesEmp.EmpName);

                    if(sales.SalesGroup == 1)
                    {
                        seller.SalesId = salesEmp.UserId;
                        seller.SellerSales = salesEmp.EmpName;
                        sp.SellerSet(seller);
                    }

                    salesEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, sales.OperationSalesEmail);
                    if (salesEmp.IsLoaded)
                    {
                        s = new SellerSale();
                        s.SellerGuid = sellerGuid;
                        s.SellerSalesId = salesEmp.UserId;
                        s.SalesGroup = sales.SalesGroup;
                        s.SalesType = (int)SellerSalesType.Operation;
                        s.CreateId = userName;
                        s.CreateTime = DateTime.Now;
                        sp.SellerSaleSet(s);
                        opSalesName.Add(salesEmp.EmpName);
                    }
                    
                }

                SellerFacade.SellerLogSet(sellerGuid, string.Format("變更 {0} 為 {1}", "開發業務", string.Join("、", deSalesName)), userName);
                SellerFacade.SellerLogSet(sellerGuid, string.Format("變更 {0} 為 {1}", "經營業務", string.Join("、", opSalesName)), userName);
            }


            if (resetSalesGroup)
            {
                SellerFacade.ResetSalesGroup(new List<Guid> { sellerGuid });
            }



        }

        /// <summary>
        /// 檢核使用者是否於多組頁中
        /// </summary>
        /// <param name="multipleSalesList"></param>
        /// <param name="sellerGuid"></param>
        /// <param name="userName"></param>
        [WebMethod]
        public static bool CheckMultipleSales(Guid sellerGuid, string userName)
        {
            List<MultipleSalesModel> model = SellerFacade.SellerSaleGetBySellerGuid(sellerGuid);
            var develope = model.Where(x => x.DevelopeSalesEmail == userName);
            var operation = model.Where(x => x.OperationSalesEmail == userName);

            if ((develope.Count() > 0 && operation.Count() > 0 && develope.Except(operation).Count() > 0) || develope.Count() > 1 || operation.Count() > 1)
            {
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// 重複業務建立提案單
        /// </summary>
        /// <param name="multipleSalesList"></param>
        /// <param name="sellerGuid"></param>
        /// <param name="userName"></param>
        [WebMethod]
        public static void MultipleCreateProposal(Guid sellerGuid, bool _IsTravelProposal, bool _IsPBeautyProposal, int _ProposalDeliveryType)
        {
            SellerContent c = new Sal.SellerContent();
            c.MCreateProposal(sellerGuid, _IsTravelProposal, _IsPBeautyProposal, _ProposalDeliveryType);
            
        }

        /// <summary>
        /// 商家提案售價顯示管理
        /// </summary>
        /// <param name="multipleSalesList"></param>
        /// <param name="sellerGuid"></param>
        /// <param name="userName"></param>
        [WebMethod]
        public static bool SaveProposalItemPriceContent(string sellerGuid, List<string> content, string userName)
        {
            return SellerFacade.SaveProposalItemPriceContent(sellerGuid, content, userName);
        }

        public void MCreateProposal(Guid sellerGuid, bool _IsTravelProposal, bool _IsPBeautyProposal, int _ProposalDeliveryType)
        {
            btnCreateProposal_Click(null, null);
            IsTravelProposal = IsTravelProposal;
            IsPBeautyProposal = IsPBeautyProposal;
            ProposalDeliveryType = _ProposalDeliveryType;
            

            //btnCreateProposal_Click(null, null);

            this.CreateProposal(this, new DataEventArgs<Guid>(sellerGuid));
        }

        

        protected void btnPassFamily_Click(object sender, EventArgs e)
        {
            if (ISPApply != null)
                ISPApply(sender, new DataEventArgs<KeyValuePair<bool, ServiceChannel>>(new KeyValuePair<bool, ServiceChannel>(false, ServiceChannel.FamilyMart)));
        }

        protected void btnRejectFamily_Click(object sender, EventArgs e)
        {
            if (ISPReject != null)
                ISPReject(sender, new DataEventArgs<KeyValuePair<string, ServiceChannel>>(new KeyValuePair<string, ServiceChannel>(tbRejectReasonFamily.Text, ServiceChannel.FamilyMart)));

            Page.ClientScript.RegisterStartupScript(this.GetType(), "anchor", "location.hash = '#aISPManage';", true);
        }

        protected void btnUndoFamily_Click(object sender, EventArgs e)
        {

        }

        protected void btnUpdateFamily_Click(object sender, EventArgs e)
        {
            if (ISPApply != null)
                ISPApply(sender, new DataEventArgs<KeyValuePair<bool, ServiceChannel>>(new KeyValuePair<bool, ServiceChannel>(true, ServiceChannel.FamilyMart)));
        }

        protected void btnPassSeven_Click(object sender, EventArgs e)
        {
            if (ISPApply != null)
                ISPApply(sender, new DataEventArgs<KeyValuePair<bool, ServiceChannel>>(new KeyValuePair<bool, ServiceChannel>(false, ServiceChannel.SevenEleven)));
        }

        protected void btnRejectSeven_Click(object sender, EventArgs e)
        {
            if (ISPReject != null)
                ISPReject(sender, new DataEventArgs<KeyValuePair<string, ServiceChannel>>(new KeyValuePair<string, ServiceChannel>(tbRejectReasonSeven.Text, ServiceChannel.SevenEleven)));

            Page.ClientScript.RegisterStartupScript(this.GetType(), "anchor", "location.hash = '#aISPManage';", true);
        }

        protected void btnUndoSeven_Click(object sender, EventArgs e)
        {

        }

        protected void btnUpdateSeven_Click(object sender, EventArgs e)
        {
            if (ISPApply != null)
                ISPApply(sender, new DataEventArgs<KeyValuePair<bool, ServiceChannel>>(new KeyValuePair<bool, ServiceChannel>(true, ServiceChannel.SevenEleven)));
        }

        protected ViewVbsInstorePickupCollection GetModel()
        {
            if (ViewVbsInstorePickupFamily.IsLoaded)
            {
                //待審核
                btnRejectFamily.Enabled = btnPassFamily.Enabled = IsISPVerifyFamily;
                //開通完成
                divIspVerifyFamily.Visible = !IsISPEnabledFamily;
                divIspUpdateFamily.Visible = IsISPEnabledFamily;
            }

            if (ViewVbsInstorePickupSeven.IsLoaded)
            {
                //待審核
                btnRejectSeven.Enabled = btnPassSeven.Enabled = IsISPVerifySeven;
                //開通完成
                divIspVerifySeven.Visible = !IsISPEnabledSeven;
                divIspUpdateSeven.Visible = IsISPEnabledSeven;
            }
            return ViewVbsInstorePickupViewData;
        }

        [WebMethod]
        public static string GetTestTag(string key, int channel)
        {
            var data = new
            {
                storeKey = key,
                serviceChannelCode = channel
            };

            var result = ISPFacade.ISPApiRequest(Newtonsoft.Json.JsonConvert.SerializeObject(data), "GetTestTag", false);
            return new JsonSerializer().Serialize(new { Data = result.Data });
        }
    }
}
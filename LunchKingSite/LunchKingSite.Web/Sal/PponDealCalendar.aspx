﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="PponDealCalendar.aspx.cs" Inherits="LunchKingSite.Web.Sal.PponDealCalendar" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Src="~/Sal/ProposalMenu.ascx" TagName="ProposalMenu" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function SelfShow(obj) {
            if ($(obj).is(':checked')) {
                $('.otherSales').parent().parent().hide();
            } else {
                $('.otherSales').parent().parent().show();
                Check($('[name=checkitem]:checked'));
            }
        }

        function Check(obj) {
            if ($(obj).is(':checked') && $(obj).attr('id') != 'none') {
                $('.deal_item').hide();
                $('.' + $(obj).attr('id')).parent().parent().show();
            } else {
                $('.deal_item').show();
            }

            if ($('#showSelf').is(':checked')) {
                $('.otherSales').parent().parent().hide();
            }
        }

        function HideContent(obj) {
            var a = $(obj).parent().parent().children().slice(3);
            if (a.is(':visible') == true) {
                a.css('display', 'none');
                $(obj).attr('src', '../Themes/default/images/17Life/G2/ArrowUp.png');
            }

            else {
                a.css('display', 'block');
                $(obj).attr('src', '../Themes/default/images/17Life/G2/ArrowDown.png');
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <style>
        .mc-navbar .mc-navbtn{
            width:10%;
        }
    </style>
    <uc2:ProposalMenu ID="ProposalMenu" runat="server" ></uc2:ProposalMenu>
    <asp:HiddenField ID="hidEmpUserId" runat="server" />
    <div class="mc-content">
        <h1 class="rd-smll">檔期表</h1>
        <hr class="header_hr">
        <p class="note" style="font-size: small; color: #BF0000; float: right;">
            <input id="none" type="radio" onclick="Check(this);" name="checkitem" />
            <label for="none">無</label>
            <input id="fa-file-text-o" type="radio" onclick="Check(this);" name="checkitem" />
            <i class="fa fa-file-text-o fw"></i>&nbsp;<label for="fa-file-text-o">紙本合約未回</label>
            <input id="fa-camera" type="radio" onclick="Check(this);" name="checkitem" />
            <i class="fa fa-camera fw"></i>&nbsp;<label for="fa-camera">尚未攝影確認</label>
            <input id="fa-eye" type="radio" onclick="Check(this);" name="checkitem" />
            <i class="fa fa-eye fw"></i>&nbsp;<label for="fa-eye">尚未頁面確認</label>
            <input id="fa-usd" type="radio" onclick="Check(this);" name="checkitem" />
            <i class="fa fa-usd fw"></i>&nbsp;<label for="fa-usd">財務資料尚未確認</label>
        </p>
        <asp:PlaceHolder ID="divShowSelf" runat="server" Visible="false">
            <p class="note" style="font-size: small; color: #666; float: left; margin-right: 20px;">
                <input id="showSelf" type="checkbox" onclick="SelfShow(this);" />
                <label for="showSelf">只顯示我的檔次資訊</label>
            </p>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="divDeptList" runat="server" Visible="false">
            <div class="grui-form">
                <div class="form-unit">
                    <div class="data-input rd-data-input" style="margin-left: 0px">
                        <asp:DropDownList ID="ddlDept" runat="server" CssClass="select-wauto" Width="200px" OnSelectedIndexChanged="ddlDept_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                        <br />
                        <asp:DropDownList ID="ddlDealType1" runat="server" ClientIDMode="Static" OnSelectedIndexChanged="ddlDealType1_SelectedIndexChanged" AutoPostBack="true" Width="100px" />
                        <asp:DropDownList ID="ddlDealType2" runat="server" ClientIDMode="Static" OnSelectedIndexChanged="ddlDealType2_SelectedIndexChanged" AutoPostBack="true" Width="100px" />
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:Repeater ID="rptMonth" runat="server" OnItemDataBound="rptMonth_ItemDataBound">
            <ItemTemplate>
                <h1 class="rd-smll">
                    <asp:Literal ID="liMonth" runat="server"></asp:Literal>
                    <span style="font-size: small; color: #BF0000">
                        <asp:Literal ID="liTotalDealCount" runat="server"></asp:Literal>
                    </span>
                    <span style="font-size: small; color: #BF0000;float:right";>
                        <asp:ImageButton ID="btnPrevious" runat="server" ImageUrl="~/Themes/default/images/17Life/ipeenX17/arrow-bg-left-hover.png" OnClick="btnPrevious_Click" />
                        <asp:ImageButton ID="btnNext" runat="server" ImageUrl="~/Themes/default/images/17Life/ipeenX17/arrow-bg-right-hover.png" OnClick="btnNext_Click" />
                    </span>
                </h1>
                <hr class="header_hr">
                <div id="mc-table">
                    <asp:Repeater ID="rptWeek" runat="server" OnItemDataBound="rptWeek_ItemDataBound">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                                <tr class="rd-C-Hide">
                                    <th class="OrderName" style="width:130px">星期日
                                    </th>
                                    <th class="OrderName" style="width:130px">星期一
                                    </th>
                                    <th class="OrderName" style="width:130px">星期二
                                    </th>
                                    <th class="OrderName" style="width:130px">星期三
                                    </th>
                                    <th class="OrderName" style="width:130px">星期四
                                    </th>
                                    <th class="OrderName" style="width:130px">星期五
                                    </th>
                                    <th class="OrderName" style="width:130px">星期六
                                    </th>
                                    
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="mc-tableContentITEM">
                                <asp:Repeater ID="rptDate" runat="server" OnItemDataBound="rptDate_ItemDataBound">
                                    <ItemTemplate>
                                        <td class="OrderName" style="vertical-align: top;text-align:left;word-break: break-all;max-width:350px">
                                            <asp:PlaceHolder ID="divDate" runat="server" Visible="false">
                                                <p style="border-style: solid; border-radius: 10px; width: 24px; height: 24px; background-color: #FFCCBF; border-width: 0px; text-align: center; color: #666666;">
                                                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                                                </p>
                                            </asp:PlaceHolder>
                                            <p style="float: right">
                                                <asp:ImageButton ID="btnHideContent" runat="server" ImageUrl="~/Themes/default/images/17Life/G2/ArrowDown.png" OnClientClick="HideContent(this); return false;" Visible="false" />
                                            </p>
                                            <span class="rd-Detailtitle">
                                                <asp:Literal ID="liWeekName" runat="server"></asp:Literal></span><p></p>
                                            <asp:Repeater ID="rptDeal" runat="server" OnItemDataBound="rptDeal_ItemDataBound">
                                                <ItemTemplate>
                                                    <span class="deal_item"  >
                                                        <asp:HyperLink ID="hkPid" runat="server" Target="_blank"></asp:HyperLink><br />
                                                        <asp:HyperLink ID="hkItemName" runat="server" Target="_blank"></asp:HyperLink><br />
                                                        <asp:Label ID="lblOrderTimeE" runat="server" ForeColor="#666666"></asp:Label>
                                                        <p style="color: #BF0000; font-size: 20px;">
                                                            <asp:Literal ID="liWarning" runat="server"></asp:Literal>
                                                        </p>
                                                        <p style="color: darkblue; text-decoration: underline;">
                                                            <asp:Literal ID="liSalesName" runat="server"></asp:Literal>
                                                        </p>
                                                    </span>
                                                     <br />
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>

                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <br />
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

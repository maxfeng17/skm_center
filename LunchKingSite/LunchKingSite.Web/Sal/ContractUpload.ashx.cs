﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.ModelPartial;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace LunchKingSite.Web.Sal
{
    /// <summary>
    /// Summary description for ContractUpload
    /// </summary>
    public class ContractUpload : IHttpHandler
    {
        private IHumanProvider hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        private ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();        
        private static ILog logger = LogManager.GetLogger(typeof(ContractUpload));
        public void ProcessRequest(HttpContext context)
        {
            AjaxSellerContractResult result = new AjaxSellerContractResult();
            string proposalId = "";
            string sellerId = "";
            try
            {
                if (string.IsNullOrEmpty(context.User.Identity.Name))
                {
                    throw new Exception("使用者未登入使用。");
                }
                
                if (context.Request.Files.Count > 0)
                {
                    #region 商家合約
                    if (context.Request.Form["UploadType"] == "Accessory")
                    {
                        string UploadType = context.Request.Form["UploadType"];
                        sellerId = context.Request.Form["SellerId"];
                        string sellerGuid = context.Request.Form["SellerGuid"];
                        string contractFileType = context.Request.Form["contractFileType"];
                        string contractFileMemo = context.Request.Form["contractFileMemo"];
                        string contractDueDate = context.Request.Form["ContractDueDate"];
                        string contractPartyBContact = context.Request.Form["ContractPartyBContact"];
                        string contractVersion = context.Request.Form["ContractVersion"];
                        string ftpFolder = EmployeeDeptGet(context.User.Identity.Name);
                        string fileName = "";

                        if (sellerId == "" || sellerGuid == "" || contractFileType == "")
                        {
                            throw new Exception("上傳失敗");
                        }
                        if (ftpFolder == "")
                        {
                            throw new Exception("FTP尚未建立資料夾，請聯絡IT人員");
                        }

                        ftpFolder = "Accessory_contract/" + ftpFolder;

                        string FolderFile = Upload(context, sellerId, sellerGuid, contractFileType, ref fileName, ftpFolder, UploadType);
                        contractVersion = contractVersion.ToUpper();
 
                        /*
                         * 寫入DB
                         * */
                        Guid _sellerGuid = Guid.Empty;
                        Guid.TryParse(sellerGuid, out _sellerGuid);
                        SellerContractFile scf = new SellerContractFile();
                        scf.Guid = Guid.NewGuid();
                        scf.SellerGuid = _sellerGuid;
                        scf.FileName = fileName;
                        scf.FilePath = FolderFile;
                        scf.CreateId = context.User.Identity.Name;
                        scf.CreateTime = DateTime.Now;
                        scf.Status = 0;
                        scf.ContractType = int.Parse(contractFileType);
                        scf.Memo = contractFileMemo;
                        if (contractDueDate != "")
                            scf.DueDate = Convert.ToDateTime(contractDueDate);
                        scf.PartyBContact = contractPartyBContact;
                        scf.Version = contractVersion;
                        sp.SellerContractFileSet(scf);

                        //上傳熟客卡主約，順便勾選熟客卡的店家屬性
                        if (contractFileType == ((int)SellerContractType.Main).ToString() || contractFileType == ((int)SellerContractType.PCPMain).ToString())
                        {
                            Seller seller = sp.SellerGet(_sellerGuid);
                            string _SellerPorperty = seller.SellerPorperty;
                            string[] listProperty = _SellerPorperty.Split(",");
                            bool isExist = false;
                            List<string> proList = new List<string>();
                            foreach (string pro in listProperty)
                            {
                                if (string.IsNullOrEmpty(pro))
                                {
                                    continue;
                                }
                                proList.Add(pro);
                                if (pro == ((int)ProposalSellerPorperty.RegularsCard).ToString())
                                {
                                    isExist = true;
                                }
                            }
                            if (!isExist)
                            {
                                proList.Add(((int)ProposalSellerPorperty.RegularsCard).ToString());
                            }
                            seller.SellerPorperty = string.Join(",", proList);
                            sp.SellerSet(seller);
                        }

                        result.IsSuccess = true;
                    }
                    #endregion
                    #region 提案單合約
                    else if (context.Request.Form["UploadType"] == "Proposal")
                    {
                        string UploadType = context.Request.Form["UploadType"];
                        sellerId = context.Request.Form["SellerId"];
                        proposalId = context.Request.Form["ProposalId"];
                        string contractFileType = context.Request.Form["ContractFileType"];
                        string contractFileMemo = context.Request.Form["ContractFileMemo"];
                        string contractDueDate = context.Request.Form["ContractDueDate"];
                        string contractPartyBContact = context.Request.Form["ContractPartyBContact"];
                        string contractVersion = context.Request.Form["ContractVersion"];
                        string ftpFolder = EmployeeDeptGet(context.User.Identity.Name);
                        string fileName = "";

                        if (sellerId == "" || proposalId == "" || contractFileType == "")
                        {
                            throw new Exception("上傳失敗");
                        }
                        if (ftpFolder == "")
                        {
                            throw new Exception("FTP尚未建立資料夾，請聯絡IT人員");
                        }

                        ftpFolder = "Proposal_contract/" + ftpFolder;

                        string FolderFile = Upload(context, sellerId + "-" + proposalId, proposalId, contractFileType, ref fileName, ftpFolder, UploadType);
                        contractVersion = contractVersion.ToUpper();

                        /*
                         * 寫入DB
                         * */
                        ProposalContractFile pcf = new ProposalContractFile();
                        pcf.Guid = Guid.NewGuid();
                        pcf.ProposalId = Convert.ToInt32(proposalId);
                        pcf.FileName = fileName;
                        pcf.FilePath = FolderFile;
                        pcf.Status = 0;
                        pcf.ContractType = int.Parse(contractFileType);
                        pcf.Memo = contractFileMemo;
                        if (contractDueDate != "")
                            pcf.DueDate = Convert.ToDateTime(contractDueDate);
                        pcf.PartyBContact = contractPartyBContact;
                        pcf.Version = contractVersion;
                        pcf.CreateId = context.User.Identity.Name;
                        pcf.CreateTime = DateTime.Now;
                        sp.ProposalContractFileSet(pcf);

                        //一般宅配要自動勾選  其餘用手動勾選
                        Proposal pro = sp.ProposalGet(Convert.ToInt32(proposalId));
                        if (int.Parse(contractFileType) == (int)SellerContractType.Pair && (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType != (int)ProposalDealType.Travel))
                        {
                            pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(true, pro.ListingFlag, ProposalListingFlag.PaperContractCheck));//新版提案單=紙本簽核
                            sp.ProposalSet(pro);
                        }



                        result.IsSuccess = true;
                    }
                    #endregion
                    #region 宅配提案單檔案
                    else if (context.Request.Form["UploadType"] == "SellerFile")
                    {
                        proposalId = context.Request.Form["ProposalId"];
                        string ProposalId = proposalId.ToString().PadLeft(7, '0');

                        try
                        {
                            StringBuilder sb = new StringBuilder();
                            Proposal pro = sp.ProposalGet(Convert.ToInt32(proposalId));
                            if (pro != null && pro.IsLoaded)
                            {
                                Seller seller = sp.SellerGet(pro.SellerGuid);

                                string FolderFile = ""; //單號-商家名稱-品牌名稱
                                if (pro.IsLoaded && pro != null)
                                {
                                    for (int i = 0; i < context.Request.Files.Count; i++)
                                    {
                                        HttpPostedFile file = context.Request.Files[i];
                                        string oriFileName = context.Request.Files[i].FileName;
                                        string _Extension = oriFileName.Substring(oriFileName.LastIndexOf("."));
                                        string realFileName = DateTime.Now.ToString("yyyyMMddHHmmss_") + oriFileName;


                                        if (config.SellerContractIsFtp)
                                        {
                                            //FTP
                                            string ftpFolder = "Seller_Files/" + ProposalId;
                                            SimpleFtpClient cli = new SimpleFtpClient(new Uri(config.SellerContractFtpUri + ftpFolder));
                                            FolderFile = ftpFolder + "/" + realFileName;
                                            if (!cli.CheckDirectoryExist(ftpFolder))
                                            {
                                                cli.MakeDirectory(ftpFolder);
                                            }

                                            cli.SimpleUpload(new HttpPostedFileWrapper(file), ftpFolder, realFileName);

                                        }
                                        else
                                        {
                                            //UNC
                                            string BaseDir = string.IsNullOrEmpty(config.CkFinderBaseDir) ? System.Web.HttpContext.Current.Server.MapPath("~/Images/") : config.CkFinderBaseDir;
                                            string Dir = BaseDir == "" ? "" : string.Format("{0}imagesU/{1}/", BaseDir, ProposalId);

                                            if (!System.IO.Directory.Exists(Dir))
                                            {
                                                System.IO.Directory.CreateDirectory(Dir);
                                            }
                                            string fname = System.IO.Path.Combine(Dir, realFileName);

                                            file.SaveAs(fname);
                                        }

                                        //寫入資料庫
                                        ProposalSellerFile sellerFile = new ProposalSellerFile()
                                        {
                                            ProposalId = pro.Id,
                                            OriFileName = oriFileName,
                                            RealFileName = realFileName,
                                            CreateId = context.User.Identity.Name,
                                            CreateTime = DateTime.Now,
                                            FileStatus = (int)ProposalFileStatus.Nomal,
                                            FileSize = unchecked((int)file.InputStream.Length)
                                        };
                                        sp.ProposalSellerFileSet(sellerFile);



                                        //寫入log
                                        ProposalFacade.ProposalLog(pro.Id, "[新增檔案]：" + oriFileName + "", context.User.Identity.Name, ProposalLogType.Initial);
                                    }

                                    //發送mail
                                    if (Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                                    {
                                        ProposalFacade.ProposalFileChangeMail(pro);
                                    }
                                }
                            }
                        }
                        catch
                        {

                        }
                        finally
                        {
                            result.Message = new JsonSerializer().Serialize(sp.ProposalSellerFileListGet(Convert.ToInt32(ProposalId)).ToList());
                        }
                    } 
                    #endregion
                }
            }
            catch(Exception e) {
                result.IsSuccess = false;
                result.Message = e.Message;
                logger.Error("合約上傳到FTP錯誤/sellerId=" + sellerId + "/Pid=" + proposalId + "/使用者=(" + context.User.Identity.Name + ")", e);
            }

            
            context.Response.Write(new JsonSerializer().Serialize(result));
        }

        private string Upload(HttpContext context, string id, string Dirid, string contractFileType, ref string fileName, string ftpFolder, string UploadType)
        {
            //重新變更檔名(賣家編號-類型-上傳日期)                                
            HttpFileCollection files = context.Request.Files;
            HttpPostedFile file = files[0];
            string extensionName = System.IO.Path.GetExtension(file.FileName);
            string FolderFile = "";

            bool flag = false;
            int idxExt = 1;
            if (UploadType == "Accessory")
                fileName = id + "-" + Helper.GetEnumDescription((SellerContractType)int.Parse(contractFileType)) + "-" + DateTime.Now.ToString("yyyyMMdd") + extensionName;
            else if (UploadType == "Proposal")
                fileName = id + "-" + Helper.GetEnumDescription((SellerContractType)int.Parse(contractFileType)) + "-" + DateTime.Now.ToString("yyyyMMdd") + extensionName;

            if (config.SellerContractIsFtp)
            {
                //FTP
                SimpleFtpClient cli = new SimpleFtpClient(new Uri(config.SellerContractFtpUri + ftpFolder));
                while (!flag)
                {
                    flag = cli.SimpleCheckFileExist(ftpFolder, fileName);
                    if (flag)
                    {
                        //如果ftp已有檔案，將檔案改名為(N)
                        if (UploadType == "Accessory")
                            fileName = id + "-" + Helper.GetEnumDescription((SellerContractType)int.Parse(contractFileType)) + "-" + DateTime.Now.ToString("yyyyMMdd") + "(" + idxExt + ")" + extensionName;
                        else if (UploadType == "Proposal")
                            fileName = id + "-" + Helper.GetEnumDescription((SellerContractType)int.Parse(contractFileType)) + "-" + DateTime.Now.ToString("yyyyMMdd") + "(" + idxExt + ")" + extensionName;

                        idxExt++;
                        flag = false;
                    }
                    else
                    {
                        //無檔案則直接上傳
                        break;
                    }
                    if (idxExt > 20)
                    {
                        //至多嘗試20次，免得無限迴圈
                        break;
                    }
                }
                FolderFile = ftpFolder + "/" + fileName;
                cli.SimpleUpload(new HttpPostedFileWrapper(file), ftpFolder, fileName);
            }
            else
            {
                //UNC
                string BaseDir = string.IsNullOrEmpty(config.CkFinderBaseDir) ? System.Web.HttpContext.Current.Server.MapPath("~/Images/") : config.CkFinderBaseDir;
                string Dir = BaseDir == "" ? "" : string.Format("{0}imagesU/{1}/", BaseDir, Dirid);

                if (!System.IO.Directory.Exists(Dir))
                {
                    System.IO.Directory.CreateDirectory(Dir);
                }
                string fname = System.IO.Path.Combine(Dir, fileName);

                FolderFile = Dir.Replace(context.Server.MapPath("~\\"), "") + "/" + fileName;

                file.SaveAs(fname);
            }
            return FolderFile;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// 判斷人員屬於哪個單位
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        private string EmployeeDeptGet(string UserName)
        {
            Employee emp = hp.EmployeeGet(UserName);
            string ftpFolder = "";
            if (emp != null)
            {
                EmployeeChildDept dept = EmployeeChildDept.TryParse(emp.DeptId, out dept) ? dept : EmployeeChildDept.S001;
                switch (dept)
                {
                    case EmployeeChildDept.S001:    //業務部(台北)
                        ftpFolder = "taipei";
                        break;
                    case EmployeeChildDept.S002:    //業務部(桃園)
                        ftpFolder = "taoyuan";
                        break;
                    case EmployeeChildDept.S003:    //業務部(竹苗)
                        ftpFolder = "hsinchu";
                        break;
                    case EmployeeChildDept.S004:    //業務部(中彰)
                        ftpFolder = "taichung";
                        break;
                    case EmployeeChildDept.S005:    //業務部(高屏)
                        ftpFolder = "kaohsiung";
                        break;
                    case EmployeeChildDept.S006:    //業務部(嘉南)
                        ftpFolder = "tainan";
                        break;
                    case EmployeeChildDept.S007:    //業務部(玩美)
                        ftpFolder = "beauty";
                        break;
                    case EmployeeChildDept.S008:    //業務部(旅遊)
                        ftpFolder = "travel";
                        break;
                    case EmployeeChildDept.S012:    //業務部(品生活)
                        ftpFolder = "piinlife";
                        break;
                    case EmployeeChildDept.S011:    //業務部(KA)
                        ftpFolder = "ka";
                        break;
                    case EmployeeChildDept.S010:    //業務部(全國宅配)
                        ftpFolder = "delivery";
                        break;
                    case EmployeeChildDept.S009:    //業務部(福利)
                        ftpFolder = "welfare";
                        break;
                    case EmployeeChildDept.S013:    //系統業務部
                        ftpFolder = "syssales";
                        break;
                    case EmployeeChildDept.S014:    //通路發展部
                        ftpFolder = "channeldev";
                        break;
                    default:
                        break;
                }
            }
            return ftpFolder;
        }
    }
}
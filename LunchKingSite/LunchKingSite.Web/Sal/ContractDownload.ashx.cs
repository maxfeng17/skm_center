﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace LunchKingSite.Web.Sal
{
    /// <summary>
    /// Summary description for ContractDownload
    /// </summary>
    public class ContractDownload : IHttpHandler
    {
        private ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public void ProcessRequest(HttpContext context)
        {
            string guid = context.Request["guid"];
            string id = context.Request["id"];
            string ticketId = context.Request["ticketId"];

            Guid _guid = Guid.Empty;
            Guid.TryParse(guid, out _guid);

            if (string.IsNullOrEmpty(context.User.Identity.Name))
            {
                throw new Exception("使用者未登入使用。");
            }
            if (string.IsNullOrEmpty(ticketId))
            {
                throw new Exception("ticketId異常。");
            }
            else
            {
                ProposalFileOauth oauth =sp.ProposalFileOauthGetByTicketId(ticketId);
                if (oauth == null || !oauth.IsLoaded)
                {
                    throw new Exception("無效的ticketId。");
                }
                else
                {
                    TimeSpan t = DateTime.Now - oauth.CreateTime;
                    if ((t.Hours * 60 * 60 + t.Minutes * 60 + t.Seconds) >= 10)
                    {
                        throw new Exception("ticketId過期。");
                    }
                }
            }

            if (context.Request["UploadType"] =="Accessory")
            {
                if (_guid != Guid.Empty)
                {
                    SellerContractFile scf = sp.SellerContractFileGetByGuid(_guid);

                    Download(context, scf.FilePath, scf.FileName);
                }
            }
            else if (context.Request["UploadType"] == "Proposal")
            {
                if (_guid != Guid.Empty)
                {
                    ProposalContractFile scf = sp.ProposalContractFileGetByGuid(_guid);

                    Download(context, scf.FilePath, scf.FileName);
                }
            }
            else if (context.Request["UploadType"] == "SellerFile")
            {
                if (!string.IsNullOrEmpty(id))
                {
                    ProposalSellerFile files = sp.ProposalSellerFileGet(Convert.ToInt32(id));
                    string ftpFolder = "Seller_Files/" + files.ProposalId.ToString().PadLeft(7, '0') + "/" + files.RealFileName;
                    string FileName = files.OriFileName;

                    if (config.SellerContractIsFtp)
                    {
                        //Use FTP
                        SimpleFtpClient cli = new SimpleFtpClient(new Uri(config.SellerContractFtpUri + ftpFolder));
                        cli.SimpleDownload(FileName, ftpFolder);
                    }
                    else
                    {
                        //Use Folder
                        string BaseDir = string.IsNullOrEmpty(config.CkFinderBaseDir) ? System.Web.HttpContext.Current.Server.MapPath("~/Images/") : config.CkFinderBaseDir;
                        string Dir = BaseDir == "" ? "" : string.Format("{0}imagesU/{1}/", BaseDir, files.ProposalId.ToString().PadLeft(7, '0'));

                        System.Net.WebClient wc = new System.Net.WebClient();
                        byte[] byteFiles = null;
                        string FilePath = System.IO.Path.Combine(Dir, files.RealFileName);
                        byteFiles = wc.DownloadData(FilePath);
                        string fileName = System.IO.Path.GetFileName(FilePath);
                        context.Response.AddHeader("content-disposition", "attachment;filename=" + HttpContext.Current.Server.UrlEncode(fileName));
                        context.Response.ContentType = "application/octet-stream";
                        context.Response.BinaryWrite(byteFiles);
                        context.Response.End();
                    }
                }
            }
            else if (context.Request["UploadType"] == "DownloadMultiFile")
            {
                if (!string.IsNullOrEmpty(id))
                {
                    string ProposalId = context.Request["ProposalId"];
                    List<string> listIds = new JsonSerializer().Deserialize<List<string>>(id);
                    string tmpFolderName = DateTime.Now.ToString("yyyyMMddHHmmss");

                    string BaseDir = string.IsNullOrEmpty(config.CkFinderBaseDir) ? System.Web.HttpContext.Current.Server.MapPath("~/Images/") : config.CkFinderBaseDir;
                    string sourDir = BaseDir == "" ? "" : string.Format("{0}imagesU/{1}/", BaseDir, ProposalId.ToString().PadLeft(7, '0'));
                    string Dir = BaseDir == "" ? "" : string.Format("{0}imagesU/compress/{1}/", BaseDir, ProposalId.ToString().PadLeft(7, '0'));
                    string tmpDir = BaseDir == "" ? "" : string.Format("{0}imagesU/compress/{1}/{2}/", BaseDir, ProposalId.ToString().PadLeft(7, '0'), tmpFolderName);

                    if (!System.IO.Directory.Exists(tmpDir))
                    {
                        System.IO.Directory.CreateDirectory(tmpDir);
                    }

                    if (config.SellerContractIsFtp)
                    {
                        //Use FTP
                        foreach (string _id in listIds)
                        {
                            ProposalSellerFile files = sp.ProposalSellerFileGet(Convert.ToInt32(_id));
                            if (files != null && files.IsLoaded)
                            {
                                string ftpFolder = "Seller_Files/" + ProposalId.PadLeft(7, '0') + "/" + files.RealFileName;
                                SimpleFtpClient cli = new SimpleFtpClient(new Uri(config.SellerContractFtpUri + ftpFolder));
                                cli.SimpleDownloadToFolder(files.OriFileName, ftpFolder, tmpDir + "/" + files.OriFileName);
                            }
                        }

                        //zip
                        string FilePath = Dir + tmpFolderName + ".zip";
                        BizLogic.Facade.ProposalFacade.ZipDownloadFile(tmpDir, FilePath);

                        System.IO.Directory.Delete(tmpDir, true);
                        //System.Net.WebClient wc = new System.Net.WebClient();
                        //byte[] byteFiles = null;
                        //byteFiles = wc.DownloadData(FilePath);
                        //string fileName = System.IO.Path.GetFileName(FilePath);
                        //context.Response.AddHeader("content-disposition", "attachment;filename=" + HttpContext.Current.Server.UrlEncode(fileName));
                        //context.Response.ContentType = "application/octet-stream";
                        //context.Response.BinaryWrite(byteFiles);
                        //context.Response.End();
                        context.Response.Write(config.SiteUrl + "/Images/imagesU/compress/" + ProposalId.ToString().PadLeft(7, '0') + "/" + tmpFolderName + ".zip");
                    }
                    else
                    {
                        //Use Folder
                        foreach (string _id in listIds)
                        {
                            ProposalSellerFile files = sp.ProposalSellerFileGet(Convert.ToInt32(_id));
                            if (files != null && files.IsLoaded)
                            {
                                //string ftpFolder = "Seller_Files/" + ProposalId.PadLeft(7, '0') + "/" + files.RealFileName;
                                //SFtpClient cli = new SFtpClient(new Uri(config.SellerContractFtpUri + ftpFolder));
                                //cli.SimpleDownloadToFolder(files.OriFileName, ftpFolder, tmpDir + "/" + files.RealFileName);
                                if (File.Exists(sourDir + "/" + files.RealFileName))
                                {
                                    File.Copy(sourDir + "/" + files.RealFileName, tmpDir + "/" + files.RealFileName);
                                }

                            }
                        }

                        string FilePath = Dir + tmpFolderName + ".zip";
                        BizLogic.Facade.ProposalFacade.ZipDownloadFile(tmpDir, FilePath);

                        System.IO.Directory.Delete(tmpDir, true);
                        //System.Net.WebClient wc = new System.Net.WebClient();
                        //byte[] byteFiles = null;
                        //byteFiles = wc.DownloadData(FilePath);
                        //string fileName = System.IO.Path.GetFileName(FilePath);
                        //context.Response.AddHeader("content-disposition", "attachment;filename=" + HttpContext.Current.Server.UrlEncode(fileName));
                        //context.Response.ContentType = "application/octet-stream";
                        //context.Response.BinaryWrite(byteFiles);
                        //context.Response.End();
                        context.Response.Write(config.SiteUrl + "/Images/imagesU/compress/" + ProposalId.ToString().PadLeft(7, '0') + "/" + tmpFolderName + ".zip");
                    }


                }
            }
        }

        private void Download(HttpContext context, string FTPPath, string FileName)
        {
            string ftpFolder = FTPPath;

            if (config.SellerContractIsFtp)
            {
                //Use FTP
                SimpleFtpClient cli = new SimpleFtpClient(new Uri(config.SellerContractFtpUri + ftpFolder));
                cli.SimpleDownload(FileName, FTPPath);
            }
            else
            {
                //Use Folder
                System.Net.WebClient wc = new System.Net.WebClient();
                byte[] byteFiles = null;
                string FilePath = Path.Combine(context.Request.PhysicalApplicationPath, ftpFolder);
                byteFiles = wc.DownloadData(FilePath);
                string fileName = System.IO.Path.GetFileName(FilePath);
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + HttpContext.Current.Server.UrlEncode(fileName));
                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.BinaryWrite(byteFiles);
                HttpContext.Current.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}
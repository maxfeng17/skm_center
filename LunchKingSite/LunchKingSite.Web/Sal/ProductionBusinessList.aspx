﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="ProductionBusinessList.aspx.cs" Inherits="LunchKingSite.Web.Sal.ProductionBusinessList" %>
<%@ Register Src="../User/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/fancybox/jquery.fancybox.css") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Tools/js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="../Tools/js/jquery.nav.js"></script>
    <script type="text/javascript" src="../Tools/js/fancybox/jquery.fancybox.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {

            //提示字
            $('#txtEmployee').attr('placeholder', 'email');

            //datepicker
            $('#txtDateStart').datepicker({ dateFormat: 'yy/mm/dd' });
            $('#txtDateEnd').datepicker({ dateFormat: 'yy/mm/dd' });

            //批次複合指派預設鎖住
            $('#btnBatchAssign').prop('disabled', 'disabled');

            //恢復工作狀態紀錄
            var status = $('#hidStatus').val();
            if (status != 0 && status != null)
            {
                $('.status').each(function () {
                    var value = $(this).val();
                    var isFlagSet = value & status;
                    if (isFlagSet > 0)
                        $(this).attr('checked', 'checked');
                    
                });
            }

            //恢復圖片類型紀錄
            var pic = $('#hidPic').val();
            if (pic != 0 && pic != null) {
                $('.pic').each(function () {
                    var value = $(this).val();
                    var isFlagSet = value & pic;
                    if (isFlagSet > 0)
                        $(this).attr('checked', 'checked');

                });
            }

            tableSort.init();
            //Table排序後要更換image
            tableSort.image();

            $("input[type=radio]").mousedown(function () {
                var $self = $(this);
                if ($self.is(':checked')) {
                    var uncheck = function () {
                        setTimeout(function () { $self.removeAttr('checked'); }, 0);
                    };
                    var unbind = function () {
                        $self.unbind('mouseup', up);
                    };
                    var up = function () {
                        uncheck();
                        unbind();
                    };
                    $self.bind('mouseup', up);
                    $self.one('mouseout', unbind);
                }
            });


            $("#chkAll").click(function () {
                if ($(this).is(":checked")) {
                    $(".rd-table input[name*='chkCheck']").prop("checked", true);
                } else {
                    $(".rd-table input[name*='chkCheck']").prop("checked", false);
                }
            });

            $(".rd-table input[name*='chkCheck']").click(function () {
                var chks = $(".rd-table input[name*='chkCheck']");
                var allChk = true;
                $.each(chks, function (idx, obj) {
                    if ($(obj).is(":checked") == false) {
                        allChk = false;
                    }
                });
                if (allChk) {
                    $("#chkAll").prop("checked", true);
                } else {
                    $("#chkAll").prop("checked", false);
                }
            });

            BindDealSubType($('#ddlDealType'));

            //批次複合指派
            $("#btnBatchAssign").fancybox({
                parent: "form:first",
                helpers: {
                    overlay: { closeClick: false }
                },
                href: '#divAssign',
                afterClose: function () {
                    //關閉時清空資料
                    $('#txtAssignEmp').val('');
                    $('.AssignFlag').each(function () {
                        $(this).attr('checked', false);
                    });
                }
            });

            
            $("#btnBatchAssign").click(function () {
                var ProposalList = [];
                $('.Check').each(function () {
                    var check = $(this).find('input[type=checkbox]');
                    if (check.is(':checked'))
                    {
                        var hk = $(this).parent().next().find('[id*=hkProposalId]');
                        ProposalList.push(hk.text());
                    }
                });
                $('#lblAssignCount').text(ProposalList.length);
                $('#<%=hidAssignPro.ClientID%>').val(ProposalList);
                
            });

            //指派
            $("#btnAssign").click(function () {
                var AssignFlagList = [];
                $('.AssignFlag').each(function () {
                    var check = $(this);
                    if (check.is(':checked')) {
                        AssignFlagList.push(check.val());
                    }
                });
                $('#<%=hidAssignFlag.ClientID%>').val(AssignFlagList);
                if ($('#<%=hidAssignFlag.ClientID%>').val() == '' || $('#txtAssignEmp').val() == '')
                {
                    alert('請輸入工作人員或指定指派工作');
                    return false;
                }
                else
                {
                    $.ajax({
                    type: "POST",
                    url: "ProductionBusinessList.aspx/AssignProduction",
                    data: "{'AssignPro': '" + $('#<%=hidAssignPro.ClientID%>').val() + "','AssignFlag':'" + $('#<%=hidAssignFlag.ClientID%>').val() + "','AssignEmp': '" + $('#<%=txtAssignEmp.ClientID%>').val() + "','UserName': '<%= UserName %>' }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var c = response.d;
                        if (c) {
                            alert("批次設定完成!");
                        } else {
                            alert("批次設定失敗!");
                        }
                        
                    }, error: function (xhr) {
                        console.log(xhr.responseText);
                    }
                });
                }

                    

            });

            //檢查是否有勾選提案單
            $(".Check").change(function () {
                var CheckCount = 0;
                $('.Check').each(function () {
                    var check = $(this).find('input[type=checkbox]');
                    if (check.is(':checked'))
                        CheckCount++;
                });

                if (CheckCount == 0)
                    $("#btnBatchAssign").prop('disabled', 'disabled');
                else
                    $("#btnBatchAssign").prop('disabled', '');

            });


            //工作人員autocomplete
            $('#txtAssignEmp').autocomplete({
                source: function (request, response) {
                    $.ajax(
                    {
                        type: "POST",
                        url: "ProductionBusinessList.aspx/GetProductionNameArray",
                        data: JSON.stringify({
                            userName: request.term
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.Label,
                                    value: item.Value
                                }
                            }))
                        },
                        error: function (res) {
                            console.log(res);
                        }
                    });
                },
                select: function () {
                    //選擇項目時
                    $(this).trigger('change');
                },
            });

            //檢查是否為創意部人員
            $('#txtAssignEmp').blur(function () {
                $.ajax(
                    {
                        type: "POST",
                        url: "ProductionBusinessList.aspx/IsProduction",
                        data: "{ 'userName': '" + $(this).val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d == false)
                            {
                                $('#NoProductionEmp').show();
                                $("#divAssginEmp *").prop('disabled', 'disabled');
                            }
                            else
                            {
                                $('#NoProductionEmp').hide();
                                $("#divAssginEmp *").prop('disabled', '');
                            }
                                
                        }, error: function (res) {
                            console.log(res);
                        }
                    });
            });


        });

        function QueryData() {
            $("#<%=hidSortSearch.ClientID%>").click();
        }

        var tableSort = {
            init: function () {
                $("#imgId").click(function () {
                    $("#<%=hdOrderField.ClientID%>").val("id");
                     QueryData();
                 });
                $("#imgSellerName").click(function () {
                    $("#<%=hdOrderField.ClientID%>").val("seller_name");
                    QueryData();
                });
                $("#imgDeliveryType").click(function () {
                    $("#<%=hdOrderField.ClientID%>").val("delivery_type");
                    QueryData();
                });
                $("#imgOrderTimeS").click(function () {
                    $("#<%=hdOrderField.ClientID%>").val("order_time_s");
                    QueryData();
                });
                $("#imgSaleName").click(function () {
                    $("#<%=hdOrderField.ClientID%>").val("emp_name");
                    QueryData();
                });
            },
            image: function () {
                var __orderby = $("#<%=hdOrderBy.ClientID%>").val();
                var __orderField = $("#<%=hdOrderField.ClientID%>").val();
                var __objId = "";
                var sortArray = [];
                var __img = "sort-down.png";

                if (__orderby == "asc") {
                    __img = "sort-up.png";
                } else {
                    __img = "sort-down.png";
                }

                switch (__orderField) {
                    case "id":
                        __objId = "imgId";
                        break;
                    case "seller_name":
                        __objId = "imgSellerName";
                        break;
                    case "delivery_type":
                        __objId = "imgDeliveryType";
                        break;
                    case "order_time_s":
                        __objId = "imgOrderTimeS";
                        break;
                    case "emp_name":
                        __objId = "imgSaleName";
                        break;
                }

                sortArray.push("imgId");
                sortArray.push("imgSellerName");
                sortArray.push("imgDeliveryType");
                sortArray.push("imgOrderTimeS");
                sortArray.push("imgSaleName");

                //先把所有的圖示都換回預設圖示
                $.each(sortArray, function (index, value) {
                    $("#" + value + "").attr("src", "../Themes/default/images/17Life/G2/sort-initial.png");
                });
                //將要排序的圖示單獨換掉                
                $("#" + __objId + "").attr("src", "../Themes/default/images/17Life/G2/" + __img);
            }
        }

        function selectChannel(obj) {

            //取得Area
            if ($(obj).is(':checked')) {
                $(obj).parent('.pChannel').parent('td').next('td').children('.pArea').find('input[type=checkbox]').each(function () {
                    $(this).attr('checked', true);
                });
            } else {
                $(obj).parent('.pChannel').parent('td').next('td').children('.pArea').find('input[type=checkbox]').each(function () {
                    $(this).attr('checked', false);
                });
            }
        }

        function selectArea(obj) {

            //取得Area共勾選幾個
            var total = 0;
            var checked = 0;
            $(obj).parent('.pA').parent().parent().find('input[type=checkbox]').each(function () {
                total++;
                if ($(this).is(':checked'))
                    checked++;

            });

            //取得channel
            if(total > 0 && checked == 0)
                $(obj).parent('.pA').parent('li').parent('ul').parent('.pArea').parent('td').prev('td').children().find('input[type=checkbox]').attr('checked', false);
            else
                $(obj).parent('.pA').parent('li').parent('ul').parent('.pArea').parent('td').prev('td').children().find('input[type=checkbox]').attr('checked', true);
        }

        function Search()
        {
            //上檔頻道
            var checkcity = [];

            $('input[id*=chkChannel]').each(function () {
                if ($(this).is(':checked')) {
                    if ($(this).next('label').text() == "宅配" || $(this).next('label').text() == "品生活" || $(this).next('label').text() == "即買即用" || $(this).next('label').text() == "最後一天" || $(this).next('label').text() == "咖啡寄杯")
                        checkcity.push($(this).prev('[id*=hidChannel]').val());
                }
            });
            $('input[id*=chkArea]').each(function () {
                if ($(this).is(':checked')) {
                    checkcity.push($(this).prev('[id*=hidArea]').val());
                }
            });

            $('#hidCheckCity').val(checkcity.join(','));

            //紀錄工作狀態
            var status = 0;
            $('.status').each(function () {
                if ($(this).is(':checked')) {
                    status +=parseInt($(this).val());
                }
            });
            $('#hidStatus').val(status);

            //紀錄圖片類型
            var pic = 0;
            $('.pic').each(function () {
                if($(this).is(':checked'))
                {
                    pic += parseInt($(this).val());
                }
            });
            $('#hidPic').val(pic);


            //檢核欄位
            if ($("input[id^='<% =rbDate.ClientID %>']:checked").val() != null && ($('#txtDateStart').val() == '' || $('#txtDateEnd').val() == ''))
            {
                alert('請輸入日期區間');
                return false;
            }
            else
            {
                if ($("input[id^='<% =rbDate.ClientID %>']:checked").val() != null && (!isValidDate($('#txtDateStart').val()) || !isValidDate($('#txtDateEnd').val())))
                {
                    alert("輸入日期格式不正確");
                    return false;
                }
            }

            if ($("input[id^='<% =rbDate.ClientID %>']:checked").val() == null && ($('#txtDateStart').val() != '' || $('#txtDateEnd').val() != '')) {
                alert('請選擇日期類型');
                return false;
            }
            

            if ($("input[id^='<% =rbAssign.ClientID %>']:checked").val() != null && (status == 0 && ($("#chkProduction").is(":checked") == false)))
            {
                alert('請選擇編輯檢核類型');
                return false;
            }

            if ($("input[id^='<% =rbAssign.ClientID %>']:checked").val() == null && status != 0) {
                alert('請選擇指派類型');
                return false;
            }

            __doPostBack('<%= ddlChangePage.UniqueID %>', '');
            return true;
                
        }

        function isValidDate(dateStr) {

            if (dateStr == null || dateStr == '') return false;
            var parts = dateStr.split('/');
            if (parts.length != 3) return false;
            if (parts[0] > 9999 || parts[0] < 1753)
                return false;
            var accDate = new Date(dateStr);
            if (parts[0].toString() == accDate.getFullYear().toString()
                && parts[1].toString() == padLeft((accDate.getMonth() + 1).toString(), 2)
                && parts[2].toString() == padLeft(accDate.getDate().toString(), 2)) {
                return true;
            }
            else {
                return false;
            }
        }

        function BindDealSubType(obj) {
            $.ajax({
                type: "POST",
                url: "ProposalContent.aspx/GetDealSubType",
                data: "{'id': '" + $(obj).val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var subType = $('#ddlDealSubType');
                    $(subType).find('option').remove();
                    $(subType).append($('<option></option>').attr('value', "").text("請選擇"));
                    $.each(response.d, function (index, item) {
                        $(subType).append($('<option></option>').attr('value', item.Key).text(item.Value));
                    });
                    $('#hidDealType').val($(obj).val());


                    $(subType).val($('#hidDealSubType').val());
                    SetDealSubType(subType);
                }
            });
        }
        function SetDealSubType(obj) {
            $('#hidDealSubType').val($(obj).val());
        }

        function padLeft(str, lenght) {
            //靠左補零
            str = '' + str;
            if (str.length >= lenght)
                return str;
            else
                return padLeft("0" + str, lenght);
        }


    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <div class="mc-content">
        <h1 class="rd-smll">創意部檔次查詢</h1>
        <hr class="header_hr">
        <div class="grui-form">
            <div class="form-unit">
                <label class="unit-label rd-unit-label">
                    上檔頻道</label>
                <div class="data-input rd-data-input">
                    <asp:Repeater ID="rptChannel" runat="server" OnItemDataBound="rptChannel_ItemDataBound">
                        <ItemTemplate>
                            <div class="form-unit">
                                <table>
                                    <tr>
                                        <td>
                                            <div class="pChannel">
                                                <asp:HiddenField ID="hidChannel" runat="server" />
                                                <asp:CheckBox ID="chkChannel" runat="server" onclick="selectChannel(this);" />
                                            </div>
                                        </td>
                                        <td style="margin-bottom:5px">
                                            <div class="pArea">
                                                <ul>
                                                    <asp:Repeater ID="rptArea" runat="server" OnItemDataBound="rptArea_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <li style="border-bottom: 1px solid #e4e4e4;margin-top:5px">
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <p class="pA">
                                                                <asp:HiddenField ID="hidArea" runat="server" />
                                                                <asp:CheckBox ID="chkArea" runat="server" onclick="selectArea(this);" />
                                                            </p>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </li>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:HiddenField ID="hidCheckCity" runat="server" ClientIDMode="Static" />
                </div>
            </div>
            <div class="form-unit" style="display:none">
                <label class="unit-label rd-unit-label">
                    提案類型</label>
                <div class="data-input rd-data-input">                    
                    <asp:DropDownList ID="ddlDealType" ClientIDMode="Static" runat="server" Width="100px" onchange="BindDealSubType(this);"></asp:DropDownList>
                    <asp:DropDownList ID="ddlDealSubType" runat="server" ClientIDMode="Static" Width="100px" onchange="SetDealSubType(this);"></asp:DropDownList>
                    <asp:HiddenField ID="hidDealType" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hidDealSubType" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdOrderField" runat="server" Value="" />
                    <span style="display: none">
                        <asp:Button ID="hidSortSearch" runat="server" Text="Button" OnClick="hidSortSearch_Click" /></span>
                    <asp:HiddenField ID="hdOrderBy" runat="server" Value="asc" />
                </div>
            </div>
            <div class="form-unit">
                <label class="unit-label rd-unit-label">
                    日期</label>
                <div class="data-input rd-data-input">                    
                    <asp:RadioButtonList ID="rbDate" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1">上檔日</asp:ListItem>
                        <asp:ListItem Value="2">工作完成日</asp:ListItem>
                        <asp:ListItem Value="3">指派日</asp:ListItem>
                        <asp:ListItem Value="4">執行排檔日</asp:ListItem>
                    </asp:RadioButtonList>

                    <br />
                    <asp:TextBox ID="txtDateStart" runat="server" CssClass="input-half" Style="width: 200px" ClientIDMode="Static"></asp:TextBox>
                    ~
                    <asp:TextBox ID="txtDateEnd" runat="server" CssClass="input-half" Style="width: 200px" ClientIDMode="Static"></asp:TextBox>
                </div>
            </div>
            <div class="form-unit">
                <label class="unit-label rd-unit-label">
                    檔次/工作狀態</label>
                <div class="data-input rd-data-input">
                    <asp:RadioButtonList ID="rbAssign" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1">未指派</asp:ListItem>
                        <asp:ListItem Value="2">已指派未完成</asp:ListItem>
                        <asp:ListItem Value="3">已指派已完成</asp:ListItem>
                    </asp:RadioButtonList>
                    <br />
                    <input id="chkSetting" type="checkbox" class="status"  value="<%=(int)ProposalEditorFlag.Setting %>" /><%= LunchKingSite.I18N.Phrase.ProposalEditorFlagSetting %>
                    <input id="chkCopyWriter" type="checkbox" class="status" value="<%=(int)ProposalEditorFlag.CopyWriter %>" /><%= LunchKingSite.I18N.Phrase.ProposalEditorFlagCopyWriter %>
                    <input id="chkImageDesign" type="checkbox" class="status" value="<%=(int)ProposalEditorFlag.ImageDesign %>" /><%= LunchKingSite.I18N.Phrase.ProposalEditorFlagImageDesign %>
                    <input id="chkART" type="checkbox" class="status" value="<%=(int)ProposalEditorFlag.ART %>" /><%= LunchKingSite.I18N.Phrase.ProposalEditorFlagART %>
                    <input id="chkListing" type="checkbox" class="status" value="<%=(int)ProposalEditorFlag.Listing %>" /><%= LunchKingSite.I18N.Phrase.ProposalEditorFlagListing %>
                    <asp:CheckBox ID="chkProduction" runat="server" ClientIDMode="Static" />申請製檔
                    <asp:HiddenField ID="hidStatus" runat="server" ClientIDMode="Static" />

                </div>
            </div>
            <div class="form-unit" style="display:none">
                <label class="unit-label rd-unit-label">
                    新宅配提案單</label>
                <div class="data-input rd-data-input">
                    
                </div>
            </div>
            <div class="form-unit">
                <label class="unit-label rd-unit-label">
                    工作人員</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox ID="txtEmployee" runat="server" CssClass="input-half" Style="width: 200px"></asp:TextBox>
                    <span style="color:gray">@17life.com</span>
                </div>
            </div>
            <div class="form-unit" style="display:none">
                <label class="unit-label rd-unit-label">
                    檔次類型</label>
                <div class="data-input rd-data-input">
                    <asp:RadioButtonList ID="rbDealProperty" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1">一般</asp:ListItem>
                        <asp:ListItem Value="2">快速上檔</asp:ListItem>
                        <asp:ListItem Value="3">完全複製檔</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:CheckBox ID="chkMarketingResource" runat="server" Text="行銷/策展專案"/>
                </div>
            </div>
            <div class="form-unit" style="display:none">
                <label class="unit-label rd-unit-label">
                    圖片類型</label>
                <div class="data-input rd-data-input">
                    <input id="chkPhotographers" type="checkbox" class="pic"  value="<%=(int)ProposalSpecialFlag.Photographers %>" /><%= LunchKingSite.I18N.Phrase.ProposalSpecialFlagPhotographers %>
                    <input id="chkFTPPhoto" type="checkbox" class="pic" value="<%=(int)ProposalSpecialFlag.FTPPhoto %>" /><%= LunchKingSite.I18N.Phrase.ProposalSpecialFlagFTPPhoto %>
                    <input id="chkSellerPhoto" type="checkbox" class="pic" value="<%=(int)ProposalSpecialFlag.SellerPhoto %>" /><%= LunchKingSite.I18N.Phrase.ProposalSpecialFlagSellerPhoto %>
                    <asp:HiddenField ID="hidPic" runat="server" ClientIDMode="Static" />
                </div>
            </div>
            <div class="form-unit end-unit">
                <label class="unit-label rd-unit-label">
                    功能</label>
                <div class="data-input rd-data-input">
                    <asp:Button ID="btnSearch" runat="server" Text="搜尋" CssClass="btn rd-mcacstbtn" OnClick="btnSearch_Click" OnClientClick="return  Search()" />
                    <asp:Button ID="btnBatchAssign" runat="server" Text="批次複合指派" ClientIDMode="Static" class="btn rd-mcacstbtn"  />

                    <div id ="divAssign" style="display:none;width:400px;height:300px">
                        指派
                        <hr class="header_hr">
                        本次共指派<label class="unit-label rd-unit-label" id="lblAssignCount"></label>張提案單
                        <asp:HiddenField ID="hidAssignPro" runat="server" ClientIDMode="Static" />
                        <br />
                        <br />
                        工作人員
                        <br />
                        <asp:TextBox ID="txtAssignEmp" runat="server" ClientIDMode="Static" CssClass="input-half" Style="width: 200px" placeholder="請輸入Email"></asp:TextBox>
                        
                        <br />
                        <span style="color: #BF0000; font-weight: bold;display:none" id="NoProductionEmp">非創意部人員</span>
                        <br />
                        <br />
                        工作區域
                        <br />
                        <div id="divAssginEmp">
                            <input id="chkAssignSetting" type="checkbox" class="AssignFlag" value="<%=(int)ProposalEditorFlag.Setting %>" /><%= LunchKingSite.I18N.Phrase.ProposalEditorFlagSetting %>
                            <input id="chkAssignCopyWriter" type="checkbox" class="AssignFlag" value="<%=(int)ProposalEditorFlag.CopyWriter %>" /><%= LunchKingSite.I18N.Phrase.ProposalEditorFlagCopyWriter %>
                            <input id="chkAssignImageDesign" type="checkbox" class="AssignFlag" value="<%=(int)ProposalEditorFlag.ImageDesign %>" /><%= LunchKingSite.I18N.Phrase.ProposalEditorFlagImageDesign %>
                            <br />
                            <input id="chkAssignART" type="checkbox" class="AssignFlag" value="<%=(int)ProposalEditorFlag.ART %>" /><%= LunchKingSite.I18N.Phrase.ProposalEditorFlagART %>
                            <input id="chkAssignListing" type="checkbox" class="AssignFlag" value="<%=(int)ProposalEditorFlag.Listing %>" /><%= LunchKingSite.I18N.Phrase.ProposalEditorFlagListing %>
                            <asp:HiddenField ID="hidAssignFlag" runat="server" ClientIDMode="Static" />
                            <br />
                            <div class="form-unit">
                                <div class="data-input rd-data-input" style="float: right">
                                    <asp:Button ID="btnAssign" runat="server" Text="指派" CssClass="btn rd-mcacstbtn" OnClick="btnAssign_Click" ClientIDMode="Static" />
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="form-unit end-unit" style="text-align:right">
                <div class="data-input rd-data-input">
                    <asp:DropDownList ID="ddlChangePage" runat="server" OnSelectedIndexChanged="btnSearch_Click" ClientIDMode="Static" AutoPostBack="True" Width="100px" onchange="return Search()">
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>40</asp:ListItem>
                        <asp:ListItem>50</asp:ListItem>
                        <asp:ListItem>100</asp:ListItem>
                        <asp:ListItem>200</asp:ListItem>
                        <asp:ListItem>500</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <asp:PlaceHolder ID="divBusinessList" runat="server" Visible="false">
            <div id="mc-table">
                <asp:Repeater ID="rptBusiness" runat="server" OnItemDataBound="rptBusiness_ItemDataBound">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                            <tr class="rd-C-Hide">
                                <th class="OrderDate">勾選<input type="checkbox" id="chkAll" />
                                </th>
                                <th class="OrderDate">單號<img id="imgId" src="../Themes/default/images/17Life/G2/sort-initial.png" alt="排序" /></a>
                                </th>
                                <th class="OrderSerial">規模
                                </th>
                                <th class="OrderSerial">商家名稱<a href="javascript:void(0);"><img id="imgSellerName" src="../Themes/default/images/17Life/G2/sort-initial.png" alt="排序" /></a>
                                </th>
                                <th class="OrderDate">類型<a href="javascript:void(0);"><img id="imgDeliveryType" src="../Themes/default/images/17Life/G2/sort-initial.png" alt="排序" /></a>
                                </th>
                                <th class="OrderName" style="width: 300px">提案內容
                                </th>
                                <th class="OrderExp">上檔日<a href="javascript:void(0);"><img id="imgOrderTimeS" src="../Themes/default/images/17Life/G2/sort-initial.png" alt="排序" /></a>
                                </th>
                                <th class="OrderExp">業務<a href="javascript:void(0);"><img id="imgSaleName" src="../Themes/default/images/17Life/G2/sort-initial.png" alt="排序" /></a>
                                </th>
                                <th class="OrderCouponState">指派
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="mc-tableContentITEM">
                            <td class="OrderDate">
                                <asp:CheckBox ID="chkCheck" CssClass="Check" runat="server" />
                            </td>
                            <td class="OrderDate">
                                <span class="rd-Detailtitle">單號：</span>
                                <asp:HyperLink ID="hkProposalId" runat="server" Target="_blank"></asp:HyperLink>
                            </td>
                            <td class="OrderSerial">
                                <span class="rd-Detailtitle">規模：</span>
                                <asp:Literal ID="liSellerLevel" runat="server"></asp:Literal>
                            </td>
                            <td class="OrderSerial" style="max-width: 80px; text-align: left">
                                <asp:PlaceHolder ID="divFirstDeal" runat="server" Visible="false">
                                    <span style="color: #BF0000; border: solid 1px; padding: 5px 5px 5px 5px;">
                                        <asp:Literal ID="liFirstDeal" runat="server"></asp:Literal>
                                    </span>
                                    <br />
                                </asp:PlaceHolder>
                                <asp:HyperLink ID="hkSellerName" runat="server" Target="_blank"></asp:HyperLink>
                            </td>
                            <td class="OrderDate" style="text-align: left">
                                <asp:Label ID="lblDeliveryType" runat="server"></asp:Label><br />
                                <asp:Literal ID="liDealType" runat="server"></asp:Literal><br />
                                <asp:Literal ID="liDealSubType" runat="server"></asp:Literal>
                            </td>
                            <td class="OrderName" style="text-align: left; word-wrap: break-word; max-width: 300px">
                                <asp:Literal ID="liProposalContent" runat="server"></asp:Literal>
                                <span style="color: #666; font-size: 12px;">
                                    <asp:Literal ID="liMarketAnalysis" runat="server"></asp:Literal>
                                </span>
                                <asp:Repeater ID="rptSpecialFlag" runat="server" OnItemDataBound="rptSpecialFlag_ItemDataBound">
                                    <HeaderTemplate>
                                        <span>
                                            <br />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblFlag" runat="server" Style="color: #BF0000; border: solid 1px; white-space: nowrap; margin: 2px; padding: 5px 5px 5px 5px; line-height: 30px;"></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </span>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                            <td class="OrderDate" style="text-align: left">
                                <span class="rd-Detailtitle">上檔日：</span>
                                <asp:Literal ID="liOrderTimeS" runat="server"></asp:Literal>
                            </td>
                            <td class="OrderDate" style="text-align: left">
                                <span class="rd-Detailtitle">業務：</span>
                                <asp:Literal ID="liSalesName" runat="server"></asp:Literal><br />
                            </td>
                            <td class="OrderDate" style="max-width: 95px">
                                <asp:Repeater ID="rptAssign" runat="server" OnItemDataBound="rptAssign_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:Literal ID="liAssign" runat="server"></asp:Literal>
                                        <asp:Image ID="imgFlag" runat="server" />
                                        <br />
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Literal ID="liAssignPhoto" runat="server"></asp:Literal>
                                <asp:Image ID="imgFlagPhoto" runat="server" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <uc1:Pager ID="ucPager" runat="server" PageSize="10" ongetcount="GetCount" onupdate="UpdateHandler"></uc1:Pager>
        </asp:PlaceHolder>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

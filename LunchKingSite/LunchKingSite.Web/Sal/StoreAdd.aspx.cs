﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using System.Web.Security;
using MongoDB.Bson;

namespace LunchKingSite.Web.Sal
{
    public partial class StoreAdd : RolePage, ISalesStoreAddView
    {
        #region props
        private SalesStoreAddPresenter _presenter;
        public SalesStoreAddPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public ObjectId BusinessOrderObjectId
        {
            get
            {
                ObjectId id = ObjectId.Empty;
                if (Request["boid"] != null)
                    ObjectId.TryParse(Request["boid"], out id);
                return id;
            }
        }

        public string UserId
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }

        public bool EnableControl
        {
            get
            {
                bool enable;
                return Request["enable"] != null ? bool.TryParse(Request["enable"], out enable) ? enable : false : false;
            }
        }

        public int AddressCityId
        {
            get
            {
                if ((Request["selCity"] == null) || (Request["selCity"] == ""))
                {
                    int cityId;
                    return int.TryParse(hidAddressCityId.Value, out cityId) ? cityId : -1;
                }
                else
                {
                    return int.Parse(Request["selCity"]);
                }
            }
            set { hidAddressCityId.Value = value.ToString(); }
        }

        public int AddressTownshipId
        {
            get
            {
                if ((Request["selTownship"] == null) || (Request["selTownship"] == ""))
                {
                    int townshipId;
                    return int.TryParse(hidAddressTownshipId.Value, out townshipId) ? townshipId : -1;
                }
                else
                {
                    return int.Parse(Request["selTownship"]);
                }
            }
            set { hidAddressTownshipId.Value = value.ToString(); }
        }

        public string CityJSonData
        {
            get
            {
                return hidCityJSonData.Value;
            }
            set
            {
                hidCityJSonData.Value = value;
            }
        }
        #endregion

        #region event
        public event EventHandler<DataEventArgs<string>> AccountingBankSelectedIndexChanged;
        public event EventHandler<DataEventArgs<ObjectId>> EditClick;
        public event EventHandler<DataEventArgs<SalesStore>> SaveClicked;
        public event EventHandler<DataEventArgs<ObjectId>> DeleteClicked;
        #endregion

        #region method
        public void SetAccountingBankInfo(BankInfoCollection bankList)
        {
            ddlAccountingBank.DataSource = bankList.Select(x => new { No = x.BankNo, Name = x.BankNo + " " + x.BankName });
            ddlAccountingBank.DataValueField = "No";
            ddlAccountingBank.DataTextField = "Name";
            ddlAccountingBank.DataBind();
        }

        public void SetAccountingBranchInfo(BankInfoCollection branchList)
        {
            ddlAccountingBranch.DataSource = branchList.Select(x => new { No = x.BranchNo, Name = x.BranchNo + " " + x.BranchName });
            ddlAccountingBranch.DataValueField = "No";
            ddlAccountingBranch.DataTextField = "Name";
            ddlAccountingBranch.DataBind();
        }

        public void GetLocationDropDownList(List<City> citys, List<City> locations)
        {
            repCity.DataSource = citys;
            repCity.DataBind();

            repTown.DataSource = locations;
            repTown.DataBind();
        }

        public void SetSalesStoreData(SalesStore store)
        {
            divEdit.Visible = true;
            divStoreList.Visible = false;
            btnAdd.Visible = false;
            hidStoreId.Value = store.Id.ToString();
            var township = CityManager.TownShipGetById(store.AddressTownshipId);
            txtBranchName.Text = store.BranchName;
            txtStoreTel.Text = store.StoreTel;
            AddressCityId = township == null ? -1 : township.ParentId.Value;
            AddressTownshipId = township == null ? -1 : township.Id;
            txtStoreAddress.Text = store.StoreAddress;
            txtOpeningTime.Text = store.OpeningTime;
            txtCloseDate.Text = store.CloseDate;
            txtQuantity.Text = store.Quantity.ToString();
            txtMrt.Text = store.Mrt;
            txtCar.Text = store.Car;
            txtBus.Text = store.Bus;
            txtOV.Text = store.OV;
            txtWebUrl.Text = store.WebUrl;
            txtFBUrl.Text = store.FBUrl;
            txtPlurkUrl.Text = store.PlurkUrl;
            txtBlogUrl.Text = store.BlogUrl;
            txtOtherUrl.Text = store.OtherUrl;
            txtRemark.Text = store.Remark;
            txtStoreName.Text = store.StoreName;
            txtStoreSignCompanyID.Text = store.StoreSignCompanyID;
            txtStoreCompanyAddress.Text = store.StoreCompanyAddress;
            txtStoreBossName.Text = store.StoreBossName;
            txtStoreID.Text = store.StoreID;
            if (ddlAccountingBank.Items.FindByValue(store.StoreBankCode) != null)
            {
                ddlAccountingBank.SelectedValue = store.StoreBankCode;
                if (ddlAccountingBranch.Items.FindByValue(store.StoreBranchCode) != null)
                    ddlAccountingBranch.SelectedValue = store.StoreBranchCode;
                else
                    ddlAccountingBranch.SelectedIndex = -1;
            }
            else
            {
                ddlAccountingBank.SelectedIndex = -1;
                ddlAccountingBranch.SelectedIndex = -1;
            }
            txtAccountingBankId.Text = store.StoreBankCode;
            txtAccountingBankDesc.Text = store.StoreBankDesc;
            txtAccountingBranchId.Text = store.StoreBranchCode;
            txtAccountingBranchDesc.Text = store.StoreBranchDesc;
            txtStoreAccount.Text = store.StoreAccount;
            txtStoreAccountName.Text = store.StoreAccountName;
            txtStoreAccountingName.Text = store.StoreAccountingName;
            txtStoreAccountingTel.Text = store.StoreAccountingTel;
            txtStoreEmail.Text = store.StoreEmail;
            txtStoreNotice.Text = store.StoreNotice;
        }

        public void SetStoreList(List<SalesStore> storeList)
        {
            // 關閉視窗並更新店鋪清單
            //Response.Write("<script language='javascript'>window.open('', '_self', ''); window.close();</" + "script>");

            gvStoreList.DataSource = storeList;
            gvStoreList.DataBind();
            divEdit.Visible = false;
            divStoreList.Visible = true;
            btnAdd.Visible = EnableControl;
        }

        public void ShowMessage(string message)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + message + "');", true);
        }
        #endregion

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!User.Identity.IsAuthenticated && !User.IsInRole("Sales"))
                {
                    FormsAuthentication.RedirectToLoginPage();
                    return;
                }
                EnableControls(EnableControl);
                _presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void ddlAccountingBank_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.AccountingBankSelectedIndexChanged != null)
                this.AccountingBankSelectedIndexChanged(this, new DataEventArgs<string>(ddlAccountingBank.SelectedValue));

            if (!string.IsNullOrEmpty(ddlAccountingBranch.SelectedValue))
            {
                txtAccountingBranchId.Text = ddlAccountingBranch.SelectedValue;
                if (ddlAccountingBranch.SelectedItem != null)
                    txtAccountingBranchDesc.Text = ddlAccountingBranch.SelectedItem.Text.Replace(txtAccountingBranchId.Text, string.Empty).Trim();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            divEdit.Visible = true;
            divStoreList.Visible = false;
            btnDelete.Visible = false;
            btnAdd.Visible = false;

            hidStoreId.Value = string.Empty;
            txtBranchName.Text = string.Empty;
            txtStoreTel.Text = string.Empty;
            txtStoreAddress.Text = string.Empty;
            txtOpeningTime.Text = string.Empty;
            txtCloseDate.Text = string.Empty;
            txtQuantity.Text = string.Empty;
            txtMrt.Text = string.Empty;
            txtCar.Text = string.Empty;
            txtBus.Text = string.Empty;
            txtOV.Text = string.Empty;
            txtWebUrl.Text = string.Empty;
            txtFBUrl.Text = string.Empty;
            txtPlurkUrl.Text = string.Empty;
            txtBlogUrl.Text = string.Empty;
            txtOtherUrl.Text = string.Empty;
            txtRemark.Text = string.Empty;
            txtStoreName.Text = string.Empty;
            txtStoreSignCompanyID.Text = string.Empty;
            txtStoreCompanyAddress.Text = string.Empty;
            txtStoreBossName.Text = string.Empty;
            txtStoreID.Text = string.Empty;
            ddlAccountingBank.SelectedIndex = -1;
            ddlAccountingBranch.SelectedIndex = -1;
            txtAccountingBankId.Text = string.Empty;
            txtAccountingBankDesc.Text = string.Empty;
            txtAccountingBranchId.Text = string.Empty;
            txtAccountingBranchDesc.Text = string.Empty;
            txtStoreAccount.Text = string.Empty;
            txtStoreAccountName.Text = string.Empty;
            txtStoreAccountingName.Text = string.Empty;
            txtStoreAccountingTel.Text = string.Empty;
            txtStoreEmail.Text = string.Empty;
            txtStoreNotice.Text = string.Empty;
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            divEdit.Visible = false;
            btnAdd.Visible = EnableControl;
            divStoreList.Visible = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.SaveClicked != null)
                this.SaveClicked(this, new DataEventArgs<SalesStore>(GetStoreData()));

            divEdit.Visible = false;
            divStoreList.Visible = true;
            btnAdd.Visible = EnableControl;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.DeleteClicked != null)
            {
                ObjectId id;
                ObjectId.TryParse(hidStoreId.Value, out id);
                if (!id.Equals(ObjectId.Empty))
                {
                    this.DeleteClicked(this, new DataEventArgs<ObjectId>(id));
                }
            }
        }

        protected void gvStoreList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is SalesStore)
            {
                SalesStore item = (SalesStore)e.Row.DataItem;
                Label lblStoreName = ((Label)e.Row.FindControl("lblStoreName"));
                Label lblQuantity = ((Label)e.Row.FindControl("lblQuantity"));
                Button btnEdit = ((Button)e.Row.FindControl("btnEdit"));
                Button btnDelete = ((Button)e.Row.FindControl("btnDelete"));

                lblStoreName.Text = !string.IsNullOrWhiteSpace(item.BranchName) ? item.BranchName : "(無分店名稱)";
                lblQuantity.Text = item.Quantity.ToString();

                Label lblAddress = ((Label)e.Row.FindControl("lblAddress"));
                lblAddress.Text = CityManager.CityTownShopStringGet(item.AddressTownshipId) + item.StoreAddress;
                if (EnableControl)
                {
                    btnDelete.Visible = true;
                    btnEdit.Text = "編輯";
                }
                else
                {
                    btnDelete.Visible = false;
                    btnEdit.Text = "檢視";
                }
            }
        }

        protected void gvStoreList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "UPD")
            {
                if (this.EditClick != null)
                {
                    ObjectId id;
                    ObjectId.TryParse(e.CommandArgument.ToString(), out id);
                    if (!id.Equals(ObjectId.Empty))
                    {
                        this.EditClick(this, new DataEventArgs<ObjectId>(id));
                    }
                }
            }
            else if (e.CommandName == "DEL")
            {
                if (this.DeleteClicked != null)
                {
                    ObjectId id;
                    ObjectId.TryParse(e.CommandArgument.ToString(), out id);
                    if (!id.Equals(ObjectId.Empty))
                    {
                        this.DeleteClicked(this, new DataEventArgs<ObjectId>(id));
                    }
                }
            }
        }

        #endregion

        #region private method
        private SalesStore GetStoreData()
        {
            SalesStore item = new SalesStore();
            ObjectId storeId;
            ObjectId.TryParse(hidStoreId.Value, out storeId);
            if (storeId != ObjectId.Empty)
            {
                item.Id = storeId;
            }
            item.BusinessOrderId = BusinessOrderObjectId;
            item.BranchName = txtBranchName.Text;
            item.StoreTel = txtStoreTel.Text;
            item.AddressCityId = AddressCityId;
            item.AddressTownshipId = AddressTownshipId;
            item.StoreAddress = txtStoreAddress.Text;
            item.OpeningTime = txtOpeningTime.Text;
            item.CloseDate = txtCloseDate.Text;
            int quantity = int.TryParse(txtQuantity.Text, out quantity) ? quantity : 0;
            item.Quantity = quantity;
            item.Mrt = txtMrt.Text;
            item.Car = txtCar.Text;
            item.Bus = txtBus.Text;
            item.OV = txtOV.Text;
            item.WebUrl = txtWebUrl.Text;
            item.FBUrl = txtFBUrl.Text;
            item.PlurkUrl = txtPlurkUrl.Text;
            item.BlogUrl = txtBlogUrl.Text;
            item.OtherUrl = txtOtherUrl.Text;
            item.Remark = txtRemark.Text;
            item.StoreName = txtStoreName.Text;
            item.StoreSignCompanyID = txtStoreSignCompanyID.Text;
            item.StoreCompanyAddress = txtStoreCompanyAddress.Text;
            item.StoreBossName = txtStoreBossName.Text;
            item.StoreID = txtStoreID.Text;
            item.StoreBankCode = txtAccountingBankId.Text;
            item.StoreBankDesc = txtAccountingBankDesc.Text;
            item.StoreBranchCode = txtAccountingBranchId.Text;
            item.StoreBranchDesc = txtAccountingBranchDesc.Text;
            item.StoreAccount = txtStoreAccount.Text;
            item.StoreAccountName = txtStoreAccountName.Text;
            item.StoreAccountingName = txtStoreAccountingName.Text;
            item.StoreAccountingTel = txtStoreAccountingTel.Text;
            item.StoreEmail = txtStoreEmail.Text;
            item.StoreNotice = txtStoreNotice.Text;
            return item;
        }

        public string CheckInputData()
        {
            //檢查輸入的資料是否正確
            if (string.IsNullOrEmpty(txtBranchName.Text.Trim()))
            {
                return "請輸入分店名稱。";
            }
            if (CityManager.TownShipGetById(AddressTownshipId) == null)
            {
                return "行政區資料錯誤，請輸入分店地址。";
            }
            if (string.IsNullOrEmpty(txtStoreAddress.Text.Trim()))
            {
                return "請輸入分店地址。";
            }
            return string.Empty;
        }

        private void EnableControls(bool enabled)
        {
            btnSave.Visible = enabled;
            btnDelete.Visible = enabled;
        }
        #endregion
    }
}
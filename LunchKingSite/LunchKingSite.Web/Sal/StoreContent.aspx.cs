﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Sal
{
    public partial class StoreContent : RolePage, ISalesStoreContentView
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region props
        private SalesStoreContentPresenter _presenter;
        public SalesStoreContentPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public Guid SellerGuid
        {
            get
            {
                Guid id = Guid.Empty;
                if (Request.QueryString["sid"] != null && Guid.TryParse(Request.QueryString["sid"].ToString(), out id))
                {
                    Guid.TryParse(Request.QueryString["sid"].ToString(), out id);
                }
                return id;
            }
        }

        public Guid StoreGuid
        {
            get
            {
                Guid id = Guid.Empty;
                if (Request.QueryString["stid"] != null && Guid.TryParse(Request.QueryString["stid"].ToString(), out id))
                {
                    Guid.TryParse(Request.QueryString["stid"].ToString(), out id);
                }
                return id;
            }
        }
        #endregion

        #region event
        public event EventHandler<EventArgs> Save;
        public event EventHandler<DataEventArgs<SellerTempStatus>> StoreApprove;
        public event EventHandler<DataEventArgs<string>> ChangeLog;
        #endregion

        #region method
        public void SetStoreContent(Seller seller, Store st, ViewEmployee emp, StoreChangeLogCollection changeLogs,StoreCollection sts)
        {
            if (seller.Guid != Guid.Empty)
            {
                hkSellerId.Text = seller.SellerId;
                hkBackToSeller.NavigateUrl = hkSellerId.NavigateUrl = ResolveUrl("~/sal/SellerContent.aspx?sid=" + seller.Guid);

                liTitle.Text = seller.SellerName + " " + st.StoreName;
                liStoreName.Text = st.StoreName;

                // 業務資訊
                if (emp.IsLoaded)
                {
                    divSalesInfo.Visible = true;
                    liSalesName.Text = emp.EmpName;
                    liSalesDeptId.Text = emp.DeptName;
                    liSalesEmail.Text = emp.Email;
                }

                if (st.Guid != Guid.Empty)
                {
                    // 審核狀態
                    liTempStatus.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerTempStatus)st.TempStatus);

                    // 聯絡資訊
                    txtStoreName.Text = st.StoreName;
                    if (st.TownshipId.HasValue)
                    {
                        City township = CityManager.TownShipGetById(st.TownshipId.Value);
                        ddlCompanyCityId.SelectedValue = township.ParentId.ToString();
                        hidTownshipId.Value = township.Id.ToString();
                    }
                    txtCompanyAddress.Text = st.AddressString;
                    txtBossName.Text = st.CompanyBossName;
                    txtPhone.Text = st.Phone;
                    chkCreditCardAvailable.Checked = st.CreditcardAvailable;
                    txtMrt.Text = st.Mrt;
                    txtCar.Text = st.Car;
                    txtBus.Text = st.Bus;
                    txtOV.Text = st.OtherVehicles;
                    txtWebUrl.Text = st.WebUrl;
                    txtFacebook.Text = st.FacebookUrl;
                    txtBlog.Text = st.BlogUrl;
                    txtOtherUrl.Text = st.OtherUrl;
                    txtOpenTime.Text = st.OpenTime;
                    txtCloseDate.Text = st.CloseDate;
                    txtOthers.Text = st.Remarks;

                    // 匯款資訊
                    txtCompanyName.Text = st.CompanyName;
                    txtCompanyID.Text = st.CompanyID;
                    if (!string.IsNullOrWhiteSpace(st.CompanyBankCode))
                    {
                        ddlBankCode.SelectedValue = st.CompanyBankCode;
                        hidBranchCode.Value = st.CompanyBranchCode;
                    }
                    txtSignCompanyID.Text = st.SignCompanyID;
                    txtAccountId.Text = st.CompanyAccount;
                    txtAccount.Text = st.CompanyAccountName;
                    txtFinanceName.Text = st.AccountantName;
                    txtFinanceTel.Text = st.AccountantTel;
                    txtFinanceEmail.Text = st.CompanyEmail;

                    // 追蹤紀錄
                    rptStoreChangeLog.DataSource = changeLogs.OrderByDescending(x => x.CreateTime);
                    rptStoreChangeLog.DataBind();

                    // 已建立的店鋪才可顯示的資訊
                    divChangeLog.Visible = true;

                    //所有分店
                    rptBStore.DataSource = sts;
                    rptBStore.DataBind();

                    rptCStore.DataSource = sts;
                    rptCStore.DataBind();

                }
            }
            EnabledSettings(seller, st);
        }

        public void RedirectStore(Guid stid)
        {
            Response.Redirect(ResolveUrl("~/sal/StoreContent.aspx?stid=" + stid));
        }

        public void ShowMessage(string msg, StoreContentMode mode, string parms = "")
        {
            string script = string.Empty;
            switch (mode)
            {
                case StoreContentMode.GenericError:
                    script = string.Format("alert('{0}');", msg);
                    break;
                case StoreContentMode.PrivilegeError:
                case StoreContentMode.StoreNotFound:
                    script = string.Format("alert('{0}'); location.href='{1}/sal/SellerList.aspx';", msg, config.SiteUrl);
                    break;
                default:
                    break;
            }

            if (!string.IsNullOrWhiteSpace(script))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "alert", script, true);
            }
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IntialControls();
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnStoreApprove_Click(object sender, EventArgs e)
        {
            if (this.StoreApprove != null)
            {
                this.StoreApprove(this, new DataEventArgs<SellerTempStatus>(SellerTempStatus.Completed));
            }
        }

        protected void btnStoreReturned_Click(object sender, EventArgs e)
        {
            if (this.StoreApprove != null)
            {
                this.StoreApprove(this, new DataEventArgs<SellerTempStatus>(SellerTempStatus.Returned));
            }
        }

        protected void btnStoreApply_Click(object sender, EventArgs e)
        {
            if (this.StoreApprove != null)
            {
                this.StoreApprove(this, new DataEventArgs<SellerTempStatus>(SellerTempStatus.Applied));
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Save != null)
            {
                this.Save(this, e);
            }
        }

        protected void btnChangeLog_Click(object sender, EventArgs e)
        {
            if (this.ChangeLog != null)
            {
                this.ChangeLog(this, new DataEventArgs<string>(txtChangeLog.Text));
                txtChangeLog.Text = string.Empty;
            }
        }
        protected void rptBStore_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is Store)
            {
                Store dataItem = (Store)e.Item.DataItem;
                Panel panStoreName = (Panel)e.Item.FindControl("panStoreName");
                CheckBox chkCheck = (CheckBox)e.Item.FindControl("chkStore");

                chkCheck.InputAttributes["value"] = dataItem.Guid.ToString();
            }
        }
        protected void rptCStore_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is Store)
            {
                Store dataItem = (Store)e.Item.DataItem;
                Panel panStoreName = (Panel)e.Item.FindControl("panStoreName");
                CheckBox chkCheck = (CheckBox)e.Item.FindControl("chkStore");

                chkCheck.InputAttributes["value"] = dataItem.Guid.ToString();
            }
        }
        #endregion

        #region private method
        private void IntialControls()
        {
            // 地址(城市)
            IEnumerable<City> citys = CityManager.Citys.Where(x => x.Code != "SYS");
            ddlCompanyCityId.DataSource = citys;
            ddlCompanyCityId.DataTextField = "CityName";
            ddlCompanyCityId.DataValueField = "Id";
            ddlCompanyCityId.DataBind();

            // 銀行代碼
            var bankS = VourcherFacade.BankInfoGetMainList().Select(x => new { BankNo = x.BankNo, BankName = x.BankNo + " " + x.BankName });
            ddlBankCode.DataSource = bankS;
            ddlBankCode.DataTextField = "BankName";
            ddlBankCode.DataValueField = "BankNo";
            ddlBankCode.DataBind();


            //營業時間
            Dictionary<int, string> weekly = new Dictionary<int, string>();
            
            weekly.Add(0, "週一");
            weekly.Add(1, "週二");
            weekly.Add(2, "週三");
            weekly.Add(3, "週四");
            weekly.Add(4, "週五");
            weekly.Add(5, "週六");
            weekly.Add(6, "週日");


            //chk_BusinessWeekly.DataValueField = "Key";
            //chk_BusinessWeekly.DataTextField = "Value";
            //chk_BusinessWeekly.DataSource = weekly;
            //chk_BusinessWeekly.DataBind();

            ////公休日
            //chk_close_BusinessWeekly.DataValueField = "Key";
            //chk_close_BusinessWeekly.DataTextField = "Value";
            //chk_close_BusinessWeekly.DataSource = weekly;
            //chk_close_BusinessWeekly.DataBind();


            Dictionary<string, string> weeklyHour = new Dictionary<string, string>();
            for (int iHour = 1; iHour <= 24; iHour++)
            {
                weeklyHour.Add(iHour.ToString().PadLeft(2, '0'), iHour.ToString().PadLeft(2, '0'));
            }
            ddl_WeeklyBeginHour.DataValueField = "Key";
            ddl_WeeklyBeginHour.DataTextField = "Value";
            ddl_WeeklyBeginHour.DataSource = weeklyHour;
            ddl_WeeklyBeginHour.DataBind();

            ddl_WeeklyEndHour.DataValueField = "Key";
            ddl_WeeklyEndHour.DataTextField = "Value";
            ddl_WeeklyEndHour.DataSource = weeklyHour;
            ddl_WeeklyEndHour.DataBind();

            //公休日
            ddl_close_WeeklyBeginHour.DataValueField = "Key";
            ddl_close_WeeklyBeginHour.DataTextField = "Value";
            ddl_close_WeeklyBeginHour.DataSource = weeklyHour;
            ddl_close_WeeklyBeginHour.DataBind();

            ddl_close_WeeklyEndHour.DataValueField = "Key";
            ddl_close_WeeklyEndHour.DataTextField = "Value";
            ddl_close_WeeklyEndHour.DataSource = weeklyHour;
            ddl_close_WeeklyEndHour.DataBind();


            Dictionary<string, string> weeklyMinute = new Dictionary<string, string>();
            for (int iMinute = 0; iMinute < 60; iMinute += 5)
            {
                weeklyMinute.Add(iMinute.ToString().PadLeft(2, '0'), iMinute.ToString().PadLeft(2, '0'));
            }
            ddl_WeeklyBeginMinute.DataValueField = "Key";
            ddl_WeeklyBeginMinute.DataTextField = "Value";
            ddl_WeeklyBeginMinute.DataSource = weeklyMinute;
            ddl_WeeklyBeginMinute.DataBind();

            ddl_WeeklyEndMinute.DataValueField = "Key";
            ddl_WeeklyEndMinute.DataTextField = "Value";
            ddl_WeeklyEndMinute.DataSource = weeklyMinute;
            ddl_WeeklyEndMinute.DataBind();

            ddl_close_WeeklyBeginMinute.DataValueField = "Key";
            ddl_close_WeeklyBeginMinute.DataTextField = "Value";
            ddl_close_WeeklyBeginMinute.DataSource = weeklyMinute;
            ddl_close_WeeklyBeginMinute.DataBind();

            ddl_close_WeeklyEndMinute.DataValueField = "Key";
            ddl_close_WeeklyEndMinute.DataTextField = "Value";
            ddl_close_WeeklyEndMinute.DataSource = weeklyMinute;
            ddl_close_WeeklyEndMinute.DataBind();

            //公休時間
            Dictionary<int, string> flags = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(StoreCloseDate)))
            {
                flags[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (StoreCloseDate)item);
            }
            ddlCloseDate.DataSource = flags;
            ddlCloseDate.DataTextField = "Value";
            ddlCloseDate.DataValueField = "Key";
            ddlCloseDate.DataBind();

            syncBusinessHour.NavigateUrl = "#divBusinessHour";
            syncCloseDate.NavigateUrl = "#divCloseDate";

        }

        private void EnabledSettings(Seller s, Store st)
        {
            #region 瀏覽權限檢查 (無全區瀏覽權限，會判斷商家是否有綁定業務，有的話只能檢視自己的工單)

            if (!CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReadAll))
            {
                if (s.SalesId != null)
                {
                    ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(s.SalesId.Value);
                    if (emp.IsLoaded && emp.Email != UserName)
                    {
                        ShowMessage(string.Format("業務歸屬: {0} 無法查看該商家店鋪資訊。", emp.EmpName), StoreContentMode.PrivilegeError);
                    }
                }
            }

            #endregion

            if (st.Guid != Guid.Empty)
            {
                #region 更新權限檢查 (尚未審核通過的店鋪，才可變更資料)

                if (st.TempStatus != (int)SellerTempStatus.Completed && CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Update))
                {
                    if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.FinanceApplied))
                    {
                        EnabledControls(true, true);
                    }
                    else
                    {
                        ViewPponDealCollection vpdc = PponFacade.GetOnUseViewPponDealListBySellerGuid(s.Guid);
                        bool isOnUse = vpdc.Count > 0;
                        EnabledControls(true, !isOnUse);
                    }
                }
                else
                {
                    EnabledControls(false, false);
                }

                #endregion

                #region 審核權限檢查

                // 申請核准
                btnStoreApprove.Visible = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Completed) && st.TempStatus == (int)SellerTempStatus.Applied;

                // 申請退回
                btnStoreReturned.Visible = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Completed) && st.TempStatus == (int)SellerTempStatus.Applied;

                // 申請變更
                btnStoreApply.Visible = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Applied) && st.TempStatus != (int)SellerTempStatus.Applied;
                
                #endregion
            }
        }

        private void EnabledControls(bool enabled, bool finance_enabled)
        {
            // 聯絡資訊
            txtStoreName.Enabled = enabled;
            ddlCompanyCityId.Enabled = enabled;
            ddlTownshipId.Enabled = enabled;
            txtCompanyAddress.Enabled = enabled;
            txtBossName.Enabled = enabled;
            txtPhone.Enabled = enabled;
            chkCreditCardAvailable.Enabled = enabled;
            txtMrt.Enabled = enabled;
            txtCar.Enabled = enabled;
            txtBus.Enabled = enabled;
            txtOV.Enabled = enabled;
            txtWebUrl.Enabled = enabled;
            txtFacebook.Enabled = enabled;
            txtBlog.Enabled = enabled;
            txtOtherUrl.Enabled = enabled;
            txtOpenTime.Enabled = enabled;
            txtCloseDate.Enabled = enabled;
            txtOthers.Enabled = enabled;

            //營業時間
            //chk_BusinessWeekly.Enabled = enabled;
            ddl_WeeklyBeginHour.Enabled = enabled;
            ddl_WeeklyBeginMinute.Enabled = enabled;
            ddl_WeeklyEndHour.Enabled = enabled;
            ddl_WeeklyEndMinute.Enabled = enabled;

            //公休日
            ddlCloseDate.Enabled = enabled;
            //chk_close_BusinessWeekly.Enabled = enabled;
            txtCloseMonthlyDay.Enabled = enabled;
            txtCloseBeginDate.Enabled = enabled;
            ddl_close_WeeklyBeginHour.Enabled = enabled;
            ddl_close_WeeklyBeginMinute.Enabled = enabled;
            ddl_close_WeeklyEndHour.Enabled = enabled;
            ddl_close_WeeklyEndMinute.Enabled = enabled;
            chk_close_allday.Enabled = enabled;

            // 匯款資訊
            txtCompanyName.Enabled = finance_enabled;
            txtCompanyID.Enabled = finance_enabled;
            ddlBankCode.Enabled = finance_enabled;
            ddlBranchCode.Enabled = finance_enabled;
            txtSignCompanyID.Enabled = finance_enabled;
            txtAccountId.Enabled = finance_enabled;
            txtAccount.Enabled = finance_enabled;
            txtFinanceName.Enabled = finance_enabled;
            txtFinanceTel.Enabled = finance_enabled;
            txtFinanceEmail.Enabled = finance_enabled;
            financeSync.Enabled = finance_enabled;

            btnSave.Visible = enabled | finance_enabled;
        }

        public Store GetStoreData(Store s)
        {
            // 店鋪資訊
            string branchname = txtStoreName.Text.Replace("《", string.Empty).Replace("》", string.Empty).Trim();
            s.StoreName = "《" + branchname + "》";
            int township = int.TryParse(hidTownshipId.Value, out township) ? township : 0;

            var TownShip = CityManager.TownShipGetById(township == 0 ? -1 : township);
            s.CityId = TownShip == null ? -1 : TownShip.ParentId.Value;

            s.TownshipId = township;
            if (s.TownshipId.HasValue)
            {
                KeyValuePair<string, string> coordinate = LocationFacade.GetArea(CityManager.CityTownShopStringGet(s.TownshipId.Value) + s.AddressString);
                if (coordinate.Key != null && coordinate.Value != null)
                {
                    s.Coordinate = LocationFacade.GetGeographyWKT(coordinate.Key, coordinate.Value);
                }
                else
                {
                    //不可為null，會造成error
                    s.Coordinate = LocationFacade.GetGeographyWKT("0", "0");
                }
            }
            s.AddressString = txtCompanyAddress.Text;
            s.CompanyBossName = txtBossName.Text;
            s.Phone = txtPhone.Text;
            s.CreditcardAvailable = chkCreditCardAvailable.Checked;
            s.Mrt = txtMrt.Text;
            s.Car = txtCar.Text;
            s.Bus = txtBus.Text;
            s.OtherVehicles = txtOV.Text;
            s.WebUrl = txtWebUrl.Text;
            s.FacebookUrl = txtFacebook.Text;
            s.BlogUrl = txtBlog.Text;
            s.OtherUrl = txtOtherUrl.Text;
            s.OpenTime = txtOpenTime.Text;
            s.CloseDate = txtCloseDate.Text;
            s.Remarks = txtOthers.Text;

            // 匯款資訊
            s.CompanyName = txtCompanyName.Text;
            s.CompanyID = txtCompanyID.Text;
            if (!string.IsNullOrWhiteSpace(ddlBankCode.SelectedValue))
            {
                if (ddlBankCode.SelectedValue != "-1")
                {
                    s.CompanyBankCode = ddlBankCode.SelectedValue;
                }                
                s.CompanyBranchCode = hidBranchCode.Value;
            }
            s.SignCompanyID = txtSignCompanyID.Text;
            s.CompanyAccount = txtAccountId.Text;
            s.CompanyAccountName = txtAccount.Text;
            s.AccountantName = txtFinanceName.Text;
            s.AccountantTel = txtFinanceTel.Text;
            s.CompanyEmail = txtFinanceEmail.Text;
            return s;
        }
        #endregion

        [WebMethod]
        public static dynamic BankInfoGetBranchList(string id)
        {
            return VourcherFacade.BankInfoGetBranchList(id).Select(x => new { BranchNo = x.BranchNo, BranchName = x.BranchNo + " " + x.BranchName });
        }
        [WebMethod]
        public static string GetWeeklyName(string frenquency, string weeklys, string beginTime, string endTime, bool allDay)
        {
            return SellerFacade.GetWeeklyNames(frenquency, weeklys, beginTime, endTime, allDay);
        }
        [WebMethod]
        public static bool SyncBusinessHour(string storeGuid,string opentime)
        {
            List<Guid> stGuid=  new JsonSerializer().Deserialize<List<Guid>>(storeGuid);
            return SellerFacade.StoreOpenTimeUpdate(stGuid, opentime);
        }
        [WebMethod]
        public static bool SyncCloseDate(string storeGuid, string closedate)
        {
            List<Guid> stGuid = new JsonSerializer().Deserialize<List<Guid>>(storeGuid);
            return SellerFacade.StoreCloseDateUpdate(stGuid, closedate);
        }
    }
}
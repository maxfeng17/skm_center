﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="ShoppingCartManage.aspx.cs" Inherits="LunchKingSite.Web.Sal.ShoppingCartManage" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>

<%@ Register Src="../User/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Src="~/Sal/ProposalMenu.ascx" TagName="ProposalMenu" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css") %>" rel="stylesheet" type="text/css" />
    <style>
        .mc-navbar .mc-navbtn {
            width: 10%;
        }
    </style>
    <script type="text/javascript">
        function checkAll(o) {
            $.each($("#tblShoppingCart input[type='checkbox']"), function (idx, obj) {
                if ($(obj).attr("id") != "checkAll") {
                    $(obj).prop("checked", $(o).is(":checked"));
                }
            });
        }
        function CheckChange() {
            var ids = [];
            $.each($("#tblShoppingCart input[type='checkbox']"), function (idx, obj) {
                if ($(obj).attr("id") != "checkAll") {
                    if ($(obj).is(":checked")) {
                        var litId = $(obj).closest("tr").find("span[id$='litId']").html();
                        if (litId != undefined && litId != "") {
                            ids.push(litId);
                        }
                    }
                }
            });
            if (ids.length != 0) {
                if (!confirm("確認異動" + ids.length + "個購物車?")) {
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url: '/Sal/ShoppingCartManage.aspx/ShoppingCartFreightsCheck',
                    data: "{'ids':'" + JSON.stringify(ids) + "','UserName': '<%= UserName %>'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var c = JSON.parse(data.d);
                        alert(c.Message);
                        location.reload();
                    },
                    error: function (xhr) {
                        console.log(xhr.responseText);
                    }
                });
            } else {
                alert("無勾選任何購物車。");
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <uc2:ProposalMenu ID="ProposalMenu" runat="server"></uc2:ProposalMenu>
    <div id="MainSet" class="mc-content">
        <div class="grui-form" id="shoppingCartSearch-form">
            <div class="form-unit">
                <label class="unit-label rd-unit-label">
                    賣家/購物車名稱</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox ID="txtName" runat="server" CssClass="input-small" Style="width: 200px" ClientIDMode="Static" placeholder="賣家名稱/購物車名稱"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" Text="搜尋" CssClass="btn btn-primary rd-mcacstbtn" OnClick="btnSearch_Click" />
                </div>
            </div>
        </div>
        <div class="grui-form" id="shoppingCartConfirm-form">
            <div>
                <input type="button" value="審核同意" class="btn btn-primary rd-mcacstbtn" onclick="return CheckChange()" />
            </div>
        </div>
        <div class="grui-form" id="shoppingCartlog-form">
            <div id="mc-table">
                <asp:Repeater ID="rptShoppingCart" runat="server" OnItemDataBound="rptShoppingCart_ItemDataBound">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table" id="tblShoppingCart">
                            <thead>
                                <tr class="rd-C-Hide">
                                    <th class="OrderDate">
                                        <asp:CheckBox ID="chkAll" runat="server" ClientIDMode="Static" onclick="checkAll(this)" /></th>
                                    <th class="OrderDate">狀態</th>
                                    <th class="OrderDate">賣家</th>
                                    <th class="OrderDate">序號</th>
                                    <th class="OrderDate">購物車名稱</th>
                                    <th class="OrderDate">免運門檻</th>
                                    <th class="OrderDate">未滿門檻收取運費</th>
                                    <th class="OrderDate">新增時間</th>
                                </tr>
                            </thead>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="mc-tableContentITEM">
                            <td style="text-align: left">
                                <asp:CheckBox ID="chkFreight" runat="server" />
                            </td>
                            <td style="text-align: left">
                                <span style="display:none">
                                <asp:Label ID="litId" runat="server" Text="<%# ((ShoppingCartManageList)Container.DataItem).Id %>"></asp:Label>
                                    </span>
                                <%# ((ShoppingCartManageList)Container.DataItem).FreightsStatusName %>
                            </td>
                            <td style="text-align: left; word-break: break-all;">
                                <%# ((ShoppingCartManageList)Container.DataItem).SellerName.Replace("|", "<br />") %>
                            </td>
                            <td style="text-align: left;">
                                <%# ((ShoppingCartManageList)Container.DataItem).UniqueId %>
                            </td>
                            <td style="text-align: left;">
                                <%# ((ShoppingCartManageList)Container.DataItem).FreightsName %>
                            </td>
                            <td style="text-align: center;">
                                <%# ((ShoppingCartManageList)Container.DataItem).NoFreightLimit %>
                            </td>
                            <td style="text-align: center;">
                                <%# ((ShoppingCartManageList)Container.DataItem).Freights %>
                            </td>
                            <td style="text-align: center;">
                                <%# ((ShoppingCartManageList)Container.DataItem).CreateTime.ToString("yyyy/MM/dd HH:mm") %>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="ProposalAssignFlag.aspx.cs" Inherits="LunchKingSite.Web.Sal.ProposalAssignFlag" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css") %>" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        $(function () {
            LoadData(1);

            $('#txtDate').datepicker({ dateFormat: 'yy/mm/dd' });

            $("#selType").change(function () {
                if ($(this).val() == "1") {
                    $("#spanBid").show();
                } else {
                    $("#spanBid").hide();
                    $('#txtDate').val("");
                }
            });

            $("#btnSearch").click(function () {
                LoadData($("#selType").val());
            });
        });

        function LoadData(type) {
            $.ajax({
                type: "POST",
                url: "ProposalAssignFlag.aspx/ProposalGetByOrderTimeS",
                contentType: "application/json; charset=utf-8",
                data: "{'type': '" + type + "','dealType':'" + $("#ddlDealType").val() + "','filterDate':'" + $("#txtDate").val() + "'}",
                dataType: "json",
                success: function (response) {
                    var c = response.d;
                    if (c != undefined) {
                        $("#proposalContentTable > tbody").empty();
                        var _html = "";
                        $.each(c, function (idx, val) {
                            _html += "<tr>";
                            _html += "<td style='text-align:center'>";
                            if (type == "1") {
                                //檔次
                                _html += "<a href='../../sal/ProposalContent.aspx?pid=" + val.Pid + "' target='_blank'>" + val.Pid + val.BrandName + "</a>";
                            } else {
                                //個人
                                _html += val.Pid;
                            }
                            _html += "</td>";
                            _html += "<td style='text-align:center' class='OrderTimeS'>" + val.OrderTimeS;//上檔日期
                            _html += "</td>";
                            _html += "<td style='text-align:center'>" + val.Setting;//檔次設定
                            _html += "</td>";
                            _html += "<td style='text-align:center'>" + val.CopyWriter;//文案編輯
                            _html += "</td>";
                            _html += "<td style='text-align:center'>" + val.ImageDesign;//圖片設計
                            _html += "</td>";
                            _html += "<td style='text-align:center'>" + val.ART;//大圖ART
                            _html += "</td>";
                            _html += "<td style='text-align:center'>" + val.Listing;//上架確認
                            _html += "</td>";
                            _html += "</tr>";
                        });
                        $("#proposalContentTable").append(_html);
                        if (type != "1") {
                            //檔次
                            $("#proposalContentTable th.OrderTimeS").hide();
                            $("#proposalContentTable td.OrderTimeS").hide();
                        } else {
                            $("#proposalContentTable th.OrderTimeS").show();
                            $("#proposalContentTable td.OrderTimeS").show();
                        }
                    }
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <asp:Panel ID="divMain" runat="server">
        <div class="mc-content">
            <div id="basic">
                <h1 class="rd-smll"><span class="breadcrumb flat">
	                <select id="selType">
                        <option value="1">提案單</option>
                        <option value="2">人名</option>
	                </select>
                    
                    <span id="spanBid">
                        <asp:DropDownList ID="ddlDealType" ClientIDMode="Static" runat="server" Width="100px"></asp:DropDownList>
                        上檔日期：<asp:TextBox ID="txtDate" runat="server" ClientIDMode="Static" Width="100px"></asp:TextBox></span>
                    <input type="button" id="btnSearch" class="btn btn-small btn-primary form-inline-btn" value="查詢" />
                  
                </h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table" id="proposalContentTable">
                                <thead>
                                <tr class="rd-C-Hide">
                                    <th> 
                                    </th>
                                    <th class="OrderTimeS">上檔日期
                                    </th>
                                    <th>檔次設定 
                                    </th>
                                    <th>文案編輯
                                    </th>
                                    <th>圖片設計
                                    </th>
                                    <th>大圖ART
                                    </th>
                                    <th>上架確認
                                    </th>
                                </tr>
                                </thead>
                        </table>
                    </div>
                 
                </div>
            </div>
        </div>
    </asp:Panel>
    
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="SellerList.aspx.cs" Inherits="LunchKingSite.Web.Sal.SellerList" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>

<%@ Register Src="../User/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Src="~/Sal/ProposalMenu.ascx" TagName="ProposalMenu" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/Tools/js/css/ui-smoothness/jquery-ui-1.10.3.custom.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/fancybox/jquery.fancybox.css") %>" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .mc-navbar .mc-navbtn {
            width: 10%;
        }
        .data-input {
            width: 45%;
            display: inline-block;
        }
        .grui-form .form-unit .data-input {
            margin-left: 0;
            margin-right: 0;
        }
            .grui-form .form-unit .data-input label {
                margin-right: 0;
            }
            .grui-form .form-unit .data-input input[type="checkbox"] {
                margin-left: 10px;
            }
        .end-unit {
            padding-left: 160px;
        }
        .unit-label {
            width: 25%;
            display: inline-block;
            padding-right: 10px;
        }
        .ui-autocomplete {
            max-height: 10em;
            overflow-y: auto;
            overflow-x: hidden;
        }
        .grui-form .form-unit .unit-label {
            display: inline-block;
            float: none; /*overwrite MasterPage.css*/
            width: 100px;
            -ms-word-break: keep-all;
            word-break: keep-all;
        }
        @media screen and (max-width: 480px) {
            .grui-form .form-unit .rd-unit-label {
                width: 35%;
            }
        }
    </style>
    <script type="text/javascript" src="../Tools/js/fancybox/jquery.fancybox.js"></script>
    <script type="text/javascript" src="../Tools/js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            RegisterSellerTreeOnClick();
            SellerNameAutoComplete();
            CompanyNameAutoComplete();
            SalesEmailAutoComplete();

            //進階搜尋1~31
            for (i = 1; i <= 31; i++)
            {
                $('#ddlNumber').append($("<option></option>").attr("value", i).text(i));
            }
            

            //顯示原先的展開
            Maintainimgbtn();

            

            
            //匯出
            $('#btnExport').click(function () {

                var SellerList = [];
                $('.Change').each(function () {
                    var check = $(this);
                    if (check.is(':checked'))
                    {
                        var SellerGuid = $(this).next();
                        SellerList.push(SellerGuid.val());
                    }
                });
                $('#hidExportSellerList').val(SellerList);

            })

            //匯出鈕預設鎖住
            $("#btnExport").prop('disabled', 'disabled');

            //匯出鈕是否鎖住
            $("[id*=chkChange]").change(function () {
                var CheckCount = 0;
                $('[id*=chkChange]').each(function () {
                    var check = $(this);
                    if (check.is(':checked'))
                        CheckCount++;
                });

                if (CheckCount == 0)
                    $("#btnExport").prop('disabled', 'disabled');
                else
                    $("#btnExport").prop('disabled', '');

            });

            //匯出是否全選
            $('#chkChangeAll').click(function () {
                if ($(this).is(':checked'))
                    $('.Change').prop('checked', true);
                else
                    $('.Change').prop('checked', false);
            })

            //權限顯示批次變更業務
            if ($('#hidPatchChangeSeller').val() == 0)
                $('.Export').hide();
            else if ($('#hidPatchChangeSeller').val() == 1)
                $('.Export').show();


            
            DataBindSellerLevel();

            
            
        });

        function WaitingBlock()
        {
            $.blockUI({
                message: "匯入中請稍後...",
                css: {
                    width: '20%',
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }

        function DataBindSellerLevel()
        {
            //塞商家類型checkbox
            $.ajax({
                type: "POST",
                url: "SellerList.aspx/GetSellerLevelList",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var table = $('<table></table>');
                    var html = '';
                    html += "<tbody>";
                    html += "<tr>";
                    $.map(data.d, function (item) {

                        var Label = item.Label;
                        var Value = item.Value;


                        html += "<td>";
                        html += '<input id="chkSellerLevel' + Value + '" type="checkbox" name="chkSellerLevel' + Value + '" value="' + Value + '">'
                        html += '<label for="chkSellerLevel">' + Label + '</label>'
                        html += "</td>";

                    })

                    html += "</tr>";
                    html += "</tbody>";

                    $('#divSellerLevel').append(html);


                    //恢復原本篩選
                    var SellerLevel = $('#hidSellerLevel').val();
                    if (SellerLevel != '')
                    {
                        var level = [];
                        level = SellerLevel.split(',');
                        for (var l in level) {
                            $('#chkSellerLevel' + level[l]).prop('checked', true);
                        }
                    }
                },
                error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        }

        function RegisterSellerTreeOnClick() {
            $('[id*=btnSellerTree]').on('click', function () {
                var sellerguid = $(this).prev().text();

                $('[id*=btnSellerTree]').fancybox({
                    maxWidth: 850,
                    maxHeight: 600,
                    fitToView: false,
                    width: '70%',
                    height: '70%',
                    autoSize: false,
                    closeClick: false,
                    openEffect: 'none',
                    closeEffect: 'none',
                    href: '../Sal/SellerTreeView?sid=' + sellerguid + '&type=proposal',
                    helpers: {
                        overlay: { closeClick: false }
                    }
                });
            });
        }

        <%--[品牌名稱]提示輸入選單--%>
        function SellerNameAutoComplete() {
            $('#<%=txtSellerName.ClientID%>').autocomplete({
                source: []
            });

            $('#<%=txtSellerName.ClientID%>').on('input', function () {
                var theInput = $(this);
                if (theInput.val().length > 0) {
                    $.ajax({
                        url: "SellerList.aspx/GetSellerNameAutoCompelte",
                        method: "POST",//1.9.0
                        contentType: "application/json",
                        data: JSON.stringify({
                            partName: $(this).val()
                        }),
                        success: function (data) {
                            theInput.autocomplete("option", "source", data.d);
                        }
                    });
                }
            });
        }
        <%--[簽約公司名稱]提示輸入選單--%>
        function CompanyNameAutoComplete() {
            $('#<%=txtCompanyName.ClientID%>').autocomplete({
                source: []
            });

            $('#<%=txtCompanyName.ClientID%>').on('input', function () {
                var theInput = $(this);
                if (theInput.val().length > 0) {
                    $.ajax({
                        url: "SellerList.aspx/GetCompanyNameAutoCompelte",
                        method: "POST",//1.9.0
                        contentType: "application/json",
                        data: JSON.stringify({
                            partName: $(this).val()
                        }),
                        success: function (data) {
                            theInput.autocomplete("option", "source", data.d);
                        }
                    });
                }
            });
        }
        <%--[負責業務]提示輸入選單--%>
        function SalesEmailAutoComplete() {
            $('#<%=txtSalesEmail.ClientID%>').autocomplete({
                source: []
            });

            $('#<%=txtSalesEmail.ClientID%>').on('input', function () {
                var theInput = $(this);
                if (theInput.val().length > 0) {
                    $.ajax({
                        url: "SellerList.aspx/GetSalesEmailAutoCompelte",
                        method: "POST",//1.9.0
                        contentType: "application/json",
                        data: JSON.stringify({
                            partEmail: $(this).val()
                        }),
                        success: function (data) {
                            theInput.autocomplete("option", "source", data.d);
                        }
                    });
                }
            });
        }

        //進階搜尋開關
        function HideContent(obj) {
            
            if ($("#AdvanceSearch").is(':visible') == true) {
                //關閉進階搜尋
                $("#AdvanceSearch").css('display', 'none');
                $(obj).attr('src', '../Themes/default/images/17Life/G2/ArrowUp.png');

                //清空篩選條件
                BindTownship($('#ddlCompanyCityId'));
                $('#ddlDept option')[0].selected = true;
                $('#ddlCompanyCityId option')[0].selected = true;
                $('#ddlNumber option')[0].selected = true;
                $('#ddlDatepart option')[0].selected = true;
                $('[id*=chkSellerLevel]').prop('checked', false);
                $('[id*=chkProperty]').prop('checked', false);

            }
            else {
                //開啟進階搜尋
                $("#AdvanceSearch").css('display', 'block');
                $(obj).attr('src', '../Themes/default/images/17Life/G2/ArrowDown.png');

            }
        }

        //撈出縣市資料
        function BindTownship(obj) {
            $.ajax({
                type: "POST",
                url: "<%= config.SSLSiteUrl %>/api/locationservice/GetTownships",
                data: "{'cid': '" + $(obj).val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var township = $('#ddlTownshipId');
                    $(township).find('option').remove();
                    $.each(response.Data, function (index, item) {
                        $(township).append($('<option></option>').attr('value', item.id).text(item.name));
                    });

                    $(township).val($('#hidTownshipId').val());
                    SetTownshipId(township);
                },
                error: function (xhr) {
                    console.log(xhr.responseText);
                }
            }); 
        }

        //設定行政區ID
        function SetTownshipId(obj) {
            $('#hidTownshipId').val($(obj).val());
        }



        //儲存進階搜尋篩選條件
        function SaveControl()
        {
            var check = true;
            if ($('#ddlNumber').val() == 0 && $('#ddlDatepart').val() != '') {
                alert('請選擇商家聯繫狀況之數字');
                check = false;
            }
            else if ($('#ddlNumber').val() != 0 && $('#ddlDatepart').val() == '') {
                alert('請選擇商家聯繫狀況之單位');
                check = false;
            }
            else
            {
                //紀錄進階搜尋展開狀態
                var hidMaintainimgbtn = document.getElementById("<%=hidMaintainimgbtn.ClientID %>");
                hidMaintainimgbtn.value = $("#imgbtnHideContent").attr("src");

                //商家規模
                var SellerLevelList = [];
                $('[id*=chkSellerLevel]').each(function () {
                    if ($(this).is(':checked'))
                    {
                        var value = $(this).val();
                        SellerLevelList.push(value)
                    }
                        
                });

                $('#hidSellerLevel').val(SellerLevelList.join(','));

            
                

                //商家類型
                var OnDeal = '';
                var HaveCooperation = '';
                var YearNoCooperation = '';
                var NotCooperation = '';
                var SellerPorperty = '';
                $('[id*=OnDeal]').each(function () {

                    if ($(this).is(':checked'))
                    {
                        var id = $(this).prop('id');
                
                        if (id.indexOf('<% =ProposalSellerPorperty.Ppon.ToString()%>') > 0)
                            OnDeal += "<%=(int)ProposalSellerPorperty.Ppon%>" + ",";
                        else if (id.indexOf('<% =ProposalSellerPorperty.GroupCoupon.ToString()%>') > 0)
                            OnDeal += "<%=(int)ProposalSellerPorperty.GroupCoupon%>" + ",";
                        else if (id.indexOf('<% =ProposalSellerPorperty.RegularsCard.ToString()%>') > 0)
                            OnDeal += "<%=(int)ProposalSellerPorperty.RegularsCard%>" + ",";
                        else if (id.indexOf('<% =ProposalSellerPorperty.HomeDelivery.ToString()%>') > 0)
                            OnDeal += "<%=(int)ProposalSellerPorperty.HomeDelivery%>" + ",";
                    } 
                });

                if (OnDeal != '')
                    OnDeal = "OnDeal[" + OnDeal.replace(/,$/gi, "") + "]";

                $('[id*=HaveCooperation]').each(function () {

                    if ($(this).is(':checked'))
                    {
                        var id = $(this).prop('id');
                
                        if (id.indexOf('<% =ProposalSellerPorperty.Ppon.ToString()%>') > 0)
                            HaveCooperation += "<%=(int)ProposalSellerPorperty.Ppon%>" + ",";
                        else if (id.indexOf('<% =ProposalSellerPorperty.GroupCoupon.ToString()%>') > 0)
                            HaveCooperation += "<%=(int)ProposalSellerPorperty.GroupCoupon%>" + ",";
                        else if (id.indexOf('<% =ProposalSellerPorperty.RegularsCard.ToString()%>') > 0)
                            HaveCooperation += "<%=(int)ProposalSellerPorperty.RegularsCard%>" + ",";
                        else if (id.indexOf('<% =ProposalSellerPorperty.HomeDelivery.ToString()%>') > 0)
                            HaveCooperation += "<%=(int)ProposalSellerPorperty.HomeDelivery%>" + ",";
                    } 
                });

                if (HaveCooperation != '')
                    HaveCooperation = "HaveCooperation[" + HaveCooperation.replace(/,$/gi, "") + "]";

            

                $('[id*=YearNoCooperation]').each(function () {

                    if ($(this).is(':checked'))
                    {
                        var id = $(this).prop('id');
                
                        if (id.indexOf('<% =ProposalSellerPorperty.Ppon.ToString()%>') > 0)
                            YearNoCooperation += "<%=(int)ProposalSellerPorperty.Ppon%>" + ",";
                        else if (id.indexOf('<% =ProposalSellerPorperty.GroupCoupon.ToString()%>') > 0)
                            YearNoCooperation += "<%=(int)ProposalSellerPorperty.GroupCoupon%>" + ",";
                        else if (id.indexOf('<% =ProposalSellerPorperty.RegularsCard.ToString()%>') > 0)
                            YearNoCooperation += "<%=(int)ProposalSellerPorperty.RegularsCard%>" + ",";
                        else if (id.indexOf('<% =ProposalSellerPorperty.HomeDelivery.ToString()%>') > 0)
                            YearNoCooperation += "<%=(int)ProposalSellerPorperty.HomeDelivery%>" + ",";
                    } 
                });

                if (YearNoCooperation != '')
                    YearNoCooperation = "YearNoCooperation[" + YearNoCooperation.replace(/,$/gi, "") + "]";


                $('[id*=NotCooperation]').each(function () {

                    if ($(this).is(':checked'))
                    {
                        var id = $(this).prop('id');
                
                        if (id.indexOf('<% =ProposalSellerPorperty.Ppon.ToString()%>') > 0)
                            NotCooperation += "<%=(int)ProposalSellerPorperty.Ppon%>" + ",";
                        else if (id.indexOf('<% =ProposalSellerPorperty.GroupCoupon.ToString()%>') > 0)
                            NotCooperation += "<%=(int)ProposalSellerPorperty.GroupCoupon%>" + ",";
                        else if (id.indexOf('<% =ProposalSellerPorperty.RegularsCard.ToString()%>') > 0)
                            NotCooperation += "<%=(int)ProposalSellerPorperty.RegularsCard%>" + ",";
                        else if (id.indexOf('<% =ProposalSellerPorperty.HomeDelivery.ToString()%>') > 0)
                            NotCooperation += "<%=(int)ProposalSellerPorperty.HomeDelivery%>" + ",";
                    } 
                });

                if (NotCooperation != '')
                    NotCooperation = "NotCooperation[" + NotCooperation.replace(/,$/gi, "") + "]";


          

                if (OnDeal != '')
                    SellerPorperty = SellerPorperty + OnDeal + "/";
                if (HaveCooperation != '')
                    SellerPorperty = SellerPorperty + HaveCooperation + "/";
                if (YearNoCooperation != '')
                    SellerPorperty = SellerPorperty + YearNoCooperation + "/";
                if (NotCooperation != '')
                    SellerPorperty = SellerPorperty + NotCooperation + "/";

                SellerPorperty = SellerPorperty.replace(/\/$/gi, "")

                $('#hidSellerPorperty').val(SellerPorperty);
                $('#hidNumber').val($('#ddlNumber').val());

            }

            return check;

        }

        //恢復進階搜尋篩選條件
        function Maintainimgbtn() {
            
            var hidMaintainimgbtn = document.getElementById("<%=hidMaintainimgbtn.ClientID %>");
            if (hidMaintainimgbtn.value != "") {
                $("#imgbtnHideContent").attr('src', hidMaintainimgbtn.value);

                if (hidMaintainimgbtn.value == '../Themes/default/images/17Life/G2/ArrowUp.png') {
                    $("#AdvanceSearch").css('display', 'none');
                    $("#imgbtnHideContent").attr('src', '../Themes/default/images/17Life/G2/ArrowUp.png');
                    $("#OriginalSearch *").prop('disabled', '');
                }
                else {
                    $("#AdvanceSearch").css('display', 'block');
                    $("#imgbtnHideContent").attr('src', '../Themes/default/images/17Life/G2/ArrowDown.png');

                    $("#OriginalSearch *").prop('disabled', 'disabled');
                }
            }

            BindTownship($('#ddlCompanyCityId'));
            if ($('#hidNumber').val() != '')
                $("#ddlNumber").prop("value", $('#hidNumber').val());


        }

        //檢查負責業務Email
        function SalesEmailExist(obj) {
            $.ajax({
                url: "ProposalList.aspx/SalesEmailExist",
                method: "POST",//1.9.0
                contentType: "application/json",
                data: JSON.stringify({
                    email: $(obj).val()
                }),
                success: function (response) {
                    if (!response.d)
                        alert("請輸入正確的負責業務Email！");
                }
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <uc2:ProposalMenu ID="ProposalMenu" runat="server" ></uc2:ProposalMenu>
    <div class="mc-content">
        <h1 class="rd-smll">商家列表</h1>
        <hr class="header_hr">
        <asp:Panel ID="divSearch" runat="server" DefaultButton="btnSearch">
            <div class="grui-form">
                <div class="form-unit">
                    <div class="data-input rd-data-input">
                        <label class="unit-label rd-unit-label">品牌/店家名稱</label>
                        <asp:TextBox ID="txtSellerName" runat="server" CssClass="input-half" Style="width: 200px"></asp:TextBox>
                    </div>
                    <div class="data-input rd-data-input">
                        <label class="unit-label rd-unit-label">負責人</label>
                        <asp:TextBox ID="txtBossName" runat="server" CssClass="input-half" Style="width: 200px"></asp:TextBox>
                    </div>
                </div>
                <div class="form-unit">
                    <div class="data-input rd-data-input">
                        <label class="unit-label rd-unit-label">簽約公司名稱</label>
                        <asp:TextBox ID="txtCompanyName" runat="server" CssClass="input-half" Style="width: 200px"></asp:TextBox>
                    </div>
                    <div class="data-input rd-data-input">
                        <label class="unit-label rd-unit-label">統一編號/ID</label>
                        <asp:TextBox ID="txtSignCompanyId" runat="server" CssClass="input-half" Style="width: 200px"></asp:TextBox>
                    </div>
                </div>
                <div class="form-unit">
                    <div class="data-input rd-data-input" style="width:45%;">
                        <label class="unit-label rd-unit-label">審核狀態</label>
                        <asp:DropDownList ID="ddlTempStatus" runat="server" AppendDataBoundItems="true" Width="212px">
                            <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="data-input rd-data-input" style="width:54%;">
                        <label class="unit-label rd-unit-label">負責業務</label>
                        <asp:TextBox ID="txtSalesEmail" runat="server" CssClass="input-half" Style="width:200px;margin-right:0;" placeholder="請輸入Email" onchange="SalesEmailExist(this)"></asp:TextBox>
                        <div style="display:inline-block;">
                            <asp:CheckBox ID="chkIsPrivate" runat="server" Text="自己的" />
                            <asp:CheckBox ID="chkIsPublic" runat="server" Text="公池" />
                        </div>
                    </div>
                </div>
                <div class="form-unit">
                    <div class="data-input rd-data-input" style="margin-left:650px">

                        <asp:DropDownList ID="ddlChangePage" runat="server" OnSelectedIndexChanged="btnSearch_Click" ClientIDMode="Static" AutoPostBack="true" Width="100px">
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>30</asp:ListItem>
                            <asp:ListItem>40</asp:ListItem>
                            <asp:ListItem>50</asp:ListItem>
                            <asp:ListItem>100</asp:ListItem>
                            <asp:ListItem>200</asp:ListItem>
                            <asp:ListItem>500</asp:ListItem>
                        </asp:DropDownList>

                        <span style="color: #BF0000; font-weight: bold;">進階搜尋</span>
                        <asp:ImageButton ID="imgbtnHideContent" runat="server" ImageUrl="~/Themes/default/images/17Life/G2/ArrowUp.png" OnClientClick="HideContent(this); return false;" ClientIDMode="Static" />
                        <asp:HiddenField ID="hidMaintainimgbtn" runat="server" ClientIDMode="Static" />

                    </div>

                </div>

                <div class="form-unit">
                    <div class="data-input rd-data-input" id="AdvanceSearch" style="width:900px;display:none">
                        <div class="form-unit">
                            <div class="data-input rd-data-input">
                                <label class="unit-label rd-unit-label">業務部門</label>
                                <asp:DropDownList ID="ddlDept" runat="server" CssClass="select-wauto" Width="200px" AppendDataBoundItems="true" ClientIDMode="Static">
                                    <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="data-input rd-data-input">
                                <label class="unit-label rd-unit-label">商家地區</label>
                                <asp:DropDownList ID="ddlCompanyCityId" runat="server" onchange="BindTownship(this);" Width="100px" ClientIDMode="Static" AppendDataBoundItems="true">
                                    <asp:ListItem Text="請選擇" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HiddenField ID="hidTownshipId" runat="server" ClientIDMode="Static" />
                                <asp:DropDownList ID="ddlTownshipId" runat="server" Width="100px"  onchange="SetTownshipId(this);" ClientIDMode="Static">
                                </asp:DropDownList>
                            </div>
                        </div>

                       <div class="form-unit">
                            <label class="unit-label rd-unit-label">商家規模<br />(可複選)</label>
                            <div class="data-input rd-data-input">
                                <%--<asp:CheckBoxList ID="chkSellerLevel" runat="server" RepeatColumns="10"></asp:CheckBoxList>--%>
                                <div id="divSellerLevel"></div>
                                <asp:HiddenField ID="hidSellerLevel" runat="server" ClientIDMode="Static" />
                            </div>
                        </div>

                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">商家類型<br />(可複選)</label>
                            <div class="data-input rd-data-input">
                                <table style="width:800px">
                                    <tr>
                                        <td>好康</td>
                                        <td><asp:CheckBox ID="chkPropertyPponOnDeal" runat="server" Text="上檔中" ClientIDMode="Static" />
                                            <asp:CheckBox ID="chkPropertyPponHaveCooperation" runat="server" Text="已合作" ClientIDMode="Static" />
                                            <asp:CheckBox ID="chkPropertyPponYearNoCooperation" runat="server" Text="一年以上未合作" ClientIDMode="Static" />
                                            <asp:CheckBox ID="chkPropertyPponNotCooperation" runat="server" Text="從未合作" ClientIDMode="Static" /></td>
                                    </tr>
                                    <tr>
                                        <td>成套票券</td>
                                        <td><asp:CheckBox ID="chkPropertyGroupCouponOnDeal" runat="server" Text="上檔中" ClientIDMode="Static" />
                                            <asp:CheckBox ID="chkPropertyGroupCouponHaveCooperation" runat="server" Text="已合作" ClientIDMode="Static" />
                                            <asp:CheckBox ID="chkPropertyGroupCouponYearNoCooperation" runat="server" Text="一年以上未合作" ClientIDMode="Static" />
                                            <asp:CheckBox ID="chkPropertyGroupCouponNotCooperation" runat="server" Text="從未合作" ClientIDMode="Static" /></td>
                                    </tr>
                                    <tr>
                                        <td>熟客卡</td>
                                        <td><asp:CheckBox ID="chkPropertyRegularsCardOnDeal" runat="server" Text="上檔中" ClientIDMode="Static" />
                                            <asp:CheckBox ID="chkPropertyRegularsCardHaveCooperation" runat="server" Text="已合作" ClientIDMode="Static" />
                                            <asp:CheckBox ID="chkPropertyRegularsCardYearNoCooperation" runat="server" Text="一年以上未合作" ClientIDMode="Static" />
                                            <asp:CheckBox ID="chkPropertyRegularsCardNotCooperation" runat="server" Text="從未合作" ClientIDMode="Static" /></td>
                                    </tr>
                                    <tr>
                                        <td>宅配</td>
                                        <td><asp:CheckBox ID="chkPropertyHomeDeliveryOnDeal" runat="server" Text="上檔中" ClientIDMode="Static" />
                                            <asp:CheckBox ID="chkPropertyHomeDeliveryHaveCooperation" runat="server" Text="已合作" ClientIDMode="Static" />
                                            <asp:CheckBox ID="chkPropertyHomeDeliveryYearNoCooperation" runat="server" Text="一年以上未合作" ClientIDMode="Static" />
                                            <asp:CheckBox ID="chkPropertyHomeDeliveryNotCooperation" runat="server" Text="從未合作" ClientIDMode="Static" /></td>
                                    </tr>
                                </table>
                                <asp:HiddenField ID="hidSellerPorperty" runat="server" ClientIDMode="Static" />
                            </div>
                        </div>

                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">商家聯繫狀況</label>
                            <div class="data-input rd-data-input">
                                <asp:DropDownList ID="ddlNumber" runat="server"  ClientIDMode="Static" Width="100px">
                                    <asp:ListItem Text="請選擇數字" Value="0" ></asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlDatepart" runat="server"  ClientIDMode="Static"  Width="120px">
                                    <asp:ListItem Text="請選擇計算單位" Value=""></asp:ListItem>
                                    <asp:ListItem Text="年" Value="year"></asp:ListItem>
                                    <asp:ListItem Text="個月" Value="month"></asp:ListItem>
                                    <asp:ListItem Text="天" Value="day"></asp:ListItem>
                                </asp:DropDownList>
                                未聯繫
                                <asp:HiddenField ID="hidNumber" runat="server" ClientIDMode="Static" />
                            </div>
                        </div>                        
                    </div>
                    <div class="form-unit end-unit">
                        <div class="data-input rd-data-input" style="width:60%">
                            <asp:Button ID="btnSearch" runat="server" Text="搜尋" CssClass="btn rd-mcacstbtn" OnClick="btnSearch_Click" OnClientClick="return SaveControl();" />
                            <asp:Button ID="btnSellerAdd" runat="server" Text="新增商家" CssClass="btn btn-primary rd-mcacstbtn" OnClick="btnSellerAdd_Click" Visible="false" />
                            <asp:Button ID="btnSellerBatch" runat="server" Text="商家批次處理" CssClass="btn btn-primary rd-mcacstbtn" OnClick="btnSellerBatch_Click" Visible="false" />
                            <p style="padding-left: 10px">
                                <a style="color: #08c" href="MembershipCardQuery.aspx" target="_blank">熟客卡管理</a>
                            </p>

                        </div>

                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hidPatchChangeSeller" runat="server"  ClientIDMode="Static" Value="0" />
            <asp:HiddenField ID="hidRowCount" runat="server"  ClientIDMode="Static" Value="0" />
        </asp:Panel>
        <asp:Panel ID="divSellerList" runat="server" Visible="false">
            <div class="Export" style="display:none">
                <label class="unit-label rd-unit-label">批次轉換業務功能</label>
                <asp:Button ID="btnExport" CssClass="btn rd-mcacstbtn" runat="server" Text="匯出" OnClick="btnExport_Click" ClientIDMode="Static" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:FileUpload ID="FUImport" runat="server" />
                <asp:Button ID="btnImport" CssClass="btn rd-mcacstbtn" runat="server" Text="匯入" OnClick="btnImport_Click" OnClientClick="WaitingBlock();" />
                <asp:HiddenField ID="hidExportSellerList" runat="server" ClientIDMode="Static" />
            </div>

            <div id="mc-table">
                <asp:Repeater ID="rptSeller" runat="server" OnItemDataBound="rptSeller_ItemDataBound">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                            <tr class="rd-C-Hide">
                                <th class="Export OrderSerial" style="display:none">
                                    <input id="chkChangeAll" type="checkbox" />
                                </th>
                                <th class="OrderDate">編號
                                </th>
                                <th class="OrderDate">地區
                                </th>
                                <th class="OrderSerial">規模
                                </th>
                                <th class="OrderDate">品牌名稱
                                </th>
                                <th class="OrderDate">上層公司名稱
                                </th>
                                <th class="OrderName">簽約公司名稱
                                </th>
                                <th class="OrderDate">業務
                                </th>
                                <th class="OrderCouponState">狀態
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="mc-tableContentITEM">
                            <td class="Export OrderSerial"  style="display:none;width:3%">
                                <input id="chkChange" type="checkbox" class="Change"/>
                                <input id="hidSellerGuid" type="hidden" value="<%# ((Seller)Container.DataItem).Guid %>" />
                            </td>
                            <td class="OrderDate" style="width:8%">
                                <span class="rd-Detailtitle">編號：</span>
                                <%# ((Seller)Container.DataItem).SellerId %>
                            </td>
                            <td class="OrderDate" style="text-align:left;width:5%">
                                <span class="rd-Detailtitle">地區：</span>
                                <%# CityManager.CityTownShopStringGet(((Seller)Container.DataItem).CityId) %>
                            </td>
                            <td class="OrderSerial" style="width:4%">
                                <%# (((Seller)Container.DataItem).SellerLevel).HasValue ? Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ((SellerLevel)((Seller)Container.DataItem).SellerLevel)).Split('：')[0] : string.Empty %>
                            </td>
                            <td class="OrderDate" style="text-align:left;width:20%">
                                <a target="_blank" href="SellerContent.aspx?sid=<%# ((Seller)Container.DataItem).Guid %>"><%# ((Seller)Container.DataItem).SellerName %></a>
                            </td>
                            <td class="OrderDate" style="text-align:left;width:18%">
                                <span class="rd-Detailtitle">上層賣家名稱：</span>
                                <asp:HyperLink ID="parentSellerLink" runat="server"></asp:HyperLink>
                                <p style="display:none">
                                    <asp:Literal ID="liSelleGuid" runat="server" Text="<%# ((Seller)Container.DataItem).Guid %>"></asp:Literal>
                                </p>
                                <asp:ImageButton ID="btnSellerTree" runat="server" ImageUrl="~/Themes/default/images/17Life/B5/b_search.jpg" Visible="false" CssClass="fancybox.iframe"/>
                                
                            </td>
                            <td class="OrderDate" style="text-align:left;width:16%">
                                <span class="rd-Detailtitle">簽約公司名稱：</span><%# ((Seller)Container.DataItem).CompanyName %>
                            </td>
                            <td class="OrderDate" style="text-align:left;width:13%">
                                <span class="rd-Detailtitle">業務：</span>
                                <asp:Literal ID="liSellerSales" runat="server"></asp:Literal>
                            </td>
                            <td class="OrderExp" style="text-align:left;width:13%">
                                <span class="rd-Detailtitle">狀態：</span>
                                <%# Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ((SellerTempStatus)((Seller)Container.DataItem).TempStatus)) %> <%# (StoreStatus)((Seller)Container.DataItem).StoreStatus == StoreStatus.Cancel ? "(隱藏)" : ""%>
                                <br />
                                <asp:Literal ID="liStatusDetail" runat="server"></asp:Literal>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <uc1:Pager ID="ucPager" runat="server" PageSize="10" ongetcount="GetCount" onupdate="UpdateHandler"></uc1:Pager>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

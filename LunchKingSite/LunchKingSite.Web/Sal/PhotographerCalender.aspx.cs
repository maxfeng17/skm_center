﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Views;
using System.Web.Services;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System.Web.Security;
using System.Net.Mail;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.SellerSales;

namespace LunchKingSite.Web.Sal
{
    public partial class PhotographerCalender : RolePage, ISalesPhotographerCalenderView
    {
        public static  ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public IHumanProvider hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        public IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        #region props
        private SalesPhotographerCalenderPresenter _presenter;
        public SalesPhotographerCalenderPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public int MonthCount
        {
            get
            {
                return 1;
            }
        }

        public int DealType1
        {
            get
            {
                int flag = int.TryParse(hidDealType.Value, out flag) ? flag : 0;
                return flag;
            }
        }
        public int DealType2
        {
            get
            {
                int flag = int.TryParse(hidDealSubType.Value, out flag) ? flag : 0;
                return flag;
            }
        }

        public string Dept
        {
            get
            {
                return ddlDept.SelectedValue;
            }
        }

        public string EmpDept
        {
            get
            {
                ViewEmployee emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, UserName);
                return emp.DeptId;
            }
        }

        public int EmpUserId
        {
            get
            {
                ViewEmployee emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, UserName);
                return emp.UserId;
            }
        }

        private int _Year;
        public int Year
        {
            get
            {
                if(_Year == 0 )
                {
                    if (rptMonth.Items.Count == 0)
                        _Year = DateTime.Now.Year;
                    else
                    {
                        Literal liMonth = (Literal)rptMonth.Items[0].FindControl("liMonth");
                        _Year = Convert.ToInt32(liMonth.Text.Substring(0, 4));
                    }
                    
                }
                
                return _Year;
            }
        }

        private int _Month;
        public int Month
        {
            get
            {
                if(_Month == 0)
                {
                    if (rptMonth.Items.Count == 0)
                        _Month = DateTime.Now.Month;
                    else
                    {
                        Literal liMonth = (Literal)rptMonth.Items[0].FindControl("liMonth");
                        _Month = Convert.ToInt32(liMonth.Text.Substring(7, 2));
                    }
                    
                }
                return _Month;
            }
        }

        public int PhotoSource
        {
            get
            {
                int flag = int.TryParse(rdblPhotoSource.SelectedValue, out flag) ? flag : 0;
                return flag;
            }
        }

        public int? SalesId
        {
            get
            {
                if (string.IsNullOrEmpty(txtSalesName.Text))
                    return null;
                else
                    return HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.EmpName, txtSalesName.Text).UserId;
            }

        }

        public string Photographer
        {
            get
            {
                return dllPhotographer.Text;
            }
        }

        #endregion

        #region event
        public event EventHandler Search;
        public event EventHandler PreviousSearch;
        public event EventHandler NextSearch;
        #endregion

        #region method
        public void SetPponDealCalendar(Dictionary<ViewPponDealCalendar, Proposal> vpdc)
        {
            
            DateTime now = DateTime.Now;
            Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>> data = new Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>();
            List<Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>> dataList = new List<Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>>();
            Dictionary<DateTime, List<Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>>> rtn = new Dictionary<DateTime, List<Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>>>();
            for (int m = 0; m < MonthCount; m++)
            {
                DateTime firstDay = new DateTime(_Year, _Month, 1);
                if (_Month + m > 12)
                {
                    firstDay = new DateTime(_Year + 1, 1, 1);
                }
                else
                {
                    firstDay = new DateTime(_Year, _Month + m, 1);
                }
                for (int i = 0; i < (int)firstDay.DayOfWeek; i++)
                {
                    data.Add(DateTime.MinValue.AddSeconds(i), new Dictionary<ViewPponDealCalendar, Proposal>());
                }
                for (int i = 0; i < firstDay.GetCountDaysOfMonth(); i++)
                {
                    DateTime thedate = firstDay.AddDays(i);
                    Dictionary<ViewPponDealCalendar, Proposal> vpds = vpdc.Where(x => x.Key.BusinessHourOrderTimeS.Date == thedate).ToDictionary(x => x.Key, y => y.Value);
                    if (vpds.Count == 0)
                    {
                        vpds.Add(new ViewPponDealCalendar(), new Proposal());
                    }
                    vpds = vpds.OrderBy(x => getPhotographyTime(x.Value)).ToDictionary(x => x.Key, y => y.Value);
                    data.Add(thedate, vpds);
                    if (thedate.DayOfWeek == DayOfWeek.Saturday || firstDay.GetLastDayOfMonth() == thedate)
                    {
                        if (firstDay.GetLastDayOfMonth() == thedate)
                        {
                            for (int l = 0; l < (int)DayOfWeek.Saturday - (int)thedate.DayOfWeek; l++)
                            {
                                if (thedate.DayOfWeek != DayOfWeek.Friday)
                                {
                                    data.Add(DateTime.MinValue.AddSeconds(l), new Dictionary<ViewPponDealCalendar, Proposal>());
                                }
                            }
                        }
                        dataList.Add(new Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>(data));
                        data.Clear();
                    }
                }
                rtn.Add(firstDay, new List<Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>>(dataList));
                dataList.Clear();
            }
            rptMonth.DataSource = rtn;
            rptMonth.DataBind();
        }

        /// <summary>
        /// 攝影時間為排序
        /// </summary>
        /// <param name="pro"></param>
        /// <returns></returns>
        private DateTime getPhotographyTime(Proposal pro)
        {
            DateTime photographyTime = new DateTime();


            if (pro.Id != 0)
            {
                string photoString = string.Empty;
                if (Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.Photographers))
                {
                    Dictionary<ProposalPhotographer, string> specialText =
                        new JsonSerializer().Deserialize<Dictionary<ProposalPhotographer, string>>(
                            pro.SpecialAppointFlagText);
                    if (specialText != null && specialText.Count > 0)
                    {
                        photoString =
                            specialText[ProposalPhotographer.PhotographerAppointCheck];

                        string tempTime = string.Format("{0} {1}:{2}",
                            photoString.Split("|").Length > 0 ? photoString.Split('|')[0] : "",
                            photoString.Split("|").Length > 1 ? photoString.Split('|')[1] : "",
                            photoString.Split("|").Length > 2 ? photoString.Split('|')[2] : "");

                        photographyTime = Convert.ToDateTime(tempTime);
                    }
                }

            }

            return photographyTime;
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IntialControls();
                //Presenter.OnViewInitialized();


            }
            Presenter.OnViewInitialized(); //javascrip postback時也能同步更新資訊
            _presenter.OnViewLoaded();


        }
        protected void rptMonth_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<DateTime, List<Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>>>)
            {
                KeyValuePair<DateTime, List<Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>>> dataItem = (KeyValuePair<DateTime, List<Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>>>)e.Item.DataItem;
                Literal liMonth = (Literal)e.Item.FindControl("liMonth");
                Literal liTotalDealCount = (Literal)e.Item.FindControl("liTotalDealCount");
                Repeater rptWeek = (Repeater)e.Item.FindControl("rptWeek");
                liMonth.Text = dataItem.Key.ToString("yyyy - MM");
                liTotalDealCount.Text = string.Format("(本月檔次數共 {0} 筆)", dataItem.Value.Sum(x => x.Sum(y => y.Value.Count(z => z.Key.BusinessHourGuid != Guid.Empty))));
                rptWeek.DataSource = dataItem.Value;
                rptWeek.DataBind();
            }
        }

        protected void rptWeek_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>)
            {
                Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>> dataItem = (Dictionary<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>)e.Item.DataItem;
                Repeater rptDate = (Repeater)e.Item.FindControl("rptDate");
                rptDate.DataSource = dataItem;// dataItem.Where(x => x.Key.DayOfWeek.EqualsNone(DayOfWeek.Saturday, DayOfWeek.Sunday));
                rptDate.DataBind();
            }
        }

        protected void rptDate_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>)
            {  
                KeyValuePair<DateTime, Dictionary<ViewPponDealCalendar, Proposal>> dataItem = (KeyValuePair<DateTime, Dictionary<ViewPponDealCalendar, Proposal>>)e.Item.DataItem;
                Literal liWeekName = (Literal)e.Item.FindControl("liWeekName");
                PlaceHolder divDate = (PlaceHolder)e.Item.FindControl("divDate");
                Label lblDate = (Label)e.Item.FindControl("lblDate");
                Repeater rptDeal = (Repeater)e.Item.FindControl("rptDeal");
                ImageButton btnHideContent = (ImageButton)e.Item.FindControl("btnHideContent");
                

                switch (dataItem.Key.DayOfWeek)
                {
                    case DayOfWeek.Friday:
                        liWeekName.Text = "星期五";
                        break;
                    case DayOfWeek.Monday:
                        liWeekName.Text = "星期一";
                        break;
                    case DayOfWeek.Saturday:
                        liWeekName.Text = "星期六";
                        break;
                    case DayOfWeek.Sunday:
                        liWeekName.Text = "星期日";
                        break;
                    case DayOfWeek.Thursday:
                        liWeekName.Text = "星期四";
                        break;
                    case DayOfWeek.Tuesday:
                        liWeekName.Text = "星期二";
                        break;
                    case DayOfWeek.Wednesday:
                        liWeekName.Text = "星期三";
                        break;
                    default:
                        break;
                }
                if (dataItem.Key.Date != DateTime.MinValue.Date)
                {
                    lblDate.Text = dataItem.Key.Day.ToString();
                    divDate.Visible = true;
                    btnHideContent.Visible = true;
                    if (dataItem.Key.Date == DateTime.Now.Date)
                    {
                        lblDate.ForeColor = System.Drawing.Color.Red;
                        lblDate.Font.Bold = true;
                        lblDate.Font.Underline = true;
                    }
                }
                else
                {
                    liWeekName.Text = string.Empty;
                }
                rptDeal.DataSource = dataItem.Value;
                rptDeal.DataBind();
            }
        }

        protected void rptDeal_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<ViewPponDealCalendar, Proposal>)
            {
                KeyValuePair<ViewPponDealCalendar, Proposal> dataItem = (KeyValuePair<ViewPponDealCalendar, Proposal>)e.Item.DataItem;
                HyperLink hkPid = (HyperLink)e.Item.FindControl("hkPid");
                HyperLink hkItemName = (HyperLink)e.Item.FindControl("hkItemName");
                Label lblSales = (Label)e.Item.FindControl("lblSales");
                Label lblPhotographer = (Label)e.Item.FindControl("lblPhotographer");
                Label lblPhotographerMailCount = (Label)e.Item.FindControl("lblPhotographerMailCount");
                Label lblPhotographerCheck = (Label)e.Item.FindControl("lblPhotographerCheck");
                ImageButton btnSendEmail = (ImageButton)e.Item.FindControl("btnSendEmail");
                ImageButton btnPhotographerCheck = (ImageButton)e.Item.FindControl("btnPhotographerCheck");
                Panel divPhotoGrapher = (Panel)e.Item.FindControl("divPhotoGrapher");

                if (dataItem.Key.BusinessHourGuid != Guid.Empty)
                {
                    hkPid.Text = "<font color='red'>(" + (e.Item.ItemIndex + 1).ToString() + ")</font>" + dataItem.Value.Id;
                    if (dataItem.Value.ProposalSourceType == (int)ProposalSourceType.Original)
                    {
                        hkPid.NavigateUrl = ResolveUrl("~/sal/ProposalContent.aspx?pid=" + dataItem.Value.Id); 
                    }
                    else
                    {
                        hkPid.NavigateUrl = config.SiteUrl + "/Sal/ProposalContent.aspx?pid=" + dataItem.Value.Id;
                    }
                    

                    hkItemName.Text = dataItem.Key.ItemName;
                    hkItemName.NavigateUrl = ResolveUrl("~/ppon/default.aspx?bid=" + dataItem.Key.BusinessHourGuid);

                    //業務
                    ProposalSalesModel model = ProposalFacade.ProposalSaleGetByPid(dataItem.Value.Id);
                    lblSales.Text = string.Format("<i class='fa fa-user fa-fw {0}'></i>{1}", (EmpUserId == dataItem.Key.DevelopeSalesId || EmpUserId == dataItem.Key.OperationSalesId) ? string.Empty : 
                                    "otherSales", model.DevelopeSalesEmpName + (string.IsNullOrEmpty(model.OperationSalesEmpName) ? "" : "/" + model.OperationSalesEmpName));


                    Proposal pro = dataItem.Value;

                    //攝影師
                    string photoString = "";
                    if (Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.Photographers))
                    {
                        Dictionary<ProposalPhotographer, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalPhotographer, string>>(pro.SpecialAppointFlagText);
                        if (specialText != null && specialText.Count > 0)
                        {
                            photoString = specialText[(ProposalPhotographer)ProposalPhotographer.PhotographerAppointCheck];
                            lblPhotographer.Text = string.Format("{0}", photoString.Split("|").Length > 3 ? MemberFacade.GetFullName(photoString.Split('|')[3]) : "");
                            divPhotoGrapher.Visible = true;
                            btnSendEmail.ImageUrl = ResolveUrl("~/Images/icons/sky_fly.png");
                            lblPhotographerMailCount.Text = ProposalFacade.GetProposalLogCount(pro.Id, "發信通知攝影師").ToString(); 

                        }
                    }
                    
                    //攝影狀態
                    lblPhotographerCheck.Visible = true;
                    btnPhotographerCheck.Visible = true;

                    lblPhotographerCheck.Text = "[";
                    if (Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.SellerPhoto))
                        lblPhotographerCheck.Text += "店/";
                    if (Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.FTPPhoto))
                        lblPhotographerCheck.Text += "舊/";

                    string PhotographerAppointFlag = "";
                    if (Helper.IsFlagSet(pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppointClose))
                        PhotographerAppointFlag = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalPhotographer.PhotographerAppointClose);
                    else if(Helper.IsFlagSet(pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppointCancel))
                        PhotographerAppointFlag = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalPhotographer.PhotographerAppointCancel);
                    else if (Helper.IsFlagSet(pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppointCheck))
                        PhotographerAppointFlag = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalPhotographer.PhotographerAppointCheck);
                    else if (Helper.IsFlagSet(pro.PhotographerAppointFlag, ProposalPhotographer.PhotographerAppoint))
                        PhotographerAppointFlag = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalPhotographer.PhotographerAppoint);




                    lblPhotographerCheck.Text += PhotographerAppointFlag;
                    lblPhotographerCheck.Text = lblPhotographerCheck.Text.TrimEnd('/') + "]";

                    
                    if (Helper.IsFlagSet(pro.BusinessFlag, (ProposalBusinessFlag)ProposalBusinessFlag.PhotographerCheck))
                    {
                        btnPhotographerCheck.ImageUrl = ResolveUrl("~/Themes/PCweb/images/Tick.png");
                    }
                    else
                    {
                        btnPhotographerCheck.ImageUrl = ResolveUrl("~/Themes/PCweb/images/x.png");
                    }
                }
            }
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            _Year = (int)DateTime.Now.Year;
            _Month = (int)DateTime.Now.Month;
            if (this.Search != null)
            {
                this.Search(this, e);
            }
        }

        protected void btnPrevious_Click(object sender, ImageClickEventArgs e)
        {
            Literal liMonth = (Literal)rptMonth.Items[0].FindControl("liMonth");
            _Year = Convert.ToInt32(liMonth.Text.Substring(0, 4));
            _Month = Convert.ToInt32(liMonth.Text.Substring(7, 2));

            if (_Month == 1)
            {
                _Year = _Year - 1;
                _Month = 12;
            }
            else
                _Month = _Month - 1;


            if (this.PreviousSearch != null)
            {
                this.PreviousSearch(this, e);
            }
        }

        protected void btnNext_Click(object sender, ImageClickEventArgs e)
        {
            Literal liMonth = (Literal)rptMonth.Items[0].FindControl("liMonth");
            _Year = Convert.ToInt32(liMonth.Text.Substring(0, 4));
            _Month = Convert.ToInt32(liMonth.Text.Substring(7, 2));

            if (_Month == 12)
            {
                _Year = _Year + 1;
                _Month = 1;
            }
            else
                _Month = _Month + 1;


            if (this.NextSearch != null)
            {
                this.NextSearch(this, e);
            }
        }


        #endregion

        #region private method
        private void IntialControls()
        {
            // 業務部門
            List<Department> Depts = HumanFacade.GetSalesDepartment();
            Depts.Insert(0, new Department
            {
                DeptName = "請選擇",
                DeptId = ""
            });
            ddlDept.DataSource = Depts;
            ddlDept.DataTextField = "DeptName";
            ddlDept.DataValueField = "DeptId";
            ddlDept.DataBind();

            // 提案類型
            Dictionary<int, string> dealType = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(ProposalDealType)))
            {
                if (item.ToString() == ProposalDealType.None.ToString())
                {
                    dealType[(int)item] = "請選擇";
                }
                else
                {
                    dealType[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDealType)item);
                }
            }
            ddlDealType1.DataSource = dealType;
            ddlDealType1.DataTextField = "Value";
            ddlDealType1.DataValueField = "Key";
            ddlDealType1.DataBind();


            rdblPhotoSource.Items.Add(new ListItem(Helper.GetLocalizedEnum(ProposalSpecialFlag.SellerPhoto),((int)ProposalSpecialFlag.SellerPhoto).ToString()));
            rdblPhotoSource.Items.Add(new ListItem(Helper.GetLocalizedEnum(ProposalSpecialFlag.FTPPhoto), ((int)ProposalSpecialFlag.FTPPhoto).ToString()));
            rdblPhotoSource.Items.Add(new ListItem(Helper.GetLocalizedEnum(ProposalSpecialFlag.Photographers), ((int)ProposalSpecialFlag.Photographers).ToString()));
            rdblPhotoSource.Items.Add(new ListItem("全部", "0"));
            rdblPhotoSource.SelectedValue = "0";

            if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReadAll))
                divSearch.Visible = true;

            //攝影師
            var empCollection = hp.ViewEmployeeCollectionGetAll().Where(x => !x.IsInvisible);

            Dictionary<string, string> photographerAppointer = new Dictionary<string, string>();
            List<string> mailToUser = FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.Photographer, "/sal/ProposalContent.aspx");

            foreach (var item in mailToUser)
            {
                if (mailToUser.IndexOf(item) == 0)
                {
                    photographerAppointer.Add(mailToUser.IndexOf(item).ToString(), "請選擇");
                    photographerAppointer.Add(mailToUser.IndexOf(item) + 1.ToString(), item);
                }
                else
                {
                    photographerAppointer.Add(mailToUser.IndexOf(item) + 1.ToString(), item);
                }
            }

            dllPhotographer.DataSource = photographerAppointer;
            dllPhotographer.DataTextField = "Value";
            dllPhotographer.DataValueField = "Value";
            dllPhotographer.DataBind();
        }

        #endregion

        #region WS
        [WebMethod]
        public static dynamic GetDealSubType(string id)
        {
            Dictionary<int, string> rtn = new Dictionary<int, string>();
            ProposalDealType type = ProposalDealType.TryParse(id, out type) ? type : ProposalDealType.None;
            if (type != ProposalDealType.None)
            {
                switch (type)
                {
                    case ProposalDealType.LocalDeal:
                        rtn.Add((int)ProposalDealSubType.DealSet, Helper.GetLocalizedEnum(ProposalDealSubType.DealSet));
                        rtn.Add((int)ProposalDealSubType.Offsetting, Helper.GetLocalizedEnum(ProposalDealSubType.Offsetting));
                        rtn.Add((int)ProposalDealSubType.Propoduct, Helper.GetLocalizedEnum(ProposalDealSubType.Propoduct));
                        rtn.Add((int)ProposalDealSubType.EatToDie, Helper.GetLocalizedEnum(ProposalDealSubType.EatToDie));
                        rtn.Add((int)ProposalDealSubType.Tickets, Helper.GetLocalizedEnum(ProposalDealSubType.Tickets));
                        rtn.Add((int)ProposalDealSubType.Others, Helper.GetLocalizedEnum(ProposalDealSubType.Others));
                        break;
                    case ProposalDealType.PBeauty:
                        rtn.Add((int)ProposalDealSubType.SPA, Helper.GetLocalizedEnum(ProposalDealSubType.SPA));
                        rtn.Add((int)ProposalDealSubType.HairSalon, Helper.GetLocalizedEnum(ProposalDealSubType.HairSalon));
                        rtn.Add((int)ProposalDealSubType.NailSalon, Helper.GetLocalizedEnum(ProposalDealSubType.NailSalon));
                        rtn.Add((int)ProposalDealSubType.Massage, Helper.GetLocalizedEnum(ProposalDealSubType.Massage));
                        rtn.Add((int)ProposalDealSubType.Others, Helper.GetLocalizedEnum(ProposalDealSubType.Others));
                        break;
                    case ProposalDealType.Piinlife:
                        rtn.Add((int)ProposalDealSubType.DealSet, Helper.GetLocalizedEnum(ProposalDealSubType.DealSet));
                        rtn.Add((int)ProposalDealSubType.Offsetting, Helper.GetLocalizedEnum(ProposalDealSubType.Offsetting));
                        rtn.Add((int)ProposalDealSubType.Propoduct, Helper.GetLocalizedEnum(ProposalDealSubType.Propoduct));
                        rtn.Add((int)ProposalDealSubType.EatToDie, Helper.GetLocalizedEnum(ProposalDealSubType.EatToDie));
                        rtn.Add((int)ProposalDealSubType.Course, Helper.GetLocalizedEnum(ProposalDealSubType.Course));
                        rtn.Add((int)ProposalDealSubType.SPA, Helper.GetLocalizedEnum(ProposalDealSubType.SPA));
                        rtn.Add((int)ProposalDealSubType.HairSalon, Helper.GetLocalizedEnum(ProposalDealSubType.HairSalon));
                        rtn.Add((int)ProposalDealSubType.NailSalon, Helper.GetLocalizedEnum(ProposalDealSubType.NailSalon));
                        rtn.Add((int)ProposalDealSubType.Massage, Helper.GetLocalizedEnum(ProposalDealSubType.Massage));
                        rtn.Add((int)ProposalDealSubType.Others, Helper.GetLocalizedEnum(ProposalDealSubType.Others));
                        break;
                    case ProposalDealType.Travel:
                        rtn.Add((int)ProposalDealSubType.Hotel, Helper.GetLocalizedEnum(ProposalDealSubType.Hotel));
                        rtn.Add((int)ProposalDealSubType.BedAndBreakfast, Helper.GetLocalizedEnum(ProposalDealSubType.BedAndBreakfast));
                        rtn.Add((int)ProposalDealSubType.HotelRest, Helper.GetLocalizedEnum(ProposalDealSubType.HotelRest));
                        rtn.Add((int)ProposalDealSubType.Motel, Helper.GetLocalizedEnum(ProposalDealSubType.Motel));
                        rtn.Add((int)ProposalDealSubType.Travel, Helper.GetLocalizedEnum(ProposalDealSubType.Travel));
                        rtn.Add((int)ProposalDealSubType.Tickets, Helper.GetLocalizedEnum(ProposalDealSubType.Tickets));
                        rtn.Add((int)ProposalDealSubType.HotSpring, Helper.GetLocalizedEnum(ProposalDealSubType.HotSpring));
                        rtn.Add((int)ProposalDealSubType.Others, Helper.GetLocalizedEnum(ProposalDealSubType.Others));
                        break;
                    case ProposalDealType.Product:
                        rtn.Add((int)ProposalDealSubType.Food, Helper.GetLocalizedEnum(ProposalDealSubType.Food));
                        rtn.Add((int)ProposalDealSubType.Living, Helper.GetLocalizedEnum(ProposalDealSubType.Living));
                        rtn.Add((int)ProposalDealSubType.Furniture, Helper.GetLocalizedEnum(ProposalDealSubType.Furniture));
                        rtn.Add((int)ProposalDealSubType.Popular, Helper.GetLocalizedEnum(ProposalDealSubType.Popular));
                        rtn.Add((int)ProposalDealSubType.ConsumerElectronic, Helper.GetLocalizedEnum(ProposalDealSubType.ConsumerElectronic));
                        rtn.Add((int)ProposalDealSubType.HomeAppliances, Helper.GetLocalizedEnum(ProposalDealSubType.HomeAppliances));
                        rtn.Add((int)ProposalDealSubType.Beauty, Helper.GetLocalizedEnum(ProposalDealSubType.Beauty));
                        rtn.Add((int)ProposalDealSubType.Child, Helper.GetLocalizedEnum(ProposalDealSubType.Child));
                        break;
                    case ProposalDealType.None:
                    default:
                        break;
                }
            }
            return rtn.ToArray();
        }

        /// <summary>
        /// 小飛機發送mail給攝影師並記錄log與次數
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        [WebMethod]
        public static bool SendEmailToPhotographers (string pid, string userName)
        {
            Proposal pro = ProposalFacade.ProposalGet(Convert.ToInt32(pid));
            Dictionary<ProposalPhotographer, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalPhotographer, string>>(pro.SpecialAppointFlagText);
            string photoString = "";
            bool falg = false;

            //get攝影師mail
            if (Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.Photographers))
            {
                if (specialText != null && specialText.Count > 0)
                {
                    photoString = specialText[(ProposalPhotographer)ProposalPhotographer.PhotographerAppointCheck];

                    if(ProposalFacade.MailToPhotographer(userName, photoString.Split('|')[3], Convert.ToInt32(pid)))
                    {
                        ProposalFacade.ProposalLog(pro.Id, "發信通知攝影師", userName);
                        falg = true;
                    }
                }
            }

            return falg;
        }

        [WebMethod]
        public static bool UpdateFlag(string pid, string userName)
        {
            #region 更新flag
            int ProposalID = Convert.ToInt32(pid);
            Proposal pro = ProposalFacade.ProposalGet(ProposalID);
            int value = (int)ProposalBusinessFlag.PhotographerCheck;
            bool hasFlag;
            if (Helper.IsFlagSet(pro.BusinessFlag, (ProposalBusinessFlag)ProposalBusinessFlag.PhotographerCheck))
            {
                hasFlag = true;
            }
            else
            {
                hasFlag = false;
            }

            string col = Proposal.Columns.BusinessFlag;

            ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
            Seller s = SellerFacade.SellerGet(pro.SellerGuid);


            if (hasFlag)
            {
                pro.SetColumnValue(col, Convert.ToInt32(pro.GetColumnValue(col)) ^ value);
            }
            else
            {
                pro.SetColumnValue(col, Convert.ToInt32(pro.GetColumnValue(col)) | value);
            }
            pro.Status = SellerFacade.GetFlagForProposalFlagCheck(pro);
            pro.ModifyId = userName;
            pro.ModifyTime = DateTime.Now;
            ProposalFacade.ProposalSet(pro); 
            #endregion

            #region 若為通過攝影確認，則寄送通知信予具指派權限人員

            if (Helper.IsFlagSet(pro.BusinessFlag, ProposalBusinessFlag.PhotographerCheck))
            {
                List<string> mailToUser = FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.Assign, "/sal/ProposalContent.aspx");
                string subject = string.Format("【{0}】 單號 NO.{1}", Helper.GetLocalizedEnum(ProposalBusinessFlag.PhotographerCheck), pro.Id);
                string url = string.Format("{0}/sal/ProposalContent.aspx?pid={1}", config.SiteUrl, pro.Id);
                ProposalFacade.SendEmail(mailToUser, subject, ProposalFacade.GetProposalBodyHtml(pro.Id, s.SellerName, emp.EmpName, url));
                
            }

            ProposalFacade.ProposalLog(pro.Id, (hasFlag ? "取消-" : string.Empty) + Helper.GetLocalizedEnum(ProposalBusinessFlag.PhotographerCheck), userName);

            #endregion

            return !hasFlag;
        }
        #endregion

    }
}
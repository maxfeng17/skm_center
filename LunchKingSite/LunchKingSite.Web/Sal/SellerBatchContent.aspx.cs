﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.Sal
{
    /// <summary>
    /// 商家批次處理
    /// </summary>
    public partial class SellerBatchContent : RolePage, ISalesSellerBatchContentView
    {
        /// <summary>
        /// System Config
        /// </summary>
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        /// <summary>
        /// 商家批次處理
        /// </summary>
        private SalesSellerBatchContentPresenter _presenter;

        /// <summary>
        /// 商家批次處理
        /// </summary>
        public SalesSellerBatchContentPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        #region Interface Propertys
        /// <summary>
        /// 上傳檔案控制項
        /// </summary>
        public FileUpload FUExport
        {
            get
            {
                return FUImport;
            }
        }

        /// <summary>
        /// 使用者名稱
        /// </summary>
        public string UserName
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }

        /// <summary>
        /// 錯誤清單資料
        /// </summary>
        public List<SellerBatchErrorData> ErrData
        {
            get
            {
                return (ViewState != null && ViewState["errData"] != null) ? (List<SellerBatchErrorData>)ViewState["errData"] : null;
            }
            set
            {
                if (ViewState == null)
                {
                    ViewState.Add("errData", value);
                }
                else
                {
                    ViewState["errData"] = value;
                }
            }
        }

        /// <summary>
        /// 頁次
        /// </summary>
        public int PageSize
        {
            get
            {
                return ucPager.PageSize;
            }

            set
            {
                ucPager.PageSize = value;
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// 取得總筆數
        /// </summary>
        public event EventHandler<DataEventArgs<int>> OnGetCount;
        /// <summary>
        /// 頁數變更
        /// </summary>
        public event EventHandler<DataEventArgs<int>> PageChanged;
        /// <summary>
        /// 匯入處理
        /// </summary>
        public event EventHandler Upload;
        #endregion

        #region Control Events
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        /// <summary>
        /// 發生於上傳按鈕按下時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (this.Upload != null)
            { 
                this.Upload(this, e);
            }
            ucPager.ResolvePagerView(ucPager.CurrentPage, true);
        }
         
        /// <summary>
        /// 發生於分頁控制器更新時
        /// </summary>
        /// <param name="pageNumber">頁數</param>
        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChanged != null)
            {
                this.PageChanged(this, new DataEventArgs<int>(pageNumber));
            }
        }

        /// <summary>
        /// 取得總筆數
        /// </summary>
        /// <returns></returns>
        protected int GetCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetCount != null)
            {
                OnGetCount(this, e);
            }
            return e.Data;
        }
        #endregion

        #region Custom Events
        /// <summary>
        /// 設定錯誤列表
        /// </summary>
        /// <param name="errData"></param>
        public void SetErrorList(List<SellerBatchErrorData> errData)
        {
            if (errData == null || errData.Count() == 0)
            {
                divErrorList.Visible = false;
            }
            else
            {
                divErrorList.Visible = true;
                rptErrorList.DataSource = errData;
                rptErrorList.DataBind();
            } 
        }

        /// <summary>
        /// 顯示訊息
        /// </summary>
        /// <param name="msg">訊息</param>
        public void ShowMessage(string msg)
        {
            string script = string.Format("alert('{0}'); location.href='{1}/sal/SellerBatchContent.aspx'", msg, config.SiteUrl);
            Page.ClientScript.RegisterStartupScript(GetType(), "alert", script, true);
        }
        #endregion
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="BusinessList.aspx.cs" Inherits="LunchKingSite.Web.Sal.BusinessList" %>

<%@ Register Src="../User/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Src="~/Sal/ProposalMenu.ascx" TagName="ProposalMenu" TagPrefix="uc2" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css") %>" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" src="../Tools/js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtOrderTimeS').datepicker({ dateFormat: 'yy/mm/dd' });
            $('#txtOrderTimeE').datepicker({ dateFormat: 'yy/mm/dd' });
            SalesEmailAutoComplete();
        });
        function confirmTransferDeal() {
            if (!confirm("確定要整批移轉檔次跟業績?")) {
                return false;
            }

            $.blockUI({
                message: "匯入中請稍後...",
                css: {
                    width: '20%',
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });

            return true;
        }
        <%--[負責業務]提示輸入選單--%>
        function SalesEmailAutoComplete() {
            $('#<%=txtSalesEmail.ClientID%>').autocomplete({
                source: []
            });

            $('#<%=txtSalesEmail.ClientID%>').on('input', function () {
                var theInput = $(this);
                if (theInput.val().length > 0) {
                    $.ajax({
                        url: "SellerList.aspx/GetSalesEmailAutoCompelte",
                        method: "POST",//1.9.0
                        contentType: "application/json",
                        data: JSON.stringify({
                            partEmail: $(this).val()
                        }),
                        success: function (data) {
                            theInput.autocomplete("option", "source", data.d);
                        }
                    });
                }
            });
        }

        //檢查負責業務Email
        function SalesEmailExist(obj) {
            $.ajax({
                url: "ProposalList.aspx/SalesEmailExist",
                method: "POST",//1.9.0
                contentType: "application/json",
                data: JSON.stringify({
                    email: $(obj).val()
                }),
                success: function (response) {
                    if (!response.d)
                        alert("請輸入正確的負責業務Email！");
                }
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <style>
        .mc-navbar .mc-navbtn{
            width:10%;
        }
    </style>
    <uc2:ProposalMenu ID="ProposalMenu" runat="server" ></uc2:ProposalMenu>
    <div class="mc-content">
        <h1 class="rd-smll">檔次列表</h1>
        <hr class="header_hr">
        <div class="grui-form">
            <div class="form-unit">
                <label class="unit-label rd-unit-label">
                    專案內容</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox ID="txtItemName" runat="server" CssClass="input-half" Style="width: 200px"></asp:TextBox>
                    <p>
                        <label class="unit-label rd-unit-label">
                            Bid</label>
                        <asp:TextBox ID="txtBid" runat="server" CssClass="input-half" Style="width: 200px"></asp:TextBox>
                    </p>
                </div>
            </div>
            <div class="form-unit">
                <label class="unit-label rd-unit-label">
                    商家名稱</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox ID="txtSellerName" runat="server" CssClass="input-half" Style="width: 200px"></asp:TextBox>
                    <p>
                        <label class="unit-label rd-unit-label">
                            負責業務</label>
                        <asp:TextBox ID="txtSalesEmail" runat="server" CssClass="input-half" Style="width:200px;margin-right:0;" placeholder="請輸入Email" onchange="SalesEmailExist(this)"></asp:TextBox>
                    </p>
                </div>
                
            </div>
            <div class="form-unit">
                <label class="unit-label rd-unit-label">
                    上檔日期</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox ID="txtOrderTimeS" ClientIDMode="Static" runat="server" Columns="8" />
                    ～
                    <asp:TextBox ID="txtOrderTimeE" ClientIDMode="Static" runat="server" Columns="8" />
                    <asp:CheckBox ID="chkIsOrderTimeSet" runat="server" Text="僅顯示已排檔" />
                </div>
            </div>
            <div class="form-unit end-unit">
                <div class="data-input rd-data-input">
                    <asp:Button ID="btnSearch" runat="server" Text="搜尋" CssClass="btn rd-mcacstbtn" OnClick="btnSearch_Click" />
                </div>
            </div>
        </div>
        <asp:PlaceHolder ID="divTransferDeal" runat="server" Visible="false">
            <div class="data-input rd-data-input">
                <label class="unit-label rd-unit-label">檔次批次轉換業務功能</label>
                <asp:Button ID="btnExport" CssClass="btn rd-mcacstbtn" runat="server" Text="匯出" OnClick="btnExport_Click" ClientIDMode="Static" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:FileUpload ID="FUImport" runat="server" />
                <asp:Button ID="btnImport" CssClass="btn rd-mcacstbtn" runat="server" Text="匯入" OnClick="btnImport_Click" OnClientClick="return confirmTransferDeal();" />
            </div>
        </asp:PlaceHolder>
        <asp:Panel ID="divBusinessList" runat="server" Visible="false">
            <div id="mc-table">
                <asp:Repeater ID="rptBusiness" runat="server" OnItemDataBound="rptBusiness_ItemDataBound">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                            <tr class="rd-C-Hide">
                                <th class="OrderDate">單號
                                </th>
                                <th class="OrderSerial">商家名稱
                                </th>
                                <th class="OrderName">專案內容
                                </th>
                                <th class="OrderName">負責業務
                                </th>
                                <th class="OrderDate">好康期間
                                </th>
                                <th class="OrderDate">館別
                                </th>
                                <th class="OrderDate">狀態
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="mc-tableContentITEM">
                            <td class="OrderDate" style="width: 40px">
                                <span class="rd-Detailtitle">單號：</span>
                                <asp:HyperLink ID="hkProposalId" runat="server" Target="_blank"></asp:HyperLink>
                            </td>
                            <td class="OrderSerial" style="text-align:left">
                                <asp:HyperLink ID="hkSellerName" runat="server" Target="_blank"></asp:HyperLink>
                            </td>
                            <td class="OrderName" style="width: 300px;text-align:left">
                                <asp:Label ID="lblMultiDeal" runat="server"></asp:Label>
                                <asp:HyperLink ID="hkItemName" runat="server" Target="_blank"></asp:HyperLink>
                            </td>
                            <td class="OrderName" style="text-align:left">
                                <asp:Literal ID="liSalesName" runat="server"></asp:Literal>
                            </td>
                            <td class="OrderDate">
                                <span class="rd-Detailtitle">好康期間：</span>
                                <asp:Literal ID="liOrderTime" runat="server"></asp:Literal>
                            </td>
                            <td class="OrderDate" style="text-align:left">
                                <span class="rd-Detailtitle">館別：</span>
                                <asp:Literal ID="liDealCatgegories" runat="server"></asp:Literal>
                            </td>
                            <td class="OrderDate" style="width: 60px">
                                <span class="rd-Detailtitle">狀態：</span>
                                <asp:Label ID="lblStatus" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <uc1:Pager ID="ucPager" runat="server" PageSize="10" ongetcount="GetCount" onupdate="UpdateHandler"></uc1:Pager>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="PhotographerCalender.aspx.cs" Inherits="LunchKingSite.Web.Sal.PhotographerCalender" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="LunchKingSite.Core" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css") %>" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

        $(document).ready(function () {

            BindDealSubType($('#ddlDealType1'));

        });

        function sendEmail(obj)
        {
            var pid = $(obj).parent().parent().find('[id*=hkPid]').text();
            pid = pid.substring(pid.indexOf(")") + 1, pid.length);
            
            $.ajax({
                type: "POST",
                url: "PhotographerCalender.aspx/SendEmailToPhotographers",
                data: "{'pid': '" + pid + "','userName': '<%=UserName%>'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if(response.d==true)
                    {
                        alert("發送成功!");
                        document.getElementById($(obj.parentElement).children(':eq(2)').attr("id")).innerText = parseInt($(obj.parentElement).children(':eq(2)').text()) + 1;
                    }
                    else
                    {
                        alert("發送失敗!");
                    }
                }, error: function (res) {
                    console.log(res);
                }
            });
        }

        function UpdtaeFlag(obj)
        {
            var pid = $(obj).parent().parent().find('[id*=hkPid]').text();
            pid = pid.substring(pid.indexOf(")") + 1, pid.length);
            
            $.ajax({
                type: "POST",
                url: "PhotographerCalender.aspx/UpdateFlag",
                data: "{'pid': '" + pid + "','userName': '<%=UserName%>'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if(response.d==true)
                    {
                        $(obj).attr("src", "../Themes/PCweb/images/Tick.png");
                    }
                    else
                    {
                        $(obj).attr("src", "../Themes/PCweb/images/x.png");
                    }
                }, error: function (res) {
                    console.log(res);
                }
            });
        }

        function BindDealSubType(obj) {
            $.ajax({
                type: "POST",
                url: "ProposalContent.aspx/GetDealSubType",
                data: "{'id': '" + $(obj).val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var subType = $('#ddlDealType2');
                    $(subType).find('option').remove();
                    $(subType).append($('<option></option>').attr('value', "").text("請選擇"));
                    $.each(response.d, function (index, item) {
                        $(subType).append($('<option></option>').attr('value', item.Key).text(item.Value));
                    });

                    $('#hidDealType').val($(obj).val());


                    if ($('#hidDealSubType').val() != '0') {
                        $(subType).val($('#hidDealSubType').val());
                        $('#hidDealSubType').val("");
                    } else {
                        SetDealSubType(subType);
                    }
                }
            });
        }

        function SetDealSubType(obj) {
            $('#hidDealSubType').val($(obj).val());
        }

        function HideContent(obj)
        {
            var a = $(obj).parent().parent().children().slice(3);
            if (a.is(':visible') == true)
            {
                a.css('display', 'none');
                $(obj).attr('src', '../Themes/default/images/17Life/G2/ArrowUp.png');
            }
                
            else
            {
                a.css('display', 'block');
                $(obj).attr('src', '../Themes/default/images/17Life/G2/ArrowDown.png');
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <div class="mc-content">
        <h1 class="rd-smll">攝影檔期表</h1>
        <hr class="header_hr">
        <asp:Panel ID="divSearch" runat="server" DefaultButton="btnSearch" Visible="false">
            <div class="grui-form">
                <div class="form-unit">
                    <div class="data-input rd-data-input">
                        <asp:DropDownList ID="ddlDept" runat="server" Width="200px" >
                            <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-unit">
                    <div class="data-input rd-data-input">
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlDealType1" runat="server" Width="100px" ClientIDMode="Static" onchange="BindDealSubType(this);"></asp:DropDownList>
                                    <asp:DropDownList ID="ddlDealType2" runat="server" Width="100px" ClientIDMode="Static" onchange="SetDealSubType(this);"></asp:DropDownList>
                                    <asp:HiddenField ID="hidDealType" runat="server" ClientIDMode="Static" />
                                    <asp:HiddenField ID="hidDealSubType" runat="server" ClientIDMode="Static" />
                                </td>
                                <td style="float:right">
                                    <asp:RadioButtonList ID="rdblPhotoSource" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                        
                        
                        
                        
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        業務</label>
                    <div class="data-input rd-data-input">
                        <asp:TextBox ID="txtSalesName" runat="server" CssClass="input-half"></asp:TextBox>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label rd-unit-label">
                        攝影師</label>
                    <div class="data-input rd-data-input">
                        <asp:DropDownList ID="dllPhotographer" runat="server" CssClass="input-half"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-unit end-unit">
                    <div class="data-input rd-data-input">
                        <asp:Button ID="btnSearch" runat="server" Text="搜尋" CssClass="btn rd-mcacstbtn" OnClick="btnSearch_Click" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        
        <asp:Repeater ID="rptMonth" runat="server" OnItemDataBound="rptMonth_ItemDataBound">
            <ItemTemplate>
                <h1 class="rd-smll">
                    <asp:Literal ID="liMonth" runat="server"></asp:Literal>
                    <span style="font-size: small; color: #BF0000">
                        <asp:Literal ID="liTotalDealCount" runat="server"></asp:Literal>
                    </span>
                    <span style="font-size: small; color: #BF0000;float:right";>
                        <asp:ImageButton ID="btnPrevious" runat="server" ImageUrl="~/Themes/default/images/17Life/ipeenX17/arrow-bg-left-hover.png" OnClick="btnPrevious_Click" />
                        <asp:ImageButton ID="btnNext" runat="server" ImageUrl="~/Themes/default/images/17Life/ipeenX17/arrow-bg-right-hover.png" OnClick="btnNext_Click" />
                    </span>
                </h1>
                <hr class="header_hr">
                <div id="mc-table">
                    <asp:Repeater ID="rptWeek" runat="server" OnItemDataBound="rptWeek_ItemDataBound">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                                <tr class="rd-C-Hide">
                                    <th class="OrderName" style="width:130px">星期日
                                    </th>
                                    <th class="OrderName" style="width:130px">星期一
                                    </th>
                                    <th class="OrderName" style="width:130px">星期二
                                    </th>
                                    <th class="OrderName" style="width:130px">星期三
                                    </th>
                                    <th class="OrderName" style="width:130px">星期四
                                    </th>
                                    <th class="OrderName" style="width:130px">星期五
                                    </th>
                                    <th class="OrderName" style="width:130px">星期六
                                    </th>

                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="mc-tableContentITEM">
                                <asp:Repeater ID="rptDate" runat="server" OnItemDataBound="rptDate_ItemDataBound">
                                    <ItemTemplate>
                                        <td class="OrderName" style="vertical-align: top; text-align: left;word-break: break-all;max-width:350px">
                                            <asp:PlaceHolder ID="divDate" runat="server" Visible="false">
                                                <p style="border-style: solid; border-radius: 10px; width: 24px; height: 24px; background-color: #FFCCBF; border-width: 0px; text-align: center; color: #666666;">
                                                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                                                </p>
                                            </asp:PlaceHolder>
                                            <p style="float: right">
                                                <asp:ImageButton ID="btnHideContent" runat="server" ImageUrl="~/Themes/default/images/17Life/G2/ArrowDown.png" OnClientClick="HideContent(this); return false;" Visible="false" />
                                            </p>
                                            
                                            <span class="rd-Detailtitle">
                                                <asp:Literal ID="liWeekName" runat="server"></asp:Literal></span><p></p>
                                            <asp:Repeater ID="rptDeal" runat="server" OnItemDataBound="rptDeal_ItemDataBound">
                                                <ItemTemplate>
                                                    <span class="deal_item">
                                                        
                                                        <asp:HyperLink ID="hkPid" runat="server" Target="_blank"></asp:HyperLink><br />
                                                        <asp:HyperLink ID="hkItemName" runat="server" Target="_blank"></asp:HyperLink><br />
                                                        <asp:Label ID="lblSales" runat="server" ForeColor="#666666"></asp:Label><br />
                                                        <asp:Panel ID="divPhotoGrapher" runat="server" Visible="false">
                                                            <asp:Label ID="lblPhotographer" runat="server" ForeColor="#666666"></asp:Label>
                                                            <asp:ImageButton ID="btnSendEmail" runat="server" Visible="true" ClientIDMode="Static" OnClientClick="sendEmail(this);"/>
                                                            <asp:Label ID="lblPhotographerMailCount" runat="server" ForeColor="red" Font-Bold="true"></asp:Label><br />
                                                        </asp:Panel>
                                                        <asp:Label ID="lblPhotographerCheck" runat="server" Text ="[攝影狀態]" ForeColor="#666666" Visible="false"></asp:Label>
                                                        <p style="float: right;"><asp:ImageButton ID="btnPhotographerCheck" runat="server" Visible="false" ClientIDMode="Static" OnClientClick="UpdtaeFlag(this); return false;" /></p>
                                                        
                                                        
                                                    </span>
                                                    <br />
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            
                                        </td>
                                      
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                             </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <br />
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

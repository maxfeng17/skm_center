﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="MembershipCardQuery.aspx.cs" Inherits="LunchKingSite.Web.Sal.WebForm1" %>

<%@ Register Src="~/Sal/ProposalMenu.ascx" TagName="ProposalMenu" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <style>
        table.accountTable {
            border-collapse: collapse;
        }

            table.accountTable th {
                border-style: solid;
                border-width: thin;
                border-color: #333333;
                background-color: #999999;
                padding-top: 5px;
                padding-bottom: 5px;
                font-weight: lighter;
            }

            table.accountTable td {
                border-style: solid;
                border-width: thin;
                border-color: #333333;
                padding-top: 8px;
                padding-bottom: 8px;
                font-weight: lighter;
            }

        hr {
            border-style: dashed;
            border-width: thin;
            border-left-style: none;
            border-right-style: none;
            border-bottom-style: none;
            margin-right: 10px;
        }

        div.pager {
            width: 100%;
            margin: 10px 0;
            position: relative;
            height: 1.8em;
        }

            div.pager ul li,
            div.pager span {
                display: inline-block;
                float: left;
                width: 1.8em;
                height: 1.8em;
                line-height: 1.8;
                text-align: center;
                cursor: pointer;
                background: #000;
                color: #fff;
                margin-right: 0.5em;
            }

                div.pager ul li.active {
                    background: #c00;
                }

        .toForward {
            position: absolute;
            left: 0;
        }

        .num-list {
            position: absolute;
            left: 80px;
        }

            .num-list ul {
                margin: 0;
                padding: 0;
            }

        .toBack {
            position: absolute;
        }
    </style>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <style>
        .mc-navbar .mc-navbtn{
            width:10%;
        }
    </style>
    <uc2:ProposalMenu ID="ProposalMenu" runat="server" ></uc2:ProposalMenu>
    <div class="mc-content">
        <h1 class="rd-smll">熟客卡管理</h1>
        <hr class="header_hr">
        <div class="grui-form">
            <div class="form-unit">
                請輸入欲查詢賣家名稱：<input type="text" id="QueryNameText" />&nbsp&nbsp&nbsp
                <button id="QuerySeller" type="button">查詢</button>
                <br /><br />
                <div id="isMulti" style="display: none"></div>  
            </div>
            <div class="form-unit end-unit">
                賣家名稱：<span id="SellerName"></span><br />
                熟客卡優惠內容
                <div id="CardDetails" style="display: none"></div>
                <br />
                <br />
                熟客卡 QR Code <button id="dlQrCode" type="button">Download</button>
            </div>
        </div>

        <input type="hidden" id="groupId" />
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">

    <script>
        var ApiResultCode = { 'Success': 0 };

        $('#dlQrCode').on('click', function () {
            window.open('/webservice/pcpservice.asmx/GetMembershipCardQrCode?GroupId=' + $('#groupId').val()
                + '&SellerName=' + $('#QueryNameText').val()
                , '_blank');
        });

        $('#QuerySeller').on('click', function () {

            var account = $('#QueryNameText').val();

            if (account === "") { return; }


            if ($('.accountSelector').length > 0) {
                var clear = true;
                $.each($('.accountSelector'), function () {
                    if (account == $(this).val()) {
                        clear = false;
                        return;
                    }
                });

                if (clear) {
                    $('#isMulti').attr('style', 'display:none');
                    $('#isMulti').empty();
                    $('#CardDetails').attr('style', 'display:none');
                    $('#CardDetails').empty();
                }
            }
            if ($('#subContentSection').length > 0) { $('#subContentSection').attr('style', 'display:none'); }

            $.ajax({
                url: '/webservice/pcpservice.asmx/MembershipCardGroupQuery',
                type: "POST",
                data: { querySellerName: account },
                dataType: "json",
                success: function (result, status, xhr) {
                    if (xhr.status == 200 && result.Code == ApiResultCode.Success) {
                        var count = result.Data.length;
                        if (count === 1) {
                            if (result.Data[0].GroupId) {
                                QueryMembershipCard(result.Data[0].GroupId);
                                $('#groupId').val(result.Data[0].GroupId);
                                var content = result.Data[0].SellerName;
                                $('#QueryNameText').val(content);
                                content += '(' + result.Data[0].GroupId + ')';
                                $('#SellerName').text(content);
                            } else {
                                alert(result.Message);
                            }
                        }
                        else if (count > 1) {
                            //$('#searchTxt').val('');
                            gMultiVendorTbl(result.Data);
                        }
                    } else {
                        alert(result.Message);
                    }
                },
                error: function () {
                }
            });
        });

        $('#QueryNameText').keypress(function (evt) {
            if (evt.which == 13) {
                $("#QuerySeller").trigger('click');
            }
        });

        $("#isMulti").on("change", ".accountSelector", function () {
            var _this = $(this);
            if (_this.val()) {
                var content = _this.closest('tr').children()[1].textContent;
                $('#QueryNameText').val(content);
                content += '(' + _this.val() + ')';                
                $('#SellerName').text(content);
                $('#CardDetails').attr('style', 'display:none');
                $('#CardDetails').empty();
                $('#groupId').val(_this.val());
                QueryMembershipCard(_this.val());
            }
        });

        //$.ajax({
        //    url: '/webservice/pcpservice.asmx/GetMembershipCardQrCode?GroupId=' + 160,
        //    type: "POST",
        //    //data: data,
        //    contentType: "text/plain",
        //    //contentType: "image/png",
        //    success: function (result, status, xhr) {
        //        $('#QRcode').attr('src', result);
        //    },
        //    error: function (e) {
        //        //alert('error，可能是HTTPS安全性問題');
        //        console.log(e);
        //    }
        //});

        var QueryMembershipCard = function (groupId) {
            var vbsName = $('#QueryNameText').val();
            $.ajax({
                url: '/webservice/pcpservice.asmx/MembershipCardGroupBySales',
                type: "POST",
                data: { groupId: groupId, vbsName: vbsName, userId: '2431701417' },
                dataType: "json",
                success: function (result, status, xhr) {
                    if (xhr.status == 200) {
                        if (result.Code == ApiResultCode.Success) {
                            ShowCardsDetail(result.Data.Cards);
                        } else {
                            alert(result.Message);
                        }
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

        function ShowCardsDetail(d) {

            $('#CardDetails').attr('style', '');
            var tbl = createTableHeader('CardsTbl', 'accountTable', { colName: ['卡別', '優惠內容'], width: ['80px','550px'] });

            var tblBody = document.createElement("tbody");

            var CardLvl = ['','普卡','金卡','白金卡','鈦金卡'];

            for (var i = 0; i < d.length; i++) {
                var row = document.createElement("tr");
                
                var cell2 = document.createElement("td");
                cell2.style.textAlign = 'center';
                var cellText2 = document.createTextNode(CardLvl[d[i].Level]);
                cell2.appendChild(cellText2);
                row.appendChild(cell2);

                var cell3 = document.createElement("td");
                cell3.appendChild(document.createTextNode('會員享' + d[i].PaymentPercent + '折'));
                cell3.appendChild(document.createElement("br"));
                cell3.appendChild(document.createTextNode('額外加贈：' + d[i].OtherPremiums));
                cell3.appendChild(document.createElement("br"));
                cell3.appendChild(document.createTextNode('注意事項：' + d[i].Instruction));
                cell3.appendChild(document.createElement("br"));
                cell3.appendChild(document.createTextNode('使用限制：'));
                //cell3.appendChild(cellText3);
                row.appendChild(cell3);

                tblBody.appendChild(row);
            }
            tbl.appendChild(tblBody);

            document.getElementById('CardDetails').appendChild(tbl);
        }

        function gMultiVendorTbl(d) {

            $('#isMulti').attr('style', '');
            var tbl = createTableHeader('multiTbl', 'accountTable', { colName: ['選擇', '賣家名稱', '熟客卡狀態'], width: [] });

            var tblBody = document.createElement("tbody");

            for (var i = 0; i < d.length; i++) {
                var row = document.createElement("tr");

                var cell = document.createElement("td");

                var r = document.createElement("INPUT");
                r.setAttribute("type", "radio");
                r.className = "accountSelector";
                r.name = "SelectedAccountId";
                r.value = d[i].GroupId;

                cell.appendChild(r);
                row.appendChild(cell);

                var cell2 = document.createElement("td");
                var cellText2 = document.createTextNode(d[i].SellerName);
                cell2.appendChild(cellText2);
                row.appendChild(cell2);

                var cell3 = document.createElement("td");
                var cellText3 = document.createTextNode(d[i].MembershipCardLevels);
                cell3.appendChild(cellText3);
                row.appendChild(cell3);

                tblBody.appendChild(row);
            }
            tbl.appendChild(tblBody);

            document.getElementById('isMulti').appendChild(tbl);
            var pager = document.createElement('div');
            pager.id = "pager";
            pager.className = "pager";
            document.getElementById('isMulti').appendChild(pager);

            createPagination('multiTbl', 'pager', 10);
        }

        function createTableHeader(idName, className, columns) {
            var tbl = document.createElement("table");
            tbl.id = idName;
            tbl.className = className;
            var tHead = tbl.createTHead();
            var row = tHead.insertRow(-1);
            for (var i = 0; i < columns.colName.length; i++) {
                if (columns.width[i] === undefined) { row.innerHTML += "<th>" + columns.colName[i] + "</th>"; }
                else { row.innerHTML += "<th width='" + columns.width[i] + "'>" + columns.colName[i] + "</th>"; }
            }
            return tbl;
        }

        function createPagination(tbl, pagerId, numPerPage, currentPage) {

            var numPerPage = numPerPage || 15;
            var currentPage = currentPage || 0;
            var showCount = 7, notToCenter = (showCount / 2) | 0;
            var $table = $('#' + tbl);
            $table.bind('repaginate', function () {
                $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
            });
            $table.trigger('repaginate');
            var numRows = $table.find('tbody tr').length;
            var numPages = Math.ceil(numRows / numPerPage);

            if (numPages <= 1) { return; }

            /*to forward*/
            var toForward = document.createElement("div");
            toForward.className = 'toForward';

            var moveFirst = document.createElement("span");
            moveFirst.id = 'moveFirst';
            moveFirst.textContent = '<<';
            var moveForward = document.createElement("span");
            moveForward.id = 'moveForward';
            moveForward.textContent = '<';

            toForward.appendChild(moveFirst);
            toForward.appendChild(moveForward);

            /*number page*/
            var numList = document.createElement("div");
            numList.className = 'num-list';
            numList.style.overflow = 'hidden';
            numList.style.width = (showCount * 40) + 'px';
            var docfrag = document.createDocumentFragment();
            var ul = document.createElement("ul");
            ul.style.width = (numPages + 1) * 40 + 'px';
            for (var page = 0; page < numPages;) {
                var li = document.createElement("li");
                li.textContent = ++page;
                li.className = 'page-nubmers';
                docfrag.appendChild(li);
            }
            ul.appendChild(docfrag);
            numList.appendChild(ul);

            /*to back*/

            var toBack = document.createElement("div");
            toBack.className = 'toBack';
            toBack.style.cssText = 'left:' + ((numPages >= showCount) ? "365px;" : (numPages * 40 + 83) + "px");

            var moveLast = document.createElement("span");
            moveLast.id = 'moveLast';
            moveLast.textContent = '>>';
            var moveBack = document.createElement("span");
            moveBack.id = 'moveBack';
            moveBack.textContent = '>';

            toBack.appendChild(moveBack);
            toBack.appendChild(moveLast);

            var contain = document.getElementById(pagerId);
            contain.appendChild(toForward);
            contain.appendChild(numList);
            contain.appendChild(toBack);


            $('div.num-list ul li:first').addClass('active');

            var moveBar = document.getElementsByClassName('num-list')[0];

            $('div.num-list ul li').on('click', function (event) {
                currentPage = ($(this).text() | 0) - 1;
                $table.trigger('repaginate');
                $(this).addClass('active').siblings().removeClass('active');

                if (currentPage >= notToCenter && currentPage <= numPages + 1 - notToCenter) {
                    moveBar.scrollLeft = (currentPage - notToCenter) * 40;
                }

            });

            $('#moveFirst').on('click', function () {
                moveBar.scrollLeft = 0;
                $('div.num-list ul li:first').addClass('active').siblings().removeClass('active');
                currentPage = 0;
                $table.trigger('repaginate');
            });

            $('#moveLast').on('click', function () {
                moveBar.scrollLeft = (numPages + 1) * 40 - (showCount * 40);
                $('div.num-list ul li:last').addClass('active').siblings().removeClass('active');
                currentPage = numPages - 1;
                $table.trigger('repaginate');
            });

            $('#moveForward').on('click', function () {
                var left = moveBar.scrollLeft - 30;
                moveBar.scrollLeft = left;
            });

            $('#moveForward').on({
                mouseover: function () {
                    thumbs_mouse_interval = setInterval(
                    function () {
                        var left = moveBar.scrollLeft - 5;
                        moveBar.scrollLeft = left;
                    },
                    20
                );
                }, mouseout: function () {
                    clearInterval(thumbs_mouse_interval);
                }
            });

            $('#moveBack').on('click', function () {
                var left = moveBar.scrollLeft + 30;
                moveBar.scrollLeft = left;
            });

            $('#moveBack').on({
                mouseover: function () {
                    thumbs_mouse_interval = setInterval(
                    function () {
                        var left = moveBar.scrollLeft + 5;
                        moveBar.scrollLeft = left;
                    },
                    20
                );
                }, mouseout: function () {
                    clearInterval(thumbs_mouse_interval);
                }
            });
        }

        function empty(element) {
            while (element.lastChild) {
                element.removeChild(element.lastChild);
            }
        }

    </script>

</asp:Content>

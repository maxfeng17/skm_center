﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProposalMenu.ascx.cs" Inherits="LunchKingSite.Web.Sal.ProposalMenu" %>

<script>
function getTabWidth(){
    var c = $("#pnProductNav").find("a").length;
    return c * 100 + 100;
}
$(document).ready(function () {
    $("#pnProductNavContents").css("width", getTabWidth() + "px");
});
</script>

<style>
.pn-ProductNav_Wrapper {
  position: relative;
  padding: 0 11px;
}

.pn-ProductNav {
  /* Make this scrollable when needed */
  overflow-x: auto;
  /* We don't want vertical scrolling */
  overflow-y: hidden;
  /* For WebKit implementations, provide inertia scrolling */
  -webkit-overflow-scrolling: touch;
  /* We don't want internal inline elements to wrap */
  white-space: nowrap;
  /* If JS present, let's hide the default scrollbar */
  /* positioning context for advancers */
  position: relative;
}
.js .pn-ProductNav {
  /* Make an auto-hiding scroller for the 3 people using a IE */
  -ms-overflow-style: -ms-autohiding-scrollbar;
  /* Remove the default scrollbar for WebKit implementations */
}
.js .pn-ProductNav::-webkit-scrollbar {
  display: none;
}

.pn-ProductNav_Contents {
  float: left;
  transition: -webkit-transform .2s ease-in-out;
  transition: transform .2s ease-in-out;
  transition: transform .2s ease-in-out, -webkit-transform .2s ease-in-out;
}

.pn-ProductNav_Contents-no-transition {
  transition: none;
}

.mc-navbtn-new {
    width: 100px;
	height: 30px;
	float: left;
	margin-right: 1px;
	font-size: 14px;
	font-weight: 600;
	line-height: 30px;
	color: #333;
	text-align: center;
	border: 1px solid #CCC;
	border-bottom: none;
	-webkit-border-radius: 5px 5px 0 0;
       -moz-border-radius: 5px 5px 0 0;
            border-radius: 5px 5px 0 0;
    background-color: #FFFFFF;
    background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#E5E5E5), to(#FFFFFF));
    background-image: -webkit-linear-gradient(#E5E5E5, #FFFFFF);
    background-image: -moz-linear-gradient(#E5E5E5, #FFFFFF);
    background-image: -o-linear-gradient(#E5E5E5, #FFFFFF);
    background-image: linear-gradient(#E5E5E5, #FFFFFF);



}
.mc-navbtn-new.on {
	color: #FFF;
	text-decoration: none;
	border: 1px solid #BF0000;
    background-color: #AF0000;
    background-image: url(../images/btn_bk_pri_ie.png);
    background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#DF0000), to(#AF0000));
    background-image: -webkit-linear-gradient(#DF0000, #AF0000);
    background-image: -moz-linear-gradient(#DF0000, #AF0000);
    background-image: -o-linear-gradient(#DF0000, #AF0000);
    background-image: linear-gradient(#DF0000, #AF0000);
}

.mc-navbtn-new + .mc-navbtn-new {
  /*margin-left: 11px;
  padding-left: 11px;
  border-left: 1px solid #eee;*/
}
.mc-navbtn-new[aria-selected="true"] {
  color: #111;
}

.pn-Advancer {
  /* Reset the button */
  -webkit-appearance: none;
     -moz-appearance: none;
          appearance: none;
  background: transparent;
  padding: 0;
  border: 0;
  /* Now style it as needed */
  position: absolute;
  top: 0;
  bottom: 0;
  /* Set the buttons invisible by default */
  opacity: 0;
  transition: opacity .3s;
}
.pn-Advancer:focus {
  outline: 0;
}
.pn-Advancer:hover {
  cursor: pointer;
}

.pn-Advancer_Left {
  left: 0;
}
[data-overflowing="both"] ~ .pn-Advancer_Left, [data-overflowing="left"] ~ .pn-Advancer_Left {
  opacity: 1;
}

.pn-Advancer_Right {
  right: 0;
}
[data-overflowing="both"] ~ .pn-Advancer_Right, [data-overflowing="right"] ~ .pn-Advancer_Right {
  opacity: 1;
}

.pn-Advancer_Icon {
  width: 20px;
  height: 44px;
  fill: #bbb;
}

</style>


<div class="mc-navbar pn-ProductNav_Wrapper">
    <nav id="pnProductNav" class="pn-ProductNav">
        <div id="pnProductNavContents" class="pn-ProductNav_Contents">
            <a href="SellerList.aspx">
                <div class="mc-navbtn-new <%=SellerList %>">
                    商家管理
                </div>
            </a><a href="ProposalList.aspx">
                <div class="mc-navbtn-new <%=ProposalList %>">
                    提案管理
                </div>
            </a><a href="BusinessList.aspx">
                <div class="mc-navbtn-new <%=BusinessList %>">
                    檔次管理
                </div>
            </a><a href="ProposalProgress.aspx">
                <div class="mc-navbtn-new <%=ProposalProgress %>">
                    進度條
                </div>
            </a><a href="PponDealCalendar.aspx">
                <div class="mc-navbtn-new <%=PponDealCalendar %>">
                    檔期表
                </div>
            </a><a href="BackendRecentDeals.aspx">
                <div class="mc-navbtn-new <%=BackendRecentDeals %>">
                    過往檔次查詢
                </div>
            </a><a href="PponOptionContnet.aspx">
                <div class="mc-navbtn-new <%=PponOptionContnet %>">
                    數量管理
                </div>
            </a>
            <asp:PlaceHolder ID="phdShoppingCartManage" runat="server" Visible="false">
                <a href="ShoppingCartManage.aspx">
                    <div class="mc-navbtn-new <%=ShoppingCartManage %>">
                        購物車審核
                    </div>
                </a>
            </asp:PlaceHolder>
            <a href="StoreQRCodeManager.aspx" style="display: none">
                <div class="mc-navbtn-new <%=StoreQrCodeManager %>">
                    QRCode管理
                </div>
            </a>
            <asp:PlaceHolder ID="phdISPManage" runat="server" Visible="true">
                <a href="ISPManage.aspx">
                    <div class="mc-navbtn-new <%=ISPManage%>">
                        超商取貨管理
                    </div>
                </a>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phdWms" runat="server" Visible="true">
                <a href="proposal/WmsManage">
                    <div class="mc-navbtn-new <%=WmsManage%>">
                        進倉單管理
                    </div>
                </a>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phdWmsReturn" runat="server" Visible="true">
                <a href="proposal/WmsReturn">
                    <div class="mc-navbtn-new <%=WmsReturn%>">
                        還貨單管理
                    </div>
                </a>
            </asp:PlaceHolder>

        </div>

    </nav>
    <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
        <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
            <path d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z" />
        </svg>
    </button>
    <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right" type="button">
        <svg class="pn-Advancer_Icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 551 1024">
            <path d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z" />
        </svg>
    </button>
</div>
  
  



<script>
var SETTINGS = {
    navBarTravelling: false,
    navBarTravelDirection: "",
	 navBarTravelDistance: 150
}

document.documentElement.classList.remove("no-js");
document.documentElement.classList.add("js");

// Out advancer buttons
var pnAdvancerLeft = document.getElementById("pnAdvancerLeft");
var pnAdvancerRight = document.getElementById("pnAdvancerRight");

var pnProductNav = document.getElementById("pnProductNav");
$("#pnProductNavContents").css("width", getTabWidth() + "px");
var pnProductNavContents = document.getElementById("pnProductNavContents");

pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));

// Handle the scroll of the horizontal container
var last_known_scroll_position = 0;
var ticking = false;

function doSomething(scroll_pos) {
    pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
}

pnProductNav.addEventListener("scroll", function() {
    last_known_scroll_position = window.scrollY;
    if (!ticking) {
        window.requestAnimationFrame(function() {
            doSomething(last_known_scroll_position);
            ticking = false;
        });
    }
    ticking = true;
});


pnAdvancerLeft.addEventListener("click", function() {
	// If in the middle of a move return
    if (SETTINGS.navBarTravelling === true) {
        return;
    }
    // If we have content overflowing both sides or on the left
    if (determineOverflow(pnProductNavContents, pnProductNav) === "left" || determineOverflow(pnProductNavContents, pnProductNav) === "both") {
        // Find how far this panel has been scrolled
        var availableScrollLeft = pnProductNav.scrollLeft;
        // If the space available is less than two lots of our desired distance, just move the whole amount
        // otherwise, move by the amount in the settings
        if (availableScrollLeft < SETTINGS.navBarTravelDistance * 2) {
            pnProductNavContents.style.transform = "translateX(" + availableScrollLeft + "px)";
        } else {
            pnProductNavContents.style.transform = "translateX(" + SETTINGS.navBarTravelDistance + "px)";
        }
        // We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
        pnProductNavContents.classList.remove("pn-ProductNav_Contents-no-transition");
        // Update our settings
        SETTINGS.navBarTravelDirection = "left";
        SETTINGS.navBarTravelling = true;
    }
    // Now update the attribute in the DOM
    pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
});

pnAdvancerRight.addEventListener("click", function() {
    // If in the middle of a move return
    if (SETTINGS.navBarTravelling === true) {
        return;
    }
    // If we have content overflowing both sides or on the right
    if (determineOverflow(pnProductNavContents, pnProductNav) === "right" || determineOverflow(pnProductNavContents, pnProductNav) === "both") {
        // Get the right edge of the container and content
        var navBarRightEdge = pnProductNavContents.getBoundingClientRect().right;
        var navBarScrollerRightEdge = pnProductNav.getBoundingClientRect().right;
        // Now we know how much space we have available to scroll
        var availableScrollRight = Math.floor(navBarRightEdge - navBarScrollerRightEdge);
        // If the space available is less than two lots of our desired distance, just move the whole amount
        // otherwise, move by the amount in the settings
        if (availableScrollRight < SETTINGS.navBarTravelDistance * 2) {
            pnProductNavContents.style.transform = "translateX(-" + availableScrollRight + "px)";
        } else {
            pnProductNavContents.style.transform = "translateX(-" + SETTINGS.navBarTravelDistance + "px)";
        }
        // We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
        pnProductNavContents.classList.remove("pn-ProductNav_Contents-no-transition");
        // Update our settings
        SETTINGS.navBarTravelDirection = "right";
        SETTINGS.navBarTravelling = true;
    }
    // Now update the attribute in the DOM
    pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
});

pnProductNavContents.addEventListener(
    "transitionend",
    function() {
        // get the value of the transform, apply that to the current scroll position (so get the scroll pos first) and then remove the transform
        var styleOfTransform = window.getComputedStyle(pnProductNavContents, null);
        var tr = styleOfTransform.getPropertyValue("-webkit-transform") || styleOfTransform.getPropertyValue("transform");
        // If there is no transition we want to default to 0 and not null
        var amount = Math.abs(parseInt(tr.split(",")[4]) || 0);
        pnProductNavContents.style.transform = "none";
        pnProductNavContents.classList.add("pn-ProductNav_Contents-no-transition");
        // Now lets set the scroll position
        if (SETTINGS.navBarTravelDirection === "left") {
            pnProductNav.scrollLeft = pnProductNav.scrollLeft - amount;
        } else {
            pnProductNav.scrollLeft = pnProductNav.scrollLeft + amount;
        }
        SETTINGS.navBarTravelling = false;
    },
    false
);

// Handle setting the currently active link
pnProductNavContents.addEventListener("click", function(e) {
	var links = [].slice.call(document.querySelectorAll(".mc-navbtn-new"));
	links.forEach(function(item) {
		item.setAttribute("aria-selected", "false");
	})
	e.target.setAttribute("aria-selected", "true");
})

function determineOverflow(content, container) {
    var containerMetrics = container.getBoundingClientRect();
    var containerMetricsRight = Math.floor(containerMetrics.right);
    var containerMetricsLeft = Math.floor(containerMetrics.left);
    var contentMetrics = content.getBoundingClientRect();
    var contentMetricsRight = Math.floor(contentMetrics.right);
    var contentMetricsLeft = Math.floor(contentMetrics.left);
	 if (containerMetricsLeft > contentMetricsLeft && containerMetricsRight < contentMetricsRight) {
        return "both";
    } else if (contentMetricsLeft < containerMetricsLeft) {
        return "left";
    } else if (contentMetricsRight > containerMetricsRight) {
        return "right";
    } else {
        return "none";
    }
}
</script>


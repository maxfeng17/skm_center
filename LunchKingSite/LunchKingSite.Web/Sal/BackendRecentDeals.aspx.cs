﻿using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Facade;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Mongo.Services;
using LunchKingSite.Mongo.Facade;
using Autofac;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.BizLogic.Model;
using log4net;

namespace LunchKingSite.Web.Sal
{
    public partial class BackendRecentDeals : RolePage, IBackendRecentDeals
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(BackendRecentDeals));

        #region props
        private BackendRecentDealsPresenter _presenter;
        private ISellerProvider sp;
        private IPponProvider pp;
        public BackendRecentDealsPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        public DateTime? BusinessOrderTimeS
        {
            get
            {
                DateTime date;
                if (DateTime.TryParse(txtBusinessStartTimeS.Text, out date))
                {
                    return date;
                }
                else
                {
                    return null;
                }
            }
        }

        public DateTime? BusinessOrderTimeE
        {
            get
            {
                DateTime date;
                if (DateTime.TryParse(txtBusinessStartTimeE.Text, out date))
                {
                    return date;
                }
                else
                {
                    return null;
                }
            }
        }

        public DateTime? BusinessDeliverTimeS
        {
            get
            {
                DateTime date;
                if (DateTime.TryParse(txtBusinessDeliverTimeS.Text, out date))
                {
                    return date;
                }
                else
                {
                    return null;
                }
            }
        }

        public DateTime? BusinessDeliverTimeE
        {
            get
            {
                DateTime date;
                if (DateTime.TryParse(txtBusinessDeliverTimeE.Text, out date))
                {
                    return date;
                }
                else
                {
                    return null;
                }
            }
        }
        public ChannelType Channel
        {
            get
            {
                ChannelType type = ChannelType.Ppon;
                ChannelType.TryParse(ddlChannelType.SelectedValue, out type);
                return type;
            }
        }

        public int CategoryId
        {
            get
            {
                int categoryId = 0;
                if (Channel == ChannelType.Ppon)
                {
                    int city = int.TryParse(ddlCityId.SelectedValue, out city) ? city : 0;
                    if (city != 0)
                    {
                        PponCity pponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(city);
                        if (pponCity != null)
                        {
                            return pponCity.CategoryId;
                        }
                    }
                }
                return categoryId;
            }
        }

        public int? Dealtype1
        {
            get
            {
                int dealtype1 = int.TryParse(ddlDealType1.SelectedValue, out dealtype1) ? dealtype1 : 0;

                return dealtype1;
            }
        }

        public int? Dealtype2
        {
            get
            {
                //int dealtype2 = int.TryParse(ddlDealType2.SelectedValue, out dealtype2) ? dealtype2 : 0;
                int dealtype2 = int.TryParse(hdDealType2.Value, out dealtype2) ? dealtype2 : 0;

                return dealtype2;
            }
        }


        public bool IsCloseDeal
        {
            get
            {
                if (chkDealTime.SelectedIndex == 0)
                    return true;
                else
                    return false;

            }
        }

        public bool IsOnDeal
        {
            get
            {
                if (chkDealTime.SelectedIndex == 1)
                    return true;
                else
                    return false;
            }
        }

        public string SellerName
        {
            get
            {
                return txtSearchStoreName.Text;
            }
        }

        public int? SalesId
        {
            get
            {
                if (string.IsNullOrEmpty(txtSalesName.Text))
                    return null;
                else
                    return HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.EmpName, txtSalesName.Text).UserId;
            }
        }

        public Guid SellerGuid
        {
            get
            {
                Guid sid = Guid.Empty;
                if (Request.QueryString["sid"] != null)
                {
                    Guid.TryParse(Request.QueryString["sid"].ToString(), out sid);
                }
                return sid;
            }
        }

        public string DealName
        {
            get
            {
                return txtSearchDealName.Text;
            }
        }
        public string BusinessHourGuid {
            get
            {
                string businessHourGuid = string.Empty;
                Guid bid = Guid.Empty;
                if (string.IsNullOrEmpty(txtBid.Text))
                {
                    return businessHourGuid;//沒輸入
                }
                else
                {
                    if (ddlBidFilter.SelectedValue == "2")
                    {
                        if (Guid.TryParse(txtBid.Text, out bid))
                        {
                            businessHourGuid = bid.ToString();
                        }
                        else
                        {
                            businessHourGuid = bid.ToString();//輸入錯誤
                        }
                    }
                }
                    
                return businessHourGuid;
            }
        }
        public int UniqueId
        {
            get
            {
                int uniqueid = 0;
                if (string.IsNullOrEmpty(txtBid.Text))
                {
                    return uniqueid;//沒輸入
                }
                else
                {
                    if (ddlBidFilter.SelectedValue == "1")
                    {
                        if (int.TryParse(txtBid.Text, out uniqueid))
                        {
                            
                        }
                        else
                        {
                            uniqueid = -1;//輸入錯誤
                        }
                    }
                    return uniqueid;
                }
                
            }
        }

        public int Wms
        {
            get
            {
                int wms = int.TryParse(ddlWms.SelectedValue, out wms) ? wms : 0;

                return wms;
            }
        }

        public int PageSize
        {
            get
            {
                return ucPager.PageSize;
            }
        }

        public int CurrentPage
        {
            get
            {
                return ucPager.CurrentPage;
            }
        }

        public string OrderField
        {
            get
            {
                //ViewPponDeal view = new ViewPponDeal();   
                return hdOrderField.Value;
            }
        }
        public string OrderBy
        {
            get
            {
                string _order = hdOrderBy.Value;

                return _order;
            }
        }


        private ViewEmployee _Emp = null;
        public ViewEmployee Emp
        {
            get { return _Emp; }
            set { _Emp = value; }
        }


        private string _CrossDeptTeam = "";
        public string CrossDeptTeam
        {
            get { return _CrossDeptTeam; }
            set { _CrossDeptTeam = value; }

        }

        #endregion

        #region event
        public event EventHandler SearchClicked;
        public event EventHandler<DataEventArgs<int>> PageChange;
        public event EventHandler<DataEventArgs<int>> GetCount; 
        #endregion

        #region method
        public void SetSelectableZone(List<PponCity> cities)
        {
            ddlCityId.DataSource = cities;
            ddlCityId.DataTextField = "EdmCityName";
            ddlCityId.DataValueField = "CityId";
            ddlCityId.DataBind();
            ddlCityId.Items.Insert(0, new ListItem("全頻道", ""));
            ddlCityId.SelectedIndex = 0;
        }

        public void SetDealType(int parentId, int codeId, List<SystemCode> dealTypes)
        {
            #region 檔次母分類

            ddlDealType1.Items.Clear();
            ddlDealType1.Items.Add(new ListItem("=請選擇母分類=", "0"));

            List<SystemCode> dealTypes1 = dealTypes.Where(x => x.ParentCodeId == null).OrderBy(x => x.Seq).ToList();
            foreach (var code in dealTypes1)
            {
                ddlDealType1.Items.Add(new ListItem(code.CodeName, code.CodeId.ToString()));
                if (code.CodeId == 1999)
                {
                    ddlDealType1.Items.FindByValue("1999").Attributes.Add("Disabled", "Disabled");
                }
            }
            ddlDealType1.SelectedValue = parentId.ToString();

            #endregion

            #region 檔次子分類

            ddlDealType2.Items.Clear();
            ddlDealType2.Items.Add(new ListItem("=請選擇子分類=", "0"));

            List<SystemCode> dealTypes2 = dealTypes.Where(x => x.ParentCodeId == parentId).OrderBy(x => x.Seq).ToList();
            foreach (var code in dealTypes2)
            {
                ddlDealType2.Items.Add(new ListItem(code.CodeName, code.CodeId.ToString()));
            }
            ddlDealType2.SelectedValue = codeId.ToString();
            hdDealType2.Value = codeId.ToString();

            #endregion
        }



        public void SetPponDeals(Dictionary<ViewPponDealCalendar, List<ViewPponDealCalendar>> deallist)
        {
            divResult.Visible = true;
            repPponRecentDeal.Visible = true;
            repPiinlifeRecentDeal.Visible = false;
            ucPager.Visible = true;

            repPponRecentDeal.DataSource = deallist;
            repPponRecentDeal.DataBind();
        }

        public void SetPiinLifeDeals(ViewHiDealSellerProductCollection deallist)
        {
            divResult.Visible = true;
            repPponRecentDeal.Visible = false;
            repPiinlifeRecentDeal.Visible = true;
            ucPager.Visible = true;

            repPiinlifeRecentDeal.DataSource = deallist;
            repPiinlifeRecentDeal.DataBind();
        } 
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!_presenter.OnViewInitialized())
                    return;

                ddlChannelType.Items.Add(new ListItem("17Life", ((int)ChannelType.Ppon).ToString()));
                ddlChannelType.Items.Add(new ListItem("舊品生活", ((int)ChannelType.PiinLife).ToString()));
                ddlChannelType.SelectedIndex = 0;


                ddlWms.Items.Add(new ListItem("請選擇", "-1"));
                ddlWms.Items.Add(new ListItem(Helper.GetDescription(WarehouseType.Normal), ((int)WarehouseType.Normal).ToString()));
                ddlWms.Items.Add(new ListItem(Helper.GetDescription(WarehouseType.Wms), ((int)WarehouseType.Wms).ToString()));

                ((Ppon.Ppon)base.Master).ViewPortEnabled = true;

                EnabledSettings(UserName);

            }
            _presenter.OnViewLoaded();


            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        }

        protected void RepPponRecentDeal_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            SalesService _salServ;
            _salServ = CoreModule.Container.Resolve<SalesService>();
            if (e.Item.DataItem is KeyValuePair<ViewPponDealCalendar, List<ViewPponDealCalendar>>)
            {
                KeyValuePair<ViewPponDealCalendar, List<ViewPponDealCalendar>> data = (KeyValuePair<ViewPponDealCalendar, List<ViewPponDealCalendar>>)e.Item.DataItem;
                HyperLink hypOrderID = (HyperLink)e.Item.FindControl("hypOrderID");
                ImageButton btnOrderID = (ImageButton)e.Item.FindControl("btnOrderID");
                Label lblRecentDealDate = (Label)e.Item.FindControl("lblRecentDealDate");
                Label lblDeliverDate = (Label)e.Item.FindControl("lblDeliverDate");
                Label lblSellerName = (Label)e.Item.FindControl("lblSellerName");
                HyperLink hypItemName = (HyperLink)e.Item.FindControl("hypItemName");
                Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
                Label lblContinuedQuantity = (Label)e.Item.FindControl("lblContinuedQuantity");
                Label lblPrice = (Label)e.Item.FindControl("lblPrice");
                HtmlGenericControl Expand = (HtmlGenericControl)e.Item.FindControl("Expand");

                //ViewProposalSeller pro = sp.ViewProposalSellerGet(ViewProposalSeller.Columns.BusinessHourGuid, data.Key.BusinessHourGuid);
                int? pid = data.Key.Id;
                ViewPponDeal v = pp.ViewPponDealGetByBusinessHourGuid(data.Key.BusinessHourGuid);
                if (pid != null)
                {
                    //提案單
                    hypOrderID.Text = pid.ToString() + (data.Key.ProposalSourceType == (int)ProposalSourceType.Original ? "(舊)" : string.Empty);
                    
                    btnOrderID.Visible = true;
                    if (data.Key.ProposalSourceType == (int)ProposalSourceType.Original)
                    {
                        btnOrderID.PostBackUrl = config.SiteUrl + "/Sal/ProposalContent.aspx?pid=" + pid;
                        hypOrderID.NavigateUrl = config.SiteUrl + "/Sal/ProposalContent.aspx?pid=" + pid;
                    }
                    else
                    {
                        btnOrderID.PostBackUrl = config.SiteUrl + "/sal/proposal/house/proposalcontent?pid=" + pid;
                        hypOrderID.NavigateUrl = config.SiteUrl + "/sal/proposal/house/proposalcontent?pid=" + pid;
                    }

                }
                else
                {
                    try
                    {
                        //工單
                        BusinessOrder businessOrder = _salServ.GetBusinessOrderByBusinessHourGuid(data.Key.BusinessHourGuid);

                        if (businessOrder != null)
                        {
                            //工單
                            //hypOrderID.Text = businessOrder.BusinessOrderId;
                            //hypOrderID.NavigateUrl = config.SiteUrl + "/Sal/BusinessOrderDetail.aspx?boid=" + businessOrder.Id;
                            btnOrderID.Visible = true;
                            btnOrderID.PostBackUrl = config.SiteUrl + "/Sal/BusinessOrderDetail.aspx?boid=" + businessOrder.Id;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("連線錯誤，使用者:" + UserName, ex);
                    }
                    
                }

                //若有調整有效期限截止日以該日期為準
                if (data.Key.ChangedExpireDate != null)
                    data.Key.BusinessHourDeliverTimeE = data.Key.ChangedExpireDate.Value;


                lblRecentDealDate.Text = data.Key.BusinessHourOrderTimeS.ToString("yyyy/MM/dd") + " ~ " + data.Key.BusinessHourOrderTimeE.ToString("yyyy/MM/dd");
                if ((data.Key.BusinessHourDeliverTimeS != null) && (data.Key.BusinessHourDeliverTimeE != null))
                {
                    lblDeliverDate.Text = ((DateTime)data.Key.BusinessHourDeliverTimeS).ToString("yyyy/MM/dd") + " ~ " + ((DateTime)data.Key.BusinessHourDeliverTimeE).ToString("yyyy/MM/dd");
                }

                ///
                //lblSellerName.Text = data.Key.DevelopeSalesId != 0 ? GetPrivilege(data.Key.DevelopeSalesId, data.Key.OperationSalesId ?? 0) ? data.Key.SellerName : string.Empty : string.Empty;
                lblSellerName.Text = data.Key.DevelopeSalesId != 0 ? GetPrivilege(data.Key.DevelopeSalesId, data.Key.DevelopeDeptId, data.Key.DevelopeTeamNo, data.Key.OperationSalesId, data.Key.OperationDeptId, data.Key.OperationTeamNo) ? data.Key.SellerName : string.Empty : string.Empty;

                int length = 60;
                hypItemName.Text = (string.IsNullOrEmpty(data.Key.ItemName)) ? "無" : ((data.Key.ItemName.Length <= length) ? data.Key.ItemName : data.Key.ItemName.Substring(0, length) + "..");
                hypItemName.NavigateUrl = config.SiteUrl + "/" + data.Key.BusinessHourGuid;
                hypItemName.Attributes.Add("title", data.Key.ItemName);


                int quantity = 0;   //實際銷售
                decimal price = 0;  //營業額
                if (Helper.IsFlagSet(data.Key.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
                {
                    //多檔次
                    if (data.Key.BusinessHourOrderTimeE > DateTime.Now)
                    {
                        quantity = data.Value.Sum(x => x.OrderedQuantity ?? 0);
                        price = data.Value.Sum(y => y.ItemPrice * (y.OrderedQuantity ?? 0));
                    }
                    else
                    {
                        foreach (ViewPponDealCalendar sub in data.Value)
                        {
                            int slug = 0;
                            if (sub.Slug != null && int.TryParse(sub.Slug, out slug))
                            {
                                quantity += slug;
                                price += sub.ItemPrice * slug;
                            }
                            else
                            {
                                quantity += sub.OrderedQuantity ?? 0;
                                price += sub.ItemPrice * (sub.OrderedQuantity ?? 0);
                            }
                           
                        }
                    }
                }
                else
                {
                    //單一檔次
                    if (data.Key.BusinessHourOrderTimeE > DateTime.Now)
                    {
                        quantity = data.Key.OrderedQuantity ?? 0;
                    }
                    else
                    {
                        int slug = 0;
                        if (data.Key.Slug != null && int.TryParse(data.Key.Slug, out slug))
                        {
                            quantity = slug;
                        }
                        else
                        {
                            quantity = data.Key.OrderedQuantity ?? 0;
                        }
                    }

                    price = data.Key.ItemPrice * quantity;
                }
                lblQuantity.Text = quantity.ToString();

                // 前台顯示銷售數量
                if (data.Key.BusinessHourOrderTimeE > DateTime.Now)
                {
                    lblContinuedQuantity.Text = v.GetAdjustedOrderedQuantity().Quantity.ToString();
                }
                else
                {
                    lblContinuedQuantity.Text = v.GetAdjustedSlug().Quantity.ToString();
                }

                lblPrice.Text = Convert.ToInt32(price).ToString();

                Repeater rpt = (Repeater)e.Item.FindControl("rptSubDeals");
                if (data.Value != null)
                {
                    rpt.DataSource = data.Value.OrderBy(x => x.ItemName);
                    rpt.DataBind();
                    Expand.Visible = true;
                }
                else
                {
                    Expand.Visible = false;
                }        
            }          
        }

        protected void rptSubDeals_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewPponDealCalendar)
            {
                ViewPponDealCalendar data = (ViewPponDealCalendar)e.Item.DataItem;
                Label lblRecentDealDate = (Label)e.Item.FindControl("lblRecentDealDate");
                Label lblDeliverDate = (Label)e.Item.FindControl("lblDeliverDate");
                Label lblSellerName = (Label)e.Item.FindControl("lblSellerName");
                HyperLink hypEventName = (HyperLink)e.Item.FindControl("hypEventName");
                Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
                Label lblPrice = (Label)e.Item.FindControl("lblPrice");

                //若有調整有效期限截止日以該日期為準
                if (data.ChangedExpireDate != null)
                    data.BusinessHourDeliverTimeE = data.ChangedExpireDate.Value;

                lblRecentDealDate.Text = data.BusinessHourOrderTimeS.ToString("yyyy/MM/dd") + " ~ " + data.BusinessHourOrderTimeE.ToString("yyyy/MM/dd");
                if ((data.BusinessHourDeliverTimeS != null) && (data.BusinessHourDeliverTimeE != null))
                {
                    lblDeliverDate.Text = ((DateTime)data.BusinessHourDeliverTimeS).ToString("yyyy/MM/dd") + " ~ " + ((DateTime)data.BusinessHourDeliverTimeE).ToString("yyyy/MM/dd");
                }


                lblSellerName.Text = data.DevelopeSalesId != 0 ? GetPrivilege(data.DevelopeSalesId, data.DevelopeDeptId, data.DevelopeTeamNo, data.OperationSalesId, data.OperationDeptId, data.OperationTeamNo) ? data.SellerName : string.Empty : string.Empty;
                hypEventName.Text = !string.IsNullOrEmpty(data.EventName) ? data.EventName : "無";
                hypEventName.NavigateUrl = config.SiteUrl + "/" + data.BusinessHourGuid;
                if (data.BusinessHourOrderTimeE > DateTime.Now)
                {
                    lblQuantity.Text = data.OrderedQuantity.ToString();
                }
                else
                {
                    int slug = 0;
                    if (data.Slug != null && int.TryParse(data.Slug, out slug))
                    {
                        lblQuantity.Text = slug.ToString();
                    }
                    else
                    {
                        lblQuantity.Text = (data.OrderedQuantity ?? 0).ToString();
                    }
                }
                //營業額
                lblPrice.Text = Convert.ToInt32((decimal.Parse(lblQuantity.Text) * data.ItemPrice)).ToString();
            }
        }

        protected void RepPiinlifeRecentDeal_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ViewHiDealSellerProduct data = (ViewHiDealSellerProduct)e.Item.DataItem;

            if (data != null)
            {
                Label lblRecentDealDate = (Label)e.Item.FindControl("lblRecentDealDate");
                Label lblSellerName = (Label)e.Item.FindControl("lblSellerName");
                HyperLink hypEventName = (HyperLink)e.Item.FindControl("hypEventName");
                Label lblProductName = (Label)e.Item.FindControl("lblProductName");
                Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");

                lblRecentDealDate.Text = ((data.DealStartTime == null) ? null : data.DealStartTime.Value.ToString("yyyy/MM/dd")) + " ~ " + ((data.DealEndTime == null) ? null : data.DealEndTime.Value.ToString("yyyy/MM/dd"));

                int length = 40;
                lblSellerName.Text = !string.IsNullOrEmpty(data.Sales) && data.EmpUserId != null ? GetPrivilege((int)data.EmpUserId, data.DeptId, data.TeamNo, null, "", null) ? data.SellerName : string.Empty : string.Empty;
                hypEventName.Text = (string.IsNullOrEmpty(data.PromoLongDesc)) ? "無" : ((data.PromoLongDesc.Length <= length) ? data.PromoLongDesc : data.PromoLongDesc.Substring(0, length) + "..");
                hypEventName.NavigateUrl = config.SiteUrl + "/piinlife/HiDeal.aspx?did=" + data.DealId;
                hypEventName.Attributes.Add("title", data.PromoLongDesc);
                lblProductName.Text = (string.IsNullOrEmpty(data.Productname)) ? "無" : ((data.Productname.Length <= 6) ? data.Productname : data.Productname.Substring(0, 6) + "..");
                lblProductName.Attributes.Add("title", data.Productname);
                lblQuantity.Text = data.OrderCount.ToString();
            }
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            if (SearchClicked != null)
            {
                
                GetEmpPrivilege();

                hdOrderBy.Value = "asc";
                hdOrderField.Value = "";

                SearchClicked(sender, e);
                ucPager.ResolvePagerView(1, true);
            }

            SetProductApproveEnabled();     
        }

        protected void hidSortSearch_Click(object sender, EventArgs e)
        {
            if (SearchClicked != null)
            {
                GetEmpPrivilege();

                if (hdOrderBy.Value == "desc")
                {
                    hdOrderBy.Value = "asc";
                }
                else
                {
                    hdOrderBy.Value = "desc";
                }


                SearchClicked(sender, e);
                ucPager.ResolvePagerView(1, true);
            }
        }
        #endregion



        #region private method

        /// <summary>
        /// 取得username資料和權限
        /// </summary>
        private void GetEmpPrivilege()
        {
            Emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, UserName);

            if (!string.IsNullOrEmpty(Emp.CrossDeptTeam))
                CrossDeptTeam = Emp.CrossDeptTeam;
            else if (Emp.TeamNo == null)
                CrossDeptTeam = Emp.DeptId + "[0]";
            else
                CrossDeptTeam = Emp.DeptId + "[" + Emp.TeamNo + "]";
        }

        private bool GetPrivilege(int developeSalesId, string developeDeptId, int? developeTeamNo, int? operationSalesId, string operationDeptId, int? operationTeamNo)
        {
            //該檔次業務人員
            //ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGet(Employee.Columns.UserId, developeSalesId);
            //ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGet(Employee.Columns.UserId, operationSalesId);
            //ViewEmployee emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, UserName);

            //開發業務組別
            int deNo = developeTeamNo != null ? (int)developeTeamNo : 0;
            string deDept = developeDeptId;

            //經營業務組別
            int opNo = operationTeamNo != null ? (int)operationTeamNo : 0;
            string opDept = operationDeptId;



            if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReadAll) || CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ProductApprove))
            {
                //全區
                return true;
            }
            else if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.CrossDeptTeam))
            {
                //跨區
                if (!string.IsNullOrEmpty(CrossDeptTeam))
                {
                    string message = "";
                    bool checkDeSales = ProposalFacade.GetCrossPrivilege(3, Emp, deDept, deNo, "", null, ref message);
                    if (!checkDeSales)
                    {
                        if (operationSalesId != null)
                        {
                            bool checkOpSales = ProposalFacade.GetCrossPrivilege(3, Emp, opDept, opNo, "", null, ref message);

                            if (!checkOpSales)
                                return false;
                            else
                                return true;
                        }
                        else
                            return false;
                    }
                    else
                        return true;
                 

                }
                else
                {
                    if ((Emp.DeptId == deDept && (int)Emp.TeamNo == deNo) || (Emp.DeptId == opDept && (int)Emp.TeamNo == opNo))
                        return true;
                    else
                        return false;
                }
            }
            else
            {
                //僅瀏覽
                if (Emp.UserId == developeSalesId || Emp.UserId == operationSalesId)
                    return true;
                else
                    return false;
            }

        }

        private void SetProductApproveEnabled()
        {
            string script = "";
            bool show = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ProductApprove);
            script = "<script type='text/javascript'>ShowOrderID('" + show + "');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowOrderID", script, false);
        }

        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChange != null)
            {
                GetEmpPrivilege();

                this.PageChange(this, new DataEventArgs<int>(pageNumber));
                ucPager.ResolvePagerView(pageNumber, false);

                SetProductApproveEnabled();
            }
        }



        protected int RecentDealGetCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (GetCount != null)
            {
                GetCount(this, e);
            }
            return e.Data;
        }

        private void EnabledSettings(string username)
        {
            ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(username));
            if (emp.IsLoaded)
            {
                if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.ReadAll))
                {
                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.CrossDeptTeam))
                {

                    if (string.IsNullOrEmpty(emp.CrossDeptTeam) && emp.TeamNo == null)
                    {
                        ShowMessage("身份權限未明，無法查看資料，請洽產品或技術人員協助");
                    }
                }
                else
                {
                }
            }
            else
            {
                ShowMessage("查無員工資料，請重新檢視員工資料維護設定。");
            }

        }

        public void ShowMessage(string msg)
        {
            string script = string.Format("alert('{0}');", msg);
            Page.ClientScript.RegisterStartupScript(GetType(), "alert", script, true);
        }

        #endregion

        #region WS
        [WebMethod]
        public static string GetDealType2(int parentId)
        {
            var scs = PponFacade.DealType2GetList(parentId);
            return new JsonSerializer().Serialize(scs);
        } 
        #endregion

        

        
    }
}
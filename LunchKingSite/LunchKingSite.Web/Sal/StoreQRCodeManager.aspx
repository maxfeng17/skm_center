﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="StoreQRCodeManager.aspx.cs" Inherits="LunchKingSite.Web.Sal.StoreQRCodeManager" %>
<%@ Register Src="~/Sal/ProposalMenu.ascx" TagName="ProposalMenu" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CSS" runat="server">
    <link href="<%=ResolveUrl("~/Tools/js/css/ui-smoothness/jquery-ui-1.10.3.custom.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/fancybox/jquery.fancybox.css") %>" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SSC" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="RG" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MC" runat="server">
    <style>
        .mc-navbar .mc-navbtn{
            width:10%;
        }
    </style>
    <uc2:ProposalMenu ID="ProposalMenu" runat="server" ></uc2:ProposalMenu>
    <div class="mc-content">
        <h1 class="rd-smll">反核銷 QRCode 管理</h1>
        <hr class="header_hr">
        <div class="grui-form">
            <div class="form-unit">
                <label class="unit-label rd-unit-label">查詢賣家名稱</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox ID="txtItemName" runat="server" CssClass="input-half" Style="width: 200px"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" Text="搜尋" CssClass="btn rd-mcacstbtn" />
                </div>
            </div>
            <div class="form-unit end-unit">
                <asp:Button ID="btnDownload" runat="server" Text="QRCode 下載" CssClass="btn rd-mcacstbtn" />
            </div>
        </div>
    
    </div>
    
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using MongoDB.Bson;
using log4net;

namespace LunchKingSite.Web.Sal
{
    public partial class RestrictionsGenerator : RolePage, ISalesRestrictionsGeneratorView
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(RestrictionsGenerator));

        #region props
        private SalesRestrictionsGeneratorPresenter _presenter;
        public SalesRestrictionsGeneratorPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public Guid Bid
        {
            get
            {
                Guid id = Guid.Empty;
                if (Request.QueryString["bid"] != null && Guid.TryParse(Request.QueryString["bid"].ToString(), out id))
                {
                    Guid.TryParse(Request.QueryString["bid"].ToString(), out id);
                }
                return id;
            }
        }

        public int Pid
        {
            get
            {
                int pid = 0;
                if (Request.QueryString["pid"] != null && int.TryParse(Request.QueryString["pid"].ToString(), out pid))
                {
                    int.TryParse(Request.QueryString["pid"].ToString(), out pid);
                }
                return pid;
            }
        }

        public ProvisionDepartmentType Type
        {
            get
            {
                ProvisionDepartmentType t = ProvisionDepartmentType.Ppon;
                if (Request.QueryString["type"] != null)
                {
                    ProvisionDepartmentType.TryParse(Request.QueryString["type"].ToString(), out t);
                }
                return t;
            }
        }

        public ObjectId DepartmentId
        {
            get
            {
                ObjectId id;
                ObjectId.TryParse(hidDepartmentId.Value, out id);
                if (!id.Equals(ObjectId.Empty))
                    return id;
                else
                {
                    ObjectId.TryParse(ddlDepartment.SelectedValue, out id);
                    DepartmentId = id;
                    return id;
                }
            }
            set
            {
                hidDepartmentId.Value = value.ToString();
            }
        }

        public ObjectId CategoryId
        {
            get
            {
                ObjectId id;
                ObjectId.TryParse(hidCategoryId.Value, out id);
                if (!id.Equals(ObjectId.Empty))
                    return id;
                else
                {
                    ObjectId.TryParse(ddlCategory.SelectedValue, out id);
                    CategoryId = id;
                    return id;
                }
            }
            set
            {
                hidCategoryId.Value = value.ToString();
            }
        }

        public ObjectId ItemId
        {
            get
            {
                ObjectId id;
                ObjectId.TryParse(hidItemId.Value, out id);
                if (!id.Equals(ObjectId.Empty))
                    return id;
                else
                {
                    ObjectId.TryParse(ddlItem.SelectedValue, out id);
                    ItemId = id;
                    return id;
                }
            }
            set
            {
                hidItemId.Value = value.ToString();
            }
        }

        public ObjectId ContentId
        {
            get
            {
                ObjectId id;
                ObjectId.TryParse(hidContentId.Value, out id);
                if (!id.Equals(ObjectId.Empty))
                    return id;
                else
                {
                    ObjectId.TryParse(ddlContent.SelectedValue, out id);
                    ContentId = id;
                    return id;
                }
            }
            set
            {
                hidContentId.Value = value.ToString();
            }
        }

        public ObjectId DescId
        {
            get
            {
                ObjectId id;
                ObjectId.TryParse(hidDescId.Value, out id);
                return id;
            }
            set
            {
                hidDescId.Value = value.ToString();
            }
        }
        #endregion

        #region event
        public event EventHandler<DataEventArgs<string>> Save;
        public event EventHandler<EventArgs> ProvisionSelectedIndexChanged;
        #endregion

        #region method
        public void SetRestrictions(IEnumerable<ProvisionDepartment> departments, DealAccounting da, BusinessHour bh, Proposal pro, string restrictions)
        {
            try
            {
                ProvisionDepartment data = departments.FirstOrDefault();
                switch (Type)
                {
                    case ProvisionDepartmentType.Product:
                        data = departments.Where(x => x.Name == "P商品").FirstOrDefault();
                        break;
                    case ProvisionDepartmentType.Travel:
                        data = departments.Where(x => x.Name == "P旅遊").FirstOrDefault();
                        break;
                    case ProvisionDepartmentType.Ppon:
                    default:
                        data = departments.Where(x => x.Name == "P好康").FirstOrDefault();
                        break;
                }
            }
            catch (Exception ex)
            {
                logger.Error("連線錯誤，使用者:" + UserName, ex);
            }
            


            if (!string.IsNullOrWhiteSpace(restrictions))
            {
                txtResult.Text = restrictions.Replace("<br />", "\n");
            }

            EnabledSettings(da, bh, pro, departments, null);         
        }

        public void SetRestrictionsModel(IEnumerable<ProvisionDepartmentModel> departments, DealAccounting da, BusinessHour bh, Proposal pro, string restrictions)
        {
            try
            {
                ProvisionDepartmentModel data = departments.FirstOrDefault();
                switch (Type)
                {
                    case ProvisionDepartmentType.Product:
                        data = departments.Where(x => x.Name == "P商品").FirstOrDefault();
                        break;
                    case ProvisionDepartmentType.Travel:
                        data = departments.Where(x => x.Name == "P旅遊").FirstOrDefault();
                        break;
                    case ProvisionDepartmentType.Ppon:
                    default:
                        data = departments.Where(x => x.Name == "P好康").FirstOrDefault();
                        break;
                }
            }
            catch (Exception ex)
            {
                logger.Error("連線錯誤，使用者:" + UserName, ex);
            }



            if (!string.IsNullOrWhiteSpace(restrictions))
            {
                txtResult.Text = restrictions.Replace("<br />", "\n");
            }

            EnabledSettings(da, bh, pro, null, departments);
        }

        public void ShowMessage(string msg, BusinessContentMode mode, string parms = "")
        {
            string script = string.Empty;
            if (!string.IsNullOrWhiteSpace(msg))
            {
                script = string.Format("alert('{0}');", msg);
            }
            switch (mode)
            {
                case BusinessContentMode.BackToList:
                    script += string.Format("location.href='{1}/sal/BusinessList.aspx?sid={2}';", msg, config.SiteUrl, parms);
                    break;
                case BusinessContentMode.GoToSetup:
                    script += string.Format("location.href='{1}/controlroom/ppon/setup.aspx?bid={2}';", msg, config.SiteUrl, parms);
                    break;
                case BusinessContentMode.Reload:
                    //script += string.Format("location.href='{1}/sal/BusinessContent.aspx?bid={2}&tab={3}';", msg, config.SiteUrl, Bid, parms);
                    script += "window.parent.$.fancybox.close();";
                    break;
                case BusinessContentMode.Redirect:
                    script += string.Format("location.href='{1}/sal/BusinessContent.aspx?bid={2}';", msg, config.SiteUrl, parms);
                    break;
                case BusinessContentMode.GoToProposal:
                    script += string.Format("location.href='{1}/sal/ProposalContent.aspx?pid={2}';", msg, config.SiteUrl, parms);
                    break;
                case BusinessContentMode.DataError:
                default:
                    break;
            }

            if (!string.IsNullOrWhiteSpace(script))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "alert", script, true);
            }
        }

        public void SetProvisionDropDownListData(IEnumerable<ProvisionDepartment> departments)
        {
            try
            {
                ddlDepartment.Items.Clear();
                ddlCategory.Items.Clear();
                ddlItem.Items.Clear();
                ddlContent.Items.Clear();
                lblDesc.Text = string.Empty;

                if (departments.Count() > 0)
                {
                    ddlDepartment.DataSource = departments;
                    ddlDepartment.DataBind();

                    ProvisionDepartment department = departments.Where(x => x.Id.Equals(DepartmentId)).FirstOrDefault();
                    if (department != null)
                    {
                        if (!DepartmentId.Equals(ObjectId.Empty))
                            ddlDepartment.SelectedValue = DepartmentId.ToString();

                        ddlCategory.DataSource = department.Categorys.OrderBy(x => x.Rank);
                        ddlCategory.DataBind();

                        ProvisionCategory category = department.Categorys.Where(x => x.Id.Equals(CategoryId)).FirstOrDefault();
                        if (category != null && !category.Id.Equals(ObjectId.Empty))
                        {
                            if (!CategoryId.Equals(ObjectId.Empty))
                                ddlCategory.SelectedValue = CategoryId.ToString();

                            ddlItem.DataSource = category.Items.OrderBy(x => x.Rank);
                            ddlItem.DataBind();

                            ProvisionItem item = category.Items.Where(x => x.Id.Equals(ItemId)).FirstOrDefault();
                            if (item != null && !item.Id.Equals(ObjectId.Empty))
                            {
                                if (!ItemId.Equals(ObjectId.Empty))
                                    ddlItem.SelectedValue = ItemId.ToString();

                                ddlContent.DataSource = item.Contents.OrderBy(x => x.Rank);
                                ddlContent.DataBind();

                                ProvisionContent content = item.Contents.Where(x => x.Id.Equals(ContentId)).FirstOrDefault();
                                if (content != null && !content.Id.Equals(ObjectId.Empty))
                                {
                                    if (!ContentId.Equals(ObjectId.Empty))
                                        ddlContent.SelectedValue = ContentId.ToString();

                                    ProvisionDesc desc = content.ProvisionDescs.FirstOrDefault();
                                    if (desc != null && !desc.Id.Equals(ObjectId.Empty))
                                    {
                                        lblDesc.Text = desc.Description;
                                        DescId = desc.Id;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("連線錯誤，使用者:" + UserName, ex);
            }
            
        }

        public void SetProvisionDropDownListModelData(IEnumerable<ProvisionDepartmentModel> departments)
        {
            try
            {
                ddlDepartment.Items.Clear();
                ddlCategory.Items.Clear();
                ddlItem.Items.Clear();
                ddlContent.Items.Clear();
                lblDesc.Text = string.Empty;

                if (departments.Count() > 0)
                {
                    ddlDepartment.DataSource = departments;
                    ddlDepartment.DataBind();

                    ProvisionDepartmentModel department = departments.Where(x => x.Id.Equals(DepartmentId.ToString())).FirstOrDefault();
                    if (department != null)
                    {
                        if (!DepartmentId.Equals(ObjectId.Empty.ToString()))
                            ddlDepartment.SelectedValue = DepartmentId.ToString();

                        ddlCategory.DataSource = department.Categorys.OrderBy(x => x.Rank);
                        ddlCategory.DataBind();

                        ProvisionCategoryModel category = department.Categorys.Where(x => x.Id.Equals(CategoryId.ToString())).FirstOrDefault();
                        if (category != null && !category.Id.Equals(ObjectId.Empty.ToString()))
                        {
                            if (!CategoryId.Equals(ObjectId.Empty.ToString()))
                                ddlCategory.SelectedValue = CategoryId.ToString();

                            ddlItem.DataSource = category.Items.OrderBy(x => x.Rank);
                            ddlItem.DataBind();

                            ProvisionItemModel item = category.Items.Where(x => x.Id.Equals(ItemId.ToString())).FirstOrDefault();
                            if (item != null && !item.Id.Equals(ObjectId.Empty.ToString()))
                            {
                                if (!ItemId.Equals(ObjectId.Empty.ToString()))
                                    ddlItem.SelectedValue = ItemId.ToString();

                                ddlContent.DataSource = item.Contents.OrderBy(x => x.Rank);
                                ddlContent.DataBind();

                                ProvisionContentModel content = item.Contents.Where(x => x.Id.Equals(ContentId.ToString())).FirstOrDefault();
                                if (content != null && !content.Id.Equals(ObjectId.Empty.ToString()))
                                {
                                    if (!ContentId.Equals(ObjectId.Empty.ToString()))
                                        ddlContent.SelectedValue = ContentId.ToString();

                                    ProvisionDescModel desc = content.ProvisionDescs.FirstOrDefault();
                                    if (desc != null && !desc.Id.Equals(ObjectId.Empty.ToString()))
                                    {
                                        lblDesc.Text = desc.Description;
                                        DescId = ObjectId.Parse(desc.Id);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("連線錯誤，使用者:" + UserName, ex);
            }

        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Presenter.OnViewInitialized();
                hidDepartmentId.Value = DepartmentId.ToString();
                hidCategoryId.Value = CategoryId.ToString();
                hidItemId.Value = ItemId.ToString();
                hidContentId.Value = ContentId.ToString();
                hidDescId.Value = DescId.ToString();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Save != null)
            {
                this.Save(this, new DataEventArgs<string>(ProposalFacade.RemoveSpecialCharacter(txtResult.Text.Trim(), true)));
            }
        }


        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            ObjectId departmentId;
            ObjectId.TryParse(ddlDepartment.SelectedValue, out departmentId);
            DepartmentId = departmentId;
            CategoryId = ItemId = ContentId = DescId = ObjectId.Empty;

            if (this.ProvisionSelectedIndexChanged != null)
                this.ProvisionSelectedIndexChanged(this, e);
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            ObjectId categoryId;
            ObjectId.TryParse(ddlCategory.SelectedValue, out categoryId);
            CategoryId = categoryId;
            ItemId = ContentId = DescId = ObjectId.Empty;

            if (this.ProvisionSelectedIndexChanged != null)
                this.ProvisionSelectedIndexChanged(this, e);
        }

        protected void ddlItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            ObjectId itemId;
            ObjectId.TryParse(ddlItem.SelectedValue, out itemId);
            ItemId = itemId;
            ContentId = DescId = ObjectId.Empty;

            if (this.ProvisionSelectedIndexChanged != null)
                this.ProvisionSelectedIndexChanged(this, e);
        }

        protected void ddlContent_SelectedIndexChanged(object sender, EventArgs e)
        {
            ObjectId contentId;
            ObjectId.TryParse(ddlContent.SelectedValue, out contentId);
            ContentId = contentId;
            DescId = ObjectId.Empty;

            if (this.ProvisionSelectedIndexChanged != null)
                this.ProvisionSelectedIndexChanged(this, e);
        }
        #endregion

        #region private method
        private void EnabledSettings(DealAccounting da, BusinessHour bh, Proposal pro, IEnumerable<ProvisionDepartment> departments, IEnumerable<ProvisionDepartmentModel> departmentsModel)
        {
            #region 瀏覽權限檢查 (無全區瀏覽權限，會判斷商家是否有綁定業務，有的話只能檢視自己的提案單)

            if (!CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReadAll))
            {
                ViewEmployee loginUser = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(UserName));

                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReadDept))
                {
                    #region 本區檢視

                    if (da != null)
                    {
                        if (loginUser.DeptId != da.DeptId)
                        {
                            Department dept = HumanFacade.GetSalesDepartment().Where(x => x.DeptId == da.DeptId).FirstOrDefault();
                            ShowMessage(string.Format("您歸屬的業務部門為 {0} ，無法檢視 {1} 的檔次資訊。", loginUser.DeptName, dept.DeptName), BusinessContentMode.BackToList);
                            return;
                        }
                    }

                    #endregion
                }
                else
                {
                    #region 無檢視權限(僅能查看負責業務是自己的檔次)

                    if (loginUser.IsLoaded)
                    {
                        if (da != null)
                        {
                            if (loginUser.EmpName != da.SalesId)
                            {
                                ShowMessage(string.Format("無法查看負責業務 {0} 的檔次資訊。", da.SalesId), BusinessContentMode.BackToList);
                                return;
                            }
                        }
                    }
                    else
                    {
                        ShowMessage("查無員工資料，請重新檢視員工資料維護設定。", BusinessContentMode.BackToList);
                        return;
                    }

                    #endregion
                }
            }

            #endregion

            if (pro.IsLoaded)
            {
                #region 更新權限檢查 (有提案單、已建檔確認、尚未設定確認、未上檔 的檔次才可編輯)
                if (bh != null)
                {
                    if (Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.Created) &&
                    !Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.SettingCheck) &&
                    bh.BusinessHourOrderTimeS > DateTime.Now &&
                    CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Update))
                    {
                        txtResult.Visible = true;
                        btnSave.Visible = true;
                    }
                    else
                    {
                        txtResult.Visible = false;
                        btnSave.Visible = false;
                    }
                }

                #endregion

                #region 鎖館別
                try
                {
                    if(departments != null)
                    {
                        ProvisionDepartment data = departments.FirstOrDefault();
                        if (pro.DealType == (int)ProposalDealType.Travel)
                        {
                            data = departments.Where(x => x.Name == "P旅遊").FirstOrDefault();
                            ddlDepartment.SelectedValue = data.Id.ToString();
                        }
                        else if (pro.DeliveryType == (int)DeliveryType.ToShop)
                        {
                            data = departments.Where(x => x.Name == "P好康").FirstOrDefault();
                            ddlDepartment.SelectedValue = data.Id.ToString();
                        }
                        else if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                        {
                            data = departments.Where(x => x.Name == "P商品").FirstOrDefault();
                            ddlDepartment.SelectedValue = data.Id.ToString();
                        }
                    }
                    else if(departmentsModel != null)
                    {
                        ProvisionDepartmentModel data = departmentsModel.FirstOrDefault();
                        if (pro.DealType == (int)ProposalDealType.Travel)
                        {
                            data = departmentsModel.Where(x => x.Name == "P旅遊").FirstOrDefault();
                            ddlDepartment.SelectedValue = data.Id.ToString();
                        }
                        else if (pro.DeliveryType == (int)DeliveryType.ToShop)
                        {
                            data = departmentsModel.Where(x => x.Name == "P好康").FirstOrDefault();
                            ddlDepartment.SelectedValue = data.Id.ToString();
                        }
                        else if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                        {
                            data = departmentsModel.Where(x => x.Name == "P商品").FirstOrDefault();
                            ddlDepartment.SelectedValue = data.Id.ToString();
                        }
                    }
                    

                    ddlDepartment.Enabled = false;
                }
                catch (Exception ex)
                {
                    logger.Error("連線錯誤，使用者:" + UserName, ex);
                }
                
                #endregion
                
            }
            else
            {
                txtResult.Visible = false;
                btnSave.Visible = false;
            }
        }
        #endregion
    }
}
﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Sal
{
    public partial class SalesCalendar : RolePage, ISalesCalendarView
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        #region props
        private SalesCalendarPresenter _presenter;
        public SalesCalendarPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }
        public string SaleGoogleCalendarAPIKey
        {
            get
            {
                return config.SaleGoogleCalendarAPIKey;
            }
        }
        public int CurrentPage
        {
            get
            {
                int pager = 0;
                int.TryParse(ddlPager.SelectedValue, out pager);
                return pager;
            }
        }
        #endregion

        #region event
        public event EventHandler<EventArgs> ChangePager;
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IntialControls();
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }
        protected void rptTaskList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is SalesCalendarEvent)
            {
                SalesCalendarEvent dataItem = (SalesCalendarEvent)e.Item.DataItem;

                Literal liEvent = (Literal)e.Item.FindControl("liEvent");
                TextBox liId = (TextBox)e.Item.FindControl("liId");
                liId.Text = dataItem.Id.ToString();
                HyperLink hypEvent = (HyperLink)e.Item.FindControl("hypEvent");
                HyperLink hypEventDel = (HyperLink)e.Item.FindControl("hypEventDel");
                liEvent.Text = "● " + dataItem.EventContent;
                hypEvent.NavigateUrl = "SalesCalendarContent.aspx?id=" + dataItem.Id ;
                hypEventDel.NavigateUrl = "javascript: void(0);" ;
            }
        }
        protected void ddlPager_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ChangePager != null)
            {
                this.ChangePager(this, e);
            }
        }
        #endregion

        #region method
        private void IntialControls()
        {
            // 類型
            txtUserName.Text = UserName.Replace(".tw", "");
        }
        public void SetCalendarContent(SalesCalendarEventCollection cals, int TotalCount)
        {
            rptTaskList.DataSource = cals;
            rptTaskList.DataBind();

            //頁數
            int CP = CurrentPage;
            int TotalPage = (TotalCount / 10);
            if((TotalCount % 10) > 0 && TotalCount != 0)
            {
                TotalPage += 1;
            }
            ddlPager.Items.Clear();
            if(TotalPage <= 1)
            {
                TotalPage = 1;
            }
            for (int t = 1; t <= TotalPage; t++)
            {
                ddlPager.Items.Add(t.ToString());
            }
            if (ddlPager.Items.Contains(new ListItem(CP.ToString())))
            {
                ddlPager.SelectedValue = CP.ToString();
            }
            if(TotalCount == 0)
            {
                ddlPager.Visible = false;
            }
            
        }
        #endregion

        #region webmethod
        [WebMethod]
        public static bool DeleteTask(int Id)
        {
            return ProposalFacade.SalesCalendarEventDelete(Id);
        }
        #endregion

        
    }
}
﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Sal
{
    public partial class ProposalMultiDeals : RolePage, ISalesProposalMultiDealsView
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        const int MAX_QTY = 99999;
        const int DAILY_AMOUNT = 20;

        #region props
        private SalesProposalMultiDealsPresenter _presenter;
        public SalesProposalMultiDealsPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public int ProposalId
        {
            get
            {
                int pid = 0;
                if (Request.QueryString["pid"] != null)
                {
                    int.TryParse(Request.QueryString["pid"].ToString(), out pid);
                }
                return pid;
            }
        }

        #endregion

        #region event
        public event EventHandler<DataEventArgs<int>> MultiSave;
        public event EventHandler<DataEventArgs<int>> MultiDelete;
        public event EventHandler<DataEventArgs<int>> Edit;
        #endregion

        #region method

        public void SetProposalMultiDeals(Proposal pro)
        {
            //divMultiEdit.Visible = false;
            //btnClose.Visible = false;
            //btnOpen.Visible = true;

            //hidDeliveryType.Value = pro.DeliveryType.ToString();
            //hkBackProposal.NavigateUrl = ResolveUrl(string.Format("~/sal/ProposalContent.aspx?pid={0}", pro.Id));

            //if (pro.DeliveryType == (int)DeliveryType.ToHouse)
            //{
            //    divMultiFreight.Visible = true;
            //}

            //List<ProposalMultiDealModel> multiDeals = new JsonSerializer().Deserialize<List<ProposalMultiDealModel>>(pro.MultiDeals);
            //if (multiDeals != null && multiDeals.Count > 0)
            //{
            //    divMultiDeals.Visible = true;
            //    rptMultiDeal.DataSource = multiDeals;
            //    rptMultiDeal.DataBind();
            //    rptMultiDeal.Visible = true;
            //}
            //else
            //{
            //    rptMultiDeal.Visible = false;
            //}

            //EnabledSettings(pro);
        }

        //public ProposalMultiDealModel GetProposalMultiDealModelData()
        //{
        //    ProposalMultiDealModel model = new ProposalMultiDealModel();
        //    int id = int.TryParse(hidMultiDealId.Value, out id) ? id : 0;
        //    model.Id = id;
        //    model.ItemName = txtItemName.Text;
        //    decimal origPrice = decimal.TryParse(txtOrigPrice.Text, out origPrice) ? origPrice : 0;
        //    model.OrigPrice = origPrice;
        //    decimal itemrice = decimal.TryParse(txtItemPrice.Text, out itemrice) ? itemrice : 0;
        //    model.ItemPrice = itemrice;
        //    decimal cost = decimal.TryParse(txtCost.Text, out cost) ? cost : 0;
        //    model.Cost = cost;
        //    int slottingFee = int.TryParse(txtSlottingFeeQuantity.Text, out slottingFee) ? slottingFee : 0;
        //    model.SlottingFeeQuantity = slottingFee;
        //    decimal orderTotalLimit = decimal.TryParse(txtOrderTotalLimit.Text, out orderTotalLimit) ? orderTotalLimit : 0;
        //    model.OrderTotalLimit = orderTotalLimit;
        //    int atm = int.TryParse(txtAtmMaximum.Text, out atm) ? atm : 0;
        //    model.AtmMaximum = atm;
        //    decimal freight = decimal.TryParse(txtFreight.Text, out freight) ? freight : 0;
        //    model.Freights = freight;
        //    decimal nofreight = decimal.TryParse(txtNonFreightLimit.Text, out nofreight) ? nofreight : 0;
        //    model.NoFreightLimit = nofreight;
        //    model.Options = txtOptions.Text;
        //    return model;
        //}

        //public void SetProposalMultiDealModel(ProposalMultiDealModel model)
        //{
        //    divMultiEdit.Visible = true;
        //    btnClose.Visible = true;
        //    btnOpen.Visible = false;
        //    btnDelete.Visible = true;
        //    hidMultiDealId.Value = model.Id.ToString();
        //    txtItemName.Text = model.ItemName;
        //    txtOrigPrice.Text = model.OrigPrice.ToString("F0");
        //    txtItemPrice.Text = model.ItemPrice.ToString("F0");
        //    txtCost.Text = model.Cost.ToString("F0");
        //    txtSlottingFeeQuantity.Text = model.SlottingFeeQuantity.ToString("F0");
        //    txtFreight.Text = model.Freights.ToString("F0");
        //    txtNonFreightLimit.Text = model.NoFreightLimit.ToString("F0");
        //    txtOrderTotalLimit.Text = model.OrderTotalLimit.ToString("F0");
        //    txtAtmMaximum.Text = model.AtmMaximum.ToString("F0");
        //    txtOptions.Text = model.Options;
        //}

        public void RedirectProposalMultiDeals(int pid)
        {
            Response.Redirect(ResolveUrl(string.Format("~/sal/ProposalMultiDeals.aspx?pid={0}", pid)));
        }

        public void ShowMessage(string msg, ProposalContentMode mode, string parms = "")
        {
            string script = string.Empty;
            switch (mode)
            {
                case ProposalContentMode.BackToList:
                    script = string.Format("alert('{0}'); location.href='{1}/sal/ProposalList.aspx';", msg, config.SiteUrl);
                    break;
                case ProposalContentMode.DataError:
                    script = string.Format("alert('{0}');", msg);
                    break;
                default:
                    break;
            }

            if (!string.IsNullOrWhiteSpace(script))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "alert", script, true);
            }
        }

        #endregion

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.MultiSave != null)
            {
                int id = int.TryParse(hidMultiDealId.Value, out id) ? id : 0;
                this.MultiSave(this, new DataEventArgs<int>(id));
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.MultiDelete != null)
            {
                int id = int.TryParse(hidMultiDealId.Value, out id) ? id : 0;
                this.MultiDelete(this, new DataEventArgs<int>(id));
            }
        }

        protected void btnOpen_Click(object sender, EventArgs e)
        {
            divMultiEdit.Visible = true;
            btnClose.Visible = true;
            btnOpen.Visible = false;
            btnDelete.Visible = false;

            hidMultiDealId.Value = string.Empty;
            txtItemName.Text = string.Empty;
            txtOrigPrice.Text = string.Empty;
            txtItemPrice.Text = string.Empty;
            txtCost.Text = string.Empty;
            txtSlottingFeeQuantity.Text = string.Empty;
            txtFreight.Text = string.Empty;
            txtNonFreightLimit.Text = string.Empty;
            txtOrderTotalLimit.Text = string.Empty;
            txtAtmMaximum.Text = string.Empty;
            txtOptions.Text = string.Empty;
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            divMultiEdit.Visible = false;
            btnClose.Visible = false;
            btnOpen.Visible = true;
        }

        #endregion

        #region private method

        private void EnabledSettings(Proposal pro)
        {
            #region 瀏覽權限檢查 (無全區瀏覽權限，會判斷商家是否有綁定業務，有的話只能檢視自己的提案單)

            if (!CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReadAll))
            {
                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReadDept))
                {
                    #region 本區檢視

                    ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(UserName));
                    if (emp.IsLoaded)
                    {
                        ViewEmployee proEmp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
                        if (proEmp.IsLoaded)
                        {
                            if (emp.DeptId != proEmp.DeptId)
                            {
                                ShowMessage(string.Format("您歸屬的業務部門為 {0} ，無法檢視 {1} 的提案單。", emp.DeptName, proEmp.DeptName), ProposalContentMode.BackToList);
                                return;
                            }
                        }
                    }
                    else
                    {
                        ShowMessage("查無員工資料，請重新檢視員工資料維護設定。", ProposalContentMode.DataError);
                        return;
                    }

                    #endregion
                }

                else
                {
                    #region 無檢視權限(僅能查看自己建立的單)

                    ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
                    if (emp.IsLoaded && emp.Email != UserName)
                    {
                        ShowMessage(string.Format("無法查看負責業務 {0} 的提案單資訊。", emp.EmpName), ProposalContentMode.BackToList);
                        return;
                    }

                    #endregion
                }
            }

            #endregion

            if (pro.IsLoaded)
            {
                #region 更新權限檢查 (尚未提案申請、非快速上檔的提案單，才可變更資料)

                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Update))
                {
                    if (!Helper.IsFlagSet(pro.ApplyFlag, ProposalApplyFlag.Apply) && pro.CopyType != (int)ProposalCopyType.Same)
                    {
                        EnableControls(true, true);
                    }
                    else
                    {
                        // 尚未設定確認的提案單，可以變更多重選項
                        if (!Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.SettingCheck))
                        {
                            EnableControls(false, true);
                        }
                        else
                        {
                            EnableControls(false, false);
                        }
                    }
                }
                else
                {
                    EnableControls(false, false);
                }

                #endregion
            }
        }

        private void EnableControls(bool enabled, bool option_enabled)
        {
            divOpen.Visible = enabled;
            divDelete.Visible = enabled;
            txtItemName.Enabled = enabled;
            txtOrigPrice.Enabled = enabled;
            txtItemPrice.Enabled = enabled;
            txtCost.Enabled = enabled;
            txtSlottingFeeQuantity.Enabled = enabled;
            txtFreight.Enabled = enabled;
            txtNonFreightLimit.Enabled = enabled;
            txtOrderTotalLimit.Enabled = enabled;
            txtAtmMaximum.Enabled = enabled;
            txtOptions.Enabled = option_enabled;

            btnOpen.Visible = enabled;
            btnDelete.Visible = enabled;
            btnSave.Visible = enabled | option_enabled;
        }

        #endregion

        protected void rptMultiDeal_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "UPD")
            {
                if (this.Edit != null)
                {
                    this.Edit(sender, new DataEventArgs<int>(int.Parse(e.CommandArgument.ToString())));
                }
            }
        }
    }
}
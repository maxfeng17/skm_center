﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.BizLogic.Model.SellerSales;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Sal
{
    public partial class BusinessContent : RolePage, ISalesBusinessContentView
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        public IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        const int MAX_QTY = 99999;
        const int DAILY_AMOUNT = 20;

        #region props
        private SalesBusinessContentPresenter _presenter;
        public SalesBusinessContentPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public Guid BusinessHourGuid
        {
            get
            {
                Guid bid = Guid.Empty;
                if (Request.QueryString["bid"] != null)
                {
                    Guid.TryParse(Request.QueryString["bid"].ToString(), out bid);
                }
                return bid;
            }
        }

        public string Tab
        {
            get
            {
                if (Request.QueryString["tab"] != null)
                {
                    return Request.QueryString["tab"].ToString();
                }
                return string.Empty;
            }
        }

        private bool _IsInputTaxRequired;
        public bool IsInputTaxRequired
        {
            get
            {
                return _IsInputTaxRequired;
            }
            set
            {
                _IsInputTaxRequired = value;
            }
        }

        public int ProposalId
        {
            get
            {
                return Convert.ToInt32(hkProposalId.Text);
            }
        }

        private Guid _SellerGuid;
        public Guid SellerGuid
        {
            get
            {
                return _SellerGuid;
            }
            set
            {
                _SellerGuid = value;
            }
        }


        /// <summary>
        /// 檔次是否已開賣
        /// </summary>
        public bool IsDealOpened { get; set; }

        public bool IsConsignment
        {
            get
            {
                return config.IsConsignment;
            }
        }

        public string ContentDescription
        {
            get { return tbEditorDesc.Text; }
            set
            {
                tbEditorDesc.Text = value;
            }
        }

        public string ContentRemark
        {
            get { return tbEditorRemark.Text; }
            set
            {
                tbEditorRemark.Text = value;
            }
        }



        /// <summary>
        /// 商家出帳方式
        /// </summary>
        private int? _SellerRemittanceType;
        public int? SellerRemittanceType
        {
            get
            {
                return _SellerRemittanceType;
            }
            set
            {
                _SellerRemittanceType = value;
            }
        }

        /// <summary>
        /// 出貨方式
        /// </summary>
        private int? _ShipType;
        public int? ShipType
        {
            get
            {
                return _ShipType;
            }
            set
            {
                _ShipType = value;
            }
        }

        #endregion

        #region event
        public event EventHandler<EventArgs> Save;
        public event EventHandler<DataEventArgs<int>> VbsSave;
        public event EventHandler<DataEventArgs<KeyValuePair<Guid, ProposalCopyType>>> ReCreateProposal;
        public event EventHandler<EventArgs> PponStoreSave;
        public event EventHandler<DataEventArgs<Guid>> SettingCheck;
        public event EventHandler<DataEventArgs<KeyValuePair<string, SellerSalesType>>> Referral;
        public event EventHandler<DataEventArgs<string>> ChangeLog;
        #endregion

        #region method

        public void SetAccField(AccBusinessGroupCollection acc, DepartmentCollection depts)
        {
            ddlAccBusinessGroup.DataSource = acc;
            ddlAccBusinessGroup.DataTextField = "AccBusinessGroupName";
            ddlAccBusinessGroup.DataValueField = "AccBusinessGroupId";
            ddlAccBusinessGroup.DataBind();

            ddlDeptId.DataSource = depts;
            ddlDeptId.DataTextField = "DeptName";
            ddlDeptId.DataValueField = "DeptId";
            ddlDeptId.DataBind();
        }

        public void SetBusinessContent(SalesBusinessModel model, ViewComboDealCollection combo, SellerCollection tscs, CategoryDealCollection cdc, PponStoreCollection psc, Proposal pro, ViewEmployee emp, BusinessChangeLogCollection logs)
        {
            // 標題
            string itemName = model.ItemProperty.ItemName;
            if (model.ItemProperty.ItemName.Length > 50)
            {
                itemName = model.ItemProperty.ItemName.Substring(0, 50) + "...";
            }
            liItemName.Text = itemName;

            #region 商家資訊

            liSellerName.Text = model.SellerProperty.SellerName;
            _SellerGuid = pro.SellerGuid;
            _SellerRemittanceType = model.SellerProperty.RemittanceType;
            _ShipType = model.Property.ShipType;
            hkSellerId.Text = model.SellerProperty.SellerId;
            hkSellerId.NavigateUrl = ResolveUrl("~/sal/SellerContent.aspx?sid=" + model.SellerProperty.Guid);

            #endregion

            #region 提案單

            if (pro.Id != 0)
            {
                divProposalInfo.Visible = true;
                hidProposalDealType.Value = pro.DealType.ToString();

                hkProposalId.Text = pro.Id.ToString();
                hkProposalId.NavigateUrl = ResolveUrl("~/sal/ProposalContent.aspx?pid=" + pro.Id);

                hidNoRestrictedStore.Value = model.Business.BusinessHourStatus.ToString();

            }

            #endregion

            #region 串接資訊

            if (model.Business.BusinessHourOrderTimeS.Date != DateTime.MaxValue.Date)
            {
                liBusinessOrderTime.Text = string.Format("{0} ~ {1}", model.Business.BusinessHourOrderTimeS.ToString("yyyy/MM/dd HH:mm"), model.Business.BusinessHourOrderTimeE.ToString("yyyy/MM/dd HH:mm"));
            }
            else
            {
                liBusinessOrderTime.Text = "未排檔";
            }

            if (DateTime.Now > model.Business.BusinessHourOrderTimeS)
            {
                IsDealOpened = true;
            }

            if (pro.ReferrerBusinessHourGuid.HasValue)
            {
                divReferrerBusinessHour.Visible = true;
                hkReferrerBusinessHour.Text = pro.ReferrerBusinessHourGuid.Value.ToString();
                hkReferrerBusinessHour.NavigateUrl = ResolveUrl("~/sal/BusinessContent.aspx?bid=" + pro.ReferrerBusinessHourGuid);
            }
            hkSetUp.NavigateUrl = ResolveUrl("~/controlroom/ppon/setup.aspx?bid=" + model.Business.Guid);
            hkPreview.NavigateUrl = ResolveUrl("~/ppon/default.aspx?bid=" + model.Business.Guid);

            #endregion

            #region 負責業務

            if (emp.IsLoaded)
            {
                divSalesInfo.Visible = true;
                ProposalSalesModel proposalSalesModel = ProposalFacade.DealGetSalesByBid(pro.BusinessHourGuid ?? Guid.Empty);
                liDevelopeSalesName.Text = proposalSalesModel.DevelopeSalesEmpName;
                liDevelopeSalesEmail.Text = proposalSalesModel.DevelopeSalesEmail;

                liOperationSalesName.Text = proposalSalesModel.OperationSalesEmpName;
                liOperationSalesEmail.Text = proposalSalesModel.OperationSalesEmail;
            }

            #endregion

            #region 上檔頻道

            if (cdc.Count > 0)
            {
                foreach (RepeaterItem channelItem in rptChannel.Items)
                {
                    HiddenField hidChannel = (HiddenField)channelItem.FindControl("hidChannel");
                    CheckBox chkChannel = (CheckBox)channelItem.FindControl("chkChannel");
                    int channel = int.TryParse(hidChannel.Value, out channel) ? channel : 0;
                    if (channel != 0)
                    {
                        if (cdc.Any(x => x.Cid == channel))
                        {
                            chkChannel.Checked = true;

                            // 區域
                            Repeater rptArea = (Repeater)channelItem.FindControl("rptArea");
                            foreach (RepeaterItem areaItem in rptArea.Items)
                            {
                                HiddenField hidArea = (HiddenField)areaItem.FindControl("hidArea");
                                CheckBox chkArea = (CheckBox)areaItem.FindControl("chkArea");
                                int area = int.TryParse(hidArea.Value, out area) ? area : 0;
                                if (area != 0)
                                {
                                    if (cdc.Any(x => x.Cid == area))
                                    {
                                        chkArea.Checked = true;

                                    }
                                }
                            }

                            Repeater rptArea2 = (Repeater)channelItem.FindControl("rptArea2");
                            foreach (RepeaterItem areaItem in rptArea2.Items)
                            {
                                // 商圈
                                Repeater rptCommercial = (Repeater)areaItem.FindControl("rptCommercial");
                                foreach (RepeaterItem commItem in rptCommercial.Items)
                                {
                                    HiddenField hidCommercial = (HiddenField)commItem.FindControl("hidCommercial");
                                    CheckBox chkCommercial = (CheckBox)commItem.FindControl("chkCommercial");
                                    int id = int.TryParse(hidCommercial.Value, out id) ? id : 0;
                                    if (id != 0)
                                    {
                                        if (cdc.Any(x => x.Cid == id))
                                        {
                                            chkCommercial.Checked = true;
                                        }
                                    }
                                }

                                //旅遊-商圈‧景點
                                Repeater rptSPRegion = (Repeater)areaItem.FindControl("rptSPRegion");
                                foreach (RepeaterItem spregion in rptSPRegion.Items)
                                {
                                    Repeater rptSpecialRegion = (Repeater)spregion.FindControl("rptSpecialRegion");
                                    foreach (RepeaterItem specialregion in rptSpecialRegion.Items)
                                    {
                                        HiddenField hidSpecialRegion = (HiddenField)specialregion.FindControl("hidSpecialRegion");
                                        CheckBox chkSpecialRegion = (CheckBox)specialregion.FindControl("chkSpecialRegion");
                                        int id = int.TryParse(hidSpecialRegion.Value, out id) ? id : 0;
                                        if (id != 0)
                                        {
                                            if (cdc.Any(x => x.Cid == id))
                                            {
                                                chkSpecialRegion.Checked = true;
                                            }
                                        }
                                    }
                                }
                            }

                            // 分類
                            Repeater rptCategory = (Repeater)channelItem.FindControl("rptCategory");
                            foreach (RepeaterItem areaItem in rptCategory.Items)
                            {
                                HiddenField hidCategory = (HiddenField)areaItem.FindControl("hidCategory");
                                CheckBox chkCategory = (CheckBox)areaItem.FindControl("chkCategory");
                                int id = int.TryParse(hidCategory.Value, out id) ? id : 0;
                                if (id != 0)
                                {
                                    if (cdc.Any(x => x.Cid == id))
                                    {
                                        chkCategory.Checked = true;
                                    }
                                }


                                // 子分類
                                Repeater rptSubDealCategoryArea = (Repeater)areaItem.FindControl("rptSubDealCategoryArea");
                                foreach (RepeaterItem subDealItem in rptSubDealCategoryArea.Items)
                                {
                                    HiddenField hidSubDealCategory = (HiddenField)subDealItem.FindControl("hidSubDealCategory");
                                    CheckBox chkSubDealCategory = (CheckBox)subDealItem.FindControl("chkSubDealCategory");
                                    int subid = int.TryParse(hidSubDealCategory.Value, out subid) ? subid : 0;
                                    if (subid != 0)
                                    {
                                        if (cdc.Any(x => x.Cid == subid))
                                        {
                                            chkSubDealCategory.Checked = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            #endregion

            #region 優惠內容

            // 優惠活動
            if (model.Property.DeliveryType.HasValue)
            {
                hidDeliveryType.Value = model.Property.DeliveryType.Value.ToString();
                liDelivery.Text = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (DeliveryType)model.Property.DeliveryType);
                txtItemName.Text = model.ContentProperty.Name;
            }

            // 原價
            txtOrigPrice.Text = model.ItemProperty.ItemOrigPrice.ToString("F0");

            // 售價
            txtItemPrice.Text = model.ItemProperty.ItemPrice.ToString("F0");



            // 進貨價
            DealCost dc = model.Costs.Where(x => x.Cost > 0).FirstOrDefault();
            if (dc != null && dc.Cost.HasValue)
            {
                txtCost.Text = dc.Cost.Value.ToString("F0");
            }
            else
            {
                txtCost.Text = "0";
            }

            //毛利率
            hidGrossMargin.Value = ((double)PponFacade.GetMinimumGrossMarginFromBids(pro.BusinessHourGuid.Value, true) * 100).ToString();

            // 上架費(份數)
            if (model.Combodeals.Count > 0)
            {
                //多檔次
                DealCost sldc = model.Costs.Where(x => x.Cost == 0).FirstOrDefault();
                if (sldc != null && sldc.Quantity.HasValue)
                {
                    txtSlottingFeeQuantity.Text = sldc.Quantity.Value.ToString("F0");
                }
                else
                {
                    txtSlottingFeeQuantity.Text = "0";
                }
            }
            else
            {
                //單檔次
                ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealGetByPid(pro.Id);
                if (multiDeals != null && multiDeals.Count > 0)
                {
                    ProposalMultiDeal deal = multiDeals.OrderBy(x => x.ItemPrice).FirstOrDefault();
                    txtSlottingFeeQuantity.Text = deal.SlottingFeeQuantity.ToString("F0");
                }
                else
                {
                    txtSlottingFeeQuantity.Text = "0";
                }
            }



            // 運費
            if (model.Property.DeliveryType == (int)DeliveryType.ToHouse)
            {
                // 運費
                CouponFreight f = model.Freight.Where(x => x.StartAmount == 0).FirstOrDefault();
                if (f != null)
                {
                    txtFreight.Text = f.FreightAmount.ToString("F0");
                }

                // 免運門檻
                CouponFreight nf = model.Freight.Where(x => x.FreightAmount == 0).FirstOrDefault();
                if (nf != null)
                {
                    txtNonFreightLimit.Text = nf.StartAmount.ToString("F0");
                }

                divFreight.Visible = true;
            }

            //成套買送份數
            if (Helper.IsFlagSet(model.Business.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon))
            {
                hidCPCBuy.Value = (model.Property.SaleMultipleBase - model.Property.PresentQuantity).ToString();
                hidCPCPresent.Value = model.Property.PresentQuantity.ToString();
            }


            #endregion

            #region 詳細說明

            this.ContentDescription = model.ContentProperty.Description;
            this.ContentRemark = PponDealHelper.GetDealContentRemark(model.ContentProperty.Remark);

            #endregion

            #region 活動人數及限制

            txtOrderTotalLimit.Text = model.Business.OrderTotalLimit.Value.ToString("F0");
            txtAtmMaximum.Text = model.Business.BusinessHourAtmMaximum.ToString("F0");
            txtDailyAmount.Text = model.ItemProperty.ItemDefaultDailyAmount.Value.ToString("F0");

            #endregion

            #region 銷售分析

            // 館別
            if (model.Property.DealAccBusinessGroupId.HasValue)
            {
                ddlAccBusinessGroup.SelectedValue = model.Property.DealAccBusinessGroupId.Value.ToString();
            }

            // 業績歸屬
            if (ddlDeptId.Items.FindByValue(model.Accounting.DeptId) != null)
            {
                ddlDeptId.SelectedValue = model.Accounting.DeptId;
            }

            // 分類
            if (model.Property.DealType.HasValue)
            {
                hidDealTypeSub.Value = model.Property.DealType.Value.ToString();
                hidDealType.Value = model.Property.DealType.Value.ToString();
                SystemCodeCollection scc = SystemCodeManager.GetDealType();
                if (scc.Any(x => x.CodeId == model.Property.DealType))
                {
                    SystemCode code = scc.Where(x => x.CodeId == model.Property.DealType).FirstOrDefault();
                    if (code != null && code.ParentCodeId.HasValue)
                    {
                        ddlDealType.SelectedValue = code.ParentCodeId.ToString();
                    }
                }
            }

            // 搜尋關鍵字
            txtKeyword.Text = model.Business.PicAlt;

            #endregion

            #region 核銷方式
            rdlOnlineUse.Checked = (model.Property.VerifyType == (int)PponVerifyType.OnlineUse ? true : false);
            rdlOnlineOnly.Checked = (model.Property.VerifyType == (int)PponVerifyType.OnlineOnly ? true : false);
            #endregion

            #region 核銷與帳務設定
            // 墨攻 為旅遊類時才顯示按鈕
            if (pro.DealType == (int)ProposalDealType.Travel)
            {
                chkMohistPanel.Visible = true;
                // 初始化，與後臺資料連動
                chkMohist.Checked = model.Accounting.VendorBillingModel == (int)VendorBillingModel.MohistSystem;
            }
            else
            {
                chkMohistPanel.Visible = false;
                chkMohist.Checked = false;
            }

            //出帳方式
            

            if (config.IsRemittanceFortnightly && Convert.ToBoolean(hidRemittanceType.Value))
            {
                if (model.Accounting.RemittanceType == (int)RemittanceType.AchMonthly ||
                    model.Accounting.RemittanceType == (int)RemittanceType.AchWeekly ||
                    model.Accounting.RemittanceType == (int)RemittanceType.ManualMonthly ||
                    model.Accounting.RemittanceType == (int)RemittanceType.ManualWeekly ||
                    model.Accounting.RemittanceType == (int)RemittanceType.ManualPartially)
                {
                    //舊的出帳方式已被移除不能選擇，但為了顯示舊的檔次(舊的出帳方式)，所以要加回來，才能在畫面上顯示
                    ListItem dynamicAddItem = new ListItem(Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (RemittanceType)model.Accounting.RemittanceType), model.Accounting.RemittanceType.ToString());
                    radioRemittanceType.Items.Add(dynamicAddItem);
                }
                if (radioRemittanceType.Items.FindByValue(model.Accounting.RemittanceType.ToString()) != null)
                {
                    radioRemittanceType.SelectedValue = model.Accounting.RemittanceType.ToString();
                }
            }
            else
            {
                if (model.Accounting.RemittanceType == (int)RemittanceType.AchMonthly ||
                                model.Accounting.RemittanceType == (int)RemittanceType.AchWeekly ||
                                model.Accounting.RemittanceType == (int)RemittanceType.ManualMonthly ||
                                model.Accounting.RemittanceType == (int)RemittanceType.ManualWeekly ||
                                model.Accounting.RemittanceType == (int)RemittanceType.ManualPartially)
                {
                    //舊的出帳方式已被移除不能選擇，但為了顯示舊的檔次(舊的出帳方式)，所以要加回來，才能在畫面上顯示
                    ListItem dynamicAddItem = new ListItem(Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (RemittanceType)model.Accounting.RemittanceType), model.Accounting.RemittanceType.ToString());
                    ddlRemittanceType.Items.Add(dynamicAddItem);
                }
                if (ddlRemittanceType.Items.FindByValue(model.Accounting.RemittanceType.ToString()) != null)
                {
                    ddlRemittanceType.SelectedValue = model.Accounting.RemittanceType.ToString();
                }
            }

            ddlVendorReceiptType.SelectedValue = model.Accounting.VendorReceiptType.ToString();
            if (model.Accounting.VendorReceiptType == (int)VendorReceiptType.Other)
            {
                txtVendorReceiptMessage.Text = model.Accounting.Message;
            }
            chkInputTax.Checked = !model.Accounting.IsInputTaxRequired;
            IsInputTaxRequired = !model.Accounting.IsInputTaxRequired;

            if (model.Group.Status.HasValue)
            {
                chkNoTax.Checked = Helper.IsFlagSet(model.Group.Status.Value, GroupOrderStatus.NoTax);
            }

            #endregion

            #region 權益說明

            int _DeliveryType = (int)DeliveryType.ToShop;
            switch (model.Property.DeliveryType)
            {
                case (int)DeliveryType.ToHouse:
                    _DeliveryType = (int)ProvisionDepartmentType.Product;
                    break;
                case (int)DeliveryType.ToShop:
                    if (pro.DealType == (int)ProposalDealType.Travel)
                    {
                        _DeliveryType = (int)ProvisionDepartmentType.Travel;
                    }
                    else
                    {
                        _DeliveryType = (int)ProvisionDepartmentType.Ppon;
                    }
                    break;
                default:
                    _DeliveryType = (int)ProvisionDepartmentType.Ppon;
                    break;
            }

            hkRestrictionsEdit.NavigateUrl = ResolveUrl("~/sal/RestrictionsGenerator.aspx?bid=" + model.Business.Guid + "&type=" + _DeliveryType.ToString());
            if (model.ContentProperty.Restrictions != null)
            {
                liRestrictions.Text = model.ContentProperty.Restrictions.Replace("\n", "<br />");
            }

            #endregion

            #region 檔次續接設定

            if (model.Property.AncestorBusinessHourGuid.HasValue)
            {
                txtAncestorBid.Text = model.Property.AncestorBusinessHourGuid.Value.ToString();
            }
            else
            {
                if (pro.AncestorBusinessHourGuid.HasValue)
                {
                    txtAncestorBid.Text = pro.AncestorBusinessHourGuid.Value.ToString();
                }
            }
            liAncestorBidQty.Text = model.Property.ContinuedQuantity.ToString();
            if (model.Property.AncestorSequenceBusinessHourGuid.HasValue)
            {
                txtAncestorSeqBid.Text = model.Property.AncestorSequenceBusinessHourGuid.Value.ToString();
            }
            else
            {
                if (pro.AncestorSequenceBusinessHourGuid.HasValue)
                {
                    txtAncestorSeqBid.Text = pro.AncestorSequenceBusinessHourGuid.Value.ToString();
                }
            }

            #endregion

            #region 多檔次設定

            if (model.Combodeals.Count > 0)
            {
                aMultiDeals.Visible = true;
                divMultiDeals.Visible = true;
                rptMultiDeal.DataSource = model.Combodeals;
                rptMultiDeal.DataBind();
                rptMultiDeal.Visible = true;
            }
            else
            {
                rptMultiDeal.Visible = false;
            }

            #endregion

            #region 分店設定
            if (tscs.Count() > 0)
            {
                if (model.Property.DeliveryType == (int)DeliveryType.ToShop || Convert.ToInt32(hidProposalDealType.Value) == (int)ProposalDealType.Travel)
                {
                    aPponStore.Visible = true;
                    divPponStore.Visible = true;
                    rptPponStore.DataSource = tscs.OrderBy(x => x.CreateTime).ToDictionary(x => x, y => psc.Where(s => s.BusinessHourGuid == model.Business.Guid && s.StoreGuid == y.Guid).FirstOrDefault());
                    rptPponStore.DataBind();
                }

                //核銷頁簽都要顯示
                rptVbs.DataSource = tscs.OrderBy(x => x.CreateTime).ToDictionary(x => x, y => psc.Where(s => s.BusinessHourGuid == model.Business.Guid && s.StoreGuid == y.Guid).FirstOrDefault());
                rptVbs.DataBind();

            }

            #endregion

            #region 變更紀錄

            rptLog.DataSource = logs.OrderByDescending(x => x.CreateTime);
            rptLog.DataBind();

            #endregion

            EnabledSettings(model, pro);
        }

        public SalesBusinessModel GetSalesBusinessModelData(SalesBusinessModel model)
        {
            #region 活動人數及限制

            // 最大購買數量
            int maxQty = int.TryParse(txtOrderTotalLimit.Text, out maxQty) ? maxQty : MAX_QTY;
            model.Business.OrderTotalLimit = maxQty;

            // ATM保留份數
            int atmQty = int.TryParse(txtAtmMaximum.Text, out atmQty) ? atmQty : MAX_QTY;
            model.Business.BusinessHourAtmMaximum = atmQty;

            // 每人限購
            int dailyAmount = int.TryParse(txtDailyAmount.Text, out dailyAmount) ? dailyAmount : DAILY_AMOUNT;
            model.ItemProperty.ItemDefaultDailyAmount = model.ItemProperty.MaxItemCount = dailyAmount;

            #endregion

            #region 優惠內容

            // 優惠活動
            model.ContentProperty.Name = txtItemName.Text;

            // 原價
            decimal origPrice = decimal.TryParse(txtOrigPrice.Text, out origPrice) ? origPrice : 0;
            model.ItemProperty.ItemOrigPrice = origPrice;

            // 售價
            decimal itemPrice = decimal.TryParse(txtItemPrice.Text, out itemPrice) ? itemPrice : 0;
            model.ItemProperty.ItemPrice = itemPrice;

            // 進貨價/上架費
            model.Costs.Clear();
            decimal cost = decimal.TryParse(txtCost.Text, out cost) ? cost : 0;
            int qty = int.TryParse(txtSlottingFeeQuantity.Text, out qty) ? qty : 0;
            if (cost > 0)
            {
                if (qty > 0)
                {
                    model.Costs.Add(new DealCost() { BusinessHourGuid = model.Business.Guid, Cost = 0, Quantity = qty, CumulativeQuantity = qty, LowerCumulativeQuantity = 0 });
                    model.Costs.Add(new DealCost() { BusinessHourGuid = model.Business.Guid, Cost = cost, Quantity = maxQty - qty, CumulativeQuantity = maxQty, LowerCumulativeQuantity = qty });
                }
                else
                {
                    model.Costs.Add(new DealCost() { BusinessHourGuid = model.Business.Guid, Cost = cost, Quantity = maxQty, CumulativeQuantity = maxQty, LowerCumulativeQuantity = 0 });
                }
            }

            // 運費/免運門檻
            model.Freight.Clear();
            decimal f = decimal.TryParse(txtFreight.Text, out f) ? f : 0;
            decimal nf = decimal.TryParse(txtNonFreightLimit.Text, out nf) ? nf : 0;
            if (f > 0)
            {
                if (nf > 0)
                {
                    model.Freight.Add(new CouponFreight() { BusinessHourGuid = model.Business.Guid, StartAmount = nf, EndAmount = 9999999, FreightAmount = 0, CreateId = UserName, CreateTime = DateTime.Now });
                    model.Freight.Add(new CouponFreight() { BusinessHourGuid = model.Business.Guid, StartAmount = 0, EndAmount = nf, FreightAmount = f, CreateId = UserName, CreateTime = DateTime.Now });
                }
                else
                {
                    model.Freight.Add(new CouponFreight() { BusinessHourGuid = model.Business.Guid, StartAmount = 0, EndAmount = 9999999, FreightAmount = f, CreateId = UserName, CreateTime = DateTime.Now });
                }
            }

            #endregion

            #region 核銷方式
            if (model.Property.DeliveryType == (int)DeliveryType.ToShop)
            {
                int VerifyType = 0;
                if (rdlOnlineUse.Checked)
                {
                    VerifyType = (int)PponVerifyType.OnlineUse;
                }
                else
                {
                    VerifyType = (int)PponVerifyType.OnlineOnly;
                }
                model.Property.VerifyType = VerifyType;
            }
            #endregion

            #region 銷售分析

            // 館別
            int accId = int.TryParse(ddlAccBusinessGroup.SelectedValue, out accId) ? accId : 0;
            model.Property.DealAccBusinessGroupId = accId;

            // 業績歸屬
            model.Accounting.DeptId = ddlDeptId.SelectedValue;

            // 分類
            int dealType = int.TryParse(hidDealTypeSub.Value, out dealType) ? dealType : 0;
            model.Property.DealType = dealType;

            // 搜尋關鍵字
            model.Business.PicAlt = txtKeyword.Text.Trim().TrimEnd('/');

            #endregion

            #region 商品寄倉
            if (config.IsConsignment)
            {
                //會影響到下方的出帳方式
                model.Property.Consignment = chkConsignment.Checked;
            }
            #endregion

            #region 核銷與帳務設定 
            // 墨攻 連動後台的墨攻系統選項
            model.Pro.Mohist = chkMohist.Checked;

            model.Accounting.VendorBillingModel = (bool)model.Pro.Mohist ? (int)VendorBillingModel.MohistSystem : (int)VendorBillingModel.BalanceSheetSystem;
            // 墨攻默認為信陽銀行
            model.Business.BusinessHourStatus = (bool)model.Pro.Mohist ? Helper.SetBusinessHourTrustProvider(model.Business.BusinessHourStatus, TrustProvider.Sunny) : Helper.SetBusinessHourTrustProvider(model.Business.BusinessHourStatus, TrustProvider.TaiShin);
            


            if (config.IsRemittanceFortnightly && Convert.ToBoolean(hidRemittanceType.Value))
            {
                if (config.IsConsignment)
                {
                    // 出帳方式
                    RemittanceType rType = model.Property.Consignment ? RemittanceType.Monthly : (bool)model.Pro.Mohist ? RemittanceType.Others : RemittanceType.TryParse(radioRemittanceType.SelectedValue, out rType) ? rType :
                        (model.Property.DeliveryType == (int)DeliveryType.ToHouse ? RemittanceType.ManualPartially : RemittanceType.AchWeekly);

                    model.Accounting.RemittanceType = (int)rType;
                }
                else
                {
                    // 出帳方式
                    RemittanceType rType = (bool)model.Pro.Mohist ? RemittanceType.Others : RemittanceType.TryParse(radioRemittanceType.SelectedValue, out rType) ? rType :
                        (model.Property.DeliveryType == (int)DeliveryType.ToHouse ? RemittanceType.ManualPartially : RemittanceType.AchWeekly);

                    model.Accounting.RemittanceType = (int)rType;
                }
            }
            else
            {
                if (config.IsConsignment)
                {
                    // 出帳方式
                    RemittanceType rType = model.Property.Consignment ? RemittanceType.Monthly : (bool)model.Pro.Mohist ? RemittanceType.Others : RemittanceType.TryParse(ddlRemittanceType.SelectedValue, out rType) ? rType :
                        (model.Property.DeliveryType == (int)DeliveryType.ToHouse ? RemittanceType.ManualPartially : RemittanceType.AchWeekly);

                    model.Accounting.RemittanceType = (int)rType;
                }
                else
                {
                    // 出帳方式
                    RemittanceType rType = (bool)model.Pro.Mohist ? RemittanceType.Others : RemittanceType.TryParse(ddlRemittanceType.SelectedValue, out rType) ? rType :
                        (model.Property.DeliveryType == (int)DeliveryType.ToHouse ? RemittanceType.ManualPartially : RemittanceType.AchWeekly);

                    model.Accounting.RemittanceType = (int)rType;
                }
            }


            // 店家單據開立方式
            VendorReceiptType vrType = VendorReceiptType.TryParse(ddlVendorReceiptType.SelectedValue, out vrType) ? vrType : VendorReceiptType.Invoice;
            model.Accounting.VendorReceiptType = (int)vrType;
            if (vrType == VendorReceiptType.Other)
            {
                model.Accounting.Message = txtVendorReceiptMessage.Text.Trim();
            }
            else
            {
                model.Accounting.Message = string.Empty;
            }

            // 免稅設定
            model.Accounting.IsInputTaxRequired = !chkInputTax.Checked;
            model.Group.Status = Convert.ToInt32(Helper.SetFlag(chkNoTax.Checked, (int)model.Group.Status, GroupOrderStatus.NoTax));

            #endregion

            #region 檔次續接設定

            // 續接數量檔次
            Guid ancBid = Guid.TryParse(txtAncestorBid.Text, out ancBid) ? ancBid : Guid.Empty;
            if (ancBid != Guid.Empty)
            {
                model.Property.AncestorBusinessHourGuid = ancBid;
                model.Property.IsContinuedQuantity = true;
            }
            else
            {
                model.Property.AncestorBusinessHourGuid = null;
                model.Property.ContinuedQuantity = 0;
                model.Property.IsContinuedQuantity = false;
            }

            // 續接憑證檔次
            Guid ancSeqBid = Guid.TryParse(txtAncestorSeqBid.Text, out ancSeqBid) ? ancSeqBid : Guid.Empty;
            if (ancSeqBid != Guid.Empty)
            {
                model.Property.AncestorSequenceBusinessHourGuid = ancSeqBid;
                model.Property.IsContinuedSequence = true;
            }
            else
            {
                model.Property.AncestorSequenceBusinessHourGuid = null;
                model.Property.IsContinuedSequence = false;
            }

            #endregion

            return model;
        }

        public CategoryDealCollection GetCategoryDeals()
        {
            List<int> categories = new List<int>();
            foreach (RepeaterItem channel in rptChannel.Items)
            {
                // 頻道
                int channelId = 0;
                CheckBox chkChannel = (CheckBox)channel.FindControl("chkChannel");
                if (chkChannel.Checked)
                {
                    HiddenField hidChannel = (HiddenField)channel.FindControl("hidChannel");
                    if (int.TryParse(hidChannel.Value, out channelId) && channelId != 0)
                    {
                        categories.Add(channelId);

                        // 區域
                        Repeater rptArea = (Repeater)channel.FindControl("rptArea");
                        foreach (RepeaterItem area in rptArea.Items)
                        {
                            CheckBox chkArea = (CheckBox)area.FindControl("chkArea");
                            if (chkArea.Checked)
                            {
                                HiddenField hidArea = (HiddenField)area.FindControl("hidArea");
                                int id = int.TryParse(hidArea.Value, out id) ? id : 0;
                                if (id != 0)
                                {
                                    categories.Add(id);
                                }
                            }
                        }

                        // 商圈
                        Repeater rptArea2 = (Repeater)channel.FindControl("rptArea2");
                        foreach (RepeaterItem areaItem in rptArea2.Items)
                        {
                            Repeater rptCommercial = (Repeater)areaItem.FindControl("rptCommercial");
                            foreach (RepeaterItem commercial in rptCommercial.Items)
                            {
                                CheckBox chkCommercial = (CheckBox)commercial.FindControl("chkCommercial");
                                if (chkCommercial.Checked)
                                {
                                    HiddenField hidCommercial = (HiddenField)commercial.FindControl("hidCommercial");
                                    int cid = int.TryParse(hidCommercial.Value, out cid) ? cid : 0;
                                    if (cid != 0)
                                    {
                                        categories.Add(cid);
                                    }
                                }
                            }

                            //旅遊-商圈‧景點
                            Repeater rptSPRegion = (Repeater)areaItem.FindControl("rptSPRegion");
                            foreach (RepeaterItem spregion in rptSPRegion.Items)
                            {
                                Repeater rptSpecialRegion = (Repeater)spregion.FindControl("rptSpecialRegion");
                                foreach (RepeaterItem specialregion in rptSpecialRegion.Items)
                                {
                                    CheckBox chkSpecialRegion = (CheckBox)specialregion.FindControl("chkSpecialRegion");
                                    if (chkSpecialRegion.Checked)
                                    {
                                        HiddenField hidSpecialRegion = (HiddenField)specialregion.FindControl("hidSpecialRegion");
                                        int cid = int.TryParse(hidSpecialRegion.Value, out cid) ? cid : 0;
                                        if (cid != 0)
                                        {
                                            categories.Add(cid);
                                        }
                                    }
                                }

                            }
                        }

                        // 分類
                        Repeater rptCategory = (Repeater)channel.FindControl("rptCategory");
                        foreach (RepeaterItem commercial in rptCategory.Items)
                        {
                            CheckBox chkCategory = (CheckBox)commercial.FindControl("chkCategory");
                            if (chkCategory.Checked)
                            {
                                HiddenField hidCategory = (HiddenField)commercial.FindControl("hidCategory");
                                int id = int.TryParse(hidCategory.Value, out id) ? id : 0;
                                if (id != 0)
                                {
                                    categories.Add(id);
                                }

                                // 子分類
                                Repeater rptSubDealCategoryArea = (Repeater)commercial.FindControl("rptSubDealCategoryArea");
                                foreach (RepeaterItem subDeal in rptSubDealCategoryArea.Items)
                                {
                                    CheckBox chkSubDealCategory = (CheckBox)subDeal.FindControl("chkSubDealCategory");
                                    if (chkSubDealCategory.Checked)
                                    {
                                        HiddenField hidSubDealCategory = (HiddenField)subDeal.FindControl("hidSubDealCategory");
                                        int subid = int.TryParse(hidSubDealCategory.Value, out subid) ? subid : 0;
                                        if (subid != 0)
                                        {
                                            categories.Add(subid);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            CategoryDealCollection rtn = new CategoryDealCollection();
            foreach (int cid in categories)
            {
                rtn.Add(new CategoryDeal()
                {
                    Bid = BusinessHourGuid,
                    Cid = cid
                });
            }

            return rtn;
        }

        public PponStoreCollection GetPponStores(Guid bid)
        {
            PponStoreCollection rtn = new PponStoreCollection();
            foreach (RepeaterItem store in rptPponStore.Items)
            {
                CheckBox chkCheck = (CheckBox)store.FindControl("chkCheck");
                if (chkCheck.Checked && chkCheck.Enabled)
                {
                    HiddenField hidStoreGuid = (HiddenField)store.FindControl("hidStoreGuid");
                    Guid stid = Guid.TryParse(hidStoreGuid.Value, out stid) ? stid : Guid.Empty;
                    if (stid != Guid.Empty)
                    {
                        rtn.Add(new PponStore()
                        {
                            BusinessHourGuid = bid,
                            StoreGuid = stid,
                            VbsRight = (int)VbsRightFlag.Location,
                            ResourceGuid = Guid.NewGuid(),
                            CreateId = UserName,
                            CreateTime = DateTime.Now
                        });
                    }
                }
            }
            return rtn;
        }

        public PponStoreCollection GetVbs(Guid bid)
        {
            PponStoreCollection rtn = new PponStoreCollection();
            foreach (RepeaterItem store in rptVbs.Items)
            {
                TextBox txtQuantity = (TextBox)store.FindControl("txtQuantity");
                CheckBox chkVerifyShop = (CheckBox)store.FindControl("chkVerifyShop");
                CheckBox chkVerify = (CheckBox)store.FindControl("chkVerify");
                CheckBox chkAccouting = (CheckBox)store.FindControl("chkAccouting");
                CheckBox chkViewBalanceSheet = (CheckBox)store.FindControl("chkViewBalanceSheet");
                Image btnHideBalanceSheet = (Image)store.FindControl("btnHideBalanceSheet");
                bool isHideBalanceSheet = bool.Parse(((HiddenField)store.FindControl("hidIsHideBalanceSheet")).Value ?? false.ToString());

                if ((chkVerifyShop.Checked && chkVerifyShop.Enabled) || (chkVerify.Checked && chkVerify.Enabled) || (chkAccouting.Checked && chkAccouting.Enabled) || (chkViewBalanceSheet.Checked && chkViewBalanceSheet.Enabled))
                {
                    int VerifyShop = 0, Verify = 0, Accouting = 0, BalanceSheet = 0, BalanceSheetHideFromDealSeller = 0;
                    if (chkVerifyShop.Checked)
                        VerifyShop = (int)VbsRightFlag.VerifyShop;
                    if (chkVerify.Checked)
                        Verify = (int)VbsRightFlag.Verify;
                    if (chkAccouting.Checked)
                        Accouting = (int)VbsRightFlag.Accouting;
                    if (chkViewBalanceSheet.Checked)
                        BalanceSheet = (int)VbsRightFlag.ViewBalanceSheet;
                    if (chkViewBalanceSheet.Checked && isHideBalanceSheet)
                        BalanceSheetHideFromDealSeller = (int)VbsRightFlag.BalanceSheetHideFromDealSeller;

                    HiddenField hidStoreGuid = (HiddenField)store.FindControl("hidStoreGuid");
                    Guid stid = Guid.TryParse(hidStoreGuid.Value, out stid) ? stid : Guid.Empty;
                    if (stid != Guid.Empty)
                    {

                        int? qty;
                        if (!string.IsNullOrWhiteSpace(txtQuantity.Text.Trim()))
                        {
                            qty = int.Parse(txtQuantity.Text.Trim());
                        }
                        else
                        {
                            qty = null;
                        }

                        rtn.Add(new PponStore()
                        {
                            BusinessHourGuid = bid,
                            StoreGuid = stid,
                            TotalQuantity = qty,
                            VbsRight = VerifyShop + Verify + Accouting + BalanceSheet + BalanceSheetHideFromDealSeller,
                            ResourceGuid = Guid.NewGuid(),
                            CreateId = UserName,
                            CreateTime = DateTime.Now

                        });
                    }
                }


            }
            return rtn;
        }

        public void ShowMessage(string msg, BusinessContentMode mode, string parms = "")
        {
            string script = string.Empty;
            if (!string.IsNullOrWhiteSpace(msg))
            {
                script = string.Format("alert('{0}');", msg);
            }
            switch (mode)
            {
                case BusinessContentMode.BackToList:
                case BusinessContentMode.BusinessNotFound:
                    script += string.Format("location.href='{1}/sal/BusinessList.aspx';", msg, config.SiteUrl);
                    break;
                case BusinessContentMode.GoToSetup:
                    script += string.Format("location.href='{1}/controlroom/ppon/setup.aspx?bid={2}';", msg, config.SiteUrl, parms);
                    break;
                case BusinessContentMode.Reload:
                    script += string.Format("location.href='{1}/sal/BusinessContent.aspx?bid={2}&tab={3}';", msg, config.SiteUrl, BusinessHourGuid, parms);
                    break;
                case BusinessContentMode.Redirect:
                    script += string.Format("location.href='{1}/sal/BusinessContent.aspx?bid={2}';", msg, config.SiteUrl, parms);
                    break;
                case BusinessContentMode.GoToProposal:
                    script += string.Format("location.href='{1}/sal/ProposalContent.aspx?pid={2}';", msg, config.SiteUrl, parms);
                    break;
                case BusinessContentMode.DataError:
                default:
                    break;
            }

            if (!string.IsNullOrWhiteSpace(script))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "alert", script, true);
            }
        }

        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IntialControls();
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnReCreateProposal_Click(object sender, EventArgs e)
        {
            if (this.ReCreateProposal != null)
            {
                this.ReCreateProposal(this, new DataEventArgs<KeyValuePair<Guid, ProposalCopyType>>(new KeyValuePair<Guid, ProposalCopyType>(BusinessHourGuid, ProposalCopyType.Similar)));
            }
        }

        protected void btnReCreateSameProposal_Click(object sender, EventArgs e)
        {
            if (this.ReCreateProposal != null)
            {
                this.ReCreateProposal(this, new DataEventArgs<KeyValuePair<Guid, ProposalCopyType>>(new KeyValuePair<Guid, ProposalCopyType>(BusinessHourGuid, ProposalCopyType.Same)));
            }
        }

        protected void rptMultiDeal_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                Literal lblBid = (Literal)e.Item.FindControl("lblBid");
                Label lblCost = (Label)e.Item.FindControl("lblCost");
                Guid gid = Guid.Empty;
                Guid.TryParse(lblBid.Text, out gid);
                DealCost dc = PponFacade.GetDealCostByGuid(gid).LastOrDefault();
                if (dc != null)
                {
                    int iCost = 0;
                    int.TryParse((dc.Cost ?? 0).ToString("F0"), out iCost);
                    lblCost.Text = iCost.ToString();
                }
            }
        }
        protected void rptChannel_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;

                // 頻道
                CheckBox chkChannel = (CheckBox)e.Item.FindControl("chkChannel");
                HiddenField hidChannel = (HiddenField)e.Item.FindControl("hidChannel");
                chkChannel.Text = dataItem.CategoryName;
                hidChannel.Value = dataItem.CategoryId.ToString();

                // 區域
                Repeater rptArea = (Repeater)e.Item.FindControl("rptArea");
                Repeater rptArea2 = (Repeater)e.Item.FindControl("rptArea2");
                List<CategoryTypeNode> area = dataItem.NodeDatas.Where(x => x.NodeType == CategoryType.PponChannelArea).ToList();
                if (area.Count > 0)
                {
                    CategoryTypeNode areaNode = area.First();
                    rptArea.DataSource = areaNode.CategoryNodes;
                    rptArea.DataBind();

                    rptArea2.DataSource = rptArea.DataSource;
                    rptArea2.DataBind();
                }

                // 分類
                Repeater rptCategory = (Repeater)e.Item.FindControl("rptCategory");
                List<CategoryTypeNode> categories = dataItem.NodeDatas.Where(x => x.NodeType == CategoryType.DealCategory).ToList();
                if (categories.Count > 0)
                {
                    CategoryTypeNode categoryNode = categories.First();
                    rptCategory.DataSource = categoryNode.CategoryNodes;
                    rptCategory.DataBind();
                }
            }
        }

        protected void rptArea_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                CheckBox chkArea = (CheckBox)e.Item.FindControl("chkArea");
                HiddenField hidArea = (HiddenField)e.Item.FindControl("hidArea");
                chkArea.Text = dataItem.CategoryName;
                hidArea.Value = dataItem.CategoryId.ToString();
            }
        }

        protected void rptArea2_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                // 商圈
                List<CategoryNode> specialRegionCategories = dataItem.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                if ((specialRegionCategories.Any(x => x.CategoryName == "商圈‧景點")))
                {
                    var fatherCategoryNode = specialRegionCategories.Where(x => x.CategoryName == "商圈‧景點").First();
                    Repeater rptCommercial = (Repeater)e.Item.FindControl("rptCommercial");
                    List<CategoryNode> commercials = fatherCategoryNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                    if (commercials.Count > 0)
                    {
                        rptCommercial.DataSource = commercials;
                        rptCommercial.DataBind();
                    }
                }

                if ((specialRegionCategories.Any(x => x.CategoryName.Contains("旅遊商圈‧景點"))))
                {
                    var fatherCategoryNode = specialRegionCategories.Where(x => x.CategoryName.Contains("旅遊商圈‧景點")).First();
                    Repeater rptCommercial = (Repeater)e.Item.FindControl("rptCommercial");
                    List<CategoryNode> commercials = fatherCategoryNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                    if (commercials.Count > 0)
                    {
                        rptCommercial.DataSource = commercials;
                        rptCommercial.DataBind();

                        //商圈景點子區域
                        if (fatherCategoryNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea).Any())
                        {
                            Repeater rptSPRegion = (Repeater)e.Item.FindControl("rptSPRegion");
                            rptSPRegion.DataSource = fatherCategoryNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                            rptSPRegion.DataBind();
                        }
                    }
                }
            }
        }

        protected void rptCommercial_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                CheckBox chkCommercial = (CheckBox)e.Item.FindControl("chkCommercial");
                HiddenField hidCommercial = (HiddenField)e.Item.FindControl("hidCommercial");
                chkCommercial.Text = dataItem.CategoryName;
                hidCommercial.Value = dataItem.CategoryId.ToString();
            }
        }

        protected void rptCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                CheckBox chkCategory = (CheckBox)e.Item.FindControl("chkCategory");
                HiddenField hidCategory = (HiddenField)e.Item.FindControl("hidCategory");
                HiddenField hidIsShowBackEnd = (HiddenField)e.Item.FindControl("hidIsShowBackEnd");
                chkCategory.Text = dataItem.CategoryName;
                hidCategory.Value = dataItem.CategoryId.ToString();
                hidIsShowBackEnd.Value = dataItem.IsShowBackEnd.ToString();

                //子分類
                Repeater rptSubDealCategoryArea = (Repeater)e.Item.FindControl("rptSubDealCategoryArea");

                //List<ViewCategoryDependency> subCategoryCol = PponFacade.ViewCategoryDependencyGetByParentId(dataItem.CategoryId);

                //List<Category> subDealCategories = new List<Category>();

                //foreach (var subCategory in subCategoryCol.Where(x => x.CategoryStatus == 1))
                //{
                //    Category subnode = PponFacade.SubDealCategoryGet(subCategory.CategoryId);
                //    if (subnode.IsLoaded)
                //    {
                //        subDealCategories.Add(subnode);
                //    }
                //}

                //if (subDealCategories.Count > 0)
                //{
                //    rptSubDealCategoryArea.DataSource = subDealCategories;
                //    rptSubDealCategoryArea.DataBind();
                //}

                if (dataItem.NodeDatas.Count() > 0)
                {
                    rptSubDealCategoryArea.DataSource = dataItem.NodeDatas.First().CategoryNodes;
                    rptSubDealCategoryArea.DataBind();
                }
            }
        }

        protected void rptSubDealCategoryArea_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //if (e.Item.DataItem is Category)
            //{
            //    Category dataItem = (Category)e.Item.DataItem;
            //    CheckBox chkSubDealCategory = (CheckBox)e.Item.FindControl("chkSubDealCategory");
            //    HiddenField hidSubDealCategory = (HiddenField)e.Item.FindControl("hidSubDealCategory");
            //    chkSubDealCategory.Text = dataItem.Name;
            //    hidSubDealCategory.Value = dataItem.Id.ToString();
            //}

            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                CheckBox chkSubDealCategory = (CheckBox)e.Item.FindControl("chkSubDealCategory");
                HiddenField hidSubDealCategory = (HiddenField)e.Item.FindControl("hidSubDealCategory");
                HiddenField hidSubIsShowBackEnd = (HiddenField)e.Item.FindControl("hidSubIsShowBackEnd");
                chkSubDealCategory.Text = dataItem.CategoryName;
                hidSubDealCategory.Value = dataItem.CategoryId.ToString();
                hidSubIsShowBackEnd.Value = dataItem.IsShowBackEnd.ToString();
            }
        }

        protected void rptSPRegion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                Repeater rptSpecialRegion = (Repeater)e.Item.FindControl("rptSpecialRegion");
                List<CategoryNode> specialRegionCategories = dataItem.GetSubCategoryTypeNode(CategoryType.PponChannelArea);

                if (specialRegionCategories.Any())
                {
                    rptSpecialRegion.DataSource = specialRegionCategories;
                    rptSpecialRegion.DataBind();
                }
            }
        }

        protected void rptSpecialRegion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode dataItem = (CategoryNode)e.Item.DataItem;
                CheckBox chkSpecialRegion = (CheckBox)e.Item.FindControl("chkSpecialRegion");
                HiddenField hidSpecialRegion = (HiddenField)e.Item.FindControl("hidSpecialRegion");
                chkSpecialRegion.Text = dataItem.CategoryName;
                hidSpecialRegion.Value = dataItem.CategoryId.ToString();
            }
        }

        protected void rptPponStore_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<Seller, PponStore>)
            {
                KeyValuePair<Seller, PponStore> dataItem = (KeyValuePair<Seller, PponStore>)e.Item.DataItem;
                HiddenField hidStoreGuid = (HiddenField)e.Item.FindControl("hidStoreGuid");
                CheckBox chkCheck = (CheckBox)e.Item.FindControl("chkCheck");
                Literal liStoreName = (Literal)e.Item.FindControl("liStoreName");
                Literal liAddress = (Literal)e.Item.FindControl("liAddress");
                Literal liOpenTime = (Literal)e.Item.FindControl("liOpenTime");
                Literal liCloseDate = (Literal)e.Item.FindControl("liCloseDate");
                hidStoreGuid.Value = dataItem.Key.Guid.ToString();
                liStoreName.Text = dataItem.Key.SellerName;
                if (dataItem.Value != null && Helper.IsFlagSet(dataItem.Value.VbsRight, VbsRightFlag.Location))
                {
                    chkCheck.Checked = true;
                }
                if (dataItem.Key.StoreTownshipId.HasValue)
                {
                    liAddress.Text = CityManager.CityTownShopStringGet(dataItem.Key.StoreTownshipId.Value) + dataItem.Key.StoreAddress;
                }
                liOpenTime.Text = dataItem.Key.OpenTime;
                liCloseDate.Text = dataItem.Key.CloseDate;
            }
        }

        protected void rptVbs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<Seller, PponStore>)
            {
                KeyValuePair<Seller, PponStore> dataItem = (KeyValuePair<Seller, PponStore>)e.Item.DataItem;
                HiddenField hidStoreGuid = (HiddenField)e.Item.FindControl("hidStoreGuid");
                Literal liStoreName = (Literal)e.Item.FindControl("liStoreName");
                Literal liVbsAccountID = (Literal)e.Item.FindControl("liVbsAccountID");
                Panel divtooltip = (Panel)e.Item.FindControl("divtooltip");
                Literal liMoreVbsAccountID = (Literal)e.Item.FindControl("liMoreVbsAccountID");
                TextBox txtQuantity = (TextBox)e.Item.FindControl("txtQuantity");
                CheckBox chkVerifyShop = (CheckBox)e.Item.FindControl("chkVerifyShop");
                CheckBox chkVerify = (CheckBox)e.Item.FindControl("chkVerify");
                CheckBox chkAccouting = (CheckBox)e.Item.FindControl("chkAccouting");
                CheckBox chkViewBalanceSheet = (CheckBox)e.Item.FindControl("chkViewBalanceSheet");
                Image btnHideBalanceSheet = (Image)e.Item.FindControl("btnHideBalanceSheet");
                HiddenField hidIsHideBalanceSheet = (HiddenField)e.Item.FindControl("hidIsHideBalanceSheet");

                hidStoreGuid.Value = dataItem.Key.Guid.ToString();
                hidIsHideBalanceSheet.Value = false.ToString();


                //顯示階層
                Guid root = SellerFacade.GetRootSellerGuid(dataItem.Key.Guid);
                int level = (int)SellerFacade.GetSellerTreeLevel(dataItem.Key.Guid, root);
                string space = "";
                for (int i = 1; i < level; i++)
                    space += "&nbsp;&nbsp;&nbsp;";

                liStoreName.Text = space + dataItem.Key.SellerName;

                //沒有proposal_store就不勾
                if (dataItem.Value != null)
                {

                    #region 顯示商家帳號
                    string strVbsAccountID = "";
                    string strMoreVbsAccountID = "";
                    bool IsShowToolTip = false;
                    ProposalFacade.ShowVbsAccount(dataItem.Value.VbsRight, root, dataItem.Key.Guid.ToString(), BusinessHourGuid, out strVbsAccountID, out strMoreVbsAccountID, out IsShowToolTip);
                    liVbsAccountID.Text = strVbsAccountID.Equals("") ? "" : space + strVbsAccountID;
                    liMoreVbsAccountID.Text = strMoreVbsAccountID.Equals("") ? "" : space + strMoreVbsAccountID;
                    divtooltip.Visible = IsShowToolTip;
                    #endregion


                    txtQuantity.Text = dataItem.Value.TotalQuantity.HasValue ? dataItem.Value.TotalQuantity.Value.ToString() : string.Empty;
                    if (Helper.IsFlagSet(dataItem.Value.VbsRight, VbsRightFlag.VerifyShop))
                    {
                        chkVerifyShop.Checked = true;
                    }
                    if (Helper.IsFlagSet(dataItem.Value.VbsRight, VbsRightFlag.Verify))
                    {
                        chkVerify.Checked = true;
                    }
                    if (Helper.IsFlagSet(dataItem.Value.VbsRight, VbsRightFlag.Accouting))
                    {
                        chkAccouting.Checked = true;
                    }
                    if (Helper.IsFlagSet(dataItem.Value.VbsRight, VbsRightFlag.ViewBalanceSheet))
                    {
                        chkViewBalanceSheet.Checked = true;

                    }
                    if (Helper.IsFlagSet(dataItem.Value.VbsRight, VbsRightFlag.BalanceSheetHideFromDealSeller))
                    {
                        btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/lock.png");
                        hidIsHideBalanceSheet.Value = true.ToString();
                    }
                    else
                    {
                        btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock.png");
                        hidIsHideBalanceSheet.Value = false.ToString();
                    }
                }
                else
                {
                    btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock.png");
                    hidIsHideBalanceSheet.Value = false.ToString();
                }

                if (hidStoreGuid.Value == _SellerGuid.ToString())
                {
                    btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock_disabled.png");
                    hidIsHideBalanceSheet.Value = false.ToString();
                }
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Save != null)
            {
                this.Save(this, e);
            }
        }


        protected void btnPponStoreSave_Click(object sender, EventArgs e)
        {
            if (this.PponStoreSave != null)
            {
                this.PponStoreSave(this, e);
            }

            if (this.VbsSave != null)
            {
                this.VbsSave(this, new DataEventArgs<int>(1));
            }
        }

        protected void btnVbsSave_Click(object sender, EventArgs e)
        {
            if (this.VbsSave != null)
            {
                this.VbsSave(this, new DataEventArgs<int>(0));
            }
        }

        protected void btnSettingCheck_Click(object sender, EventArgs e)
        {
            if (this.SettingCheck != null)
            {
                this.SettingCheck(this, new DataEventArgs<Guid>(BusinessHourGuid));
            }
        }

        protected void btnDevelopeReferral_Click(object sender, EventArgs e)
        {
            if (this.Referral != null)
            {
                this.Referral(this, new DataEventArgs<KeyValuePair<string, SellerSalesType>>(new KeyValuePair<string, SellerSalesType>(txtDevelopeReferralEmail.Text, SellerSalesType.Develope)));
            }
        }

        protected void btnOperationReferral_Click(object sender, EventArgs e)
        {
            if (this.Referral != null)
            {
                this.Referral(this, new DataEventArgs<KeyValuePair<string, SellerSalesType>>(new KeyValuePair<string, SellerSalesType>(txtOperationReferralEmail.Text, SellerSalesType.Operation)));
            }
        }

        protected void btnChangeLog_Click(object sender, EventArgs e)
        {
            if (this.ChangeLog != null)
            {
                this.ChangeLog(this, new DataEventArgs<string>(txtChangeLog.Text));
            }
        }

        #endregion

        #region private method
        private void IntialControls()
        {

            Proposal pro = ProposalFacade.ProposalGet(BusinessHourGuid);

            //該檔次是否同意新合約
            hidRemittanceType.Value = "false";

            if (pro.DeliveryType == (int)DeliveryType.ToShop || (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType == (int)ProposalDealType.Travel))
                hidRemittanceType.Value = SellerFacade.IsAgreeNewContractSeller(pro.SellerGuid, (int)DeliveryType.ToShop).ToString();
            else
                hidRemittanceType.Value = SellerFacade.IsAgreeNewContractSeller(pro.SellerGuid, (int)DeliveryType.ToHouse).ToString();
            



            ddlDealType.DataSource = SystemCodeManager.GetDealType().Where(x => x.ParentCodeId == null);
            ddlDealType.DataTextField = "CodeName";
            ddlDealType.DataValueField = "CodeId";
            ddlDealType.DataBind();

            // 上檔頻道
            List<CategoryNode> nodeData = CategoryManager.PponChannelCategoryTree.CategoryNodes.OrderBy(x => x.Seq).ToList();
            rptChannel.DataSource = nodeData.Where(x => x.CategoryId.EqualsNone(91, 92, 93));
            rptChannel.DataBind();


            //出帳方式
            Dictionary<int, string> remittanceTypeList = new Dictionary<int, string>();
            

            if (config.IsRemittanceFortnightly && Convert.ToBoolean(hidRemittanceType.Value))
            {
                ddlRemittanceType.Visible = false;

                if (pro.DeliveryType == (int)DeliveryType.ToShop || (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType == (int)ProposalDealType.Travel))
                {
                    remittanceTypeList.Add((int)RemittanceType.Weekly, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Weekly));
                    remittanceTypeList.Add((int)RemittanceType.Monthly, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Monthly));
                    remittanceTypeList.Add((int)RemittanceType.Flexible, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Flexible));
                    remittanceTypeList.Add((int)RemittanceType.Others, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Others));
                }
                else
                {
                    remittanceTypeList.Add((int)RemittanceType.Weekly, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Weekly));
                    remittanceTypeList.Add((int)RemittanceType.Fortnightly, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Fortnightly));
                    remittanceTypeList.Add((int)RemittanceType.Monthly, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Monthly));
                    remittanceTypeList.Add((int)RemittanceType.Flexible, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Flexible));
                    remittanceTypeList.Add((int)RemittanceType.Others, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Others));
                }
            }
            else
            {
                radioRemittanceType.Visible = false;
                remittanceTypeList.Add(-1, "請選擇");
                remittanceTypeList.Add((int)RemittanceType.Weekly, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Weekly));
                remittanceTypeList.Add((int)RemittanceType.Monthly, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Monthly));
                remittanceTypeList.Add((int)RemittanceType.Flexible, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Flexible));
                remittanceTypeList.Add((int)RemittanceType.Others, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, RemittanceType.Others));
            }


            

            if (config.IsRemittanceFortnightly && Convert.ToBoolean(hidRemittanceType.Value))
            {
                radioRemittanceType.DataTextField = "Value";
                radioRemittanceType.DataValueField = "Key";
                radioRemittanceType.DataSource = remittanceTypeList;
                radioRemittanceType.DataBind();

            }
            else
            {
                ddlRemittanceType.DataTextField = "Value";
                ddlRemittanceType.DataValueField = "Key";
                ddlRemittanceType.DataSource = remittanceTypeList;
                ddlRemittanceType.DataBind();
            }
            


            // 店家單據開立方式
            Dictionary<int, string> receiptType = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(VendorReceiptType)))
            {
                receiptType[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (VendorReceiptType)item);
            }
            ddlVendorReceiptType.DataSource = receiptType.Where(x => x.Key != (int)VendorReceiptType.NoTaxInvoice);
            ddlVendorReceiptType.DataTextField = "Value";
            ddlVendorReceiptType.DataValueField = "Key";
            ddlVendorReceiptType.DataBind();
        }

        private void EnabledSettings(SalesBusinessModel model, Proposal pro)
        {
            #region 瀏覽權限檢查 (無全區瀏覽權限，會判斷商家是否有綁定業務，有的話只能檢視自己的檔次)

            //登入者
            ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(UserName));
            if (emp.IsLoaded)
            {
                if (!CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReadAll))
                {

                    //檔次業務
                    //ViewEmployee dealemp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.EmpName, model.Accounting.SalesId);

                    ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.EmpName, model.Accounting.SalesId);
                    ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.EmpName, model.Accounting.OperationSalesId);

                    if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.CrossDeptTeam))
                    {
                        if (string.IsNullOrEmpty(emp.CrossDeptTeam) && emp.TeamNo == null)
                        {
                            ShowMessage("身份權限未明，無法查看資料，請洽產品或技術人員協助。", BusinessContentMode.BackToList);
                        }
                        else
                        {
                            #region 本區檢視
                            string message1 = "";
                            string message2 = "";
                            bool checkDeSales = ProposalFacade.GetCrossPrivilege(3, emp, deSalesEmp.DeptId, deSalesEmp.TeamNo, deSalesEmp.DeptName, null, ref message1);

                            if (!checkDeSales)
                            {
                                if (opSalesEmp.IsLoaded)
                                {
                                    bool checkOpSales = ProposalFacade.GetCrossPrivilege(3, emp, opSalesEmp.DeptId, opSalesEmp.TeamNo, opSalesEmp.DeptName, null, ref message2);

                                    if (!checkOpSales)
                                    {
                                        ShowMessage(message1 + "\\n" + message2, BusinessContentMode.BackToList);
                                        return;
                                    }
                                }
                            }


                            #endregion
                        }

                    }
                    else
                    {
                        #region 無檢視權限(僅能查看負責業務是自己的檔次)

                        if ((deSalesEmp.IsLoaded && deSalesEmp.Email != UserName) && ((!opSalesEmp.IsLoaded) || opSalesEmp.IsLoaded && opSalesEmp.Email != UserName))
                        {
                            ShowMessage(string.Format("無法查看負責業務 {0} 的檔次資訊。", deSalesEmp.EmpName, opSalesEmp.EmpName), BusinessContentMode.BackToList);
                            return;
                        }

                        #endregion
                    }
                }
            }
            else
            {
                ShowMessage("查無員工資料，請重新檢視員工資料維護設定。", BusinessContentMode.BackToList);
                return;
            }



            #endregion

            if (pro.IsLoaded)
            {
                bool finance_enabled = false;

                #region 財務權限檢查

                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.FinanceApplied))
                {
                    finance_enabled = true;
                }

                #endregion

                #region 更新權限檢查 (有提案單、已建檔確認、尚未設定確認、未上檔 的檔次才可編輯)

                if (Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.Created) &&
                    !Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.SettingCheck) &&
                    model.Business.BusinessHourOrderTimeS > DateTime.Now &&
                    CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Update))
                {
                    EnableControls(true, finance_enabled);
                }
                else
                {
                    EnableControls(false, finance_enabled);
                }

                #endregion

                #region 設定確認權限檢查(必須提案申請、通過建檔確認，且尚未通過設定確認)

                if (Helper.IsFlagSet(pro.ApplyFlag, ProposalApplyFlag.Apply) &&
                    Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck) &&
                    !Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.SettingCheck) &&
                    Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.Created))
                {
                    btnSettingCheck.Visible = true;
                }
                else
                {
                    btnSettingCheck.Visible = false;
                }

                if (Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.SettingCheck))
                {
                    hkSetUp.Visible = true;
                    hkPreview.Visible = true;
                }
                else
                {
                    hkSetUp.Visible = false;
                    hkPreview.Visible = false;
                }

                #endregion

                #region 變更紀錄顯示/關閉 (有提案單才有變更紀錄)

                divHistory.Visible = true;
                aHistory.Visible = true;

                #endregion

            }
            else
            {
                EnableControls(false, false);
            }

            #region 複製提案單(建檔後)權限檢查

            if (model.Business.BusinessHourOrderTimeS < DateTime.Now)
            {
                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReCreateProposal))
                    btnReCreateProposal.Visible = true;
                //if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReCreateSameProposal))
                //    btnReCreateSameProposal.Visible = true;
            }
            else
            {
                btnReCreateProposal.Visible = false;
                //btnReCreateSameProposal.Visible = false;
            }

            #endregion

            #region 轉件功能權限檢查

            if (model.Business.BusinessHourOrderTimeS > DateTime.Now && CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Referral))
            {
                divDevelopeReferral.Visible = true;
                divOperationReferral.Visible = true;
            }
            else
            {
                divDevelopeReferral.Visible = false;
                divOperationReferral.Visible = false;
            }

            #endregion

            #region 商品寄倉權限
            if (config.IsConsignment)
            {
                if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    chkConsignment.Visible = true;
                    chkConsignment.Checked = model.Property.Consignment;
                }
            }
            #endregion

            divMain.Visible = true;
        }

        private void EnableControls(bool enabled, bool finance_enabled)
        {
            #region 上檔頻道

            foreach (RepeaterItem channelItem in rptChannel.Items)
            {
                CheckBox chkChannel = (CheckBox)channelItem.FindControl("chkChannel");
                chkChannel.Enabled = enabled;

                // 區域
                Repeater rptArea = (Repeater)channelItem.FindControl("rptArea");
                foreach (RepeaterItem areaItem in rptArea.Items)
                {
                    CheckBox chkArea = (CheckBox)areaItem.FindControl("chkArea");
                    chkArea.Enabled = enabled;


                }

                Repeater rptArea2 = (Repeater)channelItem.FindControl("rptArea2");
                foreach (RepeaterItem areaItem in rptArea2.Items)
                {
                    // 商圈
                    Repeater rptCommercial = (Repeater)areaItem.FindControl("rptCommercial");
                    foreach (RepeaterItem commItem in rptCommercial.Items)
                    {
                        CheckBox chkCommercial = (CheckBox)commItem.FindControl("chkCommercial");
                        chkCommercial.Enabled = enabled;
                    }

                    //旅遊-商圈‧景點
                    Repeater rptSPRegion = (Repeater)areaItem.FindControl("rptSPRegion");
                    foreach (RepeaterItem spregion in rptSPRegion.Items)
                    {
                        Repeater rptSpecialRegion = (Repeater)spregion.FindControl("rptSpecialRegion");
                        foreach (RepeaterItem specialregion in rptSpecialRegion.Items)
                        {
                            CheckBox chkSpecialRegion = (CheckBox)specialregion.FindControl("chkSpecialRegion");
                            chkSpecialRegion.Enabled = enabled;
                        }
                    }
                }

                // 分類
                Repeater rptCategory = (Repeater)channelItem.FindControl("rptCategory");
                foreach (RepeaterItem areaItem in rptCategory.Items)
                {
                    CheckBox chkCategory = (CheckBox)areaItem.FindControl("chkCategory");
                    chkCategory.Enabled = enabled;

                    // 子分類
                    Repeater rptSubDealCategoryArea = (Repeater)areaItem.FindControl("rptSubDealCategoryArea");
                    foreach (RepeaterItem subDealItem in rptSubDealCategoryArea.Items)
                    {
                        CheckBox chkSubCategory = (CheckBox)subDealItem.FindControl("chkSubDealCategory");
                        chkSubCategory.Enabled = enabled;
                    }
                }


            }

            #endregion

            #region 優惠內容

            // 優惠內容
            //txtItemName.Enabled = enabled;
            // 原價
            //txtOrigPrice.Enabled = enabled;
            // 售價
            //txtItemPrice.Enabled = enabled;
            // 進貨價
            //txtCost.Enabled = enabled;
            // 上架費(份數)
            //txtSlottingFeeQuantity.Enabled = enabled;
            // 運費
            //txtFreight.Enabled = enabled;
            // 免運門檻
            //txtNonFreightLimit.Enabled = enabled;

            #endregion

            #region 活動人數及限制

            //txtQuantity.Enabled = enabled;
            //txtAtmMaximum.Enabled = enabled;
            //txtDailyAmount.Enabled = enabled;

            #endregion

            #region 銷售分析

            // 館別
            ddlAccBusinessGroup.Enabled = enabled;

            // 業績歸屬
            ddlDeptId.Enabled = enabled;

            // 分類
            ddlDealType.Enabled = enabled;
            ddlDealTypeSub.Enabled = enabled;

            // 搜尋關鍵字
            txtKeyword.Enabled = enabled;

            #endregion

            #region 核銷方式
            rdlOnlineUse.Enabled = enabled;
            rdlOnlineOnly.Enabled = enabled;
            #endregion

            #region 核銷與帳務設定

            if (config.IsConsignment)
            {
                chkConsignment.Enabled = enabled;
                ddlRemittanceType.Enabled = enabled;
                if (config.IsRemittanceFortnightly && Convert.ToBoolean(hidRemittanceType.Value))
                    radioRemittanceType.Enabled = enabled;
            }
            else
            {
                if (enabled && !IsDealOpened)//有權限且未開賣，才可修改出帳方式
                {
                    ddlRemittanceType.Enabled = true;
                    if (config.IsRemittanceFortnightly && Convert.ToBoolean(hidRemittanceType.Value))
                        radioRemittanceType.Enabled = true;
                }
                else
                {
                    ddlRemittanceType.Enabled = false;
                    if (config.IsRemittanceFortnightly && Convert.ToBoolean(hidRemittanceType.Value))
                        radioRemittanceType.Enabled = false;
                }
            }


            ddlVendorReceiptType.Enabled = enabled;
            txtVendorReceiptMessage.Enabled = enabled;
            chkInputTax.Enabled = enabled;
            chkNoTax.Enabled = finance_enabled;

            #endregion

            #region 權益說明

            hkRestrictionsEdit.Visible = enabled;

            #endregion

            #region 檔次續接設定

            txtAncestorBid.Enabled = enabled;
            txtAncestorSeqBid.Enabled = enabled;

            #endregion

            #region 分店設定
            bool ToHouseenable;
            //原生賣家以下的樹(包含自己)
            List<Guid> pponstore = new List<Guid>();
            Proposal pro = ProposalFacade.ProposalGet(ProposalId);
            var ChildrenList = SellerFacade.GetChildrenSellers(pro.SellerGuid).ToList();
            if ((StoreStatus)sp.SellerGetByBid(pro.SellerGuid).StoreStatus == StoreStatus.Available)
                pponstore.Add(pro.SellerGuid);
            foreach (var Children in ChildrenList)
                pponstore.Add(Children.Guid);


            foreach (RepeaterItem item in rptPponStore.Items)
            {
                CheckBox chkCheck = (CheckBox)item.FindControl("chkCheck");
                Literal liAddress = (Literal)item.FindControl("liAddress");
                HiddenField StoreGuid = (HiddenField)item.FindControl("hidStoreGuid");
                Guid pponstid = Guid.TryParse(StoreGuid.Value, out pponstid) ? pponstid : Guid.Empty;
                chkCheck.Enabled = liAddress.Text == "" || (StoreStatus)sp.SellerGet(pponstid).StoreStatus == StoreStatus.Cancel ? false : enabled;
            }
            foreach (RepeaterItem item in rptVbs.Items)
            {
                TextBox txtQuantity = (TextBox)item.FindControl("txtQuantity");
                CheckBox chkVerifyShop = (CheckBox)item.FindControl("chkVerifyShop");
                CheckBox chkVerify = (CheckBox)item.FindControl("chkVerify");
                CheckBox chkAccouting = (CheckBox)item.FindControl("chkAccouting");
                CheckBox chkViewBalanceSheet = (CheckBox)item.FindControl("chkViewBalanceSheet");
                Image btnHideBalanceSheet = (Image)item.FindControl("btnHideBalanceSheet");
                HiddenField hidIsHideBalanceSheet = (HiddenField)item.FindControl("hidIsHideBalanceSheet");
                HiddenField StoreGuid = (HiddenField)item.FindControl("hidStoreGuid");

                Guid stid = Guid.TryParse(StoreGuid.Value, out stid) ? stid : Guid.Empty;
                chkVerifyShop.Enabled = chkVerify.Enabled = chkAccouting.Enabled = chkViewBalanceSheet.Enabled = txtQuantity.Enabled = pponstore.Contains(stid);

                #region 檢視對帳單圖片設定
                if (enabled)
                {
                    if (pro.SellerGuid == stid)
                        btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock_disabled.png");
                    else
                    {
                        if (!pponstore.Contains(stid))
                            btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock_disabled.png");
                        else
                        {
                            if (hidIsHideBalanceSheet.Value == true.ToString())
                                btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/lock.png");
                            else
                                btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock.png");
                        }
                    }
                }
                else
                {
                    if (!pponstore.Contains(stid))
                        btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock_disabled.png");
                    else
                    {
                        if (hidIsHideBalanceSheet.Value == true.ToString())
                            btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/lock_disabled.png");
                        else
                            btnHideBalanceSheet.ImageUrl = ResolveUrl("~/Themes/default/images/17Life/G2/unlock_disabled.png");
                    }
                }
                #endregion

                //宅配只能勾自己
                if (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType != (int)ProposalDealType.Travel && pro.SellerGuid.ToString() != StoreGuid.Value)
                {
                    ToHouseenable = false;
                }
                else
                {
                    ToHouseenable = true;
                }

                txtQuantity.Enabled = enabled ? ToHouseenable ? txtQuantity.Enabled : false : false;
                chkVerifyShop.Enabled = enabled ? ToHouseenable ? chkVerifyShop.Enabled : false : false;
                chkVerify.Enabled = enabled ? ToHouseenable ? chkVerify.Enabled : false : false;
                chkAccouting.Enabled = enabled ? ToHouseenable ? chkAccouting.Enabled : false : false;
                chkViewBalanceSheet.Enabled = enabled ? ToHouseenable ? chkViewBalanceSheet.Enabled : false : false;
            }

            #endregion

            btnSave.Visible = enabled | finance_enabled;
            btnPponStoreSave.Visible = enabled;
            btnVbsSave.Visible = enabled;
        }

        #endregion

        [WebMethod]
        public static dynamic BindDealTypeSubList(int pid)
        {
            IEnumerable<SystemCode> dealTypes = SystemCodeManager.GetDealType();
            return dealTypes.Where(x => x.ParentCodeId == pid).OrderBy(x => x.Seq);
        }

        [WebMethod]
        public static string BindRestrictions(string bid)
        {
            Guid _bid = Guid.Empty;
            Guid.TryParse(bid, out _bid);
            CouponEventContent cec = ProposalFacade.RestrictionsGet(_bid);

            return new JsonSerializer().Serialize(cec);
        }

    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="BusinessContent.aspx.cs" Inherits="LunchKingSite.Web.Sal.BusinessContent" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/fancybox/jquery.fancybox.css") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Tools/js/fancybox/jquery.fancybox.js"></script>


    <script type="text/javascript" src="../Tools/ckeditor/jquery.query-2.1.7.js"></script>
    <script type="text/javascript" src="../Tools/ckeditor/ckeditor.js"></script>


    <style type="text/css">
        .mc-navbtn {
            cursor: pointer;
        }

        #btn-menu {
            display: block;
            top: 200px;
            margin-left: 970px;
            width: 130px;
            position: fixed;
        }


        .btn-circle {
            display: block;
            width: 70px;
            height: 70px !important;
            line-height: 50px;
            border: 2px solid #f5f5f5;
            border-radius: 50%;
            color: #f5f5f5;
            text-align: center;
            text-decoration: none;
            background: #BF0000;
            box-shadow: 0 0 3px gray;
            font-size: 17px;
            font-weight: bold;
            color: #ffffff;
            font-family: Arial;
            font-size: 17px;
            padding: 2px 2px;
            text-decoration: none;
            text-shadow: 1px 1px 0px #810e05;
        }

        .breadcrumb {
            /*centering*/
            display: inline-block;
            box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.35);
            overflow: hidden;
            border-radius: 5px;
            /*Lets add the numbers for each link using CSS counters. flag is the name of the counter. to be defined using counter-reset in the parent element of the links*/
            counter-reset: flag;
        }

            .breadcrumb a {
                text-decoration: none;
                outline: none;
                display: block;
                float: left;
                font-size: 12px;
                line-height: 36px;
                color: white;
                /*need more margin on the left of links to accomodate the numbers*/
                padding: 0 10px 0 60px;
                background: #666;
                background: linear-gradient(#666, #333);
                position: relative;
            }


                /*adding the arrows for the breadcrumbs using rotated pseudo elements*/
                .breadcrumb a:after {
                    content: '';
                    position: absolute;
                    top: 0;
                    right: -18px; /*half of square's length*/
                    /*same dimension as the line-height of .breadcrumb a */
                    width: 36px;
                    height: 36px;
                    /*as you see the rotated square takes a larger height. which makes it tough to position it properly. So we are going to scale it down so that the diagonals become equal to the line-height of the link. We scale it to 70.7% because if square's: 
	        length = 1; diagonal = (1^2 + 1^2)^0.5 = 1.414 (pythagoras theorem)
	        if diagonal required = 1; length = 1/1.414 = 0.707*/
                    transform: scale(0.707) rotate(45deg);
                    /*we need to prevent the arrows from getting buried under the next link*/
                    z-index: 1;
                    /*background same as links but the gradient will be rotated to compensate with the transform applied*/
                    background: #666;
                    background: linear-gradient(135deg, #666, #333);
                    /*stylish arrow design using box shadow*/
                    box-shadow: 2px -2px 0 2px rgba(0, 0, 0, 0.4), 3px -3px 0 2px rgba(255, 255, 255, 0.1);
                    /*
		        5px - for rounded arrows and 
		        50px - to prevent hover glitches on the border created using shadows*/
                    border-radius: 0 5px 0 50px;
                }

        .flat a, .flat a:after {
            background: white;
            color: black;
            transition: all 0.5s;
        }

            .flat a.active,
            .flat a.active:after {
                color: #fff;
                background: #AF0000;
            }

        /*上檔頻道顯示*/
        .grui-form .form-unit .data-input [class*=pCommercial] {
            display: block;
        }

        .grui-form .form-unit .data-input p.pCategory {
            display: block;
            background: #f2f2f0;
        }

        /*營業據點highlight*/
        .showtr tr:hover td {
            background-color: #EDBD3E;
            cursor: move;
        }

        .form-Panel {
            display: inline;
        }

        /*商家帳號提示*/
        .tooltip {
            position: relative;
            display: inline-block;
        }

            .tooltip .tooltiptext {
                visibility: hidden;
                background-color: white;
                text-align: center;
                border-radius: 6px;
                padding: 5px 0;
                /* Position the tooltip */
                position: absolute;
                z-index: 1;
                border-style: solid;
                border-color: gray;
                border-width: 1px;
                width: 200px;
            }

            .tooltip:hover .tooltiptext {
                visibility: visible;
            }


        ul.tabnav li{
            float:left;
            display:inline;
            margin-left:8px;
        }
        .tabnav li a{
            background-color:#fff;
            border:1px solid #FFCCBF;
            color:dimgray;
            display:block;
            padding:3px 8px 3px 3px;
            line-height:20px;
            float:left;
        }
        .tabnav li a.active,
        .tabnav li a:hover{
            background-color:#AF0000;
            border:1px solid #000;
            color:#fff;
            _position:relative;
        }

        .subNav .tabnav li a{
            margin-bottom:-1px;
        }
        .subNav .tabnav{
            border-bottom:1px solid #FFCCBF;
        }
        #divContentDescription, #divContentRemark {
            margin-top: 20px;
            display:none;
        }
        #divContentDescription {
            height:1200px;
        }
        #divContentRemark {
            height:400px;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {

            var dealType = $('#ddlDealType');
            if ($(dealType).val() != '-1') {
                BindDealTypeSub(dealType);
            }

            //顯示上檔頻道
            //頻道
            $('.pChannel').has('[id*=chkChannel]:checked').each(function () {
                $(this).next('.pArea').show();
                //後台不顯示
                var categoryList = $(this).next('.pArea').find("input[id*='chkCategory']");
                $.each(categoryList, function (cidx, cobj) {
                    var isShow = $(cobj).parent().find("input[id*='hidIsShowBackEnd']").val();
                    if (isShow != undefined) {
                        if (isShow.toLowerCase() == "false") {
                            $(cobj).attr("disabled", "disabled");
                            //需同時把子項目也disabled
                            $(cobj).parent('.pB').next('.pCategory').find("input[id*='chkSubDealCategory']").attr("disabled", "disabled");
                        }
                    }
                });
            });

            //商圈
            $('.pA').has('[id*=chkArea]:checked').each(function () {
                var id = $(this).find('[id*=hidArea]').val();
                $(this).parents('ul').find('.pCommercial' + id).show();
            });

            //商圈(旅遊)-地區
            $('[id*=chkCommercial]:checked').each(function () {
                var id = $(this).prev().val();
                $(this).parents('tr').next().find('.pSpecialRegion' + id).show();
            });

            //分類
            $('.pB').has('[id*=chkCategory]:checked').each(function () {
                var controls = $(this).next('.pCategory').find('input[type=checkbox]');
                if (controls.length > 0) {
                    $(this).next('.pCategory').show();

                    //後台不顯示
                    var subCategoryList = $(this).next('.pCategory').find("input[id*='chkSubDealCategory']");
                    $.each(subCategoryList, function (cidx, cobj) {
                        var isShow = $(cobj).parent().find("input[id*='hidSubIsShowBackEnd']").val();
                        if (isShow != undefined) {
                            if (isShow.toLowerCase() == "false") {
                                $(cobj).attr("disabled", "disabled");
                            }
                        }
                    });
                }

            });

            // 宅配才須顯示運費/免運門檻等欄位
            if ($('#hidDeliveryType').val() == '<%= (int)DeliveryType.ToShop %>') {
                $('.product').hide();
                $(".VerifyType").show();
            }

            // 憑證才顯示分店
            if ($('#hidDeliveryType').val() == '<%= (int)DeliveryType.ToHouse %>' && $('#hidProposalDealType').val() !='<%= (int)ProposalDealType.Travel %>') {
                $('.ppon').hide();
            }

            // 設定顯示的tab
            var tab = '<%= Tab %>';
            if (tab != '') {
                SelectTab($('.mc-navbar').find('[tab=' + tab + ']'));
            }

            // 顯示毛利率
            GetGrossMargin();

            // 設定店家單據開立方式
            ChangeVendorReceiptType($('#ddlVendorReceiptType'));

            var bool_value = '<%=IsInputTaxRequired%>' == "True" ? true : false
            $('#chkInputTax').attr('checked', bool_value);

            $(".Restrictions").fancybox({
                maxWidth: 850,
                maxHeight: 600,
                fitToView: false,
                width: '70%',
                height: '70%',
                autoSize: false,
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none',
                afterClose: function () {
                    $.ajax({
                        type: "POST",
                        url: "BusinessContent.aspx/BindRestrictions",
                        data: "{'bid': '<%=BusinessHourGuid%>'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var c = response.d;
                            if (c != undefined) {
                                var _json = JSON.parse(c);
                                $("#liRestrictions").html(_json.Restrictions.replace(/\r\n/g, "<br />"));
                            }
                        }
                    });
                }
            });

            //熟客17來、優惠券、即買即用、最後一天、新光三越 預設disabled (前二頻道for APP，新光for新光APP)
            $('[id*=hidChannel]').each(function () {
                if ($(this).val() == 2160 || $(this).val() == 2161 || $(this).val() == 100001 || $(this).val() == 100002 || $(this).val() == 185)
                    $(this).next().attr('disabled', 'disabled');
            });



            ResizeWindows();

            //檢核全選
            CheckAll2('Check');
            CheckAll2('VerifyShop');
            CheckAll2('Verify');
            CheckAll2('Accouting');
            CheckAll2('ViewBalanceSheet');

            $('[id$=Check]').change(function () {
                if ($(this).is(':checked')) {
                    var id = $(this).attr('id').replace('rptPponStore', 'rptVbs').replace('chkCheck', 'chkVerifyShop');
                    if (!$('#' + id).is(':disabled')) {
                        if (($("#hidDeliveryType").val() == "<%=(int)DeliveryType.ToShop%>" &&
                                "<%=Helper.IsFlagSet(Convert.ToInt32(hidNoRestrictedStore.Value), ((int)BusinessHourStatus.NoRestrictedStore))%>" ==
                            "True") ||
                            ($("#hidDeliveryType").val() == "<%=(int)DeliveryType.ToHouse%>")) {
                            //not doing
                        } else {
                            $('#' + id).attr('checked', 'checked');
                        }

                    }
                    id = $(this).attr('id').replace('rptPponStore', 'rptVbs').replace('chkCheck', 'chkVerify');
                    if (!$('#' + id).is(':disabled')) {
                        $('#' + id).attr('checked', 'checked');
                    }

                }
                CheckAll2('Check');
            });

            $('[id$=VerifyShop]').change(function () {

                if (($("#hidDeliveryType").val() == "<%=(int)DeliveryType.ToShop%>" &&
                        "<%=Helper.IsFlagSet(Convert.ToInt32(hidNoRestrictedStore.Value), ((int)BusinessHourStatus.NoRestrictedStore))%>" ==
                    "True") ||
                    ($("#hidDeliveryType").val() == "<%=(int)DeliveryType.ToHouse%>")) {
                    alert('宅配檔次/通用券檔次無須設定[銷售店鋪]賣家');
                    $('[id$=VerifyShop]').removeAttr("checked");
                } else {
                    CheckAll2('VerifyShop');
                }

            });

            $('[id$=Verify]').change(function () {
                CheckAll2('Verify');
            });

            $('[id$=Accouting]').change(function () {
                CheckAll2('Accouting');
            });

            $('[id$=ViewBalanceSheet]').change(function () {
                CheckAll2('ViewBalanceSheet');
            });



            if ('<%=IsConsignment%>' == "True") {
                


                if ('<%=config.IsRemittanceFortnightly%>' == "True" && $('#hidRemittanceType').val() == 'True')
                {
                    //墨攻
                    if ($('input[id*="chkMohist"]').prop("checked")) {

                        $("input[name*=radioRemittanceType][value='<%=(int)RemittanceType.Others%>']").prop('checked', true);
                        $("input[name*=radioRemittanceType]").prop('disabled', true);
                    
                    }



                    $('input[id*="chkMohist"]').click(function () {
                        if ($('input[id*="chkMohist"]').prop("checked")) {

                            $("input[name*=radioRemittanceType][value='<%=(int)RemittanceType.Others%>']").prop('checked', true);
                            $("input[name*=radioRemittanceType]").prop('disabled', true);
                        }
                        else {
                            $("input[name*=radioRemittanceType]").prop('disabled', false);
                        }
                    });

                    //商品寄倉
                    if ($('input[id="chkConsignment"]').prop("checked")) {
                        $("input[name*=radioRemittanceType][value='<%=(int)RemittanceType.Monthly%>']").prop('checked', true);
                        $("input[name*=radioRemittanceType]").prop('disabled', true);
                    }


                    $('input[id="chkConsignment"]').click(function () {
                        if ($('input[id="chkConsignment"]').prop("checked")) {
                            $("input[name*=radioRemittanceType][value='<%=(int)RemittanceType.Monthly%>']").prop('checked', true);
                            $("input[name*=radioRemittanceType]").prop('disabled', true);
                        }
                        else {
                            $("input[name*=radioRemittanceType]").prop('disabled', false);
                        }
                    });
                }
                else
                {
                    //墨攻
                    if ($('input[id*="chkMohist"]').prop("checked")) {
                        $('select[id*="ddlRemittanceType"]').val('<%=(int)RemittanceType.Others%>');      //'0' = other 
                        $('select[id*="ddlRemittanceType"]').prop('disabled', true);

                    
                    }



                    $('input[id*="chkMohist"]').click(function () {
                        if ($('input[id*="chkMohist"]').prop("checked")) {
                            $('select[id*="ddlRemittanceType"]').val('<%=(int)RemittanceType.Others%>');  //'0' = other
                            $('select[id*="ddlRemittanceType"]').prop('disabled', true);

                        }
                        else {
                            $('select[id*="ddlRemittanceType"]').prop('disabled', false);
                        }
                    });

                    //商品寄倉
                    if ($('input[id="chkConsignment"]').prop("checked")) {
                        $('select[id*="ddlRemittanceType"]').val('<%=(int)RemittanceType.Monthly%>');      //'0' = other 
                        $('select[id*="ddlRemittanceType"]').prop('disabled', true);
                    }


                    $('input[id="chkConsignment"]').click(function () {
                        if ($('input[id="chkConsignment"]').prop("checked")) {
                            $('select[id*="ddlRemittanceType"]').val('<%=(int)RemittanceType.Monthly%>');  //'0' = other
                            $('select[id*="ddlRemittanceType"]').prop('disabled', true);
                        }
                        else {
                            $('select[id*="ddlRemittanceType"]').prop('disabled', false);
                        }
                    });
                }
            }
            else {
                //墨攻
                if ($('input[id*="chkMohist"]').prop("checked")) {
                    $('select[id*="ddlRemittanceType"]').val('0');      //'0' = other 
                    $('select[id*="ddlRemittanceType"]').prop('disabled', true);
                } else {
                    //非開賣中的檔次才可以修改出帳方式
                    if ("<%=IsDealOpened%>" == "False") {
                        $('select[id*="ddlRemittanceType"]').prop('disabled', false);
                    }
                }

                $('input[id*="chkMohist"]').click(function () {
                    if ($('input[id*="chkMohist"]').prop("checked")) {
                        $('select[id*="ddlRemittanceType"]').val('0');  //'0' = other
                        $('select[id*="ddlRemittanceType"]').prop('disabled', true);
                    } else {
                        //非開賣中的檔次才可以修改出帳方式
                        if ("<%=IsDealOpened%>" == "False") {
                            $('select[id*="ddlRemittanceType"]').prop('disabled', false);
                        }
                    }
                });
            }


            //對帳權限
            $(".hideBalanceSheet").click(function () {
                var src = $(this).attr("src");
                var index = $(".hideBalanceSheet").index(this);
                if ($('.checkViewBalanceSheet > :checkbox:eq(' + index + ')').is(':disabled') ||
                    src.indexOf('disabled') > 0) {
                    return false;
                }
                if (!$('.checkViewBalanceSheet > :checkbox:eq(' + index + ')').is(':checked')) {
                    alert("僅限已勾選檢視對帳單權限之賣家使用");
                    return false;
                }
                if ($(':hidden[id$=hidIsHideBalanceSheet]:eq(' + index + ')').val() === "True") {
                    $(':hidden[id$=hidIsHideBalanceSheet]:eq(' + index + ')').val('False');
                    src = src.replace("lock", "unlock");
                }
                else {
                    $(':hidden[id$=hidIsHideBalanceSheet]:eq(' + index + ')').val('True');
                    src = src.replace("unlock", "lock");
                }

                $(this).attr("src", src);
            });

            //顯示商家帳號勾選
            $('#chkIsShowVbsAccountID').change(function () {
                if ($(this).is(':checked')) {
                    $('[id=IsShowVbsAccountID]').show();
                }
                else {
                    $('[id=IsShowVbsAccountID]').hide();
                }
            });


            if ('<%=config.IsRemittanceFortnightly%>' == "True" && $('#hidRemittanceType').val() == 'True')
            {
                //部分出帳鎖住
                if ($('#hidDeliveryType').val() == '<%= (int)DeliveryType.ToShop %>' || ($('#hidDeliveryType').val() == '<%= (int)DeliveryType.ToHouse %>' && $('#hidProposalDealType').val() != '<%= (int)ProposalDealType.Product %>')) {
                    //憑證&旅遊/品生活宅配不能選每周
                    $("input[name*=radioRemittanceType][value='<%=((int)RemittanceType.Weekly).ToString()%>']").attr('disabled', true);
                }
                else
                {
                    //宅配不能選每周和彈性
                    $("input[name*=radioRemittanceType][value='<%=((int)RemittanceType.Weekly).ToString()%>']").attr('disabled', true);
                    $("input[name*=radioRemittanceType][value='<%=((int)RemittanceType.Flexible).ToString()%>']").attr('disabled', true);

                    //非24不能選雙周
                    if ('<%=ShipType%>' != '<%=(int)DealShipType.Ship72Hrs%>')
                        $("input[name*=radioRemittanceType][value='<%=((int)RemittanceType.Fortnightly).ToString()%>']").attr('disabled', true);

                }
            }
            

            SetCKEditor();

            var conentNavs = $('#navContentDescription, #navContentRemark');
            var conentTabs = $('#divContentDescription, #divContentRemark');
            $(conentNavs).click(function() {
                $(conentNavs).removeClass('active');
                $(this).addClass('active');
                var tabId = '#' + $(this).attr('tab');
                $(conentTabs).hide();
                $(tabId).show();
            });

        });

        $(window).resize(function () {
            ResizeWindows();
        });

        function ResizeWindows() {
            console.log($(window).width());
            if ($(window).width() < 1018) {
                $("#btn-menu").hide();
            } else {
                $("#btn-menu").show();
            }
        }

        function SaveCheck(type) {
            $('.if-error').hide();
            $('.error').removeClass('error');

            //var channelflag = true;
            //$('.pChannel').has('[id*=chkChannel]:checked').each(function () {
            //    var objc = 0;
            //    $(this).next().find(".pA input[type=checkbox]").each(function (idx, obj) {
            //        if ($(obj).is(":checked")) {
            //            objc += 1;
            //        }
            //    });
            //    if (objc == 0) {
            //        alert("aa");
            //    }


            //    //var pBs = $(pArea > ".pB").has('[id*=chkCategory]:checked');
            //    //if (pBs.length == 0) {
            //    //    channelflag = false;
            //    //} else {

            //    //}


            //});

            //if (!channelflag) {
            //    alert("請檢查上檔頻道是否勾選完成?");
            //    return false;
            //}

            //return false;

            var check = true;

            if ($.trim($('#txtItemName').val()) == '') {
                check = false;
                $('#pItemName').show();
                $('#pItemName').parent().parent().addClass('error');
                $('#txtItemName').focus();
            }

            if ($.trim($('#txtOrigPrice').val()) == '' || $.trim($('#txtItemPrice').val()) == '') {
                $('#pItemPrice').show();
                $('#pItemPrice').parent().parent().addClass('error');
                check = false;
                $('#txtOrigPrice').focus();
            }

            //有上架費份數，進貨價不可為0
            if ($.trim($("#txtSlottingFeeQuantity").val()) != '' && $.trim($("#txtSlottingFeeQuantity").val()) > 0) {
                if ($.trim($('#txtCost').val()) == '' || $.trim($('#txtCost').val()) == '0') {
                    $('#pCost').show();
                    $('#pCost').parent().parent().addClass('error');
                    check = false;
                    $('#txtCost').focus();
                }
            }


            if ($.trim($('#txtOrderTotalLimit').val()) == '' || $.trim($('#txtDailyAmount').val()) == '') {
                check = false;
                $('#pQuantity').show();
                $('#pQuantity').parent().parent().addClass('error');
                $('#txtOrderTotalLimit').focus();
            }

            if ($('#ddlAccBusinessGroup').val() == '-1') {
                check = false;
                $('#pAccBusinessGroup').show();
                $('#pAccBusinessGroup').parent().parent().addClass('error');
                $('#ddlAccBusinessGroup').focus();
            }
            if (type == 'SettingCheck') {
                //送出排檔判斷DB
                if ($.trim($('#hidDealType').val()) == '') {
                    check = false;
                    $('#pDealTypeSub').show();
                    $('#pDealTypeSub').parent().parent().addClass('error');
                    $('#ddlDealType').focus();
                }
            }
            else if (type == 'SaveCheck') {
                //儲存判斷頁面上的控制項
                if ($('#ddlDealType').val() == '-1' || $.trim($('#hidDealTypeSub').val()) == '') {
                    check = false;
                    $('#pDealTypeSub').show();
                    $('#pDealTypeSub').parent().parent().addClass('error');
                    $('#ddlDealType').focus();
                }


                if ('<%=config.IsRemittanceFortnightly%>' == "True" && $('#hidRemittanceType').val() == 'True')
                {
                    if ($('#hidDeliveryType').val() == '<%= (int)DeliveryType.ToShop %>' || ($('#hidDeliveryType').val() == '<%= (int)DeliveryType.ToHouse %>' && $('#hidProposalDealType').val() =='<%= (int)ProposalDealType.Travel %>')) {
                        if ('<%=SellerRemittanceType%>' !=$('[name*=radioRemittanceType]:checked').val())
                        {
                            check = false;
                            alert('請確認，建檔平台與商家管理內的出帳方式需一致');
                        }
                    }
                }
                else
                {
                    //檢查出帳方式是否有選
                    if ($('#<%=ddlRemittanceType.ClientID%> option:selected').val() == "-1") {
                        check = false;
                        $('#pRemittanceType').show();
                        $('#pRemittanceType').parent().parent().addClass('error');
                        $('#ddlRemittanceType').focus();
                    }
                }
                
            }


            if ($.trim($('#txtKeyword').val()) == '') {
                check = false;
                $('#pKeyword').show();
                $('#pKeyword').parent().parent().addClass('error');
                $('#txtKeyword').focus();
            }

            if (!CheckVendorReceiptType()) {
                check = false;
            }

            if (!ChannelSaveCheck2()) {
                return false;
            }

            $("#divChannelList input[type='checkbox']").attr("disabled", false);


            if ('<%=config.IsRemittanceFortnightly%>' == "True" && $('#hidRemittanceType').val() == 'True')
            {
                //save前,出帳disabled先開啟
                $("input[name*=radioRemittanceType][value='<%=((int)RemittanceType.Weekly).ToString()%>']").attr('disabled', false);
                $("input[name*=radioRemittanceType][value='<%=((int)RemittanceType.Flexible).ToString()%>']").attr('disabled', false);
                $("input[name*=radioRemittanceType][value='<%=((int)RemittanceType.Fortnightly).ToString()%>']").attr('disabled', false);
            }
            

            return check;
        }

        function CheckVendorReceiptType() {
            if ($('#ddlVendorReceiptType').val() == '<%= (int)VendorReceiptType.Other %>') {
                if ($.trim($('#txtVendorReceiptMessage').val()) == '') {
                    $('#pVendorReceiptType').show();
                    $('#pVendorReceiptType').parent().parent().addClass('error');
                    $('#txtVendorReceiptMessage').focus();
                    $('#txtVendorReceiptMessage').show();
                    return false;
                } else {
                    $('#pVendorReceiptType').hide();
                    $('#pVendorReceiptType').parent().parent().removeClass('error');
                    return true;
                }
            } else {
                $('#txtVendorReceiptMessage').val('');
                $('#txtVendorReceiptMessage').hide();
                $('#pVendorReceiptType').hide();
                $('#pVendorReceiptType').parent().parent().removeClass('error');
                return true;
            }
        }

        function SettingCheck() {
            if (!LocationSaveCheck()) {
                return false;
            }
            if (!VbsSaveCheck()) {
                return false;
            }
            if (!SaveCheck("SettingCheck")) {
                return false;
            }
            if (!ChannelSaveCheck()) {
                return false;
            }
            return true;
        }

        function LocationSaveCheck() {
            // 憑證儲存必須至少勾選一個分店
            if ($('#hidDeliveryType').val() == '<%= (int)DeliveryType.ToShop %>') {
                if ($('#pponstore').find('[id*=chkCheck]:checked').length == 0) {
                    alert('至少需勾選一個營業據點。');
                    return false;
                }
            }
            return true;
        }

        function VbsSaveCheck() {
            // 憑證儲存必須至少各勾選一個銷售店鋪、憑證核實與匯款對象
            if ($('#vbs').find('[id*=Verify]:checked').length == 0 || $('#vbs').find('[id*=Accouting]:checked').length == 0) {
                alert('至少需各勾選一項憑證核實與匯款對象。');
                return false;
            }
            else {
                if ($('#hidDeliveryType').val() == '<%= (int)DeliveryType.ToShop %>' && "<%=Helper.IsFlagSet(Convert.ToInt32(hidNoRestrictedStore.Value), ((int)BusinessHourStatus.NoRestrictedStore))%>" == "False") {
                    if ($('#vbs').find('[id*=VerifyShop]:checked').length == 0) {
                        alert('憑證類型至少需勾選一個銷售店鋪。');
                        return false;
                    }
                }
            }
            return true;
        }

        function selectChannel(obj) {

            if ($(obj).is(':checked')) {
                $(obj).parent('.pChannel').next('.pArea').show();
                //後台不顯示
                var categoryList = $(obj).parent('.pChannel').next('.pArea').find("input[id*='chkCategory']");
                $.each(categoryList, function (cidx, cobj) {
                    var isShow = $(cobj).parent().find("input[id*='hidIsShowBackEnd']").val();
                    if (isShow != undefined) {
                        if (isShow.toLowerCase() == "false") {
                            $(cobj).attr("disabled", "disabled");
                        }
                    }
                });
            } else {
                $(obj).parent('.pChannel').next('.pArea').hide();
                $(obj).parent('.pChannel').next('.pArea').find('.pCommercial').hide();

                $(obj).parent('.pChannel').next('.pArea').find('input[type=checkbox]').each(function () {
                    $(this).next('label').css('color', '#333');
                    $(this).attr('checked', false);
                });
            }
        }
        function selectCategory(obj) {

            if ($(obj).is(':checked')) {
                var controls = $(obj).parent().next('.pCategory').find('input[type=checkbox]');
                if (controls.length > 0) {
                    $(obj).parent().next('.pCategory').show();
                    //後台不顯示
                    var subCategoryList = $(obj).parent('.pB').next('.pCategory').find("input[id*='chkSubDealCategory']");
                    $.each(subCategoryList, function (cidx, cobj) {
                        var isShow = $(cobj).parent().find("input[id*='hidSubIsShowBackEnd']").val();
                        if (isShow != undefined) {
                            if (isShow.toLowerCase() == "false") {
                                $(cobj).attr("disabled", "disabled");
                            }
                        }
                    });
                }
            } else {
                $(obj).parent().next('.pCategory').hide();

                $(obj).parent().next('.pCategory').find('input[type=checkbox]').each(function () {
                    $(this).next('label').css('color', '#333');
                    $(this).attr('checked', false);
                });
            }
        }

        function selectArea(obj) {

            var id = $(obj).prev().val();
            if ($(obj).is(':checked')) {
                $(obj).parent().parent().parent().find('.pCommercial' + id).show();
            } else {
                $(obj).parent().parent().parent().find('.pCommercial' + id).hide();

                $(obj).$(obj).parent().parent().parent().find('.pCommercial' + id).find('input[type=checkbox]').each(function () {
                    $(this).next('label').css('color', '#333');
                    $(this).attr('checked', false);
                });
            }
        }

        function selectCommercial(obj) {

            var id = $(obj).prev().val();

            if ($(obj).is(':checked')) {
                $(obj).parent().parent().parent().parent().parent().find('.pSpecialRegion' + id).show();
            } else {
                $(obj).parent().parent().parent().parent().parent().find('.pSpecialRegion' + id).hide();

                $(obj).parent().parent().parent().parent().parent().find('.pSpecialRegion' + id).find('input[type=checkbox]').each(function () {
                    $(this).next('label').css('color', '#333');
                    $(this).attr('checked', false);
                });
            }
        }

        function BindDealTypeSub(obj) {
            $.ajax({
                type: "POST",
                url: "BusinessContent.aspx/BindDealTypeSubList",
                data: "{'pid': '" + $(obj).val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var sub = $('#ddlDealTypeSub');
                    $(sub).find('option').remove();
                    $.each(response.d, function (index, item) {
                        $(sub).append($('<option></option>').attr('value', item.CodeId).text(item.CodeName));
                    });
                    if ($('#hidDealTypeSub').val() != '') {
                        $(sub).val($('#hidDealTypeSub').val());
                    }
                    SetDealTypeSub(sub);
                }
            });
        }

        function SetDealTypeSub(obj) {
            $('#hidDealTypeSub').val($(obj).val());
        }

        function CheckAll(obj) {

            var id = $(obj).attr('id');

            $('#pponstore').find('[id*=chk' + id + ']').each(function () {
                if ($(obj).is(':checked') && $(this).parent().next().next().find('[id*=pAddress]').text() != '') {
                    $(this).attr('checked', 'checked');
                } else {
                    $(this).removeAttr('checked');
                }
            });

            $('#vbs').find('[id$=chk' + id + ']').each(function () {
                if ($(obj).is(':checked') && !$(this).is(':disabled')) {
                    $(this).attr('checked', 'checked');
                } else {
                    $(this).removeAttr('checked');
                }
            });
        }

        function CheckAll2(id) {
            //檢核checkAll
            var TotalCount = 0;
            var CheckCount = 0;
            $('[id$=chk' + id + ']').each(function () {

                if (!$(this).is(':disabled'))
                    TotalCount++;
                if ($(this).is(':checked'))
                    CheckCount++;
            });
            if (TotalCount == CheckCount && CheckCount != 0 && TotalCount != 0)
                $('#' + id).attr('checked', true);
            else
                $('#' + id).attr('checked', false);

        }

        function SelectTab(obj) {
            if ($(obj).find('.mc-navbtn').hasClass('on')) {
                return;
            } else {
                var div = $(obj).attr('tab');
                $('.mc-navbtn').each(function () {
                    $(this).removeClass('on');
                });
                $(obj).find('.mc-navbtn').addClass('on');
                $('.mc-content').hide();
                $('#' + div).show();
                if (div == 'ContentEditor') {
                    $('#navContentDescription').trigger('click');
                }
            }
        }

        function ValidateNumber(e, pnumber) {
            if (!/^\d+$/.test(pnumber)) {
                $(e).val(/^\d+/.exec($(e).val()));
            }
            return false;
        }

        ///計算毛利率
        function GetGrossMargin() {
            var gross = $('#hidGrossMargin').val();
            $('#grossMargin').html(gross);
        }

        function ChannelSaveCheck() {
            if ($('[id*=chkCategory]:checked').length == 0) {
                alert('上檔頻道至少需勾選一個分類。');
                return false;
            } else {
                return true;
            }
        }

        function ChannelSaveCheck2() {
            var isHasChannel = false;
            var isHasArea = false;
            var isHasCommercial = false;
            var isHasCategory = false;
            var isHasSubDealCategory = false;

            var checkChannelNumber = 0;
            var checkAreaNumber = 0;
            var checkCommercialNumber = 0;
            var checkCategoryNumber = 0;
            var checkSubDealCategoryNumber = 0;
            $('.pChannel').each(function () {
                if ($(this).is(":visible")) {
                    isHasChannel = true;
                    checkChannelNumber += $(this).find('input[type="checkbox"][id*="chkChannel"]:checked').length;
                }
            });

            $('.pA').each(function () {
                if ($(this).is(":visible")) {
                    isHasArea = true;
                    checkAreaNumber += $(this).find('input[type="checkbox"][id*="chkArea"]:checked').length;
                }
            });

            $('[class*=pCommercial]').each(function () {
                if ($(this).is(":visible")) {
                    isHasCommercial = true;
                    checkCommercialNumber += $(this).find('input[type="checkbox"][id*="chkCommercial"]:checked').length;
                }
            });

            $('.pB').each(function () {
                if ($(this).is(":visible")) {
                    isHasCategory = true;
                    checkCategoryNumber += $(this).find('input[type="checkbox"][id*="chkCategory"]:checked').length;
                }
            });

            $('.pCategory').each(function () {
                if ($(this).is(":visible")) {
                    isHasSubDealCategory = true;
                    checkSubDealCategoryNumber += $(this).find('input[type="checkbox"][id*="chkSubDealCategory"]:checked').length;
                }
            });

            if (isHasChannel && checkChannelNumber <= 0) {
                if (!alert("請勾選頻道!")) {
                    return false;
                }
            }
            else if (isHasArea && checkAreaNumber <= 0) {
                if (!alert("請勾選區域!")) {
                    return false;
                }
            }
            else if (isHasCategory && checkCategoryNumber <= 0) {
                if (!alert("請勾選分類!")) {
                    return false;
                }
            }
            else if (isHasSubDealCategory && checkSubDealCategoryNumber <= 0) {
                if (!alert("請勾選子分類!")) {
                    return false;
                }
            }
            else if (isHasCommercial && checkCommercialNumber <= 0) {
                if (!confirm("此檔未勾選商圈喔!確定要存檔?")) {
                    return false;
                }
                else
                    return true;
            }
            else
                return true;
        }

        function ChangeVendorReceiptType(obj) {
            $('#chkInputTax').attr('checked', false);
            if ($(obj).val() == '<%= (int)VendorReceiptType.Other %>') {
                $('#txtVendorReceiptMessage').show();
            } else {
                $('#txtVendorReceiptMessage').hide();
            }
        }

        function SetCKEditor() {
            var editorDesc;
            var editorRemark;
            if ($.query.get('bid') !== '') {
                var bid = $.query.get('bid');            
                var isPiinlife = $('#divChannelList').find('label:contains(品生活)').siblings(':checkbox').is(':checked');
                var maxwidth = isPiinlife ? 650 : 1024; //品生活圖片自動縮至650，一般好康縮為560
                var imageupload = '/Tools/ckeditor/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images&bid=' + bid + '&maxwidth=' + maxwidth;
                var uploadUrl = '/Tools/ckeditor/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files&bid=' + bid + '&responseType=json&maxwidth=' + maxwidth;
                editorDesc = CKEDITOR.replace('tbEditorDesc', {
                    uploadUrl: uploadUrl,
                    filebrowserImageUploadUrl: imageupload,
                    height: 1000
                });

                editorRemark = CKEDITOR.replace('tbEditorRemark', {
                    uploadUrl: uploadUrl,
                    filebrowserImageUploadUrl: imageupload,
                    height: 200
                });
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <asp:PlaceHolder ID="divMain" runat="server" Visible="false">
        <div id="btn-menu">
            <asp:Button ID="btnFloatSave" runat="server" CssClass="btn btn-primary rd-mcacstbtn btn-circle" Text="儲存" OnClick="btnSave_Click" OnClientClick="return SaveCheck('SaveCheck');" />
        </div>
        <div class="mc-navbar" style="display: block;">
            <a onclick="SelectTab(this);" tab="MainSet">
                <div class="mc-navbtn on">
                    優惠內容
                </div>
            </a><a onclick="SelectTab(this);" tab="MultiDealSet" id="aMultiDeals" runat="server" visible="false">
                <div class="mc-navbtn">
                    多檔次設定
                </div>
            </a><a onclick="SelectTab(this);" tab="PponStoreSet" class="ppon" id="aPponStore" runat="server">
                <div class="mc-navbtn">
                    營業據點
                </div>
            </a><a onclick="SelectTab(this);" tab="VbsSet" runat="server">
                <div class="mc-navbtn">
                    核銷相關
                </div>
            </a>
            <a id="a2" runat="server" onclick="SelectTab(this);" tab="ContentEditor">
                <div class="mc-navbtn">
                    內容設定
                </div>
            </a>
            <a id="aHistory" runat="server" onclick="SelectTab(this);" tab="HistorySet" visible="false">
                <div class="mc-navbtn">
                    變更紀錄
                </div>
            </a>
        </div>
        <div id="MainSet" class="mc-content">
            <div id="basic">
                <h1 class="rd-smll">
                    <span class="breadcrumb flat">
                        <a href="#">
                            <asp:Literal ID="liItemName" runat="server"></asp:Literal></a>
                        <a href="#" class="active">建檔功能平台</a>
                    </span>
                    <a href="BusinessList.aspx" style="color: #08C;">回列表</a>
                </h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            商家資訊</label>
                        <div class="data-input rd-data-input">
                            <p>
                                <asp:Literal ID="liSellerName" runat="server"></asp:Literal>
                            </p>
                            <p>
                                <asp:HyperLink ID="hkSellerId" runat="server" Target="_blank"></asp:HyperLink>
                            </p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            上檔日期</label>
                        <div class="data-input rd-data-input">
                            <p>
                                <asp:Literal ID="liBusinessOrderTime" runat="server"></asp:Literal>
                            </p>
                            <asp:Button ID="btnReCreateProposal" runat="server" CssClass="btn btn-small form-inline-btn" Text="複製提案單" OnClick="btnReCreateProposal_Click" Visible="false" />
                            <%--<asp:Button ID="btnReCreateSameProposal" runat="server" CssClass="btn btn-small  btn-primary form-inline-btn" Text="完全複製檔" OnClick="btnReCreateSameProposal_Click" Visible="false" />--%>
                        </div>
                    </div>
                    <asp:Panel ID="divProposalInfo" runat="server" CssClass="form-unit" Visible="false">
                        <label class="unit-label rd-unit-label">
                            串接資訊</label>
                        <div class="data-input rd-data-input">
                            <p>單號:</p>
                            <p>
                                <asp:HyperLink ID="hkProposalId" runat="server" Target="_blank"></asp:HyperLink>
                            </p>
                            <p>
                                <asp:Button ID="btnSettingCheck" runat="server" CssClass="btn btn-primary btn-small form-inline-btn" Text="送出排檔" OnClick="btnSettingCheck_Click" OnClientClick="return SettingCheck();" Visible="false" />
                            </p>
                            <p>
                                <asp:HyperLink ID="hkSetUp" runat="server" Target="_blank" Text="後台" Visible="false"></asp:HyperLink>
                            </p>
                            <p>
                                <asp:HyperLink ID="hkPreview" runat="server" Target="_blank" Text="預覽" Visible="false"></asp:HyperLink>
                            </p>
                            <asp:PlaceHolder ID="divReferrerBusinessHour" runat="server" Visible="false">
                                <br />
                                <p style="color: blue;">再次上檔:</p>
                                <p style="color: blue;">
                                    <asp:HyperLink ID="hkReferrerBusinessHour" runat="server" Target="_blank"></asp:HyperLink>
                                </p>
                            </asp:PlaceHolder>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="divSalesInfo" runat="server" CssClass="form-unit" Visible="false">
                        <label class="unit-label rd-unit-label">
                            負責業務</label>
                        <div class="data-input rd-data-input">
                            <table>
                                <tr>
                                    <td style="width: 80px">
                                        <asp:Literal ID="liDevelopeSalesName" runat="server"></asp:Literal>

                                    </td>
                                    <td style="width: 250px">
                                        <asp:Literal ID="liDevelopeSalesEmail" runat="server"></asp:Literal>

                                    </td>
                                    <td>
                                        <asp:PlaceHolder ID="divDevelopeReferral" runat="server" Visible="false">
                                            <p>
                                                <asp:TextBox ID="txtDevelopeReferralEmail" runat="server" CssClass="input-small" Style="width: 200px"></asp:TextBox>
                                                <asp:Button ID="btnDevelopeReferral" runat="server" CssClass="btn btn-small btn-primary" Text="轉件" OnClick="btnDevelopeReferral_Click" />
                                            </p>
                                        </asp:PlaceHolder>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 80px">
                                        <asp:Literal ID="liOperationSalesName" runat="server"></asp:Literal>

                                    </td>
                                    <td style="width: 250px">
                                        <asp:Literal ID="liOperationSalesEmail" runat="server"></asp:Literal>

                                    </td>
                                    <td>
                                        <asp:PlaceHolder ID="divOperationReferral" runat="server" Visible="false">
                                            <p>
                                                <asp:TextBox ID="txtOperationReferralEmail" runat="server" CssClass="input-small" Style="width: 200px" ClientIDMode="Static"></asp:TextBox>
                                                <asp:Button ID="btnOperationReferral" runat="server" CssClass="btn btn-small btn-primary form-inline-btn" Text="轉件" OnClick="btnOperationReferral_Click" />
                                            </p>
                                        </asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <br />
            <div id="deal">
                <h1 class="rd-smll">優惠內容</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            類型</label>
                        <div class="data-input rd-data-input">
                            <p>
                                <asp:HiddenField ID="hidDeliveryType" runat="server" ClientIDMode="Static" />
                                <asp:Literal ID="liDelivery" runat="server"></asp:Literal>
                                <asp:HiddenField ID="hidProposalDealType" runat="server" ClientIDMode="Static" />
                                <asp:CheckBox ID="chkConsignment" runat="server" ClientIDMode="Static" Visible="false" Text="商品寄倉" />
                            </p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            專案內容</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtItemName" runat="server" ClientIDMode="Static" Width="60%" Enabled="false" Style="cursor: not-allowed;"></asp:TextBox>
                            <p id="pItemName" class="if-error" style="display: none; margin-top: 8px">請輸入專案內容。</p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            價格資訊</label>
                        <div class="data-input rd-data-input">
                            <p>原價</p>
                            <asp:TextBox ID="txtOrigPrice" runat="server" CssClass="input-small" ClientIDMode="Static" onkeyup="return ValidateNumber($(this),value);" MaxLength="5" Enabled="false" Style="cursor: not-allowed;"></asp:TextBox>
                            <p>售價</p>
                            <asp:TextBox ID="txtItemPrice" runat="server" autocomplete="off" CssClass="input-small" ClientIDMode="Static" onkeyup="ValidateNumber($(this),value); GetGrossMargin();" MaxLength="5" Enabled="false" Style="cursor: not-allowed;"></asp:TextBox>
                            <br />
                            <p>進貨價</p>
                            <asp:TextBox ID="txtCost" runat="server" autocomplete="off" CssClass="input-small" ClientIDMode="Static" onkeyup="ValidateNumber($(this),value); GetGrossMargin();" MaxLength="5" Enabled="false" Style="cursor: not-allowed;"></asp:TextBox>
                            <p>
                                <p style="color: #BF0000">毛利率</p>
                                <p id="grossMargin" style="color: #BF0000">0</p>
                                <p style="color: #BF0000; margin-left: -8px;">%</p>
                                <asp:HiddenField ID="hidGrossMargin" runat="server" ClientIDMode="Static" Value="" />
                            </p>
                            <br />
                            <p>上架費(份數)</p>
                            <asp:TextBox ID="txtSlottingFeeQuantity" runat="server" CssClass="input-small" ClientIDMode="Static" onkeyup="return ValidateNumber($(this),value);" MaxLength="5" Enabled="false" Style="cursor: not-allowed;"></asp:TextBox>
                            <p id="pItemPrice" class="if-error" style="display: none;">請輸入優惠活動內容。</p>
                            <p id="pCost" class="if-error" style="display: none;">進貨價不可為0。</p>
                            <asp:HiddenField ID="hidCPCBuy" runat="server" Value="0" ClientIDMode="Static" />
                            <asp:HiddenField ID="hidCPCPresent" runat="server" Value="0" ClientIDMode="Static" />
                        </div>
                    </div>
                    <asp:PlaceHolder ID="divFreight" runat="server" Visible="false">
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                運費</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="txtFreight" runat="server" CssClass="input-small" ClientIDMode="Static" onkeyup="return ValidateNumber($(this),value);" MaxLength="5" Enabled="false" Style="cursor: not-allowed;"></asp:TextBox>
                                <p>免運門檻</p>
                                <asp:TextBox ID="txtNonFreightLimit" runat="server" CssClass="input-small" ClientIDMode="Static" onkeyup="return ValidateNumber($(this),value);" MaxLength="5" Enabled="false" Style="cursor: not-allowed;"></asp:TextBox><p>(元)</p>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>
            <div id="Quantity">
                <h1 class="rd-smll">活動人數及限制</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            最大購買數量</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtOrderTotalLimit" ClientIDMode="Static" runat="server" CssClass="input-small" onkeyup="return ValidateNumber($(this),value);" MaxLength="5" Enabled="false" Style="cursor: not-allowed;"></asp:TextBox>
                            <p>每人限購</p>
                            <asp:TextBox ID="txtDailyAmount" runat="server" CssClass="input-small" ClientIDMode="Static" onkeyup="return ValidateNumber($(this),value);" MaxLength="2" Enabled="false" Style="cursor: not-allowed;"></asp:TextBox>
                            <p id="pQuantity" class="if-error" style="display: none; margin-top: 8px">請輸入購買數量限制。</p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            ATM保留份數</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtAtmMaximum" runat="server" CssClass="input-small" onkeyup="return ValidateNumber($(this),value);" MaxLength="5" Enabled="false" Style="cursor: not-allowed;"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div id="ChannelSet">
                <h1 class="rd-smll">上檔頻道</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div id="divChannelList" class="grui-form">
                        <asp:Repeater ID="rptChannel" runat="server" OnItemDataBound="rptChannel_ItemDataBound">
                            <ItemTemplate>
                                <div class="form-unit">
                                    <label class="unit-label rd-unit-label pChannel" style="float: none">
                                        <asp:HiddenField ID="hidChannel" runat="server" />
                                        <asp:CheckBox ID="chkChannel" runat="server" onclick="selectChannel(this);" />
                                    </label>
                                    <div class="data-input rd-data-input pArea" style="display: none;">
                                        <ul>
                                            <asp:Repeater ID="rptArea" runat="server" OnItemDataBound="rptArea_ItemDataBound">
                                                <HeaderTemplate>
                                                    <li style="border-bottom: 1px solid #e4e4e4;">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <p class="pA">
                                                        <asp:HiddenField ID="hidArea" runat="server" />
                                                        <asp:CheckBox ID="chkArea" runat="server" onclick="selectArea(this);" />
                                                    </p>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </li>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            <asp:Repeater ID="rptArea2" runat="server" OnItemDataBound="rptArea2_ItemDataBound">
                                                <HeaderTemplate>
                                                    <table>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr style="background-color: #f2f2f0">
                                                        <td colspan="2">
                                                            <p class="pCommercial<%#((CategoryNode)Container.DataItem).CategoryId %>" style="display: none;">
                                                                <label><%# ((CategoryNode)Container.DataItem).NodeDatas.Count > 0 ? ((CategoryNode)Container.DataItem).NodeDatas.FirstOrDefault().CategoryNodes.FirstOrDefault().CategoryName : "商圈‧景點：" %></label>
                                                                <asp:Repeater ID="rptCommercial" runat="server" OnItemDataBound="rptCommercial_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hidCommercial" runat="server" />
                                                                        <asp:CheckBox ID="chkCommercial" runat="server" onclick="selectCommercial(this);" />
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td style="width: 90%">
                                                            <asp:Repeater ID="rptSPRegion" runat="server" OnItemDataBound="rptSPRegion_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <p class="pSpecialRegion<%#((CategoryNode)Container.DataItem).CategoryId %>" style="display: none;">
                                                                        <asp:Repeater ID="rptSpecialRegion" runat="server" OnItemDataBound="rptSpecialRegion_ItemDataBound">
                                                                            <ItemTemplate>
                                                                                <asp:HiddenField ID="hidSpecialRegion" runat="server" />
                                                                                <asp:CheckBox ID="chkSpecialRegion" runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </p>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            <asp:Repeater ID="rptCategory" runat="server" OnItemDataBound="rptCategory_ItemDataBound">
                                                <HeaderTemplate>
                                                    <li style="border-bottom: 1px solid #e4e4e4;">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <p class="pB">
                                                        <asp:HiddenField ID="hidCategory" runat="server" />
                                                        <asp:CheckBox ID="chkCategory" runat="server" onclick="selectCategory(this);" />
                                                        <asp:HiddenField ID="hidIsShowBackEnd" runat="server" />
                                                    </p>
                                                    <p class="pCategory" style="display: none;">
                                                        <asp:Repeater runat="server" ID="rptSubDealCategoryArea" OnItemDataBound="rptSubDealCategoryArea_ItemDataBound">
                                                            <ItemTemplate>
                                                                <span>
                                                                    <asp:HiddenField ID="hidSubIsShowBackEnd" runat="server" />
                                                                    <asp:HiddenField ID="hidSubDealCategory" runat="server" />
                                                                    <asp:CheckBox ID="chkSubDealCategory" runat="server" />
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </p>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </li>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
            <div id="Ppon" class="VerifyType" style="display: none">
                <h1 class="rd-smll">核銷方式</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <ul style="list-style-type: none; padding-left: 20px;">
                        <li>
                            <asp:RadioButton ID="rdlOnlineUse" runat="server" Text="即買即用" GroupName="VerifyType"
                                onclick="OnlineUse(this);" /><br />
                            <div style="padding-left: 20px; font-size: 12px; padding-right: 20px; color: Gray; font-weight: normal;">
                                <%= LunchKingSite.I18N.Message.SalesBusinessOrderPponVerifyTypeOnlineUse %>
                            </div>
                        </li>
                        <li>
                            <asp:RadioButton ID="rdlOnlineOnly" runat="server" Text="線上核銷" GroupName="VerifyType"
                                onclick="OnlineUse(this);"
                                Checked="true" /><br />
                            <div id="OnlineOnlyText" style="padding-left: 20px; font-size: 12px; padding-right: 20px; color: Gray; font-weight: normal;">
                                <%= LunchKingSite.I18N.Message.SalesBusinessOrderPponVerifyTypeOnlineOnly %>
                            </div>
                            <span style="display: none">
                                <asp:TextBox ID="hidOnlineOnly" runat="server"></asp:TextBox></span> </li>
                    </ul>
                </div>
            </div>
            <div id="sales">
                <h1 class="rd-smll">銷售分析</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            館別</label>
                        <div class="data-input rd-data-input">
                            <p>
                                <asp:DropDownList ID="ddlAccBusinessGroup" runat="server"></asp:DropDownList>
                            </p>
                            <p id="pAccBusinessGroup" class="if-error" style="display: none; margin-top: 8px">請輸入購買數量限制。</p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            業績歸屬</label>
                        <div class="data-input rd-data-input">
                            <p>
                                <asp:DropDownList ID="ddlDeptId" runat="server"></asp:DropDownList>
                            </p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            分類</label>
                        <div class="data-input rd-data-input">
                            <p>
                                <asp:DropDownList ID="ddlDealType" runat="server" ClientIDMode="Static" onchange="BindDealTypeSub(this);" AppendDataBoundItems="true" Width="100px">
                                    <asp:ListItem Text="請選擇" Value="-1"></asp:ListItem>
                                </asp:DropDownList>

                            </p>
                            <p>
                                <asp:DropDownList ID="ddlDealTypeSub" runat="server" ClientIDMode="Static" onchange="SetDealTypeSub(this);" Width="180px"></asp:DropDownList>
                                <asp:HiddenField ID="hidDealTypeSub" ClientIDMode="Static" runat="server" />
                                <asp:HiddenField ID="hidDealType" ClientIDMode="Static" runat="server" />
                            </p>
                            <p id="pDealTypeSub" class="if-error" style="display: none; margin-top: 8px">請選擇分類。</p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            搜尋關鍵字</label>
                        <div class="data-input rd-data-input">
                            <p>
                                <asp:TextBox ID="txtKeyword" runat="server" CssClass="input-half" Width="200px" ClientIDMode="Static"></asp:TextBox>
                            </p>
                            <p id="pKeyword" class="if-error" style="display: none; margin-top: 8px">請輸入搜尋關鍵字。</p>
                            <p class="note" style="line-height: 20px;">
                                ●  建議輸入內容 (請以 " / " 分隔)：<br />
                                1. 品牌名稱：建議針對品牌名稱輸入，且可拆開輸入會更容易搜尋的到，如義大天悅可輸入：義大/天悅<br />
                                2. 拆字分解：盡量地將字詞拆開，如義美巧克力酥片，可以拆分為 義美/巧克力/巧克力酥片<br />
                                3. 相關連結：若此檔次與特殊情境相關，可額外輸入，如宜蘭住宿檔次，可輸入＂童玩節＂等類似字詞<br />
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="finance">
                <h1 class="rd-smll">核銷與帳務設定</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            出帳方式</label>
                        <div class="data-input rd-data-input">
                            <p>
                                <asp:DropDownList ID="ddlRemittanceType" runat="server" Width="160px"></asp:DropDownList>&nbsp;


                                <asp:RadioButtonList ID="radioRemittanceType" runat="server" RepeatDirection="horizontal"></asp:RadioButtonList>

                             
                                <asp:HiddenField ID="hidRemittanceType" ClientIDMode="Static" runat="server" />
                                <asp:Panel ID="chkMohistPanel" runat="server" class="form-Panel" Visible="false">
                                    <asp:CheckBox ID="chkMohist" runat="server"></asp:CheckBox>墨攻
                                </asp:Panel>
                            </p>
                            <p id="pRemittanceType" class="if-error" style="display: none; margin-top: 8px">請選擇出帳方式。</p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            店家單據開立方式</label>
                        <div class="data-input rd-data-input">
                            <p>
                                <asp:DropDownList ID="ddlVendorReceiptType" runat="server" Width="160px" onchange="ChangeVendorReceiptType(this);" ClientIDMode="Static"></asp:DropDownList>
                            </p>
                            <p>
                                <asp:TextBox ID="txtVendorReceiptMessage" runat="server" Width="150px" ClientIDMode="Static" Style="display: none;" onblur="CheckVendorReceiptType();"></asp:TextBox>
                            </p>
                            <p id="pVendorReceiptType" class="if-error" style="display: none; margin-top: 8px">請輸入店家單據開立方式。</p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            免稅設定</label>
                        <div class="data-input rd-data-input">
                            <asp:CheckBox ID="chkInputTax" runat="server" Text="店家發票免稅" ClientIDMode="Static" />
                            <asp:CheckBox ID="chkNoTax" runat="server" Text="開立免稅發票給消費者" ClientIDMode="Static" />
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div id="AncestorBid">
                <h1 class="rd-smll">檔次續接設定</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            續接數量檔次</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtAncestorBid" runat="server" Width="250px"></asp:TextBox>
                            <p>
                                接續數量:&nbsp;<asp:Literal ID="liAncestorBidQty" runat="server"></asp:Literal>
                            </p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label class="unit-label rd-unit-label">
                            續接憑證檔次</label>
                        <div class="data-input rd-data-input">
                            <asp:TextBox ID="txtAncestorSeqBid" runat="server" Width="250px"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div id="RestrictionsSet">
                <h1 class="rd-smll">權益說明
                    <asp:HyperLink ID="hkRestrictionsEdit" runat="server" ClientIDMode="Static" ForeColor="#0088cc" Font-Size="Medium" Text="編輯" CssClass="Restrictions fancybox.iframe"></asp:HyperLink>
                </h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit">
                        <div class="data-input rd-data-input" style="margin-left: 0px">
                            <p style="font-size: small; line-height: 20px;">
                                <asp:Label ID="liRestrictions" runat="server" ClientIDMode="Static"></asp:Label>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div id="save" class="form-unit">
                <div class="grui-form">
                    <label class="unit-label rd-unit-label"></label>
                    <div class="form-unit end-unit">
                        <div class="data-input rd-data-input">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="儲存" OnClick="btnSave_Click" OnClientClick="return SaveCheck('SaveCheck');" />
                            <p id="message" class="if-error">
                                <asp:Literal ID="liMessage" runat="server"></asp:Literal>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hidNoRestrictedStore" runat="server" ClientIDMode="Static" Value="0" />
            <br />
        </div>

        <div id="MultiDealSet" class="mc-content" style="display: none;">
            <h1 class="rd-smll"><span class="breadcrumb flat">
                <a href="#">多檔次設定</a>
                <a href="#" class="active">建檔功能平台</a>
            </span>
                <a href="BusinessList.aspx" style="color: #08C;">回列表</a>
            </h1>
            <hr class="header_hr">
            <asp:Panel ID="divMultiDeals" runat="server" Visible="false" ClientIDMode="Static">
                <div id="mc-table">
                    <asp:Repeater ID="rptMultiDeal" runat="server" OnItemDataBound="rptMultiDeal_ItemDataBound">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                                <tr class="rd-C-Hide">
                                    <th class="OrderDate">商品名稱
                                    </th>
                                    <th class="OrderDate">原價
                                    </th>
                                    <th class="OrderSerial">售價
                                    </th>
                                    <th class="OrderCost">進貨價
                                    </th>
                                    <th class="OrderDate">最大購買
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="mc-tableContentITEM">
                                <td class="OrderDate" style="max-width: 300px; text-align: left">
                                    <span>
                                        <%# ((ViewComboDeal)Container.DataItem).Title %>
                                    </span>
                                </td>
                                <td class="OrderDate">
                                    <span class="rd-Detailtitle">原價：</span>
                                    <span>
                                        <%# ((ViewComboDeal)Container.DataItem).ItemOrigPrice.ToString("N0") %>
                                    </span>
                                </td>
                                <td class="OrderSerial">
                                    <span class="rd-Detailtitle">售價：</span>
                                    <span>
                                        <%# ((ViewComboDeal)Container.DataItem).ItemPrice.ToString("N0") %>
                                    </span>
                                </td>
                                <td class="OrderSerial">
                                    <span class="rd-Detailtitle">進貨價：</span>
                                    <asp:Literal ID="lblBid" runat="server" Text="<%# ((ViewComboDeal)Container.DataItem).BusinessHourGuid %>" Visible="false"></asp:Literal>
                                    <asp:Label ID="lblCost" runat="server" Text="0"></asp:Label>
                                    </span>
                                </td>
                                <td class="OrderDate">
                                    <span class="rd-Detailtitle">最大購買：</span>
                                    <span>
                                        <%# ((ViewComboDeal)Container.DataItem).OrderTotalLimit.HasValue ? ((ViewComboDeal)Container.DataItem).OrderTotalLimit.Value.ToString("N0") : string.Empty %>
                                    </span>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </asp:Panel>
        </div>

        <div id="PponStoreSet" class="mc-content" style="display: none;">
            <asp:PlaceHolder ID="divPponStore" runat="server" Visible="false">
                <h1 class="rd-smll"><span class="breadcrumb flat">
                    <a href="#">分店設定</a>
                    <a href="#" class="active">建檔功能平台</a>
                </span>
                    <a href="BusinessList.aspx" style="color: #08C;">回列表</a>
                </h1>
                <hr class="header_hr">
                <div id="mc-table" class="showtr">
                    <asp:Repeater ID="rptPponStore" runat="server" OnItemDataBound="rptPponStore_ItemDataBound">
                        <HeaderTemplate>
                            <table id="pponstore" width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                                <tr class="rd-C-Hide">
                                    <th class="OrderDate">
                                        <input type="checkbox" onclick="CheckAll(this);" id="Check" />
                                    </th>
                                    <th class="OrderDate">營業據點名稱
                                    </th>
                                    <th class="OrderSerial">地址
                                    </th>
                                    <th class="OrderName">營業時間
                                    </th>
                                    <th class="OrderDate">公休日
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="mc-tableContentITEM">
                                <td class="OrderDate">
                                    <asp:HiddenField ID="hidStoreGuid" runat="server" />
                                    <asp:CheckBox ID="chkCheck" runat="server" />
                                </td>
                                <td class="OrderDate storename" style="text-align: left">
                                    <asp:Literal ID="liStoreName" runat="server"></asp:Literal>
                                </td>
                                <td class="OrderSerial" style="text-align: left">
                                    <p id="pAddress">
                                        <asp:Literal ID="liAddress" runat="server"></asp:Literal>
                                    </p>
                                </td>
                                <td class="OrderName" style="text-align: left">
                                    <span class="rd-Detailtitle">營業時間：</span>
                                    <asp:Literal ID="liOpenTime" runat="server"></asp:Literal>
                                </td>
                                <td class="OrderDate" style="text-align: left">
                                    <span class="rd-Detailtitle">公休日：</span>
                                    <asp:Literal ID="liCloseDate" runat="server"></asp:Literal>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="form-unit">
                    <div class="grui-form">
                        <label class="unit-label rd-unit-label"></label>
                        <div class="form-unit end-unit">
                            <div class="data-input rd-data-input">
                                <asp:Button ID="btnPponStoreSave" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="儲存" OnClick="btnPponStoreSave_Click" OnClientClick="return LocationSaveCheck();" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>

        <div id="VbsSet" class="mc-content" style="display: none;">
            <asp:PlaceHolder ID="divVbs" runat="server">
                <h1 class="rd-smll">
                    <span class="breadcrumb flat">
                        <a href="#">核銷相關</a>
                        <a href="#" class="active">建檔功能平台</a>
                    </span>
                    <asp:CheckBox ID="chkIsShowVbsAccountID" runat="server" Text="顯示商家帳號" ClientIDMode="Static" />
                    <a href="BusinessList.aspx" style="color: #08C;">回列表</a>
                </h1>
                <hr class="header_hr">
                <div id="mc-table">
                    <asp:Repeater ID="rptVbs" runat="server" OnItemDataBound="rptVbs_ItemDataBound">
                        <HeaderTemplate>
                            <table id="vbs" width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                                <tr class="rd-C-Hide">
                                    <th class="OrderDate"></th>
                                    <th class="OrderSerial">已售/上限
                                    </th>
                                    <th class="OrderName">
                                        <input type="checkbox" onclick="CheckAll(this);" id="VerifyShop" />
                                        銷售店舖
                                    </th>
                                    <th class="OrderName">
                                        <input type="checkbox" onclick="CheckAll(this);" id="Verify" />
                                        憑證核實
                                    </th>
                                    <th class="OrderDate">
                                        <input type="checkbox" onclick="CheckAll(this);" id="Accouting" />
                                        匯款對象
                                    </th>
                                    <th class="OrderDate">
                                        <input type="checkbox" onclick="CheckAll(this);" id="ViewBalanceSheet" />
                                        檢視對帳單
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="mc-tableContentITEM">
                                <td style="text-align: left">
                                    <asp:Literal ID="liStoreName" runat="server"></asp:Literal>
                                    <br />
                                    <span id="IsShowVbsAccountID" style="display: none">
                                        <asp:Literal ID="liVbsAccountID" runat="server"></asp:Literal>
                                        <asp:Panel ID="divtooltip" runat="server" CssClass="tooltip" Visible="false">
                                            ...
                                        <asp:Literal ID="liMoreVbsAccountID" runat="server"></asp:Literal>
                                        </asp:Panel>
                                    </span>
                                    <asp:HiddenField ID="hidStoreGuid" runat="server" />
                                </td>
                                <td class="OrderDate">
                                    <asp:TextBox ID="txtQuantity" runat="server" Width="50px" onkeyup="return ValidateNumber($(this),value);" MaxLength="5"></asp:TextBox>
                                </td>
                                <td class="OrderDate">
                                    <span class="rd-Detailtitle">銷售店舖</span>
                                    <asp:CheckBox ID="chkVerifyShop" runat="server" />
                                </td>
                                <td class="OrderDate">
                                    <span class="rd-Detailtitle">憑證核實</span>
                                    <asp:CheckBox ID="chkVerify" runat="server" />
                                </td>
                                <td class="OrderDate">
                                    <span class="rd-Detailtitle">對帳核銷</span>
                                    <asp:CheckBox ID="chkAccouting" runat="server" />
                                </td>
                                <td class="OrderDate">
                                    <span class="rd-Detailtitle">檢視對帳單</span>
                                    <asp:CheckBox ID="chkViewBalanceSheet" runat="server" CssClass="checkViewBalanceSheet" />
                                    <asp:Image ID="btnHideBalanceSheet" runat="server" CssClass="hideBalanceSheet" />
                                    <asp:HiddenField ID="hidIsHideBalanceSheet" runat="server" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="form-unit">
                    <div class="grui-form">
                        <label class="unit-label rd-unit-label"></label>
                        <div class="form-unit end-unit">
                            <div class="data-input rd-data-input">
                                <asp:Button ID="btnVbsSave" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="儲存" OnClick="btnVbsSave_Click" OnClientClick="return VbsSaveCheck();" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>
        <div id="ContentEditor" class="mc-content" style="display: none;">
            <h1 class="rd-smll">
                <span class="breadcrumb flat">
                    <a href="#">內容設定</a>
                    <a href="#" class="active">建檔功能平臺</a>
                </span>
                <a href="BusinessList.aspx" style="color: #08C;">回列表</a>
            </h1>
            <hr class="header_hr">
            <div class="showtr">
                <div class="subNav">
                    <ul class="tabnav nostyle clearfix">
                        <li><a id="navContentDescription" href="#" class="active" tab="divContentDescription">詳細介紹</a></li>
                        <li><a id="navContentRemark" href="#" tab="divContentRemark">一姬說好康</a></li>
                    </ul>
                </div>
                <div id="divContentDescription">
                    <div>
                        <asp:TextBox ID="tbEditorDesc" runat="server" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                    </div>
                </div>
                <div id="divContentRemark">
                    <div>
                        <asp:TextBox ID="tbEditorRemark" runat="server" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <asp:PlaceHolder ID="divHistory" runat="server" Visible="false">
            <div id="HistorySet" class="mc-content" style="display: none;">
                <h1 class="rd-smll"><span class="breadcrumb flat">
                    <a href="#">變更紀錄</a>
                    <a href="#" class="active">建檔功能平台</a>
                </span>
                    <a href="BusinessList.aspx" style="color: #08C;">回列表</a>
                </h1>
                <hr class="header_hr">
                <div id="divHistoryList" class="grui-form">
                    <div class="form-unit">
                        <div class="data-input rd-data-input" style="margin-left: 70px">
                            <asp:TextBox ID="txtChangeLog" runat="server" CssClass="input-half" ClientIDMode="Static" Style="margin-top: 10px"></asp:TextBox>
                            <asp:Button ID="btnChangeLog" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="送出" OnClick="btnChangeLog_Click" OnClientClick="return CheckLog();" />
                            <p id="pChangeLog" class="if-error" style="display: none; margin-top: 8px">請輸入內容。</p>
                        </div>
                        <asp:Repeater ID="rptLog" runat="server">
                            <HeaderTemplate>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li>
                                    <label class="unit-label rd-unit-label">
                                        <%# ((BusinessChangeLog)Container.DataItem).CreateId.Replace("@17life.com.tw", string.Empty).Replace("@17life.com", string.Empty) %>&nbsp;&nbsp;:
                                    </label>
                                    <div class="data-input rd-data-input">
                                        <p style="max-width: 520px;">
                                            <%# ((BusinessChangeLog)Container.DataItem).ChangeLog.Replace("|", "<br />") %>
                                        </p>
                                        <p class="note" style="float: right;"><%# ((BusinessChangeLog)Container.DataItem).CreateTime.ToString("yyyy/MM/dd HH:mm") %></p>
                                        <p class="note" style="float: right; color: #BF0000;"><%# ((BusinessChangeLog)Container.DataItem).Dept %></p>
                                    </div>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
    </asp:PlaceHolder>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

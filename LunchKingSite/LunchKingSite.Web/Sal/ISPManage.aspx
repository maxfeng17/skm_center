﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="ISPManage.aspx.cs" Inherits="LunchKingSite.Web.Sal.ISPManage" %>

<%@ Register Src="../User/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Src="~/Sal/ProposalMenu.ascx" TagName="ProposalMenu" TagPrefix="uc2" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css") %>" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .mc-navbar .mc-navbtn {
            width: 10%;
        }

        #ui-datepicker-div {
            position: absolute;
            top: 0px;
            left: 559px;
            z-index: 1;
            display: none;
        }

        .ui-datepicker {
            width: 300px;
            padding: .1em .1em 0;
        }

            .ui-datepicker table {
                width: 100%;
                font-size: .7em;
                border-collapse: collapse;
                margin: 0 0 .2em;
            }

            .ui-datepicker .ui-datepicker-title {
                margin: 0 1.3em;
                line-height: 1.3em;
                text-align: center;
            }
    </style>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.datepicker').datepicker({ dateFormat: 'yy/mm/dd' });


        });


        function block_ui(obj) {
            obj = '#' + obj;
            var target = $(obj);
            unblock_ui();
            $.blockUI({ message: $(target), css: { backgroundcolor: '#F7F7F7', border: '10px solid #fff', top: ($(window).height()) / 8 + 'px', left: ($(window).width() - 700) / 2 + 'px', width: '700px' },bindEvents : false })

            return false;
        }

        function unblock_ui() {
            $.unblockUI();
            return false;
        }

        function WaitingBlock() {
            $.blockUI({
                message: "匯入中請稍後...",
                css: {
                    width: '20%',
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }
    </script>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <uc2:ProposalMenu ID="ProposalMenu" runat="server"></uc2:ProposalMenu>
    <div id="MainSet" class="mc-content">
        <h1 class="rd-smll">超商取貨管理</h1>
        <hr class="header_hr">
        <table style="width: 100%;">
            <tr>
                <td>簽約公司名稱：</td>
                <td>
                    <asp:TextBox runat="server" ID="tbSellerName" CssClass="input-small"></asp:TextBox></td>
                <td>商家編號：</td>
                <td>
                    <asp:TextBox runat="server" ID="tbSellerId"></asp:TextBox></td>
            </tr>
            <tr>
                <td>狀態：</td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlISPStatus"></asp:DropDownList></td>
                <td>超商類型：</td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlServiceChannel"></asp:DropDownList></td>
            </tr>
            <tr>
                <td>申請日：</td>
                <td colspan="3">
                    <asp:TextBox runat="server" ID="tbStartDate" class="datepicker" ClientIDMode="Static"></asp:TextBox>
                    <i class="fa fa-calendar"></i>~&nbsp;
                <asp:TextBox runat="server" ID="tbEndDate" class="datepicker" ClientIDMode="Static"></asp:TextBox>
                    <i class="fa fa-calendar"></i>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <asp:Button runat="server" ID="btnSearch" Text="搜尋" OnClick="btnSearch_Click" CssClass="btn rd-mcacstbtn" />
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td style="width:500px"><p>處理流程：審核->超商建檔->測標->開通完成</p></td>
                <td style="width:500px">
                    <div id="enableseven" runat="server" visible="false">
                        一頁顯示
                        <asp:DropDownList ID="ddlChangePage" runat="server" OnSelectedIndexChanged="btnSearch_Click" ClientIDMode="Static" AutoPostBack="true" Width="100px">
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>50</asp:ListItem>
                            <asp:ListItem>100</asp:ListItem>
                        </asp:DropDownList>
                        筆資料
                        <asp:Button runat="server" ID="btnExport" Text="匯出Excel" OnClick="btnExport_Click" CssClass="btn rd-mcacstbtn" />
                        <asp:Button ID="btnImportShow" runat="server" Text="匯入申請狀態" CssClass="btn rd-mcacstbtn" Visible="true" ClientIDMode="static"  OnClientClick="return block_ui('divinstoreStatusImport');"/>
                    </div>
                    
                </td>
            </tr>
        </table>


        <div id="divinstoreStatusImport" class="pop-window" style="display: none; text-align: left;">
            <div>
                可進行匯入狀態
            <br />
                全家：審核狀態。
            <br />
                7-11：審核狀態，建檔狀態，測標狀態。
            <br />
                狀態值/屬性：1/成功，0/失敗 (審核&建檔狀態不可填0)。
            <br />
            <br />
                請匯入.xls檔案
            <br />
                <asp:FileUpload ID="FUImport" runat="server" />
                <br />
                <asp:Button ID="btnImport" CssClass="btn rd-mcacstbtn" runat="server" Text="匯入" OnClick="btnImport_Click" OnClientClick="WaitingBlock();" />
            </div>

            <div class="MGS-XX" onclick="unblock_ui();" style="cursor: pointer; background: url(../../Themes/PCweb/images/A2-Multi-grade-Setting_xx.png); height: 28px; width: 27px; position: absolute; top: -24px; right: -25px; z-index: 800;">
            </div>



        </div>
        
        
        <br />
        <asp:PlaceHolder ID="divISPList" runat="server" Visible="false">
            <div id="mc-table">
                <asp:Repeater ID="rptISPList" runat="server" OnItemDataBound="rptISPList_ItemDataBound">
                    <HeaderTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                            <tr class="rd-C-Hide">
                                <th class="Export OrderSerial" style="display: none">
                                    <input id="chkChangeAll" type="checkbox" />
                                </th>
                                <th class="OrderDate">商家編號
                                </th>
                                <th class="OrderDate">公司名稱<br />
                                    統一編號
                                </th>
                                <th class="OrderSerial">超商類型
                                </th>
                                <th class="OrderDate">申請日
                                </th>
                                <th class="OrderDate">狀態
                                </th>
                                <th class="OrderName">功能
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="mc-tableContentITEM">
                            <td class="OrderDate" style="">
                                <%# ((ViewVbsInstorePickup)Container.DataItem).SellerId %>
                            </td>
                            <td class="OrderDate" style="">
                                <%# ((ViewVbsInstorePickup)Container.DataItem).SellerName %>
                                <br />
                                <asp:Label runat="server" ID="lbSellerCompanyID"></asp:Label>
                            </td>
                            <td class="OrderDate" style="">
                                <asp:Label runat="server" ID="lbServiceChannel"></asp:Label>
                            </td>
                            <td class="OrderDate" style="">
                                <asp:Label runat="server" ID="lbCreateTime"></asp:Label>
                            </td>
                            <td class="OrderDate" style="">
                                <asp:Label runat="server" ID="lbStatus"></asp:Label>
                            </td>
                            <td class="OrderDate" style="">
                                <asp:HyperLink Target="_blank" runat="server" ID="hlLink">管理</asp:HyperLink>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <uc1:Pager ID="ucPager" runat="server" PageSize="10" ongetcount="GetCount" onupdate="UpdateHandler"></uc1:Pager>
        </asp:PlaceHolder>
        
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

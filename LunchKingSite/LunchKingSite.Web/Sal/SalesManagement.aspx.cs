﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using Novacode;
using Winnovative.WnvHtmlConvert;

namespace LunchKingSite.Web.Sal
{
    public partial class SalesManagement : RolePage, ISalesManagementView
    {
        #region props
        private SalesManagementPresenter _presenter;
        public SalesManagementPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string SalesId
        {
            get
            {
                return User.IsInRole(MemberRoles.Sales.ToString()) || User.IsInRole(MemberRoles.SalesManager.ToString()) ? User.Identity.Name.ToLower() : string.Empty;
            }
        }

        public string LoginUser
        {
            get
            {
                return User.Identity.Name.ToLower();
            }
        }

        public string SellerName
        {
            get { return txtSellerName.Text.Trim(); }
        }

        public string CompanyName
        {
            get { return txtCompanyName.Text.Trim(); }
        }

        public string SalesName
        {
            get { return txtSalesName.Text.Trim(); }
        }

        public string CreateIdSearch
        {
            get { return txtCreateId.Text.Trim().ToLower(); }
        }

        public string BusinessOrderId
        {
            get { return txtBusinessOrderId.Text.Trim(); }
        }

        public DateTime? CreateTimeStart
        {
            get
            {
                DateTime date;
                if (DateTime.TryParse(txtCreateTimeS.Text, out date))
                {
                    return date;
                }
                else
                {
                    return null;
                }
            }
        }

        public DateTime? CreateTimeEnd
        {
            get
            {
                DateTime date;
                if (DateTime.TryParse(txtCreateTimeE.Text, out date))
                {
                    return date;
                }
                else
                {
                    return null;
                }
            }
        }

        public DateTime? BusinessStartTimeS
        {
            get
            {
                DateTime date;
                if (DateTime.TryParse(txtBusinessStartTimeS.Text, out date))
                {
                    return date;
                }
                else
                {
                    return null;
                }
            }
        }

        public DateTime? BusinessStartTimeE
        {
            get
            {
                DateTime date;
                if (DateTime.TryParse(txtBusinessStartTimeE.Text, out date))
                {
                    return date;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<int> Status
        {
            get
            {
                List<int> statusList = new List<int>();
                if (string.IsNullOrEmpty(ddlStatus.SelectedValue))
                {
                    foreach (ListItem item in ddlStatus.Items)
                    {
                        if (item.Enabled && !string.IsNullOrEmpty(item.Value))
                            statusList.Add(Convert.ToInt32(item.Value));
                    }
                }
                else
                    statusList.Add(Convert.ToInt32(ddlStatus.SelectedValue));
                return statusList;
            }
        }

        public List<string> SalesDept
        {
            get
            {
                if (string.IsNullOrEmpty(ddlSalesDept.SelectedValue))
                    return ddlSalesDept.Items.Cast<ListItem>().Select(item => item.Value).ToList();
                else
                {
                    List<string> dept = new List<string>();
                    dept.Add(ddlSalesDept.SelectedValue);
                    return dept;
                }
            }
        }

        public int PageSize
        {
            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }
        }

        public string Separator
        {
            get { return ","; }
        }

        public Level SalesLevel
        {
            get
            {
                int level;
                return (int.TryParse(hidSalesLevel.Value, out level) ? (Level)level : Level.Specialist);
            }
            set
            {
                hidSalesLevel.Value = Convert.ToInt32(value).ToString();
            }
        }

        public string SalesArea
        {
            get
            {
                return hidSalesArea.Value;
            }
            set
            {
                hidSalesArea.Value = value;
            }
        }

        public BusinessType BusinessType
        {
            get
            {
                BusinessType type = BusinessType.None;
                BusinessType.TryParse(ddlBusinessType.SelectedValue, out type);
                return type;
            }
        }

        public bool IsTravelType
        {
            get
            {
                return chkIsTravelType.Checked;
            }
        }

        public bool IsMohistVerify
        {
            get
            {
                return chkIsMohistVerify.Checked;
            }
        }

        public bool IsTrustHwatai
        {
            get
            {
                return chkIsTrustHwatai.Checked;
            }
        }

        public InvoiceType InvoiceType
        {
            get
            {
                switch (rdlInvoiceType.SelectedValue)
                {
                    case "DuplicateUniformInvoice":
                        return InvoiceType.DuplicateUniformInvoice;
                    case "Other":
                        return InvoiceType.Other;
                    default:
                        return InvoiceType.General;
                }
            }
        }

        public bool IsNotContractSend
        {
            get
            {
                return chkIsNotContractSend.Checked;
            }
        }

        public bool IsViewer
        {
            get
            {
                return User.IsInRole(MemberRoles.BusinessOrderViewer.ToString("g"));
            }
        }

        public bool IsSalesAssistant
        {
            get
            {
                return User.IsInRole(MemberRoles.SalesAssistant.ToString());
            }
        }

        public bool IsDownloadPrivilege
        {
            get
            {
                return CommonFacade.IsInSystemFunctionPrivilege(Page.User.Identity.Name, SystemFunctionType.Download);
            }
        }

        public string EmpName
        {
            get
            {
                return hidEmpName.Value;
            }
            set
            {
                hidEmpName.Value = value;
            }
        }

        public string EmpDeptId
        {
            get
            {
                return hidEmpDeptId.Value;
            }
            set
            {
                hidEmpDeptId.Value = value;
            }
        }

        private string _salesNameArray;
        public string SalesNameArray
        {
            get { return _salesNameArray; }
            set { _salesNameArray = value; }
        }

        #endregion

        #region event
        public event EventHandler OnSearchClicked;
        public event EventHandler<DataEventArgs<Dictionary<string, string>>> OnDeptSaveClicked;
        public event EventHandler<DataEventArgs<int>> OnGetCount;
        public event EventHandler<DataEventArgs<int>> PageChanged;
        public event EventHandler<DataEventArgs<KeyValuePair<string, string>>> OnDownload;
        public event EventHandler<DataEventArgs<string>> OnShow;
        #endregion

        #region method
        public void GetSalesDepartment(List<Department> deptc)
        {
            ddlSalesDept.DataSource = deptc;
            ddlSalesDept.DataBind();
        }

        public void GetSalesLeaderList(Dictionary<SalesMember, List<Department>> dataList)
        {
            // 若登入者具業務主管權限，須設定相審核權限區域
            if (User.IsInRole(MemberRoles.SalesManager.ToString()))
            {
                repSalesDept.DataSource = dataList;
                repSalesDept.DataBind();

                if (SalesLevel.Equals(Level.Manager))
                {
                    tdDeptSetPlace.Visible = true;
                }
                else if (SalesLevel.Equals(Level.Leader))
                {
                    foreach (Department item in dataList.First().Value)
                    {
                        if (!SalesArea.Contains(item.DeptId))
                            ddlSalesDept.Items.Remove(ddlSalesDept.Items.FindByValue(item.DeptId));
                    }

                    tdDeptSetPlace.Visible = false;
                }
                else
                {
                    ddlSalesDept.Items.Clear();
                }
            }
        }

        public void GetBusinessList(IEnumerable<BusinessOrder> dataList)
        {
            gvBusinessList.DataSource = dataList.OrderByDescending(x => x.ModifyTime != null ? x.ModifyTime : x.CreateTime);
            gvBusinessList.DataBind();
            if (IsDownloadPrivilege || !string.IsNullOrWhiteSpace(SalesId))
            {
                gvBusinessList.Columns[11].Visible = true;
            }
        }

        public void DownloadTemplate(BusinessOrderDownloadType downloadType, BusinessType type, Dictionary<string, string> dataList, List<SalesStore> storeList)
        {
            if (downloadType == BusinessOrderDownloadType.Word)
            {
                DownloadWordByWordTemplate(type, dataList, storeList);
            }
            else
            {
                DownloadPdfByHtmlTemplate(type, dataList, storeList);
            }
        }

        /// <summary>
        /// 根據word範本下載word
        /// </summary>
        /// <param name="type"></param>
        /// <param name="dataList"></param>
        /// <param name="storeList"></param>
        public void OpenHtmlWindow(BusinessType type, Dictionary<string, string> dataList, List<SalesStore> storeList)
        {
            #region get template file

            string wordFile = string.Empty;
            string storelist = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderStorelist.htm");
            string storelistAccount = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderStorelistAccount.htm");

            switch (type)
            {
                case BusinessType.Ppon:
                    wordFile = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderPpon.htm");
                    break;

                case BusinessType.Product:
                    wordFile = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderProduct.htm");
                    break;

                case BusinessType.None:
                default:
                    break;
            }

            #endregion get template file

            string nextPage = @"<br clear=all style='page-break-before:always'>";
            System.Text.Encoding encode = Encoding.UTF8;

            StreamReader reader = new StreamReader(wordFile, encode);
            string content = reader.ReadToEnd();
            reader.Close();

            // replace
            foreach (KeyValuePair<string, string> item in dataList)
            {
                content = Regex.Replace(content, item.Key, !string.IsNullOrEmpty(item.Value) ? item.Value : string.Empty);
            }

            #region store list

            string storeContent = string.Empty;
            if (storeList != null && storeList.Count > 0)
            {
                storeContent += nextPage;
                storeContent += "<h2>分店資訊</h2>";

                foreach (SalesStore store in storeList)
                {
                    StreamReader readerStore = new StreamReader(storelist, encode);
                    if (!string.IsNullOrEmpty(store.StoreAccount))
                    {
                        readerStore = new StreamReader(storelistAccount, encode);
                    }

                    storeContent += readerStore.ReadToEnd();
                    readerStore.Close();

                    storeContent = Regex.Replace(storeContent, "BranchName", store.BranchName);
                    storeContent = Regex.Replace(storeContent, "StoreTel", store.StoreTel);
                    storeContent = Regex.Replace(storeContent, "OpeningTime", store.OpeningTime);
                    storeContent = Regex.Replace(storeContent, "Address", CityManager.CityTownShopStringGet(store.AddressTownshipId) + store.StoreAddress);
                    storeContent = Regex.Replace(storeContent, "CloseDate", store.CloseDate);
                    storeContent = Regex.Replace(storeContent, "StoreEmail", store.StoreEmail);
                    storeContent = Regex.Replace(storeContent, "StoreNotice", store.StoreNotice);

                    if (!string.IsNullOrEmpty(store.StoreAccount))
                    {
                        storeContent = Regex.Replace(storeContent, "StoreName", store.StoreName);
                        storeContent = Regex.Replace(storeContent, "StoreBossName", store.StoreBossName);
                        storeContent = Regex.Replace(storeContent, "StoreCompanyAdd", store.StoreCompanyAddress ?? string.Empty);
                        storeContent = Regex.Replace(storeContent, "StoreSignCompID", store.StoreSignCompanyID ?? string.Empty);
                        storeContent = Regex.Replace(storeContent, "StoreID", store.StoreID);
                        storeContent = Regex.Replace(storeContent, "StoreBranchCode", store.StoreBranchCode);
                        storeContent = Regex.Replace(storeContent, "StoreBankCode", store.StoreBankCode);
                        storeContent = Regex.Replace(storeContent, "StoreAccountName", store.StoreAccountName);
                        storeContent = Regex.Replace(storeContent, "StoreAccountingName", store.StoreAccountingName ?? string.Empty);
                        storeContent = Regex.Replace(storeContent, "StoreAccountingTel", store.StoreAccountingTel ?? string.Empty);
                        storeContent = Regex.Replace(storeContent, "StoreAccount", store.StoreAccount);
                    }
                }
                content += storeContent;
            }

            #endregion store list

            // show html content
            liBusinessOrder.Text = content;
            divBusinessOrderShow.Visible = true;
            divSearch.Visible = false;
        }

        public void ShowMessage(string msg)
        {
            string script = string.Format("alert('{0}');", msg);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", script, true);
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectToLoginPage();
                    return;
                }
                else if (!AuthorizationControl())
                {
                    ISysConfProvider config = ProviderFactory.Instance().GetConfig();
                    Response.Redirect(config.SiteUrl);
                }
                ucPager.ResolvePagerView(1, true);
                Presenter.OnViewInitialized();

                ((Ppon.Ppon)base.Master).ViewPortEnabled = false;
            }
            _presenter.OnViewLoaded();
            SetSalesmanNameArray();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.OnSearchClicked != null)
                this.OnSearchClicked(this, e);
            ucPager.ResolvePagerView(1, true);
        }

        protected void btnDeptSave_Click(object sender, EventArgs e)
        {
            if (this.OnDeptSaveClicked != null)
                this.OnDeptSaveClicked(this, new DataEventArgs<Dictionary<string, string>>(GetLeaderListAreaData()));
        }

        protected void liBackToList_Click(object sender, EventArgs e)
        {
            liBusinessOrder.Text = string.Empty;
            divBusinessOrderShow.Visible = false;
            divSearch.Visible = true;
        }

        protected void gvBusinessList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is BusinessOrder)
            {
                BusinessOrder item = (BusinessOrder)e.Row.DataItem;
                Label lblStatus = ((Label)e.Row.FindControl("lblStatus"));
                HyperLink hkBusinessOrderID = ((HyperLink)e.Row.FindControl("hkBusinessOrderID"));
                LinkButton lkBusinessOrderID = ((LinkButton)e.Row.FindControl("lkBusinessOrderID"));
                Label lblBusinessHourTimeS = (Label)e.Row.FindControl("lblBusinessHourTimeS");
                Label lblModifyTime = (Label)e.Row.FindControl("lblModifyTime");
                Label lblMohistVerify = (Label)e.Row.FindControl("lblMohistVerify");
                Label lblTrustHwatai = (Label)e.Row.FindControl("lblTrustHwatai");
                HyperLink hlSellerName = (HyperLink)e.Row.FindControl("hlSellerName");
                Label lblSalesName = (Label)e.Row.FindControl("lblSalesName");
                Label lblCreateId = (Label)e.Row.FindControl("lblCreateId");
                Label lblDept = (Label)e.Row.FindControl("lblDept");
                Label lblBusinessType = (Label)e.Row.FindControl("lblBusinessType");
                Label lblPromotion = (Label)e.Row.FindControl("lblPromotion");
                Label lblCopyType = (Label)e.Row.FindControl("lblCopyType");
                Label lblImportantMemo = (Label)e.Row.FindControl("lblImportantMemo");
                LinkButton lkDownload = (LinkButton)e.Row.FindControl("lkDownload");
                DropDownList ddlStoreList = (DropDownList)e.Row.FindControl("ddlStoreList");

                switch (item.Status)
                {
                    case BusinessOrderStatus.Temp:
                        lblStatus.Text = "草稿";
                        lblStatus.ForeColor = System.Drawing.Color.Green;
                        break;
                    case BusinessOrderStatus.Send:
                        lblStatus.Text = "送審";
                        lblStatus.ForeColor = System.Drawing.Color.Blue;
                        break;
                    case BusinessOrderStatus.Approve:
                        lblStatus.Text = "核准";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        break;
                    case BusinessOrderStatus.Review:
                        lblStatus.Text = "已複查";
                        lblStatus.ForeColor = System.Drawing.Color.Brown;
                        break;
                    case BusinessOrderStatus.Await:
                        lblStatus.Text = "已排檔";
                        lblStatus.ForeColor = System.Drawing.Color.Purple;
                        break;
                    case BusinessOrderStatus.Create:
                        lblStatus.Text = "已建檔";
                        lblStatus.ForeColor = System.Drawing.Color.Black;
                        lblStatus.Font.Bold = true;
                        break;
                    default:
                        break;
                }
                hkBusinessOrderID.Text = item.BusinessOrderId;
                hkBusinessOrderID.NavigateUrl = ResolveUrl("~/Sal/BusinessOrderDetail.aspx?boid=" + item.Id);
                lkBusinessOrderID.Visible = false;
                if (item.ModifyTime != null)
                    lblModifyTime.Text = item.ModifyTime.Value.ToLocalTime().ToString("yyyy/MM/dd HH:mm");
                if (item.BusinessHourTimeS != null)
                    lblBusinessHourTimeS.Text = item.BusinessHourTimeS.Value.ToLocalTime().ToString("yyyy/MM/dd");
                if (item.SalesDeptName != null)
                    lblDept.Text = item.SalesDeptName.Replace("業務部", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty);
                hlSellerName.Text = item.SellerName;
                if (item.Deals != null)
                    hlSellerName.Attributes["title"] = item.Deals.Where(x => !string.IsNullOrEmpty(x.PurchasePrice) && !x.PurchasePrice.Equals("0")).Select(x => x.ItemName).FirstOrDefault() ?? string.Empty;
                if (!item.BusinessHourGuid.Equals(Guid.Empty))
                    hlSellerName.NavigateUrl = ProviderFactory.Instance().GetConfig().SiteUrl + "/" + item.BusinessHourGuid;
                else if (!item.DealId.Equals(0))
                    hlSellerName.NavigateUrl = ProviderFactory.Instance().GetConfig().SiteUrl + "/piinlife/default.aspx?did=" + item.DealId;
                if (string.IsNullOrEmpty(hlSellerName.NavigateUrl))
                {
                    hlSellerName.ForeColor = System.Drawing.Color.Black;
                    hlSellerName.Font.Underline = false;
                }
                if (item.CreateId.IndexOf('@') > 0)
                {
                    lblCreateId.Text = item.CreateId.Substring(0, item.CreateId.IndexOf('@'));
                }
                lblSalesName.Text = item.SalesName;
                lblSalesName.Attributes["title"] = item.SalesDeptName;

                switch (item.BusinessType)
                {
                    case BusinessType.Ppon:
                        lblBusinessType.Text = "好康";
                        e.Row.BackColor = System.Drawing.Color.LightYellow;
                        break;
                    case BusinessType.Product:
                        lblBusinessType.Text = "宅配";
                        e.Row.BackColor = System.Drawing.Color.LightSalmon;
                        break;
                    case BusinessType.None:
                    default:
                        break;
                }
                if (item.IsTravelType)
                    lblBusinessType.BackColor = System.Drawing.Color.YellowGreen;

                lblMohistVerify.Visible = item.IsMohistVerify;
                lblTrustHwatai.Visible = item.IsTrustHwatai;

                switch (item.CopyType)
                {
                    case BusinessCopyType.None:
                        break;
                    case BusinessCopyType.General:
                        lblCopyType.Text = "一般";
                        break;
                    case BusinessCopyType.Again:
                        lblCopyType.Text = "再次";
                        lblCopyType.ForeColor = System.Drawing.Color.Red;
                        break;
                    default:
                        break;
                }

                IEnumerable<History> historyList = item.HistoryList.Where(x => x.IsImportant.Equals(true));
                if (historyList.Count() > 0)
                {
                    lblImportantMemo.Text = string.Join(",", historyList.Select(x => x.Changeset));
                    lblModifyTime.Attributes["title"] = string.Join(",", historyList.Select(x => x.Changeset));
                    lblModifyTime.Font.Bold = true;
                    lblModifyTime.Font.Underline = true;
                }
                if (item.Store.Count > 0)
                {
                    ddlStoreList.DataSource = item.Store.Select(x => x.BranchName);
                    ddlStoreList.DataBind();
                }
                else
                {
                    ddlStoreList.Visible = false;
                }
                lkDownload.CommandArgument = item.BusinessOrderId;
            }
        }

        protected void gvBusinessList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName.ToString().ToUpper())
            {
                case "DOWNLOAD":
                    if (this.OnDownload != null)
                    {
                        GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                        DropDownList ddlStoreList = (DropDownList)row.Cells[11].FindControl("ddlStoreList");
                        KeyValuePair<string, string> argument = new KeyValuePair<string, string>(ddlStoreList.SelectedValue, e.CommandArgument.ToString());
                        this.OnDownload(this, new DataEventArgs<KeyValuePair<string, string>>(argument));
                    }
                    break;
                case "SHOW":
                    if (this.OnShow != null)
                        this.OnShow(this, new DataEventArgs<string>(e.CommandArgument.ToString()));
                    break;
                default:
                    break;
            }
        }

        protected void repSalesDept_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<SalesMember, List<Department>>)
            {
                KeyValuePair<SalesMember, List<Department>> item = (KeyValuePair<SalesMember, List<Department>>)e.Item.DataItem;
                Label lblLeaderId = ((Label)e.Item.FindControl("lblLeaderId"));
                HiddenField hidLeaderId = ((HiddenField)e.Item.FindControl("hidLeaderId"));
                CheckBoxList chklLeaderArea = ((CheckBoxList)e.Item.FindControl("chklLeaderArea"));

                hidLeaderId.Value = item.Key.SalesId;
                lblLeaderId.Text = item.Key.SalesId.Replace("@17life.com", string.Empty).Replace("@17life.com.tw", string.Empty);
                chklLeaderArea.DataSource = item.Value.Select(x => new { Id = x.DeptId, Name = x.DeptName.Replace("業務", string.Empty).Replace("部", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty) });
                chklLeaderArea.DataBind();
                SetCheckBoxListSelectedValue(chklLeaderArea, item.Key.Area);

            }
        }

        protected void UpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.PageChanged != null)
                this.PageChanged(this, e);
        }

        protected int GetCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetCount != null)
                OnGetCount(this, e);
            return e.Data;
        }
        #endregion

        #region private method
        private bool AuthorizationControl()
        {
            bool authorize = false;

            if (User.IsInRole(MemberRoles.SalesAssistant.ToString("g")))
            {
                btnAddBussinessOrder.Visible = true;
                SearchSalesPlace.Visible = SearchSalesPlaceTitle.Visible = false;
                EnableControls(BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await, BusinessOrderStatus.Create);

                authorize = true;
            }
            else if (User.IsInRole(MemberRoles.Sales.ToString("g")))
            {
                if (!IsViewer)
                {
                    SearchSales.Visible = SearchSalesTitle.Visible = false;
                    SearchSalesPlace.Visible = SearchSalesPlaceTitle.Visible = false;
                }
                btnAddBussinessOrder.Visible = true;
                EnableControls(BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await, BusinessOrderStatus.Create);

                authorize = true;
            }
            else if (User.IsInRole(MemberRoles.SalesManager.ToString("g")))
            {
                btnAddBussinessOrder.Visible = true;
                EnableControls(BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await, BusinessOrderStatus.Create);
                tdDeptSetPlace.Visible = true;

                authorize = true;
            }
            else if (User.IsInRole(MemberRoles.Auditor.ToString("g")))
            {
                EnableControls(BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await, BusinessOrderStatus.Create);

                authorize = true;
            }
            else if (User.IsInRole(MemberRoles.Production.ToString("g")))
            {
                EnableControls(BusinessOrderStatus.Temp, BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await, BusinessOrderStatus.Create);

                authorize = true;
            }
            else if (User.IsInRole(MemberRoles.Financial.ToString("g")))
            {
                EnableControls(BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await, BusinessOrderStatus.Create);

                authorize = true;
            }
            else if (IsViewer)
            {
                EnableControls(BusinessOrderStatus.Send, BusinessOrderStatus.Approve, BusinessOrderStatus.Review, BusinessOrderStatus.Await, BusinessOrderStatus.Create);

                authorize = true;
            }
            return authorize;
        }

        private Dictionary<string, string> GetLeaderListAreaData()
        {
            Dictionary<string, string> areaList = new Dictionary<string, string>();
            for (int i = 0; i < repSalesDept.Items.Count; i++)
            {
                HiddenField hidLeaderId = (HiddenField)repSalesDept.Items[i].FindControl("hidLeaderId");
                CheckBoxList chklLeaderArea = (CheckBoxList)repSalesDept.Items[i].FindControl("chklLeaderArea");

                areaList.Add(hidLeaderId.Value, GetCheckBoxListSelectedValue(chklLeaderArea));
            }
            return areaList;
        }

        private string GetCheckBoxListSelectedValue(CheckBoxList chkBoxList)
        {
            return string.Join(Separator, chkBoxList.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value));
        }

        private void SetCheckBoxListSelectedValue(CheckBoxList chkBoxList, string checkItems)
        {
            for (int i = 0; i < chkBoxList.Items.Count; i++)
            {
                if (checkItems != null)
                    chkBoxList.Items[i].Selected = checkItems.Contains(chkBoxList.Items[i].Value);
            }
        }

        /// <summary>
        /// 狀態顯示控制
        /// </summary>
        /// <param name="listItemStatus">狀態顯示項目</param>
        private void EnableControls(params BusinessOrderStatus[] listItemStatus)
        {
            foreach (ListItem item in ddlStatus.Items)
            {
                if (string.IsNullOrEmpty(item.Value))
                    item.Enabled = true;
                else
                {
                    BusinessOrderStatus status = BusinessOrderStatus.Temp;
                    BusinessOrderStatus.TryParse(item.Value, out status);
                    item.Enabled = listItemStatus.Contains(status);
                }
            }
        }

        /// <summary>
        /// 根據word範本下載word
        /// </summary>
        /// <param name="type"></param>
        /// <param name="dataList"></param>
        /// <param name="storeList"></param>
        private void DownloadWordByWordTemplate(BusinessType type, Dictionary<string, string> dataList, List<SalesStore> storeList)
        {
            #region get template file

            string wordFile = string.Empty;
            string footer = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderFooter.docx");
            string storelist = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderStorelist.docx");
            string storelistAccount = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderStorelistAccount.docx");

            switch (type)
            {
                case BusinessType.Ppon:
                    wordFile = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderPponTravel.docx");
                    break;

                case BusinessType.Product:
                    wordFile = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderProductTravel.docx");
                    break;

                case BusinessType.None:
                default:
                    break;
            }

            #endregion get template file

            // Load the document.
            using (DocX document = DocX.Load(wordFile))
            {
                if (!IsDownloadPrivilege)
                {
                    // print footer
                    document.InsertDocument(DocX.Load(footer));
                }

                // Replace text in this document.
                foreach (KeyValuePair<string, string> item in dataList)
                {
                    document.ReplaceText(item.Key, !string.IsNullOrEmpty(item.Value) ? item.Value : string.Empty);
                }

                #region store list

                if (storeList != null && storeList.Count > 0)
                {
                    document.InsertParagraph().Append("分店資訊").Bold().FontSize(16);

                    foreach (SalesStore store in storeList)
                    {
                        if (!string.IsNullOrEmpty(store.StoreAccount))
                        {
                            document.InsertDocument(DocX.Load(storelistAccount));
                        }
                        else
                        {
                            document.InsertDocument(DocX.Load(storelist));
                        }

                        document.ReplaceText("BranchName", store.BranchName);
                        document.ReplaceText("StoreTel", store.StoreTel);
                        document.ReplaceText("OpeningTime", store.OpeningTime);
                        document.ReplaceText("Address", CityManager.CityTownShopStringGet(store.AddressTownshipId) + store.StoreAddress);
                        document.ReplaceText("CloseDate", store.CloseDate);
                        document.ReplaceText("StoreEmail", store.StoreEmail);
                        document.ReplaceText("StoreNotice", store.StoreNotice);

                        if (!string.IsNullOrEmpty(store.StoreAccount))
                        {
                            document.ReplaceText("StoreName", store.StoreName);
                            document.ReplaceText("StoreBossName", store.StoreBossName);
                            document.ReplaceText("StoreID", store.StoreID);
                            document.ReplaceText("StoreBranchCode", store.StoreBranchCode);
                            document.ReplaceText("StoreBankCode", store.StoreBankCode);
                            document.ReplaceText("StoreAccountName", store.StoreAccountName);
                            document.ReplaceText("StoreAccount", store.StoreAccount);
                        }
                    }
                }

                #endregion store list

                // Save changes made to this document.
                MemoryStream memoryStream = new MemoryStream();
                document.SaveAs(memoryStream);

                // print word
                Response.ContentType = "application/msword";
                Response.AddHeader("content-disposition", "attachment; filename=\"" + string.Format(dataList["SellerName"] + "_{0}", DateTime.Now.ToString("yyyy_MM_dd")) + ".doc\"");
                memoryStream.WriteTo(Response.OutputStream);
                Response.End();
            } // Release this document from memory.
        }

        /// <summary>
        /// 根據htm範本下載pdf
        /// </summary>
        /// <param name="wordFile">Part1、Part2</param>
        /// <param name="wordFile3">Part3</param>
        /// <param name="wordFile4">Part4</param>
        /// <param name="dataList">取代參數</param>
        private void DownloadPdfByHtmlTemplate(BusinessType type, Dictionary<string, string> dataList, List<SalesStore> storeList)
        {
            #region get template file

            string wordFile = string.Empty;
            string magazine = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderMagazine.htm");
            string pezStore = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderPezStore.htm");
            string footer = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderFooter.htm");
            string storelist = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderStorelist.htm");
            string storelistAccount = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderStorelistAccount.htm");

            switch (type)
            {
                case BusinessType.Ppon:
                    wordFile = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderPpon.htm");
                    break;

                case BusinessType.Product:
                    wordFile = HttpContext.Current.Server.MapPath("~/template/SalesDocs/BusinessOrderProduct.htm");
                    break;

                case BusinessType.None:
                default:
                    break;
            }

            #endregion get template file

            string nextPage = @"<br clear=all style='page-break-before:always'>";
            System.Text.Encoding encode = Encoding.UTF8;

            StreamReader reader = new StreamReader(wordFile, encode);
            string content = reader.ReadToEnd();
            reader.Close();

            // replace
            foreach (KeyValuePair<string, string> item in dataList)
            {
                content = Regex.Replace(content, item.Key, !string.IsNullOrEmpty(item.Value) ? item.Value : string.Empty);
            }

            #region store list

            string storeContent = string.Empty;
            if (storeList != null && storeList.Count > 0)
            {
                storeContent += nextPage;
                storeContent += "<h2>分店資訊</h2>";

                foreach (SalesStore store in storeList)
                {
                    StreamReader readerStore = new StreamReader(storelist, encode);
                    if (!string.IsNullOrEmpty(store.StoreAccount))
                    {
                        readerStore = new StreamReader(storelistAccount, encode);
                    }

                    storeContent += readerStore.ReadToEnd();
                    readerStore.Close();

                    storeContent = Regex.Replace(storeContent, "BranchName", store.BranchName);
                    storeContent = Regex.Replace(storeContent, "StoreTel", store.StoreTel);
                    storeContent = Regex.Replace(storeContent, "OpeningTime", store.OpeningTime);
                    storeContent = Regex.Replace(storeContent, "Address", CityManager.CityTownShopStringGet(store.AddressTownshipId) + store.StoreAddress);
                    storeContent = Regex.Replace(storeContent, "CloseDate", store.CloseDate);
                    storeContent = Regex.Replace(storeContent, "StoreEmail", store.StoreEmail);
                    storeContent = Regex.Replace(storeContent, "StoreNotice", store.StoreNotice);

                    if (!string.IsNullOrEmpty(store.StoreAccount))
                    {
                        storeContent = Regex.Replace(storeContent, "StoreName", store.StoreName);
                        storeContent = Regex.Replace(storeContent, "StoreBossName", store.StoreBossName);
                        storeContent = Regex.Replace(storeContent, "StoreCompanyAdd", store.StoreCompanyAddress ?? string.Empty);
                        storeContent = Regex.Replace(storeContent, "StoreSignCompID", store.StoreSignCompanyID ?? string.Empty);
                        storeContent = Regex.Replace(storeContent, "StoreID", store.StoreID);
                        storeContent = Regex.Replace(storeContent, "StoreBranchCode", store.StoreBranchCode);
                        storeContent = Regex.Replace(storeContent, "StoreBankCode", store.StoreBankCode);
                        storeContent = Regex.Replace(storeContent, "StoreAccountName", store.StoreAccountName);
                        storeContent = Regex.Replace(storeContent, "StoreAccountingName", store.StoreAccountingName ?? string.Empty);
                        storeContent = Regex.Replace(storeContent, "StoreAccountingTel", store.StoreAccountingTel ?? string.Empty);
                        storeContent = Regex.Replace(storeContent, "StoreAccount", store.StoreAccount);
                    }
                }
                content += storeContent;
            }

            #endregion store list

            if (!IsDownloadPrivilege)
            {
                // print footer
                StreamReader readerFooter = new StreamReader(footer, encode);
                content += readerFooter.ReadToEnd();
                readerFooter.Close();
            }

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment; filename=\"" + string.Format(dataList["SellerName"] + "_{0}", DateTime.Now.ToString("yyyy_MM_dd")) + ".pdf\"");
            PdfConverter pdfc = new PdfConverter();
            pdfc.LicenseKey = @"YklTQlNCVFZCVExSQlFTTFNQTFtbW1s=";
            pdfc.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfc.PdfDocumentOptions.TopMargin = 50;
            pdfc.PdfDocumentOptions.LeftMargin = 40;
            int buttonMargin = int.TryParse(txtDonwloadBottomMarginFix.Text, out buttonMargin) ? buttonMargin : 0;
            pdfc.PdfDocumentOptions.BottomMargin = 50 + buttonMargin;
            pdfc.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfc.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(pdfc.GetPdfBytesFromHtmlString(content));
            Response.End();
        }

        private void SetSalesmanNameArray()
        {
            EmployeeCollection empCol = HumanFacade.EmployeeGetListByDept(EmployeeDept.S000);
            SalesNameArray = "'" + string.Join("','", empCol.Select(x => x.EmpName)) + "'";
        }
        #endregion
    }
}
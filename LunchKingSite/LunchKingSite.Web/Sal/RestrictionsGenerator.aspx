﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="RestrictionsGenerator.aspx.cs" Inherits="LunchKingSite.Web.Sal.RestrictionsGenerator" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Tools/js/fancybox/jquery.fancybox.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".header").hide();
            $(".MasterPageRender").hide();
            $(".p3footer").hide();
        });
        function CheckResult() {
             var desc = $("[id*='lblDesc']").text();
            var result = $('#txtResult').val();
            if ($.trim(result) == '') {
                $('#txtResult').val(desc);
            } else {
                $('#txtResult').val(result + '\n' + desc);
            }
        }

        function SaveCheck() {
            if ($.trim($('#txtResult').val()) == '') {
                alert('至少新增一項權益說明。');
                return false;
            }
            return true;
        }
    </script>
    <style type="text/css">
        .style1
        {
            text-align: center;
            color: #bf0000;
            font-weight: bold;
        }
        .style2
        {
            background-color: #444444;
            color: White;
            font-weight: bold;
            width: 60px;
        }
        .style3
        {
            *background-color: #eeeeee;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <div class="mc-content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table>
                    <tr>
                        <td colspan="5" class="style1">好康小提示
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">項目
                        </td>
                        <td class="style2">館別
                        </td>
                        <td class="style2">類型
                        </td>
                        <td class="style2">項目
                        </td>
                        <td class="style2">內容
                        </td>
                    </tr>
                    <tr>
                        <td class="style2"></td>
                        <td class="style3">
                            <asp:HiddenField ID="hidDepartmentId" runat="server" />
                            <asp:DropDownList ID="ddlDepartment" runat="server" DataValueField="Id" DataTextField="Name"
                                OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true"
                                Width="100px">
                            </asp:DropDownList>
                        </td>
                        <td class="style3">
                            <asp:HiddenField ID="hidCategoryId" runat="server" />
                            <asp:DropDownList ID="ddlCategory" runat="server" DataValueField="Id" DataTextField="Name"
                                OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true"
                                Width="150px">
                            </asp:DropDownList>
                        </td>
                        <td class="style3">
                            <asp:HiddenField ID="hidItemId" runat="server" />
                            <asp:DropDownList ID="ddlItem" runat="server" DataValueField="Id" DataTextField="Name"
                                OnSelectedIndexChanged="ddlItem_SelectedIndexChanged" AutoPostBack="true" Width="200px">
                            </asp:DropDownList>
                        </td>
                        <td class="style3">
                            <asp:HiddenField ID="hidContentId" runat="server" />
                            <asp:DropDownList ID="ddlContent" runat="server" DataValueField="Id" DataTextField="Name"
                                OnSelectedIndexChanged="ddlContent_SelectedIndexChanged" AutoPostBack="true"
                                Width="300px">
                            </asp:DropDownList>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">描述
                        </td>
                        <td colspan="4" style="background-color: #eeeeee; width: 600px">
                            <asp:HiddenField ID="hidDescId" runat="server" />
                            <asp:Label ID="lblDesc" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">&nbsp;</td>
                        <td colspan="2" class="style3">
                            <asp:Button ID="btnAdd" runat="server" OnClientClick="CheckResult();" Text="新增" CssClass="btn btn-primary rd-mcacstbtn" />
                        </td>
                        <td colspan="1" class="style3">
                            &nbsp;</td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="grui-form">
            <div class="form-unit">
                <div class="data-input rd-data-input" style="margin-left: 0px">
                    <asp:TextBox ID="txtResult" runat="server" TextMode="MultiLine" ClientIDMode="Static" Height="250px" Width="100%"></asp:TextBox>
                </div>
            </div>
            <div class="form-unit end-unit rd-text-rg">
                <div>
                    <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="儲存" OnClick="btnSave_Click" OnClientClick="return SaveCheck();" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

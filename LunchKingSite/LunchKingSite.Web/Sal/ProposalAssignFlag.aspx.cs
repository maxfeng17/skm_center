﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Sal
{
    public partial class ProposalAssignFlag : RolePage, IProposalAssignFalgView
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region props
        private ProposalAssignFalgPresenter _presenter;
        public ProposalAssignFalgPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IntialControls();
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        #region method
        private void IntialControls()
        {
            // 提案類型
            Dictionary<int, string> dealType = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(ProposalDealType)))
            {
                dealType[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDealType)item);
            }
            ddlDealType.DataSource = dealType.Where(x => x.Key != (int)ProposalDealType.None);
            ddlDealType.DataTextField = "Value";
            ddlDealType.DataValueField = "Key";
            ddlDealType.DataBind();
        }
        public void SetProposalAssignContent(ProposalCollection pc)
        {

        }
        #endregion

        #region WebMethod
        [WebMethod]
        public static dynamic ProposalGetByOrderTimeS(int type, int dealType, string filterDate)
        {
            List<Proposal> pros;

            if (type != (int)ChkAssignType.Bid)
            {
                dealType = 0;
            }
            List<ProposalAssignLog> pals = ProposalFacade.ProposalAssignFalg(dealType,out pros).ToList();
            List<ProposalAssignLogDealModel> paldms = new List<ProposalAssignLogDealModel>();

            if(type == (int)ChkAssignType.Bid){
                //By檔次
                List<int> pids = pals.Select(x => x.Pid).Distinct().ToList();

                foreach (var pro in pros)
                {
                    ProposalAssignLogDealModel padm = new ProposalAssignLogDealModel();
                    var Setting = pals.Where(x => x.AssignFlag == (int)ProposalEditorFlag.Setting && x.Pid == pro.Id).OrderByDescending(x => x.CreateTime).FirstOrDefault(); //檔次設定
                    var CopyWriter = pals.Where(x => x.AssignFlag == (int)ProposalEditorFlag.CopyWriter && x.Pid == pro.Id).OrderByDescending(x => x.CreateTime).FirstOrDefault();//圖片設計
                    var ImageDesign = pals.Where(x => x.AssignFlag == (int)ProposalEditorFlag.ImageDesign && x.Pid == pro.Id).OrderByDescending(x => x.CreateTime).FirstOrDefault();//圖片設計
                    var ART = pals.Where(x => x.AssignFlag == (int)ProposalEditorFlag.ART && x.Pid == pro.Id).OrderByDescending(x => x.CreateTime).FirstOrDefault();//大圖ART
                    var Listing = pals.Where(x => x.AssignFlag == (int)ProposalEditorFlag.Listing && x.Pid == pro.Id).OrderByDescending(x => x.CreateTime).FirstOrDefault();//上架確認

                    Proposal p = pros.Where(x => x.Id == pro.Id).FirstOrDefault();

                    if (!string.IsNullOrEmpty(filterDate))
                    {
                        //上檔日
                        DateTime dt = DateTime.MaxValue;
                        DateTime.TryParse(filterDate, out dt);
                        TimeSpan t = dt.Subtract(p.OrderTimeS ?? DateTime.MaxValue);
                        if (t.Days != 0)
                        {
                            continue;
                        }
                    }

                    padm.Pid = pro.Id.ToString();
                    padm.BrandName = p.BrandName;
                    padm.OrderTimeS = (p.OrderTimeS ?? DateTime.MaxValue).ToString("yyyy/MM/dd");
                    padm.Setting = (Setting != null ? "<img src='/Themes/PCweb/images/Tick.png'>" : "");
                    padm.CopyWriter = (CopyWriter != null ? "<img src='/Themes/PCweb/images/Tick.png'>" : "");
                    padm.ImageDesign = (ImageDesign != null ? "<img src='/Themes/PCweb/images/Tick.png'>" : "");
                    padm.ART = (ART != null ? "<img src='/Themes/PCweb/images/Tick.png'>" : "");
                    padm.Listing = (Listing != null ? "<img src='/Themes/PCweb/images/Tick.png'>" : "");

                    if (Setting == null || CopyWriter == null || ImageDesign == null || ART == null || Listing == null)
                    {
                        //有未指派的資料才要顯示
                        paldms.Add(padm);
                    }
                }
                return paldms;
            }
            else
            {
                //By個人
                var pids = (from p in pals
                            join pro in pros on p.Pid equals pro.Id
                            where DateTime.Now.Subtract(pro.OrderTimeS ?? DateTime.MaxValue).Days == 0
                            select new
                            {
                                p.AssignEmail
                            }).Distinct().OrderBy(x=>x.AssignEmail);

                //pals.Where(x=>x.CreateTime == DateTime.Now).Select(x => x.AssignEmail).Distinct().ToList();

                foreach (var pid in pids)
                {
                    //找出「當日」被指派的名單
                    ProposalAssignLogDealModel padm = new ProposalAssignLogDealModel();
                    var Setting = pals.Where(x => x.AssignFlag == (int)ProposalEditorFlag.Setting).Where(x => x.AssignEmail == pid.AssignEmail.ToString() && DateTime.Now.Subtract(x.CreateTime).Days == 0).OrderByDescending(x => x.CreateTime); //檔次設定
                    var CopyWriter = pals.Where(x => x.AssignFlag == (int)ProposalEditorFlag.CopyWriter).Where(x => x.AssignEmail == pid.AssignEmail.ToString() && DateTime.Now.Subtract(x.CreateTime).Days == 0).OrderByDescending(x => x.CreateTime);//圖片設計
                    var ImageDesign = pals.Where(x => x.AssignFlag == (int)ProposalEditorFlag.ImageDesign).Where(x => x.AssignEmail == pid.AssignEmail.ToString() && DateTime.Now.Subtract(x.CreateTime).Days == 0).OrderByDescending(x => x.CreateTime);//圖片設計
                    var ART = pals.Where(x => x.AssignFlag == (int)ProposalEditorFlag.ART).Where(x => x.AssignEmail == pid.AssignEmail.ToString() && DateTime.Now.Subtract(x.CreateTime).Days == 0).OrderByDescending(x => x.CreateTime);//大圖ART
                    var Listing = pals.Where(x => x.AssignFlag == (int)ProposalEditorFlag.Listing).Where(x => x.AssignEmail == pid.AssignEmail.ToString() && DateTime.Now.Subtract(x.CreateTime).Days == 0).OrderByDescending(x => x.CreateTime);//上架確認

                    padm.Pid = pid.AssignEmail.ToString();
                    padm.Setting = Setting.Count().ToString();
                    padm.CopyWriter = CopyWriter.Count().ToString();
                    padm.ImageDesign = ImageDesign.Count().ToString();
                    padm.ART = ART.Count().ToString();
                    padm.Listing = Listing.Count().ToString();

                    paldms.Add(padm);
                }
                return paldms;
            }


        }
        #endregion


    }
}
﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.SellerSales;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Sal
{
    public partial class ProposalList : RolePage, ISalesProposalListView
    {
        #region props
        private SalesProposalListPresenter _presenter;
        public SalesProposalListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public int ProposalId
        {
            get
            {
                int id = int.TryParse(txtProposalId.Text, out id) ? id : 0;
                return id;
            }
        }

        public Guid Bid
        {
            get
            {
                Guid id = Guid.TryParse(txtBid.Text, out id) ? id : Guid.Empty;
                return id;
            }
        }

        public Guid Sid
        {
            get
            {
                Guid sid = Guid.Empty;
                if (Request.QueryString["sid"] != null)
                {
                    Guid.TryParse(Request.QueryString["sid"].ToString(), out sid);
                }
                return sid;
            }
        }

        public string SellerName
        {
            get { return txtSellerName.Text; }
        }

        public string BrandName
        {
            get { return txtBrandName.Text; }
        }

        public string DealContent
        {
            get { return txtDealContent.Text; }
        }

        public DateTime OrderTimeS
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtOrderTimeS.Text, out date);
                return date;
            }
        }

        public DateTime OrderTimeE
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtOrderTimeE.Text, out date);
                return date;
            }
        }

        public bool IsOrderTimeSet
        {
            get
            {
                return chkIsOrderTimeSet.Checked;
            }
        }

        public string MarketingResource
        {
            get { return txtMarketingResource.Text; }
        }

        public int SpecialFlag
        {
            get
            {
                int flag = int.TryParse(ddlSpecialFlag.SelectedValue, out flag) ? flag : 0;
                return flag;
            }
        }

        public int? SalesId
        {
            get
            {
                if (!string.IsNullOrEmpty(txtSalesEmail.Text))
                {
                    ViewEmployee emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, txtSalesEmail.Text);
                    return emp.UserId;
                }
                else
                {
                    return null;
                }

            }

        }

        public int EmpUserId
        {
            get
            {
                int id = int.TryParse(hidSalesId.Value, out id) ? id : 0;
                return id;
            }
        }

        public string Dept
        {
            get { return ddlDept.SelectedValue; }
        }

        public ProposalStatus Status
        {
            get
            {
                ProposalStatus ps = Core.ProposalStatus.Initial;
                ProposalStatus.TryParse(ddlProposalStatus.SelectedValue, out ps);
                return ps;
            }
        }

        public FileUpload FUExport
        {
            get
            {
                return FUImport;
            }
        }


        public DeliveryType? Type
        {
            get
            {
                if (ddlDeliveryType.SelectedValue != string.Empty)
                {
                    DeliveryType type = DeliveryType.ToShop;
                    if (DeliveryType.TryParse(ddlDeliveryType.SelectedValue, out type))
                    {
                        return type;
                    }
                }
                return null;
            }
        }
        public int? DeliveryNewType
        {
            get
            {
                int deliveryType = 0;
                bool flag = int.TryParse(ddlDeliveryType.SelectedValue, out deliveryType);
                if (flag)
                {
                    return deliveryType;
                }
                return null;
            }
        }

        public int? NewHouseStatus
        {
            get
            {
                int newHouseStatus = 0;
                int.TryParse(ddlNewHouseStatus.SelectedValue, out newHouseStatus);
                if(newHouseStatus != 0)
                {
                    return newHouseStatus;
                }
                return null;
            }
        }

        public ProposalCopyType? CopyType
        {
            get
            {
                if (ddlProposalCopyType.SelectedValue != string.Empty)
                {
                    ProposalCopyType type = ProposalCopyType.None;
                    if (ProposalCopyType.TryParse(ddlProposalCopyType.SelectedValue, out type))
                    {
                        return type;
                    }
                }
                return null;
            }
        }
        public int DealType
        {
            get
            {
                int flag = int.TryParse(hidDealType.Value, out flag) ? flag : 0;
                return flag;
            }
        }
        public int DealType1
        {
            get
            {
                int flag = int.TryParse(ddlDealType1.SelectedValue, out flag) ? flag : 0;
                return flag;
            }
        }
        public int DealSubType
        {
            get
            {
                int flag = int.TryParse(hidDealSubType.Value, out flag) ? flag : 0;
                return flag;
            }
        }
        public int CheckFlag
        {
            get
            {
                int flag = int.TryParse(hidCheckFlag.Value, out flag) ? flag : 0;
                return flag;
            }
        }

        public bool ShowAppley
        {
            get
            {
                return chkShowAppley.Checked;
                //return (ddlProposalStatus.SelectedValue == "0" ? false : true);
            }
        }

        public bool ShowChecked
        {
            get
            {
                return chkShowChecked.Checked;
            }
        }
        public bool ShowHouseChecked
        {
            get
            {
                return chkShowHouseCheck.Checked;
            }
        }

        public bool NoOrderTime
        {
            get
            {
                return chkNoOrderTime.Checked;
            }
        }

        public bool Assign
        {
            get
            {
                return CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Assign);
            }
        }

        public string DealStatus
        {
            get
            {
                return ddlDealStatus.SelectedValue;
            }
        }

        public int PageSize
        {
            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }
        }

        public int CurrentPage
        {
            get
            {
                return ucPager.CurrentPage;
            }
        }

        public string StatusFlag
        {
            get
            {
                return hidStatusFlagResult.Value;
            }
        }
        public int ProposalCreatedType
        {
            get
            {
                int flag = int.TryParse(ddlProposalCreatedType.SelectedValue, out flag) ? flag : 0;
                return flag;
            }
        }
        public int UniqueId
        {
            get
            {
                int _UniqueId = 0;
                int.TryParse(txtUniqueId.Text.Trim(), out _UniqueId);
                return _UniqueId;
            }
        }

        public string CrossDeptTeam
        {
            get
            {
                if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReadAll))
                {
                    return "";
                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.CrossDeptTeam))
                {
                    ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(UserName));
                    if (!string.IsNullOrEmpty(emp.CrossDeptTeam))
                        return emp.CrossDeptTeam;
                    else if (emp.TeamNo == null)
                        return emp.DeptId + "[0]";
                    else
                        return emp.DeptId + "[" + emp.TeamNo + "]";
                }
                else
                {
                    return "";
                }


            }

        }

        public int Photographer
        {
            get
            {
                int flag = int.TryParse(ddlProposalPhotographer.SelectedValue, out flag) ? flag : 0;
                return flag;

            }
        }

        public int PhotoType
        {
            get
            {
                int flag = 0;
                if (chkFTPPhoto.Checked)
                    flag = flag | (int)ProposalSpecialFlag.FTPPhoto;
                if (chkSellerPhoto.Checked)
                    flag = flag | (int)ProposalSpecialFlag.SellerPhoto;

                return flag;

            }
        }

        #endregion

        #region event
        public event EventHandler Search;
        public event EventHandler Export;
        public event EventHandler Import;
        public event EventHandler<DataEventArgs<int>> OnGetCount;
        public event EventHandler<DataEventArgs<int>> PageChanged;
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IntialControls();
                EnabledSettings(UserName);
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            PageSize = Convert.ToInt32(ddlChangePage.SelectedValue);
            if (this.Search != null)
            {
                this.Search(this, e);
            }
            ucPager.ResolvePagerView(CurrentPage, true);
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (this.Export != null)
            {
                this.Export(this, e);
            }
        }
        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (this.Import != null)
            {
                this.Import(this, e);
            }
        }
        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChanged != null)
            {
                this.PageChanged(this, new DataEventArgs<int>(pageNumber));
            }
        }

        protected int GetCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetCount != null)
            {
                OnGetCount(this, e);
            }
            return e.Data;
        }

        protected void rptProposal_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewProposalSeller)
            {
                ViewProposalSeller pro = (ViewProposalSeller)e.Item.DataItem;
                HyperLink hkProposalId = (HyperLink)e.Item.FindControl("hkProposalId");
                PlaceHolder divFirstDeal = (PlaceHolder)e.Item.FindControl("divFirstDeal");
                Literal liFirstDeal = (Literal)e.Item.FindControl("liFirstDeal");
                Literal liSellerLevel = (Literal)e.Item.FindControl("liSellerLevel");
                HyperLink hkSellerName = (HyperLink)e.Item.FindControl("hkSellerName");
                Label lblDeliveryType = (Label)e.Item.FindControl("lblDeliveryType");
                Literal liDealType = (Literal)e.Item.FindControl("liDealType");
                Literal liDealSubType = (Literal)e.Item.FindControl("liDealSubType");
                Literal liProposalContent = (Literal)e.Item.FindControl("liProposalContent");
                Literal liMarketAnalysis = (Literal)e.Item.FindControl("liMarketAnalysis");
                Repeater rptSpecialFlag = (Repeater)e.Item.FindControl("rptSpecialFlag");
                Literal liOrderTimeS = (Literal)e.Item.FindControl("liOrderTimeS");
                Literal libUniqueId = (Literal)e.Item.FindControl("libUniqueId");
                Literal liSalesName = (Literal)e.Item.FindControl("liSalesName");
                Label lblStatus = (Label)e.Item.FindControl("lblStatus");

                //單號
                if (pro.ProposalSourceType == (int)ProposalSourceType.Original)
                {
                    hkProposalId.NavigateUrl = ResolveUrl("~/sal/ProposalContent.aspx?pid=" + pro.Id);
                }
                else if (pro.ProposalSourceType == (int)ProposalSourceType.House)
                {
                    hkProposalId.NavigateUrl = ResolveUrl("~/sal/proposal/house/proposalcontent?pid=" + pro.Id);
                }
                else
                {
                    hkProposalId.NavigateUrl = ResolveUrl("~/sal/ProposalContent.aspx?pid=" + pro.Id);
                }
                hkProposalId.Text = pro.Id.ToString();

                //類型
                lblDeliveryType.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (DeliveryType)pro.DeliveryType);

                switch ((DeliveryType)pro.DeliveryType)
                {
                    case DeliveryType.ToShop:
                        lblDeliveryType.BackColor = System.Drawing.Color.Pink;
                        break;
                    case DeliveryType.ToHouse:
                        lblDeliveryType.BackColor = System.Drawing.Color.Orange;
                        break;
                    default:
                        break;
                }
                liDealType.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDealType)pro.DealType);
                if(pro.ProposalSourceType == (int)ProposalSourceType.House)
                {
                    List<SystemCode> dealType = BizLogic.Component.SystemCodeManager.GetDealType().Where(x => x.ParentCodeId == null && x.CodeId != 1999).ToList();
                    var typeName = dealType.Where(x=>x.CodeId == pro.DealType1).FirstOrDefault();
                    if(typeName != null)
                    {
                        liDealSubType.Text = typeName.CodeName;
                    }
                    else
                    {
                        liDealSubType.Text = string.Empty;
                    }
                }
                else
                {
                    liDealSubType.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDealSubType)pro.DealSubType);
                }
                

                //規模
                liSellerLevel.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, pro.SellerLevel.HasValue ? (SellerLevel)pro.SellerLevel : SellerLevel.None).Split('：')[0];
                if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.FirstDeal))
                {
                    divFirstDeal.Visible = true;
                    liFirstDeal.Text = Helper.GetLocalizedEnum(ProposalSpecialFlag.FirstDeal);
                }

                //商家名稱
                hkSellerName.NavigateUrl = ResolveUrl("~/sal/SellerContent.aspx?sid=" + pro.SellerGuid);
                if (pro.SellerGuid == Guid.Empty)
                    hkSellerName.Text = "";
                else
                    hkSellerName.Text = pro.SellerName;

                //取得後台資料
                IViewPponDeal vpd = null;
                if (pro.BusinessHourGuid != null)
                {
                    vpd = PponFacade.ViewPponDealGetByGuid(pro.BusinessHourGuid.Value);

                }

                //提案內容
                ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealGetByPid(pro.Id);


                string NoSellerGuid = "";
                if (pro.SellerGuid == Guid.Empty)
                    NoSellerGuid = "<提案諮詢>";

                if (multiDeals != null && multiDeals.Count > 0)
                {
                    List<string> content = new List<string>();
                    ProposalMultiDeal item = multiDeals.OrderBy(x => x.ItemPrice).FirstOrDefault();

                    string name = NoSellerGuid + string.Format("{0}折！{1}", item.OrigPrice.Equals(0) ? 0 : Math.Round((item.ItemPrice / item.OrigPrice) * 10, 1), !string.IsNullOrEmpty(pro.BrandName) ? "【" + pro.BrandName + "】" : string.Empty);
                    
                    //統一帶入最低的毛利率數字
                    string grossMargin = "0";
                    if (vpd != null && vpd.IsLoaded)
                    {
                        grossMargin = string.Format("{0}%", 
                            Math.Round(PponFacade.GetDealMinimumGrossMargin(vpd.BusinessHourGuid) * 100, 2, MidpointRounding.AwayFromZero));
                    }
                    else
                    {
                        if (!Helper.IsFlagSet(Convert.ToInt32(pro.BusinessCreateFlag), (int)ProposalBusinessCreateFlag.Created))
                        {
                            List<double> tmpGrossMargin = new List<double>();
                            foreach (ProposalMultiDeal deals in multiDeals)
                            {
                                tmpGrossMargin.Add(ProposalFacade.GetGrossMargin(pro, deals));
                            }
                            grossMargin = tmpGrossMargin.OrderBy(x => x).FirstOrDefault() + "%";
                        }
                        else
                        {
                            grossMargin = ProposalFacade.GetGrossMargin(pro, item) + "%";
                        }
                    }

                    if (pro.BusinessHourGuid != null)
                        content.Add(string.Format("<a target='_blank' href='{0}'>{1}</a>&nbsp;&nbsp;<span style='color: #BF0000'>{2}</span>", ResolveUrl("~/ppon/default.aspx?bid=" + pro.BusinessHourGuid), name, grossMargin));
                    else
                        content.Add(string.Format("{0}&nbsp;&nbsp;<span style='color: #BF0000'>{1}</span>", name, grossMargin));


                    liProposalContent.Text = string.Join("<br />", content);
                }
                else
                {
                    if (pro.BusinessHourGuid != null)
                        liProposalContent.Text = string.Format("<a target='_blank' href='{0}'>" + NoSellerGuid + "(無)</a>", ResolveUrl("~/ppon/default.aspx?bid=" + pro.BusinessHourGuid));
                    else
                        liProposalContent.Text = NoSellerGuid + "(無)";
                }

                //提案內容-市場分析
                liMarketAnalysis.Text = pro.MarketAnalysis.Length > 100 ? pro.MarketAnalysis.Substring(0, 100) + "..." : pro.MarketAnalysis;

                //提案內容-特殊標記
                Dictionary<string, System.Drawing.Color> flags = new Dictionary<string, System.Drawing.Color>();
                foreach (var flag in Enum.GetValues(typeof(ProposalSpecialFlag)))
                {
                    if ((ProposalSpecialFlag)flag == ProposalSpecialFlag.FirstDeal)
                    {
                        continue;
                    }
                    if (Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)flag))
                    {
                        if ((ProposalSpecialFlag)flag == ProposalSpecialFlag.Urgent)
                        {
                            if (!flags.Keys.Contains(Helper.GetLocalizedEnum((ProposalSpecialFlag)flag)))
                            {
                                flags.Add(Helper.GetLocalizedEnum((ProposalSpecialFlag)flag), System.Drawing.Color.Pink);
                            }

                        }
                        else
                        {
                            if (!flags.Keys.Contains(Helper.GetLocalizedEnum((ProposalSpecialFlag)flag)))
                            {
                                flags.Add(Helper.GetLocalizedEnum((ProposalSpecialFlag)flag), System.Drawing.Color.Transparent);
                            }
                        }
                    }
                }
                if (pro.IsEarlierPageCheck)
                {
                    flags.Add("頁", System.Drawing.Color.Orange);
                }
                if(pro.ProposalSourceType == (int)ProposalSourceType.House)
                {
                    flags.Add("新宅配", System.Drawing.Color.YellowGreen);
                }
                if (pro.ProposalCreatedType == (int)Core.ProposalCreatedType.Seller)
                {
                    flags.Add("商", System.Drawing.Color.YellowGreen);
                }
                if (pro.CopyType != (int)ProposalCopyType.None)
                {
                    switch ((ProposalCopyType)pro.CopyType)
                    {
                        case ProposalCopyType.Similar:
                            if (!flags.Keys.Contains(Helper.GetLocalizedEnum((ProposalCopyType)pro.CopyType)))
                            {
                                flags.Add(Helper.GetLocalizedEnum((ProposalCopyType)pro.CopyType), System.Drawing.Color.Chartreuse);
                            }
                            break;
                        case ProposalCopyType.Same:
                            if (!flags.Keys.Contains(Helper.GetLocalizedEnum((ProposalCopyType)pro.CopyType)))
                            {
                                flags.Add(Helper.GetLocalizedEnum((ProposalCopyType)pro.CopyType), System.Drawing.Color.NavajoWhite);
                            }
                            break;
                        case ProposalCopyType.None:
                        default:
                            break;
                    }
                }
                if (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.ShipType == (int)DealShipType.Ship72Hrs)
                {
                    flags.Add(Helper.GetLocalizedEnum(DealShipType.Ship72Hrs), System.Drawing.Color.Yellow);
                }
                if (pro.IsWms)
                {
                    flags.Add("24到貨", System.Drawing.Color.Pink);
                }
                if (pro.IsTaishinChosen)
                {
                    flags.Add("台新特談商品", System.Drawing.Color.LightBlue);
                }
                rptSpecialFlag.DataSource = flags;
                rptSpecialFlag.DataBind();

                //上檔日、檔號
                if(pro.ProposalSourceType == (int)ProposalSourceType.House)
                {
                    if (vpd != null && vpd.IsLoaded)
                    {
                        if (vpd.IsLoaded && vpd.BusinessHourOrderTimeS.Year != DateTime.MinValue.Year && vpd.BusinessHourOrderTimeS.Year != DateTime.MaxValue.Year)
                        {
                            liOrderTimeS.Text = vpd.BusinessHourOrderTimeS.ToString("yyyy/MM/dd");
                        }
                    }                  
                }
                else
                {
                    liOrderTimeS.Text = pro.OrderTimeS.HasValue ? pro.OrderTimeS.Value.ToString("yyyy/MM/dd") : string.Empty;
                }                
                if (vpd != null && vpd.IsLoaded)
                {
                    if (vpd.UniqueId != null)
                    {
                        libUniqueId.Text = vpd.UniqueId.Value.ToString();
                    }
                }

                //業務
                ProposalSalesModel model = ProposalFacade.ProposalSaleGetByPid(pro.Id);
                liSalesName.Text = model.DevelopeSalesEmpName + "<br>" + model.OperationSalesEmpName;

                //檢核狀態
                if(pro.ProposalSourceType == (int)ProposalSourceType.House)
                {
                    switch (pro.Status)
                    {
                        case (int)ProposalStatus.Apply:
                            lblStatus.Text = Helper.GetDescription(ProposalHouseStatus.VbsApprove);
                            break;
                        case (int)ProposalStatus.Approve:
                            lblStatus.Text = Helper.GetDescription(ProposalHouseStatus.SaleApprove);
                            break;
                        case (int)ProposalStatus.BusinessCheck:
                            lblStatus.Text = Helper.GetDescription(ProposalHouseStatus.ManagerApprove);
                            break;
                        case (int)ProposalStatus.Listing:
                            lblStatus.Text = Helper.GetDescription(ProposalHouseStatus.ProductionApprove);
                            break;
                        default:
                            lblStatus.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalStatus)pro.Status);
                            break;
                    }                    
                }
                else
                {
                    lblStatus.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalStatus)pro.Status);
                }                

                string flagName = GetFlagName(pro.Status,
                    pro.BusinessCreateFlag,
                    pro.BusinessFlag,
                    pro.EditFlag,
                    pro.ListingFlag,
                    pro.SellerProposalFlag
                    );
                if (!string.IsNullOrEmpty(flagName))
                {
                    if(pro.ProposalSourceType == (int)ProposalSourceType.House)
                    {
                        lblStatus.Text += "<br />" + (pro.ModifyTime ?? pro.CreateTime).ToString("yyyy/MM/dd HH:mm:ss");
                    }
                    else
                    {
                        lblStatus.Text += "(" + flagName + ")<br />" + (pro.ModifyTime ?? pro.CreateTime).ToString("yyyy/MM/dd HH:mm:ss");
                    }                    
                }


                switch ((ProposalStatus)pro.Status)
                {
                    case ProposalStatus.SellerCreate:
                    case ProposalStatus.Initial:
                        lblStatus.ForeColor = System.Drawing.Color.Gray;
                        break;
                    case ProposalStatus.Apply:
                        lblStatus.ForeColor = System.Drawing.Color.Orange;
                        break;
                    case ProposalStatus.Approve:
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        break;
                    case ProposalStatus.Created:
                        lblStatus.ForeColor = System.Drawing.Color.Blue;
                        break;
                    case ProposalStatus.BusinessCheck:
                        lblStatus.ForeColor = System.Drawing.Color.Purple;
                        break;
                    case ProposalStatus.Editor:
                        lblStatus.ForeColor = System.Drawing.Color.Green;
                        break;
                    case ProposalStatus.Listing:
                        lblStatus.ForeColor = System.Drawing.Color.Black;
                        break;
                    default:
                        break;
                }

            }

        }

        protected void rptSpecialFlag_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<string, System.Drawing.Color>)
            {
                KeyValuePair<string, System.Drawing.Color> dataItem = (KeyValuePair<string, System.Drawing.Color>)e.Item.DataItem;
                Label lblFlag = (Label)e.Item.FindControl("lblFlag");
                lblFlag.Text = dataItem.Key;
                lblFlag.BackColor = dataItem.Value;
            }
        }

        protected void rptStatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<ProposalStatus, Dictionary<Enum, bool>>)
            {
                KeyValuePair<ProposalStatus, Dictionary<Enum, bool>> dataItem = (KeyValuePair<ProposalStatus, Dictionary<Enum, bool>>)e.Item.DataItem;

                Literal liStatus = (Literal)e.Item.FindControl("liStatus");
                CheckBox chkStatus = (CheckBox)e.Item.FindControl("chkStatus");
                HiddenField hidStatus = (HiddenField)e.Item.FindControl("hidStatus");
                Repeater rptFlag = (Repeater)e.Item.FindControl("rptFlag");

                liStatus.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalStatus)dataItem.Key);
                hidStatus.Value = ((int)((ProposalStatus)dataItem.Key)).ToString();

                rptFlag.DataSource = dataItem.Value.ToDictionary(x => x, y => dataItem.Key);
                rptFlag.DataBind();
            }
        }

        protected void rptFlag_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<KeyValuePair<Enum, bool>, ProposalStatus>)
            {
                KeyValuePair<KeyValuePair<Enum, bool>, ProposalStatus> dataItem = (KeyValuePair<KeyValuePair<Enum, bool>, ProposalStatus>)e.Item.DataItem;

                Literal liFlag = (Literal)e.Item.FindControl("liFlag");
                CheckBox chkFlag = (CheckBox)e.Item.FindControl("chkFlag");
                HiddenField hidFlag = (HiddenField)e.Item.FindControl("hidFlag");

                KeyValuePair<Type, string> flagType = SellerFacade.GetProposalFlagsTypeByStatus(dataItem.Value);
                int tmp = 0;
                switch (((ProposalStatus)dataItem.Value))
                {
                    case ProposalStatus.Initial:
                        break;
                    case ProposalStatus.Apply:
                        tmp = (int)(ProposalApplyFlag)dataItem.Key.Key;
                        break;
                    case ProposalStatus.Approve:
                        tmp = (int)(ProposalApproveFlag)dataItem.Key.Key;
                        break;
                    case ProposalStatus.Created:
                        tmp = (int)(ProposalBusinessCreateFlag)dataItem.Key.Key;
                        break;
                    case ProposalStatus.BusinessCheck:
                        tmp = (int)(ProposalBusinessFlag)dataItem.Key.Key;
                        break;
                    case ProposalStatus.Editor:
                        tmp = (int)(ProposalEditorFlag)dataItem.Key.Key;
                        break;
                    case ProposalStatus.Listing:
                        tmp = (int)(ProposalListingFlag)dataItem.Key.Key;
                        break;
                    default:
                        break;
                }


                if (flagType.Key != null)
                {
                    liFlag.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, dataItem.Key.Key);
                    hidFlag.Value = tmp.ToString();

                }
            }
        }

        #endregion

        #region method
        public void SetProposalList(ViewProposalSellerCollection dataList)
        {
            //批次匯入匯出提案單
            if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.TransferProposal))
            {
                divTransferProposal.Visible = true;
            }
            divProposalList.Visible = true;
            rptProposal.DataSource = dataList.OrderByDescending(x => x.ModifyTime);
            rptProposal.DataBind();
        }

        private string GetFlagName(int status, int BusinessCreateFlag, int BusinessFlag, int EditFlag, int ListingFlag, int SellerFlag)
        {
            string name = "";
            switch (status)
            {
                case (int)ProposalStatus.SellerCreate:// 商家提案單
                    if (Helper.IsFlagSet(SellerFlag, SellerProposalFlag.Initial))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.Initial);
                    }
                    if (Helper.IsFlagSet(SellerFlag, SellerProposalFlag.Apply))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.Apply);
                    }
                    if (Helper.IsFlagSet(SellerFlag, SellerProposalFlag.Send))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.Send);
                    }
                    if (Helper.IsFlagSet(SellerFlag, SellerProposalFlag.SaleEditor))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.SaleEditor);
                    }
                    if (Helper.IsFlagSet(SellerFlag, SellerProposalFlag.CheckWaitting))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.CheckWaitting);
                    }
                    if (Helper.IsFlagSet(SellerFlag, SellerProposalFlag.SellerEditor))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.SellerEditor);
                    }
                    if (Helper.IsFlagSet(SellerFlag, SellerProposalFlag.Check))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.Check);
                    }
                    if (Helper.IsFlagSet(SellerFlag, SellerProposalFlag.SaleSend))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.SaleSend);
                    }
                    if (Helper.IsFlagSet(SellerFlag, SellerProposalFlag.SaleCreated))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.SaleCreated);
                    }
                    break;
                case (int)ProposalStatus.Initial:    //未申請
                    break;
                case (int)ProposalStatus.Apply:      //提案申請
                    name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalApplyFlag.Apply);
                    break;
                case (int)ProposalStatus.Approve:    //申請核准
                    name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalApproveFlag.QCcheck);
                    break;
                case (int)ProposalStatus.Created:    //建檔檢核
                    if (Helper.IsFlagSet(BusinessCreateFlag, ProposalBusinessCreateFlag.Created))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalBusinessCreateFlag.Created);
                    }
                    if (Helper.IsFlagSet(BusinessCreateFlag, ProposalBusinessCreateFlag.SettingCheck))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalBusinessCreateFlag.SettingCheck);
                    }
                    break;
                case (int)ProposalStatus.BusinessCheck:  //檔次檢核
                    if (Helper.IsFlagSet(BusinessFlag, ProposalBusinessFlag.ProposalAudit))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalBusinessFlag.ProposalAudit);
                    }
                    if (Helper.IsFlagSet(BusinessFlag, ProposalBusinessFlag.ScheduleCheck))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalBusinessFlag.ScheduleCheck);
                    }
                    if (Helper.IsFlagSet(BusinessFlag, ProposalBusinessFlag.PhotographerCheck))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalBusinessFlag.PhotographerCheck);
                    }
                    break;
                case (int)ProposalStatus.Editor:     //編輯檢核
                    if (Helper.IsFlagSet(EditFlag, ProposalEditorFlag.Setting))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalEditorFlag.Setting);
                    }
                    if (Helper.IsFlagSet(EditFlag, ProposalEditorFlag.CopyWriter))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalEditorFlag.CopyWriter);
                    }
                    if (Helper.IsFlagSet(EditFlag, ProposalEditorFlag.ImageDesign))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalEditorFlag.ImageDesign);
                    }
                    if (Helper.IsFlagSet(EditFlag, ProposalEditorFlag.ART))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalEditorFlag.ART);
                    }
                    if (Helper.IsFlagSet(EditFlag, ProposalEditorFlag.Listing))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalEditorFlag.Listing);
                    }
                    break;
                case (int)ProposalStatus.Listing:    //上架檢核
                    if (Helper.IsFlagSet(ListingFlag, ProposalListingFlag.PageCheck))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalListingFlag.PageCheck);
                    }
                    if (Helper.IsFlagSet(ListingFlag, ProposalListingFlag.PaperContractCheck))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalListingFlag.PaperContractCheck);
                    }
                    if (Helper.IsFlagSet(ListingFlag, ProposalListingFlag.FinanceCheck))
                    {
                        name = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ProposalListingFlag.FinanceCheck);
                    }
                    break;
                default:
                    break;
            }
            return name;
        }

        public void ShowMessage(string msg)
        {
            string script = string.Format("alert('{0}');", msg);
            Page.ClientScript.RegisterStartupScript(GetType(), "alert", script, true);
        }
        #endregion

        #region private method

        private void IntialControls()
        {
            // 特殊標記
            Dictionary<int, string> specialflags = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(ProposalSpecialFlag)))
            {
                if (item.ToString() != ProposalSpecialFlag.FTPPhoto.ToString() && item.ToString() != ProposalSpecialFlag.SellerPhoto.ToString())
                    specialflags[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalSpecialFlag)item);
            }

            specialflags[-10] = "超商取貨";
            specialflags[-11] = "活動遊戲檔";
            specialflags[-12] = "一般宅配 (自出含快速出貨)";
            specialflags[-13] = "24H到貨 (倉儲+物流)";

            ddlSpecialFlag.DataSource = specialflags;
            ddlSpecialFlag.DataTextField = "Value";
            ddlSpecialFlag.DataValueField = "Key";
            ddlSpecialFlag.DataBind();

            // 業務部門
            ddlDept.DataSource = HumanFacade.GetSalesDepartment();
            ddlDept.DataTextField = "DeptName";
            ddlDept.DataValueField = "DeptId";
            ddlDept.DataBind();

            // 類型
            Dictionary<int, string> deliveryType = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(DeliveryType)))
            {
                deliveryType[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (DeliveryType)item);
            }
            deliveryType[10] = "(舊)宅配";
            deliveryType[11] = "(新)宅配";
            ddlDeliveryType.DataSource = deliveryType;
            ddlDeliveryType.DataTextField = "Value";
            ddlDeliveryType.DataValueField = "Key";
            ddlDeliveryType.DataBind();

            //新宅配類型
            Dictionary<int, string> newHouseType = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(ProposalHouseStatus)))
            {
                newHouseType[(int)item] = Helper.GetDescription((ProposalHouseStatus)item);
            }
            ddlNewHouseStatus.DataSource = newHouseType;
            ddlNewHouseStatus.DataTextField = "Value";
            ddlNewHouseStatus.DataValueField = "Key";
            ddlNewHouseStatus.DataBind();

            // 複製類型
            Dictionary<int, string> copyType = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(ProposalCopyType)))
            {
                copyType[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalCopyType)item);
            }
            ddlProposalCopyType.DataSource = copyType;
            ddlProposalCopyType.DataTextField = "Value";
            ddlProposalCopyType.DataValueField = "Key";
            ddlProposalCopyType.DataBind();

            // 檢核狀態
            Dictionary<int, string> status = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(ProposalStatus)))
            {
                int s = (int)item;
                if (s != 0 && s != -1)
                {
                    status[s] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalStatus)item);
                }
            }
            ddlProposalStatus.DataSource = status;
            ddlProposalStatus.DataTextField = "Value";
            ddlProposalStatus.DataValueField = "Key";
            ddlProposalStatus.DataBind();

            // 提案類型
            Dictionary<int, string> dealType = new Dictionary<int, string>();

            foreach (var item in Enum.GetValues(typeof(ProposalDealType)))
            {
                if (item.ToString() == ProposalDealType.None.ToString())
                {
                    dealType[(int)item] = "請選擇";
                }
                else
                {
                    dealType[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDealType)item);
                }
            }
            ddlDealType.DataSource = dealType;//.Where(x => x.Key != (int)ProposalDealType.None);
            ddlDealType.DataTextField = "Value";
            ddlDealType.DataValueField = "Key";
            ddlDealType.DataBind();

            //檔次類型
            Dictionary<int, string> dealType1 = new Dictionary<int, string>();
            List<SystemCode> dealTypeCodes = BizLogic.Component.SystemCodeManager.GetDealType().Where(x => x.ParentCodeId == null && x.CodeId != 1999).ToList();

            dealType[0] = "請選擇";
            foreach (var item in dealTypeCodes)
            {
                dealType1[item.CodeId] = item.CodeName;
            }
            ddlDealType1.DataSource = dealType1;//.Where(x => x.Key != (int)ProposalDealType.None);
            ddlDealType1.DataTextField = "Value";
            ddlDealType1.DataValueField = "Key";
            ddlDealType1.DataBind();

            //提案來源
            Dictionary<int, string> proposalCreatedType = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(ProposalCreatedType)))
            {
                proposalCreatedType[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalCreatedType)item);
            }
            ddlProposalCreatedType.DataSource = proposalCreatedType;
            ddlProposalCreatedType.DataTextField = "Value";
            ddlProposalCreatedType.DataValueField = "Key";
            ddlProposalCreatedType.DataBind();

            //約拍狀態
            Dictionary<int, string> proposalPhotographer = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(ProposalPhotographer)))
            {
                if (item.ToString() != ProposalPhotographer.Initial.ToString())
                    proposalPhotographer[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalPhotographer)item);
            }
            ddlProposalPhotographer.DataSource = proposalPhotographer;
            ddlProposalPhotographer.DataTextField = "Value";
            ddlProposalPhotographer.DataValueField = "Key";
            ddlProposalPhotographer.DataBind();

            // 檢核狀態(進階查詢)
            Dictionary<ProposalStatus, Dictionary<Enum, bool>> data = new Dictionary<ProposalStatus, Dictionary<Enum, bool>>();
            foreach (var item in Enum.GetValues(typeof(ProposalStatus)))
            {
                ProposalStatus statusitem = (ProposalStatus)item;

                //無商家只顯示到申請核准
                if (statusitem == ProposalStatus.Initial || statusitem == ProposalStatus.SellerCreate)
                {
                    continue;
                }
                KeyValuePair<Type, string> flagType = SellerFacade.GetProposalFlagsTypeByStatus(statusitem);

                Dictionary<Enum, bool> flags = new Dictionary<Enum, bool>();
                foreach (var f in Enum.GetValues(flagType.Key))
                {
                    if ((int)f != 0)
                    {
                        flags.Add((Enum)f, true);
                    }
                }

                data.Add(statusitem, flags);
            }

            rptStatus.DataSource = data;
            rptStatus.DataBind();
        }

        private void EnabledSettings(string username)
        {
            ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(username));
            if (emp.IsLoaded)
            {
                if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.ReadAll))
                {
                    #region 全區檢視

                    divSales.Visible = true;
                    divSalesDept.Visible = true;

                    #endregion
                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.CrossDeptTeam))
                {
                    #region 本區檢視

                    if (string.IsNullOrEmpty(emp.CrossDeptTeam) && emp.TeamNo == null)
                    {
                        ShowMessage("身份權限未明，無法查看資料，請洽產品或技術人員協助");
                    }
                    else
                    {
                        divSales.Visible = true;
                        if (ddlDept.Items.FindByValue(emp.DeptId) != null)
                        {

                        }
                        else
                        {
                            ShowMessage("本區檢視非業務單位，請重新檢視權限設定。");
                        }
                    }

                    #endregion
                }

                else
                {
                    #region 無檢視權限 (僅能查看自己建立的單)

                    hidSalesId.Value = emp.UserId.ToString();

                    #endregion
                }

                if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.ProposalConsultation))
                {
                    btnCreateProposal.Visible = true;
                }

                if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.BatchCreateProposal))
                {
                    btnBatchCreateProposal.Visible = true;
                }
            }
            else
            {
                ShowMessage("查無員工資料，請重新檢視員工資料維護設定。");
            }

        }

        

        #endregion

        #region WS
        [WebMethod]
        public static dynamic GetFlagList(int p)
        {
            Dictionary<int, string> data = new Dictionary<int, string>();
            ProposalStatus status = (ProposalStatus)p;
            KeyValuePair<Type, string> flagType = SellerFacade.GetProposalFlagsTypeByStatus(status);
            if (flagType.Key != null)
            {
                foreach (var item in Enum.GetValues(flagType.Key))
                {
                    if ((int)item == 0)
                    {
                        continue;
                    }
                    data[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (Enum)item);
                }
            }
            return new JsonSerializer().Serialize(data);
        }


        [WebMethod]
        public static int CreateProposal(string UserName, bool IsTravelProposal, bool IsPBeautyProposal, int ProposalDeliveryType)
        {
            return ProposalFacade.CreateProposal(UserName, "", Guid.Empty, UserName, ProposalDeliveryType, IsTravelProposal, IsPBeautyProposal, false, false, LunchKingSite.Core.ProposalCreatedType.Sales);

        }

        /// <summary>
        /// 檢查負責業務Email
        /// </summary>
        /// <param name="email">負責業務Email</param>
        /// <returns></returns>
        [WebMethod]
        public static bool SalesEmailExist(string email)
        {
            if (string.IsNullOrEmpty(email))
                return true;

            ViewEmployee emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, email);
            return !string.IsNullOrEmpty(emp.Email);
        }
        #endregion

    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="ProposalMultiDeals.aspx.cs" Inherits="LunchKingSite.Web.Sal.ProposalMultiDeals" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Model" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        $(document).ready(function () {

            // 宅配才須顯示運費/免運門檻等欄位
            if ($('#hidDeliveryType').val() == '<%= (int)DeliveryType.ToShop %>') {
                $('.product').hide();
            }
        });

        function MultiSaveCheck() {
            $('.if-error').hide();
            $('.error').removeClass('error');

            var check = true;

            if ($.trim($('#txtItemName').val()) == '') {
                $('#pMultiItemName').show();
                $('#pMultiItemName').parent().parent().addClass('error');
                check = false;
            }

            if ($.trim($('#txtItemPrice').val()) == '0') {
                $('#pZeroItemPrice').show();
                $('#pZeroItemPrice').parent().parent().addClass('error');
                check = false;
            }

            if ($.trim($('#txtOrigPrice').val()) == '0') {
                $('#pZeroOrigPrice').show();
                $('#pZeroOrigPrice').parent().parent().addClass('error');
                check = false;
            }

            if ($.trim($('#txtOrigPrice').val()) == '' || $.trim($('#txtItemPrice').val()) == '' || $.trim($('#txtCost').val()) == '') {
                check = false;
                $('#pMultiItemPrice').show();
                $('#pMultiItemPrice').parent().parent().addClass('error');
            }

            if ($.trim($('#txtOrderTotalLimit').val()) == '') {
                $('#pOrderTotalLimit').show();
                $('#pOrderTotalLimit').parent().parent().addClass('error');
                check = false;
            }

            return check;
        }

        function ValidateNumber(e, pnumber) {
            if (!/^\d+$/.test(pnumber)) {
                $(e).val(/^\d+/.exec($(e).val()));
            }
            return false;
        }

        function calculateAtmMaximun(obj) {
            var c = parseInt($(obj).val());
            if (c != 0) {
                $('#txtAtmMaximum').val(Math.floor(c / 3));
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <div class="mc-content">
        <asp:UpdatePanel ID="UpdatePanel" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hidDeliveryType" runat="server" ClientIDMode="Static" />
                <asp:Panel ID="divMultiDealSet" runat="server" DefaultButton="btnSave">
                    <h1 class="rd-smll">多檔次設定
                        <asp:PlaceHolder ID="divOpen" runat="server">
                            <asp:Button ID="btnOpen" runat="server" ClientIDMode="Static" CssClass="btn btn-mini" Style="vertical-align: super;" Text="新增多檔次" Visible="false" OnClick="btnOpen_Click" />
                        </asp:PlaceHolder>
                        <asp:HyperLink ID="hkBackProposal" runat="server" ClientIDMode="Static" ForeColor="#0088cc" Font-Size="Medium" Text="回提案單" Style="font-size: large;"></asp:HyperLink>
                    </h1>
                    <hr class="header_hr">
                    <asp:Panel ID="divMultiEdit" runat="server" CssClass="grui-form" Visible="false">
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;專案內容</label>
                            <div class="data-input rd-data-input">
                                <asp:HiddenField ID="hidMultiDealId" runat="server" ClientIDMode="Static" />
                                <asp:TextBox ID="txtItemName" runat="server" CssClass="input-half" Width="70%" ClientIDMode="Static"></asp:TextBox>
                                <p id="pMultiItemName" class="if-error" style="display: none;">請輸入專案內容。</p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                價格資訊</label>
                            <div class="data-input rd-data-input">
                                <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;<p>原價</p>
                                <asp:TextBox ID="txtOrigPrice" runat="server" CssClass="input-small" ClientIDMode="Static" onkeyup="return ValidateNumber($(this),value);" MaxLength="6" autocomplete="off"></asp:TextBox>
                                <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;<p>售價</p>
                                <asp:TextBox ID="txtItemPrice" runat="server" CssClass="input-small" ClientIDMode="Static" onkeyup="return ValidateNumber($(this),value);" MaxLength="6" autocomplete="off"></asp:TextBox>
                                <br />
                                <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;<p>進貨價</p>
                                <asp:TextBox ID="txtCost" runat="server" CssClass="input-small" ClientIDMode="Static" onkeyup="return ValidateNumber($(this),value);" MaxLength="6" autocomplete="off"></asp:TextBox>
                                <br />
                                <p>上架費(份數)</p>
                                <asp:TextBox ID="txtSlottingFeeQuantity" runat="server" CssClass="input-small" ClientIDMode="Static" onkeyup="return ValidateNumber($(this),value);" MaxLength="6" autocomplete="off"></asp:TextBox>
                                <asp:PlaceHolder ID="divMultiFreight" runat="server" Visible="false">
                                    <br />
                                    <p>運費</p>
                                    <asp:TextBox ID="txtFreight" runat="server" CssClass="input-small" ClientIDMode="Static" onkeyup="return ValidateNumber($(this),value);" MaxLength="6" autocomplete="off"></asp:TextBox>
                                    <p>免運門檻</p>
                                    <asp:TextBox ID="txtNonFreightLimit" runat="server" CssClass="input-small" ClientIDMode="Static" onkeyup="return ValidateNumber($(this),value);" MaxLength="6" autocomplete="off"></asp:TextBox><p>(元)</p>
                                </asp:PlaceHolder>
                                <p id="pMultiItemPrice" class="if-error" style="display: none;">請輸入價格資訊。</p>
                                <p id="pZeroItemPrice" class="if-error" style="display: none;">原價不得為零。</p>
                                <p id="pZeroOrigPrice" class="if-error" style="display: none;">售價不得為零。</p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                活動人數及限制</label>
                            <div class="data-input rd-data-input">
                                <span style="color: #BF0000; font-weight: bold;">*</span>&nbsp;<p>最大購買</p>
                                <asp:TextBox ID="txtOrderTotalLimit" runat="server" CssClass="input-small" ClientIDMode="Static" onblur="calculateAtmMaximun(this);" onkeyup="return ValidateNumber($(this),value);" MaxLength="6" autocomplete="off"></asp:TextBox>
                                <p id="pOrderTotalLimit" class="if-error" style="display: none;">請輸入最大購買數量。</p>
                                <br />
                                <p>ATM保留份數</p>
                                <asp:TextBox ID="txtAtmMaximum" runat="server" CssClass="input-small" ClientIDMode="Static" onkeyup="return ValidateNumber($(this),value);" MaxLength="6" autocomplete="off"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label rd-unit-label">
                                多重選項</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="txtOptions" runat="server" TextMode="MultiLine" Height="150px" Width="80%" ClientIDMode="Static"></asp:TextBox>
                                <p class="note" style="padding-left: 10px">
                                    分店:<br />
                                    [100] 永和店<br />
                                    [80] 中山店<br />
                                    <br />
                                    顏色:<br />
                                    紅色<br />
                                    白色
                                </p>
                            </div>
                        </div>
                        <div class="form-unit end-unit">
                            <div class="data-input rd-data-input">
                                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary rd-mcacstbtn" Text="儲存" OnClick="btnSave_Click" OnClientClick="return MultiSaveCheck();" Visible="false" />
                                <asp:PlaceHolder ID="divDelete" runat="server">
                                    <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-primary rd-mcacstbtn" ClientIDMode="Static" Text="刪除" OnClick="btnDelete_Click" Visible="false" OnClientClick="return confirm('確定刪除此優惠內容?');" />
                                </asp:PlaceHolder>
                                <asp:Button ID="btnClose" runat="server" ClientIDMode="Static" CssClass="btn" Text="關閉" Visible="false" OnClick="btnClose_Click" />
                            </div>
                        </div>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="divMultiDeals" runat="server" Visible="false" ClientIDMode="Static">
                    <div id="mc-table">
                        <asp:Repeater ID="rptMultiDeal" runat="server" OnItemCommand="rptMultiDeal_ItemCommand">
                            <HeaderTemplate>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                                    <tr class="rd-C-Hide">
                                        <th class="OrderDate">專案內容
                                        </th>
                                        <th class="OrderDate">原價
                                        </th>
                                        <th class="OrderSerial">售價
                                        </th>
                                        <th class="OrderSerial">毛利率
                                        </th>
                                        <th class="OrderName">進貨價
                                        </th>
                                        <th class="OrderDate">上架費(份)
                                        </th>
                                        <th class="OrderDate">最大購買
                                        </th>
                                        <th class="OrderDate">ATM保留
                                        </th>
                                        <th class="OrderDate product">運費
                                        </th>
                                        <th class="OrderDate product">免運門檻
                                        </th>
                                        <th class="OrderDate">多重選項</th>
                                        <th class="OrderDate"></th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="mc-tableContentITEM">
                                    <td class="OrderDate">
                                        <input class="DealId" type="hidden" value="<%# ((ProposalMultiDealModel)Container.DataItem).Id %>" />
                                        <p class=" ItemName">
                                            <%# ((ProposalMultiDealModel)Container.DataItem).ItemName %>
                                        </p>
                                    </td>
                                    <td class="OrderDate">
                                        <span class="rd-Detailtitle">原價：</span>
                                        <span class="OrigPrice">
                                            <%# ((ProposalMultiDealModel)Container.DataItem).OrigPrice.ToString("F0") %>
                                        </span>
                                    </td>
                                    <td class="OrderSerial">
                                        <span class="rd-Detailtitle">售價：</span>
                                        <span class="ItemPrice">
                                            <%# ((ProposalMultiDealModel)Container.DataItem).ItemPrice.ToString("F0") %>
                                        </span>
                                    </td>
                                    <td class="OrderSerial">
                                        <span class="rd-Detailtitle">毛利率：</span>
                                        <span style="color: #BF0000"><%# Math.Round((((ProposalMultiDealModel)Container.DataItem).ItemPrice - ((ProposalMultiDealModel)Container.DataItem).Cost)/((ProposalMultiDealModel)Container.DataItem).ItemPrice * 100, 2) %>&nbsp;%</span>
                                    </td>
                                    <td class="OrderName">
                                        <span class="rd-Detailtitle">進貨價：</span>
                                        <span class="Cost">
                                            <%# ((ProposalMultiDealModel)Container.DataItem).Cost.ToString("F0") %>
                                        </span>
                                    </td>
                                    <td class="OrderDate">
                                        <span class="rd-Detailtitle">上架費(份)：</span>
                                        <span class="SlottingFeeQuantity">
                                            <%# ((ProposalMultiDealModel)Container.DataItem).SlottingFeeQuantity %>
                                        </span>
                                    </td>
                                    <td class="OrderDate">
                                        <span class="rd-Detailtitle">最大購買：</span>
                                        <span class="OrderTotalLimit">
                                            <%# ((ProposalMultiDealModel)Container.DataItem).OrderTotalLimit.ToString("F0") %>
                                        </span>
                                    </td>
                                    <td class="OrderDate">
                                        <span class="rd-Detailtitle">ATM保留：</span>
                                        <span class="AtmMaximum">
                                            <%# ((ProposalMultiDealModel)Container.DataItem).AtmMaximum %>
                                        </span>
                                    </td>
                                    <td class="OrderDate product">
                                        <span class="rd-Detailtitle">運費：</span>
                                        <span class="Freights">
                                            <%# ((ProposalMultiDealModel)Container.DataItem).Freights.ToString("F0") %>
                                        </span>
                                    </td>
                                    <td class="OrderDate product">
                                        <span class="rd-Detailtitle">免運門檻：</span>
                                        <span class="NoFreightLimit">
                                            <%# ((ProposalMultiDealModel)Container.DataItem).NoFreightLimit.ToString("F0") %>
                                        </span>
                                    </td>
                                    <td class="OrderDate" style="width: 80px; text-align: left;">
                                        <span>
                                            <%# (((ProposalMultiDealModel)Container.DataItem).Options.Length > 30 ? ((ProposalMultiDealModel)Container.DataItem).Options.Substring(0, 30) + "..." : ((ProposalMultiDealModel)Container.DataItem).Options) %>
                                        </span>
                                        <span class="Options" style="display: none;">
                                            <%# ((ProposalMultiDealModel)Container.DataItem).Options %>
                                        </span>
                                    </td>
                                    <td class="OrderDate">
                                        <asp:Button ID="btnEdit" runat="server" CssClass="btn btn-small" Text="編輯" CommandArgument="<%# ((ProposalMultiDealModel)Container.DataItem).Id %>" CommandName="UPD" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

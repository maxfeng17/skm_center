﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    EnableEventValidation="true" CodeBehind="BusinessOrderDetail.aspx.cs" Inherits="LunchKingSite.Web.Sal.BusinessOrderDetail" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Model.Sales" %>
<%@ Import Namespace="LunchKingSite.Core" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link type="text/css" href="../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
    <script type="text/javascript" src="../Tools/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="../Tools/js/json2.js"></script>
    <script type="text/javascript" src="../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../Tools/js/jquery.numeric.js"></script>
    <script type="text/javascript" src="../Tools/js/jquery.checkboxtree.js"></script>
    <link type="text/css" href="../Tools/js/jquery.checkboxtree.css" rel="stylesheet" />
    <style type="text/css">
        #dealCategoryTable label,ul[id^="dealCategorytree"] label
        {
            display:initial;
        }
        #dealCategoryTable ul,ul[id^="dealCategorytree"]
        {
            list-style-type: none;
            margin: 0 0 0 0;
            padding-left: 20px;

        }
        #dealCategoryTable li,ul[id^="dealCategorytree"] li
        {
            font-size: 12px;
            font-weight:normal;
            font-family: Trebuchet MS, Tahoma, Verdana, Arial, sans-serif;
        }

       ul
        {
            list-style-type: none;
            margin: 0 0 0 0;
        }
        li
        {
            color: #4D4D4D;
            font-weight: bold;
        }


        #dealCategoryTable td,ul[id^="dealCategorytree"] td
        {
            vertical-align: text-top;
            margin-left: 0px;
        }        
         .content {
             display: none;
         }

        .mc-menu-btn, .mbe-menu2 {
            display: none;
        }

        #navcontainer {
            margin: 10px 0 0 10px;
            padding: 0;
        }

        #navcontainer ul {
            border: 0;
            margin: 0;
            padding: 0;
            list-style-type: none;
            text-align: center;
        }

        #navcontainer ul li {
            display: block;
            float: left;
            text-align: center;
            padding: 0;
            margin: 0;
            vertical-align: middle;
        }

        #navcontainer ul li span {
            background: #fff;
            width: 176px;
            height: 24px;
            border-top: 1px solid #f5d7b4;
            border-left: 1px solid #f5d7b4;
            border-bottom: 1px solid #f5d7b4;
            border-right: none;
            padding: 0;
            margin: 0 0 0 0;
            color: #d2691e;
            text-decoration: none;
            display: block;
            text-align: center;
            font: normal 14px/20px "Lucida Grande", "Lucida Sans Unicode", verdana, lucida, sans-serif;
            cursor: pointer;
        }

        #navcontainer ul li span:hover {
            color: #930;
            background: #f5d7b4;
        }

        #navcontainer span .active {
            background: #c60;
            color: #fff;
        }

        #navcontainer .active span {
            background: #c60;
            border: 1px solid #c60;
            color: #fff;
        }

        #navcontainer .active span:hover {
            background: #c60;
            border: 1px solid #c60;
            color: #fff;
            cursor: default;
        }

        .ShowDivClass {
            margin: 0 0 0 10px;
            border: 1px solid #bbb;
            padding-left: 30px;
            padding-top: 10px;
            width: 855px;
            border-bottom-width: 0;
        }

        #ShowDiv0 table tr td, #ShowDiv1 table tr td, #ShowDiv1 table tr th {
            border-bottom: 0px #BBB dashed;
        }

        #ShowDiv0 table tr td .AuthorizationGroup tr td {
            padding: 0 0 0 0;
        }

        #ShowDiv0 .Title {
            font-weight: bold;
            padding-bottom: 3px;
        }

        #ShowDiv1 .Title {
            padding-bottom: 3px;
        }

        label {
            font-weight: normal;
        }

        .Pointer {
            cursor: pointer;
        }

        .ulShowDiv > li {
            border-bottom: 0px #BBB dashed;
        }

        .PrintClass input, .PrintClass textarea {
            font-size: 22px;
        }

        h2 {
            page-break-before: always;
        }

        fieldset {
            border: solid 1px gray;
            display: block;
            font-size: medium;
            width: 700px;
            -webkit-margin-start: 2px;
            -webkit-margin-end: 2px;
            -webkit-padding-before: 0.35em;
            -webkit-padding-start: 0.75em;
            -webkit-padding-end: 0.75em;
            -webkit-padding-after: 0.625em;
        }

        fieldset legend {
            color: #4F8AFF;
            font-weight: bolder;
        }

        .showStatus {
            color: Gray;
            font-size: large;
        }

        table {
            border-collapse: separate;
        }

        input.ui-datepicker {
            width: 80px;
        }

        #dealCategoryTable,.categoryArea {
            width: 100%;
        }

.subDealCategoryArea {padding-left: 23px;}

.ui-widget-daredevel-checkboxTree li span {top: 8px;}

        .LeftAnchor {
            border-bottom: 1px solid #CDCDCD;
            color: #666666;
            text-shadow: 2px 2px 2px #cccccc;
        }
        #navi2 {
            display: none;
        }
    </style>
    <script type="text/javascript">
        window.setInterval("DoPostBackMethod();SetDealsInfo();", 900000);

        // 防止session timeout
        function DoPostBackMethod(){
            $.ajax({
                type: "POST",
                url: "BusinessOrderDetail.aspx/DoPostBack",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                }
            });
        }

        function CloneDeal() {
            var liCnt = $("#ulDeal").find('#liDeal').size();
            var newLiDeal = $("#liCloneDeal").clone();
            newLiDeal.attr('id','liDeal');
            newLiDeal.css('display','');
            newLiDeal.find('#rdoSingleUser').attr('name', 'rdoUsersAddNew' + liCnt);
            newLiDeal.find('#rdoMultiUsers').attr('name', 'rdoUsersAddNew' + liCnt);
            newLiDeal.find('#chkIsNotDeliveryIslands').attr("checked","checked");
            $("#ulDeal").append(newLiDeal);
            BindBookingSystem();
        }
        function BindShipTypeEvent(){
            //
        }
        $(document).ready(function () {

            SetTabVisible(0);
            SetDealType();
            SetDealType2();
            SetDealType2Value();
            SetDealVisible();
            SetIsEDCVisible();
            SetIsKownEDCVisible();
            SetIsKownTaiShinEDCVisible();
            SetProductRefundContractDisplay();
            SetApplyNewPromotionVisible();
            SetBusinessTypeVisible('<%= BusinessOrderObjectId == MongoDB.Bson.ObjectId.Empty %>' == 'True');
            SetFreightCompanyVisible();
            BindProvisionData();
            SetPrivateContractRequired();
            SetDealsInfo();
            CheckContractReasonVisible();
            InvoiceOtherCheck();
            AccBusinessGroupChange($('[id*=rdlAccBusinessGroup]'));
            ShipTypeChange($('[id*=rdoShipType]'));
            BindBookingSystem();
            BindBankInfo(false);
            if ($('#txtInvoiceFounderCompanyName').val() == '') {
                $('input:radio[id*=rdlInvoiceFounder]').filter('[value="<%= (int)InvoiceFounderType.Company %>"]').attr('checked', true);
                ChangeInvoiceFounder();
            }
            SetInvoiceFounderVisible();
            
            // 標籤設定顯示無
            var tagLength = $('#ulTagSetting').find('li').filter(function() { return $(this).css('display') != 'none';}).length;
            
            if (tagLength == 0) {
                $('#TagSettingEmpty').css('display','');
            }else {
                $('#TagSettingEmpty').css('display','none');
            }

            // 隱藏在地宅配館別
            $('input[id*=rdlAccBusinessGroup]').each(function () {
                if ($(this).val() == '2') {
                    $(this).hide();
                    $(this).next('label').hide();
                }
            });

            $('#FillData').bind('click', function () {
                if (confirm('確定要填入預設值?')) {
                    FillData();
                }
            });            

            if ($('[id*=lblBusinessCopyType]').text() == '') {
                $('#BusinessCopyType').css('display','none');
            }

            if ($('[id*=lblBusinessHourTimeS]').text() == '') {
                $('#BusinessHourTimeS').css('display','none');
            }

            var disabled = $('[id*=txtSellerName]').attr('disabled');
            $('li [id*=liDeal]').each(function() {
                $(this).find('input,textarea,select').each(function() {
                    if (disabled == 'disabled') {
                        $(this).attr('disabled', 'disabled');
                    }
                });
            });

            function FillData () {
                $('li [id*=liDeal]').each(function () {
                    $(this).find('input[type=text]').not('#txtRestrictionRuleDesc').not('#txtAtmQuantity').each(function() {
                        if ($.trim($(this).val()) == '') {
                            $(this).val('0');
                        }
                    });
                });

                $('input[id*=chklTravelCategory]').eq(1).attr('checked','checked');
            }            

            $('#<%= chkIsEntrustSell.ClientID %>').bind('click', function () {
                if ($(this).attr('checked') == 'checked') {
                    $('#<%= rbHwatai.ClientID %>').attr('checked', 'checked');
                } else {
                    $('#<%= rbHwatai.ClientID %>').removeAttr('checked');
                    //                    $('#<%= rbIsMohist.ClientID %>').removeAttr('checked');
                }
            });

            //            $('#<%= rbIsMohist.ClientID %>').click(function() {
            //                if ($(this).is(':checked')) {
            //                    $('#<%= chkIsEntrustSell.ClientID %>').attr('checked', true);
            //                }
            //            });

            $('input[id$=rbHwatai]').click(function() {
                if ($(this).is(':checked')) {
                    $('#<%= chkIsEntrustSell.ClientID %>').attr('checked', true);
                }
            });

            var availableEmailTags = [<%=SalesEmailArray%>];
            $('#<%=txtReferredUser.ClientID %>').autocomplete({
                source: availableEmailTags
            });

            var availableLoveCodes = <%= SalesInfoArray %>;

            var __response = $.ui.autocomplete.prototype._response;
            $.ui.autocomplete.prototype._response = function(content) {
                __response.apply(this, [content]);
                this.element.trigger("autocompletesearchcomplete", [content]);
            };

            $("#<%=txtSalesName.ClientID %>").autocomplete({
                source: availableLoveCodes,
                minLength: 1,
                select: function(event, ui) {
                    $("#<%=txtSalesName.ClientID %>").val(ui.item.label);
                    $("#<%=txtSalesPhone.ClientID %>").val(ui.item.desc);
                    return false;
                }
            }).data("autocomplete")._renderMenu = function(ul, items) {
                var self = this;
                $.each(items, function(index, item) {
                    if (index < 10) // here we define how many results to show
                    {
                        self._renderItem = function(ul, item) {
                            var listItem = $("<li></li>").data("item.autocomplete", item)
                                       .append("<a><span class='code'>" + item.label + "</span></a>")
                                       .appendTo(ul);
                            return listItem;
                        };
                        self._renderItem(ul, item); 
                    }
                });
            };

            $('#selCity').change(function () { updateCity($('#selCity').val()); });
            $('#selCity').attr('disabled', $('[id*=txtSellerAddress]').attr('disabled'));
            $('#selTownship').attr('disabled', $('[id*=txtSellerAddress]').attr('disabled'));

            $('input[id*=chkPreferentialMonth]').bind('click', function () {
                SetPreferentialMonthsCheck($(this));
                var filterLength = $('input[id*=chkPreferentialMonth]').filter(function () { return $(this).attr('checked') == 'checked'; }).length;
                if (filterLength > 3) {
                    alert('最多勾選三期!!');
                    $(this).attr('checked',false);
                    SetPreferentialMonthsCheck($(this));
                }
            });

            $('[id*=lblListCategory]').bind('click', function () {
                var value = $(this).prev('input[type=hidden]').val();
                $('[id*=hidCategoryId]').each(function () {
                    if ($(this).val() == value) {
                        $('.ulContent').hide();
                        $(this).next().slideDown();
                    } else {
                        $(this).next().slideUp();
                    }
                });
            });

            $('#<%=chklNPponCity.ClientID%>_0').bind('click', function () {
                if ($('#<%=chklNPponCity.ClientID%>_0').is(':checked')) {
                    $('#<%=chklPponCity.ClientID%>_10').attr('checked','checked');
                } else {
                    $('#<%=chklPponCity.ClientID%>_10').removeAttr('checked');
                }        
            });

            $('#<%=chklNPponCity.ClientID%>_1').bind('click', function () {
                if ($('#<%=chklNPponCity.ClientID%>_1').is(':checked')) {
                    $('#<%=chklPponCity.ClientID%>_11').attr('checked','checked');
                } else {
                    $('#<%=chklPponCity.ClientID%>_11').removeAttr('checked');
                }                
            });

            $('#<%=txtNOtherPponCity.ClientID%>').bind('change', function () {
                $('#<%=txtOtherPponCity.ClientID%>').val($('#<%=txtNOtherPponCity.ClientID%>').val());
            });

            $('.showStatus').each(function () {
                if ($(this).attr('value') == $('[id*=hidStatus]').val()) {
                    $(this).css('color', 'red');
                    $(this).css('font-weight', 'bold');
                }
            });

            $('.liItem').bind('click', function () {
                $(this).find('.ulCategory').slideDown();
            });

            $('.liCategory').bind('click', function () {
                $(this).find('.ulContent').slideDown();
            });

            $('span[id*=lblContent]').bind('click', function () {
                var contentId = $(this).parent('.liContent').attr('id');
                var desc = $(this).parent('.liContent').find('input[id*=hidDescription]').val();
                var descId = $(this).parent('.liContent').find('input[id*=hidDescId]').val();
                SetProvisionListShow(contentId, descId, desc);
            });

            $('.liTitle').bind('click', function () {
                $('.liTitle').removeClass('active');
                SetTabVisible($(this).val());
                SetContent($(this).val());
                GoToTheTop();
            });

            $('#LegendHideProvision').bind('click', function () {
                if ($('.HideProvision').css('display') == 'none') {
                    $('.HideProvision').slideDown();
                } else {
                    $('.HideProvision').slideUp();
                }
            });

            $('#ToolBar').bind('click', function () {
                if ($('.ToolBarFool').css('display') == 'none') {
                    $('.ToolBarFool').slideDown();
                } else {
                    $('.ToolBarFool').slideUp();
                }
            });

            if ($('input[id*=txtSalesName]').attr('disabled') == 'disabled') {
                $('input[id*=btnDealClose]').attr('disabled', 'disabled');
                $('input[id*=btnAddDeal]').attr('disabled', 'disabled');
            }

            $('input[id*=chkSyncContactPerson]').bind('click', function() {
                if ($(this).attr('checked') == 'checked') {
                    $('input[id*=txtReturnedEmail]').val($('input[id*=txtBossEmail]').val());
                } else {
                    $('input[id*=txtReturnedEmail]').val('');
                }
            });

            $('input[id*=chkSyncUser]').bind('click', function() {
                if ($(this).attr('checked') == 'checked') {
                    $('input[id*=txtAccountingName]').val($('input[id*=txtBossName]').val());
                    $('input[id*=txtAccountingTel]').val($('input[id*=txtBossPhone]').val());
                    $('input[id*=txtAccountingEmail]').val($('input[id*=txtBossEmail]').val());
                } else {
                    $('input[id*=txtAccountingName]').val('');
                    $('input[id*=txtAccountingTel]').val('');
                    $('input[id*=txtAccountingEmail]').val('');
                }
            });

            $('input[id*=rdlPrivateContract]').bind('click', function() {
                SetPrivateContractRequired();
                ChangeInvoiceFounder();
            });

            $('input[id*=rdlBusinessType]').bind('click', function() {
                SetBusinessTypeVisible(true);
            });

            $('input[id*=chkIsTravelType]').bind('click', function() {
                SetBusinessTypeVisible(true);
            });

            $('[id*=rdlAccBusinessGroup]').bind('click', function() {
                SetBusinessTypeVisible(true);
            });

            $('input[id*=rdlIsEDC]').bind('click', function() {
                SetIsEDCVisible();
                SetIsKownEDCVisible();
            });

            $('input[id*=rdlIsApplyNewPromotion]').bind('click', function() {
                SetApplyNewPromotionVisible();
            });

            $('input[id*=chkIsKownEDC]').bind('click', function() {
                SetIsKownEDCVisible();
                SetIsKownTaiShinEDCVisible();
            });

            $('input[id*=chkProductRefund]').bind('click', function() {
                SetProductRefundContractDisplay();
            });

            $('input[id*=chkIsTaiShinEDC]').bind('click', function() {
                SetIsKownTaiShinEDCVisible();
            });

            $('input[id*=rdlFreightType]').bind('click', function() {
                SetFreightCompanyVisible();
            });

            $('[id*=spanRemittanceType]').find('[type=radio]').each(function () {
                $(this).click(function(){
                    var cnfrm = confirm('確定將出帳方式變更為 [' + $(this).next('label').text() + '] ??');
                    if(cnfrm != true)
                    {
                        return false;
                    }
                });    
            });

            $('input[id*=rdlAuthorizationGroup]').click(function(){
                var cnfrm = confirm('確定將審核區域轉至 [' + $(this).next('label').text() + '] ??');
                if(cnfrm != true)
                {
                    return false;
                }
            });

            $('.IsIncludeFreight').each(function () {
                SetFreightDisabled($(this));
            });

            $('.AtmCheck').each(function () {
                CheckATM($(this));
            });
            
            $('[id*=chkIsRestrictionRule]').each(function() {
                SetRestrictionRuleVisible($(this));
            });

            $('[id*=rdlCommission]:checked').each(function() {
                CommissionCheck($(this));
            });

            //// 旅遊頻道須設定旅遊區域
            //$('input[id*=chklPponCity]').each(function () {
            //    checkTravelCategory(this);
            //    $(this).bind('click', function () { checkTravelCategory(this); });
            //});
            <%--處理預設選取的資料 --%>
            ShowAllCategory();

            $('.divCommercial').each(function() {
                var selected = $(this).find('[id*=chklCommercialCategory]').filter(function() { return $(this).is(':checked') }).length;
                if (selected == 0) {
                    $(this).find('[id*=chkCommercialArea]').attr('checked', false);
                    $(this).find('[id=CommercialCategoryList]').css('display', 'none');
                } else {
                    $(this).find('[id*=chkCommercialArea]').attr('checked', 'checked');
                    $(this).find('[id=CommercialCategoryList]').css('display', '');
                }
            });
            $('[id*=txtBusinessHourTimeS]').datepicker();
            setupdatepicker('<%=txtExpiringItemsDateStart.ClientID%>', '<%=txtExpiringItemsDateEnd.ClientID%>');
            setupdatepicker('<%=txtCreateTimeS.ClientID%>', '<%=txtCreateTimeE.ClientID%>');
            $('#txtAdvanceReservationDays').numeric();
            $('#txtCouponUsers').numeric();

            <%--MoveDealCategoryItem --%>
            MoveDealCategoryItem();
            isSlottingFeeDeal();
        });

        var citys = <%= CityJSonData %>;
        function updateCity(cid) {
            $.each(citys, function(i,item) {
                if(item.id == cid) {
                    var el = $('#selTownship');
                    el.children().remove();
                    el.addItems(eval(item.Townships), function (t) { return t.name; }, function (t) { return t.id; });
                    el.get(0).selectedIndex = 0;
                    return false;
                }
            });
        }

        function CloseWindow() {
            window.close(); 
        }

        function SetDefaultData(obj) {
            if ($(obj).val() == '2') {
                $(obj).prev('#txtStartMonth').val('yyyy/MM/dd');
            }else {
                $(obj).prev('#txtStartMonth').val('');
            }
        }

        function dateSelectionChanged(sender, args) {
            selectedDate = sender.get_selectedDate();
            var value = new Date(selectedDate);
            var newValue = new Date((value.getFullYear() + 1) + '/' + (value.getMonth() + 1)  + '/' + (value.getDate()));
            $('[id*=txtCreateTimeE]').val(newValue.format('yyyy/MM/dd'));
        }

        function SetPrivateContractRequired() {
            if ($('input[id*=rdlPrivateContract]:checked').val() == 'True') {
                $('#PrivateContract').css('display','');
                $('#CompanyContract').css('display','none');
            }else {
                $('#PrivateContract').css('display','none');
                $('#CompanyContract').css('display','');
            }
        }

        function BindProvisionData() {
            var provisionResult = $('[id*=hidProvisionResult]').val();
            if (provisionResult != '') {
                var provision = $.parseJSON(provisionResult);
                $.each(provision, function(i, item) {
                    ProvisionListShow(item.contentId, item.descId, item.desc);
                });
            }
        }

        function ShowExample(obj) {
            var example = $(obj).next('span');
            if (example.css('display') == 'none') {
                example.css('display','');
            }else {
                example.css('display','none');
            }
        }

        function SetHidProvisionResult() {
            var dataArray = [];
            $('[id*=ulProvisionResult]').find('li').each(function(){
                var item = {
                    "contentId": $(this).attr('contentid'),
                    "descId": $(this).attr('descid'),
                    "desc": $(this).find('a').text()
                };
                dataArray.push(item);
            });
            
            $('[id*=hidProvisionResult]').val(JSON.stringify(dataArray));
        }

        function SetProvisionListShow(contentId, descId, desc) {
            if ($('li[contentid=' + contentId + ']').length == 0) {
                ProvisionListShow(contentId, descId, desc);
            } else {
                alert('好康條款已加入!!');
            }
        }

        function ProvisionListShow(contentId, descId, desc) {
            if ($('input[id*=' + contentId + ']').length == 0) {
                $('#' + contentId).css('color', 'blue');
                $('#' + contentId).css('font-weight', 'bold');
                var $categoryId = $('#' + contentId).parent('.ulContent').parent('.liCategory');
                $('#' + $categoryId.attr('id')).find('span[id*=lblItem]').css('color', 'blue');
                $('#' + $categoryId.attr('id')).find('span[id*=lblItem]').css('font-weight', 'bold');
                var $item = $categoryId.parent('.ulCategory').parent('.liItem');
                $('input[id*=hidListCategory]').each(function () {
                    if ($(this).val() == $item.attr('id')) {
                        $(this).next().css('color', 'blue');
                    }
                });


                $('[id*=ulProvisionResult]').append('<li contentId="' + contentId + '" descId="' + descId + '"><a id="lkTextEdit_' + contentId + '" style="cursor: pointer">' + desc + '</a></li>');

                if ($('[id*=txtSellerName]').attr('disabled') == 'disabled') {
                    $('#divProvisionEdit').css('display', 'none');
                    $('#ProvisionSample').css('display', 'none');
                } else {
                    $('[id*=ulProvisionResult]').find('li[contentId=' + contentId + ']').append('<input type="button" value="刪除" id="btnTextDelete_' + contentId + '" />');
                    $('[id*=btnTextDelete_' + contentId + ']').bind('click', function () {
                        SetContentTextDelete($(this), contentId, $('#' + $categoryId.attr('id')).find('span[id*=lblItem]'), $item);
                        SetHidProvisionResult();
                    });

                    $('[id*=lkTextEdit_' + contentId + ']').bind('click', function () {
                        ProvisionEditClear();
                        $('#divProvisionEdit').show();
                        $('#resultText').append('<table><tr><td><textarea class="Paragraph" id="' + contentId + '" style="width: 570px; height: 60px"></textarea><input type="hidden" id="' + descId + '" /></td><td><input class="TextDelete" type="button" value="儲存" id="btnTextSave" contentId="' + contentId + '" onclick="TextSave(this);" /></td></tr></table>');
                        $('#divProvisionEdit').find('#'+ contentId ).val($(this).parent().find('a').text());
                    });
                }
                SetHidProvisionResult();
            }
        }

        function TextSave(obj) {
            $('[id*=ulProvisionResult]').find('li[contentid=' + $(obj).attr('contentId') + ']').find('a').text($(obj).parent().parent().find('.Paragraph').val());
            ProvisionEditClear();
            SetHidProvisionResult();
        }

        function ProvisionEditClear() {
            $('.Paragraph').each(function() {
                $(this).parent().parent().remove();
            });
            $('#divProvisionEdit').hide();
        }

        function SetContentTextDelete(obj, value, category, item) {
            $(obj).parent().remove();
            $('li[id*=' + value + ']').css('color', 'black');
            $('li[id*=' + value + ']').css('font-weight', 'normal');
            category.next().find('')
            category.css('color', 'black');
            category.css('font-weight', 'normal');

            $('input[id*=hidListCategory]').each(function () {
                if ($(this).val() == item.attr('id')) {
                    $(this).next().css('color', 'black');
                }
            });
        }

        function SetBusinessTypeVisible(changeRemittance) {
            // 好康 or 商品

            // 出帳方式
            var rbAchWeekly = $('[id*=rbAchWeekly]'); // ACH每週出帳
            var rbManualWeekly = $('[id*=rbManualWeekly]'); // 人工每週出帳
            var rbManualMonthly = $('[id*=rbManualMonthly]'); // 人工每月出帳
            var rbPartially = $('[id*=rbPartially]'); // 商品(暫付70%)
            var rbOthers = $('[id*=rbOthers]'); // 其他付款方式
            var divAncestorSequenceBid = $('[id*=divAncestorSequenceBid]'); // 接續憑證BID
            var rbFlexible = $('[id*=rbFlexible]'); // 彈性選擇出帳
            var rbMonthly = $('[id*=rbMonthly]'); // 彈性選擇出帳
            var rbWeekly = $('[id*=rbWeekly]'); // 彈性選擇出帳
            
            rbAchWeekly.removeAttr('disabled');
            rbManualWeekly.removeAttr('disabled');
            rbManualMonthly.removeAttr('disabled');
            rbPartially.removeAttr('disabled');
            rbOthers.removeAttr('disabled');
            rbFlexible.removeAttr('disabled');
            rbMonthly.removeAttr('disabled');
            rbWeekly.removeAttr('disabled');

            if ($('input[id*=rdlBusinessType]:checked').val() == 'Ppon') {
                //好康
                $('.Ppon').show();
                $('.DealType').hide();
                $('.Product').hide();
                $('.Product').find('input[type=text]').val('');
                //$('.Product').find(':checkbox').attr('checked', false);
                $('.Product').find(':checkbox').not("#chkIsNotDeliveryIslands").attr('checked', false);
                divAncestorSequenceBid.show();
                // 出帳方式-好康(商品(暫付70%)disabled)
                rbPartially.attr('disabled','disabled');
                if (changeRemittance) {
                    if ($('[id*=rdlAccBusinessGroup]:checked').val() == '6') {
                        // 出帳方式-品生活好康(預設 ACH每週出帳)
                        rbAchWeekly.attr('checked','checked');
                    } else {
                        // 出帳方式-好康(預設 ACH每週出帳)
                        rbAchWeekly.attr('checked','checked');
                    }
                }
            } else if ($('input[id*=rdlBusinessType]:checked').val() == 'Product') {
                //憑證
                $('.Ppon').hide();
                $('.DealType').show();
                $('.Product').show();
                $('.Ppon').find('input[type=text]').val('');
                $('.Ppon').find(':checkbox').attr('checked', false);
                divAncestorSequenceBid.hide();  //宅配不會有憑證
                // 出帳方式-商品(ACH每週出帳,人工每週出帳,人工每月出帳disabled)
                rbAchWeekly.attr('disabled','disabled');
                rbManualWeekly.attr('disabled','disabled');
                rbManualMonthly.attr('disabled','disabled');
                if (changeRemittance) {
                    if ($('[id*=rdlAccBusinessGroup]:checked').val() == '6' || $('input[id*=chkIsTravelType]:checked').val() == 'on') {
                        // 出帳方式-品生活商品or旅遊商品(預設 其他付款方式)
                        rbOthers.attr('checked','checked');
                    }else {
                        // 出帳方式-商品(預設 商品(暫付70%))
                        rbPartially.attr('checked','checked');
                    }
                }
                if ($('[id*=txtUseTimeStart]').val() == '') {
                    $('[id*=txtUseTimeStart]').val('<%= productUseStartAddDays %>');
            }
            if ($('[id*=txtUseTimeEnd]').val() == '') {
                $('[id*=txtUseTimeEnd]').val('<%= productUseEndAddDays %>');
            }

            $("input[name='ShippingDateType'][value='<%= RbShippingDateType %>']").attr('checked',true);

        }
            // 是否為旅遊類
    if ($('input[id*=chkIsTravelType]:checked').val() == 'on') {
        $('.Travel').show();
        $('.NoTravel').hide();
        if ($('input[id*=rdlBusinessType]:checked').val() == 'Ppon') {
            if (changeRemittance) {
                // 出帳方式-好康旅遊(預設 人工每月出帳)
                rbManualMonthly.attr('checked','checked');
            }
        }
    }else {
        $('.Travel').hide();
        $('.Travel').find('input[type=text]').val('');
        $('.Travel').find(':checkbox').attr('checked', false);
    }
            // 其他特殊設定顯示無
    var specialLength = $('#ulSpecialSetting').find('li').filter(function() { return $(this).css('display') != 'none';}).length;
    if (specialLength == 0) {
        $('#SpecialSettingEmpty').css('display','');
    }else {
        $('#SpecialSettingEmpty').css('display','none');
    }
}

function SetIsEDCVisible() {
    if ($('input[id*=rdlIsEDC]:checked').val() == '0') {
        $('#divIsKnowEDC').show();
    } else {
        $('#divIsKnowEDC').hide();
        $('input[id*=chkIsKownEDC]').attr('checked', false);
    }
}

function SetIsKownEDCVisible() {
    if ($('input[id*=chkIsKownEDC]').attr('checked') == 'checked') {
        $('#liIsEDC').show();
    } else {
        $('#liIsEDC').hide();
        $('input[id*=chkIsTaiShinEDC]').attr('checked', false);
    }
}

function SetApplyNewPromotionVisible() {
    if ($('input[id*=rdlIsApplyNewPromotion]:checked').val() == 'True') {
        $('#divApplyNewPromotion').show();
    } else {
        $('#divApplyNewPromotion').hide();
        $('input[id*=rdlApplyNewPromotion]').each(function(){
            if ($(this).val() == 'False') {
                $(this).attr('checked',true);
            }else {
                $(this).attr('checked',false);
            }
        });
    }
}

function SetIsKownTaiShinEDCVisible() {
    if ($('input[id*=chkIsTaiShinEDC]').attr('checked') == 'checked') {
        $('#liIsTaiShinEDC').show();
    } else {
        $('#liIsTaiShinEDC').hide();
        $('input[id*=txtEdcDesc]').val('');
    }
}

function SetProductRefundContractDisplay() {
    if($('input[id*=chkProductRefund]').is(':checked')) {
        $('[id*=pProductRefund]').show();
    }else {
        $('[id*=pProductRefund]').hide();
    }
}

function SetFreightCompanyVisible() {
    if ($('input[id*=rdlFreightType]:checked').val() == '1') {
        $('#FreightCompany').show();
    } else {
        $('#FreightCompany').hide();
        $('input[id*=chklFreightCompany]').attr('checked', false);
    }
}

function SetDealVisible() {
    $('[id*=liDeal]').each(function () {
        var purchasePrice = $(this).find('[id=txtPurchasePrice]').val()
        var minQuentity = $(this).find('[id=txtMinQuentity]').val()
        var maxQuentity = $(this).find('[id=txtMaxQuentity]').val()
        if ((purchasePrice != '' && purchasePrice != '0') || (minQuentity != '' && minQuentity != '0') || (maxQuentity != '' && maxQuentity != '0'))
            $(this).css('display', '');
        else{
            $(this).css('display', 'none');
        }
    });
}

function SetDealType() {
    $('ul .DealType').find('input[type=radio]').bind('click', function () {
        $('ul .DealType').find('input[type=radio]').attr('checked', false);
        $(this).attr('checked', true);
    });
}

function SetPreferentialMonthsCheck(obj) {
    obj.next().next().find('[id*=chklPreferentialItems]').each(function () {
        if ($(this).parent().css('display') != 'none') {
            if (obj.attr('checked') == 'checked') {
                $(this).attr('checked', 'checked');
            } else {
                $(this).attr('checked', false);
            }
        }
    });
}

function SetTabVisible(value) {

    $('.liTitle').each(function () {
        if ($(this).val() == value) {
            $(this).addClass('active');
        }
    });
}

function SetContent(value) {
    $('[id*=ShowDiv]').css('display', 'none');
    $('#ShowDiv' + value).css('display', '');
}

function CheckSalesDept() {
    if ($('input[id*=rdlBusinessType]:checked').length == 0) {
        alert('請正確選擇您的工單類型!!');
        return false;
    }
}

function DataCheck() {

    // 特店折扣
    if ($('[id*=txtPezStores]').val() != '' || $('[id*=txtCreateTimeS]').val() != '') {
        if ($('[id*=txtPezStores]').val() == '') {
            alert('請輸入特店折扣優惠!!!');
            return false;
        } else if ($('[id*=txtCreateTimeS]').val() == '') {
            alert('請選擇特店折扣優惠起迄日期!!!');
            return false;
        }
    }

    // 若狀態為已排檔，需輸入bid才可更新為已建檔
    if ($('[id*=ddlStatus]').val() > '4' && $.trim($('[id*=txtBid]').val()) == '') {
        alert('請輸入對應檔次id');
        $('[id*=txtBid]').focus();
        return false;
    }

    // 需輸入上檔日期才可更新為已排檔
    if ($('[id*=ddlStatus]').val() > '3' && $.trim($('[id*=lblBusinessHourTimeS]').text() + $.trim($('[id*=txtBusinessHourTimeS]').val())) == '') {
        alert('請填入上檔日期!!');
        $('.ToolBarFool').css('display','');
        $('[id*=txtBusinessHourTimeS]').focus();
        return false;
    }

    var isValid = true;
    var type = $('input[id*=rdlBusinessType]:checked');

    //檢查區域有無選取
    var areaSelected = true;

    var errorChannel = '';
    $.each(categoryKeyArray, function(j, obj) {
        var channelBox = $('#channelCategory' + obj);
        var channelLabel = $('#channelCategoryLabel' + obj);
        var kids = $("#subChannelCol" + obj + " input[type='checkbox']");
        if (channelBox.is(':checked')) {
            if (kids.length > 0) {
                                            <%--有選取--%>
                var checkArea = false;
                                            <%--判斷子項目是否有選取--%>
                $.each(kids, function(h, subArea) {
                    if (subArea.checked) {
                        checkArea = true;
                    }
                });

                if (checkArea == false) {
                    errorChannel = channelLabel.text().trim();
                    areaSelected = false;
                    return;
                }
            }
        }
    }
        );

    if (!areaSelected) {
        alert('請勾選' + errorChannel + '區域');
        return false;
    }

    // 生活商圈檢查
    $('.divCommercial').each(function() {
        if ($(this).find('input[id*=chkCommercialArea]').is(':checked')) {
            var selected = $(this).find('[id*=chklCommercialCategory]:checkbox').filter(function(){ return $(this).is(':checked')}).length;
            if (selected == 0) {
                alert('您勾選了生活商圈 [' + $(this).find('input[id*=chkCommercialArea]').next().text() + ']，卻沒有勾選分館耶!?');
                isValid = false;
                return false;
            }
        }
    });

    //檢查業務
    var availableLoveCodes = <%= SalesInfoArray %>;
    var salesName = $('[id*=txtSalesName]').val();
    var IsValid = false;
    $.each(availableLoveCodes , function(i, val) {
        if (val.value == salesName)
        {
            IsValid = true;
            return;
        }
    });
 
    if (!IsValid)
    {
        alert('請填入有效業務!!');
        return false;
    }

    // 照片來源
    var pictureLength = $('input[id*=chklPictureSource]').filter(function () { return $(this).attr('checked') == 'checked'; }).length;
    if ($('[id*=ddlStatus]').val() > 0 && pictureLength == 0) {
        if ($('input[id*=chklPictureSource]').attr('disabled') != 'disabled') {
            alert('請至少勾選一照片來源!!');
            return false;
        }
    }

    if ($('[id*=ddlStatus]').val() > 0 && $('[id*=rdlSellerLevel]:checked').val() == undefined) {
        alert('請選擇賣家規模!!');
        return false;
    }

    if ($('[id*=ddlStatus]').val() > 0 && $('[id*=ddlStatus]').val() < '2') {
        // 退換貨連絡人必填
        if (type.val() == 'Product') {
            if ($('[id*=txtReturnedEmail]').val() == '') {
                alert('請輸入 Part1 店家資料 - 退換貨聯絡電子信箱。');
                return false;
            }
        }    
    }

    //銷售分析
    if ($('[id*=ddlStatus]').val() > 0 && $('input[id*=rdlBusinessType]:checked').val() == 'Product') {
        var ddlSub = document.getElementById('<%=ddlDealType3.ClientID %>');
        if (ddlSub.selectedIndex == null || ddlSub.selectedIndex == "0")
        {
            alert("請選擇銷售類別分析");
            return false;
        }
    }

    //關鍵字
    var words = $('#txtKeywords').val();
    if($.trim(words)==""){
        if($('#txtKeywords').prop("disabled")==false){
            alert('請填寫搜尋關鍵字');
            return false;
        }
    }
    var newWords = '';
    if (words.split('/').length > 3) {
        alert('僅限輸入至多3個字詞');
        return false;
    }
    

    var items = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R');
    for (var i = 0; i < $('[id*=liDeal]').length; i++) {
        var slottingFeeDeal = $('[id*=liDeal]').eq(i).find('[id=ckSlottingFeeDeal]').val();
        var slottingFeeQuantity = $('[id*=liDeal]').eq(i).find('[id=txtSlottingFeeQuantity]').val();
        var BuyoutCoupon = $('[id*=liDeal]').eq(i).find('[id=ckBuyoutCoupon]').val();
        var purchasePrice = $('[id*=liDeal]').eq(i).find('[id=txtPurchasePrice]').val();
        var commissionHas = $('[id*=liDeal]').eq(i).find('[id=rdlCommissionHas]');
        var commission = $('[id*=liDeal]').eq(i).find('[id=txtCommission]').val();
        var itemPrice = $('[id*=liDeal]').eq(i).find('[id=txtItemPrice]').val();
        var orignPrice = $('[id*=liDeal]').eq(i).find('[id=txtOrignPrice]').val();
        var minQuentity = $('[id*=liDeal]').eq(i).find('[id=txtMinQuentity]').val();
        var maxQuentity = $('[id*=liDeal]').eq(i).find('[id=txtMaxQuentity]').val();
        var itemName = $('[id*=liDeal]').eq(i).find('[id=txtItemName]').val();
        var limitPerQuentity = $('[id*=liDeal]').eq(i).find('[id=txtLimitPerQuentity]').val();
        var startMonth = $('[id*=liDeal]').eq(i).find('[id=txtStartMonth]').val();
        var productFreight = $('[id*=liDeal]').eq(i).find('[id=txtProductFreight]').val();
        var productNonFreightLimit = $('[id*=liDeal]').eq(i).find('[id=txtProductNonFreightLimit]').val();
        var isUsingbookingSystem = $('[id*=liDeal').eq(i).find('[id=cb_IsUsingbookingSystem]').is(':checked');
        var advanceReservationDays = $('[id*=liDeal').eq(i).find('[id=txtAdvanceReservationDays]').val();
        var multiUsers = $('[id*=liDeal').eq(i).find('[id=rdoMultiUsers]').is(':checked');
        var CouponUsers= $('[id*=liDeal').eq(i).find('[id=txtCouponUsers]').val();

        if ((purchasePrice != '' && purchasePrice != '0') || (minQuentity != '' && minQuentity != '0') || (maxQuentity != '' && maxQuentity != '0')) {
            if (itemPrice == '') {
                alert('請填入' + items[i] + '.賣價');
                return false;
            }
            if (orignPrice == '') {
                alert('請填入' + items[i] + '.原價');
                return false;
            }
            if (maxQuentity == '') {
                alert('請填入' + items[i] + '.最高購買數量');
                return false;
            }
            if (minQuentity == '') {
                alert('請填入' + items[i] + '.最低購買數量');
                return false;
            }
            if (itemName == '') {
                alert('請填入' + items[i] + '.優惠活動');
                return false;
            }
            if (limitPerQuentity == '') {
                alert('請填入' + items[i] + '.每人限購數量');
                return false;
            }
            // 旅遊類限制
            if ($('[id*=chkIsTravelType]').is(':checked')) {
                if (commissionHas.is(':checked') && (commission == '' || commission == '0')) {
                    alert('請填入' + items[i] + '.發票對開佣金');
                    return false;
                }
            }
            // 好康&宅配類限制
            if (type.val() == 'Ppon') {
                if (startMonth == '') {
                    alert('請填入' + items[i] + '.活動有效使用期間');
                    return false;
                }
            }else {
                if (productFreight == '') {
                    alert('請填入' + items[i] + '.運費');
                    return false;
                }
                if (productNonFreightLimit == '') {
                    alert('請填入' + items[i] + '.免運門檻');
                    return false;
                }
            }
        }else if ((minQuentity != '' && minQuentity != '0') || (maxQuentity != '' && maxQuentity != '0')) {
            $('[id*=liDeal]').eq(i).css('display','');
            var isConfirm = confirm('確定' + items[i] + '進貨價為 0 ?');
            if (!isConfirm) {
                return false;
            }
        }
        if(isUsingbookingSystem) {
            if(!advanceReservationDays || advanceReservationDays < 1) {
                alert('請填入' + items[i] + '.消費者提早預約天數');
                return false;
            }
            if(multiUsers) {
                if(!CouponUsers || CouponUsers < 1) {
                    alert('請填入' + items[i] + '.憑證享用人數');
                    return false;
                }
            }
        }
    }

    SetDealsInfo();
    if ($('[id*=ddlStatus]').val() > '0') {
        if ($('[id*=chkIsContractSend]').attr('checked') != 'checked') {
            if ($('[id*=txtNoContractReason]').val() == '') {
                alert('請填寫合約未回原因!!');
                isValid = false;
            } else {
                isValid = confirm('確認合約未回?');
            }
        }
    }
    if (isValid) {
        // confirm business type
        if ($('span[id*=lblCreateId]').text() == '') {
            isValid = confirm('此工單類型是否確認為［' + type.next('label').text() + '類］？\n一但設定後就回不去了喔！');
        }
    }
    return isValid;
}

function SetDealsInfo() {
    var hdJson = $('#<%=hdDeals.ClientID %>');
    var dealsDesc = [];
    $('[id*=liDeal]').each(function(e) {
        var purchasePrice = $(this).find('[id=txtPurchasePrice]').val();
        var commission = $(this).find('[id=txtCommission]').val();
        if (commission == '') {
            commission = '0';
        }
        var minQuentity = $(this).find('[id=txtMinQuentity]').val();
        var maxQuentity = $(this).find('[id=txtMaxQuentity]').val();
        if ((purchasePrice != '' && purchasePrice != '0') || (minQuentity != '' && minQuentity != '0') || (maxQuentity != '' && maxQuentity != '0')) {
            dealsDesc.push({
                "ItemPrice": $(this).find('[id=txtItemPrice]').val(),
                "OrignPrice": $(this).find('[id=txtOrignPrice]').val(),
                "ItemName": $(this).find('[id=txtItemName]').val(),
                "MultOption": $(this).find('[id=txtMultOption]').val().replace('，',',').replace('～','~').replace('；',';'),
                "IsReferenceAttachment": $(this).find('[id=chkIsReferenceAttachment]').attr('checked') == 'checked' ? true : false,
                "MinQuentity": $(this).find('[id=txtMinQuentity]').val(),
                "MaxQuentity": $(this).find('[id=txtMaxQuentity]').val(),
                "LimitPerQuentity": $(this).find('[id=txtLimitPerQuentity]').val(),
                "StartMonth": $(this).find('[id=txtStartMonth]').val(),
                "StartUseUnit": $(this).find('[id=ddlStartUseUnit]').val(),
                "IsRestrictionRule": $(this).find('[id=txtRestrictionRuleDesc]').val() != '' ? true : false,
                "RestrictionRuleDesc": $(this).find('[id=txtRestrictionRuleDesc]').val(),
                "Freight": $(this).find('[id=txtFreight]').val().replace('，',',').replace('～','~').replace('；',';'),
                "NonFreightLimit": $(this).find('[id=txtNonFreightLimit]').val(),
                "IsFreightToCollect": $(this).find('[id=chkIsFreightToCollect]').attr('checked') == 'checked' ? true : false,
                "IsNotDeliveryIslands": $(this).find('[id=chkIsNotDeliveryIslands]').attr('checked') == 'checked' ? true : false,
                "IsATM": $(this).find('[id=chkIsAtm]').attr('checked') == 'checked' ? true : false,
                "AtmQuantity": $(this).find('[id=txtAtmQuantity]').val(),
                "SlottingFeeQuantity": $(this).find('[id=txtSlottingFeeQuantity]').val(),
                "BuyoutCoupon": $(this).find('[id=ckBuyoutCoupon]').attr('checked') == 'checked' ? true : false,
                "PurchasePrice": $(this).find('[id=txtPurchasePrice]').val().replace('，',',').replace('～','~').replace('；',';'),
                "Commission": commission,
                "IsIncludeFreight": $(this).find('[id=chkIsIncludeFreight]').attr('checked') == 'checked' ? true : false,
                "BookingSystemType": ($(this).find('[id=cb_IsUsingbookingSystem]').is(":checked")) ? ($(this).find('[id=rdoBookingSystemSeat]').is(":checked") ? 1 : 2) : 0,
                "AdvanceReservationDays": $(this).find('[id=cb_IsUsingbookingSystem]').is(":checked") ? $(this).find('[id=txtAdvanceReservationDays]').val() : 0,
                "CouponUsers": $(this).find('[id=cb_IsUsingbookingSystem]').is(":checked") && $(this).find('[id=rdoSingleUser]').is(":checked") ? 1 : ($(this).find('[id=txtCouponUsers]').val()) ? $(this).find('[id=txtCouponUsers]').val() : 0,
                "IsReserveLock": $(this).find('[id=cb_IsReserveLock]').is(":checked") ? true : false
            });
        };
        if ($(this).find('[id=txtSlottingFeeQuantity]').val() == '0' && $(this).find('[id=txtPurchasePrice]').val() == '0')
        {
            $(this).find('[id=ckSlottingFeeDeal]').prop('checked',true);
        }
        SetBookingSystemDisplay(this);
    });
    hdJson.val(JSON.stringify(dealsDesc));
}

function DealClose(obj) {
    var parentDeal = $(obj).parents('#liDeal');
    parentDeal.css('display','none');
    parentDeal.find('input[type=text]').val('');
    parentDeal.find('textarea ').val('只要XXXX元 + (多人說明) + 即可享用【店名】+ 原價XXXX元 + 商品名稱');
    parentDeal.find(':checkbox').attr('checked', false);
}

function GoToTheTop() {
    $('html,body').animate({ scrollTop: 0 }, 0);
    return false;
}

function BindBankInfo(isChanged) {

    var obj = $('[id*=ddlAccountingBank]');
    var id = obj.val();
    if (id != '') {
        $('[id*=txtAccountingBankId]').val(id);
        $('[id*=txtAccountingBankDesc]').val($.trim($(obj).find(':selected').text().replace(id,'')));
    }

    $.ajax({
        type: "POST",
        url: "BusinessOrderDetail.aspx/BankInfoGetBranchList",
        data: "{'id': '" + $(obj).val() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $('select[id*=ddlAccountingBranch]').find('option').remove();
            $.each(response.d, function(index, item){
                $('select[id*=ddlAccountingBranch]').append($('<option></option>').attr('value', item.No).text(item.Name));
            });
            if (isChanged) {
                BindBranchInfo();
            }else {
                // set branch info
                var branchId = $.trim($('[id*=txtAccountingBranchId]').val());
                if (branchId != '') {
                    $('[id*=ddlAccountingBranch]').val(branchId);
                }
            }
        }
    });
            
}

function BindBranchInfo() {
    var obj = $('[id*=ddlAccountingBranch]');
    var id = obj.val();
    if (id != '' && id != null) {
        $('[id*=txtAccountingBranchId]').val(id);
        $('[id*=txtAccountingBranchDesc]').val($.trim($(obj).find(':selected').text().replace(id,'')));
    }
}

function CheckReferred() {
    var referredUser = $('[id*=txtReferredUser]');
    referredUser.val($.trim(referredUser.val()).toLowerCase());
    if (referredUser.val() == '') {
        alert('您沒有輸入轉件人員!!\n試試輸入第一個英文字母，將會有奇妙的事情發生。');
        referredUser.focus();
        return false;
    }
    else if (referredUser.val() == $('[id*=lblCreateId]').text()) {
        alert('你轉給自己做啥呢? 那不能。');
        return false;           
    }else {
        return confirm('確定將這張工單白白送給 ' + referredUser.val() + ' 嗎?');
    }
}

function CheckContractReasonVisible() {
    if ($('[id*=chkIsContractSend]').attr('checked') == 'checked') {
        $('#NoContractReason').css('display','none');
        $('[id*=txtNoContractReason]').val('');
        $('select[id*=ddlNoContractReason]').val('');
    }else {
        $('#NoContractReason').css('display','');
    }
}



function SetNoContractReason() {
    var obj = $('[id*=ddlNoContractReason]');
    if (obj.val() == '0' || obj.val() == '5') {
        $('[id*=txtNoContractReason]').val('');
    }else {
        $('[id*=txtNoContractReason]').val(obj.find(':selected').text());
    }
}

function SetFreightDisabled(obj) {
    if ($(obj).attr('checked') != 'checked' && $('[id*=txtSellerName]').attr('disabled') != 'disabled') {
        $(obj).parent().parent().parent().find('li.FreightDisabled').css('color','black');
        $(obj).parent().parent().parent().find('li.FreightDisabled').find('input').attr('disabled',false);
        //$(obj).parent().parent().parent().find('li.FreightDisabled').find('#freightExample').css('display','');
    }else {
        $(obj).parent().parent().parent().find('li.FreightDisabled').css('color','gray');
        $(obj).parent().parent().parent().find('li.FreightDisabled').find('input').not('#chkIsNotDeliveryIslands').attr('disabled','disabled');
        $(obj).parent().parent().parent().find('span#txtIsNotDeliveryIslands').css('color','black');
        //$(obj).parent().parent().parent().find('li.FreightDisabled').find('#freightExample').css('display','none');
    }
}

function SetRestrictionRuleVisible(obj) {
    if ($(obj).attr('checked') == 'checked') {
        $(obj).next('#txtRestrictionRuleDesc').css('display','');
    }else {
        $(obj).next('#txtRestrictionRuleDesc').css('display','none');
        $(obj).next('#txtRestrictionRuleDesc').val('');
    }
}

function InTaxCheck() {
    if ($('input[id*=rdlInTax]:checked').val() != undefined || $('[id*=rdlDuplicate]').is(':checked')) {
        if ($('input[id*=rdlInTax]:checked').val() == undefined) {
            $('#<%=rdlInTax.ClientID%> input[value=5]').attr('checked','checked'); // 預設應稅
    }
    $('[id*=rdlDuplicate]').attr('checked', true);
}
    InvoiceOtherCheck();
}

function InvoiceOtherCheck() {
    if ($('[id*=rdlInvoiceOther]').is(':checked')) {
        $('[id*=txtInvoiceOtherDesc]').css('display','');
    }else {
        $('[id*=txtInvoiceOtherDesc]').css('display','none');
        $('[id*=txtInvoiceOtherDesc]').val('');
    }
}

function InvoiceCheck() {
    InvoiceOtherCheck();
    $('input[id*=rdlInTax]').removeAttr('checked');
}

function CommissionCheck(obj) {
    if ($(obj).val() == '1') {
        $(obj).next('#CommissionText').css('display','');
        $(obj).prev('#rdlCommissionNo').removeAttr('checked');
        $(obj).parent().parent().find('#txtPurchasePrice').val($(obj).parent().parent().parent().parent().find('#txtItemPrice').val());
        $(obj).parent().parent().find('#txtPurchasePrice').attr('disabled','disabled');
    }else {
        $(obj).next().next('#CommissionText').css('display','none');
        $(obj).next().next('#CommissionText').find('#txtCommission').val('0');
        $(obj).next('#rdlCommissionHas').removeAttr('checked');
        if ($(obj).attr('disabled') != 'disabled') {
            $(obj).parent().parent().find('#txtPurchasePrice').removeAttr('disabled');
        }
    }
}

function ValidateNumber(e, pnumber) {
    if (!/^\d+$/.test(pnumber)) {
        $(e).val(/^\d+/.exec($(e).val()));
    }
    return false;
}
function ShipTypeChange(obj) {
    switch($(obj).find(':checked').val()) {
        case '-1':
            $('#ShipByArriveIn24Hrs').hide();
            $('#ShipByArriveIn72Hrs').hide();
            $('#ShipByNormal').show();
            break;
        case '0':
            $('#ShipByNormal').hide();
            $('#ShipByArriveIn72Hrs').hide();
            $('#ShipByArriveIn24Hrs').show();
            break;
        case '1':
            $('#ShipByNormal').hide();
            $('#ShipByArriveIn24Hrs').hide();
            $('#ShipByArriveIn72Hrs').show();
            break;
    }
}
function AccBusinessGroupChange(obj) {
    $("#hidAccBusinessGroup").val($(obj).find(':checked').val());
    if ($(obj).find(':checked').val() == '6') {
        //$('[id*=rdlOnlineOnly]').attr('checked','checked');
        $('#OnlineOnlyText').text('<%= LunchKingSite.I18N.Message.SalesBusinessOrderPponVerifyTypePiinlifeOnlineOnly %>');
        //$('li').has('[id*=rdlOnlineUse]').hide();
        $('li').has('[id*=rdlInventoryOnly]').hide();
        $('.Piinlife').show();
        $('.NonPiinlife').hide();
    }else {
        $('#OnlineOnlyText').text('<%= LunchKingSite.I18N.Message.SalesBusinessOrderPponVerifyTypeOnlineOnly %>');
        //$('li').has('[id*=rdlOnlineUse]').show();
        $('li').has('[id*=rdlInventoryOnly]').show();
        $('.Piinlife').hide();
        $('.NonPiinlife').show();
        $('.Piinlife').find('input').attr('checked', false);
    }
    var iconLength = $('[id*=cbl_Icons]').find('span').filter(function() { return $(this).css('display') != 'none';}).length;
    if (iconLength == 0) {
        $('[id*=cbl_Icons]').parent('li').hide();
    }else {
        $('[id*=cbl_Icons]').parent('li').show();
    }
    if($(obj).find(':checked').val() == '5'){
        $(".divCouponTravelTag").show();
        if ($('input[id*=rdlBusinessType]:checked').val() == 'Product') {
            $(".DeliveryTravelTag").show();
        } else{
            $(".DeliveryTravelTag").hide();
        }
    }
    else{
        $(".divCouponTravelTag").hide();
        $(".DeliveryTravelTag").hide();
    }
    $('[id*=hidOnlineOnly]').val($('#OnlineOnlyText').text());
}

function ChangeCopyTypes(obj) {
    if ($(obj).find(':checked').val() == 'Again') {
        $('#CopyBusinessContinued').css('display','');       
    }else {
        $('#CopyBusinessContinued').css('display','none');
        $('[id*=chkIsContinuedSequence]').attr('checked',false);
        $('[id*=chkIsContinuedQuantity]').attr('checked',false);
    }
}

function AutoCheckContinuedQuantity(obj) {
    if ($.trim($(obj).val()) != "") {
        $('[id*=chkIsContinuedSequence]').attr('checked',true);
    }else {
        $('[id*=chkIsContinuedSequence]').attr('checked',false);
    }
    //if ($(obj).is(':checked')) {
    //    $('[id*=chkIsContinuedSequence]').attr('checked',true);
    //}else {
    //    $('[id*=chkIsContinuedSequence]').attr('checked',false);
    //}
}

function ShowCommercialList(obj) {
    if ($(obj).is(':checked')) {
        $(obj).next().next('#CommercialCategoryList').css('display','');
    }else {
        $(obj).next().next('#CommercialCategoryList').css('display','none');
        $(obj).next().next('#CommercialCategoryList').find(':checked').each(function(){
            $(this).attr('checked',false);
        });
    }
}

//function checkTravelCategory(obj) {
//    if ($(obj).next().text() == '旅遊渡假') {
//        if ($(obj).is(':checked')) {
//            $('.travelList').css('display', '');
//        } else {
//            $('.travelList').css('display', 'none');
//        }
//    }
//}

function setupdatepicker(from, to) {
    var dates = $('#' + from + ', #' + to).datepicker({
        changeMonth: true,
        numberOfMonths: 3,
        onSelect: function (selectedDate) {
            var option = this.id == from ? "minDate" : "maxDate";
            var instance = $(this).data("datepicker");
            var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
            dates.not(this).datepicker("option", option, date);
        }
    });
}
function SetBookingSystemDisplay(obj) {
    var cnt = 0;
    while ($(obj).prop("tagName") != 'LI') {
        obj = $(obj).parent();
        cnt++;
        if(cnt > 10) return;
    }
    if ($('input[id*=rdlBusinessType]:checked').val() == 'Ppon') {
        if ($(obj).find(':checkbox[id*=cb_IsUsingbookingSystem]').is(':checked')) {
            $(obj).find('.CouponUsers').show();
            $(obj).find('#txtAdvanceReservationDays').numeric();
            if ($(obj).find(':radio[id*=rdoSingleUser]').is(':checked')) {
                $(obj).find('#txtCouponUsers').val("");
                $(obj).find('#txtCouponUsers').prop('disabled', true);
            } else if($(obj).find(':radio[id*=rdoMultiUsers]').is(':checked')) {
                $(obj).find('#txtCouponUsers').prop('disabled', false);
                $(obj).find('#txtCouponUsers').numeric();
            }
        }
        else {
            $(obj).find('.CouponUsers').hide();
        }
    }
    else
    {
        $(obj).find('.BookingSystemLi').hide();
    }
}
function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
function BindBookingSystem() {
    $('.BookingSystemManager').bind("change", function(){
        SetBookingSystemDisplay(this);
    });
    $('.SignleUser').bind("change", function(){
        SetBookingSystemDisplay(this);
    });
    $('.MultiUsers').bind("change", function(){
        SetBookingSystemDisplay(this);
    });

    
}
function OnlineUse(obj) {
    if ($(obj).attr('id') == '<%= rdlOnlineUse.ClientID %>') {
        $('.OnlineUseLimit').val('1');
        $('.OnlineUseLimit').attr('disabled','disabled');
    }else {
        $('.OnlineUseLimit').removeAttr('disabled');;
    }
}
function CheckATM(obj) {
    if ($(obj).is(':checked')) {
        $(obj).next('#spanATM').css('display','');
    }else {
        $(obj).next('#spanATM').css('display','none');
        $(obj).next('#spanATM').find('input[id*=txtAtmQuantity]').val('');
    }
}

function atmMaximumAutoFill(obj) {
    var accGroup = $("#hidAccBusinessGroup").val();
    var piinlife = ($("#hidPiinLifeForNPponCity").val() == "1") ? true : false;
    var channelData = $.parseJSON($("#<%=hiddenChannelChecked.ClientID%>").val());
    var itemPrice = $(obj).parent().parent().find("#txtItemPrice").val();

    if(accGroup != "<%=(int)AccBusinessGroupNo.PiinLife%>" && !piinlife && itemPrice != "0") 
    {
        var checkBreak = false;
        for(var i=0;i<channelData.length;i++) {
            if(channelData[i].toString() == "<%=(int)CategoryManager.Default.Family.CategoryId%>" 
                    || channelData[i].toString() == "<%=(int)CategoryManager.Default.Piinlife.CategoryId%>") {
                checkBreak = true;
            }
        }

        if(!checkBreak) {
            var objParent = $(obj).parent().parent();
            if(!$(objParent).find("input[id=chkIsAtm]").is(':checked')) {
                $(objParent).find("input[id=chkIsAtm]").click();
                CheckATM($(objParent).find("input[id=chkIsAtm]"));
            }
                
            var tempAtmQty = parseInt($(objParent).find("input[id*=txtAtmQuantity]").val());
            var tempCustomQty = parseInt($(objParent).find("input[id=hidCustomAtmQty]").val());

            if(tempCustomQty == -1 || (tempCustomQty == tempAtmQty)) {
                var tempVal = parseInt(parseInt($(obj).val()) * 0.3);
                $(objParent).find("input[id=hidCustomAtmQty]").val(tempVal);
                $(objParent).find("input[id*=txtAtmQuantity]").val(tempVal);
            }
        }
    }
}

var categoryKeyArray = <%=hiddenChannelJSon.Value%>;
        var categoryDataArray = <%=hiddenChannelCategoryJSon.Value%>;    
        
    <%--顯示所有新版分類的資料--%>
        function ShowAllCategory() {

            var selectedChannel = <%=hiddenChannelChecked.Value%>;
            var selectedSubChannel = <%=hiddenSubChannelChecked.Value%>;
            var selectedDealCategory = <%=hiddenDealCategory.Value %>;
            var selectedSubChannelSpecialRegion  =<%=hiddenSubSpecialRegionChecked.Value%>;

            var subRegionIndex = 0;
        <%--頻道與區域--%>
            $.each(selectedChannel, function(i, channelId) {
                $('#channelCategory' + channelId).attr('checked', true);
                OnChannelCategoryChange();
                $.each(selectedSubChannel[i], function(j, arealId) {
                    var subChannel = $('#subChannelCategory' + arealId);
                    subChannel.attr('checked', true);
                    if ( selectedSubChannelSpecialRegion[subRegionIndex]!=null && selectedSubChannelSpecialRegion!=null && selectedSubChannelSpecialRegion.length > 0) {
                        $.each(selectedSubChannelSpecialRegion[subRegionIndex], function(y, subRegionId) {
                            var subRegionChannel = $('#subChannelSPRegionCategory' + arealId);
                            if (subRegionChannel.find('#subChannelSpecialRegion' + subRegionId) != null && subRegionChannel.find('#subChannelSpecialRegion' + subRegionId).length > 0) {
                                subRegionChannel.find('#subChannelSpecialRegion' + subRegionId).attr('checked', true);
                            }
                        });
                    }
                    OnChannelCategoryChange();
                    subRegionIndex++;
                });
            });
            <%--檔次分類，注意!一定要讓頻道與區域先設定，分類的checkbox狀態才會對，正確後，才可以設定分類--%>
            $.each(selectedDealCategory , function(i, categoryId) {
                $('#dealCategory' + categoryId).attr('checked', true);
                //OnDealCategoryChange();
            }
            );
        }

        function OnDealCategoryChange() {
            //var kids = $("#dealCategoryTable input[type='checkbox']");
            var kids = $("table[id^='dealCategoryArea'] input[type='checkbox']");
            var selectedArray = [];
            $.each(kids, function(j, obj) {
                if (obj.checked) {
                    var categoryId = obj.id.replace('dealCategory', '');
                    selectedArray.push(parseInt(categoryId));
                }
            }
            );
            $('#<%=hiddenDealCategory.ClientID%>').val(JSON.stringify(selectedArray));

            if ($('[id*=txtProductSpec]').attr('disabled') != 'disabled') {
                var value = '品名：\n規格(重量/淨重/內容量/數量/尺寸)：內容物中液汁與固形物混合者，須分別標示容量\n內容物成分：\n添加物名稱：\n產地(國家或台灣縣市)：\n廠商名稱：\n廠商電話：\n廠商地址：\n投保產品責任險字號：若無產險資訊不得販售';
                // 若分類勾選[人氣美食]或[生鮮超市]時，規格欄位需預帶值(2015/1/1政府法令規定項目)
                if($('#<%= txtProductSpec.ClientID %>').val() == '' && ($('#dealCategory113').is(':checked') || $('#dealCategory114').is(':checked'))) {
                    $('#<%= txtProductSpec.ClientID %>').val(value);
                } else if ($('#<%= txtProductSpec.ClientID %>').val() == value && (!$('#dealCategory113').is(':checked') && !$('#dealCategory114').is(':checked'))) {
                    $('#<%= txtProductSpec.ClientID %>').val('');
                }
            }
        }

        function SyncChklPponCity(obj,ischeck) {
            if (parseInt(obj, 10) == 88) { (ischeck)?  $('#<%=chklPponCity.ClientID%>_0').attr('checked','checked'):$('#<%=chklPponCity.ClientID%>_0').removeAttr('checked');}
            else if (parseInt(obj, 10) == 89) {(ischeck)? $('#<%=chklPponCity.ClientID%>_8').attr('checked','checked'):$('#<%=chklPponCity.ClientID%>_8').removeAttr('checked');}
            else if (parseInt(obj, 10) == 90) {(ischeck)? $('#<%=chklPponCity.ClientID%>_7').attr('checked','checked'):$('#<%=chklPponCity.ClientID%>_7').removeAttr('checked');}
            else if (parseInt(obj, 10) == 91) {(ischeck)? $('#<%=chklPponCity.ClientID%>_9').attr('checked','checked'):$('#<%=chklPponCity.ClientID%>_9').removeAttr('checked');}
            else if (parseInt(obj, 10) == 94) {(ischeck)? $('#<%=chklPponCity.ClientID%>_1').attr('checked','checked'):$('#<%=chklPponCity.ClientID%>_1').removeAttr('checked');}
            else if (parseInt(obj, 10) == 95) {(ischeck)? $('#<%=chklPponCity.ClientID%>_2').attr('checked','checked'):$('#<%=chklPponCity.ClientID%>_2').removeAttr('checked');}
            else if (parseInt(obj, 10) == 96) {(ischeck)? $('#<%=chklPponCity.ClientID%>_3').attr('checked','checked'):$('#<%=chklPponCity.ClientID%>_3').removeAttr('checked');}
            else if (parseInt(obj, 10) == 97) {(ischeck)? $('#<%=chklPponCity.ClientID%>_4').attr('checked','checked'):$('#<%=chklPponCity.ClientID%>_4').removeAttr('checked');}
            else if (parseInt(obj, 10) == 98) {(ischeck)? $('#<%=chklPponCity.ClientID%>_5').attr('checked','checked'):$('#<%=chklPponCity.ClientID%>_5').removeAttr('checked');}
            else if (parseInt(obj, 10) == 99) {(ischeck)? $('#<%=chklPponCity.ClientID%>_6').attr('checked','checked'):$('#<%=chklPponCity.ClientID%>_6').removeAttr('checked');}
            else if (parseInt(obj, 10) == 101){(ischeck)? $('#<%=chklTravelCategory.ClientID%>_0').attr('checked','checked'):$('#<%=chklTravelCategory.ClientID%>_0').removeAttr('checked');}
            else if (parseInt(obj, 10) == 102){(ischeck)? $('#<%=chklTravelCategory.ClientID%>_1').attr('checked','checked'):$('#<%=chklTravelCategory.ClientID%>_1').removeAttr('checked');}
            else if (parseInt(obj, 10) == 103){(ischeck)? $('#<%=chklTravelCategory.ClientID%>_2').attr('checked','checked'):$('#<%=chklTravelCategory.ClientID%>_2').removeAttr('checked');}
            else if (parseInt(obj, 10) == 104){(ischeck)? $('#<%=chklTravelCategory.ClientID%>_3').attr('checked','checked'):$('#<%=chklTravelCategory.ClientID%>_3').removeAttr('checked');}
            else if (parseInt(obj, 10) == 105){(ischeck)? $('#<%=chklTravelCategory.ClientID%>_4').attr('checked','checked'):$('#<%=chklTravelCategory.ClientID%>_4').removeAttr('checked');}
}

<%--頻道選取發生變化時--%>  
        function OnChannelCategoryChange() {
            var selectedArray = [];
            var subSelectArray = [];
            var subSpecialRegionArray = [];            
            var subStandardRegionArray = [];            
            var enabledCategoryId = [];
            $.each(categoryKeyArray, function(j, obj) {
                var channelBox = $('#channelCategory' + obj);
                var kids = $("#subChannelCol" + obj + " input[type='checkbox']");
                var kidText = $("#subChannelCol" + obj + " label");
                SyncChklPponCity(obj,channelBox.is(':checked'));
                var spRegions = $("#specialRegion" + obj );
                if (channelBox.is(':checked')) {
                <%--有選取--%>
                    selectedArray.push(obj);
                    
                    kids.attr('disabled', false);
                    kidText.css('color', 'black');
                    var areaSelected = [];
                    var SpecialRegionSelected = [];
                    
                    var isShowSPRegion =false;                    
                    $.each(kids, function(h, subArea) {
                        var subAreaId = parseInt(subArea.id.replace('subChannelCategory', ''));
                        SyncChklPponCity(subArea.id.replace('subChannelCategory', ''),subArea.checked);
                        if (subArea.checked) {
                            SpecialRegionSelected = [];
                            StandardRegionSelected = [];                            
                            subArea.disable = false;
                            areaSelected.push(subAreaId);
                            isShowSPRegion = true;
                            if ($('#subChannelSPRegionCategory' + subAreaId).find("td input[type='checkbox']")!=null && $('#subChannelSPRegionCategory' + subAreaId).find("td input[type='checkbox']").length > 0) {
                                $('#subChannelSPRegionCategory' + subAreaId).show();

                                var specialkids = $("#subChannelSPRegionCategory" + subAreaId + " input[type='checkbox'][id^='subChannelSpecialRegion']");
                                if ( specialkids!=null && specialkids.length > 0) {
                                    $.each(specialkids, function(h, specialregion) {
                                        var subSpecialRegionId = parseInt(specialregion.id.replace('subChannelSpecialRegion', ''));
                                        if (specialregion.checked) {
                                            SpecialRegionSelected.push(subSpecialRegionId);
                                        }
                                    });
                                }
                            }
                            subSpecialRegionArray.push(SpecialRegionSelected);
                            subStandardRegionArray.push(StandardRegionSelected);
                            
                        } else {
                            $('#subChannelSPRegionCategory' + subAreaId).hide();                            
                        }
                    });
                    
                    if (isShowSPRegion) {
                        spRegions.show();
                    } else {
                        spRegions.hide();
                        spRegions.find("input[type='checkbox']").each(function() {
                            $(this).attr('checked', false);
                        });
                        spRegions.find("input[id^=specialRegion]").each(function() {
                            $(this).hide();
                        });
                    }
                    
                    subSelectArray.push(areaSelected);

                    $.each(categoryDataArray[j], function(h, cid) {
                    <%--檢查如果此項目目前不再可選取屬性中，加入此項目--%>
                        if ($.inArray(cid, enabledCategoryId) == -1) {
                            enabledCategoryId.push(cid);
                        }
                    }
                );

                } else {
                    kids.attr('checked', false);
                    kids.attr('disabled', true);
                    kidText.css('color', 'gray');
                    spRegions.hide();
                    spRegions.find("input[type='checkbox']").each(function() {
                        $(this).attr('checked', false);
                    });                                        
                }
            }
        );
            $('#<%=hiddenChannelChecked.ClientID%>').val(JSON.stringify(selectedArray));
            $('#<%=hiddenSubChannelChecked.ClientID%>').val(JSON.stringify(subSelectArray));
            $('#<%=hiddenSubSpecialRegionChecked.ClientID%>').val(JSON.stringify(subSpecialRegionArray));

            //CheckDealCategoryEnabledStatus(enabledCategoryId, $('#dealCategoryTable'), 'dealCategory', 'dealCategoryDeas');
            CheckDealCategoryEnabledStatus(enabledCategoryId, $("table[id^='dealCategoryArea']"), 'dealCategory', 'dealCategoryDeas');

            $.each(categoryKeyArray, function(j, obj) {
                if ($('#channelCategory' + obj).is(':checked')) {
                    
                    var areacontent = $('#categoryAreaContent' + obj);
                    if (areacontent.find('input[type="checkbox"]').length > 0) {
                        areacontent.show();
                        $('#categoryAreaHeader' + obj).show();                        
                    }
                    areacontent.find('input[type="checkbox"]').attr('disabled',false);
                    areacontent.find('label[id^="dealCategoryDeas"]').css('color', 'black');

                } else {
                    $('#categoryAreaHeader' + obj).hide();
                    var areacontent = $('#categoryAreaContent' + obj);
                    areacontent.hide();
                    areacontent.find('input[type="checkbox"]').prop('checked',false).attr('disabled',true);
                    areacontent.find('label[id^="dealCategoryDeas"]').css('color', 'gray');

                    areacontent.find('ul[id^="dealCategorytree"]').checkboxTree('uncheckAll');
                }
            });
            OnDealCategoryChange();
        }

        function MoveDealCategoryItem() {
            $.each(categoryKeyArray, function(j, obj) {
                var tr = $('<tr />');
                //var td = $('<td />');
                var dealCategoryArea = $('#dealCategoryArea' + obj);
                if (dealCategoryArea.find("tr").length == 0 ) {
                    dealCategoryArea.append(tr);
                }
                $.each(categoryDataArray[j], function(h, cid) {

                    var dealCategroyTree = $('#dealCategorytree' + cid);
                    if (dealCategroyTree.length > 0);
                    {
                        if (dealCategoryArea.find("tr:last").length > 0 && dealCategoryArea.find("tr:last").find("td").length >= 4) {
                            dealCategoryArea.find("tr:last").after("<tr>");
                            dealCategoryArea.find("tr:last").append(dealCategroyTree.parent().clone().css("width", "25%").css("vertical-align","top"));
                            dealCategoryArea.find("tr:last").after("</tr>");
                        } else {
                            dealCategoryArea.find("tr:last").append(dealCategroyTree.parent().clone().css("width", "25%").css("vertical-align","top"));
                        }

                    }
                });

                if ($('#channelCategory' + obj).is(':checked')) {
                    var checkareacontent = $('#categoryAreaContent' + obj);
                    if (checkareacontent.find('input[type="checkbox"]').length > 0) {
                        checkareacontent.show();
                        $('#categoryAreaHeader' + obj).show();                        
                    }
                    checkareacontent.find('input[type="checkbox"]').attr('disabled',false);
                    checkareacontent.find('label[id^="dealCategoryDeas"]').css('color', 'black');

                } else {
                    $('#categoryAreaHeader' + obj).hide();
                    var uncheckareacontent = $('#categoryAreaContent' + obj);
                    uncheckareacontent.hide();
                    uncheckareacontent.find('input[type="checkbox"]').attr('disabled',true);
                    uncheckareacontent.find('label[id^="dealCategoryDeas"]').css('color', 'gray');
                }
            });
            <%--checkboxTree --%>
            $('ul[id^="dealCategorytree"]').checkboxTree({
                initializeChecked: 'expanded',
                initializeUnchecked: 'collapsed',
                onCheck: {
                    node: 'expand',
                    ancestors: 'check',
                    descendants: 'uncheck' 
                },
                onUncheck: {
                    node: 'collapse'
                },
                collapseImage: '../../Tools/js/css/images/downArrow.gif',
                expandImage: '../../Tools/js/css/images/rightArrow.gif',
                collapse: function(){
                    //alert('collapse event triggered (passed as option)');
                },
                expand: function(){
                    //alert('expand event triggered (passed as option)');
                },
                collapseDuration: 200,
                expandDuration: 200
            });  //.removeClass(' ui-widget-content')
            OnDealCategoryChange();
        }

        function ChangeInvoiceFounder() {
            var obj = $('#<%= rdlInvoiceFounder.ClientID %>');
            if ($(obj).find(':checked').val() == '<%= (int)InvoiceFounderType.Company %>') {
                // 同簽約公司
                var companyName = $('#<%= txtCompanyName.ClientID %>').val();
                $('#txtInvoiceFounderCompanyName').val(companyName);

                var companyId;
                if ($('input[id*=rdlPrivateContract]:checked').val() == 'True') {
                    companyId = $('#<%= txtPersonalId.ClientID %>').val();
                }else {
                    companyId = $('#<%= txtGuiNumber.ClientID %>').val();
                }
                $('#txtInvoiceFounderCompanyID').val(companyId);

                var bossName = $('#<%= txtBossName.ClientID %>').val();
                $('#txtInvoiceFounderBossName').val(bossName);

            } else if ($(obj).find(':checked').val() == '<%= (int)InvoiceFounderType.Accounting %>') {
                // 同受款人
                var companyName = $('#<%= txtAccounting.ClientID %>').val();
                $('#txtInvoiceFounderCompanyName').val(companyName);

                var companyId = $('#<%= txtAccountingId.ClientID %>').val();
                $('#txtInvoiceFounderCompanyID').val(companyId);

                $('#txtInvoiceFounderBossName').val('');
                $('#lblInvoiceFounderBossName').text('');

            } else if ($(obj).find(':checked').val() == '<%= (int)InvoiceFounderType.Others %>') {
                // 其他
                $('#txtInvoiceFounderCompanyName').val('');
                $('#lblInvoiceFounderCompanyName').text('');

                $('#txtInvoiceFounderCompanyID').val('');
                $('#lblInvoiceFounderCompanyID').text('');

                $('#txtInvoiceFounderBossName').val('');
                $('#lblInvoiceFounderBossName').text('');
            }
        SetInvoiceFounderVisible();
    }

        function checkKeywords() {
            var words = $('#txtKeywords').val();
            var newWords = '';
            if (words.split('/').length > 3) {
                alert('僅限輸入至多3個字詞');
                for (var i = 0; i < 3; i++) {
                    newWords += words.split('/')[i] + '/';
                }
                $('#txtKeywords').val(newWords.slice(0,-1));
            }
        }

    function SetInvoiceFounderVisible() {
        var obj = $('#<%= rdlInvoiceFounder.ClientID %>');
        if ($(obj).find(':checked').val() == '<%= (int)InvoiceFounderType.Company %>') {
            // 同簽約公司
            $('#lblInvoiceFounderCompanyName').text($('#txtInvoiceFounderCompanyName').val());
            $('#txtInvoiceFounderCompanyName').hide();
            $('#lblInvoiceFounderCompanyID').text($('#txtInvoiceFounderCompanyID').val());
            $('#txtInvoiceFounderCompanyID').hide();
            $('#lblInvoiceFounderBossName').text($('#txtInvoiceFounderBossName').val());
            $('#txtInvoiceFounderBossName').hide();
        } else if ($(obj).find(':checked').val() == '<%= (int)InvoiceFounderType.Accounting %>') {
            // 同受款人
            $('#lblInvoiceFounderCompanyName').text($('#txtInvoiceFounderCompanyName').val());
            $('#txtInvoiceFounderCompanyName').hide();
            $('#lblInvoiceFounderCompanyID').text($('#txtInvoiceFounderCompanyID').val());
            $('#txtInvoiceFounderCompanyID').hide();
            $('#txtInvoiceFounderBossName').show();
        } else if ($(obj).find(':checked').val() == '<%= (int)InvoiceFounderType.Others %>') {
            // 其他
            $('#txtInvoiceFounderCompanyName').show();
            $('#txtInvoiceFounderCompanyID').show();
            $('#txtInvoiceFounderBossName').show();
        }
}

function setHidPiinLifeForNPponCity() 
{
    if($("#hidPiinLifeForNPponCity").val() == "0") {
        $("#hidPiinLifeForNPponCity").val("1");
    } else {
        $("#hidPiinLifeForNPponCity").val("0");
    }
            
}

function CheckDealCategoryEnabledStatus(enabledCategoryId, area, inputId, labelId) {
    <%--處理dealCategory的enabled狀態--%>
    var dealCategoryBoxList = area.find("input[type='checkbox']");
    $.each(dealCategoryBoxList, function(j, obj) {
        var categoryId = parseInt(obj.id.replace(inputId, ''));
        var categoryDesc = $('#' + labelId + categoryId);
        if ($.inArray(categoryId, enabledCategoryId) == -1) {
            <%--不包含於可選取項目中--%>
                obj.disabled = true;
                categoryDesc.css('color', 'gray');
        } else {
            obj.disabled = false;
            categoryDesc.css('color', 'black');
        }
    });
}
function SetDealType2(isChanged)
{
    var ddlFirstNode = "請選擇子分類";
    var ddlMain = $('#<%=ddlDealType1.ClientID%>');
    var ddlSub = $('#<%=ddlDealType3.ClientID%>');
    var hdSub = $('#<%=hdDealType2.ClientID%>');
    $("#<%=ddlDealType1.ClientID%> option[value='1999']").attr('disabled','disabled');
    $.ajax({
        type: "POST",
        url: "BusinessOrderDetail.aspx/GetDealType2",
        data: "{parentId: " + ddlMain.val() + "}",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var cates = $.parseJSON(data.d);
            ddlSub.empty();
            ddlSub.append($('<option></option>').val('0').html(ddlFirstNode));
            $.each(cates, function (i, item) {
                ddlSub.append($('<option></option>').val(item.CodeId).html(item.CodeName));
            })
            if (isChanged) {
                SetDealType2Value();
            }else {
                // set branch info
                var codeId = hdSub.val();
                if (codeId != '') {
                    ddlSub.val(codeId);
                }
            }
        },
        error: function (isChanged) {
            //ddlSub.empty();
            //ddlSub.append($('<option></option>').val('0').html(ddlFirstNode));
        }
    });
}

function SetDealType2Value(isChanged)
{
    var obj = $('#<%=ddlDealType3.ClientID%>');
    var id = obj.val().index;
    if (id != '' && id != null) {
        $('[id*=hdDealType2]').val(id);
    }
    if(isChanged){
        $('[id*=hdDealType2]').val($.trim($(obj).find(':selected').val().replace(id,'')));
    }
}

function OpenStoreWindow() {
    var boid = '<%= BusinessOrderObjectId %>';
    var enable = '<%= txtSalesName.Enabled.ToString().ToLower() %>';
    window.open('storeadd.aspx?enable=' + enable+ '&boid=' + boid);
}

function isSlottingFeeDeal(){
    var items = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R');
    for (var i = 0; i < $('[id*=liDeal]').length; i++) {
        var slottingFeeDeal = $('[id*=liDeal]').eq(i).find('[id=ckSlottingFeeDeal]').attr('checked');
        if (slottingFeeDeal == 'checked')
        {
            $('[id*=liDeal]').eq(i).find('[id=txtSlottingFeeQuantity]').val('0');
            $('[id*=liDeal]').eq(i).find('[id=txtSlottingFeeQuantity]').prop('disabled', true);
            $('[id*=liDeal]').eq(i).find('[id=txtPurchasePrice]').val('0');
            $('[id*=liDeal]').eq(i).find('[id=txtPurchasePrice]').prop('disabled', true);
        }
        else
        {
            $('[id*=liDeal]').eq(i).find('[id=txtSlottingFeeQuantity]').prop('disabled', false);
            $('[id*=liDeal]').eq(i).find('[id=txtPurchasePrice]').prop('disabled', false);
        }
      }
}

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <asp:Panel ID="divBusinessOrder" runat="server">
        <table style="padding: 5px 5px 5px 1px;">
            <tr>
                <td style="vertical-align: top; font-size: 14px;">
                    <table style="padding: 10px 10px 15px 20px; border-style: solid; border-color: #bbb; border-width: 1px; background-color: White; letter-spacing: 2px; line-height: 30px; width: 960px;">
                        <tr>
                            <td style="font-size: large; font-weight: bold; padding-left: 12px;">
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <div id="divBusinessOrderId">
                                                <asp:HiddenField ID="hidSalesLevel" runat="server" />
                                                <asp:Label ID="lblMohistVerify" runat="server" Text="(墨攻)" ForeColor="Red" Font-Size="XX-Large"
                                                    Visible="false"></asp:Label>
                                                <asp:Label ID="lblHwatai" runat="server" Text="(華泰)" ForeColor="DarkOrange" Font-Size="XX-Large"
                                                    Visible="false"></asp:Label>
                                                <asp:Label ID="lblTitle" runat="server" Text="新增業務工單" Font-Bold="true" Font-Size="XX-Large"></asp:Label><br />
                                            </div>
                                        </td>
                                        <td rowspan="2">
                                            <table style="font-size: small; color: Gray; font-weight: normal; line-height: 20px">
                                                <tr>
                                                    <td style="text-align: right;">工單編號：
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblBusinessOrderId" runat="server" ForeColor="Black"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="BusinessCopyType">
                                                    <td style="text-align: right;">關聯工單：
                                                    </td>
                                                    <td>
                                                        <asp:HyperLink ID="hlCopyBusinessOrderId" runat="server" ForeColor="Blue" Target="_blank"></asp:HyperLink>
                                                        <asp:Label ID="lblBusinessCopyType" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">建立人員：
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblCreateId" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">最後修改人員：
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblModifyId" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">最後修改日期：
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblModifyTime" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr id="BusinessHourTimeS">
                                                    <td style="text-align: right;">上檔日期：
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblBusinessHourTimeS" runat="server" ForeColor="Red"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 220px">
                                            <div id="divBusinessType" style="font-size: 14px">
                                                <asp:RadioButtonList ID="rdlBusinessType" runat="server" RepeatDirection="Horizontal"
                                                    RepeatLayout="Flow">
                                                    <asp:ListItem Text="好康" Value="Ppon" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="宅配" Value="Product"></asp:ListItem>
                                                </asp:RadioButtonList>
                                                <asp:CheckBox ID="chkIsTravelType" runat="server" Text="旅遊類" />
                                                <br />
                                                <asp:HyperLink ID="hkBack" ForeColor="Blue" Text="回工單列表" runat="server" NavigateUrl="~/Sal/SalesManagement.aspx"></asp:HyperLink>
                                            </div>
                                        </td>
                                        <td style="width: 340px">
                                            <asp:HiddenField ID="hidStatus" runat="server" />
                                            <asp:Panel ID="divStatus" runat="server">
                                                <span style="font-size: 14px; font-weight: normal; color: Blue;">工單審核狀態：</span><br />
                                                <asp:DropDownList ID="ddlStatus" runat="server">
                                                    <asp:ListItem Text="草稿（業務）" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="送審（業務主管）" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="核准（各區營管）" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="已複查（營管）" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="已排檔（創意編輯）" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="已建檔（完成）" Value="5"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:Button ID="btnUpdateStatus" runat="server" Text="更新" OnClick="SaveCheck" OnClientClick="return DataCheck();"
                                                    CausesValidation="false" ValidationGroup="Update" />
                                            </asp:Panel>
                                            <asp:Timer ID="UpdateTimer" runat="server" OnTick="Save" Enabled="false">
                                            </asp:Timer>
                                            <asp:UpdatePanel ID="UpdUpdateStatus" runat="server" UpdateMode="Conditional" ViewStateMode="Enabled">
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="UpdateTimer" EventName="Tick" />
                                                </Triggers>
                                                <ContentTemplate>
                                                    <asp:Label ID="lblStoreAddressMsg" runat="server" Visible="false" ForeColor="Red"></asp:Label>
                                                    <asp:Label ID="lblErrorMsg" runat="server" ForeColor="Red" Text="更新失敗，工單資訊填入不完全!!"
                                                        BackColor="Pink" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblSuccessMsg" runat="server" ForeColor="Red" BackColor="Yellow" Visible="false"></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 10px">
                                <table style="padding-left: 7px">
                                    <tr>
                                        <td style="text-align: right; padding-right: 30px">
                                            <label class="showStatus" value="0">
                                                業務
                                            </label>
                                            <label class="showStatus" value="1">
                                                >> 業務主管
                                            </label>
                                            <label class="showStatus" value="2">
                                                >> 各區營管
                                            </label>
                                            <label class="showStatus" value="3">
                                                >> 營管
                                            </label>
                                            <label class="showStatus" value="4">
                                                >> 創意編輯
                                            </label>
                                            <label class="showStatus" value="5">
                                                >> 完成</label>
                                        </td>
                                    </tr>
                                </table>
                                <fieldset style="width: 864px; margin-left: 10px; font-size: 14px;">
                                    <legend id="ToolBar" style="cursor: pointer;" title="點選收合">工具列</legend>
                                    <div class="ToolBarFool">
                                        <asp:HiddenField ID="hidStoreSelectedValue" runat="server" />
                                        <asp:PlaceHolder ID="divDonwload" runat="server">
                                            <asp:DropDownList ID="ddlStoreList" runat="server" CssClass="Ppon">
                                            </asp:DropDownList>
                                            <asp:Button ID="btnDownload" Text="工單下載" runat="server" OnClick="btnDownload_Click" />
                                            微調:<asp:TextBox ID="txtDonwloadBottomMarginFix" runat="server" Width="20px"></asp:TextBox>(1~9)
                                            <asp:Button ID="btnDownloadStore" Text="分店資料下載" runat="server" OnClick="btnDownloadStore_Click" />
                                        </asp:PlaceHolder>
                                        <asp:Button ID="btnDelete" runat="server" Text="刪除" Visible="false" OnClick="btnDelete_Click"
                                            OnClientClick="return confirm('確定義無反顧地刪除此筆工單?')" CausesValidation="false" />
                                        <div>
                                            <font style="color: Red; font-size: 14px">合約狀態：</font>
                                            <asp:CheckBox ID="chkIsLongContract" runat="server" Text="長期約" />
                                            <asp:CheckBox ID="chkIsContractSend" runat="server" Text="合約已送交" onclick="CheckContractReasonVisible();" />
                                            <span id="NoContractReason">
                                                <asp:DropDownList ID="ddlNoContractReason" runat="server" onchange="SetNoContractReason();">
                                                    <asp:ListItem Text="請選擇" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="業務尚未簽回" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="資料有誤退回" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="資料尚未備齊" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="店家內部流程處理中" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="其他原因" Value="5"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="txtNoContractReason" runat="server"></asp:TextBox></span>
                                        </div>
                                        <asp:Panel ID="divReferred" runat="server">
                                            <font style="color: Blue; font-size: 14px">轉件功能：</font>
                                            <asp:TextBox ID="txtReferredUser" runat="server" Width="150px"></asp:TextBox>
                                            <asp:Button ID="btnReferred" runat="server" Text="確定" OnClick="btnReferred_Click"
                                                OnClientClick="return CheckReferred();" />
                                        </asp:Panel>
                                        <asp:Panel ID="divCopyBusiness" runat="server">
                                            <font style="color: Blue; font-size: 14px">複製功能：</font>
                                            <asp:RadioButtonList ID="rdlBusinessCopyType" runat="server" RepeatLayout="Flow"
                                                RepeatDirection="Horizontal" Font-Size="14px" onclick="ChangeCopyTypes(this);">
                                                <asp:ListItem Text="一般" Value="General" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="再次上檔" Value="Again"></asp:ListItem>
                                            </asp:RadioButtonList>
                                            <span id="CopyBusinessContinued" style="display: none;">
                                                <%--<asp:CheckBox ID="chkIsContinuedQuantity" runat="server" Text="接續銷售數量" onclick="AutoCheckContinuedQuantity(this);" />--%>
                                                <asp:CheckBox ID="chkIsContinuedQuantity" runat="server" Text="接續銷售數量" />
                                                <asp:CheckBox ID="chkIsContinuedSequence" runat="server" Text="接續憑證號碼" style="display:none" />
                                            </span>
                                            <asp:Button ID="btnCopy" runat="server" Text="複製" OnClick="btnCopy_Click" />
                                        </asp:Panel>
                                        <div>
                                            檔號：<asp:TextBox ID="txtUniqueId" runat="server"></asp:TextBox>
                                            Id：<asp:TextBox ID="txtBid" runat="server" Width="300px"></asp:TextBox>
                                            <font style="font-size: small; color: Red;">*註:P好康檔次填入bid，品生活填入did</font>
                                        </div>
                                        <asp:Panel ID="divSellerId" runat="server">
                                            賣家編號：<asp:TextBox ID="txtSellerId" runat="server"></asp:TextBox>
                                            <asp:HiddenField ID="hidSellerGuid" runat="server" />
                                        </asp:Panel>
                                        <asp:Panel ID="divBusinessHourTimeS" runat="server" Visible="false">
                                            <b><font style="color: Red">*</font>上檔日期:</b>
                                            <asp:TextBox ID="txtBusinessHourTimeS" runat="server" />
                                        </asp:Panel>
                                        <asp:Panel ID="divHistoryMemo" runat="server" Visible="false">
                                            <b><font style="color: Red">*</font>其他異動或附註:</b>
                                            <asp:TextBox ID="txtHistory" runat="server" Width="640px"></asp:TextBox>
                                        </asp:Panel>
                                        <asp:Panel ID="divAncestorSequenceBid" runat="server" Visible="true">
                                            <b>接續憑證BID:</b>
                                            <asp:TextBox ID="txtAncestorSequenceBid" runat="server" Width="300px" onblur="AutoCheckContinuedQuantity(this);" ></asp:TextBox>
                                        </asp:Panel>
                                        <asp:Panel ID="divAncestorQuantityBid" runat="server" Visible="true">
                                            <b>接續數量BID:</b>
                                            <asp:TextBox ID="txtAncestorQuantityBid" runat="server" Width="300px"></asp:TextBox>
                                        </asp:Panel>
                                    </div>
                                </fieldset>
                                <asp:Panel ID="divBusiness" runat="server">
                                    <div id="navcontainer" style="display: none;">
                                        <ul id="navlist">
                                            <li class="liTitle" value="0"><span>新增業務工單</span></li>
                                            <li class="liTitle" value="1"><span>Part1 店家資料</span></li>
                                            <li class="liTitle" value="2"><span>Part2 好康優惠</span></li>
                                            <li class="liTitle" value="3"><span>Part3 特店折扣</span></li>
                                            <li class="liTitle" value="4"><span>Part4 歷史紀錄</span></li>
                                        </ul>
                                    </div>
                                    <div id="AnchorPoint" style="bottom: 280px; margin-left: -126px; position: fixed; -webkit-transform: translateZ(0); text-align: center; border-top: 1px solid #CDCDCD; width: 95px;">
                                        <a href="#ShowDiv0">
                                            <p class="LeftAnchor">
                                                工單設定
                                            </p>
                                        </a>
                                        <a href="#ShowDiv1">
                                            <p class="LeftAnchor">
                                                店家資料
                                            </p>
                                        </a>
                                        <a href="#ShowDiv2">
                                            <p class="LeftAnchor">
                                                好康優惠
                                            </p>
                                        </a>
                                        <a href="#ShowDiv4">
                                            <p class="LeftAnchor">
                                                歷史紀錄
                                            </p>
                                        </a>
                                        <a href="#wrap">
                                            <div class="HKL_Button_back_to_top_command" style="margin-left: 25px; display: block; margin-top: 10px; position: relative;">
                                            </div>
                                        </a>
                                    </div>
                                    <br />
                                    <div id="ShowDiv0" class="ShowDivClass">
                                        <table>
                                            <tr>
                                                <td class="Title" style="font-weight: bold;">【工單設定】</td>
                                            </tr>
                                            <tr>
                                                <td class="Title" style="color: Blue;">審核權限(業績歸屬)設定(業務主管填寫)<br />
                                                    <asp:HiddenField ID="hidSalesMemberArea" runat="server" />
                                                    <asp:RadioButtonList ID="rdlAuthorizationGroup" runat="server" RepeatDirection="Horizontal"
                                                        RepeatLayout="Table" RepeatColumns="7" CssClass="AuthorizationGroup" DataTextField="Name"
                                                        DataValueField="Id">
                                                    </asp:RadioButtonList>
                                                    <asp:HiddenField ID="hidAuthorizationGroup" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title" style="color: green;">賣家規模(業務助理填寫)：
                                                    <asp:RadioButtonList ID="rdlSellerLevel" runat="server" DataValueField="AccBusinessGroupId"
                                                        DataTextField="AccBusinessGroupName" RepeatDirection="Horizontal" RepeatLayout="Flow"
                                                        onclick="AccBusinessGroupChange(this);">
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="rdlAccBusinessGroup"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title">館別：
                                                    <asp:RadioButtonList ID="rdlAccBusinessGroup" runat="server" DataValueField="AccBusinessGroupId"
                                                        DataTextField="AccBusinessGroupName" RepeatDirection="Horizontal" RepeatLayout="Flow"
                                                        onclick="AccBusinessGroupChange(this);">
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="refAccBusinessGroup" runat="server" ControlToValidate="rdlAccBusinessGroup"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                    <asp:HiddenField ID="hidAccBusinessGroup" ClientIDMode="Static" Value="1" runat="server" />
                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td class="Title">業務所屬部門：勾選業務同仁所屬區域<br />
                                                    <asp:RadioButtonList ID="rdlSalesDept" runat="server" DataTextField="Name" DataValueField="Id"
                                                        RepeatDirection="Horizontal" RepeatLayout="Flow" Enabled="false">
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td class="Title">上檔頻道：<br>
                                                    <asp:CheckBoxList ID="chklPponCity" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                    </asp:CheckBoxList>
                                                    <asp:TextBox ID="txtOtherPponCity" runat="server" Width="80px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="travelList" style="display: none;">
                                                <td class="Title"><font style="color: Red; font-weight: bold">*</font>旅遊區域：
                                                    <asp:CheckBoxList ID="chklTravelCategory" runat="server" RepeatDirection="Horizontal"
                                                        RepeatLayout="Flow">
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title">上檔頻道：<br />
                                                    <asp:HiddenField ID="hiddenChannelChecked" runat="server" Value="[]" />
                                                    <asp:HiddenField ID="hiddenSubChannelChecked" runat="server" Value="[]" />
                                                    <asp:HiddenField ID="hiddenSubSpecialRegionChecked" runat="server" Value="[]" />
                                                    <asp:HiddenField ID="hiddenChannelJSon" runat="server" Value="[]" />
                                                    <asp:HiddenField ID="hiddenChannelCategoryJSon" runat="server" Value="[]" />
                                                    <asp:Repeater runat="server" ID="channelCheckArea" OnItemDataBound="channelCheckArea_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table class="categoryArea">
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td  style="width:12%;">
                                                                    <input type="checkbox" id="channelCategory<%#((CategoryNode)Container.DataItem).CategoryId %>"
                                                                        onchange="OnChannelCategoryChange()" />
                                                                    <label id="channelCategoryLabel<%#((CategoryNode)Container.DataItem).CategoryId %>"
                                                                        onclick="$('#channelCategory<%#((CategoryNode)Container.DataItem).CategoryId %>').click()">
                                                                        <%# ((CategoryNode)Container.DataItem).NameInConsole %>
                                                                    </label>
                                                                    <asp:HiddenField ID="channelCheckId" runat="server" />
                                                                </td>
                                                                <td id="subChannelCol<%#((CategoryNode)Container.DataItem).CategoryId %>" style="text-align: left;width:88%;">
                                                                    <asp:Repeater runat="server" ID="subChannelCheckArea">
                                                                        <ItemTemplate>
                                                                            <input type="checkbox" id="subChannelCategory<%#((CategoryNode)Container.DataItem).CategoryId %>"
                                                                                disabled="disabled" onchange="OnChannelCategoryChange()" />
                                                                            <label onclick="$('#subChannelCategory<%#((CategoryNode)Container.DataItem).CategoryId %>').click()"
                                                                                style="color: gray;">
                                                                                <%# ((CategoryNode)Container.DataItem).NameInConsole %>
                                                                            </label>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </td>
                                                            </tr>
                                                            <tr class="content" id="specialRegion<%#((CategoryNode)Container.DataItem).CategoryId %>">
                                                                <td style="width:12%;"></td>
                                                                <td style="text-align: left;width:88%;">
                                                                    <asp:Repeater runat="server" ID="subChannelSPRegionCheckArea" OnItemDataBound="subChannelSPRegionCheckArea_ItemDataBound">
                                                                        <HeaderTemplate>
                                                                            <table>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr class="content" id="subChannelSPRegionCategory<%#((CategoryNode)Container.DataItem).CategoryId %>">
                                                                                <td>
                                                                                    <label>商圈‧景點：</label>
                                                                                    <asp:Repeater runat="server" ID="subChannelSpecialRegionCheckArea">
                                                                                        <ItemTemplate>
                                                                                            <input type="checkbox" id="subChannelSpecialRegion<%#((CategoryNode)Container.DataItem).CategoryId %>"
                                                                                                onchange="OnChannelCategoryChange()" />
                                                                                            <label onclick="$('#subChannelSpecialRegion<%#((CategoryNode)Container.DataItem).CategoryId %>').click();">
                                                                                                <%# ((CategoryNode)Container.DataItem).CategoryName %>
                                                                                            </label>
                                                                                        </ItemTemplate>
                                                                                    </asp:Repeater>
                                                                                    <asp:Literal ID="llSpecialRegion" runat="server"></asp:Literal>
                                                                            </tr>
                                                                                 </td>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            </table>
                                                                        </FooterTemplate>
                                                                    </asp:Repeater>
                                                                </td>
                                                            </tr>
                                                            <tr id="categoryAreaHeader<%#((CategoryNode)Container.DataItem).CategoryId %>">
                                                                <td style="width:12%;"></td>
                                                                <td style="width:88%;">
                                                                    <table class="categoryArea">
                                                                        <tr><td><label>新分類：</label></td></tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="categoryAreaContent<%#((CategoryNode)Container.DataItem).CategoryId %>">
                                                                <td style="width:12%;"></td>
                                                                <td style="width:88%;">
                                                                    <table class="categoryArea" id="dealCategoryArea<%#((CategoryNode)Container.DataItem).CategoryId %>">
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                    <br />
                                                    <asp:HiddenField ID="hidPiinLifeForNPponCity" ClientIDMode="Static" Value="0" runat="server" />
                                                    <asp:CheckBoxList ID="chklNPponCity" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
                                                    </asp:CheckBoxList>
                                                    <asp:TextBox ID="txtNOtherPponCity" runat="server" Width="80px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title">生活商圈：<br>
                                                    <asp:Repeater ID="rptCommercialCategory" runat="server" OnItemDataBound="rptCommercialCategory_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <ul style="padding-left: 15px;">
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <li style="list-style-type: circle;"><span class="divCommercial">
                                                                <asp:HiddenField ID="hidCommercialCategoryId" runat="server"></asp:HiddenField>
                                                                <asp:CheckBox ID="chkCommercialArea" runat="server" onclick="ShowCommercialList(this);" />
                                                                <span id="CommercialCategoryList" style="display: none;">
                                                                    <asp:CheckBoxList ID="chklCommercialCategory" runat="server" RepeatDirection="Horizontal"
                                                                        RepeatColumns="7" RepeatLayout="Table">
                                                                    </asp:CheckBoxList></span>
                                                            </span>
                                                            </li>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </ul>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                            <%-- DealCategory region --%>
                                            <asp:HiddenField runat="server" ID="hiddenDealCategory" Value="[]" />
                                            <tr style="display: none;"><%//for js moveCategoryitem don't remove %>
                                                <td class="Title">分類：<br />
                                                    <asp:Repeater runat="server" ID="dealCategoryArea" OnItemDataBound="dealCategoryArea_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table class="categoryArea" id="dealCategoryTable">
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <%# Container.ItemIndex % 5 == 0 ? "<tr>" : "" %>
                                                            <td style="width:20%;">
                                                                <ul id="dealCategorytree<%#((Category)Container.DataItem).Id %>">
                                                                    <li><input type="checkbox" id="dealCategory<%#((Category)Container.DataItem).Id %>" onchange="OnDealCategoryChange()" disabled="disabled" />
                                                                        <label id="dealCategoryDeas<%#((Category)Container.DataItem).Id %>" style="color: gray;"
                                                                        onclick="$('#dealCategory<%#((Category)Container.DataItem).Id %>').click()">
                                                                        <%# ((Category)Container.DataItem).NameInConsole %>
                                                                        </label>
                                                                        <asp:Repeater runat="server" ID="subDealCategoryArea">
                                                                            <HeaderTemplate>
                                                                                <ul class="subDealCategoryArea">
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <li><input type="checkbox" id="dealCategory<%#((Category)Container.DataItem).Id %>"
                                                                                    onchange="OnDealCategoryChange()"  disabled="disabled"/>
                                                                                <label onclick="$('#dealCategory<%#((Category)Container.DataItem).Id %>').click();">
                                                                                <%# ((Category)Container.DataItem).NameInConsole %>
                                                                                </label>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                </ul>   
                                                                            </FooterTemplate>
                                                                        </asp:Repeater>
                                                                </ul>
                                                            </td>
                                                            <%# (Container.ItemIndex % 5 == 4 || Container.ItemIndex == ((IList)((Repeater)Container.Parent).DataSource).Count-1) ? "</tr>" : "" %>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                            <%-- DealCategory regionend --%>
                                            <tr>
                                                <td class="Title">其它設定：<br />
                                                    <asp:CheckBox ID="cbxAppLimitedEdition" Text="只限使用APP購買" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title">
                                                    <font style="color: Red; font-weight: bold">*</font>負責業務<font style="font-size: smaller">(請填寫中文名)</font>：<asp:TextBox
                                                        ID="txtSalesName" runat="server" Width="100px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSalesName"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                    &nbsp;&nbsp; <font style="color: Red; font-weight: bold">*</font>業務電話：<asp:TextBox
                                                        ID="txtSalesPhone" runat="server" Width="180px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSalesPhone"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title">
                                                    <font style="color: Red; font-weight: bold">*</font>搜尋關鍵字：
                                                    <asp:TextBox ID="txtKeywords" runat="server" placeholder="ex：義大/義大天悅/買新鮮" MaxLength="30" onkeyup="checkKeywords();" onblur="checkKeywords();" ClientIDMode="Static"></asp:TextBox>
                                                    <font style="font-size: smaller; font-weight: 100;">(僅限輸入至多3個字詞，並以"/"分隔)</font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title" style="vertical-align: top">
                                                    <b>建議文案重點：</b>請表列三點 ( 最多三點，依據重要性條列式填寫或提供相關網址 )<br />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span
                                                        style="font-size: smaller"> EX:強調餐點份量多、主廚得過獎、網友推薦/新聞報導</span><br />
                                                    <font style="color: Red; font-weight: bold">*</font>1.&nbsp;<asp:TextBox ID="txtImportant1"
                                                        runat="server" Width="750px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtImportant1"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator><br />
                                                    <font style="color: Red; font-weight: bold">*</font>2.&nbsp;<asp:TextBox ID="txtImportant2"
                                                        runat="server" Width="750px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ControlToValidate="txtImportant2"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator><br />
                                                    <font style="color: Red; font-weight: bold">*</font>3.&nbsp;<asp:TextBox ID="txtImportant3"
                                                        runat="server" Width="750px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator35" runat="server" ControlToValidate="txtImportant3"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator><br />
                                                    &nbsp;4.&nbsp;<asp:TextBox ID="txtImportant4" runat="server" Width="743px" TextMode="MultiLine"
                                                        Height="60px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title">照片來源：
                                                    <asp:CheckBoxList ID="chklPictureSource" runat="server" RepeatDirection="Horizontal"
                                                        RepeatLayout="Flow">
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title">行銷活動：<span style="color: red; font-size: small">(欲配合行銷活動，請註明行銷活動日期及活動名稱)</span>
                                                    <asp:TextBox ID="txtMarketEvent" runat="server" TextMode="MultiLine" Width="780px" Height="60px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td class="Title">名單來源：<asp:CheckBoxList ID="chklSourceType" runat="server" RepeatDirection="Horizontal"
                                                    RepeatLayout="Flow">
                                                    <asp:ListItem Text="自行開發" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="主動來電名單" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="競爭平台名單" Value="2"></asp:ListItem>
                                                </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                            <tr class="Ppon">
                                                <td class="Title">核銷方式：
                                                    <ul style="list-style-type: none; padding-left: 20px;">
                                                        <li>
                                                            <asp:RadioButton ID="rdlOnlineUse" runat="server" Text="即買即用" GroupName="VerifyType"
                                                                onclick="OnlineUse(this);" /><br />
                                                            <div style="padding-left: 20px; font-size: 12px; padding-right: 20px; color: Gray; font-weight: normal;">
                                                                <%= LunchKingSite.I18N.Message.SalesBusinessOrderPponVerifyTypeOnlineUse %>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <asp:RadioButton ID="rdlOnlineOnly" runat="server" Text="線上核銷" GroupName="VerifyType"
                                                                onclick="OnlineUse(this);"
                                                                Checked="true" /><br />
                                                            <div id="OnlineOnlyText" style="padding-left: 20px; font-size: 12px; padding-right: 20px; color: Gray; font-weight: normal;">
                                                                <%= LunchKingSite.I18N.Message.SalesBusinessOrderPponVerifyTypeOnlineOnly %>
                                                            </div>
                                                            <span style="display: none">
                                                                <asp:TextBox ID="hidOnlineOnly" runat="server"></asp:TextBox></span> </li>
                                                        <li>
                                                            <asp:RadioButton ID="rdlInventoryOnly" runat="server" Text="紙本清冊搭配專人核銷" GroupName="VerifyType"
                                                                onclick="OnlineUse(this);" Enabled="false" /><br />
                                                            <div style="padding-left: 20px; font-size: 12px; padding-right: 20px; color: Gray; font-weight: normal;">
                                                                <%= LunchKingSite.I18N.Message.SalesBusinessOrderPponVerifyTypeInventoryOnly %>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr style="display: none">
                                                <td>
                                                    <b>刷卡機資訊：</b>
                                                    <ul style="list-style-type: decimal; padding-left: 60px">
                                                        <li>店內是否有刷卡機：
                                                            <asp:RadioButtonList ID="rdlIsEDC" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="有安裝" Value="1" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="無安裝" Value="0"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                            &nbsp;&nbsp;&nbsp; <span id="divIsKnowEDC" style="display: none;">(<asp:CheckBox
                                                                ID="chkIsKownEDC" runat="server" Text="有裝設刷卡機意願" />) </span></li>
                                                        <li id="liIsEDC" style="display: none;">是否想了解台新銀行刷卡機之優惠：
                                                            <asp:CheckBox ID="chkIsTaiShinEDC" runat="server" Text="是" />
                                                            <span id="liIsTaiShinEDC" style="display: none;">
                                                                <ul style="list-style-type: lower-latin; padding-left: 40px">
                                                                    <li>每月平均營業額：<asp:TextBox ID="txtEdcDesc1" runat="server" Width="200px"></asp:TextBox>
                                                                    </li>
                                                                    <li>每月平均刷卡量：<asp:TextBox ID="txtEdcDesc2" runat="server" Width="200px"></asp:TextBox>
                                                                    </li>
                                                                    <li>原配合銀行：<asp:TextBox ID="txtEdcDesc3" runat="server" Width="200px"></asp:TextBox>
                                                                    </li>
                                                                    <li>原配合銀行手續費：<asp:TextBox ID="txtEdcDesc4" runat="server" Width="200px"></asp:TextBox>
                                                                    </li>
                                                                    <li>方便聯絡時間/聯絡人：<asp:TextBox ID="txtEdcDesc5" runat="server" Width="200px"></asp:TextBox>
                                                                    </li>
                                                                </ul>
                                                            </span></li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr style="display: none">
                                                <td>
                                                    <b>行銷資源申請：</b>
                                                    <asp:RadioButtonList ID="rdlIsApplyNewPromotion" runat="server" RepeatLayout="Flow"
                                                        RepeatDirection="Horizontal" Style="display: none">
                                                        <asp:ListItem Text="舊制" Value="False"></asp:ListItem>
                                                        <asp:ListItem Text="新制" Value="True" Selected="True"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <span id="divApplyNewPromotion" style="display: none;">
                                                        <asp:RadioButtonList ID="rdlApplyNewPromotion" runat="server" RepeatLayout="Flow"
                                                            RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="符合申請資格" Value="True"></asp:ListItem>
                                                            <asp:ListItem Text="不符合申請資格" Value="False" Selected="True"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="ShowDiv1" class="ShowDivClass">
                                        <table>
                                            <tr>
                                                <td class="Title" style="font-weight: bold;">【基本資料】
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title">
                                                    <font style="color: Red; font-weight: bold">*</font>品牌名稱：
                                                    <asp:TextBox ID="txtSellerName" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtSellerName"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title">
                                                    <font style="color: Red; font-weight: bold">*</font>簽約公司名稱：
                                                    <asp:TextBox ID="txtCompanyName" runat="server" onkeyup="ChangeInvoiceFounder();"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtCompanyName"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                    &nbsp; 私人簽約：
                                                    <asp:RadioButtonList ID="rdlPrivateContract" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                                        <asp:ListItem Text="是" Value="True" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="否" Value="False"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title">
                                                    <span id="CompanyContract" style="display: none"><font style="color: Red; font-weight: bold">*</font>
                                                    </span>統一編號：
                                                    <asp:TextBox ID="txtGuiNumber" runat="server" MaxLength="8" onkeyup="ChangeInvoiceFounder();"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title">
                                                    <font style="color: Red; font-weight: bold">*</font>負責人：<asp:TextBox ID="txtBossName"
                                                        runat="server" Width="90px" onkeyup="ChangeInvoiceFounder();"></asp:TextBox>&nbsp;&nbsp;<span
                                                            style="font-size: small">需同公司登記</span>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtBossName"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                    &nbsp;&nbsp;&nbsp; <span id="PrivateContract" style="display: none"><font style="color: Red; font-weight: bold">*</font></span>身分證字號：
                                                    <asp:TextBox ID="txtPersonalId" runat="server" MaxLength="10" onkeyup="ChangeInvoiceFounder();"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title">
                                                    <font style="color: Red; font-weight: bold">*</font>聯絡人：
                                                    <asp:TextBox ID="txtContactPerson" runat="server" Width="90px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtContactPerson"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                    &nbsp;&nbsp;&nbsp; <font style="color: Red; font-weight: bold">*</font>行動電話：
                                                    <asp:TextBox ID="txtBossTel" runat="server" Width="180px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtBossTel"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title">連絡電話：
                                                    <asp:TextBox ID="txtBossPhone" runat="server" Width="180px" placeholder="(區碼)12345678"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title">
                                                    <font style="color: Red; font-weight: bold">*</font>電子信箱：
                                                    <asp:TextBox ID="txtBossEmail" runat="server" Width="300px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtBossEmail"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                    &nbsp;&nbsp;&nbsp; 傳真電話：
                                                    <asp:TextBox ID="txtBossFax" runat="server" placeholder="(區碼)12345678"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title">
                                                    <font style="color: Red; font-weight: bold">*</font>簽約公司地址：
                                                    <asp:TextBox ID="txtBossAddress" runat="server" Width="400px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtBossAddress"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr class="Product">
                                                <td class="Title">退換貨連絡人：
                                                    <asp:TextBox ID="txtReturnedPerson" runat="server" Width="90px"></asp:TextBox>
                                                    &nbsp;&nbsp;&nbsp; 退換貨連絡人電話：
                                                    <asp:TextBox ID="txtReturnedTel" runat="server" Width="180px" placeholder="(區碼)12345678"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="Product">
                                                <td class="Title">
                                                    <font style="color: Red; font-weight: bold">*</font>退換貨連絡電子信箱：
                                                    <asp:TextBox ID="txtReturnedEmail" runat="server" Width="300px"></asp:TextBox>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <asp:CheckBox ID="chkSyncContactPerson" runat="server" Text="同連絡人電子信箱" Font-Size="Smaller" />
                                                </td>
                                            </tr>
                                            <tr class="Travel">
                                                <td class="Title">民宿業者須註明以下資訊：
                                                    <ul style="list-style-type: decimal; padding-left: 60px">
                                                        <li>民宿登記證字號：<asp:TextBox ID="txtHotelId" runat="server"></asp:TextBox>
                                                        </li>
                                                        <li>所在地警察局電話：<asp:TextBox ID="txtPoliceTel" runat="server" placeholder="(區碼)12345678"></asp:TextBox>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title">店家訊息：<br />
                                                    <ul style="list-style-type: upper-alpha; padding-left: 40px">
                                                        <li>網站資訊：<br />
                                                            <table>
                                                                <tr>
                                                                    <td style="width: 60px; padding: 0;">官網：
                                                                    </td>
                                                                    <td style="padding: 0;">
                                                                        <asp:TextBox ID="txtWebUrl" runat="server" Width="500px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60px; padding: 0;">FaceBook：
                                                                    </td>
                                                                    <td style="padding: 0;">
                                                                        <asp:TextBox ID="txtFBUrl" runat="server" Width="500px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60px; padding: 0;">Plurk：
                                                                    </td>
                                                                    <td style="padding: 0;">
                                                                        <asp:TextBox ID="txtPlurkUrl" runat="server" Width="500px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60px; padding: 0; vertical-align: top">部落格：
                                                                    </td>
                                                                    <td style="padding: 0;">
                                                                        <asp:TextBox ID="txtBlogUrl" runat="server" Width="500px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60px; padding: 0; vertical-align: top">其他網址：
                                                                    </td>
                                                                    <td style="padding: 0;">
                                                                        <asp:TextBox ID="txtOtherUrl" runat="server" Width="500px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </li>
                                                        <li><font style="color: Red; font-weight: bold">*</font>店名：<asp:TextBox ID="txtStoreName"
                                                            runat="server" Width="500px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator28"
                                                                runat="server" ControlToValidate="txtStoreName" ValidationGroup="Update" Display="Dynamic"
                                                                ErrorMessage="Error!!"></asp:RequiredFieldValidator></li>
                                                        <li><font style="color: Red; font-weight: bold">*</font>店址：
                                                            <asp:HiddenField ID="hidCityJSonData" runat="server" />
                                                            <asp:HiddenField ID="hidAddressCityId" runat="server" />
                                                            <asp:HiddenField ID="hidAddressTownshipId" runat="server" />
                                                            <asp:Repeater ID="repCity" runat="server">
                                                                <HeaderTemplate>
                                                                    <select name="selCity" id="selCity">
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <option value="<%#Eval("Id")%>" <%# ((City)Container.DataItem).Id == AddressCityId ? "selected='selected'" : "" %>>
                                                                        <%#Eval("CityName")%></option>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </select>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                            <asp:Repeater ID="repTown" runat="server">
                                                                <HeaderTemplate>
                                                                    <select name="selTownship" id="selTownship">
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <option value="<%#Eval("Id")%>" <%#((City)Container.DataItem).Id == AddressTownshipId ? "selected='selected'" : "" %>>
                                                                        <%#Eval("CityName")%></option>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </select>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                            <asp:TextBox ID="txtSellerAddress" runat="server" Width="350px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                                                                ID="RequiredFieldValidator29" runat="server" ControlToValidate="txtSellerAddress"
                                                                ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                        </li>
                                                        <li class="Travel">訂房電話：<asp:TextBox ID="txtHotelReservationTel" runat="server" Width="500px"
                                                            placeholder="(區碼)12345678"></asp:TextBox></li>
                                                        <li class="Ppon NoTravel">訂位電話：<asp:TextBox ID="txtReservationTel" runat="server"
                                                            Width="500px" placeholder="(區碼)12345678"></asp:TextBox></li>
                                                        <li class="Ppon">營業時間：<asp:TextBox ID="txtOpeningTime" runat="server" Width="500px"></asp:TextBox><span><span
                                                            onclick="ShowExample(this);" style="cursor: pointer; font-size: small; color: Red;">&nbsp;(點我看範例)</span>
                                                            <span style="font-size: small; color: Red; display: none">
                                                                <table>
                                                                    <tr>
                                                                        <td>填寫方式：週一~週五09:00~23:59；週六10:00~03:00
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </span></span></li>
                                                        <li class="Ppon">公休日：<asp:TextBox ID="txtCloseDate" runat="server" Width="500px"></asp:TextBox>
                                                        </li>
                                                        <li>交通資訊：<br />
                                                            <table>
                                                                <tr>
                                                                    <td style="width: 60px; padding: 0;">捷運：
                                                                    </td>
                                                                    <td style="padding: 0;">
                                                                        <asp:TextBox ID="txtMrt" runat="server" Width="500px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60px; padding: 0;">開車：
                                                                    </td>
                                                                    <td style="padding: 0;">
                                                                        <asp:TextBox ID="txtCar" runat="server" Width="500px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60px; padding: 0;">公車：
                                                                    </td>
                                                                    <td style="padding: 0;">
                                                                        <asp:TextBox ID="txtBus" runat="server" Width="500px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 60px; padding: 0; vertical-align: top">其他：
                                                                    </td>
                                                                    <td style="padding: 0;">
                                                                        <asp:TextBox ID="txtVehicleInfo" runat="server" Width="500px" TextMode="MultiLine"
                                                                            Height="60px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </li>
                                                        <li>
                                                            <asp:CheckBox ID="chkIsShowSpecailStore" runat="server" Text="特選店家不露出" /></li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                        <table>
                                            <tr>
                                                <td class="Title">
                                                    <br />
                                                    <h2>
                                                        <span style="font-weight: bold">【分店資訊】</span></h2>
                                                    <asp:Panel ID="divStoreAdd1" runat="server">
                                                        <asp:Label ID="lblUploadMsg" runat="server" ForeColor="Red" Font-Size="Small">*註: 使用Excel匯入請遵照範本格式填寫，不然資料可能會匯入不完全喔!!</asp:Label><br />
                                                        [方式一].Excel匯入：
                                                        <asp:FileUpload ID="FileUpload1" runat="server" />
                                                        <asp:Button ID="btnImport" runat="server" Text="匯入" OnClick="btnImport_Click" />&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="lkStoreExampleDownload" runat="server" ForeColor="Blue" Text="Excel範本下載"
                                                            OnClick="lkStoreExampleDownload_Click"></asp:LinkButton>
                                                    </asp:Panel>
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" OnLoad="UpdatePanel1_Load">
                                                        <ContentTemplate>
                                                            <asp:Panel ID="divStoreAdd2" runat="server">
                                                                [方式二].逐筆手動新增：<asp:Button ID="btnStoreAdd" runat="server" Text="新增分店資訊" CausesValidation="false" Visible="false" />
                                                                <input type="button" value="查看分店列表" onclick="OpenStoreWindow();" />
                                                                <br />
                                                            </asp:Panel>
                                                            <asp:GridView ID="gvStoreList" runat="server" OnRowDataBound="gvStoreList_RowDataBound"
                                                                OnRowCommand="gvStoreList_RowCommand" GridLines="Horizontal" ForeColor="Black"
                                                                CellPadding="4" BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE" BackColor="White"
                                                                EmptyDataText="無符合的資料" AutoGenerateColumns="false" Font-Size="Small" EnableViewState="False"
                                                                Width="800px">
                                                                <FooterStyle BackColor="#CCCC99"></FooterStyle>
                                                                <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
                                                                <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                                                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
                                                                <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                                                                    HorizontalAlign="Center"></HeaderStyle>
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="分店名稱">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblStoreName" runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="200px" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="數量">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField HeaderText="分店電話" DataField="StoreTel" ItemStyle-HorizontalAlign="Center"
                                                                        ItemStyle-Width="170px" />
                                                                    <asp:TemplateField HeaderText="地址">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblAddress" runat="server"></asp:Label>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" Width="370px" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <asp:Button ID="btnDelete" runat="server" Text="刪除" CommandArgument="<%# ((SalesStore)Container.DataItem).Id%>"
                                                                                CommandName="DEL" Visible="false" />
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="btnStoreAdd" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <table>
                                            <tr>
                                                <td class="Title" style="font-weight: bold;" colspan="2">
                                                    <h2>
                                                        <b>【財務資料】</b></h2>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title" colspan="2">
                                                    <font style="color: Red; font-weight: bold">*</font>帳務連絡人：
                                                    <asp:TextBox ID="txtAccountingName" runat="server" Width="90px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtAccountingName"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <font style="color: Red; font-weight: bold">*</font>帳務連絡人電話：
                                                    <asp:TextBox ID="txtAccountingTel" runat="server" Width="180px" placeholder="(區碼)12345678"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtAccountingTel"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                    <asp:CheckBox ID="chkSyncUser" runat="server" Text="同基本資料負責人" Font-Size="Smaller" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title" colspan="2">
                                                    <font style="color: Red; font-weight: bold">*</font>帳務聯絡人電子信箱：
                                                    <asp:TextBox ID="txtAccountingEmail" runat="server" Width="300px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtAccountingEmail"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title" colspan="2">
                                                    <asp:DropDownList ID="ddlAccountingBank" runat="server" AppendDataBoundItems="true"
                                                        onchange="BindBankInfo(true);">
                                                        <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <select id="ddlAccountingBranch" runat="server" onchange="BindBranchInfo();"></select>
                                                    <br />
                                                    <font style="color: Red; font-weight: bold">*</font>匯款銀行：
                                                            <asp:TextBox ID="txtAccountingBankId" runat="server" Width="50px"></asp:TextBox>
                                                    <asp:TextBox ID="txtAccountingBankDesc" runat="server" Width="200px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtAccountingBankId"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>&nbsp;
                                                    <font style="color: Red; font-weight: bold">*</font>分 行：
                                                            <asp:TextBox ID="txtAccountingBranchId" runat="server" Width="50px"></asp:TextBox>
                                                    <asp:TextBox ID="txtAccountingBranchDesc" runat="server" Width="260px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="txtAccountingBranchId"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title" colspan="2">
                                                    <font style="color: Red; font-weight: bold">*</font>統一編號／身份證字號：
                                                    <asp:TextBox ID="txtAccountingId" runat="server" MaxLength="10"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ControlToValidate="txtAccountingId"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                    <br />
                                                    <span style="font-size: smaller;">*註:匯款戶名為公司，即填統一編號；匯款戶名為個人，則填個人身份證字號。</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title" colspan="2">
                                                    <font style="color: Red; font-weight: bold">*</font>戶 名：
                                                    <asp:TextBox ID="txtAccounting" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ControlToValidate="txtAccounting"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title" colspan="2">
                                                    <font style="color: Red; font-weight: bold">*</font>帳 號：
                                                    <asp:TextBox ID="txtAccount" runat="server" Width="300px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ControlToValidate="txtAccount"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title" colspan="2"><font style="color: Red; font-weight: bold">*</font>匯款資料：
                                                    <asp:RadioButtonList ID="rdlDealAccountingPayType" runat="server" RepeatLayout="Flow"
                                                        RepeatDirection="Horizontal">
                                                    </asp:RadioButtonList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ControlToValidate="rdlDealAccountingPayType"
                                                        ValidationGroup="Update" Display="Dynamic" ErrorMessage="Error!!"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title" colspan="2" style="color: Blue">
                                                    <asp:Panel ID="divRittanceInfo" runat="server">
                                                        <asp:Label ID="lblRemittance" runat="server" Text="出帳方式："></asp:Label>
                                                        <asp:Label ID="lblRemittanceTypeDesc" runat="server" Visible="false"></asp:Label>
                                                        <span id="spanRemittanceType" runat="server" style="display: none">
                                                            <asp:RadioButton ID="rbAchWeekly" GroupName="achType" runat="server" Text="ACH每週出帳" />
                                                            <asp:RadioButton ID="rbManualWeekly" GroupName="achType" runat="server" Text="人工每週出帳"/>
                                                            <asp:RadioButton ID="rbManualMonthly" GroupName="achType" runat="server" Text="人工每月出帳" />
                                                            <asp:RadioButton ID="rbPartially" GroupName="achType" runat="server" Text="商品(暫付70%)" />
                                                            <asp:RadioButton ID="rbOthers" GroupName="achType" runat="server" Text="其他付款方式" /><br/>
                                                            <span runat="server" style="padding-left: 86px">
                                                                <asp:RadioButton ID="rbWeekly" GroupName="achType" runat="server" Text="新每週出帳" />
                                                                <asp:RadioButton ID="rbMonthly" GroupName="achType" runat="server" Text="新每月出帳" />
                                                                <asp:RadioButton ID="rbFlexible" GroupName="achType" runat="server" Text="彈性選擇出帳" />
                                                            </span>
                                                        </span>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title" colspan="2" style="display: none;">代收轉付設定：
                                                    <asp:CheckBox ID="chkIsEntrustSell" runat="server" Text="設定為代收轉付" />
                                                    (<asp:RadioButton ID="rbHwatai" runat="server" GroupName="EntrustSell" Text="信託華泰銀行" />
                                                    <asp:RadioButton ID="rbIsMohist" runat="server" GroupName="EntrustSell" Text="配合墨攻核銷" />)
                                                    &nbsp;&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="Title" colspan="2">可提供憑證：(單選，如有開立統一發票之公司，不可以收據開立請款)<br />
                                                    <div id="Invoice" style="padding-left: 30px">
                                                        <asp:RadioButton ID="rdlDuplicate" runat="server" Text="三聯式統一發票" onclick="InTaxCheck();"
                                                            GroupName="Invoice" />
                                                        (<asp:RadioButtonList ID="rdlInTax" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal"
                                                            onclick="InTaxCheck();">
                                                        </asp:RadioButtonList>
                                                        )
                                                        <br />
                                                        <asp:RadioButton ID="rdlEntertainmentTax" runat="server" Text="已含18%娛樂稅，不開立發票" Style="display: none" />
                                                        <asp:RadioButton ID="rdlGeneral" runat="server" Text="收銀機統一發票" onclick="InvoiceCheck();"
                                                            GroupName="Invoice" /><br />
                                                        <asp:RadioButton ID="rdlNonInvoice" runat="server" Text="免用統一發票收據" onclick="InvoiceCheck();"
                                                            GroupName="Invoice" /><br />
                                                        <asp:RadioButton ID="rdlInvoiceOther" runat="server" Text="其它" onclick="InvoiceCheck();"
                                                            GroupName="Invoice" />
                                                        <asp:TextBox ID="txtInvoiceOtherDesc" runat="server" Width="200px"></asp:TextBox><br />
                                                        <span style="font-size: smaller">*提供之其他憑證請說明完整，例如：印花稅收據(通常是診所收據或補習班收據)、 農 (漁) 民出售農(漁)產物收據……等其中，印花稅收據上是要貼上印花稅票</span><br />
                                                        <span style="color: Red; font-size: smaller">附 註：滙款銀行戶名需與簽約公司名稱相同，若由個人與我們簽約請提供簽約人的帳戶</span>
                                                    </div>
                                                    <div class="Product">
                                                        運費代收單位：
                                                        <asp:RadioButtonList ID="rdlFreightType" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="合作店家" Value="0" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="公司配合之貨運公司" Value="1"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                        <span id="FreightCompany">
                                                            <asp:CheckBoxList ID="chklFreightCompany" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="新竹物流" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="郵局" Value="1"></asp:ListItem>
                                                            </asp:CheckBoxList>
                                                        </span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><font style="color: Red; font-weight: bold">*</font>發票開立人：
                                                    <asp:RadioButtonList ID="rdlInvoiceFounder" runat="server" onchange="ChangeInvoiceFounder();"
                                                        RepeatLayout="Flow" RepeatDirection="Horizontal">
                                                    </asp:RadioButtonList>
                                                    <br />
                                                    <table style="padding-left: 30px">
                                                        <tr>
                                                            <td style="width: 100px"><font style="color: Red; font-weight: bold">*</font>公司名稱：
                                                            </td>
                                                            <td>
                                                                <label id="lblInvoiceFounderCompanyName"></label>
                                                                <asp:TextBox ID="txtInvoiceFounderCompanyName" runat="server" ClientIDMode="Static"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100px"><font style="color: Red; font-weight: bold">*</font>統編 / ID：
                                                            </td>
                                                            <td>
                                                                <label id="lblInvoiceFounderCompanyID"></label>
                                                                <asp:TextBox ID="txtInvoiceFounderCompanyID" runat="server" ClientIDMode="Static"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100px"><font style="color: Red; font-weight: bold">*</font>負責人：
                                                            </td>
                                                            <td>
                                                                <label id="lblInvoiceFounderBossName"></label>
                                                                <asp:TextBox ID="txtInvoiceFounderBossName" runat="server" ClientIDMode="Static"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="ShowDiv2" class="ShowDivClass" style="border-bottom-width: 1px;">
                                        <table>
                                            <tr>
                                                <td class="Title" style="font-weight: bold;" colspan="2">【好康優惠內容】
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <ul class="ulShowDiv" style="list-style-type: decimal; padding-left: 40px">
                                                        <li style="padding-bottom: 10px; display: none;">本檔次狀況：
                                                            <asp:DropDownList ID="ddlMultiDeal" runat="server">
                                                            </asp:DropDownList>
                                                        </li>
                                                        <li style="padding-bottom: 10px">優惠條件內容使用說明：(如有多檔次 請類推)<br />
                                                            <asp:HiddenField ID="hdDeals" runat="server" />
                                                            <ul id="ulDeal" style="list-style-type: upper-alpha; padding-left: 40px;">
                                                                <asp:Repeater ID="repDeal" runat="server">
                                                                    <ItemTemplate>
                                                                        <li id="liDeal" style="display: none;">
                                                                            <ul style="list-style-type: decimal; padding-left: 20px">
                                                                                <br />
                                                                                <li><font style="color: Red; font-weight: bold">*</font>賣價：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    $<input id="txtItemPrice" type="text" style="width: 60px" value="<%# ((Deal)Container.DataItem).ItemPrice %>" />&nbsp;&nbsp;&nbsp;<span
                                                                                        class="Product"><input id="chkIsIncludeFreight" class="IsIncludeFreight" type="checkbox"
                                                                                            onclick="SetFreightDisabled(this);"
                                                                                            <%# (((Deal)Container.DataItem).IsIncludeFreight) ? "checked=checked" : ""%> />此價格已含運</span>
                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font style="color: Red; font-weight: bold">*</font>原價：&nbsp;&nbsp;&nbsp; $<input id="txtOrignPrice" type="text"
                                                                                        style="width: 60px" value="<%# ((Deal)Container.DataItem).OrignPrice %>" /><br />
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td style="vertical-align: top;">
                                                                                                <font style="color: Red; font-weight: bold">*</font>優惠活動：
                                                                                            </td>
                                                                                            <td>
                                                                                                <textarea id="txtItemName" cols="20" style="height: 80px; width: 540px;"><%# ((Deal)Container.DataItem).ItemName.Equals(string.Empty) ? "只要XXXX元 + (多人說明) + 即可享用【店名】+ 原價XXXX元 + 商品名稱" : ((Deal)Container.DataItem).ItemName %></textarea>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="vertical-align: top;">款式選項：
                                                                                            </td>
                                                                                            <td>
                                                                                                <input id="chkIsReferenceAttachment" type="checkbox" <%# (((Deal)Container.DataItem).IsReferenceAttachment) ? "checked=checked" : ""%> />請參見附件<br />
                                                                                                <textarea id="txtMultOption" cols="20" style="height: 80px; width: 540px;"><%# ((Deal)Container.DataItem).MultOption%></textarea><br />
                                                                                                <span><span onclick="ShowExample(this);" style="cursor: pointer; font-size: small; color: Red;">(點我看範例)</span> <span style="font-size: small; color: Red; display: none">
                                                                                                    <table>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <fieldset style="font-size: 10px; width: 200px; line-height: 20px;">
                                                                                                                    <legend>有數量限制</legend>
                                                                                                                    款式選擇:<br />
                                                                                                                    [50] 綠豆<br />
                                                                                                                    [50] 薏仁<br />
                                                                                                                    [50] 紅豆<br />
                                                                                                                </fieldset>
                                                                                                            </td>
                                                                                                            <td valign="top">
                                                                                                                <fieldset style="font-size: 10px; width: 200px; line-height: 20px;">
                                                                                                                    <legend>無數量限制</legend>
                                                                                                                    尺寸選擇:<br />
                                                                                                                    大顆<br />
                                                                                                                    小顆<br />
                                                                                                                </fieldset>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </span></span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </li>
                                                                                <li><span class="NoPrint"><font style="color: Red; font-weight: bold">*</font>最低門檻數量：
                                                                                    <input id="txtMinQuentity" class="OnlineUseLimit" type="text" value="<%# ((Deal)Container.DataItem).MinQuentity %>"
                                                                                        style="width: 80px;" />
                                                                                    &nbsp;單位；<br />
                                                                                </span><font style="color: Red; font-weight: bold">*</font>最高購買數量：
                                                                                    <input id="txtMaxQuentity" type="text" value="<%# ((Deal)Container.DataItem).MaxQuentity %>"
                                                                                        style="width: 220px;" placeholder="請填寫本優惠內容的最高銷售份數" onkeyup="return ValidateNumber($(this),value);" onblur="return atmMaximumAutoFill(this);"
                                                                                        maxlength="5" />
                                                                                    &nbsp;單位；<br />
                                                                                    <font style="color: Red; font-weight: bold">*</font>每人限購：
                                                                                    <input id="txtLimitPerQuentity" type="text" value="<%# ((Deal)Container.DataItem).LimitPerQuentity %>"
                                                                                        style="width: 220px;" placeholder="請填寫每人每次最高可購買的份數" onkeyup="return ValidateNumber($(this),value);"
                                                                                        maxlength="5" />
                                                                                    &nbsp;張數</li>
                                                                                <li class="Ppon"><font style="color: Red; font-weight: bold">*</font>活動使用有效時間：<br />
                                                                                    活動使用有效時間為活動開始兌換後
                                                                                    <input id="txtStartMonth" type="text" value="<%# ((Deal)Container.DataItem).StartMonth %>"
                                                                                        style="width: 80px;" />
                                                                                    <select id="ddlStartUseUnit" onchange="SetDefaultData(this);">
                                                                                        <option <%# (((int)((Deal)Container.DataItem).StartUseUnit) == 0) ? "selected=selected" : ""%>
                                                                                            value="0">天</option>
                                                                                        <option <%# (((int)((Deal)Container.DataItem).StartUseUnit) == 1) ? "selected=selected" : ""%>
                                                                                            value="1">月</option>
                                                                                        <option <%# (((int)((Deal)Container.DataItem).StartUseUnit) == 2) ? "selected=selected" : ""%>
                                                                                            value="2">前</option>
                                                                                    </select>
                                                                                    &nbsp;皆可使用<br />
                                                                                    <input id="chkIsRestrictionRule" onclick="SetRestrictionRuleVisible(this);" type="checkbox"
                                                                                        <%# (((Deal)Container.DataItem).IsRestrictionRule) ? "checked=checked" : ""%> />特殊使用日期限制：
                                                                                    <input id="txtRestrictionRuleDesc" style="display: none; width: 360px" type="text"
                                                                                        placeholder="清明節、端午節不適用" value="<%# ((Deal)Container.DataItem).RestrictionRuleDesc %>" />
                                                                                </li>
                                                                                <li class="Product FreightDisabled"><font style="color: Red; font-weight: bold">*</font>
                                                                                    運費
                                                                                    <input id="txtFreight" type="text" value="<%# ((Deal)Container.DataItem).Freight %>"
                                                                                        style="width: 360px;" />
                                                                                    元整 ；
                                                                                    <input id="txtNonFreightLimit" type="text" value="<%# ((Deal)Container.DataItem).NonFreightLimit %>"
                                                                                        style="width: 80px;" />
                                                                                    份免運&nbsp;<span id="freightExample" onclick="ShowExample(this);" style="cursor: pointer; font-size: small; color: Red; display: none;">(點我看範例)</span> <span style="font-size: small; color: Red; display: none">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>填寫方式：<br />
                                                                                                    階梯式運費：1~10份,100元;11~19份,105元<br />
                                                                                                    一般運費：100元<br />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </span>
                                                                                    <br />
                                                                                    <input id="chkIsFreightToCollect" type="checkbox" <%# (((Deal)Container.DataItem).IsFreightToCollect) ? "checked=checked" : ""%>
                                                                                        style="display: none;" />
                                                                                    <input id="chkIsNotDeliveryIslands" type="checkbox" <%# (((Deal)Container.DataItem).IsNotDeliveryIslands) ? "checked=checked" : ""%> /><span id="txtIsNotDeliveryIslands">無法配送外島</span>
                                                                                </li>
                                                                                <li>
                                                                                    <ul style="list-style-type: lower-latin; padding-left: 20px">
                                                                                        <li class="" style="display: none;">發票對開：
                                                                                            <input id="rdlCommissionNo" type="radio" value="0" onclick="CommissionCheck(this);"
                                                                                                <%# (((Deal)Container.DataItem).Commission == 0) ? "checked=checked" : ""%> />否
                                                                                            <input id="rdlCommissionHas" type="radio" value="1" onclick="CommissionCheck(this);"
                                                                                                <%# (((Deal)Container.DataItem).Commission != 0) ? "checked=checked" : ""%> />是
                                                                                            <span id="CommissionText" style="display: none">，佣金為：$&nbsp;<input type="text" id="txtCommission"
                                                                                                style="width: 80px" onkeyup="return ValidateNumber($(this),value);" value="<%# ((Deal)Container.DataItem).Commission %>" /></span>
                                                                                        </li>
                                                                                        <li>
                                                                                            <input id="ckSlottingFeeDeal" type="checkbox" onclick="isSlottingFeeDeal()" />純上架費檔次
                                                                                        </li>
                                                                                        <li>
                                                                                            上架費份數：
                                                                                            <input id="txtSlottingFeeQuantity" type="text" value="<%# ((Deal)Container.DataItem).SlottingFeeQuantity %>"
                                                                                                style="width: 360px;" />
                                                                                        </li>
                                                                                        <li><font style="color: Red; font-weight: bold">*</font>進貨價格：
                                                                                            <input id="txtPurchasePrice" type="text" value="<%# ((Deal)Container.DataItem).PurchasePrice %>"
                                                                                                style="width: 360px;" />
                                                                                            (含稅)<input id="ckBuyoutCoupon" type="checkbox" <%# (((Deal)Container.DataItem).BuyoutCoupon) ? "checked=checked" : ""%> />買斷票券
                                                                                            <span><span onclick="ShowExample(this);" style="cursor: pointer; font-size: small; color: Red; display: none;">(點我看範例)</span> <span style="font-size: small; color: Red; display: none">
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td>填寫方式：<br />
                                                                                                            階梯式進貨價：1~100份,100元;101~200份,105元<br />
                                                                                                            一般進貨價：100元<br />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </span></span></li>
                                                                                    </ul>
                                                                                </li>
                                                                                <span class="NoPrint">
                                                                                    <li>ATM：
                                                                                        <input id="chkIsAtm" type="checkbox" <%# (((Deal)Container.DataItem).IsATM) ? "checked=checked" : ""%>
                                                                                            class="AtmCheck" onclick="CheckATM(this);" />配合
                                                                                        <span id="spanATM">；保留份數
                                                                                        <input id="txtAtmQuantity" type="text" value="<%# ((Deal)Container.DataItem).AtmQuantity %>"
                                                                                            style="width: 60px;" />
                                                                                            <input id="hidCustomAtmQty" type="hidden" value="-1" />
                                                                                        </span>
                                                                                        &nbsp;&nbsp;&nbsp;<input type="button" id="btnDealClose" value="刪除此檔" onclick="DealClose(this);" />
                                                                                    </li>
                                                                                </span>
                                                                                <li class="Ppon BookingSystemLi" style="padding-bottom: 10px">預約管理：
                                                                                    <label>
                                                                                        <input type="checkbox" class="BookingSystemManager" id="cb_IsUsingbookingSystem"
                                                                                            <%# (((Deal)Container.DataItem).BookingSystemType == (int)BookingType.Coupon) || (((Deal)Container.DataItem).BookingSystemType == (int)BookingType.Travel) ? "checked=checked" : string.Empty %> />配合
                                                                                    </label>
                                                                                    <div class="CouponUsers" style="display: none">
                                                                                        服務類型:
                                                                                        <label>
                                                                                            <input type="radio" id="rdoBookingSystemSeat" name="rdoBookingSystemType<%# Container.ItemIndex %>"
                                                                                                value="1"
                                                                                                <%# (((Deal)Container.DataItem).BookingSystemType == (int)BookingType.Coupon) ? " checked=checked" : string.Empty %> />訂位
                                                                                        </label>
                                                                                        <label>
                                                                                            <input type="radio" id="rdoBookingSystemRoom" name="rdoBookingSystemType<%# Container.ItemIndex %>"
                                                                                                disabled="disabled" value="2"
                                                                                                <%# (((Deal)Container.DataItem).BookingSystemType == (int)BookingType.Travel) ? " checked=checked" : string.Empty %> />訂房
                                                                                        </label>
                                                                                        <br />
                                                                                        <label>
                                                                                            消費者需提早
                                                                                            <input type="text" id="txtAdvanceReservationDays" maxlength="2" style="width: 25px"
                                                                                                value="<%# ((Deal)Container.DataItem).AdvanceReservationDays %>" />
                                                                                            天預約(至少需填1天)
                                                                                        </label>
                                                                                        <br />
                                                                                        <label>
                                                                                            <input type="radio" id="rdoSingleUser" class="SignleUser" name="rdoUsers<%# Container.ItemIndex %>"
                                                                                                <%# ((Deal)Container.DataItem).CouponUsers == 1 ? "checked=checked" : string.Empty %> />
                                                                                            單人優惠(1人使用一張，或無使用限制的檔次)
                                                                                        </label>
                                                                                        <br />
                                                                                        <span>
                                                                                            <label style="display: inline-block">
                                                                                                <input type="radio" id="rdoMultiUsers" class="MultiUsers" name="rdoUsers<%# Container.ItemIndex %>"
                                                                                                    <%# ((Deal)Container.DataItem).CouponUsers > 1 ? "checked=checked" : string.Empty %> />
                                                                                                多人優惠，
                                                                                            </label>
                                                                                            本次優惠為
                                                                                            <input type="text" id="txtCouponUsers" maxlength="2" style="width: 25px" value="<%# ((Deal)Container.DataItem).CouponUsers > 1 ? ((Deal)Container.DataItem).CouponUsers.ToString() : string.Empty %>" />
                                                                                            人享用一張憑證(以最多人數計算，如3~4人套餐，則填4)
                                                                                        </span>
                                                                                    </div>
                                                                                </li>
                                                                                <li class="Ppon Travel" style="padding-bottom: 10px">住宿/活動類憑證鎖定：
                                                                                    <label>
                                                                                        <input type="checkbox" id="cb_IsReserveLock" <%# ((Deal)Container.DataItem).IsReserveLock ? "checked=checked" : string.Empty %> />住宿與活動類憑證，商家系統提供「憑證鎖定」功能
                                                                                    </label>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                <li id="liCloneDeal" style="display: none;">
                                                                    <ul style="list-style-type: decimal; padding-left: 20px">
                                                                        <br />
                                                                        <li><font style="color: Red; font-weight: bold">*</font>賣價：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            $<input id="txtItemPrice" type="text" style="width: 60px" />&nbsp;&nbsp;&nbsp;<span
                                                                                class="Product"><input id="chkIsIncludeFreight" type="checkbox" onclick="SetFreightDisabled(this);" />此價格已含運</span>
                                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font style="color: Red; font-weight: bold">*</font>原價：&nbsp;&nbsp;&nbsp; $<input id="txtOrignPrice" type="text"
                                                                                style="width: 60px" /><br />
                                                                            <table>
                                                                                <tr>
                                                                                    <td style="vertical-align: top;">
                                                                                        <font style="color: Red; font-weight: bold">*</font>優惠活動：
                                                                                    </td>
                                                                                    <td>
                                                                                        <textarea id="txtItemName" cols="20" style="height: 80px; width: 540px;">只要XXXX元 + (多人說明) + 即可享用【店名】+ 原價XXXX元 + 商品名稱</textarea>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="vertical-align: top;">款式選項：
                                                                                    </td>
                                                                                    <td>
                                                                                        <input id="chkIsReferenceAttachment" type="checkbox" />請參見附件<br />
                                                                                        <textarea id="txtMultOption" cols="20" style="height: 80px; width: 540px;"></textarea><br />
                                                                                        <span><span onclick="ShowExample(this);" style="cursor: pointer; font-size: small; color: Red;">(點我看範例)</span> <span style="font-size: small; color: Red; display: none">
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <fieldset style="font-size: 10px; width: 200px; line-height: 20px;">
                                                                                                            <legend>有數量限制</legend>
                                                                                                            款式選擇:<br />
                                                                                                            [50] 綠豆<br />
                                                                                                            [50] 薏仁<br />
                                                                                                            [50] 紅豆<br />
                                                                                                        </fieldset>
                                                                                                    </td>
                                                                                                    <td valign="top">
                                                                                                        <fieldset style="font-size: 10px; width: 200px; line-height: 20px;">
                                                                                                            <legend>無數量限制</legend>
                                                                                                            尺寸選擇:<br />
                                                                                                            大顆<br />
                                                                                                            小顆<br />
                                                                                                        </fieldset>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </span></span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </li>
                                                                        <li><span class="NoPrint"><font style="color: Red; font-weight: bold">*</font>最低門檻數量：
                                                                            <input id="txtMinQuentity" class="OnlineUseLimit" type="text" style="width: 80px;"
                                                                                value="1" />
                                                                            &nbsp;單位；<br />
                                                                        </span><font style="color: Red; font-weight: bold">*</font>最高購買數量：
                                                                            <input id="txtMaxQuentity" type="text" style="width: 220px;" placeholder="請填寫本優惠內容的最高銷售份數"
                                                                                onkeyup="return ValidateNumber($(this),value);" maxlength="5" onblur="return atmMaximumAutoFill(this);" />
                                                                            &nbsp;單位；<br />
                                                                            <font style="color: Red; font-weight: bold">*</font>每人限購：
                                                                            <input id="txtLimitPerQuentity" type="text" style="width: 220px;" placeholder="請填寫每人每次最高可購買的份數"
                                                                                onkeyup="return ValidateNumber($(this),value);" maxlength="5" />
                                                                            &nbsp;張數</li>
                                                                        <li class="Ppon"><font style="color: Red; font-weight: bold">*</font>活動使用有效時間：<br />
                                                                            活動使用有效時間為活動開始兌換後
                                                                            <input id="txtStartMonth" type="text" style="width: 80px;" />
                                                                            <select id="ddlStartUseUnit" onchange="SetDefaultData(this);">
                                                                                <option selected="selected" value="0">天</option>
                                                                                <option value="1">月</option>
                                                                                <option value="2">前</option>
                                                                            </select>
                                                                            &nbsp;皆可使用<br />
                                                                            <input id="chkIsRestrictionRule" type="checkbox" onclick="SetRestrictionRuleVisible(this);" />特殊使用日期限制：
                                                                            <input id="txtRestrictionRuleDesc" type="text" style="display: none; width: 360px"
                                                                                placeholder="清明節、端午節不適用" />
                                                                        </li>
                                                                        <li class="Product FreightDisabled"><font style="color: Red; font-weight: bold">*</font>
                                                                            運費
                                                                            <input id="txtFreight" type="text" style="width: 360px;" />
                                                                            元整 ；
                                                                            <input id="txtNonFreightLimit" type="text" style="width: 80px;" />
                                                                            份免運&nbsp;<span id="freightExample" onclick="ShowExample(this);" style="cursor: pointer; font-size: small; color: Red; display: none;">(點我看範例)</span> <span style="font-size: small; color: Red; display: none">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>填寫方式：<br />
                                                                                            階梯式運費：1~10份,100元;11~19份,105元<br />
                                                                                            一般運費：100元<br />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </span>
                                                                            <br />
                                                                            <input id="chkIsFreightToCollect" type="checkbox" style="display: none;" />
                                                                            <input id="chkIsNotDeliveryIslands" type="checkbox"/><span id="txtIsNotDeliveryIslands">無法配送外島</span> </li>
                                                                        <li>
                                                                            <ul style="list-style-type: lower-latin; padding-left: 20px">
                                                                                <li class="" style="display: none;">發票對開：
                                                                                    <input id="rdlCommissionNo" type="radio" value="0" onclick="CommissionCheck(this);"
                                                                                        checked="checked" />否
                                                                                    <input id="rdlCommissionHas" type="radio" value="1" onclick="CommissionCheck(this);" />是
                                                                                    <span id="CommissionText" style="display: none">，佣金為：$&nbsp;<input type="text" id="txtCommission"
                                                                                        style="width: 80px" onkeyup="return ValidateNumber($(this),value);" value="0" /></span>
                                                                                </li>
                                                                                 <li>
                                                                                    <input id="ckSlottingFeeDeal" type="checkbox" onclick="isSlottingFeeDeal()"/>純上架費檔次
                                                                                </li>
                                                                                <li>
                                                                                    上架費份數：
                                                                                    <input id="txtSlottingFeeQuantity" type="text" style="width: 360px;" />
                                                                                </li>
                                                                                <li><font style="color: Red; font-weight: bold">*</font>進貨價格：
                                                                                    <input id="txtPurchasePrice" type="text" style="width: 360px;" />
                                                                                    (含稅)<input id="ckBuyoutCoupon" type="checkbox" />買斷票券
                                                                                    <span><span onclick="ShowExample(this);" style="cursor: pointer; font-size: small; color: Red; display: none;">(點我看範例)</span> <span style="font-size: small; color: Red; display: none">
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>填寫方式：<br />
                                                                                                    階梯式進貨價：1~100份,100元;101~200份,105元<br />
                                                                                                    一般進貨價：100元<br />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </span></span></li>
                                                                            </ul>
                                                                        </li>
                                                                        <span class="NoPrint">
                                                                            <li>ATM：
                                                                                <input id="chkIsAtm" type="checkbox" class="AtmCheck" onclick="CheckATM(this);" />配合
                                                                                <span id="spanATM">；保留份數
                                                                                <input id="txtAtmQuantity" type="text" style="width: 60px;" /></span>
                                                                                &nbsp;&nbsp;&nbsp;<input type="button" id="btnDealClose" value="刪除此檔" onclick="DealClose(this);" />
                                                                                <input id="hidCustomAtmQty" type="hidden" value="-1" />
                                                                            </li>
                                                                        </span>
                                                                        <li class="Ppon" style="padding-bottom: 10px">預約管理：
                                                                            <label>
                                                                                <input type="checkbox" class="BookingSystemManager" id="cb_IsUsingbookingSystem" />配合
                                                                            </label>
                                                                            <div class="CouponUsers" style="display: none">
                                                                                服務類型:
                                                                                <label>
                                                                                    <input type="radio" id="rdoBookingSystemSeat" checked="checked" value="1" />訂位
                                                                                </label>
                                                                                <label>
                                                                                    <input type="radio" id="rdoBookingSystemRoom" disabled="disabled" value="2" />訂房
                                                                                </label>
                                                                                <label>
                                                                                    消費者需提早
                                                                                    <input type="text" id="txtAdvanceReservationDays" maxlength="2" style="width: 25px"
                                                                                        value="1" />
                                                                                    天預約(至少需填1天)
                                                                                </label>
                                                                                <br />
                                                                                <label>
                                                                                    <input type="radio" id="rdoSingleUser" class="SignleUser" name="rdoUsersAddNew" checked="checked" />
                                                                                    單人優惠(1人使用一張，或無使用限制的檔次)
                                                                                </label>
                                                                                <br />
                                                                                <span>
                                                                                    <label style="display: inline-block">
                                                                                        <input type="radio" id="rdoMultiUsers" class="MultiUsers" name="rdoUsersAddNew" />
                                                                                        多人優惠，
                                                                                    </label>
                                                                                    本次優惠為
                                                                                    <input type="text" id="txtCouponUsers" maxlength="2" style="width: 25px" />
                                                                                    人享用一張憑證(以最多人數計算，如3~4人套餐，則填4)
                                                                                </span>
                                                                            </div>
                                                                        </li>
                                                                        <li class="Ppon Travel" style="padding-bottom: 10px">住宿/活動/展演票券類憑證鎖定：
                                                                            <label>
                                                                                <input type="checkbox" id="cb_IsReserveLock" />住宿/活動/展演票券類憑證，商家系統提供「憑證鎖定」功能
                                                                            </label>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                            <input type="button" id="btnAddDeal" onclick="CloneDeal();" value="新增檔次" />
                                                        </li>
                                                        <li style="padding-bottom: 10px">好康憑證使用方式：
                                                            <fieldset>
                                                                <legend style="cursor: pointer;" id="LegendHideProvision">好康小提示</legend>
                                                                <fieldset id="ProvisionSample" class="HideProvision" style="width: 660px;">
                                                                    <legend style="cursor: pointer; font-size: 14px; color: Black">範本</legend>
                                                                    <asp:Repeater ID="repDataList" runat="server" OnItemDataBound="repDataList_ItemDataBound">
                                                                        <ItemTemplate>
                                                                            <asp:HiddenField ID="hidListDepartment" runat="server" />
                                                                            <asp:DataList ID="dlCategory" runat="server" RepeatColumns="5" RepeatDirection="Horizontal"
                                                                                OnItemDataBound="dlCategory_ItemDataBound">
                                                                                <ItemTemplate>
                                                                                    <asp:HiddenField ID="hidListCategory" runat="server" />
                                                                                    <asp:Label ID="lblListCategory" runat="server" Font-Size="Small" Font-Bold="true"
                                                                                        Style="cursor: pointer; padding-right: 10px"></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:DataList>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                    <ul style="list-style-type: none;">
                                                                        <asp:Repeater ID="repDepartment" runat="server" OnItemDataBound="repDepartment_ItemDataBound">
                                                                            <ItemTemplate>
                                                                                <li>
                                                                                    <asp:HiddenField ID="hidDepartmentId" runat="server" />
                                                                                    <%--<asp:Label ID="lblDepartment" runat="server"></asp:Label>--%>
                                                                                    <ul class="ulItem">
                                                                                        <asp:Repeater ID="repCategory" runat="server" OnItemDataBound="repCategory_ItemDataBound">
                                                                                            <ItemTemplate>
                                                                                                <li class="liItem" style="cursor: pointer; list-style-type: none; font-weight: bold; font-size: 14px"
                                                                                                    id='<%# Eval("Id") %>'>
                                                                                                    <asp:HiddenField ID="hidCategoryId" runat="server" />
                                                                                                    <%--<asp:Label ID="lblCategory" runat="server"></asp:Label>--%>
                                                                                                    <ul class="ulCategory" style="list-style-type: decimal; padding-left: 40px; display: none;">
                                                                                                        <asp:Repeater ID="repItem" runat="server" OnItemDataBound="repItem_ItemDataBound">
                                                                                                            <ItemTemplate>
                                                                                                                <li class="liCategory" style="font-weight: normal;" id='<%# Eval("Id") %>'>
                                                                                                                    <asp:HiddenField ID="hidItemId" runat="server" />
                                                                                                                    <asp:Label ID="lblItem" runat="server"></asp:Label>
                                                                                                                    <ul class="ulContent" style="padding-left: 40px; display: none;">
                                                                                                                        <asp:Repeater ID="repContent" runat="server" OnItemDataBound="repContent_ItemDataBound">
                                                                                                                            <ItemTemplate>
                                                                                                                                <li class="liContent" style="font-weight: normal; cursor: pointer;" id='<%# Eval("Id") %>'>
                                                                                                                                    <asp:HiddenField ID="hidContentId" runat="server" />
                                                                                                                                    <asp:Label ID="lblContent" runat="server"></asp:Label>
                                                                                                                                    <asp:HiddenField ID="hidDescId" runat="server" />
                                                                                                                                    <asp:HiddenField ID="hidDescription" runat="server" />
                                                                                                                                </li>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:Repeater>
                                                                                                                    </ul>
                                                                                                                </li>
                                                                                                            </ItemTemplate>
                                                                                                        </asp:Repeater>
                                                                                                    </ul>
                                                                                                </li>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </ul>
                                                                                </li>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </ul>
                                                                </fieldset>
                                                                <fieldset id="divProvisionEdit" class="HideProvision" style="width: 660px; display: none;">
                                                                    <legend style="cursor: pointer; font-size: 14px; color: Black">編輯模式</legend>
                                                                    <ul id="resultText" style="padding-left: 20px;">
                                                                    </ul>
                                                                </fieldset>
                                                                <fieldset class="HideProvision" style="width: 660px;">
                                                                    <legend style="cursor: pointer; font-size: 14px; color: Black">產生結果</legend>
                                                                    <ul id="ulProvisionResult" style="padding-left: 40px; font-size: 13px; list-style-type: decimal;"
                                                                        runat="server">
                                                                    </ul>
                                                                    <asp:HiddenField ID="hidProvisionResult" runat="server" />
                                                                </fieldset>
                                                            </fieldset>
                                                            <ul style="list-style-type: upper-alpha; padding-left: 40px;">
                                                                <li style="display: none;">貨運公司：<asp:TextBox ID="txtProductFreightOtherCompany" runat="server"></asp:TextBox>
                                                                    <asp:CheckBoxList ID="chklFreightMode" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                                                        <asp:ListItem Text="常溫" Value="0"></asp:ListItem>
                                                                        <asp:ListItem Text="冷藏" Value="0"></asp:ListItem>
                                                                        <asp:ListItem Text="冷凍" Value="0"></asp:ListItem>
                                                                    </asp:CheckBoxList>
                                                                </li>
                                                                <li style="display: none;">
                                                                    <asp:CheckBox ID="chkIslandsFreight" runat="server" Text="離島地區(金/馬/澎)是否配送" />
                                                                </li>
                                                                <li style="display: none;">商品瑕疵證明：
                                                                    <asp:RadioButtonList ID="rdlIsTakePicture" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                                                        <asp:ListItem Text="須拍照" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="無須拍照" Value="0"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </li>
                                                                <li class="Product NoTravel">
                                                                    <asp:CheckBox ID="chkIsParallelProduct" runat="server" Text="平行輸入商品" /><br />
                                                                    <font style="color: Gray; font-size: small">(平行輸入:商品來源採購自品牌國外原廠,並無透過品牌代理商進貨而販售的商品 ,為正品,不是仿冒品.)</font>
                                                                </li>
                                                                <li class="Product NoTravel">
                                                                    <asp:CheckBox ID="chkIsExpiringItems" runat="server" Text="即期品" />(有效期限：
                                                                    <asp:TextBox ID="txtExpiringItemsDateStart" runat="server" Columns="8" />
                                                                    ～
                                                                    <asp:TextBox ID="txtExpiringItemsDateEnd" runat="server" Columns="8" />
                                                                    )<font style="color: Gray; font-size: small">(即期品指有效期限僅剩總期限之1/3稱之)</font></li>
                                                                <li class="Product NoTravel">製造日期/有效期限/保存方式：<br />
                                                                    <asp:TextBox ID="txtProductPreservation" runat="server" Width="650px" TextMode="MultiLine"
                                                                        Rows="6"></asp:TextBox>
                                                                </li>
                                                                <li class="Product NoTravel">功能 / 使用方法：<br />
                                                                    <asp:TextBox ID="txtProductUse" runat="server" Width="650px" TextMode="MultiLine"
                                                                        Rows="6"></asp:TextBox>
                                                                </li>
                                                                <li class="Product NoTravel">規格(重量/淨重/內容量/數量/產地)：<br />
                                                                    <asp:TextBox ID="txtProductSpec" runat="server" Width="650px" TextMode="MultiLine"
                                                                        Rows="11"></asp:TextBox>
                                                                    <br />
                                                                    主要成份(美妝商品填寫)：<br />
                                                                    <asp:TextBox ID="txtProductIngredients" runat="server" Width="650px" TextMode="MultiLine"
                                                                        Rows="6"></asp:TextBox>
                                                                    <br />
                                                                    核準字號(美妝商品填寫)：<br />
                                                                    <asp:TextBox ID="txtSanctionNumber" runat="server" Width="650px" TextMode="MultiLine"
                                                                        Rows="6"></asp:TextBox>
                                                                </li>
                                                                <li class="Product NoTravel">檢驗規定：
                                                                    <asp:CheckBoxList ID="chklInspectRule" runat="server">
                                                                    </asp:CheckBoxList>
                                                                </li>
                                                                <li class="Product">出貨與退貨：
                                                                    <ul style="list-style-type: circle; padding-left: 20px">
                                                                        <li>商品將依照訂單出貨，於結檔後開始陸續出貨。</li>
                                                                        <li>
                                                                            <asp:RadioButtonList ID="rdoShipType" runat="server" RepeatDirection="Horizontal"
                                                                                RepeatLayout="Flow" onclick="ShipTypeChange(this);">
                                                                                <asp:ListItem Text="一般出貨" Value="-1" Selected="True"></asp:ListItem>
                                                                                <asp:ListItem Text="快速出貨" Value="1"></asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </li>
                                                                        <li id="ShipByNormal" class="NonPiinlife">
                                                                            <ul style="list-style-type: circle; padding-left: 0px">
                                                                                <li>
                                                                                    <input  type="radio" name="ShippingDateType" value="1" >
                                                                                    訂單成立後<asp:TextBox
                                                                                    ID="txtShippingDate" runat="server" Width="30px" onkeyup="return ValidateNumber($(this),value);"
                                                                                    MaxLength="2"></asp:TextBox>個工作日內出貨完畢，出貨後配送約2~3個工作日，恕無法指定送達時間。<br />(限台灣本島配送)</li>
                                                                                        <br />
                                                                                     <input  type="radio" name="ShippingDateType" value="0" >
                                                                                    最早出貨日：<span>上檔後</span>+<asp:TextBox
                                                                                    ID="txtUseTimeStart" runat="server" Width="30px" onkeyup="return ValidateNumber($(this),value);"
                                                                                    MaxLength="2"></asp:TextBox>個工作日；最晚出貨日：統一結檔後+<asp:TextBox
                                                                                        ID="txtUseTimeEnd" runat="server" Width="30px" onkeyup="return ValidateNumber($(this),value);"
                                                                                        MaxLength="2"></asp:TextBox>個工作日。
                                                                                <li>週休假日或例假日成立之訂單，均視為次一工作天之訂單。</li>
                                                                            </ul>
                                                                        </li>
                                                                        <li id="ShipByArriveIn24Hrs" class="NonPiinlife">
                                                                            <ul style="list-style-type: circle; padding-left: 0px">
                                                                                <li>訂單成立後24小時出貨完畢，出貨後配送約2~3個工作日，恕無法指定送達時間。(限台灣本島配送)。</li>
                                                                                <li>週休假日或例假日成立之訂單，均視為次一工作日之訂單。</li>
                                                                                <li>此專案供應商每日應登入本平台商家系統回報出貨狀況並回填出貨單號。</li>
                                                                                <li>供應商同意支付本平台因未於24小時完成出貨之懲罰性違約金以彌補消費者損失及本平台商譽,本平台得直接於供應商之貨款中扣除,違約金計算方式依每一訂單金額計算:訂單1000元以下違約金新台幣50元整;訂單1000元以上違約金100元整。
                                                                                </li>
                                                                                <li>若因收貨人資訊不完整或臨時更改、因收貨人自身因素致無法收貨、遇颱風地震等天災、公共工程，或遇系統設備維護，倉儲調整、盤點等情況，致出貨時間將順延，將不受前條懲罰性違約金規範。惟供應商於遭遇前述情形時應提前盡速告知本平台，以便本平台通知收貨人，供應商並應盡快將商品配送到貨。
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                        <li id="ShipByArriveIn72Hrs" class="NonPiinlife">
                                                                            <ul style="list-style-type: circle; padding-left: 0px">
                                                                                <li>訂單成立後24小時出貨完畢，出貨後配送約2~3個工作日，恕無法指定送達時間。(限台灣本島配送)。</li>
                                                                                <li>週休假日或例假日成立之訂單，均視為次一工作日之訂單。</li>
                                                                                <li>此專案供應商每日應登入本平台商家系統回報出貨狀況並回填出貨單號。</li>
                                                                                <li>供應商同意支付本平台因未於24小時完成出貨之懲罰性違約金以彌補消費者損失及本平台商譽,本平台得直接於供應商之貨款中扣除,違約金計算方式依每一訂單金額計算:訂單1000元以下違約金新台幣50元整;訂單1000元以上違約金100元整。
                                                                                </li>
                                                                                <li>若因收貨人資訊不完整或臨時更改、因收貨人自身因素致無法收貨、遇颱風地震等天災、公共工程，或遇系統設備維護，倉儲調整、盤點等情況，致出貨時間將順延，將不受前條懲罰性違約金規範。惟供應商於遭遇前述情形時應提前盡速告知本平台，以便本平台通知收貨人，供應商並應盡快將商品配送到貨。
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                        <li id="PiinlifeDeliveryDesc" runat="server" class="Piinlife"><%= LunchKingSite.I18N.Message.SalesBusinessOrderPiinlifeDeliveryDesc %>
                                                                        </li>
                                                                        <li>鑑賞期：消費者收到貨品後七天內(含六日)。</li>
                                                                        <li>商品退貨：於鑑賞期內完成退貨申請，鑑賞期後不再接受消費者退貨。</li>
                                                                    </ul>
                                                                </li>
                                                                <li class="Product">商品保證：
                                                                    <div style="padding-right: 10px;">
                                                                        <%= LunchKingSite.I18N.Message.SalesBusinessOrderProductGuaranteeClauseDesc %>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li style="padding-bottom: 10px"><span id="TagSettingEmpty" style="display: none;">
                                                            無</span>
                                                            <ul id="ulTagSetting" style="list-style-type: upper-alpha; padding-left: 20px">
                                                                <li>
                                                                    <label>Icon：</label>
                                                                    <asp:CheckBoxList ID="cbl_Icons" AutoPostBack="false" runat="server" Font-Size="Small"
                                                                        RepeatLayout="Flow" RepeatDirection="Horizontal" RepeatColumns="7" CssClass="CouponIconCheckbox">
                                                                        <asp:ListItem Text="五星級飯店" Value="2" style="display: none"></asp:ListItem>
                                                                        <asp:ListItem Text="四星級飯店" Value="3" style="display: none"></asp:ListItem>
                                                                        <asp:ListItem Text="Visa御璽卡.無限卡.白金卡 優先選購專區" class="Piinlife" Value="24"></asp:ListItem>
                                                                    </asp:CheckBoxList>
                                                                </li>
                                                                <li class="DeliveryTravelTag"><label>標籤設定：</label><br />
                                                                    <label>旅遊類：</label>
                                                                    <asp:CheckBoxList ID="cbl_deliveryTravelDealTag" runat="server" Font-Size="Small"
                                                                        RepeatLayout="Table"
                                                                        RepeatDirection="Horizontal" RepeatColumns="7" CssClass="DeliveryTravelDealTag">
                                                                    </asp:CheckBoxList>
                                                                </li>
                                                                <li class="Ppon"><label>標籤設定：</label>
                                                                    <div id="CouponDealTag" class="divCouponDealTag">
                                                                        <label>憑證類：</label>
                                                                        <asp:CheckBoxList ID="cbl_couponDealTag" runat="server" Font-Size="Small" RepeatLayout="Table"
                                                                            RepeatDirection="Horizontal" RepeatColumns="7" CssClass="CouponDealTag">
                                                                        </asp:CheckBoxList>
                                                                        <div id="CouponTravel" class="divCouponTravelTag">
                                                                            <label>旅遊類：</label>
                                                                            <asp:CheckBoxList ID="cbl_couponTravelDealTag" runat="server" Font-Size="Small" RepeatLayout="Table"
                                                                                RepeatDirection="Horizontal" RepeatColumns="7" CssClass="CouponTravelDealTag">
                                                                            </asp:CheckBoxList>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="DealType">
                                                                    <font style="color: Red; font-weight: bold">*</font><label>銷售類別分析：</label>
                                                                    <span id="SaleDealType" style="">
                                                                        <asp:DropDownList ID="ddlDealType1" runat="server" onchange="SetDealType2(true)" />&nbsp;&nbsp;
                                                                    <select id="ddlDealType3" runat="server" onchange="SetDealType2Value(true);"></select>
                                                                        <asp:HiddenField ID="hdDealType2" runat="server" Value="" />
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li style="padding-bottom: 10px">其他特殊設定： <span id="SpecialSettingEmpty" style="display: none;">無</span><ul id="ulSpecialSetting" style="list-style-type: upper-alpha; padding-left: 40px;">
                                                            <li class="Ppon NoTravel">檔次退貨方式：
                                                                    <asp:RadioButtonList ID="rdlReturnType" runat="server" RepeatDirection="Vertical">
                                                                        <asp:ListItem Text="全程接受退貨（一般檔次）" Value="0" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Text="不接受退貨（廠商提供序號活動，如：雲端印刷網、story365…等）" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="結檔後七天不能退貨（展覽票券，如：外星人探索特展、鬼太郎…等）" Value="2" Enabled="false"></asp:ListItem>
                                                                        <asp:ListItem Text="過期不能退貨（展覽票券或廠商提供序號活動，如：中友奇幻仿生獸、CatchPlay…等）" Value="3"></asp:ListItem>
                                                                        <asp:ListItem Text="演出時間前十日內不能退貨（表演票券，如：魔幻極境-拉斯維加斯大型魔術秀…等）" Value="4"></asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                            </li>
                                                            <li class="Ppon NoTravel">憑證使用方式：
                                                                    <asp:CheckBox ID="chkIsNotAccessSms" runat="server" Text="不可使用簡訊憑證" />
                                                            </li>
                                                        </ul>
                                                        </li>
                                                        <li style="padding-bottom: 10px; display: none;">合約條款：<br />
                                                            <asp:CheckBox ID="chkProductRefund" runat="server" CssClass="Product" Text="商品出貨/回報/付款" />
                                                            <div id="pProductRefund" runat="server" style="font-size: small; color: Gray; padding-left: 20px; padding-right: 60px; display: none;">
                                                            </div>
                                                        </li>
                                                        <li style="padding-bottom: 10px">其他附註說明：<br />
                                                            <asp:TextBox ID="txtOtherMemo" runat="server" TextMode="MultiLine" Width="720px"
                                                                Height="120px"></asp:TextBox>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="display: none;">
                                        <table>
                                            <tr>
                                                <td class="Title" style="font-weight: bold;" colspan="2">【特店折扣】
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <ul style="list-style-type: circle; padding-left: 20px">
                                                        <li>特約優惠起迄日期：
                                                <asp:TextBox ID="txtCreateTimeS" runat="server" Columns="8" />
                                                            ～
                                                <asp:TextBox ID="txtCreateTimeE" runat="server" Columns="8" />
                                                            <font style="color: Gray; font-size: small">(*註:預設帶入為期一年)</font></li>
                                                        <li>優惠內容：<br />
                                                            <asp:TextBox ID="txtPezStores" runat="server" TextMode="MultiLine" Width="730px"
                                                                Height="100px"></asp:TextBox></li>
                                                        <li>注意事項：<br />
                                                            <asp:TextBox ID="txtPezStoreNotice" runat="server" TextMode="MultiLine" Width="730px"
                                                                Height="100px"></asp:TextBox></li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="ShowDiv4" style="padding-left: 10px; border: 0;">
                                        <span class="Title" style="font-weight: bold;">【歷史紀錄】</span>
                                        <asp:GridView ID="gvHistoryList" runat="server" OnRowDataBound="gvHistoryList_RowDataBound"
                                            AllowSorting="True" GridLines="Horizontal" ForeColor="Black" CellPadding="4"
                                            BorderStyle="None" BackColor="White" EmptyDataText="無符合的資料"
                                            AutoGenerateColumns="False" Font-Size="Small" Width="887px" EnableViewState="False">
                                            <FooterStyle BackColor="#bbbC99"></FooterStyle>
                                            <RowStyle BackColor="#F7F7DE" Height="30px" BorderColor="#DEDFDE"></RowStyle>
                                            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
                                            <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="30px"></HeaderStyle>
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblChangeId" runat="server" ForeColor="Red"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="20px" />
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="使用者帳號" DataField="ModifyId" ItemStyle-HorizontalAlign="Center"
                                                    ItemStyle-Width="60px" />
                                                <asp:TemplateField HeaderText="修改日期" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblModifyTime" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="修改內容" DataField="Changeset" ItemStyle-HorizontalAlign="Center"
                                                    ItemStyle-Width="540px" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <div id="navcontainer" style="display: none;">
                                        <ul id="navlist">
                                            <li class="liTitle" value="0"><span>新增業務工單</span></li>
                                            <li class="liTitle" value="1"><span>Part1 店家資料</span></li>
                                            <li class="liTitle" value="2"><span>Part2 好康優惠</span></li>
                                            <li class="liTitle" value="3"><span>Part3 特店折扣</span></li>
                                            <li class="liTitle" value="4"><span>Part4 歷史紀錄</span></li>
                                        </ul>
                                    </div>
                                </asp:Panel>
                                <span id="FillData" style="cursor: pointer; color: White;">FillData</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

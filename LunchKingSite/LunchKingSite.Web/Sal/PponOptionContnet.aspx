﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="PponOptionContnet.aspx.cs" Inherits="LunchKingSite.Web.Sal.PponOptionContnet" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>

<%@ Register Src="~/Sal/ProposalMenu.ascx" TagName="ProposalMenu" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css") %>" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .breadcrumb {
            /*centering*/
            display: inline-block;
            box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.35);
            overflow: hidden;
            border-radius: 5px;
            /*Lets add the numbers for each link using CSS counters. flag is the name of the counter. to be defined using counter-reset in the parent element of the links*/
            counter-reset: flag;
        }

            .breadcrumb a {
                text-decoration: none;
                outline: none;
                display: block;
                float: left;
                font-size: 12px;
                line-height: 36px;
                color: white;
                /*need more margin on the left of links to accomodate the numbers*/
                padding: 0 10px 0 60px;
                background: #666;
                background: linear-gradient(#666, #333);
                position: relative;
            }


                /*adding the arrows for the breadcrumbs using rotated pseudo elements*/
                .breadcrumb a:after {
                    content: '';
                    position: absolute;
                    top: 0;
                    right: -18px; /*half of square's length*/
                    /*same dimension as the line-height of .breadcrumb a */
                    width: 36px;
                    height: 36px;
                    /*as you see the rotated square takes a larger height. which makes it tough to position it properly. So we are going to scale it down so that the diagonals become equal to the line-height of the link. We scale it to 70.7% because if square's: 
	        length = 1; diagonal = (1^2 + 1^2)^0.5 = 1.414 (pythagoras theorem)
	        if diagonal required = 1; length = 1/1.414 = 0.707*/
                    transform: scale(0.707) rotate(45deg);
                    /*we need to prevent the arrows from getting buried under the next link*/
                    z-index: 1;
                    /*background same as links but the gradient will be rotated to compensate with the transform applied*/
                    background: #666;
                    background: linear-gradient(135deg, #666, #333);
                    /*stylish arrow design using box shadow*/
                    box-shadow: 2px -2px 0 2px rgba(0, 0, 0, 0.4), 3px -3px 0 2px rgba(255, 255, 255, 0.1);
                    /*
		        5px - for rounded arrows and 
		        50px - to prevent hover glitches on the border created using shadows*/
                    border-radius: 0 5px 0 50px;
                }

        .flat a, .flat a:after {
            background: white;
            color: black;
            transition: all 0.5s;
        }

            .flat a.active,
            .flat a.active:after {
                color: #fff;
                background: #AF0000;
            }

        .highlight {
            background-color: #f7a873;
        }

        .edit {
            display: none;
        }
    </style>
    <script type="text/javascript">
        $(function () {

            $(".number").keyup(function () {
                if (/\D/g.test(this.value)) {
                    this.value = "";
                }
            });

            $("#txtBusinessHourGuid")
                .focus(function () { $(this).select(); })
                .mouseup(function (e) { e.preventDefault(); });
        });

        function clickDeals(obj) {
            if ($("#hidEditMode").val() == "1") {
                alert("頁面內容尚未儲存，請先執行儲存或取消後，再執行切換檔次，謝謝。");
                return false;
            }
            var row = $(obj);
            var bid = row.find("#bid").html();
            $("#txtAssignBusinessHourGuid").val(bid);
            $('#tabDeals tr').removeClass('highlight');
            $(obj).toggleClass("highlight");

            LoadData($("#txtBusinessHourGuid").val(), bid);
        }

        function LoadData(bid, assignBid) {
            if ($.trim(bid) == "") {
                alert("請輸入檔次Bid");
                return false;
            }
            $.ajax({
                type: "POST",
                url: "PponOptionContnet.aspx/DataGet",
                data: "{'bid': '" + bid + "','assignBid':'" + assignBid + "','UserName': '<%=UserName%>','Url': '" + location.pathname + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var c = response.d;
                    if (c != undefined) {
                        $("#phResult").show();
                        var data = JSON.parse(c);
                        if (data.BusinessHourGuid != "<%=Guid.Empty%>") {
                            var PponDeals = data.PponDeals;         //檔次
                            var PponOptions = data.pponOption;      //多重選項
                            var Stores = data.store;                //分店資訊
                            var pponStores = data.pponStores;       //分店資訊
                            var pponOptionLog = data.pponOptionLog; //變更紀錄
                            var Verified = data.Verified;           //已核銷總數量
                            var StoreVerified = data.StoreVerified; //分店已核銷數量
                            var Sale = data.Sale;                   //已售總數量
                            var StoreSale = data.StoreSale;         //分店已售數量
                            var Return = data.Return;               //已售總數量

                            clearData();
                            $("#btnEdit").hide();
                            $("#txtAssignBusinessHourGuid").val(data.BusinessHourGuid);

                            
                            if (data.IsClosed) {
                                alert("該檔次已結檔/未開檔，無法調整數量。");
                                return false;
                            }

                            if (!data.IsSelf) {
                                alert("不是該檔負責業務，無法進入！");
                                return false;
                            }

                            if (!data.IsPrivilege) {
                                alert(data.NoPrivilege);
                                return false;
                            }

                            //是否為通用券檔次
                            if (data.IsNoRestrictedStore)
                                $('.NoRestrictedStore').hide();
                            else
                                $('.NoRestrictedStore').show();
                                


                            //是否為上架費檔次或者商品寄倉
                            if (data.IsSlottingFeeQuantity || data.IsConsignment) {
                                $('#btnEdit').hide();
                            }
                            else {
                                //是否有權限更新
                                if (data.IsUpdate) {
                                    $("#btnEdit").show();
                                } else {
                                    $("#btnEdit").hide();
                                }
                            }

                            $("#changeLog").empty();

                            $("#tabOrderTotalLimit").find('tbody').empty();
                            $("#tabPponOption").empty();

                            if (PponDeals.length > 0) {
                                $("#litOrderTotalLimit").html(PponDeals[0].OrderTotalLimit);
                            }

                            if (data.multiDeals) {
                                $("#spanMultiOrderTotalLimit").show();
                                $("#MainOrderTotalLimit").html(data.MainOrderTotalLimit);
                                $("#SumOrderTotalLimit").html(data.SumOrderTotalLimit);
                            } else {
                                $("#spanMultiOrderTotalLimit").hide();
                            }


                            var sHtml = "";
                            /*
                            *方案
                            */
                            if (assignBid == "<%=Guid.Empty%>") {
                                sHtml = "";
                                $("#tabDeals").find('tbody').empty();
                                $.each(PponDeals, function (idx, obj) {
                                    sHtml += "<tr style='cursor: pointer' onclick='clickDeals(this)' " + (idx == 0 ? " class='highlight'" : "") + ">";
                                    sHtml += "<td style='max-width: 200px'><span id='bid' style='display:none'>" + obj.BusinessHourGuid + "</span>" + obj.UniqueId + "</td>";
                                    sHtml += "<td style='max-width: 250px'>" + obj.ItemName + "</td>";
                                    sHtml += "<td style='padding-left:10px'>銷售數量 " + Sale[obj.BusinessHourGuid] + "</td>";
                                    //sHtml += "<td style='text-align:left'>" + Sale[obj.BusinessHourGuid] + "</td>";   //銷售份數
                                    sHtml += "<td style='padding-left:10px'>退貨數量 " + Return[obj.BusinessHourGuid] + "</td>";
                                    //sHtml += "<td style='text-align:left'>" + Return[obj.BusinessHourGuid] + "</td>";   //銷售份數
                                    sHtml += "<td style='padding-left:10px'>核銷數量 " + Verified[obj.BusinessHourGuid] + "</td>";
                                    //sHtml += "<td style='text-align:left'>" + Verified[obj.BusinessHourGuid] + "</td>";   //核銷份數
                                    sHtml += "</tr>";
                                });
                                $("#tabDeals").append(sHtml);

                            }

                            /*
                            *分店
                            */
                            sHtml = "";
                            if (Stores.length > 0) {
                                $("#tabStore").find('tbody').empty();
                                $.each(Stores, function (idx, obj) {
                                    var TotalQuantity = findPponStore(pponStores, obj.Guid);
                                    //var OrderedQuantity = findOrderedQuantity(pponStores, obj.Guid);
                                    sHtml += "<tr>";
                                    sHtml += "<td>" + (idx + 1) + "</td>";   //排序
                                    if (!data.IsNoRestrictedStore)
                                        sHtml += "<td><span id='spanBid' style='display:none'>" + data.BusinessHourGuid + "</span><span id='storeGid' style='display:none'>" + obj.Guid + "</span><span id='litQuantity'>" +
                                                 (StoreSale[data.BusinessHourGuid][obj.Guid] == null ? "0" : StoreSale[data.BusinessHourGuid][obj.Guid]) + "</span></td>";   //已售
                                    sHtml += "<td><span id='litStoreVerifiedCount'>" + (StoreVerified[data.BusinessHourGuid][obj.Guid] == null ? "0" : StoreVerified[data.BusinessHourGuid][obj.Guid]) + "</span></td>";   //核銷
                                    sHtml += "<td><span id='litStoreCount' class='view'>" + TotalQuantity + "</span><input type='text' id='txtStoreCount' class='edit number' Style='width:50px;display: none' value='" + TotalQuantity + "' /></td>";   //上限
                                    sHtml += "<td>" + obj.SellerName + "-" + (obj.StoreAddress == null ? obj.SellerAddress : obj.StoreAddress) + "</td>";   //分店資訊
                                    sHtml += "</tr>";
                                });
                                $("#tabStore").append(sHtml);
                            } else {
                                //$("#tabStore").parent().parent().empty();
                            }

                            /*
                            *多重選項
                            */
                            var OptionItems = [];

                            $.each(PponOptions, function (idx, obj) {
                                if ($.inArray(obj.CatgName, OptionItems) == -1) {
                                    OptionItems.push(obj.CatgName);
                                }
                            });

                            sHtml = "";
                            $.each(OptionItems, function (cidx, cg) {
                                sHtml += cg + "<br />";
                                sHtml += "<div id='mc-table'><table style='width:100%'>";
                                sHtml += "<thead>";
                                sHtml += "<tr>";
                                sHtml += "<th style='background-color: #444444;color: #FFF;'>單項品名</th>";
                                sHtml += "<th style='background-color: #444444;color: #FFF;'>目前可售數量</th>";
                                sHtml += "<th style='background-color: #444444;color: #FFF;'>調整</th>";
                                sHtml += "<th style='background-color: #444444;color: #FFF;'>調整後可售數量</th>";
                                sHtml += "</tr>";
                                sHtml += "</thead>";

                                $.each(PponOptions, function (idx, obj) {
                                    if (obj.CatgName == cg) {
                                        sHtml += "<tr>";
                                        sHtml += "<td><span id='spanId' style='display:none'>" + obj.Id + "</span><span id='spanBid' style='display:none'>" + data.BusinessHourGuid + "</span><span id='spanOptionName'>" + obj.OptionName + "</span></td>";
                                        sHtml += "<td><span id='spanQuantity'>" + (obj.Quantity == null || isNaN(obj.Quantity) ? "" : obj.Quantity) + "</span></td>";
                                        sHtml += "<td><input type='text' id='txtModifyCatgSeq' value='' class='edit number' Style='width:50px;display: none' onblur='modifySpan(this)' /><span class='view'></span></td>";
                                        sHtml += "<td><span id='litModifiedCatgSeq' class='edit view'>" + (obj.Quantity == null || isNaN(obj.Quantity) ? "" : obj.Quantity) + "</span></td>";
                                        sHtml += "</tr>";
                                    }
                                });
                                sHtml += "</table></div><p>";
                            });

                            $("#tabPponOption").append(sHtml);

                            /*
                            * 最大購買數量
                            */
                            sHtml = "";
                            sHtml += "<tr>";
                            sHtml += "<td><span id='spanBid' style='display:none'>" + data.BusinessHourGuid + "</span><span id='spanOrderTotalLimit'>" + data.OrderTotalLimit + "</span></td>";
                            sHtml += "<td><input type='text' id='txtModifyOrderTotalLimit' value='' class='edit number' Style='width:50px;display: none' onblur='modifySpan2(this)' /></td>";
                            sHtml += "<td><span id='retOrderTotalLimit'>" + data.OrderTotalLimit + "</span></td>";
                            sHtml += "</tr>";

                            $("#tabOrderTotalLimit").append(sHtml);

                            /*
                            *變更紀錄
                            */
                            sHtml = "";
                            $.each(pponOptionLog, function (cidx, cg) {
                                sHtml = "<li>";
                                sHtml += "<div class='data-input rd-data-input'>";
                                sHtml += cg.LogContent;
                                sHtml += "</div>";
                                sHtml += "</li>";
                            });
                            $("#changeLog").append(sHtml);
                            
                        } else {
                            alert("查無資料");
                            $("#phResult").hide();
                            return false;
                        }

                    }
                }, error: function (response) {
                    console.log(response.responseText);
                }
            });
        }

        function modifySpan(ele) {
            var tr = $(ele).closest('tr');
            var spanQuantity = tr.find("span#spanQuantity");
            var litModifiedCatgSeq = tr.find("span#litModifiedCatgSeq");
            if (litModifiedCatgSeq.html() == "") {
                litModifiedCatgSeq.html($(ele).val());
            } else {
                var data = 0;
                if ($.trim($(ele).val()) != "") {
                    data = parseInt($(ele).val()) + parseInt(spanQuantity.html());
                } else {
                    data = parseInt(spanQuantity.html());
                }
                 
                litModifiedCatgSeq.html(data);
            }
        }

        function modifySpan2(ele) {
            var tr = $(ele).closest('tr');
            var retOrderTotalLimit = tr.find("span#retOrderTotalLimit");
            var spanOrderTotalLimit = tr.find("span#spanOrderTotalLimit");
            if (spanOrderTotalLimit.html() == "") {
                retOrderTotalLimit.html($(ele).val());
            } else {
                var data = 0;
                if ($.trim($(ele).val()) != "") {
                    data = parseInt($(ele).val()) + parseInt(spanOrderTotalLimit.html());
                } else {
                    data = parseInt(spanOrderTotalLimit.html());
                }

                retOrderTotalLimit.html(data);
            }
        }

        function clearData() {
            //$("#tabDeals").find('tbody').empty();
            $("#changeLog").empty();
            $("#tabOrderTotalLimit").find('tbody').empty();
            $("#tabPponOption").empty();

            $("#tabPponOption").empty();
            $("#tabStore").find('tbody').empty();
            //$("#tabStore").parent().parent().empty();
        }

        function findPponStore(ps, sid) {
            var data = "";
            $.each(ps, function (idx, obj) {
                if (obj.StoreGuid == sid) {
                    data = (obj.TotalQuantity == null ? "" : obj.TotalQuantity);
                }
            });
            return data;
        }

        function findOrderedQuantity(ps, sid) {
            var data = "";
            $.each(ps, function (idx, obj) {
                if (obj.StoreGuid == sid) {
                    data = (obj.OrderedQuantity == null ? "" : obj.OrderedQuantity);
                }
            });
            return data;
        }

        function Search() {
            $("#tabDeals").find('tbody').empty();
            editMode(false);
            LoadData($("#txtBusinessHourGuid").val(), "<%=Guid.Empty%>");
            return false;
        }

        function editData() {
            editMode(true);
            return false;
        }
        function cancelData() {
            if (!confirm("確定取消?將不會儲存任何資料...")) {
                return false;
            }
            editMode(false);
            return false;
        }
        function editMode(edit) {
            if (edit) {
                $(".view").hide();
                $(".edit").show();
                $("#hidEditMode").val("1");
            } else {
                $(".view").show();
                $(".edit").hide();
                $("#hidEditMode").val("0");
            }
        }
        function saveData() {
            var flag = true;
            /*
            *分店
            */
            var storeList = [];
            $("#tabStore tr").not(':first').each(function () {
                var Bid = $(this).find("#spanBid").html();
                var txtStoreCount = $(this).find("#txtStoreCount").val();
                var storeGid = $(this).find("#storeGid");
                if ($.trim(txtStoreCount) != "") {
                    if (!isNaN($.trim(txtStoreCount))) {
                        var store = {
                            Bid: Bid,
                            Sid: storeGid.html(),
                            TotalQuantity: txtStoreCount
                        }
                        storeList.push(store);
                    }
                }
            });
            /*
            *多重選項
            */
            var QuantityList = [];
            $("#tabPponOption table").each(function () {
                $(this).find("tr").not(':first').each(function () {
                    var Bid = $(this).find("#spanBid").html();
                    var Id = $(this).find("#spanId").html();
                    var spanOptionName = $(this).find("#spanOptionName").html();//品名
                    var txtModifyCatgSeq = $(this).find("#txtModifyCatgSeq").val();    //調整
                    var spanQuantity = $(this).find("#spanQuantity").html();    //目前可售數量
                    if ($.trim(txtModifyCatgSeq) != "") {
                        if (!isNaN($.trim(txtModifyCatgSeq))) {
                            if (spanQuantity == "") {
                                spanQuantity = "0";
                            } else {
                                if (txtModifyCatgSeq == "0") {
                                    alert("正值不可輸入「0」。");
                                    flag = false;
                                    return false;
                                }
                            }
                            var count = parseInt(txtModifyCatgSeq) + parseInt(spanQuantity);
                            var Quantity = {
                                Id: Id,
                                Bid: Bid,
                                ModifyCatgSeq: txtModifyCatgSeq,
                                Quantity: count
                            }
                            QuantityList.push(Quantity);
                        }
                    }
                });
            });

            /*
            *最大購買數量
            */
            var OrderTotalLimitList = [];
            $("#tabOrderTotalLimit tr").not(':first').each(function () {
                var Bid = $(this).find("#spanBid").html();
                var spanOrderTotalLimit = $(this).find("#spanOrderTotalLimit").html();
                var txtModifyOrderTotalLimit = $(this).find("#txtModifyOrderTotalLimit").val();
                if ($.trim(txtModifyOrderTotalLimit) != "") {
                    if (!isNaN(spanOrderTotalLimit) && !isNaN(txtModifyOrderTotalLimit)) {
                        var count = parseInt(spanOrderTotalLimit) + parseInt(txtModifyOrderTotalLimit);
                        var OrderTotalLimit = {
                            Bid: Bid,
                            ModifyOrderTotalLimit: txtModifyOrderTotalLimit,
                            OrderTotalLimit: count
                        }
                        OrderTotalLimitList.push(OrderTotalLimit);
                    }
                }

            });

            if (flag) {
                $.ajax({
                    type: "POST",
                    url: "PponOptionContnet.aspx/DataSave",
                    data: "{'bid': '" + $("#txtAssignBusinessHourGuid").val() + "', 'storeList': '" + JSON.stringify(storeList) + "','QuantityList':'" + JSON.stringify(QuantityList) + "','OrderTotalLimitList':'" + JSON.stringify(OrderTotalLimitList) + "','UserName': '<%=UserName%>','Url': '" + location.pathname + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var c = response.d;
                        if (c != undefined) {
                            if (c) {
                                var data = JSON.parse(c);
                                alert(data.Message);
                                if (data.IsSuccess) {
                                    LoadData($("#txtBusinessHourGuid").val(), $("#txtAssignBusinessHourGuid").val());
                                    editMode(false);
                                    //location.reload();
                                }
                            }
                        }
                    }, error: function (response) {
                        console.log(response.responseText);
                    }
                });
            }
            
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <asp:Panel ID="divMain" runat="server">
         <style>
        .mc-navbar .mc-navbtn{
            width:10%;
        }
    </style>
    <uc2:ProposalMenu ID="ProposalMenu" runat="server" ></uc2:ProposalMenu>
        <div id="MainSet" class="mc-content">
            <div id="basic">
                <div class="form-unit" style="border-bottom: none;">
                    <div id="dealDate">
                        <h1 class="rd-smll">數量管理</h1>
                        <hr class="header_hr">
                        <div class="grui-form">
                            <div class="form-unit">
                                <label class="unit-label rd-unit-label">
                                    檔次BID</label>
                                <div class="data-input rd-data-input">
                                    <asp:TextBox ID="txtBusinessHourGuid" runat="server" ClientIDMode="Static" Width="250px"></asp:TextBox>
                                    <span style="display:none">
                                    <asp:TextBox ID="txtAssignBusinessHourGuid" runat="server" ClientIDMode="Static" Width="250px"></asp:TextBox>
                                        </span>
                                    <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" CssClass="btn rd-mcacstbtn" ClientIDMode="Static" OnClientClick="return Search();" />
                                </div>
                            </div>
                            <span id="phResult" style="display: none">
                                <h1 class="rd-smll">選擇查詢的方案</h1>
                                <hr class="header_hr">
                                <div class="form-unit ppon">
                                    <div class="data-input rd-data-input">
                                        <div>

                                            <table width="100%" id="tabDeals" border="0" cellspacing="0" cellpadding="0" class="rd-table">

                                                <%-- <tr style="cursor: pointer">
                                                        <td style="max-width: 200px"><span id="bid" style="display: none"><%# ((ViewComboDeal)Container.DataItem).BusinessHourGuid %></span><%# ((ViewComboDeal)Container.DataItem).Title %></td>
                                                        <td>目前銷售數量</td>
                                                        <td></td>
                                                    </tr>--%>
                                            </table>

                                            <br />
                                            <span id="spanMultiOrderTotalLimit">
                                                <p>代表檔最大購買數量：<span id="MainOrderTotalLimit"></span></p>
                                                <br />
                                                <p>調整後代表檔最大購買數量：<span id="SumOrderTotalLimit"></span></p>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-unit ppon">
                                        <div class="data-input rd-data-input" style="float: right">
                                            <asp:Button ID="btnEdit" runat="server" Text="我要調整數量" CssClass="btn btn-primary rd-mcacstbtn btn-circle view" OnClientClick="return editData(this)" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                    <!--分店-->
                                    <h1 class="rd-smll">分店</h1>
                                    <hr class="header_hr">
                                    <div class="form-unit ppon">
                                        <div class="data-input rd-data-input">
                                            <div id="mc-table">

                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table" id="tabStore">
                                                    <thead>
                                                        <tr>
                                                            <th>排序</th>
                                                            <th  class='NoRestrictedStore'>已售</th>
                                                            <th>已核銷</th>
                                                            <th>上限</th>
                                                            <th>分店資訊</th>
                                                        </tr>
                                                    </thead>

                                                    <%--<tr>
                                                        <td></td>
                                                        <td>
                                                            <asp:Literal ID="litQuantity" runat="server"></asp:Literal>
                                                            <asp:Literal ID="litStoreGuid" runat="server" Visible="false"></asp:Literal>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtStoreCount" runat="server" MaxLength="6" Width="50" Text="0" class="edit number" Style="display: none"></asp:TextBox>
                                                            <asp:Label ID="litStoreCount" runat="server" Text="0" class="view"></asp:Label>
                                                        </td>
                                                        <td style="text-align: left"></td>
                                                    </tr>--%>
                                                </table>

                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>
                                    <!--//分店-->

                                    <!--多重選項-->
                                    <h1 class="rd-smll">多重選項</h1>
                                    <hr class="header_hr">
                                    <div class="form-unit ppon">
                                        <div class="data-input rd-data-input">
                                            <div id="tabPponOption">
                                            </div>
                                            <div>
                                                <asp:Repeater ID="rptPponOption" runat="server">
                                                    <HeaderTemplate>
                                                        <table width="100%" border="0" id="tabPponOption1">
                                                            <tr>
                                                                <td>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border: 1px solid #999;">
                                                            <thead>
                                                                <tr>
                                                                    <th style="background-color: #444444; color: #FFF;">單項品名</th>
                                                                    <th style="background-color: #444444; color: #FFF;">目前可售數量</th>
                                                                    <th style="background-color: #444444; color: #FFF;">調整</th>
                                                                    <th style="background-color: #444444; color: #FFF;">調整後可售數量</th>
                                                                </tr>
                                                            </thead>
                                                            <tr>
                                                                <td><%# ((PponOption)Container.DataItem).CatgName %></td>
                                                                <td><%# ((PponOption)Container.DataItem).CatgSeq %></td>
                                                                <td>
                                                                    <asp:TextBox ID="txtModifyCatgSeq" runat="server" MaxLength="6" Width="50" Text="0" class="edit number" Style="display: none"></asp:TextBox>
                                                                    <asp:Label ID="litModifyCatgSeq" runat="server" Text="0" class="view"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Literal ID="litModifiedCatgSeq" runat="server" Text="<%# ((PponOption)Container.DataItem).CatgSeq %>"></asp:Literal>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </td></tr>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>
                                    <!--//多重選項-->

                                    <!--最大購買數量-->
                                    <h1 class="rd-smll">最大購買數量</h1>
                                    <hr class="header_hr">
                                    <div class="form-unit ppon">
                                        <div class="data-input rd-data-input">
                                            <div id="mc-table">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table" id="tabOrderTotalLimit">
                                                    <thead>
                                                        <tr>
                                                            <th>目前</th>
                                                            <th>調整</th>
                                                            <th>調整後</th>
                                                        </tr>
                                                    </thead>
                                                    <%--<tr>
                                                        <td>
                                                            <asp:Literal ID="litOrderTotalLimit" runat="server" ClientIDMode="Static" Text="<%# Container.DataItem %>"></asp:Literal></td>
                                                        <td>
                                                            <asp:TextBox ID="txtModifyOrderTotalLimit" runat="server" MaxLength="6" Width="50" Text="0" class="edit number" Style="display: none"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:Literal ID="litModifiedOrderTotalLimit" runat="server" Text="<%# Container.DataItem %>"></asp:Literal>
                                                        </td>
                                                    </tr>--%>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                    <!--//最大購買數量-->
                                    <!--最大購買數量-->
                                    <h1 class="rd-smll">變更紀錄</h1>
                                    <hr class="header_hr">
                                    <div class="form-unit ppon">
                                        <div class="data-input rd-data-input">
                                            <ul id="changeLog">
                                            </ul>
                                        </div>
                                </div>
                                <!--//最大購買數量-->
                                </div>
                            </span>
                            <input type="hidden" id="hidEditMode" value="0" />
                            <asp:PlaceHolder ID="phEditMode" runat="server">
                                <div style="float: right" class="edit">
                                    <div>

                                        <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default rd-mcacstbtn btn-circle" OnClientClick="return cancelData()" />
                                        <asp:Button ID="btnSave" runat="server" Text="儲存" CssClass="btn btn-primary rd-mcacstbtn btn-circle" OnClientClick="return saveData()" />
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                        </div>
                    </div>
                </div>
            </div>
    </asp:Panel>

</asp:Content>

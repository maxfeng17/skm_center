﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="SalesCalendar.aspx.cs" Inherits="LunchKingSite.Web.Sal.SalesCalendar" %>


<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>

<%@ Register Src="~/Sal/ProposalMenu.ascx" TagName="ProposalMenu" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Tools/js/fancybox/jquery.fancybox.css") %>" rel="stylesheet" type="text/css" />

    <link href="<%=ResolveUrl("~/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css") %>" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<%=ResolveUrl("~/Tools/js/sales.calendar.js") %>"></script>

    <script type="text/javascript">
        var Sales;
        var CLIENT_ID = '<%=SaleGoogleCalendarAPIKey%>';
        $(function () {
           
        });

        jQuery(window).load(function () {
            Sales.Calendar.GetSalesCalenderID();
        });

        function DeleteTask(obj) {
            var id = $(obj).parent().find("#liId").val();
            if (!confirm("是否確認刪除訊息?")) {
                return false;
            }
            $.ajax(
            {
                type: "POST",
                url: "SalesCalendar.aspx/DeleteTask",
                data: "{ 'Id': " + id + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                dataFilter: function (data) { return data; },
                success: function (data) {
                    if (data.d) {
                        location.reload();
                    }
                }, error: function (res) {
                    console.log(res);
                }
            });
        }
    </script>
    <script type="text/javascript" src="https://apis.google.com/js/client.js?onload=checkAuth" />

    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <style>
        .calListSquare {
            background: #fff;
            border: 1px solid #dcdcdc;
            border-radius: 1px;
            height: 11px;
            margin-right: 4px;
            position: relative;
            top: 2px;
            width: 11px;
            -moz-border-radius: 1px;
            -webkit-border-radius: 1px;
        }

        
    </style>

    <pre id="output"></pre>

    <style>
        .mc-navbar .mc-navbtn {
            width: 10%;
        }
    </style>
    <uc2:ProposalMenu ID="ProposalMenu" runat="server" ></uc2:ProposalMenu>
    <div class="mc-content">

        <asp:HyperLink ID="btnCreateCalendar" runat="server" ClientIDMode="Static" class="btn btn-small btn-primary form-inline-btn openclose" Style="color: #fff" NavigateUrl="SalesCalendarContent.aspx">建立</asp:HyperLink>
        日曆顯示：
        <span id="spanCalendarList"></span>
        <hr class="header_hr">
        <div id="maincontent" class="clearfix"></div>
        <table style="width: 100%">
            <tr>
                <td style="width: 70%;" valign="top">
                    <div id="authorize-div" style="display: none; width: 100%; text-align: center">
                        <button id="authorize-button" onclick="handleAuthClick(event)">
                            請求認證
                        </button>
                    </div>
                    <iframe id="gFrame" src="" style="border: 0" width="100%" height="600" frameborder="0" scrolling="no"></iframe>

                </td>
                <td style="width: 5%; padding-left: 5px" valign="top"></td>
                <td style="width: 25%; padding-left: 5px; padding-top: 10px" valign="top">
                    <asp:Repeater ID="rptTaskList" runat="server" OnItemDataBound="rptTaskList_ItemDataBound">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="mc-tableContentITEM" style="width: 100%;">
                                <td style="word-break: break-all; text-align: left">
                                    <asp:Literal ID="liEvent" runat="server"></asp:Literal>
                                    <asp:HyperLink ID="hypEvent" runat="server"><image src="../Themes/default/images/17Life/G2/edit.png" width="16px"/></image></asp:HyperLink>
                                    <asp:HyperLink ID="hypEventDel" runat="server" onclick="DeleteTask(this)"><img src='../Themes/default/images/17Life/G2/trash.png' width="16px" /></asp:HyperLink>
                                    <asp:TextBox ID="liId" runat="server" ClientIDMode="Static" style="display:none"></asp:TextBox>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <br />頁數：
                    <asp:DropDownList ID="ddlPager" runat="server" class="data-input rd-data-input" OnSelectedIndexChanged="ddlPager_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="1">1</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <input type="text" id="CalenderId" value="" style="display: none" />
        <input type="text" id="SalesCalenderName" value="業務系統" style="display: none" />
        <asp:TextBox ID="txtUserName" runat="server" ClientIDMode="Static" Style="display: none"></asp:TextBox>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LastLoadJs" runat="server">
</asp:Content>

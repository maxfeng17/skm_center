﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.Sal
{
    public partial class ISPManage : RolePage, ISalesISPManageView
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region props
        private SalesISPManagePresenter _presenter;
        public SalesISPManagePresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string SellerName
        {
            get
            {
                return tbSellerName.Text;
            }
        }

        public string SellerId
        {
            get
            {
                return tbSellerId.Text;
            }
        }

        public string ISPStatus
        {
            get
            {
                return ddlISPStatus.SelectedValue;
            }
        }

        public string ServiceChannel
        {
            get
            {
                return ddlServiceChannel.SelectedValue;
            }
        }

        public string StartDate
        {
            get
            {
                return tbStartDate.Text;
            }
        }

        public string EndDate
        {
            get
            {
                return tbEndDate.Text;
            }
        }

        public int PageSize
        {
            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }
        }

        public int CurrentPage
        {
            get
            {
                return ucPager.CurrentPage;
            }
        }

        public FileUpload FUExport
        {
            get
            {
                return FUImport;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        #endregion

        #region event

        public event EventHandler<DataEventArgs<int>> Search;
        public event EventHandler Export;
        public event EventHandler Import;
        public event EventHandler<DataEventArgs<int>> OnGetCount;
        public event EventHandler<DataEventArgs<int>> PageChanged;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                ddlISPStatus.Items.Add(new ListItem { Text = "全部", Value = "" });
                foreach (ISPStatus item in Enum.GetValues(typeof(ISPStatus)))
                {
                    if ((int)item > 0)
                    {
                        ddlISPStatus.Items.Add(new ListItem { Text = Helper.GetDescription(item), Value = ((int)item).ToString() });
                    }
                }

                ddlServiceChannel.Items.Add(new ListItem { Text = "全部", Value = "" });
                foreach (ServiceChannel item in Enum.GetValues(typeof(ServiceChannel)))
                {
                    ddlServiceChannel.Items.Add(new ListItem { Text = Helper.GetDescription(item), Value = ((int)item).ToString() });
                }

                
            }
            if (config.EnableSevenIsp)
            {
                enableseven.Visible = true;
            }
            this.Presenter.OnViewLoaded();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (config.EnableSevenIsp)
            {
                PageSize = Convert.ToInt32(ddlChangePage.SelectedValue);
            }
            else
            {
                PageSize = 99999;
            }
            

            if (Search != null)
            {
                Search(sender, new DataEventArgs<int>(0));
            }
            ucPager.ResolvePagerView(CurrentPage, true);
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (this.Export != null)
            {
                this.Export(this, e);
            }
        }
        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (this.Import != null)
            {
                this.Import(this, e);
            }
        }

        public void SetISPList(ViewVbsInstorePickupCollection data)
        {
            divISPList.Visible = true;
            rptISPList.DataSource = data;
            rptISPList.DataBind();
        }


        protected void rptISPList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewVbsInstorePickup)
            {
                ViewVbsInstorePickup dataItem = (ViewVbsInstorePickup)e.Item.DataItem;
                ((Label)e.Item.FindControl("lbServiceChannel")).Text = Helper.GetDescription((ServiceChannel)dataItem.ServiceChannel);
                ((Label)e.Item.FindControl("lbCreateTime")).Text = dataItem.CreateTime.ToString("yyyy/MM/dd HH:mm");
                ((Label)e.Item.FindControl("lbStatus")).Text = Helper.GetDescription((ISPStatus)dataItem.Status);
                ((HyperLink)e.Item.FindControl("hlLink")).NavigateUrl = "~/sal/SellerContent.aspx?sid=" + dataItem.SellerGuid.ToString() + "#aISPManage";

            }
        }

        protected int GetCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetCount != null)
            {
                OnGetCount(this, e);
            }
            return e.Data;
        }

        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChanged != null)
            {
                this.PageChanged(this, new DataEventArgs<int>(pageNumber));
            }
        }

        public void ShowMessage(string msg)
        {
            string script = string.Format("alert('{0}');", msg);
            Page.ClientScript.RegisterStartupScript(GetType(), "alert", script, true);
        }
    }
}
﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Sal
{
    public partial class ProposalProgress : RolePage, ISalesProposalProgressView
    {
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region props
        private SalesProposalProgressPresenter _presenter;
        public SalesProposalProgressPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public string CrossDeptTeam
        {
            get
            {
                ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(UserName));
                if (!string.IsNullOrEmpty(emp.CrossDeptTeam))
                    return emp.CrossDeptTeam;
                else if (emp.TeamNo == null)
                    return emp.DeptId + "[0]";
                else
                    return emp.DeptId + "[" + emp.TeamNo + "]";
            }
        }

        public int EmpUserId
        {
            get
            {
                ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(UserName));
                return emp.UserId;
            }
        }
        public int Monthly
        {
            get
            {
                int rtnMonth = default(int);
                int.TryParse(txtMonth.Text, out rtnMonth);
                return rtnMonth;
            }
        }
        #endregion

        #region event
        public event EventHandler<DataEventArgs<string>> MonthChanged;
        #endregion

        #region method
        public void SetProposalProgress(IEnumerable<ViewProposalSeller> pros)
        {
            DateTime max = DateTime.MaxValue;
            rptMonthProposal.DataSource = pros.GroupBy(x => x.OrderTimeS.HasValue ? new DateTime(x.OrderTimeS.Value.Year, x.OrderTimeS.Value.Month, 1) :
                    new DateTime(max.Year, max.Month, 1), y => y).OrderByDescending(x => x.Key);
            rptMonthProposal.DataBind();
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Presenter.OnViewInitialized();
                EnabledSettings(UserName);
            }
            _presenter.OnViewLoaded();
        }

        protected void rptMonthProposal_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is IGrouping<DateTime, ViewProposalSeller>)
            {
                IGrouping<DateTime, ViewProposalSeller> dataItem = (IGrouping<DateTime, ViewProposalSeller>)e.Item.DataItem;
                Literal liMonth = (Literal)e.Item.FindControl("liMonth");
                Repeater rptProposal = (Repeater)e.Item.FindControl("rptProposal");
                if (dataItem.Key.Year == DateTime.MaxValue.Year)
                {
                    liMonth.Text = "未排檔";
                }
                else
                {
                    liMonth.Text = dataItem.Key.ToString("yyyy - MM");
                }
                rptProposal.DataSource = dataItem.OrderByDescending(x => x.OrderTimeS);
                rptProposal.DataBind();
            }
        }

        protected void rptProposal_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewProposalSeller)
            {
                ViewProposalSeller dataItem = (ViewProposalSeller)e.Item.DataItem;
                HyperLink hkProposalId = (HyperLink)e.Item.FindControl("hkProposalId");
                Literal liOrderTimeS = (Literal)e.Item.FindControl("liOrderTimeS");
                HyperLink hkSellerName = (HyperLink)e.Item.FindControl("hkSellerName");
                HyperLink hkBusinessHour = (HyperLink)e.Item.FindControl("hkBusinessHour");
                Image imgFlag = (Image)e.Item.FindControl("imgFlag");
                Literal liSellerTempStatus = (Literal)e.Item.FindControl("liSellerTempStatus");
                Repeater rptStatus = (Repeater)e.Item.FindControl("rptStatus");
                hkProposalId.Text = dataItem.Id.ToString();
                if (dataItem.ProposalSourceType == (int)ProposalSourceType.Original)
                {
                    hkProposalId.NavigateUrl = ResolveUrl("~/sal/ProposalContent.aspx?pid=" + dataItem.Id);
                }
                else
                {
                    hkProposalId.NavigateUrl = ResolveUrl("~/sal/proposal/house/proposalcontent?pid=" + dataItem.Id);
                }
                    
                if (dataItem.OrderTimeS.HasValue)
                {
                    liOrderTimeS.Text = dataItem.OrderTimeS.Value.ToString("MM/dd");
                }
                hkSellerName.Text = "【" + dataItem.SellerName + "】";
                hkSellerName.NavigateUrl = ResolveUrl("~/sal/SellerContent.aspx?sid=" + dataItem.SellerGuid);
                if (dataItem.BusinessHourGuid.HasValue)
                {
                    ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealGetByPid(dataItem.Id);

                    if (multiDeals != null && multiDeals.Count > 0)
                    {
                        List<string> content = new List<string>();
                        foreach (var item in multiDeals)
                        {
                            content.Add(string.Format("{0}折！只要{1}元，即可購得原價{2}元【{3}】{4}", item.OrigPrice.Equals(0) ? 0 : Math.Round((item.ItemPrice / item.OrigPrice) * 10, 1), item.ItemPrice.ToString("F0"), item.OrigPrice.ToString("F0"), dataItem.BrandName, item.ItemName.Length > 10 ? item.ItemName.Substring(0, 10) + "..." : item.ItemName));
                        }
                        hkBusinessHour.Text = string.Join("<br />", content);
                    }
                    else
                    {
                        hkBusinessHour.Text = "(無)";
                    }
                    if (dataItem.ProposalSourceType == (int)ProposalSourceType.Original)
                    {
                        hkBusinessHour.NavigateUrl = ResolveUrl("~/sal/BusinessContent.aspx?bid=" + dataItem.BusinessHourGuid);
                    }
                    else
                    {
                        hkBusinessHour.NavigateUrl = ResolveUrl("~/sal/proposal/house/proposalcontent?pid=" + dataItem.Id);
                    }                    
                }
                liSellerTempStatus.Text = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (SellerTempStatus)dataItem.TempStatus);
                imgFlag.ImageUrl = ResolveUrl(dataItem.TempStatus == (int)SellerTempStatus.Completed ? "~/Themes/PCweb/images/Tick.png" : "~/Themes/PCweb/images/x.png");
                rptStatus.DataSource = SellerFacade.GetProposalFlags(dataItem).Where(x => x.Key != ProposalStatus.Apply);
                rptStatus.DataBind();
            }
        }

        protected void rptStatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<ProposalStatus, Dictionary<Enum, bool>>)
            {
                KeyValuePair<ProposalStatus, Dictionary<Enum, bool>> dataItem = (KeyValuePair<ProposalStatus, Dictionary<Enum, bool>>)e.Item.DataItem;

                Literal liFlagTitle = (Literal)e.Item.FindControl("liFlagTitle");
                Repeater rptFlag = (Repeater)e.Item.FindControl("rptFlag");
                liFlagTitle.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, dataItem.Key);
                rptFlag.DataSource = dataItem.Value.ToDictionary(x => x, y => dataItem.Key);
                rptFlag.DataBind();
            }
        }

        protected void rptFlag_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<KeyValuePair<Enum, bool>, ProposalStatus>)
            {
                KeyValuePair<KeyValuePair<Enum, bool>, ProposalStatus> dataItem = (KeyValuePair<KeyValuePair<Enum, bool>, ProposalStatus>)e.Item.DataItem;
                Image imgFlag = (Image)e.Item.FindControl("imgFlag");
                Literal liFlag = (Literal)e.Item.FindControl("liFlag");
                KeyValuePair<Type, string> flagType = SellerFacade.GetProposalFlagsTypeByStatus(dataItem.Value);
                if (flagType.Key != null)
                {
                    string text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, dataItem.Key.Key);
                    liFlag.Text = text;
                    imgFlag.ImageUrl = ResolveUrl(dataItem.Key.Value ? "~/Themes/PCweb/images/Tick.png" : "~/Themes/PCweb/images/x.png");
                }
            }
        }
        protected void ddlMonth_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            txtMonth.Text = ((DropDownList)sender).SelectedValue;
            if (this.MonthChanged != null)
            {
                this.MonthChanged(sender, new DataEventArgs<string>(txtMonth.Text));
            }
        }
        #endregion

        #region private method

        private void EnabledSettings(string username)
        {
            ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(username));
            if (emp.IsLoaded)
            {
                if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.ReadAll))
                {

                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(username, SystemFunctionType.CrossDeptTeam))
                {
                    if (string.IsNullOrEmpty(emp.CrossDeptTeam) && emp.TeamNo == null)
                    {
                        ShowMessage("身份權限未明，無法查看資料，請洽產品或技術人員協助");
                    }
                }
            }
            else
            {
                ShowMessage("查無員工資料，請重新檢視員工資料維護設定。");
            }

        }

        public void ShowMessage(string msg)
        {
            string script = string.Format("alert('{0}');", msg);
            Page.ClientScript.RegisterStartupScript(GetType(), "alert", script, true);
        }

        #endregion

    }
}
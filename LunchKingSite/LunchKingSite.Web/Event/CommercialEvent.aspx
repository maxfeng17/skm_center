﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="CommercialEvent.aspx.cs" Inherits="LunchKingSite.Web.Event.CommercialEvent" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href="../Themes/default/images/17Life/G2/PPImage/PPWeb.css" type="text/css"
        rel="stylesheet" />
    <%--<link href="../Themes/default/style/RDL.css" rel="stylesheet" type="text/css" />--%>
    <link href="../Themes/default/images/17Life/G2/skm/skm.css" rel="stylesheet" type="text/css" />
    <%--<link href="../Themes/default/images/17Life/G2/skm/skm-RDL.css" rel="stylesheet" type="text/css" />--%>

    <script type='text/javascript'>

        function slidingMenuToggle() {
            $("#wrap").toggleClass("mbe-menu-show");
        }
        $(function () {
            $(".mbe-menu-btn").click(slidingMenuToggle);
            $(".mbe-channel").click(slidingMenuToggle);
        });


        $(document).ready(function () {
            $(window).scroll(function () {
                $('[id*=hidScroll]').val(typeof window.pageYOffset != 'undefined' ? window.pageYOffset : document.documentElement.scrollTop);
                if ($(window).scrollTop() >= 650) {
                    $('#skmOutsideAd').addClass('skm-ad-fixed');
                } else {
                    $('#skmOutsideAd').removeClass('skm-ad-fixed');
                }
            });


        });

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <asp:HiddenField ID="hidCommercialName" runat="server" />

    <asp:Panel ID="divMain" runat="server" CssClass="center">
        <div class="ly-skmfarme">
            <uc1:Paragraph ID="pMain" ExternalCache="true" runat="server" />

            <div class="skm-areamenu-bar">
                <div class="skm-bar clearfix">
                    <asp:Repeater ID="rptCategory" runat="server">
                        <ItemTemplate>
                            <div class="skm-tn-a1-farm">
                                <a href="CommercialEvent.aspx?eventcode=<%= EventCode %>&commcode=<%= CommercialCode %>#cate<%# ((Category)Container.DataItem).Id %>" class="skm-areamenu-btn"><%# ((Category)Container.DataItem).Name %></a>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
        <asp:Repeater ID="rptCommercial" runat="server" OnItemDataBound="rptCommercial_ItemDataBound">
            <HeaderTemplate>
                <div class="ly-skmarea-deal">
            </HeaderTemplate>
            <ItemTemplate>
                <!--deal-->
                <div class="skm-area-deal clearfix" id="cate<%# ((KeyValuePair<KeyValuePair<int, EventPromo>, Dictionary<EventPromoItem, ViewPponDeal>>)(Container.DataItem)).Key.Key %>">
                    <div class="skm-area-dealblock">
                        <div class="skm-area-dealleft">
                            <asp:Literal ID="liTitle" runat="server"></asp:Literal>
                        </div>
                        <div class="skm-area-dealright">
                            <asp:Repeater ID="rptEventPromoItem" runat="server">
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <ItemTemplate>

                                    <a class="skm-deal" href='<%# "../" + ((KeyValuePair<EventPromoItem, ViewPponDeal>)(Container.DataItem)).Value.BusinessHourGuid %>' target="_blank">
                                        <div class="skm-dealpic">
                                            <img src="<%# ImageFacade.GetMediaPathsFromRawData(((KeyValuePair<EventPromoItem, ViewPponDeal>)(Container.DataItem)).Value.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty("../Themes/default/images/17Life/G2/skm/pic.png").First()%>" alt="" onerror="this.onerror=null;this.src='../Themes/default/images/17Life/G2/skm/pic.png';" />
                                        </div>
                                        <div class="st-skmdealtext"><%# ((KeyValuePair<EventPromoItem, ViewPponDeal>)(Container.DataItem)).Key.Title.Length <=18 ? ((KeyValuePair<EventPromoItem, ViewPponDeal>)(Container.DataItem)).Key.Title : ((KeyValuePair<EventPromoItem, ViewPponDeal>)(Container.DataItem)).Key.Title.Substring(0,18)+"..."  %></div>
                                    </a>

                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                            </asp:Repeater>

                            <div class="skm-deal-btn-fm clearfix">
                                <a class="st-skmmore" href='<%# "ExhibitionCommercial.aspx?catid=" + (int)EventPromoItemType.Vourcher + "&u=" + ((KeyValuePair<KeyValuePair<int, EventPromo>, Dictionary<EventPromoItem, ViewPponDeal>>)(Container.DataItem)).Key.Value.Url %>' target="_blank"><p style="color: white;">更多優惠券</p></a>
                                    <a class="st-skmmore" href='<%# "ExhibitionCommercial.aspx?catid=" + (int)EventPromoItemType.Ppon + "&u=" + ((KeyValuePair<KeyValuePair<int, EventPromo>, Dictionary<EventPromoItem, ViewPponDeal>>)(Container.DataItem)).Key.Value.Url %>' target="_blank"><p style="color: white;">更多團購券</p></a>
                                    
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
            <FooterTemplate>
                <div class="ly-skm-sponsors"></div>
                </div>                
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <asp:Panel ID="divNoEvent" runat="server" Visible="false">
        <div class="PPClose">
            <div class="PPSpace">
            </div>
            <a href="https://www.17life.com" class="BackBtn"></a>
        </div>
    </asp:Panel>
</asp:Content>

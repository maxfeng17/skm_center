﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.Event
{
    public partial class ExhibitionSubCategoryBar : System.Web.UI.UserControl
    {
        public List<ViewEventPromo> Item { get; set; }
        public EventPromo EventPromoData { get; set; }

        public string KeyListJSon { get; private set; }
        public string ValueListJson { get; private set; }


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public override void DataBind()
        {
            JsonSerializer json = new JsonSerializer();
            var keyList = new List<List<string>>();
            var valueList = new List<List<string>>();

            var groupCategory = Item.GroupBy(x => string.IsNullOrEmpty(x.Category) ? string.Empty : x.Category).OrderBy(x => x.Key);

            int groupIndex = 0;
            int subGroupIndex = 0;
            foreach (var group in groupCategory)
            {
                var subGroupCategory =
                    group.ToList()
                         .GroupBy(x => string.IsNullOrEmpty(x.SubCategory) ? string.Empty : x.SubCategory)
                         .OrderBy(x => x.Key);
                var subKeyList = new List<string>();
                var subValueList = new List<string>();
                foreach (var subGroup in subGroupCategory)
                {
                    string value = RegexReplaceSequence(subGroup.First().SubCategory);
                    string key = string.Empty;
                    if (!string.IsNullOrWhiteSpace(subGroup.First().SubCategory) && !string.IsNullOrWhiteSpace(value))
                    {
                        key = subGroup.First().SubCategory.Replace(value, string.Empty).Replace(".", "-");
                    }
                    subGroupIndex++;
                    subKeyList.Add(key);
                    subValueList.Add(value);
                }
                keyList.Add(subKeyList);
                valueList.Add(subValueList);
                groupIndex++;
            }
            KeyListJSon = json.Serialize(keyList);
            ValueListJson = json.Serialize(valueList);

            SetBarCSS();
        }

        protected string RegexReplaceSequence(string category)
        {
            return Regex.Replace(category, @"^[0-9]*\.[0-9]*", string.Empty);
        }

        private void SetBarCSS()
        {
            if (EventPromoData == null)
            {
                return;
            }
            string cssData = string.Empty;
            string backgroundOriginal = string.Empty;
            string backgroundHover = string.Empty;
            string backgroundActive = string.Empty;

            string fontOriginal = string.Empty;
            string fontHover = string.Empty;
            string fontActive = string.Empty;

            //文字顏色
            if (!string.IsNullOrWhiteSpace(EventPromoData.SubCategoryBtnFontOriginal))
            {
                fontOriginal = string.Format("color:#{0};",EventPromoData.SubCategoryBtnFontOriginal);
            }
            if (!string.IsNullOrWhiteSpace(EventPromoData.SubCategoryBtnFontHover))
            {
                fontHover = string.Format("color:#{0};",EventPromoData.SubCategoryBtnFontHover);
            }
            if (!string.IsNullOrWhiteSpace(EventPromoData.SubCategoryBtnFontActive))
            {
                fontActive = string.Format("color:#{0};",EventPromoData.SubCategoryBtnFontActive);
            }

            //底圖
            if (EventPromoData.SubCategoryShowImage)
            {
                if (!string.IsNullOrWhiteSpace(EventPromoData.SubCategoryBtnImageOriginal))
                {
                    backgroundOriginal =
                        string.Format(
                            ".nav-anchor a:link{{background-image: url({0});{1}}} .nav-anchor a:visited{{background-image: url({0});{1}}} ",
                            EventPromoData.SubCategoryBtnImageOriginal , fontOriginal);
                }
                if (!string.IsNullOrWhiteSpace(EventPromoData.SubCategoryBtnImageHover))
                {
                    backgroundHover = string.Format(".nav-anchor a:Hover{{background-image: url({0});{1}}} ",
                                                    EventPromoData.SubCategoryBtnImageHover , fontHover);
                }
                if (!string.IsNullOrWhiteSpace(EventPromoData.SubCategoryBtnImageActive))
                {
                    backgroundActive = string.Format(".nav-anchor a a:Active{{background-image: url({0});{1}}} .nav-anchor .current a{{background-image: url({0});{1}}}",
                                                     EventPromoData.SubCategoryBtnImageActive , fontActive);
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(EventPromoData.SubCategoryBtnColorOriginal))
                {
                    backgroundOriginal =
                        string.Format(
                            ".nav-anchor a:link{{background:#{0};{1}}} .nav-anchor a:visited{{background:#{0};{1}}} ",
                            EventPromoData.SubCategoryBtnColorOriginal,fontOriginal);
                }
                if (!string.IsNullOrWhiteSpace(EventPromoData.SubCategoryBtnColorHover))
                {
                    backgroundHover = string.Format(".nav-anchor a:Hover{{background:#{0};{1}}} ",
                                                    EventPromoData.SubCategoryBtnColorHover,fontHover);
                }
                if (!string.IsNullOrWhiteSpace(EventPromoData.SubCategoryBtnColorActive))
                {
                    backgroundActive = string.Format(".nav-anchor a a:Active{{background:#{0};{1}}} .nav-anchor .current a{{background:#{0};{1}}}",
                                                     EventPromoData.SubCategoryBtnColorActive,fontActive);
                }
            }

            cssData = string.Format("{0}{1}{2}", backgroundOriginal, backgroundHover, backgroundActive);
            if (!string.IsNullOrWhiteSpace(cssData))
            {
                cssData = string.Format("<style type='text/css'>{0}</style>", cssData);
            }

            categoryBarCSS.Text = cssData;
        }
    }
}
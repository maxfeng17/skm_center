﻿using System;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.Web.Event
{
    public partial class ExhibitionListMobile : BasePage
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public string OgTitle { set; get; }
        public string OgImage { set; get; }
        public string OgDescription { set; get; }
        public string FB_AppId
        {
            get
            {
                return config.FacebookApplicationId;
            }
        }
        private bool IsMobileBroswer
        {
            get
            {
                var userAgent = HttpContext.Current.Request.UserAgent;
                if ((!string.IsNullOrEmpty(userAgent)) &&
                    (userAgent.ToLower().Contains("iphone")
                        || (userAgent.ToLower().Contains("ipod")
                        || (userAgent.ToLower().Contains("android")
                        || (userAgent.ToLower().Contains("Blackberry"))))))
                {
                    return true;
                }
                return false;
            }
        }
        public int ThemeMainId
        {
            get
            {
                int themeMainId;
                return int.TryParse(Request["id"] ?? string.Empty, out themeMainId) ? themeMainId : 0;
            }
        }

        protected override void OnInit(System.EventArgs e)
        {
            base.OnInit(e);
            if (!IsMobileBroswer)
            {
                Response.Redirect("exhibitionlist.aspx" + Request.Url.Query);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EventListContent items = (EventListContent)Page.LoadControl("EventListContent.ascx");
                items.ShareArea = GetShareArea();
                items.themeMainId = ThemeMainId;
                items.RuleUrl = "ExhibitionListRuleMobile.aspx";
                items.DataBind();
                pan_EventList.Controls.Add(items);

                this.Page.Title  = items.title;
                OgTitle = items.FbTitle;
                OgImage = items.FBimage;
            }
        }

        private string GetShareArea()
        {
            string url = HttpUtility.UrlEncode(config.SiteUrl + Request.RawUrl);
            return string.Format(@"<ul>
                                        <li>
                                            <a href='javascript:void(0)' class='coupon_share_on'>
                                                <i class='fa fa-share-alt  fa-lg fa-fw'></i>
                                                <i class='fa fa-close  fa-lg fa-fw' style='display:none;'></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='{0}' target='_blank' class='coupon_share_fb' style='display:none;'>
                                                <img src='https://www.17life.com/images/17P/active/store/icon_fb.png'/>
                                            </a>
                                        </li>
                                        <li>
                                            <a href='{1}' target='_blank' class='coupon_share_line' style='display:none;'>
                                                <img src='https://www.17life.com/images/17P/active/store/icon_line.png'/>
                                            </a>
                                        </li>
                                    </ul>",
                                            string.Format("https://www.facebook.com/dialog/share?app_id={0}&display=popup&href={1}&redirect_uri={2}",
                                                            config.FacebookShareAppId, url, url
                                                        ),
                                            string.Format("http://line.naver.jp/R/msg/text/?{0}",
                                                            string.Format("{0}%0D%0A{1}", "@OgTitle", url)
                                                         )
                                    );
        }
    }
}
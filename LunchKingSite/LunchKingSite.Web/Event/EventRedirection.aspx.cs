﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.Event
{
    public partial class EventRedirection : System.Web.UI.Page, IEventRedirectionView
    {
        private EventRedirectionPresenter _presenter;
        public EventRedirectionPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get { return this._presenter; }
        }

        public string ReferenceId
        {
            get
            {
                string referenceId = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["eid"]) && Request.QueryString["eid"].Split(',').Length > 1)
                {
                    referenceId = Request.QueryString["eid"].Split(',')[1];
                }
                return referenceId;
            }
        }

        public string WebRoot
        {
            get
            {
                return WebUtility.GetSiteRoot();
            }
        }

        public Guid EventId
        {
            get
            {
                Guid bGuid = Guid.Empty;

                if (!string.IsNullOrEmpty(Request.QueryString["eid"]) && Request.QueryString["eid"].Split(',').Length > 1 && !String.IsNullOrEmpty(Request.QueryString["eid"].Split(',')[0]))
                {
                    try { bGuid = new Guid(Request.QueryString["eid"].Split(',')[0]); }
                    catch { }
                }

                return bGuid;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public void ShowMessage(string message, string goToUrl)
        {
            if (string.IsNullOrEmpty(goToUrl))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + message + "');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + message + "');window.location.href='" + goToUrl + "';", true);
            }
        }

        public void RedirectPage(string goToUrl)
        {
            if (!string.IsNullOrEmpty(goToUrl))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "window.location.href='" + goToUrl + "';", true);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                this.Presenter.OnViewInitialized();

            this.Presenter.OnViewLoaded();
        }

        public bool SetCookie(String value, DateTime expireTime, LkSiteCookie cookieField)
        {
            HttpCookie referenceCookie = CookieManager.NewCookie(cookieField.ToString("g"));

            referenceCookie.Value = value;
            referenceCookie.Expires = expireTime;
            //設定cookie值於使用者電腦
            try
            {
                Response.Cookies.Add(referenceCookie);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public void SetPageMeta(EventContent ec)
        {
            liF.Text = "<meta property='fb:app_id' content='" + ProviderFactory.Instance().GetConfig().FacebookApplicationId + "'/>";
            string ogTitle = ec.Title.TrimToMaxLength(60, "...");
            liT.Text = "<meta property='og:title' content='" + HttpUtility.HtmlEncode(ogTitle) + "'/>";
            liOI.Text = "<meta property='og:image' content='" + (String.IsNullOrEmpty(ec.ViewImg) ? string.Empty : ec.ViewImg) + "' />";
            liD.Text = "<meta property='og:description' content='" + Phrase.FacebookShareDescription + "' />";
            liI.Text = "<link rel='image_src' href='" + (String.IsNullOrEmpty(ec.ViewImg) ? string.Empty : ec.ViewImg) + "' />";
        }
    }
}
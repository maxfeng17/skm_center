﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="PremiumPromo.aspx.cs" Inherits="LunchKingSite.Web.Event.PremiumPromo" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.WebLib" %>

<asp:Content ID="cPremiumsPromoMeta" ContentPlaceHolderID="cphPponMeta" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href="../Themes/default/style/RDL.css" rel="stylesheet" type="text/css" />
    <%--<link href="//www.17life.com/images/17P/20140409-brandsAC/brandsAC.css" rel="stylesheet" type="text/css" />--%>
    <link href="//www.17life.com/images/17P/20140409-brandsAC/brandsAC-Normal.css" rel="stylesheet" type="text/css" />
    <link href="//www.17life.com/images/17P/20140409-brandsAC/brands-RDL.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .PPClose { width:100%; height:350px; background:url(<%=ResolveUrl("../Themes/PCweb/images/CloseBG.png")%>) no-repeat; display:block; }
        .PPSpace { width:100%; height:187px; display:block; }
        .BackBtn { width:286px; height:76px; background:url(<%=ResolveUrl("../Themes/PCweb/images/CloseBtn.png")%>); margin-left:445px; display:block; }
        .BackBtn:hover { background:url(<%=ResolveUrl("../Themes/default/images/17Life/G2/PPImage/CloseBtn_Hov.png")%>); }

.brandinfo { 
	width: 100%;
	height: 110px; 
	padding: 12px 0 7px;
	background: url(https://www.17life.com/images/17P/20140409-brandsAC/brands-2.jpg) repeat;
	border-top: 21px solid #e4548a;
	text-align: center;
	color: <%=BannerFontColor %>;
	font-size: 22px;
	font-family: "微軟正黑體";
	font-weight: bold;
}

.a_demo_two {
	letter-spacing: 0.1em;
	padding:5px 35px;
	position: relative;
	font-family: 'Open Sans', sans-serif;
	font-size:17px;
	font-family: "微軟正黑體";
	text-decoration:none;
	color:<%=ButtonFontColor %>;
	background: <%=ButtonBackgroundColor %>; 
	box-shadow: 0 2px 2px 1px #909090;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	-o-border-radius: 6px;
	border-radius: 6px;
	z-index: 5;
	top: 0;
	margin-bottom: 20px;
	 border: solid 3px #850048;
	 cursor: pointer;
}

.a_demo_two:hover{ color: <%=ButtonFontColor %>; text-decoration: none; background: <%=ButtonBackgroundHoverColor %>;}

.brad-shd a{
	color: <%=ButtonFontColor %>;
}
    </style>
    <script type="text/javascript" src="../Tools/js/jquery.cascadeAddress.js"></script>
    <script type='text/javascript'>

        $(document).ready(function () {
            var urlParams = {};
            (function () {
                var e,
              a = /\+/g,  // Regex for replacing addition symbol with a space
              r = /([^&=]+)=?([^&]*)/g,
              d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
              q = window.location.search.substring(1);
                while (e = r.exec(q)) {
                    urlParams[d(e[1])] = d(e[2]);
                }
            })();
            initCss();

            $("#<%= tbx_AwardName.ClientID %>").click(function () { $("#err_AwardName").hide(); });
            $("#<%= tbx_AwardMoblie.ClientID %>").click(function () { $("#err_AwardMobile").hide(); });
            $("#<%= tbx_AwardEMail.ClientID %>").click(function () { $("#err_AwardEMail").hide(); });
            $("#<%= tbx_AwardAddress.ClientID %>").click(function () { $("#err_AwardAddress").hide(); });
            $("#<%= cbx_RuleAgree.ClientID %>").click(function () { $("#err_RuleAgree").hide(); });

            $.cascadeAddress.setup('<%=WebUtility.GetSiteRoot()%>', function () {
                $.cascadeAddress.bind($('#<%= ddlCity.ClientID %>'), $('#<%= ddlArea.ClientID %>'), $('#<%= tbx_AwardAddress.ClientID %>'), $('#<%=hid_pcCityId.ClientID%>').val(), $('#<%=hid_CityId.ClientID%>').val(), $('#<%=hid_SubAddress.ClientID%>').val(), false);
            });



        });

        function initCss() {
            $('#wrap').attr("style", "<%=BgCss %>");
        }

        function checkData() {
            var isAllCheck = true;

            var isErrName = false;
            var isErrEMail = false;
            var isErrAddr = false;
            var isErrMobile = false;
            var isErrRuleAgree = false;

            if (!checkName()) {
                isAllCheck = false;
                isErrName = true;
            }

            if (!checkEMail()) {
                isAllCheck = false;
                isErrEMail = true;
            }

            if (!checkAddr()) {
                isAllCheck = false;
                isErrAddr = true;
            }

            if (!checkMobile()) {
                isAllCheck = false;
                isErrMobile = true;
            }
            if (!$("#<%= cbx_RuleAgree.ClientID %>").is(':checked')) {
                $("#err_RuleAgree").show();
                isErrRuleAgree = true;
                isAllCheck = false;
            }

            var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
            if (isErrName) {
                $body.animate({ scrollTop: $("#err_AwardName").offset().top }, 800);
            } else if (isErrMobile) {
                $body.animate({ scrollTop: $("#err_AwardMobile").offset().top }, 800);
            } else if (isErrEMail) {
                $body.animate({ scrollTop: $("#err_AwardEMail").offset().top }, 800);
            } else if (isErrAddr) {
                $body.animate({ scrollTop: $("#err_AwardAddress").offset().top }, 800);
            } else if (isErrRuleAgree) {
                $body.animate({ scrollTop: $("#err_RuleAgree").offset().top }, 800);
            }


            return isAllCheck;
        }

        function checkName() {
            var name = $("#<%= tbx_AwardName.ClientID %>").val();
            if (name == '') {
                $("#err_AwardName").show();
                return false;
            }
            return true;
        }

        function checkEMail() {
            var mail = $("#<%= tbx_AwardEMail.ClientID %>").val();
            if (mail == '') {
                $("#err_AwardEMail").show();
                return false;
            }
            return true;
        }


        function checkMobile() {
            var phone = $("#<%= tbx_AwardMoblie.ClientID %>").val();
            if (phone != '') {
                var tenNumber = /^[0][9]\d{8}$/;
                if (tenNumber.test(phone)) {
                    return true;
                }
            }
            $("#err_AwardMobile").show();
            return false;
        }

        function checkAddr() {
            var selectedcity = $("#<%= ddlCity.ClientID %>").children(":selected").html();
            var selectedArea = $("#<%= ddlArea.ClientID %>").children(":selected").html();
            var addr = $("#<%= tbx_AwardAddress.ClientID %>").val();
            if (addr == '' || selectedcity == "請選擇" || (selectedArea == "請選擇" || selectedArea == "")) {
                $("#err_AwardAddress").show();
                return false;
            }
            var s = addr.trim();
            if (s.length == 0) {
                $("#err_AwardAddress").show();
                return false;
            }

            var chineseregx = new RegExp(/[\u4e00-\u9fa5]/gi);
            if (!s.charAt(0).match(chineseregx)) {
                $("#err_AwardAddress").show();
                return false;
            }

            var AwardAddr = $("#<%= ddlCity.ClientID %>").children(":selected").html()
                + $("#<%= ddlArea.ClientID %>").children(":selected").html()
                + $("#<%= tbx_AwardAddress.ClientID %>").val();
            $("#<%= hid_AwardAddress.ClientID %>").val(AwardAddr);

            return true;
        }

    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <script type="text/javascript">
        Sys.Application.add_load(initCss);
    </script>
    <asp:HiddenField ID="hid_AwardAddress" runat="server" />
    <asp:HiddenField ID="hid_pcCityId" runat="server" />
    <asp:HiddenField ID="hid_CityId" runat="server" />
    <asp:HiddenField ID="hid_SubAddress" runat="server" />

    <asp:panel ID="pl_Main" runat="server" CssClass="brandAC-box" Visible="True">
        <div class="brandTOP">
            <asp:Literal ID="li_MainBanner" runat="server"></asp:Literal>
            <%--<img src="https://www.17life.com/images/17P/20140409-brandsAC/brands-1.jpg" />--%>
        </div>
        <div class="brandinfo">
            <asp:Literal ID="lit_Message" runat="server"></asp:Literal>
        </div>
        <div class="brandcont">
            <div class="brandcotcen">
                <!--panel_login-->
                <asp:Panel ID="pl_Login" runat="server" CssClass="barndcotTbox" Visible="False">
                    <div class="btext">請先登入17Life才能領取喔!</div>
                    <div class="brad-shd">
                        <asp:LinkButton ID="lb_login" runat="server" CssClass="a_demo_two"  OnClick="lb_login_Click">登入</asp:LinkButton>
                    </div>
                </asp:Panel>
                <!--App連結-->
                <asp:Panel ID="pl_AppAd" runat="server" CssClass="barndcotTbox" Visible="False">
                    <a target="_blank" href="https://www.17life.com/ppon/promo.aspx?cn=app">
                        <div class="barapp">
                            <img src="https://www.17life.com/images/17P/20140409-brandsAC/app.jpg" />
                            <div class="barappTT">
                                點此免費下載17Life app<br />
                                隨時得知最新好康活動
                            </div>
                        </div>
                    </a>
                </asp:Panel>
                <!--panel_登入、已索取-->
                <asp:Panel ID="pl_OverUserAmount" runat="server" CssClass="barndcotTbox" Visible="False">
                    <div class="btext">一人限索取一次喔~</div>
                    <a target="_blank" href="https://www.17life.com/ppon/promo.aspx?cn=app">
                        <div class="barapp">
                            <img src="https://www.17life.com/images/17P/20140409-brandsAC/app.jpg" />
                            <div class="barappTT">
                                點此免費下載17Life app<br />
                                隨時得知最新好康活動
                            </div>
                        </div>
                    </a>
                </asp:Panel>
                <!--panel_登入、未索取-->
                <asp:Panel ID="pl_ApplyUserInfo" runat="server" CssClass="barndcotTbox" Visible="False">
                    <div class="barsFillin">
                        <div class="baform-unit">
                            <div class="bafu-tit">姓名：</div>
                            <asp:TextBox ID="tbx_AwardName" runat="server" CssClass="bafuinput" MaxLength="50"></asp:TextBox>
                            <div id="err_AwardName" class="bafu-warg bafu-left" style="display: none;">請填寫正確姓名</div>
                        </div>
                        <div class="baform-unit">
                            <div class="bafu-tit">手機：</div>
                            <asp:TextBox ID="tbx_AwardMoblie" runat="server" CssClass="bafuinput" MaxLength="25"></asp:TextBox>
                            <div id="err_AwardMobile" class="bafu-warg bafu-left" style="display: none;">請填寫正確手機</div>
                        </div>
                        <div class="baform-unit">
                            <div class="bafu-tit">e-mail：</div>
                            <asp:TextBox ID="tbx_AwardEMail" runat="server" CssClass="bafuinput" MaxLength="100"></asp:TextBox>
                            <div id="err_AwardEMail" class="bafu-warg bafu-left" style="display: none;">請填寫正確e-mail</div>
                        </div>
                        <div class="baform-unit">
                            <div class="bafu-tit">收件地址：</div>
                            <div class="bafuipBox">
                                
                                <asp:DropDownList ID="ddlCity" runat="server" CssClass="bafusele">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlArea" runat="server" CssClass="bafusele">
                                </asp:DropDownList>
                                <asp:TextBox ID="tbx_AwardAddress" runat="server" CssClass="bafuinput" MaxLength="40"></asp:TextBox>
                                <div id="err_AwardAddress" class="bafu-warg bafu-left rdl-wag-show" style="display: none;">請填寫正確地址</div>
                            </div>
                        </div>

                        <div class="barbox">
                            <asp:CheckBox ID="cbx_RuleAgree" runat="server" CssClass="barHook" Checked="False" />
                            <div class="barAgree">
                                我同意提供上述之個人資料予馬來西亞商食益補國際股份有限
公司台灣分公司(以下稱白蘭氏)，並同意白蘭氏及關係企業得為推
銷產品、市場調查、客戶服務、帳款收取、資料儲存與整理等用途
蒐集、處理及利用（包括國際傳輸）本人之個人資料，利用之期
間、地區、對象及方式均不受任何限制，並得委託第三人為前述之
行為。本人了解本人有權請求查詢、閱覽、製給複製本、補充或更
正、請求停止蒐集、處理或利用及刪除本人個人資料，本人了解，
若本人行使前述權利，本人將不能繼續獲得白蘭氏所提供之各項服
務與優惠措施。詳細隱私權政聲明，隱私權政策與法律條款請見<a target="_blank" href="http://www.brands.com.tw">http://www.brands.com.tw</a>  。
                            </div>
                        </div>

                        <div class="brad-shd">
                            <asp:LinkButton ID="lb_Send" runat="server" CssClass="a_demo_two" OnClientClick="return checkData();" OnClick="lb_Send_Click">確認送出</asp:LinkButton>
                        </div>
                        <div id="err_RuleAgree" class="baform-unit bafu-warg" style="display: none;">請詳閱相關隱私權政策</div>

                    </div>
                </asp:Panel>
                <!--panel_索取成功-->
                <asp:Panel ID="pl_Success" runat="server" CssClass="barndcotTbox" Visible="False">
                    <div class="btext">恭喜您！已索取成功！</div>
                    
                        <asp:Literal ID="li_ApplySuccessContent" runat="server"></asp:Literal>
                    
                    <a target="_blank" href="https://www.17life.com/ppon/promo.aspx?cn=app">
                        <div class="barapp">
                            <img src="https://www.17life.com/images/17P/20140409-brandsAC/app.jpg" />
                            <div class="barappTT">
                                點此免費下載17Life app<br />
                                隨時得知最新好康活動
                            </div>
                        </div>
                    </a>
                </asp:Panel>
                <!--panel_索取失敗-->
                <asp:Panel ID="pl_Error" runat="server" CssClass="barndcotTbox" Visible="False">
                    <div class="btext">目前系統忙碌中，請重新索取~</div>
                    
                        <div class="brad-shd">
                            <a href="<%=Request.Url.AbsoluteUri.ToString() %>" class="a_demo_two">重新填單</a>
                            
                        </div>                    

                    <a target="_blank" href="https://www.17life.com/ppon/promo.aspx?cn=app">
                        <div class="barapp">
                            <img src="https://www.17life.com/images/17P/20140409-brandsAC/app.jpg" />
                            <div class="barappTT">
                                點此免費下載17Life app<br />
                                隨時得知最新好康活動
                            </div>
                        </div>
                    </a>
                </asp:Panel>
                <!--活動說明-->
                <asp:Literal ID="lit_Description" runat="server" Visible="False"></asp:Literal>
            </div>
        </div>

    </asp:panel>
    
    <asp:Panel ID="pl_PremiumsExpire" runat="server" Visible="false">
        <div class="PPClose">
            <div class="PPSpace">
            </div>
            <a href="https://www.17life.com" class="BackBtn"></a>
        </div>
    </asp:Panel>



</asp:Content>

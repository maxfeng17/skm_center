﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.Event
{
    public partial class ExhibitionItem : System.Web.UI.UserControl
    {
        public List<ViewEventPromo> Items { get; set; }
        public EventPromo EventPromoData { get; set; }
        public List<IGrouping<string, ViewEventPromo>> SubGroupList { get; private set; }
        public string ItemId { get; set; }
        public bool IsEnablePager { get; set; }
        public int PageSize { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public override void DataBind()
        {
            //已售完檔次自動排序最後
            int piinlifeCityId = PponCityGroup.DefaultPponCityGroup.Piinlife.CityId;
            SubGroupList = Items.Where(x => !x.CityList.Contains(piinlifeCityId.ToString())).GroupBy(x => x.SubCategory).OrderBy(x => x.Key).ToList();
            rpt_mainItem.DataSource = SubGroupList;
            rpt_mainItem.DataBind();
        }

        protected void rpt_MainItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var dataItem = ((IGrouping<string, ViewEventPromo>) e.Item.DataItem);
                if (SubGroupList.Count > 1)
                {
                    string style = SubCategoryTitleStyle();
                    ViewEventPromo eventPromo = dataItem.First();
                    Literal headerData = (Literal)e.Item.FindControl("headerData");
                    Literal footerData = (Literal)e.Item.FindControl("footerData");
                    string subCategory = Regex.Replace(eventPromo.SubCategory, @"^[0-9]*\.[0-9]*", string.Empty);
                    string subCategoryId = eventPromo.SubCategory;
                    if (!string.IsNullOrWhiteSpace(eventPromo.SubCategory))
                    {
                        subCategoryId = subCategoryId.Replace(subCategory, string.Empty).Replace(".", "-");
                        if (!string.IsNullOrWhiteSpace(subCategoryId))
                        {
                            subCategoryId = subCategoryId.Replace(".", "-");
                        }
                    }
                    headerData.Text = string.Format("<div id='section-{0}' class='clearfix'><h3 class='local-title' {1}>{2}</h3>", subCategoryId , style, subCategory);
                    footerData.Text = "</div>";
                }


                Repeater rpt_Item = (Repeater) e.Item.FindControl("rpt_Item");

                ConcurrentDictionary<ViewEventPromo, bool>  promoItems = new ConcurrentDictionary<ViewEventPromo, bool>();
                foreach (ViewEventPromo promo in ((IGrouping<string, ViewEventPromo>)e.Item.DataItem).ToList())
                {
                    bool isSoldOut = false;
                    IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(promo.BusinessHourGuid);
                    PponDealStage stage = deal.GetDealStage();
                    //已售完檔次自動排序最後
                    if ((stage == PponDealStage.CouponGenerated || stage == PponDealStage.ClosedAndFail ||
                         stage == PponDealStage.ClosedAndOn) ||
                        deal.OrderedQuantity >= ((deal.OrderTotalLimit.HasValue) ? deal.OrderTotalLimit : 0))
                    {
                        promo.Seq = 9999 + promo.Seq;
                        isSoldOut = true;
                    }

                    promoItems.GetOrAdd(promo, isSoldOut);
                }

                rpt_Item.DataSource = promoItems.OrderBy(x => x.Key.Seq);
                rpt_Item.DataBind();
            }
        }

        protected void rpt_Item_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (((KeyValuePair<ViewEventPromo, bool>)e.Item.DataItem).Key is ViewEventPromo) 
            {
                ViewEventPromo promo = (ViewEventPromo)((KeyValuePair<ViewEventPromo, bool>)e.Item.DataItem).Key;
                if (!string.IsNullOrEmpty(promo.DealPromoImage)) 
                {
                    Literal litDealPromoImage = (Literal)e.Item.FindControl("lit_DealPromoImage");
                    Literal litDealPromoImageDisableLink = (Literal)e.Item.FindControl("lit_DealPromoImageDisableLink");
                    litDealPromoImage.Text = litDealPromoImageDisableLink.Text = GetDealPromoImageHtmlTag(promo.DealPromoImage);
                }
            }
        }

        private string GetDealPromoImageHtmlTag(string dealPromoImage)
        {
            return (string.IsNullOrEmpty(dealPromoImage)) ? string.Empty : string.Format(@"<img src='{0}' alt='' />"
                , ImageFacade.GetMediaPath(dealPromoImage, MediaType.DealPromoImage));
        }

        private string SubCategoryTitleStyle()
        {
            string rtn = string.Empty;
            if (EventPromoData != null)
            {
                string bgColor = string.Empty;
                string fontColor = string.Empty;
                if (!string.IsNullOrWhiteSpace(EventPromoData.SubCategoryBgColor))
                {
                    bgColor = string.Format("background:#{0}; ", EventPromoData.SubCategoryBgColor);
                }
                if (!string.IsNullOrWhiteSpace(EventPromoData.SubCategoryFontColor))
                {
                    fontColor = string.Format("color:#{0}; ", EventPromoData.SubCategoryFontColor);
                }
                rtn = string.Format("{0}{1}", bgColor, fontColor);
                if (!string.IsNullOrWhiteSpace(rtn))
                {
                    rtn = string.Format("style='{0}'", rtn);
                }
            }
            return rtn;           
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Event
{
    public partial class PromoMembershipCard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["groupId"]))
            {
                Response.Redirect("/ppon/Promo.aspx?cn=17APP&groupId=" + Request.QueryString["groupId"]);
            }
        }
    }
}
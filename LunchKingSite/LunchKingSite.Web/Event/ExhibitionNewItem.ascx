﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExhibitionNewItem.ascx.cs"
    Inherits="LunchKingSite.Web.Event.ExhibitionNewItem" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<div id="<%=ItemId %>" style="display: none;" class="cate">
    <%= IsEnablePager && Items.Count > PageSize ? "<div id='banner-slide'><ul class='bjqs'><li>" : string.Empty %>
    <asp:Repeater ID="rpt_Item" runat="server">
        <ItemTemplate>
            <asp:Panel ID="pan_EnableLink" runat="server" Visible="<%# ((ViewEventPromo)(Container.DataItem)).BusinessHourOrderTimeS<=DateTime.Now %>" CssClass="skm-Homedealfarm" >
                <a href='<%#"../" + ((ViewEventPromo)(Container.DataItem)).BusinessHourGuid + (string.IsNullOrEmpty(((ViewEventPromo)(Container.DataItem)).Cpa) ? string.Empty : ("/" + ((ViewEventPromo)(Container.DataItem)).Cpa))%>'
                    target="_blank">
    
      <div class="skm-Homedealpic">
        <img src="<%# ImageFacade.GetMediaPathsFromRawData(((ViewEventPromo)(Container.DataItem)).ImagePath, MediaType.PponDealPhoto).DefaultIfEmpty("https://www.17life.com/images/17P/20130914-Shin/290X162.jpg").First()%>"  onerror="this.onerror=null;this.src='https://www.17life.com/images/17P/20130914-Shin/290X162.jpg';" />
        <div class="skm-Homedealtext">
          <div class="skm-hodeal-title"><%# ((ViewEventPromo)(Container.DataItem)).Title%></div>
          <div class="skm-smallttxt"><%# ((ViewEventPromo)(Container.DataItem)).Description%></div>
          <div class="skm-btnfarm clearfix" style='<%# ((ViewEventPromo)(Container.DataItem)).BusinessHourOrderTimeS <= DateTime.Now ? " display:block": " display:none"%>'>
            <div class="skm-pric">售價：$<%# ((ViewEventPromo)(Container.DataItem)).ItemPrice.ToString("F0")%></div>
            <input type="button" class="skm-buybtn" value="馬上買">
          </div>
          <div class="skm-btnfarm clearfix" style='<%# ((ViewEventPromo)(Container.DataItem)).BusinessHourOrderTimeS > DateTime.Now ? " display:block": " display:none"%>'>
              <div class="skm-pric">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%# ((ViewEventPromo)(Container.DataItem)).BusinessHourOrderTimeS.ToString("MM/dd")%>&nbsp;開賣敬請期待！</div>
          </div>    
        </div>
      </div>
    
                </a>
            </asp:Panel>
            <asp:Panel ID="pan_DisableLink" runat="server" Visible="<%# ((ViewEventPromo)(Container.DataItem)).BusinessHourOrderTimeS>DateTime.Now %>" CssClass="skm-Homedealfarm">
                
    
      <div class="skm-Homedealpic">
        <img src="<%# ImageFacade.GetMediaPathsFromRawData(((ViewEventPromo)(Container.DataItem)).ImagePath, MediaType.PponDealPhoto).DefaultIfEmpty("https://www.17life.com/images/17P/20130914-Shin/290X162.jpg").First()%>"  onerror="this.onerror=null;this.src='https://www.17life.com/images/17P/20130914-Shin/290X162.jpg';" />
        <div class="skm-Homedealtext">
          <div class="skm-hodeal-title"><%# ((ViewEventPromo)(Container.DataItem)).Title%></div>
          <div class="skm-smallttxt"><%# ((ViewEventPromo)(Container.DataItem)).Description%></div>
          <div class="skm-btnfarm clearfix" style='<%# ((ViewEventPromo)(Container.DataItem)).BusinessHourOrderTimeS <= DateTime.Now ? " display:block": " display:none"%>'>
            <div class="skm-pric">售價：$<%# ((ViewEventPromo)(Container.DataItem)).ItemPrice.ToString("F0")%></div>
            <input type="button" class="skm-buybtn" value="馬上買">
          </div>
          <div class="skm-btnfarm clearfix" style='<%# ((ViewEventPromo)(Container.DataItem)).BusinessHourOrderTimeS > DateTime.Now ? " display:block": " display:none"%>'>
              <div class="skm-pric">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%# ((ViewEventPromo)(Container.DataItem)).BusinessHourOrderTimeS.ToString("MM/dd")%>&nbsp;開賣敬請期待！</div>
          </div>    
        </div>
      </div>
    
            </asp:Panel>
            <%# IsEnablePager ? (Items.Count > PageSize ? ((Items.Count == (Container.ItemIndex + 1) && Items.Count % PageSize == 0) ? string.Empty : ((Container.ItemIndex + 1) % PageSize == 0 ? "</li><li>" : string.Empty)) : string.Empty) : string.Empty %>
        </ItemTemplate>
    </asp:Repeater>
    <%= IsEnablePager && Items.Count > PageSize  ? "</li></ul></div>" : string.Empty %>
    <div class="clear">
    </div>
</div>

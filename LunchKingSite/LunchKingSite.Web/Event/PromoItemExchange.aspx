﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="PromoItemExchange.aspx.cs" Inherits="LunchKingSite.Web.User.PromoItemExchange" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href="../Themes/default/images/17Life/G2/PPB.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/default/style/RDL.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="mc-content">
        <h1>
            <asp:Literal ID="liEventName" runat="server"></asp:Literal></h1>
        <hr class="header_hr" />
        <asp:Panel ID="divExchange" runat="server" Height="100px" Visible="false">
            請輸入活動序號：<asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
            <asp:Button ID="btnExchange" runat="server" Text=" 確 認 " OnClick="CouponExchange" />&nbsp;&nbsp;
            <asp:Button ID="btnShowExchangeInfo" runat="server" Text="查詢兌換紀錄" OnClick="ShowExchangeInfo" />
            <br />
            <asp:Label ID="lab_Message" runat="server"></asp:Label>
        </asp:Panel>
        <asp:Panel ID="divExchangeDataList" runat="server" Visible="false">
            【<asp:Literal ID="liEvent" runat="server"></asp:Literal>】兌換紀錄
            <div id="mc-table">
                <asp:Repeater ID="rpt_dataList" runat="server">
                    <HeaderTemplate>
                        <table width="920px" border="0" cellspacing="0" cellpadding="0" id="DiscountCodeTable" class="rd-table">
                            <thead>
                                <tr class="rd-C-Hide">
                                    <th class="OrderDate">期間
                                    </th>
                                    <th class="OrderExp">成功兌換數量
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="mc-tableContentITEM">
                            <td class="OrderDate">
                                <span class="rd-Detailtitle">期間：</span>
                                <%# ((KeyValuePair<string, int>)(Container.DataItem)).Key%>
                            </td>
                            <td class="OrderExp">
                                <span class="rd-Detailtitle">成功兌換數量：</span>
                                <%# ((KeyValuePair<string, int>)(Container.DataItem)).Value%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <div class="clearfix">
            </div>
            <br />
            <asp:Button ID="btnBack" runat="server" Text="回上一頁" OnClick="BackToExchange" />
        </asp:Panel>
    </div>
</asp:Content>

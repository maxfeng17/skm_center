﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;

namespace LunchKingSite.Web.Event
{
    public partial class ExhibitionKind : BasePage, IExhibition
    {
        #region property

        private ExhibitionPresenter _presenter;

        public ExhibitionPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string Url
        {
            get
            {
                return string.IsNullOrEmpty(Request.QueryString["u"]) ? string.Empty : Request.QueryString["u"];
            }
        }

        public string Preview
        {
            get
            {
                return string.IsNullOrEmpty(Request.QueryString["p"]) ? string.Empty : Request.QueryString["p"];
            }
        }

        public string BgCss
        {
            get;
            set;
        }

        public string MainPic
        {
            set
            {
                liMainPic.Text = HttpUtility.HtmlDecode(value);
            }
        }

        public string BtnActive
        {
            get;
            set;
        }

        public string BtnOriginal
        {
            get;
            set;
        }

        public string BtnHover
        {
            get;
            set;
        }

        public string BtnFontColor
        {
            get;
            set;
        }

        public int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(Page.User.Identity.Name);
            }
        }

        public int EventId
        {
            get;
            set;
        }

        public string Rsrc
        {
            get;
            set;
        }

        public string EventTitle
        {
            get;
            set;
        }

        #region SEO Keyword

        public string SeoKeyword
        {
            get;
            set;
        }
        public string SeoDescription
        {
            get;
            set;
        }
        #endregion

        #region The Open Graph protocol

        public string FB_AppId
        {
            get
            {
                return ProviderFactory.Instance().GetConfig().FacebookApplicationId;
            }
        }
        /// <summary>
        /// meta og:title content
        /// </summary>
        public string OgTitle { set; get; }
        /// <summary>
        /// meta og:url content
        /// </summary>
        public string OgUrl { set; get; }
        /// <summary>
        /// meta og:description content
        /// </summary>
        public string OgDescription { set; get; }
        /// <summary>
        /// meta og:image content
        /// </summary>
        public string OgImage { set; get; }
        /// <summary>
        /// link rel=canonical href
        /// </summary>
        public string LinkCanonicalUrl { set; get; }
        /// <summary>
        /// link rel=image_src href
        /// </summary>
        public string LinkImageSrc { set; get; }
        /// <summary>
        /// 團購版型
        /// </summary>
        public EventPromoTemplateType TemplateType { set; get; }

        #endregion

        /// <summary>
        /// 啟用行動版商品主題活動頁
        /// </summary>
        public bool EnableMobileEventPromo { set; get; }

        /// <summary>
        /// 是否為策展活動
        /// </summary>
        public bool IsCuration { set; get; }
        #endregion property

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
            }
            this.Presenter.OnViewLoaded();
            Page.MetaDescription = string.IsNullOrEmpty(SeoDescription) ? Page.MetaDescription : SeoDescription;
            Page.MetaKeywords = string.IsNullOrEmpty(SeoKeyword) ? Page.MetaKeywords : SeoKeyword;
        }

        #region local method

        protected string RegexReplaceSequence(string category)
        {
            return Regex.Replace(category, @"^[0-9]*\.", string.Empty);
        }

        #endregion local method

        #endregion page

        #region method

        public void SetCategory(EventPromoItemCollection list)
        { }
        public void SetAllCategory(EventPromo eventPromo, List<ViewEventPromo> dataList)
        { }
        public void ShowEventPromo(EventPromo eventPromo, List<ViewEventPromo> list)
        {
            pan_Main.Visible = pan_Sub.Visible = false;
            DateTime now = DateTime.Now;
            string bgstyle = "background: center 91px no-repeat ";
            if (list.Count > 0)
            {
                var first = list.First();
                this.Page.Title = first.EventTitle;
                if (!string.IsNullOrEmpty(first.BgPic))
                {
                    bgstyle += "url(" + list.First().BgPic + ") ";
                }

                if (!string.IsNullOrEmpty(first.BgColor))
                {
                    bgstyle += "#" + first.BgColor;
                }

                BgCss = bgstyle;
                MainPic = first.MainPic;

                if (!string.IsNullOrEmpty(first.BtnFontColor))
                {
                    BtnFontColor = "#" + first.BtnFontColor;
                }

                if (!string.IsNullOrEmpty(first.BtnActive))
                {
                    BtnActive = "url(" + first.BtnActive + ")";
                }

                if (!string.IsNullOrEmpty(first.BtnHover))
                {
                    BtnHover = "url(" + first.BtnHover + ")";
                }

                if (!string.IsNullOrEmpty(first.BtnOriginal))
                {
                    BtnOriginal = "url(" + first.BtnOriginal + ")";
                }

                if (string.IsNullOrEmpty(first.EventDescription) || first.EventDescription == "\r\n")
                {
                    pan_ActivityMeasures.Visible = false;

                }
                else
                {
                    pan_ActivityMeasures.Visible = true;
                    lit_Description.Text = first.EventDescription;
                }

                List<ViewEventPromo> data = new List<ViewEventPromo>();
                var group_category = list.GroupBy(x => string.IsNullOrEmpty(x.Category) ? string.Empty : x.Category).OrderBy(x => x.Key);

                if (group_category.Count() == 1 && string.IsNullOrEmpty(group_category.First().Key))
                {
                    pan_Category.Visible = false;
                }

                else
                {
                    rpt_Category.DataSource = group_category.Select(x => string.IsNullOrEmpty(x.Key) ? string.Empty : x.Key);
                    rpt_Category.DataBind();
                }

                if ((first.EventStatus && first.StartDate <= now && first.EndDate >= now && first.EventStatus) || Preview == "show_me_the_preview")
                {
                    int i = 0;
                    foreach (var inneritem in group_category)
                    {
                        ExhibitionItemKind items = (ExhibitionItemKind)Page.LoadControl("ExhibitionItemKind.ascx");
                        items.ItemId = "cate" + i;
                        items.Items = inneritem.ToList();
                        items.DataBind();
                        pan_Items.Controls.Add(items);
                        i++;
                    }
                    pan_Main.Visible = true;
                }
                else
                {
                    ShowEventExpire();
                }
            }
        }

        public void ShowVourcherEventPromo(Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> dataList, Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> lotteryList, Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> storeList)
        {
        }

        public void ShowPiinlifeEventPromo(List<ViewEventPromo> dataList)
        { }
        public void ShowPiinlifeEventPromo(EventPromo eventPromo, List<ViewEventPromo> dataList)
        { }

        public void ShowEventExpire()
        {
            pan_Sub.Visible = true;
        }

        #endregion method
    }
}
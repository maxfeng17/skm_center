﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExhibitionCategoryBar.ascx.cs" Inherits="LunchKingSite.Web.Event.ExhibitionCategoryBar" %>


<asp:PlaceHolder ID="pan_categoryBarCSS" runat="server">
    <asp:Literal runat="server" ID="categoryBarCSS"></asp:Literal>
</asp:PlaceHolder>

<asp:PlaceHolder runat="server" ID="pan_Category">
    <div class="ly-e-cop-area-menubar ">
        <div class="evn-cop-tn-bar clearfix">
            <asp:Repeater ID="rpt_Category" runat="server" >
                <ItemTemplate>
                    <div class="st-e-cop-area-bar" data-cat='<%# RegexReplaceSequence(string.IsNullOrEmpty(Container.DataItem.ToString())?"全部":Container.DataItem.ToString())%>' onclick='changecategory(<%# Container.ItemIndex %>);appendUrl(this);' id='<%# "c"+Container.ItemIndex %>'>
                        <a href="#" class="st-e-cop-optionsbn" onclick="return false;"><i class="fa fa-check-circle"></i><%# RegexReplaceSequence(string.IsNullOrEmpty(Container.DataItem.ToString())?"全部":Container.DataItem.ToString())%></a>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:PlaceHolder>
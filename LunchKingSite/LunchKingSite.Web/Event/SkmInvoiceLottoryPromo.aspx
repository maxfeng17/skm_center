﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SkmInvoiceLottoryPromo.aspx.cs"
    Inherits="LunchKingSite.Web.Event.SkmInvoiceLottoryPromo" MasterPageFile="~/Ppon/Ppon.Master"
    EnableViewState="true" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="LunchKingSite.WebLib" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="ucr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="../Themes/default/style/RDL.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/default/images/17Life/G2/skm/skm.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/default/images/17Life/G2/skm/skm-RDL.css" rel="stylesheet" type="text/css" />
    <script type='text/javascript'>

        function slidingMenuToggle() {
            $("#wrap").toggleClass("mbe-menu-show");
        }
        $(function () {
            $(".mbe-menu-btn").click(slidingMenuToggle);
            $(".mbe-channel").click(slidingMenuToggle);
        });

        var verification = true;
        $(document).ready(function () {
            $('html,body').scrollTop(parseInt($('[id*=hidScroll]').val(), 10));

            $('#tbTaishinCreditCard').bind('blur', function () {
                var regex = /^\d{8}$/;
                if (regex.test($('#tbTaishinCreditCard').val())) {
                    CheckCreditCard($('#tbTaishinCreditCard').val());
                } else {

                    $('#creditCardErrMsg').text("抱歉您的卡號有誤，請重新輸入。");
                    verification = false;
                }
            });

            $('#tbInvoiceInfo').bind('blur', function () {
                var regex = /^[a-zA-Z][a-zA-Z]\d{8}$/;
                if (regex.test($('#tbInvoiceInfo').val())) {
                    CheckInvoice($('#tbInvoiceInfo').val());
                } else {
                    $('#invoiceErrMsg').html("抱歉您的發票號碼有誤，請重新輸入。");
                    verification = false;
                }
            });

            $(".skm-login-btn img").hover(
                function () {
                    if ($(this).attr('src') == '../Themes/default/images/17Life/G2/skm/skm-login-bn1.png') {
                        $(this).attr('src', '../Themes/default/images/17Life/G2/skm/skm-login-bn1-hover.png');
                    } else {
                        $(this).attr('src', '../Themes/default/images/17Life/G2/skm/skm-login-bn2-hover.png');
                    }
                },
                function () {
                    if ($(this).attr('src') == '../Themes/default/images/17Life/G2/skm/skm-login-bn1-hover.png') {
                        $(this).attr('src', '../Themes/default/images/17Life/G2/skm/skm-login-bn1.png');
                    } else {
                        $(this).attr('src', '../Themes/default/images/17Life/G2/skm/skm-login-bn2.png');
                    }
                });

        });

        $(window).scroll(function () {
            $('[id*=hidScroll]').val(typeof window.pageYOffset != 'undefined' ? window.pageYOffset : document.documentElement.scrollTop);
        });

        function checkExchange() {
            verification = true;

            $('#tbTaishinCreditCard').blur();
            $('#tbInvoiceInfo').blur();

            if ($('#tbStoreName').val() == "") {
                $('#storeNameErrMsg').text("請填寫正確發票店家。");
                verification = false;
            } else {
                $('#storeNameErrMsg').text("");
            }

            if ($('#tbLottoryUserName').val() == "") {
                $('#userNameErrMsg').text("請填寫完整姓名。");
                verification = false;
            } else {
                $('#userNameErrMsg').text("");
            }

            if ($('#tbLottoryUserAddr').val() == "") {
                $('#addrErrMsg').text("請填寫完整地址。");
                verification = false;
            } else {
                $('#addrErrMsg').text("");
            }

            if ($('#tbLottoryUserTel').val() == "") {
                $('#mobileErrMsg').text("請填寫完整電話。");
                verification = false;
            } else {
                $('#mobileErrMsg').text("");
            }

            if (!$('#cbIsReadRules').attr('checked')) {
                $('#readRulesErrMsg').text("您是否已經閱讀並同意17Life的各項條款？");
                verification = false;
            } else {
                $('#readRulesErrMsg').text("");
            }


            if (verification) {
                return confirm("請再次確認資料皆填寫正確，送出後即無法修改");
            }

            return verification;
        }

        function CheckCreditCard(cardNumber) {
            $.ajax({
                type: 'POST',
                url: "SkmInvoiceLottoryPromo.aspx/CheckTaishinCreditCard",
                data: "{creditCardNumber:'" + cardNumber + "'}",
                dataType: 'json',
                async: false,
                contentType: 'application/json; charset=utf-8',
                success: function (msg) {
                    if (msg.d == "") {
                        $('#creditCardErrMsg').html(msg.d);
                    } else {
                        $('#creditCardErrMsg').html(msg.d);
                        verification = false;
                    }
                },
                error: function () {
                    $('#creditCardErrMsg').html("call webservice error");
                    verification = false;
                }
            });
        }

        function CheckInvoice(invoiceNumber) {
            $.ajax({
                type: 'POST',
                url: "SkmInvoiceLottoryPromo.aspx/CheckInvoice",
                data: "{invoiceNumber:'" + invoiceNumber + "'}",
                dataType: 'json',
                async: false,
                contentType: 'application/json; charset=utf-8',
                success: function (msg) {
                    if (msg.d == "") {
                        $('#invoiceErrMsg').text(msg.d);
                    } else {
                        $('#invoiceErrMsg').text(msg.d);
                        verification = false;
                    }
                },
                error: function () {
                    $('#invoiceErrMsg').text("call webservice error");
                    verification = false;
                }
            });
        }

    </script>    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <!--活動內容-->

  <div class="ly-skmfarme">    
     <!-- banner -->
      <ucr:Paragraph ID="paragraph1" runat="server" />
      <!-- banner -->
      
        <asp:Panel ID="divEvent" runat="server" Visible="False">
            <asp:Literal ID="liTop" runat="server"></asp:Literal>
            <asp:PlaceHolder ID="divInitial" runat="server">
                <asp:PlaceHolder ID="divExchange" runat="server" Visible="False">
                    <div class="ly-invoicesLogin-farm">
                        <div class="skm-invoices-log">
                            <label class="skm-invlabel">請輸入您的台新信用卡前8碼：</label>
                            <div class="skm-inputbox">
                                <asp:TextBox ID="tbTaishinCreditCard" runat="server" ClientIDMode="Static" CssClass="skm-input-half"></asp:TextBox>
                                <p id="creditCardErrMsg" class="skm-invo-error"></p>
                            </div>
                            <div class="skm-explanation-pic">
                                <img src="../Themes/default/images/17Life/G2/skm/Credit-card.png" width="250" height="158" /></div>
                        </div>
                        <div class="skm-invoices-log">
                            <label class="skm-invlabel">請輸入您消費的店家名稱：</label>
                            <div class="skm-inputbox">
                                <asp:TextBox ID="tbStoreName" runat="server" ClientIDMode="Static" CssClass="skm-input-half"></asp:TextBox>
                                <p id="storeNameErrMsg" class="skm-invo-error"></p>
                            </div>
                        </div>

                        <div class="skm-invoices-log">
                            <label class="skm-invlabel">請輸入您的發票號碼：</label>
                            <div class="skm-inputbox">
                                <asp:TextBox ID="tbInvoiceInfo" runat="server" ClientIDMode="Static" CssClass="skm-input-half"></asp:TextBox>
                                <p id="invoiceErrMsg" class="skm-invo-error"></p>
                            </div>
                            <div class="skm-explanation-pic">
                                <img src="../Themes/default/images/17Life/G2/skm/taiwan-Invoice.png" width="167" height="123" /></div>
                        </div>

                        <div class="skm-invoices-log">
                            <label class="skm-invlabel-2">請填妥以下連絡資訊，確保您的得獎權益唷 ~</label>
                        </div>

                        <div class="skm-invoices-log">
                            <label class="skm-invlabel">聯絡人姓名：</label>
                            <div class="skm-inputbox">
                                <asp:TextBox ID="tbLottoryUserName" runat="server" ClientIDMode="Static" CssClass="skm-input-half"></asp:TextBox>
                                <p id="userNameErrMsg" class="skm-invo-error"></p>
                            </div>
                        </div>
                        <div class="skm-invoices-log">
                            <label class="skm-invlabel">連絡人電話：</label>
                            <div class="skm-inputbox">
                                <asp:TextBox ID="tbLottoryUserTel" runat="server" ClientIDMode="Static" CssClass="skm-input-half"></asp:TextBox>
                                <p id="mobileErrMsg" class="skm-invo-error"></p>
                            </div>
                        </div>
                        <div class="skm-invoices-log">
                            <label class="skm-invlabel">連絡人地址：</label>
                            <div class="skm-inputbox">
                                <asp:TextBox ID="tbLottoryUserAddr" runat="server" ClientIDMode="Static" CssClass="skm-input-half"></asp:TextBox>
                                <p id="addrErrMsg" class="skm-invo-error"></p>
                            </div>
                        </div>
                        
                        <div class="skm-invoices-log">
                            <input class="skm-check" type="checkbox" id="cbIsReadRules" />
                            <label class="skm-loginlabel">您同意參加本活動，並同意17Life得依Payeasy-17Life<a target="_blank" href="<%=SiteUrl%>/newmember/privacypolicy.aspx">會員服務條款</a>及<a target="_blank" href="<%=SiteUrl%>/newmember/privacy.aspx">隱私權政策</a>規範，蒐集、處理、利用您提供之個人資料</label>
                            <p id="readRulesErrMsg" class="skm-invo-error skm-invobtn-error rd-skm7-red"></p>         
                        </div>
                        <div class="clearfix"></div>
                        <asp:LinkButton ID="lbExchange" runat="server" CssClass="skm-invo-confirmbtn" OnClientClick="return checkExchange();" OnClick="lbExchange_Click"></asp:LinkButton><p class="skm-invo-error skm-invobtn-error rd-skm7-red"></p>
                        <div class="clearfix"></div>
                    </div>
                </asp:PlaceHolder>

                <asp:PlaceHolder ID="divLogin" runat="server" Visible="False">
                    <div class="ly-invoicesLogin-farm">
                       <div class="skm-invo"> 
                           <p class="skm-invo-error skm-invologinerror rd-skm7-2-red">請先登入17life 才能兌換喔！</p>
                           <a href='<%=ResolveUrl(FormsAuthentication.LoginUrl+"?ReturnUrl="+SiteUrl+"/event/SkmInvoiceLottoryPromo.aspx?IsPromoLogin=1") %>'>
                              <div class="skm-invo-loginbtn"></div>
                           </a>
                           <a href='<%=LunchKingSite.BizLogic.Component.MemberActions.MemberUtilityCore.RegisterUrl %>'>
                              <div class="skm-invo-memberbtn"></div>
                           </a>
                        </div>
                    </div>                    
                </asp:PlaceHolder>
                
                

            </asp:PlaceHolder>
            
            <asp:PlaceHolder ID="divSuccess" runat="server" Visible="False">
                <div class="ly-invoicesLogin-farm">
                    <div class="skm-invo">
                       <p class="skm-invo-error skm-invo-record">恭喜您完成登錄程序！<br />
                        一張簽單/發票，即代表一次抽獎機會刷越多筆、中獎機率越高唷</p>
                       <a href="<%=SiteUrl%>/Event/CommercialEvent.aspx?eventcode=2013_SKM&commcode=4000">
                          <div class="skm-invo-backbtn"></div>
                       </a>
                    </div>
                </div>
            </asp:PlaceHolder>      
        </asp:Panel>
      
              <!--登錄發票選擇頁-->
        <asp:PlaceHolder ID="divChoseEvent" runat="server" Visible="False">
            <div class="skm-loginfarm ">
                <div class="skm-logbox">
                    <div class="skm-loginpic1">
                        <img src="../Themes/default/images/17Life/G2/skm/skm-login-bntext1.png" width="360" height="69" /></div>
                    <a target="_blank" href="https://www.taishinbank.com.tw/TS/TS02/TS0201/TS020104/TS02010407/TS0201040701/index.htm" class="skm-login-btn">
                        <img src="../Themes/default/images/17Life/G2/skm/skm-login-bn1.png" width="410" height="151" /></a>
                </div>

                <div class="skm-logbox">
                    <div class="skm-loginpic2">
                        <img src="../Themes/default/images/17Life/G2/skm/skm-login-bntext2.png" width="285" height="69" /></div>
                    <asp:LinkButton ID="lbShowEvent" CssClass="skm-login-btn" runat="server" OnClick="lbShowEvent_Click">
           <img src="../Themes/default/images/17Life/G2/skm/skm-login-bn2.png" width="410" height="151" />
                    </asp:LinkButton>
                </div>
                <div class="clearfix"></div>
            </div>
        </asp:PlaceHolder>
        <!--無活動警示-->
        <asp:Panel ID="divNoEvent" runat="server" Visible="False">
            <div class="PPClose">
                <div class="PPSpace"></div>
                <a href="https://www.17life.com" class="BackBtn"></a>
            </div>
        </asp:Panel>


     
   </div>


</asp:Content>

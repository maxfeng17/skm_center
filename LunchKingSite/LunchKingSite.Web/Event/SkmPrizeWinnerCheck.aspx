﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SkmPrizeWinnerCheck.aspx.cs"
    Inherits="LunchKingSite.Web.Event.SkmPrizeWinnerCheck" MasterPageFile="~/Ppon/Ppon.Master"
    EnableViewState="true" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="ucr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="//www.17life.com/images/17p/20140219-SkmActivity/tai-skm.css" rel="stylesheet" type="text/css" />
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <ucR:Paragraph ID="paragraph1" runat="server" />
    <asp:Panel ID="pl_WinnerInfoEdit" runat="server" CssClass="ly-rt-full-block">
        <div class="rec-content">
            <img src="https://www.17life.com/images/17p/20140219-SkmActivity/images/tai-skm-activity.jpg" alt="" />
            <div class="st-h1name st-color-red">
                <p>感謝您參與商圈一起週年慶活動，請依序填寫您的資料，以完成17Life折價券$50領獎程序</p>
            </div>
            <div class="st-h1name st-color-black">
                <p>
                    ※請填入商圈一起週年慶活動於台新網站所填寫之資訊 
                </p>
            </div>
            <div class="header_hr"></div>
            <div class="grui-form">
                <asp:Panel runat="server" ID="plName" CssClass="form-unit">
                    <label class="unit-label" for="">
                        姓名</label>
                    <div class="data-input">
                        <asp:TextBox ID="tbPrizeName" runat="server" CssClass="input-full" MaxLength="50"></asp:TextBox>
                        <asp:PlaceHolder ID="phErrorName" runat="server" Visible="False">
                            <p>
                                <span class="st-rec-warningred">※<asp:Label ID="lbErrorNameMsg" runat="server" Text="Label"></asp:Label>
                                </span>
                            </p>
                        </asp:PlaceHolder>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="plIDNumber" CssClass="form-unit">
                    <label class="unit-label" for="">
                        身分證字號</label>
                    <div class="data-input">
                        <asp:TextBox ID="tbPrizeIDNumber" runat="server" CssClass="input-full" MaxLength="50"></asp:TextBox>
                        <asp:PlaceHolder ID="phErrorIDNumber" runat="server" Visible="False">
                            <p>
                                <span class="st-rec-warningred">※<asp:Label ID="lbErrorIDNumberMsg" runat="server" Text="Label"></asp:Label>
                                </span>
                            </p>
                        </asp:PlaceHolder>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="plEMail" CssClass="form-unit">
                    <label class="unit-label" for="">
                        您想收到折價券之有效e-mail</label>
                    <div class="data-input">
                        <asp:TextBox ID="tbPrizeEMail" runat="server" CssClass="input-full" MaxLength="50"></asp:TextBox>
                        <asp:PlaceHolder ID="phErrorEMail" runat="server" Visible="False">
                            <p>
                                <span class="st-rec-warningred">※<asp:Label ID="lbErrorEMailMsg" runat="server" Text="Label"></asp:Label>
                                </span>
                            </p>
                        </asp:PlaceHolder>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pl_Error" runat="server" CssClass="st-h1warning st-color-red" Visible="False">
                    <p>
                        ※很抱歉，您所填寫之內容不符合得獎人資料 
                    </p>
                </asp:Panel>
                <div class="form-unit st-rec-border-none">
                    <div class="data-input center">
                        <asp:Button ID="btn_Send" runat="server" CssClass="btn btn-large btn-primary" OnClick="btnSendClick" OnClientClick="return CheckData();" Text="送出" />
                    </div>
                </div>
                <div class="st-h1name">
                    <p>
                        *貼心提醒 
                    </p>
                </div>
                <div class="st-h2name">
                    <p>
                        17Life折價券$50預計於2014/3/17前發送至您所填寫之e-mail 請您務必於2014/3/7-23:59前於本活動頁填寫完畢相關資料，避免影響您的權益，再次感謝您對17Life商圈一起週年慶的支持！ 
                    </p>
                </div>
            </div>
            <!--grui-->
        </div>
        <!--content-->
    </asp:Panel>
    <!--full-block-->

    <asp:Panel ID="pl_SuccessContent" runat="server" CssClass="ly-rt-full-block" Visible="False">
        <div class="rec-content">
            <img src="https://www.17life.com/images/17p/20140219-SkmActivity/images/tai-skm-activity.jpg" alt="" />
            <div class="st-h1warning st-color-black">
                <p>恭喜您完成領獎程序！</p>
                <p>17Life折價券預計於2014/3/17前發送至</p>
                <p>
                    <asp:Label ID="lbVerificationEMail" runat="server" Text="Label"></asp:Label>
                </p>
            </div>
        </div>
        <!--content-->
    </asp:Panel>
    <!--full-block-->

</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Presenters;
using System.Text.RegularExpressions;
using System.Web;

namespace LunchKingSite.Web.Event
{
    public partial class ExhibitionMobile : BasePage, IExhibition
    {
        #region property

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public static ISysConfProvider SystemConfig
        {
            get
            {
                return config;
            }
        }

        private ExhibitionPresenter _presenter;
        public ExhibitionPresenter Presenter
        {
            set
            {
                _presenter = value;
                if (value != null)
                {
                    _presenter.View = this;
                }
            }
            get
            {
                return _presenter;
            }
        }

        private bool _eventAlive = true;
        public bool EventAlive
        {
            set
            {
                _eventAlive = value;
            }
            get
            {
                return _eventAlive;
            }
        }

        private string _mainPic;
        /// <summary>
        /// 活動主圖
        /// </summary>
        public string MainPic
        {
            set
            {
                _mainPic = value;
            }
            get
            {
                return (string.IsNullOrEmpty(_mainPic)) ? string.Empty : _mainPic;
            }
        }

        private string _eventConditions;
        /// <summary>
        /// 活動辦法
        /// </summary>
        public string EventConditions
        {
            set
            {
                _eventConditions = value;
            }
            get
            {
                return (string.IsNullOrEmpty(_eventConditions)) ? string.Empty : _eventConditions;
            }
        }

        public string Url
        {
            get
            {
                return string.IsNullOrEmpty(Request.QueryString["u"]) ? string.Empty : Request.QueryString["u"];
            }
        }

        public string Preview
        {
            get
            {
                return string.IsNullOrEmpty(Request.QueryString["p"]) ? string.Empty : (Request.QueryString["p"] == "show_me_the_preview") ? Request.QueryString["p"] : string.Empty;
            }
        }

        public int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(Page.User.Identity.Name);
            }
        }

        public int EventId
        {
            get;
            set;
        }

        public string Rsrc
        {
            get;
            set;
        }

        public string EventTitle
        {
            get;
            set;
        }

        public string SubCategoryJson { get; set; }

        #region SEO Keyword

        public string SeoKeyword
        {
            get;
            set;
        }
        public string SeoDescription
        {
            get;
            set;
        }
        #endregion

        #region The Open Graph protocol

        public string FB_AppId
        {
            get
            {
                return ProviderFactory.Instance().GetConfig().FacebookApplicationId;
            }
        }
        /// <summary>
        /// meta og:title content
        /// </summary>
        public string OgTitle { set; get; }
        /// <summary>
        /// meta og:url content
        /// </summary>
        public string OgUrl { set; get; }
        /// <summary>
        /// meta og:description content
        /// </summary>
        public string OgDescription { set; get; }
        /// <summary>
        /// meta og:image content
        /// </summary>
        public string OgImage { set; get; }
        /// <summary>
        /// link rel=canonical href
        /// </summary>
        public string LinkCanonicalUrl { set; get; }
        /// <summary>
        /// link rel=image_src href
        /// </summary>
        public string LinkImageSrc { set; get; }
        /// <summary>
        /// 團購版型
        /// </summary>
        public EventPromoTemplateType TemplateType { set; get; }
        /// <summary>
        /// Ppon 分類數
        /// </summary>
        public int CategoryCount { set; get; }

        public int VourcherCount
        {
            get;
            set;
        }

        //活動合作店家-優惠劵數
        public int LottoryVourcherCount
        {
            get;
            set;
        }

        //新光三越合作店家-優惠劵數
        public int SkmStoreVourcherCount
        {
            get;
            set;
        }

        #endregion

        /// <summary>
        /// 啟用行動版商品主題活動頁
        /// </summary>
        public bool EnableMobileEventPromo { set; get; }

        /// <summary>
        /// 是否為策展活動
        /// </summary>
        public bool IsCuration { set; get; }

        public int CityId
        {
            get
            {
                int cid;
                if (Request.QueryString["cid"] != null && int.TryParse(Request.QueryString["cid"].ToString(), out cid))
                {
                    return cid;
                }
                else
                {
                    HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.CityId.ToString("g"));
                    if (cookie != null && int.TryParse(cookie.Value, out cid))
                    {
                        return cid;
                    }
                }
                return PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Presenter.OnViewInitialized();
            }
            Presenter.OnViewLoaded();
            Page.MetaDescription = string.IsNullOrEmpty(SeoDescription) ? Page.MetaDescription : SeoDescription;
            Page.MetaKeywords = string.IsNullOrEmpty(SeoKeyword) ? Page.MetaKeywords : SeoKeyword;
        }

        /// <summary>
        /// PponDeal
        /// </summary>
        /// <param name="eventPromo"></param>
        /// <param name="list"></param>
        public void ShowEventPromo(EventPromo eventPromo, List<ViewEventPromo> list)
        {
            if (list.Count == 0)
            {
                return;
            }

            var firstItem = list.First();
            Title = firstItem.EventTitle;
            MainPic = firstItem.MobileMainPic;
            EventConditions = firstItem.EventDescription;


            ConcurrentDictionary<ViewEventPromo, bool> promoItems = new ConcurrentDictionary<ViewEventPromo, bool>();

            //已售完檔次自動排序最後
            foreach (ViewEventPromo promo in list)
            {
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(promo.BusinessHourGuid);
                PponDealStage stage = deal.GetDealStage();
                bool isSoldOut = false;
                if ((stage == PponDealStage.CouponGenerated || stage == PponDealStage.ClosedAndFail || stage == PponDealStage.ClosedAndOn) || deal.OrderedQuantity >= ((deal.OrderTotalLimit.HasValue) ? deal.OrderTotalLimit : 0))
                {
                    promo.Seq = 9999 + promo.Seq;
                    isSoldOut = true;
                }
                promoItems.GetOrAdd(promo, isSoldOut);
            }

            TemplateType = (EventPromoTemplateType)eventPromo.TemplateType;

            if (TemplateType == EventPromoTemplateType.Kind)
            {
                rptKindDeals.Visible = true;
                rptKindDeals.DataSource = promoItems.OrderBy(x => x.Key.Seq);
                rptKindDeals.DataBind();
            }
            else if (TemplateType == EventPromoTemplateType.Zero)
            {
                rptZeroDeals.Visible = true;
                rptZeroDeals.DataSource = promoItems.OrderBy(x => x.Key.Seq);
                rptZeroDeals.DataBind();
            }
            else if (TemplateType == EventPromoTemplateType.OnlyNewPiinlife)
            {
                rptPponDeals.Visible = false;
            }
            else
            {
                rptPponDeals.Visible = true;
                rptPponDeals.DataSource = promoItems.OrderBy(x => x.Key.Seq);
                rptPponDeals.DataBind();
            }
        }
        public string GetPponDealCategoryCssClassName(string categoryName, string subCategoryName)
        {
            string result = "";
            var category = new JsonSerializer().Deserialize<List<Dictionary<string, string>>>(SubCategoryJson);

            foreach (var item in category)
            {
                string tempIndex = "";
                bool categoryMatch = false;
                bool subCategoryMatch = false;
                foreach (KeyValuePair<string, string> categoryValue in item)
                {
                    if (categoryValue.Key == EventPromoGroupByColumn.index.ToString())
                    {
                        tempIndex = categoryValue.Value;
                    }
                    else if (categoryValue.Key == EventPromoGroupByColumn.Category.ToString() && categoryValue.Value == categoryName)
                    {
                        categoryMatch = true;
                    }
                    else if (categoryValue.Key == EventPromoGroupByColumn.SubCategory.ToString() && categoryValue.Value == subCategoryName)
                    {
                        subCategoryMatch = true;
                    }

                }
                if (categoryMatch && subCategoryMatch)
                {
                    result = tempIndex;
                    break;
                }
                if (categoryMatch && subCategoryName == "品生活")
                {
                    if (result == "")
                    {
                        result = tempIndex;
                    }
                    else
                    {
                        result += "|" + tempIndex;
                    }
                }

            }
            if (subCategoryName == "品生活")
            {
                if (result.IndexOf("|") > 0)
                {
                    string[] sTemp = result.Split('|');
                    result = "";
                    foreach (var item in sTemp)
                    {
                        result += (item != "") ? string.Format(" pponCategory{0}", item) : "";
                    }
                }
            }
            return (result != "") ? string.Format(" pponCategory{0}", result) : " pponCategory0";
        }
        protected void rptPponDeals_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null) { 
                var eventPromo = ((KeyValuePair<ViewEventPromo, bool>)e.Item.DataItem);
                if (eventPromo.Key is ViewEventPromo)
                {
                    ViewEventPromo promo = eventPromo.Key;
                    if (!string.IsNullOrEmpty(promo.DealPromoImage))
                    {
                        Literal litDealPromoImage = (Literal)e.Item.FindControl("lit_DealPromoImage");
                        litDealPromoImage.Text = GetDealPromoImageHtmlTag(promo.DealPromoImage);
                    }
                }
            }
        }
        /// <summary>
        /// 優惠券
        /// </summary>
        /// <param name="dataList"></param>
        /// <param name="lotteryList"></param>
        /// <param name="storeList"></param>
        public void ShowVourcherEventPromo(Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> dataList, Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> lotteryList, Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> storeList)
        {
            if (dataList.Count > 0)
            {
                VourcherCount = dataList.Count;
                rptVourcher.DataSource = dataList.OrderBy(x => x.Key.Seq);
                rptVourcher.DataBind();
                rptVourcher.Visible = true;
            }
            if (lotteryList.Count > 0)
            {
                LottoryVourcherCount = lotteryList.Count;
                rptVourcherLottery.Visible = true;
                rptVourcherLottery.DataSource = lotteryList.OrderBy(x => x.Key.Seq);
                rptVourcherLottery.DataBind();
            }
            if (storeList.Count > 0)
            {
                SkmStoreVourcherCount = storeList.Count;
                rptVourcherStore.Visible = true;
                rptVourcherStore.DataSource = storeList.OrderBy(x => x.Key.Seq);
                rptVourcherStore.DataBind();
            }
        }

        public void ShowPiinlifeEventPromo(List<ViewEventPromo> dataList)
        {
            foreach (var list in dataList)
            {
                list.CategoryName = GetPponDealCategoryCssClassName(list.Category, "品生活");
            }
            if (dataList.Count > 0)
            {
                rptPiinlife.Visible = true;
                rptPiinlife.DataSource = dataList.OrderBy(x => x.Seq);
                rptPiinlife.DataBind();
            }

            if (TemplateType == EventPromoTemplateType.OnlyNewPiinlife)
            {
                var firstItem = dataList.First();
                Title = firstItem.EventTitle;
                MainPic = firstItem.MobileMainPic;
                EventConditions = firstItem.EventDescription;
            }

        }
        public void ShowPiinlifeEventPromo(EventPromo eventPromo, List<ViewEventPromo> dataList)
        { }
        /// <summary>
        /// 活動過期
        /// </summary>
        public void ShowEventExpire()
        {
            EventAlive = false;
        }
        public void SetCategory(EventPromoItemCollection list)
        { }
        public void SetAllCategory(EventPromo eventPromo, List<ViewEventPromo> dataList)
        {
            if (TemplateType == EventPromoTemplateType.OnlyNewPiinlife)
            {
                int piinlifeCityId = PponCityGroup.DefaultPponCityGroup.Piinlife.CityId;
                var groupCategory = dataList.Where(y=>y.CityList.Contains(piinlifeCityId.ToString())).GroupBy(x => string.IsNullOrEmpty(x.Category) ? string.Empty : x.Category).OrderBy(x => x.Key);
                CategoryCount = groupCategory.Count();
                if (CategoryCount > 1)
                {
                    rptCategory.DataSource = groupCategory.Select(x => string.IsNullOrEmpty(x.Key) ? string.Empty : x.Key);
                    rptCategory.DataBind();
                }
            }
            else
            {
                var groupCategory = dataList.GroupBy(x => string.IsNullOrEmpty(x.Category) ? string.Empty : x.Category).OrderBy(x => x.Key);
                CategoryCount = groupCategory.Count();
                if (CategoryCount > 1)
                {
                    rptCategory.DataSource = groupCategory.Select(x => string.IsNullOrEmpty(x.Key) ? string.Empty : x.Key);
                    rptCategory.DataBind();
                }
            }
            var subCategoryList = dataList.GroupBy(x => new { x.Category, x.SubCategory }).Select(x => new { x.Key.Category, x.Key.SubCategory }).OrderBy(m => m.Category).ThenBy(n => n.SubCategory).Select((x, index) => new { index, x.Category, x.SubCategory });
            SubCategoryJson = new JsonSerializer().Serialize(subCategoryList);
        }
        private string GetDealPromoImageHtmlTag(string dealPromoImage)
        {
            return (string.IsNullOrEmpty(dealPromoImage)) ? string.Empty : string.Format(@"<img src='{0}' alt='' />"
                , ImageFacade.GetMediaPath(dealPromoImage, MediaType.DealPromoImage));
        }
        public static string GetOrderedTotal(Guid bid)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            return deal.GetAdjustedOrderedQuantity().Quantity.ToString(CultureInfo.InvariantCulture);
        }

        protected void rptVourcher_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var vourcher = ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(e.Item.DataItem));

                Image img = (Image)e.Item.FindControl("VourcherImage");
                img.ImageUrl = ImageFacade.GetMediaPathsFromRawData(vourcher.Value.Key.PicUrl, MediaType.PponDealPhoto).DefaultIfEmpty("").First();

                if (img.ImageUrl == "")
                {
                    Seller seller = VourcherFacade.SellerGetByGuid(vourcher.Value.Key.SellerGuid);
                    img.ImageUrl = ImageFacade.GetMediaPathsFromRawData(seller.SellerLogoimgPath, MediaType.SellerPhotoLarge).DefaultIfEmpty("").First();

                    if (img.ImageUrl == "")
                    {
                        img.ImageUrl = "https://www.17life.com/images/17P/20130914-Shin/skm-Coupon.jpg";
                    }
                }
            }
        }

        protected string RegexReplaceSequence(string category)
        {
            return Regex.Replace(category, @"^[0-9]*\.", string.Empty);
        }

        public bool IsSoldOut(int hidealId)
        {
            return HiDealDealManager.DealProductIsSoldOut(hidealId);
        }

        public void rptPiinlife_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewEventPromo)
            {
                ViewEventPromo item = (ViewEventPromo)e.Item.DataItem;
                //Literal llRegionTab = (Literal)e.Item.FindControl("ll_RegionTab");
                PlaceHolder phEnableLink = (PlaceHolder)e.Item.FindControl("phEnableLink");
                PlaceHolder phDisabledLink = (PlaceHolder)e.Item.FindControl("phDisabledLink");
                Literal llHiDealStauts = (Literal)e.Item.FindControl("llHiDealStauts");
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(item.BusinessHourGuid);
                PponDealStage stage = deal.GetDealStage();
                //已售完檔次自動排序最後

                phEnableLink.Visible = false;
                phDisabledLink.Visible = true;

                if ((stage == PponDealStage.CouponGenerated || stage == PponDealStage.ClosedAndFail || stage == PponDealStage.ClosedAndOn) || deal.OrderedQuantity >= ((deal.OrderTotalLimit.HasValue) ? deal.OrderTotalLimit : 0))
                {
                    llHiDealStauts.Text = DateTime.Now <= item.BusinessHourOrderTimeS ? "<div class='evn-cpp-pinnlife-date-coming-bar-3column'><div class='date-coming-text-3column'>" + item.BusinessHourOrderTimeS.ToString("MM/dd") + "活動即將開始</div></div>" : "<div class='evn-cpp-pinnlife-sold-out-bar-3column'><img src='../Themes/default/images/17Life/G2/soldout_bar_440.png' width='440' height='243' alt='' /></div>";
                }
                else
                {
                    if (DateTime.Now <= item.BusinessHourOrderTimeS)
                    {
                        llHiDealStauts.Text = "<div class='evn-cpp-pinnlife-date-coming-bar-3column'><div class='date-coming-text-3column'>" + item.BusinessHourOrderTimeS.ToString("MM/dd") + "活動即將開始</div></div>";
                    }
                    else if (item.BusinessHourOrderTimeE <= DateTime.Now)
                    {

                        llHiDealStauts.Text = "<div class='evn-cpp-pinnlife-sold-out-bar-3column'><img src='../Themes/default/images/17Life/G2/soldout_bar_440.png' width='440' height='243' alt='' /></div>";
                    }
                    else
                    {
                        phEnableLink.Visible = true;
                        phDisabledLink.Visible = false;
                    }
                }
            }

        }

        protected void RandomPponMasterInit(object sender, EventArgs e)
        {
            curation.CityId = CityId != -1 ? CityId : PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId;
            curation.Type = RandomCmsType.PponMasterPage;
            curation.IsexhibitionM = true;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using System.Web.Security;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.User
{
    public partial class PromoItemExchange : LocalizedBasePage, IPromoItemExchangeView
    {
        #region property
        public EventActivity Activity
        {
            get
            {
                if (ViewState["Activity"] != null)
                    return (EventActivity)ViewState["Activity"];
                else
                    return null;
            }
            set
            {
                ViewState["Activity"] = value;
            }
        }

        public string CodeName
        {
            get
            {
                if (ViewState["CodeGroupName"] != null)
                    return ViewState["CodeGroupName"].ToString();
                else
                    return null;
            }
            set
            {
                ViewState["CodeGroupName"] = liEvent.Text = value;
            }
        }

        public int EventId
        {
            get
            {
                int id = 0;
                if (Request.QueryString["eventId"] != null)
                     id = int.TryParse(Request.QueryString["eventId"].ToString(), out id) ? id : 0;
                return id;

            }
        }

        private PromoItemExchangePresenter _presenter;
        public PromoItemExchangePresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public string UserName
        {
            get { return this.Page.User.Identity.Name; }
        }
        public int UserId
        {
            get { return MemberFacade.GetUniqueId(this.Page.User.Identity.Name); }
        }
        #endregion
        #region method
        public void ShowExchange()
        {
            divExchange.Visible = true;
        }
        public void PromoItemGetList(Dictionary<string, int> dataList)
        {
            rpt_dataList.DataSource = dataList;
            rpt_dataList.DataBind();
        }
        public void ShowMessage(string message, System.Drawing.Color color)
        {
            lab_Message.Text = message;
            lab_Message.ForeColor = color;
        }
        public void AlertMessage(string message)
        {
            string action = string.Format("alert('{0}');", message);
            ScriptManager.RegisterStartupScript(this, typeof(Button), "eventmessage", action, true);
        }
        #endregion
        #region event
        public event EventHandler<DataEventArgs<string>> Exchange;
        public event EventHandler<EventArgs> ExchangeInfo;
        #endregion
        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }

            if (!Page.IsPostBack)
            {
                Presenter.OnViewInitialized();
                if (Activity != null)
                {
                    this.Title = Activity.Eventname;
                    liEventName.Text = Activity.Eventname;
                }
            }
            Presenter.OnViewLoaded();
        }
        protected void CouponExchange(object sender, EventArgs e)
        {
            if (this.Exchange != null)
                this.Exchange(this, new DataEventArgs<string>(txtCode.Text.Trim()));
        }
        protected void ShowExchangeInfo(object sender, EventArgs e)
        {
            divExchange.Visible = false;
            divExchangeDataList.Visible = true;
            if (this.ExchangeInfo != null)
                this.ExchangeInfo(this, e);
        }
        protected void BackToExchange(object sender, EventArgs e)
        {
            divExchange.Visible = true;
            divExchangeDataList.Visible = false;
        }
        #endregion
    }
}
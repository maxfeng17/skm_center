﻿using System;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using System.Web.UI.HtmlControls;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Linq;

namespace LunchKingSite.Web.Event
{
    public partial class EventListContent : System.Web.UI.UserControl
    {
        public string ShareArea { get; set; }
        public string RuleUrl { get; set; }
        public string TodayAnchor { get; set; }
        public int themeMainId { get; set; }
        public string title { get; set; }
        public string FbTitle { get; set; }
        public string FBimage { get; set; }
        public string MobileCategoryImage { get; set; }

        public string Preview
        {
            get
            {
                return string.IsNullOrEmpty(Request.QueryString["p"]) ? string.Empty : Request.QueryString["p"];
            }
        }

        private bool IsIOSUser
        {
            get
            {
                var userAgent = System.Web.HttpContext.Current.Request.UserAgent;
                if ((!string.IsNullOrEmpty(userAgent)) &&
                    (userAgent.ToLower().Contains("iphone")
                     || userAgent.ToLower().Contains("ipod")
                     || userAgent.ToLower().Contains("ipad")
                    )
                   )
                {
                    return true;
                }
                return false;
            }
        }

        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        public override void DataBind()
        {
            divPageTop.Visible = divEventEnd.Visible = false;
            if (themeMainId == 0) {
                themeMainId = pp.GetNowThemeCurationMainCollection().OrderByDescending(x => x.StartDate).First().Id;
            }
            CurationsByTheme lists = PromotionFacade.GetCurationsByTheme(themeMainId);
            if (lists != null && !(string.IsNullOrEmpty(lists.MainImageUrl)))
            {
                ThemeCurationMain theme = pp.GetThemeCurationMainById(themeMainId);
                if (((theme.StartDate <= DateTime.Now && DateTime.Now <= theme.EndTime && theme.ShowInApp) || Preview == "show_me_the_preview"))
                {
                    TodayAnchor = "#";
                    themeMainId = theme.Id;
                    RuleUrl = string.Format("{0}?id={1}", RuleUrl, themeMainId);
                    imgMain.ImageUrl = ImageFacade.GetMediaPath(theme.MobileMainImage, MediaType.ThemeImage); //主視覺
                    MobileCategoryImage = ImageFacade.GetMediaPath(theme.MobileCategoryImage, MediaType.ThemeImage);//M版分類底圖
                    title = theme.Title;
                    FbTitle = theme.FbShareTitle;
                    FBimage = ImageFacade.GetMediaPath(theme.FbShareIamge, MediaType.ThemeImage); //FB分享圖

                    if (!string.IsNullOrEmpty(theme.CssStyle))
                    {
                        litChangeCss.Text = theme.CssStyle.Split("@")[1];
                    }

                    if (!string.IsNullOrEmpty(ShareArea))
                    {
                        ShareArea = ShareArea.Replace("@OgTitle", title);
                        litFbShare.Text = ShareArea;
                    }

                    OneDayCuration todayList = lists.TodayCurations;
                    if (todayList != null && todayList.IsActive)
                    {
                        litHotBannerTitle.Text = litNavToadayHot.Text = todayList.GroupName;
                        rpTodayBanner.DataSource = todayList.Curations;
                        rpTodayBanner.DataBind();
                        divHot.Attributes.Remove("style");
                        liNavToadayHot.Attributes.Remove("style");
                    }
                    else
                    {
                        divHot.Attributes.Add("style", "display:none");
                        liNavToadayHot.Attributes.Add("style", "display:none");
                    }

                    var allList = lists.AllCurations.Where(x => !x.IsTopGroup && x.IsActive);
                    if (allList != null)
                    {
                        rpBannerList.DataSource = allList;
                        rpBannerList.DataBind();
                     
                        rpNav.DataSource = allList;
                        rpNav.DataBind();
                    }

                    divPageTop.Visible = true;
                }
                else
                {
                    divEventEnd.Visible = true;
                }
            }
            else
            {
                divEventEnd.Visible = true;
            }
        }

        protected void rpTodayBanner_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ThemeCurationInfo)
            {
                ThemeCurationInfo tci = (ThemeCurationInfo)e.Item.DataItem;

                HtmlAnchor linkToday = e.Item.FindControl("linkToday") as HtmlAnchor;
                Image imgToday = e.Item.FindControl("imgToday") as Image;

                linkToday.HRef = (IsIOSUser) ? tci.CurationLink.Replace("eventid","EventId") : tci.CurationLink;
                imgToday.ImageUrl = tci.ImageUrl;
                imgToday.Attributes.Add("class","lazy");
            }
        }

        protected void rpBannerList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is OneDayCuration)
            {
                OneDayCuration odc = (OneDayCuration)e.Item.DataItem;

                Literal litDay = (Literal)e.Item.FindControl("litDay");
                litDay.Text = odc.GroupName ?? "Day" + (e.Item.ItemIndex + 1) + " " + odc.EventDate.ToString("MM.dd");

                Repeater rpSubList = (Repeater)e.Item.FindControl("rpSubList");
                rpSubList.DataSource = odc.Curations.Where(x => x.ShowInApp);
                rpSubList.DataBind();
            }
        }

        protected void rpSubList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ThemeCurationInfo)
            {
                ThemeCurationInfo tci = (ThemeCurationInfo)e.Item.DataItem;

                HtmlAnchor linkBanner = e.Item.FindControl("linkBanner") as HtmlAnchor;
                Image imgBanner = e.Item.FindControl("imgBanner") as Image;

                linkBanner.HRef = (IsIOSUser) ? tci.CurationLink.Replace("eventid", "EventId") : tci.CurationLink;
                imgBanner.ImageUrl = tci.ImageUrl;


                HtmlControl divBlock = e.Item.FindControl("divBlock") as HtmlControl;
                if (tci.IsActive)
                {
                    divBlock.Attributes.Add("style", "display:none;");
                }
                else
                {
                    divBlock.Attributes.Add("style", "display:block;");
                }
            }
        }
        
        protected void rpNav_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is OneDayCuration)
            {
                OneDayCuration odc = (OneDayCuration)e.Item.DataItem;

                HtmlAnchor linkNav = (HtmlAnchor)e.Item.FindControl("linkNav");
                linkNav.HRef = "#mobile_index_" + (e.Item.ItemIndex + 1);
                linkNav.InnerText = odc.GroupName;
            }
        }
    }
}
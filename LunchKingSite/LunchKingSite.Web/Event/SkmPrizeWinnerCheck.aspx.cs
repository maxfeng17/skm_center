﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.Web.Event
{
    public partial class SkmPrizeWinnerCheck :BasePage, ISkmPrizeWinnerCheckView
    {
        private SkmPrizeWinnerCheckPresenter _presenter;
        public SkmPrizeWinnerCheckPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }


        public string UserName
        {
            get { return User.Identity.Name; }
        }

        public int UserId
        {
            get { return MemberFacade.GetUniqueId(User.Identity.Name); }
        }

        public event EventHandler<DataEventArgs<SkmPrizeWinnerData>> SendWinnerInfo = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('中獎登錄活動已截止。');window.location.href='" + ResolveClientUrl("~/ppon/") + "';", true);
            if (!Page.IsPostBack)
            {
                if (!_presenter.OnViewInitialized())
                {
                    Response.Redirect("~/error-sgo.aspx");
                    return;
                }
            }
            _presenter.OnViewLoaded();

        }

        public void RedirectToLogin()
        {
            FormsAuthentication.RedirectToLoginPage();
        }

        public void ShowEditContent(bool isShowError = false)
        {
            pl_WinnerInfoEdit.Visible = true;
            pl_SuccessContent.Visible = false;

            pl_Error.Visible = isShowError;

        }

        public void ShowSuccessContent(string prizeEmail)
        {
            pl_WinnerInfoEdit.Visible = false;
            pl_SuccessContent.Visible = true;
            lbVerificationEMail.Text = prizeEmail;

        }

        public void ShowNameError(string errorMsg)
        {
            plName.CssClass = "form-unit error";
            tbPrizeName.CssClass = "input-half";
            phErrorName.Visible = true;
            lbErrorNameMsg.Text = errorMsg;
        }

        public void ShowIDNumberError(string errorMsg)
        {
            plIDNumber.CssClass = "form-unit error";
            tbPrizeIDNumber.CssClass = "input-half";
            phErrorIDNumber.Visible = true;
            lbErrorIDNumberMsg.Text = errorMsg;
        }

        public void ShowEmailError(string errorMsg)
        {
            plEMail.CssClass = "form-unit error";
            tbPrizeEMail.CssClass = "input-half";
            phErrorEMail.Visible = true;
            lbErrorEMailMsg.Text = errorMsg;
        }


        protected void btnSendClick(object sender, EventArgs e)
        {
            bool isCheckData = true;
            ResetErrorMsg();

            //檢查姓名
            if (string.IsNullOrEmpty(tbPrizeName.Text))
            {
                isCheckData = false;
                ShowNameError("姓名不得為空");
            }
            //檢查身分證

            if (CheckIdentificationId(tbPrizeIDNumber.Text)==false)
            {
                isCheckData = false;
                ShowIDNumberError("身分證字號格式不正確");
            }

            //檢查Email格式

            if (RegExRules.CheckEmail(tbPrizeEMail.Text) == false)
            {
                isCheckData = false;
                ShowEmailError("EMail格式不正確");
            }

            if (isCheckData && SendWinnerInfo != null)
            {
                var checkWinner = new SkmPrizeWinnerData();
                checkWinner.PrizeName = tbPrizeName.Text;
                checkWinner.PrizeIDNumber = tbPrizeIDNumber.Text.ToUpper();
                checkWinner.PrizeEmail = tbPrizeEMail.Text;

                this.SendWinnerInfo(this, new DataEventArgs<SkmPrizeWinnerData>(checkWinner));
            }
            else
            {
                pl_Error.Visible = true;
            }

        }

        protected void ResetErrorMsg()
        {
            plName.CssClass = "form-unit";
            tbPrizeName.CssClass = "input-full";
            phErrorName.Visible = false;
            lbErrorNameMsg.Text = string.Empty;

            plIDNumber.CssClass = "form-unit";
            tbPrizeIDNumber.CssClass = "input-full";
            phErrorIDNumber.Visible = false;
            lbErrorIDNumberMsg.Text = string.Empty;

            plEMail.CssClass = "form-unit";
            tbPrizeEMail.CssClass = "input-full";
            phErrorEMail.Visible = false;
            lbErrorEMailMsg.Text = string.Empty;


        }

        public bool CheckIdentificationId(string Input_ID)
        {
            bool IsTrue = false;
            if (Input_ID.Length == 10)
            {
                Input_ID = Input_ID.ToUpper();
                if (Input_ID[0] >= 0x41 && Input_ID[0] <= 0x5A)
                {
                    int[] Location_No = new int[] { 10, 11, 12, 13, 14, 15, 16, 17, 34, 18, 19, 20, 21, 22, 35, 23, 24, 25, 26, 27, 28, 29, 32, 30, 31, 33 };
                    int[] Temp = new int[11];
                    Temp[1] = Location_No[(Input_ID[0]) - 65] % 10;
                    int Sum = Temp[0] = Location_No[(Input_ID[0]) - 65] / 10;
                    for (int i = 1; i <= 9; i++)
                    {
                        Temp[i + 1] = Input_ID[i] - 48;
                        Sum += Temp[i] * (10 - i);
                    }
                    if (((Sum % 10) + Temp[10]) % 10 == 0)
                    {
                        IsTrue = true;
                    }
                }
            }
            return IsTrue;
        }
    }
}
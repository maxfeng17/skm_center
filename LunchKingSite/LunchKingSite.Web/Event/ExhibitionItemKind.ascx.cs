﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Models;

namespace LunchKingSite.Web.Event
{
    public partial class ExhibitionItemKind : System.Web.UI.UserControl
    {
        public List<ViewEventPromo> Items { get; set; }
        public ConcurrentDictionary<ViewEventPromo, bool> _promoItems { get; private set; }
        public string ItemId { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public override void DataBind()
        {
            //已售完檔次自動排序最後
            _promoItems = new ConcurrentDictionary<ViewEventPromo, bool>();

            foreach (ViewEventPromo promo in Items)
            {
                bool isSoldOut = false;
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(promo.BusinessHourGuid);
                PponDealStage stage = deal.GetDealStage();

                if ((stage == PponDealStage.CouponGenerated || stage == PponDealStage.ClosedAndFail || stage == PponDealStage.ClosedAndOn) || deal.OrderedQuantity >= ((deal.OrderTotalLimit.HasValue) ? deal.OrderTotalLimit : 0))
                {
                    promo.Seq = 9999 + promo.Seq;
                    isSoldOut = true;
                }

                _promoItems.GetOrAdd(promo, isSoldOut);
            }

            rpt_Item.DataSource = _promoItems.OrderBy(x => x.Key.Seq);
            rpt_Item.DataBind();
        }

        protected void rpt_Item_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (((KeyValuePair<ViewEventPromo, bool>)e.Item.DataItem).Key is ViewEventPromo)
            {
                ViewEventPromo promo = (ViewEventPromo)((KeyValuePair<ViewEventPromo, bool>)e.Item.DataItem).Key;
                if (!string.IsNullOrEmpty(promo.DealPromoImage))
                {
                    Literal litDealPromoImage = (Literal)e.Item.FindControl("lit_DealPromoImage");
                    Literal litDealPromoImageDisableLink = (Literal)e.Item.FindControl("lit_DealPromoImageDisableLink");
                    litDealPromoImage.Text = litDealPromoImageDisableLink.Text = GetDealPromoImageHtmlTag(promo.DealPromoImage);
                }
            }
        }

        private string GetDealPromoImageHtmlTag(string dealPromoImage)
        {
            return (string.IsNullOrEmpty(dealPromoImage)) ? string.Empty : string.Format(@"<img src='{0}' alt='' />"
                , ImageFacade.GetMediaPath(dealPromoImage, MediaType.DealPromoImage));
        }
    }
}
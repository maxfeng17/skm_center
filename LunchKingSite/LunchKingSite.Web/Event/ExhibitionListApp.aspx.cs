﻿using System;
using System.Web.UI;

namespace LunchKingSite.Web.Event
{
    public partial class ExhibitionListApp : System.Web.UI.Page
    {
        public int ThemeMainId
        {
            get
            {
                int themeMainId;
                return int.TryParse(Request["id"] ?? string.Empty, out themeMainId) ? themeMainId : 1;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            EventListContent items = (EventListContent)Page.LoadControl("EventListContent.ascx");
            items.themeMainId = ThemeMainId;
            items.RuleUrl = "ExhibitionListRuleApp.aspx";
            items.DataBind();
            pan_EventList.Controls.Add(items);

            this.Page.Title = items.title;
        }
    }
}
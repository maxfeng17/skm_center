﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExhibitionListRuleMobile.aspx.cs" Inherits="LunchKingSite.Web.Event.ExhibitionListRuleMobile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="apple-touch-icon-precomposed" href="../Themes/mobile/images/apple_touch_144x144.jpg"/>
    <link rel="stylesheet" href="../Themes/mobile/css/style.css"  type="text/css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <title>活動說明</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:PlaceHolder ID="pan_rule" runat="server"></asp:PlaceHolder>
    </form>
</body>
</html>
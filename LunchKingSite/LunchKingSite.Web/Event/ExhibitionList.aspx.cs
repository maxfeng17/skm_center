﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using System;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Event
{
    public partial class ExhibitionList : BasePage
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        
        public string Share_FB_Url
        {
            get;
            set;
        }
        public string MainBackgroundImage
        {
            get;
            set;
        }
        public string MainSpacerImage
        {
            get;
            set;
        }
        public string WebCategoryImage
        {
            get;
            set;
        }

        #region The Open Graph protocol
        public string FB_AppId
        {
            get
            {
                return ProviderFactory.Instance().GetConfig().FacebookApplicationId;
            }
        }
        /// <summary>
        /// meta og:title content
        /// </summary>
        public string OgTitle { set; get; }
        /// <summary>
        /// meta og:url content
        /// </summary>
        public string OgUrl { set; get; }
        /// <summary>
        /// meta og:image content
        /// </summary>
        public string OgImage { set; get; }
        /// <summary>
        /// meta og:description content
        /// </summary>
        public string OgDescription { set; get; }
        /// <summary>
        /// link rel=canonical href
        /// </summary>
        public string LinkCanonicalUrl { set; get; }
        /// <summary>
        /// link rel=image_src href
        /// </summary>
        public string LinkImageSrc { set; get; }
        #endregion
        private bool IsMobileBroswer
        {
            get
            {
                var userAgent = HttpContext.Current.Request.UserAgent;
                if ((!string.IsNullOrEmpty(userAgent)) &&
                    (userAgent.ToLower().Contains("iphone")
                        || (userAgent.ToLower().Contains("ipod")
                        || (userAgent.ToLower().Contains("android")
                        || (userAgent.ToLower().Contains("Blackberry"))))))
                    {
                    return true;
                }
                return false;
            }
        }

        private int _themeMainId;
        public int ThemeMainId
        {
            get
            {
                int themeMainId;
                return int.TryParse(Request["id"] ?? string.Empty, out themeMainId) ? themeMainId : _themeMainId;
            }
            set
            {
                _themeMainId = value;
            }
        }

        public string Preview
        {
            get
            {
                return string.IsNullOrEmpty(Request.QueryString["p"]) ? string.Empty : Request.QueryString["p"];
            }
        }

        protected override void OnInit(System.EventArgs e)
        {
            base.OnInit(e);
            if (IsMobileBroswer) {
                Response.Redirect("exhibitionlistmobile.aspx" + Request.Url.Query);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pan_main.Visible = pan_sub.Visible = false;
                if (ThemeMainId == 0)
                {
                    _themeMainId = pp.GetNowThemeCurationMainCollection().OrderByDescending(x => x.StartDate).First().Id;
                }
                CurationsByTheme lists = PromotionFacade.GetCurationsByTheme(ThemeMainId);
                if (lists != null && !(string.IsNullOrEmpty(lists.MainImageUrl)))
                {
                    ThemeCurationMain theme = pp.GetThemeCurationMainById(ThemeMainId);
                    if ((theme.StartDate <= DateTime.Now && DateTime.Now <= theme.EndTime && theme.ShowInWeb) || Preview == "show_me_the_preview")
                    {
                        imgMain.ImageUrl = LinkImageSrc = OgImage = ImageFacade.GetMediaPath(theme.MainImage, MediaType.ThemeImage); //主視覺
                        MainBackgroundImage = (!string.IsNullOrEmpty(theme.MainBackgroundImage)) ? ImageFacade.GetMediaPath(theme.MainBackgroundImage, MediaType.ThemeImage) : null;
                        MainSpacerImage = (!string.IsNullOrEmpty(theme.MainSpacerImage)) ? ImageFacade.GetMediaPath(theme.MainSpacerImage, MediaType.ThemeImage) : null;
                        WebCategoryImage = (!string.IsNullOrEmpty(theme.WebCategoryImage)) ? ImageFacade.GetMediaPath(theme.WebCategoryImage, MediaType.ThemeImage) : null;
                        OgUrl = LinkCanonicalUrl = string.Format("{0}/event/exhibitionlist.aspx?id={1}", config.SiteUrl, ThemeMainId);
                        this.Page.Title = theme.Title;
                        OgTitle = theme.FbShareTitle;
                        OgImage = (!string.IsNullOrEmpty(theme.FbShareIamge)) ? ImageFacade.GetMediaPath(theme.FbShareIamge, MediaType.ThemeImage) : null; //FB分享圖
                        if (!string.IsNullOrEmpty(theme.CssStyle))
                        {
                            litChangeCss.Text = theme.CssStyle.Split("@")[0];
                        }

                        OneDayCuration todayList = lists.TodayCurations;
                        if (todayList != null && todayList.IsActive)
                        {
                            litHotBannerTitle.Text = litNavToadayHot.Text = todayList.GroupName;
                            rpTodayBanner.DataSource = todayList.Curations;
                            rpTodayBanner.DataBind();
                            divTodayHot.Attributes.Remove("style");
                            liNavToadayHot.Attributes.Remove("style");
                        }
                        else
                        {
                            divTodayHot.Attributes.Add("style", "display:none");
                            liNavToadayHot.Attributes.Add("style", "display:none");
                        }

                        var allList = lists.AllCurations.Where(x => !x.IsTopGroup && x.IsActive);
                        if (allList != null)
                        {
                            rpBannerList.DataSource = allList;
                            rpBannerList.DataBind();

                            rpNav.DataSource = allList;
                            rpNav.DataBind();
                            SetShareUrl();
                        }
                        pan_main.Visible = true;
                    }
                    else
                    {
                        pan_sub.Visible = true;
                        if (theme.StartDate > DateTime.Now && theme.ShowInWeb) {
                            divClose.Attributes.Add("style", "display:none");
                            divComming.Attributes.Add("style", "display:block");
                        }
                        divTodayHot.Attributes.Add("style", "display:none");
                        liNavToadayHot.Attributes.Add("style", "display:none");
                    }
                }
                else
                {
                    pan_sub.Visible = true;
                    divTodayHot.Attributes.Add("style", "display:none");
                    liNavToadayHot.Attributes.Add("style", "display:none");
                }
            }
        }
        private void SetShareUrl()
        {
            Share_FB_Url = string.Format("https://www.facebook.com/dialog/share?app_id={0}&display=popup&href={1}&redirect_uri={2}",
                config.FacebookShareAppId,
                HttpUtility.UrlEncode(config.SiteUrl + Request.RawUrl),
                HttpUtility.UrlEncode(config.SiteUrl + Request.RawUrl)
            );
        }

        protected void rpTodayBanner_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ThemeCurationInfo)
            {
                ThemeCurationInfo tci = (ThemeCurationInfo)e.Item.DataItem;

                HtmlAnchor linkToday = e.Item.FindControl("linkToday") as HtmlAnchor;
                Image imgToday = e.Item.FindControl("imgToday") as Image;

                linkToday.HRef = tci.CurationLink;
                imgToday.ImageUrl = tci.ImageUrl;

                HtmlControl divBlock = e.Item.FindControl("divBlock") as HtmlControl;
                if (tci.IsActive)
                {
                    divBlock.Attributes.Add("style", "display:none;");
                }
                else
                {
                    divBlock.Attributes.Add("style", "display:block;");
                }
            }
        }

        protected void rpBannerList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is OneDayCuration)
            {
                OneDayCuration odc = (OneDayCuration)e.Item.DataItem;

                Literal litBannerTitle = (Literal)e.Item.FindControl("litBannerTitle");
                litBannerTitle.Text = odc.GroupName ?? "Day" + (e.Item.ItemIndex + 1) + " " + odc.EventDate.ToString("MM.dd");

                Literal litHeader = (Literal)e.Item.FindControl("litHeader");
                if (litHeader != null)
                {
                    litHeader.Text = "<div id=\"list_" + (e.Item.ItemIndex + 1).ToString("00") + "\" class=\"bn_day_wrap bn_day_wrap_bgcolor\">";
                }

                Literal litFooter = (Literal)e.Item.FindControl("litFooter");
                if (litFooter != null)
                {
                    litFooter.Text = "</div>";
                }

                Repeater rpSubList = (Repeater)e.Item.FindControl("rpSubList");
                rpSubList.DataSource = odc.Curations.Where(x => x.ShowInWeb);
                rpSubList.DataBind();
            }
        }

        protected void rpSubList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ThemeCurationInfo)
            {
                ThemeCurationInfo tci = (ThemeCurationInfo)e.Item.DataItem;

                HtmlAnchor linkBanner = e.Item.FindControl("linkBanner") as HtmlAnchor;
                Image imgBanner = e.Item.FindControl("imgBanner") as Image;

                linkBanner.HRef = tci.CurationLink;
                imgBanner.ImageUrl = tci.ImageUrl;
                imgBanner.Attributes.Add("class", "lazy");

                HtmlControl divBlock = e.Item.FindControl("divBlock") as HtmlControl;
                if (tci.IsActive)
                {
                    divBlock.Attributes.Add("style", "display:none;");
                }
                else
                {
                    divBlock.Attributes.Add("style", "display:block;");
                }
            }
        }

        protected void rpNav_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is OneDayCuration)
            {
                OneDayCuration odc = (OneDayCuration)e.Item.DataItem;

                HtmlAnchor linkNav = (HtmlAnchor)e.Item.FindControl("linkNav");
                linkNav.HRef = "#list_" + (e.Item.ItemIndex + 1).ToString("00");
                linkNav.InnerText = odc.GroupName ?? "Day" + (e.Item.ItemIndex + 1) + " " + odc.EventDate.ToString("MM.dd");
            }
        }
    }
}
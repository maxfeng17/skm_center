﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Presenters;
using System.Web;
using System.Text;
using System.Web.UI;
using LunchKingSite.WebLib.Models.Mobile;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

namespace LunchKingSite.Web.Event
{
    public partial class BrandMobile : BasePage, IBrandEvent
    {
        #region property

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public static ISysConfProvider SystemConfig
        {
            get
            {
                return config;
            }
        }

        private BrandEventPresenter _presenter;
        public BrandEventPresenter Presenter
        {
            set
            {
                _presenter = value;
                if (value != null)
                {
                    _presenter.View = this;
                }
            }
            get
            {
                return _presenter;
            }
        }

        private bool _eventAlive = true;
        public bool EventAlive
        {
            set
            {
                _eventAlive = value;
            }
            get
            {
                return _eventAlive;
            }
        }

        private string _mainPic;
        /// <summary>
        /// 活動主圖
        /// </summary>
        public string MainPic
        {
            set
            {
                _mainPic = value;
            }
            get
            {
                return (string.IsNullOrEmpty(_mainPic)) ? string.Empty : _mainPic;
            }
        }
        
        public string Url
        {
            get
            {
                if (Page.RouteData.Values["u"] != null)
                {
                    return Page.RouteData.Values["u"].ToString();
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["u"]))
                {
                    return Request.QueryString["u"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string Preview
        {
            get
            {
                return string.IsNullOrEmpty(Request.QueryString["p"]) ? string.Empty : (Request.QueryString["p"] == "show_me_the_preview") ? Request.QueryString["p"] : string.Empty;
            }
        }

        public string BgCss
        {
            get;
            set;
        }

        public bool DiscountPageShow
        {
            get;
            set;
        }

        public int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(Page.User.Identity.Name);
            }
        }

        public int BrandId
        {
            get;
            set;
        }

        public string Rsrc
        {
            get;
            set;
        }

        public string BrandTitle
        {
            get;
            set;
        }
        public string Share_FB_Url
        {
            get;
            set;
        }
        public string Share_Line_Url
        {
            get;
            set;
        }

        /// <summary>
        /// 啟用行動版商品主題活動頁
        /// </summary>
        public bool EnableMobileEventPromo { set; get; }

        public List<IGrouping<int?, ViewBrandCategory>> SubGroupList { get; private set; }

        public int CityId
        {
            get
            {
                int cid;
                if (Request.QueryString["cid"] != null && int.TryParse(Request.QueryString["cid"].ToString(), out cid))
                {
                    return cid;
                }
                else
                {
                    HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.CityId.ToString("g"));
                    if (cookie != null && int.TryParse(cookie.Value, out cid))
                    {
                        return cid;
                    }
                }
                return PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId;
            }
        }

        private bool IsMobileBroswer
        {
            get
            {
                var userAgent = HttpContext.Current.Request.UserAgent;
                if ((!string.IsNullOrEmpty(userAgent)) &&
                    (
                        (userAgent.ToLower().Contains("iphone")
                        || (userAgent.ToLower().Contains("ipod")
                        || (userAgent.ToLower().Contains("android")
                        || (userAgent.ToLower().Contains("Blackberry")))))))
                {
                    return true;
                }
                return false;
            }
        }

        public string BackMainUrl { get; set; }
        public string jsContentIds { get; set; }
        public string nsContentIds { get; set; }
        public Uri LatestUrl
        {
            get
            {
                return Request.UrlReferrer;
            }
        }

        public bool IsShowActDesc { set; get; }
        #endregion

        #region The Open Graph protocol
        public string FB_AppId
        {
            get
            {
                return ProviderFactory.Instance().GetConfig().FacebookApplicationId;
            }
        }
        /// <summary>
        /// meta og:title content
        /// </summary>
        public string OgTitle { set; get; }
        /// <summary>
        /// meta og:url content
        /// </summary>
        public string OgUrl { set; get; }
        /// <summary>
        /// meta og:description content
        /// </summary>
        public string OgDescription { set; get; }
        /// <summary>
        /// meta og:image content
        /// </summary>
        public string OgImage { set; get; }
        /// <summary>
        /// meta og:image:width content
        /// </summary>
        public int OgImageWidth { set; get; }
        /// <summary>
        /// meta og:image:height content
        /// </summary>
        public int OgImageHeight { set; get; }
        /// <summary>
        /// link rel=canonical href
        /// </summary>
        public string LinkCanonicalUrl { set; get; }
        /// <summary>
        /// link rel=image_src href
        /// </summary>
        public string LinkImageSrc { set; get; }
        #endregion

        protected override void OnInit(System.EventArgs e)
        {
            base.OnInit(e);
            if (!IsMobileBroswer)
            {
                if (Page.RouteData.Values["u"] != null)
                {
                    Response.Redirect(string.Format("/event/brandevent/{0}{1}", Url, Request.Url.Query));
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["u"]))
                {
                    Response.Redirect("brandevent.aspx" + Request.Url.Query);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Presenter.OnViewInitialized();
            }
            Presenter.OnViewLoaded();
        }

        /// <summary>
        /// PponDeal
        /// </summary>
        /// <param name="eventPromo"></param>
        /// <param name="list"></param>
        public void ShowBrandEvent(Brand brand, List<ViewBrandCategory> list)
        {
            if (!string.IsNullOrEmpty(MetaDescription))
            {
                Page.MetaDescription = string.Format("{0}-{1}", MetaDescription, config.Title);
            }
            else
            {
                Page.MetaDescription = config.DefaultMetaDescription;
            }

            if (list.Count == 0)
            {
                return;
            }

            if ((brand.Status && brand.StartTime <= DateTime.Now && brand.EndTime >= DateTime.Now) || Preview == "show_me_the_preview")
            {
                Title = brand.BrandName;
                if (!string.IsNullOrEmpty(brand.MobileMainPic))
                {
                    //過渡期程式，之後需移除
                    string tempMobileMainPic = ImageFacade.GetHtmlFirstSrc(brand.MobileMainPic);
                    if (tempMobileMainPic == string.Empty) tempMobileMainPic = brand.MobileMainPic;
                    imgMainPic.ImageUrl = ImageFacade.GetMediaPath(tempMobileMainPic, MediaType.DealPromoImage);

                    panMainPic.Visible = true;
                    SetShareUrl(brand);

                    //過渡期程式，之後需移除
                    string tempMainPic = ImageFacade.GetHtmlFirstSrc(brand.MainPic);
                    if (tempMainPic == string.Empty) tempMainPic = brand.MainPic;
                    OgImage = ImageFacade.GetMediaPath(tempMainPic, MediaType.DealPromoImage);
                }
                else
                {
                    panMainPic.Visible = false;
                }

                string bgstyle = string.Empty;
                if (!string.IsNullOrEmpty(brand.Bgpic))
                {
                    bgstyle += "url(" + brand.Bgpic + ") ";
                }

                if (!string.IsNullOrEmpty(brand.Bgcolor))
                {
                    bgstyle += "#" + brand.Bgcolor;
                }
                
                if (string.IsNullOrEmpty(brand.Description) || brand.Description == "\r\n")
                {
                    IsShowActDesc = false;
                }
                else
                {
                    IsShowActDesc = true;
                    lit_Description.Text = brand.Description;
                }

                BgCss = bgstyle;
                SubGroupList = list.GroupBy(x => x.BrandItemCategoryId).OrderBy(x => x.First().BrandItemCategorySeq).ToList();
                SetTabS();
                rpt_mainItem.DataSource = SubGroupList;
                rpt_mainItem.DataBind();

                //顯示可領取的折價券
                if (!string.IsNullOrEmpty(brand.DiscountList))
                {
                    if (!string.IsNullOrEmpty(brand.MobileRelayImage))
                    {
                        imgMobileRelay.Src = ImageFacade.GetMediaPath(brand.MobileRelayImage, MediaType.DealPromoImage);
                    }
                }
            }
            else
            {
                ShowEventExpire();
            }
        }

        protected void rpt_MainItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var dataItem = ((IGrouping<int?, ViewBrandCategory>)e.Item.DataItem);
                if (SubGroupList.Count > 0)
                {
                    ViewBrandCategory eventPromo = dataItem.First();
                    Literal headerData = (Literal)e.Item.FindControl("headerData");
                    Literal footerData = (Literal)e.Item.FindControl("footerData");
                    headerData.Text = string.Format("<div id='tabs-{0}'>", dataItem.Key.ToString());
                    footerData.Text = "</div>";
                }


                Repeater rpt_Item = (Repeater)e.Item.FindControl("rpt_Item");
                
                List<MultipleMainDealPreview> dealList = new List<MultipleMainDealPreview>();

                foreach (ViewBrandCategory vbc in dataItem.ToList())
                {
                    IViewPponDeal ppdeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vbc.BusinessHourGuid);

                    PponDealStage stage = ppdeal.GetDealStage();
                    //已售完檔次自動排序最後
                    if ((stage == PponDealStage.CouponGenerated || stage == PponDealStage.ClosedAndFail ||
                         stage == PponDealStage.ClosedAndOn) ||
                        ppdeal.OrderedQuantity >= ((ppdeal.OrderTotalLimit.HasValue) ? ppdeal.OrderTotalLimit : 0))
                    {
                        vbc.Seq = 9999 + vbc.Seq;
                    }

                    if (!(ppdeal.BusinessHourOrderTimeE <= DateTime.Now) && !(ppdeal.GroupOrderStatus != null && Helper.IsFlagSet((long)ppdeal.GroupOrderStatus, GroupOrderStatus.Completed))) //已下檔、已結檔的不加入列表
                    {
                        //非已結檔，非已下檔的才顯示
                        MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(0, vbc.Seq, ppdeal, new List<int>(), DealTimeSlotStatus.Default, 0);
                        dealList.Add(mainDealPreview);
                    }
                }
                
                List<MobileMainDealModel> brandItems = new MobileManager().GetMobileMainDeals(
                    ViewPponDealManager.DefaultManager.SortMultipleMainDealPreview(dealList, CategorySortType.Default), CityId);
                                
                rpt_Item.DataSource = brandItems;
                rpt_Item.DataBind();
            }
        }

        /// <summary>
        /// 活動過期
        /// </summary>
        public void ShowEventExpire()
        {
            EventAlive = panMainPic.Visible = false;
        }

        public void SetMemberCollection(List<Guid> collectBids)
        {
            string jsonStr = ProviderFactory.Instance().GetSerializer()
                .Serialize(collectBids);
            hdMemberCollectDealGuidJson.Value = jsonStr;
        }

        public string GetItemUrl(string Url) {
            return Url + (string.IsNullOrEmpty(Rsrc) ? "" : "/" + Rsrc);
        }

        #region private method
        private void SetTabS()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var tab in SubGroupList)
            {
                sb.AppendFormat("<li><a href='#tabs-{0}'>{1}</a></li>", tab.Key.ToString(), tab.First().CategoryName);
            }
            litItemTabs.Text = sb.ToString();
        }

        private void SetShareUrl(Brand brand)
        {
            var shareTitle = "";
            if (!string.IsNullOrEmpty(brand.DiscountList))
            {
                shareTitle = string.Format("Ｈi！我在17life 看到不錯的優惠活動，分享給你喔！ %0D%0A %0D%0A{0}（可領取折價折價券）:%0D%0A{1}", brand.BrandName, HttpUtility.UrlEncode(PromotionFacade.GetCurationTwoLink(brand)));
            }
            else
            {
                shareTitle = string.Format("Ｈi！我在17life 看到不錯的優惠活動，分享給你喔！ %0D%0A %0D%0A{0}（商品破盤價）:%0D%0A{1}", brand.BrandName, HttpUtility.UrlEncode(PromotionFacade.GetCurationTwoLink(brand)));
            }
            //%0D%0A 為LINE的換行符號
            Share_Line_Url = string.Format("http://line.naver.jp/R/msg/text/?{0}", shareTitle);
        }
        #endregion private method
    }
}
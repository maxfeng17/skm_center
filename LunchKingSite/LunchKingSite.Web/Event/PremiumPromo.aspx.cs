﻿using System.Web.Security;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;

namespace LunchKingSite.Web.Event
{
    public partial class PremiumPromo : BasePage, IPremiumPromo
    {
        #region property

        private PremiumPromoPresenter _presenter;

        public PremiumPromoPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string Url
        {
            get
            {
                return string.IsNullOrEmpty(Request.QueryString["u"]) ? string.Empty : Request.QueryString["u"];
            }
        }

        public string Preview
        {
            get
            {
                return string.IsNullOrEmpty(Request.QueryString["p"]) ? string.Empty : Request.QueryString["p"];
            }
        }

        public int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(Page.User.Identity.Name);
            }
        }

        public string UserName
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }



        public string BgCss { get; set; }

        public string MainBanner
        {
            set
            {
                li_MainBanner.Text = HttpUtility.HtmlDecode(value);
            }
        }

        public string BannerFontColor { get; set; }
        public string ButtonBackgroundColor { get; set; }
        public string ButtonFontColor { get; set; }
        public string ButtonBackgroundHoverColor { get; set; }

        private IEventProvider ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
        public int PremiumPromoId
        {
            get
            {
                EventPremiumPromo premiumPromo = ep.EventPremiumPromoGet(Url, EventPremiumPromoType.Basic);
                if (premiumPromo == null)
                {
                    return -1;
                }
                return premiumPromo.Id;
            }
            set
            {
                Session["PremiumPromoId"] = value;
            }
        }
        public string Rsrc { get; set; }
        public string PremiumTitle { get; set; }
        public int AppliedPremiumAmount { get; set; }
        public string ApplySuccessContent
        {
            set
            {
                li_ApplySuccessContent.Text = HttpUtility.HtmlDecode(value);
            }
        }

        #endregion property

        public event EventHandler<DataEventArgs<EventPremiumPromoItem>> SendAwardUserInfo;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                _presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        public void EventPremiumPromoSetting(EventPremiumPromo premiumpromo)
        {
            PremiumPromoId  = premiumpromo.Id;
            
            Rsrc = premiumpromo.Cpa;
            PremiumTitle = premiumpromo.Title;

            lit_Message.Text="<p></p><p></p>";
            if (AppliedPremiumAmount < premiumpromo.PremiumAmount)
            {
                lit_Message.Text = "<p>已有<span class=\"brfonT\">" + AppliedPremiumAmount + "</span>人成功領取~ </p><p>領完為止，要搶要快！</p>";
            }
            else
            {
                lit_Message.Text = "<p>感謝大家支持！<span class=\"brfonT\">" + premiumpromo.PremiumAmount + "</span>份已領取完畢！</p><p>&nbsp;</p>";
                ShowEventFinish();
            }

            string bgstyle = "background: center 91px no-repeat ";

            Page.Title = premiumpromo.Title;
            if (!string.IsNullOrEmpty(premiumpromo.BackgroundPicture))
            {
                bgstyle += "url(" + premiumpromo.BackgroundPicture + ") ";
            }

            if (!string.IsNullOrEmpty(premiumpromo.BackgroundColor))
            {
                bgstyle += "#" + premiumpromo.BackgroundColor;
            }

            BgCss = bgstyle;
            MainBanner = premiumpromo.MainBanner;

            BannerFontColor = (string.IsNullOrEmpty(premiumpromo.BannerFontColor)) ? "#fff200" : "#" + premiumpromo.BannerFontColor;
            ButtonBackgroundColor = (string.IsNullOrEmpty(premiumpromo.ButtonBackgroundColor)) ? "#f5278a" : "#" + premiumpromo.ButtonBackgroundColor;
            ButtonBackgroundHoverColor = (string.IsNullOrEmpty(premiumpromo.ButtonBackgroundHoverColor)) ? "#ff0060" : "#" + premiumpromo.ButtonBackgroundHoverColor;
            ButtonFontColor = (string.IsNullOrEmpty(premiumpromo.ButtonFontColor)) ? "#fff" : "#" + premiumpromo.ButtonFontColor;


            if (string.IsNullOrEmpty(premiumpromo.Description) || premiumpromo.Description == "\r\n" || premiumpromo.Description == "<br />")
            {

                lit_Description.Visible = false;
            }
            else
            {
                lit_Description.Visible = true;
                lit_Description.Text = premiumpromo.Description;
            }
            ApplySuccessContent = premiumpromo.ApplySuccessContent;

            if ((premiumpromo.Status && premiumpromo.StartDate <= DateTime.Now && premiumpromo.EndDate >= DateTime.Now && premiumpromo.Status) || Preview == "show_me_the_preview")
            {
            }
            else
            {
                ShowPremiumExpire();
            }
        }

        public void AwardUserInfoSetting(ViewMemberBuildingCityParentCity vmbcpc)
        {
            tbx_AwardName.Text = vmbcpc.LastName + vmbcpc.FirstName;
            tbx_AwardEMail.Text = vmbcpc.UserName;
            tbx_AwardMoblie.Text = vmbcpc.Mobile;

            hid_AwardAddress.Value = MemberFacade.GetMemberAddress(vmbcpc).Replace(vmbcpc.BuildingStreetName, "");

            hid_pcCityId.Value = string.IsNullOrEmpty(vmbcpc.PcCityId.ToString()) ? "-1" : vmbcpc.PcCityId.ToString();
            hid_CityId.Value = string.IsNullOrEmpty(vmbcpc.BCityId.ToString()) ? "-1" : vmbcpc.BCityId.ToString();
            hid_SubAddress.Value = string.IsNullOrEmpty(hid_AwardAddress.Value) ? "" : hid_AwardAddress.Value.Replace(vmbcpc.CityName, "").Replace(vmbcpc.PcCityName, "").Replace(vmbcpc.BuildingStreetName, "");
        }

        public void ShowLogin()
        {
            pl_Main.Visible = true;
            pl_Login.Visible = true;
            pl_AppAd.Visible = false;
            pl_OverUserAmount.Visible = false;
            pl_ApplyUserInfo.Visible = false;
            pl_Success.Visible = false;
            lit_Description.Visible = true;
            pl_PremiumsExpire.Visible = false;
            pl_Error.Visible = false;
        }

        public void ShowEventFinish()
        {
            pl_Main.Visible = true;
            pl_Login.Visible = false;
            pl_AppAd.Visible = true;
            pl_OverUserAmount.Visible = false;
            pl_ApplyUserInfo.Visible = false;
            pl_Success.Visible = false;
            lit_Description.Visible = true;
            pl_PremiumsExpire.Visible = false;
            pl_Error.Visible = false;
        }


        public void ShowPremiumExpire()
        {
            pl_Main.Visible = false;
            pl_Login.Visible = false;
            pl_AppAd.Visible = false;
            pl_OverUserAmount.Visible = false;
            pl_ApplyUserInfo.Visible = false;
            pl_Success.Visible = false;
            lit_Description.Visible = false;
            pl_PremiumsExpire.Visible = true;
            pl_Error.Visible = false;
        }

        public void ShowOverUserAmount()
        {
            pl_Main.Visible = true;
            pl_Login.Visible = false;
            pl_AppAd.Visible = false;
            pl_OverUserAmount.Visible = true;
            pl_ApplyUserInfo.Visible = false;
            pl_Success.Visible = false;
            lit_Description.Visible = true;
            pl_PremiumsExpire.Visible = false;
            pl_Error.Visible = false;
        }

        public void ShowApplyUserInfo()
        {
			pl_Main.Visible = true;
            pl_Login.Visible = false;
            pl_AppAd.Visible = false;
            pl_OverUserAmount.Visible = false;
            pl_ApplyUserInfo.Visible = true;
            pl_Success.Visible = false;
            lit_Description.Visible = false;
            pl_PremiumsExpire.Visible = false;
            pl_Error.Visible = false;
        }

        public void ShowSuccess()
        {
			pl_Main.Visible = true;
            pl_Login.Visible = false;
            pl_AppAd.Visible = false;
            pl_OverUserAmount.Visible = false;
            pl_ApplyUserInfo.Visible = false;
            pl_Success.Visible =  true;
            lit_Description.Visible = false;
            pl_PremiumsExpire.Visible = false;
            pl_Error.Visible = false;
        }

        public void ShowError()
        {
            pl_Main.Visible = true;
            pl_Login.Visible = false;
            pl_AppAd.Visible = false;
            pl_OverUserAmount.Visible = false;
            pl_ApplyUserInfo.Visible = false;
            pl_Success.Visible = false;
            lit_Description.Visible = false;
            pl_PremiumsExpire.Visible = false;
            pl_Error.Visible = true;
        }


        protected void lb_login_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(UserName))
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                //需檢查  已索取或未索取狀態。

                Response.Redirect(Request.RawUrl);

                //ShowCeckOrderSequence();
            }

        }

        protected void lb_Send_Click(object sender, EventArgs e)
        {
            bool isCheckData = true;
            //檢查姓名
            if (string.IsNullOrEmpty(tbx_AwardName.Text))
            {
                isCheckData = false;
            }

            //檢查Email格式
            if (RegExRules.CheckEmail(tbx_AwardEMail.Text) == false)
            {
                isCheckData = false;
            }

            if (isCheckData && SendAwardUserInfo != null)
            {
                var premiumPromoItem = new EventPremiumPromoItem();
                premiumPromoItem.EventPremiumPromoId = PremiumPromoId;
                premiumPromoItem.AwardName = tbx_AwardName.Text;
                premiumPromoItem.AwardMobile = tbx_AwardMoblie.Text;
                premiumPromoItem.AwardEmail = tbx_AwardEMail.Text;
                premiumPromoItem.AwardAddress = hid_AwardAddress.Value;
                premiumPromoItem.UserId = UserId;
                premiumPromoItem.CreateTime = DateTime.Now;

                if (premiumPromoItem.EventPremiumPromoId == -1)
                {
                    ShowError();
                }
                else
                {
                    this.SendAwardUserInfo(this, new DataEventArgs<EventPremiumPromoItem>(premiumPromoItem));
                }
                
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Models;


namespace LunchKingSite.Web.Event
{
    public partial class ExhibitionJoinPiinlife : BasePage, IExhibition
    {

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region property
        private ExhibitionPresenter _presenter;
        public ExhibitionPresenter Presenter
        {
            set
            {
                _presenter = value;
                if (value != null)
                {
                    _presenter.View = this;
                }
            }
            get
            {
                return _presenter;
            }
        }
        public string Url
        {
            get
            {
                return string.IsNullOrEmpty(Request.QueryString["u"]) ? string.Empty : Request.QueryString["u"];
            }
        }
        public string Preview
        {
            get
            {
                return string.IsNullOrEmpty(Request.QueryString["p"]) ? string.Empty : Request.QueryString["p"];
            }
        }
        public string BgCss
        {
            get;
            set;
        }
        public string MainPic
        {
            set { liMainPic.Text = HttpUtility.HtmlDecode(value); }
        }
        public string BtnActive
        {
            get;
            set;
        }
        public string BtnOriginal
        {
            get;
            set;
        }
        public string BtnHover
        {
            get;
            set;
        }
        public string BtnFontColor
        {
            get;
            set;
        }
        public int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(Page.User.Identity.Name);
            }
        }
        public int EventId
        {
            get;
            set;
        }
        public string Rsrc
        {
            get;
            set;
        }
        public string EventTitle
        {
            get;
            set;
        }

        #region SEO Keyword

        public string SeoKeyword
        {
            get;
            set;
        }
        public string SeoDescription
        {
            get;
            set;
        }
        #endregion

        #region The Open Graph protocol

        public string FbAppId
        {
            get
            {
                return ProviderFactory.Instance().GetConfig().FacebookApplicationId;
            }
        }
        /// <summary>
        /// meta og:title content
        /// </summary>
        public string OgTitle { set; get; }
        /// <summary>
        /// meta og:url content
        /// </summary>
        public string OgUrl { set; get; }
        /// <summary>
        /// meta og:description content
        /// </summary>
        public string OgDescription { set; get; }
        /// <summary>
        /// meta og:image content
        /// </summary>
        public string OgImage { set; get; }
        /// <summary>
        /// link rel=canonical href
        /// </summary>
        public string LinkCanonicalUrl { set; get; }
        /// <summary>
        /// link rel=image_src href
        /// </summary>
        public string LinkImageSrc { set; get; }
        /// <summary>
        /// 團購版型
        /// </summary>
        public EventPromoTemplateType TemplateType { set; get; }

        #endregion

        /// <summary>
        /// 啟用行動版商品主題活動頁
        /// </summary>
        public bool EnableMobileEventPromo { set; get; }

        /// <summary>
        /// 是否為策展活動
        /// </summary>
        public bool IsCuration { set; get; }
        #endregion

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                Presenter.OnViewInitialized();
            Presenter.OnViewLoaded();
            Page.MetaDescription = string.IsNullOrEmpty(SeoDescription) ? Page.MetaDescription : SeoDescription;
            Page.MetaKeywords = string.IsNullOrEmpty(SeoKeyword) ? Page.MetaKeywords : SeoKeyword;
        }
        #region local method
        protected string RegexReplaceSequence(string category)
        {
            return Regex.Replace(category, @"^[0-9]*\.", string.Empty);
        }
        #endregion
        #endregion
        #region method
        public void SetCategory(EventPromoItemCollection list)
        {

        }
        public void SetAllCategory(EventPromo eventPromo, List<ViewEventPromo> dataList)
        {
            DateTime now = DateTime.Now;
            string bgstyle = "background: center 91px no-repeat ";
            if (dataList.Count > 0)
            {
                var first = dataList.First();
                Page.Title = first.EventTitle;
                if (!string.IsNullOrEmpty(first.BgPic))
                    bgstyle += "url(" + dataList.First().BgPic + ") ";
                if (!string.IsNullOrEmpty(first.BgColor))
                    bgstyle += "#" + first.BgColor;
                BgCss = bgstyle;
                MainPic = first.MainPic;

                if (!string.IsNullOrEmpty(first.BtnFontColor))
                    BtnFontColor = "#" + first.BtnFontColor;
                if (!string.IsNullOrEmpty(first.BtnActive))
                    BtnActive = "url(" + first.BtnActive + ")";
                if (!string.IsNullOrEmpty(first.BtnHover))
                    BtnHover = "url(" + first.BtnHover + ")";
                if (!string.IsNullOrEmpty(first.BtnOriginal))
                    BtnOriginal = "url(" + first.BtnOriginal + ")";

                if (string.IsNullOrEmpty(first.EventDescription) || first.EventDescription == "\r\n")
                {
                    pan_ActivityMeasures.Visible = false;

                }
                else
                {
                    pan_ActivityMeasures.Visible = true;
                    lit_Description.Text = first.EventDescription;
                }
                
                ucCategoryBar.EventPromoData = eventPromo;
                ucCategoryBar.Item = dataList;
                ucCategoryBar.DataBind();
                ucSubCategoryBar.Item = dataList;
                ucSubCategoryBar.EventPromoData = eventPromo;
                ucSubCategoryBar.DataBind();
            }
        }
        public void ShowEventPromo(EventPromo eventPromo, List<ViewEventPromo> list)
        {
            pan_Main.Visible = pan_Sub.Visible = false;
            DateTime now = DateTime.Now;
            if (list.Count > 0)
            {
                var first = list.First();
                
                if ((first.EventStatus && first.StartDate <= now && first.EndDate >= now && first.EventStatus) || Preview == "show_me_the_preview")
                {
                    int i = 0;
                    foreach (var inneritem in ucCategoryBar.GroupCategory)
                    {
                        ExhibitionItem items = (ExhibitionItem)Page.LoadControl("ExhibitionItem.ascx");
                        items.ItemId = "cate" + i;
                        items.Items = inneritem.ToList();
                        items.EventPromoData = eventPromo;
                        items.DataBind();
                        pan_Items.Controls.Add(items);
                        i++;
                    }
                    pan_Main.Visible = true;
                }
                else
                {
                    ShowEventExpire();
                }
            }
        }

        public void ShowVourcherEventPromo(Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> dataList, Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> lotteryList, Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> storeList)
        {

        }
        public void ShowPiinlifeEventPromo(EventPromo eventPromo, List<ViewEventPromo> dataList)
        {
            if (dataList.Count > 0)
            {

                var first = dataList.First();

                if (string.IsNullOrEmpty(first.EventDescription) || first.EventDescription == "\r\n")
                {
                    pan_ActivityMeasures.Visible = false;

                }
                else
                {
                    pan_ActivityMeasures.Visible = true;
                    lit_Description.Text = first.EventDescription;
                }
            }
            var i = 0;
            foreach (var inneritem in ucCategoryBar.GroupCategory)
            {
                foreach (var list in dataList)
                {
                    if (list.Category == inneritem.Key)
                    {
                        list.CategoryName = "PiinlifeCategoryID" + i;
                    }
                }
                i++;
            }

                rptPiinlife.DataSource = dataList;
                rptPiinlife.DataBind();
                divPiinlife.Visible = true;
            
        }
        public void ShowPiinlifeEventPromo(List<ViewEventPromo> dataList)
        {
            var i = 0;
            foreach (var inneritem in ucCategoryBar.GroupCategory)
            {
                foreach (var list in dataList)
                {
                    if (list.Category == inneritem.Key)
                    {
                        list.CategoryName = "PiinlifeCategoryID" + i;
                    }
                }
                i++;
            }

            rptPiinlife.DataSource = dataList;
            rptPiinlife.DataBind();
            divPiinlife.Visible = true;  
        }
        public void ShowEventExpire()
        {
            pan_Sub.Visible = true;
        }


        public void rptPiinlife_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewEventPromo)
            {
                ViewEventPromo item = (ViewEventPromo)e.Item.DataItem;
                //Literal llRegionTab = (Literal)e.Item.FindControl("ll_RegionTab");
                PlaceHolder phEnableLink = (PlaceHolder)e.Item.FindControl("phEnableLink");
                PlaceHolder phDisabledLink = (PlaceHolder)e.Item.FindControl("phDisabledLink");
                Literal llHiDealStauts = (Literal)e.Item.FindControl("llHiDealStauts");

                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(item.BusinessHourGuid);

                if (config.InstallmentPayEnabled && deal.Installment3months.GetValueOrDefault())
                {
                    PlaceHolder phInstallment = e.Item.FindControl("phInstallment") as PlaceHolder;
                    if (phInstallment != null)
                    {
                        phInstallment.Visible = true;
                    }
                }

                PponDealStage stage = deal.GetDealStage();
                //已售完檔次自動排序最後

                if ((stage == PponDealStage.CouponGenerated || stage == PponDealStage.ClosedAndFail || stage == PponDealStage.ClosedAndOn) || deal.OrderedQuantity >= ((deal.OrderTotalLimit.HasValue) ? deal.OrderTotalLimit : 0))
                {
                    phEnableLink.Visible = false;
                    phDisabledLink.Visible = true;
                    if (DateTime.Now <= item.BusinessHourOrderTimeS)
                    {
                        llHiDealStauts.Text = @"<div class='evn-cpp-pinnlife-coming-bar'><img src='../Themes/default/images/17Life/G2/coming_bar_440.png' width='440' height='243' alt='' /></div>";
                    }
                    else
                    {
                        llHiDealStauts.Text = @"<div class='evn-cpp-pinnlife-sold-out-bar'><img src='../Themes/default/images/17Life/G2/soldout_bar_440.png' width='440' height='243' alt='' /></div>";
                    }
                }
                else
                {
                    if (DateTime.Now <= item.BusinessHourOrderTimeS)
                    {
                        phEnableLink.Visible = false;
                        phDisabledLink.Visible = true;
                        llHiDealStauts.Text = @"<div class='evn-cpp-pinnlife-coming-bar'><img src='../Themes/default/images/17Life/G2/coming_bar_440.png' width='440' height='243' alt='' /></div>";
                    }
                    else if (item.BusinessHourOrderTimeE <= DateTime.Now)
                    {
                        phEnableLink.Visible = false;
                        phDisabledLink.Visible = true;
                        llHiDealStauts.Text = @"<div class='evn-cpp-pinnlife-sold-out-bar'><img src='../Themes/default/images/17Life/G2/soldout_bar_440.png' width='440' height='243' alt='' /></div>";
                    }
                    else
                    {
                        phEnableLink.Visible = true;
                        phDisabledLink.Visible = false;
                    }
                }
            }
        }

        #endregion
    }
}
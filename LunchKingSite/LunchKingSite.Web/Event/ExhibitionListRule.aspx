﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="ExhibitionListRule.aspx.cs" Inherits="LunchKingSite.Web.Event.ExhibitionListRule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
    <meta property="fb:app_id" content="<%=FB_AppId%>">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<%=OgTitle%>" />
    <meta property="og:url" content="<%=OgUrl%>" />
    <meta property="og:site_name" content="17Life" />
    <meta property="og:description" content="<%=OgDescription%>" />
    <meta property="og:image" content="<%=OgImage%>" />
    <meta property="og:image:width" content="960" />
    <meta property="og:image:height" content="250" />
    <link rel="image_src" href="<%=LinkImageSrc%>" />
    <link rel="canonical" href="<%=LinkCanonicalUrl%>" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SSC" runat="server">
    <link href="../Themes/PCweb/css/MasterPage.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/PCweb/css/ppon.css" rel="stylesheet" type="text/css" />
    <%=LunchKingSite.Core.Helper.RenderCss("/Themes/PCweb/css/ppon_item.min.css") %>
    <link href="../themes/PCweb/css/Rightside.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/PCweb/plugin/font-awesome/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/PCweb/plugin/swiper/swiper.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../Tools/js/jquery.nav.js"></script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MC" runat="server">
    <asp:Literal ID="litHtml" runat="server" />
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="BrandEvent.aspx.cs" Inherits="LunchKingSite.Web.Event.BrandEvent" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.WebLib.Component" %>

<asp:Content ID="cDefaultMeta" ContentPlaceHolderID="cphPponMeta" runat="server">
    <meta property="fb:app_id" content="<%=FB_AppId%>">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<%=OgTitle%>" />
    <meta property="og:url" content="<%=OgUrl%>" />
    <meta property="og:site_name" content="17Life" />
    <meta property="og:description" content="<%=OgDescription%>" />
    <meta property="og:image" content="<%=OgImage%>" />
    <meta property="og:image:width" content="<%=OgImageWidth%>" />
    <meta property="og:image:height" content="<%=OgImageHeight%>" />
    <link rel="image_src" href="<%=LinkImageSrc%>" />
    <link rel="canonical" href="<%=LinkCanonicalUrl%>" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href="/themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="/themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="/themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    <link href="/Themes/default/images/17Life/G2/A3-event_coupons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/Tools/js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="/Tools/js/jquery.nav.js"></script>
    <%=LunchKingSite.Core.Helper.RenderCss("/Themes/PCweb/css/ppon_item.min.css") %>
    <script type="text/javascript" src="/Themes/PCweb/plugin/jquery.bxslider.js"></script>
    <script type="text/javascript" src="/Tools/js/jquery-ui-1.12.1.min.js"></script>
    <script type="text/javascript" src="/Tools/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="/Tools/js/ppon/pponMemberCollect.js"></script>

    <style type="text/css">
        .centercontent{
            width: 960px;
	        height: auto;
	        display: block;
	        margin: 10px auto;
        }

       @media only screen and (max-width : 1024px) and (max-width : 1366px){
           .right-menu{
               margin-right: 0px;
           }
           .item_search_result_wrap .item_3col_wrap .item_3col{
               margin-left: 0.5%;
               margin-right: 0.5%;
               margin-bottom: 2%;
           }
           .HKL_Button_to_event_page, .HKL_Button_back_to_top_command{
               right: 10px;
               margin-left: 0px;
           }
           .centercontent{
               width: 100%;
               margin: 0px;
	           height: auto;
	           display: block;
           }
        }      
      @media only screen and (min-width : 768px) and (max-width : 1023px)  {
           .function-menu-bar{
               width: auto;
               display:block;
           }
           .function-menu-bar .funtion-menu-block{
               width:100%;
           }
           .function-menu-bar .right-menu{
               margin-right: 0px;
           }
           .function-menu-bar .mc{
               display: block;
           }
           .header_wrap{
               display: none !important;
           }
           .mbe-menu #navi{
               width: 100%;
               display: block;
               margin: 20px 0 25px 0;
           }
           .mbe-menu #navimain{
               width: 100%;
               height: 30px;
           }
           #navimain .logo-inline{
               width: 15%;
               height: 60px;
               margin-top: -15px;
               margin-right: 10px;
               display:block;
           }
           #navimain .navbtn{
               display:block;
               font-size:12px;
           }
           #navimain li{
               width: 8%;
               height: 30px;
           }
           #navimain li img{
               display: none;
           }
           li .hot_icon, li .new_icon{
               margin-left:20px;
               margin-top: -12px;
           }
           .fr .search-block{
               display: block;
               margin-top: 2px;
               margin-left: -76px;
           }
           .search-block .search-bar{
               width: 88px;
               height: 22px;
           }
           .search-block .search-button{
               width: 38px;
               height: 26px;
           }
           .ch-itemdeli, .ch-beauty{
               display: block !important;
           }
           .centercontent{
               width: 100%;
               margin: 0px;
	           height: auto;
	           display: block;
           }
           .HKL_Button_to_event_page, .HKL_Button_back_to_top_command{
               right: 10px;
               margin-left: 0px;
           }
           .coupon_wrap{
               height: 108px;
           }
           .nd_banner{
               margin-left: 5px;
               margin-right: 0px;
           }
           .zitem{
               margin-left: 5px;
               margin-right: 0px;
               margin-bottom: 5px;
               height: 104px;
           }
           .item_search_result_wrap .item_3col_wrap .item_3col{
               margin-left: 0.5%;
               margin-right: 0.5%;
               margin-bottom: 2%;
           }
           .ly-evn-pprule{
               float: left;
               display: block;
               padding: 10px 0;
           }
           .ly-evn-pprule .evn-rule-title, .ly-evn-pprule .evn-rule-content{
               width: 98%;
           }
           .p3footer .contentblock_300{
                width: 31.38%;
                padding-top: 20px;
                padding-left: 8px;
           }
           .p3footer .contentblock_180{
                width: 16.54%;
                padding-top: 20px;
                padding-left: 8px;
           }
           .p3footer .footer_logo{
                width: 72px;
                height: 48px;
                margin: 0px;
           }
           .p3footer .footer_logo image{
               width: 72px;
               height: 48px;
           }
           .p3footer p {
               font-size: 12px;
               text-align: left;
           }
           .p3footer .support_c{
               display:none;
           }
           .p3footer .part-17life, .p3footer .part-pez-cert, .p3footer .part-serivce{
               display: block;
           }
           .TP_17life, .TP_PEZ, .TP_Certification, .TP_Service, .TP_twService{
               width:120px;
               height:24px;
           }
           .p3footer .twService{
               height: 48px;
               width: 90%;
           }
           .p3footer .twService .twService-img{
               width: 55px;
               height: 48px;
               margin-right: 0px;
           }
           .p3footer .twService image{
               width: 43px;
               height: 40px;
           }
      }

       .PPClose {
            width: 100%;
            height: 350px;
            background: url(../../Themes/PCweb/images/CloseBG.png) no-repeat;
            display: block;
        }

        .PPSpace {
            width: 100%;
            height: 187px;
            display: block;
        }

        .BackBtn {
            width: 286px;
            height: 76px;
            background: url(../../Themes/PCweb/images/CloseBtn.png);
            margin-left: 445px;
            display: block;
        }
        .st_banner_wrap {
            background: pink;
            <%=string.IsNullOrEmpty(MainBackgroundPic) ? "background: none;" : "background: center -0px no-repeat url(" + MainBackgroundPic + ");"%>            
            margin-top: -32px;
        }
		.item_3col .item_3col_pic img {
			width:initial;
		}
    	.zitem {
			margin-right: 5px;
			margin-bottom: 8px;
    	}
    	.bxslider {
			display:none;
    	}
    	.zitem a img {
			width: initial;
    	}
    </style>
    <script type='text/javascript'>
		//Facebook Pixel Code
		fbq('track', 'ViewCategory', {
			content_name: '<%=OgTitle%>',
			content_ids: [<%=jsContentIds%>],
			content_type: 'product'
		});

		function getVars() {
			var vars = {};
			var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
				function (m, key, value) {
					vars[key] = value;
				});
			return vars;
		}

		$(document).ready(function () {
			initCss();
			$("#show_my_group").click(function () {
				$(this).next(".my_group").fadeToggle("slow");
				$(this).toggleClass("click_on");
			});
			$('.bxslider').show();
			$('.bxslider').bxSlider({
				mode: 'fade',
				auto: true,
				captions: false,
				speed: 200,
				touchEnabled: false
			});
			$('.img-zoom').hover(function () {
				$(this).addClass('transition');

			}, function () {
				$(this).removeClass('transition');
			});
			$("#tabsA").tabs();
			var li_banner_index = $(".bx-viewport ul li").length; //li (行銷Banner)個數
			//行銷Banner個數<2 不顯示左右按鈕
			if (li_banner_index < 2) {
				$(".bx-controls-direction .bx-prev").css('display', 'none');
				$(".bx-controls-direction .bx-next").css('display', 'none');
				$(".bx-pager-item").attr("style", "display:none");
			}
			//===================領取coupon 201610 by stan========================
			var li_index = $(".coupon_slider ul li").length; //li (折價券)個數
			var slider_width_max = (154 * (li_index - 4)); //可滑動最大值
			var left_num = 0;
			var slider_width = (400 * li_index); //動態調整ul的寬度
			$(".coupon_wrap ul").css('width', slider_width);
			//折價券個數<5 不顯示左右按鈕
			if (li_index < 5) {
				$(".coupon_wrap .bx-prev").css('display', 'none');
				$(".coupon_wrap .bx-next").css('display', 'none');
			}
			$(".coupon_wrap .bx-prev").click(function () {
				left_num += 154;
				left_pos = left_num + 'px';
				if (left_num < slider_width_max) {
					$(".coupon_wrap ul").stop().animate({ left: 30 }, 400);
					left_num = 30;
				} else {
					$(".coupon_wrap ul").stop().animate({ left: left_pos }, 400);
				}
			});
			$(".coupon_wrap .bx-next").click(function () {
				left_num -= 154;
				left_pos = left_num + 'px';
				if (left_num < -slider_width_max) {
					$(".coupon_wrap ul").stop().animate({ left: 30 }, 400);
					left_num = 30;
				} else {
					$(".coupon_wrap ul").stop().animate({ left: left_pos }, 400);
				}
			});
			//======================領取coupon  End================================

			var url = window.location.href.split('&');
			if ('<%=DiscountPageShow%>' == 'True' && <%=BrandId%> != 0 && url.length > 1) {
				block_coupon();
			}
			$('.close_layer').attr('title', 'Click to unblock').click($.unblockUI);
			$("#mainpic_img").mouseover(function () {
				$('.coupon_btn').attr('src', 'https://www.17life.com/images/17P/active/exhibitionCh/btn_w_hover.png');
			})
				.mouseout(function () {
					$('.coupon_btn').attr('src', 'https://www.17life.com/images/17P/active/exhibitionCh/btn_w.png');
				});

			$("img.multipleDealLazy").lazyload({ placeholder: "https://www.17life.com/Themes/default/images/17Life/G2/ppon-M1_pic.jpg", effect: "fadeIn" }).removeClass("multipleDealLazy");
			$("img.lazy").lazyload({ placeholder: "https://www.17life.com/Themes/PCweb/images/ppon-M1_pic.jpg", threshold: 200, effect: "fadeIn" });

			//調整masterpage造成的影響
			$(".center").removeClass("center");

			$(window).resize(delayFunction(function () { resizefun(); }, { delay: 300 }));

			//only for活動說明滾動視窗
			$('a[href="#divActRule"]').click(function () {
				if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
					var target = $(this.hash);
					target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
					if (target.length) {
						$('html, body').animate({
							scrollTop: target.offset().top
						}, 1600);
						return false;
					}
				}
			});

			//分類若只剩全部則隱藏
			if ($('.btn_type').find('li').size() < 2) {
				$('.btn_type').hide();
			}

			//======================收藏================================
			var memberCollectDealGuids = $.parseJSON($('#hdMemberCollectDealGuidJson').val());
			$('#<%=(pan_Main).ClientID%>').pponMemberCollectList({ 'memberCollectionDealGuids': memberCollectDealGuids }).initMember();
		});

		function block_coupon() {

			var block_wrapper = $('#coupon_wrapper');
			block_wrapper.css('left', ($(window).width() - 700) / 2);
			block_wrapper.css('top', ($('.function-menu-bar').height() + $('.logo-inline').height()) * 1.5);

			$.blockUI({
				message: $('#coupon_wrapper'),
				css: {
					background: 'none',
					border: 'none',
					position: 'absolute',
					width: '100%',
					top: '0',
					left: '0',
				}
			});
		}

		function initCss() {
			$('#wrap').attr("style", "<%=BgCss %>");

			//行銷滿版模式
			if ('<%= IsMarketingMode %>' == 'True') {
				$(".guild").css('display', 'none');
			} else {
				//業務普通模式
				$(".st_banner_wrap").css('background', 'none');
			}

			//預設右下按鈕位置
			resizefun();
		}

		function resizefun(id, userId, index) {
			//調整右下策展和Go Top按鈕
			var window_width = $(window).width();
			var margin = window_width / 2 + 480 + 10;
			$('.HKL_Button_to_event_page').css('margin-left', margin + 'px');
			$('.HKL_Button_back_to_top_command').css('margin-left', margin + 'px');
		}

		function getInstantDiscount(id, userId, index) {
			if ('<%= (!isLogin) %>' == 'True') {
				var loginUrl = $('#HfLoginUrl').val();
				location.href = loginUrl;
				return;
			} else {
				$.ajax(
					{
						type: "POST",
						url: "BrandEvent.aspx/GetInstantDiscount",
						data: " { 'dcid': '" + id + "','userid':'" + userId + "'}",
						contentType: "application/json; charset=utf-8",
						dataType: "json",
						success: function (response) {
							var obj = $.parseJSON(response.d);
							switch (obj.Message) {
								case <%= (int)InstantGenerateDiscountCampaignResult.Success %> :
									$(".imgCoupon" + id).attr("src", "https://www.17life.com/images/17P/active/store/coupon_off.png");
									$(".btnCoupon" + id).removeAttr("onclick");
									$(".btnCoupon" + id).removeAttr("href");
									alert('領取成功！');
									break;
								case <%= (int)InstantGenerateDiscountCampaignResult.AlreadySent %> :
									$(".imgCoupon" + id).attr("src", "https://www.17life.com/images/17P/active/store/coupon_off.png");
									$(".btnCoupon" + id).removeAttr("onclick");
									$(".btnCoupon" + id).removeAttr("href");
									alert('您已經領取過折價券了喔！');
									break;
								case <%= (int)InstantGenerateDiscountCampaignResult.Exceed %> :
								case <%= (int)InstantGenerateDiscountCampaignResult.Expire %> :
									$("#ctl00_ctl00_ML_MC_rpDiscountList_ctl" + ("0" + index).slice(-2) + "_divSoldOut").attr("style", "");
									$(".btnCoupon" + id).removeAttr("onclick");
									$(".btnCoupon" + id).removeAttr("href");
									alert('這個折價券已經被領取完了！');
									break;
								default:
									alert('系統有誤！');
									break;
							}
						}, error: function () {
							alert('領取失敗！');
						}
					});
			}
		}
    </script>
    <!--Facebook Pixel Code-->
    <noscript>
          <img height="0" width="0" style="display:none" src="https://www.facebook.com/tr?id=274288942924758&ev=ViewCategory&cd[content_name]=<%=OgTitle%>&cd[content_ids]=<%=nsContentIds%>&cd[content_type]=product&noscript=1" />
    </noscript>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <input id="HfLoginUrl" type="hidden" value="<%=string.Format("{0}?ReturnUrl={1}", FormsAuthentication.LoginUrl, Server.UrlEncode((string)Session[LkSiteSession.NowUrl.ToString()]))%>" />
    <input id="hdMemberCollectDealGuidJson" type="hidden" runat="server" clientidmode="Static" />
    <script type="text/javascript">
		Sys.Application.add_load(initCss);
    </script>  
        <!--Navi-->
        <asp:PlaceHolder ID="divNavi" runat="server">
            <div class="guild">
                <div class="link"><asp:Literal ID="litBackMainUrl" runat="server" /></div>
            </div>
            <div class="clear"></div>
        </asp:PlaceHolder>

        <asp:PlaceHolder ID="divMainEvent" runat="server">
            <!--大圖-->
            <div class="st_banner_wrap">
                <div id="divMain" class="st_banner_main">
                <!-- // fb // -->
                <div class="fb">
                <div class="LoveBlindFBShare">
                    <div class="LoveBlindShare">
                        <a href="<%= Share_FB_Url %>" target="_blank" class="LoveBlindShare">
                            <img class="lazy" height="22" src="https://www.17life.com/images/17P/Promo/Love20131014/FB_Btn.png" width="147" />
                        </a>
                    </div>
                    <div class="LoveBlindFB">
                        <fb:like href="https://www.facebook.com/17life.com.tw" layout="button_count" show_faces="false"></fb:like>
                    </div>
                </div>
                </div>
                <!-- // fb //end -->                    
                  <ul class="st_banner_btn">
                    <li class="st_btn_coupon" style="<%= DiscountPageShow ? "":"display:none" %>">
                      <a href="#" onclick="<%= DiscountPageShow==true?"block_coupon();":"" %>"><i class="fa fa-ticket fa-fw"></i>領取折價券</a>
                    </li>
                    <li class="st_btn_notice" style="<%= ActRuleShow ? "":"display:none" %>">
                      <a href="#divActRule"><i class="fa fa-hand-pointer-o fa-fw"></i>活動辦法</a>
                    </li>
                  </ul>
                  <asp:Image ID="imgMainPic" runat="server" CssClass="main_img" />
            </div>
            </div>
            <div class="clear"></div>
        </asp:PlaceHolder>
        
    <div class="centercontent">      
        <!--行銷BANNER-->
        <asp:PlaceHolder ID="pan_banners" runat="server"></asp:PlaceHolder>
        
        <asp:Panel ID="pan_Main" runat="server" CssClass="item_search_result_wrap">
            <asp:PlaceHolder ID="pan_Items" runat="server"></asp:PlaceHolder>
            <div class="clear"></div>
        </asp:Panel>

        <asp:Panel ID="pan_ActivityMeasures" runat="server" CssClass="ly-evn-pprule" ClientIDMode="Static">
            <div id="divActRule" class="evn-rule-title">
                活動辦法
            </div>
            <div class="evn-rule-content">
                <asp:Literal ID="lit_Description" runat="server"></asp:Literal>
            </div>
        </asp:Panel>
        <asp:Panel ID="pan_Sub" runat="server" Visible="false">
            <div class="PPClose">
                <div class="PPSpace">
                </div>
                <a href="https://www.17life.com/ppon/" class="BackBtn"></a>
            </div>
        </asp:Panel>
        <div id="coupon_wrapper" style="display: none;">
            <div class="close_layer"><i class="fa fa-times"></i></div>
            <div>
                <%
                    if (BrandId!=0 && DiscountPageShow)
                    {
                        WebFormMVCUtil.RenderAction("GetDiscountCode", "Event", new { brandId = BrandId});
                    }
                %>
            
            </div>
        </div>
    </div>
     <script type="text/javascript">
        _bw('track', 'PageView', {
            page_type: 'Promotion',
        });
    </script>
</asp:Content>

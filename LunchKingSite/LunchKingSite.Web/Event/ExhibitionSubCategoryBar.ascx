﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExhibitionSubCategoryBar.ascx.cs" Inherits="LunchKingSite.Web.Event.ExhibitionSubCategoryBar" %>


<script type='text/javascript'>
    function ChangeSubMenu(index) {
        var keyArray = <%=KeyListJSon%>;
        var valueArray = <%=ValueListJson%>;
        var subKeyList = keyArray[index];
        var subValueList = valueArray[index];
        $("#subMenu").empty();

        if (subKeyList.length > 1) {
            $("#subMenu").show();
            $.each(subKeyList, function(i, data) {
                if(data!=""){
                var $newitem = $("<li><a href='#section-" + data + "'>" + subValueList[i] + "</a></li>");
                $("#subMenu").append($newitem);
            }
            });
        } else {
            $("#subMenu").hide();
        }

        $('.nav-anchor').onePageNav({
            currentClass: 'current',
            changeHash: false,
            scrollSpeed: 750,
            scrollThreshold: 0.5,
            filter: '',
            easing: 'swing',
            begin: function() {
                //I get fired when the animation is starting
            },
            end: function() {
                //I get fired when the animation is ending
            },
            scrollChange: function($currentListItem) {
                //I get fired when you enter a section and I pass the list item of the section
            }
        });
    }
</script>

<asp:PlaceHolder ID="pan_categoryBarCSS" runat="server">
    <asp:Literal runat="server" ID="categoryBarCSS"></asp:Literal>
</asp:PlaceHolder>

<asp:PlaceHolder runat="server" ID="pan_Category">
  <!--側邊anchor Start-->
  <ul id="subMenu" class="nav-anchor" style="top:150px">
  </ul>
<!--側邊anchor End-->
</asp:PlaceHolder>
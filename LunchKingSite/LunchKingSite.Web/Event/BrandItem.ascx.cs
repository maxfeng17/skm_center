﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using System.Text;
using LunchKingSite.WebLib.Models.Ppon;
using LunchKingSite.I18N;

namespace LunchKingSite.Web.Event
{
    public partial class BrandItem : System.Web.UI.UserControl
    {
        private ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public List<ViewBrandCategory> Items { get; set; }
        public Brand MainData { get; set; }        
        public List<IGrouping<int?, ViewBrandCategory>> SubGroupList { get; private set; }
        public bool IsEnablePager { get; set; }
        public int PageSize { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public override void DataBind()
        {
            SubGroupList = Items.GroupBy(x => x.BrandItemCategoryId).OrderBy(x => x.First().BrandItemCategorySeq).ToList();
            SetTabS();
            rpt_mainItem.DataSource = SubGroupList;
            rpt_mainItem.DataBind();
        }

        protected void rpt_MainItem_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var dataItem = ((IGrouping<int?, ViewBrandCategory>)e.Item.DataItem);
                if (SubGroupList.Count > 0)
                {
                    Literal headerData = (Literal)e.Item.FindControl("headerData");
                    Literal footerData = (Literal)e.Item.FindControl("footerData");
                    headerData.Text = string.Format("<div id='tabs-{0}' class='tab_content'><div class='item_3col_wrap'>", dataItem.Key.ToString());
                    footerData.Text = "</div></div>";
                }


                Repeater rpt_Item = (Repeater)e.Item.FindControl("rpt_Item");

                List<ViewMultipleMainDeal> model = new List<ViewMultipleMainDeal>();
                
                List<MultipleMainDealPreview> dealList = new List<MultipleMainDealPreview>();

                foreach (ViewBrandCategory vbc in dataItem.ToList())
                {
                    IViewPponDeal ppdeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vbc.BusinessHourGuid, true);
                    MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(0,
                        vbc.Seq, ppdeal, new List<int>(), DealTimeSlotStatus.Default, 0);
                    dealList.Add(mainDealPreview);
                }

                model = GetBrandEventViewMultipleMainDeal(dealList.OrderBy(x => x.Sequence).ToList());

                rpt_Item.DataSource = model;
                rpt_Item.DataBind();
            }
        }

        private void SetTabS()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var tab in SubGroupList) {
                sb.AppendFormat("<li class='tab_title'><a href='#tabs-{0}'>{1}</a></li>", tab.Key.ToString(), tab.First().CategoryName);
            }
            litItemTabs.Text = sb.ToString();
        }
        
        /// <summary>
        /// 將Default頁檔次列表整理成廠商館要顯示的資訊
        /// </summary>
        /// <param name="deals"></param>
        /// <returns></returns>
        private List<ViewMultipleMainDeal> GetBrandEventViewMultipleMainDeal(List<MultipleMainDealPreview> deals)
        {
            List<ViewMultipleMainDeal> viewMultipleMainDeals = new List<ViewMultipleMainDeal>();

            foreach (var deal in deals)
            {
                ViewMultipleMainDeal viewDealPreview = new ViewMultipleMainDeal();
                viewDealPreview.deal = deal;
                bool _isKindDeal = Helper.IsFlagSet(deal.PponDeal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal);
                PponCity pponcity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(deal.CityID);

                //貼上檔次 Icon 標籤
                string icon_string = PponFacade.GetDealIconHtmlContentList(deal.PponDeal, 2);


                viewDealPreview.deal_Label_1 = icon_string;
                viewDealPreview.deal_Label_2 = icon_string;
                viewDealPreview.clit_CityName = deal.PponDeal.IsMultipleStores ?
                   "<div class='hover_place_3col' style='display:none;'><span class='hover_place_text'><i class='fa fa-map-marker'></i>" + deal.PponDeal.HoverMessage + "</span></div>" : string.Empty;

                viewDealPreview.clit_CityName2 = ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(
                pponcity, deal.PponDeal.BusinessHourGuid);


                switch (deal.PponDeal.GetDealStage())
                {
                    case PponDealStage.Created:
                    case PponDealStage.Ready:
                        viewDealPreview.litCountdownTime = "即將開賣";
                        break;

                    case PponDealStage.ClosedAndFail:
                    case PponDealStage.ClosedAndOn:
                    case PponDealStage.CouponGenerated:
                        viewDealPreview.litCountdownTime = "已結束販售";
                        break;
                    case PponDealStage.Running:
                    case PponDealStage.RunningAndFull:
                    case PponDealStage.RunningAndOn:
                    default:
                        int daysToClose = (int)(deal.PponDeal.BusinessHourOrderTimeE - DateTime.Now).TotalDays;
                        if (daysToClose > 3)
                        {
                            viewDealPreview.litCountdownTime = "限時優惠中!";
                        }
                        else
                        {
                            viewDealPreview.litDays = daysToClose + Phrase.Day;
                            viewDealPreview.litCountdownTime = SetCountdown(deal.PponDeal.BusinessHourOrderTimeE);

                            viewDealPreview.timerTitleField_dataStart = deal.PponDeal.BusinessHourOrderTimeS.ToJavaScriptMilliseconds();
                            viewDealPreview.timerTitleField_dataEnd = deal.PponDeal.BusinessHourOrderTimeE.ToJavaScriptMilliseconds();
                        }
                        break;
                }

                if (_isKindDeal)
                {
                    viewDealPreview.discount_1 = viewDealPreview.discount_2 = "公益";
                }
                else if (deal.PponDeal.ItemPrice == deal.PponDeal.ItemOrigPrice || Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown)
                      || Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice))
                {
                    viewDealPreview.discount_1 = viewDealPreview.discount_2 = "特選";
                }
                else if (deal.PponDeal.ItemPrice == 0)
                {
                    {
                        viewDealPreview.discount_1 = viewDealPreview.discount_2 = "優惠";
                    }
                }
                else
                {
                    string discount = ViewPponDealManager.GetDealDiscountString(
                        Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), 
                        PponFacade.CheckZeroPriceToShowPrice(deal.PponDeal, 0), deal.PponDeal.ItemOrigPrice);
                    int length = discount.Length;
                    if (length > 0)
                    {
                        viewDealPreview.discount_2 = discount.Substring(0, 1);
                        if (discount.IndexOf('.') != -1 && length > 2)
                        {
                            viewDealPreview.discount_2 += discount.Substring(1, length - 1);
                        }

                        viewDealPreview.discount_1 = viewDealPreview.discount_2 + "折";
                        viewDealPreview.discount_2 += "<span class=\"smalltext\">折</span>";
                    }
                }

                if (cp.InstallmentPayEnabled == false)
                {
                    viewDealPreview.litInstallmentPriceLabel = "<p>好康價</p>";
                }
                else if (deal.PponDeal.Installment3months.GetValueOrDefault() == false)
                {
                    viewDealPreview.litInstallmentPriceLabel = "<p>好康價</p>";
                }
                else
                {
                    viewDealPreview.litInstallmentPriceLabel = "<p class='price_installment'>分期0利率</p>";
                }
                viewMultipleMainDeals.Add(viewDealPreview);
                decimal ratingScore;
                int ratingNumber;
                bool ratingShow;
                viewDealPreview.RatingString = PponFacade.GetNewDealStarRating(
                    deal.PponDeal.BusinessHourGuid, "pc", out ratingScore, out ratingNumber, out ratingShow);
            }

            return viewMultipleMainDeals;
        }

        private string SetCountdown(DateTime timeE)
        {
            TimeSpan ts = timeE - DateTime.Now;
            int hourUntil = ts.Hours;
            int minutesUntil = ts.Minutes;
            int secondsUntil = ts.Seconds;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<div class='TimeConDigit hn'>{0}</div>", hourUntil);
            sb.AppendFormat("<div class='TimeConUnit hl'>{0}</div>", Phrase.Hour);
            sb.AppendFormat("<div class='TimeConDigit mn'>{0}</div>", minutesUntil);
            sb.AppendFormat("<div class='TimeConUnit ml'>{0}</div>", "分");
            sb.AppendFormat("<div class='TimeConDigit sn'>{0}</div>", secondsUntil);
            sb.AppendFormat("<div class='TimeConUnit sl'>{0}</div>", Phrase.Second);
            return sb.ToString();
        }

        protected string GetDiscountValue(bool bln)
        {
            throw new NotImplementedException();
        }
    }
}
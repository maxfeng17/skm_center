﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="BrandMobile.aspx.cs" Inherits="LunchKingSite.Web.Event.BrandMobile" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Models.Mobile" %>
<%@ Import Namespace="LunchKingSite.WebLib.Component" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
    <meta property="fb:app_id" content="<%=FB_AppId%>">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<%=OgTitle%>" />
    <meta property="og:url" content="<%=OgUrl%>" />
    <meta property="og:site_name" content="17Life" />
    <meta property="og:description" content="<%=OgDescription%>" />
    <meta property="og:image" content="<%=OgImage%>" />
    <meta property="og:image:width" content="<%=OgImageWidth%>" />
    <meta property="og:image:height" content="<%=OgImageHeight%>" />
    <link rel="image_src" href="<%=LinkImageSrc%>" />
    <link rel="canonical" href="<%=LinkCanonicalUrl%>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="/themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="/themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="/themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/Themes/mobile/css/style.css" type="text/css" />
    <link type="text/css" rel="stylesheet" href="/Themes/mobile/css/font-awesome-4.3.0/css/font-awesome.css">
    <link href="/Themes/default/images/17Life/G2/A3-event_coupons.css" rel="stylesheet" type="text/css" />

    <script src="/Tools/js/jquery.mousewheel.min.js" type="text/javascript"></script>
    <script src="/Tools/js/jquery.kinetic.min.js" type="text/javascript"></script>
    <script src="/Tools/js/TweenMax.min.js" type="text/javascript"></script>
    <style type="text/css">
        .center {
            margin-top: 0px; /*調整masterpage造成的影響*/
        }
    </style>
    <script type='text/javascript'>
		//Facebook Pixel Code
		fbq('track', 'ViewCategory', {
			content_name: '<%=OgTitle%>',
			content_ids: [<%=jsContentIds%>],
			content_type: 'product'
		});

		function getVars() {
			var vars = {};
			var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
				function (m, key, value) {
					vars[key] = value;
				});
			return vars;
		}

		$(document).ready(function () {
			if ($("#hidEventAlive").val() == "False") {
				$("#brandEventContent").hide();
				$("#brandEventEnd").show();
			} else {
				initCss();
				$("#tabsA").tabs();
				ClickMenuOn();
			}

			//show 活動辦法
			var categoryBtn = $("#categoryBtn");
			var conditionsBtn = $("#conditionsBtn");
			categoryBtn.click(function () {
				categoryBtn.addClass("rdl-on");
				conditionsBtn.removeClass("rdl-on");
				$("#dealContent").show();
				$("#conditions").hide();
			});
			conditionsBtn.click(function () {
				conditionsBtn.addClass("rdl-on");
				categoryBtn.removeClass("rdl-on");
				$("#conditions").show();
				$("#dealContent").hide();
			});

			//調整masterpage造成的影響
			$("#target").hide();
			$("#tabsA").removeClass('ui-widget-content');
			$(".ui-tabs-anchor").css('text-decoration', 'none');
			$(".p3footer").addClass('clearfix');

			TweenMax.from($("#coupon_wrapper"), 0.1, { y: 0, opacity: 0 });
			TweenMax.to($("#coupon_wrapper"), 0.1, { y: window_height, opacity: 0 });
			$('.coupon_rule').addClass('last_div');

			var url = window.location.href.split('&');
			if ('<%=DiscountPageShow%>' == 'True' && <%=BrandId%> != 0 && url.length > 1) {
				setTimeout(function () {
					block_coupon()
				}, 1500);
			}
		});

		var window_height = $(window).height();
		function block_coupon() {
			TweenMax.from($("#coupon_wrapper"), 0.5, { y: window_height, opacity: 1 });;
			TweenMax.to($("#coupon_wrapper"), 0.5, { y: 0, opacity: 1 });

			$.blockUI({
				message: $('#coupon_wrapper'),
				css: {
					background: 'none',
					border: 'none',
					position: 'absolute',
					width: '100%',
					top: '0',
					left: '0',
				}
			});

			$(".coupon_center").on("touchmove", function (event) {
				event.preventDefault;
			}, false);

		}

		function unblock_coupon_mb() {
			TweenMax.from($("#coupon_wrapper"), 0.5, { y: 0, opacity: 1 });;
			TweenMax.to($("#coupon_wrapper"), 0.5, { y: window_height, opacity: 0 });
			$(".coupon_center").off("touchmove");
			$.unblockUI();
		}

		function initCss() {
			$('#brandEventContent').attr("style", "<%=BgCss %>");
		}

		function ClickMenuOn() {
			var eventUrl = (window.location.pathname + window.location.search).toLowerCase().replace("brandmobile", "brandevent");

			var itemList = $(".content_store ul.store_nav li a");
			$.each(itemList, function (i, item) {
				var href = item.href.substr(item.href.indexOf("?")).toLowerCase();
				if (eventUrl.indexOf(href) > 0) {
					$(item).attr("class", "on");
					$(item).focus();
				}
			});
		}

		//分享
		$(function () {
			var flag_share = 0; //未點擊 
			$(".store_bn a.coupon_share_on").click(function () {
				if (flag_share == 0) {
					$(this).find(".fa-share-alt").css("display", "none");
					$(this).find(".fa-close").css("display", "inline-block");
					$('a.coupon_share_line').css("display", "block");
					$('a.coupon_share_fb').css("display", "block");
					$('a.coupon_share_line').animate({
						"bottom": "25px",
						"right": "70px"
					}, 200, "easeOutExpo");
					$('a.coupon_share_fb').animate({
						"bottom": "70px",
						"right": "25px"
					}, 200, "easeOutExpo");
					flag_share = 1;
				} else {
					$(this).find(".fa-share-alt").css("display", "inline-block");
					$(this).find(".fa-close ").css("display", "none");
					$('a.coupon_share_line').css("display", "none");
					$('a.coupon_share_fb').css("display", "none");
					$('a.coupon_share_line').animate({
						"bottom": "10px",
						"right": "10px"
					}, 200, "easeOutExpo");
					$('a.coupon_share_fb').animate({
						"bottom": "10px",
						"right": "10px"
					}, 200, "easeOutExpo");
					flag_share = 0;
				}
			});
        });
        
        _bw('track', 'PageView', {
            page_type: 'Promotion'
        });
    </script>
    <!--Facebook Pixel Code-->
    <noscript>
        <img height="0" width="0" style="display: none" src="https://www.facebook.com/tr?id=274288942924758&ev=ViewCategory&cd[content_name]=<%=OgTitle%>&cd[content_ids]=<%=nsContentIds%>&cd[content_type]=product&noscript=1" />
    </noscript>
    <input id="hdMemberCollectDealGuidJson" type="hidden" runat="server" clientidmode="Static" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <script type="text/javascript">
		Sys.Application.add_load(initCss);
    </script>
    <input id="hidEventAlive" type="hidden" value="<%=EventAlive%>" />
    <input id="HfLoginUrl" type="hidden" value="<%=string.Format("{0}?ReturnUrl={1}", FormsAuthentication.LoginUrl, Server.UrlEncode((string)Session[LkSiteSession.NowUrl.ToString()]))%>" />
    <div id="brandEventContent" class="content_store">
        <input id="HfCityId" type="hidden" value="<%= CityId%>" />

        <!--// 主圖bn //-->
        <asp:PlaceHolder ID="panMainPic" runat="server">
            <div class="store_bn">
                <ul>
                    <li>
                        <a href="javascript:void(0)" class="coupon_share_on">
                            <i class="fa fa-share-alt  fa-lg fa-fw"></i>
                            <i class="fa fa-close  fa-lg fa-fw" style="display: none;"></i>
                        </a>
                    </li>
                    <li>
                        <a href="<%= Share_FB_Url %>" target="_blank" class="coupon_share_fb" style="display: none;">
                            <img src="https://www.17life.com/images/17P/active/store/icon_fb.png" />
                        </a>
                    </li>
                    <li>
                        <a href="<%= Share_Line_Url %>" target="_blank" class="coupon_share_line" style="display: none;">
                            <img src="https://www.17life.com/images/17P/active/store/icon_line.png" />
                        </a>
                    </li>
                </ul>
                <asp:Image ID="imgMainPic" runat="server" />
            </div>
        </asp:PlaceHolder>
        <!--// 主圖bn //end-->
        <div id="coupon_area" style="<%= DiscountPageShow==true?"": "display:none;" %>">
            <div class="coupon_img" onclick="block_coupon();">
                <img src="/Themes/default/images/17P/coupon_banner_mb.jpg" alt="" id="imgMobileRelay" runat="server" />
            </div>
        </div>

        <div class="rdl-list-box clearfix" style="<%= (IsShowActDesc) ? "": "display:none"%>">
            <div class="rdl-Btn-box clearfix">
                <div id="categoryBtn" class="rdl-list-Lbtn rdl-on">
                    優惠商品
                </div>
                <div id="conditionsBtn" class="rdl-list-Rbtn">活動辦法</div>
            </div>
        </div>

        <div id="dealContent" class="ly-e-cop-farme clearfix">
            <!--// tabs //-->
            <div id="tabsA">
                <ul class="store_filter">
                    <asp:Literal ID="litItemTabs" runat="server" />
                </ul>
                <!--// maincontent 主要內容//-->
                <div class="maincontent">
                    <asp:Repeater ID="rpt_mainItem" OnItemDataBound="rpt_MainItem_ItemDataBound" runat="server">
                        <ItemTemplate>
                            <asp:Literal runat="server" ID="headerData" />
                            <asp:Repeater ID="rpt_Item" runat="server">
                                <ItemTemplate>
                                    <div class="dealcard">
                                        <a href="<%# ((MobileMainDealModel)(Container.DataItem)).Url %>">
                                            <div class="dealcard_left">
                                                <div class="left_img">
                                                    <img class="soldout_img" src="https://www.17life.com/Images/17P/17topbn/<%# ((MobileMainDealModel)(Container.DataItem)).IsBankDeal ? "completed_260x260.png" : "17_soldout_260x260.png" %>" style="<%# (((MobileMainDealModel)(Container.DataItem)).IsSoldOut) ? "": "display:none" %>">
                                                    <div class="activeLogo_img" style="<%# string.IsNullOrEmpty(((MobileMainDealModel)(Container.DataItem)).EventImgUrl)? "display:none": "" %>">
                                                        <%# !string.IsNullOrEmpty(((MobileMainDealModel)(Container.DataItem)).EventImgUrl)? ((MobileMainDealModel)(Container.DataItem)).EventImgUrl: "#" %>
                                                    </div>
                                                    <img src="<%# !string.IsNullOrEmpty(((MobileMainDealModel)(Container.DataItem)).AppPicUrl)? ((MobileMainDealModel)(Container.DataItem)).AppPicUrl:((MobileMainDealModel)(Container.DataItem)).PicUrl %>">													
													<%# ((MobileMainDealModel)(Container.DataItem)).ChannelAndRegionCategories.Contains(LunchKingSite.BizLogic.Component.ViewPponDealManager._24H_ARRIVAL_CATEGORY_ID) ? "<img class='flag-24h' src='/themes/PCweb/images/24h_cover_flag.svg' alt='' />" : string.Empty%>
                                                </div>
                                            </div>
                                            <div class="dealcard_right">
                                                <div class="dealcard_title">
                                                    <%# string.IsNullOrEmpty(((MobileMainDealModel)(Container.DataItem)).Location) ? ""
                                                            : "<span class='title_location'>" + ((MobileMainDealModel)(Container.DataItem)).Location + "</span>"%>
                                                    <span class="title_brand"><%#((MobileMainDealModel)(Container.DataItem)).Title %></span>
                                                    <span class="title_name"><%#((MobileMainDealModel)(Container.DataItem)).Description %></span>
                                                </div>
                                                <div class="dealcard_price" style="<%# (((MobileMainDealModel)(Container.DataItem)).IsBankDeal ? "display:none;": "" )%>">
                                                    <span class="price_oriprice"><%#((MobileMainDealModel)(Container.DataItem)).OrigPriceString %></span>
                                                    <span class="price_discount"><%#((MobileMainDealModel)(Container.DataItem)).DiscountString %></span>
                                                    <span class="price_dealprice">
                                                        <%#((MobileMainDealModel)(Container.DataItem)).PriceString %>
                                                    </span>
                                                </div>
                                                <!-- 在地檔才有評價 -->
                                                <%# ((MobileMainDealModel)(Container.DataItem)).RatingString %>
                                               <div class='dealcard_discount clearfix'>
                                                <%# string.IsNullOrEmpty(((MobileMainDealModel)(Container.DataItem)).DiscountPrice) ? 
                                                        @"" 
                                                        :  
                                                        @"<i class='fa fa-bullhorn' aria-hidden='true'></i>
                                                        <div class='disc_text'>使用折價</div>
                                                        <span class='disc_price color_red'>" + ((MobileMainDealModel)(Container.DataItem)).DiscountPrice 
                                                        + @"</span>
                                                        <span class='disc_gray' style='display:inline-block'>" + ((MobileMainDealModel)(Container.DataItem)).DiscountPriceTail
                                                        + @"</span>
                                                    " %>
                                               </div>
                                                <div class="dealcard_info" style="<%# (((MobileMainDealModel)(Container.DataItem)).IsBankDeal ? "display:none;": "" )%>">
                                                    <div class="info_buyer"><%#((MobileMainDealModel)(Container.DataItem)).BuyerString %></div>
                                                    <ul class="info_tag">
                                                        <%#((MobileMainDealModel)(Container.DataItem)).TagsString %>
                                                    </ul>
                                                </div>
                                            </div>
                                        </a>
                                    </div>



                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:Literal runat="server" ID="footerData" />
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <!--// maincontent 主要內容//end-->
            </div>
            <!--// tabs //end-->
        </div>

        <div class="rdl-list-box" id="conditions" style="display: none">
            <div class="rdl-evn-pprule e_shadow">
                <div class="rdl-ru-title">活動辦法</div>
                <div class="rdl-ru-content">
                    <asp:Literal ID="lit_Description" runat="server" />
                </div>
            </div>
        </div>

        <!--// content 內容區塊//end-->
    </div>

    <div id="brandEventEnd" class="content_store" style="display: none">
        <div class="ly-e-cop-ppon piinlife rptTitle">本活動已經結束囉</div>
        <a href="<%=SystemConfig.SiteUrl%>">去看看其它好康</a>
    </div>
    <div id="coupon_wrapper" style="display: none;">
        <div>
            <%
                if (BrandId != 0 && DiscountPageShow)
                {
                    WebFormMVCUtil.RenderAction("GetDiscountCode", "Event", new { brandId = BrandId });
                }
            %>
        </div>
        <div>
        </div>
    </div>
</asp:Content>

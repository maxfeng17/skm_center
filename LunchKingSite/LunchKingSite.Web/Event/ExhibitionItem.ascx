﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExhibitionItem.ascx.cs"
    Inherits="LunchKingSite.Web.Event.ExhibitionItem" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="System.Collections.Concurrent" %>
<div id="<%=ItemId %>" style="display: none;" class="cate">
    <%= IsEnablePager && Items.Count > PageSize ? "<div id='banner-slide'><ul class='bjqs'><li>" : string.Empty %>
    <asp:Repeater ID="rpt_mainItem" OnItemDataBound="rpt_MainItem_ItemDataBound"  runat="server">
        <ItemTemplate>
            <asp:Literal runat="server" ID="headerData"></asp:Literal>
            <asp:Repeater ID="rpt_Item" OnItemDataBound="rpt_Item_ItemDataBound" runat="server">
                <ItemTemplate>
                    <asp:Panel ID="pan_EnableLink" runat="server" CssClass="evn-cop-buy-sf-box e_shadow" Visible="<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS<=DateTime.Now %>">
                        <a href='<%#"../deal/" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid + (string.IsNullOrEmpty(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa) ? string.Empty : ("?rsrc=" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa))%>'
                            target="_blank">
                                <div class="evn-cop-buy-pic">
                                    <img src="<%# ImageFacade.GetMediaPathsFromRawData(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.ImagePath, MediaType.PponDealPhoto).DefaultIfEmpty("../Themes/default/images/17Life/G2/PPImage/PPDPic.jpg").First()%>" alt="<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Title%>" width="290" height="162" />
                                    <div class="activeLogo alo-m"><asp:Literal ID="lit_DealPromoImage" runat="server"></asp:Literal></div>
                                    <div class="evn-cop-buy-sold-out-bar-290" style="<%# (((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Value) ? " display:block": " display:none" %>">
                                        <img src="../Themes/PCweb/images/soldout_bar_290.png" width="290" height="162" />
                                    </div>
                                </div>
                                <div class="evn-cop-buytitle">
                                    <a href="<%#"../deal/" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid + (string.IsNullOrEmpty(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa) ? string.Empty : ("?rsrc=" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa))%>" target="_blank">
                                        <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Title%>
                                    </a>
                                </div>
                                <div class="evn-cop-minorange-title">
                                    <a href="<%#"../deal/" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid + (string.IsNullOrEmpty(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa) ? string.Empty : ("?rsrc=" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa))%>" target="_blank">
                                        <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Description%>
                                    </a>
                                </div>
                                <div class="evn-cop-buyinformation" style='<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS <= DateTime.Now ? " display:block": " display:none"%>'>
                                    <div class="evn-cop-buytotle">$<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.ItemPrice.ToString("F0")%></div>
                                    <a <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Value ? string.Empty : "href=\"../deal/" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid + (string.IsNullOrEmpty(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa) ? string.Empty : ("?rsrc=" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa)) + "\" target='_blank'"%>>
                                        <div class="e_rounded e_shadow<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Value ? " st-e-cop-disablebtn" : " st-e-cop-buybtn"%>"><%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Value ? "已售完" : PponFacade.GetDealItemBtnText(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid) %></div>
                                    </a>
                                </div>
                                <div class="evn-cop-buyinformation" style='<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS > DateTime.Now ? " display:block": " display:none"%>'>
                                    <div class="PPDComeSoonNote" style="line-height: 38px"><%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS.ToString("MM/dd")%>&nbsp;開賣敬請期待！</div>
                                </div>
                        </a>
                    </asp:Panel>
                    <asp:Panel ID="pan_DisableLink" runat="server" CssClass="evn-cop-buy-sf-box e_shadow" Visible="<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS>DateTime.Now %>">
                            <div class="evn-cop-buy-pic">
                                <img src="<%# ImageFacade.GetMediaPathsFromRawData(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.ImagePath, MediaType.PponDealPhoto).DefaultIfEmpty("../Themes/default/images/17Life/G2/PPImage/PPDPic.jpg").First()%>" width="290" height="162" />
                                <div class="activeLogo alo-m"><asp:Literal ID="lit_DealPromoImageDisableLink" runat="server"></asp:Literal></div>
                                <div class="evn-cop-buy-sold-out-bar-290" style='<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS <= DateTime.Now ? " display:block": " display:none"%>'></div>
                            </div>
                            <div class="evn-cop-buytitle">
                                <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Title%>
                            </div>
                            <div class="evn-cop-minorange-title">
                                <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Description%>
                            </div>
                            <div class="evn-cop-buyinformation" style='<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS <= DateTime.Now ? " display:block": " display:none"%>'>
                                <div class="evn-cop-buytotle">$<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.ItemPrice.ToString("F0")%></div>
                                <div class="st-e-cop-buybtn e_rounded e_shadow">馬上買</div>
                            </div>
                            <div class="evn-cop-buyinformation" style='<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS > DateTime.Now ? " display:block": " display:none"%>'>
                                <div class="PPDComeSoonNote" style="line-height: 38px"><%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS.ToString("MM/dd")%>&nbsp;開賣敬請期待！</div>
                            </div>
                    </asp:Panel>
                    <%# IsEnablePager ? (Items.Count > PageSize ? ((Items.Count == (Container.ItemIndex + 1) && Items.Count % PageSize == 0) ? string.Empty : ((Container.ItemIndex + 1) % PageSize == 0 ? "</li><li>" : string.Empty)) : string.Empty) : string.Empty %>
                </ItemTemplate>
            </asp:Repeater>
            <asp:Literal runat="server" ID="footerData"></asp:Literal>
        </ItemTemplate>
    </asp:Repeater>
    <%= IsEnablePager && Items.Count > PageSize ? "</li></ul></div>" : string.Empty %>
    <div class="clear">
    </div>
</div>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="DiscountUserExchange.aspx.cs" Inherits="LunchKingSite.Web.Event.DiscountUserExchange" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-L.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-M.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-S.css")%>' rel="stylesheet" type="text/css" />
    <style type="text/css">
        .slide-m-browser {
            display: none;
        }
        .slide-bn-block {
            display: none;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtEventCode').keyup(function () {
                $(this).val($(this).val().toUpperCase());
            });
        });

        function checkCode() {
            if ($.trim($('#txtEventCode').val()) == '' || $.trim($('#txtCode').val()) == '') {
                $('#message').html('請輸入活動代碼');
                $('#divMessage').css('display', 'block');
                $('#divEventCode').addClass('error');
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <div id="FULL" class="rd-payment-Left-width">
        <div class="mc-content clearfix">
            <div class="content-group payment content-group-MoneyTicket-web">
                <h1 class="rd-payment-h1">兌換折價券</h1>
                <hr class="header_hr  rd-payment-width">
                <div class="grui-form message_box_wrapper discount_user_exchange_wrapper">
                    <asp:Panel ID="divEventCode" runat="server" ClientIDMode="Static" CssClass="form-unit" DefaultButton="btnSend">
                        <label for="" class="unit-label">請輸入活動代碼</label>
                        <div class="data-input discount_user_exchange">
                            <asp:TextBox ID="txtEventCode" runat="server" MaxLength="6" CssClass="input-2column-long" placeholder="輸入英數6碼" ClientIDMode="Static" onkeyup="next(this,'txtCode')"></asp:TextBox>
                            <span style="color: #999;">-</span>
                            <asp:TextBox ID="txtCode" runat="server" MaxLength="4" CssClass="input-2column-short" placeholder="輸入數字4碼" ClientIDMode="Static"></asp:TextBox>
                            <p class="cancel-mr">
                                <asp:Button ID="btnSend" runat="server" Text="送出" CssClass="btn btn-small btn-primary rd-payment-large-btn" OnClick="btnSend_Click" OnClientClick="return checkCode();" />
                            </p>
                        </div>
                        <asp:Panel ID="divMessage" runat="server" ClientIDMode="Static" CssClass="data-input enter-error" style="display: none;">
                            <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20" alt=""><p id="message">
                                <asp:Literal ID="liMessage" ClientIDMode="Static" runat="server"></asp:Literal>
                            </p>
                        </asp:Panel>
                    </asp:Panel>
                    <div class="discount_user_exchange_note">
                        <p class="important">兌換方式</p>
                        <ul>
                            <li>1.登入會員，輸入活動代碼：英數6碼＋數字4碼，共10碼，如ABC3DE-1234。</li>
                            <li>2.點選「送出」後，若您符合活動條件，會立刻匯入您的帳戶，並於會員中心&gt;折價券 列表中顯示。</li>
                            <li>3.使用方式：結帳時，若您的會員帳戶中有符合條件可使用的折價券，可點選「選擇折價券」使用。</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

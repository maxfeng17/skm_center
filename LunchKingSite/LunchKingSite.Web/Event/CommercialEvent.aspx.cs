﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.Event
{
    public partial class CommercialEvent : BasePage, ICommercialEventView
    {
        #region property
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private CommercialEventPresenter _presenter;
        public CommercialEventPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public string EventCode
        {
            get
            {
                if (Request.QueryString["eventcode"] != null)
                {
                    return Request.QueryString["eventcode"].ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        public int CommercialCode
        {
            get
            {
                int code = 0;
                if (Request.QueryString["commcode"] != null)
                    code = int.TryParse(Request.QueryString["commcode"].ToString(), out code) ? code : 0;
                return code;
            }
        }
        public string CommercialName
        {
            get
            {
                return hidCommercialName.Value;
            }
            set
            {
                hidCommercialName.Value = value;
            }
        }

        public string SiteUrl
        {
            get { return config.SiteUrl; }
        }


        #endregion
        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                this.Presenter.OnViewInitialized();
            this.Presenter.OnViewLoaded();
        }
        protected void rptCommercial_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<KeyValuePair<int, EventPromo>, Dictionary<EventPromoItem, ViewPponDeal>>)
            {
                KeyValuePair<KeyValuePair<int, EventPromo>, Dictionary<EventPromoItem, ViewPponDeal>> dataItem = (KeyValuePair<KeyValuePair<int, EventPromo>, Dictionary<EventPromoItem, ViewPponDeal>>)e.Item.DataItem;
                Literal liTitle = (Literal)e.Item.FindControl("liTitle");
                liTitle.Text = dataItem.Key.Value.Title.Replace(CommercialName + "-", string.Empty);
                Repeater rptEventPromoItem = (Repeater)e.Item.FindControl("rptEventPromoItem");
                rptEventPromoItem.DataSource = dataItem.Value;
                rptEventPromoItem.DataBind();
            }
        }
        #endregion

        #region method
        public void SetCategory(IEnumerable<Category> list)
        {
            rptCategory.DataSource = list;
            rptCategory.DataBind();
        }
        public void SetEventPromo(Dictionary<KeyValuePair<int, EventPromo>, Dictionary<EventPromoItem, IViewPponDeal>> list)
        {
            rptCommercial.DataSource = list;
            rptCommercial.DataBind();
        }
        public void ShowEventExpire()
        {
            divNoEvent.Visible = true;
            divMain.Visible = false;
        }
        #endregion
    }
}
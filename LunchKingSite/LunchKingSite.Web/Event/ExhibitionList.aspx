﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="ExhibitionList.aspx.cs" Inherits="LunchKingSite.Web.Event.ExhibitionList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
    <meta property="fb:app_id" content="<%=FB_AppId%>">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<%=OgTitle%>" />
    <meta property="og:url" content="<%=OgUrl%>" />
    <meta property="og:site_name" content="17Life" />
    <meta property="og:description" content="<%=OgDescription%>" />
    <meta property="og:image" content="<%=OgImage%>" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
    <link rel="image_src" href="<%=LinkImageSrc%>" />
    <link rel="canonical" href="<%=LinkCanonicalUrl%>" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SSC" runat="server">
<link href="../Themes/PCweb/css/MasterPage.css" rel="stylesheet" type="text/css" />
<link href="../Themes/PCweb/css/ppon.css" rel="stylesheet" type="text/css" />
<%=LunchKingSite.Core.Helper.RenderCss("/Themes/PCweb/css/ppon_item.min.css") %>
<link href="../themes/PCweb/css/Rightside.css" rel="stylesheet" type="text/css" />
<link href="../Themes/PCweb/plugin/font-awesome/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="../Themes/PCweb/plugin/swiper/swiper.css" rel="stylesheet" type="text/css" />
<link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
<link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
<link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../Tools/js/jquery.nav.js"></script>
    <style>
        /*行銷設計編輯用 配合主視覺banner調整*/
        body #wrap{
            background:center 91px no-repeat url('<%= MainSpacerImage %>'); /*大背景*/
        }
        .main_bn_wrap{
            background: url('<%= MainBackgroundImage %>')center no-repeat;/*主視覺banner背景(寬1920)*/
        }
        .exhibition_list_wrap .center_middle .horizontal_line {
            background: url('<%= WebCategoryImage %>'); /*分類底圖*/
            background-repeat:no-repeat; /*不要動*/
            background-size:contain;     /*不要動*/
            background-position:center;  /*不要動*/
        }
      @media only screen and (min-device-width : 768px) and (max-device-width : 1024px)  {
           .function-menu-bar{
               width: auto;
               display:block;
           }
           .function-menu-bar .funtion-menu-block{
               width:100%;
           }
           .function-menu-bar .right-menu{
               margin-right: 0px;
           }
           .function-menu-bar .mc{
               display: block;
           }
           .header_wrap{
               display: none !important;
           }
           .mbe-menu #navi{
               width: 100%;
               display: block;
               margin: 20px 0 25px 0;
           }
           .mbe-menu #navimain{
               width: 100%;
               height: 30px;
           }
           #navimain .logo-inline{
               width: 15%;
               height: 60px;
               margin-top: -15px;
               margin-right: 10px;
               display:block;
           }
           #navimain .navbtn{
               display:block;
               font-size:12px;
           }
           #navimain li{
               width: 8%;
               height: 30px;
           }
           #navimain li img{
               display: none;
           }
           #navimain li .fr{
               /*width: 135px !important;*/
           }
           li .hot_icon, li .new_icon{
               margin-left:20px;
               margin-top: -12px;
           }
           .fr .search-block{
               display: block;
               margin-top: 2px;
               margin-left: -76px;
           }
           .search-block .search-bar{
               width: 88px;
               height: 22px;
           }
           .search-block .search-button{
               width: 38px;
               height: 26px;
           }
           .ch-itemdeli, .ch-beauty{
               display: block !important;
           }
           .bn_day_wrap{
               width: auto;
           }
           .bn_day_wrap .center_middle{
               width: 100%;
           }
           .bn_day_wrap .clearfix{
               width: auto;
           }
           .zitem{
               width:32% !important;
               margin: 0px 5px 10px 5px !important;
           }
           .p3footer .contentblock_300{
                width: 31.38%;
                padding-top: 20px;
                padding-left: 8px;
           }
           .p3footer .contentblock_180{
                width: 16.54%;
                padding-top: 20px;
                padding-left: 8px;
           }
           .p3footer .footer_logo{
                width: 72px;
                height: 48px;
                margin: 0px;
           }
           .p3footer .footer_logo image{
               width: 72px;
               height: 48px;
           }
           .p3footer p {
               font-size: 12px;
               text-align: left;
           }
           .p3footer .support_c{
               display:none;
           }
           .p3footer .part-17life, .p3footer .part-pez-cert, .p3footer .part-serivce{
               display: block;
           }
           .TP_17life, .TP_PEZ, .TP_Certification, .TP_Service, .TP_twService{
               width:120px;
               height:24px;
           }
           .p3footer .twService{
               height: 48px;
               width: 90%;
           }
           .p3footer .twService .twService-img{
               width: 55px;
               height: 48px;
               margin-right: 0px;
           }
           .p3footer .twService image{
               width: 43px;
               height: 40px;
           }
           .center{
               width: 100%;
               height: 0px;
               margin: 0px;
           }
           .nav-anchor{
               margin-left: 0px;
               right: 22px; 
           }
      }
    </style>
    
    <style type="text/css">
        .PPClose {
            width: 100%;
            height: 350px;
            background: url(../Themes/PCweb/images/CloseBG.png) no-repeat;
            display: block;
        }

        .PPComingSoon {
            width: 100%;
            height: 350px;
            background: url(../Themes/PCweb/images/CommingSoonBG.jpg) no-repeat;
            display: block;
        }

        .PPSpace {
            width: 100%;
            height: 187px;
            display: block;
        }

        .BackBtn {
            width: 286px;
            height: 76px;
            background: url(../Themes/PCweb/images/CloseBtn.png);
            margin-left: 445px;
            display: block;
        }
    </style>

    <script type='text/javascript'>

		$(document).ready(function () {
			//移除master page 有影響到的CSS
			$("#divCenter").removeAttr("class");
			$("#btnBackTop").remove();

			$("img.lazy").lazyload({ placeholder: ServerLocation + "Themes/PCweb/images/ppon-M1_pic.jpg", threshold: 200, effect: "fadeIn" });
		});
    </script>
    
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MC" runat="server">
<asp:Panel ID="pan_main" runat="server" Visible="false">
    <asp:Literal ID="litChangeCss" runat="server"></asp:Literal>
    <!--// 策展列表頁面 //-->
    <div class="exhibition_list_wrap">
        <!--// 策展列表主視覺bn //-->
        <div class="main_bn_wrap clearfix">
            <div class="main_bn">

                <!-- // fb // -->
                <div class="fb">
                    <div class="LoveBlindFBShare">
                        <div class="LoveBlindShare">
                            <a href="<%= Share_FB_Url %>" target="_blank" class="LoveBlindShare">
                                <img class="lazy" height="22" src="https://www.17life.com/images/17P/Promo/Love20131014/FB_Btn.png" width="147" />
                            </a>
                        </div>
                        <div class="LoveBlindFB" style="float: right; margin-right: 10px;">
                            <fb:like href="http://www.facebook.com/17life.com.tw" layout="button_count" show_faces="false"></fb:like>
                        </div>
                    </div>
                </div>
                <!-- // fb //end -->

                <a href="ExhibitionListRule.aspx?id=<%=ThemeMainId %>" target="_blank" class="notice">
                    <asp:Image CssClass="lazy" ID="imgMain" runat="server" />
                </a>
            </div>
        </div>
        <!--// 策展列表主視覺bn //end-->

        <!--//  今日bn區域 //-->
        <div id="list_00" class="bn_day_wrap bn_day_wrap_today">
            <div class="center_middle" id="divTodayHot" runat="server">
                <p class="horizontal_line">
                    <span class="font_big"><i class="fa fa-bullhorn fa-fw"></i><asp:Literal ID="litHotBannerTitle" runat="server" /></span>
                </p>
            </div>
            <ul class="clearfix">
                <asp:Repeater ID="rpTodayBanner" runat="server" OnItemDataBound="rpTodayBanner_ItemDataBound">
                    <ItemTemplate>
                        <li class="zitem">
                            <div class="img-zoom">
                                <div id="divBlock" runat="server" class="item_block">
                                    <img src="https://www.17life.com/images/17P/active/20161201_exhibition/item_block.png" />
                                </div>
                                <a ID="linkToday" runat="server"><asp:Image CssClass="lazy" ID="imgToday" runat="server" /></a>
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <!--//  今日輪播區域 //end-->

        <asp:Repeater ID="rpBannerList" runat="server" OnItemDataBound="rpBannerList_ItemDataBound">
            <ItemTemplate>
                <!--//  Day //-->
                <asp:Literal ID="litHeader" runat="server" />
                    <div class="center_middle">
                        <p class="horizontal_line">
                            <span class="font_big"><asp:Literal ID="litBannerTitle" runat="server" /></span>
                        </p>
                    </div>
                    <ul class="clearfix">
                        <asp:Repeater ID="rpSubList" runat="server" OnItemDataBound="rpSubList_ItemDataBound">
                            <ItemTemplate>
                                <li class="zitem">
                                    <div class="img-zoom">
                                        <div id="divBlock" runat="server" class="item_block">
                                            <img src="https://www.17life.com/images/17P/active/20161201_exhibition/item_block.png" />
                                        </div>
                                        <a id="linkBanner" runat="server"><asp:Image CssClass="lazy" ID="imgBanner" runat="server" /></a>
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                <asp:Literal ID="litFooter" runat="server" />
                <!--//  Day //end-->
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <!--// 策展列表頁面 //end-->
</asp:Panel>

<asp:Panel ID="pan_sub" runat="server" Visible="false" CssClass="center">
    <div class="PPClose" id="divClose" runat="server">
        <div class="PPSpace">
        </div>
        <a href="/" class="BackBtn"></a>
    </div>
    <div class="PPComingSoon" id="divComming" runat="server" style="display:none">
        <div class="PPSpace">
        </div>
        <a href="/" class="BackBtn"></a>
    </div>
</asp:Panel>
<!--//center//-->
<div class="center" id="divNav">
  <ul id="subMenu" class="nav-anchor">
    <li class="current" id="liNavToadayHot" runat="server"><a href="#list_00"><asp:Literal ID="litNavToadayHot" runat="server" /></a></li>
    <asp:Repeater ID="rpNav" runat="server" OnItemDataBound="rpNav_ItemDataBound">
        <ItemTemplate>
            <li> <a id="linkNav" runat="server" /></li>
        </ItemTemplate>
    </asp:Repeater>
  </ul>
</div>
<!--//center//end-->
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="LastLoadJs" runat="server">

<script>
	$(document).ready(function () {
		//<!-- zoomin效果 -->
		$('.img-zoom').hover(function () {
			$(this).addClass('transition');
		}, function () {
			$(this).removeClass('transition');
		});
		//<!-- zoomin效果 end -->

		$('.nav-anchor').onePageNav({
			currentClass: 'current',
			changeHash: false,
			scrollSpeed: 750,
			scrollThreshold: 0.5,
			filter: ':not(.external)',
			easing: 'swing',
			begin: function () {
				//I get fired when the animation is starting
			},
			end: function () {
				//I get fired when the animation is ending
			},
			scrollChange: function ($currentListItem) {
				//I get fired when you enter a section and I pass the list item of the section
			}
		});

		if ($('#subMenu').find('li').size() < 2) {
			$('#divNav').hide();
		}
	});
</script>
</asp:Content>
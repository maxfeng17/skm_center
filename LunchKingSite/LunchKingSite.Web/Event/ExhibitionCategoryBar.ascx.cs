﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.Event
{
    public partial class ExhibitionCategoryBar : System.Web.UI.UserControl
    {
        public List<ViewEventPromo> Item { get; set; }
        public EventPromo EventPromoData { get; set; }

        public IEnumerable<IGrouping<string, ViewEventPromo>> GroupCategory { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public override void DataBind()
        {
            GroupCategory = Item.GroupBy(x => string.IsNullOrEmpty(x.Category) ? string.Empty : x.Category).OrderBy(x => x.Key);

            if (GroupCategory.Count() == 1 && string.IsNullOrEmpty(GroupCategory.First().Key))
            {
                pan_Category.Visible = false;
                pan_categoryBarCSS.Visible = false;
            }
            else
            {
                rpt_Category.DataSource = GroupCategory.Select(x => string.IsNullOrEmpty(x.Key) ? string.Empty : x.Key);
                rpt_Category.DataBind();

                string cssData = string.Empty;
                string btnFontOriginalCSS = string.Empty;
                string btnFontHoverCSS = string.Empty;
                string btnFonActiveCSS = string.Empty;
                //處理主分類按鈕
                if (!string.IsNullOrWhiteSpace(EventPromoData.BtnFontColor))
                {
                    btnFontOriginalCSS = string.Format(".st-e-cop-area-bar a:link{{color:#{0}}} .st-e-cop-area-bar a:visited{{color:#{0}}} ", EventPromoData.BtnFontColor);
                }
                if (!string.IsNullOrWhiteSpace(EventPromoData.BtnFontColorHover))
                {
                    btnFontHoverCSS = string.Format(".st-e-cop-area-bar a:Hover{{color:#{0}}} ", EventPromoData.BtnFontColorHover);
                }
                if (!string.IsNullOrWhiteSpace(EventPromoData.BtnFontColorActive))
                {
                    btnFonActiveCSS = string.Format(".st-e-cop-area-bar a:Active{{color:#{0}}} ", EventPromoData.BtnFontColorActive);
                }


                cssData = string.Format("{0}{1}{2}", btnFontOriginalCSS, btnFontHoverCSS ,btnFonActiveCSS);
                if (!string.IsNullOrWhiteSpace(cssData))
                {
                    cssData = string.Format("<style type='text/css'>{0}</style>", cssData);
                }

                categoryBarCSS.Text = cssData;
            }
        }


        protected string RegexReplaceSequence(string category)
        {
            return Regex.Replace(category, @"^[0-9]*\.", string.Empty);
        }

    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="ExhibitionListMobile.aspx.cs" Inherits="LunchKingSite.Web.Event.ExhibitionListMobile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
    <meta property="fb:app_id" content="<%=FB_AppId%>">
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://www.17life.com/Event/exhibitionlistmobile.aspx?id=<%= ThemeMainId %>" />
    <meta property="og:title" content="<%=OgTitle%>" />
    <meta property="og:site_name" content="17Life" />
    <meta property="og:description" content="<%=OgDescription%>" />
    <meta property="og:image" content="<%=OgImage%>" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
    <link rel="canonical" href="https://www.17life.com/Event/exhibitionlistmobile.aspx?id=<%= ThemeMainId %>" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SSC" runat="server">
    <script type='text/javascript'>
        
        $(document).ready(function () {
            //調整masterpage造成的影響
            $(".p3footer").addClass('clearfix');
            $("#target").hide();
        });

        //分享
        $(function () {
            var flag_share = 0; //未點擊 
            $(".main_bn a.coupon_share_on").click(function () {
                if (flag_share == 0) {
                    $(this).find(".fa-share-alt").css("display", "none");
                    $(this).find(".fa-close").css("display", "inline-block");
                    $('a.coupon_share_line').css("display", "block");
                    $('a.coupon_share_fb').css("display", "block");
                    $('a.coupon_share_line').animate({
                        "bottom": "25px",
                        "right": "70px"
                    }, 200, "easeOutExpo");
                    $('a.coupon_share_fb').animate({
                        "bottom": "70px",
                        "right": "25px"
                    }, 200, "easeOutExpo");
                    flag_share = 1;
                } else {
                    $(this).find(".fa-share-alt").css("display", "inline-block");
                    $(this).find(".fa-close ").css("display", "none");
                    $('a.coupon_share_line').css("display", "none");
                    $('a.coupon_share_fb').css("display", "none");
                    $('a.coupon_share_line').animate({
                        "bottom": "10px",
                        "right": "10px"
                    }, 200, "easeOutExpo");
                    $('a.coupon_share_fb').animate({
                        "bottom": "10px",
                        "right": "10px"
                    }, 200, "easeOutExpo");
                    flag_share = 0;
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MC" runat="server">
    <asp:PlaceHolder ID="pan_EventList" runat="server"></asp:PlaceHolder>
</asp:Content>

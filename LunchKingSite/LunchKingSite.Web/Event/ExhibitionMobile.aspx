﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="ExhibitionMobile.aspx.cs" Inherits="LunchKingSite.Web.Event.ExhibitionMobile" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Src="~/UserControls/RandomParagraph.ascx" TagName="RandomParagraph"
    TagPrefix="ucR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
    <meta property="fb:app_id" content="<%=FB_AppId%>">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<%=OgTitle%>" />
    <meta property="og:url" content="<%=OgUrl%>" />
    <meta property="og:site_name" content="17Life" />
    <meta property="og:description" content="<%=OgDescription%>" />
    <meta property="og:image" content="<%=OgImage%>" />
    <link rel="image_src" href="<%=LinkImageSrc%>" />
    <link rel="canonical" href="<%=LinkCanonicalUrl%>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <%if (TemplateType == EventPromoTemplateType.OnlyNewPiinlife)
      { %>
    <link href="../Themes/PCweb/HighDeal/MasterPage_HD.css" rel="stylesheet" type="text/css">
    <!--for PiinLife-->
    <link href="../Themes/PCweb/HighDeal/ppon_HD.css" rel="stylesheet" type="text/css" />
    <!--for PiinLife-->
    <%} %>
    <link type="text/css" href="../Themes/mobile/css/swiper.css" rel="stylesheet" />
    <link href="../Themes/default/images/17Life/G2/A3-event_coupons.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    <%if (TemplateType == EventPromoTemplateType.OnlyNewPiinlife)
      { %>
    <link href="../Themes/PCweb/HighDeal/RDL_HD.css" rel="stylesheet" type="text/css" />
    <!--for PiinLife-->
    <%} %>
    <script src="../Tools/js/jquery.mousewheel.min.js" type="text/javascript"></script>
    <script src="../Tools/js/jquery.kinetic.min.js" type="text/javascript"></script>
    <script src="../Tools/js/jquery.smoothdivscroll-1.3-min.js" type="text/javascript"></script>
    <script src="../Themes/mobile/js/swiper.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var ServerLocation = "/";
        if (window.document.location.pathname.toLowerCase().indexOf('lksite/') > 0) {
            ServerLocation = "/lksite/";
        }
        if (window.document.location.pathname.toLowerCase().indexOf('releasebranch/') > 0) {
            ServerLocation = "/releasebranch/";
        }
        function resizefun() { 
            var x = $(window).width();
            if(x > 1000) {
                var typeValue = $("#hidTemplateType").val();
                var myUrl = window.location.toString().toLowerCase();

                if(typeValue == "<%=(int)EventPromoTemplateType.PponOnly%>") {
                    location.href = myUrl.replace("exhibitionmobile.aspx", "exhibition.aspx");
                } else if(typeValue == "<%=(int)EventPromoTemplateType.JoinPiinlife%>"|| typeValue == "<%=(int)EventPromoTemplateType.NewJoinPiinlife%>") {
                    location.href = myUrl.replace("exhibitionmobile.aspx", "exhibitionjoinpiinlife.aspx");
                } else if(typeValue == "<%=(int)EventPromoTemplateType.Commercial%>") {
                    location.href = myUrl.replace("exhibitionmobile.aspx", "exhibitionCommercial.aspx");
                } else if(typeValue == "<%=(int)EventPromoTemplateType.Kind%>") {
                    location.href = myUrl.replace("exhibitionmobile.aspx", "exhibitionkind.aspx");
                } else if(typeValue == "<%=(int)EventPromoTemplateType.Zero%>") {
                    location.href = myUrl.replace("exhibitionmobile.aspx", "exhibitionzero.aspx");
                }else if(typeValue == "<%=(int)EventPromoTemplateType.OnlyNewPiinlife%>") {
                    location.href = myUrl.replace("exhibitionmobile.aspx", "exhibitionnewpiinlife.aspx");
                }
}
}
var delayFunction = function (mainFunc, option) {
    var delayTimer;
    option = option || {};
    if (!option.delay) {
        return mainFunc;
    } else {
        return function () {
            var that = this;
            var args = arguments;
            if (delayTimer) clearTimeout(delayTimer);
            delayTimer = setTimeout(function () {
                mainFunc.apply(that, args);
            }, option.delay);
        };
    }
};

$(document).ready(function () {
    $(window).resize(delayFunction(function () { resizefun(); }, { delay: 300 }));
    if ($("#hidEventAlive").val() == "False") {
        $("#eventPromoContent").hide();
        $("#eventPromoEnd").show();
    }
    else {
        $("#btnBackTop").css("right", "10px");
        $("#btnBackTop").css("bottom", "25px");
        $("#btnBackTop").css("width", "50px");
        $("#btnBackTop").css("height", "50px");
        var categoryBtn = $("#categoryBtn");
        var conditionsBtn = $("#conditionsBtn");
        var templateType = $("#hidTemplateType").val();
        var categoryCount = $("#hidCategoryCount").val();
        if ($.trim($('.rdl-ru-content').html()) == '') {
            conditionsBtn.hide();
        }
        if (!(templateType == "<%=(int)EventPromoTemplateType.PponOnly%>" && categoryCount == "1")) {
            categoryBtn.click(function () {
                conditionsBtn.removeClass("rdl-on");
                categoryBtn.addClass("rdl-on");
                if (templateType == "<%=(int)EventPromoTemplateType.OnlyNewPiinlife%>") {
                    conditionsBtn.removeClass("rdl-on-piin");
                    categoryBtn.addClass("rdl-on-piin");
                }
                $("#dealContent").show();
                if($("#btnName").html() != "品生活" && $("#btnName").html() != "優惠券" && $("#subCategoryContent").find("li").length > 1) {
                    $("#subCategoryContent").show();
                }
                        
                if (!$("#conditions").is(":visible"))
                {
                    menuToggle(this, "#categoryList");
                }
                $("#conditions").hide();
            });
        } else {
            $("#listArrow").hide();
            categoryBtn.click(function () {
                conditionsBtn.removeClass("rdl-on");
                categoryBtn.addClass("rdl-on");
                if (templateType == "<%=(int)EventPromoTemplateType.OnlyNewPiinlife%>") {
                    conditionsBtn.removeClass("rdl-on-piin");
                    categoryBtn.addClass("rdl-on-piin");
                }
                if ($.parseJSON($("#hidSubCategory").val()).length > 1) {
                    if($("#btnName").html() != "品生活" && $("#btnName").html() != "優惠券" && $("#subCategoryContent").find("li").length > 1) {
                        $("#subCategoryContent").show();
                    }
                }
                $("#dealContent").show();
                $("#conditions").hide();
            });
        }
                
        conditionsBtn.click(function () {
            conditionsBtn.addClass("rdl-on");
            categoryBtn.removeClass("rdl-on");
            if (templateType == "<%=(int)EventPromoTemplateType.OnlyNewPiinlife%>") {
                        conditionsBtn.removeClass("rdl-on-piin");
                        categoryBtn.addClass("rdl-on-piin");
                    }
                    if ($("#categoryList").is(":visible")) {
                        menuToggle(categoryBtn, "#categoryList");
                    }
                    $("#conditions").show();
                    $("#dealContent").hide();
                    $("#subCategoryContent").hide();
                    moveCategoryTop();
                });
                $("img.lazy").lazyload({ placeholder: ServerLocation + "Themes/PCweb/images/ppon-M1_pic.jpg", threshold: 200, effect: "fadeIn" });
                var dealPicLength = $("img.lazy").length;
                dealPicLength = (dealPicLength > 3) ? 3 : dealPicLength;
                for (var i = 0; i < dealPicLength; i++) {
                    $($("img.lazy")[i]).attr("src", $($("img.lazy")[i]).attr("data-original"));
                    $($("img.lazy")[i]).removeAttr("data-original");
                }

                $(".pponDeal").hide();
                $(".pponCategory0").show();
                showDefaultCategory(categoryCount);
                $($("#subCategoryContent").find("li")[0]).addClass("tab-on");

                if((categoryCount == 1 && $.parseJSON($("#hidSubCategory").val()).length <=1) || templateType == "<%=(int)EventPromoTemplateType.PponOnly%>") {
                    $($("#mainCategoryBox").find("li")[0]).addClass("subbox-on");
                } else {
                    <% if (TemplateType != EventPromoTemplateType.OnlyNewPiinlife)
                       { %>
                    $($("#mainCategoryBox").find("li")[1]).addClass("subbox-on");
                    $("#hidCurrentCategoryName").val($($("#mainCategoryBox").find("li")[1]).html());
                    $("#btnName").html($($("#mainCategoryBox").find("li")[1]).html());
                    <% }
                       else
                       {%>
                    $($("#mainCategoryBox").find("li")[0]).addClass("subbox-on");
                    $("#hidCurrentCategoryName").val($($("#mainCategoryBox").find("li")[0]).html());
                    $("#btnName").html($($("#mainCategoryBox").find("li")[0]).html());
                    <% } %>
                }
                
        $(".scroll-tab").smoothDivScroll({
            hotSpotScrolling: false,
            touchScrolling: true,
        });
        bindSubCategoryTouchEvent();
    }

    // 策展
    if ($('.activity-banner a').length > 0) {

        $('.activity-banner').show();
        $('.swiper-wrapper li').each(function(){
            $(this).replaceWith($("<div class='swiper-slide' style='width: auto; height: auto; line-height: 20px;'>" + this.innerHTML + "</div>"));
        });

        var swiper = new Swiper('.swiper-container', {
            slidesPerView: 'auto',
            paginationClickable: true,
        });

        $('.activity-banner').find('a').each(function () {
            if ($(this).attr('href').indexOf('<%= Url%>') > 0) {
                $(this).parent().addClass('on');
            }
            var cityId = $('#HfCityId').val();
            var url = $(this).attr('href') + '&cid=' + cityId;
            $(this).attr('href', url.replace('Exhibition.aspx', 'ExhibitionMobile.aspx'));
        });
    }else {
        $('.activity-banner').hide();
    }

    $('.scrollableArea:last ').css('display', 'block');
    $('.scrollableArea:last ').css('width', '500px');
});

function bindSubCategoryTouchEvent() {
    var touchTime;
    $("div.scrollableArea").find("div").each(function() {
        if (typeof window.ontouchstart !== 'undefined') {
            $(this).bind('touchstart', function () {
                var d = new Date();
                touchTime = d.getTime();
            });
            $(this).bind('touchend', function () {
                var t = new Date();
                if ((t.getTime() - touchTime) < 300) {
                    $(this).click();   
                }
            });
        }
    });
}

function changeCategory(obj, subCategoryId, categoryName) {
    var piinlifeContent = $("#piinlifeContent");
    if (piinlifeContent.find("a[class*=pponCategory]").length > 0) {
        piinlifeContent.show();
        piinlifeContent.find("div[class*=pponCategory]").hide();
        piinlifeContent.find("div.pponCategory"+ subCategoryId).show();
                
                <% if (TemplateType != EventPromoTemplateType.OnlyNewPiinlife)
                   { %>
        var mainCategoryBox = $("#mainCategoryBox");
        $("#divpiinlifeTitle").show();
        $("#mainCategoryBox li").removeClass("inside");
        var subCategoryContent = $("#subCategoryContent div div li div").first();
        if (subCategoryContent.first().html() == "全部") {
            if (subCategoryContent.is(":visible")) {
                subCategoryId++;
            }
            subCategoryContent.first().hide();
                    
        }
                <% } %>
    } else {
        $("#piinlifeContent").hide();
        $("#divpiinlifeTitle").hide();
    }
            
    $("#couponContent").hide();
    $("#pponDealContent").show();
    $(".pponDeal").hide();
    $(".pponCategory" + subCategoryId).show();
    if ($("#categoryList").is(":visible")) {
        menuToggle($("#categoryBtn"), "#categoryList");
    }
    if(obj != null){
        $(obj).parent().parent().find("li").each(function(){
            $(this).removeClass("tab-on");
        });
        $($(obj).parent()).addClass("tab-on");
    }
    $("#hidCurrentCategoryName").val(categoryName);
    $("#hidCurrentSubCategoryId").val(subCategoryId);
    if(categoryName)
    {
        var regex = /^[0-9]*\./;
        $("#btnName").html(categoryName.replace(regex, ""));
    } else {
        $("#btnName").html("團購優惠");
    }
            
    if(obj != null) {
        moveCategoryTop();
    }
    $(window).trigger('scroll');

    if ($("#pponDealContent").find("div:visible").length == 1) {
        $("#17LifeTitle").hide();
    } else {
        $("#17LifeTitle").show();
    }
    if ($("#piinlifeContent").find("div:visible").length == 1) {
        $("#divpiinlifeTitle").hide();
    } else {
                <% if (TemplateType != EventPromoTemplateType.OnlyNewPiinlife)
                   {%>
        $("#divpiinlifeTitle").show();
                <%}%>
    }

}
        function menuToggle(toggleObj, showItem) {
            $(toggleObj).toggleClass("arrowOn");
            $(showItem).slideToggle("normal", moveCategoryTop);

            return false;
        }
        function moveCategoryTop() {
            setArrowImage($("#categoryBtn"));
            var menu = ($("#smartbanner").css("top") == "0px") ? ($("#categoryBtn").offset().top + 86) : $("#categoryBtn").offset().top;
            $('html, body').animate({
                scrollTop: menu
            }, 500);

            if(!$("#conditions").is(":visible")) {
                if (!$("#categoryList").is(":visible") && !$("#piinlifeContent").is(":visible") && !$("#couponContent").is(":visible")) {
                    //showSubCategory($("#hidCurrentCategoryName").val());
                    //$($("#subCategoryContent").find("#subCat" + $("#hidCurrentSubCategoryId").val())).addClass("tab-on");
                    $("#mainCategoryBox").find("li").each(function(){
                        if($(this).html().replace("全部", "") == $("#hidCurrentCategoryName").val()) {
                            $(this).addClass("subbox-on");
                        }
                        else {
                            $(this).removeClass("subbox-on");
                        }
                    });
                }
                if($("#couponContent").is(":visible"))
                {
                    $("#subCategoryContent").hide();
                }            
            }
        }
        function setArrowImage(checkObj) {
            if (checkObj.hasClass("arrowOn")) {
                checkObj.find("img").attr("src", "../Themes/default/images/17Life/G2/rdl-event-upArrow.png");
            } else {
                checkObj.find("img").attr("src", "../Themes/default/images/17Life/G2/rdl-event-DownArrow.png");
            }
        }
        function VourcherCollect(btn, id) {
            if ($(btn).val() == '已收藏') {
                return false;
            }
            $.ajax({
                type: "POST",
                url: "ExhibitionCommercial.aspx/SetVourcherCollect",
                data: '{"eventId":' + id + '}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function () {
                    $(btn).removeClass().addClass('st-skmcpcacol-favoritetoo-btn').val('已收藏');
                    alert("收藏成功！您可在有效期間內依使用規則享有此優惠。\n\n請先下載 17Life App，登入即可在「我的」→「優惠券收藏」內開啟此優惠券，於消費時出示即可使用");
                },
                error: function(msg) {
                    console.log(msg);
                    if (confirm("請先登入會員")) {
                        location.href = '<%=ResolveUrl(FormsAuthentication.LoginUrl) %>';
                    }
                }
            });
            return false;
        }
        
        function showDefaultCategory(categoryCount) {
            if(categoryCount > 1 || (categoryCount == 1 && $.parseJSON($("#hidSubCategory").val()).length > 0)) {
                var subCat = $.parseJSON($("#hidSubCategory").val());
                if(subCat.length > 0) {
                    showSubCategory(subCat[0].Category);
                }
            }
        }

        function showSubCategory(categoryName)
        {
            if(!categoryName) {
                $("#mainCategoryBox").find("li").each(function(){
                    if($(this).hasClass("subbox-on")) {
                        categoryName = $(this).html();
                        $("#hidCurrentCategoryName").val(categoryName);
                    }
                });
            }
            var sigleSubCatId = -1;
            var subCat = $.parseJSON($("#hidSubCategory").val());
            if ($(".scroll-tab").data() != null && $(".scroll-tab").data().smoothDivScroll != null) {
                $(".scroll-tab").smoothDivScroll("destroy");
            }
            $("#subCategoryContent").html("");
            var regex = /^[0-9]*\./;
            var subCategoryRegex = /^[0-9]*\.[0-9]*/;
            for(var i=0;i<subCat.length;i++) {
                if(subCat[i].Category.replace(regex, "") == categoryName.replace(regex, ""))
                {
                    var subCatName = subCat[i].SubCategory.replace(subCategoryRegex, "");
                    if(!subCatName)
                    {
                        subCatName = "全部";
                    }
                    $("#subCategoryContent").append("<li id='subCat" + subCat[i].index + "'><div style='line-height:50px' onclick=\"changeCategory(this, " + subCat[i].index + ", '" + subCat[i].Category.replace(regex, "") + "')\">" + subCatName + "</div></li>");
                    if(sigleSubCatId < 0) {
                        sigleSubCatId = subCat[i].index;
                    }
                }
            }
            if(sigleSubCatId == -1) sigleSubCatId = 0;
            if($("#subCategoryContent").find("li").length > 1) {
                $("#subCategoryContent").show();
                $(".scroll-tab").smoothDivScroll({
                    hotSpotScrolling: false,
                    touchScrolling: true,
                });
                bindSubCategoryTouchEvent();
                changeCategory($($("#subCategoryContent").find("li")[0]).find("a"), sigleSubCatId, categoryName);
            }
            else {
                $("#subCategoryContent").hide();
                changeCategory(null, sigleSubCatId, categoryName);
            }
        }

        function showPiinlife() {
            $("#pponDealContent").hide();
            $("#subCategoryContent").hide();
            $("#piinlifeContent").show();
            menuToggle($("#categoryBtn"), "#categoryList");
            $(window).trigger('scroll');
            $("#btnName").html("品生活");
        }
        function showCoupon() {
            $("#pponDealContent").hide();
            $("#subCategoryContent").hide();
            $("#couponContent").show();
            menuToggle($("#categoryBtn"), "#categoryList");
            $(window).trigger('scroll');
            $("#btnName").html("優惠券");
        }
        function setSubboxOn(obj)
        {
            $("#mainCategoryBox").find("li").each(function(){
                $(this).removeClass("subbox-on");
            });
            $(obj).addClass("subbox-on");
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <div class="ac-center">

        <%--策展 Start--%>
        <input id="HfCityId" type="hidden" value="<%= CityId%>" />
        <div class="activity-banner">
            <div class="swiper-container activity-site">
                <div class="swiper-wrapper">
                    <ucR:RandomParagraph ID="curation" OnInit="RandomPponMasterInit" runat="server" />
                </div>
            </div>
        </div>
        <%--策展 End--%>

        <div id="btnBackTop" class="HKL_Button_back_to_top_command"
            style="display: none; cursor: pointer" title="返回頂部">
        </div>
        <input id="hidEventAlive" type="hidden" value="<%=EventAlive%>" />
        <input id="hidTemplateType" type="hidden" value="<%=(int)TemplateType%>" />
        <input id="hidCategoryCount" type="hidden" value="<%=CategoryCount%>" />
        <input id="hidSubCategory" type="hidden" value='<%=SubCategoryJson%>' />
        <input id="hidCurrentCategoryName" type="hidden" value="" />
        <input id="hidCurrentSubCategoryId" type="hidden" value="0" />
        <div id="eventPromoContent">
            <div id="Act-smallTOP"><%=MainPic%></div>
            <div class="rdl-list-box clearfix">
                <div class="<%=(TemplateType==EventPromoTemplateType.OnlyNewPiinlife) ? "rdl-Btn-box-piin  clearfix" :"rdl-Btn-box clearfix"%>">
                    <div id="categoryBtn" class="<%=(TemplateType==EventPromoTemplateType.OnlyNewPiinlife) ?"rdl-list-Lbtn-piin rdl-on-piin":"rdl-list-Lbtn rdl-on" %>">
                        <span id="btnName">團購優惠</span>
                        <span id="listArrow" class="<%=(TemplateType==EventPromoTemplateType.OnlyNewPiinlife) ? "rdl-list-downarrow-piin" :"rdl-list-downarrow"%>">
                            <img src="../Themes/default/images/17Life/G2/rdl-event-DownArrow.png" />
                        </span>
                    </div>
                    <div id="conditionsBtn" class="<%=(TemplateType==EventPromoTemplateType.OnlyNewPiinlife) ? "rdl-list-Rbtn-piin" :"rdl-list-Rbtn"%>">活動辦法</div>
                </div>
                <div id="categoryList" class="<%=(TemplateType==EventPromoTemplateType.OnlyNewPiinlife) ? "rdl-Subbox-piin" :"rdl-Subbox"%>" style="display: none">
                    <ul id="mainCategoryBox">
                        <%if (TemplateType == EventPromoTemplateType.NewJoinPiinlife || TemplateType == EventPromoTemplateType.Commercial)
                          { %>
                        <%if (CategoryCount == 1)
                          { %>
                        <li onclick="changeCategory(this, 0, '團購優惠');setSubboxOn(this);">17Life</li>
                        <%}
                          else
                          {%>
                        <li>17Life</li>
                        <%}%>
                        <%}%>
                        <asp:Repeater ID="rptCategory" runat="server">
                            <ItemTemplate>
                                <li<%# (TemplateType == EventPromoTemplateType.PponOnly || TemplateType == EventPromoTemplateType.Kind || TemplateType == EventPromoTemplateType.Zero) ? string.Empty : " class='inside'" %> onclick="showSubCategory('<%# RegexReplaceSequence(Container.DataItem.ToString())%>');setSubboxOn(this);"><%# (string.IsNullOrEmpty(Container.DataItem.ToString()) ? "全部" : RegexReplaceSequence(Container.DataItem.ToString())) %>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                        <% if (TemplateType == EventPromoTemplateType.NewJoinPiinlife)
                           { %>
                        <li onclick="showPiinlife();setSubboxOn(this);">品生活</li>
                        <%}%>
                        <% if (TemplateType == EventPromoTemplateType.Commercial)
                           { %>
                        <li onclick="showCoupon(this);setSubboxOn(this);">優惠券</li>
                        <%} %>
                    </ul>
                </div>
            </div>
            <div id="subCategoryDiv" class="<%=(TemplateType==EventPromoTemplateType.OnlyNewPiinlife) ? "rdl-list-box-piin" :"rdl-list-box"%>">
                <ul id="subCategoryContent" class="scroll-tab" style="display: none">
                </ul>
            </div>
            <div class="<%=(TemplateType==EventPromoTemplateType.OnlyNewPiinlife) ? "rdl-list-box-piin" :"rdl-list-box"%>" id="conditions" style="display: none">
                <div class="rdl-evn-pprule e_shadow">
                    <div class="rdl-ru-title">活動辦法</div>
                    <div class="rdl-ru-content"><%=EventConditions%></div>
                </div>
            </div>
            <div id="dealContent" class="ly-e-cop-farme clearfix">
                <div id="pponDealContent">
                    <asp:Repeater ID="rptPponDeals" OnItemDataBound="rptPponDeals_ItemDataBound" runat="server" Visible="false">
                        <HeaderTemplate>
                            <div id="17LifeTitle" class="ly-e-cop-ppon rptTitle ppon">17Life</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="pponDeal evn-cop-buy-sf-box e_shadow<%#GetPponDealCategoryCssClassName(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Category, ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.SubCategory)%>">
                                <div class="evn-cop-buy-pic">
                                    <img class="lazy" data-original="<%# ImageFacade.GetMediaPathsFromRawData(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.ImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(SystemConfig.SiteUrl + "/Themes/PCweb/images/ppon-M1_pic.jpg").First()%>" alt="<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Title%>" width="290" height="162" />
                                    <div class="activeLogo alo-m">
                                        <asp:Literal ID="lit_DealPromoImage" runat="server"></asp:Literal>
                                    </div>
                                    <div class="evn-cop-buy-sold-out-bar-290" style="<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Value ? " display:block": " display:none" %>">
                                        <img src="../Themes/PCweb/images/soldout_bar_290.png" width="290" height="162" />
                                    </div>
                                </div>
                                <div class="evn-cop-buytitle e_textFamily">
                                    <a href="<%#"../deal/" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid + (string.IsNullOrEmpty(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa) ? string.Empty : ("?rsrc=" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa)) %>">
                                        <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Title%>
                                    </a>
                                </div>
                                <div class="evn-cop-minorange-title">
                                    <a href="<%#"../deal/" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid + (string.IsNullOrEmpty(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa) ? string.Empty : ("/rsrc=" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa)) %>">
                                        <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Description%>
                                    </a>
                                </div>
                                <div class="evn-cop-buyinformation" style='<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS <= DateTime.Now ? " display:block": " display:none"%>'>
                                    <div class="evn-cop-buytotle">$<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.ItemPrice.ToString("F0")%></div>
                                    <a <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Value ? string.Empty : "href=\"../deal/" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid + (string.IsNullOrEmpty(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa) ? string.Empty : ("?rsrc=" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa)) + "\""%>>
                                        <div class="e_rounded<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Value ? " st-e-cop-disablebtn" : " st-e-cop-buybtn"%>"><%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Value ? "已售完" : "馬上買" %></div>
                                    </a>
                                </div>
                                <div class="evn-cop-acNotice" style='<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS > DateTime.Now ? " display:block": " display:none"%>'><%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS.ToString("MM/dd")%>&nbsp; 活動即將開始</div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <asp:Repeater ID="rptKindDeals" OnItemDataBound="rptPponDeals_ItemDataBound" runat="server" Visible="false">
                    <HeaderTemplate>
                        <div class="ly-e-cop-ppon rptTitle ppon">17Life</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="pponDeal evn-cop-buy-sf-box e_shadow evn-cop-community-sf-box<%#GetPponDealCategoryCssClassName(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Category, ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.SubCategory)%>">
                            <div class="evn-cop-buy-pic">
                                <img class="lazy" data-original="<%# ImageFacade.GetMediaPathsFromRawData(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.ImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(SystemConfig.SiteUrl + "/Themes/PCweb/images/ppon-M1_pic.jpg").First()%>" alt="<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Title%>" width="290" height="162" />
                                <div class="activeLogo alo-m">
                                    <asp:Literal ID="lit_DealPromoImage" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="evn-cop-buytitle">
                                <a href="<%#"../deal/" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid + (string.IsNullOrEmpty(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa) ? string.Empty : ("?rsrc=" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa)) %>">
                                    <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Title%>
                                </a>
                            </div>
                            <div class="evn-cop-minorange-title evn-cop-community-title">
                                <a href="<%#"../deal/" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid + (string.IsNullOrEmpty(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa) ? string.Empty : ("rsrc=?" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa)) %>">
                                    <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Description%>
                                </a>
                            </div>
                            <div class="evn-cop-buyinformation" style='<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS <= DateTime.Now ? " display:block": " display:none"%>'>
                                <div class="evn-cop-buytotle">
                                    <%#((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.ItemPrice.ToString("F0")%>
                                </div>
                                <a <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Value ? string.Empty : "href=\"../deal/" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid + (string.IsNullOrEmpty(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa) ? string.Empty : ("?rsrc=" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa)) + "\""%>>
                                    <div class="e_rounded e_shadow<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Value ? " st-e-cop-disablebtn" : " st-e-cop-cmmunity-btn st-e-cop-buybtn"%>"><%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Value ? "已截止" : ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.EntrustSell == (int)EntrustSellType.KindReceipt ? "我要捐款" : "我要認購" %></div>
                                </a>
                            </div>
                            <div class="evn-cop-donationinfo e_textFamily" style='<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS > DateTime.Now ? " display:block": " display:none"%>'>
                                <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS.ToString("MM/dd")%>&nbsp;活動即將開始！
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:Repeater ID="rptZeroDeals" OnItemDataBound="rptPponDeals_ItemDataBound" runat="server" Visible="false">
                    <HeaderTemplate>
                        <div class="ly-e-cop-ppon rptTitle ppon">17Life</div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="pponDeal evn-cop-buy-sf-box e_shadow<%#GetPponDealCategoryCssClassName(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Category, ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.SubCategory)%>">
                            <div class="evn-cop-buy-pic">
                                <img class="lazy" data-original="<%# ImageFacade.GetMediaPathsFromRawData(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.ImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(SystemConfig.SiteUrl + "/Themes/PCweb/images/ppon-M1_pic.jpg").First()%>" alt="<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Title%>" width="290" height="162" />
                                <div class="activeLogo alo-m">
                                    <asp:Literal ID="lit_DealPromoImage" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="evn-cop-buytitle">
                                <a href="<%#"../deal/" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid + (string.IsNullOrEmpty(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa) ? string.Empty : ("?rsrc=" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa)) %>">
                                    <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Title%>
                                </a>
                            </div>
                            <div class="evn-cop-minorange-title">
                                <a href="<%#"../deal/" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid + (string.IsNullOrEmpty(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa) ? string.Empty : ("?rsrc=" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa)) %>">
                                    <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Description%>
                                </a>
                            </div>
                            <div class="evn-cop-buyinformation">
                                <div class="evn-cop-buytotle"><%# GetOrderedTotal(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid) %>  <span class="evn-cop-zeroget">人已索取</span></div>
                                <a <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Value ? string.Empty : "href=\"../deal/" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid + (string.IsNullOrEmpty(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa) ? string.Empty : ("?rsrc=" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa)) + "\""%>>
                                    <div class="e_rounded <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Value ? " st-e-cop-disablebtn" : " st-e-cop-buybtn"%>"><%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Value ? "已截止" : "參加" %></div>
                                </a>
                            </div>
                            <div class="evn-cop-donationinfo e_textFamily" style='<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS > DateTime.Now ? " display:block": " display:none"%>'>
                                <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS.ToString("MM/dd")%>&nbsp;開始敬請期待！
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <div id="couponContent" style="display: none">
                    <asp:Repeater ID="rptVourcher" OnItemDataBound="rptVourcher_ItemDataBound" runat="server" Visible="false">
                        <HeaderTemplate>
                            <div class="ly-e-cop-ppon coupon rptTitle">優惠券</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="evn-copcafarm clearfix">
                                <a href="#" class="rdl-p6-coupon-pic">
                                    <asp:Image ID="VourcherImage" AlternateText="<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Title%>" runat="server" />
                                </a>
                                <div class="evn-copcadealname">
                                    <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Title%>
                                </div>
                                <div class="evn-coptextinfo">
                                    <p class="evn-cop-ptext">
                                        <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Count()<=12? ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description : ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Substring(0,12)+".."%>
                                        <br class="rd-C-Hide" />
                                    </p>
                                    <input type="button"
                                        class='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "st-evncop-favoritetoo-btn" : "st-evncopca-col-btn" %>'
                                        value='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "已收藏" : "收藏" %>'
                                        onclick='return VourcherCollect(this, <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.ItemId %>);'>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:Repeater ID="rptVourcherLottery" OnItemDataBound="rptVourcher_ItemDataBound" runat="server" Visible="false">
                        <ItemTemplate>
                            <div class="evn-copcafarm clearfix">
                                <a href="#" class="rdl-p6-coupon-pic">
                                    <asp:Image ID="VourcherImage" AlternateText="<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Title%>" runat="server" />
                                </a>
                                <div class="evn-copcadealname">
                                    <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Title%>
                                </div>
                                <div class="evn-coptextinfo">
                                    <p class="evn-cop-ptext">
                                        <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Count()<=12? ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description : ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Substring(0,12)+".."%>
                                        <br class="rd-C-Hide" />
                                    </p>
                                    <input type="button"
                                        class='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "st-evncop-favoritetoo-btn" : "st-evncopca-col-btn" %>'
                                        value='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "已收藏" : "收藏" %>'
                                        onclick='return VourcherCollect(this, <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.ItemId %>);'>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div id="piinlifeContent" style="display: none;clear:both;">
                    <asp:Repeater ID="rptPiinlife" runat="server" OnItemDataBound="rptPiinlife_ItemDataBound">
                        <HeaderTemplate>
                            <div id="divpiinlifeTitle" class="ly-e-cop-ppon rptTitle ppon" style="<%=(TemplateType == EventPromoTemplateType.OnlyNewPiinlife)?"display:none;": ""%>">品生活</div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:PlaceHolder ID="phEnableLink" runat="server">
                                <div class="<%#((ViewEventPromo)(Container.DataItem)).CategoryName %>">
                                    <a class="<%#((ViewEventPromo)(Container.DataItem)).CategoryName %>" href="<%# "../piinlife/Deal.aspx?bid=" + ((ViewEventPromo)(Container.DataItem)).BusinessHourGuid + (string.IsNullOrEmpty(Rsrc) ? string.Empty : ("&rsrc=" + Rsrc)) %>" target="_blank">
                                        <div class="evn-cpp-pinnlife-box<%=(TemplateType == EventPromoTemplateType.OnlyNewPiinlife)?"-3column":""%> e_shadow">
                                            <div class="evn-cpp-pinnlife-pic-3column">
                                                <img class="lazy" data-original="<%# ImageFacade.GetMediaPathsFromRawData(((ViewEventPromo)(Container.DataItem)).ImagePath, MediaType.PponDealPhoto).DefaultIfEmpty("../Themes/default/images/17Life/G2/pinnLife_loading.jpg").First()%>" alt="<%# ((ViewEventPromo)(Container.DataItem)).Title%>" width="440" height="243" />
                                            </div>
                                            <div class="evn-cop-buytitle evn-cop-piin-text-width<%=(TemplateType == EventPromoTemplateType.OnlyNewPiinlife)?"-3column":""%>">
                                                <a href="<%# "../piinlife/Deal.aspx?bid=" + ((ViewEventPromo)(Container.DataItem)).BusinessHourGuid + (string.IsNullOrEmpty(Rsrc) ? string.Empty : ("&rsrc=" + Rsrc)) %>" target="_blank">
                                                    <%# ((ViewEventPromo)(Container.DataItem)).Title%>
                                                </a>
                                            </div>
                                            <div class="evn-cop-minorange-title evn-cop-piin-text-org-width<%=(TemplateType == EventPromoTemplateType.OnlyNewPiinlife)?"-3column":""%>">
                                                <a href="<%# "../piinlife/Deal.aspx?bid=" + ((ViewEventPromo)(Container.DataItem)).BusinessHourGuid + (string.IsNullOrEmpty(Rsrc) ? string.Empty : ("&rsrc=" + Rsrc)) %>" target="_blank"><%# ((ViewEventPromo)(Container.DataItem)).Description%></a>
                                            </div>
                                            <div class="rdl-pl-show">
                                                <a href="<%# "../piinlife/Deal.aspx?bid=" + ((ViewEventPromo)(Container.DataItem)).BusinessHourGuid + (string.IsNullOrEmpty(Rsrc) ? string.Empty : ("&rsrc=" + Rsrc)) %>" target="_blank">
                                                    <div class="st-e-cop-buybtn-piin e_rounded e_shadow ">馬上買</div>
                                                </a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="phDisabledLink" runat="server">
                                <div class="<%#((ViewEventPromo)(Container.DataItem)).CategoryName %>">
                                    <div class="evn-cpp-pinnlife-box<%=(TemplateType == EventPromoTemplateType.OnlyNewPiinlife)?"-3column":""%> e_shadow">
                                        <div class="evn-cpp-pinnlife-pic<%=(TemplateType == EventPromoTemplateType.OnlyNewPiinlife)?"-3column":""%>">
                                            <img class="lazy" data-original="<%# ImageFacade.GetMediaPathsFromRawData(((ViewEventPromo)(Container.DataItem)).ImagePath, MediaType.PponDealPhoto).DefaultIfEmpty("../Themes/default/images/17Life/G2/pinnLife_loading.jpg").First()%>" alt="<%# ((ViewEventPromo)(Container.DataItem)).Title%>" width="440" height="243" />
                                            <asp:Literal ID="llHiDealStauts" runat="server"></asp:Literal>
                                        </div>
                                        <div class="evn-cop-buytitle evn-cop-piin-text-width<%=(TemplateType == EventPromoTemplateType.OnlyNewPiinlife)?"-3column":""%>">
                                            <%# ((ViewEventPromo)(Container.DataItem)).Title%>
                                        </div>
                                        <div class="evn-cop-minorange-title evn-cop-piin-text-org-width<%=(TemplateType == EventPromoTemplateType.OnlyNewPiinlife)?"-3column":""%>">
                                            <%# ((ViewEventPromo)(Container.DataItem)).Description%>
                                        </div>
                                        <div class="rdl-pl-show">
                                            <div class="st-e-cop-buybtn-piin e_rounded e_shadow ">馬上買</div>
                                        </div>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:Repeater ID="rptVourcherStore" OnItemDataBound="rptVourcher_ItemDataBound" runat="server" Visible="false">
                        <ItemTemplate>
                            <div class="evn-copcafarm clearfix <%#((ViewEventPromo)(Container.DataItem)).CategoryName %>">
                                <a href="#" class="rdl-p6-coupon-pic">
                                    <asp:Image ID="VourcherImage" AlternateText="<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Title%>" runat="server" />
                                </a>
                                <div class="evn-copcadealname">
                                    <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Title%>
                                </div>
                                <div class="evn-coptextinfo">
                                    <p class="evn-cop-ptext">
                                        <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Count()<=12? ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description : ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Substring(0,12)+".."%>
                                        <br class="rd-C-Hide" />
                                    </p>
                                    <input type="button"
                                        class='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "st-evncop-favoritetoo-btn" : "st-evncopca-col-btn" %>'
                                        value='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "已收藏" : "收藏" %>'
                                        onclick='return VourcherCollect(this, <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.ItemId %>);'>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
        <div id="eventPromoEnd" style="display: none">
            <div class="ly-e-cop-ppon piinlife rptTitle">本活動已經結束囉</div>
            <a href="<%=SystemConfig.SiteUrl%>">去看看其它好康</a>
        </div>
    </div>
</asp:Content>

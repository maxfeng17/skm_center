﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="ExhibitionCommercial.aspx.cs" Inherits="LunchKingSite.Web.Event.ExhibitionCommercial" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph" TagPrefix="uc1" %>
<asp:Content ID="cDefaultMeta" ContentPlaceHolderID="cphPponMeta" runat="server">
    <meta property="fb:app_id" content="<%=FB_AppId%>">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<%=OgTitle%>" />
    <meta property="og:url" content="<%=OgUrl%>" />
    <meta property="og:site_name" content="17Life" />
    <meta property="og:description" content="<%=OgDescription%>" />
    <meta property="og:image" content="<%=OgImage%>" />
    <link rel="image_src" href="<%=LinkImageSrc%>" />
    <link rel="canonical" href="<%=LinkCanonicalUrl%>" />    
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../Tools/js/bjqs-1.3.skm.min.js"></script>
    <link href="../Tools/js/css/bjqs.css" type="text/css"
        rel="stylesheet" />
    <link href="../Themes/default/images/17Life/G2/A3-event_coupons.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/default/style/RDL.css" rel="stylesheet" type="text/css" />
    <%--<link href="../Themes/default/images/17Life/G2/skm/skm.css" rel="stylesheet" type="text/css" />--%>
    <%--<link href="../Themes/default/images/17Life/G2/skm/skm-RDL.css" rel="stylesheet" type="text/css" />--%>

    <script type='text/javascript'>
        function resizefun() {
            var x = $(window).width();
            if (x <= 960) {
                var myUrl = window.location.toString().toLowerCase();
                location.href = myUrl.replace("exhibitioncommercial.aspx", "exhibitionMobile.aspx");
            }
        }
        var delayFunction = function (mainFunc, option) {
            var delayTimer;
            option = option || {};
            if (!option.delay) {
                return mainFunc;
            } else {
                return function () {
                    var that = this;
                    var args = arguments;
                    if (delayTimer) clearTimeout(delayTimer);
                    delayTimer = setTimeout(function () {
                        mainFunc.apply(that, args);
                    }, option.delay);
                };
            }
        };

        function slidingMenuToggle() {
            $("#wrap").toggleClass("mbe-menu-show");
        }
        $(function () {
            $(".mbe-menu-btn").click(slidingMenuToggle);
            $(".mbe-channel").click(slidingMenuToggle);
        });

        $(document).ready(function () {

            if ("<%=EnableMobileEventPromo%>".toLowerCase() == "true") {
                resizefun();
                $(window).resize(delayFunction(function () { resizefun(); }, { delay: 300 }));
            }

            var urlParams = {};
            (function () {
                var e,
              a = /\+/g,  // Regex for replacing addition symbol with a space
              r = /([^&=]+)=?([^&]*)/g,
              d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
              q = window.location.search.substring(1);
                while (e = r.exec(q)) {
                    urlParams[d(e[1])] = d(e[2]);
                }
            })();
            
            var catid = urlParams['catid'];
            initCss();
            if (catid != undefined) {
                changecategory(catid);
            }else {
                changecategory(0);
            }

            $('html,body').scrollTop(parseInt($('[id*=hidScroll]').val(), 10));
            if ($('#banner-slide').length == 1) {
                $('#banner-slide').bjqs({
                    'animtype': 'slide',
                    'height': 850,
                    'width': 960,
                    'nexttext': '',
                    'prevtext': '',
                    'automatic ':false
                });
            }
            if ($('#lottory-banner-slide').length == 1) {
                $('#lottory-banner-slide').bjqs({
                    'animtype': 'slide',
                    'height': 560,
                    'width': 960,
                    'nexttext': '',
                    'prevtext': '',
                    'automatic ':false
                });
            }
            if ($('#skmstore-banner-slide').length == 1) {
                $('#skmstore-banner-slide').bjqs({
                    'animtype': 'slide',
                    'height': 560,
                    'width': 960,
                    'nexttext': '',
                    'prevtext': '',
                    'automatic ':false
                });
            }
           	
        });

        function initCss() {
            $('#wrap').attr("style", "<%=BgCss %>");
            if ('<%=BtnFontColor %>' != '') {
                $('.st-e-cop-optionsbn-big').css('color', '<%=BtnFontColor %>');
            }
        }


        function changecategory(id) {
  
            if (id == 0) {
                $('.skm-QrMessage').hide();
                $('#pan_Piinlife').show();
            } else {
                $('.skm-QrMessage').show();
                $('#pan_Piinlife').hide();
            }

            $.each($('.ly-e-cop-area-menubar .evn-cop-tn-bar'),function() {
                $(this).find('a').removeClass().addClass('st-e-cop-optionsbn-big');
            });

            $.each($('.evn-copcafarm'),function() {
                //console.log($(this).find('img').attr('src'));
                if ($(this).find('img').attr('src') == "") {
                    $(this).find('img').attr('src', '../Themes/default/images/17Life/G2/skm/topic_04.png');
                }
            });


            $('#c' + id).find('a').removeClass('st-e-cop-optionsbn-big').addClass('st-e-cop-optionsbn-big-on');
            $('.cate').hide();
            $('#cate' + id).show();
            
            if ('<%=BtnHover %>' != '' && '<%=BtnOriginal %>' != '') {
                $('.st-e-cop-optionsbn-big').css('background-image', '<%=BtnOriginal %>');
                $('.st-e-cop-optionsbn-big').hover(
                function () { $(this).css('background-image', '<%=BtnHover %>'); }, function () { $(this).css('background-image', '<%=BtnOriginal %>'); }
                );
            } else {
                $('.st-e-cop-optionsbn-big').removeClass("st-e-cop-optionsbn-big-on");
                $('.st-e-cop-optionsbn-big').hover(
                   function () { $(this).addClass("st-e-cop-optionsbn-big:hover"); }, function () { $(this).removeClass("st-e-cop-optionsbn-big:hover"); }
                );
            }

            if ('<%=BtnActive %>' != '') {
                $('#c' + id).find('.st-e-cop-optionsbn-big').unbind().css('background-image', '<%=BtnActive %>');
            } else {
                $('#c' + id).find('.st-e-cop-optionsbn-big').unbind().removeClass("st-e-cop-optionsbn-big:hover").addClass("st-e-cop-optionsbn-big-on");
            }

        }
        
        function VourcherCollect(btn, id) {
            if ($(btn).val() == '已收藏') {
                return false;
            }
            $.ajax({
                type: "POST",
                url: "ExhibitionCommercial.aspx/SetVourcherCollect",
                data: '{"eventId":' + id + '}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    $(btn).removeClass().addClass('st-skmcpcacol-favoritetoo-btn').val('已收藏');
                    alert("收藏成功！您可在有效期間內依使用規則享有此優惠。\n\n請先下載 17Life App，登入即可在「我的」→「優惠券收藏」內開啟此優惠券，於消費時出示即可使用");
                },
                error: function(msg) {
                    console.log(msg);
                    if (confirm("請先登入會員")) {
                        location.href = '<%=ResolveUrl(FormsAuthentication.LoginUrl) %>';
                    }
                }
            });
        }
    </script>
    <style type="text/css">
        .center
        {
            margin-top: 0;
        }        
        ol.bjqs-markers li a
        {
            width: 34px;
            height: 34px;
            margin: 5px;
            display: inline-block;
            font-size: 22px;
            line-height: 36px;
            color: #FFF;
            border-radius: 5px;
            background-color: rgba(24, 24, 24, 0.5);
        }

            ol.bjqs-markers li.active-marker a,
            ol.bjqs-markers li a:hover
            {
                background-color: rgba(24, 24, 24, 1);
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <script type="text/javascript">
        Sys.Application.add_load(initCss);
    </script>
    <div id="btnBackTop" class="HKL_Button_back_to_top_command"
        style="display: none; cursor: pointer" title="返回頂部">
    </div>
    <asp:TextBox ID="hidScroll" runat="server" Style="display: none;" Text="0"></asp:TextBox>
        <div id="TOPBanner">
            <asp:Literal ID="liMainPic" runat="server"></asp:Literal>
        </div>

        <asp:Panel ID="pan_Category" runat="server" CssClass="ly-e-cop-area-menubar">
            <div class="evn-cop-tn-bar clearfix">
                <asp:Repeater ID="rpt_Category" runat="server">
                    <ItemTemplate>
                        <div class="st-e-cop-optionsbar" data-cat='<%# ((KeyValuePair<int, string>)Container.DataItem).Value %>'
                            onclick='changecategory(<%# ((KeyValuePair<int, string>)Container.DataItem).Key %>);'
                            id='<%# "c"+((KeyValuePair<int, string>)Container.DataItem).Key %>'>
                            <a target="_blank" href="#" class="st-e-cop-optionsbn-big" onclick="return false;"><%# ((KeyValuePair<int, string>)Container.DataItem).Value %></a>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </asp:Panel>
        <div class="skm-QrMessagefarm">
            <div class="skm-QrMessage">
                <div class="skm-QrMesText">免費下載App，隨時隨地抓住好康 ─</div>
                <br />
                <a href="<%=SiteUrl%>/ppon/promo.aspx?cn=app">
                    <div class="skm-qr"></div>
                </a>
            </div>
        </div>
        <asp:Panel ID="pan_Main" runat="server">
            <asp:Panel ID="pan_Items" runat="server" CssClass="ly-e-cop-farme clearfix">
            </asp:Panel>
            <asp:Panel ID="pan_Piinlife" runat="server" CssClass="ly-e-cop-farme clearfix" ClientIDMode="Static">
                <uc1:Paragraph ID="piinlifeDeal" ExternalCache="true" runat="server" />
            </asp:Panel>
            <asp:Panel ID="divVourcher" runat="server" Visible="false">
                <div id="cate<%= (int)EventPromoItemType.Vourcher %>" class="cate">
                    <asp:Panel ID="divVourcherMain" runat="server" Visible="false" CssClass="ly-e-cop-farme clearfix">
                        <%= VourcherCount > 12 ? "<div id='banner-slide'><ul class='bjqs'><li>" : string.Empty %>
                        <asp:Repeater ID="rptVourcher" runat="server" OnItemDataBound="rptVourcher_ItemDataBound">
                            <ItemTemplate>
                                <div class="evn-copcafarm clearfix">
                                    <a href="#" class="" onclick="return false;">
                                        <asp:Image ID="VourcherImage" AlternateText="<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Title%>" runat="server" />
                                    </a>
                                    <div class="evn-copcadealname">
                                        <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Title%>
                                    </div>
                                    <div class="evn-coptextinfo" style='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Key.StartDate <= DateTime.Now ? " display:block": " display:none"%>'>
                                        <p class="evn-cop-ptext">
                                            <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Count()<=12? ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description : ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Substring(0,12)+".."%>
                                        </p>
                                        <input type="button"
                                            class='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "st-evncop-favoritetoo-btn" : "st-evncopca-col-btn" %>'
                                            value='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "已收藏" : "收藏" %>'
                                            onclick='return VourcherCollect(this, <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.ItemId %>);'>
                                    </div>
                                </div>
                                <%# VourcherCount > 12 ? ((VourcherCount == (Container.ItemIndex + 1) && VourcherCount % 12 == 0) ? string.Empty : ((Container.ItemIndex + 1) % 12 == 0 ? "</li><li>" : string.Empty)) : string.Empty %>
                            </ItemTemplate>
                        </asp:Repeater>
                        <%= VourcherCount > 12 ? "</li></ul></div>" : string.Empty %>
                    </asp:Panel>
                    <br />
                    <asp:Panel ID="divVourcherLottery" runat="server" Visible="false" CssClass="ly-e-cop-farme clearfix">
                        <div class="ly-skm-ctegory-title">
                            活動合作店家  
  <p class="skm-ct-p">前往以下店家消費刷台新卡，可獲得一次抽獎機會</p>
                        </div>
                        <%= LottoryVourcherCount > 8 ? "<div id='lottory-banner-slide'><ul class='bjqs'><li>" : string.Empty %>
                        <asp:Repeater ID="rptVourcherLottery" runat="server" OnItemDataBound="rptVourcher_ItemDataBound">
                            <ItemTemplate>
                                <div class="evn-copcafarm clearfix">
                                    <a href="#" class="" onclick="return false;">
                                        <asp:Image ID="VourcherImage" runat="server" />
                                    </a>

                                    <div class="evn-copcadealname">
                                        <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Title%>
                                    </div>
                                    <div class="evn-coptextinfo" style='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Key.StartDate <= DateTime.Now ? " display:block": " display:none"%>'>
                                        <p class="evn-cop-ptext">
                                            <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Count()<=12? ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description : ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Substring(0,12)+".."%>
                                        </p>
                                        <input type="button"
                                            class='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "st-evncop-favoritetoo-btn" : "st-evncopca-col-btn" %>'
                                            value='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "已收藏" : "收藏" %>'
                                            onclick='return VourcherCollect(this, <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.ItemId %>);'>
                                    </div>
                                </div>
                                <%# LottoryVourcherCount > 8 ? ((LottoryVourcherCount == (Container.ItemIndex + 1) && LottoryVourcherCount % 8 == 0) ? string.Empty : ((Container.ItemIndex + 1) % 8 == 0 ? "</li><li>" : string.Empty)) : string.Empty %>
                            </ItemTemplate>
                        </asp:Repeater>
                        <%= LottoryVourcherCount > 8 ? "</li></ul></div>" : string.Empty %>
                    </asp:Panel>
                    <br />
                    <asp:Panel ID="divVourcherStore" runat="server" Visible="false" CssClass="ly-e-cop-farme clearfix">
                        <div class="ly-skm-ctegory-title">
                            新光三越合作店家  
                            <p class="skm-ct-p">前往以下店家消費刷台新卡，可獲得一次抽獎機會</p>
                        </div>
                        <%= SkmStoreVourcherCount > 8 ? "<div id='skmstore-banner-slide'><ul class='bjqs'><li>" : string.Empty %>
                        <asp:Repeater ID="rptVourcherStore" runat="server" OnItemDataBound="rptVourcher_ItemDataBound">
                            <ItemTemplate>
                                <div class="evn-copcafarm clearfix">
                                    <a href="#" class="" onclick="return false;">
                                        <asp:Image ID="VourcherImage" runat="server" />
                                    </a>

                                    <div class="evn-copcadealname">
                                        <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Title%>
                                    </div>
                                    <div class="evn-coptextinfo" style='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Key.StartDate <= DateTime.Now ? " display:block": " display:none"%>'>
                                        <p class="evn-cop-ptext">
                                            <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Count()<=12? ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description : ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Substring(0,12)+".."%>
                                        </p>
                                        <input type="button"
                                            class='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "st-evncop-favoritetoo-btn" : "st-evncopca-col-btn" %>'
                                            value='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "已收藏" : "收藏" %>'
                                            onclick='return VourcherCollect(this, <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.ItemId %>);'>
                                    </div>
                                </div>
                                <%# SkmStoreVourcherCount > 8 ? ((SkmStoreVourcherCount == (Container.ItemIndex + 1) && SkmStoreVourcherCount % 8 == 0) ? string.Empty : ((Container.ItemIndex + 1) % 8 == 0 ? "</li><li>" : string.Empty)) : string.Empty %>
                            </ItemTemplate>
                        </asp:Repeater>
                        <%= SkmStoreVourcherCount > 8 ? "</li></ul></div>" : string.Empty %>
                    </asp:Panel>
                </div>
            </asp:Panel>
            <asp:Panel ID="pan_ActivityMeasures" runat="server" CssClass="ly-evn-pprule" ClientIDMode="Static">
                <div class="evn-rule-title">活動辦法</div>
                <div class="evn-rule-content"><asp:Literal ID="lit_Description" runat="server"></asp:Literal></div>
                    
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pan_Sub" runat="server" Visible="false">
            <div class="PPClose">
                <div class="PPSpace">
                </div>
                <a href="https://www.17life.com" class="BackBtn"></a>
            </div>
        </asp:Panel>
    </div>
</asp:Content>

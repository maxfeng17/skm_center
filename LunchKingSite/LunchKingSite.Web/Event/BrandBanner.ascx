﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BrandBanner.ascx.cs" Inherits="LunchKingSite.Web.Event.BrandBanner" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>

<asp:PlaceHolder ID="pan_banner" runat="server">
	<div class="nd_banner" id="nd_banner" runat="server">
		<ul class="bxslider">
			<asp:Literal ID="litBanner" runat="server"></asp:Literal>
		</ul>
	</div>
	<div class="rd_banner" id="rd_banner" runat="server">
		<% 
			foreach (var hotDeal in this.hotDeals)
			{
				string link = Helper.CombineUrl(config.SiteUrl, "deal", hotDeal.BusinessHourGuid.ToString());
				string img = ImageFacade.GetDealPic(hotDeal);
				string title = hotDeal.EventTitle;
				bool is24H = hotDeal.CategoryIds.Contains(ViewPponDealManager._24H_ARRIVAL_CATEGORY_ID);
		%> 
		<div class="zitem item-pic" style="width:230px">
			<a href="<%=link %>" target="_blank">
				<img src="<%=img %>" class="img-zoom lazy" style="width:230px; height:128px" />
				<% if (is24H) {%>
				<img class="flag-24h" src="/themes/PCweb/images/24h_cover_flag.svg" alt="" />
				<%} %>
			</a>
			<div class="caption">
			<a href="<%=link %>" target="_blank">
				<%=title %>
			</a>
				</div>
		</div>
		<% } %>
	</div>
	<div class="clear" id="banner_end" runat="server"></div>
</asp:PlaceHolder>

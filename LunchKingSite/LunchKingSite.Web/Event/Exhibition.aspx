﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="Exhibition.aspx.cs" Inherits="LunchKingSite.Web.Event.Exhibition" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>

<%@ Register Src="~/Event/ExhibitionCategoryBar.ascx" TagName="ExhibitionCategoryBar"
    TagPrefix="ucCategoryBar" %>
<%@ Register Src="~/Event/ExhibitionSubCategoryBar.ascx" TagName="ExhibitionSubCategoryBar"
    TagPrefix="ucCategoryBar" %>

<asp:Content ID="cDefaultMeta" ContentPlaceHolderID="cphPponMeta" runat="server">
    <meta property="fb:app_id" content="<%=FB_AppId%>">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<%=OgTitle%>" />
    <meta property="og:url" content="<%=OgUrl%>" />
    <meta property="og:site_name" content="17Life" />
    <meta property="og:description" content="<%=OgDescription%>" />
    <meta property="og:image" content="<%=OgImage%>" />
    <link rel="image_src" href="<%=LinkImageSrc%>" />
    <link rel="canonical" href="<%=LinkCanonicalUrl%>" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href="../Themes/default/images/17Life/G2/A3-event_coupons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Tools/js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="../Tools/js/jquery.nav.js"></script>
    <script type='text/javascript'>
        function resizefun() {
            var x = $(window).width();
            if (x <= 1000) {
                var myUrl = window.location.toString().toLowerCase();
                location.href = myUrl.replace("exhibition.aspx", "exhibitionMobile.aspx");
            }
        }
        var delayFunction = function (mainFunc, option) {
            var delayTimer;
            option = option || {};
            if (!option.delay) {
                return mainFunc;
            } else {
                return function () {
                    var that = this;
                    var args = arguments;
                    if (delayTimer) clearTimeout(delayTimer);
                    delayTimer = setTimeout(function () {
                        mainFunc.apply(that, args);
                    }, option.delay);
                };
            }
        };
        $(document).ready(function () {
            if ("<%=EnableMobileEventPromo%>".toLowerCase() == "true") {
                resizefun();
                $(window).resize(delayFunction(function () { resizefun(); }, { delay: 300 }));
            }

            var urlParams = {};
            (function () {
                var e,
              a = /\+/g,  // Regex for replacing addition symbol with a space
              r = /([^&=]+)=?([^&]*)/g,
              d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
              q = window.location.search.substring(1);
                while (e = r.exec(q)) {
                    urlParams[d(e[1])] = d(e[2]);
                }
            })();
            initCss();

            if ($('.Curatorial-main li').length > 0) {
                $('.Curatorial-main').find('a').each(function () {
                    if ($(this).attr('href').indexOf('<%= Url%>') > 0) {
                        $(this).parent().addClass('on')
                    }
                });

                $(".NaviCityW").hover(
                function () {
                    $(this).stop(true, true).removeClass("fadeOut").addClass("fadeIn");
                },
                function () {
                    $(this).removeClass("fadeIn").addClass("fadeOut");
                });
            }

            if ("<%=IsCuration%>".toLowerCase() == "true") {
                $('.Curatorial-Sort').show();
            }

            changecategory(0);
        });


        function initCss() {
            $('#wrap').attr("style", "<%=BgCss %>");
        }


        function changecategory(id) {
            $('.cate').hide();
            $('#cate' + id).show();
            if ('<%=BtnHover %>' != '' && '<%=BtnOriginal %>' != '') {
                $('.st-e-cop-optionsbn').css('background-image', '<%=BtnOriginal %>');
                $('.st-e-cop-optionsbn').hover(
                function () { $(this).css('background-image', '<%=BtnHover %>'); }, function () { $(this).css('background-image', '<%=BtnOriginal %>'); }
                );
            } else {
                $('.st-e-cop-optionsbn').removeClass("st-e-cop-optionsbn-on");
                $('.st-e-cop-optionsbn').hover(
                   function () { $(this).addClass("st-e-cop-optionsbn:hover"); }, function () { $(this).removeClass("st-e-cop-optionsbn:hover"); }
                );
            }

            if ('<%=BtnActive %>' != '') {
                $('#c' + id).find('.st-e-cop-optionsbn').unbind().css('background-image', '<%=BtnActive %>');
            } else {
                $('#c' + id).find('.st-e-cop-optionsbn').unbind().removeClass("st-e-cop-optionsbn:hover").addClass("st-e-cop-optionsbn-on");
            }

            ChangeSubMenu(id);
        }
    </script>
    <style type="text/css">
        .center {
            margin-top: 0;
        }

        .PPClose {
            width: 100%;
            height: 350px;
            background: url(../Themes/PCweb/images/CloseBG.png) no-repeat;
            display: block;
        }

        .PPSpace {
            width: 100%;
            height: 187px;
            display: block;
        }

        .BackBtn {
            width: 286px;
            height: 76px;
            background: url(../Themes/PCweb/images/CloseBtn.png);
            margin-left: 445px;
            display: block;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <script type="text/javascript">
        Sys.Application.add_load(initCss);
    </script>
    <div class="center ac-center">
        <input id="HfCityId" type="hidden" value="<%= CityId%>" />
        <div id="btnBackTop" class="HKL_Button_back_to_top_command"
            style="display: none; cursor: pointer" title="返回頂部">
        </div>
        <%-- 側邊anchor Start --%>
        <ucCategoryBar:ExhibitionSubCategoryBar ID="ucSubCategoryBar" runat="server" />
        <%-- 側邊anchor End --%>
        <asp:PlaceHolder ID="divMainEvent" runat="server">
            <div id="Act-TOP" class="rd-top-box">
                <asp:Literal ID="liMainPic" runat="server"></asp:Literal>
            </div>
        </asp:PlaceHolder>
        <ucCategoryBar:ExhibitionCategoryBar ID="ucCategoryBar" runat="server" />
        <asp:Panel ID="pan_Main" runat="server" CssClass="ly-e-cop-farme clearfix">
            <asp:PlaceHolder ID="pan_Items" runat="server"></asp:PlaceHolder>
        </asp:Panel>
        <asp:Panel ID="pan_ActivityMeasures" runat="server" CssClass="ly-evn-pprule" ClientIDMode="Static">
            <div class="evn-rule-title">
                活動辦法
            </div>
            <div class="evn-rule-content">
                <asp:Literal ID="lit_Description" runat="server"></asp:Literal>
            </div>
        </asp:Panel>
    </div>
    <!--center-->
    <asp:Panel ID="pan_Sub" runat="server" Visible="false">
        <div class="PPClose">
            <div class="PPSpace">
            </div>
            <a href="https://www.17life.com" class="BackBtn"></a>
        </div>
    </asp:Panel>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="ExhibitionJoinPiinlife.aspx.cs" Inherits="LunchKingSite.Web.Event.ExhibitionJoinPiinlife" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>

<%@ Register Src="~/Event/ExhibitionCategoryBar.ascx" TagName="ExhibitionCategoryBar"
    TagPrefix="ucCategoryBar" %>
<%@ Register Src="~/Event/ExhibitionSubCategoryBar.ascx" TagName="ExhibitionSubCategoryBar"
    TagPrefix="ucCategoryBar" %>

<asp:Content ID="cDefaultMeta" ContentPlaceHolderID="cphPponMeta" runat="server">
    <meta property="fb:app_id" content="<%=FbAppId%>">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<%=OgTitle%>" />
    <meta property="og:url" content="<%=OgUrl%>" />
    <meta property="og:site_name" content="17Life" />
    <meta property="og:description" content="<%=OgDescription%>" />
    <meta property="og:image" content="<%=OgImage%>" />
    <link rel="image_src" href="<%=LinkImageSrc%>" />
    <link rel="canonical" href="<%=LinkCanonicalUrl%>" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../Tools/js/bjqs-1.3.min.js"></script>
    <link href="../Tools/js/css/bjqs.css" type="text/css" rel="stylesheet" />
    <link href="../Themes/default/images/17Life/G2/A3-event_coupons.css" rel="stylesheet" type="text/css" />
    
    <%--<link href="../Themes/default/style/RDL.css" rel="stylesheet" type="text/css" />--%>
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Tools/js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="../Tools/js/jquery.nav.js"></script>
    <script type='text/javascript'>
        function resizefun() {
            var x = $(window).width();
            if (x <= 960) {
                var myUrl = window.location.toString().toLowerCase();
                location.href = myUrl.replace("exhibitionjoinpiinlife.aspx", "exhibitionMobile.aspx");
            }
        }
        var delayFunction = function (mainFunc, option) {
            var delayTimer;
            option = option || {};
            if (!option.delay) {
                return mainFunc;
            } else {
                return function () {
                    var that = this;
                    var args = arguments;
                    if (delayTimer) clearTimeout(delayTimer);
                    delayTimer = setTimeout(function () {
                        mainFunc.apply(that, args);
                    }, option.delay);
                };
            }
        };
        $(document).ready(function () {
            if ("<%=EnableMobileEventPromo%>".toLowerCase() == "true") {
                resizefun();
                $(window).resize(delayFunction(function () { resizefun(); }, { delay: 300 }));
            }
            var urlParams = {};
            (function () {
                var e,
              a = /\+/g,  // Regex for replacing addition symbol with a space
              r = /([^&=]+)=?([^&]*)/g,
              d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
              q = window.location.search.substring(1);
                while (e = r.exec(q)) {
                    urlParams[d(e[1])] = d(e[2]);
                }
            })();
            var cat = urlParams['cat'];
            initCss();
            var selectedIdx = 0;
            var _optionsbn = $('.st-e-cop-optionsbn');
            $.each(_optionsbn, function (__idx, __obj) {
                if ($.trim(__obj.innerText) == $.trim(cat)) {
                    selectedIdx = __idx;
                }
            });

            changecategory(selectedIdx);

            $('html,body').scrollTop(parseInt($('[id*=hidScroll]').val(), 10));

            $('.cate').each(function () {
                var banner = $(this).find('#banner-slide');
                if (banner.length == 1) {
                    banner.find('ul').removeClass();
                    //banner.bjqs({
                    //    'animtype': 'slide',
                    //    'height': 650,
                    //    'width': 960,
                    //    'nexttext': '',
                    //    'prevtext': '',
                    //    'hoverpause': true
                    //});
                }
            });
        });

        $(window).scroll(function () {
            $('[id*=hidScroll]').val(typeof window.pageYOffset != 'undefined' ? window.pageYOffset : document.documentElement.scrollTop);
        });

        function initCss() {
            $('#wrap').attr("style", "<%=BgCss %>");
        }

        function changecategory(id) {
            $('#piinLifeTitleDiv').show();
            $('[id*=pan_Main]').show();
            $('.cate').hide();
            $('[class*=PiinlifeCategoryID]').hide();
            $('#cate' + id).show();
            $('*.PiinlifeCategoryID' + id).show();
            if ('<%=BtnHover %>' != '' && '<%=BtnOriginal %>' != '') {
                $('.st-e-cop-optionsbn').css('background-image', '<%=BtnOriginal %>');
                $('.st-e-cop-optionsbn').hover(
                function () { $(this).css('background-image', '<%=BtnHover %>'); }, function () { $(this).css('background-image', '<%=BtnOriginal %>'); }
                );
            } else {
                $('.st-e-cop-optionsbn').removeClass("st-e-cop-optionsbn-on");
                $('.st-e-cop-optionsbn').hover(
                   function () { $(this).addClass("st-e-cop-optionsbn:hover"); }, function () { $(this).removeClass("st-e-cop-optionsbn:hover"); }
                );
            }

            if ('<%=BtnActive %>' != '') {
                $('#c' + id).find('.st-e-cop-optionsbn').unbind().css('background-image', '<%=BtnActive %>');
            } else {
                $('#c' + id).find('.st-e-cop-optionsbn').unbind().removeClass("st-e-cop-optionsbn:hover").addClass("st-e-cop-optionsbn-on");
            }
            if ($('[id*=pan_Main]').find("[id*=cate]:visible").find("[id*=pan_EnableLink]").length == 0) {
                $('[id*=pan_Main]').hide();
            }
            if ($('[class*=PiinlifeCategoryID]:visible').length == 0) {
                $('#piinLifeTitleDiv').hide();
            }
            ChangeSubMenu(id);
        }
        function appendUrl(obj) {
            ChangeUrl("cat", obj.innerText);
        }

        function ChangeUrl(key, value) {
            key = encodeURIComponent(key); value = encodeURIComponent(value);

            var s = document.location.search;
            var kvp = key + "=" + value;

            s = getParams(key, value);
            s += (s.length > 0 ? '&' : '?') + kvp;

            try {
                if (typeof (history.pushState) != "undefined") {
                    history.pushState({ id: 'SOME ID' }, '', s);
                } else {
                }
            } catch (err) {
                console.log(err.message);
            }

        }

        function getParams(key, value) {
            var s = document.location.search;
            var newUrl = "";
            params = s.split("&");
            for (i = 0 ; i < params.length; i++) {
                var paramKey = params[i].split("=")[0];
                if (paramKey != key) {
                    newUrl += params[i] + "&";
                }
            }
            if (newUrl.endsWith("&")) {
                newUrl = newUrl.substring(0, newUrl.length - 1);
            }
            return newUrl;
        }
    </script>
    <style type="text/css">
        .center
        {
            margin-top: 0;
        }

        ol.bjqs-markers li a
        {
            width: 34px;
            height: 34px;
            margin: 5px;
            display: inline-block;
            font-size: 22px;
            line-height: 36px;
            color: #FFF;
            border-radius: 5px;
            background-color: rgba(24, 24, 24, 0.5);
        }

        ol.bjqs-markers li.active-marker a,
        ol.bjqs-markers li a:hover
        {
            background-color: rgba(24, 24, 24, 1);
        }
        .evn-cpp-pinnlife-pic
        {
            height:183px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <script type="text/javascript">
        Sys.Application.add_load(initCss);
    </script>
    <div class="center ac-center">
        <div id="btnBackTop" class="HKL_Button_back_to_top_command"
            style="display: none; cursor: pointer" title="返回頂部">
        </div>
        <asp:TextBox ID="hidScroll" runat="server" Style="display: none;" Text="0"></asp:TextBox>

        <%-- 側邊anchor Start --%>
        <ucCategoryBar:ExhibitionSubCategoryBar ID="ucSubCategoryBar" runat="server" />
        <%-- 側邊anchor End --%>

        <div id="Act-TOP" class="rd-top-box">
            <asp:Literal ID="liMainPic" runat="server"></asp:Literal>
        </div>
        <asp:Literal ID="liItemType" runat="server"></asp:Literal>

        <ucCategoryBar:ExhibitionCategoryBar ID="ucCategoryBar" runat="server" />
        

        <asp:Panel ID="pan_Main" runat="server" CssClass="ly-e-cop-farme clearfix">
            <div class="ly-e-cop-ppon">17Life</div>
            <asp:PlaceHolder ID="pan_Items" runat="server"></asp:PlaceHolder>
            <br />
            <br />
        </asp:Panel>
        
        <asp:PlaceHolder ID="divPiinlife" runat="server" Visible="false">
            <div id="piinLifeTitleDiv" class="ly-e-cop-ppon">PiinLife.品生活</div>
            <div class="clearfix">
                <asp:Repeater ID="rptPiinlife" runat="server" OnItemDataBound="rptPiinlife_ItemDataBound">
                    <ItemTemplate>
                        <asp:PlaceHolder ID="phEnableLink" runat="server">
                                <a href="<%# "../piinlife/Deal.aspx?bid=" + ((ViewEventPromo)(Container.DataItem)).BusinessHourGuid + (string.IsNullOrEmpty(Rsrc) ? string.Empty : ("&rsrc=" + Rsrc)) %>" target="_blank">
                                    <div class="evn-cpp-pinnlife-box e_shadow <%# ((ViewEventPromo)(Container.DataItem)).CategoryName %>">
                                        <div class="evn-cpp-pinnlife-pic">
                                            <img src="<%# ImageFacade.GetMediaPathsFromRawData(((ViewEventPromo)(Container.DataItem)).ImagePath, MediaType.PponDealPhoto).DefaultIfEmpty("../Themes/default/images/17Life/G2/PPImage/PPDPic.jpg").First() %>" alt="<%# ((ViewEventPromo)(Container.DataItem)).Title%>" />
                                        </div>
                                        <div class="evn-cop-buytitle evn-cop-piin-text-width">
                                            <a href="<%# "../piinlife/Deal.aspx?bid=" + ((ViewEventPromo)(Container.DataItem)).BusinessHourGuid + (string.IsNullOrEmpty(Rsrc) ? string.Empty : ("&rsrc=" + Rsrc)) %>" target="_blank">
                                                <%# ((ViewEventPromo)(Container.DataItem)).Title%>
                                            </a>
                                        </div>
                                        <div class="evn-cop-minorange-title evn-cop-piin-text-org-width">
                                            <a href="<%# "../piinlife/Deal.aspx?bid=" + ((ViewEventPromo)(Container.DataItem)).BusinessHourGuid + (string.IsNullOrEmpty(Rsrc) ? string.Empty : ("&rsrc=" + Rsrc)) %>" target="_blank"><%# ((ViewEventPromo)(Container.DataItem)).Description%></a>
                                        </div>
                                        <asp:PlaceHolder ID="phInstallment" runat="server" Visible="false">
                                            <div class="visa-installment">分期0利率</div>
                                        </asp:PlaceHolder>
                                    </div>
                                </a>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="phDisabledLink" runat="server">
                                <a href="<%# "../piinlife/Deal.aspx?bid=" + ((ViewEventPromo)(Container.DataItem)).BusinessHourGuid + (string.IsNullOrEmpty(Rsrc) ? string.Empty : ("&rsrc=" + Rsrc)) %>" target="_blank">
                                    <div class="evn-cpp-pinnlife-box e_shadow <%# ((ViewEventPromo)(Container.DataItem)).CategoryName %>">
                                        <div class="evn-cpp-pinnlife-pic">
                                            <img src="<%# ImageFacade.GetMediaPathsFromRawData(((ViewEventPromo)(Container.DataItem)).ImagePath, MediaType.PponDealPhoto).DefaultIfEmpty("../Themes/default/images/17Life/G2/PPImage/PPDPic.jpg").First()%>" alt="<%# ((ViewEventPromo)(Container.DataItem)).Title%>" />
                                            <asp:Literal ID="llHiDealStauts" runat="server"></asp:Literal>
                                        </div>
                                        <div class="evn-cop-buytitle evn-cop-piin-text-width">
                                            <%# ((ViewEventPromo)(Container.DataItem)).Title%>
                                        </div>
                                        <div class="evn-cop-minorange-title evn-cop-piin-text-org-width">
                                            <%# ((ViewEventPromo)(Container.DataItem)).Description%>
                                        </div>
                                    </div>
                                </a>
                        </asp:PlaceHolder>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </asp:PlaceHolder>
        
        <asp:Panel ID="pan_ActivityMeasures" runat="server" CssClass="ly-evn-pprule" ClientIDMode="Static" Visible="false">
            <div class="evn-rule-title">
                活動辦法
            </div>
            <div class="evn-rule-content">
                <asp:Literal ID="lit_Description" runat="server"></asp:Literal>
            </div>
        </asp:Panel>

    </div>
    <asp:Panel ID="pan_Sub" runat="server" Visible="false">
        <div class="PPClose">
            <div class="PPSpace">
            </div>
            <a href="https://www.17life.com" class="BackBtn"></a>
        </div>
    </asp:Panel>
</asp:Content>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BrandItem.ascx.cs"
    Inherits="LunchKingSite.Web.Event.BrandItem" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.WebLib.Models.Ppon" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>

<div id="tabsA" class="typeB">
    <ul class="btn_type">
        <asp:Literal ID="litItemTabs" runat="server" />
    </ul>
    <asp:Repeater ID="rpt_mainItem" OnItemDataBound="rpt_MainItem_ItemDataBound" runat="server">
        <ItemTemplate>
            <asp:Literal runat="server" ID="headerData" />
                <asp:Repeater ID="rpt_Item" runat="server">
                    <ItemTemplate>
                        <div class="item_3col">
                            <a class="open_new_window" href="<%# string.Format("/deal/{0}", ((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.BusinessHourGuid) %>" target="_blank">
                                <div class="item_soldout_480" style="<%# ((((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.OrderedQuantity >= ((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.OrderTotalLimit) ? " ": "display:none;") %>">
                                    <img src="/Themes/PCweb/images/<%# (((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.IsBankDeal ?? false) ? "completed_bar_480.png" : "soldout_bar_480.png" %>">
                                </div>
                                <div class="item_activeLogo" style="display: block">
                                    <%# ((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.PromoImageHtml %>
                                </div>
                                <%# ((ViewMultipleMainDeal)(Container.DataItem)).clit_CityName %>
                                <div class="item_3col_pic item_pic">
                                    <img class="multipleDealLazy" src="<%# ((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.DefaultDealImage %>">
									<%# ((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.CategoryIds.Contains(ViewPponDealManager._24H_ARRIVAL_CATEGORY_ID) ? 
											"<img class='flag-24h' src='/themes/PCweb/images/24h_cover_flag.svg' alt='' />" : "" %>
                                </div>
                                <div class="item_3col_pic_bg">
                                    <img src="/Themes/PCweb/images/ppon-M1_pic.jpg">
                                </div>
                                <div class="item_title">
                                    <%# ((((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.IsBankDeal ?? false) ? "" : OrderedQuantityHelper.Show(((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal, OrderedQuantityHelper.ShowType.BetaInPortal) )%>
                                    <ul style="<%# ((((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.IsBankDeal ?? false) ? "display:none;": "" )%>">
                                        <%# ((ViewMultipleMainDeal)(Container.DataItem)).deal_Label_2 %>
                                    </ul>
                                    <%# string.IsNullOrEmpty(((ViewMultipleMainDeal)(Container.DataItem)).clit_CityName2) ? ""
                                            : "<span class='tag_place'>" + ((ViewMultipleMainDeal)(Container.DataItem)).clit_CityName2 + "</span>"%>
                                    <div class="tag_title"><%# PponFacade.GetDisplayPponName(((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal) %></div>
                                    <p class="tag_subtitle"><%# ((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.EventTitle + ((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.DealPromoTitle %></p>

                                    <div class="item_info clearfix">
                                        <div class="item_info_detail">
                                            <div style="display: inline-block">
                                                <span>原價</span><span class="text_line">$<%# ((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.ItemOrigPrice.ToString("F0") %></span>
                                                <span>| </span>
                                                <span><%# ((ViewMultipleMainDeal)(Container.DataItem)).discount_1 %></span>
                                            </div>
                                            <div style="<%# (((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.DiscountPrice.HasValue ? "display: inline-block": "display:none;") %>">
                                                <span>好康價</span>
                                                <span class="text_big" style="">$<%# PponFacade.CheckZeroPriceToShowPrice(((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal, 0).ToString("F0")%></span>
                                                <span style=""><%#((((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.BusinessHourStatus & (int)(BusinessHourStatus.ComboDealMain)) > 0 ? "起" : "元") %></span>  
                                            </div>
                                        </div>
                                        <!-- 在地檔才有評價 -->
                                        <%# ((ViewMultipleMainDeal)(Container.DataItem)).RatingString %>
                                     </div>
                                </div>
                                <!-- 卡片底欄 -->
                                <div class="item_3col_price_less">
                                    <div class="item_discount clearfix" style="<%# (((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.DiscountPrice.HasValue ? "display:none;": "") %>">
                                        <span class="disc_black">超值好康價</span>
                                        <span class="disc_price color_red">$<%# PponFacade.CheckZeroPriceToShowPrice(((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal, 0).ToString("F0")%></span>
                                        <span class="disc_black" style="display:inline-block"><%#((((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.BusinessHourStatus & (int)(BusinessHourStatus.ComboDealMain)) > 0 ? "起" : "元") %></span>
                                    </div>
                                   
                                    <div class="item_discount clearfix" style="<%# (((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.DiscountPrice.HasValue ? "": "display:none;") %>">
                                        <i class="fa fa-bullhorn" aria-hidden="true"></i>
                                        <div class="disc_text">結帳使用折價</div>
                                        <span class="disc_price color_red">$<%#(((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal).DiscountPrice.ToString("0")%></span>
                                        <span class="disc_black" style="display:inline-block"><%# ((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.ComboDelas != null && ((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.ComboDelas.Count > 1 ? "起" : "元" %></span>
                                    </div>
                                    
                                    <div class="item_collect" data-bid="<%# string.Format("{0}", ((ViewMultipleMainDeal)(Container.DataItem)).deal.PponDeal.BusinessHourGuid) %>">
                                        <a href="javascript:void(0)" class="">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>收藏
                                        </a>
                                    </div>

                                </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            <asp:Literal runat="server" ID="footerData" />
        </ItemTemplate>
    </asp:Repeater>
</div>

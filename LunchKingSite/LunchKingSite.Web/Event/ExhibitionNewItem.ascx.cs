﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.DataOrm;
namespace LunchKingSite.Web.Event
{
    public partial class ExhibitionNewItem : System.Web.UI.UserControl
    {
        public List<ViewEventPromo> Items { get; set; }
        public string ItemId { get; set; }
        public bool IsEnablePager { get; set; }
        public int PageSize { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public override void DataBind()
        {
            rpt_Item.DataSource = Items;
            rpt_Item.DataBind();
        }
    }
}
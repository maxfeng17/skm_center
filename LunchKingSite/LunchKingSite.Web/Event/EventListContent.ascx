﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventListContent.ascx.cs"
    Inherits="LunchKingSite.Web.Event.EventListContent" %>

    <link href="/Themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="/Themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="/Themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    <link href="/Themes/PCweb/plugin/font-awesome/font-awesome.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="/Themes/mobile/css/swiper.css">
    <link rel="stylesheet" href="/Themes/mobile/css/style.css"  type="text/css" />
    <script type="text/javascript" src="/Tools/js/jquery.nav.js"></script>
    <script type="text/javascript" src="/Tools/js/jquery.lazyload.min.js"></script>
    
    <script type='text/javascript'>
        $(document).ready(function () {
            var swiper = new Swiper('.swiper-container', {
                pagination: '.swiper-pagination',
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev',
                slidesPerView: 1,//1
                paginationClickable: true,
                spaceBetween: 10,//10
                loop: false,
                autoplay: 3500,
                autoplayDisableOnInteraction: false
            });

            $("img.lazy").lazyload({ placeholder: "https://www.17life.com/Themes/PCweb/images/ppon-M1_pic.jpg", threshold: 200, effect: "fadeIn" });
            
            $('.nav-anchor').onePageNav({
                currentClass: 'current',
                changeHash: false,
                scrollSpeed: 750,
                scrollThreshold: 0.5,
                filter: ':not(.external)',
                easing: 'swing',
                begin: function () {
                    //I get fired when the animation is starting
                },
                end: function () {
                    //I get fired when the animation is ending
                },
                scrollChange: function ($currentListItem) {
                    //I get fired when you enter a section and I pass the list item of the section
                },
            });

            //調整浮動選單TOP位置
            $(function () {
                $('a[href*="#"]:not([href="#"])').click(function () {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if (target.length) {
                            $('html, body').animate({
                                scrollTop: target.offset().top - 100
                            }, 1000);
                            return false;
                        }
                    }
                });
            });

            ////浮動選單
            $('#btnBackTop').remove();
            $(window).scroll(function () {
                if ($(document).scrollTop() >= 200) {
                    $('.header_wrapper_fixed').show();
                } else {
                    $('.header_wrapper_fixed').hide();
                }
            });
        });
    </script>

<style>
    .content_exhibition_active_list p.horizontal_line{
        background: url('<%= MobileCategoryImage %>'); /*分類底圖*/
        background-repeat:no-repeat; /*不要動*/
        background-size:contain;     /*不要動*/
        background-position:center;  /*不要動*/
    }
</style>

    <asp:Literal ID="litChangeCss" runat="server" />
    <div id="divPageTop" class="content_exhibition_active_list" runat="server" visible="false">
        
        <div class="header_wrapper_fixed" style="display:none;">
            <div class="top_wrap">
                <ul class="nav-anchor">
                    <li id="liNavToadayHot" runat="server"><a href="#mobile_index_0"><asp:Literal ID="litNavToadayHot" runat="server" /></a></li>
                    <asp:Repeater ID="rpNav" runat="server" OnItemDataBound="rpNav_ItemDataBound">
                        <ItemTemplate>
                            <li><a id="linkNav" runat="server" /></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
        </div>
        <!--// 策展列表主視覺bn //-->
        <div class="main_bn">
            <asp:Literal ID="litFbShare" runat="server" />
            <a href="<%= RuleUrl %>">
                <asp:Image CssClass="lazy" ID="imgMain" runat="server" />
            </a>
        </div>
        <!--// 策展列表主視覺bn //end-->

        <!--//  今日輪播區域 //-->
        <div class="bn_today_wrap" id="divHot" runat="server">
            <p id="mobile_index_0" class="horizontal_line"><i class="fa fa-bullhorn fa-fw"></i><asp:Literal ID="litHotBannerTitle" runat="server" /></p>
            <!--// 今日輪播banner// -->
            <div class="banner">
                <!-- 如果需要导航按钮 -->
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>

                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <asp:Repeater ID="rpTodayBanner" runat="server" OnItemDataBound="rpTodayBanner_ItemDataBound">
                            <ItemTemplate>
                                <div class="swiper-slide">
                                    <a ID="linkToday" runat="server"><asp:Image CssClass="lazy" ID="imgToday" runat="server" /></a>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <!-- 如果需要分页器 -->
                    <div class="swiper-pagination"></div><!---->
	                <!-- 如果需要滚动条
	                <div class="swiper-scrollbar"></div>
	                -->
                </div>
            </div>
            <!--// banner//end-->
        </div>
        <!--// 今日輪播區域 //end-->

        <!--// maincontent 主要內容//-->
        <div class="maincontent banner_list">
            <asp:Repeater ID="rpBannerList" runat="server" OnItemDataBound="rpBannerList_ItemDataBound">
                <ItemTemplate>
                    <!-- // 分類 //-->
                    <p id="mobile_index_<%#Container.ItemIndex + 1 %>" class="horizontal_line"><asp:Literal ID="litDay" runat="server" /></p>
                    <ul>
                        <asp:Repeater ID="rpSubList" runat="server" OnItemDataBound="rpSubList_ItemDataBound">
                            <ItemTemplate>
                                <li>
                                    <div id="divBlock" runat="server" class="item_block">
                                        <img src="https://www.17life.com/images/17P/active/20161201_exhibition/item_block.png" />
                                    </div>
                                    <a ID="linkBanner" runat="server"><asp:Image CssClass="lazy" ID="imgBanner" runat="server" /></a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    <!-- // 分類 //end-->
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <!--// maincontent 主要內容//end-->

        <!--// content 內容區塊//end-->
    </div>

    <div id="divEventEnd" class="content_exhibition_list" runat="server" visible="false">
        <div class="main_bn">
            <div class="ly-e-cop-ppon">本活動已經結束囉</div>
        </div>
    </div>
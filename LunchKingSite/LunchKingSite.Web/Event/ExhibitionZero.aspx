﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="ExhibitionZero.aspx.cs" Inherits="LunchKingSite.Web.Event.ExhibitionZero" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<asp:Content ID="cDefaultMeta" ContentPlaceHolderID="cphPponMeta" runat="server">
    <meta property="fb:app_id" content="<%=FB_AppId%>">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<%=OgTitle%>" />
    <meta property="og:url" content="<%=OgUrl%>" />
    <meta property="og:site_name" content="17Life" />
    <meta property="og:description" content="<%=OgDescription%>" />
    <meta property="og:image" content="<%=OgImage%>" />
    <link rel="image_src" href="<%=LinkImageSrc%>" />
    <link rel="canonical" href="<%=LinkCanonicalUrl%>" />    
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href="../Themes/default/images/17Life/G2/A3-event_coupons.css" rel="stylesheet" type="text/css" />    
    <link href="../Themes/default/style/RDL.css" rel="stylesheet" type="text/css" />
    <script type='text/javascript'>
        function resizefun() {
            var x = $(window).width();
            if (x <= 960) {
                var myUrl = window.location.toString().toLowerCase();
                location.href = myUrl.replace("exhibitionzero.aspx", "exhibitionMobile.aspx");
            }
        }
        var delayFunction = function (mainFunc, option) {
            var delayTimer;
            option = option || {};
            if (!option.delay) {
                return mainFunc;
            } else {
                return function () {
                    var that = this;
                    var args = arguments;
                    if (delayTimer) clearTimeout(delayTimer);
                    delayTimer = setTimeout(function () {
                        mainFunc.apply(that, args);
                    }, option.delay);
                };
            }
        };
        $(document).ready(function () {
            if ("<%=EnableMobileEventPromo%>".toLowerCase() == "true") {
                resizefun();
                $(window).resize(delayFunction(function () { resizefun(); }, { delay: 300 }));
            }
            var urlParams = {};
            (function () {
                var e,
              a = /\+/g,  // Regex for replacing addition symbol with a space
              r = /([^&=]+)=?([^&]*)/g,
              d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
              q = window.location.search.substring(1);
                while (e = r.exec(q)) {
                    urlParams[d(e[1])] = d(e[2]);
                }
            })();
            initCss();
            changecategory(0);
        });

        function initCss() {
            $('#wrap').attr("style", "<%=BgCss %>");
            if ('<%=BtnFontColor %>' != '') {
                $('.st-e-cop-optionsbn').css('color', '<%=BtnFontColor %>');
            }
        }

        function changecategory(id) {
            $('.cate').hide();
            $('#cate' + id).show();
            if ('<%=BtnHover %>' != '' && '<%=BtnOriginal %>' != '') {
                $('.st-e-cop-optionsbn').css('background-image', '<%=BtnOriginal %>');
                $('.st-e-cop-optionsbn').hover(
                function () { $(this).css('background-image', '<%=BtnHover %>'); }, function () { $(this).css('background-image', '<%=BtnOriginal %>'); }
                );
            } else {
                $('.st-e-cop-optionsbn').removeClass("st-e-cop-optionsbn-on");
                $('.st-e-cop-optionsbn').hover(
                   function () { $(this).addClass("st-e-cop-optionsbn:hover"); }, function () { $(this).removeClass("st-e-cop-optionsbn:hover"); }
                );
            }

            if ('<%=BtnActive %>' != '') {
                $('#c' + id).find('.st-e-cop-optionsbn').unbind().css('background-image', '<%=BtnActive %>');
            } else {
                $('#c' + id).find('.st-e-cop-optionsbn').unbind().removeClass("st-e-cop-optionsbn:hover").addClass("st-e-cop-optionsbn-on");
            }

        }
    </script>
    <style type="text/css">
        .center
        {
            margin-top: 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <script type="text/javascript">
        Sys.Application.add_load(initCss);
    </script>

                <div id="TOPBanner">
                    <asp:Literal ID="liMainPic" runat="server"></asp:Literal>
                </div>
                <asp:Panel ID="pan_Category" runat="server" CssClass="ly-e-cop-area-menubar">
                    <div class="evn-cop-tn-bar clearfix">
                        <asp:Repeater ID="rpt_Category" runat="server">
                            <ItemTemplate>
                                <div class="st-e-cop-area-bar" data-cat='<%# RegexReplaceSequence(string.IsNullOrEmpty(Container.DataItem.ToString())?"全部":Container.DataItem.ToString())%>' onclick='changecategory(<%# Container.ItemIndex %>);' id='<%# "c"+Container.ItemIndex %>'>
                                    <a  href="#" class="st-e-cop-optionsbn" onclick="return false;"><%# RegexReplaceSequence(string.IsNullOrEmpty(Container.DataItem.ToString())?"全部":Container.DataItem.ToString())%></a>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </asp:Panel>
            
            <asp:Panel ID="pan_Main" runat="server">
                    <asp:Panel ID="pan_Items" runat="server">
                    </asp:Panel>
                <asp:Panel ID="pan_ActivityMeasures" runat="server"  CssClass="ly-evn-pprule"  ClientIDMode="Static">
                    <div class="evn-rule-title">
                        活動辦法
                    </div>
                    <div class="evn-rule-content">
                        <asp:Literal ID="lit_Description" runat="server"></asp:Literal>
                    </div>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pan_Sub" runat="server" Visible="false">
                <div class="PPClose">
                    <div class="PPSpace">
                    </div>
                    <a href="https://www.17life.com" class="BackBtn"></a>
                </div>
            </asp:Panel>

</asp:Content>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExhibitionItemZero.ascx.cs"
    Inherits="LunchKingSite.Web.Event.ExhibitionItemZero" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="System.Collections.Concurrent" %>
<div id="<%=ItemId %>" style="display: none;" class="cate">
    <asp:Repeater ID="rpt_Item" OnItemDataBound="rpt_Item_ItemDataBound" runat="server">
        <ItemTemplate>
            <asp:Panel ID="pan_EnableLink" runat="server" CssClass="evn-cop-buy-sf-box e_shadow" Visible="<%# ( ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS<=DateTime.Now) %>">
                <a href="<%#"../" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid + (string.IsNullOrEmpty(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa) ? string.Empty : ("/" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa))%>"
                    target="_blank">
                        <div class="evn-cop-buy-pic">
                            <img src="<%# ImageFacade.GetMediaPathsFromRawData(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.ImagePath, MediaType.PponDealPhoto).DefaultIfEmpty("../Themes/default/images/17Life/G2/loading.gif").First()%>" alt="<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Title%>" width="290" height="162" />
                            <div class="activeLogo alo-m"><asp:Literal ID="lit_DealPromoImage" runat="server"></asp:Literal></div>
                        </div>
                        <div class="evn-cop-buytitle">
                            <a href="<%#"../" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid + (string.IsNullOrEmpty(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa) ? string.Empty : ("/" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa))%>" target="_blank">
                                <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Title%>
                            </a>
                        </div>
                        <div class="evn-cop-minorange-title" style='<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS <= DateTime.Now ? " display:block": " display:none"%>'>
                            <a href="<%#"../" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid + (string.IsNullOrEmpty(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa) ? string.Empty : ("/" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa))%>" target="_blank">
                                <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Description%>
                            </a>
                        </div>
                        <div class="evn-cop-buyinformation" style='<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS <= DateTime.Now ? " display:block": " display:none"%>'>
                            <div class="evn-cop-buytotle"><%# GetOrderedTotal(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid) %> <span class="evn-cop-zeroget">人已參加</span></div>
                            <a <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Value ? string.Empty : "href=\"../" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid + (string.IsNullOrEmpty(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa) ? string.Empty : ("/" + ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Cpa)) + "\" target='_blank'"%>>
                                <div class="e_rounded <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Value ? " st-e-cop-disablebtn" : " st-e-cop-buybtn"%>"><%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Value ? "已截止" : "參加" %></div>
                            </a>
                        </div>
                        <div class="evn-cop-donationinfo e_textFamily" style='<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS > DateTime.Now ? " display:block": " display:none"%>'>
                            <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS.ToString("MM/dd")%>&nbsp;開始敬請期待！
                        </div>
                </a>
            </asp:Panel>
            <asp:Panel ID="pan_DisableLink" runat="server" CssClass="evn-cop-buy-sf-box e_shadow" Visible="<%# ( ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS>DateTime.Now) %>">
                    <div class="evn-cop-buy-pic">
                        <img src="<%# ImageFacade.GetMediaPathsFromRawData(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.ImagePath, MediaType.PponDealPhoto).DefaultIfEmpty("../Themes/default/images/17Life/G2/loading.gif").First()%>" width="290" height="162" />                        
                        <div class="activeLogo alo-m"><asp:Literal ID="lit_DealPromoImageDisableLink" runat="server"></asp:Literal></div>
                    </div>
                    <div class="evn-cop-buytitle">
                        <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Title%>
                    </div>
                    <div class="evn-cop-minorange-title" style='<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS <= DateTime.Now ? " display:block": " display:none"%>'>
                        <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.Description%>
                    </div>
                    <div class="evn-cop-buyinformation" style='<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS <= DateTime.Now ? " display:block": " display:none"%>'>
                        <div class="evn-cop-buytotle"><%# GetOrderedTotal(((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourGuid) %> <span class="evn-cop-zeroget">人已參加</span></div>
                        <div class="st-e-cop-buybtn e_rounded ">參加</div>
                    </div>
                    <div class="evn-cop-donationinfo e_textFamily" style='<%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS > DateTime.Now ? " display:block": " display:none"%>'>
                        <%# ((KeyValuePair<ViewEventPromo, bool>)(Container.DataItem)).Key.BusinessHourOrderTimeS.ToString("MM/dd")%>&nbsp;開始敬請期待！
                    </div>
            </asp:Panel>
        </ItemTemplate>
    </asp:Repeater>
    <div class="clear">
    </div>
</div>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;
using System.Text.RegularExpressions;
using System.Web.Services;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using System.Web.Security;

namespace LunchKingSite.Web.Event
{
    public partial class SkmExhibitionCommercial : BasePage, IExhibition
    {
        #region property

        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private ExhibitionPresenter _presenter;
        public ExhibitionPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public string Url
        {
            get
            {
                return string.IsNullOrEmpty(Request.QueryString["u"]) ? string.Empty : Request.QueryString["u"];
            }
        }
        public string Preview
        {
            get
            {
                return string.IsNullOrEmpty(Request.QueryString["p"]) ? string.Empty : Request.QueryString["p"];
            }
        }
        public string BgCss
        {
            get;
            set;
        }
        public string MainPic
        {
            set { liMainPic.Text = HttpUtility.HtmlDecode(value); }
        }
        public string BtnActive
        {
            get;
            set;
        }
        public string BtnOriginal
        {
            get;
            set;
        }
        public string BtnHover
        {
            get;
            set;
        }
        public string BtnFontColor
        {
            get;
            set;
        }
        public int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(Page.User.Identity.Name);
            }
        }
        public int EventId
        {
            get;
            set;
        }
        public string Rsrc
        {
            get;
            set;
        }
        public string EventTitle
        {
            get;
            set;
        }

        public int VourcherCount
        {
            get;
            set;
        }

        //活動合作店家-優惠劵數
        public int LottoryVourcherCount
        {
            get;
            set;
        }

        //新光三越合作店家-優惠劵數
        public int SkmStoreVourcherCount
        {
            get;
            set;
        }

        public string SiteUrl
        {
            get { return config.SiteUrl; }
        }

        #region SEO Keyword

        public string SeoKeyword
        {
            get;
            set;
        }
        public string SeoDescription
        {
            get;
            set;
        }
        #endregion

        #region The Open Graph protocol

        public string FB_AppId
        {
            get
            {
                return ProviderFactory.Instance().GetConfig().FacebookApplicationId;
            }
        }
        /// <summary>
        /// meta og:title content
        /// </summary>
        public string OgTitle { set; get; }
        /// <summary>
        /// meta og:url content
        /// </summary>
        public string OgUrl { set; get; }
        /// <summary>
        /// meta og:description content
        /// </summary>
        public string OgDescription { set; get; }
        /// <summary>
        /// meta og:image content
        /// </summary>
        public string OgImage { set; get; }
        /// <summary>
        /// link rel=canonical href
        /// </summary>
        public string LinkCanonicalUrl { set; get; }
        /// <summary>
        /// link rel=image_src href
        /// </summary>
        public string LinkImageSrc { set; get; }
        /// <summary>
        /// 團購版型
        /// </summary>
        public EventPromoTemplateType TemplateType { set; get; }
        #endregion

        /// <summary>
        /// 啟用行動版商品主題活動頁
        /// </summary>
        public bool EnableMobileEventPromo { set; get; }

        /// <summary>
        /// 是否為策展活動
        /// </summary>
        public bool IsCuration { set; get; }
        #endregion

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["u"]))
            {
                piinlifeDeal.ContentName = Request.QueryString["u"];
                ViewState["c"] = Request.QueryString["u"];
            }

            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
            }
            this.Presenter.OnViewLoaded();
            Page.MetaDescription = string.IsNullOrEmpty(SeoDescription) ? Page.MetaDescription : SeoDescription;
            Page.MetaKeywords = string.IsNullOrEmpty(SeoKeyword) ? Page.MetaKeywords : SeoKeyword;
        }

        #region local method

        protected string RegexReplaceSequence(string category)
        {
            return Regex.Replace(category, @"^[0-9]*\.", string.Empty);
        }

        protected void rptVourcher_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var vourcher = ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(e.Item.DataItem));


                Image img = (Image)e.Item.FindControl("VourcherImage");
                img.ImageUrl = ImageFacade.GetMediaPathsFromRawData(vourcher.Value.Key.PicUrl, MediaType.PponDealPhoto).DefaultIfEmpty("").First();

                if (img.ImageUrl == "")
                {
                    Seller seller = sp.SellerGet(vourcher.Value.Key.SellerGuid);
                    img.ImageUrl = ImageFacade.GetMediaPathsFromRawData(seller.SellerLogoimgPath, MediaType.SellerPhotoLarge).DefaultIfEmpty("").First();

                    if (img.ImageUrl == "")
                    {
                        img.ImageUrl = "https://www.17life.com/images/17P/20130914-Shin/skm-Coupon.jpg";
                    }
                }
            }
        }

        #endregion local method

        #endregion page

        #region method

        public void SetCategory(EventPromoItemCollection list)
        {
            var group_category = list.GroupBy(x => x.ItemType).OrderBy(x => x.Key);

            if (group_category.Count() == 1)
                pan_Category.Visible = false;
            else
            {
                rpt_Category.DataSource = group_category.ToDictionary(x => x.Key, y => Helper.GetLocalizedEnum((EventPromoItemType)y.Key));
                rpt_Category.DataBind();
            }
        }
        public void SetAllCategory(EventPromo eventPromo, List<ViewEventPromo> dataList)
        { }
        public void ShowEventPromo(EventPromo eventPromo, List<ViewEventPromo> list)
        {
            pan_Main.Visible = pan_Sub.Visible = false;
            DateTime now = DateTime.Now;
            string bgstyle = "background: no-repeat local center 0 ";
            if (list.Count > 0)
            {
                var first = list.First();
                this.Page.Title = first.EventTitle;
                if (!string.IsNullOrEmpty(first.BgPic))
                    bgstyle += "url(" + list.First().BgPic + ") ";
                if (!string.IsNullOrEmpty(first.BgColor))
                    bgstyle += "#" + first.BgColor;
                BgCss = bgstyle;
                MainPic = first.MainPic;

                if (!string.IsNullOrEmpty(first.BtnFontColor))
                    BtnFontColor = "#" + first.BtnFontColor;
                if (!string.IsNullOrEmpty(first.BtnActive))
                    BtnActive = "url(" + first.BtnActive + ")";
                if (!string.IsNullOrEmpty(first.BtnHover))
                    BtnHover = "url(" + first.BtnHover + ")";
                if (!string.IsNullOrEmpty(first.BtnOriginal))
                    BtnOriginal = "url(" + first.BtnOriginal + ")";

                if (string.IsNullOrEmpty(first.EventDescription) || first.EventDescription == "\r\n")
                {
                    pan_ActivityMeasures.Visible = false;

                }
                else
                {
                    pan_ActivityMeasures.Visible = true;
                    lit_Description.Text = first.EventDescription;
                }

                if ((first.EventStatus && first.StartDate <= now && first.EndDate >= now && first.EventStatus) || Preview == "show_me_the_preview")
                {
                    ExhibitionNewItem items = (ExhibitionNewItem)Page.LoadControl("ExhibitionNewItem.ascx");
                    items.ItemId = "cate" + (int)EventPromoItemType.Ppon;
                    items.Items = list.ToList();
                    items.DataBind();
                    pan_Items.Controls.Add(items);

                    pan_Main.Visible = true;
                }
                else
                {
                    ShowEventExpire();
                }
            }
        }

        public void ShowVourcherEventPromo(Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> dataList, Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> lotteryList, Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> storeList)
        {
            if (dataList.Count > 0)
            {
                VourcherCount = dataList.Count;
                rptVourcher.DataSource = dataList.OrderBy(x => x.Key.Seq);
                rptVourcher.DataBind();
                divVourcherMain.Visible = true;
            }
            if (lotteryList.Count > 0)
            {
                LottoryVourcherCount = lotteryList.Count;
                rptVourcherLottery.DataSource = lotteryList.OrderBy(x => x.Key.Seq);
                rptVourcherLottery.DataBind();
                divVourcherLottery.Visible = true;
            }
            if (storeList.Count > 0)
            {
                SkmStoreVourcherCount = storeList.Count;
                rptVourcherStore.DataSource = storeList.OrderBy(x => x.Key.Seq);
                rptVourcherStore.DataBind();
                divVourcherStore.Visible = true;
            }
            divVourcher.Visible = dataList.Count > 0 || lotteryList.Count > 0 || storeList.Count > 0;
        }
        public void ShowPiinlifeEventPromo(List<ViewEventPromo> dataList)
        { }
        public void ShowPiinlifeEventPromo(EventPromo eventPromo, List<ViewEventPromo> dataList)
        { }
        public void ShowEventExpire()
        {
            pan_Sub.Visible = true;
        }

        #endregion

        [WebMethod]
        public static void SetVourcherCollect(int eventId)
        {
            if (string.IsNullOrWhiteSpace(HttpContext.Current.User.Identity.Name))
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }
            int userId = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
            VourcherFacade.SetVourcherCollect(userId, eventId, VourcherCollectStatus.Initial);
        }
    }
}
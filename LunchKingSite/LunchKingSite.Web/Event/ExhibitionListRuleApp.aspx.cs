﻿using System;
using System.Web.UI;

namespace LunchKingSite.Web.Event
{
    public partial class ExhibitionListRuleApp : System.Web.UI.Page
    {
        public int ThemeMainId
        {
            get
            {
                int themeMainId;
                return int.TryParse(Request["id"] ?? string.Empty, out themeMainId) ? themeMainId : 1;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            EventListRule rule = (EventListRule)Page.LoadControl("EventListRule.ascx");
            rule.backUrl = "ExhibitionListApp.aspx?id=" + ThemeMainId;
            rule.themeMainId = ThemeMainId;
            rule.DataBind();
            pan_rule.Controls.Add(rule);

            this.Page.Title = rule.title;
        }
    }
}
﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Event
{
    public partial class DiscountUserExchange : BasePage, IDiscountUserExchangeView
    {
        #region props
        private DiscountUserExchangePresenter _presenter;
        public DiscountUserExchangePresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get
            {
                return Page.User.Identity.Name; 
            }
        }

        public string EventCode
        {
            get
            {
                return txtEventCode.Text.Trim();
            }
        }

        public string Code
        {
            get
            {
                return txtCode.Text.Trim();
            }
        }
        #endregion

        #region event
        public event EventHandler Send;
        #endregion

        #region mothod
        public void ShowMessage(string message)
        {
            string errorClass = " error";
            divMessage.Style.Add("display", "block");
            divEventCode.CssClass += errorClass;
            liMessage.Text = message;
        }

        public void RedirectDiscountList()
        {
            Response.Redirect(ResolveUrl("~/User/DiscountList.aspx"));
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!Page.User.Identity.IsAuthenticated)
                {
                    Response.Redirect(WebUtility.GetLoginPath());
                    return;
                }
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            if (this.Send != null)
            {
                this.Send(this, e);
            }
        }
        #endregion
    }
}
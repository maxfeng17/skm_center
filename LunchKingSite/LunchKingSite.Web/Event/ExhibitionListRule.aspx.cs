﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using System;
using System.Web;

namespace LunchKingSite.Web.Event
{
    public partial class ExhibitionListRule : BasePage
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        public string Share_FB_Url
        {
            get;
            set;
        }


        #region The Open Graph protocol
        public string FB_AppId
        {
            get
            {
                return ProviderFactory.Instance().GetConfig().FacebookApplicationId;
            }
        }
        /// <summary>
        /// meta og:title content
        /// </summary>
        public string OgTitle { set; get; }
        /// <summary>
        /// meta og:image content
        /// </summary>
        public string OgImage { set; get; }
        /// <summary>
        /// meta og:url content
        /// </summary>
        public string OgUrl { set; get; }
        /// <summary>
        /// meta og:description content
        /// </summary>
        public string OgDescription { set; get; }
        /// <summary>
        /// link rel=canonical href
        /// </summary>
        public string LinkCanonicalUrl { set; get; }
        /// <summary>
        /// link rel=image_src href
        /// </summary>
        public string LinkImageSrc { set; get; }
        #endregion

        private bool IsMobileBroswer
        {
            get
            {
                var userAgent = HttpContext.Current.Request.UserAgent;
                if ((!string.IsNullOrEmpty(userAgent)) &&
                    ((userAgent.ToLower().Contains("ipad")
                        || (userAgent.ToLower().Contains("iphone")
                        || (userAgent.ToLower().Contains("ipod")
                        || (userAgent.ToLower().Contains("android")
                        || (userAgent.ToLower().Contains("Blackberry"))))))))
                {
                    return true;
                }
                return false;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (IsMobileBroswer)
            {
                Response.Redirect("ExhibitionListRuleMobile.aspx" + Request.Url.Query);
            }
        }

        public int ThemeMainId
        {
            get
            {
                int themeMainId;
                return int.TryParse(Request["id"] ?? string.Empty, out themeMainId) ? themeMainId : 1;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var theme = pp.GetThemeCurationMainById(ThemeMainId);
                litHtml.Text = theme.WebRuleContent.Replace("@mainid", ThemeMainId.ToString());
                this.Page.Title = string.Format("{0}-活動說明", theme.Title);
                OgTitle = string.Format("{0}-活動說明", theme.FbShareTitle);
                LinkImageSrc = OgImage = ImageFacade.GetMediaPath(theme.FbShareIamge, MediaType.ThemeImage); //FB分享圖
                OgUrl = LinkCanonicalUrl = Request.Url.AbsoluteUri.Replace("&p=show_me_the_preview", "");
            }
        }
    }
}
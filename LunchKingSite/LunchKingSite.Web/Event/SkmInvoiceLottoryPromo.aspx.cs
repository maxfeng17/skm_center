﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;


namespace LunchKingSite.Web.Event
{
    public partial class SkmInvoiceLottoryPromo : BasePage, ISkmInvoiceLottoryPromoView
    {

        #region property

        public string UserName
        {
            get { return User.Identity.Name; }
        }

        public int UserId
        {
            get { return MemberFacade.GetUniqueId(User.Identity.Name); }
        }
        public string SiteUrl
        {
            get { return ProviderFactory.Instance().GetConfig().SiteUrl; }
        }

        public string UserInfo
        {
            get
            {
                return (Session["UserInfo"] == null) ? "" : Session["UserInfo"].ToString();
            }
            set
            {
                Session["UserInfo"] = value;
            }
        }

        public bool IsPromoLogin
        {
            get
            {
                return (Session["IsPromoLogin"] != null) ? ((Session["IsPromoLogin"].ToString() == "1") ? true : false) : false;

            }
            set
            {
                Session["IsPromoLogin"] = value;
            }


        }


        private SkmInvoiceLottoryPromoPresenter _presenter;
        public SkmInvoiceLottoryPromoPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        #endregion
        #region event
        public event EventHandler<DataEventArgs<SkmLottoryInvoice>> Exchange = null;
        #endregion
        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["IsPromoLogin"] != null)
            {
                Session["IsPromoLogin"] = Request["IsPromoLogin"];
            }
            else
            {
                Session["IsPromoLogin"] = null;
            }

            if (!Page.IsPostBack)
            {
                _presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }


        protected void lbShowEvent_Click(object sender, EventArgs e)
        {
            ShowExchange();
        }

        protected void lbExchange_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(UserName))
            {
                divLogin.Visible = true;
                divExchange.Visible = false;
            }
            else
            {
                if (this.Exchange != null)
                {
                    var data = new SkmLottoryInvoice()
                    {
                        CardNumber = tbTaishinCreditCard.Text.Trim(),
                        StoreName = tbStoreName.Text.Trim(),
                        InvoiceNumber = tbInvoiceInfo.Text.Trim(),
                        LoginUserName = tbLottoryUserName.Text.Trim(),
                        LoginUserAddress = tbLottoryUserAddr.Text.Trim(),
                        LoginUserMobile = tbLottoryUserTel.Text.Trim()
                    };

                    this.Exchange(this, new DataEventArgs<SkmLottoryInvoice>(data));
                }


            }
        }

        #endregion
        #region method
        public void ShowNoEvent()
        {
            divNoEvent.Visible = true;
            divEvent.Visible = false;
            divChoseEvent.Visible = false;
        }

        public void ShowChoseEvent()
        {
            divChoseEvent.Visible = true;
            divLogin.Visible = false;
            divExchange.Visible = false;

        }

        public void ShowExchange()
        {
            divChoseEvent.Visible = false;
            divEvent.Visible = true;
            if (string.IsNullOrWhiteSpace(UserName))
            {
                divLogin.Visible = true;
                divExchange.Visible = false;
            }
            else
            {
                divLogin.Visible = false;
                divExchange.Visible = true;

                //預帶聯絡人資訊
                string[] badd = string.IsNullOrWhiteSpace(UserInfo) ? new string[] { } : UserInfo.Split('|');

                if (badd.Length > 0)
                {
                    tbLottoryUserName.Text = UserName;
                    tbLottoryUserName.Text = badd[0] + badd[1];
                    tbLottoryUserTel.Text = badd[2];
                    tbLottoryUserAddr.Text = badd[6];
                }

            }
        }

        public void ShowSuccess()
        {
            divSuccess.Visible = true;
            divInitial.Visible = false;
            divChoseEvent.Visible = false;
        }
        #endregion
        #region private method

        #endregion

        #region WebService

        [WebMethod]
        public static string CheckTaishinCreditCard(string creditCardNumber)
        {
            Regex regCreditCardNumber = new Regex(@"^\d{8}$", RegexOptions.Compiled);
            if (regCreditCardNumber.IsMatch(creditCardNumber))
            {
                CreditCardPremiumCollection taishinCardRules = CreditCardPremiumManager.GetTaishinCreditCardsNumberRules();

                var item = taishinCardRules.Where(t => creditCardNumber.Length >= t.CardNum.Length &&
                          t.CardNum == creditCardNumber.Substring(0, t.CardNum.Length)).FirstOrDefault();

                return (item == null) ? "抱歉，您所使用的非台新銀行信用卡" : "";
            }
            else
            {
                return "抱歉您的卡號有誤，請重新輸入";
            }

        }

        [WebMethod]
        public static string CheckInvoice(string invoiceNumber)
        {
            Regex regInvoice = new Regex(@"^[a-zA-Z][a-zA-Z]\d{8}$", RegexOptions.Compiled);
            if (regInvoice.IsMatch(invoiceNumber))
            {

                var skmInvoices =
                    PromotionFacade.GetSkmEventPromoInvoiceContentCollection()
                                   .Where(x => x.InvoiceNumber.ToLower() == invoiceNumber.ToLower());
                return skmInvoices.Any() ? "抱歉，此發票號碼已登錄過" : "";
            }
            else
            {
                return "抱歉您的發票號碼有誤，請重新輸入";
            }
        }

        #endregion WebService
    }
}
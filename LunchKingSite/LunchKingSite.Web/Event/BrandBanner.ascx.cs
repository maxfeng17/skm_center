﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Text;

namespace LunchKingSite.Web.Event
{
    public partial class BrandBanner : UserControl
    {
        private ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public Brand brand { get; set; }
        protected List<IViewPponDeal> hotDeals = new List<IViewPponDeal>();
        protected ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        protected void Page_Load(object sender, EventArgs e)
        {
            SetBanner();
            SetHotItem();
        }
        private void SetBanner()
        {
            bool IsShow = false;
            StringBuilder sb = new StringBuilder();
            string[] bidArr = { brand.Mk1ActName, brand.Mk2ActName, brand.Mk3ActName, brand.Mk4ActName, brand.Mk5ActName};
            for (int i = 0; i < 5; i++)
            {
                Guid tmpGuid;
                if ( Guid.TryParse(bidArr[i], out tmpGuid))
                {
                    IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(tmpGuid, true);
                    var url = string.Format("{0}/{1}", cp.SiteUrl, tmpGuid.ToString());
                    var webImage = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(ResolveUrl("~/Themes/PCweb/images/ppon-M1_pic.jpg")).First() + "?" + DateTime.Now.ToString("ss");
                    IsShow = true;
                    sb.AppendFormat("<li><a href='{0}' target='_blank'><img class='lazy' src='{1}' alt='{2}' /></a></li>", url, ImageFacade.GetMediaPathsFromRawData(webImage.ToString(), MediaType.DealPromoImage).DefaultIfEmpty("../Themes/default/images/17Life/G2/PPImage/PPDPic.jpg").First() + "?" + DateTime.Now.ToString("ss"), vpd.EventTitle);
                }
                else
                {
                    if (!string.IsNullOrEmpty(bidArr[i]))
                    {
                        var url = DataBinder.Eval(brand, "Mk" + ( i + 1 ) + "Url");
                        var webImage = DataBinder.Eval(brand, "Mk" + ( i + 1 ) + "WebImage");
                        var actname = DataBinder.Eval(brand, "Mk" + ( i + 1 ) + "ActName");
                        if (webImage != null)
                        {
                            IsShow = true;
                            sb.AppendFormat("<li><a href='{0}' target='_blank'><img class='lazy' src='{1}' alt='{2}' /></a></li>", url, ImageFacade.GetMediaPathsFromRawData(webImage.ToString(), MediaType.DealPromoImage).DefaultIfEmpty("../Themes/default/images/17Life/G2/PPImage/PPDPic.jpg").First() + "?" + DateTime.Now.ToString("ss"), actname);
                        }
                    }                   
                }        
            }

            litBanner.Text = sb.ToString();
            pan_banner.Visible = IsShow;
        }

        private void SetHotItem()
        {
            if (brand != null)
            {                
                if (brand.HotItemBid1 != null)
                {
                    hotDeals.Add(ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(brand.HotItemBid1.Value, true));
                }
                if (brand.HotItemBid2 != null)
                {
                    hotDeals.Add(ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(brand.HotItemBid2.Value, true));
                }
                if (brand.HotItemBid3 != null)
                {
                    hotDeals.Add(ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(brand.HotItemBid3.Value, true));
                }
                if (brand.HotItemBid4 != null)
                {
                    hotDeals.Add(ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(brand.HotItemBid4.Value, true));
                }
            }
            else
            {
                rd_banner.Visible = false;
                nd_banner.Visible = false;
                banner_end.Visible = false;
            }
        }
    }
}
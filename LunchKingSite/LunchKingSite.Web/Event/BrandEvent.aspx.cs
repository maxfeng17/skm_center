﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using LunchKingSite.BizLogic.Component;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Services;

namespace LunchKingSite.Web.Event
{
    public partial class BrandEvent : BasePage, IBrandEvent
    {
        #region property
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        private BrandEventPresenter _presenter;

        public BrandEventPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string Url
        {
            get
            {
                if (Page.RouteData.Values["u"] != null)
                {
                    return Page.RouteData.Values["u"].ToString();
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["u"]))
                {
                    return Request.QueryString["u"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string Preview
        {
            get
            {
                return string.IsNullOrEmpty(Request.QueryString["p"]) ? string.Empty : Request.QueryString["p"];
            }
        }

        public string BgCss { get; set; }

        public bool DiscountPageShow { get; set; }
        public bool ActRuleShow { get; set; }
        public bool IsMarketingMode { get; set; }
        public string MainBackgroundPic { get; set; }
        public string BtnOriginalUrl { get; set; }

        public string BtnActiveUrl { get; set; }

        public string BtnHoverUrl { get; set; }

        public string BtnOriginalFontColor { get; set; }

        public string BtnActiveFontColor { get; set; }

        public string BtnHoverFontColor { get; set; }

        public int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(Page.User.Identity.Name);
            }
        }

        public int BrandId { get; set; }

        public string Rsrc { get; set; }

        public string BrandTitle { get; set; }

        public string Share_FB_Url { get; set; }

        public bool EnableMobileEventPromo { set; get; }

        public int CityId
        {
            get
            {
                int cid;
                if (Request.QueryString["cid"] != null && int.TryParse(Request.QueryString["cid"].ToString(), out cid))
                {
                    return cid;
                }
                else
                {
                    return PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId;
                }
            }
        }

        public bool isLogin {
            get
            {
                return Page.User.Identity.IsAuthenticated;
            }
        }

        private bool IsMobileBroswer
        {
            get
            {
                var userAgent = HttpContext.Current.Request.UserAgent;
                if ((!string.IsNullOrEmpty(userAgent)) &&
                    (
                        (userAgent.ToLower().Contains("iphone")
                        || (userAgent.ToLower().Contains("ipod")
                        || (userAgent.ToLower().Contains("android")
                        || (userAgent.ToLower().Contains("Blackberry")))))))
                {
                    return true;
                }
                return false;
            }
        }

        public string BackMainUrl
        {
            get
            {
                return litBackMainUrl.Text;
            }
            set
            {
                litBackMainUrl.Text = value;
            }
        }

        public string jsContentIds{ get; set; }
        public string nsContentIds { get; set; }
        public Uri LatestUrl
        {
            get
            {
                return Request.UrlReferrer;
            }
        }
        #endregion property

        #region The Open Graph protocol
        public string FB_AppId
        {
            get
            {
                return ProviderFactory.Instance().GetConfig().FacebookApplicationId;
            }
        }
        /// <summary>
        /// meta og:title content
        /// </summary>
        public string OgTitle { set; get; }
        /// <summary>
        /// meta og:url content
        /// </summary>
        public string OgUrl { set; get; }
        /// <summary>
        /// meta og:description content
        /// </summary>
        public string OgDescription { set; get; }
        /// <summary>
        /// meta og:image content
        /// </summary>
        public string OgImage { set; get; }
        /// <summary>
        /// meta og:image:width content
        /// </summary>
        public int OgImageWidth { set; get; }
        /// <summary>
        /// meta og:image:height content
        /// </summary>
        public int OgImageHeight { set; get; }
        /// <summary>
        /// link rel=canonical href
        /// </summary>
        public string LinkCanonicalUrl { set; get; }
        /// <summary>
        /// link rel=image_src href
        /// </summary>
        public string LinkImageSrc { set; get; }

        #endregion

        #region page
        
        protected override void OnInit(System.EventArgs e)
        {
            base.OnInit(e);
            if (IsMobileBroswer)
            {
                if (Page.RouteData.Values["u"] != null)
                {
                    Response.Redirect(string.Format("/event/brandmobile/{0}{1}", Url, Request.Url.Query));
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["u"]))
                {
                    Response.Redirect("brandmobile.aspx" + Request.Url.Query);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
            }
            this.Presenter.OnViewLoaded();
        }
        #endregion page

        public void SetMemberCollection(List<Guid> collectBids)
        {
            string jsonStr = ProviderFactory.Instance().GetSerializer()
                .Serialize(collectBids);
            hdMemberCollectDealGuidJson.Value = jsonStr;
        }

        #region method        

        public void ShowBrandEvent(Brand brand, List<ViewBrandCategory> list)
        {
            pan_Main.Visible = pan_Sub.Visible = false;
            DateTime now = DateTime.Now;
            string bgstyle = "background: center 91px no-repeat ";

            if (!string.IsNullOrEmpty(MetaDescription))
            {
                Page.MetaDescription = string.Format("{0}-{1}", MetaDescription, config.Title);
            }
            else
            {
                Page.MetaDescription = config.DefaultMetaDescription;
            }

            if (list.Count > 0)
            {
                var first = brand;
                this.Page.Title = first.BrandName;
                if (!string.IsNullOrEmpty(first.Bgpic))
                {
                    bgstyle += "url(" + first.Bgpic + ") ";
                }

                if (!string.IsNullOrEmpty(first.Bgcolor))
                {
                    bgstyle += "#" + first.Bgcolor;
                }

                BgCss = bgstyle;
                if (!string.IsNullOrEmpty(first.MainPic))
                {
                    //過渡期程式，之後需移除
                    string tempMainPic = ImageFacade.GetHtmlFirstSrc(first.MainPic);
                    if (tempMainPic == string.Empty) tempMainPic = first.MainPic;
                    imgMainPic.ImageUrl = ImageFacade.GetMediaPath(tempMainPic, MediaType.DealPromoImage);
                }
                else
                {
                    divMainEvent.Visible = false;
                }

                if (!string.IsNullOrEmpty(first.Description))
                {
                    ActRuleShow = true;
                }
                else
                {
                    ActRuleShow = false;
                }

                if (!string.IsNullOrEmpty(first.MainBackgroundPic))
                {
                    MainBackgroundPic = ImageFacade.GetMediaPath(first.MainBackgroundPic, MediaType.DealPromoImage);
                    IsMarketingMode = true;
                }
                else
                {
                    IsMarketingMode = false;
                }

                if (!string.IsNullOrEmpty(first.BtnOriginalFontColor))
                {
                    BtnOriginalFontColor = "#" + first.BtnOriginalFontColor;
                }

                if (!string.IsNullOrEmpty(first.BtnActiveFontColor))
                {
                    BtnActiveFontColor = "#" + first.BtnActiveFontColor;
                }

                if (!string.IsNullOrEmpty(first.BtnHoverFontColor))
                {
                    BtnHoverFontColor = "#" + first.BtnHoverFontColor;
                }

                if (!string.IsNullOrEmpty(first.BtnActiveUrl))
                {
                    BtnActiveUrl = "url(\"" + first.BtnActiveUrl + "\")";
                }

                if (!string.IsNullOrEmpty(first.BtnHoverUrl))
                {
                    BtnHoverUrl = "url(\"" + first.BtnHoverUrl + "\")";
                }

                if (!string.IsNullOrEmpty(first.BtnOriginalUrl))
                {
                    BtnOriginalUrl = "url(\"" + first.BtnOriginalUrl + "\")";
                }

                if (string.IsNullOrEmpty(first.Description) || first.Description == "\r\n")
                {
                    pan_ActivityMeasures.Visible = false;
                }
                else
                {
                    pan_ActivityMeasures.Visible = true;
                    lit_Description.Text = first.Description;
                }



                BrandBanner brandBanner = (BrandBanner)Page.LoadControl("BrandBanner.ascx");
                brandBanner.brand = first;
                pan_banners.Controls.Add(brandBanner);

                if ((first.Status && first.StartTime <= now && first.EndTime >= now) || Preview == "show_me_the_preview")
                {
                    BrandItem items = (BrandItem)Page.LoadControl("BrandItem.ascx");
                    items.Items = list;
                    items.MainData = first;
                    items.DataBind();
                    pan_Items.Controls.Add(items);
                    pan_Main.Visible = true;
                }
                else
                {
                    ShowEventExpire();
                }
            }
        }

        public void ShowEventExpire()
        {
            divMainEvent.Visible = divNavi.Visible = pan_banners.Visible = pan_Main.Visible = pan_ActivityMeasures.Visible = false;
            pan_Sub.Visible = true;
        }

        [WebMethod]
        public static string GetInstantDiscount(int dcid, int userid)
        {
            InstantGenerateDiscountCampaignResult result = InstantGenerateDiscountCampaignResult.Cancel;
            if (userid != 0)
            {
                string code;
                int amount, discountCodeId, minimumAmount;
                DateTime? endTime;
                result = PromotionFacade.InstantGenerateDiscountCode(dcid, userid, true, out code, out amount, out endTime, out discountCodeId, out minimumAmount);
            }
            return new JsonSerializer().Serialize((new { Success = false, Message = result }));
        }
        #endregion method
    }
}
﻿using LunchKingSite.BizLogic.Facade;
using System;
using System.Web;
using System.Web.UI;

namespace LunchKingSite.Web.Event
{
    public partial class ExhibitionListRuleMobile : Page
    {
        public int ThemeMainId
        {
            get
            {
                int themeMainId;
                return int.TryParse(Request["id"] ?? string.Empty, out themeMainId) ? themeMainId : 1;
            }
        }
        private bool IsMobileBroswer
        {
            get
            {
                var userAgent = HttpContext.Current.Request.UserAgent;
                if ((!string.IsNullOrEmpty(userAgent)) &&
                    ((userAgent.ToLower().Contains("ipad")
                        || (userAgent.ToLower().Contains("iphone")
                        || (userAgent.ToLower().Contains("ipod")
                        || (userAgent.ToLower().Contains("android")
                        || (userAgent.ToLower().Contains("Blackberry"))))))))
                {
                    return true;
                }
                return false;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!IsMobileBroswer)
            {
                Response.Redirect("ExhibitionListRule.aspx" + Request.Url.Query);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EventListRule Rule = (EventListRule)Page.LoadControl("EventListRule.ascx");
                Rule.backUrl = "exhibitionlistmobile.aspx?id=" + ThemeMainId;
                Rule.themeMainId = ThemeMainId;
                Rule.DataBind();
                pan_rule.Controls.Add(Rule);

                this.Page.Title = Rule.title;
            }
        }
    }
}
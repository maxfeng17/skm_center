﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="SkmExhibitionCommercial.aspx.cs" Inherits="LunchKingSite.Web.Event.SkmExhibitionCommercial" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph" TagPrefix="uc1" %>
<asp:Content ID="cDefaultMeta" ContentPlaceHolderID="cphPponMeta" runat="server">
    <meta property="fb:app_id" content="<%=FB_AppId%>">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<%=OgTitle%>" />
    <meta property="og:url" content="<%=OgUrl%>" />
    <meta property="og:site_name" content="17Life" />
    <meta property="og:description" content="<%=OgDescription%>" />
    <meta property="og:image" content="<%=OgImage%>" />
    <link rel="image_src" href="<%=LinkImageSrc%>" />
    <link rel="canonical" href="<%=LinkCanonicalUrl%>" />    
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../Tools/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../Tools/js/bjqs-1.3.skm.min.js"></script>
    <link href="../Tools/js/css/bjqs.css" type="text/css"
        rel="stylesheet" />
    <%--<link href="../Themes/default/style/RDL.css" rel="stylesheet" type="text/css" />--%>
    <link href="../Themes/default/images/17Life/G2/skm/skm.css" rel="stylesheet" type="text/css" />
    <%--<link href="../Themes/default/images/17Life/G2/skm/skm-RDL.css" rel="stylesheet" type="text/css" />--%>

    <script type='text/javascript'>

        function slidingMenuToggle() {
            $("#wrap").toggleClass("mbe-menu-show");
        }
        $(function () {
            $(".mbe-menu-btn").click(slidingMenuToggle);
            $(".mbe-channel").click(slidingMenuToggle);
        });

        $(document).ready(function () {
            var urlParams = {};
            (function () {
                var e,
              a = /\+/g,  // Regex for replacing addition symbol with a space
              r = /([^&=]+)=?([^&]*)/g,
              d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
              q = window.location.search.substring(1);
                while (e = r.exec(q)) {
                    urlParams[d(e[1])] = d(e[2]);
                }
            })();
            var cat = urlParams['cat'];
            var catid = urlParams['catid'];
            if (catid != undefined) {
                changecategory(catid);
            }else {
                changecategory(0);
            }
            if (cat != undefined) {
                var menuBar = $('.PPMenuBar div');
                for (var i = 0; i < menuBar.length; i++) {
                    if (menuBar.eq(i).text().trim() == cat) {
                        menuBar.eq(i).click();
                    }
                }
            }
            $('html,body').scrollTop(parseInt($('[id*=hidScroll]').val(), 10));
            if ($('#banner-slide').length == 1) {
                $('#banner-slide').bjqs({
                    'animtype': 'slide',
                    'height': 850,
                    'width': 960,
                    'nexttext': '',
                    'prevtext': '',
                    'automatic ':false
                });
            }
            if ($('#lottory-banner-slide').length == 1) {
                $('#lottory-banner-slide').bjqs({
                    'animtype': 'slide',
                    'height': 560,
                    'width': 960,
                    'nexttext': '',
                    'prevtext': '',
                    'automatic ':false
                });
            }
            if ($('#skmstore-banner-slide').length == 1) {
                $('#skmstore-banner-slide').bjqs({
                    'animtype': 'slide',
                    'height': 560,
                    'width': 960,
                    'nexttext': '',
                    'prevtext': '',
                    'automatic ':false
                });

            }
           
            $(".skm-Homedealpic").hover(
              function() {
                  $(this).find(".skm-Homedealtext").stop(true,true).addClass("st-cbg",400);
              },
              function() {
                  $(this).find(".skm-Homedealtext").stop(true,true).removeClass("st-cbg",400);
              });		

            $(".skm-cpcafarm").hover(
    function() {
        $(this).find(".skm-cpcadealname").stop(true,true).addClass("st-cbg",400);
    },
    function() {
        $(this).find(".skm-cpcadealname").stop(true,true).removeClass("st-cbg",400);
    }
);	
        });

        $(window).scroll(function () {
            $('[id*=hidScroll]').val(typeof window.pageYOffset != 'undefined' ? window.pageYOffset : document.documentElement.scrollTop);
            if ($(window).scrollTop() >= 650) {
                $('#skmOutsideAd').addClass('skm-ad-fixed');
            } else {
                $('#skmOutsideAd').removeClass('skm-ad-fixed');
            }
        });

        function changecategory(id) {
  
            if (id == 0) {
                $('.skm-qrmessage').hide();
                $('#pan_Piinlife').show();
            } else {
                $('.skm-qrmessage').show();
                $('#pan_Piinlife').hide();
            }

            $.each($('.skm-areamenu-bar .skm-bar'),function() {
                $(this).find('a').removeClass().addClass('skm-areamenu-btn');
            });

            $.each($('.skm-cpcafarm'),function() {
                //console.log($(this).find('img').attr('src'));
                if ($(this).find('img').attr('src') == "") {
                    $(this).find('img').attr('src', '../Themes/default/images/17Life/G2/skm/topic_04.png');
                }
            });


            $('#c' + id).find('a').removeClass('skm-areamenu-btn').addClass('skm-areamenu-btn-on');
            $('.cate').hide();
            $('#cate' + id).show();
        }
        
        function VourcherCollect(btn, id) {
            if ($(btn).val() == '已收藏') {
                return false;
            }
            $.ajax({
                type: "POST",
                url: "SkmExhibitionCommercial.aspx/SetVourcherCollect",
                data: '{"eventId":' + id + '}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    $(btn).removeClass().addClass('st-skmcpcacol-favoritetoo-btn').val('已收藏');
                    alert("收藏成功！您可在有效期間內依使用規則享有此優惠。\n\n請先下載 17Life App，登入即可在「我的」→「優惠券收藏」內開啟此優惠券，於消費時出示即可使用");
                },
                error: function(msg) {
                    console.log(msg);
                    if (confirm("請先登入會員")) {
                        location.href = '<%=ResolveUrl(FormsAuthentication.LoginUrl) %>';
                    }
                }
            });
        }
    </script>
    <style type="text/css">
        ol.bjqs-markers li a
        {
            width: 34px;
            height: 34px;
            margin: 5px;
            display: inline-block;
            font-size: 22px;
            line-height: 36px;
            color: #FFF;
            border-radius: 5px;
            background-color: rgba(24, 24, 24, 0.5);
        }

            ol.bjqs-markers li.active-marker a,
            ol.bjqs-markers li a:hover
            {
                background-color: rgba(24, 24, 24, 1);
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <asp:TextBox ID="hidScroll" runat="server" Style="display: none;" Text="0"></asp:TextBox>
    <div class="ly-skmFarme" style="<%=BgCss %>">
        <div class="ly-skm-inside-pagestitle">
            <asp:Literal ID="liMainPic" runat="server"></asp:Literal>
        </div>
        <asp:Literal ID="liItemType" runat="server"></asp:Literal>
        <asp:Panel ID="pan_Category" runat="server" CssClass="skm-areamenu-bar">
            <div class="skm-bar clearfix">
                <asp:Repeater ID="rpt_Category" runat="server">
                    <ItemTemplate>
                        <div class="skm-tnfarm" data-cat='<%# ((KeyValuePair<int, string>)Container.DataItem).Value %>'
                            onclick='changecategory(<%# ((KeyValuePair<int, string>)Container.DataItem).Key %>);'
                            id='<%# "c"+((KeyValuePair<int, string>)Container.DataItem).Key %>'>
                            <a target="_blank" href="#" class="skm-areamenu-btn" onclick="return false;"><%# ((KeyValuePair<int, string>)Container.DataItem).Value %></a>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </asp:Panel>
        <div class="skm-qrmessagefarm">
            <div class="skm-qrmessage">
                <div class="skm-qrmes-text">免費下載App，隨時隨地抓住好康 ─</div>
                <br />
                <div class="skm-qrmes-smltext">消費刷台新銀行信用卡付款，至17Life登錄資料即獲得抽獎資格！</div>
                <a href="<%=SiteUrl%>/ppon/promo.aspx?cn=app">
                    <div class="skm-qr"></div>
                </a>
            </div>
        </div>
        <asp:Panel ID="pan_Main" runat="server">
            <asp:Panel ID="pan_Items" runat="server" CssClass="ly-skmarea-deal">
            </asp:Panel>
            <asp:Panel ID="pan_Piinlife" runat="server" CssClass="ly-skmarea-deal" ClientIDMode="Static">
                <uc1:Paragraph ID="piinlifeDeal" ExternalCache="true" runat="server" />
            </asp:Panel>
            <asp:Panel ID="divVourcher" runat="server" Visible="false">
                <div id="cate<%= (int)EventPromoItemType.Vourcher %>" class="cate">
                    <asp:Panel ID="divVourcherMain" runat="server" Visible="false" CssClass="ly-skmMgdeal clearfix">
                        <%= VourcherCount > 12 ? "<div id='banner-slide'><ul class='bjqs'><li>" : string.Empty %>
                        <asp:Repeater ID="rptVourcher" runat="server" OnItemDataBound="rptVourcher_ItemDataBound">
                            <ItemTemplate>
                                <div class="skm-cpcafarm clearfix">
                                    <a href="#" class="" onclick="return false;">
                                        <asp:Image ID="VourcherImage" runat="server" />
                                    </a>
                                    <div class="skm-cpcadealname">
                                        <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Title%>
                                    </div>
                                    <div class="skm-textinfo" style='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Key.StartDate <= DateTime.Now ? " display:block": " display:none"%>'>
                                        <p class="skm-ptext">
                                            <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Count()<=12? ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description : ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Substring(0,12)+".."%>
                                        </p>
                                        <input type="button"
                                            class='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "st-skmcpcacol-favoritetoo-btn" : "st-skmcpcacol-btn" %>'
                                            value='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "已收藏" : "收藏" %>'
                                            onclick='return VourcherCollect(this, <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.ItemId %>);'>
                                    </div>
                                </div>
                                <%# VourcherCount > 12 ? ((VourcherCount == (Container.ItemIndex + 1) && VourcherCount % 12 == 0) ? string.Empty : ((Container.ItemIndex + 1) % 12 == 0 ? "</li><li>" : string.Empty)) : string.Empty %>
                            </ItemTemplate>
                        </asp:Repeater>
                        <%= VourcherCount > 12 ? "</li></ul></div>" : string.Empty %>
                    </asp:Panel>
                    <br />
                    <asp:Panel ID="divVourcherLottery" runat="server" Visible="false" CssClass="ly-skmMgdeal skm-mgtop clearfix">
                        <div class="ly-skm-ctegory-title">
                            活動合作店家  
  <p class="skm-ct-p">前往以下店家消費刷台新卡，可獲得一次抽獎機會</p>
                        </div>
                        <%= LottoryVourcherCount > 8 ? "<div id='lottory-banner-slide'><ul class='bjqs'><li>" : string.Empty %>
                        <asp:Repeater ID="rptVourcherLottery" runat="server" OnItemDataBound="rptVourcher_ItemDataBound">
                            <ItemTemplate>
                                <div class="skm-cpcafarm clearfix">
                                    <a href="#" class="" onclick="return false;">
                                        <asp:Image ID="VourcherImage" runat="server" />
                                    </a>

                                    <div class="skm-cpcadealname">
                                        <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Title%>
                                    </div>
                                    <div class="skm-textinfo" style='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Key.StartDate <= DateTime.Now ? " display:block": " display:none"%>'>
                                        <p class="skm-ptext">
                                            <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Count()<=12? ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description : ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Substring(0,12)+".."%>
                                        </p>
                                        <input type="button"
                                            class='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "st-skmcpcacol-favoritetoo-btn" : "st-skmcpcacol-btn" %>'
                                            value='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "已收藏" : "收藏" %>'
                                            onclick='return VourcherCollect(this, <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.ItemId %>);'>
                                    </div>
                                </div>
                                <%# LottoryVourcherCount > 8 ? ((LottoryVourcherCount == (Container.ItemIndex + 1) && LottoryVourcherCount % 8 == 0) ? string.Empty : ((Container.ItemIndex + 1) % 8 == 0 ? "</li><li>" : string.Empty)) : string.Empty %>
                            </ItemTemplate>
                        </asp:Repeater>
                        <%= LottoryVourcherCount > 8 ? "</li></ul></div>" : string.Empty %>
                    </asp:Panel>
                    <br />
                    <asp:Panel ID="divVourcherStore" runat="server" Visible="false" CssClass="ly-skmMgdeal skm-mgtop clearfix">
                        <div class="ly-skm-ctegory-title">
                            新光三越合作店家  
                            <p class="skm-ct-p">前往以下店家消費刷台新卡，可獲得一次抽獎機會</p>
                        </div>
                        <%= SkmStoreVourcherCount > 8 ? "<div id='skmstore-banner-slide'><ul class='bjqs'><li>" : string.Empty %>
                        <asp:Repeater ID="rptVourcherStore" runat="server" OnItemDataBound="rptVourcher_ItemDataBound">
                            <ItemTemplate>
                                <div class="skm-cpcafarm clearfix">
                                    <a href="#" class="" onclick="return false;">
                                        <asp:Image ID="VourcherImage" runat="server" />
                                    </a>

                                    <div class="skm-cpcadealname">
                                        <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Title%>
                                    </div>
                                    <div class="skm-textinfo" style='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Key.StartDate <= DateTime.Now ? " display:block": " display:none"%>'>
                                        <p class="skm-ptext">
                                            <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Count()<=12? ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description : ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.Description.Substring(0,12)+".."%>
                                        </p>
                                        <input type="button"
                                            class='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "st-skmcpcacol-favoritetoo-btn" : "st-skmcpcacol-btn" %>'
                                            value='<%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Value.Value ? "已收藏" : "收藏" %>'
                                            onclick='return VourcherCollect(this, <%# ((KeyValuePair<EventPromoItem, KeyValuePair<VourcherEvent, bool>>)(Container.DataItem)).Key.ItemId %>);'>
                                    </div>
                                </div>
                                <%# SkmStoreVourcherCount > 8 ? ((SkmStoreVourcherCount == (Container.ItemIndex + 1) && SkmStoreVourcherCount % 8 == 0) ? string.Empty : ((Container.ItemIndex + 1) % 8 == 0 ? "</li><li>" : string.Empty)) : string.Empty %>
                            </ItemTemplate>
                        </asp:Repeater>
                        <%= SkmStoreVourcherCount > 8 ? "</li></ul></div>" : string.Empty %>
                    </asp:Panel>
                </div>
            </asp:Panel>
            <br />
            <br />
            <asp:Panel ID="pan_ActivityMeasures" runat="server" CssClass="ly-skm-sponsors" ClientIDMode="Static"
                Visible="False">
                    <asp:Literal ID="lit_Description" runat="server"></asp:Literal>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pan_Sub" runat="server" Visible="false">
            <div class="PPClose">
                <div class="PPSpace">
                </div>
                <a href="https://www.17life.com" class="BackBtn"></a>
            </div>
        </asp:Panel>
    </div>
</asp:Content>

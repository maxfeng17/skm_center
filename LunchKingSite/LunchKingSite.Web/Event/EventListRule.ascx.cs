﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;

namespace LunchKingSite.Web.Event
{
    public partial class EventListRule : System.Web.UI.UserControl
    {
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        public string backUrl { get; set; }
        public int themeMainId { get; set; }
        public string title { get; set; }
        public override void DataBind()
        {
            if (themeMainId > 0)
            {
                var theme = pp.GetThemeCurationMainById(themeMainId);
                litHtml.Text = theme.MobileRuleContent.Replace("@backUrl", backUrl);
                title = string.Format("{0}-活動說明", theme.Title);
            }
        }
    }
}
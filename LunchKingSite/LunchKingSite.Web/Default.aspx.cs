﻿using Autofac;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web
{
    public partial class _Default :BasePage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            Response.Redirect("~/ppon/default.aspx", true);
        }
    }
}
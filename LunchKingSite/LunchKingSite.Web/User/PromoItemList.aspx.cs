﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using System.Web.Security;
using LunchKingSite.DataOrm;
using Winnovative.WnvHtmlConvert;

namespace LunchKingSite.Web.User
{
    public partial class PromoItemList : LocalizedBasePage, IPromoItemListView
    {
        #region property
        public EventActivity Activity
        {
            get
            {
                if (ViewState["Activity"] != null)
                    return (EventActivity)ViewState["Activity"];
                else
                    return null;
            }
            set
            {
                ViewState["Activity"] = value;
            }
        }

        public int EventId
        {
            get
            {
                int id = 0;
                if (Request.QueryString["eventId"] != null)
                     id = int.TryParse(Request.QueryString["eventId"].ToString(), out id) ? id : 0;
                return id;

            }
        }

        private PromoItemListPresenter _presenter;
        public PromoItemListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public string UserName
        {
            get { return this.Page.User.Identity.Name; }
        }
        public int UserId
        {
            get { return MemberFacade.GetUniqueId(this.Page.User.Identity.Name); }
        }
        public int PageCount
        {
            get
            {
                int pagecount;
                if (ViewState["pagecountlist"] != null && int.TryParse(ViewState["pagecountlist"].ToString(), out pagecount))
                {
                    return pagecount;
                }
                else
                    return 1;
            }
            set
            {
                ViewState["pagecountlist"] = value;
            }
        }
        public int CurrentPage
        {
            get
            {
                int currentpage;
                if (ViewState["currentpagelist"] != null && int.TryParse(ViewState["currentpagelist"].ToString(), out currentpage))
                {
                    return currentpage;
                }
                else
                    return 1;
            }
            set
            {
                ViewState["currentpagelist"] = value;
            }
        }

        public int PageSize
        {
            get
            {
                return page_promoitem.PageSize;
            }

        }
        #endregion
        #region method
        public void PromoItemGetListPaging(PromoItemCollection promoItemList)
        {
            rpt_promoitem.DataSource = promoItemList;
            rpt_promoitem.DataBind();
        }
        public void ShowMessage(string message)
        {
            string action = string.Format("alert('{0}');", message);
            ScriptManager.RegisterStartupScript(this, typeof(Button), "eventmessage", action, true);
        }
        public void Download(string eventName, string template)
        {
            PdfConverter pdfc = new PdfConverter();
            pdfc.LicenseKey = @"YklTQlNCVFZCVExSQlFTTFNQTFtbW1s=";
            pdfc.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfc.PdfDocumentOptions.TopMargin = 50;
            pdfc.PdfDocumentOptions.LeftMargin = 40;
            pdfc.PdfDocumentOptions.BottomMargin = 50;
            pdfc.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfc.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment; filename=\"" + eventName + ".pdf\"");
            Response.BinaryWrite(pdfc.GetPdfBytesFromHtmlString(template));
            Response.End();
        }
        #endregion
        #region event
        public event EventHandler<DataEventArgs<int>> PageChange;
        public event EventHandler<DataEventArgs<int>> Print;
        public event EventHandler<DataEventArgs<int>> Send;
        #endregion
        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }

            if (!Page.IsPostBack)
            {
                Presenter.OnViewInitialized();
                if (Activity != null)
                {
                    this.Title = Activity.Eventname;
                    liEventName.Text = Activity.Eventname;
                    liExplanation.Text = HttpUtility.HtmlDecode(Activity.Description1);
                }
            }
            Presenter.OnViewLoaded();
        }
        protected void Updatepromoitem(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            CurrentPage = pageNumber;
            if (this.PageChange != null)
                this.PageChange(this, e);
        }
        protected int RetrievepromoitemCount()
        {
            return PageCount;
        }
        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is PromoItem)
            {
                PromoItem item = (PromoItem)e.Item.DataItem;
                Label lab_ExpireDate = (Label)(e.Item.FindControl("lab_ExpireDate"));
                Label lab_Status = (Label)(e.Item.FindControl("lab_Status"));
                LinkButton lkPrint = (LinkButton)(e.Item.FindControl("lkPrint"));
                LinkButton lkEmail = (LinkButton)(e.Item.FindControl("lkEmail"));
                Literal liUseTime = (Literal)(e.Item.FindControl("liUseTime"));
                lab_ExpireDate.Text = Activity.EndDate.AddSeconds(-1).ToString("yyyy/MM/dd HH:mm");
                if (item.IsUsed)
                {
                    liUseTime.Text = "<span class=\"rd-Detailtitle\">使用日期：</span>";
                    lab_Status.Text = string.Format("已於 {0} 使用", item.Memo);
                    lkPrint.Visible = lkEmail.Visible = false;
                }
                else
                    lab_Status.Text = "尚未使用";
            }
        }
        protected void rpt_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            int id = int.TryParse(e.CommandArgument.ToString(), out id) ? id : 0;
            switch (e.CommandName)
            {
                case "Print":
                    if (this.Print != null)
                        this.Print(this, new DataEventArgs<int>(id));
                    break;
                case "Email":
                    if (this.Send != null)
                        this.Send(this, new DataEventArgs<int>(id));
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}
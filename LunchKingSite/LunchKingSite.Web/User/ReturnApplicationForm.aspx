﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReturnApplicationForm.aspx.cs"
    Inherits="LunchKingSite.Web.User.ReturnApplicationForm" %>

<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>退貨申請書</title>
    <link href="../Themes/default/images/17Life/G7/ReturnApplication.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pan_NoData" runat="server" Visible="false">
        <div class="ReturnApplicationForm">
            <div class="ReAppFoInfo">
                <div class="ReAppFpOffLine">
                    <div class="ReAppFoContent1">
                        <div class="ReAppFoTittle">
                            <asp:Label ID="lab_NoData" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pan_Data" runat="server">
        <div class="ReturnApplicationForm">
            <div class="ReAppFoTop">
                <img src="../Themes/default/images/17Life/G7/ReApFormTop2.png" width="740" height="45"
                    alt="" />
                <div class="ReAppNumber">
                    訂單編號：<asp:Label ID="lbOrderId" runat="server" Text=""></asp:Label>
                </div>
                <div class="ReAppBarcode">
                    <asp:Image ID="imgBarcode" runat="server" />
                </div>
            </div>
            <div class="ReturnForm">
                <table width="725" border="0" cellspacing="0" cellpadding="0" class="ReturnFormTable">
                    <tr>
                        <td width="199">
                            退貨申請日期
                        </td>
                        <td width="489">
                            <asp:Label ID="lbReturnYear" runat="server" Text=""></asp:Label>
                            年
                            <asp:Label ID="lbReturnMonth" runat="server" Text=""></asp:Label>
                            月
                            <asp:Label ID="lbReturnDay" runat="server" Text=""></asp:Label>
                            日
                        </td>
                    </tr>
                    <tr>
                        <td>
                            好康檔名
                        </td>
                        <td>
                            <asp:Label ID="lbDealNumber" runat="server" />
                            -
                            <asp:Label ID="lbDealName" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            憑證編號<br />
                            (商品則無需填寫)
                        </td>
                        <td>&nbsp;
                            <asp:Label ID="lbCouponList" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            公司抬頭(或原買受人)
                        </td>
                        <td align="right" style="padding-right: 30px; font-size: 14px;">
                            (請簽章)
                        </td>
                    </tr>
                </table>
                <div id="atmInfoContainer" runat="server">
                    <table width="725" border="0" cellspacing="0" cellpadding="0" class="ReturnFormTable">
                        <tr>
                            <td colspan="2">
                                受款退貨金額金融帳戶資料
                            </td>
                        </tr>
                        <tr>
                            <td width="360" align="left">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;戶名：<asp:Label ID="lab_ATM_AccountName" runat="server"
                                    Text=""></asp:Label>
                            </td>
                            <td width="365" align="left">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;身分證字號或統編：<asp:Label ID="lab_ATM_UserID" runat="server"
                                    Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td width="360" align="left">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;銀行名稱：<asp:Label ID="lab_ATM_BankName" runat="server"
                                    Text=""></asp:Label>
                            </td>
                            <td width="365" align="left">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;分行別：<asp:Label ID="lab_ATM_BranchName" runat="server"
                                    Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td width="360" align="left">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;帳號：<asp:Label ID="lab_ATM_Account" runat="server" Text=""></asp:Label>
                            </td>
                            <td width="365" align="left">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;連絡電話：<asp:Label ID="lab_ATM_Phone" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                ●請勿直接修改受款內容以免造成退款失敗，如有問題請與17Life客服聯繫
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="ReturnFormRedText">
                    ※若您訂單已開立發票/代收轉付收據，請連同退回或折讓證明單填寫完整（若為您訂單申請三聯式發票/代收轉付收據請填寫「營業事業統一編號」並於「公司抬頭」蓋公司發票章），一併郵寄掛號或傳真至17Life退貨處理中心；如於訂購時已索取紙本發票/代收轉付收據，請將紙本發票/代收轉付收據一併郵寄掛號寄回，否則恕無法辦理退費。
                </div>
                <div class="ReturnFormAddress">
                    17Life退貨處理中心寄件地址：104 台北市中山區中山北路一段11號13樓 17Life退貨處理中心收<br />
                    17Life退貨處理中心傳真號碼：(02)2511-9110
                </div>
                <div class="ReturnFormRedText">
                    （為確保您的退貨權益，請您於傳真完成後主動告知您傳真的日期、時間、訂單編號等資訊，我們將盡速為您確認是否已收到您的文件。）
                </div>
            </div>
            <div class="ReAppFoInfo">
                <div class="ReAppFpOffLine">
                    <div class="ReAppFoContent1">
                        <div class="ReAppFoTittle">
                            <span style="font-size: 15px;">※</span> 退貨須知<span style="font-size: 15px;">※</span>
                        </div>
                        <div class="ReAppFoSecondTi">
                            <span style="font-family: Arial, Helvetica, sans-serif;">1.</span> 申請退貨方式：</div>
                        <div class="ReAppFo">
                            <span class="ReAppFoText">《好康憑證退貨相關規定》</span>
                            <ul>
                                <li><strong>(1)</strong> 若憑證超過優惠期限未使用，可向17Life提出申請全額退費，作業手續費$0；<br />
                                    部分好康憑證所指稱之內容為依據特定單一時間與特定場合所提供之服務（包括但不限於演唱會、音樂會或展覽），當好康憑證持有人因故未能於該特定時間與特定場合兌換該服務，恕無法要求退費或另行補足差額重現該服務。
                                </li>
                                <li><strong>(2)</strong>另購買2011/04/01以前上檔的好康，不適用全程退貨保證。請於好康憑證未超過猶豫期7天（以該檔好康活動時間結束起計算）提出退貨申請，若該張好康憑證超出優惠期限，仍可至店家現場抵票面價值之商品，請您放心。
                                </li>
                            </ul>
                            <span class="ReAppFoText">《宅配商品退換貨相關規定》</span>
                            <ul>
                                <li>商品在收貨7天鑑賞期內可申請退貨；生鮮食品類需在收貨後24小時內申請退貨。<br />
                                    特殊商品退貨規則需依好康權益說明為主。</li>
                                <li><strong>(1)</strong> 如欲退貨，商品必須為全新狀態且包裝完整（包含商品、內外包裝及所有隨付配件等）。</li>
                                <li><strong>(2)</strong> 如商品含附送之贈品，請務必隨商品一併退回。</li>
                                <li><strong>(3)</strong> 如您收到商品時有損壞，請不要拆封，請來信或來電告訴我們您要辦理換貨，以保障您換貨權益。</li>
                            </ul>
                        </div>
                        <div class="ReAppFoSecondTi">
                            <span style="font-family: Arial, Helvetica, sans-serif;">2.</span> 退款方式及時間：一般退貨處理時間為10-20個工作天</div>
                        <table border="0" cellspacing="0" cellpadding="0" class="ReAppFoTwoTable">
                            <tr bgcolor="#888686">
                                <td width="119" height="25" align="center" style="color: #FFF; font-weight: bold;
                                    font-size: 15px;">
                                    付款方式
                                </td>
                                <td width="119" align="center" style="color: #FFF; font-weight: bold; font-size: 15px;">
                                    退款方式
                                </td>
                                <td width="430" align="center" style="color: #FFF; font-weight: bold; font-size: 15px;">
                                    退款工作日
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    刷卡
                                </td>
                                <td align="center">
                                    退刷
                                </td>
                                <td align="left" style="padding-left: 8px;">
                                    收到退款申請書後，以一般退貨處理時間再額外等待30-60個工作天退至您刷卡帳戶。<br />
                                    ※由於各家銀行的作業時間不一，申請到退款完成可能需等待30-60個工作天。
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    ATM轉帳
                                </td>
                                <td align="center">
                                    退款至指定帳戶
                                </td>
                                <td align="left" style="padding-left: 8px;">
                                    收到退款申請書後，以一般退貨處理時間再額外等待25-30個工作日退款至您的指定帳戶 。
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    17Life購物金
                                </td>
                                <td align="center">
                                    購物金退款
                                </td>
                                <td align="left" style="padding-left: 8px;">
                                    為一般退貨處理時間為 10~20 個工作天 。
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    PayEasy購物金
                                </td>
                                <td align="center">
                                    購物金退款
                                </td>
                                <td align="left" style="padding-left: 8px;">
                                    為一般退貨處理時間為 10~20 個工作天 。
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    17Life 紅利金
                                </td>
                                <td align="center">
                                    紅利金退款
                                </td>
                                <td align="left" style="padding-left: 8px;">
                                    為一般退貨處理時間為 10~20 個工作天 。
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    17Life折價券
                                </td>
                                <td align="center">
                                    不退款
                                </td>
                                <td align="left" style="padding-left: 8px;">
                                    使用<span lang="EN-US" xml:lang="EN-US">17Life</span>折價券折抵，無論是否退貨皆不得返還 。
                                </td>
                            </tr>
                        </table>
                        <span style="font-size: 15px; padding-left: 4px;">※收到您的退貨申請書並確認資料無誤後，17Life退貨處理中心將會依各別退款方式處理。</span>
                    </div>
                </div>
                <table border="0" cellspacing="0" cellpadding="0" class="ReAppFoFooterTable">
                    <tr>
                        <td height="10">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            客服專線：<%=config.ServiceTel %>&nbsp;&nbsp; 服務時間：平日 9:00~18:00
                        </td>
                    </tr>
                    <tr>
                        <td>
                            康太數位整合股份有限公司 版權所有 轉載必究 17Life 17life.com <script language="javascript"> var Today = new Date(); document.write(Today.getFullYear()); </script>
                        </td>
                    </tr>
                    <tr>
                        <td height="5">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>

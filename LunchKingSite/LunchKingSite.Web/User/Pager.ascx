﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Pager.ascx.cs" Inherits="LunchKingSite.Web.User.Pager" %>
<asp:Panel ID="divPager" runat="server" CssClass="Pagination">
    <asp:LinkButton ID="btnFirst" CssClass="Pagination-arrow-previous rd-page-dis" OnClick="link_Click"
        runat="server" Text="第一頁" CommandArgument="First"></asp:LinkButton>
    <asp:LinkButton ID="btnPrev" CssClass="Pagination-arrow-previous" OnClick="link_Click"
        runat="server" onmouseout="MM_swapImgRestore()" Text="上一頁" CommandArgument="Prev"></asp:LinkButton>
    <asp:LinkButton ID="btnNext" CssClass="Pagination-arrow-next" OnClick="link_Click"
        runat="server" Text="下一頁" onmouseout="MM_swapImgRestore()" CommandArgument="Next"></asp:LinkButton>
    <asp:LinkButton ID="btnLast" CssClass="Pagination-arrow-next rd-page-dis" OnClick="link_Click"
        runat="server" Text="最末頁" onmouseout="MM_swapImgRestore()" CommandArgument="Last">
    </asp:LinkButton>
    <span class="rd-page-dis">跳頁</span>
    <asp:DropDownList ID="ddlPages" runat="server" OnSelectedIndexChanged="ddlPages_SelectedIndexChanged"
        AutoPostBack="True" CssClass="rd-page-dis">
    </asp:DropDownList>
    <span class="rd-page-dis">共<asp:Label ID="lblPageCount" Font-Bold="true" runat="server"></asp:Label>
    頁&nbsp;(<asp:Label ID="lrc" runat="server" />
    筆資料)</span>
    <asp:Label ID="lblRecentCount" runat="server" Visible="false" />
</asp:Panel>
﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="BonusListNew.aspx.cs" Inherits="LunchKingSite.Web.User.BonusListNew" %>

<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="uc1" %>
<%@ Register Src="Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<asp:Content ID="Ssc" ContentPlaceHolderID="SSC" runat="server">
    <link href="../Themes/default/images/17Life/G2/PPB.css" rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-L.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-M.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-S.css") %>' rel="stylesheet" type="text/css" />
    <style type="text/css">
        .cash-value { font-weight: 800; color: #BF0000; }
        p.note {
            font-size: 12px;
            color: #666;
        }
        <%if (MGMFacade.IsEnabled) {%>
        .mc-navbtn {
            width:13% !important;
        }
        <%}%>
    </style>
    <script type="text/javascript">
        function showLoadingImg(objName) {
            $("." + objName).children(".Pagination").children(".Pagination-arrow-previous").bind("click", function () {
                $("." + objName + "Img").show();
            });
            $("." + objName).children(".Pagination").children(".Pagination-arrow-next").bind("click", function () {
                $("." + objName + "Img").show();
            });
            var ddlVal = '';
            $("." + objName).children(".Pagination").children(".rd-page-dis").focus(function () { ddlVal = $(this).val(); }).blur(function () {
                if (ddVal == $(this).val()) {
                    $(this).change();
                }
            }).change(function () {
                $("." + objName + "Img").show();
            });
        }
        function SetLinkButtonBind() {
            showLoadingImg("tdGridPager1");
            showLoadingImg("tdGridPager2");
            showLoadingImg("tdGridPager3");
        }
        $(document).ready(function () {
            SetLinkButtonBind();
            $('#naviMemberLink').removeClass().addClass('navbtn_inpage fr');


            $('.note-text-wrap').mouseenter(function () {
                var target = $(this);
                target.queueInterruptibleAction(200, function () {
                    target.find(".note-text-box").stop(false, true).fadeIn(500);
                });
            });
            $('.note-text-wrap').mouseleave(function () {
                var target = $(this);
                target.queueInterruptibleAction(50, function () {
                    target.find(".note-text-box").stop(false, true).fadeOut(500);
                });
            });

            var taishinBindStatus = getUrlQueryVariable('taishinBindStatus');
            if (taishinBindStatus != '')
            {
                alert(decodeURI(taishinBindStatus));
            }

        }); 

        function slidingMenuToggle() {
            $("#wrap").toggleClass("mbe-menu-show");
        }
        $(function () {
            $(".mbe-menu-btn").click(slidingMenuToggle);
            $(".mbe-channel").click(slidingMenuToggle);
        });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MC" runat="server">
    <uc1:Paragraph ID="pMain" ExternalCache="true" runat="server" />
    <div class="mc-navbar">
        <a href="coupon_List.aspx">
            <div class="mc-navbtn">
                訂單/憑證
            </div>
        </a>
        <%if (MGMFacade.IsEnabled) {%>
        <a href="/user/GiftBox">
            <div class="mc-navbtn">禮物盒</div>
        </a>
        <%} %>
        <a href="DiscountList.aspx">
            <div class="mc-navbtn">
                折價券
            </div>
        </a><a>
            <div class="mc-navbtn on">
                餘額明細
            </div>
        </a><a href="Collect.aspx">
            <div class="mc-navbtn">
                我的收藏
            </div>
        </a><a href="UserAccount.aspx">
            <div class="mc-navbtn">
                帳號設定
            </div>
        </a><a href="/User/ServiceList">
            <div class="mc-navbtn rd-C-Hide">
                客服紀錄
            </div>
        </a>
    </div>
    <div class="mc-content">
        <h1 class="rd-smll">餘額摘要</h1>
        <div class="mc-cash-states rd-smll">
            <ul>
                <li>【購物金】共 <span class="cash-value">
                    <asp:Literal ID="litScash" runat="server" /></span> 元，全額信託中 
                    <ul>
                        <li style="margin-left:48px; list-style-type:disc">17Life：
                            <span class="cash-value">
                                <asp:Literal ID="litScashE7" runat="server" /></span> 元
                            <div class="note-text-wrap rd-C-Hide" href="#">
                                <img src="../Themes/PCweb/images/lmQuestion.png" width="21" height="21">
                                <div class="note-text-box">
                                    為17Life會員專屬，無使用期限。<br>
                                    17Life購物金1元，可折抵新台幣1元。
                                </div>
                            </div>
                        </li>
                        <li style="margin-left:48px; list-style-type:disc">由PayEasy兌換：
                            <span class="cash-value">
                                <asp:Literal ID="litScashPez" runat="server" /></span> 元
                            <div class="note-text-wrap rd-C-Hide" href="#">
                                <img src="../Themes/PCweb/images/lmQuestion.png" width="21" height="21">
                                <div class="note-text-box" style="width:360px">
                                    由PayEasy購物金兌換而來，僅限17Life站內使用，無使用期限。<br />
                                    1元可折抵新台幣1元，可再轉換至PayEasy。
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>【紅利金】共 <span class="cash-value">
                    <asp:Label ID="lbl_Sumbo" runat="server"></asp:Label></span> 元 
                    <div class="note-text-wrap rd-C-Hide" href="#">
                        <img src="../Themes/PCweb/images/lmQuestion.png" width="21" height="21">
                        <div class="note-text-box">
                            為17Life行銷活動所發放之點數，<br />
                            可於未來消費時折抵，有使用期限。
                        </div>
                    </div>
                </li>
                <asp:PlaceHolder ID="phTaishinPayInfo" runat="server">
                    <li>台新儲值支付帳戶：共 <span class="cash-value">
                        <asp:Label ID="lbl_TaishinCashPoint" runat="server"></asp:Label></span> 元 
                        <div class="note-text-wrap rd-C-Hide" href="#">
                            <img src="../Themes/PCweb/images/lmQuestion.png" width="21" height="21">
                            <div class="note-text-box">
                                您目前台新儲值支付帳戶的餘額，欲儲值<br />
                                金額、查詢詳細消費明細請至台新儲值支付平台。
                            </div>
                        </div>
                        <asp:Button ID="btnUnbindTaishinPay" runat="server" Text="解除" Style="margin-left: 8px; cursor:pointer" OnClick="btnUnbindTaishinPay_Click" />
                    </li>
                </asp:PlaceHolder>
            </ul>
            <div class="clearfix">
            </div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <h1 class="rd-smll">購物金(餘額：<asp:Literal ID="litScash2" runat="server" />元，全額信託中)
                <div class="note-text-wrap rd-C-Hide" href="#">
                    <img src="../Themes/PCweb/images/lmQuestion.png" width="21" height="21">
                    <div class="note-text-box" style="width:300px">
                        購物金的使用優先順序為兌換來的購物金優先抵扣，<br />
                        17Life購物金則最後抵扣項目
                    </div>
                </div>
                    </h1>
                <ul>
                    <li style="margin-left:24px; list-style-type:disc">17Life：
                        <span class="cash-value">
                            <asp:Literal ID="litScashE72" runat="server" /></span> 元
                    </li>
                    <li style="margin-left:24px; list-style-type:disc">由PayEasy兌換：
                        <span class="cash-value">
                            <asp:Literal ID="litScashPez2" runat="server" /></span> 元
                    </li>
                </ul>
                <div style="padding:5px; display:none" class="tag-visable">
                    ※ 由PayEasy購物金兌換而來，僅限17Life站內使用，無使用期限。購物金的扣抵順序以兌換優先。
                </div>
                <hr class="header_hr" />
                <div id="mc-table" class="cash-table">
                    <asp:Repeater ID="repScash" runat="server" OnItemDataBound="repScash_ItemDataBound">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                                <thead>
                                    <tr>
                                        <th class="CashDate rd-M-Hide" style="width:15%">異動日期
                                        </th>
                                        <th class="CashSummary rd-M-Hide" style="width:54%">摘要
                                        </th>
                                        <th class="CashIn text-right rd-M-Hide" style="width:7%">存入
                                        </th>
                                        <th class="CashOut text-right rd-M-Hide" style="width:7%">支出
                                        </th>
                                        <th class="CashBalance text-right rd-M-Hide" style="width:7%">餘額
                                        </th>
                                        <th class="CashNote rd-M-Hide" style="width:10%">備註
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="mc-tableContentITEM">
                                <td class="CashDate">
                                    <asp:Label ID="lbl_CDT" runat="server"></asp:Label>
                                </td>
                                <td class="CashSummary text-left">
                                    <a runat="server" id="description_link">
                                        <asp:Label ID="lbl_Description" runat="server"></asp:Label></a>
                                </td>
                                <td class="CashIn text-right">
                                    <asp:Label ID="lbl_InCome" runat="server"></asp:Label>
                                </td>
                                <td class="CashOut text-right">
                                    <asp:Label ID="lbl_OutCome" runat="server"></asp:Label>
                                </td>
                                <td class="CashBalance text-right">
                                    <asp:Label ID="lbl_Surplus" runat="server"></asp:Label>
                                </td>
                                <td class="CashNote">
                                    <asp:Label ID="lbl_Memo" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <table width="100%">
                    <tr>
                        <td style="width: 30px; vertical-align: top; padding-top: 12px">
                            <asp:Image ID="Image1" Style="display: none" class="tdGridPager1Img" ImageUrl="../Themes/PCweb/images/spinner.gif" runat="server" />
                        </td>
                        <td style="align-content: center" class="tdGridPager1">
                            <uc1:Pager ID="gridPager1" runat="server" PageSize="6" OnGetCount="RetrieveOrderCount1"
                                OnUpdate="UpdateHandler1"></uc1:Pager>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel4" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <h1 class="rd-smll">紅利金(餘額：<asp:Label ID="lbl_Sumbo2" runat="server"></asp:Label>元)</h1>
                <hr class="header_hr" />
                <div id="mc-table" class="cash-table">
                    <asp:Repeater ID="rro2" runat="server" OnItemDataBound="rro2_ItemDataBound">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                                <thead>
                                    <tr>
                                        <th class="BonusDate rd-M-Hide" style="width:13%">異動日期
                                        </th>
                                        <th class="BonusActive rd-M-Hide" style="width:13%">生效日期
                                        </th>
                                        <th class="BonusExp rd-M-Hide" style="width:13%">有效期限
                                        </th>
                                        <th class="BonusSummary rd-M-Hide" style="width:40%">摘要
                                        </th>
                                        <th class="BonusIn text-right rd-M-Hide" style="width:7%">存入
                                        </th>
                                        <th class="BonusOut text-right rd-M-Hide" style="width:7%">支出
                                        </th>
                                        <th class="BonusBalance text-right rd-M-Hide" style="width:7%">餘額&nbsp;
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="mc-tableContentITEM">
                                <td class="BonusDate">
                                    <asp:Label ID="lbl_CDT" runat="server"></asp:Label>
                                </td>
                                <td class="BonusActive">
                                    <asp:Label ID="lbl_SDT" runat="server"></asp:Label>
                                </td>
                                <td class="BonusExp">
                                    <asp:Label ID="lbl_EDT" runat="server"></asp:Label>
                                </td>
                                <td class="BonusSummary text-left">
                                    <asp:Label ID="lbl_Description" runat="server"></asp:Label>
                                </td>
                                <td class="BonusIn text-right">
                                    <asp:Label ID="lbl_InCome" runat="server"></asp:Label>
                                </td>
                                <td class="BonusOut text-right">
                                    <asp:Label ID="lbl_OutCome" runat="server"></asp:Label>
                                </td>
                                <td class="BonusBalance text-right">
                                    <asp:Label ID="lbl_Surplus" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <table width="100%">
                    <tr>
                        <td style="width: 30px; vertical-align: top; padding-top: 12px">
                            <asp:Image ID="Image2" Style="display: none" class="tdGridPager2Img" ImageUrl="../Themes/PCweb/images/spinner.gif" runat="server" />
                        </td>
                        <td style="align-content: center" class="tdGridPager2">
                            <uc1:Pager ID="gridPager2" runat="server" PageSize="6" OnGetCount="RetrieveOrderCount2"
                                OnUpdate="UpdateHandler2"></uc1:Pager>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SetLinkButtonBind);
        </script>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="PromoItemList.aspx.cs" Inherits="LunchKingSite.Web.User.PromoItemList" %>

<%@ Register Src="Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href="../Themes/default/images/17Life/G2/PPB.css" rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-L.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-M.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-S.css")%>' rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="mc-content">
        <h1>17Life兌換券</h1>
        <hr class="header_hr" />
        <p>
            感謝您參加「<asp:Literal ID="liEventName" runat="server"></asp:Literal>」，以下是您獲得的序號，請參照活動辦法即可使用。
        </p>
        <div id="mc-table">
            <asp:Repeater ID="rpt_promoitem" runat="server" OnItemDataBound="rpt_ItemDataBound" OnItemCommand="rpt_ItemCommand">
                <HeaderTemplate>
                    <table width="920px" border="0" cellspacing="0" cellpadding="0" id="DiscountCodeTable" class="rd-table">
                        <thead>
                            <tr class="rd-C-Hide">
                                <th class="OrderDate">活動序號
                                </th>
                                <th class="OrderExp">使用期限
                                </th>
                                <th class="OrderName">使用狀態
                                </th>
                                <th class="OrderName">使用日期
                                </th>
                                <th class="OrderName">下載序號 / 重寄Email
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="mc-tableContentITEM">
                        <td class="OrderDate">
                            <span class="rd-Detailtitle">活動序號：</span>
                            <%# ((PromoItem)(Container.DataItem)).Code%>
                        </td>
                        <td class="OrderExp">
                            <span class="rd-Detailtitle">使用期限：</span>
                            <asp:Label ID="lab_ExpireDate" runat="server"></asp:Label>
                        </td>
                        <td class="OrderName">
                            <asp:Label ID="lab_Status" runat="server"></asp:Label>
                        </td>
                        <td class="OrderName">
                            <asp:Literal ID="liUseTime" runat="server"></asp:Literal>
                            <%# ((PromoItem)(Container.DataItem)).UseTime.HasValue ? ((PromoItem)(Container.DataItem)).UseTime.Value.ToString("yyyy/MM/dd") : string.Empty%>
                        </td>
                        <td class="OrderName">
                            <asp:LinkButton ID="lkPrint" runat="server" Text="下載此份序號" CommandArgument="<%# ((PromoItem)(Container.DataItem)).Id%>" CommandName="Print"></asp:LinkButton>
                            &nbsp;&nbsp;
                            <asp:LinkButton ID="lkEmail" runat="server" Text="重寄此份序號" CommandArgument="<%# ((PromoItem)(Container.DataItem)).Id%>" CommandName="Email"></asp:LinkButton>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody> </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <uc1:Pager ID="page_promoitem" runat="server" PageSize="10" OnGetCount="RetrievepromoitemCount"
            OnUpdate="Updatepromoitem"></uc1:Pager>
        <div class="clearfix">
        </div>
        <asp:Literal ID="liExplanation" runat="server"></asp:Literal>
    </div>
</asp:Content>

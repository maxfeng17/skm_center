﻿using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.WebLib.UI;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.ModelCustom;
using System.Text;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Web.ControlRoom;

namespace LunchKingSite.Web.User
{
    public partial class CouponDetail : LocalizedBasePage, IPponCouponDetailView
    {
        public ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
        public IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        public IFamiportProvider fp = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
        private static readonly  IOrderEntityProvider oep = ProviderFactory.Instance().GetProvider<IOrderEntityProvider>();
        private static ILog logger = LogManager.GetLogger(typeof(CouponDetail));

        #region Property defined in View
        public PponCouponDetailPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(User.Identity.Name, true);
            }
        }
        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }
        public string UserEmail
        {
            get
            {
                return MemberFacade.GetUserEmail(User.Identity.Name);
            }
        }
        public int PageCount
        {
            get
            {
                int pagecount;
                return int.TryParse((ViewState["pagecount"] == null ? string.Empty : ViewState["pagecount"].ToString()), out pagecount) ? pagecount : 0;
            }
            set
            {
                ViewState["pagecount"] = value;
            }
        }
        public bool PanelType
        {
            get
            {
                return cbx_PanelType.Checked;
            }
            set
            {
                cbx_PanelType.Checked = value;
                //pan_Main.Visible = value;
                pan_Detail.Visible = !value;
            }
        }
        public int Department
        {
            get
            {
                int dep;
                return int.TryParse(hif_Dep.Value, out dep) ? dep : 0;
            }
        }
        public int OrderStatus
        {
            get
            {
                int orderstatus;
                return int.TryParse(hif_OrderStatus.Value, out orderstatus) ? orderstatus : 0;
            }
        }
        public int GroupStatus
        {
            get
            {
                int groupstatus;
                return int.TryParse(hif_GroupStatus.Value, out groupstatus) ? groupstatus : 0;
            }
        }
        public int CancelStatus
        {
            get
            {
                int cancelstatus;
                return int.TryParse(hif_CancelStatus.Value, out cancelstatus) ? cancelstatus : -1;
            }
        }
        public int BusinessHourStatus
        {
            get
            {
                int businesshourstatus;
                return int.TryParse(hif_BusinessHourStatus.Value, out businesshourstatus) ? businesshourstatus : 0;
            }
        }
        public int CurrentQuantity
        {
            get
            {
                int currentquantity;
                return int.TryParse(hif_CurrentQuantity.Value, out currentquantity) ? currentquantity : 0;
            }
            set
            {
                hif_CurrentQuantity.Value = value.ToString();
            }
        }

        public DateTime ExpiredTime
        {
            get
            {
                DateTime expiredtime;
                return DateTime.TryParse(hif_ExpiredTime.Value, out expiredtime) ? expiredtime : DateTime.Now;
            }
        }
        public DateTime OrderEndDate
        {
            get
            {
                DateTime orderenddate;
                return DateTime.TryParse(hif_OrderEndDate.Value, out orderenddate) ? orderenddate : DateTime.Now;
            }
        }
        public Guid OrderGuid
        {
            get
            {
                Guid oid;
                if (Request.QueryString["oid"] != null)
                {
                    Guid.TryParse(Request.QueryString["oid"].ToString(), out oid);
                }
                else
                {
                    Guid.TryParse(hif_Oid.Value, out oid);
                }

                return oid;
            }
        }
        public int Slug
        {
            get
            {
                int slug;
                return int.TryParse(hif_Slug.Value, out slug) ? slug : 0;
            }
        }
        public int OrderMin
        {
            get
            {
                int ordermin;
                return int.TryParse(hif_OrderMin.Value, out ordermin) ? ordermin : 0;
            }
        }
        public int ItemPrice
        {
            get
            {
                int itemprice;
                return int.TryParse(hif_ItemPrice.Value, out itemprice) ? itemprice : 0;
            }
        }

        public bool IsFamiportGroupCoupon { get; set; }
        public bool IsHiLifeGroupCoupon { get; set; }

        public Dictionary<int, FamiUserCouponBarcodeInfo> FamiBarcodeInfoDic { get; set; }

        public int AccBusinessGroupId
        {
            get
            {
                int id;
                return int.TryParse((ViewState["AccBusinessGroupId"] == null ? string.Empty : ViewState["AccBusinessGroupId"].ToString()), out id) ? id : 0;
            }
            set
            {
                ViewState["AccBusinessGroupId"] = value;
            }
        }
        public string OrderId
        {
            get
            {
                if (ViewState["OrderId"] != null)
                    return ViewState["OrderId"].ToString();
                else
                    return string.Empty;
            }
            set
            {
                ViewState["OrderId"] = value;
            }
        }
        public ThirdPartyPayment ThirdPartyPaymentSystem
        {
            set { _thirdPartyPaymentSystem = value; }
            get { return _thirdPartyPaymentSystem; }
        }

        /// <summary>
        /// 是否紙本折讓單
        /// </summary>
        public bool IsPaperAllowance { get; set; }

        public bool IsInvoicePapered { get; set; }

        public bool ShowLinePoints
        {
            get
            {
                return oep.GetViewLineshopOrderinfoByOrderGuid(this.OrderGuid)
                    .Any(p => p.LastExternalRsrc == conf.LineRsrc && Helper.IsFlagSet(p.OrderStatus,Core.OrderStatus.Complete));
            }

        }

        public string LinePoinstDetailsLink
        {
            get
            {
                return "https://www.17life.com/join/20190610";
            }

        }
        #endregion

        #region Property
        private PponCouponDetailPresenter _presenter;
        public DateTime OrderDate
        {
            get
            {
                DateTime orderdate;
                return DateTime.TryParse(hif_OrderDate.Value, out orderdate) ? orderdate : DateTime.Now;
            }
        }
        public DateTime UseEndDate
        {
            get;
            set;
        }
        public bool IsServiceButtonVisible
        {
            get
            {
                return conf.PponServiceButtonVisible;
            }
        }

        private bool _isReverseVerify = false;
        public bool IsReverseVerify
        {
            get { return _isReverseVerify; }
        }
        private ThirdPartyPayment _thirdPartyPaymentSystem = ThirdPartyPayment.None;
        private Dictionary<int, string> couponSN = new Dictionary<int, string>();
        public class ApplyResult
        {
            public bool isAllow { get; set; }
            public string reson { get; set; }
        }
        private List<ViewMgmGift> _mgmGifts = null;
        #endregion Property

        #region Event defined in View
        public event EventHandler<DataEventArgs<bool>> PanelChange = null;
        public event EventHandler<DataEventArgs<SMSQuery>> SendSMS = null;
        public event EventHandler<DataEventArgs<SMSQuery>> GetSMS = null;
        public event EventHandler<DataEventArgs<MultipleEinvoiceRequestQuery>> RequestEinvoice = null;
        public event EventHandler<Guid> GetViewPponDealGetByBid;
        public event EventHandler<string> RequestInvoicePaper;
        #endregion event

        #region Method defined in View

        public void SetCouponListSequence(ViewCouponListSequenceCollection data, EinvoiceMainCollection einvoices, ViewPponOrderDetailCollection od_system,
            PaymentTransactionCollection pt, PaymentTransactionCollection pt_atm, CashTrustLogCollection ctls, IEnumerable<OrderReturnList> orToExchange,
            List<EinvoiceAllowanceInfo> allowances, CtAtmRefund ctatmrefund, bool receiptCodeGenerated, IList<ReturnFormEntity> returnForms, Order o, DealProperty dp, List<ViewMgmGift> mgmGifts)
        {
            DateTime now = DateTime.Now;
            pan_OrderDetailRefund.Visible = pan_OrderDetailATM.Visible = pan_OrderDetailATMFail.Visible = /*hyp_DownLoadReturnDiscount.Visible =*/ pan_ShioInfo.Visible = pan_OrderDetailExchange.Visible = false;
            pan_OrderDetailPayments.Visible = true;

            Guid bid;
            if (Guid.TryParse(hif_Bid.Value, out bid))
            {
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                hif_CurrentQuantity.Value = (deal.OrderedQuantity ?? 0).ToString();
                if (hif_CurrentQuantity.Value == "0")
                {
                    //如果等於0的話 ，重新撈取一次最新資料
                    if (GetViewPponDealGetByBid != null)
                    {
                        this.GetViewPponDealGetByBid(this, bid);
                    }
                }
            }

            IEnumerable<ViewPponOrderDetail> orderDetails = od_system.Where(x => x.OrderDetailStatus == (int)OrderDetailStatus.None);
            _mgmGifts = mgmGifts;
            _isReverseVerify = dp.VerifyActionType == (int)VerifyActionType.Reverse;

            if ((OrderStatus & (int)LunchKingSite.Core.OrderStatus.ATMOrder) > 0)
            {
                PaymentTransaction pt_atm1 = new PaymentTransaction();
                if (pt_atm.Count > 0)
                {
                    pt_atm1 = pt_atm.FirstOrDefault(x => x.PaymentType == (int)LunchKingSite.Core.PaymentType.ATM);
                }
                else if (pt.Count > 0)
                {
                    pt_atm1 = pt.FirstOrDefault(x => x.PaymentType == (int)LunchKingSite.Core.PaymentType.ATM && x.TransType == (int)PayTransType.Authorization);
                }

                if (pt_atm1 != null && pt_atm1.Id != 0)
                {
                    if (pt_atm1.Message.Length >= 14)
                    {
                        lab_ATM_Account.Text = pt_atm1.Message.Insert(3, "-").Insert(8, "-").Insert(13, "-");
                    }
                    else
                    {
                        lab_ATM_Account.Text = pt_atm1.Message;
                    }

                    lab_ATM_Total.Text = pt_atm1.Amount.ToString("F0");
                    lab_ATM_ExpiredDay1.Text = OrderDate.ToString("yyyy-MM-dd");
                    lab_ODInfo_ATM.Text = pt_atm1.Amount.ToString("F0");
                }

                if ((OrderStatus & (int)LunchKingSite.Core.OrderStatus.Complete) > 0)
                {
                    pan_OrderDetailSequence.Visible = data.Count > 0;
                    pan_ShioInfo.Visible = !(data.Count > 0);
                    if (data.Count > 0)
                    {
                        rp_CouponListSequence.DataSource = data.OrderBy(x => x.CouponStatus);
                        rp_CouponListSequence.DataBind();
                        rp_TrustCouponList.DataSource = ctls.OrderByDescending(x => x.Amount);
                        rp_TrustCouponList.DataBind();
                    }
                }
                else
                {
                    pan_OrderDetailSequence.Visible = pan_OrderDetailRefund.Visible = pan_OrderDetailPayments.Visible = pan_OrderDetailExchange.Visible = false;
                    lab_ATM_ItemName.Text = lab_ATM_ItemName2.Text = hif_ItemName.Value;
                    if (now.Year == OrderDate.Year && now.Month == OrderDate.Month && now.Day == OrderDate.Day)
                    {
                        pan_OrderDetailATM.Visible = true;
                    }
                    else
                    {
                        pan_OrderDetailATMFail.Visible = true;
                        lab_ATM_ExpiredDay2.Text = OrderDate.ToString("yyyy-MM-dd");
                    }
                }
            }
            else
            {
                orderDetails.ForEach(x => { x.ItemName = OrderFacade.GetOrderDetailItemName(OrderDetailItemModel.Create(x)); });
                pan_OrderDetailSequence.Visible = data.Count > 0;
                pan_ShioInfo.Visible = !(data.Count > 0);
                if (data.Count > 0)
                {
                    rp_CouponListSequence.DataSource = data.OrderBy(x => x.CouponStatus);
                    rp_CouponListSequence.DataBind();
                    rp_TrustCouponList.DataSource = ctls.OrderByDescending(x => x.Amount);
                    rp_TrustCouponList.DataBind();
                }
            }



            #region 退貨明細

            if (returnForms.Any())
            {
                pan_OrderDetailRefund.Visible = true;
                IsInvoicePapered = einvoices.Any(x => x.InvoicePapered);
                List<ReturnFormLog> log = GetRefundLogList(returnForms);
                rpReturnForm.DataSource = log;
                rpReturnForm.DataBind();
            }
            #endregion 退貨明細

            #region orderdetail info

            rpt_Details.DataSource = orderDetails;
            rpt_Details.DataBind();

            //下載收據
            ViewPponOrderDetail od = orderDetails.FirstOrDefault();
            if (Helper.IsFlagSet(OrderStatus, LunchKingSite.Core.OrderStatus.Complete) &&
                (od != null && od.OrderDetailCreateTime >= Presenter.GetNewInvoiceDate()))
            {
                downloadReceiptLink.Visible = true;
            }
            else
            {
                downloadReceiptLink.Visible = false;
            }

            #region 付款明細改sum cashtrustlog判斷

            ClearDetailText("0", lab_ODInfo_Ship, lab_ODInfo_CreditCard, lab_ODInfo_Bouns, litScashE7, litScashPez, lab_ODInfo_Discount, lab_ODInfo_PEZ, lab_ODInfo_Total, lab_ODInfo_TotalAmount, lab_ODInfo_ThirdPartyPay);
            int shipamount = ctls.Where(x => (x.SpecialStatus & 16) > 0).Sum(x => x.CreditCard + x.Pcash + x.Scash + x.Atm + x.Bcash);
            lab_ODInfo_Ship.Text = shipamount == 0 ? ((BusinessHourStatus & (int)LunchKingSite.Core.BusinessHourStatus.NoShippingFeeMessage) > 0 ? "貨到付運費" : "免運費") : shipamount.ToString("F0");
            lab_ODInfo_CreditCard.Text = ctls.Sum(x => x.CreditCard).ToString("F0");
            //加入銀行分期顯示文字
            if (int.Parse(ctls.Sum(x => x.CreditCard).ToString()) > 0)
            {
                if (o.IsLoaded)
                {
                    if (o.Installment == (int)OrderInstallment.In3Months || o.Installment == (int)OrderInstallment.In6Months || o.Installment == (int)OrderInstallment.In12Months)
                    {
                        lab_ODInfo_CreditCard.Text += "<br/>(分" + o.Installment + "期)";
                    }
                    if (o.MobilePayType != null)
                    {
                        lab_ODInfo_CreditCard.Text += "<br/>(" + (MobilePayType)o.MobilePayType.Value + ")";
                    }
                }
            }
            lab_ODInfo_Bouns.Text = ctls.Sum(x => x.Bcash).ToString("F0");

            //litScash.Text = ctls.Sum(x => x.Scash).ToString("F0");
            decimal e7Scash;
            decimal pezScash;
            decimal scash = OrderFacade.GetSCashSum2ByOrder(ctls[0].OrderGuid, out e7Scash, out pezScash);
            litScashE7.Text = e7Scash.ToString("F0");
            litScashPez.Text = pezScash.ToString("F0");

            lab_ODInfo_Discount.Text = ctls.Sum(x => x.DiscountAmount).ToString("F0");
            lab_ODInfo_PEZ.Text = ctls.Sum(x => x.Pcash).ToString("F0");
            lab_ODInfo_Total.Text = lab_ODInfo_TotalAmount.Text = ctls.Sum(x => x.Amount).ToString("F0");
            lab_ODInfo_ThirdPartyPay.Text = ctls.Sum(x => x.Tcash).ToString("F0");
            lab_ODInfo_FamilyIsp.Text = ctls.Sum(x => x.FamilyIsp).ToString("F0");
            lab_ODInfo_SevenIsp.Text = ctls.Sum(x => x.SevenIsp).ToString("F0");

            if (lab_ODInfo_PEZ.Text == string.Empty || lab_ODInfo_PEZ.Text == "0")
            {
                cellpPayeasy.Visible = false;
                cellmPayeasy.Visible = false;
            }
            else
            {
                cellpPayeasy.Visible = true;
                cellmPayeasy.Visible = true;
            }

            if (lab_ODInfo_ATM.Text == string.Empty || lab_ODInfo_ATM.Text == "0")
            {
                cellpATM.Visible = false;
                cellmATM.Visible = false;
            }
            else
            {
                cellpATM.Visible = true;
                cellmATM.Visible = true;
            }
            if (lab_ODInfo_FamilyIsp.Text == string.Empty || lab_ODInfo_FamilyIsp.Text == "0")
            {
                cellpFamilyIsp.Visible = false;
                cellmFamilyIsp.Visible = false;
            }
            else
            {
                cellpFamilyIsp.Visible = true;
                cellmFamilyIsp.Visible = true;
            }
            if (lab_ODInfo_SevenIsp.Text == string.Empty || lab_ODInfo_SevenIsp.Text == "0")
            {
                cellpSevenIsp.Visible = false;
                cellmSevenIsp.Visible = false;
            }
            else
            {
                cellpSevenIsp.Visible = true;
                cellmSevenIsp.Visible = true;
            }
            if (lab_ODInfo_CreditCard.Text == string.Empty || lab_ODInfo_CreditCard.Text == "0")
            {
                cellpCreditCard.Visible = false;
                cellmCreditCard.Visible = false;
            }
            else
            {
                cellpCreditCard.Visible = true;
                cellmCreditCard.Visible = true;
            }

            #endregion 付款明細改sum cashtrustlog判斷

            #endregion orderdetail info

            #region 換貨明細

            if (orToExchange.Any())
            {
                pan_OrderDetailExchange.Visible = true;

                var exchangeLog = GetExchangeLogList(orToExchange);
                rptExchangeLog.DataSource = exchangeLog;
                rptExchangeLog.DataBind();
            }

            #endregion 換貨明細

            SetEinvoice(einvoices.Where(x => x.InvoiceNumber != null && x.InvoiceStatus != (int)EinvoiceType.C0501 && x.InvoiceStatus != (int)EinvoiceType.D0401).ToList(), allowances, ctls, od_system, dp);
        }

        #region 退貨電子折讓明細按鈕
        protected void rpReturnFormItemBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //HyperLink chyp_DownLoadReturnDiscountElec = (HyperLink)e.Item.FindControl("hyp_DownLoadReturnDiscountElec");
                HyperLink chyp_DownLoadReturnDiscountPaper = (HyperLink)e.Item.FindControl("hyp_DownLoadReturnDiscountPaper");
                Button cbtnReturnDiscountElec = (Button)e.Item.FindControl("btnReturnDiscountElec");
                HtmlControl panelPcash = e.Item.FindControl("panelPcash") as HtmlControl;
                //chyp_DownLoadReturnDiscountElec.Visible = false;
                chyp_DownLoadReturnDiscountPaper.Visible = false;
                cbtnReturnDiscountElec.Visible = false;
                DateTime now = DateTime.Now;
                CashTrustLogCollection os = Presenter.getCashTrustLogGetListByOrderGuid();
                EinvoiceMainCollection einvoices = Presenter.getEinvoiceMainGetListByOrderGuid();
                EntrustSellReceipt receipt = Presenter.getEntrustSellReceiptGetByOrder();
                bool receiptCodeGenerated = (receipt.IsLoaded && receipt.ReceiptCode != null);
                IsPaperAllowance = !einvoices.Any(x => x.AllowanceStatus == (int)AllowanceStatus.ElecAllowance);
                //退貨中才顯示折讓或退貨申請書
                if (CancelStatus > -1 && CancelStatus == (int)ProgressStatus.Processing)//有退貨申請紀錄
                {
                    if (Slug <= OrderMin && now > OrderEndDate)//排除未達門檻自動退貨
                    {
                        cbtnReturnDiscountElec.Visible = false;
                        chyp_DownLoadReturnDiscountPaper.Visible = false;
                    }
                    else
                    {
                        if (os.Any(x => (x.CreditCard + x.Atm > 0 || x.UninvoicedAmount > 0) && x.Status < (int)TrustStatus.Returned) &&
                           CancelStatus == (int)ProgressStatus.Processing &&
                           (einvoices.Any(x => x.VerifiedTime.HasValue) || receiptCodeGenerated)
                        )
                        {
                            if (einvoices.Any(x => x.AllowanceStatus == (int)AllowanceStatus.ElecAllowance))
                            {
                                Presenter.couponDetailPopRtnDetail();
                                //chyp_DownLoadReturnDiscountElec.Visible = true;
                                //chyp_DownLoadReturnDiscountElec.Style.Add("cursor", "pointer");
                                //chyp_DownLoadReturnDiscountElec.Attributes.Add("onclick", "javascript:window.location.replace('" + WebUtility.GetSiteRoot() + "/service/returndiscount.ashx?oid=" + OrderGuid.ToString() + "')");
                                hyp_DownLoadReturnDiscount.Visible = true;
                                hyp_DownLoadReturnDiscount.Style.Add("cursor", "pointer");
                                hyp_DownLoadReturnDiscount.Attributes.Add("onclick", "javascript:window.location.replace('" + WebUtility.GetSiteRoot() + "/service/returndiscount.ashx?oid=" + OrderGuid.ToString() + "')");
                                cbtnReturnDiscountElec.Visible = true;
                                cbtnReturnDiscountElec.Style.Add("cursor", "pointer");
                            }
                            else
                            {
                                chyp_DownLoadReturnDiscountPaper.Visible = einvoices.Any(x => x.InvoicePapered); //有印製才顯示
                                chyp_DownLoadReturnDiscountPaper.Style.Add("cursor", "pointer");
                                chyp_DownLoadReturnDiscountPaper.Attributes.Add("onclick", "javascript:window.location.replace('" + WebUtility.GetSiteRoot() + "/service/returndiscount.ashx?oid=" + OrderGuid.ToString() + "')");
                            }

                        }
                    }
                }
                if (einvoices.Any(x => x.AllowanceStatus == (int)AllowanceStatus.ElecAllowance) &&
                    (CancelStatus == (int)ProgressStatus.Completed ||
                     CancelStatus == (int)ProgressStatus.CompletedWithCreditCardQueued ||
                     CancelStatus == (int)ProgressStatus.AtmQueueing))
                {
                    Presenter.couponDetailPopRtnDetail();
                    //chyp_DownLoadReturnDiscountElec.Visible = true;
                    //chyp_DownLoadReturnDiscountElec.Style.Add("cursor", "pointer");
                    //chyp_DownLoadReturnDiscountElec.Attributes.Add("onclick", "javascript:window.location.replace('" + WebUtility.GetSiteRoot() + "/service/returndiscount.ashx?oid=" + OrderGuid.ToString() + "')");
                    hyp_DownLoadReturnDiscount.Visible = true;
                    hyp_DownLoadReturnDiscount.Style.Add("cursor", "pointer");
                    hyp_DownLoadReturnDiscount.Attributes.Add("onclick", "javascript:window.location.replace('" + WebUtility.GetSiteRoot() + "/service/returndiscount.ashx?oid=" + OrderGuid.ToString() + "')");
                    cbtnReturnDiscountElec.Visible = true;
                    cbtnReturnDiscountElec.Style.Add("cursor", "pointer");
                }
                if (panelPcash != null && os.All(t => t.Pcash == 0))
                {
                    panelPcash.Visible = false;
                }
            }
        }
        #endregion 退貨電子折讓明細按鈕

        public void SetShipInfo(
            ViewOrderShipList osInfo, DateTime? deliveryDate, ShipInfo shipInfo, string orderDesc)
        {
            if (osInfo != null) //已成立訂單才需顯示出貨資訊
            {
                //配送資訊
                //收件人資訊
                lab_RecipientName.Text = osInfo.MemberName;
                lab_RecipientTel.Text = osInfo.PhoneNumber;
                if (osInfo.ProductDeliveryType == (int)ProductDeliveryType.Normal || osInfo.ProductDeliveryType == (int)ProductDeliveryType.Wms)
                {
                    lab_DeliveryAddress.Text = osInfo.DeliveryAddress;
                }
                else
                {
                    lab_DeliveryAddress.Text = shipInfo.PickupStore.StoreName;
                    divSubDeliveryAddress.Visible = true;
                    divSubDeliveryAddress.InnerText = shipInfo.PickupStore.StoreAddr;
                }
                lit_ShippedInfo.Text = orderDesc;


            }
        }

        public void ShowAlert(string msg)
        {
            string js = string.Format("alert('{0}'); location.href='{1}'", msg, WebUtility.GetSiteRoot());
            ScriptManager.RegisterStartupScript(this, typeof(Button), "alert", js, true);
        }

        public void FinishEinvoiceRequestRequest()
        {
            //lblEinvoicePaperRequest.Visible = true;
            btn_RequestPaperInvoice.Visible = false;
            ScriptManager.RegisterStartupScript(this, typeof(Button), "panblock", "getdetailheight();", true);
        }

        public void GetAlreadySmsCount(int count, int type)
        {
            if (count >= 3)
            {
                hif_SmsAlreadyCount.Value = "3";
                hif_SmsRemainCount.Value = "0";
            }
            else
            {
                hif_SmsAlreadyCount.Value = count.ToString();
                hif_SmsRemainCount.Value = (3 - count).ToString();
            }

            if (type == 1)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Button), "refundsuccess", "getsmsmobile(2);block_ui($('#Pop_SMS3'),0,0);", true);
            }
            else if (type == 0)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Button), "refundsuccess", "getsmsmobile(1);block_ui($('#Pop_SMS1'),0,0);", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Button), "refundsuccess", "getsmsmobile(3);block_ui($('#Pop_SMS3'),0,0);", true);
            }
        }

        public void SetReceiptDealInfo(EntrustSellType entrustSell, bool booked, DateTime? vacationStartEnd, DateTime? vacationEndTime, bool isPhysical, bool applyRefund)
        {
            switch (entrustSell)
            {
                case EntrustSellType.KindReceipt:
                    pan_Receipt.Visible = true;
                    chkPhysicalReceipt.Checked = isPhysical;
                    chkPhysicalReceipt.Visible = false;
                    pKindDeal.Visible = true;
                    pan_ShioInfo.Visible = false;
                    pan_EinvoiceMain.Visible = false;
                    break;

                case EntrustSellType.No:
                default:
                    pan_Receipt.Visible = false;
                    break;
            }
        }

        /// <summary>
        /// 全家遙遙茶的例外處理，顯示說明頁面網址
        /// </summary>
        /// <param name="message"></param>
        /// <param name="show"></param>
        public void ShowFamiTea(string message, bool show)
        {
            if (show)
            {
                famiteaLink.Text = message;
                famiteaLink.Enabled = true;
            }
            else
            {
                famiteaLink.Enabled = false;
            }
        }

        public bool ShowDetail(Guid orderGuid, ViewCouponListMain main)
        {
            cbx_PanelType.Checked = false;
            ClearDetailText(hif_ExpiredTime, hif_ItemName, hif_Bid, hif_Oid, hif_OrderStatus, hif_Dep, hif_SubTotal, lab_DetailOrderId, hif_OrderDate, lab_ATM_ItemName, lab_ATM_ItemName2);
            ClearDetailText("0", hif_Total, hif_ItemPrice, hif_ItemOPrice, lab_ODInfo_Bouns, lab_ODInfo_CreditCard, lab_ODInfo_PEZ, litScashE7, litScashPez, lab_ODInfo_Ship, lab_ODInfo_ATM, lab_ODInfo_ThirdPartyPay);

            hif_Mobile.Value = Presenter.GetUserMobileNumber();
            hif_Addr.Value = Presenter.GetUserAddress();
            //clbt_rpOrderDetail = (LinkButton)e.Item.FindControl("lbt_rpOrderDetail");
            hif_ExpiredTime.Value = main.BusinessHourDeliverTimeE == null
                ? string.Empty
                : main.BusinessHourDeliverTimeE.Value.AddDays(1).ToString();
            hif_OrderEndDate.Value = main.BusinessHourOrderTimeE.ToString();

            hif_ItemName.Value = main.ItemName;
            hif_Bid.Value = main.BusinessHourGuid.ToString();
            hif_Oid.Value = main.Guid.ToString();
            hif_OrderStatus.Value = main.OrderStatus.ToString();
            hif_GroupStatus.Value = main.Status.ToString();
            hif_BusinessHourStatus.Value = main.BusinessHourStatus.ToString();
            hif_CancelStatus.Value = main.ReturnStatus.ToString();
            hif_Dep.Value = main.Department.ToString();
            hif_SubTotal.Value = main.Subtotal.ToString("F0");
            hif_Total.Value = main.Total.ToString("F0");
            hif_ItemPrice.Value = main.ItemPrice.ToString("F0");
            hif_ItemOPrice.Value = main.ItemOrigPrice.ToString("F0");
            hif_Slug.Value = main.Slug;
            hif_OrderMin.Value = main.BusinessHourOrderMinimum.ToString("F0");
            lab_DetailOrderId.Text = main.OrderId;

            //判斷為全家成套票券(咖啡寄杯)
            if ((main.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0 &&
            Helper.IsFlagSet(main.BusinessHourStatus, Core.BusinessHourStatus.GroupCoupon) &&
            (main.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0)
            {
                IsFamiportGroupCoupon = true;
                FamiBarcodeInfoDic = FamiGroupCoupon.GetUserCouponBarcodeInfoListByOrderGuid(orderGuid);
            }

            //判斷為萊爾富家成套票券(咖啡寄杯)
            if ((main.GroupOrderStatus & (int)GroupOrderStatus.HiLifeDeal) > 0 &&
            Helper.IsFlagSet(main.BusinessHourStatus, Core.BusinessHourStatus.GroupCoupon) &&
            (main.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0 )
            {
                IsHiLifeGroupCoupon = true;
            }


            string ReturnsMessage = "";
            bool IsReturnsMessage = false;
            //用來判斷退貨商品是否為憑證或宅配
            bool isPpon = main.DeliveryType.Value == (int)DeliveryType.ToShop;
            var OrderCreateTime = (DateTime)main.CreateTime;

            //新版退貨判斷
            if (ReturnService.HasProcessingReturnForm(orderGuid))
            {
                IsReturnsMessage = true;
                ReturnsMessage = "已申請退貨，無法再申請。";
            }
            else
            {
                if (isPpon)
                {
                    //憑證
                    if (main.TotalCount != 0)
                    {
                        IsReturnsMessage = (main.RemainCount == 0);
                        ReturnsMessage = ((main.RemainCount == 0) ? " 憑證已使用完畢，無法退貨。" : ("此好康未使用:" + main.RemainCount));
                    }
                }
            }

            if (!IsReturnsMessage)
            {
                lab_ReturnMessage.Text = "";
            }
            else
            {
                lab_ReturnMessage.Text = ReturnsMessage;
            }

            if (conf.IsReturnsBtnEnabled)
            {
                if (main.ItemPrice.Equals(0) ||
                    (main.Status & (int)GroupOrderStatus.KindDeal) > 0 ||
                    !Helper.IsFlagSet(main.OrderStatus, Core.OrderStatus.Complete))
                {
                    pan_Returns.Visible = false;
                    pan_Cancel.Visible = (conf.EnableCancelPaidByIspOrder &&
                        (Helper.IsFlagSet(main.OrderStatus, (int)Core.OrderStatus.FamilyIspOrder) ||
                         Helper.IsFlagSet(main.OrderStatus, (int)Core.OrderStatus.SevenIspOrder)) &&
                        !Helper.IsFlagSet(main.OrderStatus, (int)Core.OrderStatus.Complete));
                }
                else
                {
                    pan_Returns.Visible = true;
                    pan_Cancel.Visible = false;
                }
            }

            hif_OrderDate.Value = main.CreateTime.ToString();
            //add deliverytype for shoppingcart by may
            hif_DeliveryType.Value = (main.DeliveryType.HasValue ? main.DeliveryType.Value.ToString() : ((int)DeliveryType.ToShop).ToString());



            return true;
        }

        #endregion

        #region Page (inline method or event)

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
                //InitSetUp();
                //btnService.Visible = IsServiceButtonVisible;
            }
            _presenter.OnViewLoaded();
            ifRecept.Src = conf.SSLSiteUrl + "/Ppon/ReceiptPrint.aspx?oid=" + OrderGuid;
        }

        protected void lvEinvoice_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            EinvoiceMain em = e.Item.DataItem as EinvoiceMain;

            if (em != null)
            {
                Label lblEinvoiceWinner = (Label)e.Item.FindControl("lblEinvoiceWinner");
                Label lblEinvoicePaperRequest = (Label)e.Item.FindControl("lblEinvoicePaperRequest");
                Literal litInvoiceHeader = (Literal)e.Item.FindControl("litInvoiceHeader");
                Literal litInvoiceDateMon = (Literal)e.Item.FindControl("litInvoiceDateMon");
                Literal litInvoiceDate = (Literal)e.Item.FindControl("litInvoiceDate");
                //Literal litBuyerName = (Literal)e.Item.FindControl("litBuyerName");
                Literal litInvoicePass = (Literal)e.Item.FindControl("litInvoicePass"); // 個人識別碼 / 電子發票隨機碼
                Literal litItemName = (Literal)e.Item.FindControl("litItemName");     //品名
                Literal litOrderId = (Literal)e.Item.FindControl("litOrderId");     //訂單編號
                //Literal litCouponSequenceNumber = (Literal)e.Item.FindControl("litCouponSequenceNumber"); //憑證編號
                //Literal litMemo = (Literal)e.Item.FindControl("litMemo");    //備註
                Literal litItemUnitPrice = (Literal)e.Item.FindControl("litItemUnitPrice");    //單價
                Literal litItemSumAmount = (Literal)e.Item.FindControl("litItemSumAmount"); //金額
                Literal litTotalAmount = (Literal)e.Item.FindControl("litTotalAmount");    //總計
                Literal litHasTax = (Literal)e.Item.FindControl("litHasTax");
                Literal litTaxAmount = (Literal)e.Item.FindControl("litTaxAmount");
                Literal litTaxAmount2 = (Literal)e.Item.FindControl("litTaxAmount2");
                Literal litNoTax = (Literal)e.Item.FindControl("litNoTax");
                //Literal litChineseAmount = (Literal)e.Item.FindControl("litChineseAmount");
                //Literal litGovernmentIssuedCode = (Literal)e.Item.FindControl("litGovernmentIssuedCode");

                lblEinvoiceWinner.Visible = em.InvoiceWinning;

                lblEinvoicePaperRequest.Visible = em.InvoiceRequestTime.HasValue;
                lblEinvoicePaperRequest.Text = GetMessageYouGotPaperInvoice(em);

                litInvoiceHeader.Text = em.IssueCompanyName;
                //litGovernmentIssuedCode.Text = em.GovernmentIssuedCode;

                if (int.Parse(em.InvoiceNumberTime.Value.Month.ToString()) % 2 == 1)
                {
                    litInvoiceDateMon.Text = em.InvoiceNumberTime.Value.AddYears(-1911).ToString("yyyy 年 ").TrimStart('0') + em.InvoiceNumberTime.Value.Month.ToString().PadLeft(2, '0') + "-" + (int.Parse(em.InvoiceNumberTime.Value.Month.ToString()) + 1).ToString().PadLeft(2, '0') + "月";
                }
                else
                {
                    litInvoiceDateMon.Text = em.InvoiceNumberTime.Value.AddYears(-1911).ToString("yyyy 年 ").TrimStart('0') + (int.Parse(em.InvoiceNumberTime.Value.Month.ToString()) - 1).ToString().PadLeft(2, '0') + "-" + em.InvoiceNumberTime.Value.Month.ToString().PadLeft(2, '0') + "月";
                }
                litInvoiceDate.Text = em.InvoiceNumberTime.Value.AddYears(-1911).ToString("yyyy-MM-dd HH:mm:ss").TrimStart('0');

                //string sn;
                //if (em.CouponId.HasValue && couponSN.TryGetValue(em.CouponId.Value, out sn))
                //{
                //    litCouponSequenceNumber.Text = sn;
                //}

                litInvoicePass.Text = em.InvoicePassAsString.Replace("電子發票隨機碼:", "");
                if (em.IsEinvoice3) //是否三聯式發票
                {
                    decimal saleAmount = Math.Round((em.OrderAmount / (1 + em.InvoiceTax)), 0);
                    //litBuyerName.Text = em.InvoiceComName;
                    litItemUnitPrice.Text = litItemSumAmount.Text = saleAmount.ToString("F0");
                    litTaxAmount.Text = (em.OrderAmount - saleAmount).ToString("F0");
                }
                else
                {
                    //litBuyerName.Text = em.InvoiceBuyerName;
                    litItemUnitPrice.Text = litItemSumAmount.Text = em.OrderAmount.ToString("F0");
                    litTaxAmount.Text = "0";
                }
                litTaxAmount2.Text = litTaxAmount.Text;
                litTotalAmount.Text = em.OrderAmount.ToString("F0");

                //if (!decimal.Equals(0, em.InvoiceTax) && !em.OrderIsPponitem)
                //{
                //    litMemo.Text = em.OrderItem;
                //}

                if (decimal.Equals(0, em.InvoiceTax) || em.OrderIsPponitem)
                {
                    litItemName.Text = em.OrderItem;
                }
                else
                {
                    litItemName.Text = string.Format("17Life購物金-{0}", em.OrderItem);
                }
                litOrderId.Text = em.OrderId;

                if (decimal.Equals(0, em.InvoiceTax))
                {
                    litNoTax.Text = "免稅";
                }
                else
                {
                    litHasTax.Text = "應稅";
                }

                //litChineseAmount.Text = CommonFacade.GetChineseAmountString((int)em.OrderAmount) + I18N.Phrase.Dollar;
            }
        }

        protected void lvmEinvoice_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            EinvoiceMain em = e.Item.DataItem as EinvoiceMain;
            if (em != null)
            {
                Literal litInvoiceDate = (Literal)e.Item.FindControl("litInvoiceDate");
                Literal litTotalAmount = (Literal)e.Item.FindControl("litTotalAmount");    //總計            
                litInvoiceDate.Text = em.InvoiceNumberTime.Value.AddYears(-1911).ToString("yyyy-MM-dd HH:mm:ss").TrimStart('0');
                litTotalAmount.Text = em.OrderAmount.ToString("F0");
            }
        }

        /// <summary>
        /// 回列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChangePanel(object sender, EventArgs e)
        {
            if (sender is LinkButton)
            {
                LinkButton lbt = (LinkButton)sender;
                cbx_PanelType.Checked = lbt.ID == "lbt_CloseDetail";
                hif_Oid.Value = Guid.Empty.ToString();
            }
            GetPanelChange();
        }

        protected void rp_SequenceItemBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewCouponListSequence)
            {
                DateTime now = DateTime.Now;
                ViewCouponListSequence coupon = (ViewCouponListSequence)e.Item.DataItem;
                Label clab_rpUseCount = ((Label)e.Item.FindControl("lab_rpCouponSequenceStatus"));//好康憑證列表的狀態
                Label clab_rpCouponCode = ((Label)e.Item.FindControl("lab_rpCouponCode"));
                HyperLink chyp_rpCouponSequencePrint = ((HyperLink)e.Item.FindControl("hyp_rpCouponSequencePrint"));
                HyperLink chyp_rpCouponSequenceDownLoad = ((HyperLink)e.Item.FindControl("hyp_rpCouponSequenceDownLoad"));

                HyperLink hyp_email = ((HyperLink)e.Item.FindControl("hyp_email"));
                HyperLink hyp_OpenApp = ((HyperLink)e.Item.FindControl("hyp_OpenApp"));

                LinkButton hyp_Evaluate = ((LinkButton)e.Item.FindControl("hyp_Evaluate"));

                LinkButton clbt_rpCouponSequenceSMS = ((LinkButton)e.Item.FindControl("lbt_rpCouponSequenceSMS"));
                HiddenField chif_rpCouponId = ((HiddenField)e.Item.FindControl("hif_rpCouponId"));
                HiddenField chif_deliverytype = ((HiddenField)e.Item.FindControl("hif_deliverytype"));

                // 預設關閉
                EnabledSMS(false, clbt_rpCouponSequenceSMS);
                EnabledDownLoad(false, chyp_rpCouponSequenceDownLoad, chif_rpCouponId.Value);
                EnabledPrint(false, chyp_rpCouponSequencePrint, chif_rpCouponId.Value);
                EnabledEmail(false, hyp_email, chif_rpCouponId.Value);
                EnabledOpedApp(true, hyp_OpenApp);

                hyp_Evaluate.Visible = false;

                if (coupon.Status < 0)
                {
                    #region no trust data
                    if ((OrderStatus & (int)Core.OrderStatus.Cancel) > 0)
                    {
                        clab_rpUseCount.Text = "已退貨";
                    }
                    else if (IsPastFinalExpireDate(coupon))
                    {
                        clab_rpUseCount.Text = "憑證過期";
                    }
                    else if (OrderEndDate < now)
                    {
                        if (Slug >= OrderMin)
                        {
                            clab_rpUseCount.Text = "未使用";
                            EnabledPrint(true, chyp_rpCouponSequencePrint, chif_rpCouponId.Value);
                            EnabledDownLoad(true, chyp_rpCouponSequenceDownLoad, chif_rpCouponId.Value);
                            EnabledEmail(true, hyp_email, chif_rpCouponId.Value);
                            EnabledSMS(true, clbt_rpCouponSequenceSMS);
                        }
                        else
                        {
                            clab_rpUseCount.Text = "未達門檻";
                        }
                    }
                    else
                    {
                        if (OrderEndDate > now && CurrentQuantity < OrderMin && CurrentQuantity != 0)
                        {
                            EnabledPrint(false, chyp_rpCouponSequencePrint, chif_rpCouponId.Value);
                            EnabledDownLoad(false, chyp_rpCouponSequenceDownLoad, chif_rpCouponId.Value);
                            EnabledEmail(false, hyp_email, chif_rpCouponId.Value);
                            EnabledSMS(false, clbt_rpCouponSequenceSMS);
                        }
                        else
                        {
                            clab_rpUseCount.Text = "未使用";
                            EnabledPrint(true, chyp_rpCouponSequencePrint, chif_rpCouponId.Value);
                            EnabledDownLoad(true, chyp_rpCouponSequenceDownLoad, chif_rpCouponId.Value);
                            EnabledEmail(true, hyp_email, chif_rpCouponId.Value);
                            EnabledSMS(true, clbt_rpCouponSequenceSMS);
                        }
                    }
                    #endregion no trust data
                }
                else
                {
                    int couponId = 0;
                    int.TryParse(chif_rpCouponId.Value, out couponId);
                    //取得憑證狀態
                    string couponStatus = Presenter.GetCouponStatus(couponId);
                    string couponUsageVerifiedTime = Presenter.GetCouponUsageVerifiedTime(couponId);
                    if (!string.IsNullOrEmpty(couponUsageVerifiedTime) && !IsHiLifeGroupCoupon)
                    {
                        couponStatus += couponUsageVerifiedTime;
                    }
                    clab_rpUseCount.Text = couponStatus;


                    if (coupon.Status < 2)
                    {
                        #region unused
                        if (clab_rpUseCount.Text == "未使用")
                        {
                            if (OrderEndDate < now)
                            {
                                if (Slug >= OrderMin)
                                {
                                    EnabledPrint(true, chyp_rpCouponSequencePrint, chif_rpCouponId.Value);
                                    EnabledDownLoad(true, chyp_rpCouponSequenceDownLoad, chif_rpCouponId.Value);
                                    EnabledEmail(true, hyp_email, chif_rpCouponId.Value);
                                    EnabledSMS(true, clbt_rpCouponSequenceSMS);
                                }
                            }
                            else
                            {
                                if (CurrentQuantity >= OrderMin && CurrentQuantity != 0)
                                {
                                    EnabledPrint(true, chyp_rpCouponSequencePrint, chif_rpCouponId.Value);
                                    EnabledDownLoad(true, chyp_rpCouponSequenceDownLoad, chif_rpCouponId.Value);
                                    EnabledEmail(true, hyp_email, chif_rpCouponId.Value);
                                    EnabledSMS(true, clbt_rpCouponSequenceSMS);
                                }
                            }
                        }
                        #endregion unused
                    }

                    if (coupon.Status == (int)TrustStatus.Verified)
                    {
                        hyp_Evaluate.Visible = true;
                        if (coupon.UsageVerifiedTime == null && coupon.UsageVerifiedTime.Value.AddDays(7) < DateTime.Now)
                        {
                            hyp_Evaluate.Enabled = false;
                            hyp_Evaluate.CssClass = "btn btn-mini btn-dontclick display_block rd-mc";
                            hyp_Evaluate.ToolTip = "已過評價時間";
                        }
                        else
                        {
                            var tid = Presenter.GetTrustId(couponId);
                            if (tid != new Guid())
                            {
                                hyp_Evaluate.PostBackUrl = string.Format(@"/User/EvaluateStar.aspx?tid={0}&ft={1}", tid, EvaluateSourceType.Web);
                            }
                            else
                            {
                                hyp_Evaluate.Enabled = false;
                                hyp_Evaluate.CssClass = "btn btn-mini btn-dontclick display_block rd-mc";
                                hyp_Evaluate.ToolTip = "已填寫完畢";
                            }
                        }
                    }

                }

                #region pez events
                if ((GroupStatus & (int)GroupOrderStatus.PEZevent) > 0)
                {
                    if ((GroupStatus & (int)GroupOrderStatus.PEZeventCouponDownload) > 0 && chyp_rpCouponSequenceDownLoad.Enabled)
                    {
                        EnabledPrint(true, chyp_rpCouponSequencePrint, chif_rpCouponId.Value);
                        EnabledDownLoad(true, chyp_rpCouponSequenceDownLoad, chif_rpCouponId.Value);
                        EnabledEmail(true, hyp_email, chif_rpCouponId.Value);
                        EnabledSMS(false, clbt_rpCouponSequenceSMS);
                    }
                    else
                    {
                        //強制格式20160112
                        if (string.IsNullOrEmpty(coupon.ItemName))
                        {
                            if (coupon.ItemName.ToLower().LastIndexOf("keywear") > 0)
                            {
                                EnabledPrint(true, chyp_rpCouponSequencePrint, chif_rpCouponId.Value);
                                EnabledDownLoad(true, chyp_rpCouponSequenceDownLoad, chif_rpCouponId.Value);
                                EnabledEmail(true, hyp_email, chif_rpCouponId.Value);
                                EnabledSMS(false, clbt_rpCouponSequenceSMS);
                            }
                            else
                            {
                                EnabledPrint(false, chyp_rpCouponSequencePrint, chif_rpCouponId.Value);
                                EnabledDownLoad(false, chyp_rpCouponSequenceDownLoad, chif_rpCouponId.Value);
                                EnabledEmail(false, hyp_email, chif_rpCouponId.Value);
                                EnabledSMS(false, clbt_rpCouponSequenceSMS);
                            }
                        }
                    }
                }
                #endregion pez events

                #region $0 events
                if (ItemPrice == 0)
                {
                    Guid bid = new Guid(hif_Bid.Value);
                    var isSkm = PponFacade.DealPropertyCityListGetByGuid(bid).Contains(PponCityGroup.DefaultPponCityGroup.Skm.CityId);

                    if ((GroupStatus & (int)GroupOrderStatus.PEZeventCouponDownload) > 0 || isSkm)
                    {
                        EnabledPrint(true, chyp_rpCouponSequencePrint, chif_rpCouponId.Value);
                        EnabledDownLoad(true, chyp_rpCouponSequenceDownLoad, chif_rpCouponId.Value);
                        EnabledEmail(true, hyp_email, chif_rpCouponId.Value);
                        EnabledSMS(false, clbt_rpCouponSequenceSMS);
                    }
                    else
                    {
                        EnabledPrint(false, chyp_rpCouponSequencePrint, chif_rpCouponId.Value);
                        EnabledDownLoad(false, chyp_rpCouponSequenceDownLoad, chif_rpCouponId.Value);
                        EnabledEmail(false, hyp_email, chif_rpCouponId.Value);
                        EnabledSMS(false, clbt_rpCouponSequenceSMS);
                    }

                    if ((GroupStatus & (int)GroupOrderStatus.PEZevent) > 0)
                    {
                        if (coupon.IsUsed.HasValue)
                        {
                            clab_rpUseCount.Text = coupon.IsUsed.Value ? "已列印" : "未使用";
                        }
                    }
                }
                #endregion $0 events

                #region forced download
                if (coupon.Available != null && coupon.Available.Value)
                {
                    ClearDetailText(clab_rpUseCount);
                    EnabledDownLoad(true, chyp_rpCouponSequenceDownLoad, chif_rpCouponId.Value);
                    EnabledPrint(true, chyp_rpCouponSequencePrint, chif_rpCouponId.Value);
                    EnabledEmail(true, hyp_email, chif_rpCouponId.Value);
                    EnabledSMS(true, clbt_rpCouponSequenceSMS);
                }
                #endregion forced download

                #region sms unable
                if (((GroupStatus & (int)GroupOrderStatus.DisableSMS) > 0))
                {
                    EnabledSMS(false, clbt_rpCouponSequenceSMS);
                }
                #endregion sms unable

                #region 全家咖啡寄杯(代金)

                if (IsFamiportGroupCoupon)
                {
                    hyp_Evaluate.Visible = false;
                    Label labStoreName = ((Label)e.Item.FindControl("lab_famiStoreName"));
                    Label labStoreAddress = ((Label)e.Item.FindControl("lab_famiStoreAddress"));
                    Label labUsageVerifiedTime = ((Label)e.Item.FindControl("lab_usageVerifiedTime"));
                    if (clab_rpUseCount.Text == "未使用")
                    {
                        labStoreName.Text = "--";
                        labStoreAddress.Text = "--";
                        labUsageVerifiedTime.Text = "--";
                        // 如果為全家成套票券要增加是否[處理中]判斷
                        if (FamiBarcodeInfoDic.Keys.Contains(coupon.Id))
                        {
                            if (FamiBarcodeInfoDic[coupon.Id].IsLock)
                            {
                                clab_rpUseCount.Text = "處理中";
                            }
                        }
                    }
                    else
                    {
                        if (coupon.UsageVerifiedTime.HasValue)
                        {
                            labUsageVerifiedTime.Text = coupon.UsageVerifiedTime.Value.ToString("yyyy-MM-dd HH:mm");
                        }
                        if (FamiBarcodeInfoDic.Keys.Contains(coupon.Id))
                        {
                            labStoreName.Text = FamiBarcodeInfoDic[coupon.Id].StoreName;
                            labStoreAddress.Text = FamiBarcodeInfoDic[coupon.Id].StoreAddress;
                        }
                    }
                }
                #endregion

                #region 萊爾富成套咖啡寄杯

                if (IsHiLifeGroupCoupon)
                {
                    hyp_Evaluate.Visible = false;
                    Label labhiLifeVerifiedTime = ((Label)e.Item.FindControl("lab_hiLifeVerifiedTime"));
                    if (coupon.UsageVerifiedTime.HasValue)
                    {
                        labhiLifeVerifiedTime.Text = coupon.UsageVerifiedTime.Value.ToString("yyyy-MM-dd HH:mm");
                    }
                }
                #endregion

                #region MGM(送禮狀態)

                if (_mgmGifts.Any())
                {
                    var giftCoupon = _mgmGifts.FirstOrDefault(x => x.CouponId == coupon.Id);
                    if (giftCoupon != null)
                    {
                        if (giftCoupon.Accept == (int)MgmAcceptGiftType.Unknown)
                        {
                            clab_rpUseCount.Text = "送禮中";
                        }
                        else if (giftCoupon.Accept == (int)MgmAcceptGiftType.Accept)
                        {
                            clab_rpUseCount.Text = "已送禮";
                        }
                        else if (giftCoupon.Accept == (int)MgmAcceptGiftType.Quick)
                        {
                            clab_rpUseCount.Text = "已送禮";
                        }

                        EnabledSMS(false, clbt_rpCouponSequenceSMS);
                        EnabledDownLoad(false, chyp_rpCouponSequenceDownLoad, chif_rpCouponId.Value);
                        EnabledPrint(false, chyp_rpCouponSequencePrint, chif_rpCouponId.Value);
                        EnabledEmail(false, hyp_email, chif_rpCouponId.Value);
                    }
                }

                #endregion

                #region 反核銷判斷
                if (_isReverseVerify)
                {
                    EnabledSMS(false, clbt_rpCouponSequenceSMS);
                    EnabledDownLoad(false, chyp_rpCouponSequenceDownLoad, chif_rpCouponId.Value);
                    EnabledPrint(false, chyp_rpCouponSequencePrint, chif_rpCouponId.Value);
                    EnabledEmail(false, hyp_email, chif_rpCouponId.Value);
                }
                #endregion
            }
        }

        protected void rp_SequenceItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "SendMS":
                    hif_CouponId.Value = e.CommandArgument.ToString();
                    if (GetSMS != null)
                    {
                        this.GetSMS(this, new DataEventArgs<SMSQuery>(new SMSQuery(e.CommandArgument.ToString(), hif_Bid.Value, hif_Mobile.Value)));
                    }
                    break;
            }
        }

        /// <summary>
        /// 信託狀態
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rp_TrustSequenceItemBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewCouponListSequence)
            {
                Label clab_rpUseCount = ((Label)e.Item.FindControl("lab_rpCouponSequenceStatus"));
                HiddenField chif_rpCouponId = ((HiddenField)e.Item.FindControl("hif_rpCouponId"));
                int couponId = 0;
                int.TryParse(chif_rpCouponId.Value, out couponId);
                //todo:超過一年以上的?  重複查詢getcouponstataus
                string couponStatusText = Presenter.GetCouponStatus(couponId);
                clab_rpUseCount.Text = couponStatusText == "已使用" ? "已使用" : "未使用";

                // 如果為全家成套票券要增加是否[處理中]判斷
                if (IsFamiportGroupCoupon && clab_rpUseCount.Text == "未使用")
                {
                    if (FamiBarcodeInfoDic[couponId].IsLock)
                    {
                        clab_rpUseCount.Text = "處理中";
                    }
                }
            }
        }

        protected void SendCouponSms(object sender, CommandEventArgs e)
        {
            if (SendSMS != null)
            {
                this.SendSMS(this, new DataEventArgs<SMSQuery>(new SMSQuery(hif_CouponId.Value, hif_Bid.Value, hif_Mobile.Value, OrderGuid.ToString())));
            }
        }

        protected void RequestPaperInvoice(object sender, EventArgs e)
        {
            if (RequestEinvoice != null)
            {
                this.RequestEinvoice(this, new DataEventArgs<MultipleEinvoiceRequestQuery>(
                    new MultipleEinvoiceRequestQuery(
                        this.OrderGuid, hif_EinvoiceRequest_Receiver.Value, hif_EinvoiceRequest_Address.Value)));
            }
        }

        protected string GetFinalExpireDateContent(ViewCouponListMain view)
        {
            if (view == null)
            {
                return string.Empty;
            }

            if (view.Department > 3)
            {
                return string.Empty;
            }

            if (!Helper.IsFlagSet(view.BusinessHourStatus, Core.BusinessHourStatus.FinalExpireTimeChange))
            {
                return string.Empty;
            }

            string expireDateContent = GetFinalExpireDate(view.Guid);
            if (expireDateContent.Length == 0)
            {
                return string.Empty;
            }
            else
            {
                return string.Format("<br/><span class='OrdertablegreenClosed'>{0}</span>", expireDateContent);
            }
        }

        protected string GetFinalExpireDateContent(Guid orderGuid)
        {
            if (orderGuid == Guid.Empty)
            {
                return string.Empty;
            }

            string expireDateContent = GetFinalExpireDate(orderGuid);
            if (expireDateContent.Length == 0)
            {
                return string.Empty;
            }
            else
            {
                return string.Format("<br/><span class='OrdertablegreenClosed'>{0}</span>", expireDateContent);
            }
        }

        protected string GetTrimToMaxByteLength(string itemName, int byteLengeth, string ignoreString)
        {
            if (!string.IsNullOrEmpty(itemName))
            {
                itemName = itemName.Trim();
                byte[] bytes = Encoding.Default.GetBytes(itemName);
                if (bytes != null)
                {
                    if (bytes.Length > byteLengeth)
                    {
                        itemName = Encoding.Default.GetString(bytes, 0, byteLengeth) + ignoreString;
                    }
                }
                return itemName;
            }
            return string.Empty;
        }

        protected string GetBranchName(ViewCouponListSequence couponView)
        {
            if (couponView.StoreCount <= 1)
            {
                return "";
            }
            return couponView.StoreName ?? "";
        }

        protected int CouponSeparateDigits(string bid)
        {
            Guid Gbid;
            if (!Guid.TryParse(bid, out Gbid))
            {
                return 0;
            }
            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(Gbid, true);

            return vpd.CouponSeparateDigits;
        }
        #endregion

        #region private method
        private void SetEinvoice(List<EinvoiceMain> einvoices, List<EinvoiceAllowanceInfo> allowances, 
            CashTrustLogCollection ctlogs,ViewPponOrderDetailCollection orderDetails, DealProperty dp)
        {
            btn_RequestPaperInvoice.Visible = false;    //索取發票按鈕

            if (ItemPrice == 0 || (BusinessHourStatus & (int)Core.BusinessHourStatus.PriceZeorShowOriginalPrice) > 0)
            {
                pan_EinvoiceMain.Visible = pan_EinvoiceInfo.Visible = pan_mEinvoiceInfo.Visible = true;
                pan_EinvoiceDetail.Visible = false;
                lbl_Einvoice_Others.Text = lbl_mEinvoice_Others.Text = "不開立發票:本訂單消費為0元好康活動";

                return;
            }

            bool allPaidByInvoicedScashAndBcash = ctlogs.Sum(t => t.UninvoicedAmount) == 0;
            if (allPaidByInvoicedScashAndBcash)
            {
                pan_EinvoiceMain.Visible = pan_EinvoiceInfo.Visible = pan_mEinvoiceInfo.Visible = true;
                pan_EinvoiceDetail.Visible = false;
                lbl_Einvoice_Others.Text = lbl_mEinvoice_Others.Text = "不開立發票:本訂單消費僅使用購物金/紅利折抵金額";
                return;
            }

            //新發票流程(核銷後開立發票): 7/1 以後的憑證檔, 每張憑證都有發票
            if (orderDetails[0].OrderDetailCreateTime >= Presenter.GetNewInvoiceDate() && ctlogs[0].CouponId.HasValue && (dp.SaleMultipleBase ?? 0) <= 0)
            {
                NewInvoiceContainer.Visible = true;
                OldInvoiceContainer.Visible = false;
                ShowUsingNewEinvoiceLogic(einvoices, ctlogs);
            }
            else
            {
                NewInvoiceContainer.Visible = false;
                if (einvoices.Count > 0)
                {
                    OldInvoiceContainer.Visible = true;
                    ShowUsingOldEinvoiceLogic(einvoices[0], allowances);
                }
                else
                {
                    pan_EinvoiceDetail.Visible = false;
                }
            }

            lbRequestPaper.Visible = einvoices.Any(x => x.InvoiceStatus == (int)EinvoiceType.C0401 && x.InvoiceRequestTime == null && string.IsNullOrEmpty(x.LoveCode) && x.CarrierType != (int)CarrierType.None && x.InvoiceMode2  == (int)InvoiceMode2.Duplicate);
        }

        private void ShowUsingNewEinvoiceLogic(List<EinvoiceMain> einvoices, CashTrustLogCollection ctlogs)
        {
            pan_EinvoiceDetail.Visible = pan_EinvoiceInfo.Visible = pan_EinvoiceMain.Visible = pan_mEinvoiceInfo.Visible = false;

            List<int> nonReturnBackCouponIds = ctlogs.Where(x => x.Status != (int)TrustStatus.Refunded && x.Status != (int)TrustStatus.Returned)
                    .Select(x => x.CouponId ?? 0).ToList();     // 退貨/刷退 以外的 coupon id

            //可顯示在畫面上的發票
            List<EinvoiceMain> viewableInvoice = einvoices.Where(x =>
                        !string.IsNullOrEmpty(x.InvoiceNumber)
                        && x.InvoiceStatus != (int)EinvoiceType.C0501 && x.InvoiceStatus != (int)EinvoiceType.D0401
                        && x.CouponId.HasValue && nonReturnBackCouponIds.Contains(x.CouponId.Value)
                    ).ToList();

            if (viewableInvoice.Count > 0)
            {
                pan_EinvoiceMain.Visible = true;
                pan_EinvoiceDetail.Visible = true;

                EinvoiceMain oneEinvoice = viewableInvoice.First();
                invoiceFooter.Version = (InvoiceVersion)oneEinvoice.Version;

                if (oneEinvoice.IsDonateMark)
                {
                    pan_EinvoiceInfo.Visible = pan_mEinvoiceInfo.Visible = true;
                    lbl_Einvoice_Others.Text = lbl_mEinvoice_Others.Text =
                        string.Format("發票已捐贈：{0}", LoveCodeManager.Instance.QueryLoveCodeString(oneEinvoice.LoveCode));
                    invoiceFooter.Visible = false;
                }
                else
                {
                    hidDefaultBuyerName.Value = oneEinvoice.IsEinvoice3 ? oneEinvoice.InvoiceComName : oneEinvoice.InvoiceBuyerName;
                    hidDefaultBuyerAddress.Value = oneEinvoice.InvoiceBuyerAddress;

                    //可申請紙本的發票
                    //2014/03/01後，我們可不另開紙本，會員只有在購買時可以選2聯紙本。Ｓ
                    if (oneEinvoice.Version == (int)InvoiceVersion.Old)
                    {
                        List<string> printableCouponNumbers = viewableInvoice
                            .Where(t => t.InvoiceRequestTime.HasValue == false && t.InvoiceWinning == false)
                            .Select(x => x.InvoiceNumber).ToList();
                        btn_RequestPaperInvoice.Visible = printableCouponNumbers.Count > 0;
                    }

                    foreach (EinvoiceMain invoice in viewableInvoice)
                    {
                        if (invoice.CouponId.HasValue)
                        {
                            IEnumerable<CashTrustLog> logs = ctlogs.Where(x => x.CouponId == invoice.CouponId);
                            if (logs.Count<CashTrustLog>() > 0)
                            {
                                couponSN.Add(invoice.CouponId.Value, logs.First<CashTrustLog>().CouponSequenceNumber);
                            }
                        }
                    }

                    lvEinvoice.DataSource = viewableInvoice;
                    lvEinvoice.DataBind();

                    //m版發票
                    lvmEinvoice.DataSource = viewableInvoice;
                    lvmEinvoice.DataBind();

                }
            }
        }

        private void ShowUsingOldEinvoiceLogic(EinvoiceMain einvoice, List<EinvoiceAllowanceInfo> allowances)
        {
            if ((OrderStatus & ((int)LunchKingSite.Core.OrderStatus.Cancel)) > 0 &&
                    (allowances.Sum(x => x.Amount)) == einvoice.OrderAmount)
            {
                pan_EinvoiceDetail.Visible = pan_EinvoiceInfo.Visible = pan_EinvoiceMain.Visible = pan_mEinvoiceInfo.Visible = false;
                return;
            }

            pan_EinvoiceDetail.Visible = pan_EinvoiceInfo.Visible = pan_EinvoiceMain.Visible = pan_mEinvoiceInfo.Visible = false;

            if (einvoice.Id != 0 && !string.IsNullOrEmpty(einvoice.InvoiceNumber) &&
                (einvoice.InvoiceStatus != (int)EinvoiceType.C0501))
            {
                pan_EinvoiceMain.Visible = true;
                lab_Einvoice_Head.Text = einvoice.IssueCompanyName;
                //lab_Einvoice_Code.Text = einvoice.GovernmentIssuedCode;
                invoiceFooter.Version = (InvoiceVersion)einvoice.Version;

                if (einvoice.IsDonateMark)
                {
                    pan_EinvoiceInfo.Visible = pan_mEinvoiceInfo.Visible = true;
                    lbl_Einvoice_Others.Text = lbl_mEinvoice_Others.Text =
                        string.Format("發票已捐贈：{0}", LoveCodeManager.Instance.QueryLoveCodeString(einvoice.LoveCode));
                    invoiceFooter.Visible = false;
                }
                else
                {
                    pan_EinvoiceDetail.Visible = true;
                    decimal saleamount = Math.Round((einvoice.OrderAmount / (1 + einvoice.InvoiceTax)), 0);
                    //lblEinvoicePaperRequest.Visible = einvoice.InvoiceRequestTime.HasValue;
                    //lblEinvoicePaperRequest.Text = GetMessageYouGotPaperInvoice(einvoice);

                    //lblEinvoiceWinner.Visible = einvoice.InvoiceWinning;
                    if (einvoice.Version == (int)InvoiceVersion.Old)
                    {
                        //中獎 or 已所索取紙本 or 檔次未結檔 不顯示按鈕
                        //註: 由於結檔前可以無條件退貨, 無法保證消費者會寄回紙本發票, 所以不開放申請紙本發票.
                        //註 (2012/06/14): 檔次未結檔這個判斷未來可以拿掉
                        if (einvoice.OrderTime >= Presenter.GetNewInvoiceDate())
                        {
                            btn_RequestPaperInvoice.Visible = !(einvoice.InvoiceRequestTime.HasValue
                                || einvoice.InvoiceWinning);
                        }
                        else
                        {
                            //註 (2012/06/14): 核銷後開立發票需要緩衝期 (上線一個月後 -- 2012/08/01 -- 可拿掉)
                            btn_RequestPaperInvoice.Visible = !(einvoice.InvoiceRequestTime.HasValue
                                || einvoice.InvoiceWinning
                                || !Helper.IsFlagSet(GroupStatus, GroupOrderStatus.Completed));
                        }
                    }

                    lbl_Einvoice_InvoiceNum.Text = einvoice.InvoiceNumber;
                    if (int.Parse(einvoice.InvoiceNumberTime.Value.Month.ToString()) % 2 == 1)
                    {

                        lbl_Einvoice_InvoiceDate_Mon.Text = einvoice.InvoiceNumberTime.Value.AddYears(-1911).ToString("yyyy 年 ").TrimStart('0') + einvoice.InvoiceNumberTime.Value.Month.ToString().PadLeft(2, '0') + "-" + (int.Parse(einvoice.InvoiceNumberTime.Value.Month.ToString()) + 1).ToString().PadLeft(2, '0') + "月";
                    }
                    else
                    {
                        lbl_Einvoice_InvoiceDate_Mon.Text = einvoice.InvoiceNumberTime.Value.AddYears(-1911).ToString("yyyy 年 ").TrimStart('0') + (int.Parse(einvoice.InvoiceNumberTime.Value.Month.ToString()) - 1).ToString().PadLeft(2, '0') + "-" + einvoice.InvoiceNumberTime.Value.Month.ToString().PadLeft(2, '0') + "月";
                    }
                    lbl_Einvoice_InvoiceDate.Text = einvoice.InvoiceNumberTime.Value.AddYears(-1911).ToString("yyyy-MM-dd HH:mm:ss").TrimStart('0');
                    //lbl_Einvoice_BuyerAddress.Text = hidDefaultBuyerAddress.Value = einvoice.InvoiceBuyerAddress;
                    //lbl_Einvoice_BuyerName.Text = hidDefaultBuyerName.Value =
                    //    (einvoice.IsEinvoice3 ? einvoice.InvoiceComName : einvoice.InvoiceBuyerName);
                    lbl_Einvoice_ComId.Text = einvoice.InvoiceComId;
                    lbl_Einvoice_OrderId.Text = einvoice.OrderId;
                    //lbl_Einvoice_OrderItem.Text = (einvoice.InvoiceTax == 0 ? string.Empty : (einvoice.OrderIsPponitem ? string.Empty : einvoice.OrderItem));
                    lbl_Einvoice_OrderAmount1.Text = lbl_Einvoice_OrderAmount2.Text =
                        einvoice.IsEinvoice3 ? saleamount.ToString("F0") : einvoice.OrderAmount.ToString("F0");
                    lbl_Einvoice_OrderAmount3.Text = einvoice.OrderAmount.ToString("F0");
                    lbl_Einvoice_HasTax.Text = einvoice.InvoiceTax > 0 ? "應稅" : string.Empty;
                    lbl_Einvoice_NoTax.Text = einvoice.InvoiceTax == 0 ? "免稅" : string.Empty;
                    lbl_Einvoice_TaxAmount.Text =
                        einvoice.IsEinvoice3 ? (einvoice.OrderAmount - saleamount).ToString("F0") : "0";
                    lbl_Einvoice_TaxAmount2.Text = lbl_Einvoice_TaxAmount.Text;
                    litEinvoicePass.Text = einvoice.InvoicePassAsString.Replace("電子發票隨機碼:", "");
                    //lbl_Einvoice_ChineseAmount.Text = CommonFacade.GetChineseAmountString((int)einvoice.OrderAmount) + I18N.Phrase.Dollar;
                    lbl_Einvoice_ItemName.Text = (einvoice.InvoiceTax == 0 ? einvoice.OrderItem :
                            (einvoice.OrderIsPponitem ?
                                einvoice.OrderItem :
                                string.Format("17Life購物金-{0}", einvoice.OrderItem)
                            )
                        );
                    //m版發票
                    List<EinvoiceMain> einvoiceMains = new List<EinvoiceMain>();
                    einvoiceMains.Add(einvoice);
                    lvmEinvoice.DataSource = einvoiceMains;
                    lvmEinvoice.DataBind();
                }
            }
        }

        private string GetZeroDealTagContent(CouponCodeType CodeType)
        {
            string tagContent = string.Empty;
            if (conf.EnableFami3Barcode)
            {
                tagContent = @"<div class='tag tag-fami-barcode'>優惠條碼</div>
                            <span class='tag-note'>請直接於櫃檯出示結帳</sapn>";
            }
            else
            {
                switch (CodeType)
                {
                    case CouponCodeType.Pincode:
                        tagContent = @"<div class='tag tag-fami-pincode'>紅利Pin碼</div>
                            <span class='tag-note'>請於FamiPort列印條碼</sapn>";
                        break;
                    case CouponCodeType.Barcode128:
                    case CouponCodeType.BarcodeEAN13:
                        tagContent = @"<div class='tag tag-fami-barcode'>優惠條碼</div>
                            <span class='tag-note'>請直接於櫃檯出示結帳</sapn>";
                        break;
                    default:
                        break;
                }
            }

            return tagContent;
        }

        private string GetMessageYouGotPaperInvoice(EinvoiceMain einvoice)
        {
            if (einvoice.Version == (int)InvoiceVersion.Old)
            {
                return "●您已索取紙本發票";
            }
            else
            {
                return "●您已索取電子發票紙本";
            }
        }

        private void ClearDetailText(params object[] controls)
        {
            foreach (object item in controls)
            {
                if (item is HiddenField)
                {
                    ((HiddenField)item).Value = string.Empty;
                }
                else if (item is Label)
                {
                    ((Label)item).Text = string.Empty;
                }
                else if (item is Literal)
                {
                    ((Literal)item).Text = string.Empty;
                }
            }
        }

        private void ClearDetailText(string default_string, params object[] controls)
        {
            foreach (object item in controls)
            {
                if (item is HiddenField)
                {
                    ((HiddenField)item).Value = default_string;
                }
                else if (item is Label)
                {
                    ((Label)item).Text = default_string;
                }
            }
        }

        private void EnabledSMS(bool sms, LinkButton lbt_sms)
        {
            lbt_sms.CssClass = sms ? string.Empty : "disabled";
            lbt_sms.Enabled = sms;
        }
        private void EnabledOpedApp(bool openapp, HyperLink lbt_openapp)
        {
            lbt_openapp.CssClass = openapp ? string.Empty : "disabled";
            string downloadUrl = "javascript:openAtAPP()";
            lbt_openapp.NavigateUrl= downloadUrl;
            lbt_openapp.Enabled = openapp;
        }

        private void EnabledDownLoad(bool download, HyperLink hyp_download, string couponid)
        {
            if (download)
            {
                hyp_download.Style.Add("cursor", "pointer");
                string downloadUrl = WebUtility.GetSiteRoot() + (conf.NewCouponPdfWriter ? "/User/CouponGet.aspx?cid=" : "/service/coupon.ashx?cid=") + couponid;
                hyp_download.NavigateUrl = downloadUrl;
            }
            hyp_download.CssClass = download ? string.Empty : "disabled";
            hyp_download.Enabled = download;
        }

        private void EnabledPrint(bool print, HyperLink hyp_print, string couponid)
        {
            if (print)
            {
                if (IsFamiportGroupCoupon)
                {
                    hyp_print.NavigateUrl = ResolveUrl("~/User/FamiCouponDetail.aspx") + "?cid=" + (couponid);
                }
                else if (IsHiLifeGroupCoupon)
                {
                    hyp_print.NavigateUrl = ResolveUrl("~/User/FamiCouponDetail.aspx") + "?cid=" + (couponid);
                }
                else
                {
                    hyp_print.NavigateUrl = ResolveUrl("~/ppon/coupon_print.aspx") + "?cid=" + (couponid);
                }
            }
            hyp_print.CssClass = print ? string.Empty : "disabled";
            hyp_print.Enabled = print;
        }

        private void EnabledEmail(bool email, HyperLink hyp_email, string couponid)
        {
            if (email)
            {
                hyp_email.Style.Add("cursor", "pointer");
                hyp_email.Attributes["couponId"] = couponid;
            }
            hyp_email.CssClass = email ? "MailCoupon" : "disabled";
            hyp_email.Enabled = email;
        }

        private string GetReturnAccountData(CtAtmRefund ctatmrefund)
        {
            return (ctatmrefund.Si > 0)
                    ? "<br>銀行(分行)：" + ctatmrefund.BankName + "(" + ctatmrefund.BranchName + ")<br>匯款帳號：*********" + ctatmrefund.AccountNumber.Substring(9, 5)
                    : string.Empty;
        }

        private List<ReturnFormLog> GetRefundLogList(IList<ReturnFormEntity> returnForms)
        {
            List<ReturnFormLog> log = new List<ReturnFormLog>();

            #region returnForm

            foreach (var returnForm in returnForms)
            {
                string requestedItems = returnForm.GetReturnSpec().Replace(Environment.NewLine, "<br/>");
                string returnedItems = returnForm.GetRefundedSpec().Replace(Environment.NewLine, "<br/>");
                bool isShowCash = (returnForm.ProgressStatus == ProgressStatus.Completed ||
                                   returnForm.ProgressStatus == ProgressStatus.CompletedWithCreditCardQueued);
                RefundedAmount summary = returnForm.GetRefundedAmount();

                #region 退貨進度

                string returnProgress = string.Empty;

                switch (returnForm.ProgressStatus)
                {
                    case ProgressStatus.Processing:
                    case ProgressStatus.AtmQueueing:
                    case ProgressStatus.AtmQueueSucceeded:
                    case ProgressStatus.AtmFailed:
                    case ProgressStatus.ConfirmedForCS:
                    case ProgressStatus.ConfirmedForUnArrival:
                        returnProgress = "退貨處理中";
                        break;
                    case ProgressStatus.Retrieving:
                    case ProgressStatus.RetrieveToPC:
                    case ProgressStatus.RetrieveToCustomer:
                        returnProgress = "已前往收件";
                        break;
                    case ProgressStatus.ConfirmedForVendor:
                        returnProgress = "廠商確認中";
                        break;
                    case ProgressStatus.Completed:
                    case ProgressStatus.CompletedWithCreditCardQueued:
                        returnProgress = "退貨已完成";
                        break;
                    case ProgressStatus.Canceled:
                        returnProgress = "取消退貨";
                        break;
                    case ProgressStatus.Unreturnable:
                        returnProgress = "退貨失敗";
                        break;
                    default:
                        break;
                }


                #endregion 退貨進度

                log.Add(new ReturnFormLog(returnForm.CreateTime.ToString("yyyy/MM/dd"), requestedItems, returnedItems,
                                        summary.PCash, summary.BCash, summary.SCash, summary.Pscash, summary.CreditCard, summary.Atm,
                                        summary.TCash, returnProgress, isShowCash, returnForm.IsCreditNoteReceived));
            }

            #endregion returnForm

            return log;
        }

        private List<ReturnFormLog> GetExchangeLogList(IEnumerable<OrderReturnList> orToExchange)
        {
            var log = new List<ReturnFormLog>();

            #region orToExchange

            foreach (var exchange in orToExchange)
            {
                string requestedItems = string.IsNullOrEmpty(exchange.Reason)
                                        ? string.Empty
                                        : exchange.Reason.Replace("\n", "</br>");
                switch ((OrderReturnStatus)exchange.Status)
                {
                    case OrderReturnStatus.SendToSeller:
                        log.Add(new ReturnFormLog(exchange.CreateTime.ToString("yyyy/MM/dd"), requestedItems, string.Empty, 0, 0, 0, 0, 0, 0, 0, "換貨處理中", false, false));
                        break;

                    default:
                        log.Add(new ReturnFormLog(exchange.CreateTime.ToString("yyyy/MM/dd"), requestedItems, string.Empty, 0, 0, 0, 0, 0, 0, 0,
                            Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, (OrderReturnStatus)exchange.Status), false, false));
                        break;
                }
            }

            #endregion orToExchange

            return log;
        }

        private string GetFinalExpireDate(Guid orderGuid)
        {
            if (orderGuid == Guid.Empty)
            {
                return string.Empty;
            }

            ExpirationDateSelector selector = Presenter.GetExpirationComponent(orderGuid);

            DateTime expireDate = selector.GetExpirationDate();
            if (expireDate != selector.ExpirationDates.DealOriginalExpireDate)  //selector.ExpirationDates.DealOriginalExpireDate
            {
                if (DateTime.Equals(expireDate, selector.ExpirationDates.SellerClosedDownDate ?? DateTime.MaxValue) ||
                    DateTime.Equals(expireDate, selector.ExpirationDates.StoreClosedDownDate ?? DateTime.MaxValue))  //若結束營業日&截止日期調整兩者相同, 結束營業優先
                {
                    return string.Format("(至{0}停止營業)", expireDate.ToString("yyyy/MM/dd"));
                }
                return string.Format("(至{0}憑證停止使用)", expireDate.ToString("yyyy/MM/dd"));
            }

            return string.Empty;
        }

        private bool IsPastFinalExpireDate(ViewCouponListSequence view)
        {
            ExpirationDateSelector selector = Presenter.GetExpirationComponent(view.Guid);

            if (selector == null)
            {
                return false;
            }

            return selector.IsPastExpirationDate(DateTime.Now);
        }

        private DateTime? GetExpireDate(Guid orderGuid)
        {
            ExpirationDateSelector selector = Presenter.GetExpirationComponent(orderGuid);

            if (selector == null)
            {
                return null;
            }

            return selector.GetExpirationDate();
        }

        private void GetPanelChange()
        {
            if (PanelChange != null)
            {
                this.PanelChange(this, new DataEventArgs<bool>(cbx_PanelType.Checked));
            }
        }

        #endregion

        /// <summary>
        /// 索取憑證-Email
        /// </summary>
        /// <param name="emailTo"></param>
        /// <param name="couponId"></param>
        /// <returns></returns>
        [WebMethod]
        public static bool SendCouponMail(string emailTo, int couponId)
        {
            //check login
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                HttpContext.Current.Response.StatusCode = 500;
                HttpContext.Current.Response.StatusDescription = "userNotLogin";
                return false;
            }

            string userName = HttpContext.Current.User.Identity.Name;
            Stopwatch watch = new Stopwatch();
            watch.Start();
            try
            {
                EmailFacade.SendCouponToMember(emailTo, userName, couponId);
                return true;
            }
            catch (Exception ex)
            {
                string msg = string.Format("Email憑證失敗. emailTo={0}, couponId={1}, user={2}, agent={3}, connected={4}, time={5}ms"
                                           , emailTo, couponId, userName,
                                           HttpContext.Current.Request.UserAgent,
                                           HttpContext.Current.Response.IsClientConnected,
                                           watch.Elapsed.TotalMilliseconds);

                logger.Warn(msg, ex);
                return false;
            }
            finally
            {
                watch.Stop();
            }
        }

        /// <summary>
        /// 退貨申請
        /// </summary>
        /// <param name="OrderGuid"></param>
        /// <returns></returns>
        [WebMethod]
        public static ApplyResult IsRefund(string OrderGuid)
        {
            var r = new ApplyResult();
            string reson = string.Empty;
            string userName = HttpContext.Current.User.Identity.Name;
            r.isAllow = OrderFacade.CanApplyRefund(new Guid(OrderGuid), userName, true, out reson);
            r.reson = reson;
            return r;
        }

        #region ReturnDiscountMethod

        public void SetRefundFormInfo(EinvoiceMain einvoice, CashTrustLogCollection cs, OldCashTrustLogCollection ocs, CtAtmRefund atmrefund, DealProperty dp, Order o, bool isleagl, string message, int returnFormStatus)
        {
            ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
            pan_Data.Visible = pan_NoData.Visible = false;
            if (isleagl)
            {
                pan_Data.Visible = true;
                List<RefundDiscountListClass> rfs = new List<RefundDiscountListClass>();
                List<RefundDiscountListClass> rfs_temp = new List<RefundDiscountListClass>();
                int i = 0;
                int mode = einvoice.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? 3 : 2;
                decimal tax = einvoice.InvoiceTax;
                bool istax = einvoice.InvoiceTax == 0.05m;

                if (cs.Count > 0)
                {
                    foreach (var item in cs)
                    {
                        //原本這邊有判斷, 如果全部憑證已核銷或是已刷退, 則不顯示折讓單. 但因為客服常常遇到強制處理, 或是需要補件的狀況, 暫時取消此判斷

                        //修改rfs 建構參數, 原本rfs的ItemAmount = trust.CreditCard + trust.Atm, 現改為依照訂單決定, 如果訂單建立日期 < 核銷後開發票起始日, 則維持原金額; 否則的話改為 trust.uninvoiced_amount
                        int totalAmount = o.CreateTime < Convert.ToDateTime(cp.NewInvoiceDate) ? item.CreditCard + item.Atm : item.UninvoicedAmount;
                        decimal salesamount = (Math.Round((totalAmount / (1 + tax)), 0));
                        rfs.Add(new RefundDiscountListClass(i, dp.UniqueId.ToString(), einvoice.OrderId, string.IsNullOrEmpty(item.CouponSequenceNumber) ? string.Empty : ("憑證編號#" + item.CouponSequenceNumber),
                            mode, einvoice.InvoiceNumberTime == null ? einvoice.OrderTime : einvoice.InvoiceNumberTime.Value, einvoice.InvoiceNumberTime == null,
                            string.IsNullOrEmpty(einvoice.InvoiceNumber) ? string.Empty.PadLeft(10, ' ') : einvoice.InvoiceNumber,
                            Helper.IsFlagSet(item.SpecialStatus, TrustSpecialStatus.Freight) ? "運費" : ((!einvoice.OrderIsPponitem && einvoice.InvoiceTax != 0) ? "購物金" : NoComma(einvoice.OrderItem)), totalAmount, (int)salesamount, totalAmount - (int)salesamount, istax, einvoice.OrderIsPponitem));
                        //rfs[i].breakstring = "<div style='page-break-before:always;' />";
                        i++;
                    }
                }
                else if (ocs.Count > 0)
                {
                    int price = (int)einvoice.OrderAmount / ocs.Count;
                    foreach (var item in ocs)
                    {
                        if (item.Status < ((int)TrustStatus.Verified))
                        {
                            decimal salesamount = (Math.Round(((price) / (1 + tax)), 0));
                            rfs.Add(new RefundDiscountListClass(i, dp.UniqueId.ToString(), einvoice.OrderId, string.IsNullOrEmpty(item.SequenceNumber) ? string.Empty : ("憑證編號#" + item.SequenceNumber),
                                mode, einvoice.InvoiceNumberTime == null ? einvoice.OrderTime : einvoice.InvoiceNumberTime.Value, einvoice.InvoiceNumberTime == null,
                                string.IsNullOrEmpty(einvoice.InvoiceNumber) ? string.Empty.PadLeft(10, ' ') : einvoice.InvoiceNumber,
                               ((!einvoice.OrderIsPponitem && einvoice.InvoiceTax != 0) ? "購物金" : NoComma(einvoice.OrderItem)), price, (int)salesamount, (price) - (int)salesamount, istax, einvoice.OrderIsPponitem));
                            rfs[i].breakstring = "<div style='page-break-before:always;' />";
                            i++;
                        }
                    }
                }

                if (rfs.Count > 0)
                {
                    rfs.Last().breakstring = string.Empty;
                }

                rpt_Form.DataSource = rfs;
                rpt_Form.DataBind();
            }
            else
            {
                lab_NoData.Text = message;
                pan_NoData.Visible = true;
            }
        }

        public void SetRefundFormInfoForMultiInvoices(List<EinvoiceMain> einvoices, CashTrustLogCollection cs, CtAtmRefund atmrefund, DealProperty dp, Order o, bool isleagl, string message, int returnFormStatus)
        {
            ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
            pan_Data.Visible = pan_NoData.Visible = false;
            if (isleagl)
            {
                pan_Data.Visible = true;
                List<RefundDiscountListClass> rfs = new List<RefundDiscountListClass>();
                List<RefundDiscountListClass> rfs_temp = new List<RefundDiscountListClass>();
                int i = 0;
                if (cs.Count > 0)
                {
                    foreach (var item in cs.Where(x => x.Amount > 0))
                    {
                        EinvoiceMain einvoice = (einvoices.Where(x => x.CouponId == item.CouponId).Count() > 0)
                                                    ? einvoices.Where(x => x.CouponId == item.CouponId).First()
                                                    : null;
                        einvoice = dp.SaleMultipleBase > 0 ? einvoices.Where(x => x.OrderGuid == item.OrderGuid).First() : einvoice;

                        if (einvoice != null)
                        {
                            int mode = einvoice.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? 3 : 2;
                            decimal tax = einvoice.InvoiceTax;
                            bool istax = einvoice.InvoiceTax == 0.05m;

                            //原本這邊有判斷, 如果全部憑證已核銷或是已刷退, 則不顯示折讓單. 但因為客服常常遇到強制處理, 或是需要補件的狀況, 暫時取消此判斷

                            //修改rfs 建構參數, 原本rfs的ItemAmount = trust.CreditCard + trust.Atm, 現改為依照訂單決定. 如果訂單建立日期 < 核銷後開發票起始日, 則維持原金額; 否則的話改為 trust.uninvoiced_amount
                            int totalAmount = o.CreateTime < Convert.ToDateTime(cp.NewInvoiceDate) ? item.CreditCard + item.Atm : item.UninvoicedAmount;
                            decimal salesamount = (Math.Round((totalAmount / (1 + tax)), 0));
                            rfs.Add(new RefundDiscountListClass(i, dp.UniqueId.ToString(), einvoice.OrderId, string.IsNullOrEmpty(item.CouponSequenceNumber) ? string.Empty : ("憑證編號#" + item.CouponSequenceNumber),
                                mode, einvoice.InvoiceNumberTime == null ? einvoice.OrderTime : einvoice.InvoiceNumberTime.Value, einvoice.InvoiceNumberTime == null,
                                string.IsNullOrEmpty(einvoice.InvoiceNumber) ? string.Empty.PadLeft(10, ' ') : einvoice.InvoiceNumber,
                                Helper.IsFlagSet(item.SpecialStatus, TrustSpecialStatus.Freight) ? "運費" : ((!einvoice.OrderIsPponitem && einvoice.InvoiceTax != 0) ? "購物金" : NoComma(einvoice.OrderItem)), totalAmount, (int)salesamount, totalAmount - (int)salesamount, istax, einvoice.OrderIsPponitem));
                            rfs[i].breakstring = "<div style='page-break-before:always;' />";
                            i++;
                        }
                    }
                }

                if (rfs.Count > 0)
                {
                    rfs.Last().breakstring = string.Empty;
                }

                rpt_Form.DataSource = rfs;
                rpt_Form.DataBind();
            }
            else
            {
                lab_NoData.Text = message;
                pan_NoData.Visible = true;
            }
        }

        public void SetEntrustSellRefundFormInfo(List<EntrustSellReceipt> receipts, Order o, CashTrustLogCollection cs, DealProperty dp, bool isleagl, string message, int returnFormStatus)
        {
            pan_Data.Visible = pan_NoData.Visible = false;
            if (isleagl)
            {
                pan_Data.Visible = true;
                List<RefundDiscountListClass> rfs = new List<RefundDiscountListClass>();
                List<RefundDiscountListClass> rfs_temp = new List<RefundDiscountListClass>();
                int i = 0;
                if (cs.Count > 0)
                {
                    foreach (var item in cs)
                    {
                        EntrustSellReceipt receipt = receipts.Single(t => t.TrustId == item.TrustId);
                        int mode = receipt.ForBusiness ? 3 : 2;
                        if ((receipt.CouponId > 0 && item.Status < ((int)TrustStatus.Refunded)) ||
                              (receipt.CouponId == 0 && item.Status != ((int)TrustStatus.Verified) && item.Status != ((int)TrustStatus.Refunded)))
                        {
                            rfs.Add(new RefundDiscountListClass(i, dp.UniqueId.ToString(),
                                o.OrderId, string.IsNullOrEmpty(item.CouponSequenceNumber) ? string.Empty : ("憑證編號#" + item.CouponSequenceNumber),
                                mode, receipt.ReceiptCodeTime == null ? o.CreateTime : receipt.ReceiptCodeTime.Value, receipt.ReceiptCodeTime == null,
                                receipt.ReceiptCode == null ? string.Empty.PadLeft(10, ' ') : " S" + receipt.ReceiptCode,
                                Helper.IsFlagSet(item.SpecialStatus, TrustSpecialStatus.Freight) ? "運費" : NoComma(receipt.ItemName),
                                (int)receipt.Amount,
                                (int)receipt.Amount,
                                0,
                                false,
                                receipt.CouponId == 0));
                            rfs[i].breakstring = "<div style='page-break-before:always;' />";
                            i++;
                        }
                    }
                }

                if (rfs.Count > 0)
                {
                    rfs.Last().breakstring = string.Empty;
                }

                rpt_Form.DataSource = rfs;
                rpt_Form.DataBind();
            }
            else
            {
                lab_NoData.Text = message;
                pan_NoData.Visible = true;
            }
        }

        public static string NoComma(string input, string comma = ",")
        {
            return string.IsNullOrEmpty(input) ? string.Empty : input.Replace(comma, string.Empty);
        }

        #endregion ReturnDiscountMethod

        protected void lbRequestPaper_Click(object sender, EventArgs e)
        {
            lbRequestPaper.Visible = false;
            if (RequestInvoicePaper != null)
            {
                RequestInvoicePaper(sender, OrderId);
            }
        }
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="EvaluateStar.aspx.cs" Inherits="LunchKingSite.Web.User.EvaluateStar" %>


<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <style type="text/css">
        p.rateTxt {
            position: absolute;
            margin-top: -40px;
            margin-left: 180px;
            width: 100px;
            height: 33px;
            line-height: 22px;
            text-align: left;
            font-size: 16px;
            color: green;
        }
    </style>
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="center">
        <div id="evaluateMain" class="msg-block clearfix">
            <p class="msg-p">
                謝謝您消費使用 【<asp:Literal ID="Item_Name" runat="server"></asp:Literal>】，
                <br>
                您的寶貴意見會作為店家增進服務品質的重要依據（<span class="nam-value">＊</span>號為必填問題）
                <br>
                非常感謝您的回饋！
            </p>
            <div class="grui-form">
                <asp:Literal ID="Question_Table" runat="server"></asp:Literal>
                <div class='btn-center'>
                    <asp:Button ID="btn_submit" runat="server" Text="送出資料" OnClick="btn_submit_Click" OnClientClick="return CheckRequire();" class='btn btn-large btn-primary m-btn100' />
                    <button id="clearContent" class='btn btn-large m-btn100'>清除重填</button>
                </div>
            </div>
        </div>
    </div>

    <div id="exitPop" class='pop-window' style="display: none;margin: 50px auto;">
        <div class='pop-content'>
            <div class='pay_success'></div>
            <h1 class='success'>評價成功</h1>
            <hr />
            <p>
                您的評價已送出，非常感謝您的回饋!<br>
                帶您前往今日好康
            </p>
            <div class='text-center'>
                <input type='button' class='btn btn-large btn-primary' onclick="location.href = '../ppon/default.aspx'" value='確定'>
            </div>
        </div>
    </div>

    <div id="dobefore" class='pop-window' style="display: none; margin: 50px auto;">
        <div class='pop-content' >
            <div class='pay_success'></div>
            <h1 class='success'>已評價</h1>
            <hr />
            <p>
                您的評價於先前已送出，非常感謝您的回饋!<br>
                帶您前往今日好康
            </p>
            <div class='text-center'>
                <input type="button" class='btn btn-large btn-primary' onclick="location.href = '../ppon/default.aspx'" value='確定'>
            </div>
        </div>
    </div>
    
    <div id="expired" class='pop-window' style="display: none; margin: 50px auto;">
        <div class='pop-content' >
            <div class='pay_success'></div>
            <h1 class='success'>已過評價時間</h1>
            <hr />
            <p>
                可填寫評價時間為憑證使用後七天內，本筆憑證已過評價時間，謝謝您。<br>
                帶您前往今日好康
            </p>
            <div class='text-center'>
                <input type="button" class='btn btn-large btn-primary' onclick="location.href = '../ppon/default.aspx'" value='確定'>
            </div>
        </div>
    </div>

    <script src="../Tools/js/jstar.js" type="text/javascript"></script>
    <script src="../Tools/js/jquery.blockUI.js" type="text/javascript"></script>

    <script type="text/javascript">
        $('.cancel-mr').hide();
        $('#dialog_exit_message').dialog({
            autoOpen: false,
            title: 'Thanks!',
            modal: true
        });

        $('.starbox').starbox({
            average: 0,
            changeable: 'true',
            autoUpdateAverage: true,
            ghosting: true
        });

        $(".btn_message").each(function() {
            $(this).on("click", function () {
                var msg = $("#q5");//q5=意見欄
                var tmp = $(this).val();
                if (msg.val().length == 0) {
                    msg.val(tmp);
                } else {
                    msg.val(msg.val() + "、" + tmp);
                }
                $('input[name=q5_val]').val($('#q5').attr('coltype') + "^" + msg.val());
            });
        });

        $('#clearContent').on('click', function () {
            $.each($('.data'), function () {
                if ($(this).hasClass("starbox")) {
                    $('#' + this.id).starbox("setValue", 0);
                }
                else {
                    $('#' + this.id).val("");
                }
            });
        });

        $('.data').on(
            {
                'touchstart touchmove touchend tap click': function (e) {
                    if ($(this).hasClass("starbox")) {
                        $('input[name=' + $(this).attr('id') + '_val]')
                            .val($(this).attr('coltype') + "^" + ($(this).starbox("getValue")) * 5);

                        var qList = new Array('q1', 'q2', 'q6', 'q4');
                        if ($(this).attr('id') == qList[0]) {
                            if (($(this).starbox("getValue")) * 5 >= 3) {
                                $('#' + qList[1]).starbox("setValue", ($(this).starbox("getValue")));
                                $('#' + qList[2]).starbox("setValue", ($(this).starbox("getValue")));
                                $('#' + qList[3]).starbox("setValue", ($(this).starbox("getValue")));
                                $('input[name=' + qList[1] + '_val]').val($(this).attr('coltype') + "^" + ($(this).starbox("getValue")) * 5);
                                $('input[name=' + qList[2] + '_val]').val($(this).attr('coltype') + "^" + ($(this).starbox("getValue")) * 5);
                                $('input[name=' + qList[3] + '_val]').val($(this).attr('coltype') + "^" + ($(this).starbox("getValue")) * 5);
                                                                
                                $('#div' + qList[1]).hide();
                                $('#div' + qList[2]).hide();
                                $('#div' + qList[3]).hide();
                                $('.cancel-mr').hide();
                            } else {

                                $('#' + qList[1]).starbox("setValue", 0);
                                $('#' + qList[2]).starbox("setValue", 0);
                                $('#' + qList[3]).starbox("setValue", 0);

                                $('input[name=' + qList[1] + '_val]').val('') ? $('#' + qList[1] + '_text').text('') : true;
                                $('input[name=' + qList[2] + '_val]').val('') ? $('#' + qList[2] + '_text').text('') : true;
                                $('input[name=' + qList[3] + '_val]').val('') ? $('#' + qList[3] + '_text').text('') : true;
                           
                                $('#div' + qList[1]).show();
                                $('#div' + qList[2]).show();
                                $('#div' + qList[3]).show();
                                $('.cancel-mr').show();
                            }
                        }
                    }
                    else {
                        $('input[name=' + $(this).attr('id') + '_val]').val($(this).attr('coltype') + "^" +
                            $('#' + $(this).attr('id')).val());
                    }
                },
                'blur': function () {
                    $('input[name=' + $(this).attr('id') + '_val]').val($(this).attr('coltype') + "^" +
                            $('#' + $(this).attr('id')).val());
                },
                'mousemove': function (event) {
                    if ($(this).hasClass("starbox")) {
                        val = ($(this).starbox("getValue")) * 5;
                        if (val >= 0 && val <= 5.0) {
                            $('#' + $(this).attr('id') + '_text').text(TipText($(this).attr('id'), val));
                        }
                    }
                },
                'mouseleave': function () {
                    if ($(this).hasClass("starbox")) {
                        val = ($(this).starbox("getValue")) * 5;
                        val = isNaN(val) ? 0 : val;
                        if (val > 0 && val <= 5.0) {
                            $('#' + $(this).attr('id') + '_text').text(TipText($(this).attr('id'), val));
                        } else {
                            $('#' + $(this).attr('id') + '_text').text('');
                        }
                    }
                }
            }
        );

        function TipText(q, v) {
            v -= 1;
            var q1Text = ["非常不滿意", "不滿意", "普通", "滿意", "非常滿意"], q2Text = ["非常不願意", "不願意", "普通", "願意", "非常願意"]
            , q3Text = ["非常不同意", "不同意", "普通", "同意", "非常同意"];

            //safari mobile device不知道為時麼就是要點兩次
            //改天把套件換掉
            if (/\d.NaN$/.test($('input[name=' + q + '_val]').val()) && isMobileSafari()) {
                if (q == 'q1') { //修正safari第一次點高評價下方還是會出現選項的問題
                    var qList = new Array('q1', 'q2', 'q6', 'q4');
                    $('#div' + qList[1]).hide();
                    $('#div' + qList[2]).hide();
                    $('#div' + qList[3]).hide();
                    $('.cancel-mr').hide();
                }
                return "請再點一次確認。";
            }
            if (q == 'q1') {
                return q1Text[v];
            } else if (q == 'q2') {
                return q2Text[v];
            } else if (q == 'q6' || q == 'q4') {
                return q3Text[v];
            }
        }

        function CheckLength(w) {
            var max = 140;
            if (w.value.length > max)
                w.value = w.value.substring(0, max);
            var curr = (max - w.value.length) + '/' + max;

            $('#' + $(w).attr('id') + '_tips').html(curr.toString());
        }

        function CheckRequire() {

            var result = true;

            $.each($('[isrequire=1]'), function () {
                if (isNaN($('#' + this.id).starbox("getValue")) && !isMobileSafari()) {
                    alert('必填項目請勾選');
                    result = false;
                    return false;
                }
            });

            if (result) {
                $.each($('.starbox'), function () {
                    var tmpVal = $('#' + this.id).starbox("getValue");
                    if ((!isNaN(tmpVal)) && tmpVal < 0.6) {
                        var msg = $("#q5").val();//q5=意見欄
                        if (msg.length == 0) {
                            alert('請選擇或填寫您不滿意的原因，作為店家改進的參考，謝謝您。');
                            result = false;
                            return false;
                        } 
                    }
                });
            }

            return result;
        }

        function isMobileSafari() {
            return navigator.userAgent.match(/(iPod|iPhone|iPad)/) && navigator.userAgent.match(/AppleWebKit/);
        }

        function block_ui(target, iwidth, iheight) {
            $('#evaluateMain').hide();
            $(target).show();
        }
        function unblock_ui() {
            $.unblockUI();
            $('#ReasonAlert').hide();
            return false;
        }

    </script>
</asp:Content>
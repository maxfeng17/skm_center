﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="coupon_List.aspx.cs" Inherits="LunchKingSite.Web.User.coupon_List" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph" TagPrefix="uc1" %>
<%@ Register Src="Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href="<%=ResolveUrl("~/Themes/default/images/17Life/G2/PPB.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Themes/PCweb/css/OrderDetail.css")%>" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .email_pop p
        {
            font-family: "微軟正黑體" , "Microsoft JhengHei";
        }
        <%if (MGMFacade.IsEnabled) {%>
        .mc-navbtn {
            width:13% !important;
        }
        <%}%>
        #mc-table span .btn{
            font-size:95%;
            padding: 1px 8px;
        }
    </style>
    <!-- Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-3157038-1', 'auto');
        ga('send', 'pageview');
   </script>
   <!-- End Google Analytics -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <CompositeScript ScriptMode="Release">
            <Scripts>
                <asp:ScriptReference Path="~/Tools/js/jquery.blockUI.js" />
            </Scripts>
        </CompositeScript>
    </asp:ScriptManagerProxy>
    <script type="text/javascript">

        $(function () {
            $(".mbe-menu-btn").click(slidingMenuToggle);
            $(".mbe-channel").click(slidingMenuToggle);
            $('#naviMemberLink').removeClass().addClass('navbtn_inpage fr');
        });

        ///////////////////////////////////

        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if ($.trim(email) != '' && regex.test(email))
                return true;
            else
                return false;
        }
        function block_ui(target, iwidth, iheight) {
            unblock_ui();
            $.blockUI({ message: $(target), css: { backgroundcolor: 'transparent', border: 'none', top: ($(window).height()) / 8 + 'px', left: ($(window).width() - 700) / 2 + 'px', width: iwidth, height: iheight} })
        }
        function unblock_ui() {
            $.unblockUI();
            return false;
        }

        function inputcheck(target, temp, messgee) {
            var leng = $(target).val();
            if (leng.length > 0) {
                $(temp).val(leng);
                unblock_ui();
                return true;
            }
            else {
                $(messgee).show();
                return false;
            }
        }
        
        function openFamiCoupon(cid) {
            window.open('../User/FamiCouponDetail.aspx?cid=' + cid, '_blank');
        }
        function slidingMenuToggle() {
            $("#wrap").toggleClass("mbe-menu-show");
        }

        //訂位
        function openBookingSystem(orderGuid, apiKey) {
            if (orderGuid) {
                window.open('../BookingSystem/ReservationService.aspx?ApiKey=' + apiKey + '&gid=' + orderGuid, '_blank');
            } else {
                window.open('../BookingSystem/ReservationRecords.aspx?ApiKey=' + apiKey, '_blank');
            }
        }

        function linkclick(obj) {
            var link = $(obj).find('.OrderSerial a');
            if (link != null && link.length > 0 && $(window).width() < 984) {
                var url = $(link).attr('href');
                window.location = url;
            } else {
                var fami = $(obj).find("input[id*=rpCouponDetail]");
                if ($(fami).attr('id') != undefined) {
                    $(fami).click();
                } 
            }
        }

        function sendGa() {
            ga('send', 'event', '訂單問題', '發問', '');
            return true;
        }

        function redirectInformation(guid) {
            window.open('/User/CouponDetail.aspx?oid=' + guid, '_self');
        }

        function redirectService(guid) {
            window.open("/User/ServiceConversation?issueType=2&no=" + guid, "_self");
        }

        //註冊[申請退貨]按鈕事件
        function btnReturnsEventRegister(deliveryType, orderGuid, orderId) {
            if (deliveryType != '<%=(int)DeliveryType.ToHouse%>') {
                $.ajax({
                    type: "POST",
                    url: "CouponDetail.aspx/IsRefund",
                    data: "{ OrderGuid:'" + orderGuid + "' }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result.d != null) {
                            if (result.d.isAllow && result.d.reson != "") {
                                if (confirm(result.d.reson)) {
                                    location.href = 'UserRefund.aspx?orderid=' + orderId;
                                }
                            } else if (result.d.isAllow) {
                                location.href = 'UserRefund.aspx?orderid=' + orderId;
                            } else {
                                alert(result.d.reson);
                            }
                        }
                    },
                    error: function () {
                        alert('系統繁忙中，請稍後再試!!');
                    }
                });
            } else {
                location.href = 'UserRefund.aspx?orderid=' + orderId;
            }
            return true;

        }

        //註冊[申請換貨]按鈕事件
        function btnExchangeEventRegister(deliveryType, orderId) {
            location.href = 'UserExchange.aspx?orderid=' + orderId;
            return true;
        }
    </script>
    <asp:HiddenField ID="hif_Mobile" runat="server" />
    <asp:HiddenField ID="hif_OrderDate" runat="server" />
    <asp:HiddenField ID="hif_ExpiredTime" runat="server" />
    <asp:HiddenField ID="hif_OrderEndDate" runat="server" />
    <asp:HiddenField ID="hif_ItemName" runat="server" />
    <asp:HiddenField ID="hif_Bid" runat="server" />
    <asp:HiddenField ID="hif_Oid" runat="server" />
    <asp:HiddenField ID="hif_OrderStatus" runat="server" />
    <asp:HiddenField ID="hif_GroupStatus" runat="server" />
    <asp:HiddenField ID="hif_BusinessHourStatus" runat="server" />
    <asp:HiddenField ID="hif_CancelStatus" runat="server" />
    <asp:HiddenField ID="hif_Dep" runat="server" />
    <asp:HiddenField ID="hif_Slug" runat="server" />
    <asp:HiddenField ID="hif_OrderMin" runat="server" />
    <asp:HiddenField ID="hif_SubTotal" runat="server" />
    <asp:HiddenField ID="hif_Total" runat="server" />
    <asp:HiddenField ID="hif_ItemPrice" runat="server" />
    <asp:HiddenField ID="hif_ItemOPrice" runat="server" />
    <asp:HiddenField ID="hif_EinvoiceRequest_Receiver" runat="server" />
    <asp:HiddenField ID="hif_EinvoiceRequest_Address" runat="server" />
    <asp:HiddenField ID="hif_CouponId" runat="server" />
    <asp:HiddenField ID="hif_SmsAlreadyCount" runat="server" />
    <asp:HiddenField ID="hif_SmsRemainCount" runat="server" />
    <asp:HiddenField ID="hif_CurrentQuantity" runat="server" />
    <asp:HiddenField ID="hif_DeliveryType" runat="server" />
    <asp:CheckBox ID="cbx_PanelType" runat="server" Checked="true" Visible="false" />
    <div class="block-background">
        <uc1:Paragraph ID="pMain" ExternalCache="true" runat="server" />
        <asp:RadioButtonList ID="rbl_PanelType" runat="server" Visible="false">
            <asp:ListItem Selected="True" Text="0" Value="0"></asp:ListItem>
            <asp:ListItem Text="1" Value="1"></asp:ListItem>
            <asp:ListItem Text="2" Value="2"></asp:ListItem>
        </asp:RadioButtonList>
        <asp:Panel ID="pan_Main" runat="server">
            <div class="mc-navbar">
                <a>
                    <div class="mc-navbtn on">訂單/憑證</div>
                </a>
                <%if (MGMFacade.IsEnabled) {%>
                <a href="/user/SendGift">
                    <div class="mc-navbtn">禮物盒</div>
                </a>
                <%} %>
                <a href="DiscountList.aspx">
                    <div class="mc-navbtn">折價券</div>
                </a>
                <a href="BonusListNew.aspx">
                    <div class="mc-navbtn">餘額明細</div>
                </a>
                <a href="Collect.aspx">
                    <div class="mc-navbtn">我的收藏</div>
                </a>
                <a href="UserAccount.aspx">
                    <div class="mc-navbtn">帳號設定</div>
                </a>
                <a href="/User/ServiceList">
                    <div class="mc-navbtn rd-C-Hide">客服紀錄</div>
                </a>
            </div>
            <div class="mc-content">
                <h1 class="rd-smll">
                    訂單/憑證列表</h1>
                <hr class="header_hr" />
                <div class="mc-order-states">
                    <% if (DateTime.Now > conf.NewPiinlifeDate)
                       { %>
                    <div id="OrderSavearea">
                        <span style="color: #bf0000; font-weight: normal;"><%= conf.NewPiinlifeDate.ToString("yyyy-MM-dd") %> 以前購買的品生活，請 </span>
                        <span style="color: #08c; font-weight: bold;"><a href="<%=ResolveUrl("~/piinlife/member/OrderList.aspx")%>">查看舊訂單</a></span>
                        <asp:Label ID="lab_HasCreditCardAmount" runat="server" ForeColor="White"></asp:Label>
                    </div>
                    <% } %>
                    <div class="Orderselectarea">
                        篩選分類：
                        <asp:DropDownList ID="ddl_Filter" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChangeFilter">
                        </asp:DropDownList>
                    </div>
                    <div class="RecentOrder">
                        <asp:CheckBox ID="cbx_LatestOrders" runat="server" Text="只顯示近4個月的訂單 " AutoPostBack="true"
                            Checked="true" OnCheckedChanged="ChangeFilter" />
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div id="mc-table">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="rd-table">
                        <asp:Repeater ID="rp_CouponListMain" runat="server" OnItemDataBound="rp_MainItemBound"
                            OnItemCommand="rp_CouponListItemCommand">
                            <HeaderTemplate>
                                <thead>
                                    <tr class="rd-C-Hide">
                                        <th class="OrderDate">
                                            訂購日期
                                        </th>
                                        <th class="OrderSerial" width="90">
                                            訂單編號
                                        </th>
                                        <th class="OrderName">
                                            好康名稱
                                        </th>
                                        <th class="OrderAmount" style="width:30px;">
                                            數量
                                        </th>
                                        <th class="OrderExp">
                                            兌換期限
                                        </th>
                                        <th class="OrderCouponState">
                                            憑證狀態
                                        </th>
                                        <th class="OrderState">
                                            訂單狀態
                                        </th>
                                        <th>
                                            換貨<br />退貨
                                        </th>
                                        <th>
                                            客戶服務
                                        </th>
                                        <th>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="mc-tableContentITEM">
                                    <td class="OrderDate">
                                        <span class="rd-Detailtitle" onclick="linkclick($(this).parent().parent());">訂購日期：</span>
                                        <asp:Label ID="lab_orderedTime" runat="server"></asp:Label>
                                        <asp:Button ID="btnBookingSystem2" runat="server" CssClass="btn btn-mini rd-btn" Text="訂位" Visible="false" />

                                    </td>
                                    <td class="OrderSerial">
                                        <asp:Label ID="lab_rpOrderId" runat="server" Text="<%# ((ViewCouponListMain)(Container.DataItem)).OrderId %>"></asp:Label>
                                        <asp:LinkButton ID="lbt_rpOrderDetail" runat="server" Text="<%# ((ViewCouponListMain)(Container.DataItem)).OrderId %>"
                                            CommandArgument="<%# ((ViewCouponListMain)(Container.DataItem)).Guid %>" CommandName="ShowDetail"></asp:LinkButton>
                                        <asp:Button ID="btnOrderDetail" runat="server" CssClass="btn btn-mini rd-btn" Visible="false"
                                            CommandArgument="<%# ((ViewCouponListMain)(Container.DataItem)).Guid %>" CommandName="ShowDetail"
                                            Text="明細" /><br />
                                        <asp:Button ID="btnBookingSystem" runat="server" CssClass="btn btn-mini rd-btn" Text="訂位" Visible="false" />
                                    </td>
                                    <td class="OrderName">
                                        <div id="mc-tableContentTitleLeft">
                                            <a href="../deal/<%# ((ViewCouponListMain)(Container.DataItem)).BusinessHourGuid %><%# (((ViewCouponListMain)(Container.DataItem)).Status & (int)GroupOrderStatus.FamiDeal) > 0 ? "?cid=" + PponCityGroup.DefaultPponCityGroup.Family.CityId : "" %>" style="color: Black"
                                                target="_blank">
                                                <%# ((ViewCouponListMain)(Container.DataItem)).ItemName.TrimToMaxLength(17,"...") %>
                                            </a>
                                        </div>
                                        <div id="mc-tableContentTitleRight" <%#((((ViewCouponListMain)(Container.DataItem)).Status & (int)GroupOrderStatus.FamiDeal) > 0)?"class='tag-visable'":"" %> >
                                            <span id="pItemPrice" runat="server">
                                                好康價:
                                                <%#  (((ViewCouponListMain)(Container.DataItem)).BusinessHourStatus & (int)LunchKingSite.Core.BusinessHourStatus.PriceZeorShowOriginalPrice) > 0?
                                                                                                                            ((ViewCouponListMain)(Container.DataItem)).ItemOrigPrice.ToString("F0") : ((ViewCouponListMain)(Container.DataItem)).ItemPrice.ToString("F0")%>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="OrderAmount" onclick="linkclick($(this).parent());">
                                        <%# ((ViewCouponListMain)(Container.DataItem)).TotalCount != 0 ? ((ViewCouponListMain)(Container.DataItem)).TotalCount.ToString() : string.Empty %>
                                    </td>
                                    <td class="OrderExp" onclick="linkclick($(this).parent());">
                                        <span class="rd-Detailtitle">兌換期限：</span>
                                       <%# GetExpireDate(((ViewCouponListMain)(Container.DataItem))) %>
                                       <%# GetFinalExpireDateContent(((ViewCouponListMain)(Container.DataItem)))%>
                                        <br />
                                    </td>
                                    <td class="OrderCouponState">
                                        <span class="rd-float-lft" onclick="linkclick($(this).parent().parent());">
                                            <asp:Label ID="lab_rpUseCount" runat="server"></asp:Label></span><br />
                                            <asp:Label ID="lbl_information" runat="server"></asp:Label>
                                        <asp:Button ID="lbt_rpCouponDetail" runat="server" CssClass="btn btn-mini btn-primary rd-c1-mcorbtn rd-c1-mcorbtn-mini"
                                            CommandArgument="<%# ((ViewCouponListMain)(Container.DataItem)).Guid %>" CommandName="ShowDetail"
                                            Text="索取憑證" />
                                        <asp:Button ID="lbt_rpMCouponEvaluate" runat="server" OnClientClick="linkclick($(this).parent().parent());" CssClass="btn btn-mini btn-green rd-float-lft display_none rd-DisplayPanel"
                                            CommandArgument="<%# ((ViewCouponListMain)(Container.DataItem)).Guid %>" CommandName="ShowDetail"
                                            Text="填評價★" />
                                    </td>
                                    <td class="OrderState">
                                        <div class="LastDate">
                                            <asp:Button ID="lbt_rp_PayATM" runat="server" CssClass="btn btn-mini btn-primary"
                                                CommandArgument="<%# ((ViewCouponListMain)(Container.DataItem)).Guid %>" CommandName="ShowDetail"
                                                Visible="false" Text="繼續付款" />
                                            <asp:Label ID="lab_rpOrderStatus" runat="server"></asp:Label>
                                        </div>
                                        <asp:Button ID="lbt_rpCouponEvaluate" runat="server" OnClientClick="linkclick($(this).parent().parent());" CssClass="btn btn-mini btn-green rd-C-Hide"
                                            CommandArgument="<%# ((ViewCouponListMain)(Container.DataItem)).Guid %>" CommandName="ShowDetail"
                                            Text="填評價★" />
                                    </td>
                                    <td class="rd-C-Hide">
                                        <asp:Label ID="lbl_exchangeAndReturn" runat="server"></asp:Label>
                                    </td>
                                    <td class="rd-C-Hide">
                                        <asp:Label ID="lbl_contactService" runat="server"></asp:Label>
                                    </td>
                                    <td class="OrderServiceFunction-mb display_none">
                                        <asp:Label ID="lbl_mbutton" runat="server"></asp:Label>
                                    </td>
                                    <td><span class="OrderArrow_btn_box" onclick="linkclick($(this).parent().parent());"></span></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody> </table>
                            </FooterTemplate>
                        </asp:Repeater>                        
                    </table>
                </div>
                <uc1:Pager ID="gridPager" runat="server" PageSize="10" OnGetCount="RetrieveOrderCount"
                    OnUpdate="UpdateHandler" />
            </div>
        </asp:Panel>
    </div>
</asp:Content>

using System;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using log4net;
using System.Web;
using System.Web.Security;

namespace LunchKingSite.Web.User
{
    public partial class sso : BasePage
    {
        private IMemberProvider mp;
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        protected void Page_Load(object sender, EventArgs e)
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            if (!Page.IsPostBack)
            {
                if (FacebookSSOValidation()) //So far Facebook Connect doesn't implement GET parameter mechanism for us, therefore we should place this validation at the last to avoid possible issues like logging in from PEZ but another browser tab is a logged in Facebook page!
                {
                    FacebookUser fbuser = FacebookUser.Get(Request["atk"]);

                    if (fbuser == null)
                    {
                        LoadFailurePanel();
                    }
                    else
                    {
                        Session[LkSiteSession.AccessToken.ToString()] = Request["atk"];
                        VerifyAndRedirectUser(SingleSignOnSource.Facebook, fbuser);
                    }
                }
                else if (LineSSOValidation())
                {
                    LineUser lineUser = LineUser.Get(Request["atk"]);
                    if (lineUser == null)
                    {
                        LoadFailurePanel();
                    }
                    else
                    {
                        Session[LkSiteSession.AccessToken.ToString()] = Request["atk"];
                        VerifyAndRedirectUser(SingleSignOnSource.Line, lineUser);
                    }
                }
                else
                {
                    // block bots
                    if (!string.IsNullOrEmpty(Request.UserAgent) && (Request.UserAgent.IndexOf("Trend Micro WTP") >= 0 || Request.UserAgent.IndexOf("Yahoo! Slurp") >= 0))
                    {
                        return;
                    }

                    LoadFailurePanel();
                }
            }
        }

        private bool FacebookSSOValidation()
        {
            return !string.IsNullOrWhiteSpace(Request["atk"]) && !string.IsNullOrWhiteSpace(Request["source"])
                && Request["source"].ToString() == ((int)SingleSignOnSource.Facebook).ToString();
        }

        private bool LineSSOValidation()
        {
            return !string.IsNullOrWhiteSpace(Request["atk"]) && !string.IsNullOrWhiteSpace(Request["source"])
                && Request["source"].ToString() == ((int)SingleSignOnSource.Line).ToString();
        }

        private void VerifyAndRedirectUser(SingleSignOnSource source, ExternalUser user)
        {
            ExternalMemberInfo memDict = new ExternalMemberInfo();
            Session[LkSiteSession.ExternalMemberId.ToString()] = memDict;
            memDict[source] = user.Id;

            string redirUrl;

            MemberLink link = mp.MemberLinkGetByExternalId(user.Id, source);
            if (link.IsLoaded)
            {
                //如果使用者還有登入類別尚未串接，且沒有勾選不要詢問的話，就轉址到串接頁面
                Member mem = mp.MemberGet(link.UserId);

                if (mem.IsLoaded == false)
                {
                    throw new MemberAccessException("member [" + link.UserId + "] does not exist!");
                }
                //之後使用者可以自己更換大頭照，如果原有圖片不是fb，就不再自動替換
                if (mem.Pic != user.Pic &&
                    (mem.Pic == null || mem.Pic.Contains("facebook", StringComparison.OrdinalIgnoreCase)))
                {
                    mem.Pic = user.Pic;
                    mp.MemberPicSet(mem.UniqueId, mem.Pic);
                }

                redirUrl = GetRedirUrlByMember(source, link, mem);

                if (!string.IsNullOrEmpty(Request["v"]) && Request["v"].Replace("?FromId=pez", "") == "r")
                {
                    redirUrl = ResolveUrl("~/NewMember/ConfirmMember");
                }
            }
            else
            {
                redirUrl = ResolveUrl("~/NewMember/ConfirmMember");

                if (!string.IsNullOrWhiteSpace(Request["accessCode"]))
                {
                    redirUrl += "?accessCode=" + Request["accessCode"];
                }
            }

            Response.Redirect(redirUrl);
        }

        private string GetRedirUrlByMember(SingleSignOnSource source, MemberLink link, Member mem)
        {
            string redirUrl;
            var status = MemberUtility.GetMemberSignInStatus(link.UserId);

            if (status == SignInReply.Success)
            {
                NewMemberUtility.UserSignIn(MemberFacade.GetUserName(link.UserId), source, false);
                if ((Session[LkSiteSession.NowUrl.ToString()] != null) &&
                (!string.IsNullOrEmpty(Session[LkSiteSession.NowUrl.ToString()].ToString().Trim())))
                {
                    redirUrl = Session[LkSiteSession.NowUrl.ToString()].ToString();
                }
                else
                {
                    redirUrl = ResolveUrl("~/ppon/");
                }

                MobileMember mm = mp.MobileMemberGet(mem.UniqueId);
                if (mm.IsLoaded && mm.Status == (int)MobileMemberStatusType.None)
                {
                    redirUrl = Helper.CombineUrl(config.SSLSiteUrl,
                  "NewMember/ForgetPassword.aspx?account=" + HttpUtility.UrlEncode(mem.UserEmail));
                }

            }
            else if (status == SignInReply.EmailInactive)
            {
                Session["RegisterByEmailWaitConfirmMssage"] = "請啟用帳號後，再重新綁定";
                MemberAuthInfo mai = mp.MemberAuthInfoGet(mem.UniqueId);
                string authKey = mai.AuthKey;
                redirUrl = Helper.CombineUrl(config.SSLSiteUrl, "/mvc/NewMember/RegisterByEmailWaitConfirm?send=1&key=" + authKey);
            }
            else if (status == SignInReply.MobileInactive)
            {
                redirUrl = Helper.CombineUrl(config.SSLSiteUrl,
                   "NewMember/ForgetPassword.aspx?account=" + HttpUtility.UrlEncode(mem.UserEmail));
            }
            else
            {
                redirUrl = FormsAuthentication.LoginUrl;
            }

            return redirUrl;
        }

        #region class methods

        protected void LoadFailurePanel()
        {
            phFail.Visible = true;
            Session.Remove(LkSiteSession.ExternalMemberId.ToString("g"));
        }

        #endregion class methods
    }
}
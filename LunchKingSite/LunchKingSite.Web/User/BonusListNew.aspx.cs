﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.Web.User
{
    public partial class BonusListNew : MemberPage, IUserBounsListPponView
    {
        private IOrderProvider odrProv;
        private IPponProvider pponProv;
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public event EventHandler GoBackToListClicked;
        public event EventHandler UnbindTaishinPay;

        public event EventHandler<DataEventArgs<int>> PageChange;

        #region props

        private UserBonusListPponPresenter _presenter;
        /// <summary>
        /// 解除
        /// </summary>
        public const string _STR_UNBIND = "解除";
        /// <summary>
        /// 開通
        /// </summary>
        public const string _STR_BIND = "開通";

        public UserBonusListPponPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get
            {
                return this.Page.User.Identity.Name;
            }
        }

        public Dictionary<BounsListType, int> PageCount
        {
            get
            {
                if (ViewState["pagecountlist"] == null)
                {
                    Dictionary<BounsListType, int> pagecount = new Dictionary<BounsListType, int>();
                    pagecount[BounsListType.Scash] = 1;
                    pagecount[BounsListType.Bouns] = 1;
                    pagecount[BounsListType.Pcash] = 1;
                    pagecount[BounsListType.OldScash] = 1;
                    pagecount[BounsListType.Discount] = 1;
                    ViewState["pagecountlist"] = pagecount;
                    return pagecount;
                }
                else
                {
                    return (Dictionary<BounsListType, int>)ViewState["pagecountlist"];
                }
            }
            set
            {
                ViewState["pagecountlist"] = value;
            }
        }

        public Dictionary<BounsListType, int> CurrentPage
        {
            get
            {
                if (ViewState["currentpagelist"] == null)
                {
                    Dictionary<BounsListType, int> currentpage = new Dictionary<BounsListType, int>();
                    currentpage[BounsListType.Scash] = 1;
                    currentpage[BounsListType.Bouns] = 1;
                    currentpage[BounsListType.Pcash] = 1;
                    currentpage[BounsListType.OldScash] = 1;
                    currentpage[BounsListType.Discount] = 1;
                    ViewState["currentpagelist"] = currentpage;
                    return currentpage;
                }
                else
                {
                    return (Dictionary<BounsListType, int>)ViewState["currentpagelist"];
                }
            }
            set
            {
                ViewState["currentpagelist"] = value; ;
            }
        }

        public Dictionary<BounsListType, int> PageSize
        {
            get
            {
                if (ViewState["pagesizelist"] == null)
                {
                    Dictionary<BounsListType, int> pagesize = new Dictionary<BounsListType, int>();
                    pagesize[BounsListType.Scash] = 1;
                    pagesize[BounsListType.Bouns] = 1;
                    pagesize[BounsListType.Pcash] = 1;
                    pagesize[BounsListType.OldScash] = 1;
                    pagesize[BounsListType.Discount] = 1;
                    ViewState["pagesizelist"] = pagesize;
                    return pagesize;
                }
                else
                {
                    return (Dictionary<BounsListType, int>)ViewState["pagesizelist"];
                }
            }
            set
            {
                ViewState["pagesizelist"] = value; ;
            }
        }

        public void TransferToPponDealPage(string bid)
        {
            Response.Redirect(WebUtility.GetSiteRoot() + "/ppon/default.aspx?bid=" + bid);
        }

        public void PopulateGeneralScashRecords(List<UserScashTransactionInfo> records)
        {
            //顯示購物金的資料
            repScash.DataSource = records;
            repScash.DataBind();
        }

        public void Populate_rro2(ViewMemberPromotionTransactionCollection records_2)
        {
            rro2.DataSource = records_2;
            rro2.DataBind();
        }

        public void Populate_SumValue(decimal scash, decimal scashE7, decimal scashPez, decimal userTotalBonusPoint, 
            decimal userTaishinPayCashPoint, bool isTaishinPayLinked)
        {
            litScash.Text = litScash2.Text = scash.ToString("N0");
            litScashE7.Text = litScashE72.Text = scashE7.ToString("N0");
            litScashPez.Text = litScashPez2.Text = scashPez.ToString("N0");
            lbl_Sumbo.Text = lbl_Sumbo2.Text = Math.Floor(userTotalBonusPoint / 10).ToString("F0");
            if (config.EnableTaishinThirdPartyPay)
            {
                phTaishinPayInfo.Visible = true;
                if (isTaishinPayLinked)
                {
                    lbl_TaishinCashPoint.Text = userTaishinPayCashPoint.ToString("F0");
                    btnUnbindTaishinPay.Text = _STR_UNBIND;
                    btnUnbindTaishinPay.CssClass = "";
                    btnUnbindTaishinPay.OnClientClick = 
                        "if (confirm('確定要解除帳戶串接? 解除後您將無法使用此儲值支付帳戶付款!')==false) {return false;}";
                }
                else
                {
                    lbl_TaishinCashPoint.Text = "-";
                    btnUnbindTaishinPay.Text = _STR_BIND;
                    btnUnbindTaishinPay.CssClass = "btn-primary";
                    btnUnbindTaishinPay.OnClientClick = "";
                }
            }
            else
            {
                phTaishinPayInfo.Visible = false;
            }
        }

        #endregion props

        protected void Page_Load(object sender, EventArgs e)
        {
            odrProv = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }
            if (!Page.IsPostBack)
            {
                PageSize[BounsListType.Scash] = gridPager1.PageSize;
                PageSize[BounsListType.Bouns] = gridPager2.PageSize;
                ViewState["pagesizelist"] = PageSize;
                Presenter.OnViewInitialized();
            }

            Presenter.OnViewLoaded();
        }

        protected string ReplaceDescriptionContent(string descriptionContent, string oldValue, string newValue)
        {
            if (oldValue != null && descriptionContent.IndexOf(oldValue) > -1)
            {
                return descriptionContent.Replace(oldValue, newValue == null ? string.Empty : newValue);
            }
            else
            {
                return descriptionContent;
            }
        }

        protected void old_rro_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewMemberCashpointList)
            {
                ViewMemberCashpointList data = (ViewMemberCashpointList)e.Item.DataItem;
                Label cdt = (Label)e.Item.FindControl("lbl_CDT");
                Label description = (Label)e.Item.FindControl("lbl_description");
                Label income = (Label)e.Item.FindControl("lbl_InCome");
                Label outcome = (Label)e.Item.FindControl("lbl_OutCome");
                Label surplus = (Label)e.Item.FindControl("lbl_Surplus");
                Label memo = (Label)e.Item.FindControl("lbl_Memo");

                cdt.Text = data.CreateTime.ToString("yyyy.MM.dd");
                memo.Text = data.Message == "17Life購物金購買(系統)" ? (((data.OrderStatus & (int)LunchKingSite.Core.OrderStatus.ATMOrder) > 0) ? "刷卡" : "ATM") : data.Message;

                string linkUrl = string.Empty;
                if (data.OrderClassification.Equals((int)OrderClassification.HiDeal))
                {
                    linkUrl = ResolveUrl("~/piinlife/member/CouponList.aspx") + "?oid=" + data.Reference;
                }
                else
                {
                    linkUrl = ResolveUrl("~/User/coupon_List.aspx?p=") + MyStatus.OrderDetail.ToString() + "&o=" + data.Reference;
                }

                if (data.Type == 0)
                {
                    description.Text = data.ScashlistMessage.TrimToMaxLength(30, "...");
                    income.Text = data.Amount.ToString("F0");
                    surplus.Text = data.Sumamount.Value.ToString("F0");
                }
                else if (data.Type == 1)
                {
                    description.Text = "<a style='color:Black' href='" + linkUrl + "'>" + (string.IsNullOrEmpty(data.ScashlistMessage) ? "支付:" : (data.ScashlistMessage.Contains("刷退") ? "轉刷退:" : "支付:")) + (string.IsNullOrEmpty(data.ItemName) ? data.ScashlistMessage.TrimToMaxLength(30, "...") : data.ItemName.TrimToMaxLength(30, "...")) + "</a>";
                    outcome.Text = (data.Amount * -1).ToString("F0");
                    surplus.Text = data.Sumamount.Value.ToString("F0");
                }
                else
                {
                    description.Text = "<a style='color:Black' href='" + linkUrl + "'>取消訂單:" + (string.IsNullOrEmpty(data.ItemName) ? data.ScashlistMessage.TrimToMaxLength(30, "...") : data.ItemName.TrimToMaxLength(30, "...")) + "</a>";
                    income.Text = data.Amount.ToString("F0");
                    surplus.Text = data.Sumamount.Value.ToString("F0");
                }
            }
        }

        protected void repScash_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is UserScashTransactionInfo)
            {
                UserScashTransactionInfo data = (UserScashTransactionInfo)e.Item.DataItem;
                //調整購物金支付顯示(原帶店家名稱改帶好康券短標)
                //data.Message = ReplaceDescriptionContent(data.Message, data.SellerName, data.CouponUsage);
                Label cdt = (Label)e.Item.FindControl("lbl_CDT");   //異動日期
                Label description = (Label)e.Item.FindControl("lbl_description");   //摘要
                HtmlAnchor description_link = (HtmlAnchor)e.Item.FindControl("description_link");
                Label income = (Label)e.Item.FindControl("lbl_InCome"); //存入
                Label outcome = (Label)e.Item.FindControl("lbl_OutCome");   //支出
                Label surplus = (Label)e.Item.FindControl("lbl_Surplus");   //餘額
                Label memo = (Label)e.Item.FindControl("lbl_Memo"); //備註

                cdt.Text = data.CreateTime.ToString("yyyy.MM.dd");
                surplus.Text = data.SubTotal.HasValue ? data.SubTotal.Value.ToString("F0") : string.Empty;

                //string linkUrl = string.Empty;
                //if (data.OrderClassification.Equals((int)OrderClassification.HiDeal))
                //{
                //    linkUrl = ResolveUrl("~/piinlife/member/CouponList.aspx") + "?oid=" + data.OrderGuid;
                //}
                //else
                //{
                //    linkUrl = ResolveUrl("~/User/coupon_List.aspx?p=") + MyStatus.OrderDetail.ToString() + "&o=" + data.OrderGuid;
                //}

                if (data.Amount > 0)
                {
                    income.Text = data.Amount.HasValue ? data.Amount.Value.ToString("F0") : "";
                    description.Text = data.Message;
                    //description_link.Style.Add(HtmlTextWriterStyle.TextDecoration, "none");
                }
                else
                {
                    //memo.Text = Helper.IsFlagSet(data.OrderStatus ?? 0, LunchKingSite.Core.OrderStatus.ATMOrder) ? "ATM" : "";
                    outcome.Text = data.Amount.HasValue ? (-1 * data.Amount.Value).ToString("F0") : "";
                    description.Text = "支付:" + data.Message;
                    //description_link.HRef = linkUrl;
                }
            }
        }

        protected void rro2_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewMemberPromotionTransaction)
            {
                ViewMemberPromotionTransaction data = (ViewMemberPromotionTransaction)e.Item.DataItem;
                data.Action = ReplaceDescriptionContent(data.Action, data.CouponEventContentName, data.CouponUsage);
                Label cdt = (Label)e.Item.FindControl("lbl_CDT");
                Label sdt = (Label)e.Item.FindControl("lbl_SDT");
                Label edt = (Label)e.Item.FindControl("lbl_EDT");
                Label description = (Label)e.Item.FindControl("lbl_description");
                Label income = (Label)e.Item.FindControl("lbl_InCome");
                Label outcome = (Label)e.Item.FindControl("lbl_OutCome");
                Label surplus = (Label)e.Item.FindControl("lbl_Surplus");
                Label memo = (Label)e.Item.FindControl("lbl_Memo");

                cdt.Text = data.CreateTime.ToString("yyyy.MM.dd");
                sdt.Text = data.WithdrawalOrderGuid == null ? data.StartTime.ToString("yyyy.MM.dd HH:mm") : data.CreateTime.ToString("yyyy.MM.dd HH:mm");
                edt.Text = (data.ExpireTime.Hour == 0 && data.ExpireTime.Minute == 0) ? (data.ExpireTime.AddSeconds(-1)).ToString("yyyy.MM.dd HH:mm") : data.ExpireTime.ToString("yyyy.MM.dd HH:mm");
                int bonusType = -1;
                if (data.Type != null)
                {
                    bonusType = data.Type.Value;
                }

                switch ((BonusTransactionType)bonusType)
                {
                    case BonusTransactionType.SystemAdjustment:
                        description.Text = "系統新增:" + data.Action.TrimToMaxLength(25, "...");
                        break;

                    case BonusTransactionType.ManualAdjustment:
                        description.Text = "紅利調整:" + data.Action.TrimToMaxLength(25, "...");
                        break;

                    case BonusTransactionType.UserRedeeming:
                        description.Text = "紅利兌換:" + data.Action.TrimToMaxLength(25, "...");
                        break;

                    case BonusTransactionType.OrderAmtRedeeming:
                        description.Text = "<a style='color:Black' href='" + ResolveUrl("~/User/coupon_List.aspx?p=") + MyStatus.OrderDetail.ToString() + "&o=" + data.WithdrawalOrderGuid + "'>" + data.Action.TrimToMaxLength(25, "...") + "</a>";
                        break;

                    default:
                        description.Text = data.WithdrawalOrderGuid != null ? ("<a style='color:Black' href='" + ResolveUrl("~/User/coupon_List.aspx?p=") + MyStatus.OrderDetail.ToString() + "&o=" + data.WithdrawalOrderGuid + "'>" + data.Action.TrimToMaxLength(25, "...") + "</a>") : data.Action.TrimToMaxLength(25, "...");
                        break;
                }
                double promovalue = data.PromotionValue ?? 0;
                if (promovalue < 0)
                {
                    outcome.Text = ((promovalue / 10) * (-1)).ToString();
                }
                else
                {
                    income.Text = (promovalue / 10).ToString();
                }
                surplus.Text = data.RunSum == null ? "0" : Math.Floor(data.RunSum.Value / 10).ToString("N0");
            }
        }

        protected void lbGoBack_Click(object sender, EventArgs e)
        {
            if (GoBackToListClicked != null)
            {
                GoBackToListClicked(sender, e);
            }
        }

        protected void UpdateHandler1(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);

            CurrentPage[BounsListType.Scash] = pageNumber;
            ViewState["currentpagelist"] = CurrentPage;

            if (this.PageChange != null)
            {
                this.PageChange(BounsListType.Scash, e);
            }
        }

        protected int RetrieveOrderCount1()
        {
            return PageCount[BounsListType.Scash];
        }

        protected void UpdateHandler2(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);

            CurrentPage[BounsListType.Bouns] = pageNumber;
            ViewState["currentpagelist"] = CurrentPage;

            if (this.PageChange != null)
            {
                this.PageChange(BounsListType.Bouns, e);
            }
        }

        protected int RetrieveOrderCount2()
        {
            return PageCount[BounsListType.Bouns];
        }

        protected void UpdateHandler3(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);

            CurrentPage[BounsListType.Pcash] = pageNumber;
            ViewState["currentpagelist"] = CurrentPage;

            if (this.PageChange != null)
            {
                this.PageChange(BounsListType.Pcash, e);
            }
        }

        protected int RetrieveOrderCount3()
        {
            return PageCount[BounsListType.Pcash];
        }

        protected string GetSummary(Guid oid, int paymentType, int transType)
        {
            string summary = string.Empty;

            switch ((PayTransType)transType)
            {
                case PayTransType.Authorization:
                    if ((LunchKingSite.Core.PaymentType)paymentType == LunchKingSite.Core.PaymentType.Creditcard)
                    {
                        summary = Resources.Localization.PayTransAuthorization + "：";
                    }
                    else
                    {
                        summary = Resources.Localization.PayTransCharging + "：";
                    }
                    break;

                case PayTransType.Charging:
                    summary = Resources.Localization.PayTransCharging + "：";
                    break;

                case PayTransType.Refund:
                    summary = Resources.Localization.PayTransRefund + "：";
                    break;

                default:
                    break;
            }

            Order o = odrProv.OrderGet(oid);
            Guid pid = (o != null) ? (Guid)o.ParentOrderId : new Guid();
            ViewPponDeal d = pponProv.ViewPponDealGet(pid);
            summary += (d != null) ? d.CouponUsage : string.Empty;

            return summary;
        }

        protected string GetRefundAmount(int transType, decimal amount, PayTransType type)
        {
            return ((PayTransType)transType == type) ? amount.ToString("F0") : string.Empty;
        }

        #region Discount 折價券

        protected void UpdateHandler4(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            CurrentPage[BounsListType.Discount] = pageNumber;
            ViewState["currentpagelist"] = CurrentPage;

            if (this.PageChange != null)
            {
                this.PageChange(BounsListType.Discount, e);
            }
        }

        protected int RetrieveOrderCount4()
        {
            return PageCount[BounsListType.Discount];
        }

        protected void rro4_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewDiscountDetail)
            {
                ViewDiscountDetail discountcode = (ViewDiscountDetail)e.Item.DataItem;
                Label clab_status = (Label)(e.Item.FindControl("lab_status"));
                if (discountcode.UseTime.HasValue)
                {
                    clab_status.Text = "<span style=\"color:gray;\">" + Resources.Localization.CouponCodeUsed + "</span>";
                }
                else if ((discountcode.EndTime ?? new DateTime()) < DateTime.Now)
                {
                    clab_status.Text = "<span style=\"color:#800517;\">" + Resources.Localization.CouponCodeExpired + "</span>";
                }
                else if (discountcode.CancelTime.HasValue)
                {
                    clab_status.Text = "<span style=\"color:gray;\">" + Resources.Localization.CouponCodeCancelled + "</span>";
                }
                else if (!discountcode.UseTime.HasValue)
                {
                    clab_status.Text = "<span style=\"color:#6698FF;\">" + Resources.Localization.CouponCodeNotUsed + "</span>";
                }
            }
        }

        #endregion Discount 折價券

        protected void btnUnbindTaishinPay_Click(object sender, EventArgs e)
        {
            if (this.UnbindTaishinPay != null)
            {
                Button btn = (Button)sender;
                if (btn.Text == _STR_UNBIND)
                {
                    this.UnbindTaishinPay(sender, e);
                }
                else
                {
                    Response.Redirect(config.SSLSiteUrl + "/mvc/ThirdPartyPay/TaishinBindBegin?returnUrl="+ config.SiteUrl + "/User/bonuslistnew.aspx");
                }
            }
        }
    }
}
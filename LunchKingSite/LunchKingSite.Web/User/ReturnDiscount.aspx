﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReturnDiscount.aspx.cs" Inherits="LunchKingSite.Web.User.ReturnDiscount" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.Core.Component" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Themes/default/images/17Life/G7/ReturnApplication.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="pan_NoData" runat="server" Visible="false">
            <div class="ReturnApplicationForm">
                <div class="ReAppFoInfo">
                    <div class="ReAppFpOffLine">
                        <div class="ReAppFoContent1">
                            <div class="ReAppFoTittle">
                                <asp:Label ID="lab_NoData" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pan_Data" runat="server">
            <div class="ReturnApplicationForm" style="margin-top: 30px; border: none;">
                <% if (IsNeedTempReturnDiscount == true || !IsNewAllowance == false)
                   {%>
                <table border="0" cellpadding="0" cellspacing="0" class="ReAppFoCont3Table1_3" style="border-top-style: none; width: 750px">
                    <tr valign="top">
                        <td style="width: 390px">
                            <p style="font-weight: 700">●填寫並寄回【紙本折讓證明單】</p>
                            <span>開立三聯式發票者，請您將此份[退回或折讓證明單]<br />
                                列印出來，填寫「營利事業統一編號」並蓋上公司發<br />
                                票章，以郵局掛號、Email或傳真方式寄回。<br />
                                (若您開立二聯式發票，建議您使用電子折讓，便不<br />
                                需要將紙本折讓寄回，詳情請洽線上客服。)
                            </span>
                        </td>
                        <td style="width: 10px">&nbsp;&nbsp;</td>
                        <td style="width: 350px">
                            <p style="font-weight: 700">●請注意</p>
                            <span>1. 使用傳真者，請於傳真完成後主動告知您傳真的日期、 時間、 訂單編號等資訊， 我們將盡速確認是否已收到您的文件。</span><br />
                            <span>2. 使用Email寄回者，務必將紙本印出、填妥蓋章，並拍照或掃描成圖檔寄回。請確保圖檔清楚、可辨，避免耽誤退貨程序。</span>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td style="width: 390px">
                            <p style="font-weight: 700">●【17Life退貨處理中心】地址 / 傳真電話</p>
                            <span>寄件地址：104台北市中山區中山北路一段 11號 13樓<br />
                                17Life退貨處理中心收<br />
                                Email : <a href="mailto:csr@17life.com" target="_top">csr@17life.com</a><br />
                                傳真號碼： (02) 2511-9110<br />
                                &nbsp;
                            </span>
                        </td>
                        <td style="width: 10px">&nbsp;&nbsp;</td>
                        <td style="width: 350px">
                            <p style="font-weight: 700">●客服專線</p>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Image ID="Image1" ImageUrl="~/Images/icons/tel.png" runat="server" />
                                    </td>
                                    <td>
                                        <span style="font-size: x-large; font-weight: 700"><%=config.ServiceTel %></span><br />
                                        <span>週一至週五 9:00 ~ 18:00</span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div style="height: 20"></div>
                <asp:Repeater ID="rpt_FormTemp" runat="server">
                    <ItemTemplate>
                        <div class="ReAppFoInfo" style="border: 1px solid #333333;">
                            <div class="ReAppFoContent3">
                                <!--Invoice--1---ST----->
                                <div class="ReAppFoInvoice">
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" class="ReAppFoCont3Table1_3" style="border-top-style: none">
                                        <tr>
                                            <td align="center" style="height: 10"></td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="color: red; font-size: 48px; font-weight: 900; padding-top: 10px">範本
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" height="29" style="font-size: 21px;">
                                                <span style="font-weight: bold; font-family: Arial, Helvetica, sans-serif;">
                                                    <%# ((RefundDiscountListClass)(Container.DataItem)).CouponSequence%>
                                                </span>退回或折讓證明單
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" height="23" style="font-size: 17px;">(<%# ((RefundDiscountListClass)(Container.DataItem)).DealNumber%>)訂單編號：
                                            <%# ((RefundDiscountListClass)(Container.DataItem)).OrderId%>
                                                <img id="iVC" alt="" src="<%# ResolveUrl("~/service/barcode39.ashx") + "?id="+ ((RefundDiscountListClass)(Container.DataItem)).OrderId %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" height="7"></td>
                                        </tr>
                                    </table>
                                    <table border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table">
                                        <tr>
                                            <td width="379" rowspan="2">
                                                <table width="359" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_1">
                                                    <tr>
                                                        <td width="78" align="right" style="padding-right: 6px;">
                                                            <span style="letter-spacing: 3.2em;">名</span>稱
                                                        </td>
                                                        <td width="288" align="center">
                                                            <%# (((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.IsBetween(ProviderFactory.Instance().GetConfig().ContactToPayeasy,ProviderFactory.Instance().GetConfig().PayeasyToContact.AddDays(-1))) ? "康迅數位整合股份有限公司" : "康太數位整合股份有限公司" %> 
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; padding-left: 10px;">統&nbsp;&nbsp;一&nbsp;編&nbsp;&nbsp;號
                                                        </td>
                                                        <td align="center">
                                                            <%# (((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.IsBetween(ProviderFactory.Instance().GetConfig().ContactToPayeasy,ProviderFactory.Instance().GetConfig().PayeasyToContact.AddDays(-1))) ? ProviderFactory.Instance().GetConfig().PayeasyCompanyId : ProviderFactory.Instance().GetConfig().ContactCompanyId %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="padding-right: 6px;">
                                                            <span style="letter-spacing: 3.2em;">地</span>址
                                                        </td>
                                                        <td align="center">台北市中山區中山北路一段11號13樓
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <%if (!IsNewAllowance) { %>
                                            <td width="344" height="60" align="center" style="font-size: 20px;">
                                                <strong>營業人銷貨退回或折讓證明單</strong>
                                            </td>
                                            <%} %>
                                            <%else { %>
                                            <td width="344" height="60" align="center" style="font-size: 15px;">
                                                <strong>電子發票銷貨退回、進貨退出或折讓證明單證明聯</strong>
                                            </td>
                                            <%} %>
                                        </tr>
                                        <tr>
                                            <td align="center" style="line-height: 18px;">
                                                <span style="padding-left:126px;"></span><span class="ReturnFormAlertWord">請填寫填表的日期</span>
                                                <br />
                                                <span style="border: 3px solid DodgerBlue; height:20px;">
                                                    <!--<%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumberDate_Year%>-->
                                                    &nbsp;&nbsp; 年 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <!--<%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumberDate_Month%>-->
                                                    &nbsp;&nbsp; 月 &nbsp;&nbsp;&nbsp;&nbsp&nbsp;
                                                <!--<%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumberDate_Day%>-->
                                                    &nbsp;&nbsp; 日
                                                </span>
                                                <span class="ReturnFormAlertWord" style="">年份請使用西元格式</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right" height="18" style="line-height: 18px; padding-right: 33px; color: #506A00;">第ㄧ聯：交付原銷貨人作為銷項稅額之扣減憑證
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_2">
                                                    <tr>
                                                        <td colspan="6" height="20">開立發票
                                                        </td>
                                                        <td colspan="5">退貨或折讓內容
                                                        </td>
                                                        <td colspan="3" rowspan="2">(打V處)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%if (!IsNewAllowance) { %>
                                                        <td width="3%" rowspan="2">聯式
                                                        <%} %>
                                                        <%else { %>
                                                        <td width="3%" rowspan="2">一般/特種
                                                        <%} %>
                                                        <td width="6%" rowspan="2">年
                                                        </td>
                                                        <td width="3%" rowspan="2">月
                                                        </td>
                                                        <td width="3%" rowspan="2">日
                                                        </td>
                                                        <td width="4%" rowspan="2">字<br />軌
                                                        </td>
                                                        <td width="11%" rowspan="2">號碼
                                                        </td>
                                                        <td width="18%" rowspan="2">品名
                                                        </td>
                                                        <td width="5%" rowspan="2">數量
                                                        </td>
                                                        <td width="6%" rowspan="2">單價
                                                        </td>
                                                        <td colspan="2">退出或折讓
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="19%">金額(不含稅之進貨額)
                                                        </td>
                                                        <td width="9%">營業稅額
                                                        </td>
                                                        <td width="4%">應稅
                                                        </td>
                                                        <td width="6%">零稅率
                                                        </td>
                                                        <td width="3%" style="line-height: 16px;">免稅
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20">
                                                            <%if (!IsNewAllowance) { %>
                                                                <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceMode%>
                                                            <%} %>
                                                            <%else { %>
                                                                一
                                                            <%} %>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.Year%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.Month%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.Day%>
                                                        </td>
                                                        <td>&nbsp;
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumber.Substring(0,2)%>
                                                        </td>
                                                        <td>&nbsp;
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumber.Substring(2,8)%>
                                                        </td>
                                                        <td style="text-align: left; padding-left: 4px;">
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceItemName%>
                                                        </td>
                                                        <td>1
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemAmount%>
                                                        </td>
                                                        <td>$
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemNoTaxAmount%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemTax%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).IsTax ? "v" : "&nbsp;" %>
                                                        </td>
                                                        <td>&nbsp;
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).IsTax ? "&nbsp;" :"v"  %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="9" height="20">合計
                                                        </td>
                                                        <td>$
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemNoTaxAmount%></td>
                                                        <td>$
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemTax%></td>
                                                        <td>&nbsp;
                                                        </td>
                                                        <td>&nbsp;
                                                        </td>
                                                        <td>&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <span class="ReAppFoText1">本證明單所列銷貨退回進貨退出或折讓，確屬事實，特此證明。</span>
                                    <table>
                                        <tr valign="bottom">
                                            <td width="337" align="center"><span class="ReturnFormAlertWord">個人戶，請簽名 / 公司戶：請蓋公司票章</span></td>
                                            <td width="368" align="center"><span class="ReturnFormAlertWord">三聯式發票(公司戶)請務必填寫營利事業統一編號</span></td>
                                        </tr>
                                    </table>
                                    <div>
                                        <table height="80" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="377">
                                                    <table style="border: 3px solid DodgerBlue;">
                                                        <tr>
                                                            <%if (!IsNewAllowance) { %>
                                                            <td width="167" align="right">公司抬頭(或原買受人)：
                                                            </td>
                                                            <%} %>
                                                            <%else { %>
                                                            <td width="167" align="left">&nbsp;&nbsp;簽收人：
                                                            </td>
                                                            <%} %>
                                                            </td>
                                                            <td width="170">&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="15"></td>
                                                            <%if (!IsNewAllowance) { %>
                                                            <td>
                                                                <span style="color: #666;">(請簽章)</span>
                                                            </td>
                                                            <%} %>
                                                            <%else { %>
                                                            <td>
                                                            </td>
                                                            <%} %>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="368">
                                                    <table style="border: 3px solid DodgerBlue;">
                                                        <tr>
                                                            <%if (!IsNewAllowance) { %>
                                                            <td width="368">營利事業統一編號（此欄二聯式發票免填）：
                                                            </td>
                                                            <%} %>
                                                            <%else { %>
                                                            <td width="368">進貨營業人(或原買受人)<br />
                                                                蓋統一發票專用章
                                                            </td>
                                                            <%} %>
                                                        </tr>
                                                        <tr><td height="20">&nbsp;</td></tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <hr style="width: 700px; margin: 15px auto;" />
                                <!--Invoice--1--END-->
                            </div>

                            <div class="ReAppFoContent3">
                                <div class="ReAppFoInvoice">
                                    <table border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table">
                                        <tr>
                                            <td width="379" rowspan="2">
                                                <table width="359" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_1">
                                                    <tr>
                                                        <td width="78" align="right" style="padding-right: 6px;">
                                                            <span style="letter-spacing: 3.2em;">名</span>稱
                                                        </td>
                                                        <td width="288" align="center">
                                                            <%# (((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.IsBetween(ProviderFactory.Instance().GetConfig().ContactToPayeasy,ProviderFactory.Instance().GetConfig().PayeasyToContact.AddDays(-1))) ? "康迅數位整合股份有限公司" : "康太數位整合股份有限公司" %> 
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; padding-left: 10px;">統&nbsp;&nbsp;一&nbsp;編&nbsp;&nbsp;號
                                                        </td>
                                                        <td align="center">
                                                            <%# (((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.IsBetween(ProviderFactory.Instance().GetConfig().ContactToPayeasy,ProviderFactory.Instance().GetConfig().PayeasyToContact.AddDays(-1))) ? ProviderFactory.Instance().GetConfig().PayeasyCompanyId : ProviderFactory.Instance().GetConfig().ContactCompanyId %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="padding-right: 6px;">
                                                            <span style="letter-spacing: 3.2em;">地</span>址
                                                        </td>
                                                        <td align="center">台北市中山區中山北路一段11號13樓
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <%if (!IsNewAllowance) { %>
                                            <td width="344" height="60" align="center" style="font-size: 20px;">
                                                <strong>營業人銷貨退回或折讓證明單</strong>
                                            </td>
                                            <%} %>
                                            <%else { %>
                                            <td width="344" height="60" align="center" style="font-size: 15px;">
                                                <strong>電子發票銷貨退回、進貨退出或折讓證明單證明聯</strong>
                                            </td>
                                            <%} %>
                                        </tr>
                                        <tr>
                                            <td align="center" style="line-height: 18px;">
                                                <span style="padding-left:126px;"></span><span class="ReturnFormAlertWord">請填寫填表的日期</span>
                                                <br />
                                                <span style="border: 3px solid DodgerBlue;">
                                                    <!--<%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumberDate_Year%>-->
                                                    &nbsp;&nbsp; 年 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <!--<%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumberDate_Month%>-->
                                                    &nbsp;&nbsp; 月 &nbsp;&nbsp;&nbsp;&nbsp&nbsp;
                                                    <!--<%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumberDate_Day%>-->
                                                    &nbsp;&nbsp; 日
                                                </span>
                                                <span class="ReturnFormAlertWord">年份請使用西元格式</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right" height="18" style="line-height: 18px; padding-right: 33px; color: #506A00;">第二聯：交付原銷貨人作為記帳憑證
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_2">
                                                    <tr>
                                                        <td colspan="6" height="20">開立發票
                                                        </td>
                                                        <td colspan="5">退貨或折讓內容
                                                        </td>
                                                        <td colspan="3" rowspan="2">(打V處)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%if (!IsNewAllowance) { %>
                                                        <td width="3%" rowspan="2">聯式
                                                        <%} %>
                                                        <%else { %>
                                                        <td width="3%" rowspan="2">一般/特種
                                                        <%} %>
                                                        </td>
                                                        <td width="6%" rowspan="2">年
                                                        </td>
                                                        <td width="3%" rowspan="2">月
                                                        </td>
                                                        <td width="3%" rowspan="2">日
                                                        </td>
                                                        <td width="4%" rowspan="2">字<br />軌
                                                        </td>
                                                        <td width="11%" rowspan="2">號碼
                                                        </td>
                                                        <td width="18%" rowspan="2">品名
                                                        </td>
                                                        <td width="5%" rowspan="2">數量
                                                        </td>
                                                        <td width="6%" rowspan="2">單價
                                                        </td>
                                                        <td colspan="2">退出或折讓
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="19%">金額(不含稅之進貨額)
                                                        </td>
                                                        <td width="9%">營業稅額
                                                        </td>
                                                        <td width="4%">應稅
                                                        </td>
                                                        <td width="6%">零稅率
                                                        </td>
                                                        <td width="3%" style="line-height: 16px;">免稅
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20">
                                                            <%if (!IsNewAllowance) { %>
                                                                <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceMode%>
                                                            <%} %>
                                                            <%else { %>
                                                                一
                                                            <%} %>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.Year%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.Month%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.Day%>
                                                        </td>
                                                        <td>&nbsp;
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumber.Substring(0,2)%>
                                                        </td>
                                                        <td>&nbsp;
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumber.Substring(2,8)%>
                                                        </td>
                                                        <td style="text-align: left; padding-left: 4px;">
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceItemName%>
                                                        </td>
                                                        <td>1
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemAmount%>
                                                        </td>
                                                        <td>$
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemNoTaxAmount%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemTax%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).IsTax ? "v" : "&nbsp;"%>
                                                        </td>
                                                        <td>&nbsp;
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).IsTax ? "&nbsp;" :"v"  %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="9" height="20">合計
                                                        </td>
                                                        <td>$
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemNoTaxAmount%></td>
                                                        <td>$
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemTax%></td>
                                                        <td>&nbsp;
                                                        </td>
                                                        <td>&nbsp;
                                                        </td>
                                                        <td>&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <span class="ReAppFoText1">本證明單所列銷貨退回進貨退出或折讓，確屬事實，特此證明。</span>
                                    <table>
                                        <tr valign="bottom">
                                            <td width="337" align="center"><span class="ReturnFormAlertWord">個人戶，請簽名 / 公司戶：請蓋公司票章</span></td>
                                            <td width="368" align="center"><span class="ReturnFormAlertWord">三聯式發票(公司戶)請務必填寫營利事業統一編號</span></td>
                                        </tr>
                                    </table>
                                    <div>
                                        <table height="80" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="377">
                                                    <table style="border: 3px solid DodgerBlue;">
                                                        <tr>
                                                            <%if (!IsNewAllowance) { %>
                                                            <td width="167" align="right">公司抬頭(或原買受人)：
                                                            </td>
                                                            <%} %>
                                                            <%else { %>
                                                            <td width="167" align="left">&nbsp;&nbsp;簽收人：
                                                            </td>
                                                            <%} %>
                                                            <td width="170">&nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="15"></td>
                                                            <%if (!IsNewAllowance) { %>
                                                            <td>
                                                                <span style="color: #666;">(請簽章)</span>
                                                            </td>
                                                            <%} %>
                                                            <%else { %>
                                                            <td>
                                                            </td>
                                                            <%} %>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="368">
                                                    <table style="border: 3px solid DodgerBlue;">
                                                        <tr>
                                                            <%if (!IsNewAllowance) { %>
                                                            <td width="368">營利事業統一編號（此欄二聯式發票免填）：
                                                            </td>
                                                            <%} %>
                                                            <%else { %>
                                                            <td width="368">進貨營業人(或原買受人)<br />
                                                                蓋統一發票專用章
                                                            </td>
                                                            <%} %>
                                                        </tr>
                                                        <tr><td height="20">&nbsp;</td></tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%# ((RefundDiscountListClass)(Container.DataItem)).breakstring%>
                        <br />
                        <br />
                    </ItemTemplate>
                </asp:Repeater>
                <div style="width: 750px; text-align: center;">-----------------------------------------(請填寫下方表格並寄回)----------------------------------------</div>
                <%} %>
                <asp:Repeater ID="rpt_Form" runat="server">
                    <ItemTemplate>
                        <div class="ReAppFoInfo" style="border: 1px solid #333333;">
                            <div class="ReAppFoContent3">
                                <!--Invoice--1---ST----->
                                <div class="ReAppFoInvoice">
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" class="ReAppFoCont3Table1_3" style="border-top-style: none">
                                        <tr>
                                            <td align="center" height="10"></td>
                                        </tr>
                                        <tr>
                                            <td align="center" height="29" style="font-size: 21px;">
                                                <span style="font-weight: bold; font-family: Arial, Helvetica, sans-serif;">
                                                    <%# ((RefundDiscountListClass)(Container.DataItem)).CouponSequence%>
                                                </span>退回或折讓證明單
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" height="23" style="font-size: 17px;">(<%# ((RefundDiscountListClass)(Container.DataItem)).DealNumber%>)訂單編號：
                                            <%# ((RefundDiscountListClass)(Container.DataItem)).OrderId%>
                                                <img id="iVC" alt="" src="<%# ResolveUrl("~/service/barcode39.ashx") + "?id="+ ((RefundDiscountListClass)(Container.DataItem)).OrderId %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" height="7"></td>
                                        </tr>
                                    </table>
                                    <table border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table">
                                        <tr>
                                            <td width="379" rowspan="2">
                                                <table width="359" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_1">
                                                    <tr>
                                                        <td width="78" align="right" style="padding-right: 6px;">
                                                            <span style="letter-spacing: 3.2em;">名</span>稱
                                                        </td>
                                                        <td width="288" align="center">
                                                            <%# (((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.IsBetween(ProviderFactory.Instance().GetConfig().ContactToPayeasy,ProviderFactory.Instance().GetConfig().PayeasyToContact.AddDays(-1))) ? "康迅數位整合股份有限公司" : "康太數位整合股份有限公司" %> 
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; padding-left: 10px;">統&nbsp;&nbsp;一&nbsp;編&nbsp;&nbsp;號
                                                        </td>
                                                        <td align="center">
                                                            <%# (((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.IsBetween(ProviderFactory.Instance().GetConfig().ContactToPayeasy,ProviderFactory.Instance().GetConfig().PayeasyToContact.AddDays(-1))) ? ProviderFactory.Instance().GetConfig().PayeasyCompanyId : ProviderFactory.Instance().GetConfig().ContactCompanyId %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="padding-right: 6px;">
                                                            <span style="letter-spacing: 3.2em;">地</span>址
                                                        </td>
                                                        <td align="center">台北市中山區中山北路一段11號13樓
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <%if (!IsNewAllowance) { %>
                                            <td width="344" height="60" align="center" style="font-size: 20px;">
                                                <strong>營業人銷貨退回或折讓證明單</strong>
                                            </td>
                                            <%} %>
                                            <%else { %>
                                            <td width="344" height="60" align="center" style="font-size: 15px;">
                                                <strong>電子發票銷貨退回、進貨退出或折讓證明單證明聯</strong>
                                            </td>
                                            <%} %>
                                        </tr>
                                        <tr>
                                            <td align="center" style="line-height: 18px;">
                                                <!--<%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumberDate_Year%>-->
                                                &nbsp;&nbsp; 年 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <!--<%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumberDate_Month%>-->
                                                &nbsp;&nbsp; 月 &nbsp;&nbsp;&nbsp;&nbsp&nbsp;
                                                <!--<%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumberDate_Day%>-->
                                                &nbsp;&nbsp; 日
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right" height="18" style="line-height: 18px; padding-right: 33px; color: #506A00;">第ㄧ聯：交付原銷貨人作為銷項稅額之扣減憑證
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_2">
                                                    <tr>
                                                        <td colspan="6" height="20">開立發票
                                                        </td>
                                                        <td colspan="5">退貨或折讓內容
                                                        </td>
                                                        <td colspan="3" rowspan="2">(打V處)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%if (!IsNewAllowance) { %>
                                                        <td width="3%" rowspan="2">聯式
                                                        <%} %>
                                                        <%else { %>
                                                        <td width="3%" rowspan="2">一般/特種
                                                        <%} %>
                                                        <td width="6%" rowspan="2">年
                                                        </td>
                                                        <td width="3%" rowspan="2">月
                                                        </td>
                                                        <td width="3%" rowspan="2">日
                                                        </td>
                                                        <td width="4%" rowspan="2">字<br />軌
                                                        </td>
                                                        <td width="11%" rowspan="2">號碼
                                                        </td>
                                                        <td width="18%" rowspan="2">品名
                                                        </td>
                                                        <td width="5%" rowspan="2">數量
                                                        </td>
                                                        <td width="6%" rowspan="2">單價
                                                        </td>
                                                        <td colspan="2">退出或折讓
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="19%">金額(不含稅之進貨額)
                                                        </td>
                                                        <td width="9%">營業稅額
                                                        </td>
                                                        <td width="4%">應稅
                                                        </td>
                                                        <td width="6%">零稅率
                                                        </td>
                                                        <td width="3%" style="line-height: 16px;">免稅
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20">
                                                            <%if (!IsNewAllowance) { %>
                                                                <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceMode%>
                                                            <%} %>
                                                            <%else { %>
                                                                一
                                                            <%} %>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.Year%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.Month%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.Day%>
                                                        </td>
                                                        <td>&nbsp;
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumber.Substring(0,2)%>
                                                        </td>
                                                        <td>&nbsp;
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumber.Substring(2,8)%>
                                                        </td>
                                                        <td style="text-align: left; padding-left: 4px;">
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceItemName%>
                                                        </td>
                                                        <td>1
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemAmount%>
                                                        </td>
                                                        <td>$
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemNoTaxAmount%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemTax%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).IsTax ? "v" : "&nbsp;" %>
                                                        </td>
                                                        <td>&nbsp;
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).IsTax ? "&nbsp;" :"v"  %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="9" height="20">合計
                                                        </td>
                                                        <td>$
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemNoTaxAmount%></td>
                                                        <td>$
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemTax%></td>
                                                        <td>&nbsp;
                                                        </td>
                                                        <td>&nbsp;
                                                        </td>
                                                        <td>&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <span class="ReAppFoText1">本證明單所列銷貨退回進貨退出或折讓，確屬事實，特此證明。</span>
                                    <div class="ReAppFoSitBlockOut">
                                        <table height="80" border="0" cellpadding="0" cellspacing="0" class="ReAppFoSitBlockTable">
                                            <tr>
                                                <%if (!IsNewAllowance) { %>
                                                <td width="167" align="right">公司抬頭(或原買受人)：
                                                </td>
                                                <%} %>
                                                <%else { %>
                                                <td width="167" align="left">&nbsp;&nbsp;簽收人：
                                                </td>
                                                <%} %>
                                                <td width="170">&nbsp;
                                                </td>
                                                <%if (!IsNewAllowance) { %>
                                                <td width="368">營利事業統一編號（此欄二聯式發票免填）：
                                                </td>
                                                <%} %>
                                                <%else { %>
                                                <td width="368">進貨營業人(或原買受人)<br />
                                                    蓋統一發票專用章
                                                </td>
                                                <%} %>
                                            </tr>
                                            <tr>
                                                <td height="15"></td>
                                                <%if (!IsNewAllowance) { %>
                                                <td>
                                                    <span style="color: #666;">(請簽章)</span>
                                                </td>
                                                <%} %>
                                                <%else { %>
                                                <td>
                                                </td>
                                                <%} %>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </div>                                    
                                </div>
                                <hr style="width: 700px; margin: 15px auto;" />
                                <!--Invoice--1--END-->
                            </div>

                            <div class="ReAppFoContent3">
                                <div class="ReAppFoInvoice">
                                    <table border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table">
                                        <tr>
                                            <td width="379" rowspan="2">
                                                <table width="359" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_1">
                                                    <tr>
                                                        <td width="78" align="right" style="padding-right: 6px;">
                                                            <span style="letter-spacing: 3.2em;">名</span>稱
                                                        </td>
                                                        <td width="288" align="center">
                                                            <%# (((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.IsBetween(ProviderFactory.Instance().GetConfig().ContactToPayeasy,ProviderFactory.Instance().GetConfig().PayeasyToContact.AddDays(-1))) ? "康迅數位整合股份有限公司" : "康太數位整合股份有限公司" %> 
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; padding-left: 10px;">統&nbsp;&nbsp;一&nbsp;編&nbsp;&nbsp;號
                                                        </td>
                                                        <td align="center">
                                                            <%# (((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.IsBetween(ProviderFactory.Instance().GetConfig().ContactToPayeasy,ProviderFactory.Instance().GetConfig().PayeasyToContact.AddDays(-1))) ? ProviderFactory.Instance().GetConfig().PayeasyCompanyId : ProviderFactory.Instance().GetConfig().ContactCompanyId %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="padding-right: 6px;">
                                                            <span style="letter-spacing: 3.2em;">地</span>址
                                                        </td>
                                                        <td align="center">台北市中山區中山北路一段11號13樓
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <%if (!IsNewAllowance) { %>
                                            <td width="344" height="60" align="center" style="font-size: 20px;">
                                                <strong>營業人銷貨退回或折讓證明單</strong>
                                            </td>
                                            <%} %>
                                            <%else { %>
                                            <td width="344" height="60" align="center" style="font-size: 15px;">
                                                <strong>電子發票銷貨退回、進貨退出或折讓證明單證明聯</strong>
                                            </td>
                                            <%} %>
                                        </tr>
                                        <tr>
                                            <td align="center" style="line-height: 18px;">
                                                <!--<%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumberDate_Year%>-->
                                                &nbsp;&nbsp; 年 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <!--<%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumberDate_Month%>-->
                                                &nbsp;&nbsp; 月 &nbsp;&nbsp;&nbsp;&nbsp&nbsp;
                                                <!--<%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumberDate_Day%>-->
                                                &nbsp;&nbsp; 日
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right" height="18" style="line-height: 18px; padding-right: 33px; color: #506A00;">第二聯：交付原銷貨人作為記帳憑證
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_2">
                                                    <tr>
                                                        <td colspan="6" height="20">開立發票
                                                        </td>
                                                        <td colspan="5">退貨或折讓內容
                                                        </td>
                                                        <td colspan="3" rowspan="2">(打V處)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%if (!IsNewAllowance) { %>
                                                        <td width="3%" rowspan="2">聯式
                                                        <%} %>
                                                        <%else { %>
                                                        <td width="3%" rowspan="2">一般/特種
                                                        <%} %>
                                                        <td width="6%" rowspan="2">年
                                                        </td>
                                                        <td width="3%" rowspan="2">月
                                                        </td>
                                                        <td width="3%" rowspan="2">日
                                                        </td>
                                                        <td width="4%" rowspan="2">字<br />軌
                                                        </td>
                                                        <td width="11%" rowspan="2">號碼
                                                        </td>
                                                        <td width="18%" rowspan="2">品名
                                                        </td>
                                                        <td width="5%" rowspan="2">數量
                                                        </td>
                                                        <td width="6%" rowspan="2">單價
                                                        </td>
                                                        <td colspan="2">退出或折讓
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="19%">金額(不含稅之進貨額)
                                                        </td>
                                                        <td width="9%">營業稅額
                                                        </td>
                                                        <td width="4%">應稅
                                                        </td>
                                                        <td width="6%">零稅率
                                                        </td>
                                                        <td width="3%" style="line-height: 16px;">免稅
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20">
                                                            <%if (!IsNewAllowance) { %>
                                                                <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceMode%>
                                                            <%} %>
                                                            <%else { %>
                                                                一
                                                            <%} %>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.Year%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.Month%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.Day%>
                                                        </td>
                                                        <td>&nbsp;
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumber.Substring(0,2)%>
                                                        </td>
                                                        <td>&nbsp;
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumber.Substring(2,8)%>
                                                        </td>
                                                        <td style="text-align: left; padding-left: 4px;">
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceItemName%>
                                                        </td>
                                                        <td>1
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemAmount%>
                                                        </td>
                                                        <td>$
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemNoTaxAmount%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemTax%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).IsTax ? "v" : "&nbsp;"%>
                                                        </td>
                                                        <td>&nbsp;
                                                        </td>
                                                        <td>
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).IsTax ? "&nbsp;" :"v"  %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="9" height="20">合計
                                                        </td>
                                                        <td>$
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemNoTaxAmount%></td>
                                                        <td>$
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemTax%></td>
                                                        <td>&nbsp;
                                                        </td>
                                                        <td>&nbsp;
                                                        </td>
                                                        <td>&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <span class="ReAppFoText1">本證明單所列銷貨退回進貨退出或折讓，確屬事實，特此證明。</span>
                                    <div class="ReAppFoSitBlockOut">
                                        <table height="66" border="0" cellpadding="0" cellspacing="0" class="ReAppFoSitBlockTable">
                                            <tr>
                                                <%if (!IsNewAllowance) { %>
                                                <td width="167" align="right">公司抬頭(或原買受人)：
                                                </td>
                                                <%} %>
                                                <%else { %>
                                                <td width="167" align="left">&nbsp;&nbsp;簽收人：
                                                </td>
                                                <%} %>
                                                <td width="170">&nbsp;
                                                </td>
                                                <%if (!IsNewAllowance) { %>
                                                <td width="368">營利事業統一編號（此欄二聯式發票免填）：
                                                </td>
                                                <%} %>
                                                <%else { %>
                                                <td width="368">進貨營業人(或原買受人)<br />
                                                    蓋統一發票專用章
                                                </td>
                                                <%} %>
                                            </tr>
                                            <tr>
                                                <td height="15"></td>
                                                <%if (!IsNewAllowance) { %>
                                                <td>
                                                    <span style="color: #666;">(請簽章)</span>
                                                </td>
                                                <%} %>
                                                <%else { %>
                                                <td>
                                                </td>
                                                <%} %>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <table border="0" cellspacing="0" cellpadding="0" class="ReAppFoFooterTable">
                                    <tr>
                                        <td height="10"></td>
                                    </tr>
                                    <tr>
                                        <td>康太數位整合股份有限公司   版權所有 轉載必究 17Life 17life.com
                                            <script language="javascript"> var Today = new Date(); document.write(Today.getFullYear()); </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="5"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <%# ((RefundDiscountListClass)(Container.DataItem)).breakstring%>
                        <br />
                        <br />
                    </ItemTemplate>
                </asp:Repeater>
            </div>

        </asp:Panel>
    </form>
</body>
</html>

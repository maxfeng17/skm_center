﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System.Web.Security;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.Web.User
{
    public partial class einvoice_winner : LocalizedBasePage, IEinvoiceWinner
    {
        ILocationProvider lp = null;
        public event EventHandler<DataEventArgs<KeyValuePair<string, KeyValuePair<string, string>>>> UpdateWinnerInfo = null;
        #region Property
        private EinvoiceWinnerPresenter _presenter;
        public EinvoiceWinnerPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public string UserName
        {
            get { return User.Identity.Name; }
        }
        public int UserId
        {
            get { return MemberFacade.GetUniqueId(User.Identity.Name); }
        }

        #endregion
        #region method
        public void GetEinvoiceMain(EinvoiceMainCollection data)
        {
            if (data.Count > 0)
            {
                if (data.Count(x => x.InvoiceWinnerresponseTime == null) > 0)
                {
                    rp_EinvoiceMains.DataSource = data;
                    rp_EinvoiceMains.DataBind();
                }
                else
                {
                    lbt_Confirm.Enabled = tbx_WinnerName.Enabled = tbx_WinnerPhone.Enabled = ddlcity.Enabled = ddlArea.Enabled = ddlbuilding.Enabled = false;
                    ScriptManager.RegisterStartupScript(this, typeof(Button), "alert", "alert('您已確認中獎發票資訊，中獎的發票將盡速寄送給您，謝謝。')", true);
                }
            }
            //else
            //    Response.Redirect("/ppon/default.aspx");
        }

        public void ShowMessage(string msg)
        {
            lbt_Confirm.Enabled = tbx_WinnerName.Enabled = tbx_WinnerPhone.Enabled = ddlcity.Enabled = ddlArea.Enabled = ddlbuilding.Enabled = false;
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + msg + "');", true);
        }

        public void FinishUpdate()
        {
            Response.Redirect("einvoice_winner.aspx");
        }
        #endregion
        #region event

        #endregion
        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();

            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
                ddlcity.DataSource = CityManager.Citys.Where(x => x.Code != "SYS");
                ddlcity.DataBind();
                ddlcity_SelectedIndexChanged(sender, e);
                ddlArea_SelectedIndexChanged(sender, e);
                ddlbuilding_SelectedIndexChanged(sender, e);
            }
            _presenter.OnViewLoaded();
        }
        protected void ddlcity_SelectedIndexChanged(object sender, EventArgs e)
        {
            CityCollection cc = lp.CityGetList(int.Parse(ddlcity.SelectedItem.Value));
            ddlArea.DataSource = cc;
            ddlArea.DataBind();
            ddlArea.SelectedIndex = 0;
            ddlArea_SelectedIndexChanged(sender, e);
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItemCollection buildings = lp.BuildingGetListInZoneAndPrefix(int.Parse(ddlArea.SelectedValue), "");
            ddlbuilding.DataSource = buildings;
            ddlbuilding.DataBind();
            ddlbuilding.SelectedIndex = 0;
            ddlbuilding_SelectedIndexChanged(sender, e);
        }

        protected void ddlbuilding_SelectedIndexChanged(object sender, EventArgs e)
        {
            aib.BuildingAddress = ddlbuilding.SelectedItem.Text;
            aib.BuildingZoneType = Helper.GetZoneType(int.Parse(ddlArea.SelectedValue));
            if (aib.BuildingZoneType == ZoneType.Company)
            {
                Building b = lp.BuildingGet(new Guid(ddlbuilding.SelectedValue));
                if (!string.IsNullOrEmpty(b.BuildingStreetName) && b.BuildingStreetName != b.BuildingName)
                    aib.BuildingAddress = b.BuildingStreetName;
            }
            aib.CityName = ddlcity.SelectedItem.Text + ddlArea.SelectedItem.Text;
            aib.PresetAddress = null;
            aib.DataBind();
        }

        protected void ConfirmAddress(object sender, EventArgs e)
        {
            aib.BuildingAddress = ddlbuilding.SelectedItem.Text;
            aib.BuildingZoneType = Helper.GetZoneType(int.Parse(ddlcity.SelectedValue));
            if (aib.BuildingZoneType == ZoneType.Company)
            {
                Building b = lp.BuildingGet(new Guid(ddlbuilding.SelectedValue));
                if (!string.IsNullOrEmpty(b.BuildingStreetName) && b.BuildingStreetName != b.BuildingName)
                    aib.BuildingAddress = b.BuildingStreetName;
            }
            aib.CityName = ddlcity.SelectedItem.Text + ddlArea.SelectedItem.Text;
            aib.PresetAddress = null;
            aib.DataBind();
            if (Page.IsValid && UpdateWinnerInfo != null)
                this.UpdateWinnerInfo(sender, new DataEventArgs<KeyValuePair<string, KeyValuePair<string, string>>>(new KeyValuePair<string, KeyValuePair<string, string>>(aib.FullAddress, new KeyValuePair<string, string>(tbx_WinnerName.Text, tbx_WinnerPhone.Text))));
        }
        #endregion

    }

}
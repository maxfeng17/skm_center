﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="refund_form.aspx.cs" Inherits="LunchKingSite.Web.User.refund_form" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.Core.Component" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../Themes/default/images/17Life/G7/ReturnApplication.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pan_NoData" runat="server" Visible="false">
        <div class="ReturnApplicationForm">
            <div class="ReAppFoInfo">
                <div class="ReAppFpOffLine">
                    <div class="ReAppFoContent1">
                        <div class="ReAppFoTittle">
                            <asp:Label ID="lab_NoData" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pan_Data" runat="server">
        <div class="ReturnApplicationForm">
            <div class="ReAppFoTop">
                <asp:Image runat="server" ID="img_Head" ImageUrl="~/Themes/default/images/17Life/G7/ReApFormTop1.png" Width="740" Height="81" AlternateText="17LifeLogo"></asp:Image>
            </div> 
            <div class="ReAppFoInfo">
                <div class="ReAppFpOffLine">
                    <div class="ReAppFoContent1">
                        <div class="ReAppFoTittle">
                            <span style="font-size: 17px;">●</span> 退貨須知</div>
                        <div class="ReAppFoSecondTi">
                            <strong>1.</strong> 申請退貨方式：</div>
                        <div class="ReAppFo">
                            <span class="ReAppFoText">《好康憑證退貨相關規定》</span>
                            <ul>
                                <li><strong>(1)</strong> 若憑證超過優惠期限未使用，可向17Life提出申請全額退費，作業手續費$0； 部分好康憑證所指稱之內容為依據特定單一時間與特定場合所提供之服務(包括但不限於演唱會、音樂會或展覽)，當好康憑證持有人因故未能於該特定時間與特定場合兌換該服務，恕無法要求退費或另行補足差額重現該服務。</li>
                                <li><strong>(2)</strong> 另購買2011/04/01以前上檔的好康，不適用全程退貨保證。請於好康憑證未超過猶豫期7天（以該檔好康 活動時間結束起計算）提出退貨申請，若該張好康憑證超出優惠期限，仍可至店家現場抵票面價值之商品，
                                    請您放心。</li>
                            </ul>
                            <span class="ReAppFoText">《宅配商品退換貨相關規定》</span>
                            <ul>
                                <li>商品在收貨7天鑑賞期內可申請退貨；生鮮食品類需在收貨後24小時內申請退貨。特殊商品退貨規則需依好康權益說明為主。</li>
                                <li><strong>(1)</strong> 如欲退貨，商品必須為全新狀態且包裝完整(包含商品、內外包裝及所有隨付配件等)。</li>
                                <li><strong>(2)</strong> 如商品含附送之贈品，請務必隨商品一併退回。</li>
                                <li><strong>(3)</strong> 如您收到商品時有損壞，請不要拆封，請來信或來電告訴我們您要辦理換貨，以保障您換貨權益。</li>
                            </ul>
                        </div>
                        <div class="ReAppFoSecondTi">
                            <strong>2.</strong> 退款方式及時間：一般退貨處理時間為 10~20 個工作天</div>
                        <table border="0" cellspacing="0" cellpadding="0" class="ReAppFoTwoTable">
                            <tr bgcolor="#646464">
                                <td width="119" height="30" align="center" style="color: #FFF; font-weight: bold;
                                    font-size: 15px;">
                                    付款方式
                                </td>
                                <td width="119" align="center" style="color: #FFF; font-weight: bold; font-size: 15px;">
                                    退款方式
                                </td>
                                <td width="430" align="center" style="color: #FFF; font-weight: bold; font-size: 15px;">
                                    退款工作日
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    刷卡
                                </td>
                                <td align="center">
                                    刷退
                                </td>
                                <td align="left" style="padding-left: 8px;">
                                    收到退款申請書後，以一般退貨處理時間再額外等待30 - 60個工作天退至您刷卡帳戶<br />
                                    ※由於各家銀行的作業時間不一，申請到退款完成可能需等待30~60個工作天
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    ATM轉帳
                                </td>
                                <td align="center">
                                    退款至指定帳戶
                                </td>
                                <td align="left" style="padding-left: 8px;">
                                    收到退款申請書後，以一般退貨處理時間再額外等待30個工作日退款至您的指定帳戶
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    17Life購物金
                                </td>
                                <td align="center">
                                    購物金退款
                                </td>
                                <td align="left" style="padding-left: 8px;">
                                    為一般退貨處理時間為 10~20 個工作天
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    PayEasy購物金
                                </td>
                                <td align="center">
                                    購物金退款
                                </td>
                                <td align="left" style="padding-left: 8px;">
                                    為一般退貨處理時間為 10~20 個工作天
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    17Life 紅利金
                                </td>
                                <td align="center">
                                    紅利金退款
                                </td>
                                <td align="left" style="padding-left: 8px;">
                                    為一般退貨處理時間為 10~20 個工作天
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    17Life折價券
                                </td>
                                <td align="center">
                                    不退款
                                </td>
                                <td align="left" style="padding-left: 8px;">
                                    使用17Life折價券折抵，無論是否退貨皆不得返還
                                </td>
                            </tr>
                        </table>
                        <span style="margin-left: 30px; font-size: 15px;">※收到您的退貨申請書並確認資料無誤後，17Life將會依各別退款方式處理。</span>
                        <table width="640" border="0" cellspacing="0" cellpadding="0" class="ReAppFoTwoTable2">
                            <tr>
                                <td height="28">
                                    若退貨商品發票為紙本發票，請將發票連同退貨申請書簽章一併以掛號方式寄回，否則恕無法辦理退費。
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#646464" class="SignatureTeMark">
                                    請於下方簽名欄位簽章並填妥資料後，掛號寄至17Life或傳真至(02)2511-9110
                                </td>
                            </tr>
                        </table>
                    </div>
                    <asp:Panel ID="pan_ATM" runat="server">
                        <div class="ReAppFoContent2">
                            <div class="ReAppFoTittle">
                                <span style="font-size: 17px;">●</span> 受款退貨金額金融帳戶資料 <span style="font-size: 14px;
                                    margin-left: 4px; font-weight: normal;">請勿直接修改受款內容以免造成退款失敗，如有問題請與17Life客服聯繫</span>
                            </div>
                            <ul>
                                <li>戶名：<asp:Label ID="lab_ATM_AccountName" runat="server"></asp:Label></li>
                                <li>身分證號碼或統編：<asp:Label ID="lab_ATM_UserID" runat="server"></asp:Label></li>
                                <li>銀行名稱：<asp:Label ID="lab_ATM_BankName" runat="server"></asp:Label><span style="margin-left: 160px;">分行別：<asp:Label
                                    ID="lab_ATM_BranchName" runat="server"></asp:Label></span></li>
                                <li>帳號：<asp:Label ID="lab_ATM_Account" runat="server"></asp:Label></li>
                                <li>連絡電話：<asp:Label ID="lab_ATM_Phone" runat="server"></asp:Label></li>
                            </ul>
                        </div>
                    </asp:Panel>
                    <hr />
                </div>
            </div>
        </div>
        <span style="page-break-after: auto"></span>
        <div class="ReturnApplicationForm" style="margin-top: 30px">
            <div class="ReAppFoInfo">
                <asp:Panel ID="pan_Border" runat="server" BorderWidth="4">
                    <asp:Repeater ID="rpt_Form" runat="server">
                        <ItemTemplate>
                            <div class="ReAppFoContent3">
                                <!--Invoice--1---ST----->
                                <div class="ReAppFoInvoice">
                                    <table border="0" align="center" cellpadding="0" cellspacing="0" class="ReAppFoCont3Table1_3" style="border-top-style: none">
                                        <tr>
                                            <td align="center" height="10">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" height="29" style="font-size: 21px;">
                                                <span style="font-weight: bold; font-family: Arial, Helvetica, sans-serif;">
                                                    <%# ((RefundFormListClass)(Container.DataItem)).CouponSequence%>
                                                </span>退回或折讓證明單
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" height="23" style="font-size: 17px;">
                                                訂單編號：
                                            <%# ((RefundFormListClass)(Container.DataItem)).OrderId%>
                                            <img id="iVC" alt="" src="<%# ResolveUrl("~/service/barcode39.ashx") + "?id="+ ((RefundFormListClass)(Container.DataItem)).OrderId %>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" height="7">
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table">
                                        <tr>
                                            <td width="379" rowspan="2">
                                                <table width="359" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_1">
                                                    <tr>
                                                        <td width="78" align="right" style="padding-right: 6px;">
                                                            <span style="letter-spacing: 3.2em;">名</span>稱
                                                        </td>
                                                        <td width="288" align="center">
                                                            康太數位整合股份有限公司
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="padding-right: 6px;">
                                                            <span style="letter-spacing: 0.4em;">統一編</span>號
                                                        </td>
                                                        <td align="center">
                                                            <%# ProviderFactory.Instance().GetConfig().ContactCompanyId%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="padding-right: 6px;">
                                                            <span style="letter-spacing: 3.2em;">地</span>址
                                                        </td>
                                                        <td align="center">
                                                            台北市中山區中山北路一段11號13樓
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%# (((RefundFormListClass)(Container.DataItem)).EinvoiceDate<new DateTime(2011,7,16))?"此折讓金額包含紅利、購物金、Pcash、刷卡金額。":string.Empty%>
                                            </td>
                                            <td width="344" height="60" align="center" style="font-size: 20px;">
                                                <strong>營業人銷貨退回或折讓證明單</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="line-height: 18px;">
                                                <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceNumberDate_Year %> &nbsp;&nbsp;  年 &nbsp;&nbsp;
                                                <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceNumberDate_Month%> &nbsp;&nbsp; 月 &nbsp;&nbsp;
                                                <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceNumberDate_Day%> &nbsp;&nbsp; 日
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right" height="18" style="line-height: 18px; padding-right: 33px;
                                                color: #506A00;">
                                                第ㄧ聯：交付原銷貨人作為銷項稅額之扣減憑證
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_2">
                                                    <tr>
                                                        <td colspan="6" height="20">
                                                            開立發票
                                                        </td>
                                                        <td colspan="5">
                                                            退貨或折讓內容
                                                        </td>
                                                        <td colspan="3" rowspan="2">
                                                            (打V處)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="3%" rowspan="2">
                                                            聯式
                                                        </td>
                                                        <td width="6%" rowspan="2">
                                                            年
                                                        </td>
                                                        <td width="3%" rowspan="2">
                                                            月
                                                        </td>
                                                        <td width="3%" rowspan="2">
                                                            日
                                                        </td>
                                                        <td width="4%" rowspan="2">
                                                            字<br />
                                                            軌
                                                        </td>
                                                        <td width="11%" rowspan="2">
                                                            號碼
                                                        </td>
                                                        <td width="18%" rowspan="2">
                                                            品名
                                                        </td>
                                                        <td width="5%" rowspan="2">
                                                            數量
                                                        </td>
                                                        <td width="6%" rowspan="2">
                                                            單價
                                                        </td>
                                                        <td colspan="2">
                                                            退出或折讓
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="19%">
                                                            金額(不含稅之進貨額)
                                                        </td>
                                                        <td width="9%">
                                                            營業稅額
                                                        </td>
                                                        <td width="4%">
                                                            應稅
                                                        </td>
                                                        <td width="6%">
                                                            零稅率
                                                        </td>
                                                        <td width="3%" style="line-height: 16px;">
                                                            免稅
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20">
                                                            <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceMode%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceDate.Year%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceDate.Month%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceDate.Day%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceNumber.Substring(0,2)%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceNumber.Substring(2,8)%>
                                                        </td>
                                                        <td style="text-align: left; padding-left: 4px;">
                                                            <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceItemName%>
                                                        </td>
                                                        <td>
                                                            1
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).ItemAmount%>
                                                        </td>
                                                        <td>
                                                            $
                                                            <%# ((RefundFormListClass)(Container.DataItem)).ItemNoTaxAmount%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).ItemTax%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).IsTax ? "v" : string.Empty%>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).IsTax ?string.Empty :"v"  %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="9" height="20">
                                                            合計
                                                        </td>
                                                        <td>
                                                            $<%# ((RefundFormListClass)(Container.DataItem)).ItemAmount%></td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <span class="ReAppFoText1">本證明單所列銷貨退回進貨退出或折讓，確屬事實，特此證明。</span>
                                    <div class="ReAppFoSitBlockOut">
                                        <table height="80" border="0" cellpadding="0" cellspacing="0" class="ReAppFoSitBlockTable">
                                            <tr>
                                                <td width="224" align="right">
                                                    買受人姓名(或公司抬頭)：
                                                </td>
                                                <td width="184">
                                                    &nbsp;
                                                </td>
                                                <td width="297">
                                                    身分證字號 (或營利事業統一編號)：
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="15">
                                                </td>
                                                <td>
                                                    <span style="color: #666;">(請簽章)</span>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <hr style="width: 700px; margin: 15px auto;" />
                                <!--Invoice--1--END-->
                            </div>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <div class="ReAppFoContent3">
                                <div class="ReAppFoInvoice">
                                    <table border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table">
                                        <tr>
                                            <td width="379" rowspan="2">
                                                <table width="359" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_1">
                                                    <tr>
                                                        <td width="78" align="right" style="padding-right: 6px;">
                                                            <span style="letter-spacing: 3.2em;">名</span>稱
                                                        </td>
                                                        <td width="288" align="center">
                                                            康太數位整合股份有限公司
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="padding-right: 6px;">
                                                            <span style="letter-spacing: 0.4em;">統一編</span>號
                                                        </td>
                                                        <td align="center">
                                                            <%# ProviderFactory.Instance().GetConfig().ContactCompanyId%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="padding-right: 6px;">
                                                            <span style="letter-spacing: 3.2em;">地</span>址
                                                        </td>
                                                        <td align="center">
                                                            台北市中山區中山北路一段11號13樓
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="344" height="60" align="center" style="font-size: 20px;">
                                                <strong>營業人銷貨退回或折讓證明單</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="line-height: 18px;">
                                                <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceNumberDate_Year %> &nbsp;&nbsp;  年 &nbsp;&nbsp;
                                                <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceNumberDate_Month%> &nbsp;&nbsp; 月 &nbsp;&nbsp;
                                                <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceNumberDate_Day%> &nbsp;&nbsp; 日
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right" height="18" style="line-height: 18px; padding-right: 33px;
                                                color: #506A00;">
                                                第二聯：交付原銷貨人作為記帳憑證
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_2">
                                                    <tr>
                                                        <td colspan="6" height="20">
                                                            開立發票
                                                        </td>
                                                        <td colspan="5">
                                                            退貨或折讓內容
                                                        </td>
                                                        <td colspan="3" rowspan="2">
                                                            (打V處)
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="3%" rowspan="2">
                                                            聯式
                                                        </td>
                                                        <td width="6%" rowspan="2">
                                                            年
                                                        </td>
                                                        <td width="3%" rowspan="2">
                                                            月
                                                        </td>
                                                        <td width="3%" rowspan="2">
                                                            日
                                                        </td>
                                                        <td width="4%" rowspan="2">
                                                            字<br />
                                                            軌
                                                        </td>
                                                        <td width="11%" rowspan="2">
                                                            號碼
                                                        </td>
                                                        <td width="18%" rowspan="2">
                                                            品名
                                                        </td>
                                                        <td width="5%" rowspan="2">
                                                            數量
                                                        </td>
                                                        <td width="6%" rowspan="2">
                                                            單價
                                                        </td>
                                                        <td colspan="2">
                                                            退出或折讓
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="19%">
                                                            金額(不含稅之進貨額)
                                                        </td>
                                                        <td width="9%">
                                                            營業稅額
                                                        </td>
                                                        <td width="4%">
                                                            應稅
                                                        </td>
                                                        <td width="6%">
                                                            零稅率
                                                        </td>
                                                        <td width="3%" style="line-height: 16px;">
                                                            免稅
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20">
                                                            <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceMode%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceDate.Year%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceDate.Month%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceDate.Day%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceNumber.Substring(0,2)%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceNumber.Substring(2,8)%>
                                                        </td>
                                                        <td style="text-align: left; padding-left: 4px;">
                                                            <%# ((RefundFormListClass)(Container.DataItem)).EinvoiceItemName%>
                                                        </td>
                                                        <td>
                                                            1
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).ItemAmount%>
                                                        </td>
                                                        <td>
                                                            $
                                                            <%# ((RefundFormListClass)(Container.DataItem)).ItemNoTaxAmount%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).ItemTax%>
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).IsTax ? "v" : string.Empty%>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <%# ((RefundFormListClass)(Container.DataItem)).IsTax ?string.Empty :"v"  %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="9" height="20">
                                                            合計
                                                        </td>
                                                        <td>
                                                            $<%# ((RefundFormListClass)(Container.DataItem)).ItemAmount%></td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <span class="ReAppFoText1">本證明單所列銷貨退回進貨退出或折讓，確屬事實，特此證明。</span>
                                    <div class="ReAppFoSitBlockOut">
                                        <table height="80" border="0" cellpadding="0" cellspacing="0" class="ReAppFoSitBlockTable">
                                            <tr>
                                                <td width="224" align="right">
                                                    買受人姓名(或公司抬頭)：
                                                </td>
                                                <td width="184">
                                                    &nbsp;
                                                </td>
                                                <td width="297">
                                                    身分證字號 (或營利事業統一編號)：
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="15">
                                                </td>
                                                <td>
                                                    <span style="color: #666;">(請簽章)</span>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <table border="0" cellspacing="0" cellpadding="0" class="ReAppFoFooterTable">
                                    <tr>
                                        <td bgcolor="#646464" height="5">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#646464" class="ReAppFoFooterTeMark">
                                            請填寫「個人身分證字號」並簽名或蓋章擇一 填妥以掛號方式寄至17Life或傳真至 (02)2511-9110
                                            <br />
                                            <span style="font-size: 14px; line-height: 20px;">( 若為三聯式發票請填寫「營業事業統一編號」並蓋公司發票章 )</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#646464" height="7">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="10">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            客服專線：<%=config.ServiceTel %> 服務時間：平日 09:00 ~ 18:00  傳真號碼：(02)2511-9110
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            地址：104 台北市中山區中山北路一段11號13樓 收件人：17Life退貨處理中心 收
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <hr />
                                        </td>
                                    </tr>
                                </table>
                                <%# ((RefundFormListClass)(Container.DataItem)).breakstring%>
                            </div>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </asp:Panel>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>

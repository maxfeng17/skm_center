﻿using System.Net;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Web.ControlRoom.Controls;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUtility = LunchKingSite.WebLib.WebUtility;
using LunchKingSite.BizLogic.Model;
using MongoDB.Bson;

namespace LunchKingSite.Web.User
{
    public partial class UserAccount : MemberPage, IUserAccount
    {
        #region property

        protected ISysConfProvider conf = ProviderFactory.Instance().GetConfig();

        private UserAccountPresenter presenter;

        public UserAccountPresenter Presenter
        {
            get
            {
                return presenter;
            }
            set
            {
                this.presenter = value;
                if (value != null)
                {
                    this.presenter.View = this;
                }
            }
        }

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        public bool Mode
        {
            get
            {
                return cbx_Mode.Checked;
            }
            set
            {
                cbx_Mode.Checked = value;
            }
        }

        public int MID
        {
            get
            {
                int id;
                if (int.TryParse(hif_ID.Value, out id))
                {
                    return id;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                hif_ID.Value = value.ToString();
            }
        }

        public Dictionary<int, bool> SubscribeRegions
        {
            get
            {
                var foodsubscribes = chklFoodSubscription.Items.Cast<ListItem>().ToDictionary(x => Convert.ToInt32(x.Value), x => x.Selected);
                var subscribes = chklSubscription.Items.Cast<ListItem>().ToDictionary(x => Convert.ToInt32(x.Value), x => x.Selected);
                foreach (var subscribe in foodsubscribes)
                {
                    subscribes.Add(subscribe.Key, subscribe.Value);
                }

                return subscribes;
            }
            set
            {
                if (value != null)
                {
                    for (int i = 0; i < chklFoodSubscription.Items.Count; i++)
                    {
                        chklFoodSubscription.Items[i].Selected = value.Where(x => x.Key.Equals(Convert.ToInt32(chklFoodSubscription.Items[i].Value)) && x.Value.Equals(true)).Count() > 0;
                    }
                    for (int i = 0; i < chklSubscription.Items.Count; i++)
                    {
                        chklSubscription.Items[i].Selected = value.Where(x => x.Key.Equals(Convert.ToInt32(chklSubscription.Items[i].Value)) && x.Value.Equals(true)).Count() > 0;
                    }
                    isSubscriptionDelivery.Value = chklSubscription.Items.FindByValue(CategoryManager.Default.Delivery.CategoryId.ToString()).Selected.ToString();
                    isSubscriptionPponSelect.Value = chklSubscription.Items.FindByValue(CategoryManager.Default.PponSelect.CategoryId.ToString()).Selected.ToString();
                }
            }
        }

        public bool IfReceiveEDM
        {
            get
            {
                return rdlIfReceiveEDM.SelectedValue == "Yes";
            }
        }

        public bool IfReceiveStartedSell
        {
            get
            {
                return rdlIfReceiveStartedSell.SelectedValue == "Yes";
            }
        }

        public string  BindingMemberButtonText
        {
            get;
            set;
        }

        public MobileMember MobileMember { get; set; }

        protected Dictionary<SingleSignOnSource, bool> MemberLinkSet { get; set; }

        public string CityListStr { get; set; }

        #endregion property

        #region event

        public event EventHandler<DataEventArgs<KeyValuePair<Guid, string>>> GetAddressInfo = null;

        public event EventHandler<DataEventArgs<int>> CityChange = null;

        public event EventHandler<DataEventArgs<int>> AreaChange = null;

        public event EventHandler<DataEventArgs<Guid>> BuildingChange = null;

        public event EventHandler<DataEventArgs<Member>> UpdateMemberInfo = null;

        public event EventHandler<DataEventArgs<EinvoiceTriple>> UpdateEinvoiceTriple = null;

        public event EventHandler<DataEventArgs<EinvoiceWinner>> UpdateWinEinvoice = null;

        public event EventHandler<EventArgs> UpdateSubscription = null;

        public event EventHandler<DataEventArgs<UserEditCreditCard>> AddCreditCard = null;

        public event EventHandler<DataEventArgs<Guid>> DeleteCreditCard = null;

        public event EventHandler<DataEventArgs<MemberDelivery>> UpdateMemberDelivery = null;

        public event EventHandler DeleteDeliveryInfo = null;

        public event EventHandler<DataEventArgs<List<string>>> UpdateMemberPassword = null;

        public event EventHandler<DataEventArgs<string>> ChangeMemberEmail = null;

        public event EventHandler ChangeMemberEmailCancel = null;

        public event EventHandler ChangeMemberEmailResend = null;

        public event EventHandler BindingMemberEmailResend = null;

        public event EventHandler BindingMemberEmailCancel = null;

        public event EventHandler<DataEventArgs<PponDeliveryInfo>> UpdateMemberEInvoiceInfo = null;
        #endregion event

        #region method

        /// <summary>
        /// 更新密碼顯示結果
        /// </summary>
        /// <param name="updateResult"></param>
        public void SetPasswordUpdateResult(bool updateResult)
        {
            if (updateResult)
            {
                if (divOldPassword.Attributes["class"].IndexOf("error ") > -1)
                {
                    divOldPassword.Attributes["class"] = "form-unit OldPasswordArea";
                }
                lblPasswordError.Visible = false;
                ShowMessageAndLogout("密碼修改完成。可使用新密碼以17Life方式登入。");
            }
            else
            {
                string a = divOldPassword.Style.Value;
                divOldPassword.Attributes["class"] = "error form-unit OldPasswordArea";
                lblPasswordError.Visible = true;
            }
        }

        public void SetChangeEmailError(Core.ReAuthStatus changeResult)
        {
            string msg;
            switch (changeResult)
            {
                case ReAuthStatus.SameEmailSkip:
                case ReAuthStatus.EmailEmpty:
                    msg = "您尚未輸入新的電子信箱。";
                    break;

                case ReAuthStatus.Unavailable:
                    msg = "此電子信箱已被使用過了，請重新輸入。";
                    break;

                case ReAuthStatus.IllegalMailFormat:
                    msg = "您輸入的信箱格式錯誤，請重新輸入。";
                    break;

                case ReAuthStatus.DisposableEmailAddress:
                    msg = "無法變更為拋棄式信箱，請重新輸入。";
                    break;

                default:
                    msg = "系統繁忙中，請稍後再試。";
                    break;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "setChangeEmailError",
                "setChangeEmailError('" + msg + "');", true);
        }

        public void SetChangeEmailAlert(ReAuthStatus changeResult)
        {
            string msg = "";
            switch (changeResult)
            {
                case ReAuthStatus.ExcceedMailLimit:
                    msg = "已達重發上限。信件可能延遲，請稍候24小時，並檢查信件是否被存放到垃圾郵件中。";
                    break;

                case ReAuthStatus.OK:
                    msg = "重發認證信成功。";
                    break;

                default:
                    msg = "系統繁忙中，請稍後再試。";
                    break;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "setChangeEmailAlert",
                "setChangeEmailAlert('" + msg + "');", true);
        }

        public void SetBindEmailAlert(ReAuthStatus changeResult)
        {
            string msg = "";
            switch (changeResult)
            {
                case ReAuthStatus.ExcceedMailLimit:
                    msg = "已達重發上限。信件可能延遲，請稍候24小時，並檢查信件是否被存放到垃圾郵件中。";
                    break;

                case ReAuthStatus.OK:
                    msg = "重發認證信成功。";
                    break;

                default:
                    msg = "系統繁忙中，請稍後再試。";
                    break;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "setBindEmailAlert",
                "setBindEmailAlert('" + msg + "');", true);
        }

        public void SetDeleteCreditCardAlert(bool result, string msg)
        {
            if (result == false)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "setDeleteCreditCardAlert",
                    "alert('" + msg + "');", true);
            }
        }

        public void SetEinvoiceTriple(EinvoiceTriple data)
        {
            hidEinvoiceTripleId.Value = data.Id.ToString();
            InvoiceTitle.Text = data.Title;
            InvoiceNumber.Text = data.VatNumber;
            InvoiceBuyerName.Text = data.UserName;
            string invoiceAddress = string.Empty;

            if (!string.IsNullOrEmpty(data.Adress))
            {
                var invoiceAddressArray = data.Adress.Split('|');
                if (invoiceAddressArray.Count() == 3)
                {
                    hidInvoiceCity.Value = invoiceAddressArray[0];
                    hidInvoiceArea.Value = invoiceAddressArray[1];

                    var addressArray = invoiceAddressArray[2].Split('@');
                    if (addressArray.Count() == 2)
                    {
                        InvoiceAddress.Text = addressArray[0];
                    }
                    else
                    {
                        InvoiceAddress.Text = invoiceAddressArray[2];
                    }
                }
                else
                {
                    InvoiceAddress.Text = data.Adress;
                }
            }
            else
            {
                InvoiceAddress.Text = data.Adress;
            }
        }

        public void SetEinvoiceWinner(EinvoiceWinner data)
        {
            hidWinEinvoiceId.Value = data.Id.ToString();
            wInvoiceName.Text = data.UserName;
            wPhoneNumber.Text = data.Mobile;
            wInvoAddr.Text = data.Adress;
        }

        public void SetCreditCardForm(List<dynamic> creditcardViews)
        {
            txtCard1.Text = "";
            txtCard2.Text = "";
            txtCard3.Text = "";
            txtCard4.Text = "";
            txtCreditCardName.Text = "";
            ddlCreditCardYear.Items.Clear();
            ddlCreditCardYear.Items.Add(new ListItem { Text = "--", Value = "" });
            for (int i = 0; i < 15; i++)
            {
                string shortYear = (DateTime.Now.AddYears(i).Year % 100).ToString();
                ddlCreditCardYear.Items.Add(new ListItem { Text = shortYear, Value = shortYear });
            }
            ddlCreditCardYear.SelectedIndex = 0;
            ddlCreditCardMonth.SelectedIndex = 0;

            repMemberCreditCards.DataSource = creditcardViews;
            repMemberCreditCards.DataBind();
        }

        public void SetDropDownList(List<City> cities)
        {
            AddListItem(ddlBirthYear, 1910, DateTime.Now.Year);
            AddListItem(ddlBirthMonth, 1, 12);
            ddlBirthDay.Items.Clear();
            ddlBirthDay.Items.Add(new ListItem("請選擇", "-1"));
            cities.Insert(0, new City() { CityName = "請選擇", Id = -1 });
            ddlcity_m.DataSource = ddlcity_d.DataSource = cities;
            ddlcity_m.DataBind();
            ddlcity_d.DataBind();
        }

        public void GetMemberInfo(Member m, MemberDeliveryCollection mdc, bool lifemember, MemberLinkCollection mlc, MemberAuthInfo mai)
        {
            object sender = new object();
            EventArgs e = new EventArgs();
            lab_Email.Text = UserName;
            phEmailShow.Visible = 
                RegExRules.CheckEmail(UserName) && Helper.IsFlagSet(m.Status, MemberStatusFlag.Active17Life);
            tbLastName.Text = m.LastName;
            tbFirstName.Text = m.FirstName;
            tbMobile.Text = m.Mobile;
            tbUserEmail.Text = m.UserEmail;
            if (m.Gender.HasValue)
            {
                if (m.Gender.Value == 1)
                {
                    rb_M.Checked = true;
                }
                else
                {
                    rb_F.Checked = true;
                }
            }

            if (m.Birthday.HasValue)
            {
                ddlBirthYear.SelectedValue = m.Birthday.Value.Year.ToString();
                ddlBirthMonth.SelectedValue = m.Birthday.Value.Month.ToString();
                ChangeBirthDays(sender, e);
                ddlBirthDay.SelectedValue = m.Birthday.Value.Day.ToString();
            }

            if (m.BuildingGuid.HasValue)
            {
                if (GetAddressInfo != null)
                {
                    GetAddressInfo(sender, new DataEventArgs<KeyValuePair<Guid, string>>(new KeyValuePair<Guid, string>(m.BuildingGuid.Value, m.CompanyAddress)));
                }
            }
            repAddr.DataSource = mdc;
            repAddr.DataBind();
            rdlIfReceiveEDM.SelectedValue = Helper.IsFlagSet((MemberEdmType)m.EdmType, MemberEdmType.PponEvent) ? "Yes" : "No";
            rdlIfReceiveStartedSell.SelectedValue = Helper.IsFlagSet((MemberEdmType)m.EdmType, MemberEdmType.PponStartedSell) ? "Yes" : "No";
            //有開通17Life帳號才顯示密碼修改區塊
            if (lifemember)
            {
                pan_PasswordSet.Visible = true;
            }
            //顯示目前登入方式
            lblLoginSource.Text = ExternalLoginInfo.SourceToString(this.LoginSource);
            //顯示已開通的登入方式
            lblLoginMethod.Text = string.Join("<br />", GetExternalOrgNameList(mlc));
            MemberLinkSet = new Dictionary<SingleSignOnSource, bool>();

            bool enableLife17 = mlc.Any(t => t.ExternalOrg == (int)SingleSignOnSource.ContactDigitalIntegration) && Helper.IsFlagSet(m.Status, MemberStatusFlag.Active17Life);
            MemberLinkSet.Add(SingleSignOnSource.ContactDigitalIntegration, enableLife17);
            bool enablePez = mlc.Any(t => t.ExternalOrg == (int)SingleSignOnSource.PayEasy);
            MemberLinkSet.Add(SingleSignOnSource.PayEasy, enablePez);
            bool enableFb = mlc.Any(t => t.ExternalOrg == (int)SingleSignOnSource.Facebook);
            MemberLinkSet.Add(SingleSignOnSource.Facebook, enableFb);
            if (MemberFacade.IsMobileAuthEnabled)
            {
                bool enableMobile17 = mlc.Any(t => t.ExternalOrg == (int)SingleSignOnSource.Mobile17Life);
                MemberLinkSet.Add(SingleSignOnSource.Mobile17Life, enableMobile17);
            }
            bool enableLine = mlc.Any(t => t.ExternalOrg == (int)SingleSignOnSource.Line);
            MemberLinkSet.Add(SingleSignOnSource.Line, enableLine);

            if (ProviderFactory.Instance().GetConfig().EnableChangeEmail == false)
            {
                //關閉更改信箱的功能
                phChangeEmailButton.Visible = false;
                phChangeEmailConfirm.Visible = false;
            }
            else
            {
                //是信箱註冊，而且已是會員，才能變更信箱
                if (RegExRules.CheckEmail(m.UserName) && Helper.IsFlagSet(m.Status, MemberStatusFlag.Active17Life))
                {
                     //信箱未變更
                    if (String.Equals(mai.Email, this.UserName, StringComparison.OrdinalIgnoreCase) ||
                                       mai.HasAuthData == false || mai.LinkExpired)
                    {
                        phChangeEmailButton.Visible = true;
                        phChangeEmailConfirm.Visible = false;
                    }
                    else
                    {
                        phChangeEmailButton.Visible = false;
                        phChangeEmailConfirm.Visible = true;
                        litNewEmail.Text = mai.Email;
                        litNewEmail2.Text = mai.Email;
                        litNewEmailApplyDate.Text = mai.AuthDate.Value.ToShortDateString();
                    }
                }
                else
                {
                    phChangeEmailButton.Visible = false;
                    phChangeEmailConfirm.Visible = false;
                }

            }

            //綁定信箱
            if (RegExRules.CheckEmail(m.UserName) && String.Equals(mai.Email, this.UserName, StringComparison.OrdinalIgnoreCase) &&
                mai.HasAuthData && !Helper.IsFlagSet(m.Status, MemberStatusFlag.Active17Life))
            {
                phBindingEmailConfirm.Visible = true;
                litEmail.Text = litEmail2.Text = mai.Email;
                litEmailApplyDate.Text = mai.AuthDate.Value.ToShortDateString();
                BindingMemberButtonText = "更改";
            }
            else
            {
                phBindingEmailConfirm.Visible = false;
                BindingMemberButtonText = "開通";
            }


            //電子發票載具管理
            ddlCarrierType.SelectedValue = m.CarrierType.ToString();
            EInvoiceBuyerName.Text = m.InvoiceBuyerName;
            EInvoiceAddress.Text = m.InvoiceBuyerAddress;
            switch (m.CarrierType)
            {
                case 2:
                    txtPhoneCarrier.Text = m.CarrierId;
                    break;
                case 3:
                    txtPersonalCertificateCarrier.Text = m.CarrierId;
                    break;
            }
        }

        /// <summary>
        /// 取得 Member_link 中 ExternalOrg 欄位所開通的登入方式
        /// </summary>
        /// <returns>string</returns>
        public List<string> GetExternalOrgNameList(MemberLinkCollection mlc)
        {
            List<string> ExtenalOrgLst = new List<string>();
            foreach (MemberLink ml in mlc)
            {
                switch (ml.ExternalOrg)
                {
                    case 0:
                        ExtenalOrgLst.Add("PayEasy");
                        break;

                    case 1:
                        ExtenalOrgLst.Add("Facebook");
                        break;

                    case 2:
                        ExtenalOrgLst.Add("17Life");
                        break;

                    case 3:
                        ExtenalOrgLst.Add("Google");
                        break;
                }
            }
            return ExtenalOrgLst;
        }

        public void SetAddressInfo(string address, Building bld, City city)
        {
            SetAddressUserControl(address, bld, city, ddlcity_m, ddlarea_m, ddlbuilding_m, aib_m);
        }

        public void GetCityChange(CityCollection cities)
        {
            EventArgs e = new EventArgs();
            DropDownList ddlarea = Mode ? ddlarea_m : ddlarea_d;
            ddlarea.DataSource = cities;
            ddlarea.DataBind();
            ddlarea.SelectedIndex = 0;
            ddlarea_SelectedIndexChanged(ddlarea, e);
        }

        public void GetAreaChange(ListItemCollection buildings)
        {
            EventArgs e = new EventArgs();
            DropDownList ddlbuilding = Mode ? ddlbuilding_m : ddlbuilding_d;
            ddlbuilding.DataSource = buildings;
            ddlbuilding.DataBind();
            ddlbuilding.SelectedIndex = 0;
            ddlbuilding_SelectedIndexChanged(ddlbuilding, e);
        }

        public void GetBuildingChange(Building building)
        {
            AddressInputBox aib = Mode ? aib_m : aib_d;
            if (building.CityId == 0)
            {
                aib.Dispose();
            }
            else
            {
                DropDownList ddlbuilding = Mode ? ddlbuilding_m : ddlbuilding_d;
                DropDownList ddlarea = Mode ? ddlarea_m : ddlarea_d;
                DropDownList ddlcity = Mode ? ddlcity_m : ddlcity_d;
                aib.BuildingAddress = ddlbuilding.SelectedItem.Text;
                aib.BuildingZoneType = Helper.GetZoneType(Convert.ToInt32(ddlarea.SelectedValue));
                if (aib.BuildingZoneType == ZoneType.Company)
                {
                    if (!string.IsNullOrEmpty(building.BuildingStreetName) && building.BuildingStreetName != building.BuildingName)
                    {
                        aib.BuildingAddress = building.BuildingStreetName;
                    }
                }
                aib.CityName = ddlcity.SelectedItem.Text + ddlarea.SelectedItem.Text;
                aib.PresetAddress = null;
            }
            aib.DataBind();
        }

        public void SetDeliveryInfo(MemberDelivery md, Building bld, City city)
        {
            ClearText();
            txtAlias.Text = md.AddressAlias;
            tbCompanyName.Text = md.BuildingName;
            tbDepartment.Text = md.Section;
            tbMobileNumber.Text = md.Mobile;
            if (!string.IsNullOrEmpty(md.Tel) && md.Tel.Length > 2)
            {
                tbAreaCode.Text = md.Tel.Substring(0, 2);
                tbCompanyTel.Text = md.Tel.Substring(3);
            }
            tbContactName.Text = md.ContactName;//收件人姓名
            tbExtension.Text = md.Ext;
            SetAddressUserControl(md.Address, bld, city, ddlcity_d, ddlarea_d, ddlbuilding_d, aib_d);
        }

        public void SetPponCityGroupList(List<CategoryNode> list)
        {
            CategoryNode pponDeal = CategoryManager.Default.PponDeal;

            // 設定美食的訂閱頻道
            List<CategoryNode> pponDealArea = pponDeal.NodeDatas
                .Where(x => x.NodeType == Core.CategoryType.PponChannelArea)
                .FirstOrDefault().CategoryNodes;

            //for 美食
            chklFoodSubscription.DataSource = pponDealArea;
            chklFoodSubscription.DataTextField = "CategoryName";
            chklFoodSubscription.DataValueField = "CategoryId";
            chklFoodSubscription.DataBind();

            //for other
            chklSubscription.DataSource = list.Where(x => !pponDealArea.Select(y => y.CategoryId).Contains(x.CategoryId));
            chklSubscription.DataTextField = "CategoryName";
            chklSubscription.DataValueField = "CategoryId";
            chklSubscription.DataBind();
        }

        public void ShowMessage(string msg)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + msg + "');", true);
        }

        public void ShowMessageAndLogout(string msg)
        {
            string root = WebUtility.GetSiteRoot();
            if (root.EndsWith("/") == false)
            {
                root += "/";
            }
            string script = string.Format("alert('{0}');" + "location.href='{1}NewMember/Logout.aspx?login=1'", msg, root);
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", script, true);
        }

        #endregion method

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }
            if (User.IsInRole(MemberRoles.Administrator.ToString()) == false && conf.EnableMemberLinkBind == false)
            {
                phMemberLinkBindEditContent.Visible = false;
                phMemberLinkBindScript.Visible = false;
                phMemberLinkBindReadonlyContent.Visible = true;
            }
            else
            {
                phMemberLinkBindEditContent.Visible = true;
                phMemberLinkBindScript.Visible = true;
                phMemberLinkBindReadonlyContent.Visible = false;
            }
            presenter.OnViewLoaded();
            if (!IsPostBack)
            {
                presenter.OnViewInitialized();
                ddlcity_SelectedIndexChanged(ddlcity_d, e);
            }
            mobileAuthPanel.Visible = MemberFacade.IsMobileAuthEnabled;
            PaperInvoice.Visible = conf.EnabledInvoicePaperRequest;
            MobileMember = MemberFacade.GetMobileMember(this.UserName);
        }

        public void AddListItem(DropDownList ddl, int istart, int iend)
        {
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("請選擇", "-1"));
            for (int i = istart; i <= iend; i++)
            {
                ddl.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }

        public void ChangeBirthDays(object sender, EventArgs e)
        {
            int year, month;
            DateTime date;
            if (int.TryParse(ddlBirthYear.SelectedValue, out year) && year > 0 && int.TryParse(ddlBirthMonth.SelectedValue, out month) && month > 0)
            {
                if (DateTime.TryParse(string.Format("{0}/{1}/1", year, month), out date))
                {
                    AddListItem(ddlBirthDay, 1, date.AddMonths(1).AddDays(-1).Day);
                }
            }
        }

        protected void SetAddressUserControl(string address, Building bld, City city, DropDownList ddlcity, DropDownList ddlarea, DropDownList ddlbuilding, AddressInputBox aib)
        {
            EventArgs e = new EventArgs();
            if (ddlcity.Items.FindByValue(city.ParentId.Value.ToString()) != null)
            {
                ddlcity.SelectedValue = city.ParentId.Value.ToString();
                ddlcity_SelectedIndexChanged(ddlcity, e);
                ddlarea.SelectedValue = bld.CityId.ToString();
                ddlarea_SelectedIndexChanged(ddlarea, e);
                ddlbuilding.SelectedValue = bld.Guid.ToString();
                ddlbuilding_SelectedIndexChanged(ddlbuilding, e);
                aib.BuildingAddress = bld.BuildingStreetName + " " + bld.BuildingAddressNumber;
                aib.BuildingZoneType = Helper.GetZoneType(bld.CityId);
                aib.CityName = ddlcity.SelectedItem.Text + ddlarea.SelectedItem.Text;
                aib.PresetAddress = address;
                aib.DataBind();
            }
            else
            {
                ddlcity.SelectedValue = "-1";
                ddlcity_SelectedIndexChanged(ddlcity, e);
                ddlarea.SelectedValue = "-1";
                ddlarea_SelectedIndexChanged(ddlarea, e);
                aib.Dispose();
                aib.DataBind();
            }
        }

        protected void CheckMode(DropDownList ddl)
        {
            if (ddl == ddlarea_d || ddl == ddlbuilding_d || ddl == ddlcity_d)
            {
                Mode = false;
            }
            else
            {
                Mode = true;
            }
        }

        protected void ddlcity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is DropDownList)
            {
                int city;
                DropDownList ddl = (DropDownList)sender;
                if (int.TryParse(ddl.SelectedValue, out city) && CityChange != null)
                {
                    CheckMode(ddl);
                    CityChange(sender, new DataEventArgs<int>(city));
                }
            }
        }

        protected void ddlarea_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is DropDownList)
            {
                int area;
                DropDownList ddl = (DropDownList)sender;
                if (int.TryParse(ddl.SelectedValue, out area) && AreaChange != null)
                {
                    CheckMode(ddl);
                    AreaChange(sender, new DataEventArgs<int>(area));
                }
            }
        }

        protected void ddlbuilding_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is DropDownList)
            {
                Guid building;
                DropDownList ddl = (DropDownList)sender;
                if (Guid.TryParse(ddl.SelectedValue, out building) && BuildingChange != null)
                {
                    CheckMode(ddl);
                    BuildingChange(sender, new DataEventArgs<Guid>(building));
                }
            }
        }


        protected void lbEdit_Click(object sender, EventArgs e)
        {
            Member m = new Member();
            Guid buildguid;
            if (Guid.TryParse(ddlbuilding_m.SelectedValue, out buildguid))
            {
                m.BuildingGuid = buildguid;
            }

            m.LastName = tbLastName.Text.Trim();
            m.FirstName = tbFirstName.Text.Trim();
            if (rb_F.Checked)
            {
                m.Gender = 2;
            }
            else if (rb_M.Checked)
            {
                m.Gender = 1;
            }

            else
            {
                m.Gender = null;
            }

            DateTime date;
            if (DateTime.TryParse(ddlBirthYear.SelectedValue + "/" + ddlBirthMonth.SelectedValue + "/" + ddlBirthDay.SelectedValue, out date))
            {
                m.Birthday = date;
            }
            else
            {
                m.Birthday = null;
            }
            m.Mobile = tbMobile.Text;
            m.UserEmail = tbUserEmail.Text.Trim();
            m.CompanyAddress = aib_m.FullAddress;

            if (UpdateMemberInfo != null)
            {
                UpdateMemberInfo(sender, new DataEventArgs<Member>(m));
                ShowMessage("您的資料已儲存成功!");
            }
        }

        protected void btnEinvoiceSave_Click(object sender, EventArgs e)
        {
            PponDeliveryInfo info = new PponDeliveryInfo();
            if (ddlCarrierType.SelectedValue == "2")
            {
                info.CarrierType = CarrierType.Phone;
                info.CarrierId = txtPhoneCarrier.Text;
            }
            else if (ddlCarrierType.SelectedValue == "3")
            {
                info.CarrierType = CarrierType.PersonalCertificate;
                info.CarrierId = txtPersonalCertificateCarrier.Text;
            }
            else if (ddlCarrierType.SelectedValue == "0")
            {
                info.CarrierType = CarrierType.None;
                info.InvoiceBuyerName = EInvoiceBuyerName.Text.Trim();
                info.InvoiceBuyerAddress = EInvoiceAddress.Text.Trim();
            }

            if (UpdateMemberInfo != null)
            {
                UpdateMemberEInvoiceInfo(sender, new DataEventArgs<PponDeliveryInfo>(info));
                ShowMessage("您的資料已儲存成功!");
            }
        }

        protected void btnUpdateEinvoiceTriple_Click(object sender, EventArgs e)
        {
            EinvoiceTriple data = new EinvoiceTriple();
            int tmpData;
            if (hidEinvoiceTripleId.Value != "0")
            {

                int.TryParse(hidEinvoiceTripleId.Value, out tmpData);
                data.Id = tmpData;
                data.IsNew = false;
                data.IsLoaded = true;
            }
            data.Title = InvoiceTitle.Text.Trim();
            data.VatNumber = int.TryParse(InvoiceNumber.Text, out tmpData) ? InvoiceNumber.Text : "0";
            data.UserName = InvoiceBuyerName.Text.Trim();
            data.Adress = (string.IsNullOrEmpty(hidInvoiceCity.Value) == true ? "" : hidInvoiceCity.Value + "|") + (string.IsNullOrEmpty(hidInvoiceArea.Value) == true ? "" : hidInvoiceArea.Value + "|") + InvoiceAddress.Text + "@" + hidInvoiceAddress.Value;
            data.Type = 3;
            data.UserId = UserId;
            if (UpdateMemberInfo != null)
            {
                UpdateEinvoiceTriple(sender, new DataEventArgs<EinvoiceTriple>(data));
                ShowMessage("您的資料已儲存成功!");
            }
        }

        protected void btnUpdateWinEinvoice_Click(object sender, EventArgs e)
        {
            EinvoiceWinner data = new EinvoiceWinner();
            int tmpData;
            if (hidWinEinvoiceId.Value != "0")
            {

                int.TryParse(hidWinEinvoiceId.Value, out tmpData);
                data.Id = tmpData;
                data.IsNew = false;
                data.IsLoaded = true;
            }
            data.UserId = UserId;
            data.UserName = wInvoiceName.Text.Trim();
            data.Adress = hiddenAddrCity.Value.Trim() + wInvoAddr.Text.Trim();
            data.Mobile = wPhoneNumber.Text.Trim();
            data.ModificationTime = DateTime.Now;
            if (UpdateMemberInfo != null)
            {
                UpdateWinEinvoice(sender, new DataEventArgs<EinvoiceWinner>(data));
                ShowMessage("您的資料已儲存成功!");
            }
        }

        protected void btnUpdateSubscription_Click(object sender, EventArgs e)
        {
            if (UpdateSubscription != null)
            {
                UpdateSubscription(sender, e);
                ShowMessage("您的訂閱已更新！");
            }
        }

        protected void btnAddCreditCard_Click(object sender, EventArgs e)
        {
            if (AddCreditCard != null)
            {
                UserEditCreditCard data = new UserEditCreditCard
                {
                    Number = txtCard1.Text + txtCard2.Text + txtCard3.Text + txtCard4.Text,
                    Year = ddlCreditCardYear.Text,
                    Month = ddlCreditCardMonth.Text,
                    Name = txtCreditCardName.Text
                };

                AddCreditCard(sender, new DataEventArgs<UserEditCreditCard>(data));
                //ShowMessage("您的信用卡號已更新！");
            }
        }

        protected void repMemberCreditCards_RowCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "_DELETE")
            {
                Guid creditCardGuid;
                if (Guid.TryParse(e.CommandArgument.ToString(), out creditCardGuid))
                {
                    DeleteCreditCard(sender, new DataEventArgs<Guid>(creditCardGuid));
                }
            }
        }

        protected void rpt_RowCommand(object sender, RepeaterCommandEventArgs e)
        {
            hif_ID.Value = e.CommandArgument.ToString();
            if (e.CommandName == "delitem")
            {
                if (DeleteDeliveryInfo != null)
                {
                    DeleteDeliveryInfo(sender, e);
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearText();
            Mode = true;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            MemberDelivery md = new MemberDelivery();
            md.CreateTime = DateTime.Now;
            md.AddressAlias = txtAlias.Text;
            Guid building_guid;
            if (Guid.TryParse(ddlbuilding_d.SelectedItem.Value, out building_guid))
            {
                md.BuildingGuid = building_guid;
            }

            md.UserId = this.UserId;
            md.Tel = tbAreaCode.Text + " " + tbCompanyTel.Text;
            md.Ext = tbExtension.Text;
            md.Mobile = tbMobileNumber.Text;
            md.Address = aib_d.FullAddress;
            md.BuildingName = tbCompanyName.Text;
            md.Section = tbDepartment.Text;
            md.Memo = tbAddressMemo.Text;
            md.ContactName = tbContactName.Text.Trim();//收件人姓名
            md.ModifyTime = DateTime.Now;

            if (UpdateMemberDelivery != null)
            {
                UpdateMemberDelivery(sender, new DataEventArgs<MemberDelivery>(md));
            }
        }

        protected void btnSaveNewPassword_Click(object sender, EventArgs e)
        {
            List<string> lst = new List<string>();
            lst.Add(tbOriginalPassword.Text);
            lst.Add(tbNewPassword.Text);

            if (UpdateMemberPassword != null)
            {
                UpdateMemberPassword(sender, new DataEventArgs<List<string>>(lst));
            }
        }

        protected void btnConfirmChangeEmail_Click(object sender, EventArgs e)
        {
            if (ChangeMemberEmail != null)
            {
                ChangeMemberEmail(sender, new DataEventArgs<string>(txtNewEmail.Value));
            }
        }

        protected void btnChangeMailResend_Click(object sender, EventArgs e)
        {
            if (ChangeMemberEmailResend != null)
            {
                ChangeMemberEmailResend(sender, new DataEventArgs<int>(this.UserId));
            }
        }

        protected void btnChangeMailCancel_Click(object sender, EventArgs e)
        {
            if (ChangeMemberEmailCancel != null)
            {
                ChangeMemberEmailCancel(sender, new DataEventArgs<int>(this.UserId));
            }
        }

        protected void btnBindingMailResend_Click(object sender, EventArgs e)
        {
            if (BindingMemberEmailResend != null)
            {
                BindingMemberEmailResend(sender, new DataEventArgs<int>(this.UserId));
            }
        }

        protected void btnBindingMailCancel_Click(object sender, EventArgs e)
        {
            if (BindingMemberEmailCancel != null)
            {
                BindingMemberEmailCancel(sender, new DataEventArgs<int>(this.UserId));

                if (User.Identity.IsAuthenticated)
                {
                    //取消成功
                    string msg = "取消成功";
                    string url = Helper.CombineUrl(conf.SiteUrl, "/User/UserAccount.aspx");
                    Response.Write(String.Format("<script>alert('{0}'); location.href='{1}';</script>", msg, url));
                    Response.End();
                }
            }
        }

        protected void ClearText()
        {
            aib_d.DataBind();

            txtAlias.Text = tbCompanyName.Text = tbDepartment.Text =
            tbMobileNumber.Text = tbAreaCode.Text = tbCompanyTel.Text =
            tbContactName.Text = tbExtension.Text = string.Empty;
        }

        protected void AddDelivery(object sender, EventArgs e)
        {
            Mode = false;
            MemberDelivery md = new MemberDelivery();
            md.CreateTime = DateTime.Now;
            md.AddressAlias = txtAlias.Text;
            md.BuildingGuid = new Guid(ddlbuilding_d.SelectedItem.Value);
            md.UserId = this.UserId;
            md.Tel = tbAreaCode.Text + " " + tbCompanyTel.Text;
            md.Ext = tbExtension.Text;
            md.Mobile = tbMobileNumber.Text;
            md.Address = aib_d.FullAddress;
            md.BuildingName = tbCompanyName.Text;
            md.Section = tbDepartment.Text;
            md.Memo = tbAddressMemo.Text;
            md.ContactName = tbContactName.Text.Trim();//收件人姓名
            md.ModifyTime = DateTime.Now;
            if (UpdateMemberDelivery != null)
            {
                UpdateMemberDelivery(sender, new DataEventArgs<MemberDelivery>(md));
                ShowMessage("收件人資料新增成功！");
            }

            ClearText();
        }

        #endregion page
    }
}
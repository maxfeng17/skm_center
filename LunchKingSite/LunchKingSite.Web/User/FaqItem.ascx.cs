﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.User
{
    public partial class FaqItem : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FaqCollection faqs = ProviderFactory.Instance().GetProvider<IPponProvider>().FaqCollectionGet();
            FaqItemList.DataSource = faqs.Where(x => x.Pid == 0 && x.Mark != "H").OrderBy(x => x.Mark).ThenBy(x => x.Sequence);
            FaqItemList.DataBind();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using System.Web.UI;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.CouponList;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.User
{
    public partial class SendCouponListToEmail : Page
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public static ISysConfProvider SystemConfig
        {
            get
            {
                return config;
            }
        }

        [WebMethod]
        public static dynamic SendCouponListMail(string userEmail, int duringSelected, string duringString)
        {
            var startDate = DateTime.Today;
            var endDate = DateTime.Today;
            
            List<ViewCouponListMain> couponListMain;

            var userId = MemberFacade.GetUniqueId(userEmail);

            if (duringSelected == (int)MailOrderDuringFilter.All)
            {
                couponListMain = MemberFacade.ViewCouponListMainGetListByUser(0, 0, userId, NewCouponListFilterType.None, Guid.Empty, false, true).ToList();
                if (couponListMain.Count != 0)
                {
                    startDate = couponListMain.Min(x => x.CreateTime);
                }
            }
            else
            {
                startDate = (duringSelected == (int)MailOrderDuringFilter.ThreeMonths) ? startDate.AddMonths(-3) : startDate.AddMonths(-6);
                couponListMain = MemberFacade.ViewCouponListMainGetListByUser(0, 0, userId, NewCouponListFilterType.None, Guid.Empty, false, true)
                                    .Where(x => x.CreateTime >= startDate).ToList();
            }

            if (couponListMain.Count == 0)
            {
                return new { IsSuccess = true, Msg = "您輸入的信箱並無購買紀錄，請您填寫曾訂購過的信箱哦！" };
            }
            
            var member = MemberFacade.GetMember(userEmail);
            var mailCouponListMainList = SetCouponListMain(couponListMain);
            var userTotalBonusPoint = MemberFacade.GetMemberPromotionValue(userEmail);

            var template = TemplateFactory.Instance().GetTemplate<MemberCouponListMail>();
            template.UserEmail = userEmail;
            template.UserName = member.DisplayName;
            template.IsGuest = member.IsGuest;
            template.AuthLink = member.IsGuest ? GetAuthLink(member) : string.Empty;
            template.CashPoint = OrderFacade.GetSCashSum(userEmail).ToString("F0");
            template.BonusPoint = Math.Floor(userTotalBonusPoint / 10).ToString("F0");  
            template.PayeasyCash = MemberFacade.GetPayeasyCashPoint(member.UniqueId).ToString("F0");
            template.DuringDate = string.Format("({0:yyyy/MM/dd} ~ {1:yyyy/MM/dd})", startDate, endDate);
            template.DuringString = duringString;
            template.MainList = mailCouponListMainList;

            bool isSuccess = EmailFacade.SendCouponListToMail(userEmail, member.DisplayName, template);
            return isSuccess ? new { IsSuccess = true, Msg = "17Life已將購買紀錄寄到您的信箱中，請您前往查看，謝謝！" } 
                             : new { IsSuccess = false, Msg = "寄送失敗！" };
        }

        /// <summary>
        /// 取得訂單
        /// </summary>
        /// <param name="couponListMain"></param>
        /// <returns></returns>
        private static List<MailCouponListMain> SetCouponListMain(List<ViewCouponListMain> couponListMain)
        {
            var mailCouponListMainList = new List<MailCouponListMain>();
            var couponSeqList = MemberFacade.GetCouponListSequenceByOidList(couponListMain.Select(x => x.Guid).ToList()).ToList(); //全部憑證

            foreach (var main in couponListMain.OrderByDescending(x=>x.CreateTime))
            {
                var isFami = (main.Status & (int)GroupOrderStatus.FamiDeal) > 0;
                var mainInfo = OrderFacade.GetCouponListMainStatus(main);

                if (isFami || mainInfo.IsZeroPponDeal) continue;

                var orderGuid = main.Guid;
                var cuponListDetail = SetCouponListDetail(main, mainInfo, couponSeqList.Where(x => x.Guid == orderGuid).ToList());
                var orderPrice = (((main.BusinessHourStatus & (int) LunchKingSite.Core.BusinessHourStatus.PriceZeorShowOriginalPrice) > 0)
                    ? main.ItemOrigPrice
                    : main.ItemPrice);
                List<int> statusType;
                if (mainInfo.IsPpon)
                {
                    if (mainInfo.IsPayAtm)
                    {
                        statusType = mainInfo.IsShowPayAtm
                            ? new List<int> { (int)MailCouponListStatusType.NotUsed } //ATM 尚未付款
                            : new List<int> { (int)MailCouponListStatusType.None };//ATM 逾期未付款
                    }
                    else
                    {
                        statusType = cuponListDetail.Select(x => x.StatusType).ToList();
                    }
                }
                else
                {
                    statusType = new List<int> { mainInfo.MainOrderStatusType };
                }

                mailCouponListMainList.Add(
                    new MailCouponListMain
                    {
                        OrderGuid = main.Guid,
                        OrderDate = main.CreateTime,
                        OrderId = main.OrderId,
                        OrderName = main.ItemName.TrimToMaxLength(17, "..."),
                        OrderNameLink = string.Format("{0}/{1}", config.SiteUrl, main.BusinessHourGuid),
                        OrderNameTag = "好康價: " + orderPrice.ToString("F0"),
                        OrderExp = (main.DeliveryType.HasValue && int.Equals(2, main.DeliveryType.Value))
                            ? "-"
                            : ((main.BusinessHourDeliverTimeE == null)
                                ? string.Empty
                                : main.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd")),
                        OrderStatus = (mainInfo.IsPayAtm && mainInfo.IsShowPayAtm) ? "ATM 尚未付款" : mainInfo.MainOrderStatus,
                        IsDownloadCouponPdf = mainInfo.IsDownloadCouponPdf,
                        CouponListDetail = cuponListDetail,
                        StatusType = statusType
                    });
            }
            return mailCouponListMainList;
        }

        /// <summary>
        /// 取得憑證明細
        /// </summary>
        /// <param name="main"></param>
        /// <param name="mainInfo"></param>
        /// <param name="couponDetailList"></param>
        /// <returns></returns>
        private static List<MailCouponListDetail> SetCouponListDetail(ViewCouponListMain main, CouponListMainInfo mainInfo, List<ViewCouponListSequence> couponDetailList)
        {
            var mailCouponListDetailList = new List<MailCouponListDetail>();
            var cancelStatus = main.ReturnStatus;
            var isFami = (main.Status & (int)GroupOrderStatus.FamiDeal) > 0;
            var isPEZevent = ((main.Status & (int)GroupOrderStatus.PEZevent) > 0) &&
                         (cancelStatus == (int) ProgressStatus.Completed ||
                          cancelStatus == (int) ProgressStatus.CompletedWithCreditCardQueued);
            
            foreach (var detail in couponDetailList)
            {
                var detailOrderSn = (isFami || isPEZevent || mainInfo.IsPayAtm) ? string.Empty : detail.SequenceNumber;
                var detailOrderCode = (isFami || isPEZevent || mainInfo.IsPayAtm) ? string.Empty : detail.Code;
                var downloadLink = (mainInfo.IsDownloadCouponPdf)
                    ? string.Format("{0}/User/CouponGet.aspx?cid={1}&sequencenumber={2}&couponcode={3}&ct={4}", 
                                    config.SiteUrl, detail.Id, detail.SequenceNumber, detail.Code, detail.OrderDetailCreateTime.Ticks)
                    : string.Empty;
                
                var detailStatus = OrderFacade.GetCouponListDetailStatus(main, detail);
                
                mailCouponListDetailList.Add(new MailCouponListDetail
                {
                    OrderDetailCreatime = detail.OrderDetailCreateTime,
                    OrderSn = mainInfo.IsPayAtm ? "-" : detailOrderSn,
                    OrderCode = mainInfo.IsPayAtm ? "-" : detailOrderCode,
                    CouponStatus = mainInfo.IsPayAtm ? "-" : detailStatus.Status,
                    StatusType = detailStatus.StatusType,
                    IsEnabledDownLoad = detailStatus.IsEnabledDownLoad,
                    DownloadLink = downloadLink
                });
            }

            //!isPpon 並沒有憑證明細，需new一個detail於前端顯示「憑證狀態」、以及做為「憑證分類」依據
            if (couponDetailList.Count == 0 && !mainInfo.IsPpon) 
            {
                mailCouponListDetailList.Add(new MailCouponListDetail
                {
                    OrderDetailCreatime = main.CreateTime,
                    OrderSn = "-",
                    OrderCode = "-",
                    CouponStatus = mainInfo.MainCouponStatus,
                    StatusType = (int)MailCouponListStatusType.None,
                    DownloadLink = ""
                });
            }

            return mailCouponListDetailList;
        }

        /// <summary>
        /// 取得認證連結
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        private static string GetAuthLink(Member member)
        {
            var result = string.Empty;

                var authInfo = MemberFacade.MemberAuthInfoGet(member.UniqueId);
            result = string.Format("{0}/NewMember/memberauth.aspx?uid={1}&key={2}&code={3}", config.SiteUrl, member.UniqueId, authInfo.AuthKey, authInfo.AuthCode);

            return result;
        }

    }
}
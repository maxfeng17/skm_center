﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.Services;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.User
{
    public partial class Collect : LocalizedBasePage, IUserCollectView
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        #region property

        private UserCollectPresenter _presenter;
        public UserCollectPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public bool IsAuthenticated
        {
            get
            {
                return Page.User.Identity.IsAuthenticated;
            }
        }

        public string UserName
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }

        public int UserId
        {
            get
            {
                int uniqueId;
                return int.TryParse(hidMemberUserId.Value, out uniqueId) ? uniqueId : 0;
            }
            set
            {
                hidMemberUserId.Value = value.ToString();
            }
        }

        public int PageSize
        {
            get
            {
                return collectListPager.PageSize;
            }
        }

        public int CurrentPage
        {
            get
            {
                return collectListPager.CurrentPage;
            }
        }

        public string CollectTimeOrderByStatus
        {
            set
            {
                hidCollectTimeOrderByStatus.Value = value;
            }
            get
            {
                return (string.IsNullOrEmpty(hidCollectTimeOrderByStatus.Value)) ? "desc" : hidCollectTimeOrderByStatus.Value;
            }
        }

        public string BusinessHourOrderTimeEndOrderByStatus
        {
            set
            {
                hidBusinessHourOrderTimeEndOrderByStatus.Value = value;
            }
            get
            {
                return (string.IsNullOrEmpty(hidBusinessHourOrderTimeEndOrderByStatus.Value)) ? "asc" : hidBusinessHourOrderTimeEndOrderByStatus.Value;
            }
        }

        public string FirstOrderByColumn
        {
            set
            {
                hidFirstOrderByColumn.Value = value;
            }
            get
            {
                return (string.IsNullOrEmpty(hidFirstOrderByColumn.Value)) ? string.Format("{0} {1}", ViewMemberCollectDeal.Columns.CollectTime, CollectTimeOrderByStatus) : hidFirstOrderByColumn.Value;
            }
        }

        public string SecondOrderByColumn
        {
            set
            {
                hidSecondOrderByColumn.Value = value;
            }
            get
            {
                return (string.IsNullOrEmpty(hidSecondOrderByColumn.Value)) ? string.Format("{0} {1}", ViewMemberCollectDeal.Columns.BusinessHourOrderTimeE, BusinessHourOrderTimeEndOrderByStatus) : hidSecondOrderByColumn.Value;
            }
        }

        public bool MemberCollectDealExpireMessage
        {
            set
            {
                cbMemberCollectDealExpireMessage.Checked = value;
            }
            get
            {
                return cbMemberCollectDealExpireMessage.Checked;
            }
        }

        public int OutOfDateDealCount
        {
            set
            {
                litOutOfDateCount.Text = value.ToString();
            }
            get
            {
                int outOfDateCount;
                return int.TryParse(litOutOfDateCount.Text, out outOfDateCount) ? outOfDateCount : 0;
            }
        }

        /// <summary>
        /// 由master page取得的cityid
        /// </summary>
        public int CityId
        {
            get
            {
                return Master.SelectedCityId;
            }
            set
            {
                Master.SelectedPponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(value);
            }
        }

        public int CollectCount
        {
            get
            {
                int temp = 0;
                return int.TryParse(hidCollectCount.Value, out temp) ? temp : 0;
            }
            set
            {
                hidCollectCount.Value = value.ToString();
            }
        }

        public string CollectBidList
        {
            set
            {
                hidCollectBidList.Value = value;
            }
            get
            {
                return (string.IsNullOrEmpty(hidCollectBidList.Value)) ? "" : hidCollectBidList.Value;
            }
        }

        #endregion property

        #region event

        public event EventHandler<DataEventArgs<int>> PageChange = null;
        public event EventHandler CancelOutOfDateCollectDeal = null;
        public event EventHandler<DataEventArgs<Guid>> RemoveCollectDeal = null;

        #endregion

        #region Method

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();
        }

        public void RedirectToLogin()
        {
            FormsAuthentication.RedirectToLoginPage();
        }

        public void SetDealCollectList(ViewMemberCollectDealCollection vmcd)
        {
            rp_CollectList.DataSource = vmcd;
            rp_CollectList.DataBind();
        }

        public int RetrieveRecordCount()
        {
            return CollectCount;
        }

        public void UpdateHandler(int pageNumber)
        {
            if (this.PageChange != null)
            {
                this.PageChange(this, new DataEventArgs<int>(pageNumber));
            }
        }

        public void ReloadPagerTotalCount()
        {
            collectListPager.ResolvePagerView(CurrentPage, true);
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "setPanelShow();", true);
            if (this.PageChange != null)
            {
                this.PageChange(this, new DataEventArgs<int>(collectListPager.CurrentPage));
            }
        }

        protected void lkbtnBusinessHourOrderTimeEnd_Click(object sender, EventArgs e)
        {
            BusinessHourOrderTimeEndOrderByStatus = (BusinessHourOrderTimeEndOrderByStatus == "asc") ? "desc" : "asc";
            FirstOrderByColumn = string.Format("{0} {1}", ViewMemberCollectDeal.Columns.BusinessHourOrderTimeE, BusinessHourOrderTimeEndOrderByStatus);
            SecondOrderByColumn = string.Format("{0} {1}", ViewMemberCollectDeal.Columns.CollectTime, CollectTimeOrderByStatus);

            if (this.PageChange != null)
            {
                this.PageChange(this, new DataEventArgs<int>(collectListPager.CurrentPage));
            }
            UpdatePanel1.Update();
        }

        protected void lkbtnCollectTime_Click(object sender, EventArgs e)
        {
            CollectTimeOrderByStatus = (CollectTimeOrderByStatus == "asc") ? "desc" : "asc";
            FirstOrderByColumn = string.Format("{0} {1}", ViewMemberCollectDeal.Columns.CollectTime, CollectTimeOrderByStatus);
            SecondOrderByColumn = string.Format("{0} {1}", ViewMemberCollectDeal.Columns.BusinessHourOrderTimeE, BusinessHourOrderTimeEndOrderByStatus);

            if (this.PageChange != null)
            {
                this.PageChange(this, new DataEventArgs<int>(collectListPager.CurrentPage));
            }
            UpdatePanel1.Update();
        }

        protected void rp_CollectList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ViewMemberCollectDeal deal = e.Item.DataItem as ViewMemberCollectDeal;

            if (deal != null)
            {
                Image imgDeal = (Image)e.Item.FindControl("imgDeal");
                Literal litDealTitle = (Literal)e.Item.FindControl("litDealTitle");
                Literal litDealPrice = (Literal)e.Item.FindControl("litDealPrice");
                Literal litSoldCount = (Literal)e.Item.FindControl("litSoldCount");              

                IViewPponDeal vpd = PponFacade.ViewPponDealGetByGuid(deal.BusinessHourGuid);

                if (vpd != null)
                {
                    imgDeal.ImageUrl = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).Skip(1).DefaultIfEmpty(ResolveUrl("~/Themes/PCweb/images/ppon-M1_pic.jpg")).First();
                    imgDeal.AlternateText = vpd.EventName;
                    litDealTitle.Text = (string.IsNullOrEmpty(vpd.AppTitle)) ? vpd.CouponUsage : vpd.AppTitle;
                    litDealPrice.Text = CheckZeroPriceToShowPrice(vpd).ToString("F0") + (Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.ComboDealMain) ? "起" : string.Empty);
                    litSoldCount.Text = OrderedQuantityHelper.Show(vpd, OrderedQuantityHelper.ShowType.InPortal);
                }
            }
        }

        protected void btnCancelOutOfDateCollectDeal_Click(object sender, EventArgs e)
        {
            if (OutOfDateDealCount != 0 && this.CancelOutOfDateCollectDeal != null)
            {
                this.CancelOutOfDateCollectDeal(this, e);
            }
        }

        protected void rp_CollectList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "RemoveCollectDeal")
            {
                Guid bid = new Guid();
                if (Guid.TryParse(e.CommandArgument.ToString(), out bid) && this.RemoveCollectDeal != null)
                {
                    this.RemoveCollectDeal(this, new DataEventArgs<Guid>(bid));
                }
            }
        }

        public decimal CheckZeroPriceToShowPrice(IViewPponDeal deal)
        {
            if (decimal.Equals(0, deal.ItemPrice))
            {
                if ((deal.BusinessHourStatus & (int)BusinessHourStatus.PriceZeorShowOriginalPrice) > 0)
                {
                    return deal.ItemOrigPrice;
                }
                else if (CityId.Equals(PponCityGroup.DefaultPponCityGroup.Family.CityId) && deal.ExchangePrice.HasValue && deal.ExchangePrice.Value > default(decimal))
                {
                    return deal.ExchangePrice.Value;
                }
                else
                {
                    return deal.ItemPrice;
                }
            }
            else
            {
                return deal.ItemPrice;
            }
        }

        #endregion Method

        #region WebMethod

        [WebMethod]
        public static bool SetCollectDealExpireMessage(int memberUnique, bool isEnable)
        {
            //check login
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return MemberFacade.MemberSetCollectDealExpireMessage(memberUnique, isEnable);
            }
            return false;
        }

        #endregion WebMethod
    }
}
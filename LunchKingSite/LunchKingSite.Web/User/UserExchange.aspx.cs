﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using System;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Models;

namespace LunchKingSite.Web.User
{
    public partial class UserExchange : LocalizedBasePage, IUserExchange
    {
        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region Property

        private UserExchangePresenter _presenter;

        public UserExchangePresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        public int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(UserName, true);
            }
        }

        public string PanelMode
        {
            get
            {
                return hif_PamelMode.Value;
            }
            set
            {
                PanelChange(value);
            }
        }

        public Guid OrderGuid
        {
            get
            {
                Guid oid;
                return Guid.TryParse(hif_OrderGuid.Value, out oid) ? oid : Guid.Empty;
            }
            set
            {
                hif_OrderGuid.Value = value.ToString();
            }
        }

        public string OrderID
        {
            get
            {
                return hif_OrderID.Value;
            }
        }

        private int OrderStatus
        {
            get
            {
                int orderstatus;
                return int.TryParse(hif_OrderStatus.Value, out orderstatus) ? orderstatus : 0;
            }
        }                   

        public TextBox TxtOrderId
        {
            get { return tbx_OrderID; }
        }
        
        public string alertMessage
        {
            set { AjaxControlToolkit.ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), string.Empty, "alert('" + value + "')", true); }
        }

        public bool IsCoupon
        {
            get
            {
                return cbx_IsCoupon.Checked;
            }
            set
            {
                cbx_IsCoupon.Checked = value;
            }
        }

        #endregion Property

        #region event

        public event EventHandler<DataEventArgs<string>> GetOrderInfo = null;

        public event EventHandler<ExchangeFormArgs> RequestExchange;
        

        #endregion event

        #region method

        public void SetOrderInfo(ViewCouponListMain orderInfo, ViewOrderShipList osInfo)
        {
            lab_OrderStatusMessage.Text = string.Empty;
            DateTime now = DateTime.Now;
            int slug;

            if (orderInfo == null || !orderInfo.IsLoaded)
            {
                lab_OrderStatusMessage.Text = "查無此訂單。";
            }
            else
            {
                #region 頁面填值
                cbx_IsCoupon.Checked = orderInfo.DeliveryType == (int)DeliveryType.ToShop;
                hif_OrderGuid.Value = orderInfo.Guid.ToString();
                hif_OrderID.Value = orderInfo.OrderId;
                hif_OrderStatus.Value = orderInfo.OrderStatus.ToString();

                if (osInfo != null)
                {
                    txtReceiverName.Text = osInfo.MemberName;
                    txtReceiverAddress.Text = osInfo.DeliveryAddress;
                }
                #endregion

                bool gonext = true;
                
                string failReson = "";
                //換貨申請檢查
                if (!OrderExchangeUtility.AllowExchange(orderInfo.Guid, out failReson))
                {
                    gonext = false;

                    if (config.IsConsignment && orderInfo.Consignment)
                    {
                        failReson = string.Format("{0}若您仍須換貨，請<a href='{1}{2}' style='color:blue;'>聯絡客服人員</a>。", failReson, config.SiteUrl, config.SiteServiceUrl);
                    }

                    lab_OrderStatusMessage.Text = failReson;
                }
                else if (orderInfo.BusinessHourOrderTimeE <= now && int.TryParse(orderInfo.Slug, out slug) &&
                         slug < orderInfo.BusinessHourOrderMinimum)
                {
                    //已結檔且未達門檻
                    lab_OrderStatusMessage.Text = "未達門檻無法換貨。";
                    if ((orderInfo.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0 &&
                        (orderInfo.OrderStatus & (int)Core.OrderStatus.Complete) > 0) //ATM已付款，未達門檻顯示退款
                    {
                        lab_OrderStatusMessage.Text =
                            @"此好康未達門檻，請至<a href='coupon_list.aspx' style='color:blue;'>【會員專區】</a>進行指定帳戶退款";
                    }
                }

                #region ATM 檢核

                if ((orderInfo.OrderStatus & (int)Core.OrderStatus.Complete) == 0)
                {
                    if ((orderInfo.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0)
                    {
                        gonext = false;
                        lab_OrderStatusMessage.Text = orderInfo.CreateTime.Date.Equals(DateTime.Now.Date)
                                                        ? "尚未完成ATM付款，無法換貨。"
                                                        : "ATM 逾期未付款，無法換貨。";
                    }
                    else if ((orderInfo.OrderStatus & (int)Core.OrderStatus.FamilyIspOrder) > 0 || (orderInfo.OrderStatus & (int)Core.OrderStatus.SevenIspOrder) > 0)
                    {
                        gonext = false;
                        lab_OrderStatusMessage.Text = "未完成付款的訂單，無法換貨。";
                    }
                    else
                    {
                        lab_OrderStatusMessage.Text = "未完成付款的訂單，無法換貨。";
                    }
                }

                #endregion

                if((orderInfo.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup ||
                    orderInfo.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup) && !(orderInfo.IsReceipt ?? false))
                {
                    gonext = false;
                    lab_OrderStatusMessage.Text = "尚未取貨的訂單，無法換貨。";
                }

                if (gonext)
                {
                    PanelChange("ExchangeNotice");
                }
            }

            if (!string.IsNullOrEmpty(lab_OrderStatusMessage.Text))
            {
                ScriptManager.RegisterStartupScript(this, typeof(Button), "exchangefail", "errormessagebg();", true);
            }
        }

        public void ExchangePanel(string panelmode)
        {
            PanelChange(panelmode);
        }

        protected void ExchangeNoticeConfirm(object sender, EventArgs e)
        {
            PanelChange("ExchangeReasonInput");
        }

        protected void ConfirmExchange(object sender, EventArgs e)
        {
            if (RequestExchange != null)
            {
                var args = new ExchangeFormArgs
                {
                    ExchangeReason = hif_Reason.Value.Trim(),
                    ReceiverName = txtReceiverName.Text,
                    ReceiverAddress = txtReceiverAddress.Text
                };
                
                this.RequestExchange(this, args);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "unblockUI", "$.unblockUI();", true);
            }
        }

        #endregion method

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }

            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
                string orderid = Request.QueryString["orderid"];
                tbx_OrderID.Text = orderid;
            }
            _presenter.OnViewLoaded();

        }

        //1.1
        protected void SerachOrderInfo(object sender, EventArgs e)
        {
            ClearHidden(hif_OrderGuid, hif_OrderID, hif_OrderStatus, hif_PamelMode, hif_Reason);
            if (GetOrderInfo != null)
            {
                GetOrderInfo(sender, new DataEventArgs<string>(tbx_OrderID.Text.Trim()));
            }
        }

        protected void PanelChange(string panelmode)
        {
            foreach (var item in pan_Main.Controls)
            {
                if (item is Panel)
                {
                    ((Panel)item).Visible = false;
                }
            }

            switch (panelmode)
            {
                case "ExchangeOrderIdInput":
                    pan_ExchangeOrderIdInput.Visible = true;
                    break;

                #region 換貨須知 

                case "ExchangeNotice":
                    ClearText(pan_ExchangeOrderIdInput);
                    pan_ExchangeNotice.Visible = true;
                    break;

                #endregion 換貨須知

                case "ExchangeReasonInput":
                    ClearText(pan_ExchangeNotice);
                    hif_Reason.Value = string.Empty;
                    pan_ExchangeReasonInput.Visible = true;
                    break;


                #region 換貨申請成功

                case "ExchangeSuccess":
                    ClearHidden(hif_OrderGuid, hif_OrderID, hif_OrderStatus, hif_PamelMode, hif_Reason);
                    pan_ExchangeSuccess.Visible = true;
                    break;                    

                #endregion 換貨申請成功

                case "ExchangeFail":
                    pan_ExchangeFail.Visible = true;
                    break;
            }
        }

        protected void ClearText(Panel panel)
        {
            foreach (var item in panel.Controls)
            {
                if (item is TextBox)
                {
                    ((TextBox)item).Text = string.Empty;
                }
                else if (item is Label)
                {
                    ((Label)item).Text = string.Empty;
                }
            }
        }

        protected void ClearText(params Label[] items)
        {
            foreach (var item in items)
            {
                item.Text = string.Empty;
            }
        }

        protected void ClearHidden(params HiddenField[] items)
        {
            foreach (var item in items)
            {
                item.Value = string.Empty;
            }
        }

        #endregion page
    }
}
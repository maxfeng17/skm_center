﻿using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.WebLib.UI;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;

namespace LunchKingSite.Web.User
{
    public partial class coupon_List : LocalizedBasePage, IPponCouponListView
    {
        public ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
        public IFamiportProvider fp = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
        private static ILog logger = LogManager.GetLogger(typeof(coupon_List));
        private static IMGMProvider mgmp = ProviderFactory.Instance().GetProvider<IMGMProvider>();

        #region Property defined in View
        public PponCouponListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(User.Identity.Name, true);
            }
        }
        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }
        public string UserEmail
        {
            get
            {
                return User.Identity.Name;
            }
        }
        public bool IsLatest
        {
            get
            {
                return cbx_LatestOrders.Checked;
            }
        }
        public CouponListFilterType FilterType
        {
            get
            {
                CouponListFilterType type;
                if (Enum.TryParse<CouponListFilterType>(ddl_Filter.SelectedValue, out type))
                {
                    return type;
                }
                else
                {
                    return CouponListFilterType.None;
                }
            }
        }
        public NewCouponListFilterType NewFilterType
        {
            get
            {
                NewCouponListFilterType type;
                if (Enum.TryParse<NewCouponListFilterType>(ddl_Filter.SelectedValue, out type))
                {
                    return type;
                }
                else
                {
                    return NewCouponListFilterType.None;
                }
            }
        }
        public MemberOrderMainFilterType MainFilterType
        {
            get
            {
                MemberOrderMainFilterType type;
                if (Enum.TryParse<MemberOrderMainFilterType>(ddl_Filter.SelectedValue, out type))
                {
                    return type;
                }
                else
                {
                    return MemberOrderMainFilterType.All;
                }
            }
        }
        public int PageCount
        {
            get
            {
                int pagecount;
                return int.TryParse((ViewState["pagecount"] == null ? string.Empty : ViewState["pagecount"].ToString()), out pagecount) ? pagecount : 0;
            }
            set
            {
                ViewState["pagecount"] = value;
            }
        }
        public int Department
        {
            get
            {
                int dep;
                return int.TryParse(hif_Dep.Value, out dep) ? dep : 0;
            }
        }
        public int OrderStatus
        {
            get
            {
                int orderstatus;
                return int.TryParse(hif_OrderStatus.Value, out orderstatus) ? orderstatus : 0;
            }
        }
        public int GroupStatus
        {
            get
            {
                int groupstatus;
                return int.TryParse(hif_GroupStatus.Value, out groupstatus) ? groupstatus : 0;
            }
        }
        public int CancelStatus
        {
            get
            {
                int cancelstatus;
                return int.TryParse(hif_CancelStatus.Value, out cancelstatus) ? cancelstatus : -1;
            }
        }
        public int BusinessHourStatus
        {
            get
            {
                int businesshourstatus;
                return int.TryParse(hif_BusinessHourStatus.Value, out businesshourstatus) ? businesshourstatus : 0;
            }
        }
        public int CurrentQuantity
        {
            get
            {
                int currentquantity;
                return int.TryParse(hif_CurrentQuantity.Value, out currentquantity) ? currentquantity : 0;
            }
            set
            {
                hif_CurrentQuantity.Value = value.ToString();
            }
        }
        public DateTime ExpiredTime
        {
            get
            {
                DateTime expiredtime;
                return DateTime.TryParse(hif_ExpiredTime.Value, out expiredtime) ? expiredtime : DateTime.Now;
            }
        }
        public DateTime OrderEndDate
        {
            get
            {
                DateTime orderenddate;
                return DateTime.TryParse(hif_OrderEndDate.Value, out orderenddate) ? orderenddate : DateTime.Now;
            }
        }
        public Guid OrderGuid
        {
            get
            {
                Guid oid;
                if (Request.QueryString["oid"] != null)
                {
                    Guid.TryParse(Request.QueryString["oid"].ToString(), out oid);
                }
                else
                {
                    Guid.TryParse(hif_Oid.Value, out oid);
                }

                return oid;
            }
        }
        public int Slug
        {
            get
            {
                int slug;
                return int.TryParse(hif_Slug.Value, out slug) ? slug : 0;
            }
        }
        public int OrderMin
        {
            get
            {
                int ordermin;
                return int.TryParse(hif_OrderMin.Value, out ordermin) ? ordermin : 0;
            }
        }
        public int ItemPrice
        {
            get
            {
                int itemprice;
                return int.TryParse(hif_ItemPrice.Value, out itemprice) ? itemprice : 0;
            }
        }
        public int AccBusinessGroupId
        {
            get
            {
                int id;
                return int.TryParse((ViewState["AccBusinessGroupId"] == null ? string.Empty : ViewState["AccBusinessGroupId"].ToString()), out id) ? id : 0;
            }
            set
            {
                ViewState["AccBusinessGroupId"] = value;
            }
        }
        public string OrderId
        {
            get
            {
                if (ViewState["OrderId"] != null)
                    return ViewState["OrderId"].ToString();
                else
                    return string.Empty;
            }
            set
            {
                ViewState["OrderId"] = value;
            }
        }
        public ThirdPartyPayment ThirdPartyPaymentSystem
        {
            set { _thirdPartyPaymentSystem = value; }
            get { return _thirdPartyPaymentSystem; }
        }
        #endregion

        #region Property
        private PponCouponListPresenter _presenter;
        public DateTime OrderDate
        {
            get
            {
                DateTime orderdate;
                return DateTime.TryParse(hif_OrderDate.Value, out orderdate) ? orderdate : DateTime.Now;
            }
        }
        public DateTime UseEndDate
        {
            get;
            set;
        }
        public bool IsServiceButtonVisible
        {
            get
            {
                return conf.PponServiceButtonVisible;
            }
        }
        private ThirdPartyPayment _thirdPartyPaymentSystem = ThirdPartyPayment.None;
        private Dictionary<int, string> couponSN = new Dictionary<int, string>();
        public class ApplyRefund
        {
            public bool isAllow { get; set; }
            public string reson { get; set; }
        }

        #endregion Property

        #region Event defined in View
        public event EventHandler<DataEventArgs<int>> PageChange = null;
        public event EventHandler FilterChange = null;
        public event EventHandler<DataEventArgs<Guid>> ServiceAsk;
        #endregion event


        /// <summary>
        /// 因全家寄杯, 故要固定下拉順序
        /// </summary>
        /// <returns></returns>
        private ListItem[] GetNewCouponListFilterType()
        {
            List<ListItem> lic = new List<ListItem>();
            if (conf.OrderFilterVerion == (int)OrderFilterVerion.MemberOrderMainFilterType)
            {
                foreach (var d in MemberFacade.MemberOrderMainFilterTypes()) // query each fields from enum
                {
                    lic.Add(new ListItem
                    {
                        Text = Helper.GetLocalizedEnum(d),
                        Value = ((int)d).ToString(),
                    });
                }
            }
            else
            {
                foreach (var d in MemberFacade._newCouponListFilterTypes()) // query each fields from enum
                {
                    lic.Add(new ListItem
                    {
                        Text = Helper.GetLocalizedEnum(d),
                        Value = ((int)d).ToString(),
                    });
                }
            }
            return lic.ToArray();
        }

        #region Method defined in View
        /// <summary>
        /// 初始化篩選用下拉選單
        /// </summary>
        public void InitFilterDropDwonList()
        {
            ddl_Filter.Items.Clear();
            ddl_Filter.Items.AddRange(GetNewCouponListFilterType());

            //暫時判斷,待全家咖啡寄杯正式上線後就可以刪了
            if (conf.EnableFamiCoffee == false)
            {
                ListItem famiListItem = ddl_Filter.Items.FindByValue(((int)NewCouponListFilterType.FamiCoffee).ToString());
                if (famiListItem != null)
                {
                    ddl_Filter.Items.Remove(famiListItem);
                }
            }

            //暫時判斷,待SKMfilter正式上線後就可以刪了
            if (conf.IsShowSKMFilter == false)
            {
                ListItem skmListItem = ddl_Filter.Items.FindByValue(((int)NewCouponListFilterType.SKM).ToString());
                if (skmListItem != null)
                {
                    ddl_Filter.Items.Remove(skmListItem);
                }
            }
        }

        public void GetCouponList(ViewCouponListMainCollection data)
        {
            rp_CouponListMain.DataSource = data;
            rp_CouponListMain.DataBind();
        }

        public void SetUpPageCount()
        {
            gridPager.ResolvePagerView(PageCount > 0 ? 1 : 0, true);
        }

        public void SetUserMobileNumber()
        {
            hif_Mobile.Value = Presenter.GetUserMobileNumber();
        }

        public void ShowAlert(string msg)
        {
            string js = string.Format("alert('{0}'); location.href='{1}'", msg, WebUtility.GetSiteRoot());
            ScriptManager.RegisterStartupScript(this, typeof(Button), "alert", js, true);
        }

        #endregion

        #region Page (inline method or event)
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
                InitFilterDropDwonList();
            }
            _presenter.OnViewLoaded();

            cbx_LatestOrders.Visible = conf.OrderFilterVerion != (int)OrderFilterVerion.MemberOrderMainFilterType;
        }

        protected int RetrieveOrderCount()
        {
            return PageCount;
        }

        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChange != null)
            {
                this.PageChange(this, new DataEventArgs<int>(pageNumber));
            }
        }

        protected void ChangeFilter(object sender, EventArgs e)
        {
            if (this.FilterChange != null)
            {
                this.FilterChange(this, e);
            }
        }

        protected void rp_MainItemBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewCouponListMain)
            {
                bool isZeroPponDeal = false;
                DateTime now = DateTime.Now;
                Label clab_rpUseCount = ((Label)e.Item.FindControl("lab_rpUseCount"));//憑證狀態
                Label clab_rpOrderStatus = ((Label)e.Item.FindControl("lab_rpOrderStatus"));//訂單狀態
                Label clab_rpOrderId = ((Label)e.Item.FindControl("lab_rpOrderId"));
                Label clab_exchangeAndReturn = ((Label)e.Item.FindControl("lbl_exchangeAndReturn"));//申請換貨 申請退貨
                Label clab_mbutton = ((Label)e.Item.FindControl("lbl_mbutton"));//m版Button 申請換貨 申請退貨 聯絡客服            
                Label clab_contactService = ((Label)e.Item.FindControl("lbl_contactService"));//聯絡客服
                Label clab_information = ((Label)e.Item.FindControl("lbl_information"));//出貨資訊鈕
                Label clab_orderedTime = (Label) e.Item.FindControl("lab_orderedTime");
                Button clbt_CouponDetail = ((Button)e.Item.FindControl("lbt_rpCouponDetail"));//憑證鈕
                Button clbt_CouponEvaluate = ((Button)e.Item.FindControl("lbt_rpCouponEvaluate"));//評價紐
                Button clbt_MCouponEvaluate = ((Button)e.Item.FindControl("lbt_rpMCouponEvaluate"));//M評價紐
                LinkButton clbt_rpOrderDetail = ((LinkButton)e.Item.FindControl("lbt_rpOrderDetail")); //訂單編號鈕
                Button clbt_rp_PayATM = ((Button)e.Item.FindControl("lbt_rp_PayATM"));
                //Button btnService = ((Button)e.Item.FindControl("btnService"));
                //Button btnService2 = ((Button)e.Item.FindControl("btnService2"));
                Button btnOrderDetail = ((Button)e.Item.FindControl("btnOrderDetail"));
                Button clbt_BookingSystem = ((Button)e.Item.FindControl("btnBookingSystem"));
                Button clbt_BookingSystem2 = ((Button)e.Item.FindControl("btnBookingSystem2"));

                ViewCouponListMain main = (ViewCouponListMain)(e.Item.DataItem);
                isZeroPponDeal = decimal.Equals(0, main.ItemPrice) ? true : false;

                clab_orderedTime.Text = main.CreateTime.ToString("yyyy/MM/dd");
                var ispOrder = OrderFacade.GetIspPaymentOrder(main.OrderId);
                if (ispOrder.IsLoaded && ispOrder.DeliveryTime != null)
                {
                    clab_orderedTime.Text = ((DateTime)ispOrder.DeliveryTime).ToString("yyyy/MM/dd");
                }

                clab_rpOrderId.Visible = isZeroPponDeal;
                clbt_CouponEvaluate.Visible = false;
                clbt_MCouponEvaluate.Visible = false;
                clab_information.Visible = false;
                //隱藏換貨按鈕
                if (conf.EnabledExchangeProcess == false || main.DeliveryType != (int)DeliveryType.ToHouse)
                {
                    clab_exchangeAndReturn.Text = string.Format(
                        "<a href='javascript:void(0)' onclick='btnReturnsEventRegister(\"{0}\",\"{1}\",\"{2}\")'>" +
                        "<span class='btn btn-mini rd-C-Hide' style='width:52px;'>退貨</span></a>",
                        main.DeliveryType, main.Guid, main.OrderId);
                    
                    //m版案件區塊
                    clab_mbutton.Text = string.Format(
                        "<a href='javascript:void(0)' onclick='btnReturnsEventRegister(\"{0}\",\"{1}\",\"{2}\")'>" +
                        "<span class='btn btn-mini'>申請退貨</span></a>&nbsp;" +
                        "<a href='javascript:void(0)' onclick='redirectService(\"{3}\")'>" +
                        "<span class='btn btn-mini'>聯絡客服<i class='fa fa-headphones fa-fw'></i></span></a>",
                        main.DeliveryType, main.Guid, main.OrderId, main.Guid);
                }
                else
                {
                    //超付取消訂單
                    bool showCancelBtn = conf.EnableCancelPaidByIspOrder &&
                        (Helper.IsFlagSet(main.OrderStatus, (int)Core.OrderStatus.FamilyIspOrder) ||
                        Helper.IsFlagSet(main.OrderStatus, (int)Core.OrderStatus.SevenIspOrder)) &&
                        !Helper.IsFlagSet(main.OrderStatus, (int)Core.OrderStatus.Complete);

                    clab_exchangeAndReturn.Text = string.Format(
                        "<a href='javascript:void(0)' onclick='btnExchangeEventRegister(\"{0}\",\"{1}\")'>" +
                        "<span class='btn btn-mini rd-C-Hide' style='margin-bottom:5px; {2}'>{3}</span></a><br />" +
                        "<a href='javascript:void(0)' onclick='btnReturnsEventRegister(\"{4}\",\"{5}\",\"{6}\")'>" +
                        "<span class='btn btn-mini rd-C-Hide' style='width:52px;'>{7}</span></a>",
                        main.DeliveryType, main.OrderId, showCancelBtn ? "display:none;" : "",
                        conf.EnabledRefundOrExchangeEditReceiverInfo ? "換貨/缺件" : "換貨", main.DeliveryType, main.Guid, main.OrderId,
                        showCancelBtn ? "取消訂單" : "退貨");
                        
                    //m版案件區塊
                    clab_mbutton.Text = string.Format(
                        "<a href='javascript:void(0)' onclick='btnReturnsEventRegister(\"{0}\",\"{1}\",\"{2}\")'>" +
                        "<span class='btn btn-mini'>{3}</span></a>&nbsp;" +
                        "<a href='javascript:void(0)' onclick='btnExchangeEventRegister(\"{4}\",\"{5}\")'>" +
                        "<span class='btn btn-mini'{6}>{7}</span></a>&nbsp;" +
                        "<a href='javascript:void(0)' onclick='redirectService(\"{8}\")'>" +
                        "<span class='btn btn-mini'>聯絡客服<i class='fa fa-headphones fa-fw'></i></span></a>",
                        main.DeliveryType, main.Guid, main.OrderId, showCancelBtn ? "取消訂單" : "申請退貨", main.DeliveryType,
                        main.OrderId, showCancelBtn ? " style='display:none;'" : string.Empty, 
                        conf.EnabledRefundOrExchangeEditReceiverInfo ? "換貨/缺件" : "申請換貨", main.Guid);
                }

                //聯絡客服
                clab_contactService.Text = "<a href='javascript:void(0)' onclick='redirectService(\"" + main.Guid + "\")'><span class='btn btn-mini rd-C-Hide'>聯絡客服<i class='fa fa-headphones fa-fw'></i></span></a>";

                var evaluateCount = Presenter.GetEvaluatCount(main.Guid);
                var gifts = mgmp.GiftGetByOid(main.Guid).Where(x => x.IsActive == true && x.IsUsed == false).ToList();
                var giftCount = gifts.Any() ? gifts.Count : 0; //正在送禮階段數量

                //first time into the page
                //零元檔次
                if (isZeroPponDeal)
                {
                    clbt_rpOrderDetail.Visible = false;
                    //btnService.Visible = false;
                    //btnService2.Visible = false;
                    btnOrderDetail.Visible = false;

                    if ((main.Status & (int)GroupOrderStatus.FamiDeal) > 0)
                    {
                        HtmlGenericControl ItemPrice = ((HtmlGenericControl)e.Item.FindControl("pItemPrice"));
                        ItemPrice.InnerHtml = GetZeroDealTagContent((CouponCodeType)Enum.ToObject(typeof(CouponCodeType), main.CouponCodeType));
                        clbt_CouponDetail.OnClientClick = string.Format("openFamiCoupon({0});return false;", _presenter.GetFirstCouponIdByOrderId(main.Guid));

                    }
                }
                else
                {
                    if (main.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToShop, main.DeliveryType.Value) && main.ChangedExpireDate.HasValue)
                    {
                        clbt_rpOrderDetail.Attributes.Add("expiredtime", (main.BusinessHourDeliverTimeE == null ? string.Empty : main.ChangedExpireDate.Value.AddDays(1).ToString()));
                    }
                    else
                    {
                        clbt_rpOrderDetail.Attributes.Add("expiredtime", (main.BusinessHourDeliverTimeE == null ? string.Empty : main.BusinessHourDeliverTimeE.Value.AddDays(1).ToString()));
                    }


                    clbt_rpOrderDetail.Attributes.Add("orderenddate", main.BusinessHourOrderTimeE.ToString());
                    clbt_rpOrderDetail.Attributes.Add("itemname", main.ItemName);
                    clbt_rpOrderDetail.Attributes.Add("bid", main.BusinessHourGuid.ToString());
                    clbt_rpOrderDetail.Attributes.Add("orderid", main.OrderId);
                    clbt_rpOrderDetail.Attributes.Add("orderstatus", main.OrderStatus.ToString());
                    clbt_rpOrderDetail.Attributes.Add("groupstatus", main.Status.ToString());
                    clbt_rpOrderDetail.Attributes.Add("businesshourstatus", main.BusinessHourStatus.ToString());
                    clbt_rpOrderDetail.Attributes.Add("cancelstatus", main.ReturnStatus.ToString());
                    clbt_rpOrderDetail.Attributes.Add("dep", main.Department.ToString());
                    clbt_rpOrderDetail.Attributes.Add("subtotal", main.Subtotal.ToString("F0"));
                    clbt_rpOrderDetail.Attributes.Add("total", main.Total.ToString("F0"));
                    clbt_rpOrderDetail.Attributes.Add("itemprice", main.ItemPrice.ToString("F0"));
                    clbt_rpOrderDetail.Attributes.Add("itemoprice", main.ItemOrigPrice.ToString("F0"));
                    clbt_rpOrderDetail.Attributes.Add("slug", main.Slug);
                    clbt_rpOrderDetail.Attributes.Add("ordermin", main.BusinessHourOrderMinimum.ToString("F0"));
                    clbt_rpOrderDetail.Attributes.Add("orderdate", main.CreateTime.ToString());
                    clbt_rpOrderDetail.Attributes.Add("deliverytype", (main.DeliveryType.HasValue ? main.DeliveryType.Value.ToString() : ((int)DeliveryType.ToShop).ToString())); //add deliverytype for shoppingcart by may

                    //btnService.Visible = btnService2.Visible = IsServiceButtonVisible;
                }
                //訂單已取消
                bool iscancel = false;
                bool isPpon = (!main.DeliveryType.HasValue || int.Equals((int)DeliveryType.ToShop, main.DeliveryType.Value));
                //部分退貨
                int returnedQuantity = _presenter.GetReturnedQuantity(main.Guid);
                bool isPartialCancel = returnedQuantity > 0 && main.TotalCount > returnedQuantity;
                //換貨訂單
                bool isExchangeOrder = (main.ExchangeStatus == (int)OrderLogStatus.ExchangeCancel ||
                                        main.ExchangeStatus == (int)OrderLogStatus.ExchangeFailure ||
                                        main.ExchangeStatus == (int)OrderLogStatus.ExchangeProcessing ||
                                        main.ExchangeStatus == (int)OrderLogStatus.ExchangeSuccess ||
                                        main.ExchangeStatus == (int)OrderLogStatus.SendToSeller);
                //退貨訂單
                bool isReturnOrder = (main.ReturnStatus != (int)ProgressStatus.Canceled && main.ReturnStatus != -1);

                if (!isPpon)
                {
                    clab_rpUseCount.Text = "購物商品";
                    clab_information.Visible = true;
                    clab_information.Text = "<a href='javascript:void(0)' onclick='redirectInformation(\"" + main.Guid+"\")'><span class='btn btn-mini'>出貨資訊</span></a>";
                }
                else
                {
                    if (evaluateCount > 0)
                    {
                        clbt_MCouponEvaluate.Visible = true;
                        clbt_MCouponEvaluate.ToolTip = "還有" + evaluateCount + "筆可以填";

                        clbt_CouponEvaluate.Visible = true;
                        clbt_CouponEvaluate.ToolTip = "還有" + evaluateCount + "筆可以填";
                    }
                }

                if ((main.OrderStatus & ((int)LunchKingSite.Core.OrderStatus.Cancel)) > 0) //退貨完成
                {
                    iscancel = true;
                    clbt_CouponDetail.Visible = clbt_rp_PayATM.Visible = false; 

                    if (conf.EnableCancelPaidByIspOrder)
                    {
                        clab_rpOrderStatus.Text = isPartialCancel
                        ? "部分退貨"
                        : (main.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0 && main.CreateTime.ToShortDateString() != now.ToShortDateString()
                            ? "逾期未付款"
                            : main.SellerGoodsStatus == (int)GoodsStatus.DcReturn
                                ? "逾期未取件"
                                : main.SellerGoodsStatus == (int)GoodsStatus.DcReceiveFail
                                    ? "訂單異常，<br/>訂單已取消" :
                                    //當退貨明細顯示退貨已完成時，廠商出貨資訊的出貨進度不能顯示"訂單已取消"，要顯示"退貨已完成"
                                     (main.ReturnStatus == (int)ProgressStatus.Completed || main.ReturnStatus == (int)ProgressStatus.CompletedWithCreditCardQueued) ?
                                    "退貨已完成" : "訂單已取消";
                    }
                    else
                    {
                        clab_rpOrderStatus.Text = isPartialCancel
                        ? "部分退貨"
                        : (main.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0 && main.CreateTime.ToShortDateString() != now.ToShortDateString()
                             ? "逾期未付款" : "訂單已取消";
                    }

                    if (isPpon && main.TotalCount != 0)
                    {
                        if (isPartialCancel)
                        {
                            clab_rpUseCount.Text = string.Empty;

                            if (main.RemainCount > 0)
                            {
                                clbt_CouponDetail.Visible = true; //憑證鈕
                                clab_rpUseCount.Text = string.Format("未使用:{0}", main.RemainCount - giftCount);
                                clbt_BookingSystem2.Visible =
                                    clbt_BookingSystem.Visible =
                                        _presenter.IsEnableBookingSystem(main.Guid, (BookingType)main.BookingSystemType);
                                if (IsPastFinalExpireDate(main))
                                {
                                    clbt_CouponDetail.Visible = false;
                                    clab_rpUseCount.Text = "憑證已過期";
                                    SetBookingToQueryBookingButton(clbt_BookingSystem, clbt_BookingSystem2);
                                }
                                else
                                {
                                    EnableBookingButton(clbt_BookingSystem, clbt_BookingSystem2, main.Guid,
                                        (BookingType)main.BookingSystemType);
                                }
                            }

                        }
                        if (evaluateCount == 0)
                        {
                            clbt_MCouponEvaluate.Visible = true;
                            clbt_MCouponEvaluate.Enabled = false;
                            clbt_MCouponEvaluate.CssClass = "btn btn-mini btn-dontclick rd-float-lft display_none rd-DisplayPanel";
                            clbt_MCouponEvaluate.ToolTip = "已填寫完畢或逾期";

                            clbt_CouponEvaluate.Visible = true;
                            clbt_CouponEvaluate.Enabled = false;
                            clbt_CouponEvaluate.CssClass = "btn btn-mini btn-dontclick rd-C-Hide";
                            clbt_CouponEvaluate.ToolTip = "已填寫完畢或逾期";
                        }
                    }
                }
                else if (isExchangeOrder || isReturnOrder)  //退or換貨訂單
                {
                    clbt_CouponDetail.Visible = clbt_rp_PayATM.Visible = false;
                    iscancel = isReturnOrder;
                    var returnModifyTime = main.ReturnModifyTime ?? DateTime.MinValue;
                    var exchangeModifyTime = main.ExchangeModifyTime ?? DateTime.MinValue;

                    //依最新一筆退換貨單紀錄之異動時間 抓取最新一筆 顯示訂單退換貨狀態
                    if (exchangeModifyTime > returnModifyTime)
                    {

                        #region 換貨部分

                        switch (main.ExchangeStatus)
                        {

                            case (int)OrderLogStatus.SendToSeller:
                            case (int)OrderLogStatus.ExchangeProcessing:
                                clab_rpOrderStatus.Text = Resources.Localization.ExchangeProcessing;
                                break;

                            case (int)OrderLogStatus.ExchangeSuccess:
                                clab_rpOrderStatus.Text = Resources.Localization.ExchangeSuccess;
                                break;

                            case (int)OrderLogStatus.ExchangeFailure:
                                clab_rpOrderStatus.Text = Resources.Localization.ExchangeFailure;
                                break;

                            case (int)OrderLogStatus.ExchangeCancel:
                                clab_rpOrderStatus.Text = Resources.Localization.ExchangeCancel;
                                break;

                        }

                        #endregion 換貨部分

                    }
                    else
                    {

                        #region 退貨部分

                        switch (main.ReturnStatus)
                        {
                            case (int)ProgressStatus.Processing:
                            case (int)ProgressStatus.AtmQueueing:
                            case (int)ProgressStatus.AtmQueueSucceeded:
                            case (int)ProgressStatus.AtmFailed:
                            case (int)ProgressStatus.Retrieving:
                            case (int)ProgressStatus.ConfirmedForCS:
                            case (int)ProgressStatus.RetrieveToPC:
                            case (int)ProgressStatus.RetrieveToCustomer:
                            case (int)ProgressStatus.ConfirmedForVendor:
                            case (int)ProgressStatus.ConfirmedForUnArrival:
                                clab_rpOrderStatus.Text = "退貨處理中";
                                break;

                            case (int)ProgressStatus.Completed:
                            case (int)ProgressStatus.CompletedWithCreditCardQueued:
                                #region Completed
                                clab_rpOrderStatus.Text = "訂單已取消";

                                if (isPpon)
                                {
                                    if (main.TotalCount != 0)
                                    {
                                        if (isPartialCancel)
                                        {
                                            clab_rpUseCount.Text = string.Empty;
                                            if (main.RemainCount > 0)
                                            {
                                                clbt_CouponDetail.Visible = true;
                                                clab_rpUseCount.Text = string.Format("未使用:{0}", main.RemainCount - giftCount);
                                                if (IsPastFinalExpireDate(main))
                                                {
                                                    clbt_CouponDetail.Visible = false;
                                                    clab_rpUseCount.Text = "憑證已過期";
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (isPartialCancel)
                                    {
                                        clab_rpOrderStatus.Text = "部分退貨<br/>";

                                        if (PponOrderManager.CheckIsOverTrialPeriod(main.Guid))
                                        {
                                            clab_rpOrderStatus.Text += " 已過鑑賞期";
                                        }
                                        else
                                        {
                                            //訂單狀態顯示出貨資訊
                                            //檢查是否已出貨
                                            if (PponOrderManager.CheckIsShipped(main.Guid, OrderClassification.LkSite))
                                            {
                                                clab_rpOrderStatus.Text += " 已出貨";
                                            }
                                            else
                                            {
                                                ShipInfo shipInfo = OrderFacade.GetOrderShipInfo(main);
                                                clab_rpOrderStatus.Text = string.IsNullOrEmpty(shipInfo.LastShippingDate) ? "照會中" : "最後出貨日<br/>" + shipInfo.LastShippingDate;
                                            }
                                        }
                                    }
                                }
                                break;
                            #endregion Completed

                            case (int)ProgressStatus.Unreturnable:

                                #region Unreturnable

                                clab_rpOrderStatus.Text = "退貨失敗";
                                clbt_BookingSystem2.Visible = clbt_BookingSystem.Visible = _presenter.IsEnableBookingSystem(main.Guid, (BookingType)main.BookingSystemType);
                                if (!IsPastFinalExpireDate(main))
                                {
                                    EnableBookingButton(clbt_BookingSystem, clbt_BookingSystem2, main.Guid, (BookingType)main.BookingSystemType);
                                }
                                else
                                {
                                    SetBookingToQueryBookingButton(clbt_BookingSystem, clbt_BookingSystem2);
                                }

                                break;

                                #endregion Unreturnable
                        }

                        #endregion 退貨部分

                    }

                }
                else if (main.IsCanceling && conf.EnableCancelPaidByIspOrder)
                {
                    clbt_CouponDetail.Visible = clbt_rp_PayATM.Visible = false;
                    clab_rpOrderStatus.Text = "訂單取消中";
                }
                else if (main.BusinessHourOrderTimeE <= now)    //已結檔
                {
                    int slug;
                    if (int.TryParse(main.Slug, out slug) && slug < main.BusinessHourOrderMinimum)
                    {
                        clbt_CouponDetail.Visible = clbt_rp_PayATM.Visible = false;
                        clab_rpOrderStatus.Text = "未達門檻";
                    }
                    else
                    {
                        if (isPpon)
                        {
                            if (main.TotalCount != 0)
                            {
                                clbt_CouponDetail.Visible = (main.RemainCount > 0);
                                clbt_BookingSystem2.Visible = clbt_BookingSystem.Visible = _presenter.IsEnableBookingSystem(main.Guid, (BookingType)main.BookingSystemType);

                                if (main.RemainCount > 0)
                                {
                                    clab_rpUseCount.Text = "未使用:" + main.RemainCount;
                                    if (IsPastFinalExpireDate(main))
                                    {
                                        clbt_CouponDetail.Visible = false;
                                        clab_rpUseCount.Text = "憑證已過期";
                                        SetBookingToQueryBookingButton(clbt_BookingSystem, clbt_BookingSystem2);
                                        if (string.IsNullOrEmpty(clab_rpOrderStatus.Text))
                                        {
                                            if (OrderFacade.IsExpiredVerifiedDeal(main))
                                            {
                                                clab_rpOrderStatus.Text = "逾期無法退費";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        EnableBookingButton(clbt_BookingSystem, clbt_BookingSystem2, main.Guid, (BookingType)main.BookingSystemType);
                                    }
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(clab_rpUseCount.Text))
                                    {
                                        if (OrderFacade.IsExpiredVerifiedDeal(main))
                                        {
                                            clab_rpUseCount.Text = "逾期未使用";
                                        }
                                    }
                                    if (string.IsNullOrEmpty(clab_rpUseCount.Text))
                                    {
                                        clab_rpUseCount.Text = "使用完畢";
                                    }                                       
                                    SetBookingToQueryBookingButton(clbt_BookingSystem, clbt_BookingSystem2);
                                    if (evaluateCount == 0)
                                    {
                                        clbt_MCouponEvaluate.Visible = true;
                                        clbt_MCouponEvaluate.Enabled = false;
                                        clbt_MCouponEvaluate.CssClass = "btn btn-mini btn-dontclick rd-float-lft display_none rd-DisplayPanel";
                                        clbt_MCouponEvaluate.ToolTip = "已填寫完畢或逾期";

                                        clbt_CouponEvaluate.Visible = true;
                                        clbt_CouponEvaluate.Enabled = false;
                                        clbt_CouponEvaluate.CssClass = "btn btn-mini btn-dontclick rd-C-Hide";
                                        clbt_CouponEvaluate.ToolTip = "已填寫完畢或逾期";
                                    }
                                }
                            }
                            else if (IsPastFinalExpireDate(main))
                            {
                                clbt_CouponDetail.Visible = false;
                                clab_rpUseCount.Text = "憑證已過期";
                            }
                        }
                        else
                        {
                            clbt_CouponDetail.Visible = false;
                            if (PponOrderManager.CheckIsOverTrialPeriod(main.Guid))
                            {
                                clab_rpOrderStatus.Text = "已過鑑賞期";
                            }
                            else
                            {
                                //訂單狀態顯示出貨資訊
                                //檢查是否已出貨
                                if (PponOrderManager.CheckIsShipped(main.Guid, OrderClassification.LkSite))
                                {
                                    clab_rpOrderStatus.Text = "已出貨";
                                }
                                else
                                {
                                    ShipInfo shipInfo = OrderFacade.GetOrderShipInfo(main);
                                    clab_rpOrderStatus.Text = string.IsNullOrEmpty(shipInfo.LastShippingDate) ? "照會中" : "最後出貨日<br/>" + shipInfo.LastShippingDate;
                                }
                            }
                        }

                        if ((main.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0 && (main.OrderStatus & (int)Core.OrderStatus.Complete) == 0) //ATM未付款，已達門檻不顯示退款
                        {
                            if (isPpon)
                            {
                                clab_rpUseCount.Text = string.Empty;
                            }
                            clbt_CouponDetail.Visible = false;
                            if (main.CreateTime.Year == now.Year && main.CreateTime.Month == now.Month && main.CreateTime.Day == now.Day)
                            {
                                clbt_rp_PayATM.Visible = true;
                            }
                        }
                    }
                }
                else    //尚在搶購中
                {
                    if (isPpon)
                    {
                        if (main.TotalCount != 0)
                        {
                            clbt_CouponDetail.Visible = (main.RemainCount > 0);
                            clbt_BookingSystem2.Visible = clbt_BookingSystem.Visible = _presenter.IsEnableBookingSystem(main.Guid, (BookingType)main.BookingSystemType);

                            if (main.RemainCount > 0)
                            {
                                clab_rpUseCount.Text = string.Format("未使用:{0}", main.RemainCount - giftCount);

                                if (IsPastFinalExpireDate(main))
                                {
                                    clbt_CouponDetail.Visible = false;
                                    clab_rpUseCount.Text = "憑證已過期";
                                    SetBookingToQueryBookingButton(clbt_BookingSystem, clbt_BookingSystem2);
                                }
                                else
                                {
                                    EnableBookingButton(clbt_BookingSystem, clbt_BookingSystem2, main.Guid, (BookingType)main.BookingSystemType);
                                }
                            }
                            else
                            {
                                clab_rpUseCount.Text = "使用完畢";
                                SetBookingToQueryBookingButton(clbt_BookingSystem, clbt_BookingSystem2);
                                if (evaluateCount == 0)
                                {
                                    clbt_MCouponEvaluate.Visible = true;
                                    clbt_MCouponEvaluate.Enabled = false;
                                    clbt_MCouponEvaluate.CssClass = "btn btn-mini btn-dontclick rd-float-lft display_none rd-DisplayPanel";
                                    clbt_MCouponEvaluate.ToolTip = "已填寫完畢或逾期";

                                    clbt_CouponEvaluate.Visible = true;
                                    clbt_CouponEvaluate.Enabled = false;
                                    clbt_CouponEvaluate.CssClass = "btn btn-mini btn-dontclick rd-C-Hide";
                                    clbt_CouponEvaluate.ToolTip = "已填寫完畢或逾期";
                                }
                            }
                        }
                        //ATM
                        if ((main.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0 && (main.OrderStatus & (int)Core.OrderStatus.Complete) == 0)
                        {
                            clab_rpUseCount.Text = string.Empty;
                            clbt_CouponDetail.Visible = false;
                            if (main.CreateTime.Year == now.Year && main.CreateTime.Month == now.Month && main.CreateTime.Day == now.Day)
                            {
                                clbt_rp_PayATM.Visible = true;
                            }
                            else
                            {
                                clbt_rp_PayATM.Visible = false;
                                clab_rpOrderStatus.Text = "逾期未付款";
                            }
                        }
                    }
                    else
                    {
                        bool isIsp = main.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup ||
                                     main.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup;

                        clbt_CouponDetail.Visible = false;
                        if (PponOrderManager.CheckIsOverTrialPeriod(main.Guid))
                        {
                            clab_rpOrderStatus.Text = "已過鑑賞期";
                        }
                        else
                        {
                            //訂單狀態顯示出貨資訊
                            //檢查是否已出貨
                            if (isIsp) //超取
                            {
                                ShipInfo shipInfo = OrderFacade.GetOrderShipInfo(main);
                                clab_rpOrderStatus.Text = shipInfo.ShipStatusDesc;
                                if (shipInfo.ShipStatus == BizLogic.Model.API.ShippingStatus.ReadyToShip)
                                {
                                    clab_rpOrderStatus.Text += "<br />" + (string.IsNullOrEmpty(shipInfo.LastShippingDate) ? "照會中" : shipInfo.LastShippingDate);
                                }
                            }
                            else if (main.IsWms)  //wms(pchome)
                            {
                                ShipInfo shipInfo = OrderFacade.GetOrderShipInfo(main);
                                clab_rpOrderStatus.Text = shipInfo.ShipStatusDesc;
                            }
                            else
                            {
                                if (PponOrderManager.CheckIsShipped(main.Guid, OrderClassification.LkSite))
                                {
                                    clab_rpOrderStatus.Text = "已出貨";
                                }
                                else
                                {
                                    ShipInfo shipInfo = OrderFacade.GetOrderShipInfo(main);
                                    clab_rpOrderStatus.Text = string.IsNullOrEmpty(shipInfo.LastShippingDate) ? "照會中" : "最後出貨日<br/>" + shipInfo.LastShippingDate;
                                }
                            }

                            //ATM
                            if ((main.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0 && (main.OrderStatus & (int)Core.OrderStatus.Complete) == 0)
                            {
                                if (main.CreateTime.Year == now.Year && main.CreateTime.Month == now.Month && main.CreateTime.Day == now.Day)
                                {
                                    clbt_rp_PayATM.Visible = true;
                                    clab_rpOrderStatus.Visible = false; //ATM未付款無須顯示出貨資訊
                                }
                                else
                                {
                                    clbt_rp_PayATM.Visible = false;
                                    clab_rpOrderStatus.Text = "逾期未付款";
                                }
                            }
                        }
                    }
                }

                #region 退貨條件顯示

                //不接受退貨
                if ((main.Status & (int)GroupOrderStatus.NoRefund) > 0 && !iscancel && !isExchangeOrder && !isReturnOrder)
                {
                    clab_rpOrderStatus.Text = I18N.Phrase.NoRefund;
                }

                //結檔後七天不接受退貨
                if ((main.Status & (int)GroupOrderStatus.DaysNoRefund) > 0 && main.BusinessHourOrderTimeE.AddDays(conf.ProductRefundDays) < DateTime.Now)
                {
                    clab_rpOrderStatus.Text = I18N.Phrase.NoRefund;
                }

                //過期不接受退貨
                if ((main.Status & (int)GroupOrderStatus.ExpireNoRefund) > 0)
                {
                    if (main.BusinessHourDeliverTimeE != null && main.BusinessHourDeliverTimeE.Value.AddDays(1) < now)
                    {
                        if (!iscancel)
                        {
                            clab_rpOrderStatus.Text = I18N.Phrase.NoRefund;
                        }
                    }
                }

                //演出時間前十日過後不能退貨
                if ((main.Status & (int)GroupOrderStatus.NoRefundBeforeDays) > 0 && now.Date >= ((DateTime)main.BusinessHourDeliverTimeS).AddDays(-conf.PponNoRefundBeforeDays).Date)
                {                    
                    if (string.IsNullOrEmpty(clab_rpOrderStatus.Text))
                    {
                        if (OrderFacade.IsExpiredVerifiedDeal(main))
                        {
                            clab_rpOrderStatus.Text = "逾期無法退費";
                        }
                    }
                    if (string.IsNullOrEmpty(clab_rpOrderStatus.Text))
                    {
                        clab_rpOrderStatus.Text = I18N.Phrase.NoRefund;
                    }                        
                }

                #endregion

                //0元好康
                if (isZeroPponDeal)
                {
                    clab_rpUseCount.Text = string.Empty;

                    if ((main.Status & (int)GroupOrderStatus.PEZevent) > 0 || (main.Status & (int)GroupOrderStatus.FamiDeal) > 0)
                    {
                        if (main.PinType == (int)PinType.Single)
                        {
                            //如果只有一組序號則不顯示使用狀態
                            clab_rpUseCount.Text = (IsPastFinalExpireDate(main)) ? "已過期" : "可使用";
                            clbt_CouponDetail.Visible = !(IsPastFinalExpireDate(main));
                        }
                        else if (main.PrintCount == 0)
                        {
                            if (IsPastFinalExpireDate(main))
                            {
                                clab_rpUseCount.Text = "已過期";
                                clbt_CouponDetail.Visible = false;
                            }
                            else
                            {
                                clab_rpUseCount.Text = (main.CouponCodeType == (int)CouponCodeType.Pincode) ? "未使用" : "可使用";
                                clbt_CouponDetail.Visible = true;
                            }
                        }
                        else
                        {
                            clab_rpUseCount.Text = "使用完畢";
                            clbt_CouponDetail.Visible = false;
                        }
                    }
                }

                //公益檔
                if ((main.Status & (int)GroupOrderStatus.KindDeal) > 0)
                {
                    HtmlGenericControl pItemPrice = ((HtmlGenericControl)e.Item.FindControl("pItemPrice"));
                    pItemPrice.Visible = false;
                    clab_rpUseCount.Text = "公益檔次";
                    clab_rpOrderStatus.Text = string.Empty;
                }

                //全家成套禮券 (未使用數量要扣掉處理中)
                if ((main.Status & (int)GroupOrderStatus.FamiDeal) > 0 && Helper.IsFlagSet(main.BusinessHourStatus, Core.BusinessHourStatus.GroupCoupon) &&
                   (main.Status & (int)GroupOrderStatus.PEZevent) > 0)
                {
                    clbt_MCouponEvaluate.Visible = false;
                    clbt_CouponEvaluate.Visible = false;

                    if (clab_rpUseCount.Text.Contains("未使用") && main.RemainCount > 0)
                    {
                        int useCount;

                        //全家被鎖定憑證
                        var famiCoupons = fp.GetFamiCouponsByLocked(main.Guid);
                        //對方已收禮
                        var acceptedGifts = gifts.Where(g => g.Accept == (int)MgmAcceptGiftType.Accept || g.Accept==(int)MgmAcceptGiftType.Quick).ToList();
                        //對方已收禮並Lock
                        var acceptedGiftLockCount = famiCoupons.Join(acceptedGifts, f => f.CouponId, g => g.CouponId, (f, g) => new { CouponId = f.CouponId }).Count();
                        //自己lock數量
                        var selfLockCount = famiCoupons.Count - acceptedGiftLockCount;

                        //未使用 = 剩餘憑證總數 - 自己Lock總數 - 送禮總數
                        useCount = (int) main.RemainCount - selfLockCount - giftCount;
                        clab_rpUseCount.Text = string.Format("未使用:{0}", useCount);
                    }
                }


                //萊爾富成套禮券 
                if (conf.EnableHiLifeDealSetup && (main.Status & (int) GroupOrderStatus.HiLifeDeal) > 0 &&
                    Helper.IsFlagSet(main.BusinessHourStatus, Core.BusinessHourStatus.GroupCoupon) &&
                    (main.Status & (int) GroupOrderStatus.PEZevent) > 0)
                {
                    clbt_MCouponEvaluate.Visible = false;
                    clbt_CouponEvaluate.Visible = false;
                }
            }
        }

        protected void rp_CouponListItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            Guid orderGuid = Guid.Empty;

            switch (e.CommandName)
            {
                //push the orderid show the detail
                case "ShowDetail":
                    string orderId = e.CommandArgument.ToString();
                    RedirectOrderDetail(orderId);
                    break;
                case "ServiceAsk":
                    if (ServiceAsk != null)
                    {
                        Guid.TryParse(e.CommandArgument.ToString(), out orderGuid);
                        this.ServiceAsk(this, new DataEventArgs<Guid>(orderGuid));
                    }
                    RedirectService();
                    break;
                case "ServiceOnlineAsk":
                    Guid.TryParse(e.CommandArgument.ToString(), out orderGuid);
                    NewRedirectService(orderGuid);
                    break;
                case "GotoBookingSystem":
                    break;
            }
        }

        protected string GetFinalExpireDateContent(ViewCouponListMain view)
        {
            if (view == null)
            {
                return string.Empty;
            }

            if (view.Department > 3)
            {
                return string.Empty;
            }

            if (!Helper.IsFlagSet(view.BusinessHourStatus, Core.BusinessHourStatus.FinalExpireTimeChange))
            {
                return string.Empty;
            }

            string expireDateContent = GetFinalExpireDate(view.Guid);
            if (expireDateContent.Length == 0)
            {
                return string.Empty;
            }
            else
            {
                return string.Format("<br/><span class='OrdertablegreenClosed'>{0}</span>", expireDateContent);
            }
        }
        protected string GetExpireDate(ViewCouponListMain view)
        {
            if (view == null)
            {
                return string.Empty;
            }

            if (view.Department > 3)
            {
                return string.Empty;
            }

            //全家代金
            if ((view.Status & (int)GroupOrderStatus.FamiDeal) > 0 &&
                Helper.IsFlagSet(view.BusinessHourStatus, LunchKingSite.Core.BusinessHourStatus.GroupCoupon) &&
                (view.Status & (int)GroupOrderStatus.PEZevent) > 0)
            {
                return view.BusinessHourDeliverTimeE == null ? string.Empty : view.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd HH:mm");
            }
            else
            {
                #region 原始View判斷
                //<%# (((ViewCouponListMain)(Container.DataItem)).DeliveryType.HasValue && int.Equals((int)DeliveryType.ToHouse, ((ViewCouponListMain)(Container.DataItem)).DeliveryType.Value)) ? "-" : (((ViewCouponListMain)(Container.DataItem)).BusinessHourDeliverTimeE == null ? string.Empty : ((((ViewCouponListMain)(Container.DataItem)).ChangedExpireDate.HasValue )?((ViewCouponListMain)(Container.DataItem)).ChangedExpireDate.Value.ToString("yyyy/MM/dd"):((ViewCouponListMain)(Container.DataItem)).BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd")))%>   
                //<%# (view.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToHouse, view.DeliveryType.Value)) 
                //    ? "-"
                //    : (view.BusinessHourDeliverTimeE == null
                //        ? string.Empty
                //        : ((view.ChangedExpireDate.HasValue)
                //            ? view.ChangedExpireDate.Value.ToString("yyyy/MM/dd")
                //            : view.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd"))) %>
                #endregion
                if ((view.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToHouse, view.DeliveryType.Value)))
                {
                    return "-";
                }
                else if (view.BusinessHourDeliverTimeE == null)
                {
                    return string.Empty;
                }
                else
                {
                    return view.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd");
                }
            }
        }

        #endregion  Page (inline method or event)

        #region private method
        private string GetZeroDealTagContent(CouponCodeType CodeType)
        {
            string tagContent = string.Empty;
            if (conf.EnableFami3Barcode)
            {
                tagContent = @"<div class='tag tag-fami-barcode'>優惠條碼</div>
                            <span class='tag-note'>請直接於櫃檯出示結帳</sapn>";
            }
            else
            {
                switch (CodeType)
                {
                    case CouponCodeType.Pincode:
                        tagContent = @"<div class='tag tag-fami-pincode'>紅利Pin碼</div>
                            <span class='tag-note'>請於FamiPort列印條碼</sapn>";
                        break;
                    case CouponCodeType.Barcode128:
                    case CouponCodeType.BarcodeEAN13:
                        tagContent = @"<div class='tag tag-fami-barcode'>優惠條碼</div>
                            <span class='tag-note'>請直接於櫃檯出示結帳</sapn>";
                        break;
                    default:
                        break;
                }
            }

            return tagContent;
        }

        private void RedirectService()
        {
            string url = ResolveUrl("~/User/Service.aspx");
            if (AccBusinessGroupId.Equals(5))
            {
                url = ResolveUrl("~/User/TravelService.aspx");
            }
            Response.Redirect(string.Format("{0}?orderid={1}", url, OrderId));
        }

        /// <summary>
        /// 新版客服使用
        /// </summary>
        private void NewRedirectService(Guid orderGuid)
        {
            string url = ResolveUrl("~/User/ServiceConversation"); 
            Response.Redirect(string.Format("{0}?issueType=2&no={1}", url, orderGuid));
        }

        private void RedirectOrderDetail(string orderId)
        {
            string preUrl = ResolveUrl("~/User/CouponDetail.aspx");
            Response.Redirect(string.Format("{0}?oid={1}", preUrl, orderId));
        }

        private string GetFinalExpireDate(Guid orderGuid)
        {
            if (orderGuid == Guid.Empty)
            {
                return string.Empty;
            }

            ExpirationDateSelector selector = Presenter.GetExpirationComponent(orderGuid);

            DateTime expireDate = selector.GetExpirationDate();
            if (expireDate != selector.ExpirationDates.DealOriginalExpireDate)  //selector.ExpirationDates.DealOriginalExpireDate
            {
                if (DateTime.Equals(expireDate, selector.ExpirationDates.SellerClosedDownDate ?? DateTime.MaxValue) ||
                    DateTime.Equals(expireDate, selector.ExpirationDates.StoreClosedDownDate ?? DateTime.MaxValue))  //若結束營業日&截止日期調整兩者相同, 結束營業優先
                {
                    return string.Format("(至{0}停止營業)", expireDate.ToString("yyyy/MM/dd"));
                }
                return string.Format("(至{0}憑證停止使用)", expireDate.ToString("yyyy/MM/dd"));
            }

            return string.Empty;
        }

        /// <summary>
        /// 判斷憑證是否過期. 過期日會考慮到店家倒店以及變更使用期限的情況.
        /// </summary>
        /// <param name="view"></param>
        /// <returns></returns>
        private bool IsPastFinalExpireDate(ViewCouponListMain view)
        {
            if (view == null)
            {
                return false;
            }

            if (view.Department > 3)
            {
                return false;
            }

            ExpirationDateSelector selector = Presenter.GetExpirationComponent(view.Guid);

            if (selector == null)
            {
                return false;
            }

            return selector.IsPastExpirationDate(DateTime.Now);
        }

        private DateTime? GetExpireDate(Guid orderGuid)
        {
            ExpirationDateSelector selector = Presenter.GetExpirationComponent(orderGuid);

            if (selector == null)
            {
                return null;
            }

            return selector.GetExpirationDate();
        }

        /// <summary>
        /// 憑證使用時間及提前天數預約檢查，是否顯示按鈕
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="bookType"></param>
        /// <returns></returns>
        private bool CheckIsBookable(Guid orderGuid, BookingType bookType)
        {
            var expireDate = GetExpireDate(orderGuid);
            if (!expireDate.HasValue)
            {
                return false;
            }

            var advanceReservationDays = _presenter.GetAdvanceReservationDays(orderGuid, bookType);
            DateTime lastReservationDate = expireDate.Value.AddDays(-advanceReservationDays);

            return DateTime.Today <= lastReservationDate.Date;
        }

        private void EnableBookingButton(Button booking, Button bookingMini, Guid orderGuid, BookingType bookType)
        {
            if (!booking.Visible)
            {
                return;
            }

            bookingMini.Text = booking.Text = (bookType == BookingType.Travel) ? "訂房" : "訂位";

            if (BookingSystemFacade.CheckCouponsStatus(orderGuid.ToString(), bookType))
            {
                bookingMini.OnClientClick = booking.OnClientClick = "openBookingSystem('" + orderGuid + "', '" + Helper.Encrypt("17Life") + "');return false;";

                #region 憑證使用時間及提前天數預約檢查，是否顯示按鈕

                bookingMini.Visible = booking.Visible = CheckIsBookable(orderGuid, bookType);

                #endregion 憑證使用時間及提前天數預約檢查，是否顯示按鈕
            }
            else
            {
                SetBookingToQueryBookingButton(booking, bookingMini);
            }
        }

        private void SetBookingToQueryBookingButton(Button booking, Button bookingMini)
        {
            if (!booking.Visible)
            {
                return;
            }

            bookingMini.Text = booking.Text = "查詢" + booking.Text;
            bookingMini.OnClientClick = booking.OnClientClick = "openBookingSystem('', '" + Helper.Encrypt("17Life") + "');return false;";
        }

        #endregion

    }
}
﻿using System;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.User
{
    public partial class ReturnApplicationForm : LocalizedBasePage,IReturnApplicationView
    {
        #region property
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private ReturnApplicationPresenter _presenter;
        public ReturnApplicationPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public string UserName
        {
            get { return string.IsNullOrEmpty(Request.QueryString["u"]) ? Page.User.Identity.Name : Request.QueryString["u"]; }
        }

        public int UserId
        {
            get { return MemberFacade.GetUniqueId(UserName); }
        }

        public Guid OrderGuid
        {
            get
            {
                Guid guid;
                return Guid.TryParse(Request.QueryString["oid"], out guid) ? guid : Guid.Empty;
            }
        }
        public DateTime ReturnTime
        {
            get
            {
                DateTime rt;
                return (DateTime.TryParse(lbReturnYear.Text + "-" + lbReturnMonth.Text + "-" + lbReturnDay.Text, out rt)) ? rt : DateTime.Now;
            }
            set 
            {
                lbReturnYear.Text = value.Year.ToString();
                lbReturnMonth.Text = value.Month.ToString();
                lbReturnDay.Text = value.Day.ToString();
            }
        }
        public string CouponList
        {
            get { return lbCouponList.Text; }
            set { lbCouponList.Text = value; }
        }
        public string DealNumber
        {
            get { return lbDealNumber.Text; }
            set { lbDealNumber.Text = value; }
        }
        public string DealName
        {
            set
            {
                lbDealName.Text = value;
            }
        }
        public string OrderId
        {
            get { return lbOrderId.Text; }
            set 
            { 
                if(!string.IsNullOrEmpty(value))
                {
                    lbOrderId.Text = value;
                    imgBarcode.ImageUrl = ResolveUrl("~/service/barcode39.ashx") + "?id=" + value;
                }
            }
        }
        public string message
        {
            set { lab_NoData.Text = value; }
        }
        public bool isleagl
        {
            set
            {
                if (value)
                {
                    pan_Data.Visible = true;
                    pan_NoData.Visible = false;
                }
                else
                {
                    pan_Data.Visible = false;
                    pan_NoData.Visible = true;
                }
            }
        }
        public bool IsAtm
        {
            set
            {
                atmInfoContainer.Visible = value;
            }
        }
        #endregion
        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                if (!_presenter.OnViewInitialized())
                {
                    Response.Redirect("/ppon/default.aspx");
                    return;
                }
            _presenter.OnViewLoaded();
        }
        #endregion
        #region method
        public void SetAtm(CtAtmRefund atm)
        {
            if (atm.Si != 0)
            {
                lab_ATM_AccountName.Text = atm.AccountName;
                lab_ATM_UserID.Text = atm.Id;
                lab_ATM_BankName.Text = atm.BankName;
                lab_ATM_BranchName.Text = atm.BranchName;
                lab_ATM_Account.Text = atm.AccountNumber;
                lab_ATM_Phone.Text = atm.Phone;
            }
        }

        #endregion
    }
}
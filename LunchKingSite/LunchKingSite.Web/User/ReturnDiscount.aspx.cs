﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;

namespace LunchKingSite.Web.User
{
    public partial class ReturnDiscount : LocalizedBasePage, IReturnDiscountView
    {
        #region property

        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        private ReturnDiscountPresenter _presenter;

        public ReturnDiscountPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get
            {
                return string.IsNullOrEmpty(Request.QueryString["u"]) ? Page.User.Identity.Name : Request.QueryString["u"];
            }
        }

        public int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(UserName);
            }
        }

        public Guid OrderGuid
        {
            get
            {
                Guid guid;
                return Guid.TryParse(Request.QueryString["oid"], out guid) ? guid : Guid.Empty;
            }
        }

        public DateTime Now
        {
            get
            {
                return DateTime.Now;
            }
        }

        public string message
        {
            set
            {
                lab_NoData.Text = value;
            }
        }

        public bool isleagl
        {
            set
            {
                if (value)
                {
                    pan_Data.Visible = true;
                    pan_NoData.Visible = false;
                }
                else
                {
                    pan_Data.Visible = false;
                    pan_NoData.Visible = true;
                }
            }
        }

        /// <summary>
        /// 是否需要範本:若退貨狀態是已完成、立即刷退型 則不需要
        /// </summary>
        public bool IsNeedTempReturnDiscount { get; set; }
        /// <summary>
        /// 是否使用新版折讓單
        /// </summary>
        public bool IsNewAllowance { get; set; }
        #endregion property

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!_presenter.OnViewInitialized())
                {
                    Response.Redirect("/ppon/default.aspx");
                    return;
                }
            }
            _presenter.OnViewLoaded();
        }

        #endregion page

        #region method

        public void SetRefundFormInfo(EinvoiceMain einvoice, CashTrustLogCollection cs, OldCashTrustLogCollection ocs, CtAtmRefund atmrefund, DealProperty dp, Order o, bool isleagl, string message, int returnFormStatus)
        {
            ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
            pan_Data.Visible = pan_NoData.Visible = false;
            if (isleagl)
            {
                pan_Data.Visible = true;
                List<RefundDiscountListClass> rfs = new List<RefundDiscountListClass>();
                List<RefundDiscountListClass> rfs_temp = new List<RefundDiscountListClass>();
                int i = 0;
                int mode = einvoice.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? 3 : 2;
                decimal tax = einvoice.InvoiceTax;
                bool istax = einvoice.InvoiceTax == 0.05m;
                IsNewAllowance = einvoice.OrderTime >= cp.EinvAllowanceStartDate;

                if (cs.Count > 0)
                {
                    foreach (var item in cs)
                    {
                        //原本這邊有判斷, 如果全部憑證已核銷或是已刷退, 則不顯示折讓單. 但因為客服常常遇到強制處理, 或是需要補件的狀況, 暫時取消此判斷

                        //修改rfs 建構參數, 原本rfs的ItemAmount = trust.CreditCard + trust.Atm, 現改為依照訂單決定, 如果訂單建立日期 < 核銷後開發票起始日, 則維持原金額; 否則的話改為 trust.uninvoiced_amount
                        int totalAmount = o.CreateTime < Convert.ToDateTime(cp.NewInvoiceDate) ? item.CreditCard + item.Atm : item.UninvoicedAmount;
                        decimal salesamount = (Math.Round((totalAmount / (1 + tax)), 0));
                        rfs.Add(new RefundDiscountListClass(i, dp.UniqueId.ToString(), einvoice.OrderId, string.IsNullOrEmpty(item.CouponSequenceNumber) ? string.Empty : ("憑證編號#" + item.CouponSequenceNumber),
                            mode, einvoice.InvoiceNumberTime == null ? einvoice.OrderTime : einvoice.InvoiceNumberTime.Value, einvoice.InvoiceNumberTime == null,
                            string.IsNullOrEmpty(einvoice.InvoiceNumber) ? string.Empty.PadLeft(10, ' ') : einvoice.InvoiceNumber,
                            Helper.IsFlagSet(item.SpecialStatus, TrustSpecialStatus.Freight) ? "運費" : ((!einvoice.OrderIsPponitem && einvoice.InvoiceTax != 0) ? "購物金" : NoComma(einvoice.OrderItem)), totalAmount, (int)salesamount, totalAmount - (int)salesamount, istax, einvoice.OrderIsPponitem));
                        rfs[i].breakstring = "<div style='page-break-before:always;' />";
                        i++;
                    }
                }
                else if (ocs.Count > 0)
                {
                    int price = (int)einvoice.OrderAmount / ocs.Count;
                    foreach (var item in ocs)
                    {
                        if (item.Status < ((int)TrustStatus.Verified))
                        {
                            decimal salesamount = (Math.Round(((price) / (1 + tax)), 0));
                            rfs.Add(new RefundDiscountListClass(i, dp.UniqueId.ToString(), einvoice.OrderId, string.IsNullOrEmpty(item.SequenceNumber) ? string.Empty : ("憑證編號#" + item.SequenceNumber),
                                mode, einvoice.InvoiceNumberTime == null ? einvoice.OrderTime : einvoice.InvoiceNumberTime.Value, einvoice.InvoiceNumberTime == null,
                                string.IsNullOrEmpty(einvoice.InvoiceNumber) ? string.Empty.PadLeft(10, ' ') : einvoice.InvoiceNumber,
                               ((!einvoice.OrderIsPponitem && einvoice.InvoiceTax != 0) ? "購物金" : NoComma(einvoice.OrderItem)), price, (int)salesamount, (price) - (int)salesamount, istax, einvoice.OrderIsPponitem));
                            rfs[i].breakstring = "<div style='page-break-before:always;' />";
                            i++;
                        }
                    }
                }

                if (rfs.Count > 0)
                {
                    rfs.Last().breakstring = string.Empty;
                }

                rpt_Form.DataSource = rfs;
                rpt_Form.DataBind();
                //範本
                rfs_temp.Add(rfs.FirstOrDefault());
                rpt_FormTemp.DataSource = rfs_temp;
                rpt_FormTemp.DataBind();
                IsNeedTempReturnDiscount = (returnFormStatus == (int)ProgressStatus.Completed ||
                                            returnFormStatus == (int)ProgressStatus.CompletedWithCreditCardQueued ? false : true);

            }
            else
            {
                lab_NoData.Text = message;
                pan_NoData.Visible = true;
            }
        }

        public void SetRefundFormInfoForMultiInvoices(List<EinvoiceMain> einvoices, CashTrustLogCollection cs, CtAtmRefund atmrefund, DealProperty dp, Order o, bool isleagl, string message, int returnFormStatus)
        {
            ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
            pan_Data.Visible = pan_NoData.Visible = false;
            if (isleagl)
            {
                pan_Data.Visible = true;
                List<RefundDiscountListClass> rfs = new List<RefundDiscountListClass>();
                List<RefundDiscountListClass> rfs_temp = new List<RefundDiscountListClass>();
                int i = 0;
                if (cs.Count > 0)
                {
                    foreach (var item in cs.Where(x => x.UninvoicedAmount > 0))
                    {
                        EinvoiceMain einvoice = (einvoices.Where(x => x.CouponId == item.CouponId).Count() > 0)
                                                    ? einvoices.Where(x => x.CouponId == item.CouponId).First()
                                                    : null;
                        einvoice = dp.SaleMultipleBase > 0 ? einvoices.Where(x => x.OrderGuid == item.OrderGuid).First() : einvoice;
                        IsNewAllowance = (!IsNewAllowance ? einvoice.OrderTime >= cp.EinvAllowanceStartDate : IsNewAllowance);

                        if (einvoice != null)
                        {
                            int mode = einvoice.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? 3 : 2;
                            decimal tax = einvoice.InvoiceTax;
                            bool istax = einvoice.InvoiceTax == 0.05m;

                            //原本這邊有判斷, 如果全部憑證已核銷或是已刷退, 則不顯示折讓單. 但因為客服常常遇到強制處理, 或是需要補件的狀況, 暫時取消此判斷

                            //修改rfs 建構參數, 原本rfs的ItemAmount = trust.CreditCard + trust.Atm, 現改為依照訂單決定. 如果訂單建立日期 < 核銷後開發票起始日, 則維持原金額; 否則的話改為 trust.uninvoiced_amount
                            int totalAmount = o.CreateTime < Convert.ToDateTime(cp.NewInvoiceDate) ? item.CreditCard + item.Atm : item.UninvoicedAmount;
                            decimal salesamount = (Math.Round((totalAmount / (1 + tax)), 0));
                            rfs.Add(new RefundDiscountListClass(i, dp.UniqueId.ToString(), einvoice.OrderId, string.IsNullOrEmpty(item.CouponSequenceNumber) ? string.Empty : ("憑證編號#" + item.CouponSequenceNumber),
                                mode, einvoice.InvoiceNumberTime == null ? einvoice.OrderTime : einvoice.InvoiceNumberTime.Value, einvoice.InvoiceNumberTime == null,
                                string.IsNullOrEmpty(einvoice.InvoiceNumber) ? string.Empty.PadLeft(10, ' ') : einvoice.InvoiceNumber,
                                Helper.IsFlagSet(item.SpecialStatus, TrustSpecialStatus.Freight) ? "運費" : ((!einvoice.OrderIsPponitem && einvoice.InvoiceTax != 0) ? "購物金" : NoComma(einvoice.OrderItem)), totalAmount, (int)salesamount, totalAmount - (int)salesamount, istax, einvoice.OrderIsPponitem));
                            rfs[i].breakstring = "<div style='page-break-before:always;' />";
                            i++;
                        }
                    }
                }

                if (rfs.Count > 0)
                {
                    rfs.Last().breakstring = string.Empty;
                }

                rpt_Form.DataSource = rfs;
                rpt_Form.DataBind();
                //範本
                rfs_temp.Add(rfs.FirstOrDefault());
                rpt_FormTemp.DataSource = rfs_temp;
                rpt_FormTemp.DataBind();
                IsNeedTempReturnDiscount = (returnFormStatus == (int)ProgressStatus.Completed ||
                            returnFormStatus == (int)ProgressStatus.CompletedWithCreditCardQueued ? false : true);

            }
            else
            {
                lab_NoData.Text = message;
                pan_NoData.Visible = true;
            }
        }

        public void SetEntrustSellRefundFormInfo(List<EntrustSellReceipt> receipts, Order o, CashTrustLogCollection cs, DealProperty dp, bool isleagl, string message, int returnFormStatus)
        {
            pan_Data.Visible = pan_NoData.Visible = false;
            if (isleagl)
            {
                pan_Data.Visible = true;
                List<RefundDiscountListClass> rfs = new List<RefundDiscountListClass>();
                List<RefundDiscountListClass> rfs_temp = new List<RefundDiscountListClass>();
                //沒有發票，所以預設帶不用紙本折讓
                IsNewAllowance = false;
                int i = 0;
                if (cs.Count > 0)
                {
                    foreach (var item in cs)
                    {
                        EntrustSellReceipt receipt = receipts.Single(t => t.TrustId == item.TrustId);
                        int mode = receipt.ForBusiness ? 3 : 2;
                        if ((receipt.CouponId > 0 && item.Status < ((int)TrustStatus.Refunded)) ||
                              (receipt.CouponId == 0 && item.Status != ((int)TrustStatus.Verified) && item.Status != ((int)TrustStatus.Refunded)))
                        {
                            rfs.Add(new RefundDiscountListClass(i, dp.UniqueId.ToString(),
                                o.OrderId, string.IsNullOrEmpty(item.CouponSequenceNumber) ? string.Empty : ("憑證編號#" + item.CouponSequenceNumber),
                                mode, receipt.ReceiptCodeTime == null ? o.CreateTime : receipt.ReceiptCodeTime.Value, receipt.ReceiptCodeTime == null,
                                receipt.ReceiptCode == null ? string.Empty.PadLeft(10, ' ') : " S" + receipt.ReceiptCode,
                                Helper.IsFlagSet(item.SpecialStatus, TrustSpecialStatus.Freight) ? "運費" : NoComma(receipt.ItemName),
                                (int)receipt.Amount,
                                (int)receipt.Amount,
                                0,
                                false,
                                receipt.CouponId == 0));
                            rfs[i].breakstring = "<div style='page-break-before:always;' />";
                            i++;
                        }
                    }
                }

                if (rfs.Count > 0)
                {
                    rfs.Last().breakstring = string.Empty;
                }

                rpt_Form.DataSource = rfs;
                rpt_Form.DataBind(); 
                //範本
                rfs_temp.Add(rfs.FirstOrDefault());
                rpt_FormTemp.DataSource = rfs_temp;
                rpt_FormTemp.DataBind();
                IsNeedTempReturnDiscount = (returnFormStatus == (int)ProgressStatus.Completed ||
                            returnFormStatus == (int)ProgressStatus.CompletedWithCreditCardQueued ? false : true);
            }
            else
            {
                lab_NoData.Text = message;
                pan_NoData.Visible = true;
            }
        }

        public static string NoComma(string input, string comma = ",")
        {
            return string.IsNullOrEmpty(input) ? string.Empty : input.Replace(comma, string.Empty);
        }

        #endregion method
    }
}
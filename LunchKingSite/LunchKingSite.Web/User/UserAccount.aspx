﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="UserAccount.aspx.cs" Inherits="LunchKingSite.Web.User.UserAccount" %>

<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="uc1" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.WebLib" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Register Src="../ControlRoom/Controls/AddressInputBox.ascx" TagName="AddressInputBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content4" ContentPlaceHolderID="MC" runat="server">
    <script type="text/javascript" src="../Tools/js/jquery.cascadeAddress.js?ver=0.0.0.2"></script>
    <link href="../Themes/default/images/17Life/G2/PPB.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Tools/js/jquery.blockUI.js"></script>
    <style>
        .trred {
            background-color: #FFDFDF;
        }
        .input-credit {
            width:40px !important;
            text-align:center !important;
        }
        <%if (MGMFacade.IsEnabled) {%>
        .mc-navbtn {
            width:13% !important;
        }
        <%}%>
    </style>
    <asp:PlaceHolder ID="phMemberLinkBindScript" runat="server">
        <script type="text/javascript">
            window.RegisterMemberLinkBindFuncs = function () {
				$('#btnBindFacebook, #btnUnbindFacebook,#btnUnbindPayeasy,#btnBindLife17,#btnChangeMobile,#btnUnbindLine,#btnBindLine, .navi-btn')
                    .click(function () {
						$('#btnBindFacebook,#btnUnbindFacebook,#btnUnbindPayeasy,#btnBindLife17,#btnChangeMobile, .navi-btn, #btnUnbindLine, #btnBindLine')
							.attr('disabled', true);
                    });

                $('#btnBindFacebook').click(function () {
                    window.location.href = "https://www.facebook.com/dialog/oauth?client_id=<%=conf.FacebookApplicationId%>&redirect_uri=<%=Helper.CombineUrl(conf.SiteUrl, "newmember/fbbind.ashx")%>&scope=<%=FacebookUser.DefaultScope%>";
                });
                $('#btnUnbindFacebook').click(function () {
                    window.location.href = "../newmember/fbunbind.ashx";
				});
				$('#btnBindLine').click(function () {
					window.location.href = "https://access.line.me/oauth2/<%=LineUtility._LINE_API_VER%>authorize?response_type=code&client_id=<%=conf.LineApplicationId%>&redirect_uri=<%=Helper.CombineUrl(conf.SiteUrl, "newmember/linebind.ashx")%>&scope=<%=LineUser.DefaultScope%>&state=0203";
				});
				$('#btnUnbindLine').click(function () {
					window.location.href = "../newmember/lineunbind.ashx";
				});
                $('#btnUnbindPayeasy').click(function () {
                    window.location.href = "../newmember/pezunbind.ashx";
                });
                $('#btnBindLife17').click(function () {
                    window.location.href = "../newmember/life17bind.aspx";
                });
                $('#btnBindMobile, #btnBindMobile2').click(function () {
                    window.location.href = "../newmember/mobileAuth.aspx";
                });
                $('#btnChangeMobile').click(function () {
                    window.location.href = "../newmember/mobileAuth.aspx?g=0";
                });
            }
        </script>
    </asp:PlaceHolder>
    <script type="text/javascript">

        $(document).ready(function () {

            if ($('#hidInvoiceCity').val() != '' && $('#hidInvoiceArea').val() != '') {
                $.cascadeAddress.setup('<%=WebUtility.GetSiteRoot()%>', function() {
                    $.cascadeAddress.bind($('#ddlInvoiceCity'), $('#ddlInvoiceArea'), $('#InvoiceAddress'), $('#hidInvoiceCity').val(), $('#hidInvoiceArea').val(), $('#InvoiceAddress').val(), window.deliveryToIsland);
                });
            }
            else {
                $.cascadeAddress.setup('<%=WebUtility.GetSiteRoot()%>', function() {
                    $.cascadeAddress.bind($('#ddlInvoiceCity'), $('#ddlInvoiceArea'), $('#InvoiceAddress'), -1, -1, $('#InvoiceAddress').val(), window.deliveryToIsland);
                });
            }

            var array = JSON.parse('<%=CityListStr%>');
            var addr = $("#<%=wInvoAddr.ClientID %>").val();
            var city = "";
            var area = "";

            if (addr != "") {
                city = addr.slice(0, 3);
                area = addr.slice(3, 6);
                $("#<%=wInvoAddr.ClientID %>").val(addr.slice(6));
            }

            $.each(array, function(i, val) {
                if (array[i] == city) {
                    $("#wInvoAddrCity").append("<option value=\"" + i +"\" selected>" + array[i] + "</option>");

                    $.ajax({
                        type: "POST",
                        url: "/service/ppon/GetAreaByCity",
                        data: "{cid:'" + i + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (result) {
                            if (result.Code==1000) {
                                var array = JSON.parse(result.Data);
                                $("#wInvoAddrArea").html("");
                                $("#wInvoAddrArea").append("<option value=\"" + -1 +"\">" + "請選擇" + "</option>");
                                $.each(array,
                                    function(i, val) {
                                        if (array[i] == area) {
                                            $("#wInvoAddrArea").append("<option value=\"" + i +"\" selected>" + array[i] + "</option>");
                                        } else {
                                            $("#wInvoAddrArea").append("<option value=\"" + i +"\">" + array[i] + "</option>");
                                        }
                                        
                                    }); 
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert("網路連線異常");
                        }
                    }); 

                } else {
                    $("#wInvoAddrCity").append("<option value=\"" + i +"\" >" + array[i] + "</option>");
                }
                
            });

            $("#wInvoAddrCity").change(function(){
                var id= $(this).val();
                $.ajax({
                    type: "POST",
                    url: "/service/ppon/GetAreaByCity",
                    data: "{cid:'" + id + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result.Code==1000) {
                            var array = JSON.parse(result.Data);
                            $("#wInvoAddrArea").html("");
                            $("#wInvoAddrArea").append("<option value=\"" + -1 +"\">" + "請選擇" + "</option>");
                            $.each(array,
                                function(i, val) {
                                    $("#wInvoAddrArea").append("<option value=\"" + i +"\">" + array[i] + "</option>");
                                }); 
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("網路連線異常");
                    }
                });  
            });
            $("#wInvoAddrArea").change(function(){
                var areaName= $(this).find("option:selected").text();
                var cityName= $("#wInvoAddrCity").find("option:selected").text();
                $("#<%=hiddenAddrCity.ClientID %>").val(cityName + areaName);
            });

            $('#naviMemberLink').removeClass().addClass('navbtn_inpage fr');
            // Stop chrome's autocomplete from making your input fields that nasty yellow.
            if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
                $(window).load(function () {
                    $('input:-webkit-autofill').each(function () {
                        var text = $(this).val();
                        var name = $(this).attr('name');
                        $(this).after(this.outerHTML).remove();
                        $('input[name=' + name + ']').val(text);
                    });
                });
            }

            $('#btnChange').click(function () {
                $('#emailShow').hide();
                $('#emailEdit').show();
                $('#txtNewEmail').val($('#lab_Email').text()).select();
            });

            $('#btnEmailCancel').click(function () {
                $('#emailShow').show();
                $('#emailEdit').hide();
            });
            $('html,body').scrollTop(parseInt($('[id*=hidScroll]').val(), 10));

            if (window.RegisterMemberLinkBindFuncs != undefined) {
                window.RegisterMemberLinkBindFuncs();
            }

            $('#addPponSelect').click(function () {
                var email = $('#lab_Email').html();
                var categoryId = $('#pponSelectCategoryId').val();
                Ppon.module.SubscriptionEDM(email, categoryId, "userPponSelect", false);
                setSubscribePponSelect();
                $("#btnUpdateSubscription").click();
            });

            $('#withNotPponSelect').click(function () {
                $("#userPponSelect").hide();
                $("#btnUpdateSubscription").click();
            });
            
            //---個人發票載具/資料管理相關-----
            //API驗證電子發票手機條碼
            $('#txtPhoneCarrier').focusout(function (e) {

                if ($('#txtPhoneCarrier').val().length == 8) {
                    $.blockUI({ message: $('.divMessage') });
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "../WebService/EinvoiceService.asmx/CheckCarrid",
                        data: '{carrid: "' + $('#txtPhoneCarrier').val() + '" }',
                        dataType: "json",
                        success: function (data) {
                            if (data.d == "Y" || data.d == "P") {
                                $('#phoneCarrierInvalid').hide();
                                $('#hidCarrIdVerify').val('true');
                            }
                            else {
                                $('#phoneCarrierInvalid').show();
                                $('#hidCarrIdVerify').val('false');
                                $('#divPhoneCarrier').addClass("error");
                            }
                            $.unblockUI();
                        },
                        error: function (data) {
                            $('#phoneCarrierInvalid').hide();
                            $('#hidCarrIdVerify').val('true');
                            $.unblockUI();
                        }
                    });
                }
                else {
                    $('#phoneCarrierInvalid').hide();
                }
            });

            $("#txtPhoneCarrier").click(function () {
                $("#divPhoneCarrier").removeClass("error");
                $("#phoneCarrierErr").hide();
                $('#phoneCarrierInvalid').hide();
            });
            $("#txtPersonalCertificateCarrier").click(function () {
                $("#divPersonalCertificateCarrier").removeClass("error");
                $('#personalCertificateCarrierErr').hide();
                $('#personalCertificateCarrierInvalid').hide();
            });
            $("#EInvoiceBuyerName").click(function () {
                $('#EinvoNameErr').hide();
                $("#tdEInvoName").removeClass("error");
            });
            $("#EInvoiceAddress").click(function () {
                $('#EinvoAddrErr').hide();
                $("#tdEInvoAddr").removeClass("error");
            });
            $("#txtPhoneCarrier, #txtPersonalCertificateCarrier").bind("keyup", function () {
                if (window.currKeyisLowerCase) {
                    $(this).val($(this).val().toUpperCase());
                }
            });
            $("#txtPhoneCarrier, #txtPersonalCertificateCarrier").keydown(function (e) {
                if (e.which >= 65 && e.which <= 90) {
                    window.currKeyisLowerCase = true;
                } else {
                    window.currKeyisLowerCase = false;
                }
            });
            ChangeCarrierType();           

            $('#ckbGetMember').click(function () {
                if ($(this).attr('checked') == 'checked') {
                    var lastname = $.trim($("#<%=tbLastName.ClientID %>").val());
                    var firstname = $.trim($("#<%=tbFirstName.ClientID %>").val());
                    var mobile = $.trim($("#<%=tbMobile.ClientID %>").val());
                    var addresstext = $.trim($('#addressin input:first ').val());
                    $('#wInvoiceName').val(lastname + firstname);
                    $('#wPhoneNumber').val(mobile);
                    $('#wInvoAddrCity').val($('#ddlcity_m').val());
                    var id = $('#ddlcity_m').val();
                    $.ajax({
                        type: "POST",
                        url: "/service/ppon/GetAreaByCity",
                        data: "{cid:'" + id + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (result) {
                            if (result.Code == 1000) {
                                var array = JSON.parse(result.Data);
                                $("#wInvoAddrArea").html("");
                                $("#wInvoAddrArea").append("<option value=\"" + -1 + "\">" + "請選擇" + "</option>");
                                $.each(array,
                                    function (i, val) {
                                        $("#wInvoAddrArea").append("<option value=\"" + i + "\">" + array[i] + "</option>");
                                    });
                                $('#wInvoAddrArea').val($('#ddlarea_m').val());
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert("網路連線異常");
                        }
                    });
                    $('#wInvoAddr').val(addresstext);
                }
            })
            
            //---個人發票載具/資料管理相關--End---

            $(".input-credit").bind("keyup",function()
            {
                var tb = this;
                var maxLength = $(tb).attr('maxlength');
                if ($(tb).val().length == maxLength) {
                    GetCreditCardName();
                }
            });

            function GetCreditCardName()
            {
                var CardNo1=$(".input-credit").eq(0);
                var CardNo2=$(".input-credit").eq(1);
                var CardNo3=$(".input-credit").eq(2);
                var CardNo4=$(".input-credit").eq(3);

                if(CardNo1.val().length == 4 && CardNo2.val().length == 4 && CardNo3.val().length == 4 && CardNo4.val().length == 4)
                {
                    var CardNo=CardNo1.val()+CardNo2.val()+CardNo3.val()+CardNo4.val();
                
                    $.ajax({
                        type: "POST",
                        url: window.ServerLocation + "service/ppon/GetCreditCardName",
                        data: "{cardno:'" + CardNo + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if(data.Code==window.ApiSuccessCode)
                            {
                                $("#txtCreditCardName").val(data.Data.BankName);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert("(" + xhr.status + ", " + thrownError + " " + ajaxOptions + ")");
                        }
                    });


                }
            }

            function CreditCardValidator(inputCredit, cardRow, errMsg, checkCreditCardEnabled) {
                this.inputCredit = inputCredit;
                this.cardRow = cardRow;
                this.errMsg = errMsg;
                this.checkCreditCardEnabled = checkCreditCardEnabled;
                $(inputCredit).bind("keyup",
                    function() {
                        var tb = this;
                        //$(cardRow).css("background-color", "");
                        $(cardRow).removeClass("error");
                        $(errMsg).hide();
                        var maxLength = $(tb).attr('maxlength');
                        var reg = new RegExp("\\D");
                        if (reg.test($(tb).val())) {
                            $(tb).val($(tb).val().replace(reg, ''));
                        }

                        if ($(tb).val().length == maxLength) {
                            var cc = $(inputCredit);
                            cc.eq((cc.index($(tb))) + 1).focus();
                        }
                    }).css("text-align", "center").attr("autocomplete", "off");
                $("#<%=ddlCreditCardMonth.ClientID %>,#<%=ddlCreditCardYear.ClientID %>").change(function()
                {
                    if($("#<%=ddlCreditCardMonth.ClientID %>").val() != '' || $("#<%=ddlCreditCardYear.ClientID %>").val() != '')
                    {
                        $('#ExpDateRow').removeClass('error');
                        $('#ExpDateError').hide();
                    }
                });
                this.check = function() {
                    var reg = new RegExp("\\D");
                    var cards = $(inputCredit);
                    var creditCardNo = "";
                    var isPass = true;
                    
                    $.each(cards,
                        function() {
                            if (reg.test($(this).val()) || $(this).val().length != $(this).attr('maxlength')) {
                                isPass = false;
                            }
                        });

                    if (!isPass) {
                        //$(cardRow).css("background-color", "#feb3b3").css("width", "95%");
                        $(cardRow).addClass("error");
                        //$("#errMsg").text('輸入錯誤');
                        $(errMsg).text('請輸入您的信用卡資訊');
                        $(errMsg).show();
                        return false;
                    }

                    // assembling credit card number. 
                    $.each(cards,
                        function() {
                            creditCardNo += $(this).val();
                        });

                    if (checkCreditCardEnabled) {
                        // Luhn algorithm.
                        if (!checkCreditCardWithLuhnAlgorithm(creditCardNo)) {
                            //$(cardRow).css("background-color", "#feb3b3").css("width", "95%");
                            $(cardRow).addClass("error");
                            $(errMsg).text('信用卡卡號錯誤');
                            $(errMsg).show();
                            return false;
                        }
                    }
                    notifyAppReloadUrl();
                    if($("#<%=ddlCreditCardMonth.ClientID %>").val()=='' || $("#<%=ddlCreditCardYear.ClientID %>").val()=='')
                    {
                        $("#ExpDateRow").addClass("error");
                        $("#ExpDateError").text('請選擇有效期限');
                        $("#ExpDateError").show();
                        return false;
                    }
                };
            };
                       
            window.creditCardValidator = new CreditCardValidator('.input-credit', "#cardRow", "#creditCardError", <%=conf.CheckCreditCardEnabled ? "true" : "false"%>);


            $("input[id*='FoodSubscription_1']").next().text("桃竹苗");
            $("input[id*='FoodSubscription_2']").hide();
            $("input[id*='FoodSubscription_2']").next().hide();

            $("input[id*='FoodSubscription_3']").next().text("中彰投");

            $("input[id*='FoodSubscription_5']").next().text("雲嘉南");
            $("input[id*='FoodSubscription_4']").hide();
            $("input[id*='FoodSubscription_4']").next().hide();


            $("input[id*='chklSubscription_1']").next().text("旅遊‧玩美‧休閒");
            $("input[id*='chklSubscription_0']").hide();
            $("input[id*='chklSubscription_0']").next().hide();

        });

        $(window).scroll(function () {
            $('[id*=hidScroll]').val(typeof window.pageYOffset != 'undefined' ? window.pageYOffset : document.documentElement.scrollTop);
        });

        function CheckInvoiceInfo() {
            var rlt = true;
            var InvoiceTitle = $.trim($("#<%=InvoiceTitle.ClientID %>").val());
            var InvoiceNumber = $.trim($("#<%=InvoiceNumber.ClientID %>").val());
            var InvoiceBuyerName = $.trim($("#<%=InvoiceBuyerName.ClientID %>").val());
            var InvoiceAddress = $.trim($("#<%=InvoiceAddress.ClientID %>").val());

            var addr = $("#<%= ddlInvoiceCity.ClientID %>").children(":selected").html() +
            $("#<%= ddlInvoiceArea.ClientID %>").children(":selected").html() +
                $("#<%= InvoiceAddress.ClientID %>").val();
            $('#hidInvoiceAddress').val(addr);

            if (InvoiceTitle == "") {
                $('#invoTitleErr').show();
                $("#divInvoTitle").addClass("error");
                $("#<%=InvoiceTitle.ClientID %>").focus();
                rlt = false;
            } else {
                $('#invoTitleErr').hide();
                $("#divInvoTitle").removeClass("error");
            }

            if (InvoiceNumber == "") {
                $('#invoNumberErr').show();
                $("#divInvoNumber").addClass("error");
                $("#<%=InvoiceNumber.ClientID %>").focus();
                rlt = false;
            } else {
                var invalidList = "00000000,11111111";
                if (/^\d{8}$/.test(InvoiceNumber) == false || invalidList.indexOf(InvoiceNumber) != -1) {
                    $('#invoNumberError').show();
                    $("#divInvoNumber").addClass("error");
                    $("#<%=InvoiceNumber.ClientID %>").focus();
                    rlt = false;
                } else {
                    $('#invoNumberError').hide();
                    $('#invoNumberErr').hide();
                    $("#divInvoNumber").removeClass("error");
                }
                var validateOperator = [1, 2, 1, 2, 1, 2, 4, 1],
                    sum = 0,
                    calculate = function (product) { 
                        var ones = product % 10,
                            tens = (product - ones) / 10;
                        return ones + tens;
                    };
                for (var i = 0; i < validateOperator.length; i++) {
                    sum += calculate(InvoiceNumber[i] * validateOperator[i]);
                }
                if (rlt) {
                    if (!(sum % 10 == 0 || (InvoiceNumber[6] == "7" && (sum + 1) % 10 == 0))) {
                        $('#invoNumberError').show();
                        $("#divInvoNumber").addClass("error");
                        $("#<%=InvoiceNumber.ClientID %>").focus();
                        rlt = false;
                    }
                    else {
                        $('#invoNumberErr').hide();
                        $('#invoNumberError').hide();
                        $("#divInvoNumber").removeClass("error");
                    }
                } 
            }
            
            if (InvoiceBuyerName == "") {
                $('#invoNameErr').show();
                $("#divInvoName").addClass("error");
                $("#<%=InvoiceBuyerName.ClientID %>").focus();
                rlt = false;
            } else {
                $('#invoNameErr').hide();
                $("#divInvoName").removeClass("error");
            }

            if (InvoiceAddress == "" || $.trim($('#ddlInvoiceCity').val())=="-1" || $.trim($('#ddlInvoiceArea').val())=="-1") {
                $('#invoAddrErr').show();
                $("#divInvoAddr").addClass("error");
                $("#<%=InvoiceAddress.ClientID %>").focus();
                rlt = false;
            } else {
                $('#invoAddrErr').hide();
                $("#divInvoAddr").removeClass("error");
            }
            return rlt;

        }

        function CheckWinInvoiceInfo() {

            var InvoiceName = $.trim($("#<%=wInvoiceName.ClientID %>").val());
            var PhoneNumber = $.trim($("#<%=wPhoneNumber.ClientID %>").val());
            var InvoiceAddress = $.trim($("#<%=wInvoAddr.ClientID %>").val());
            var AddrCity= $("#wInvoAddrCity").val();
            var AddrArea = $("#wInvoAddrArea").val();
            var rlt = true;

            if (InvoiceName == "") {
                $('#wInvoiceNameErr').show();
                $("#divWInvoiceName").addClass("error");
                $("#<%=wInvoiceName.ClientID %>").focus();
                rlt = false;
            } else {
                $('#wInvoiceNameErr').hide();
                $("#divWInvoiceName").removeClass("error");
            }

            if (PhoneNumber == "") {
                $('#wPhoneNumberErr').show();
                $("#wPhoneNumber").addClass("error");
                $("#<%=wPhoneNumber.ClientID %>").focus();
                rlt = false;
            } else {
                $('#wPhoneNumberErr').hide();
                $("#wPhoneNumber").removeClass("error");
            }

            if (AddrCity < 0 || AddrArea < 0 || InvoiceAddress == "") {
                $('#wInvoAddrErr').show();
                $("#divWInvoAddr").addClass("error");
                $("#<%=wInvoAddr.ClientID %>").focus();
                rlt = false;
            }
            else {
                $('#wInvoAddrErr').hide();
                $("#divWInvoAddr").removeClass("error");
            }

            return rlt;
        }

        function CheckBasic() {
            var check = true;
            var lastname = $.trim($("#<%=tbLastName.ClientID %>").val());
            var firstname = $.trim($("#<%=tbFirstName.ClientID %>").val());
            $('#li_name').removeClass('error');
            $('#li_name').find('.if-error').hide();
            if (lastname.length == 0 || firstname.length == 0) {
                $('#li_name').addClass('error');
                $('#li_name').find('.if-error').show();
                check = false;
            }

            var mobile = $.trim($("#<%=tbMobile.ClientID %>").val());
            $('#li_mobile').removeClass('error');
            $('#li_mobile').find('#MobileEmptyError').hide();
            $('#li_mobile').find('#MobileRegexError').hide();
            $('#li_mobile').find('.mobile_note').show();
            if (mobile.length == 0) {
                $('#li_mobile').addClass('error');
                $('#li_mobile').find('#MobileEmptyError').show();
                $('#li_mobile').find('.mobile_note').hide();
                check = false;
            } else if (!mobile.match(/^[0-9]{10}$/)) {
                $('#li_mobile').addClass('error');
                $('#li_mobile').find('#MobileRegexError').show();
                $('#li_mobile').find('.mobile_note').hide();
                check = false;
            }

            var userEmail = $.trim($("#<%=tbUserEmail.ClientID %>").val());
            $('#li_useremail').removeClass('error');
            $('#li_useremail').find('#EmailEmptyError').hide();
            $('#li_useremail').find('#EmailRegexError').hide();
            $('#li_useremail').find('.mobile_note').show();
            if (userEmail.length == 0) {
                $('#li_useremail').addClass('error');
                $('#li_useremail').find('#EmailEmptyError').show();
                $('#li_useremail').find('#EmailRegexError').hide();
                $('#li_useremail').find('.useremail_note').hide();
                check = false;
            }
            else {
                var regMail = /^[a-zA-Z0-9._%+-]+[^.]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
                var res = regMail.test(userEmail);
                if (res) {
                    res = checkEmailTwoDots(userEmail);
                    if (res)
                    {
                        $('#li_useremail').find('.useremail_note').show();
                        $('#li_useremail').removeClass('error');
                        $('#li_useremail').find('#EmailEmptyError').hide();
                        $('#li_useremail').find('#EmailRegexError').hide();
                    }
                    else
                    {
                        $('#li_useremail').find('.useremail_note').hide();
                        $('#li_useremail').addClass('error');
                        $('#li_useremail').find('#EmailEmptyError').hide();
                        $('#li_useremail').find('#EmailRegexError').show();
                    }
                    
                } else {
                    $('#li_useremail').find('.useremail_note').hide();
                    $('#li_useremail').addClass('error');
                    $('#li_useremail').find('#EmailEmptyError').hide();
                    $('#li_useremail').find('#EmailRegexError').show();
                }
                check = res;
            }

            var address = $('#addressin span:first ').css('display');
            $('#li_address').removeClass('error');
            $('#li_address').find('.if-error').hide();
            if (address == 'inline') {
                $('#li_address').addClass('error');
                $('#li_address').find('.if-error').show();
                check = false;
            }
            else {
                var addresstext = $.trim($('#addressin input:first ').val());
                if (addresstext.length == 0) {
                    $('#li_address').addClass('error');
                    $('#li_address').find('.if-error').show();
                    check = false;
                }
            }
            if ($("#ddlcity_m").val() == '-1') {
                $('#li_address').addClass('error');
                $('#li_address').find('.if-error').show();
                check = false;
            }
            if ($("#ddlarea_m").val() == '-1') {
                $('#li_address').addClass('error');
                $('#li_address').find('.if-error').show();
                check = false;
            }
            return check;
        }
        function CheckShip() {
            $('#shipmessage').hide();
            var check = true;
            var alias = $.trim($("#<%=txtAlias.ClientID %>").val());
            $('#li_r_alias').removeClass('error');
            $('#li_r_alias').find('.if-error').hide();
            $('#li_r_alias').find('.note').show();
            if (alias.length == 0) {
                $('#li_r_alias').addClass('error');
                $('#li_r_alias').find('.if-error').show();
                $('#li_r_alias').find('.note').hide();
                check = false;
            }
            var contactname = $.trim($("#<%=tbContactName.ClientID %>").val());
            $('#li_r_contact_name').removeClass('error');
            $('#li_r_contact_name').find('.if-error').hide();
            if (contactname.length == 0) {
                $('#li_r_contact_name').addClass('error');
                $('#li_r_contact_name').find('.if-error').show();
                check = false;
            }
            var address = $('#addressin2 span:first ').css('display');
            $('#li_r_address').removeClass('error');
            $('#li_r_address').find('.if-error').hide();
            if (address == 'inline') {
                $('#li_r_address').addClass('error');
                $('#li_r_address').find('.if-error').show();
                check = false;
            }
            else {
                var addresstext = $.trim($('#addressin2 input:first ').val());
                if (addresstext.length == 0) {
                    $('#li_r_address').addClass('error');
                    $('#li_r_address').find('.if-error').show();
                    check = false;
                }
            }
            if ($("#<%=ddlbuilding_d.ClientID %> :selected").val() == '00000000-0000-0000-0000-000000000000') {
                $('#li_r_address').addClass('error');
                $('#li_r_address').find('.if-error').show();
                check = false;
            }
            var mobilenumber = $.trim($("#<%=tbMobileNumber.ClientID %>").val());
            $('#li_r_mobile').removeClass('error');
            $('#li_r_mobile').find('#rMobileEmptyError').hide();
            $('#li_r_mobile').find('#rMobileRegexError').hide();
            $('#li_r_mobile').find('.note').show();
            if (mobilenumber.length == 0) {
                $('#li_r_mobile').addClass('error');
                $('#li_r_mobile').find('#rMobileEmptyError').show();
                $('#li_r_mobile').find('.note').hide();
                check = false;
            } else if (!mobilenumber.match(/^[0-9]{10}$/)) {
                $('#li_r_mobile').addClass('error');
                $('#li_r_mobile').find('#rMobileRegexError').show();
                $('#li_r_mobile').find('.note').hide();
                check = false;
            }
            return check;
        }
        //修改密碼 submit onClient check
        function checkPassword() {
            var returnValue = true;
            //舊密碼必填檢查
            if ($(".OriginalPassword").val() == "") {
                $(".OldPasswordArea").addClass("error");
                $(".OldPasswordEdit").show();
                returnValue = false;
            }
            else {
                $(".OldPasswordArea").removeClass("error");
                $(".OldPasswordEdit").hide();
            }
            //新密碼必填檢查
            if ($(".NewPassword").val() == "") {
                $(".NewPasswordArea").addClass("error");
                $(".pNewPassword").addClass("if-error");
                $(".NewPasswordNormal").hide();
                $(".NewPasswordEdit").show();
                returnValue = false;
            }
            else {
                $(".NewPasswordArea").removeClass("error");
                $(".pNewPassword").removeClass("if-error");
                $(".NewPasswordEdit").hide();
                $(".NewPasswordNormal").show();
            }
            //新密碼格式檢查
            if (!regularNewPassword()) {
                $(".NewPasswordArea").addClass("error");
                returnValue = false;
            }
            else { $(".NewPasswordArea").removeClass("error"); }
            //確認新密碼
            if ($(".PasswordCheckEdit").val() == "") {
                $(".ConfirmPasswordArea").addClass("error");
                $(".ConfirmPasswordCompare").hide();
                $(".ConfirmPasswordRequired").show();
            } else {
                $(".ConfirmPasswordArea").removeClass("error");
                $(".ConfirmPasswordCompare").hide();
                $(".ConfirmPasswordRequired").hide();
            }
            if (!confirmNewPassword()) {
                $(".ConfirmPasswordArea").addClass("error");
                returnValue = false;
            } else { $(".ConfirmPasswordArea").removeClass("error"); }
            return returnValue;
        }
        function confirmNewPassword() {
            var passwordCheck = $(".PasswordCheckEdit").val();
            if (passwordCheck != "") {
                var newPassword = $(".NewPassword").val();
                $(".ConfirmPasswordRequired").hide();
                if (newPassword != passwordCheck) {
                    $(".ConfirmPasswordArea").addClass("error");
                    $(".ConfirmPasswordCompare").show();
                    return false;
                }
                else {
                    $(".ConfirmPasswordArea").removeClass("error");
                    $(".ConfirmPasswordCompare").hide();
                    return true;
                }
            }
            else {
                //$(".ConfirmPasswordArea").removeClass("error");
                return false;
            }
        }
        function regularNewPassword() {
            var passwordValue = $(".NewPassword").val();
            if (passwordValue != "") {
                if (!passwordValue.match("^[0-9a-zA-Z]{6,100}$")) {
                    $(".NewPasswordArea").addClass("error");
                    $(".pNewPassword").addClass("if-error");
                    $(".NewPasswordRegularMessage").show();
                    return false;
                }
                else {
                    $(".NewPasswordArea").removeClass("error");
                    $(".pNewPassword").removeClass("if-error");
                    $(".NewPasswordRegularMessage").hide();
                    return true;
                }
            }
            else {
                //$(".NewPasswordArea").removeClass("error");
                return false;
            }
        }

        function setChangeEmailError(msg) {
            $('#emailShow').hide();
            $('#emailEdit').addClass('error').show();
            $('#newEmailErrorMessage').text(msg);
            $('#txtNewEmail').keyup(function () {
                $('#newEmailErrorMessage').text('');
                $('#emailEdit').removeClass('error');
            });
        }

        function setChangeEmailAlert(msg) {
            alert(msg);
        }

         function setBindEmailAlert(msg) {
            alert(msg);
        }

        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(
            function () {
                $(".OriginalPassword").bind("keyup", function () {
                    $(".PasswordEdit").hide();
                    if ($(".OriginalPassword").val() != "") $(".OldPasswordEdit").hide();
                });
                $(".OriginalPassword").bind("blur", function () {
                    $(".OldPasswordArea").removeClass("error");
                });
                $(".NewPassword").bind("keyup", function () {
                    $(".NewPasswordRegularMessage").hide();
                    $(".NewPasswordArea").removeClass("error");
                    if ($(".NewPassword").val() != "") {
                        $(".NewPasswordEdit").hide();
                        $(".pNewPassword").removeClass("if-error");
                        $(".NewPasswordNormal").show();
                    }
                });
                $(".NewPassword").bind("blur", function () {
                    regularNewPassword();
                    confirmNewPassword();
                });
                $(".PasswordCheckEdit").bind("keyup", function () {
                    $(".ConfirmPasswordArea").removeClass("error");
                    $(".ConfirmPasswordCompare").hide();
                });
                $(".PasswordCheckEdit").bind("blur", function () {
                    confirmNewPassword();
                });
            });
        function slidingMenuToggle() {
            $("#wrap").toggleClass("mbe-menu-show");
        }
        $(function () {
            $(".mbe-menu-btn").click(slidingMenuToggle);
            $(".mbe-channel").click(slidingMenuToggle);
        });

        function checkSubscribe() {
            if (!isSubscribeDelivery() && $("#isSubscriptionDelivery").val() == "True" && $("#isSubscriptionPponSelect").val() == "False") {
                $("#userPponSelect").show();
            }
            else {
                $("#btnUpdateSubscription").click();
            }
        }
        function isSubscribeDelivery() {
            var checkedDelivery = false;
            $("input:checkbox[name*='chklSubscription']:checked").each(function () {
                if ($(this).next().text() == "<%=CategoryManager.Default.Delivery.CategoryName%>") {
                    checkedDelivery = true;
                }
            });
            return checkedDelivery;
        }
        function setSubscribePponSelect() {
            $("input:checkbox[name*='chklSubscription']").each(function () {
                if ($(this).next().text() == "<%=CategoryManager.Default.PponSelect.CategoryName%>") {
                    $(this).attr("checked", "checked");
                }
            });
        }

        // for EInvoiceMode
        function ChangeCarrierType() {
            $('#divPhoneCarrier').hide();
            $('#divPersonalCertificateCarrier').hide();
            $('#PaperInvoice').css('display', 'none');
            //$('#txtPhoneCarrier').val('');
            //$('#txtPersonalCertificateCarrier').val('');
            //$('#EInvoiceBuyerName').val('');
            //$('#EInvoiceAddress').val('');
            $('#divPhoneCarrier').removeClass("error");
            $('#hidCarrIdVerify').val('false');
            $('#phoneCarrierErr').hide();
            $('#phoneCarrierInvalid').hide();
            $('#divPersonalCertificateCarrier').removeClass("error");
            $('#personalCertificateCarrierErr').hide();
            $('#personalCertificateCarrierInvalid').hide();
            $("#tdInvoName").removeClass("error");
            $('#EinvoNameErr').hide();
            $("#tdEInvoAddr").removeClass("error");
            $('#EinvoAddrErr').hide();

            var opt = $('[id*=ddlCarrierType]').val();
            if (opt == '<%= (int)CarrierType.Phone %>') {
                $('#divPhoneCarrier').css('display', 'block');
                $('#carrierNote').text('使用手機條碼載具或自然人憑證載具，將由財政部通知中獎。');
            } else if (opt == '<%= (int)CarrierType.PersonalCertificate %>') {
                $('#divPersonalCertificateCarrier').css('display', 'block');
                $('#carrierNote').text('使用手機條碼載具或自然人憑證載具，將由財政部通知中獎。');
            } else if (opt == '<%= (int)CarrierType.None %>') {
                $('#PaperInvoice').css('display', 'block');
                $('#carrierNote').text('響應發票無紙化，建議使用自動兌獎無負擔的電子發票。');
            }
        }

        function CheckEInvoiceInfo() {
            var rlt = true;
            var carrier = $('[id*=ddlCarrierType]').val();
            if (carrier == '<%= (int)CarrierType.Phone %>') {
                var phoneCarrier = $.trim($('#txtPhoneCarrier').val());
                if (phoneCarrier == '') {
                    $('#divPhoneCarrier').addClass("error");
                    $('#phoneCarrierErr').show();
                    rlt = false;
                } else {
                    var phone_code39 = /^\/[0-9A-Z\-.$/\+%\*\s]*$/;
                    if (!phone_code39.test(phoneCarrier) || phoneCarrier.length != 8 || $('#hidCarrIdVerify').val() == 'false') {
                        $('#phoneCarrierInvalid').show();
                        $('#divPhoneCarrier').addClass("error");
                        rlt = false;
                    } else {
                        $('#phoneCarrierErr').hide();
                        $('#divPhoneCarrier').removeClass("error");
                    }
                }
            } else if (carrier == '<%= (int)CarrierType.PersonalCertificate %>') {
                var PersonalCertificateCarrier = $.trim($('#txtPersonalCertificateCarrier').val());
                if (PersonalCertificateCarrier == '') {
                    $('#divPersonalCertificateCarrier').addClass("error");
                    $('#personalCertificateCarrierErr').show();
                    rlt = false;
                } else {
                    var PersonalCertificate = /^[A-Z][A-Z]\d{14}$/;
                    if (!PersonalCertificate.test(PersonalCertificateCarrier)) {
                        $('#divPersonalCertificateCarrier').addClass("error");
                        $('#personalCertificateCarrierInvalid').show();
                        rlt = false;
                    } else {
                        $('#divPersonalCertificateCarrier').removeClass("error");
                        $('#personalCertificateCarrierInvalid').hide();
                    }
                }
            } else if (carrier == '<%= (int)CarrierType.None %>') {

                if ($.trim($('#EInvoiceBuyerName').val()) == "") {
                    $('#EinvoNameErr').show();
                    $("#tdEInvoName").addClass("error");
                    $('#EInvoiceBuyerName').focus();
                    rlt = false;
                } else {
                    $('#EinvoNameErr').hide();
                    $("#tdEInvoName").removeClass("error");
                }

                if ($.trim($('#EInvoiceAddress').val()) == "") {
                    $('#EinvoAddrErr').show();
                    $("#tdEInvoAddr").addClass("error");
                    $('#EInvoiceAddress').focus();
                    rlt = false;
                } else {
                    $('#EinvoAddrErr').hide();
                    $("#tdEInvoAddr").removeClass("error");
                }
            }
            return rlt;
        }
        function notifyAppReloadUrl() {
            appInterface.reLoadUrl('<%=Request.Url.AbsoluteUri%>');
        }
        function CopyInvoiceInfo() {
            if ($('#hidInvoiceCity').val() != $('#ddlInvoiceCity').val()) {
                $('#hidInvoiceArea').val(-1);
            } else {
                $('#hidInvoiceArea').val($('#ddlInvoiceArea').val());
            }

            $('#hidInvoiceCity').val($('#ddlInvoiceCity').val());
        }

    </script>
    <style>
        .password_error {
            color: #BF0000;
        }
    </style>
    <asp:HiddenField ID="hidInvoiceCity" runat="server" ClientIDMode="Static"/>
    <asp:HiddenField ID="hidInvoiceArea" runat="server" ClientIDMode="Static"/>
    <asp:HiddenField ID="hidInvoiceAddress" runat="server" ClientIDMode="Static"/>
    <asp:TextBox ID="hidScroll" runat="server" Style="display: none;" Text="0"></asp:TextBox>
    <uc1:Paragraph ID="pMain" ExternalCache="true" runat="server" />
    <div class="mc-navbar">
        <a href="coupon_List.aspx">
            <div class="mc-navbtn">
                訂單/憑證
            </div>
        </a>
        <%if (MGMFacade.IsEnabled) {%>
        <a href="/user/GiftBox">
            <div class="mc-navbtn">禮物盒</div>
        </a>
        <%} %>
        <a href="DiscountList.aspx">
            <div class="mc-navbtn">
                折價券
            </div>
        </a><a href="BonusListNew.aspx">
            <div class="mc-navbtn">
                餘額明細
            </div>
        </a><a href="Collect.aspx">
            <div class="mc-navbtn">
                我的收藏
            </div>
        </a><a>
            <div class="mc-navbtn on">
                帳號設定
            </div>
        </a><a href="/User/ServiceList">
            <div class="mc-navbtn rd-C-Hide">
                客服紀錄
            </div>
        </a>
    </div>
    <div class="mc-content">
        <div class="divMessage" style="display:none">資料處理中，請稍後...</div>
        
        <%--帳號設定--%>
        <asp:Panel ID="pan_E1" runat="server">
            <h1 class="rd-smll">帳號設定</h1>
            <hr class="header_hr" />
            <div class="grui-form">
                <asp:PlaceHolder ID="phEmailShow" runat="server">
                <div class="form-unit" id="emailShow">
                    <label class="unit-label">
                        電子信箱</label>
                    <div class="data-input rd-data-input">
                        <p>
                            <asp:Label ID="lab_Email" runat="server" ClientIDMode="Static"></asp:Label>
                        </p>
                    </div>
                    <asp:PlaceHolder ID="phChangeEmailButton" runat="server">
                        <div class="data-input rd-data-input" style="">
                            <input type="button" id="btnChange" class="btn btn-small btn-primary form-inline-btn" value="變更信箱">
                        </div>
                </asp:PlaceHolder>
                </div>
                    </asp:PlaceHolder>
                <div class="form-unit" id="emailEdit" style="display: none">
                    <label class="unit-label">
                        電子信箱</label>
                    <div class="data-input rd-data-input">
                        <input type="text" id="txtNewEmail" runat="server" class="input-half" placeholder="請輸入新的信箱" clientidmode="Static" />
                        <p class="if-error">
                            <sapn id="newEmailErrorMessage"></sapn>
                        </p>
                    </div>
                    <div class="data-input rd-data-input">
                        <asp:Button ID="btnConfirmChangeEmail" runat="server" CssClass="btn btn-small btn-primary form-inline-btn"
                            Text="確定變更" OnClick="btnConfirmChangeEmail_Click" />
                        <input type="button" id="btnEmailCancel" class="btn btn-small form-inline-btn" value="取消">
                    </div>
                </div>
                <!-- 手機認證 -->
                <div class="form-unit" runat="server" id="mobileAuthPanel">
                    <% if (User.Identity.IsAuthenticated && MemberLinkSet[SingleSignOnSource.Mobile17Life])
                       { %>
                    <label for="" class="unit-label unit-label-check" title="已認證">
                        手機號碼
                    </label>
                    <div class="data-input rd-data-input" style="display: block">
                        <p><%=MobileMember.MobileNumber %></p>
                    </div>
                    <div class="data-input rd-data-input" style="display: block">
                        <input type="button" id="btnChangeMobile" runat="server" clientidmode="Static"
                            class="btn btn-small btn-primary form-inline-btn" value="變更手機" />
                    </div>
                    <% }
                       else
                       {%>
                    <label for="" class="unit-label">
                        手機號碼
                    </label>
                    <div class="data-input rd-data-input" style="display: block">
                        <input type="button" class="btn btn-small btn-primary form-inline-btn navi-btn" id="btnBindMobile2" value="前往認證">
                    </div>
                    <%} %>
                </div>
                <div class="form-unit">
                    <label class="unit-label">目前登入方式</label>
                    <div class="data-input rd-data-input">
                        <p>
                            <asp:Label ID="lblLoginSource" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-unit" id="phMemberLinkBindReadonlyContent" runat="server">
                    <label class="unit-label">已開通的登入方式</label>
                    <div class="data-input rd-data-input">
                        <p>
                            <asp:Label ID="lblLoginMethod" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-unit" id="phMemberLinkBindEditContent" runat="server">
                    <label class="unit-label">登入方式</label>
                    <div class="data-input rd-data-input">
                        <ul class="login-way">
                            <%
                                if (MemberLinkSet != null)
                                {
                                    if (MemberLinkSet[SingleSignOnSource.ContactDigitalIntegration])
                                    {
                                        Response.Write("<li><img src='../Themes/PCweb/images/Tick.png' /><span>17life</span></li>");
                                    }
                                    else
                                    {
                                        Response.Write("<li><img src='../Themes/PCweb/images/x.png' /><span>17Life</span>" +
                                            "<input type='button' id='btnBindLife17' class='btn btn-small btn-primary form-inline-btn' value='" + BindingMemberButtonText + "' /></li>");
                                    }
                                    if (MemberLinkSet[SingleSignOnSource.PayEasy])
                                    {
                                        Response.Write("<li><img src='../Themes/PCweb/images/Tick.png' /><span>PayEasy</span>" +
                                            "<input type='button' id='btnUnbindPayeasy' class='btn btn-small form-inline-btn' value='解除' /></li>");
                                    }
                                    else
                                    {
                                        Response.Write("<li><img src='../Themes/PCweb/images/x.png' /><span>PayEasy</span></li>");
                                    }
                                    if (MemberLinkSet[SingleSignOnSource.Facebook])
                                    {
                                        Response.Write("<li><img src='../Themes/PCweb/images/Tick.png' /><span>Facebook</span>" +
                                            "<input type='button' id='btnUnbindFacebook' class='btn btn-small form-inline-btn' value='解除' /></li>");
                                    }
                                    else
                                    {
                                        Response.Write("<li><img src='../Themes/PCweb/images/x.png' /><span>Facebook</span>" +
                                            "<input type='button' id='btnBindFacebook' class='btn btn-small btn-primary form-inline-btn' value='開通' /></li>");
                                    }
                                    if (MemberLinkSet[SingleSignOnSource.Line])
                                    {
                                        Response.Write("<li><img src='../Themes/PCweb/images/Tick.png' /><span>Line</span>" +
                                            "<input type='button' id='btnUnbindLine' class='btn btn-small form-inline-btn' value='解除' /></li>");
                                    }
                                    else
                                    {
                                        Response.Write("<li><img src='../Themes/PCweb/images/x.png' /><span>Line</span>" +
                                            "<input type='button' id='btnBindLine' class='btn btn-small btn-primary form-inline-btn' value='開通' /></li>");
                                    }
                                    if (MemberLinkSet.ContainsKey(SingleSignOnSource.Mobile17Life))
                                    {
                                        if (MemberLinkSet[SingleSignOnSource.Mobile17Life])
                                        {
                                            if (MobileMember.Status == (int)MobileMemberStatusType.NumberChecked)
                                            {
                                                Response.Write("<li><img src='../Themes/PCweb/images/x.png' /><span>手機號碼</span>" +
                                                           "<input type='button' id='btnBindMobile' class='btn btn-small btn-primary form-inline-btn navi-btn' value='設定密碼' /></li>");
                                            }
                                            else if (MobileMember.Status == (int)MobileMemberStatusType.Activated)
                                            {
                                                Response.Write("<li><img src='../Themes/PCweb/images/Tick.png' /><span>手機號碼</span>" +
                                                               "</li>");
                                            }
                                        }
                                        else
                                        {
                                            Response.Write("<li><img src='../Themes/PCweb/images/x.png' /><span>手機號碼</span>" +
                                                           "<input type='button' id='btnBindMobile' class='btn btn-small btn-primary form-inline-btn navi-btn' value='開通' /></li>");
                                        }
                                    }
                                }
                            %>
                        </ul>
                    </div>
                </div>
                <asp:PlaceHolder ID="phChangeEmailConfirm" runat="server">
                    <div class="form-unit end-unit">
                        <label class="unit-label">變更信箱申請</label>
                        <div class="data-input rd-data-input">
                            <p>
                                <asp:Literal ID="litNewEmail" runat="server" />
                            </p>
                        </div>
                        <div class="data-input rd-data-input">
                            <input type="button" id="btnChangeMailResend" runat="server" clientidmode="Static"
                                class="btn btn-small btn-primary form-inline-btn" value="重發認證信" onserverclick="btnChangeMailResend_Click" />
                            <input type="button" id="btnChangeMailCancel" runat="server" clientidmode="Static"
                                class="btn btn-small form-inline-btn" value="取消申請" onserverclick="btnChangeMailCancel_Click" />
                            <p class="if-error">
                                您尚未完成信箱變更程序。<br>
                                <br>
                                申請變更時間：<asp:Literal ID="litNewEmailApplyDate" runat="server" />
                                <br>
                                系統已寄發認證信到您的信箱中：<asp:Literal ID="litNewEmail2" runat="server" />。請您於七天內收取變更信箱認證信，並完成認證步驟。
                            </p>
                        </div>
                    </div>
                </asp:PlaceHolder>

                <asp:PlaceHolder ID="phBindingEmailConfirm" runat="server">
                    <div class="form-unit end-unit">
                        <label class="unit-label">開通信箱申請</label>
                        <div class="data-input rd-data-input">
                            <p>
                                <asp:Literal ID="litEmail" runat="server" />
                            </p>
                        </div>
                        <div class="data-input rd-data-input">
                            <input type="button" id="btnBindingMailResend" runat="server" clientidmode="Static"
                                class="btn btn-small btn-primary form-inline-btn" value="重發認證信" onserverclick="btnBindingMailResend_Click" />
                            <input type="button" id="btnBindingMailCancel" runat="server" clientidmode="Static"
                                class="btn btn-small form-inline-btn" value="取消申請" onserverclick="btnBindingMailCancel_Click" />
                            <p class="if-error">
                                您尚未完成信箱開通程序。<br>
                                <br>
                                申請變更時間：<asp:Literal ID="litEmailApplyDate" runat="server" />
                                <br>
                                系統已寄發認證信到您的信箱中：<asp:Literal ID="litEmail2" runat="server" />。請您於七天內收取變更信箱認證信，並完成認證步驟。
                            </p>
                        </div>
                    </div>
                </asp:PlaceHolder>
            </div>
        </asp:Panel>

        <%--17Life登入密碼設定--%>
        <asp:Panel ID="pan_PasswordSet" runat="server" Visible="false">
            <h1 class="rd-smll">17Life登入密碼設定
            </h1>
            <hr class="header_hr" />
            <asp:UpdatePanel ID="UpdatePanel_PasswordSet" runat="server">
                <ContentTemplate>
                    <div class="grui-form">
                        <div class="form-unit">
                            <div class="data-input rd-data-input">
                                <p>設定17Life登入密碼，並不會異動到您的Facebook或Payeasy的登入密碼。</p>
                            </div>
                        </div>
                        <div runat="server" id="divOldPassword" class="form-unit OldPasswordArea">
                            <label class="unit-label rd-unit-label">舊密碼</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="tbOriginalPassword" CssClass="input-half OriginalPassword" TextMode="Password" AutoComplete="off" Columns="12" runat="server"></asp:TextBox>
                                <p class="if-error">
                                    <asp:Label ID="lblOriginalPasswordRequiredFieldMessage" CssClass="OldPasswordEdit" Style="display: none" runat="server" Text="密碼錯誤。提醒您：登入密碼應為6碼以上英文或數字。"></asp:Label>
                                    <asp:Label ID="lblPasswordError" CssClass="PasswordEdit" Visible="false" runat="server" Text="密碼錯誤。提醒您：登入密碼應為6碼以上英文或數字。"></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit NewPasswordArea">
                            <label class="unit-label rd-unit-label">新密碼</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="tbNewPassword" CssClass="input-half NewPassword" TextMode="Password" AutoComplete="off" Columns="12" runat="server"></asp:TextBox>
                                <p class="pNewPassword">
                                    <asp:Label ID="lblNewPasswordRegularError" CssClass="NewPasswordRegularMessage" Style="display: none" runat="server" Text="密碼格式錯誤！"></asp:Label>
                                    <asp:Label ID="lblNewPasswordNormalMessage" CssClass="NewPasswordNormal" runat="server" Text="請輸入6碼以上，英文或數字，英文請區分大小寫。"></asp:Label>
                                    <asp:Label ID="lblNewPasswordMessage" CssClass="NewPasswordEdit" Style="display: none" runat="server" Text="請輸入6碼以上，英文或數字，英文請區分大小寫。"></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit ConfirmPasswordArea">
                            <label class="unit-label rd-unit-label">再次輸入新密碼</label>
                            <div class="data-input rd-data-input">
                                <asp:TextBox ID="tbNewPasswordCheck" CssClass="input-half PasswordCheckEdit" TextMode="Password" AutoComplete="off" Columns="12" runat="server"></asp:TextBox>
                                <p class="if-error">
                                    <asp:Label ID="lblConfirmPasswordRequired" Style="display: none" CssClass="ConfirmPasswordRequired" runat="server" Text="請再輸入一次您的新密碼。"></asp:Label>
                                    <asp:Label ID="lblConfirmPasswordCompare" Style="display: none" CssClass="ConfirmPasswordCompare" runat="server" Text="兩次新密碼輸入不一致。"></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit end-unit">
                            <div class="data-input rd-data-input">
                                <asp:Button ID="btnSaveNewPassword" ValidationGroup="PasswordGroup"
                                    CssClass="btn btn-primary rd-mcacstbtn" runat="server" Text="儲存"
                                    OnClick="btnSaveNewPassword_Click" OnClientClick="return checkPassword()" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        
        <%--個人資料設定--%>
        <h1 class="rd-smll">個人資料設定</h1>
        <hr class="header_hr" />
        <div class="grui-form">
            <div class="form-unit" id="li_name">
                <label class="unit-label rd-unit-label">
                    姓名</label>
                <div class="data-input rd-data-input">
                    <p>
                        姓
                    </p>
                    <asp:TextBox ID="tbLastName" runat="server" CssClass="input-small"></asp:TextBox>
                    <p>
                        名
                    </p>
                    <asp:TextBox ID="tbFirstName" runat="server" CssClass="input-small"></asp:TextBox>
                    <p class="if-error" style="display: none;">請輸入完整的姓名。</p>
                </div>
            </div>
            <div class="form-unit">
                <label class="unit-label rd-unit-label">
                    性別</label>
                <div class="data-input rd-data-input">
                    <asp:RadioButton ID="rb_M" runat="server" Text="男" GroupName="Gender" />
                    <asp:RadioButton ID="rb_F" runat="server" Text="女" GroupName="Gender" />
                </div>
            </div>
            <div class="form-unit">
                <label class="unit-label rd-unit-label">
                    生日</label>
                <div class="data-input rd-data-input">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:DropDownList ID="ddlBirthYear" runat="server" OnSelectedIndexChanged="ChangeBirthDays"
                                AutoPostBack="true" CssClass="select-wauto">
                            </asp:DropDownList>
                            年
                            <br />
                            <asp:DropDownList ID="ddlBirthMonth" runat="server" OnSelectedIndexChanged="ChangeBirthDays"
                                AutoPostBack="true" CssClass="select-wauto">
                            </asp:DropDownList>
                            月
                            <asp:DropDownList ID="ddlBirthDay" runat="server" CssClass="select-wauto">
                            </asp:DropDownList>
                            日
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="form-unit" id="li_mobile">
                <label class="unit-label rd-unit-label">
                    手機</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox ID="tbMobile" runat="server" CssClass="input-half" placeholder="0917171717" pattern="[0-9]*"></asp:TextBox>
                    <p class="mobile_note">
                        簡訊憑證將會發送至此號碼，請確實填寫，避免購買後權益受損。
                    </p>
                    <p id="MobileEmptyError" class="if-error" style="display: none;">請輸入您的手機號碼。</p>
                    <p id="MobileRegexError" class="if-error" style="display: none;">請輸入正確的手機格式。ex：0917171717</p>
                </div>
            </div>
            <div class="form-unit" id="li_useremail">
                <label class="unit-label rd-unit-label">
                    通訊 Email</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox ID="tbUserEmail" runat="server" CssClass="input-half" placeholder="請輸入信箱" ></asp:TextBox>
                    <p class="useremail_note">
                        所有通知信箱將會寄到此信箱，請確實填寫您的常用信箱。
                    </p>
                    <p id="EmailEmptyError" class="if-error" style="display: none;">請輸入您的通訊信箱。</p>
                    <p id="EmailRegexError" class="if-error" style="display: none;">請輸入正確的信箱格式。</p>
                </div>
            </div>
            <div class="form-unit" id="li_address">
                <label class="unit-label rd-unit-label">
                    地址</label>
                <div class="data-input rd-data-input">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" ChildrenAsTriggers="true" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:DropDownList ID="ddlcity_m" runat="server" AutoPostBack="true" CssClass="select-wauto" ClientIDMode="Static"
                                DataTextField="CityName" DataValueField="id" OnSelectedIndexChanged="ddlcity_SelectedIndexChanged" />
                            <asp:DropDownList ID="ddlarea_m" runat="server" AutoPostBack="true" CssClass="select-wauto" ClientIDMode="Static"
                                DataTextField="CityName" DataValueField="id" OnSelectedIndexChanged="ddlarea_SelectedIndexChanged" />
                            <asp:DropDownList ID="ddlbuilding_m" runat="server" AutoPostBack="true" CssClass="select-wauto" Visible="false"
                                DataTextField="Text" DataValueField="Value" OnSelectedIndexChanged="ddlbuilding_SelectedIndexChanged" />
                            <br />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" ChildrenAsTriggers="false" RenderMode="inline"
                        UpdateMode="conditional">
                        <ContentTemplate>
                            <span style="color: Black" id="addressin">
                                <uc1:AddressInputBox ID="aib_m" runat="server" ValidationDisplay="Dynamic" ValidationGroup="cm"
                                    ValidationMessage="" ShowStreetName="False" CssClass="input-half" />
                            </span>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlcity_m" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ddlarea_m" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ddlbuilding_m" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <p class="if-error" style="display: none;">請選擇，並輸入完整的地址。</p>
                </div>
            </div>
            <div class="form-unit end-unit">
                <div class="data-input rd-data-input">
                    <asp:Button ID="btn_Edit" runat="server" OnClick="lbEdit_Click" CssClass="btn btn-primary rd-mcacstbtn"
                        ValidationGroup="cm" OnClientClick="return CheckBasic();" Text="儲存" />
                </div>
            </div>
        </div>
        
        <%--訂閱管理--%>
        <h1 id="edmTpis" class="">訂閱管理</h1>
        <hr class="header_hr" />
        <div class="grui-form">
            <div class="form-unit">
                <label class="unit-label">
                    每日好康報</label>
                <div class="data-input rd-data-input">
                    <p class="note">
                        好康主動通知您，再也不錯過每日最新的優質好康。
                    </p>
                </div>
            </div>
            <div class="form-unit">
                <div class="data-input rd-data-input">
                    <div id="userPponSelect" style="display: none" class="sub-jump">
                        <div class="sub-jp-x">
                            <img src="../Themes/PCweb/images/x.png" onclick="$('#userPponSelect').hide()" width="20" height="20" />
                        </div>
                        <p>已為您取消訂閱宅配！</p>
                        <p>建議您可以訂閱「精選好康」，我們將發送全省通用的好康給您！</p>
                        <div class="sub-jp-date rd-data-input">
                            <input id="pponSelectCategoryId" type="hidden" value="<%=CategoryManager.Default.PponSelect.CategoryId%>" />
                            <input id="addPponSelect" class="btn btn-primary" type="button" value="我要訂">
                            <input id="withNotPponSelect" class="nobtn " type="button" value="不了謝謝!">
                            <label><a target="_blank" href="<%=ResolveUrl("~/newmember/privacy.aspx")%>">隱私權政策</a></label>
                        </div>
                    </div>
                    <p>
                        請勾選訂閱的好康頻道
                    </p>
                    <br />
                    <p>美食：</p>
                    <asp:CheckBoxList ID="chklFoodSubscription" RepeatColumns="8" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                    </asp:CheckBoxList>
                    <br />
                    <asp:CheckBoxList ID="chklSubscription" RepeatColumns="8" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                    </asp:CheckBoxList>
                    <asp:HiddenField ID="isSubscriptionDelivery" ClientIDMode="Static" Value="False" runat="server" />
                    <asp:HiddenField ID="isSubscriptionPponSelect" ClientIDMode="Static" Value="False" runat="server" />
                </div>
            </div>
            <div class="form-unit">
                <label class="unit-label">
                    優惠活動訊息通知</label>
                <div class="data-input rd-data-input">
                    <asp:RadioButtonList ID="rdlIfReceiveEDM" runat="server" RepeatDirection="Horizontal"
                        RepeatLayout="Flow">
                        <asp:ListItem Selected="True" Text="開啟" Value="Yes" />
                        <asp:ListItem Text="關閉" Value="No" />
                    </asp:RadioButtonList>
                    <p class="note">
                        紅利優惠送不完，獲得免費好康的機會就在這裡。
                    </p>
                </div>
            </div>
            <div class="form-unit">
                <label class="unit-label">
                    優質強檔開賣通知</label>
                <div class="data-input rd-data-input">
                    <asp:RadioButtonList ID="rdlIfReceiveStartedSell" runat="server" RepeatDirection="Horizontal"
                        RepeatLayout="Flow">
                        <asp:ListItem Selected="True" Text="開啟" Value="Yes" />
                        <asp:ListItem Text="關閉" Value="No" />
                    </asp:RadioButtonList>
                    <p class="note">
                        不用怕搶不到超級大牌，開賣前第一個通知您。
                    </p>
                </div>
            </div>
            <div class="form-unit end-unit">
                <div class="data-input rd-data-input">
                    <input onclick="checkSubscribe()" class="btn btn-primary" value="儲存" />
                    <asp:Button ID="btnUpdateSubscription" ClientIDMode="Static" OnClick="btnUpdateSubscription_Click" Style="display: none" runat="server" Text="儲存" />
                </div>
            </div>
        </div>
        <%-- 信用卡管理 --%>
        <h1 class="rd-smll">信用卡管理</h1>
        <hr class="header_hr" />
        <div class="grui-form">
            <div class="form-unit" id="cardRow">
                <label for="" class="unit-label title">信用卡號</label>
                <label class="taishinAlert" style="color:red;"></label>
                <div class="data-input">
                    <asp:TextBox type="text" class="input-credit" ID="txtCard1" runat="server" MaxLength="4" autocomplete="off" pattern="[0-9]*"/>
                    -
          <asp:TextBox type="text" class="input-credit" ID="txtCard2" runat="server" MaxLength="4" autocomplete="off" pattern="[0-9]*"/>
                    -
          <asp:TextBox type="text" class="input-credit" ID="txtCard3" runat="server" MaxLength="4" autocomplete="off" pattern="[0-9]*"/>
                    -
          <asp:TextBox type="text" class="input-credit" ID="txtCard4" runat="server" MaxLength="4" autocomplete="off" pattern="[0-9]*"/>
                    <p id="creditCardError" class="if-error"></p>
                </div>
                
            </div>

            <div class="form-unit"  id="ExpDateRow">
                <label for="" class="unit-label title">有效期限</label>
                <div class="data-input">
                    <asp:DropDownList ID="ddlCreditCardMonth" runat="server" CssClass="select-wauto">
                        <asp:ListItem Text="--" Value="" />
                        <asp:ListItem Text="01" Value="01" />
                        <asp:ListItem Text="02" Value="02" />
                        <asp:ListItem Text="03" Value="03" />
                        <asp:ListItem Text="04" Value="04" />
                        <asp:ListItem Text="05" Value="05" />
                        <asp:ListItem Text="06" Value="06" />
                        <asp:ListItem Text="07" Value="07" />
                        <asp:ListItem Text="08" Value="08" />
                        <asp:ListItem Text="09" Value="09" />
                        <asp:ListItem Text="10" Value="10" />
                        <asp:ListItem Text="11" Value="11" />
                        <asp:ListItem Text="12" Value="12" />
                    </asp:DropDownList>

                    <p class="credit">月/20</p>
                    <asp:DropDownList ID="ddlCreditCardYear" runat="server" CssClass="select-wauto" />
                    <p>年</p>
                    <p id="ExpDateError" class="if-error"></p>
                </div>
            </div>
            <div class="form-unit">
                <label for="" class="unit-label title">信用卡名稱</label>
                <div class="data-input">
                    <asp:TextBox ID="txtCreditCardName" ClientIDMode="Static" runat="server" placeholder="請填寫信用卡名稱(非必填)"/>
                </div>
            </div>

            <div class="form-unit end-unit">
                <div class="data-input rd-data-input">
                    <asp:Button class="btn btn-primary rd-mcacstbtn" id="btnAddCreditCard" runat="server" Text="新增" 
                        OnClientClick="if (window.creditCardValidator.check()==false) {return false;}"
                        OnClick="btnAddCreditCard_Click" />
                </div>
            </div>
        </div>
        <asp:Repeater ID="repMemberCreditCards" runat="server" OnItemCommand="repMemberCreditCards_RowCommand">
            <ItemTemplate>
                <div class="mc-addressee-box">
                    <h2 class="rd-c-h2"><%#Eval("CreditCardName") %></h2>
                    <div class="grui-form rd-grui-form">
                        <div class="form-unit end-unit">
                            <label for="" class="unit-label account-label rd-unit-label">卡號</label>
                            <div class="data-input account-input rd-data-input">
                                <p class="rd-c-Cleartop">XXXX-XXXX-XXXX-<%#Eval("CreditCardTail") %></p>
                            </div>
                        </div>
                        <div class="form-unit end-unit">
                            <label for="" class="unit-label account-label rd-unit-label">有效日期</label>
                            <div class="data-input account-input rd-data-input">
                                <p class="rd-c-Cleartop" style="font-size:14px;"><%#Eval("CreditCardMonth") %>月/<%#Eval("CreditCardYear") %>年</p>
                                <asp:Button ID="btnDeleteCreditCard" runat="server" CssClass="btn btn-primary btn-delete rd-mcacstbtn"
                                    CommandName="_DELETE" CommandArgument='<%#Eval("Guid") %>' Text="刪除" OnClientClick="if (confirm('確定刪除嗎')==false) {return false;}" />
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <%--發票歸戶--%>
        <h1 class="rd-smll">發票歸戶</h1>
        <hr class="header_hr" />
        <div class="grui-form">
            <div class="form-unit">
                <div class="data-input rd-data-input">
                    <p>想將會員載具中的發票進行通用載具歸戶，可執行發票歸戶功能。</p>
                    <a href="../Service/einvoiceaccount.ashx?username=<%= User.Identity.Name %>" class="btn btn-small btn-primary form-inline-btn rd-mcacstbtn" target="_blank" style="color: white;">發票歸戶</a>
                </div>
            </div>
            <div class="form-unit">
                <div class="data-input rd-data-input">
                    <p class="note">
                        ● 依財政部令此副本僅供參考，不可直接兌獎。<br />
                        ● 使用會員載具(未歸戶)，17Life會自動進行中獎通知以及中獎發票交付。<br />
                        ● 使用手機條碼以及自然人憑證條碼載具，或已歸戶者，由財政部通知中獎。<br />
                        消費者可至Kiosk列印中獎發票，或由財政部通知，並自動匯款。<br />
                        ● Kiosk列舉: 7-11的ibon; 全家的FarmiPort; 萊爾富的Life-ET。<br />
                        ● 有關發票詳細說明:
         <a href="https://www.einvoice.nat.gov.tw/APMEMBERVAN/GeneralCarrier/generalCarrier?CSRT=5861599567505696271" target="_blank">電子發票整合服務平台
         </a>、
          <a href="https://www.einvoice.nat.gov.tw/home/DownLoad?fileName=1377486728934_0.pdf" target="_blank">電子發票實施作業要點
          </a>。
                    </p>
                </div>
            </div>
        </div>
        
        <%--個人發票載具/資料管理--%>
        <h1 class="rd-smll">個人發票載具/資料管理</h1>
        <hr class="header_hr" />
        <div class="grui-form grui-form-row">
            <div class="form-unit">
                <div class="data-input rd-data-input">
                    <asp:DropDownList ID="ddlCarrierType" runat="server" CssClass="select-wauto" onchange="ChangeCarrierType(this);">
                        <asp:ListItem value="2">手機條碼載具</asp:ListItem>
                        <asp:ListItem value="3">自然人憑證載具</asp:ListItem>
                    </asp:DropDownList>
                    <p id="carrierNote" class="note">使用手機條碼載具或自然人憑證載具，將由財政部通知中獎。</p>
                </div>
            </div>
        </div>
        <div class="grui-form">
            <div class="form-unit " id="divPhoneCarrier">
                <!--user 輸入錯誤格式 增加claa="error"-->
                <label for="" class="unit-label rd-unit-label">手機條碼</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox ID="txtPhoneCarrier" runat="server" CssClass="input-half" MaxLength="8" ClientIDMode="Static" />
                    <asp:HiddenField ID="hidCarrIdVerify" ClientIDMode="Static" runat="server" Value="" EnableViewState="false" />
                    <p class=" ">共8碼。限數字與大寫英文，並以 / 開頭。</p>
                    <!--user 輸入錯誤格式 增加claa="if-error"-->
                    <p class="if-error" id="phoneCarrierErr" style="display:none">請輸入手機條碼。</p>
                    <p class="if-error" id="phoneCarrierInvalid" style="display:none">此手機條碼無效，請您重新輸入。</p>
                </div>
            </div>
            <div class="form-unit" id="divPersonalCertificateCarrier">
                <!--user 輸入錯誤格式 增加claa="error"-->
                <label for="" class="unit-label rd-unit-label">自然人憑證</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox ID="txtPersonalCertificateCarrier" runat="server" CssClass="input-half" ClientIDMode="Static" MaxLength="16" />
                    <p>共16碼：2碼大寫英文+14碼數字。</p>
                    <!--user 輸入錯誤格式 增加claa="if-error"-->
                    <p id="personalCertificateCarrierErr" class="if-error" style="display: none">請輸入自然人憑證條碼。</p>
                    <p id="personalCertificateCarrierInvalid" class="if-error" style="display:none">此自然人憑證條碼無效，請您重新輸入。</p>
                </div>
            </div>
            <div runat="server" id="PaperInvoice">
                <div class="form-unit" id="tdEInvoName">
                    <label for="" class="unit-label rd-unit-label">收件人</label>
                    <div class="data-input rd-data-input">
                        <asp:TextBox runat="server" ID="EInvoiceBuyerName" CssClass="input-half" ClientIDMode="Static" />
                        <p id="EinvoNameErr" class="if-error" style="display: none">請填寫發票收件人姓名。</p>
                    </div>
                </div>
                <div class="form-unit" id="tdEInvoAddr">
                    <label for="" class="unit-label rd-unit-label">發票地址</label>
                    <div class="data-input rd-data-input">
                        <asp:TextBox runat="server" ID="EInvoiceAddress" CssClass="input-full" ClientIDMode="Static" />
                        <p id="EinvoAddrErr" class="if-error" style="display: none">請填寫發票收件地址。</p>
                    </div>
                </div>
            </div>
            <div class="form-unit end-unit">
                <div class="data-input rd-data-input">
                    <asp:Button ID="EinvoiceSave" runat="server" OnClick="btnEinvoiceSave_Click"  CssClass="btn btn-primary rd-mcacstbtn"
                         OnClientClick="return CheckEInvoiceInfo();" Text="儲存" />
                </div>
            </div>
        </div>
        
        <%--公司紙本發票(三聯式)管理--%>
        <h1 class="rd-smll">公司紙本發票(三聯式)管理</h1>
        <hr class="header_hr" />
        <div class="grui-form">
            <asp:HiddenField ID="hidEinvoiceTripleId" runat="server" />    
            <div id="divInvoTitle" class="form-unit">
                <label for="" class="unit-label rd-unit-label">發票抬頭</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox ID="InvoiceTitle" runat="server" CssClass="input-half" ClientIDMode="Static"></asp:TextBox>
                    <p id="invoTitleErr" class="if-error" style="display: none;">請填入發票抬頭。</p>
                </div>
            </div>
            <div id="divInvoNumber" class="form-unit">
                <label for="" class="unit-label rd-unit-label">統一編號</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox runat="server" ID="InvoiceNumber" CssClass="input-half" MaxLength="8" ClientIDMode="Static"></asp:TextBox>
                    <p id="invoNumberErr" class="if-error" style="display: none;">請填寫統一編號八碼。</p>
                    <p id="invoNumberError" class="if-error" style="display: none;">請填入正確的統一編號。</p>
                </div>
            </div>
            <div id="divInvoName" class="form-unit">
                <label for="" class="unit-label rd-unit-label">收件人</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox runat="server" ID="InvoiceBuyerName" CssClass="input-half" ClientIDMode="Static"></asp:TextBox>
                    <p id="invoNameErr" class="if-error" style="display: none;">請填寫發票收件人姓名。</p>
                </div>
            </div>
            <div id="divInvoAddr" class="form-unit">
                <label for="" class="unit-label rd-unit-label">發票地址</label>
                <div class="data-input rd-data-input">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true" RenderMode="Inline">
                    <ContentTemplate> 
                        <asp:DropDownList ID="ddlInvoiceCity" runat="server" CssClass="select-wauto"  onchange="CopyInvoiceInfo();" ClientIDMode="Static">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlInvoiceArea" runat="server" CssClass="select-wauto"  onchange="CopyInvoiceInfo();" ClientIDMode="Static">
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
                    <asp:TextBox runat="server" ID="InvoiceAddress" CssClass="input-half" ClientIDMode="Static"></asp:TextBox>
                    <p id="invoAddrErr" class="if-error" style="display: none;">請填寫發票收件地址。</p>
                </div>
            </div>
            <div class="form-unit end-unit">
                <div class="data-input rd-data-input">
                    <asp:Button ID="InvoiceTripleSave" runat="server" OnClick="btnUpdateEinvoiceTriple_Click"  CssClass="btn btn-primary rd-mcacstbtn"
                         OnClientClick="return CheckInvoiceInfo();" Text="儲存" />
                </div>
            </div>
        </div>
        <%--中獎發票管理--%>
        <h1 class="rd-smll">中獎發票管理</h1>
        <hr class="header_hr" />
        <div class="grui-form">
            <asp:HiddenField ID="hidWinEinvoiceId" runat="server" />   
            <div id="divWInvoiceName" class="form-unit">
                <label for="" class="unit-label rd-unit-label">中獎人姓名</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox ID="wInvoiceName" runat="server" CssClass="input-half" ClientIDMode="Static"></asp:TextBox>
                    <p id="wInvoiceNameErr" class="if-error" style="display: none;">請填寫發票收件人姓名。</p>
                </div>
            </div>
            <div id="divWPhoneNumber" class="form-unit">
                <label for="" class="unit-label rd-unit-label">聯絡電話</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox runat="server" ID="wPhoneNumber" CssClass="input-half" MaxLength="10" ClientIDMode="Static"></asp:TextBox>
                    <p id="wPhoneNumberErr" class="if-error" style="display: none;">請填寫電話號碼。</p>
                </div>
            </div>
            <div id="divWInvoAddr" class="form-unit">
                <label for="" class="unit-label rd-unit-label">地址</label>
                <div class="data-input rd-data-input">
                    <select id="wInvoAddrCity" name="wInvoAddrCity" style="width: 10%">
                        <option value="-1">請選擇</option>
                    </select>
                    <select id="wInvoAddrArea" name="wInvoAddrArea" style="width: 10%">
                        <option value="0">請選擇</option>
                    </select>
                    <br/>
                    <asp:HiddenField ID="hiddenAddrCity" runat="server" />
                    <asp:TextBox runat="server" ID="wInvoAddr" CssClass="input-half" ClientIDMode="Static"></asp:TextBox>
                    <p id="wInvoAddrErr" class="if-error" style="display: none;">請填寫發票收件地址。</p>
                </div>
            </div>
            <div class="form-unit end-unit">
                <div class="data-input rd-data-input">
                    <asp:Button ID="WinInvoiceSave" runat="server" OnClick="btnUpdateWinEinvoice_Click"  CssClass="btn btn-primary rd-mcacstbtn"
                         OnClientClick="return CheckWinInvoiceInfo();" Text="儲存" />
                    <input type="checkbox" id="ckbGetMember" />同通訊地址
                </div>
            </div>
        </div>
        <%--收件人管理--%>
        <h1 class="rd-smll">收件人管理</h1>
        <hr class="header_hr" />
        <div class="grui-form">
            <div class="form-unit rd-C-Hide" id="li_r_alias">
                <label class="unit-label">
                    名稱</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox ID="txtAlias" runat="server" CssClass="input-half" />
                    <p class="note">
                        為此收件人資料命名，使用上會更方便唷。ex：一姬的公司 or 一姬住家
                    </p>
                    <p class="if-error" style="display: none;">請填入資料名稱，方便後續選取使用。</p>
                </div>
            </div>
            <div class="form-unit" id="li_r_contact_name">
                <label class="unit-label rd-unit-label">
                    收件人</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox ID="tbContactName" runat="server" CssClass="input-half" />
                    <asp:TextBox ID="tbCompanyName" runat="server" CssClass="input-half" Visible="false" />
                    <asp:TextBox ID="tbDepartment" runat="server" CssClass="input-half" Visible="false" />
                    <p class="if-error" style="display: none;">請填入收件人名稱。</p>
                </div>
            </div>
            <div class="form-unit" id="li_r_mobile">
                <label class="unit-label rd-unit-label">
                    手機號碼</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox ID="tbMobileNumber" CssClass="input-half" runat="server" placeholder="0917171717" pattern="[0-9]*" />
                    <p class="note rd-c-br">
                        請輸入十位數字號碼。ex : 0917171717
                    </p>
                    <p id="rMobileEmptyError" class="if-error" style="display: none;">請輸入您的手機號碼。</p>
                    <p id="rMobileRegexError" class="if-error" style="display: none;">請輸入正確的手機格式。ex：0917171717</p>
                </div>
            </div>
            <div class="form-unit">
                <label class="unit-label rd-unit-label">
                    電話</label>
                <div class="data-input rd-data-input">
                    <asp:TextBox ID="tbAreaCode" runat="server" CssClass="input-mini rd-input-mini" pattern="[0-9]*"></asp:TextBox>
                    <p>
                        -
                    </p>
                    <asp:TextBox ID="tbCompanyTel" runat="server" Columns="10" CssClass="input-small rd-input-small" pattern="[0-9]*" />
                    <p>
                        #
                    </p>
                    <asp:TextBox ID="tbExtension" runat="server" Columns="2" CssClass="input-mini rd-input-mini" pattern="[0-9]*" />
                    <p class="note rd-c-br">
                        ex : 02-21171717#17
                    </p>
                </div>
            </div>
            <div class="form-unit" id="li_r_address">
                <label class="unit-label rd-unit-label">
                    收件地址</label>
                <div class="data-input rd-data-input">
                    <span class="rd-c-br">
                        <asp:UpdatePanel ID="upz" runat="server" ChildrenAsTriggers="true" RenderMode="Inline">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlcity_d" runat="server" AutoPostBack="true" DataTextField="CityName"
                                    DataValueField="id" CssClass="select-wauto" OnSelectedIndexChanged="ddlcity_SelectedIndexChanged" />
                                <asp:DropDownList ID="ddlarea_d" runat="server" AutoPostBack="true" DataTextField="CityName"
                                    DataValueField="id" CssClass="select-wauto" OnSelectedIndexChanged="ddlarea_SelectedIndexChanged" />
                                <br />
                                <asp:DropDownList ID="ddlbuilding_d" runat="server" AutoPostBack="true" DataTextField="Text" Visible="false"
                                    DataValueField="Value" CssClass="select-wauto" OnSelectedIndexChanged="ddlbuilding_SelectedIndexChanged" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </span>
                    <asp:UpdatePanel ID="upa" runat="server" ChildrenAsTriggers="false" RenderMode="inline"
                        UpdateMode="conditional">
                        <ContentTemplate>
                            <span id="addressin2">
                                <uc1:AddressInputBox ID="aib_d" runat="server" ValidationDisplay="Dynamic" ValidationGroup="ci"
                                    ShowStreetName="False" ValidationMessage="" CssClass="input-half" />
                            </span>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlcity_d" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ddlarea_d" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ddlbuilding_d" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:TextBox ID="tbAddressMemo" runat="server" Visible="false" />
                    <p class="if-error" style="display: none;">請選擇，並輸入完整的地址。</p>
                </div>
            </div>
            <div class="form-unit end-unit">
                <div class="data-input rd-data-input">
                    <asp:Button ID="btn_Add" runat="server" OnClick="AddDelivery" CssClass="btn btn-primary rd-mcacstbtn"
                        ValidationGroup="ci" Text="新增" OnClientClick="return CheckShip();" />
                </div>
            </div>
        </div>
        <asp:Repeater ID="repAddr" runat="server" OnItemCommand="rpt_RowCommand">
            <ItemTemplate>
                <div class="mc-addressee-box">
                    <h2 class="rd-c-h2">
                        <%# ((MemberDelivery)(Container.DataItem)).AddressAlias %></h2>
                    <div class="grui-form rd-grui-form">
                        <div class="form-unit end-unit">
                            <label class="unit-label rd-unit-label">
                                收件人</label>
                            <div class="data-input rd-data-input">
                                <p class="rd-c-Cleartop">
                                    <%# ((MemberDelivery)(Container.DataItem)).ContactName %>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit end-unit">
                            <label class="unit-label rd-unit-label">
                                手機號碼</label>
                            <div class="data-input rd-data-input">
                                <p class="rd-c-Cleartop">
                                    <%# ((MemberDelivery)(Container.DataItem)).Mobile %>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit end-unit">
                            <label class="unit-label rd-unit-label">
                                市內電話</label>
                            <div class="data-input rd-data-input">
                                <p class="rd-c-Cleartop">
                                    <%# ((MemberDelivery)(Container.DataItem)).Tel %>
                                    #<%# ((MemberDelivery)(Container.DataItem)).Ext %></p>
                            </div>
                        </div>
                        <div class="form-unit end-unit">
                            <label class="unit-label rd-unit-label">
                                收件地址</label>
                            <div class="data-input rd-data-input">
                                <p class="rd-c-Cleartop">
                                    <%# ((MemberDelivery)(Container.DataItem)).Address %>
                                </p>
                                <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary btn-delete rd-mcacstbtn" CommandArgument="<%# ((MemberDelivery)(Container.DataItem)).Id %>"
                                    OnClientClick="return confirm('確定要刪除?');" CommandName="delitem" Text="刪除" />
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    
    </div>
    <asp:HiddenField ID="hif_Type" runat="server" />
    <asp:CheckBox ID="cbx_Mode" runat="server" Visible="false" />
    <asp:HiddenField ID="hif_ID" runat="server" />
    
     
</asp:Content>

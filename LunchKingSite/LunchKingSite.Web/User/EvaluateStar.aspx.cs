﻿using LunchKingSite.WebLib.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using System.Web.Services;
using LunchKingSite.Core;
using System.Xml.Linq;
using log4net;

namespace LunchKingSite.Web.User
{
    public partial class EvaluateStar : LocalizedBasePage, IEvaluateStar
    {

        public event EventHandler SaveFeedback;
        public event EventHandler SaveOpenEvaluteMail;

        #region Property

        protected static ILog logger = LogManager.GetLogger(typeof(EvaluateStar));

        private EvaluateStarPresenter _presenter;
        public EvaluateStarPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        private XElement qResult;
        public XElement QuestionResult { get { return qResult; } }

        public Guid tId
        {
            get
            {
                string tId = Request.QueryString["tId"];
                Guid tGuid = Guid.Empty;

                if (!string.IsNullOrWhiteSpace(tId) && Guid.TryParse(tId, out tGuid))
                {
                    return tGuid;
                }
                return tGuid;
            }
        }
        public string flag { get { return Request.QueryString["flag"] ?? ""; } }

        public int fromType
        {
            get
            {
                string queryString = Request.QueryString["ft"];

                if (queryString != null)
                {
                    var isDefined = Enum.IsDefined(typeof(EvaluateSourceType), queryString);

                    if (isDefined)
                    {
                        var sorceType = (EvaluateSourceType)Enum.Parse(typeof(EvaluateSourceType), queryString);
                        return (int)sorceType;
                    }
                }
                return (int)EvaluateSourceType.None;    
            }
        }

        public string QTable { get { return ""; } set { Question_Table.Text = value; } }

        public string ItemName { get { return ""; } set { Item_Name.Text = value; } }

        public int DataType { get; set; }

        public Guid orderGuid
        {
            get
            {
                string order_guid = Request.QueryString["oGuid"];
                Guid oGuid = Guid.Empty;

                if (!string.IsNullOrWhiteSpace(order_guid) && Guid.TryParse(order_guid, out oGuid))
                {
                    return oGuid;
                }
                return oGuid;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();

            if (orderGuid != Guid.Empty)
            {
                //計算開信數
                SaveOpenEvaluteMail(sender, e);
                return;
            }
        }

        protected void btn_submit_Click(object sender, EventArgs e)
        {
            var q1Score = 0;
            try
            {
                DataType = (int)EvaluateDataType.Final;
                var qMin = Convert.ToInt16(Request.Form["qMinId"]);
                var qMax = Convert.ToInt16(Request.Form["qMaxId"]);
                qResult = new XElement("Root");

                for (int i = qMin; i <= qMax; i++)
                {
                    string htmlId = string.Format("q{0}_val", i);
                    var elem = Request.Form[htmlId];

                    if (!string.IsNullOrWhiteSpace(elem))
                    {
                        int type = Convert.ToInt16(elem.Split('^')[0]);
                        var val = elem.Split('^').Skip(1);
                        switch (type)
                        {
                            case (int)EvaluateType.Rate:
                                qResult.Add(new XElement("row", new XElement("qid", i)
                                    , new XElement("type", (int)EvaluateType.Rate)
                                    , new XElement("val", val)));
                                if (i == 1) int.TryParse(elem.Split('^')[1], out q1Score);
                                break;
                            case (int)EvaluateType.Comment:
                                qResult.Add(new XElement("row", new XElement("qid", i)
                                    , new XElement("type", (int)EvaluateType.Comment)
                                    , new XElement("val", val)));
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
            }

            if (q1Score < 3 && !User.Identity.IsAuthenticated) //第一題分數若小於3需登入才能正式紀錄評分
            {
                DataType = (int)EvaluateDataType.Temp;
                SaveFeedback(sender, e); //save temp data
                //FormsAuthentication.RedirectToLoginPage("flag=none"); //若這樣使用會接不到第二個參數flag，所以用下面方式
                Response.Redirect(FormsAuthentication.LoginUrl + "?ReturnUrl=" + HttpUtility.UrlEncode(Request.Url.AbsolutePath + "?tid=" + tId + "&flag=done"));
                return;
            }
            else
            {
                SaveFeedback(sender, e);
                SayMessage("exitPop");
            }
        }

        public void SayMessage(string msg)
        {
            ScriptManager.RegisterStartupScript(this, typeof(Button), "refundsuccess", "block_ui($('#" + msg + "'),0,0);", true);
        }
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="Einvoice_Winner.aspx.cs" Inherits="LunchKingSite.Web.User.einvoice_winner"
    Title="發票中獎確認" %>

<%@ Register Src="../ControlRoom/Controls/AddressInputBox.ascx" TagName="AddressInputBox"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">

    <script type="text/javascript">
        $(function () {
            $(".sliderWrapMenber").hide();
        });
        function checktext() {
            $('#InvoiceConfirmName').removeClass("error");
            if ($("#<%=tbx_WinnerName.ClientID %>").val() == '') {
                $('#InvoiceConfirmName').addClass("error");
            }
            $('#InvoiceConfirmTel').removeClass("error");
            if ($("#<%=tbx_WinnerPhone.ClientID %>").val() == '') {
                $('#InvoiceConfirmTel').addClass("error");
            }
            $('#InvoiceConfirmAddress').removeClass("error");
            var v = $('#InvoiceConfirmAddress input[type="text"]:first');
            if ($(v).val() == '') {
                $('#InvoiceConfirmAddress').addClass("error");
            }
           
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div id="FULL" class="rd-payment-Left-width">
    <div id="maincontent" class="clearfix">
      <div class="content-group payment">
        
        <h1 >中獎發票資料填寫</h1>
        <hr class="header_hr" />
        
        <p class="info">
          恭喜您！您交付17Life（康太數位整合股份有限公司）託管的發票已中獎！
          <br/>
          請確實填寫發票資訊，以利發票寄送並確保您的中獎權益。
          <br/>
          <span class="hint">中獎的發票將於10個工作天內，統一以掛號方式寄發至您填寫之指定發票寄送地址，感謝您的支持！</span>
        </p>
        <div class="grui-form">
            <asp:Repeater ID="rp_EinvoiceMains" runat="server">
                <HeaderTemplate>
                    <br/><span style="font-size:18px; font-weight:bold;">發票資料<br></span>
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="form-unit">
                        <label for="" class="unit-label">訂單編號</label>
                        <div class="data-input">
                            <p><%# ((EinvoiceMain)(Container.DataItem)).OrderId %></p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label for="" class="unit-label">開立時間</label>
                        <div class="data-input">
                            <p><%# ((EinvoiceMain)(Container.DataItem)).InvoiceNumberTime.Value.ToString("yyyy/MM/dd") %></p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label for="" class="unit-label">發票號碼</label>
                        <div class="data-input">
                            <p><%# ((EinvoiceMain)(Container.DataItem)).InvoiceNumber %> </p>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
          <div id="InvoiceConfirmName" class="form-unit">
            <label for="" class="unit-label">中獎人</label>
            <div class="data-input">
                <asp:TextBox ID="tbx_WinnerName" runat="server" MaxLength="20" Width="160" CssClass="input-2over3"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="<p><img src='../Themes/PCweb/images/enter-error.png' width='20' height='20' />請填寫真實姓名，若中獎發票上的買受人資料與身分證件不符時，將無法兌獎。</p>" Display="Dynamic" ValidationGroup="ci" ControlToValidate="tbx_WinnerName" CssClass="enter-error"></asp:RequiredFieldValidator>
            </div>
          </div>
          <div id="InvoiceConfirmTel" class="form-unit">
            <label for="" class="unit-label">聯絡電話</label>
            <div class="data-input">
                <asp:TextBox ID="tbx_WinnerPhone" runat="server" MaxLength="20" Width="160" CssClass="input-2over3"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="<p><img src='../Themes/PCweb/images/enter-error.png' width='20' height='20' />請填入正確的發票聯絡電話。</p>" Display="Dynamic" ValidationGroup="ci" ControlToValidate="tbx_WinnerPhone" CssClass="enter-error"></asp:RequiredFieldValidator>
            </div>
          </div>
          <div id="InvoiceConfirmAddress" class="form-unit">
            <label for="" class="unit-label">地址</label>
            <div class="data-input">
                <asp:UpdatePanel ID="upz" runat="server" ChildrenAsTriggers="true" RenderMode="Inline">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddlcity" runat="server" AutoPostBack="true" DataTextField="CityName"
                            DataValueField="id" OnSelectedIndexChanged="ddlcity_SelectedIndexChanged" CssClass="select-wauto" />
                        <asp:DropDownList ID="ddlArea" runat="server" AutoPostBack="true" DataTextField="CityName"
                            DataValueField="id" OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" CssClass="select-wauto" />
                        <asp:DropDownList ID="ddlbuilding" runat="server" AutoPostBack="true" DataTextField="Text" Visible="false"
                            DataValueField="Value" OnSelectedIndexChanged="ddlbuilding_SelectedIndexChanged" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
                <asp:UpdatePanel ID="upa" runat="server" ChildrenAsTriggers="false" RenderMode="inline" UpdateMode="conditional">
                    <ContentTemplate>
                        <span style="color: Black">
                            <uc1:AddressInputBox ID="aib" runat="server" ValidationDisplay="Dynamic" ValidationGroup="ci" CssClass="input-full" ValidationCssclass="enter-error"
                                ValidationMessage="<p><img src='../Themes/PCweb/images/enter-error.png' width='20' height='20' />請確實填寫地址，以利中獎發票寄送給您。</p>" />
                        </span>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlcity" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="ddlArea" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="ddlbuilding" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
          </div>
          <p class="info btn-center">
              <asp:Button ID="lbt_Confirm" runat="server" Text="送出" OnClick="ConfirmAddress" ValidationGroup="ci" CssClass="btn btn-large btn-primary rd-payment-xlarge-btn rd-dis-s"  OnClientClick="checktext();" />
          </p>
        </div>
      </div><!--// content-group //end-->
    </div><!--// #maincontent //end-->
  </div><!--// #FULL //end-->
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/site.master" AutoEventWireup="true"
    CodeBehind="coupon_detail.aspx.cs" Inherits="LunchKingSite.Web.User.coupon_detail" %>

<%@ Register Src="~/Ppon/AvailabilityCtrl.ascx" TagName="AvailabilityCtrl" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="../Themes/default/images/17life/A16New/a16style.css" type="text/css"
        rel="stylesheet" />
    <style type="text/css">
        a:link
        {
            text-decoration: none;
            color: #8E63B7;
        }
        a:visited
        {
            text-decoration: none;
            color: #8E63B7;
        }
        a:hover
        {
            text-decoration: none;
            color: #8E63B7;
        }
        a:active
        {
            font-size: 18px;
            font-weight: bold;
            color: #8E63B7;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MC" runat="server">
    <div id="mainWrapper">
        <div id="pgNavigation">
            <a href="coupon_list.aspx">回P好康列表</a> <a href="default.aspx">回會員專區</a></div>
        <div id="pgrightNavi">
            <a href="../ppon/coupon_print.aspx?cid=<%=CouponId%>">
                <img src="../Themes/default/images/17life/A32/A32_longBtnp.png" width="104" height="33" /></a></div>
        <div class="pgroupTop">
        </div>
        <div class="pgroupCenter">
            <div class="pgleftarea">
                <asp:HyperLink ID="hN" runat="server" />
                <asp:HyperLink ID="hNB" runat="server" Visible="false"></asp:HyperLink>
                <table width="468" cellpadding="2">
                    <tr>
                        <td>
                            <p class="dateline">
                                <%--有效期間 :
                                <asp:Literal ID="lUS" runat="server" />
                                <asp:Literal ID="lus2" Visible="false" runat="server" />
                                ~
                                <asp:Literal ID="lUE" runat="server"></asp:Literal>
                                <asp:Literal ID="lue2" Visible="false" runat="server"></asp:Literal>--%>
                                <span id="StrikeThroughDecoration" runat="server" style="color: Red; font-size: 36px;">
                                    <span id="StrikeThroughDecoration2" runat="server" style="color: Red; font-size: 36px;"></span>
                                </span><span id="ChangeExpireDateContent" runat="server" style="color: Red;">
                                           <span id="ChangeExpireDateContent2" visible="false" runat="server" style="color: Red;"></span>
                                    <asp:Literal ID="ChangedExpireDate" runat="server" Visible="false" />
                                    <asp:Literal ID="ChangedExpireDate2" runat="server" Visible="false" />
                                       </span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                購買人:
                                <asp:Literal ID="lU" runat="server" /></p>
                        </td>
                    </tr>
                </table>
                <p>
                    好康特別備註:
                </p>
                <asp:Literal ID="litDealTags" runat="server"></asp:Literal>
                <asp:Literal ID="lV" runat="server" />
                <asp:Literal ID="lR" runat="server" /><br />
                <asp:Panel ID="pan_Trust" runat="server">
                    ●此憑證由等值購物金兌換之<br />
                    <asp:Literal ID="liTaishin" runat="server" Visible="false">
            ●信託期間：發售日起一年內<br />
            ●本商品(服務)禮券所收取之金額，已存入發行人於台新銀行開立之信託專戶，專款專用。所稱專用，係指供發行人履行交付商品或提供服務義務使用<br />
                    </asp:Literal>
                    <asp:Literal ID="liHwatai" runat="server" Visible="false">
            ●信託期間：發售日起一年內<br />
            ●本商品(服務)禮券所收取之金額，已存入發行人於華泰銀行開立之信託專戶，專款專用。所稱專 用，係指供發行人履行交付商品或提供服務義務使用<br />
                    </asp:Literal>
                    <asp:Literal ID="liMohist" runat="server" Visible="false">
            ●信託期間：發售日起一年內<br />
            ●本商品(服務)禮券所收取之金額，已存入發行人於陽信銀行開立之信託專戶，專款專用。所稱專用，係指供發行人履行交付商品或提供服務義務使用<br />
            ●信託履約禮券查詢網址 http://trust.sunnybank.com.tw/ (於收到兌換券後次日10點起即可查詢)<br />
                    </asp:Literal>
                    <asp:Literal ID="liEntrustSell" runat="server" Visible="false">
            ●代收轉付收據將於核銷使用後開立<br />
                    </asp:Literal>
                </asp:Panel>
                <asp:Literal runat="server" ID ="liGroupCoupon" Visible="false">
                    ●成套票券商品為整套/組購入，故僅限於所有套/組憑證均兌換完畢才能享有完整優惠<br />
                    ●當有退貨情事發生時，將依其整套/組進貨規則進行剩餘面額價值進行退款<br />
                </asp:Literal>
            </div>
            <div class="pgrightarea">
                <h1>
                    <asp:Literal ID="StoreSequenceCode" runat="server" Visible="false">序號：</asp:Literal></h1>
                <h1>
                    <asp:Literal ID="CSSC" runat="server" Visible="false"></asp:Literal></h1>
            </div>
            <div class="pgrightarea">
                憑證編號：
                <h1>
                    <asp:Literal ID="lCS" runat="server"></asp:Literal>-<asp:Literal ID="liCouponCode"
                        runat="server" /></h1>
                <div class="codebox">
                    確認碼：
                    <p>
                        <asp:Image ID="iVC" runat="server" />
                        <asp:Image ID="iQC" runat="server" Visible="false" />
                    </p>
                </div>
            </div>
        </div>
        <div class="pgroupdown">
            <div class="pgdownleft">
                <h1>
                    如何使用 17Life P好康?</h1>
                <div class="pgdbox">
                    <asp:Literal ID="lUse" runat="server"></asp:Literal>
                </div>
                <p>
                    &nbsp;</p>
            </div>
            <div class="pgdownright">
                <asp:Panel ID="pan_ShopInfo" runat="server">
                    <p class="addresstitle">
                        商店資訊:</p>
                    <p class="addresstitle">
                    </p>
                    <uc1:AvailabilityCtrl ID="avc" runat="server" />
                    <p>
                        <asp:Literal ID="lSl" runat="server" /></p>
                    <div id="map" style="width: 300px; height: 250px">
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="pgroupdownnavi">
            <a href="../ppon/coupon_print.aspx?cid=<%=CouponId%>">
                <img src="../Themes/default/images/17life/A32/A32_longBtnp.png" width="104" height="33" /></a></div>
    </div>
    <asp:Label ID="lab_Notice" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lab_Notice2" runat="server" Visible="false"></asp:Label>
</asp:Content>

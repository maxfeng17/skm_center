﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using System;
using System.Web;

namespace LunchKingSite.Web.User
{
    public partial class CouponGet : LocalizedBasePage, ICouponDetailView
    {
        #region old props

        public int? CouponId
        {
            get
            {
                int id;
                return int.TryParse(Request.QueryString["cid"], out id) ? (int?)id : null;
            }
        }

        public string UserName
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }
        
        #endregion old props

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!_presenter.OnViewInitialized())
                {
                    return;
                }
            }
            _presenter.OnViewLoaded();
        }

        private CouponDetailPresenter _presenter;

        public CouponDetailPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public Guid? PreviewBizHourId
        {
            get
            {
                Guid? ret = null;
                if (!string.IsNullOrEmpty(Request.QueryString["bid"]))
                {
                    try
                    {
                        ret = new Guid?(new Guid(Request.QueryString["bid"]));
                    }
                    catch
                    {
                    }
                }
                return ret;
            }
        }

        public string SequenceNumber
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["sequencenumber"]))
                {
                    return Request.QueryString["sequencenumber"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public bool IsEntrepotOrder 
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["epo"]) && Request.QueryString["epo"] == "true") 
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public string CouponCode
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["couponcode"]))
                {
                    return Request.QueryString["couponcode"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string MemberName
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["membername"]))
                {
                    return Request.QueryString["membername"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string GoogleApiKey { set { } }

        public string EncryptedCouponCode { set { } }

        public bool ForceStaticMap
        {
            get
            {
                return false;
            }
        }

        public bool IsFami { get; set; }

        public bool IsZeroActivityShowCoupon { get; set; }

        public CouponCodeType CodeType { set; get; }

        private string dealTags;
        public string DealTags { set { dealTags = value; } }
        public string AvailablesInfo { set; get; }

        public string CreateTime
        {
            get
            {
                if (string.IsNullOrEmpty(Request.QueryString["ct"])) return null;
                try
                {
                    return new DateTime(Int64.Parse(Request.QueryString["ct"])).ToString("yyyy/MM/dd HH:mm:ss.fff"); 
                }
                catch
                {
                    return null;
                }
            }
        }

        public void SetDetail(ViewPponCoupon coupon, IViewPponDeal vpd)
        {
            CouponDocumentBase doc;

            if (Helper.IsFlagSet((GroupOrderStatus)coupon.GroupOrderStatus.Value, GroupOrderStatus.FamiDeal))
            {
                doc = new FamiCouponDocument(coupon, CodeType, vpd);
            }
            else if (Helper.IsFlagSet((GroupOrderStatus) coupon.GroupOrderStatus.Value, GroupOrderStatus.HiLifeDeal))
            {
                doc = new HiLifeCouponDocument(coupon, CodeType, vpd);
            }
            else
            {
                doc = new PponCouponDocument(coupon, vpd, IsEntrepotOrder);
            }

            byte[] pdfBuffer = doc.GetBuffer();

            HttpResponse response = HttpContext.Current.Response;
            response.Cache.SetCacheability(HttpCacheability.NoCache);
            response.ContentType = "application/pdf";
            string fileName = (coupon.CouponUsage + "_" + coupon.SequenceNumber).Replace(" ", "_");
            response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(fileName) + ".pdf");
            response.AddHeader("Content-Length", pdfBuffer.Length.ToString());

            response.OutputStream.Write(pdfBuffer, 0, pdfBuffer.Length);
            response.Buffer = true;
            response.Flush();
            response.End();
        }
    }
}
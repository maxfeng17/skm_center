﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Web.Security;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.User
{
    public partial class DiscountList : MemberPage, IUserDiscountListView
    {
        public event EventHandler<DataEventArgs<int>> GetDiscountListCount;
        public event EventHandler<DataEventArgs<int>> PageChange;
        public event EventHandler<EventArgs> UseStatusChanged;
        public event EventHandler<EventArgs> SortTypeChanged;

        #region props

        private UserDiscountListPresenter _presenter;
        public UserDiscountListPresenter Presenter
        {
            set
            {
                _presenter = value;
                if (value != null)
                {
                    _presenter.View = this;
                }
            }
            get
            {
                return _presenter;
            }
        }

        public string UserName
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }

        public FilterStatus DiscountStatus
        {
            get
            {
                FilterStatus status = FilterStatus.Unused;
                if (ViewState["DiscountStatus"] != null)
                {
                    FilterStatus.TryParse(ViewState["DiscountStatus"].ToString(), out status);
                }
                return status;
            }
            set
            {
                ViewState["DiscountStatus"] = value;
            }
        }

        public SortType Sort
        {
            get
            {
                SortType sort = SortType.ExpireDesc;
                SortType.TryParse(ddlSortType.SelectedValue, out sort);
                return sort;
            }
        }

        public int CurrentPage
        {
            get { return gridPager4.CurrentPage; }
        }

        public int PageSize
        {
            get { return gridPager4.PageSize; }
            set { gridPager4.PageSize = value; }
        }

        public void SetDiscountList(Dictionary<ViewDiscountDetail, bool> discounts)
        {
            divNoDiscount.Visible = false;
            pan_DiscountCode.Visible = false;
            divUseContent.Visible = false;
            divSortArea.Visible = false;

            rptDiscountList.DataSource = discounts;
            rptDiscountList.DataBind();

            gridPager4.ResolvePagerView(CurrentPage, true);

            if (discounts.Count > 0)
            {
                pan_DiscountCode.Visible = true;
                divSortArea.Visible = true;

                if (DiscountStatus == FilterStatus.Unused)
                {
                    divUseContent.Visible = true;
                }
            }
            else
            {
                divNoDiscount.Visible = true;

                switch (DiscountStatus)
                {
                    case FilterStatus.Unused:
                        liNoDiscountMessage.Text = "您目前尚無折價券可使用，請密切注意活動訊息哦！";
                        break;
                    case FilterStatus.Expire:
                        liNoDiscountMessage.Text = "您目前無已失效折價券。";
                        break;
                    case FilterStatus.Used:
                        liNoDiscountMessage.Text = "您目前無已使用折價券。";
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion props

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }

            if (!Page.IsPostBack)
            {
                Presenter.OnViewInitialized();

                ddlSortType.Items.Add(new ListItem("折價券金額低到高", ((int)SortType.AmountAsc).ToString()));
                ddlSortType.Items.Add(new ListItem("折價券金額高到低", ((int)SortType.AmountDesc).ToString()));
                ddlSortType.Items.Add(new ListItem("有效期限近到遠", ((int)SortType.ExpireDesc).ToString()));

                // 更新折價券資料
                CookieManager.SetMemberDiscount(PromotionFacade.GetMemberDiscountCode(UserId));
            }

            Presenter.OnViewLoaded();
        }

        protected void UpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.PageChange != null)
            {
                this.PageChange(this, e);
            }
        }

        protected int RetrieveDiscountCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (GetDiscountListCount != null)
            {
                GetDiscountListCount(this, e);
            }
            return e.Data;
        }

        protected void rptDiscountList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<ViewDiscountDetail, bool>)
            {
                KeyValuePair<ViewDiscountDetail, bool> discountcode = (KeyValuePair<ViewDiscountDetail, bool>)e.Item.DataItem;
                Label lab_UseStatus = (Label)(e.Item.FindControl("lab_UseStatus"));
                if (discountcode.Key.UseTime.HasValue)
                {
                    lab_UseStatus.Text = "<span style=\"color:gray;\">" + Resources.Localization.CouponCodeUsed + "</span>";
                }
                else if ((discountcode.Key.EndTime ?? new DateTime()) < DateTime.Now)
                {
                    lab_UseStatus.Text = "<span style=\"color:#800517;\">" + Resources.Localization.CouponCodeExpired + "</span>";
                }
                else if (discountcode.Key.CancelTime.HasValue)
                {
                    lab_UseStatus.Text = "<span style=\"color:gray;\">" + Resources.Localization.CouponCodeCancelled + "</span>";
                }
                else if (!discountcode.Key.UseTime.HasValue)
                {
                    lab_UseStatus.Text = "<span style=\"color:#6698FF;\">" + Resources.Localization.CouponCodeNotUsed + "</span>";
                }
            }
        }

        protected void btnUnused_Click(object sender, EventArgs e)
        {
            DiscountStatus = FilterStatus.Unused;
            if (this.UseStatusChanged != null)
            {
                this.UseStatusChanged(this, e);
            }
        }

        protected void btnUsed_Click(object sender, EventArgs e)
        {
            DiscountStatus = FilterStatus.Used;
            if (this.UseStatusChanged != null)
            {
                this.UseStatusChanged(this, e);
            }
        }

        protected void btnExpire_Click(object sender, EventArgs e)
        {
            DiscountStatus = FilterStatus.Expire;
            if (this.UseStatusChanged != null)
            {
                this.UseStatusChanged(this, e);
            }
        }

        protected void ddlSortType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.SortTypeChanged != null)
            {
                this.SortTypeChanged(this, e);
            }
        }
    }
}
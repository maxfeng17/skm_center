﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FaqItem.ascx.cs" Inherits="LunchKingSite.Web.User.FaqItem" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>

<ul>
<asp:Repeater ID="FaqItemList" runat="server">
    <ItemTemplate>
        <li><a href="../Ppon/NewbieGuide.aspx?id=li<%#((Faq)Container.DataItem).Id%>"><%#((Faq)Container.DataItem).Contents%></a>        
    </ItemTemplate>
</asp:Repeater>
</ul>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="UserRefund.aspx.cs" Inherits="LunchKingSite.Web.User.UserRefund" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href="../Themes/default/images/17Life/G8Return/ReturnNew.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("../themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("../themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("../themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .return_modify .btn-secondary {
            color: #ffffff;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            background-image: url(btn_bk_pri_ie.png);
            background-color: #2482c1;
            background-image: -moz-linear-gradient(top, #3399dd, #2482c1);
            background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#3399dd), to(#2482c1));
            background-image: -webkit-linear-gradient(top, #3399dd, #2482c1);
            background-image: -o-linear-gradient(top, #3399dd, #2482c1);
            background-image: linear-gradient(to bottom, #3399dd, #2482c1);
            background-repeat: repeat-x;
            border-color: #2482c1;
            border-color: rgba(0, 0, 0, 0.1);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f#3399dd', endColorstr='#2482c1', GradientType=0);
            filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
            text-decoration: none;
            font-size: 14px;
            padding: 6px 12px;
        }
        .input-full {
            height: 20px;
            padding: 3px 5px;
            border: 1px solid #cccccc;
            /*border-radius: 3px;*/
            vertical-align: middle;
            display: inline-block;
            height: 20px;
            padding: 3px 5px;
            margin-bottom: 10px;
            font-size: 12px;
            line-height: 20px;
            color: #555555;
            vertical-align: middle;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=tbx_Reason.ClientID%>').focus(function (e) {
                $('#reasonOther').prop('checked', true);
            });

            if ($("#<%=hif_IsRefundScashToCash.ClientID %>").val() == 'True') {
                $('.isScash').hide();
                $('.isRefund').show();
            }
            else {
                $('.isRefund').hide();
                $('.isScash').show();
            }

            $('input[name=isReceive]').change(function () {
                if ($(this).val() == "false")
                {
                    $('#divReceiver').hide();
                    $('#divPchomeDesc').show();
                }
                else
                {
                    $('#divReceiver').show();
                    $('#divPchomeDesc').hide();
                }
            })

            <%if (IsWms && confProv.WmsRefundEnable && (string.IsNullOrEmpty(confProv.WmsRefundAccount) || (!string.IsNullOrEmpty(confProv.WmsRefundAccount) && UserName == confProv.WmsRefundAccount)))
            {%>
                $('#divReceiver').hide();
                $("#pan_c1").show();
                $("#pan_c3").show();
             <%}else{%>
                $("#pan_c2").show();
                $("#pan_c4").show();
            <%}%>
        });

        function getrefundreasonP() {
            $('#Pop_Refund1P_Alert').hide();
            $('#Pop_Refund_IsReceive_Alert').hide();
            <%if (confProv.EnabledRefundOrExchangeEditReceiverInfo && !IsCoupon)
            {%>
            $('#ReceiverNameErr').hide();
            $('#ReceiverAddressErr').hide();
            var rad = $('input[name=reason]:checked');
            var rad2 = $('input[name=isReceive]:checked');

            if ($(rad2).val() != "false" && ($('.txtReceiverName').val() == "" || $('.txtReceiverAddress').val() == "")) {
                if ($('.txtReceiverName').val() == "") {
                    $('#ReceiverNameErr').show();
                    $('.txtReceiverName').focus();
                }
                if ($('.txtReceiverAddress').val() == "") {
                    $('#ReceiverAddressErr').show();
                    $('.txtReceiverAddress').focus();
                }
                return false;
            }
            <%}%>
            var check = true;
            
            if ($(rad).val() == null || $(rad).val() == "") {
                $('#Pop_Refund1P_Alert').show();
                check = false;
            }
            else if ('<%=IsWms.ToString()%>' == 'True' && $('input[name=isReceive]').length != 0 && $(rad2).length == 0) {
                $('#Pop_Refund_IsReceive_Alert').show();
                check = false;
            }
            else {
                var showMessage;
                showMessage = $(rad).val();
                var radlast = $('input[name=reason]:last');
                if ($(radlast).attr('checked') == 'checked') {
                    if ($("#<%=tbx_Reason.ClientID %>").val() == '') {
                        $('#Pop_Refund1P_Alert').show();
                        check = false;
                    }
                    else {
                        showMessage = $("#<%=tbx_Reason.ClientID %>").val();
                    }
                }
                if (check) {
                    $("#<%=hif_Reason.ClientID %>").val(showMessage);
                    $("#<%=tbx_Reason.ClientID %>").val('');
                    if ($('[name=isReceive]').length > 0)
                    {
                        $("#<%=hif_IsReceive.ClientID %>").val($('[name=isReceive]:checked').val());
                    }
                    
                    
                }
            }
            return check;
        }

        function checkatminfo() {
            var check = true;
            var accountName = $('#note_accountname');
            var divAccountName = $('#div_accountname');
            var bank = $('#error_bank');
            var divBank = $('#div_bank');
            var branch = $('#error_branch');
            var divBranch = $('#div_branch');
            var account = $('#error_account');
            var divAccount = $('#div_account');
            var mobile = $('#error_mobile');
            var divMobile = $('#div_mobile');
            var userId = $('#error_userid');
            var divUserId = $('#div_userid');
            var noteUserId = $('#note_userid');

            //戶名
            var atmaccountname = $.trim($("#<%=tbx_ATMAccountName.ClientID %>").val());
            if (atmaccountname.length == 0) {
                check = false;
                accountName.addClass("if-error");
                divAccountName.addClass("error-bg");
            } else {
                accountName.removeClass("if-error");
                divAccountName.removeClass("error-bg");
            }

            //銀行別
            if ($("#<%=ddl_ATMBankName.ClientID %> :selected").val() == '-1') {
                check = false;
                bank.show();
                bank.addClass("if-error");
                divBank.addClass("error-bg");
            } else {
                bank.hide();
                bank.removeClass("if-error");
                divBank.removeClass("error-bg");
            }

            //分行別
            if ($("#<%=ddl_ATMBankBranch.ClientID %> :selected").val() == '-1') {
                check = false;
                branch.show();
                branch.addClass("if-error");
                divBranch.addClass("error-bg");
            } else {
                branch.hide();
                branch.removeClass("if-error");
                divBranch.removeClass("error-bg");
            }

            //帳號
            var accountNumber = $.trim($("#<%=tbx_ATMAccount.ClientID %>").val());
            if (accountNumber.length == 0) {
                check = false;
                account.show();
                account.addClass("if-error");
                divAccount.addClass("error-bg");
            } else {
                account.hide();
                account.removeClass("if-error");
                divAccount.removeClass("error-bg");
            }

            //手機號碼
            var mobileNumber = $.trim($("#<%=tbx_ATMMobile.ClientID %>").val());
            if (mobileNumber.length == 0) {
                check = false;
                mobile.show();
                mobile.addClass("if-error");
                divMobile.addClass("error-bg");
            } else {
                mobile.hide();
                mobile.removeClass("if-error");
                divMobile.removeClass("error-bg");
            }

            //身份證或統編
            var atmUserId = $.trim($("#<%=tbx_ATMUserID.ClientID %>").val());
            if (atmUserId.length == 0) {
                check = false;
                userId.show();
                noteUserId.hide();
                userId.addClass("if-error");
                divUserId.addClass("error-bg");
            } else {
                userId.hide();
                noteUserId.show();
                userId.removeClass("if-error");
                divUserId.removeClass("error-bg");
            }

            if (check == false) {
                $('#span_atmmessage').show();
                $('.atmwarnning').attr('style', 'color:red');
            }
            return check;
        }

        function errormessagebg() {
            $('#divReturnOrderIdInput').removeClass("form-unit no-line");
            $('#divReturnOrderIdInput').addClass("form-unit error-bg");
        }

        function downloadCreditNote() {
            window.location.href = "<%=confProv.SiteUrl%>/Service/returndiscount.ashx?oid=<%=OrderGuid%>";
            return false;
        }

        function backToCouponDetail() {
            window.location.href = "<%=confProv.SiteUrl%>/User/CouponDetail.aspx?oid=<%=OrderGuid%>";
            return false;
        }

        function goDefault() {
            window.location.href = "<%=confProv.SiteUrl%>";
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <asp:checkbox id="cbx_IsCoupon" runat="server" visible="false" />
    <asp:hiddenfield id="hif_PamelMode" runat="server" />
    <asp:hiddenfield id="hif_Reason" runat="server" />
    <asp:HiddenField ID="hif_ReceiverName" runat="server" />
    <asp:HiddenField ID="hif_ReceiverAddress" runat="server" />
    <asp:hiddenfield id="hif_IsReceive" runat="server" />
    <asp:hiddenfield id="hif_OrderID" runat="server" />
    <asp:hiddenfield id="hif_OrderGuid" runat="server" />
    <asp:hiddenfield id="hif_OrderStatus" runat="server" />
    <asp:hiddenfield id="hif_OrderCreateTime" runat="server" />
    <asp:hiddenfield id="hif_IsEntrustSell" runat="server" />
    <asp:hiddenfield id="hif_IsRefundScashToCash" runat="server" />
    <asp:hiddenfield id="hif_IsThridPartyPay" runat="server" />
    <asp:hiddenfield id="hif_ThirdPartyPaymentSystem" runat="server" />
    <asp:hiddenfield id="hif_IsPartialCancel" runat="server" />
    <asp:hiddenfield id="hif_IsInstallment" runat="server" />
    <asp:hiddenfield id="hif_IsIsp" runat="server" />
    <asp:hiddenfield id="hif_IsGroupCoupon" runat="server" />
    <asp:hiddenfield id="hif_IsWms" runat="server" Visible="false" />
    <asp:panel id="pan_Main" runat="server">
        <!-- 退貨申請起始 -->
        <asp:Panel ID="pan_ReturnOrderIdInput" runat="server">
            <div class="ReturnFrame">
                <h1>退貨申請</h1>
                <div class="ReturnCenter">
                    <div class="grui-form">
                        <div id="divReturnOrderIdInput" class="form-unit no-line">
                            <label class="unit-label">請輸入訂單編號</label>
                            <div class="data-input">
                                <asp:TextBox ID="tbx_OrderID" runat="server" Width="180" CssClass="input-half"></asp:TextBox>
                                <asp:Label ID="lab_OrderStatusMessage" runat="server" CssClass="error-tip"></asp:Label>
                            </div>
                        </div>
                        <div class="form-unit no-line">
                            <label for="" class="unit-label"></label>
                            <div class="data-input">
                                <p>
                                    您可至 <a href="coupon_List.aspx" target="_blank">訂單/憑證</a> 查看訂單編號。請勿輸入憑證號碼。
                                </p>

                                <p class="ReturnRefuRedViceTe">
                                    <%if (!confProv.EnabledExchangeProcess)
                                      { %>
                                    若需要換貨，請聯繫
                                    <a href="<%=CustomerSiteService%>">客服人員</a>
                                    <%}
                                      else
                                      {%>
                                    若需要換貨，請至
                                    <a href="<%=ResolveUrl("~/User/UserExchange.aspx?orderid=" + Request.QueryString["orderid"])%>">換貨申請</a>
                                    <%} %>
                                    申請換貨，請勿直接退貨。
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ReturnBottom">
                    <asp:Button ID="btn_SearchOrderID" runat="server" CssClass="btn btn-large btn-primary"
                        OnClick="SerachOrderInfo" ValidationGroup="A1" Width="146" Text="申請退貨"></asp:Button>
                </div>
            </div>
        </asp:Panel>
        <!-- 選擇退貨申請原因 -->
        <asp:Panel ID="pan_ReturnReasonInput" runat="server" Visible="false">
            <div class="ReturnFrame">
                <h1>退貨申請原因</h1>
                <div class="ReturnCenter">
                    <div class="ReturnApplitext">為了提供您更優質的商品與售後服務，請協助我們了解您的退貨原因。</div>

                    <div class="ReturnAppliReason">
                        <ul>
                            <asp:Panel ID="pan_ReturnReasonInputReason" runat="server">
                            </asp:Panel>
                            <li onclick="this.getElementsByTagName('input')[0].checked=true">
                                <input id="reasonOther" name="reason" type="radio" value="其它原因與建議" />其它原因與建議<br />
                                <asp:TextBox ID="tbx_Reason" runat="server" Rows="4" TextMode="MultiLine" Width="100%" Height="60px"></asp:TextBox>
                            </li>
                        </ul>
                    </div>
                    <div class="ReturnAppliRedTips" id="Pop_Refund1P_Alert" style="display: none">
                        請務必協助我們了解您退貨的原因！
                    </div>
                    <%if (confProv.EnabledRefundOrExchangeEditReceiverInfo && !IsCoupon)
                        {%>
                    <%if (IsWms && confProv.WmsRefundEnable && (string.IsNullOrEmpty(confProv.WmsRefundAccount) || (!string.IsNullOrEmpty(confProv.WmsRefundAccount) && UserName == confProv.WmsRefundAccount)))
                        {%>
                        <div>
                            <span style="color: red; font-weight: bold">請問您是否已收到商品</span>
                            <input name="isReceive" type="radio" value="false" />A.沒有，我尚未收到商品
                            <input name="isReceive" type="radio" value="true" />B.是的，我已收到商品
                        </div>
                        <div class="ReturnAppliRedTips" id="Pop_Refund_IsReceive_Alert" style="display: none">
                            請務必協助我們了解您是否收到商品！
                        </div>
                    <%}%>
                    <div id="divPchomeDesc" style="display:none">
                        24H到貨商品無法及時通知司機取消配送，請近期協助<span style="color: red; font-weight: bold">拒收</span>
                    </div>
                    <div id="divReceiver">
                        <div class="ReturnApplitext" style="color: red; font-weight: bold">收件人資訊</div>
                        <div id="tdReceiver" class="form-unit">
                            <label id="lbReceiverName" for="ReceiverName" class="unit-label">姓名</label>&nbsp;
                            <asp:TextBox ID="txtReceiverName" runat="server" CssClass="txtReceiverName input-2over3 input-full" width="100px" />
                            <span id="ReceiverNameErr" style="display: none;color:darkred;">
                            <img src="../Themes/PCweb/images/enter-error.png"  style="width:18px;height:18px;"><span>請填入姓名！</span>
                            </span>
                            <br/>
                            <label id="lbReceiverAddress" for="ReceiverAddress" class="unit-label">地址</label>&nbsp;
                            <asp:TextBox ID="txtReceiverAddress" runat="server" CssClass="txtReceiverAddress input-full" width="250px"/>
                            <span id="ReceiverAddressErr" style="display: none; color: darkred;">
                            <img src="../Themes/PCweb/images/enter-error.png" style="width:18px;height:18px;"><span>請填入地址！</span>
                            </span>
                        </div>
                    </div>
                    <%}%>
                </div>
                <div class="ReturnBottom">
                    <asp:Button ID="btn_GetReason" runat="server" CssClass="btn btn-large btn-primary"
                        OnClick="GetRefundReason" OnClientClick="return getrefundreasonP();" Text="下一步"></asp:Button>
                </div>
            </div>
        </asp:Panel>

        <!-- 退購物金申請確認 -->
        <asp:Panel ID="pan_RefundCashPointConfirm" runat="server" Visible="false">
            <div class="ReturnFrame">
                <h1>退貨申請</h1>
                <div class="ReturnCenter">
                    <!-- 憑證訂單 -->
                    <asp:Panel ID="pan_RefundCashPointConfirm_ToShopOrder" runat="server" Visible="false">
                        <asp:Panel ID="pan_RefundCash" runat="server" Visible="false">
                            <p class="ReturnRefuRedViceTe">
                                本次申請退貨款項為：<asp:Label runat="server" ID="lbReufndCash"></asp:Label>
                                元。
                            </p>
                        </asp:Panel>
                        <p class="ReturnStepTittle">1、退款說明</p>
                        <p>按下〔確認申請〕後，將為您轉退17Life購物金，未來您可快速便捷的於17life購物；若有需要時，購物金也能轉返現金喔！若您希望直接退現金，請點選〔刷退或帳戶退款〕，並完成申退流程。<br/>
                        購買時，若有使用17Life購物金、17Life紅利金或抵扣消費，將會依比例分別退回；17Life折價券為一次折抵，退貨不返還；使用刷卡或ATM付款金額，將按比例轉退為17Life購物金。</p>
                        <p class="ReturnStepTittle">2、退貨注意事項</p>
                        <p>當您申請退貨，即代表同意授權17Life代為處理發票作廢及銷貨折讓單事宜。17Life將於退貨完成後，開立電子折讓單並以mail方式通知。若您開立三聯式發票，退貨申請完成後，請務必將紙本折讓單寄回。</p>
                        <p>訂單成立日於2016/1/1以前的訂單，不適用電子折讓事宜。</p>
                        <asp:Literal runat="server" ID="liGroupCoupon" Visible="false">
                        <p class="ReturnStepTittle">3、成套票券訂單</p>
                        <p>成套票券所產生的訂單，未能於兌換期間內將整套/組憑證兌換完畢、或於兌換期間發生退貨時，會先將已使用過的憑證轉換為原價，再進行退貨相關處理。</p>
                        </asp:Literal>
                        <p style="padding-top: 10px;">提醒您，退貨一經申請成功，將會退掉訂單內尚未使用的憑證。按下〔確認申請〕後，即無法再取消退貨。</p>
                    </asp:Panel>
                    <!-- 宅配訂單 -->
                    <asp:Panel ID="pan_RefundCashPointConfirm_ToHouseOrder" runat="server" Visible="false">
                        <p class="ReturnStepTittle">1、退貨商品退回須知</p>
                        <p><u>24H到貨宅配商品</u></p>
                        <p>若您已收到商品，我們將通知PChome物流安排人員盡快前往取件。
                        若您還沒收到欲退貨的商品，因時間差來不及取消出貨，請您近期協助拒收。
                        </p>
                        <p><u>一般宅配商品、超商取貨商品</u></p>
                        <p>我們將通知廠商，並安排物流人員盡快前往取件。<br />
                        若您還沒收到欲退貨的商品，我們將請廠商取消出貨，可能因時間差來不及取消出貨，請您拒收即可。<br />
                        另外如頁面(權益說明)有針對退貨商品定義其他規範，也請依頁面說明為主。</p>
                        <p><u>實體票券商品</u></p>
                        <p>因各票券商皆有不同規範，故退貨票券退回方式，請依頁面及權益說明處相關辦法為主，
                        如未依頁面規定時間將票券寄回，恐影響您的退貨權益。</p>
                        <p class="ReturnStepTittle">2、確認商品完整性</p>
                        <p>請保持商品本體、附件、內外包裝、配件、贈品、保證書、原廠包裝、所有隨附文件或資料的完整性；切勿缺漏任何配件或損毀原廠外盒。
                            若商品屬瑕疵情形，請務必拍照，並將圖檔提供於<a href="<%=CustomerSiteService %>" target="_blank" style="color: #08C;"><strong>客服中心</strong></a>，以避免爭議。
                        </p>
                        <p class="ReturnStepTittle">3、包裝原則說明</p>
                        <p><u>24H到貨宅配商品</u></p>
                        <p>請以原包裝紙箱將退貨商品包裝妥當，<strong>若原紙箱已遺失，將無法退貨</strong>。</p>
                        <p><u>一般宅配商品、超商取貨商品</u></p>
                        <p>請儘量以原包裝紙箱將退貨商品包裝妥當，若原紙箱已遺失，請另使用其他紙箱包覆於商品原廠包裝之外，切勿直接於原廠包裝上黏貼紙張或書寫文字。</p>
                        <p class="ReturnStepTittle">4、退費金額計算</p>
                        <p>按下〔確認申請〕後，將為您轉退17Life購物金，未來您可快速便捷的於17Life購物；若有需要時，購物金也能轉返現金喔！若您希望直接退現金，請點選〔刷退或帳戶退款〕，並完成申退流程。<br />
                        購買時，若有使用17 Life購物金、17 Life紅利金或抵扣消費，將會依比例分別退回；17Life折價券為一次折抵，退貨不返還；使用刷卡或ATM付款金額，將按比例轉退為17Life購物金。</p>
                        <p class="ReturnStepTittle">5 、無法退貨的特殊商品</p>
                        <p>若商品為以下狀況，符合『通訊交易解除權合理例外情事適用準則』則排除七天猶豫期，恕無法接受退換貨申請：</p>
                        <p>◆ 總保存期限低於七天(含)/易腐敗商品(如：牛奶)</p>
                        <p>◆ 客製化商品(依您需求開通製作完成)</p>
                        <p>◆ 報紙、期刊、雜誌、拆封後的電腦影音軟體、線上數位化服務</p>
                        <p>◆ 已拆封的個人衛生用品</p>
                        <p>商品若因本身瑕疵申請換貨者，則不在此限，請務必詳細拍照記錄(外箱/宅配單/瑕疵/總商品數量)。</p>
                        <p class="ReturnStepTittle">6、生鮮食品退貨方式</p>
                        <p>◆ 生鮮食品異常或有損毀情形發生，請立即拍照記錄並保留完整包裝，並將商品盡速置放到冷藏/冷凍設備儲存。</p>
                        <p>◆ 請務必於到貨當日，來電或來信告知。</p>
                        <p class="ReturnStepTittle">7、退貨注意事項</p>
                        <p>◆ 商品在收貨7天猶豫期內可接受退貨，特殊商品退貨規則需依頁面權益說明為主。</p>
                        <p>◆ 於七天猶豫期內，若您逾越檢查之必要或因可歸責於您之事由，以致於商品有故障、破損、磨損、刮傷、沾染汙漬等毀損、滅失或變更之情形時，都可能會影響您退、換貨之權利。</p>
                        <p>◆ 當您申請退貨，即代表同意授權17Life代為處理發票作廢及銷貨折讓單事宜，17Life將於退貨完成後，開立電子折讓單並以mail方式通知。若您開立三聯式發票，退貨申請完成後，請務必將紙本折讓單寄回。</p>
                        <p>◆ 訂單成立日於2016/1/1以前的訂單，不適用電子折讓事宜。</p>
                        <p style="padding-top: 10px;">提醒您，退貨一經申請成功，將會退掉訂單內所有商品。取消退貨，請與<a href="<%=CustomerSiteService %>" target="_blank" style="color: #08C;"><strong>客服中心</strong></a>聯繫。</p>
                    </asp:Panel>
                </div>
                <div class="ReturnBottom">
                    <asp:Button ID="btn_RefundCashPoint" runat="server" CssClass="btn btn-large btn-primary"
                        OnClick="ConfirmRefundCashPoint" Text="快速退購物金"></asp:Button>
                    <asp:Button ID="lbt_RefundCash" runat="server" CssClass="btn btn-large"
                        OnClick="ChooseRefundCash" Text="刷退或帳戶退款"></asp:Button>
                </div>
            </div>
        </asp:Panel>

        <!-- 退購物金 申請成功(購物金Only) -->
        <asp:Panel ID="pan_RefundCashPointSuccess" runat="server" Visible="false">
            <div class="ReturnFrame">
                <h1>退貨申請完成</h1>
                <div class="ReturnCenter">
                    <div class="ReturnApplitext">
                        <asp:Label ID="lab_P1111" runat="server" CssClass="ReturnApplitext"></asp:Label>
                    </div>
                    <!-- 兌換開始前 -->
                    <asp:Panel ID="pan_RefundCashPointSuccess_BeforeUseStartTime" runat="server" Visible="false">
                        <p>恭喜您完成退貨申請，退貨處理完畢，將會以e-mail通知您。</p>
                    </asp:Panel>
                    <!-- 兌換開始後 -->
                    <asp:Panel ID="pan_RefundCashPointSuccess_AfterUseStartTime" runat="server" Visible="false">
                        <p>恭喜您完成退貨申請，為提供您更快速的退貨服務，請完成以下步驟：</p>
                        <p><span>● &nbsp;&nbsp;</span>退貨申請確認信（含退貨需知），已寄至您的電子信箱，請前往確認。</p>
                        <asp:Panel ID="pan_IsPaperAllowance1" runat="server" Visible="false">
                            若您開立三聯式發票，請務必將紙本折讓證明單填寫完畢並寄回。您也可以前往〔退貨明細〕下載紙本折讓單，或者直接於此下載
                            <asp:Button ID="btnDownloadCreditNote" runat="server" CssClass="btn btn-small btn-primary" Text="點此下載紙本折讓" OnClientClick="return downloadCreditNote();"></asp:Button>
                        </asp:Panel>                         
                        <!-- 憑證 -->
                        <asp:Panel ID="pan_RefundCashPointSuccess_AfterUseStartTime_ToShopOrder" runat="server" Visible="true">
                            <p><span>● &nbsp;&nbsp;</span><span>待我們審核完後，將會以電子郵件再通知您。</span></p>
                            <p><span>● &nbsp;&nbsp;</span><span>上述步驟皆完成後，購物金將立即退至您的購物金帳戶之中。</span></p>
                        </asp:Panel>
                        <!-- 商品 -->
                        <asp:Panel ID="pan_RefundCashPointSuccess_AfterUseStartTime_ToHouseOrder" runat="server" Visible="false">
                            <span id="pan_c1" style="display:none"><p><span>● &nbsp;&nbsp;</span><span>請以原包裝紙箱將退貨商品包裝妥當，<strong>若原紙箱已遺失，將無法退貨</strong>。</span></p></span>
                            <span id="pan_c2" style="display:none"><p><span>● &nbsp;&nbsp;</span><span>請將商品重新妥善包裝，我們將通知廠商盡速取回；若您還沒收到欲退貨的商品，我們將請廠商取消出貨，可能因時間差來不及取消出貨，請您拒收即可。</span></p></span>
                            <p><span>● &nbsp;&nbsp;</span><span>待廠商確認無誤後，我們將會以電子郵件再通知您。</span></p>
                            <p><span>● &nbsp;&nbsp;</span><span>上述步驟皆完成後，購物金將立即退至您的購物金帳戶之中。</span></p>
                        </asp:Panel>
                        <p style="padding-top: 10px;">退貨處理時間為10~20天，您可以於【會員專區】→【訂單/憑證】下，點選訂單號碼即可進入〔訂單明細〕查詢，隨時了解您的退貨狀態。<asp:Panel ID="pan_IsPaperAllowance3" runat="server" Visible="false">開立三聯式發票者，亦可在退貨明細中，下載〔紙本折讓單〕。</asp:Panel></p>
                    </asp:Panel>
                </div>
                <div class="ReturnBottom">
                    <asp:Button ID="backCouponDetail" runat="server" CssClass="btn btn-large btn-primary" Text="查看退貨狀態" OnClientClick="return backToCouponDetail();"></asp:Button>
                </div>
            </div>
        </asp:Panel>

        <!-- 刷退的申請確認 -->
        <asp:Panel ID="pan_RefundCashConfirm" runat="server" Visible="false">
            <div class="ReturnFrame">
                <h1>退貨申請</h1>
                <div class="ReturnCenter">
                    <p style="text-align: center;">
                        待我們進行審核並確定完畢後，即會進行退款作業，處理完畢將會另以E-mail通知您。<br/>
                        退款金額將直接退至您的信用卡內，退款時間將因各家銀行作業時間不同而異，麻煩耐心等候，謝謝。<br/>
                        提醒您，退貨申請成功後，將會退掉訂單內所有物品，按下［好的，確認申請〕後，即無法再取消退貨。
                    </p>
                </div>
                <div class="ReturnBottom return_modify">
                    <asp:Button ID="btn_ConfirmRefundCash" runat="server" CssClass="btn btn-middle btn-secondary" OnClick="ConfirmRefundCash" Text="好的，確認申請"></asp:Button>
                </div>
                <div class="isRefund">
                    <p class="ReturnRefuRedViceTe error-bg">
                        此筆訂單已退購物金給您，詳見會員中心 > 
                            <a href="<%=confProv.SiteUrl%>/User/BonusListNew.aspx" target="_blank">餘額明細</a>，是否確認申請轉刷退（需10~20工作天）
                    </p>
                </div>
                <div class="isScash">
                    <p style="text-align: center; padding-top: 20px; border-top: 1px dashed #ccc;">
                        選擇轉退17Life購物金，可省去繁複的退貨程序。我們也將優先，並加快您的退貨處理。
                    </p>
                    <p style="text-align: center; padding-top: 10px;">
                        若有需要時，購物金也能再轉返現金喔！
                    </p>
                    <p>&nbsp;</p>
                    <div class="ReturnBottom return_modify">
                        <asp:Button ID="btn_ConfirmRefundCashPoint" runat="server" CssClass="btn btn-middle btn-primary"
                            Text="快速退貨" OnClick="ConfirmRefundCashPoint"></asp:Button>
                    </div>
                </div>
            </div>
        </asp:Panel>

        <!-- 指定帳號退款的申請確認 -->
        <asp:Panel ID="pan_RefundATMConfirm" runat="server" Visible="false">
            <div class="ReturnFrame">
                <h1>退貨申請</h1>
                <div class="ReturnCenter">
                    <p style="text-align: center;">
                        待我們進行審核並確定完畢後，即會進行退款作業，處理完畢將會另以E-mail通知您。<br>
                        退款金額將直接退至您的指定退款帳戶內，因各家銀行作業時間不同，<span style="color: #F00;">約需10-20個工作天</span>，麻煩耐心等候。<br>
                        為了完成指定退戶手續，請填寫帳戶資料
                    </p>
                </div>
                <div class="ReturnBottom return_modify">
                    <asp:Button ID="lbt_RefundATMConfirm" runat="server" CssClass="btn btn-middle btn-secondary" Text="填寫退款帳戶資料" OnClick="ChooseRefundCashATM"></asp:Button>
                </div>
                <div class="isRefund">
                    <p class="ReturnRefuRedViceTe error-bg">
                        此筆訂單已退購物金給您，詳見會員中心 > 
		                <a href="<%=confProv.SiteUrl%>/User/BonusListNew.aspx" target="_blank">餘額明細</a>，是否確認申請轉刷退（需10~20工作天）
                    </p>
                </div>
                <div class="isScash">
                    <p style="text-align: center; padding-top: 20px; border-top: 1px dashed #ccc;">
                        選擇轉退17Life購物金，可省去繁複的退貨程序。我們也將優先，並加快您的退貨處理。
                    </p>
                    <p style="text-align: center; padding-top: 10px;">
                        若有需要時，購物金也能再轉返現金喔！
                    </p>
                    <p>&nbsp;</p>
                    <div class="ReturnBottom return_modify">
                        <asp:Button ID="btn_RefundATMConfirm_RefundCashPoint" runat="server" CssClass="btn btn-middle btn-primary"
                            Text="快速退貨" OnClick="ConfirmRefundCashPoint"></asp:Button>
                    </div>
                </div>
            </div>
        </asp:Panel>

        <!-- 指定帳號退款的帳號填寫 -->
        <asp:Panel ID="pan_RefundAccountInput" runat="server" Visible="false">
            <div class="ReturnFrame">
                <h1>退貨申請</h1>
                <div class="ReturnCenter">
                    <div class="grui-form">
                        <div id="div_accountname" class="form-unit">
                            <label class="unit-label">戶名</label>
                            <div class="data-input">
                                <asp:TextBox ID="tbx_ATMAccountName" runat="server" MaxLength="10" CssClass="input-half"></asp:TextBox>
                                <p id="note_accountname" class="note">請輸入存摺上的帳戶名稱。</p>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div id="div_bank" class="form-unit">
                                    <label class="unit-label">銀行名稱</label>
                                    <div class="data-input">
                                        <asp:DropDownList ID="ddl_ATMBankName" runat="server" DataValueField="bankno" DataTextField="bankname" Width="195"
                                            OnSelectedIndexChanged="ChangeATMBranch" AutoPostBack="true" CssClass="select-wauto">
                                        </asp:DropDownList>
                                        <p id="error_bank" style="display: none;">請選擇銀行別。</p>
                                    </div>
                                </div>
                                <div id="div_branch" class="form-unit">
                                    <label class="unit-label">分行別</label>
                                    <div class="data-input">
                                        <asp:DropDownList ID="ddl_ATMBankBranch" CssClass="select-wauto" Width="195"
                                            runat="server" DataValueField="branchno" DataTextField="branchname">
                                        </asp:DropDownList>
                                        <p id="error_branch" style="display: none;">請選擇分行別。</p>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddl_ATMBankName" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <div id="div_account" class="form-unit">
                            <label class="unit-label">帳號</label>
                            <div class="data-input">
                                <asp:TextBox ID="tbx_ATMAccount" runat="server" MaxLength="14" CssClass="input-half"></asp:TextBox>
                                <p id="error_account" style="display: none;">請輸入帳號。</p>
                            </div>
                        </div>
                        <div id="div_mobile" class="form-unit">
                            <label class="unit-label">手機號碼</label>
                            <div class="data-input">
                                <asp:TextBox ID="tbx_ATMMobile" runat="server" MaxLength="10" CssClass="input-half"></asp:TextBox>
                                <p id="error_mobile" style="display: none;">請輸入手機號碼。</p>
                            </div>
                        </div>
                        <div id="div_userid" class="form-unit">
                            <label class="unit-label">身分證字號或統編</label>
                            <div class="data-input">
                                <asp:TextBox ID="tbx_ATMUserID" runat="server" CssClass="input-half" MaxLength="10"></asp:TextBox>
                                <p id="note_userid" class="note">請確認身分證字號或統一編號，與戶名身分一致。</p>
                                <p id="error_userid" style="display: none;">請輸入與戶名身分一致的身分證字號或統一編號。</p>
                            </div>
                        </div>
                    </div>
                    <p>
                        送出資料後，請繼續後續的申請步驟。
                        我們將於收到退貨文件後開始處理退款，需再額外等待30個工作天。
                    </p>
                    <div class="isScash">
                        <p class="ReturnRefuRedViceTe">
                            選擇轉退17Life購物金，可省去繁複的退貨程序。我們也將優先，並加快您的退貨處理。
                            <asp:Button ID="lbt_RefundAccountInput_RefundCashPoint" runat="server" CssClass="btn btn-small btn-primary" OnClick="ConfirmRefundCashPoint" Text="快速退貨"></asp:Button>
                        </p>
                    </div>
                </div>
                <div class="ReturnBottom">
                    <asp:Button ID="btn_RefundAccountInput_RefundCash" runat="server" CssClass="btn btn-large btn-primary" OnClick="CheckRefundCashATMInfo"
                        OnClientClick="return checkatminfo();" Text="送出資料"></asp:Button>
                </div>
            </div>
        </asp:Panel>

        <!-- 指定帳號退款的帳號確認 -->
        <asp:Panel ID="pan_RefundAccountConfirm" runat="server" Visible="false">
            <div class="ReturnFrame">
                <h1>退貨申請確認</h1>
                <div class="ReturnCenter">
                    <p>為了您的權益，請再確認一次受款資訊，如因資料填寫錯誤造成匯款失敗，將影響退款處理時間。</p>
                    <div class="grui-form">
                        <div class="form-unit">
                            <label class="unit-label">戶名</label>
                            <div class="data-input">
                                <p>
                                    <asp:Label ID="lab_ATMAccountName" runat="server"></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label">銀行名稱</label>
                            <div class="data-input">
                                <p>
                                    <asp:Label ID="lab_ATMBankName" runat="server"></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label">分行別</label>
                            <div class="data-input">
                                <p>
                                    <asp:Label ID="lab_ATMBankBranch" runat="server"></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label">帳號</label>
                            <div class="data-input">
                                <p>
                                    <asp:Label ID="lab_ATMAccount" runat="server"></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label">手機號碼</label>
                            <div class="data-input">
                                <p>
                                    <asp:Label ID="lab_ATMMobile" runat="server"></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label class="unit-label">身分證字號或統編</label>
                            <div class="data-input">
                                <p>
                                    <asp:Label ID="lab_ATMUserID" runat="server"></asp:Label>
                                </p>
                            </div>
                        </div>
                    </div>
                    <p>
                        送出資料後，請繼續後續的申請步驟。我們將於收到退貨文件後開始處理退款，需再額外等待30個工作天。
                    </p>
                    <div class="isScash">
                        <p class="ReturnRefuRedViceTe">
                            選擇轉退17Life購物金，可省去繁複的退貨程序。我們也將優先，並加快您的退貨處理。
                            <asp:Button ID="btn_RefundAccountConfirm_RefundCashPoint" runat="server" CssClass="btn btn-small btn-primary" OnClick="ConfirmRefundCashPoint" Text="快速退貨"></asp:Button>
                        </p>
                    </div>
                    <p>按下〔確認申請〕後，即無法再取消退貨。</p>
                </div>
                <div class="ReturnBottom">
                    <asp:Button ID="btn_RefundAccount_Confirm" runat="server" CssClass="btn btn-large btn-primary" OnClick="ConfirmRefundCashATM" Text="確認申請"></asp:Button>
                    <asp:Button ID="btn_RefundAccount_Modify" runat="server" CssClass="btn btn-large" OnClick="ModifyRefundATMInfo" Text="修改資料"></asp:Button>
                </div>
            </div>
        </asp:Panel>

        <!-- 取消未付款訂單並返還其他支付金額 -->
        <asp:Panel ID="pan_CancelConfirm" runat="server" Visible="false">
            <div class="ReturnFrame">
                <h1>訂單取消申請</h1>
                <div class="ReturnCenter">
                    <p><span style="color:red;">1、退貨商品退回須知</span></p>
                    <p>我們將請廠商不要進行出貨，此筆訂單將會自動取消。若因時間差，導致您收到取貨的簡訊，請勿前往取貨。</p>
                    <p>若您前往取貨，系統便視此訂單成立；屆時若您仍需要退貨，請重新於訂單列表中，進行退貨申請。</p>
                    <br />
                    <p>若您已取貨且欲辦理退貨，請待系統資料更新，並於取貨後隔日中午以後，再進行申請退貨。</p>
                    <p>若於系統尚未更新[已取貨]的資訊前，進行訂單取消作業，會導致申請失敗。</p>
                    <br />
                    <p><span style="color:red;">2、退費金額計算</span></p>
                    <p>購買時，若有使用17 Life購物金、17 Life紅利金或抵扣消費，將會依比例分別退回；17Life折價券為一次折抵，退貨不返還；</p>
                    <p>退款作業將會於 3 ~ 7個工作天內完成，再請確認，謝謝。</p>
                    <br />
                    <p><span style="color:red;">3、發票與折讓處理</span></p>
                    <p>當您申請退貨，即代表同意授權17Life代為處理發票作廢及銷貨折讓單事宜，17Life將於退貨完成後，開立電子折讓單並以mail方式通知。</p>
                    <p>若您開立三聯式發票，退貨申請完成後，請務必將紙本折讓單寄回。</p>
                    <br />
                    <p>提醒您，退貨一經申請成功，將會退掉訂單內所有商品。按下〔申請退貨〕後，即無法再取消退貨。</p>
                </div>
                <div class="ReturnBottom">
                     <asp:Button ID="btn_CancelOrder" runat="server" CssClass="btn btn-large btn-primary" OnClick="ConfirmCancelOrder" Text="申請退貨"></asp:Button>
                </div>
            </div>
        </asp:Panel>

        <!-- 取消未付款訂單並返還其他支付金額 -->
        <asp:Panel ID="pan_CancelSuccess" runat="server" Visible="false">
            <div class="ReturnFrame">
                <h1>訂單取消申請完成</h1>
                <div class="ReturnCenter">
                    <p>您已完成訂單取消申請。</p>
                    <p>申請訂單取消後，若因包裹來不及通知廠商，而導致出貨，提醒您請勿前往取件，若前往取件，訂單取消申請將無法通過。</p>
                    <p>訂購時若有使用購物金或紅利金，系統將會於3-7個工作日完成退款。</p>
                </div>
                <div class="ReturnBottom">
                     <asp:Button ID="backOrderCancelSuccess" runat="server" CssClass="btn btn-large btn-primary" Text="查看申請狀態" OnClientClick="return backToCouponDetail();"></asp:Button>
                </div>
            </div>
        </asp:Panel>

        <!-- 刷退 或 退款至帳戶 申請成功 -->
        <asp:Panel ID="pan_RefundCashATMSuccess" runat="server" Visible="false">
            <div class="ReturnFrame">
                <h1>退貨申請完成</h1>
                <div class="ReturnCenter">
                    <p>恭喜您完成退貨申請，為提供您更快速的退貨服務，請完成以下步驟：</p>
                    <p><span>● &nbsp;&nbsp;</span>退貨申請確認信（含退貨需知），已寄至您的電子信箱，請前往確認。</p>
                    <asp:Panel ID="pan_IsPaperAllowance2" runat="server" Visible="false">
                        若您開立三聯式發票，請務必將紙本折讓證明單填寫完畢並寄回。您也可以前往〔退貨明細〕下載紙本折讓單，或者直接於此下載
                        <asp:Button ID="btn_DownloadCreditNote" runat="server" CssClass="btn btn-middle btn-primary" Text="點此下載紙本折讓" OnClientClick="return downloadCreditNote();"></asp:Button>
                    </asp:Panel>
                    <asp:Panel ID="pan_RefundCashATMSuccess_ToShopOrder" runat="server" Visible="false">
                        <p><span>● &nbsp;&nbsp;</span>待我們審核完後，將會以電子郵件再通知您。</p>
                    </asp:Panel>
                    <asp:Panel ID="pan_RefundCashATMSuccess_ToHouseOrder" runat="server" Visible="false">                        
                        <span id="pan_c3" style="display:none"><p><span>● &nbsp;&nbsp;</span>請以原包裝紙箱將退貨商品包裝妥當，<strong>若原紙箱已遺失，將無法退貨</strong>。</p></span>
                        <span id="pan_c4" style="display:none"><p><span>● &nbsp;&nbsp;</span>請將商品重新妥善包裝，我們將通知廠商盡速取回；若您還沒收到欲退貨的商品，我們將請廠商取消出貨，可能因時間差來不及取消出貨，請您拒收即可。</p></span>
                        <p><span>● &nbsp;&nbsp;</span>待廠商確認無誤後，我們將會以電子郵件再通知您。</p>
                    </asp:Panel>
                    <asp:Literal ID="lt_Cash" runat="server" Visible="false"><span>● &nbsp;&nbsp;</span>上述步驟皆完成後，退款金額將直接退至您的信用卡內，因各家銀行作業時間不同，您應可於下一期信用卡帳單看到此退款（視信用卡結帳週期而定），請您耐心等候。</asp:Literal>
                    <asp:Literal ID="lt_ATM" runat="server" Visible="false"><span>● &nbsp;&nbsp;</span>上述步驟皆完成後，退款金額將直接退到您的指定退款帳戶。完成退貨程序到款項退至您指定的帳戶，約須10-20個工作天，請您耐心等候。</asp:Literal>
                    <asp:Literal ID="lt_Tcash" runat="server" Visible="false"><span>● &nbsp;&nbsp;</span>上述步驟皆完成後，退款金額將直接退至您的{0}內，因各家銀行作業時間不同，您應可於下一期信用卡帳單看到此退款（視信用卡結帳周期而定），請您耐心等候。</asp:Literal>
                    <asp:Literal ID="lt_TcashTaishinPay" runat="server" Visible="false"><span>● &nbsp;&nbsp;</span>上述步驟皆完成後，退款金額將直接退至您的台新儲值支付帳戶內，因銀行作業時間不同，請您耐心等候。</asp:Literal>
                    <p style="padding-top: 20px;">您可以於【會員專區】→【訂單/憑證】下，點選訂單號碼即可進入〔訂單明細〕查詢，隨時了解您的退貨狀態。<asp:Panel ID="pan_IsPaperAllowance4" runat="server" Visible="false">開立三聯式發票者，亦可在退貨明細中，下載〔紙本折讓單〕。</asp:Panel></p>
                </div>
                <div class="ReturnBottom">
                    <asp:Button ID="backCouponDetailSuccess" runat="server" CssClass="btn btn-large btn-primary" Text="查看退貨狀態" OnClientClick="return backToCouponDetail();"></asp:Button>
                </div>
            </div>
        </asp:Panel>

        <!-- 申請(建立退貨單)失敗 -->
        <asp:Panel ID="pan_RefundFail" runat="server" Visible="false">
            <div class="ReturnFrame">
                <h1>退貨申請失敗</h1>
                <div class="ReturnCenter">
                    <p>您的退貨申請發生異常，若您仍須退貨,請<a href="<%=CustomerSiteService%>">聯絡客服人員</a>。</p>
                </div>
                <div class="ReturnBottom">
                    <asp:Button ID="Button1" runat="server" CssClass="btn btn-large btn-primary" Text="回首頁" OnClientClick="return goDefault();"></asp:Button>
                </div>
            </div>
        </asp:Panel>
    </asp:panel>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using System.ComponentModel;

namespace LunchKingSite.Web.User
{
    public partial class Pager : BaseUserControl
    {
        public delegate int GetCountHandler();
        public delegate void UpdateHandler(int pageNumber);

        private event GetCountHandler _getCountFunc = null;
        [Browsable(true), Category("Behavior"), Description("Handler for getting record count"), DesignOnly(true)]
        public event GetCountHandler GetCount
        {
            add { _getCountFunc += new GetCountHandler(value); }
            remove { _getCountFunc = null; }
        }

        private event UpdateHandler _updateFunc = null;
        [Browsable(true), Category("Behavior"), Description("Handler for getting record count"), DesignOnly(true)]
        public event UpdateHandler Update
        {
            add { _updateFunc += new UpdateHandler(value); }
            remove { _updateFunc = null; }
        }

        private int _pageCount = 0;
        public int PageCount
        {
            get { return _pageCount; }
        }

        public string CssClass
        {
            set { divPager.CssClass += " " + value; }
        }

        public int PageSize
        {
            get { return ViewState["ps"] != null ? (int)ViewState["ps"] : 15; }
            set { ViewState["ps"] = value; }
        }

        int _curPage = 1;
        public int CurrentPage
        {
            get
            {
                int.TryParse(ddlPages.SelectedValue, out _curPage);
                if (_curPage == 0) _curPage = 1; //無分頁給1，分頁才不會錯誤
                return _curPage;
            }
            set { ResolvePagerView(value, false); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetupPaging();
                ResolvePagerView(1);
            }
        }

        public void SetupPaging()
        {
            int totalRecords = 0;
            int pageSize = this.PageSize;

            if (_getCountFunc != null)
                totalRecords = _getCountFunc();
            else
                totalRecords = 0;
            //check to see if the totals have been set
            /*
            if (lblPageCount.Text == string.Empty)
            {
                totalRecords = lp.BuildingGetCount();
            }
            */

            _pageCount = totalRecords / pageSize;
            if (totalRecords % pageSize > 0)
                _pageCount++;

            lblPageCount.Text = _pageCount.ToString();

            //load up the list items
            ddlPages.Items.Clear();
            if (_pageCount == 0)
            {
                ddlPages.Items.Add(new ListItem("0", "0"));
            }
            else
            {
                for (int i = 1; i <= _pageCount; i++)
                {
                    ddlPages.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
            }
            lrc.Text = totalRecords.ToString();
        }
        public void ResolvePagerView(int currentPage, bool recount)
        {
            int pageCount = 1;

            if (recount)
            {
                SetupPaging();
            }

            int.TryParse(lblPageCount.Text, out pageCount);

            if (currentPage > pageCount)
                currentPage = 1;

            int nextPage = currentPage + 1;
            int prevPage = currentPage - 1;

            btnPrev.Visible = true;
            btnNext.Visible = true;
            btnLast.Visible = true;
            btnFirst.Visible = true;


            if (currentPage >= pageCount)
            {
                btnNext.Visible = false;
                btnLast.Visible = false;
            }
            if (currentPage == 1)
            {
                btnPrev.Visible = false;
                btnFirst.Visible = false;
            }
            if (ddlPages.Items.Count == 1 && ddlPages.Items[0].Value == "0")
                currentPage = 0;
            ddlPages.SelectedValue = currentPage.ToString();
            _curPage = currentPage;
        }

        public void ResolvePagerView(int currentPage)
        {
            ResolvePagerView(currentPage, false);
            if (!currentPage.Equals(PageCount))
                lblRecentCount.Text = (currentPage * PageSize).ToString();
            else
                lblRecentCount.Text = lrc.Text;
        }

        protected void ddlPages_SelectedIndexChanged(object sender, EventArgs e)
        {
            //reload the grid
            if (_updateFunc != null)
                _updateFunc(ddlPages.SelectedIndex + 1);
            ResolvePagerView(ddlPages.SelectedIndex + 1);
        }

        protected void link_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            string pageCommand = btn.CommandArgument;
            int currentPage = ddlPages.SelectedIndex + 1;

            switch (pageCommand)
            {
                case "First":
                    currentPage = 1;
                    break;
                case "Prev":
                    currentPage--;
                    break;
                case "Next":
                    currentPage++;
                    break;
                case "Last":
                    currentPage = int.Parse(lblPageCount.Text);
                    break;
            }

            //reload the grid
            if (_updateFunc != null)
                _updateFunc(currentPage);
            ResolvePagerView(currentPage);
        }
    }
}
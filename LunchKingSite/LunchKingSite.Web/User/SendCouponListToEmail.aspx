﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="SendCouponListToEmail.aspx.cs" Inherits="LunchKingSite.Web.User.SendCouponListToEmail" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Register Src="FaqItem.ascx" TagName="FaqItem" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="../Themes/PCweb/css/ie6.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/PCweb/css/ppf-tw.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/PCweb/css/service.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    <link href="../Tools/js/css/completer.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Tools/js/completer.js"></script>
    <script type="text/javascript">
            $(document).ready(function () {
                //FAQ-M版分類展開收合 --------------------------------- START
                $(".faq").addClass("rdl-faqarrow-on"); //faq上箭頭
                $(".mail").addClass("rdl-faqarrow-on");//mail上箭頭 
                var c = 'x0';
                $(".faq a").click(function () {
                    if (c == 'x1') {
                        $(".faqArea").slideUp(500);//全部先收起來
                        $(".faqMail").slideUp(500);//全部先收起來
                        $(".faq").addClass("rdl-faqarrow-on"); //faq上箭頭
                        $(".mail").addClass("rdl-faqarrow-on"); //mail上箭頭
                        c = 'x0';
                    }
                    else {
                        $(".faqArea").slideUp(500);//全部先收起來
                        $(".faqMail").slideUp(500);//全部先收起來
                        $(".faqArea").slideDown(500);//點下去的這個打開
                        $(".faq").removeClass("rdl-faqarrow-on"); //faq下箭頭
                        $(".mail").addClass("rdl-faqarrow-on"); //mail上箭頭
                        c = 'x1';
                    }
                });
                $(".mail a").click(function () {
                    if (c == 'x2') {
                        $(".faqArea").slideUp(500);//全部先收起來
                        $(".faqMail").slideUp(500);//全部先收起來
                        $(".faq").addClass("rdl-faqarrow-on"); //faq上箭頭
                        $(".mail").addClass("rdl-faqarrow-on"); //mail上箭頭
                        c = 'x0';
                    }
                    else {
                        $(".faqArea").slideUp(500);//全部先收起來
                        $(".faqMail").slideUp(500);//全部先收起來
                        $(".faqMail").slideDown(500);//點下去的這個打開
                        $(".faq").addClass("rdl-faqarrow-on"); //faq上箭頭
                        $(".mail").removeClass("rdl-faqarrow-on"); //mail下箭頭
                        c = 'x2';
                    }
                });
                //FAQ-M版分類展開收合 --------------------------------- END
            });
    </script>
    <script type="text/javascript">
        function sendCouponListMail() {
            var userEmail = $('.userEmail').val();
            if (userEmail == '') {
                alert('請輸入E-mail!');
                return false;
            }

            if (checkAccount(userEmail)) {
                $('.errorEmailMsg').hide();
                var duringSelected = $('.rbDuring:checked').val();
                var duringString = $('.rbDuring:checked').attr('text');
                $.ajax({
                    type: "POST",
                    url: "SendCouponListToEmail.aspx/SendCouponListMail",
                    data: '{"userEmail":"' + userEmail + '","duringSelected":"' + duringSelected + '","duringString":"' + duringString + '"}',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                        success: function(data) {
                            alert(data.d.Msg);
                    }
                });
                return true;
            } else {
                $('.errorEmailMsg').show();
                return false;
            }
        }

    </script>
    <style type="text/css">
        input.rbDuring {
            width: 20px;
        }                    
                     
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <div class="fq_box">
        <!--faq-M 版分類 Start-->
        <div class="rdl-fqtitle-box rdl-faqcol-1-2">
            <ul>
                <li class="rdl-faqarrow-down faq"><a href="#">常見問題<span class="icon-chevron-faq"></span></a></li>
                <li class="rdl-faqarrow-down mail"><a href="#">其他服務<span class="icon-chevron-faq"></span></a></li>
            </ul>
        </div>
        <div class="mbe-switch mbe-switch2 rdl-faqdis">
            <!--常見問題-->
            <div class="faqArea">
                <ul>
                    <uc1:FaqItem ID="faqItemList" runat="server" />
                </ul>
                <div class="clearfix"></div>
            </div>
            <!--客服信箱-->
            <div class="faqMail">
                <ul>
                    <li><a href="../User/Service.aspx">17Life客服信箱</a></li>
                        <li><a href="../User/TravelService.aspx">康迅旅遊客服信箱</a></li>
                        <li><a href="/User/ServiceList">您的客服問答記錄</a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <!--faqArea-->
            <!--其他服務-->
            <div class="faqMail">
                <ul>
                    <li><a href="../User/SendCouponListToEmail.aspx">查詢購買紀錄</a></li>
                        <li><a href="../User/SendMemberAuthToEMail.aspx">會員認證</a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <!--faqArea-->
        </div>
        <!--faq-M 版分類 end-->

        <div class="service service_detail">
            <div class="service">
                <div class="form">
                    <h1 class="titlebg">查詢購買記錄</h1>
                    <div class="col100" id="divLoginPanel" runat="server">
                        <p>若您已是會員，建議您直接<a href='<%=ResolveUrl(FormsAuthentication.LoginUrl) %>'>登入17Life</a> ，有更詳細的資訊哦！</p>
                    </div>
                    <div>
                        <br/><h2 class="rd-payment-h2"><div class="step-num-dot">1</div> 選擇查詢期間</h2>
                        <div class="grui-form">
                            <div class="form-unit rd-pay-margin">
                                <div class="data-input">
                                    <ul class="permutation">
                                        <li>
                                            <input class="rbDuring" type="radio" name="rbGroup" value="3" text="最近三個月的紀錄" checked="checked">
                                            <p>最近三個月的紀錄</p>
                                        </li>
                                        <li>
                                            <input class="rbDuring" type="radio" name="rbGroup" value="6" text="最近六個月的紀錄">
                                            <p>最近六個月的紀錄</p>
                                        </li>
                                        <li>
                                            <input class="rbDuring" type="radio" name="rbGroup" value="-1" text="全部的記錄">
                                            <p>全部的紀錄</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <br />
                        <br />
                        <h2 class="rd-payment-h2">
                            <div class="step-num-dot">2</div>
                            輸入欲查詢的E-mail帳號</h2>
                            <div class="grui-form">
                                <div class="form-unit">
                                    <div class="data-input">
                                        <input type="text" id="userEmail" class="userEmail input-full" placeholder="輸入您的E-mail" clientidmode="Static" />
                                        <input type="button" ID="btnSend" OnClick="return sendCouponListMail();" value="查詢" class="btn btn-large btn-primary rd-payment-xlarge-btn btn-fordetail"/>
                                    </div>
                                    <div class="errorEmailMsg data-input enter-error" style="display: none">
                                        <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                        <p>請使用字母、數字及英文句點，如17life@example.com</p>
                                    </div>
                                </div>
                            </div>
                            <br />
                    </div>
                </div>

                <div class="aside">
                    <div class="fq-inlbox">
                        <div class="qtitle">
                            <a href="../Ppon/NewbieGuide.aspx">
                                <img src="../Themes/PCweb/images/FAQtitlepic_11.png" /></a>
                        </div>
                        <div class="etitle">
                            <a href="../User/Service.aspx">
                                <img src="../Themes/PCweb/images/FAQtitlepic_13.png" /></a>
                        </div>
                    </div>
                    <div class="etitle linetop">
                        <a href="/User/ServiceList">
                            <img src="../Themes/PCweb/images/FAQtitlepic_15.png" alt="" /></a>
                    </div>
                    <div class="etitle">
                        <a href="<%=SystemConfig.SiteUrl+"/Ppon/ContactUs"%>">
                            <img src="../Themes/PCweb/images/FAQtitlepic_14.png" alt="" /></a>
                    </div>
                    <div class="etitle">
                        <img src="../Themes/PCweb/images/FAQtitlepic_16.png" alt="" />
                    </div>
                    <ul>
                        <li class="on">查詢購買記錄</li>
                        <li onclick="javascript:window.location='../User/SendMemberAuthToEmail.aspx';">會員認證</li>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</asp:Content>

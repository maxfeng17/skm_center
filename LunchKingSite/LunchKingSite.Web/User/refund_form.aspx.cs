﻿using System;
using System.Web.Security;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.WebLib;
using System.Linq;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.User
{
    public partial class refund_form : LocalizedBasePage, IRefundForm
    {
        #region property
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private RefundFormPresenter _presenter;
        public RefundFormPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public string UserName
        {
            get { return string.IsNullOrEmpty(Request.QueryString["u"]) ? Page.User.Identity.Name : Request.QueryString["u"]; }
        }
        public int UserId
        {
            get { return MemberFacade.GetUniqueId(UserName); }
        }
        public Guid OrderGuid
        {
            get
            {
                Guid guid;
                return Guid.TryParse(Request.QueryString["oid"], out guid) ? guid : Guid.Empty;
            }
        }
        public DateTime Now
        {
            get { return DateTime.Now; }
        }
        #endregion
        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                if (!_presenter.OnViewInitialized())
                {
                    Response.Redirect("/ppon/default.aspx");
                    return;
                }
            _presenter.OnViewLoaded();
        }
        #endregion
        #region method
        public void SetRefundFormInfo(EinvoiceMain einvoice, CashTrustLogCollection cs, OldCashTrustLogCollection ocs, CtAtmRefund atmrefund, bool isleagl, string message)
        {
            pan_Data.Visible = pan_NoData.Visible = false;
            if (isleagl)
            {
                pan_Data.Visible = true;
                List<RefundFormListClass> rfs = new List<RefundFormListClass>();
                int i = 0;
                int mode = einvoice.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? 3 : 2;
                decimal tax = einvoice.InvoiceTax;
                bool istax = einvoice.InvoiceTax == 0.05m;
                img_Head.ImageUrl = WebUtility.GetSiteRoot() + (einvoice.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? "/Themes/default/images/17Life/G7/ReApFormTop2.png" : "/Themes/default/images/17Life/G7/ReApFormTop1.png");
                pan_Border.BorderStyle = einvoice.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? BorderStyle.Dashed : BorderStyle.None;
                bool isatm = false;
                if (cs.Count > 0)
                {
                    foreach (var item in cs)
                    {
                        if ( (einvoice.OrderIsPponitem && item.Status < ((int)TrustStatus.Refunded)) || 
                              (!einvoice.OrderIsPponitem && item.Status != ((int)TrustStatus.Verified) && item.Status != ((int)TrustStatus.Refunded))  )
                        {
                            if (item.Atm > 0)
                                isatm = true;
                            decimal salesamount = (Math.Round(((item.CreditCard + item.Atm) / (1 + tax)), 0));
                            rfs.Add(new RefundFormListClass(i, einvoice.OrderId, string.IsNullOrEmpty(item.CouponSequenceNumber) ? string.Empty : ("憑證編號#" + item.CouponSequenceNumber),
                                mode, einvoice.InvoiceNumberTime == null ? einvoice.OrderTime : einvoice.InvoiceNumberTime.Value, einvoice.InvoiceNumberTime == null,
                                string.IsNullOrEmpty(einvoice.InvoiceNumber) ? string.Empty.PadLeft(10, ' ') : einvoice.InvoiceNumber,
                                Helper.IsFlagSet(item.SpecialStatus, TrustSpecialStatus.Freight) ? "運費" : ((!einvoice.OrderIsPponitem && einvoice.InvoiceTax != 0) ? "購物金" : NoComma(einvoice.OrderItem)), item.CreditCard + item.Atm, (int)salesamount, (item.CreditCard + item.Atm) - (int)salesamount, istax, einvoice.OrderIsPponitem));
                            i++;
                            rfs.Add(new RefundFormListClass(i, einvoice.OrderId, string.IsNullOrEmpty(item.CouponSequenceNumber) ? string.Empty : ("憑證編號#" + item.CouponSequenceNumber),
                                mode, einvoice.InvoiceNumberTime == null ? einvoice.OrderTime : einvoice.InvoiceNumberTime.Value, einvoice.InvoiceNumberTime == null,
                                string.IsNullOrEmpty(einvoice.InvoiceNumber) ? string.Empty.PadLeft(10, ' ') : einvoice.InvoiceNumber,
                                Helper.IsFlagSet(item.SpecialStatus, TrustSpecialStatus.Freight) ? "運費" : ((!einvoice.OrderIsPponitem && einvoice.InvoiceTax != 0) ? "購物金" : NoComma(einvoice.OrderItem)), (item.CreditCard + item.Atm), (int)salesamount, (item.CreditCard + item.Atm) - (int)salesamount, istax, einvoice.OrderIsPponitem) { breakstring = "<br/><div style=\"page-break-after: always\"></div><br/><br/>" });
                            i++;
                        }
                    }
                }
                else if (ocs.Count > 0)
                {
                    int price = (int)einvoice.OrderAmount / ocs.Count;
                    foreach (var item in ocs)
                    {
                        if (item.Status < ((int)TrustStatus.Verified))
                        {
                            decimal salesamount = (Math.Round(((price) / (1 + tax)), 0));
                            rfs.Add(new RefundFormListClass(i, einvoice.OrderId, string.IsNullOrEmpty(item.SequenceNumber) ? string.Empty : ("憑證編號#" + item.SequenceNumber),
                                mode, einvoice.InvoiceNumberTime == null ? einvoice.OrderTime : einvoice.InvoiceNumberTime.Value, einvoice.InvoiceNumberTime == null,
                                string.IsNullOrEmpty(einvoice.InvoiceNumber) ? string.Empty.PadLeft(10, ' ') : einvoice.InvoiceNumber,
                               ((!einvoice.OrderIsPponitem && einvoice.InvoiceTax != 0) ? "購物金" : NoComma(einvoice.OrderItem)), price, (int)salesamount, (price) - (int)salesamount, istax, einvoice.OrderIsPponitem));
                            i++;
                            rfs.Add(new RefundFormListClass(i, einvoice.OrderId, string.IsNullOrEmpty(item.SequenceNumber) ? string.Empty : ("憑證編號#" + item.SequenceNumber),
                                mode, einvoice.InvoiceNumberTime == null ? einvoice.OrderTime : einvoice.InvoiceNumberTime.Value, einvoice.InvoiceNumberTime == null,
                                string.IsNullOrEmpty(einvoice.InvoiceNumber) ? string.Empty.PadLeft(10, ' ') : einvoice.InvoiceNumber,
                                ((!einvoice.OrderIsPponitem && einvoice.InvoiceTax != 0) ? "購物金" : NoComma(einvoice.OrderItem)), (price), (int)salesamount, (price) - (int)salesamount, istax, einvoice.OrderIsPponitem) { breakstring = "<br/><div style=\"page-break-after: always\"></div><br/><br/>" });
                            i++;
                        }
                    }
                }
                if (rfs.Count > 0)
                    rfs.Last().breakstring = string.Empty;
                pan_ATM.Visible = isatm;
                if (isatm && atmrefund.Si != 0)
                {
                    lab_ATM_AccountName.Text = atmrefund.AccountName;
                    lab_ATM_UserID.Text = atmrefund.Id;
                    lab_ATM_BankName.Text = atmrefund.BankName;
                    lab_ATM_BranchName.Text = atmrefund.BranchName;
                    lab_ATM_Account.Text = atmrefund.AccountNumber;
                    lab_ATM_Phone.Text = atmrefund.Phone;
                }
                rpt_Form.DataSource = rfs;
                rpt_Form.DataBind();
            }
            else
            {
                lab_NoData.Text = message;
                pan_NoData.Visible = true;
            }
        }

        public static string NoComma(string input, string comma = ",")
        {
            return string.IsNullOrEmpty(input) ? string.Empty : input.Replace(comma, string.Empty);
        }
        #endregion
    }
}
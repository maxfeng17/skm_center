﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace LunchKingSite.Web.User
{
    public partial class UserRefund : LocalizedBasePage, IUserRefund
    {
        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected ISysConfProvider confProv = ProviderFactory.Instance().GetConfig();

        #region Property

        private UserRefundPresenter _presenter;

        public UserRefundPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string CustomerSiteService
        {
            get
            {
                return confProv.SSLSiteUrl + confProv.CustomerSiteService;
            }
        }

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        public int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(UserName, true);
            }
        }

        public string PanelMode
        {
            get
            {
                return hif_PamelMode.Value;
            }
            set
            {
                PanelChange(value);
            }
        }

        public bool IsCoupon
        {
            get
            {
                return cbx_IsCoupon.Checked;
            }
            set
            {
                cbx_IsCoupon.Checked = value;
            }
        }

        public Guid OrderGuid
        {
            get
            {
                Guid oid;
                return Guid.TryParse(hif_OrderGuid.Value, out oid) ? oid : Guid.Empty;
            }
            set
            {
                hif_OrderGuid.Value = value.ToString();
            }
        }

        public string ReceiverName
        {
            get
            {
                return hif_ReceiverName.Value;
            }
        }
        public string ReceiverAddress
        {
            get
            {
                return hif_ReceiverAddress.Value;
            }
        }

        public string OrderID
        {
            get
            {
                return hif_OrderID.Value;
            }
        }

        private int OrderStatus
        {
            get
            {
                int orderstatus;
                return int.TryParse(hif_OrderStatus.Value, out orderstatus) ? orderstatus : 0;
            }
        }

        public DateTime OrderCreateTime
        {
            get
            {
                DateTime dt = new DateTime();
                return DateTime.TryParse(hif_OrderCreateTime.Value, out dt) ? dt : dt;
            }
            set
            {
                hif_OrderCreateTime.Value = value.ToShortDateString();
            }
        }

        public bool IsRefundCashOnly
        {
            get
            {
                return (ViewState["isRefundCashOnly"] != null) ? (bool)ViewState["isRefundCashOnly"] : false;
            }
            set
            {
                ViewState["isRefundCashOnly"] = value;
            }
        }

        private bool IsEntrustSell
        {
            get
            {
                bool result;
                return bool.TryParse(hif_IsEntrustSell.Value, out result) ? result : false;
            }
        }

        /// <summary>
        /// 是否使用現金購買:刷卡或ATM
        /// </summary>
        public bool IsPaidByCash
        {
            get
            {
                return (ViewState["isPaidByCash"] != null) ? (bool)ViewState["isPaidByCash"] : false;
            }
            set
            {
                ViewState["isPaidByCash"] = value;
            }
        }

        private bool IsPartialCancel
        {
            get
            {
                bool result;
                return bool.TryParse(hif_IsPartialCancel.Value, out result) ? result : false;
            }
        }

        private bool IsInstallment
        {
            get
            {
                bool result;
                return bool.TryParse(hif_IsInstallment.Value, out result) ? result : false;
            }
        }

        private bool IsIspPay
        {
            get
            {
                bool result;
                return bool.TryParse(hif_IsIsp.Value, out result) ? result : false;
            }
        }

        public TextBox TxtOrderId
        {
            get { return tbx_OrderID; }
        }

        public TextBox TxtAccountId
        {
            get { return tbx_ATMAccount; }
        }

        public TextBox TxtMobile
        {
            get { return tbx_ATMMobile; }
        }

        public bool isGroupCoupon { get { return bool.Parse(hif_IsGroupCoupon.Value); } }

        public string alertMessage
        {
            set { AjaxControlToolkit.ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), string.Empty, "alert('" + value + "')", true); }
        }

        public bool IsWms
        {
            get
            {
                bool isWms = false;
                bool.TryParse(hif_IsWms.Value, out isWms);
                return isWms;
            }
            set
            {
                hif_IsWms.Value = value.ToString();
            }
        }

        #endregion Property

        #region event

        public event EventHandler<DataEventArgs<string>> GetOrderInfo = null;

        public event EventHandler<DataEventArgs<KeyValuePair<OrderStatusLog, RefundType>>> RequestRefund = null;

        public event EventHandler<DataEventArgs<KeyValuePair<CtAtmRefund, KeyValuePair<OrderStatusLog, RefundType>>>> RequestRefundATM = null;

        public event EventHandler<DataEventArgs<string>> CancelNotCompleteOrder = null;

        public event EventHandler<DataEventArgs<string>> GetATMBranch = null;

        #endregion event

        #region method

        public void SetOrderInfo(RefundOrderInfo orderinfo, ViewOrderShipList osInfo)
        {
            lab_OrderStatusMessage.Text = string.Empty;
            DateTime now = DateTime.Now;
            int slug;

            if (orderinfo.Coupons.Count == 0)
            {
                lab_OrderStatusMessage.Text = "查無此訂單。";
            }
            else
            {
                #region 頁面填值

                ViewCouponListMain main = orderinfo.Coupons.First();
                hif_OrderGuid.Value = main.Guid.ToString();
                hif_OrderID.Value = main.OrderId;
                hif_OrderStatus.Value = main.OrderStatus.ToString();
                hif_IsEntrustSell.Value = orderinfo.IsEntrustSell.ToString();
                hif_IsThridPartyPay.Value = orderinfo.IsThridPartyPay.ToString();
                hif_ThirdPartyPaymentSystem.Value = Convert.ToString((byte) orderinfo.ThirdPartyPaymentSystem);
                hif_IsPartialCancel.Value = orderinfo.HasPartialCancelOrVerified.ToString();
                hif_IsInstallment.Value = orderinfo.IsInstallment.ToString();
                hif_IsIsp.Value = orderinfo.IsIspPay.ToString();
                //用來判斷退貨商品是否為憑證或宅配
                bool isPpon = main.DeliveryType.Value == (int)DeliveryType.ToShop;
                hif_IsGroupCoupon.Value = Helper.IsFlagSet(main.BusinessHourStatus, BusinessHourStatus.GroupCoupon).ToString();
                cbx_IsCoupon.Checked = isPpon;
                IsWms = main.IsWms;

                EinvoiceMainCollection em = op.EinvoiceMainGetListByOrderGuid(main.Guid);
                if (em.Any(x => x.InvoiceMode2 == (int)InvoiceMode2.Duplicate && x.OrderTime >= confProv.EinvAllowanceStartDate))
                {
                    pan_IsPaperAllowance1.Visible = pan_IsPaperAllowance2.Visible = pan_IsPaperAllowance3.Visible = pan_IsPaperAllowance4.Visible = false;
                }
                else
                {
                    pan_IsPaperAllowance1.Visible = pan_IsPaperAllowance2.Visible = pan_IsPaperAllowance3.Visible = pan_IsPaperAllowance4.Visible = em.Count > 0 && em.Any(x => x.InvoicePapered);
                }
                #endregion

                OrderCreateTime = main.CreateTime;
                bool gonext = true;
                bool isCancelOrder = false;

                bool isRefundScashToCash = false;
                string failReson = "";
                //新版退貨判斷
                if (orderinfo.HasProcessingReturnForm)
                {
                    //全數退貨中
                    gonext = false;
                    lab_OrderStatusMessage.Text = "已申請退貨，無法再申請。";
                }
                else if (!isPpon && orderinfo.IsExchangeProcessing)
                {
                    gonext = false;
                    lab_OrderStatusMessage.Text = "您已申請換貨處理中，若有需要協助，請洽客服人員。";
                }
                else if (OrderFacade.CanApplyRefund(OrderGuid, UserName, false, out failReson))
                {
                    //可申請購物金轉刷退
                    isRefundScashToCash = true;
                }
                else if (!orderinfo.IsReturnable)
                {
                    //全數退貨完成
                    gonext = false;
                    lab_OrderStatusMessage.Text = "訂單已全數退貨，無法再申請。";
                }
                else if (main.BusinessHourOrderTimeE <= now && int.TryParse(main.Slug, out slug) &&
                         slug < main.BusinessHourOrderMinimum)
                {
                    //已結檔且未達門檻
                    lab_OrderStatusMessage.Text = "未達門檻不需退貨。";
                    if ((main.OrderStatus & (int) Core.OrderStatus.ATMOrder) > 0 &&
                        (main.OrderStatus & (int) Core.OrderStatus.Complete) > 0) //ATM已付款，未達門檻顯示退款
                    {
                        lab_OrderStatusMessage.Text =
                            @"此好康未達門檻，請至<a href='coupon_list.aspx' style='color:blue;'>【會員專區】</a>進行指定帳戶退款";
                    }
                }
                else if (orderinfo.IsInvoice2To3)
                {
                    gonext = false;
                    lab_OrderStatusMessage.Text =
                            @"發票二聯改三聯處理中，無法申請退貨，若有需要協助，請洽客服人員。";
                }
                else
                {
                    if (isPpon)
                    {
                        //憑證
                        if (main.TotalCount != 0)
                        {
                            gonext = (main.RemainCount > 0);
                            lab_OrderStatusMessage.Text = ((main.RemainCount == 0)
                                                               ? " 憑證已使用完畢，無法退貨。"
                                                               : ("此好康未使用:" + main.RemainCount));
                        }
                    }
                    else
                    {
                        if (osInfo != null)
                        {
                            txtReceiverName.Text = osInfo.MemberName;
                            txtReceiverAddress.Text = osInfo.DeliveryAddress;
                        }

                        //商品檔鑑賞期檢查
                        if (PponOrderManager.CheckIsOverTrialPeriod(main.Guid))
                        {
                            gonext = false;
                            lab_OrderStatusMessage.Text = "商品已過鑑賞期，無法退貨。";
                        }
                    }
                }
                hif_IsRefundScashToCash.Value = isRefundScashToCash.ToString();

                #region ATM 檢核

                if ((main.OrderStatus & (int)Core.OrderStatus.Complete) == 0)
                {
                    if ((main.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0)
                    {
                        gonext = false;
                        lab_OrderStatusMessage.Text = main.CreateTime.Date.Equals(DateTime.Now.Date)
                                                        ? "尚未完成ATM付款，無需退貨。"
                                                        : "ATM 逾期未付款，無法退貨。";
                    }
                    else if (Helper.IsFlagSet(main.OrderStatus, (int)Core.OrderStatus.FamilyIspOrder) || 
                        Helper.IsFlagSet(main.OrderStatus, (int)Core.OrderStatus.SevenIspOrder))
                    {
                        if (!confProv.EnableCancelPaidByIspOrder)
                        {
                            gonext = false;
                            lab_OrderStatusMessage.Text = "未完成付款的訂單，無需退貨。";
                        }
                        else
                        {
                            if (Helper.IsFlagSet(main.OrderStatus, (int) Core.OrderStatus.Cancel))
                            {
                                gonext = false;
                                lab_OrderStatusMessage.Text = "已取消的訂單。";
                            }
                            else if (main.IsCanceling)
                            {
                                //todo:後台是否要做一個取消 取消中訂單 的功能
                                gonext = false;
                                lab_OrderStatusMessage.Text = "訂單已在取消中，請耐心等候。";
                            }
                            else
                            {
                                isCancelOrder = true;
                            }
                        }
                    }
                    else
                    {
                        lab_OrderStatusMessage.Text = "未完成付款的訂單。";
                    }
                }

                #endregion

                #region 不得退貨條件

                string noRefundString = "此訂單不接受退貨申請。";

                //不接受退貨
                if ((main.Status & (int)GroupOrderStatus.NoRefund) > 0)
                {
                    gonext = false;
                    lab_OrderStatusMessage.Text = noRefundString;
                }

                //結檔後七天不接受退貨
                if ((main.Status & (int)GroupOrderStatus.DaysNoRefund) > 0 && main.BusinessHourOrderTimeE.AddDays(confProv.ProductRefundDays) < DateTime.Now)
                {
                    gonext = false;
                    lab_OrderStatusMessage.Text = noRefundString;
                }
                
                //過期不接受退貨
                if ((main.Status & (int)GroupOrderStatus.ExpireNoRefund) > 0)
                {
                    if (main.BusinessHourDeliverTimeE != null && main.BusinessHourDeliverTimeE.Value.AddDays(1) < now)
                    {
                        gonext = false;
                        lab_OrderStatusMessage.Text = noRefundString;
                    }
                }

                //演出時間前十日過後不能退貨
                if ((main.Status & (int)GroupOrderStatus.NoRefundBeforeDays) > 0 && now.Date >= ((DateTime)main.BusinessHourDeliverTimeS).AddDays(-confProv.PponNoRefundBeforeDays).Date)
                {
                    gonext = false;
                    lab_OrderStatusMessage.Text = noRefundString;
                }

                #endregion

                #region 零元檔，全家檔，捐款檔，公益檔

                //0元好康
                if (main.ItemPrice.Equals(0) || (main.Status & (int)GroupOrderStatus.KindDeal) > 0)
                {
                    gonext = false;
                    lab_OrderStatusMessage.Text = noRefundString;
                }

                if (main.ItemPrice > 0 && (main.Status & (int)GroupOrderStatus.FamiDeal) > 0 && (main.BusinessHourStatus & (int)BusinessHourStatus.GroupCoupon) > 0)
                {
                    pan_RefundCash.Visible = true;
                    var refundamount = UserRefundFacade.GetOrderRefundAmount(UserId, OrderGuid);
                    lbReufndCash.Text = refundamount == null ? string.Empty : refundamount.ToString();
                }
                #endregion

                #region 預約訂單、2011/4/1前訂單

                //4月1號以前
                if (main.BusinessHourOrderTimeS < new DateTime(2011, 4, 1))
                {
                    lab_OrderStatusMessage.Text = "此好康不提供退貨服務。";
                    gonext = false;
                }

                if (gonext && orderinfo.IsReservationLock)
                {
                    lab_OrderStatusMessage.Text = "訂單已預約，無法申請退貨。";
                    gonext = false;
                }

                #endregion

                if (gonext)
                {
                    if (isPpon)
                    {
                        if (isRefundScashToCash)
                        {
                            PanelChange("RefundScashToCash");
                        }
                        else
                        {
                            PanelChange("ReturnReasonInput");
                        }
                    }
                    else
                    {
                        if (isCancelOrder)
                        {
                            PanelChange("CancelOrder");
                        }
                        else
                        {
                            if (isRefundScashToCash)
                            {
                                PanelChange("RefundScashToCash");
                            }
                            else
                            {
                                PanelChange("ReturnReasonInput_ToHouseOrder");
                            }
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(lab_OrderStatusMessage.Text))
            {
                ScriptManager.RegisterStartupScript(this, typeof(Button), "refundfail", "errormessagebg();", true);
            }
        }

        public void ReturnPanel(string panelmode)
        {
            PanelChange(panelmode);
        }

        public void SetATMBranch(BankInfoCollection bks)
        {
            ddl_ATMBankBranch.DataSource = bks;
            ddl_ATMBankBranch.DataBind();
        }

        public void SetBankInfo(BankInfoCollection bks)
        {
            ddl_ATMBankName.DataSource = bks;
            ddl_ATMBankName.DataBind();
        }

        #endregion method

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }

            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
                string orderid = Request.QueryString["orderid"];
                tbx_OrderID.Text = orderid;
            }
            _presenter.OnViewLoaded();

        }

        //1.1
        protected void SerachOrderInfo(object sender, EventArgs e)
        {
            ClearHidden(hif_OrderGuid, hif_OrderID, hif_OrderStatus, hif_PamelMode, hif_Reason);
            if (GetOrderInfo != null)
            {
                GetOrderInfo(sender, new DataEventArgs<string>(tbx_OrderID.Text.Trim()));
            }
        }


        protected void GetRefundReason(object sender, EventArgs e)
        {
            hif_ReceiverName.Value = txtReceiverName.Text;
            hif_ReceiverAddress.Value = txtReceiverAddress.Text;

            //增加代收轉付的判斷
            if (this.IsEntrustSell) // 代收轉付
            {
                if (IsCoupon)
                {
                    PanelChange("RefundCashConfirm_ToShopOrder");
                }
                else
                {
                    PanelChange("RefundCashConfirm_ToHouseOrder");
                }
            }
            else if (IsRefundCashOnly)
            {
                ChangePanelForCashRefund();
            }
            else
            {
                if (IsCoupon)
                {
                    PanelChange("RefundCashPointConfirm_ToShopOrder");
                }
                else
                {
                    PanelChange("RefundCashPointConfirm_ToHouseOrder");
                }
            }
        }

        //退購物金
        protected void ConfirmRefundCashPoint(object sender, EventArgs e)
        {
            OrderStatusLog log = new OrderStatusLog();
            log.OrderGuid = OrderGuid;
            log.Status = 0;
            log.CreateId = UserName;
            log.CreateTime = DateTime.Now;
            log.Reason = hif_Reason.Value;
            log.ReceiverName = ReceiverName;
            log.ReceiverAddress = ReceiverAddress;
            if (hif_IsReceive.Value != "")
            {
                log.IsReceive = Convert.ToBoolean(hif_IsReceive.Value);
            }

            KeyValuePair<OrderStatusLog, RefundType> kp = new KeyValuePair<OrderStatusLog, RefundType>(log, RefundType.Scash);
            if (RequestRefund != null)
            {
                this.RequestRefund(this, new DataEventArgs<KeyValuePair<OrderStatusLog, RefundType>>(kp));
            }
        }

        //選刷退
        protected void ChooseRefundCash(object sender, EventArgs e)
        {
            ChangePanelForCashRefund();
        }

        private void ChangePanelForCashRefund()
        {
            //若為 1. ATM單 2.超過360天前以致無法刷退的訂單 3.使用購物金 4.LinePay超過60天,則導到指定帳戶退款頁面以利填寫帳號 5.TaishinPay退貨api未接上之前, 一律以ATM退
            if ((OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0 ||
                 (ThirdPartyPayment)int.Parse(hif_ThirdPartyPaymentSystem.Value) == ThirdPartyPayment.TaishinPay ||
                 ((ThirdPartyPayment)int.Parse(hif_ThirdPartyPaymentSystem.Value) == ThirdPartyPayment.LinePay
                    && OrderCreateTime < DateTime.Now.AddDays(-confProv.LinePayRefundPeriodLimit)) ||
                 OrderCreateTime < DateTime.Now.AddDays(-confProv.NoneCreditRefundPeriod) ||
                 (IsPartialCancel && IsInstallment) //部分退且付款方式為分期 
                 || IsIspPay
                )     
            {
                if (IsCoupon)
                {
                    PanelChange("RefundATMConfirm_ToShopOrder");
                }
                else
                {
                    PanelChange("RefundATMConfirm_ToHouseOrder");
                }
            }
            else
            {
                if (IsCoupon)
                {
                    PanelChange("RefundCashConfirm_ToShopOrder");
                }
                else
                {
                    PanelChange("RefundCashConfirm_ToHouseOrder");
                }
            }
        }

        //刷退
        protected void ConfirmRefundCash(object sender, EventArgs e)
        {
            OrderStatusLog log = new OrderStatusLog();
            log.OrderGuid = OrderGuid;
            log.Status = 0;
            log.CreateId = UserName;
            log.CreateTime = DateTime.Now;
            log.Reason = hif_Reason.Value;
            log.ReceiverName = ReceiverName;
            log.ReceiverAddress = ReceiverAddress;
            if (hif_IsReceive.Value != "")
            {
                log.IsReceive = Convert.ToBoolean(hif_IsReceive.Value);
            }

            var rt = RefundType.Cash;
            if(bool.Parse(hif_IsThridPartyPay.Value))
            {
                rt = RefundType.Tcash;
            }

            KeyValuePair<OrderStatusLog, RefundType> kp = new KeyValuePair<OrderStatusLog, RefundType>(log, rt);
            if (RequestRefund != null)
            {
                this.RequestRefund(this, new DataEventArgs<KeyValuePair<OrderStatusLog, RefundType>>(kp));
            }
        }

        //ATM退款
        protected void ChooseRefundCashATM(object sender, EventArgs e)
        {
            PanelChange("RefundAccountInput");
        }

        //確認帳戶
        protected void CheckRefundCashATMInfo(object sender, EventArgs e)
        {
            if (RegExRules.CheckPersonIdOrCompanyNo(tbx_ATMUserID.Text) != string.Empty)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), string.Empty, "alert('請輸入正確的身份證字號');", true);
                return;
            }

            if (tbx_ATMAccount.Text.Trim().Length > 14 || !tbx_ATMAccount.Text.Trim().All(char.IsDigit))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), string.Empty, "alert('請輸入正確的ATM帳號(應小於14碼且不須輸入分隔符號)');", true);
                return;
            }

            lab_ATMAccountName.Text = tbx_ATMAccountName.Text;
            lab_ATMUserID.Text = tbx_ATMUserID.Text;
            lab_ATMBankName.Text = ddl_ATMBankName.SelectedItem.Text;
            lab_ATMBankBranch.Text = ddl_ATMBankBranch.SelectedItem.Text;
            lab_ATMAccount.Text = tbx_ATMAccount.Text.Trim();
            lab_ATMMobile.Text = tbx_ATMMobile.Text;
            PanelChange("RefundAccountConfirm");
        }

        //修改ATM匯款資料
        protected void ModifyRefundATMInfo(object sender, EventArgs e)
        {
            PanelChange("RefundAccountInput");
        }

        //確認ATM退款
        protected void ConfirmRefundCashATM(object sender, EventArgs e)
        {
            #region set ATM refund info

            CtAtmRefund ct = new CtAtmRefund();
            ct.UserId = UserId;
            ct.OrderGuid = OrderGuid;
            ct.AccountName = tbx_ATMAccountName.Text;
            ct.Id = tbx_ATMUserID.Text;
            ct.BankName = ddl_ATMBankName.SelectedItem.Text;
            ct.BranchName = ddl_ATMBankBranch.SelectedItem.Text;
            ct.BankCode = ddl_ATMBankName.SelectedValue + ddl_ATMBankBranch.SelectedValue;
            ct.AccountNumber = tbx_ATMAccount.Text.Trim().PadLeft(14, '0');
            ct.Phone = tbx_ATMMobile.Text;
            ct.CreateTime = DateTime.Now;
            ct.Status = (int)AtmRefundStatus.Initial;

            #endregion set ATM refund info

            OrderStatusLog log = new OrderStatusLog();
            log.OrderGuid = OrderGuid;
            log.Status = 0;
            log.CreateId = UserName;
            log.CreateTime = DateTime.Now;
            log.Reason = hif_Reason.Value;
            log.ReceiverName = ReceiverName;
            log.ReceiverAddress = ReceiverAddress;
            if (hif_IsReceive.Value != "")
            {
                log.IsReceive = Convert.ToBoolean(hif_IsReceive.Value);
            }
            

            KeyValuePair<OrderStatusLog, RefundType> kp = new KeyValuePair<OrderStatusLog, RefundType>(log, RefundType.Atm);
            KeyValuePair<CtAtmRefund, KeyValuePair<OrderStatusLog, RefundType>> kpp = new KeyValuePair<CtAtmRefund, KeyValuePair<OrderStatusLog, RefundType>>(ct, kp);
            if (RequestRefundATM != null)
            {
                RequestRefundATM(sender, new DataEventArgs<KeyValuePair<CtAtmRefund, KeyValuePair<OrderStatusLog, RefundType>>>(kpp));
            }
        }

        protected void ConfirmCancelOrder(object sender, EventArgs e)
        {
            CancelNotCompleteOrder(sender, new DataEventArgs<string>(""));
        }

        protected void ChangeATMBranch(object sender, EventArgs e)
        {
            if (GetATMBranch != null)
            {
                GetATMBranch(sender, new DataEventArgs<string>(ddl_ATMBankName.SelectedValue));
            }
        }

        protected void PanelChange(string panelmode)
        {
            foreach (var item in pan_Main.Controls)
            {
                if (item is Panel)
                {
                    ((Panel)item).Visible = false;
                }
            }

            switch (panelmode)
            {
                case "ReturnOrderIdInput":
                    pan_ReturnOrderIdInput.Visible = true;
                    break;

                case "ReturnReasonInput":
                    ClearText(pan_ReturnOrderIdInput);
                    hif_Reason.Value = string.Empty;
                    pan_ReturnReasonInput.Visible = true;
                    pan_ReturnReasonInputReason.Controls.Clear();
                    foreach (var item in UserRefundFacade.RefundReasonForOther(true))
                    {
                        if (!item.Value)
                        {
                            HtmlGenericControl li = new HtmlGenericControl("li");
                            li.InnerHtml = string.Format("<li onclick=\"this.getElementsByTagName('input')[0].checked=true\"><input name='reason' type='radio' value='{0}' />{0}</li>", item.Key);
                            pan_ReturnReasonInputReason.Controls.Add(li);
                        }
                    }
                    break;

                case "ReturnReasonInput_ToHouseOrder":
                    PanelChange("ReturnReasonInput");
                    pan_ReturnReasonInputReason.Controls.Clear();
                    foreach (var item in UserRefundFacade.RefundReasonForOther(false))
                    {
                        if (!item.Value)
                        {
                            HtmlGenericControl li = new HtmlGenericControl("li");
                            li.InnerHtml = string.Format("<li onclick=\"this.getElementsByTagName('input')[0].checked=true\"><input name='reason' type='radio' value='{0}' />{0}</li>", item.Key);
                            pan_ReturnReasonInputReason.Controls.Add(li);
                        }
                    }
                    break;

                #region 退購物金 申請確認

                case "RefundCashPointConfirm":
                    ClearText(pan_ReturnReasonInput);
                    lbt_RefundCash.Visible = IsPaidByCash;
                    pan_RefundCashPointConfirm.Visible = true;
                    break;

                case "RefundCashPointConfirm_ToShopOrder":
                    PanelChange("RefundCashPointConfirm");
                    pan_RefundCashPointConfirm_ToShopOrder.Visible = true;
                    liGroupCoupon.Visible = isGroupCoupon;
                    break;

                case "RefundCashPointConfirm_ToHouseOrder":
                    PanelChange("RefundCashPointConfirm");
                    pan_RefundCashPointConfirm_ToHouseOrder.Visible = true;
                    break;

                #endregion 退購物金 申請確認

                #region 刷退 申請確認

                case "RefundCashConfirm":
                    pan_RefundCashConfirm.Visible = true;
                    break;

                case "RefundScashToCash":
                    ChangePanelForCashRefund();
                    break;

                case "RefundCashConfirm_ToShopOrder":
                    PanelChange("RefundCashConfirm");
                    break;

                case "RefundCashConfirm_ToHouseOrder":
                    PanelChange("RefundCashConfirm");
                    break;

                #endregion 刷退 申請確認

                #region 退ATM 申請確認

                case "RefundATMConfirm":
                    ClearText(pan_RefundAccountInput);
                    ClearText(pan_RefundAccountConfirm);
                    pan_RefundATMConfirm.Visible = true;
                    break;

                case "RefundATMConfirm_ToShopOrder":
                    PanelChange("RefundATMConfirm");
                    break;

                case "RefundATMConfirm_ToHouseOrder":
                    PanelChange("RefundATMConfirm");
                    break;

                #endregion 退ATM 申請確認

                case "RefundAccountInput":
                    pan_RefundAccountInput.Visible = true;
                    break;

                case "RefundAccountConfirm":
                    pan_RefundAccountConfirm.Visible = true;
                    break;

                #region 退購物金 申請成功

                case "RefundCashPointSuccess":
                    ClearHidden(hif_OrderID, hif_OrderStatus, hif_PamelMode, hif_Reason);
                    pan_RefundCashPointSuccess.Visible = true;
                    break;

                case "RefundCashPointSuccess_BeforeUseStartTime":
                    PanelChange("RefundCashPointSuccess");
                    pan_RefundCashPointSuccess_BeforeUseStartTime.Visible = true;
                    break;

                case "RefundCashPointSuccess_AfterUseStartTime":
                    PanelChange("RefundCashPointSuccess");
                    pan_RefundCashPointSuccess_AfterUseStartTime.Visible = true;
                    break;

                case "RefundCashPointSuccess_AfterUseStartTime_ToHouseOrder":
                    PanelChange("RefundCashPointSuccess_AfterUseStartTime");
                    pan_RefundCashPointSuccess_AfterUseStartTime_ToHouseOrder.Visible = true;
                    pan_RefundCashPointSuccess_AfterUseStartTime_ToShopOrder.Visible = false;
                    break;

                #endregion 退購物金 申請成功

                #region 刷退 或 退ATM 申請成功

                case "RefundCashATMSuccess":
                    pan_RefundCashATMSuccess.Visible = true;
                    break;

                case "RefundCashATMSuccess_ToShopOrder":
                    PanelChange("RefundCashATMSuccess");
                    pan_RefundCashATMSuccess_ToShopOrder.Visible = true;
                    break;

                case "RefundCashSuccess_ToShopOrder":
                    PanelChange("RefundCashATMSuccess_ToShopOrder");
                    if ((ThirdPartyPayment) int.Parse(hif_ThirdPartyPaymentSystem.Value) == ThirdPartyPayment.None)
                    {
                        lt_Cash.Visible = true;
                    }
                    else if ((ThirdPartyPayment)int.Parse(hif_ThirdPartyPaymentSystem.Value) == ThirdPartyPayment.TaishinPay)
                    {
                        lt_TcashTaishinPay.Text = string.Format(lt_TcashTaishinPay.Text,
                            Helper.GetEnumDescription((ThirdPartyPayment)int.Parse(hif_ThirdPartyPaymentSystem.Value)));
                        lt_TcashTaishinPay.Visible = true;
                    }
                    else
                    {
                        lt_Tcash.Text = string.Format(lt_Tcash.Text,
                            Helper.GetEnumDescription((ThirdPartyPayment)int.Parse(hif_ThirdPartyPaymentSystem.Value)));
                        lt_Tcash.Visible = true;
                    }
                    break;

                case "RefundATMSuccess_ToShopOrder":
                    PanelChange("RefundCashATMSuccess_ToShopOrder");
                    lt_ATM.Visible = true;
                    break;
                case "RefundCashATMSuccess_ToHouseOrder":
                    PanelChange("RefundCashATMSuccess");
                    pan_RefundCashATMSuccess_ToHouseOrder.Visible = true;
                    break;

                case "RefundCashSuccess_ToHouseOrder":
                    PanelChange("RefundCashATMSuccess_ToHouseOrder");
                    if ((ThirdPartyPayment)int.Parse(hif_ThirdPartyPaymentSystem.Value) == ThirdPartyPayment.None)
                    {
                        lt_Cash.Visible = true;
                    }
                    else if ((ThirdPartyPayment)int.Parse(hif_ThirdPartyPaymentSystem.Value) == ThirdPartyPayment.TaishinPay)
                    {
                        lt_TcashTaishinPay.Text = string.Format(lt_TcashTaishinPay.Text,
                            Helper.GetEnumDescription((ThirdPartyPayment)int.Parse(hif_ThirdPartyPaymentSystem.Value)));
                        lt_TcashTaishinPay.Visible = true;
                    }
                    else
                    {
                        lt_Tcash.Text = string.Format(lt_Tcash.Text,
                        Helper.GetEnumDescription((ThirdPartyPayment)int.Parse(hif_ThirdPartyPaymentSystem.Value)));
                        lt_Tcash.Visible = true;
                    }
                    break;

                case "RefundATMSuccess_ToHouseOrder":
                    PanelChange("RefundCashATMSuccess_ToHouseOrder");
                    lt_ATM.Visible = true;
                    break;

                #endregion 刷退 或 退ATM 申請成功

                case "RefundFail":
                    pan_RefundFail.Visible = true;
                    break;
                #region 取消訂單
                case "CancelOrder":
                    pan_CancelConfirm.Visible = true;
                    break;
                case "CancelComplete":
                    pan_CancelSuccess.Visible = true;
                    break;
                #endregion
            }
        }

        protected void ClearText(Panel panel)
        {
            foreach (var item in panel.Controls)
            {
                if (item is TextBox)
                {
                    ((TextBox)item).Text = string.Empty;
                }
                else if (item is Label)
                {
                    ((Label)item).Text = string.Empty;
                }
            }
        }

        protected void ClearText(params Label[] items)
        {
            foreach (var item in items)
            {
                item.Text = string.Empty;
            }
        }

        protected void ClearHidden(params HiddenField[] items)
        {
            foreach (var item in items)
            {
                item.Value = string.Empty;
            }
        }
       
        #endregion page
    }
}
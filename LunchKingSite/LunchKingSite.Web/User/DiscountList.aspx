﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="DiscountList.aspx.cs" Inherits="LunchKingSite.Web.User.DiscountList" %>

<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="uc1" %>
<%@ Register Src="Pager.ascx" TagName="Pager" TagPrefix="uc2" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<asp:Content ID="Ssc" ContentPlaceHolderID="SSC" runat="server">
    <link href="../Themes/default/images/17Life/G2/PPB.css" rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-L.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-M.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-S.css") %>' rel="stylesheet" type="text/css" />
    <style type="text/css">
        @media screen and (max-width: 767px) {
            #vc-table > table {
                margin-top: 0px;
            }
            #vc-table thead {
                display: none;
            }
            #vc-table tbody tr:first-child {
               margin-top: 0px;
            }
        }
        @media screen and (max-width: 1000px) {            
            #vc-table tr td.CashCouponsNumber {
                max-width: 100%;
            }
        }
        <%if (MGMFacade.IsEnabled) {%>
        .mc-navbtn {
            width:13% !important;
        }
        <%}%>
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.CashCouponsNew').find('.new').each(function (index, value) {
                $(this).parent().parent().delay(200 * index).fadeIn(800);
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MC" runat="server">
    <uc1:Paragraph ID="pMain" ExternalCache="true" runat="server" />
    <div class="mc-navbar">
        <a href="coupon_List.aspx">
            <div class="mc-navbtn">
                訂單/憑證
            </div>
        </a>
        <%if (MGMFacade.IsEnabled) {%>
        <a href="/user/GiftBox">
            <div class="mc-navbtn">禮物盒</div>
        </a>
        <%} %>
        <a>
            <div class="mc-navbtn on">
                折價券
            </div>
        </a>
        <a href="BonusListNew.aspx">
            <div class="mc-navbtn">
                餘額明細
            </div>
        </a>
        <a href="Collect.aspx">
            <div class="mc-navbtn">
                我的收藏
            </div>
        </a>
        <a href="UserAccount.aspx">
            <div class="mc-navbtn">
                帳號設定
            </div>
        </a>
        <a href="/User/ServiceList">
            <div class="mc-navbtn">
                客服紀錄
            </div>
        </a>
    </div>
    <div class="mc-content">
        <h1 class="rd-smll">17Life折價券</h1>
        <hr class="header_hr" />
        <div class="mc-order-states">
            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div class="voucherstatus_area">
                        <span class='unused <%= DiscountStatus == LunchKingSite.WebLib.Views.FilterStatus.Unused ? "status-on" : string.Empty %>'>
                            <asp:LinkButton ID="btnUnused" runat="server" Text="未使用" OnClick="btnUnused_Click"></asp:LinkButton></span> ｜
                        <span class='used <%= DiscountStatus == LunchKingSite.WebLib.Views.FilterStatus.Used ? "status-on" : string.Empty %>'>
                            <asp:LinkButton ID="btnUsed" runat="server" Text="已使用" OnClick="btnUsed_Click"></asp:LinkButton></span>  ｜
                        <span class='expired <%= DiscountStatus == LunchKingSite.WebLib.Views.FilterStatus.Expire ? "status-on" : string.Empty %>'>
                            <asp:LinkButton ID="btnExpire" runat="server" Text="已失效" OnClick="btnExpire_Click"></asp:LinkButton></span>
                    </div>
                    <asp:Panel ID="divSortArea" runat="server">
                        <div class="voucher_sbn_area" style="margin-bottom: 15px;">
                            <div class="voucherselect_sbn">
                                排序：
                          <asp:DropDownList ID="ddlSortType" runat="server" AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlSortType_SelectedIndexChanged" Width="145px">
                              <asp:ListItem Text="請選擇" Value="0"></asp:ListItem>
                          </asp:DropDownList>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="text-right">
                        <p class="check-point" style='<%= DiscountStatus == LunchKingSite.WebLib.Views.FilterStatus.Expire ? "": "display:none;" %>'>僅顯示1個月內資料</p>
                        </div>  
                    </asp:Panel>
                    <asp:Panel ID="pan_DiscountCode" Visible="false" runat="server">
                        <div id="vc-table" class=" <%= DiscountStatus.ToString() %>-class">
                            <asp:Repeater ID="rptDiscountList" runat="server" OnItemDataBound="rptDiscountList_ItemDataBound">
                                <HeaderTemplate>
                                    <table width="920px" border="0" cellspacing="0" cellpadding="0" id="DiscountCodeTable">
                                        <thead>
                                            <tr>
                                                <th width="30px"></th>
                                                <th class="CashCouponsDEN">金額
                                                </th>
                                                <th class="CashCouponsChannel">適用館別
                                                </th>
                                                <th class="CashCouponsLimited">使用門檻
                                                </th>
                                                <th class="CashCouponsEFD"><%= DiscountStatus == LunchKingSite.WebLib.Views.FilterStatus.Used ? "使用期限" : "有效期限" %>
                                                </th>
                                                <th class="CashCouponsTitle">活動
                                                </th>
                                                <th class="h-discountcode CashCouponsNumber" style='<%= DiscountStatus == LunchKingSite.WebLib.Views.FilterStatus.Unused ? "": "display:none;" %>'>序號
                                                </th>
                                                <th class="h-usedate CashCouponsUseSN" style='<%= DiscountStatus == LunchKingSite.WebLib.Views.FilterStatus.Used ? "": "display:none;" %>'>使用日期
                                                </th>
                                                <th class="h-status CashCouponsUseSN" style='<%= DiscountStatus == LunchKingSite.WebLib.Views.FilterStatus.Expire ? "": "display:none;" %>'>狀態
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="vc-tableContentITEM" style='<%# ((KeyValuePair<ViewDiscountDetail, bool>)(Container.DataItem)).Value ? "display:none": "" %>'>
                                        <td class="CashCouponsNew">
                                            <a class="new" style="color: #bf0000;"><span class='<%# ((KeyValuePair<ViewDiscountDetail, bool>)(Container.DataItem)).Value ? "fa fa-gift fa-fw fa-lg": "" %>'></span></a>
                                        </td>
                                        <td class="CashCouponsDEN">$<%# (((KeyValuePair<ViewDiscountDetail, bool>)(Container.DataItem)).Key.Amount??0).ToString("F0") %>
                                        </td>
                                        <td class="CashCouponsChannelAll">
                                            <%# Helper.IsFlagSet(((KeyValuePair<ViewDiscountDetail, bool>)Container.DataItem).Key.Flag, DiscountCampaignUsedFlags.CategoryLimit) ? PromotionFacade.GetDiscountCategoryString(((KeyValuePair<ViewDiscountDetail, bool>)Container.DataItem).Key.Id) : "全站(部分不適用)" %>
                                        </td>
                                        <td class="CashCouponsLimited">
                                            <%# ((KeyValuePair<ViewDiscountDetail, bool>)(Container.DataItem)).Key.MinimumAmount != null ? "滿" + ((KeyValuePair<ViewDiscountDetail, bool>)(Container.DataItem)).Key.MinimumAmount.Value.ToString("F0") : "無" %>
                                        </td>
                                        <td class="CashCouponsEFD">
                                            <%# ((KeyValuePair<ViewDiscountDetail, bool>)(Container.DataItem)).Key.StartTime.Value.ToString("yyyy/MM/dd HH:mm~") + ((KeyValuePair<ViewDiscountDetail, bool>)(Container.DataItem)).Key.EndTime.Value.ToString("yyyy/MM/dd HH:mm")%>
                                        </td>
                                        <td class="CashCouponsTitle">
                                            <span>
                                                <%# ((KeyValuePair<ViewDiscountDetail, bool>)(Container.DataItem)).Key.Name %></span>
                                        </td>
                                        <td class="h-discountcode CashCouponsNumber" style='<%= DiscountStatus == LunchKingSite.WebLib.Views.FilterStatus.Unused ? "": "display:none;" %>'>
                                            <%# ((KeyValuePair<ViewDiscountDetail, bool>)(Container.DataItem)).Key.Code %>  <%# Helper.IsFlagSet(((KeyValuePair<ViewDiscountDetail, bool>)Container.DataItem).Key.Flag, DiscountCampaignUsedFlags.AppUseOnly) ? " <span class='limited'>限app用</span>": "" %>
                                        </td>
                                        <td class="h-usedate CashCouponsUsed" style='<%= DiscountStatus == LunchKingSite.WebLib.Views.FilterStatus.Used ? "": "display:none;" %>'>
                                            <%# ((KeyValuePair<ViewDiscountDetail, bool>)(Container.DataItem)).Key.UseTime.HasValue ? ((KeyValuePair<ViewDiscountDetail, bool>)(Container.DataItem)).Key.UseTime.Value.ToString("yyyy/MM/dd") : string.Empty %>
                                        </td>
                                        <td class="h-status CashCouponsExpired" style='<%= DiscountStatus == LunchKingSite.WebLib.Views.FilterStatus.Expire ? "": "display:none;" %>'>
                                            <asp:Label ID="lab_UseStatus" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody> </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <table style="border: 3px" width="100%">
                            <tr>
                                <td style="width: 30px; vertical-align: top; padding-top: 12px">
                                    <asp:Image ID="imgLoading1" Style="display: none" class="tdGridPager4Img" ImageUrl="../Themes/PCweb/images/spinner.gif" runat="server" />
                                </td>
                                <td style="align-content: center" class="tdGridPager4">
                                    <uc2:Pager ID="gridPager4" runat="server" PageSize="20" OnGetCount="RetrieveDiscountCount"
                                        OnUpdate="UpdateHandler" CssClass="Pagination"></uc2:Pager>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="divNoDiscount" runat="server" Visible="false">
                        <div class="voucher_content">
                            <div class="voucher_content_status">
                                <asp:Literal ID="liNoDiscountMessage" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="divUseContent" runat="server" Visible="false">
                        <div class="txtlistArea">
                            <h1 class="rd-smll">使用說明</h1>
                            <hr class="header_hr">
                            <div class="txtsteps">
                                1.確認商品適用折價券，並檢查適用館別及使用門檻<br>
                                2.若您的會員帳戶有折價券，結帳頁面會出現 "選擇折價券" 按鈕<br>
                                3.點選按鈕並選擇欲使用的折價券，即可於結帳時成功折抵<br>
                            </div>
                            <div class="txtimg">
                                <img src="../Themes/PCweb/images/u54.png?v=2" width="640" height="160" alt="">
                            </div>
                        </div>
                        <div class="txtlistArea_m">
                            <h1 class="rd-smll">使用說明</h1>
                            <hr class="header_hr">
                            <div class="txtsteps_m">
                                1.確認商品適用折價券，並檢查適用館別及使用門檻<br>
                                2.若您的會員帳戶有折價券，結帳頁面會出現 "選擇折價券" 按鈕<br>
                                3.點選按鈕並選擇欲使用的折價券，即可於結帳時成功折抵<br>
                            </div>
                            <div class="txtimg_m">
                                <img src="../Themes/PCweb/images/u23.png" width="660" height="166" alt="">
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>

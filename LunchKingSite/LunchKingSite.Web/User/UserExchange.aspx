﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="UserExchange.aspx.cs" Inherits="LunchKingSite.Web.User.UserExchange" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../Tools/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../Tools/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="../Tools/js/jquery-ui.js"></script>
    <link href="../Themes/default/images/17Life/G8Return/ReturnNew.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("../themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("../themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("../themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%= ResolveUrl("../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css") %>" rel="stylesheet" />
    <style type="text/css">

        .ui-widget-header {
            background: #808080;
            border: 0;
            color: #fff;
            font-weight: normal;
        }

        .ui-widget-header .ui-icon {
            background-image: url("../Tools/js/css/ui-lightness/images/ui-icons_ffffff_256x240.png");
        }

        .ui-dialog .ui-dialog-buttonpane button {
            cursor: pointer;
            float: right;
            line-height: 1.4em;
            margin: 0.5em 0.4em 0.5em 0;
            overflow: visible;
            padding: 0.2em 0.6em 0.3em;
            width: auto;
            background:#b90404;
            color:#fff;
            font-weight: normal;
        }
                
        .ui-widget-content a:link {
            color: #0026ff;
            text-decoration: none;
        }        
        
        .ui-widget-content a:visited {
            color: #0026ff;
            text-decoration: none;
        }

        .input-full {
            height: 20px;
            padding: 3px 5px;
            border: 1px solid #cccccc;
            /*border-radius: 3px;*/
            vertical-align: middle;
            display: inline-block;
            height: 20px;
            padding: 3px 5px;
            margin-bottom: 10px;
            font-size: 12px;
            line-height: 20px;
            color: #555555;
            vertical-align: middle;
        }
    </style>
    
    <script type="text/javascript">
        var defalutReason = "1.換貨商品的名字：\n\n2.申請要換的數量：\n\n3.您拿到的商品發生了什麼事 (瑕疪？故障？包裝？)：\n\n4.有問題的、與想要換的規格 (例如：S紅要換L藍…)：\n\n5.其他備註與說明 (如果您的聯絡資料與訂單不同，請務必在這裡告訴我們)：\n";

        $(document).ready(function(){
            registInputClick();
            $(window).resize(function () {
                $("#dialog-confirm").dialog("option", "position", ['center', 'center']);
            });
        });

        function registInputClick() {
            //fix ios click not working at first time
            $('input[name=reason]').on('click', function () {
                if ($(this).prop('checked')) {
                    if ($.trim($("#<%=tbx_Reason.ClientID %>").val()).length === 0) {
                        $("#<%=tbx_Reason.ClientID %>").val(defalutReason);
                    }
                }
            });
        }

        function showDefaultReason() {
            if($.trim($("#<%=tbx_Reason.ClientID %>").val()).length === 0) {
                $("#<%=tbx_Reason.ClientID %>").val(defalutReason);
            }
        }

        function getrefundreasonP(event) {
            $('#Pop_Refund1P_Alert').hide();
            <%if (config.EnabledRefundOrExchangeEditReceiverInfo && !IsCoupon)
            {%>
            $('#ReceiverNameErr').hide();
            $('#ReceiverAddressErr').hide();
            if ($('.txtReceiverName').val() == "" || $('.txtReceiverAddress').val() == "") {
                if ($('.txtReceiverName').val() == "") {
                    $('#ReceiverNameErr').show();
                    $('.txtReceiverName').focus();
                }
                if ($('.txtReceiverAddress').val() == "") {
                    $('#ReceiverAddressErr').show();
                    $('.txtReceiverAddress').focus();
                }
                return false;
            }
            <%}%>
            var rad = $('input[name=reason]:checked');
            if ($(rad).val() == undefined || $(rad).val() == null || $(rad).val() == "") {
                $('#Pop_Refund1P_Alert').show();
                return false;
            }
            else {
                if ($.trim($("#<%=tbx_Reason.ClientID %>").val()).length === 0 ||
                    $.trim($("#<%=tbx_Reason.ClientID %>").val()) === $.trim(defalutReason)) {
                    $('#Pop_Refund1P_Alert').show();
                    return false;
                }
                if ($(rad).val() !== '尺寸不合' && $(rad).val() !== '顏色不喜歡') {
                    var showMessage = $(rad).val();
                    if ($.trim($("#<%=tbx_Reason.ClientID %>").val()) !== $.trim(defalutReason)) {
                        showMessage = $.trim(showMessage + "\n" + $("#<%=tbx_Reason.ClientID %>").val());
                    }
                    $("#<%=hif_Reason.ClientID %>").val(showMessage);
                    block_postback();
                    return true;
                }
                else {                    
                    var confirmMsg = '<font style="color:red">請注意！</font><br/>若因個人因素換貨，由於廠商需確認庫存數量，<br/>請見諒無法保證更換顏色或尺寸，並將有換貨運費產生，<br/>若商品已不符預期，請依<a href="<%=config.SiteUrl%>/ppon/newbieguide.aspx?s=3&q=20469#anc20469">退貨規定</a>改辦理<a href="<%=config.SiteUrl%>/Ppon/NewbieGuide.aspx?s=3&q=30">退貨申請</a>';

                    event.preventDefault();
                    fnOpenNormalDialog(confirmMsg);
                }
            }
        }
                
        function errormessagebg() {
            $('#divExchangeOrderIdInput').removeClass("form-unit no-line");
            $('#divExchangeOrderIdInput').addClass("form-unit error-bg");
        }
        
        function goDefault() {
            window.location.href = "<%=config.SiteUrl%>";
            return false;
        }

        function goCouponList() {
            window.location.href = "coupon_List.aspx";
            return false;
        }        

        function fnOpenNormalDialog(confirmMsg) {
            $("#dialog-confirm").html(confirmMsg);

            // Define the Dialog and its properties.
            $("#dialog-confirm").dialog({
                resizable: false,
                modal: true,
                title: "確認申請",
                height: 'auto',
                width: 'auto',
                position: ['center', 'center'],
                buttons: {
                    "確定": function () {
                        $(this).dialog('close');
                        dialogConfirm();
                    },
                    "取消": function () {
                        $(this).dialog('close');
                    }
                }
            });
        }

        function dialogConfirm() {
            var showMessage = $('input[name=reason]:checked').val();
            if ($.trim($("#<%=tbx_Reason.ClientID %>").val()) !== defalutReason) {
                showMessage = $.trim(showMessage + "\n" + $("#<%=tbx_Reason.ClientID %>").val());
            }
            $("#<%=hif_Reason.ClientID %>").val(showMessage);
            block_postback();
            __doPostBack('<%=btn_Confirm.UniqueID %>', "");
        }

        function block_postback() {
            $.blockUI({ message: "<center><img src='../Themes/PCweb/images/spinner.gif' />處理中，請稍候...</center>", css: { left: 0, width: '100%' } });
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <asp:checkbox id="cbx_IsCoupon" runat="server" visible="false" />
    <asp:HiddenField ID="hif_PamelMode" runat="server" />
    <asp:HiddenField ID="hif_Reason" runat="server" />
    
    <asp:HiddenField ID="hif_OrderID" runat="server" />
    <asp:HiddenField ID="hif_OrderGuid" runat="server" />
    <asp:HiddenField ID="hif_OrderStatus" runat="server" />
    <asp:Panel ID="pan_Main" runat="server">
        <!-- 換貨申請起始 -->
        <asp:Panel ID="pan_ExchangeOrderIdInput" runat="server">
            <div class="ReturnFrame">
                <h1>換貨申請</h1>
                <div class="ReturnCenter">
                    <div class="grui-form">
                        <div id="divExchangeOrderIdInput" class="form-unit no-line">
                            <label class="unit-label">請輸入訂單編號</label>
                            <div class="data-input">
                                <asp:TextBox ID="tbx_OrderID" runat="server" Width="180" CssClass="input-half"></asp:TextBox>
                                <asp:Label ID="lab_OrderStatusMessage" runat="server" CssClass="error-tip"></asp:Label>
                            </div>
                        </div>
                        <div class="form-unit no-line">
                            <label for="" class="unit-label"></label>
                            <div class="data-input">
                                <p>
                                    您可至 <a href="coupon_List.aspx" target="_blank">訂單/憑證</a> 查看訂單編號。
                                </p>                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ReturnBottom">
                    <asp:Button ID="btn_SearchOrderID" runat="server" CssClass="btn btn-large btn-primary"
                        OnClick="SerachOrderInfo" ValidationGroup="A1" Width="146" Text="申請換貨"></asp:Button>
                </div>
            </div>
        </asp:Panel>
        <!-- 換貨須知 -->
        <asp:Panel ID="pan_ExchangeNotice" runat="server" Visible="false">
            <div class="ReturnFrame">
                <h1>申請換貨須知</h1>
                <div class="ReturnCenter">
                    <p style="color:red;font-weight:bold">宅配商品於簽收後七日猶豫期內可進行檢視商品瑕疪之權益，請留意，猶豫期非試用期。</p>
                    <p>申請換貨的範圍在於您本次購買的商品本身帶有瑕疵、破損、規格不符、零件短缺或者有缺件的狀況，可以在鑑賞期內申請換貨，
                    換貨內容最多限於同款A換同款B，若商品為主觀意識(購買時尺寸、顏色、款式‧‧‧等選擇錯誤)所導致的換貨需求，我方則有權保留是否換貨的權益。</p>
                    <p></p>
                    <p style="color:red;font-weight:bold">換貨申請步驟：</p>
                    <p>1、為了加速換貨流程，請您協助先將瑕疪部分拍照後，寄發17Life客服中心：<a href="mailto:csr@17life.com">csr@17life.com</a>，寄信時請加註訂單編號於信件中，以利我們快速處理。在換貨結束之前，請您暫時留存照片檔。</p>
                    <p>2、收件人員前往收取商品，請您協助事先備妥商品，並依原物流包裝方式完整封箱、封袋。原商品若以專屬紙箱(袋)包裝，請勿直接於原廠紙箱(袋)上黏貼紙張或書寫文字，務必準備另一紙箱(袋)包裝。</p>
                    <p>3、訂單若為組合式商品申請部分換貨，請保留完好商品數量，暫勿使用或分送，以維護退換權益。</p>
                    <p>4、換貨商品因廠商將需回收檢視且需視庫存數量可能，處理期間約需7-14個工作天，請您見諒。</p>
                    <p>5、若商品已確認不符預期，建議您可依退貨規定改辦理退貨申請。</p>
                </div>
                <div class="ReturnBottom">
                    <asp:LinkButton ID="lbt_Cancel" runat="server" CssClass="btn-link"
                        Text="取消" Style="text-decoration: underline; display:inline-block" OnClientClick="return goCouponList();"></asp:LinkButton>
                    <asp:Button ID="btn_NoticeConfirm" runat="server" CssClass="btn btn-large btn-primary"
                        OnClick="ExchangeNoticeConfirm" Text="確認"></asp:Button>
                </div>
            </div>
        </asp:Panel>
        <!-- 選擇換貨申請原因 -->
        <asp:Panel ID="pan_ExchangeReasonInput" runat="server" Visible="false">
            <div class="ReturnFrame">
                <h1>換貨申請原因</h1>
                <div class="ReturnCenter">
                    <div class="ReturnApplitext" style="color: red; font-weight: bold">為了提供您更優質的商品與售後服務，請告訴我們您申請換貨的原因和必需要注意的事情。</div>

                    <div class="ReturnAppliReason">
                        <ul>
                            <li onclick="this.getElementsByTagName('input')[0].checked=true; showDefaultReason();">
                                <input name="reason" type="radio" value="顏色送錯" />顏色送錯</li>
                            <li onclick="this.getElementsByTagName('input')[0].checked=true; showDefaultReason();">
                                <input name="reason" type="radio" value="品項送錯" />品項送錯</li>
                            <li onclick="this.getElementsByTagName('input')[0].checked=true; showDefaultReason();">
                                <input name="reason" type="radio" value="尺寸送錯" />尺寸送錯</li>
                            <li onclick="this.getElementsByTagName('input')[0].checked=true; showDefaultReason();">
                                <input name="reason" type="radio" value="商品瑕疪" />商品瑕疪</li>
                            <li onclick="this.getElementsByTagName('input')[0].checked=true; showDefaultReason();">
                                <input name="reason" type="radio" value="商品故障" />商品故障</li>
                            <li onclick="this.getElementsByTagName('input')[0].checked=true; showDefaultReason();">
                                <input name="reason" type="radio" value="尺寸不合" />尺寸不合</li>
                            <li onclick="this.getElementsByTagName('input')[0].checked=true; showDefaultReason();">
                                <input name="reason" type="radio" value="顏色不喜歡" />顏色不喜歡</li>

                            <%if (config.EnabledRefundOrExchangeEditReceiverInfo && !IsCoupon)
                                {%>
                            <li onclick="this.getElementsByTagName('input')[0].checked=true; showDefaultReason();">
                                <input name="reason" type="radio" value="包裏缺件" />包裏缺件</li>
                            <li><div class="ReturnApplitext" style="color: red; font-weight: bold">收件人資訊</div>
                                <div id="tdReceiver" class="form-unit">
                                    <label id="lbReceiverName" class="unit-label">姓名</label>&nbsp;
                                    <asp:TextBox ID="txtReceiverName" runat="server" CssClass="txtReceiverName input-2over3 input-full" width="100px" />
                                    <span id="ReceiverNameErr" style="display: none;color:darkred;">
                                    <img src="../Themes/PCweb/images/enter-error.png"  style="width:18px;height:18px;"><span>請填入姓名！</span>
                                    </span>
                                    <br/>
                                    <label id="lbReceiverAddress" class="unit-label">地址</label>&nbsp;
                                    <asp:TextBox ID="txtReceiverAddress" runat="server" CssClass="txtReceiverAddress input-full" width="250px"/>
                                    <span id="ReceiverAddressErr" style="display: none; color: darkred;">
                                    <img src="../Themes/PCweb/images/enter-error.png" style="width:18px;height:18px;"><span>請填入地址！</span>
                                    </span>
                                </div></li>
                            <%}%>
                            <li>
                                <span style="color:red; font-weight:bold">請協助描述您要換的商品品項、數量以及相關資訊，這樣可以幫助我們加快處理作業，而且減少打擾您的機會。</span>
                                <asp:TextBox ID="tbx_Reason" runat="server" TextMode="MultiLine" Width="100%" Height="200px" Style="font-size:13px;resize:none;" ClientIDMode="Static"></asp:TextBox></li>
                        </ul>
                    </div>
                    <div class="ReturnAppliRedTips" id="Pop_Refund1P_Alert" style="display: none">
                        為免您的換貨資訊無法確實傳達給商家，請您協助填寫備註欄位中的相關條件描述，以加快處理速度，謝謝。
                    </div>
                </div>
                <div class="ReturnBottom">
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn-link"
                        Text="取消" Style="text-decoration: underline; display:inline-block;" OnClientClick="return goCouponList();"></asp:LinkButton>
                    <asp:Button ID="btn_Confirm" runat="server" CssClass="btn btn-large btn-primary"
                        OnClick="ConfirmExchange" OnClientClick="return getrefundreasonP(event);" Text="確認" ClientIDMode="Static"></asp:Button>
                </div>
            </div>
            <div id="dialog-confirm"></div>
        </asp:Panel>
        <!-- 換申請成功 -->
        <asp:Panel ID="pan_ExchangeSuccess" runat="server" Visible="false">
            <div class="ReturnFrame">
                <h1>換貨申請成功</h1>
                <div class="ReturnCenter">
                    <div class="ReturnApplitext">
                        <asp:Label ID="lab_P1111" runat="server" CssClass="ReturnApplitext"></asp:Label>
                    </div>
                    <p style="color:red; font-weight:bold;">您的申請已經完成，我們將為您送交17Life退換貨處理中心進行換貨程序。</p>
                    <p>1、請您於換貨期間，注意您的聯絡地址與電話，我們將儘快聯絡廠商安排物流人員，盡速前往取貨或與您聯繫。</p>
                    <p>2、您所要更換貨的商品，請儘量以原送貨使用之包裝紙箱包裝妥當，並保持商品本體、原廠內外包裝、配件、贈品、保證書、及所有附隨文件或資料的完整性，切勿缺漏任何配件。</p>
                    <p>3、換貨申請後，請您協助先將瑕疪部分拍照，寄發17Life客服中心：<a href="mailto:csr@17life.com">csr@17life.com</a>，寄信時請加註訂單編號於信件中，以利我們快速處理。在換貨結束之前，請您暫時留存照片檔。</p>
                    <p>4、換貨商品因廠商將需回收檢視且需視庫存數量後方能完成換貨程序，處理期間約需7-14個工作天，請您見諒。</p>
                    <p>5、待雙方確認更換商品無誤後，17Life將儘速為您結案。</p>
                </div>
                <div class="ReturnBottom">
                    <input type="button" value="回首頁" class="btn btn-large btn-primary" onclick="return goDefault();">
                </div>
            </div>
        </asp:Panel>   
        <!-- 申請(建立換貨單)失敗 -->
        <asp:Panel ID="pan_ExchangeFail" runat="server" Visible="false">            
            <div class="ReturnFrame">
                <h1>換貨申請失敗</h1>
                <div class="ReturnCenter">
                    <p>您的換貨申請發生異常，若您仍須換貨，請<a href="<%=config.SiteUrl + config.SiteServiceUrl%>">聯絡客服人員</a>。</p>
                </div>
                <div class="ReturnBottom">
                    <asp:Button runat="server" CssClass="btn btn-large btn-primary" Text="回首頁" OnClientClick="return goDefault();"></asp:Button>
                </div>
            </div>
        </asp:Panel>
    </asp:Panel>
</asp:Content>

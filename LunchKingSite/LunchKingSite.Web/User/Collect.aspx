﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="Collect.aspx.cs" Inherits="LunchKingSite.Web.User.Collect" Title="我的收藏" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="uc1" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Register Src="Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/PPon/PPon.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-L.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-M.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-S.css")%>' rel="stylesheet" type="text/css" />
    <style type="text/css">
        <%if (MGMFacade.IsEnabled) {%>
        .mc-navbtn {
            width:13% !important;
        }
        <%}%>

        /* 用bpopup.js製作的彈窗 以及 其內UI Start */
        .btn-default {
            width: 100px;
            height: 20px;
            background: #ccc;
            padding: 10px;
            text-align: center;
            border-radius: 5px;
            color: white;
            font-size:medium;
        }
        .btn-warn {
            width: 100px;
            height: 20px;
            background: #bf0000;
            padding: 10px;
            text-align: center;
            border-radius: 5px;
            color: white;
            font-size:medium;
        }
        .bpopup-modal {
            /*彈窗預設為隱藏，直到jquery調用才顯示*/
            display: none;
            /*margin-left: 250px;*/
            width: calc(100vw - 575px);
            background: #fff;
        }

        .bpopup-modal .row {
            margin-bottom: 0;
        }

        .bpopup-modal.bpopup-modal-toast {
            width: 600px;
            color: #000;
            border-radius: 10px;
        }


        .bpopup-modal.bpopup-modal-toast .bpopup-modal-title {
            text-align: left;
            margin: 20px;
            margin-top:50px;
            margin-bottom:50px;
            font-size: 18px;
        }

        .bpopup-modal-footer {
            margin-top: 20px;
            margin-left:20px;
            margin-bottom:50px;
            text-align:center;
        }

        .bpopup-modal a {
            cursor: pointer;
        }

        /* 用bpopup.js製作的彈窗 以及 其內UI End */
    </style>
    <script type="text/javascript" src="/Themes/OWriteOff/plugin/bpopup/jquery.bpopup.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            
            setPanelShow();
            SetLinkButtonBind();

           $("#btnConfirmOK").bind("click", function () {
               $("#<%=btnCancelOutOfDateCollectDeal.ClientID%>").click();
           });

            $("#btnRemoveCollectDealConfirmOK").click(function () {
                $('#' + removeCollectDealId).click();
            });
        });

        var removeCollectDealId;

        function showConfirm(e) {
                $('#bpopup-toast-message').bPopup({
                    modalClose: true,
                });
        };

        function showRemoveCollectDealConfirm(id) {
            $('#bpopup-removeCollectDeal-message').bPopup({
                modalClose: true,
            });

            removeCollectDealId = id;
        };

        function setPanelShow() {
            if ($('#hidCollectCount').val() == "0") {
                $('#collectMsg').show();
                $('#OutOfDate').hide();
                $('#mc-table').hide();
                $('.collectPager').hide();
                $('.mc-plr-box-L').hide();
            }
            else {
                $('.mc-plr-box-L').show();
                $('#OutOfDate').show();
                $('#mc-table').show();
                $('.collectPager').show();
                $('#collectMsg').hide();
            }
        }

        function setCollectDealExpireMessage()
        {
            var dealExpireMessage = $('input:checkbox[id=cbMemberCollectDealExpireMessage]').is(":checked");
            var uid = $('#hidMemberUserId').val();
            $.ajax({
                type: "POST",
                url: "Collect.aspx/SetCollectDealExpireMessage",
                data: "{memberUnique:" + uid + ", isEnable:" + dealExpireMessage + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $('input:checkbox[id=cbMemberCollectDealExpireMessage]').prop("checked", msg.d);
                }, error: function (response, q, t) {
                    $('input:checkbox[id=cbMemberCollectDealExpireMessage]').prop("checked", false);
                }
            });
            return false;
        }
        function SetLinkButtonBind() {
            $(".collectPager").find(".Pagination-arrow-previous").bind("click", function () {
                $('html, body').animate({ scrollTop: 0 }, 'normal');
            });
            $(".collectPager").find(".Pagination-arrow-next").bind("click", function () {
                $('html, body').animate({ scrollTop: 0 }, 'normal');
            });
            var ddlVal = '';
            $(".collectPager").find(".rd-page-dis").focus(function () { ddlVal = $(this).val(); }).blur(function () {
                if (ddVal == $(this).val()) {
                    $(this).change();
                }
            }).change(function () {
                $('html, body').animate({ scrollTop: 0 }, 'normal');
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <asp:HiddenField ID="hidMemberUserId" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hidCollectBidList" ClientIDMode="Static" runat="server" />
    
    <div id="bpopup-toast-message" class="card-panel bpopup-modal bpopup-modal-toast">
        <div class="row">
            <div class="col s12 bpopup-modal-title alert_message">
                確定刪除所有已結束販售的收藏？
            </div>
            <div class="col s12 center-align bpopup-modal-footer">
                <a id="btnConfirmOK" class="waves-effect waves-light btn-warn b-close bpopup-step_alert-confirm">確定</a> 
                <a class="waves-effect waves-light btn-default b-close">取消</a>
            </div>
        </div>
    </div>

    <div id="bpopup-removeCollectDeal-message" class="card-panel bpopup-modal bpopup-modal-toast">
        <div class="row">
            <div class="col s12 bpopup-modal-title alert_message">
                確定要刪除此筆收藏嗎？
            </div>
            <div class="col s12 center-align bpopup-modal-footer">
                <a id="btnRemoveCollectDealConfirmOK" class="waves-effect waves-light btn-warn b-close bpopup-step_alert-confirm">確定</a>
                <a class="waves-effect waves-light btn-default b-close">取消</a>
            </div>
        </div>
    </div>


    <div class="center">
        <uc1:Paragraph ID="pMain" ExternalCache="true" runat="server" />
        <div class="mc-navbar">
            <a href="coupon_List.aspx">
                <div class="mc-navbtn">訂單/憑證</div>
            </a>
            <%if (MGMFacade.IsEnabled) {%>
            <a href="/user/GiftBox">
                <div class="mc-navbtn">禮物盒</div>
            </a>
            <%} %>
            <a href="DiscountList.aspx">
                <div class="mc-navbtn">
                    折價券</div>
            </a>
            <a href="BonusListNew.aspx">
                <div class="mc-navbtn">餘額明細</div>
            </a>
            <a>
                <div class="mc-navbtn on">我的收藏</div>
            </a>
            <a href="UserAccount.aspx">
                <div class="mc-navbtn">帳號設定</div>
            </a>
            <a href="/User/ServiceList">
                <div class="mc-navbtn rd-PastCustomer">客服紀錄</div>
            </a>
        </div>
        <div class="mc-content">
            <h1 class="rd-smll">我的收藏</h1>
            <hr class="header_hr" />
            <div class="grui-form">
                <div id="collectMsg" style="display:none">
                    <p class="mc-c5p">
                        您尚未有任何收藏。<br />
                        現在可以在瀏覽時，點擊<span class="mc-c5-Collect">收藏</span>進行收藏。
                    </p>
                </div>
                <p class="mc-c5p">
                    <div class="mc-plr-box-L">
                        <asp:CheckBox ID="cbMemberCollectDealExpireMessage" OnClick="return setCollectDealExpireMessage();" AutoPostBack="false" ClientIDMode="Static" Text="開啟收藏通知信，即將售完通知您。" runat="server" />
                    </div>
            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <asp:HiddenField ID="hidCollectCount" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="hidCollectTimeOrderByStatus" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="hidBusinessHourOrderTimeEndOrderByStatus" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="hidFirstOrderByColumn" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="hidSecondOrderByColumn" ClientIDMode="Static" runat="server" />
                    <div id="OutOfDate">
                        <div class="mc-plr-box-R">
                            <span class="mc-C5-MGR">
                                您有<span class="mc-error"><asp:Literal ID="litOutOfDateCount" runat="server"></asp:Literal></span>筆已結束販售的收藏</span>
                            <input type="button" ID="btnExecCancelOutOfDateCollectDeal" OnClick="showConfirm()" class="btn btn-small" value="刪除結束的收藏" />
                            <div style="display: none">
                                <asp:Button ID="btnCancelOutOfDateCollectDeal" OnClick="btnCancelOutOfDateCollectDeal_Click" runat="server" /> <%--刪除結束的收藏--%>
                            </div>
                        </div>
                    </div>
                </p>
                <div id="mc-table">
                        <asp:Repeater ID="rp_CollectList" OnItemDataBound="rp_CollectList_ItemDataBound" OnItemCommand="rp_CollectList_ItemCommand" runat="server">
                            <HeaderTemplate>
                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="mc-c5tdwe0"></th>
                                        <th class="mc-c5tdwe1">優惠內容</th>
                                        <th class="mc-c5tdwe2 <%# (FirstOrderByColumn.Split(' ')[0] == ViewMemberCollectDeal.Columns.BusinessHourOrderTimeE) ? ((BusinessHourOrderTimeEndOrderByStatus == "asc") ? "mc-sort-up" : "mc-sort-down") : "mc-sort-initial"%>">
                                            <asp:LinkButton ID="lkbtnBusinessHourOrderTimeEnd" OnClick="lkbtnBusinessHourOrderTimeEnd_Click" runat="server">
                                                販售結束時間
                                            </asp:LinkButton>
                                        </th>
                                        <th class="mc-c5tdwe3 <%# (FirstOrderByColumn.Split(' ')[0] == ViewMemberCollectDeal.Columns.CollectTime) ? ((CollectTimeOrderByStatus == "asc") ? "mc-sort-up" : "mc-sort-down") : "mc-sort-initial"%>">
                                            <asp:LinkButton ID="lkbtnCollectTime" OnClick="lkbtnCollectTime_Click" runat="server">
                                                收藏時間
                                            </asp:LinkButton>
                                        </th>
                                        <th class="mc-c5tdwe4">管理</th>
                                    </tr>
                                </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="mc-c5tdwe0">
                                        <div class="mc-c5-pic">
                                            <asp:Image ID="imgDeal" runat="server" />
                                        </div>
                                    </td>
                                    <td valign="top" class="rd-Off-line">
                                        <div class="mc-infoBox">
                                            <div class="mc5T">
                                                <asp:Literal ID="litDealTitle" runat="server"></asp:Literal>
                                            </div>
                                            <div class="mc5-dealpricetext">好康價：<span class="mc-error">
                                                <asp:Literal ID="litDealPrice" runat="server"></asp:Literal>
                                            </span></div>
                                            <div class="mc5-Sold ">已售出：
                                                <asp:Literal ID="litSoldCount" runat="server"></asp:Literal>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="mc-c5tdwe2">
                                        <%# (((ViewMemberCollectDeal)(Container.DataItem)).BusinessHourOrderTimeE > System.DateTime.Now) ? "<p class='mc-error'>限時優惠中～</p>" : "<p>已結束販售<p>"%>
                                        <%# (((ViewMemberCollectDeal)(Container.DataItem)).BusinessHourOrderTimeE > System.DateTime.Now) ? string.Format("<p>{0}</p>", ((ViewMemberCollectDeal)(Container.DataItem)).BusinessHourOrderTimeE.ToString("yyyy-MM-dd HH:mm")) : string.Empty%>
                                        <%# (((ViewMemberCollectDeal)(Container.DataItem)).ChangedExpireDate !=null) ? string.Format("<p>{0}</p>", "(至" + ((ViewMemberCollectDeal)(Container.DataItem)).ChangedExpireDate.Value.ToString("yyyy-MM-dd HH:mm")) + "憑證停止使用)" : string.Empty%>
                                    </td>
                                    <td class="mc-c5tdwe3">
                                        <p>
                                            <%# ((ViewMemberCollectDeal)(Container.DataItem)).CollectTime.ToString("yyyy-MM-dd")%>
                                        </p>
                                    </td>
                                    <td class="rd-Off-line">
                                         <div style="display: none">
                                            <asp:Button ID="btnRemoveCollectDeal" CommandName="RemoveCollectDeal" CommandArgument="<%# ((ViewMemberCollectDeal)(Container.DataItem)).BusinessHourGuid%>" CssClass="removeCollectDeal" runat="server" /> <%--刪除收藏--%>
                                        </div>
                                        <a class="btn btn-mini btn-primary mc5-btnSpacing" href="<%# ResolveUrl(string.Format("~/deal/{0}?cpa={1}", ((ViewMemberCollectDeal)(Container.DataItem)).BusinessHourGuid, "Collect-Go"))%>" target="_blank"><font style="color:white">馬上看</font></a>
                                        <br class="rd-C-Hide"/>
                                        <asp:HiddenField ID="hidBid" runat="server" Value="<%# ((ViewMemberCollectDeal)(Container.DataItem)).BusinessHourGuid%>" />
                                        <input type="button" ID="btnExecCancelOutOfDateDeal" OnClick="showRemoveCollectDealConfirm($(this).parents('td').find('.removeCollectDeal').attr('id'))" class="btn btn-mini btn-return mc5-btnSpacing fix_btn_width" value="刪除收藏" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                </div>
                <uc1:Pager ID="collectListPager" CssClass="collectPager" runat="server" PageSize="10" OnGetCount="RetrieveRecordCount"
                    OnUpdate="UpdateHandler" />
            </ContentTemplate>
        </asp:UpdatePanel>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!--center-->
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SetLinkButtonBind);
    </script>
</asp:Content>

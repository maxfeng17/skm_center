﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="CouponGet.aspx.cs" Inherits="LunchKingSite.Web.User.CouponGet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href="../Themes/default/images/17Life/NewHomepage/IE6.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/PCweb/css/MasterPage17life.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/PCweb/css/17lifeMember.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/PCweb/css/footer.css" rel="stylesheet" type="text/css" />

    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <div class="Homepagecenter Homepageconter">

            <!---(正常顯示)-->
            <!--<div class="LifeMemberFrame">-->
            <div class="MemberCardFrame">

                <!--<div class="LifeMember">-->
                <div class="MemberCard">

                    <!--<div class="LifeMemberTopBlack">-->
                    <div class="MemberCardTopBlack">
                        <div class="LifeMemberTopText">憑證已失效</div>
                    </div>

                    <div class="LifeMemberCenter">
                        <div class="LifeMemberConfirmMessage">
                            <br/>
                           <span class="LifeMemberConfirmEPText">此憑證已失效。來看看今日好康吧！</span>
                           <div class="text-center">
                               <a href="../Default.aspx" class ="btn btn-large btn-primary"><span style="color:white">看今日好康</span></a>
                           </div>
                      </div>
                    </div>

                </div>
            </div>
        </div>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="CouponDetail.aspx.cs" Inherits="LunchKingSite.Web.User.CouponDetail" ClientIDMode="Static" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.Core.Component" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="uc1" %>
<%@ Register Src="Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Src="~/ControlRoom/Controls/EinvoiceFooterControl.ascx" TagName="EinvoiceFooter" TagPrefix="uc" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.Core.Models.Entities" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href="<%=ResolveUrl("~/Themes/default/images/17Life/G2/PPB.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Themes/PCweb/css/OrderDetail.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("../themes/PCweb/css/RDL-L.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("../themes/PCweb/css/RDL-M.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("../themes/PCweb/css/RDL-S.css") %>" rel="stylesheet" type="text/css" />
    <link href="../Themes/default/images/17Life/G7/ReturnApplication.css" rel="stylesheet" type="text/css" />
    <!-- 彈窗jquery和它的功能宣告 -->
    <link href="<%= ResolveUrl("../Tools/js/css/jquery-confirm.min.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%= ResolveUrl("../Tools/js/jquery-confirm.min.js") %>" type="text/javascript"></script>
    <script src="../Themes/OWriteOff/plugin/bpopup/jquery.bpopup.js" type="text/javascript"></script>
    <style type="text/css">
        .email_pop p {
            font-family: "微軟正黑體", "Microsoft JhengHei";
        }

        @media screen and (max-width: 1000px) and (min-width: 768px) {
            .pop-window {
                width: 700px;
            }
        }

        @media screen and (max-width: 767px) and (min-width: 571px) {
            .pop-window {
                width: 600px;
                left: 50px;
            }
        }

        @media screen and (max-width: 570px) and (min-width: 481px) {
            .pop-window {
                width: 500px;
                left: 100px;
            }
        }

        @media screen and (max-width: 480px) {
            .pop-window {
                width: 300px;
                left: 200px;
            }
        }

        @media screen and (max-width: 320px) {
            .pop-window {
                width: 300px;
                left: 200px;
                margin-top: -65px;
            }
        }

        .return_modify .btn-secondary {
            color: #ffffff;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            background-image: url(btn_bk_pri_ie.png);
            background-color: #2482c1;
            background-image: -moz-linear-gradient(top, #3399dd, #2482c1);
            background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#3399dd), to(#2482c1));
            background-image: -webkit-linear-gradient(top, #3399dd, #2482c1);
            background-image: -o-linear-gradient(top, #3399dd, #2482c1);
            background-image: linear-gradient(to bottom, #3399dd, #2482c1);
            background-repeat: repeat-x;
            border-color: #2482c1;
            border-color: rgba(0, 0, 0, 0.1);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f#3399dd', endColorstr='#2482c1', GradientType=0);
            filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
            text-decoration: none;
            font-size: 14px;
            padding: 6px 12px;
        }

        .OrderReturnCash p .item {
            text-align: left;
        }


    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <CompositeScript ScriptMode="Release">
            <Scripts>
                <asp:ScriptReference Path="~/Tools/js/jquery.blockUI.js" />
            </Scripts>
        </CompositeScript>
    </asp:ScriptManagerProxy>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.note-text-wrap').mouseenter(function () {
                var target = $(this);
                target.queueInterruptibleAction(200, function () {
                    target.find(".note-text-box-description").stop(false, true).fadeIn(500);
                });
            });
            $('.note-text-wrap').mouseleave(function () {
                var target = $(this);
                target.queueInterruptibleAction(50, function () {
                    target.find(".note-text-box-description").stop(false, true).fadeOut(500);
                });
            });

            if ($("input[id=HfChannelAgent]").val() != "<%=(int)AgentChannel.TaiShinMall%>") {
                $(".pop-download-app").show();
            }
            $("table#tab1 > tbody > tr").each(function () {
                var $tds = $(this).find('td');
                var span = $(this).find('td#tdPponState > span:last');
                if (span.text() == "未使用" || span.text() == "處理中") {
                    if ($("#isFamiportGroupCoupon").val() != '' && $("#isFamiportGroupCoupon").val() != undefined) {
                        $tds.eq(2).removeClass("OrderSN");
                        $tds.eq(2).addClass("OrderSN hide_in_m");

                        $tds.eq(3).removeClass("OrderCode");
                        $tds.eq(3).addClass("OrderCode hide_in_m");

                        $tds.eq(4).removeClass("OrderEXP");
                        $tds.eq(4).addClass("OrderEXP hide_in_m");
                    }
                }
            });

            if (navigator.userAgent.match(<%=string.Format("/{0}/i", conf.AndroidUserAgent)%>)) {
                $("#<%=hif_device.ClientID %>").val('android');
            } else if (navigator.userAgent.match(<%=string.Format("/{0}/i", conf.iOSUserAgent)%>)) {
                if (navigator.userAgent.indexOf('iPad') < 0) {
                    $("#<%=hif_device.ClientID %>").val('ios');
                } else {
                    $("a .order-get-coupon .c5").hide();
                }
            } else {
                $("a .order-get-coupon .c5").hide();
            }

            //popup
            $('#downloadReceiptLink').bind('click', function (e) {
                $('div#Pop_Receipt').bPopup({
                    modalClose: true,
                });
            });
        });

        $(function () {
            $(".mbe-menu-btn").click(slidingMenuToggle);
            $(".mbe-channel").click(slidingMenuToggle);
            $('#naviMemberLink').removeClass().addClass('navbtn_inpage fr');

            btn_MailCouponEventRegister();
            btnReturnsEventRegister();
            btnExchangeEventRegister();
        });

        $.fn.center = function () {
            if ($(window).width() > 800) {
                this.css("width", ((800 / $(window).width()) * 100) + "%");
            }
            else {
                this.css("width", '80%');
            }
            this.css("position", "absolute");
            this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
            this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
            return this;
        }

        ///////////////////////////////////
        //電子折讓單彈出畫面按鈕事件
        function btn_ReturnDiscountElec() {
            var target = $('#Pop_rptForm');
            $.blockUI({ message: $(target), css: { backgroundcolor: '#F7F7F7', border: '10px solid #fff', cursor: 'defalut', display: 'block', height: '80%' } })
            $('.blockUI.blockMsg').center();
        }

        function unblock_ui() {
            $.unblockUI();
            return false;
        }

        //註冊[Email索取憑證]按鈕事件
        function btn_MailCouponEventRegister() {
            $('.MailCoupon').css('cursor', 'pointer')
                .on("click", function () {
                    var couponId = $(this).attr("couponId");
                    $('#btn_MailCoupon').attr('couponId', couponId);
                    PopWindow($('#Pop_MailCoupon'));

                    $('#email_pop_content').show();
                    $('#email_pop_result').hide();
                    $('#error_email').hide();
                    window.couponMailSent = false;
                });

            $('#btn_MailCoupon').on("click", function () {
                if (window.couponMailSent) {
                    return;
                } else {
                    window.couponMailSent = true;
                }
                var couponId = $(this).attr("couponId");
                var userEmail = $('#userEmail').val();
                if (isEmail(userEmail) == false) {
                    $('#error_email').show();
                    return false;
                } else {
                    $('#error_email').hide();
                }
                $.ajax({
                    type: "POST",
                    url: "CouponDetail.aspx/SendCouponMail",
                    data: "{ emailTo: '" + userEmail + "', couponId: " + couponId + " }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        window.couponMailSent = false;
                        if (msg.d == false) {
                            alert('系統繁忙中，請稍後再試!!');
                        } else {
                            $('#email_pop_content_email').text(userEmail);
                            $('#email_pop_content').hide();
                            $('#email_pop_result').show();
                        }
                    }, error: function (response, q, t) {
                        if (t == 'userNotLogin') {
                            location.href = '<%=FormsAuthentication.LoginUrl %>';
                        }
                    }
                });
                return false;
            });
        }

        //註冊[申請退貨]按鈕事件
        function btnReturnsEventRegister() {
            $('#btnReturns, #btnCancel').on("click", function () {
                var type = $("#<%=hif_DeliveryType.ClientID %>").val();
                if (type != '<%=(int)DeliveryType.ToHouse%>') {
                    $.ajax({
                        type: "POST",
                        url: "CouponDetail.aspx/IsRefund",
                        data: "{ OrderGuid:'<%=this.OrderGuid %>' }",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (result) {
                            if (result.d != null) {
                                if (result.d.isAllow && result.d.reson != "") {
                                    if (confirm(result.d.reson)) {
                                        location.href = 'UserRefund.aspx?orderid=<%=this.OrderId %>';
                                    }
                                } else if (result.d.isAllow) {
                                    location.href = 'UserRefund.aspx?orderid=<%=this.OrderId %>';
                                } else {
                                    alert(result.d.reson);
                                }
                            }
                        },
                        error: function () {
                            alert('系統繁忙中，請稍後再試!!');
                        }
                    });
                } else {
                    location.href = 'UserRefund.aspx?orderid=<%=this.OrderId %>';
                }
                return true;
            });
        }

        //註冊[申請換貨]按鈕事件
        function btnExchangeEventRegister() {
            var type = $("#<%=hif_DeliveryType.ClientID %>").val();
            if (type != '<%=(int)DeliveryType.ToHouse%>' || '<%=conf.EnabledExchangeProcess%>' === 'False') {
                $('#btnExchange').hide();
            }
            else {
                $('#btnExchange').on("click", function () {
                    location.href = 'UserExchange.aspx?orderid=<%=this.OrderId %>';
                    return true;
                });
            }
        }

        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if ($.trim(email) != '' && regex.test(email))
                return true;
            else
                return false;
        }
        function block_ui(target, iwidth, iheight) {
            unblock_ui();
            $.blockUI({ message: $(target), css: { backgroundcolor: 'transparent', border: 'none', top: ($(window).height()) / 8 + 'px', left: ($(window).width() - 700) / 2 + 'px', width: iwidth, height: iheight } })
        }
        function unblock_ui() {
            $.unblockUI();
            return false;
        }

        //送出索取
        function paperinvoicecheck() {
            var leng1 = $("#<%=tbx_Einvoice_Receiver.ClientID %>").val();
            var leng2 = $("#<%=tbx_Einvoice_Address.ClientID %>").val();
            if (leng1.length > 0 && leng2.length > 0) {
                $("#<%=hif_EinvoiceRequest_Receiver.ClientID %>").val(leng1);
                $("#<%=hif_EinvoiceRequest_Address.ClientID %>").val(leng2);
                $('#EinvoiceRequestMessage').hide();
                unblock_ui();
                return true;
            }
            else {
                $('#EinvoiceRequestMessage').show();
                return false;
            }
        }

        //索取紙本發票
        function inserteinvoiceinfo() {
            $('#EinvoiceRequestMessage').hide();
        }


        function getsmsmobile(mode) {
            $("#SMSMobile").html($("#<%=hif_Mobile.ClientID %>").val());
            $("#SMSMobile2").html($("#<%=hif_Mobile.ClientID %>").val());
            $("#SMSAlreadyCount").html($("#<%=hif_SmsAlreadyCount.ClientID %>").val());
            $("#SMSRemainCount").html($("#<%=hif_SmsRemainCount.ClientID %>").val());
            $("#SMSAlreadyCount3").html($("#<%=hif_SmsAlreadyCount.ClientID %>").val());
            $("#SMSRemainCount3").html($("#<%=hif_SmsRemainCount.ClientID %>").val());
            if (mode == "2") {
                $('#sms_2').show();
                $('#sms_3').hide();
            }
            else if (mode == "3") {
                $('#sms_3').show();
                $('#sms_2').hide();
            }
        }

        function getdetailheight() {
            var h = $("#<%=pan_Detail.ClientID %>").height();
            $('#BLeft2').height(h);
            $('#pandetail').height(h);
            $('#panblock').height(0);
        }

        function slidingMenuToggle() {
            $("#wrap").toggleClass("mbe-menu-show");
        }

        function openAtAPP() {
            var d = $("#<%=hif_device.ClientID %>").val();
            if (d == "ios") {
                var start = new Date();
                window.setTimeout(function () {
                    if (new Date() - start > 2000) {
                        return;
                    }
                    document.location = "https://itunes.apple.com/tw/app/17life/id543439591";
                }, 1000);
                document.location = "open17life://www.17life.com/OrderDetail?orderguid=<%=OrderGuid%>";
            }
            else if (d == "android") {
                var start = new Date();
                window.setTimeout(function () {
                    if (new Date() - start > 2000) {
                        return;
                    }
                    document.location = "market://details?id=com.uranus.e7plife";
                }, 1000);
                document.location = "<%=string.Format("open17life://www.17life.com/OrderDetail?orderguid={0}",OrderGuid)%>";
            }
        }
        function gotoShipUrl(obj) {
            var shipNo = $(obj).parent().find("span#shipNo");
            copy(shipNo.html());
            var shipUrl = $(obj).parent().find("span#shipUrl");
            //window.open(shipUrl.html());
            $.confirm({
                type: 'red',
                title: '訊息 ',
                content: '物流單號已複製，前往網站查詢物流進度。',
                boxWidth: 300,
                useBootstrap: false,
                animation: 'zoom',
                closeAnimation: 'left',
                buttons: {
                    cancel: {
                        text: '取消'
                    }, specialKey: {
                        text: '開啟',
                        btnClass: 'btn-red any-other-class',
                        action: function () {
                            window.open(shipUrl.html());
                        }
                    }
                }
            });
        }
        function copy(s) {
            $('body').append('<textarea id="clip_area"></textarea>');

            var clip_area = $('#clip_area');

            clip_area.text(s);
            clip_area.select();

            document.execCommand('copy');

            clip_area.remove();
        }
        function requestPaper() {
            if ($('#hif_Addr').val() == '') {
                alert('通訊地址不完整，請盡速至【會員中心】>【個人資料設定】檢查並更新。')
            } else {
                alert('申請成功，發票將寄送至通訊地址：' + $('#hif_Addr').val());
            }
        }

        //註冊event
        window.onload = function () {

            for (i = 0; i < document.getElementsByClassName("history-hover").length; i++) {
                let history_hover_modal = document.getElementsByClassName("history-hover-modal")[i];
                let history_hover = document.getElementsByClassName("history-hover")[i];
                let cta_history_blue = document.getElementsByClassName("cta-2019-blue")[i];


                cta_history_blue.onmouseover = function () {
                    if (screen.width > 768) {
                        history_hover.classList.add("show")
                    }
                }
                cta_history_blue.onmouseout = function () {
                    if (screen.width > 768) {
                        history_hover.classList.remove("show")
                    }
                }
                cta_history_blue.onclick = function () {
                    if (screen.width <= 768) {
                        history_hover_modal.classList.toggle("active");
                    }
                }
                history_hover_modal.onclick = function () {
                    if (screen.width <= 768) {
                        history_hover_modal.classList.toggle("active");
                    }
                }
            }


        }

    </script>
    <asp:HiddenField ID="hif_device" runat="server" />
    <asp:HiddenField ID="hif_Mobile" runat="server" />
    <asp:HiddenField ID="hif_OrderDate" runat="server" />
    <asp:HiddenField ID="hif_ExpiredTime" runat="server" />
    <asp:HiddenField ID="hif_OrderEndDate" runat="server" />
    <asp:HiddenField ID="hif_ItemName" runat="server" />
    <asp:HiddenField ID="hif_Bid" runat="server" />
    <asp:HiddenField ID="hif_Oid" runat="server" />
    <asp:HiddenField ID="hif_OrderStatus" runat="server" />
    <asp:HiddenField ID="hif_GroupStatus" runat="server" />
    <asp:HiddenField ID="hif_BusinessHourStatus" runat="server" />
    <asp:HiddenField ID="hif_CancelStatus" runat="server" />
    <asp:HiddenField ID="hif_Dep" runat="server" />
    <asp:HiddenField ID="hif_Slug" runat="server" />
    <asp:HiddenField ID="hif_OrderMin" runat="server" />
    <asp:HiddenField ID="hif_SubTotal" runat="server" />
    <asp:HiddenField ID="hif_Total" runat="server" />
    <asp:HiddenField ID="hif_ItemPrice" runat="server" />
    <asp:HiddenField ID="hif_ItemOPrice" runat="server" />
    <asp:HiddenField ID="hif_EinvoiceRequest_Receiver" runat="server" />
    <asp:HiddenField ID="hif_EinvoiceRequest_Address" runat="server" />
    <asp:HiddenField ID="hif_CouponId" runat="server" />
    <asp:HiddenField ID="hif_SmsAlreadyCount" runat="server" />
    <asp:HiddenField ID="hif_SmsRemainCount" runat="server" />
    <asp:HiddenField ID="hif_CurrentQuantity" runat="server" />
    <asp:HiddenField ID="hif_DeliveryType" runat="server" />
    <asp:HiddenField ID="hif_Addr" runat="server" ClientIDMode="Static" />
    <asp:CheckBox ID="cbx_PanelType" runat="server" Checked="true" Visible="false" />
    <div class="block-background">
        <uc1:Paragraph ID="pMain" ExternalCache="true" runat="server" />
        <asp:RadioButtonList ID="rbl_PanelType" runat="server" Visible="false">
            <asp:ListItem Selected="True" Text="0" Value="0"></asp:ListItem>
            <asp:ListItem Text="1" Value="1"></asp:ListItem>
            <asp:ListItem Text="2" Value="2"></asp:ListItem>
        </asp:RadioButtonList>
        <%--訂單/憑證 明細--%>
        <asp:Panel ID="pan_Detail" runat="server">
            <div class="mc-navbar">
            </div>
            <div class="mc-content">
            <h1 class="rd-smll">訂單編號：

                <asp:Label CssClass="dis-blk" ID="lab_DetailOrderId" runat="server"></asp:Label>
                <label class="dis-blk" ><img <%=ShowLinePoints?"":"style='display:none'" %> src="../Themes/PCweb/images/icons/Line%20return@3x.png" width="95"/></label>
                    
                <asp:Label ID="lab_ReturnMessage" runat="server" Visible="false"></asp:Label>
                    <div class="rd-box" style="display: inline-block;">
                        <asp:Panel runat="server" ID="pan_Returns">
                            <input type="button" class="btn btn-mini" id="btnReturns" value="退貨申請">
                            <input type="button" class="btn btn-mini" id="btnExchange" value="換貨/缺件">
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pan_Cancel" Visible="false">
                            <input type="button" class="btn btn-mini" id="btnCancel" value="取消訂單">
                        </asp:Panel>
                    </div>
                    <%if (Request["oid"] != null)
                        { %>
                    <a id="link_CloseDetail" runat="server" href="coupon_List.aspx">回列表</a>
                    <%}
                        else
                        { %>
                    <asp:LinkButton ID="lbt_CloseDetail" runat="server" OnClick="ChangePanel">回列表</asp:LinkButton>
                    <%} %>
                </h1>
                <hr class="header_hr rd-header_hr" />
                <asp:HyperLink runat="server" ID="famiteaLink" Enabled="false" NavigateUrl="https://www.17life.com/event/famitea"></asp:HyperLink>
                <!--ATM付款-->
                <asp:Panel ID="pan_OrderDetailATM" runat="server" Visible="false">
                    <p class="order-atm-info">
                        感謝您以ATM付款方式訂購【<asp:Label ID="lab_ATM_ItemName2" runat="server"></asp:Label>】優質好康。
                        提醒您，請於下單購買當日的 <span class="important">
                            <asp:Label ID="lab_ATM_ExpiredDay2" runat="server"></asp:Label>
                            23:59:59</span> 前使用ATM或網路ATM完成付款，您的訂單才算交易成功喔! 若您超過這個時間匯款，ATM會顯示交易失敗，您的訂單將被取消。
                    </p>

                    <p class="order-atm-info">
                        提醒您：<br />
                        由於繳費帳號只限於ATM操作，無法臨櫃付款，<span style="color: #bf0000">若您的訂單付款金額超過 ATM 當日轉帳 30000 元的金額限制，請您改以使用 ATM 中的繳費功能進行，</span>
                        因每家銀行 ATM 中繳費項目不盡相同，建議您可以選擇 ATM 繳費項目中的信用卡繳費，或其他繳費項目，輸入帳號與金額後完成付款。

                    </p>
                    <h2 class="order-atm-header">訂單匯款資訊</h2>
                    <div class="grui-form">
                        <div class="form-unit">
                            <label for="" class="unit-label title">
                                銀行代碼</label>
                            <div class="data-input">
                                <p>
                                    中國信託商業銀行(822)
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for="" class="unit-label title">
                                銀行帳號</label>
                            <div class="data-input">
                                <p>
                                    <asp:Label ID="lab_ATM_Account" runat="server"></asp:Label>
                                    (共14碼)
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for="" class="unit-label title">
                                繳費金額</label>
                            <div class="data-input">
                                <p>
                                    <asp:Label ID="lab_ATM_Total" runat="server"></asp:Label>
                                    元
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for="" class="unit-label title">
                                付款期限</label>
                            <div class="data-input">
                                <p>
                                    <asp:Label ID="lab_ATM_ExpiredDay1" runat="server"></asp:Label>
                                    23:59:59 (請於下單購買當日的 23:59:59 前完成付款 )
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for="" class="unit-label title">
                                &nbsp;</label>
                            <div class="data-input">
                                <p>
                                    若您超過這個時間付款，ATM會顯示交易失敗，您的訂單將被取消。
                                </p>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <!--ATM付款(超過匯款期限)-->
                <asp:Panel ID="pan_OrderDetailATMFail" runat="server" Visible="false">
                    <p class="order-atm-info">
                        感謝您以ATM付款方式訂購【<asp:Label ID="lab_ATM_ItemName" runat="server"></asp:Label>】優質好康。
                        由於已經超過匯款期限，您的好康已經被取消囉！ 提醒您，避免影響您的好康權益，下次請盡早付款唷。
                    </p>
                </asp:Panel>
                <!--好康憑證-->
                <asp:Panel ID="pan_OrderDetailSequence" runat="server">
                    <div class="order-table-title">
                        <h1>好康憑證 <a href="http://get.adobe.com/tw/reader" target="_blank" class="rd-C-Hide">下載憑證專用PDF閱讀器</a>
                            <a href="#" onclick="block_ui($('#Pop_WhatIsTrust'),0,0);" style="cursor: pointer; text-decoration: underline;"
                                class="rd-C-Hide">信託讓您的消費更有保障！</a>
                        </h1>
                    </div>
                    <div id="mc-table" class="order-detail">
                        <asp:Repeater ID="rp_CouponListSequence" runat="server" OnItemDataBound="rp_SequenceItemBound"
                            OnItemCommand="rp_SequenceItemCommand">
                            <HeaderTemplate>
                                <table id="tab1" width="920px" border="0" cellspacing="0" cellpadding="0" class="rd-mcOrdetail">
                                    <thead>
                                        <tr class="rd-C-Hide">
                                            <th class="OrderNumber"></th>
                                            <th class="OrderItemName">商品名稱
                                            </th>
                                            <% if (IsFamiportGroupCoupon){%>
                                            <input type="hidden" id="isFamiportGroupCoupon" value="1">
                                            <th class="OrderSN">兌換店名</th>
                                            <th class="OrderCode">兌換店家地址</th>
                                            <th class="OrderEXP">憑證使用時間</th>
                                            <% }else if (IsHiLifeGroupCoupon){ %>
                                            <th class="OrderEXP">憑證使用時間</th>
                                            <% }else{%>
                                            <th class="OrderSN">
                                                <%= (GroupStatus & 2048) > 0 ? "兌換序號" : "憑證編號"%>  </th>
                                            <th class="OrderCode">確認碼</th>
                                            <%}%>
                                            <th class="OrderEXP">使用期限</th>
                                            <th class="OrderGetPon">索取憑證</th>
                                            <th class="OrderPonState">憑證狀態</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="mc-tableContentITEM">
                                    <td class="OrderNumber">
                                        <%# (Container.ItemIndex+1).ToString().PadLeft(2,'0')%>
                                    </td>
                                    <td class="OrderItemName text-left">
                                        <%# GetTrimToMaxByteLength(hif_ItemName.Value, 34, "...") %>
                                        <%# GetBranchName(((ViewCouponListSequence)(Container.DataItem)))%>
                                    </td>
                                    <% if (IsFamiportGroupCoupon) {%>
                                    <td class="OrderSN">
                                        <span class="rd-DisplayPanel rd-redColor">兌換店名:</span>
                                        <asp:Label ID="lab_famiStoreName" runat="server"></asp:Label><br />
                                    </td>
                                    <td class="OrderCode">
                                        <span class="rd-DisplayPanel rd-redColor">兌換店家地址:</span>
                                        <asp:Label ID="lab_famiStoreAddress" runat="server"></asp:Label><br />
                                    </td>
                                    <td class="OrderEXP">
                                        <span class="rd-DisplayPanel rd-redColor">憑證使用時間:</span>
                                        <asp:Label ID="lab_usageVerifiedTime" runat="server"></asp:Label><br />
                                    </td>
                                    <% } else if (IsHiLifeGroupCoupon){%>
                                    <td class="OrderEXP">
                                        <span class="rd-DisplayPanel rd-redColor">憑證使用時間:</span>
                                        <asp:Label ID="lab_hiLifeVerifiedTime" runat="server"></asp:Label><br />
                                    </td>
                                    <%}else{%>
                                    <td class="OrderSN">
                                        <span class="rd-DisplayPanel rd-redColor">憑證編號:</span>
                                        <% if (IsReverseVerify)
                                            { %>
                                             需下載ＡＰＰ才可進行對換
                                            <% }
                                                else
                                                {
                                            %>
                                        <%# ((GroupStatus&(int)GroupOrderStatus.PEZevent)>0&&(CancelStatus==(int)LunchKingSite.Core.ProgressStatus.Completed || CancelStatus==(int)LunchKingSite.Core.ProgressStatus.CompletedWithCreditCardQueued))? string.Empty:(GroupStatus & 2048) > 0 && CouponSeparateDigits(hif_Bid.Value) >0 ? LunchKingSite.BizLogic.Facade.CouponFacade.GetCouponSeparateCode(((ViewCouponListSequence)(Container.DataItem)).SequenceNumber,CouponSeparateDigits(hif_Bid.Value)):((ViewCouponListSequence)(Container.DataItem)).SequenceNumber%>
                                        <%
                                            }
                                        %>
                                    </td>
                                    <td class="OrderCode">
                                        <span class="rd-DisplayPanel rd-redColor">確認碼:</span>
                                        <% 
                                            if (!IsReverseVerify)
                                            {
                                        %>
                                        <asp:Label ID="lab_rpCouponCode" runat="server" Text="<%# ((GroupStatus&(int)GroupOrderStatus.PEZevent)>0&&(CancelStatus==(int)LunchKingSite.Core.ProgressStatus.Completed || CancelStatus==(int)LunchKingSite.Core.ProgressStatus.CompletedWithCreditCardQueued)||(GroupStatus & 2048) > 0)?string.Empty: ((ViewCouponListSequence)(Container.DataItem)).Code%>"></asp:Label>
                                        <%
                                            }
                                        %>
                                    </td>
                                    <% }%>

                                    <td class="OrderEXP">
                                        <span class="rd-DisplayPanel rd-redColor">使用期限:</span>
                                        <%# ExpiredTime.AddDays(-1).ToString("yyyy/MM/dd HH:mm") %>
                                        <%# GetFinalExpireDateContent(((ViewCouponListSequence)(Container.DataItem)).Guid)%>
                                        <asp:HiddenField ID="hif_rpCouponId" runat="server" Value="<%# ((ViewCouponListSequence)(Container.DataItem)).Id%>" />
                                    </td>

                                    <td class="OrderGetPon">
                                        <div class="order-get-coupon-box">
                                            <asp:HyperLink ID="hyp_rpCouponSequencePrint" runat="server" Target="_blank">
                                            <div class="order-get-coupon">
                                                <div class="order-get-coupon-icon c1">
                                                    <p>
                                                        列印</p>
                                                </div>
                                            </div>
                                            </asp:HyperLink>
                                            <asp:HyperLink ID="hyp_rpCouponSequenceDownLoad" runat="server">
                                            <div class="order-get-coupon">
                                                <div class="order-get-coupon-icon c2">
                                                    <p>
                                                        下載</p>
                                                </div>
                                            </div>
                                            </asp:HyperLink>
                                            <asp:HyperLink ID="hyp_email" runat="server">
                                            <div class="order-get-coupon">
                                                <div class="order-get-coupon-icon c3">
                                                    <p>
                                                        Email</p>
                                                </div>
                                            </div>
                                            </asp:HyperLink>
                                            <% if (!IsFamiportGroupCoupon && !IsHiLifeGroupCoupon)
                                                {%>
                                            <asp:LinkButton ID="lbt_rpCouponSequenceSMS" runat="server" CommandArgument="<%# ((ViewCouponListSequence)(Container.DataItem)).Id%>"
                                                CommandName="SendMS">
                                                <div class="order-get-coupon">
                                                    <div class="order-get-coupon-icon c4">
                                                        <p>
                                                            簡訊</p>
                                                    </div>
                                                </div>
                                            </asp:LinkButton>
                                            <% }%>
                                            <asp:HyperLink ID="hyp_OpenApp" runat="server">
                                            <div class="order-get-coupon">
                                                <div class="order-get-coupon-icon c5">
                                                    <p>開啟</p>
                                                </div>
                                            </div>
                                            </asp:HyperLink>
                                        </div>
                                    </td>
                                    <td id="tdPponState" class="OrderPonState fixed_width">
                                        <span class="rd-DisplayPanel rd-redColor">憑證狀態:</span>
                                        <asp:Label ID="lab_rpCouponSequenceStatus" runat="server"></asp:Label><br />
                                        <asp:LinkButton ID="hyp_Evaluate" runat="server" CssClass="btn btn-mini btn-green display_block rd-mc" Text="填評價★" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                    <a class="pop-download-app" href="https://www.17life.com/ppon/promo.aspx?cn=app" target="_blank" style="display: none"></a>
                </asp:Panel>
                <!--配送資訊-->
                <asp:Panel ID="pan_ShioInfo" runat="server">
                    <div class="order-table-title">
                        <h1>配送資訊
                        </h1>
                    </div>
                    <div id="mc-table" class="order-detail">
                        <table width="920px" border="0" cellspacing="0" cellpadding="0" class="rd-mcOrdetail">
                            <thead>
                                <tr class="rd-C-Hide">
                                    <th class="OrderAddressee">收件人
                                    </th>
                                    <th class="OrderAddrPhone">收件人電話
                                    </th>
                                    <th class="OrderAddrAdd">收件地址
                                    </th>
                                    <th class="OrderDeliverInfo text-left">廠商出貨資訊
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="mc-tableContentITEM">
                                    <td class="OrderAddressee">
                                        <span class="rd-DisplayPanel rd-redColor">收件人:</span>
                                        <asp:Label ID="lab_RecipientName" runat="server" />
                                    </td>
                                    <td class="OrderAddrPhone">
                                        <span class="rd-DisplayPanel rd-redColor">收件人電話:</span>
                                        <asp:Label ID="lab_RecipientTel" runat="server" />
                                    </td>
                                    <td class="OrderAddrAdd">
                                        <span class="rd-DisplayPanel rd-redColor">收件地址:<br />
                                        </span>
                                        <asp:Label ID="lab_DeliveryAddress" runat="server" />
                                        <div id="divSubDeliveryAddress" runat="server" visible="false">
                                        </div>
                                    </td>
                                    <td class="OrderDeliverInfo text-left">
                                        <span class="rd-DisplayPanel rd-redColor">廠商出貨資訊:</span>
                                        <asp:Literal ID="lit_ShippedInfo" runat="server" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </asp:Panel>
                <!--訂單明細-->
                <div class="order-table-title">
                    <h1>訂單明細
                    </h1>
                </div>
            <div id="mc-table" class="order-detail">
                <table width="920px" border="0" cellspacing="0" cellpadding="0" class="rd-mcOrdetail">
                    <thead>
                    <tr class="rd-C-Hide">
                        <th class="OrderDetailItemName text-left">商品名稱
                        </th>
                        <th class="OrderDetailSale text-right">原價
                        </th>
                        <th class="OrderDetailPrice text-right">好康價
                        </th>
                        <th class="OrderDetailAmount text-right">數量
                        </th>
                        <th class="OrderDetailSubTotal text-right">小計
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <asp:Repeater ID="rpt_Details" runat="server">
                        <ItemTemplate>
                            <tr class="mc-tableContentITEM">
                                <td class="OrderDetailItemName text-left">
                                    <span class="rd-DisplayPanel rd-redColor ">商品名稱</span><span class="rd-float-right">
                                        <%# ((ViewPponOrderDetail)(Container.DataItem)).ItemName%></span>
                                </td>
                                <td class="OrderDetailSale text-right">
                                    <% =hif_ItemOPrice.Value%>
                                </td>
                                <td class="OrderDetailPrice text-right">
                                    <span class="rd-DisplayPanel rd-redColor">好康價</span><span class="rd-float-right">
                                        <% =(ItemPrice==0&&( BusinessHourStatus & (int)LunchKingSite.Core.BusinessHourStatus.PriceZeorShowOriginalPrice) > 0) ? hif_ItemOPrice.Value : hif_ItemPrice.Value%></span>
                                </td>
                                <td class="OrderDetailAmount text-right">
                                    <span class="rd-DisplayPanel rd-redColor">數量</span><span class="rd-float-right">
                                        <%# ((ViewPponOrderDetail)(Container.DataItem)).ItemQuantity%></span>
                                </td>
                                <td class="OrderDetailSubTotal text-right">
                                    <%# ((ViewPponOrderDetail)(Container.DataItem)).OrderDetailTotal.Value.ToString("F0")%>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <tr class="mc-tableContentITEM">
                        <td colspan="5" class="OrderDetailTotal text-right">
                            <span class="rd-text-rg">
                                <p class="rd-C-Hide">
                                    運費<span class="price">
                                        <asp:Label ID="lab_ODInfo_Ship" runat="server"></asp:Label></span>
                                </p>
                                <p>
                                    <span class="rd-float-lft">總金額</span> <span class="price">
                                        <asp:Label ID="lab_ODInfo_TotalAmount" runat="server"></asp:Label></span>
                                </p>
                            </span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div style="text-align: right;<%=ShowLinePoints?"":"display:none;"%>">
                <a href="<%=LinePoinstDetailsLink%>"><u style="color: #08c;font-size:90%;text-decoration:none">點我了解更多返饋 LINE Points 細節</u></a>
            </div>
                <!--付款明細-->
                <asp:Panel ID="pan_OrderDetailPayments" runat="server">
                    <div class="order-table-title">
                        <h1>付款明細
                        </h1>
                    </div>
                    <div id="mc-table" class="order-detail">
                        <table width="920px" border="0" cellspacing="0" cellpadding="0" class="rd-mcOrdetail">
                            <thead>
                                <tr class="rd-C-Hide">
                                    <th class="OrderPayBonus text-right">紅利金扣抵
                                    </th>
                                    <th class="OrderPayCash text-right">購物金扣抵
                                    </th>
                                    <th class="OrderPayCoupon text-right">折價券抵用
                                    </th>
                                    <th class="OrderPayPEZ text-right" runat="server" id="cellpPayeasy">PayEasy購物金扣抵
                                    </th>
                                    <th class="OrderPayCredit text-right" runat="server" id="cellpCreditCard">刷卡
                                    </th>
                                    <th class="OrderPayAtm text-right" runat="server" id="cellpATM">ATM
                                    </th>
                                    <th class="OrderPayAtm text-right" runat="server" id="cellpFamilyIsp">全家取貨付款
                                    </th>
                                    <th class="OrderPayAtm text-right" runat="server" id="cellpSevenIsp">7-11取貨付款
                                    </th>
                                    <th class="OrderPayAtm text-right" <%=(ThirdPartyPaymentSystem == ThirdPartyPayment.None) ? "style='display:none'" : string.Empty %>>
                                        <%=Helper.GetEnumDescription(ThirdPartyPaymentSystem)%>
                                    </th>
                                    <th class="OrderPayTotal text-right">總計
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="OrderPayBonus text-right">
                                        <div class="rd-DisplayPanel rd-float-lft">
                                            紅利金扣抵
                                        </div>
                                        <asp:Label ID="lab_ODInfo_Bouns" runat="server"></asp:Label>
                                    </td>
                                    <td class="OrderPayCash text-right">
                                        <div class="rd-DisplayPanel rd-float-lft">
                                            購物金扣抵
                                        </div>
                                        17Life：<asp:Literal ID="litScashE7" runat="server" /><br />
                                        由Payeasy兌換：<asp:Literal ID="litScashPez" runat="server" />
                                    </td>
                                    <td class="OrderPayCoupon text-right">
                                        <div class="rd-DisplayPanel rd-float-lft">
                                            折價券抵用
                                        </div>
                                        <asp:Label ID="lab_ODInfo_Discount" runat="server"></asp:Label>
                                    </td>
                                    <td class="OrderPayPEZ text-right" runat="server" id="cellmPayeasy">
                                        <div class="rd-DisplayPanel rd-float-lft">
                                            PayEasy購物金扣抵
                                        </div>
                                        <asp:Label ID="lab_ODInfo_PEZ" runat="server"></asp:Label>
                                    </td>

                                    <td class="OrderPayCredit text-right" runat="server" id="cellmCreditCard">
                                        <div class="rd-DisplayPanel rd-float-lft">
                                            刷卡
                                        </div>
                                        <asp:Label ID="lab_ODInfo_CreditCard" runat="server"></asp:Label>
                                    </td>
                                    <td class="OrderPayAtm text-right" runat="server" id="cellmATM">
                                        <div class="rd-DisplayPanel rd-float-lft">
                                            ATM
                                        </div>
                                        <asp:Label ID="lab_ODInfo_ATM" runat="server"></asp:Label>
                                    </td>
                                    <td class="OrderPayAtm text-right" runat="server" id="cellmFamilyIsp">
                                        <div class="rd-DisplayPanel rd-float-lft">
                                            全家取貨付款
                                        </div>
                                        <asp:Label ID="lab_ODInfo_FamilyIsp" runat="server"></asp:Label>
                                    </td>
                                    <td class="OrderPayAtm text-right" runat="server" id="cellmSevenIsp">
                                        <div class="rd-DisplayPanel rd-float-lft">
                                            7-11取貨付款
                                        </div>
                                        <asp:Label ID="lab_ODInfo_SevenIsp" runat="server"></asp:Label>
                                    </td>
                                    <td class="OrderPayAtm text-right" <%=(ThirdPartyPaymentSystem == ThirdPartyPayment.None) ? "style='display:none'" : string.Empty %>>
                                        <div class="rd-DisplayPanel rd-float-lft">
                                            <%=Helper.GetEnumDescription(ThirdPartyPaymentSystem)%>
                                        </div>
                                        <asp:Label ID="lab_ODInfo_ThirdPartyPay" runat="server"></asp:Label>
                                    </td>
                                    <td class="OrderPayTotal text-right">
                                        <span class="rd-DisplayPanel rd-redColor rd-float-lft">總計</span><asp:Label ID="lab_ODInfo_Total"
                                            runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </asp:Panel>
                <!--退貨明細-->
                <asp:Panel ID="pan_OrderDetailRefund" runat="server" Visible="False">
                    <div class="order-table-title">
                        <h1>退貨明細
                            
                        </h1>
                    </div>
                    <div id="mc-table" class="order-detail">
                        <asp:Repeater ID="rpReturnForm" runat="server" OnItemDataBound="rpReturnFormItemBound">
                            <HeaderTemplate>
                                <table width="920px" border="0" cellspacing="0" cellpadding="0" class="rd-mcOrdetail">
                                    <thead>
                                        <tr class="rd-C-Hide">
                                            <th class="OrderReturnDate text-right">退貨申請日
                                            </th>
                                            <th class="OrderReturnAmount text-right">申請退貨數
                                            </th>
                                            <th class="OrderReturnAmount text-right">實際退貨數
                                            </th>
                                            <th class="OrderReturnCash text-right">退回金額
                                            </th>
                                            <th class="OrderReturnProcess text-right">紙本折讓狀態
                                            </th>
                                            <th class="OrderReturnProcess text-right">退貨進度
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="OrderPayPEZ text-right">
                                        <div class="rd-DisplayPanel rd-float-lft">退貨申請日</div>
                                        <%# ((ReturnFormLog)(Container.DataItem)).RequestTime%>
                                    </td>
                                    <td class="OrderPayPEZ text-right">
                                        <div class="rd-DisplayPanel rd-float-lft">申請退貨數</div>
                                        <%# ((ReturnFormLog)(Container.DataItem)).RequestedItems%>
                                    </td>
                                    <td class="OrderPayPEZ text-right">
                                        <div class="rd-DisplayPanel rd-float-lft">實際退貨數</div>
                                        <%# ((ReturnFormLog)(Container.DataItem)).ReturnedItems%><br />
                                    </td>
                                    <td class="OrderPayPEZ text-right OrderReturnCash">
                                        <div class="rd-DisplayPanel rd-float-lft">退回金額</div>
                                        <asp:Panel ID="pan_rpCash" runat="server" Visible="<%# ((ReturnFormLog)(Container.DataItem)).ShowCash%>">
                                            <p id="panelPcash" runat="server">
                                                <span class="item">PayEasy購物金：</span> <span class="cash">
                                                    <%# ((ReturnFormLog)(Container.DataItem)).Pcash.ToString("F0")%></span>
                                            </p>
                                            <p>
                                                <span class="item">紅利金：</span> <span class="cash">
                                                    <%# ((ReturnFormLog)(Container.DataItem)).Bcash.ToString("F0")%></span>
                                            </p>
                                            <p>
                                                <span class="item">購物金：</span> <span class="cash"></span>
                                            </p>
                                            <p>
                                                <span class="item" style="font-size: 11px">17Life：</span> <span class="cash">
                                                    <%# ((ReturnFormLog)(Container.DataItem)).Scash.ToString("F0")%></span>
                                            </p>
                                            <p>
                                                <span class="item" style="font-size: 11px">由Payeasy兌換：</span> <span class="cash">
                                                    <%# ((ReturnFormLog)(Container.DataItem)).Pscash.ToString("F0")%></span>
                                            </p>
                                            <p>
                                                <span class="item">刷退：</span> <span class="cash">
                                                    <%# ((ReturnFormLog)(Container.DataItem)).Ccash.ToString("F0")%></span>
                                            </p>
                                            <p>
                                                <span class="item">指定帳戶退款：</span> <span class="cash">
                                                    <%# ((ReturnFormLog)(Container.DataItem)).Atm.ToString("F0")%></span>
                                            </p>
                                            <p>
                                                <span class="item"><%=Helper.GetEnumDescription(ThirdPartyPaymentSystem)%>退款：</span> <span class="cash">
                                                    <%# ((ReturnFormLog)(Container.DataItem)).TCash.ToString("F0")%></span>
                                            </p>
                                        </asp:Panel>
                                        <br />
                                    </td>
                                    <td class="OrderPayPEZ text-right">
                                        <div class="rd-DisplayPanel rd-float-lft">紙本折讓狀態</div>
                                        <% if (IsPaperAllowance)
                                            { %>
                                        <%# ((ReturnFormLog)(Container.DataItem)).IsCreditNoteReceived ? "紙本折讓單已回" : (IsInvoicePapered ? "尚未收回" : "不須折讓") %>
                                        <% }
                                            else
                                            { %> 無須繳回 <% } %>
                                        <br />
                                        <div class="ReturnBottom return_modify">
                                            <asp:Button ID="btnReturnDiscountElec" runat="server" Text="電子折讓明細" CssClass="btn btn-middle btn-secondary" Visible="true" OnClientClick="btn_ReturnDiscountElec();return false;" />
                                        </div>
                                        <div class="ReturnBottom return_modify">
                                            <asp:HyperLink ID="hyp_DownLoadReturnDiscountPaper" runat="server" Text="下載紙本折讓單" CssClass="btn btn-middle btn-secondary"></asp:HyperLink>
                                        </div>
                                    </td>
                                    <td class="OrderPayPEZ text-right">
                                        <div class="rd-DisplayPanel rd-float-lft">退貨進度</div>
                                        <%# ((ReturnFormLog)(Container.DataItem)).Message%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody> </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </asp:Panel>
                <!--換貨明細-->
                <asp:Panel ID="pan_OrderDetailExchange" runat="server" Visible="False">
                    <div class="order-table-title">
                        <h1>換貨明細
                        </h1>
                    </div>
                    <div id="mc-table" class="order-detail">
                        <asp:Repeater ID="rptExchangeLog" runat="server">
                            <HeaderTemplate>
                                <table width="920px" border="0" cellspacing="0" cellpadding="0" class="rd-mcOrdetail">
                                    <thead>
                                        <tr class="rd-C-Hide">
                                            <th class="OrderReturnDate text-center">換貨申請日
                                            </th>
                                            <th class="OrderReturnAmount text-center">換貨原因
                                            </th>
                                            <th class="OrderReturnProcess text-center">換貨進度
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="OrderPayPEZ text-center">
                                        <div class="rd-DisplayPanel rd-float-lft">換貨申請日</div>
                                        <%# ((ReturnFormLog)(Container.DataItem)).RequestTime%>
                                    </td>
                                    <td class="OrderPayPEZ text-left">
                                        <div class="rd-DisplayPanel rd-float-lft">換貨原因</div>
                                        <%# ((ReturnFormLog)(Container.DataItem)).RequestedItems%>
                                    </td>
                                    <td class="OrderPayPEZ text-center">
                                        <div class="rd-DisplayPanel rd-float-lft">換貨進度</div>
                                        <%# ((ReturnFormLog)(Container.DataItem)).Message%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody> </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </asp:Panel>
                <!--發票明細-->
                <asp:HiddenField ID="hidDefaultBuyerName" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hidDefaultBuyerAddress" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hidPrintableEinvoiceNumbers" runat="server" ClientIDMode="Static" />
                <!--可申請紙本的發票; csv格示-->
                <asp:Panel ID="pan_EinvoiceMain" runat="server">
                    <!--發票明細WEB版 Start -->
                    <div class="order-table-title-invoice">
                        <h1>發票明細 <a id="downloadReceiptLink" runat="server" href="javascript:void(0)" class="rd-C-Hide" clientidmode="Static">購買證明</a>
                        </h1>
                    </div>
                    <div class="OrderDetailInvoice">
                        <asp:Panel ID="pan_EinvoiceDetail" runat="server">
                            <asp:Panel ID="NewInvoiceContainer" runat="server">
                                <asp:ListView ID="lvEinvoice" runat="server" OnItemDataBound="lvEinvoice_ItemDataBound">
                                    <LayoutTemplate>
                                        <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <div class="OrderDetailInvoiceInfor">
                                            <ul>
                                                <li>
                                                    <asp:Label ID="lblEinvoiceWinner" runat="server" Visible="false" Text="●本發票已中獎" Style="margin-left: 20px;" />
                                                    <asp:Label ID="lblEinvoicePaperRequest" runat="server" Visible="false" Text="●您已索取紙本發票" Style="margin-left: 20px;" />
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="OrDetElectricinvoiceForNewStyle">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td width="255" valign="top">
                                                            <table class="OrDetElectricinvoiceTableNew">
                                                                <tr class="OrDetInvoiceCenter">
                                                                    <td colspan="2" width="255">
                                                                        <span style="font-size: 14px;"><strong>
                                                                            <asp:Literal ID="litInvoiceHeader" runat="server" />數位整合股份有限公司<br></br>
                                                                            電子發票證明聯</strong>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" height="30" width="300"></td>
                                                                </tr>
                                                                <tr class="OrDetInvoiceCenter">
                                                                    <td colspan="2" width="255">
                                                                        <span style="font-size: 20px;">
                                                                            <asp:Literal ID="litInvoiceDateMon" runat="server" />
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <tr class="OrDetInvoiceCenter">
                                                                    <td colspan="2" width="255">
                                                                        <span style="font-size: 20px;">
                                                                            <asp:Literal ID="litInvoiceNumber" runat="server" Text='<%# Eval("InvoiceNumber") %>'></asp:Literal>
                                                                        </span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="150">
                                                                        <span style="font-size: 12px;">&nbsp;&nbsp;&nbsp;
                                                                <asp:Literal ID="litInvoiceDate" runat="server" />
                                                                        </span>
                                                                    </td>
                                                                    <td width="105"><span style="font-size: 12px;">格式:25</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="150"><span style="font-size: 12px;">&nbsp;&nbsp;&nbsp;隨機碼&nbsp;<asp:Literal ID="litInvoicePass" runat="server" /></td>
                                                                    <td width="105"><span style="font-size: 12px;">總計&nbsp;<asp:Literal ID="litTotalAmount" runat="server" /></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="150"><span style="font-size: 12px;">&nbsp;&nbsp;&nbsp;賣方&nbsp;24317014</span></td>
                                                                    <td width="105"><span style="font-size: 12px;">買方&nbsp;<asp:Literal ID="litComanyId" runat="server" Text='<%# Eval("InvoiceComId") %>' /></span></td>
                                                                </tr>
                                                                <tr height="200">
                                                                    <td colspan="2" class="OrDetInvoiceCenter" width="255">副本<br />
                                                                        <span style="color: red;">此副本僅供參考，<br />
                                                                            不可持本聯兌換。</span></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="15"></td>
                                                        <td width="530" valign="top">
                                                            <table class="OrDetElectricinvoiceTableNew">
                                                                <tr class="OrDetInvoiceCenter OrDetInvoiceBorderButtom">
                                                                    <td width="350">品 名</td>
                                                                    <td width="60" class="OrDetInvoiceBorderLeft">數 量</td>
                                                                    <td width="60" class="OrDetInvoiceBorderLeft">單 價</td>
                                                                    <td width="60" class="OrDetInvoiceBorderLeft">小 計</td>
                                                                </tr>
                                                                <tr height="50">
                                                                    <td width="350" style="text-align: left;" class="OrDetInvoiceBorderLeft">
                                                                        <asp:Literal ID="litItemName" runat="server" /><br />
                                                                        <br />
                                                                        &nbsp;訂單編號:<asp:Literal ID="litOrderId" runat="server" Text='<%# Eval("OrderId") %>' />
                                                                    </td>
                                                                    <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft"><span style="margin-right: 5px;">1</span></td>
                                                                    <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft"><span style="margin-right: 5px;">
                                                                        <asp:Literal ID="litItemUnitPrice" runat="server"></asp:Literal></span></td>
                                                                    <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft">
                                                                        <div class="OrDetETCInvoiceAmount">
                                                                            <asp:Literal ID="litItemSumAmount" runat="server"></asp:Literal>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr height="50">
                                                                    <td width="350" style="text-align: left;" class="OrDetInvoiceBorderLeft">課稅別：
                                                                        <asp:Literal ID="litHasTax" runat="server" /><asp:Literal ID="litNoTax" runat="server" /></td>
                                                                    <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft"><span style="margin-right: 5px;">1</span></td>
                                                                    <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft">0</td>
                                                                    <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft">0</td>
                                                                </tr>
                                                                <tr height="50">
                                                                    <td width="350" style="text-align: left;" class="OrDetInvoiceBorderLeft">稅額</td>
                                                                    <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft"><span style="margin-right: 5px;">1</span></td>
                                                                    <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft">
                                                                        <asp:Literal ID="litTaxAmount" runat="server" /></td>
                                                                    <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft">
                                                                        <asp:Literal ID="litTaxAmount2" runat="server" /></td>
                                                                </tr>
                                                                <tr height="200">
                                                                    <td width="350" class="OrDetInvoiceBorderLeft"></td>
                                                                    <td width="60" class="OrDetInvoiceBorderLeft"></td>
                                                                    <td width="60" class="OrDetInvoiceBorderLeft"></td>
                                                                    <td width="60" class="OrDetInvoiceBorderLeft"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </ItemTemplate>
                                </asp:ListView>
                            </asp:Panel>
                            <asp:Panel ID="OldInvoiceContainer" runat="server">
                                <div class="OrderDetailInvoiceInfor">
                                    <ul>
                                        <li>
                                            <asp:Label ID="lblEinvoiceWinner" runat="server" Text="本發票已中獎" Visible="false" Style="margin-left: 20px;" />
                                            <asp:Label ID="lblEinvoicePaperRequest" runat="server" Text="您已索取紙本發票" Visible="false" Style="margin-left: 20px;" />
                                        </li>
                                    </ul>
                                </div>
                                <div class="OrDetElectricinvoiceForNewStyle">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td width="255" valign="top">
                                                    <table class="OrDetElectricinvoiceTableNew">
                                                        <tr class="OrDetInvoiceCenter">
                                                            <td colspan="2" width="255">
                                                                <span style="font-size: 14px;"><strong>
                                                                    <asp:Label ID="lab_Einvoice_Head" runat="server"></asp:Label>數位整合股份有限公司<br></br>
                                                                    電子發票證明聯</strong>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" height="30" width="300"></td>
                                                        </tr>
                                                        <tr class="OrDetInvoiceCenter">
                                                            <td colspan="2" width="255">
                                                                <span style="font-size: 20px;">
                                                                    <asp:Label ID="lbl_Einvoice_InvoiceDate_Mon" runat="server"></asp:Label>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr class="OrDetInvoiceCenter">
                                                            <td colspan="2" width="255">
                                                                <span style="font-size: 20px;">
                                                                    <asp:Label ID="lbl_Einvoice_InvoiceNum" runat="server"></asp:Label>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="150">
                                                                <span style="font-size: 12px;">&nbsp;&nbsp;&nbsp;
                                                                <asp:Label ID="lbl_Einvoice_InvoiceDate" runat="server"></asp:Label>
                                                                </span>
                                                            </td>
                                                            <td width="105"><span style="font-size: 12px;">格式:25</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="150"><span style="font-size: 12px;">&nbsp;&nbsp;&nbsp;隨機碼&nbsp;<asp:Literal ID="litEinvoicePass" runat="server" /></span></td>
                                                            <td width="105"><span style="font-size: 12px;">總計&nbsp;<asp:Label ID="lbl_Einvoice_OrderAmount3" runat="server"></asp:Label></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="150"><span style="font-size: 12px;">&nbsp;&nbsp;&nbsp;賣方&nbsp;24317014</span></td>
                                                            <td width="105"><span style="font-size: 12px;">買方&nbsp;<asp:Label ID="lbl_Einvoice_ComId" runat="server"></asp:Label></span></td>
                                                        </tr>
                                                        <tr height="200">
                                                            <td colspan="2" class="OrDetInvoiceCenter" width="255">副本<br />
                                                                <span style="color: red;">此副本僅供參考，<br />
                                                                    不可持本聯兌換。</span></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="15"></td>
                                                <td width="530" valign="top">
                                                    <table class="OrDetElectricinvoiceTableNew">
                                                        <tr class="OrDetInvoiceCenter OrDetInvoiceBorderButtom">
                                                            <td width="350">品 名</td>
                                                            <td width="60" class="OrDetInvoiceBorderLeft">數 量</td>
                                                            <td width="60" class="OrDetInvoiceBorderLeft">單 價</td>
                                                            <td width="60" class="OrDetInvoiceBorderLeft">小 計</td>
                                                        </tr>
                                                        <tr height="50">
                                                            <td width="350" style="text-align: left;" class="OrDetInvoiceBorderLeft">
                                                                <asp:Label ID="lbl_Einvoice_ItemName" runat="server"></asp:Label><br />
                                                                <br />
                                                                &nbsp;訂單編號:<asp:Label ID="lbl_Einvoice_OrderId" runat="server"></asp:Label>
                                                            </td>
                                                            <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft"><span style="margin-right: 5px;">1</span></td>
                                                            <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft"><span style="margin-right: 5px;">
                                                                <asp:Label ID="lbl_Einvoice_OrderAmount1" runat="server"></asp:Label></span></td>
                                                            <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft">
                                                                <div class="OrDetETCInvoiceAmount">
                                                                    <asp:Label ID="lbl_Einvoice_OrderAmount2" runat="server"></asp:Label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr height="50">
                                                            <td width="350" style="text-align: left;" class="OrDetInvoiceBorderLeft">課稅別：
                                                                        <asp:Label ID="lbl_Einvoice_HasTax" runat="server"></asp:Label>
                                                                <asp:Label ID="lbl_Einvoice_NoTax" runat="server"></asp:Label></td>
                                                            <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft"><span style="margin-right: 5px;">1</span></td>
                                                            <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft">0</td>
                                                            <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft">0</td>
                                                        </tr>
                                                        <tr height="50">
                                                            <td width="350" style="text-align: left;" class="OrDetInvoiceBorderLeft">稅額</td>
                                                            <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft"><span style="margin-right: 5px;">1</span></td>
                                                            <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft">
                                                                <asp:Label ID="lbl_Einvoice_TaxAmount" runat="server"></asp:Label></td>
                                                            <td width="60" style="text-align: right;" class="OrDetInvoiceBorderLeft">
                                                                <asp:Label ID="lbl_Einvoice_TaxAmount2" runat="server"></asp:Label></td>
                                                        </tr>
                                                        <tr height="200">
                                                            <td width="350" class="OrDetInvoiceBorderLeft"></td>
                                                            <td width="60" class="OrDetInvoiceBorderLeft"></td>
                                                            <td width="60" class="OrDetInvoiceBorderLeft"></td>
                                                            <td width="60" class="OrDetInvoiceBorderLeft"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </asp:Panel>
                            <div class="order-get-invoice" onclick="inserteinvoiceinfo();block_ui($('#Pop_PaperRequest2'),0,0);"
                                runat="server" id="btn_RequestPaperInvoice">
                                <input type="button" class="btn btn-large" value="索取紙本發票" />
                            </div>
                            <uc:EinvoiceFooter ID="invoiceFooter" runat="server" Version="Old" DealType="LkSite" />
                        </asp:Panel>
                        <asp:Panel ID="pan_EinvoiceInfo" runat="server">
                            <div class="OrderDetailInvoiceInText">
                                <asp:Label ID="lbl_Einvoice_Others" runat="server"></asp:Label>
                            </div>
                        </asp:Panel>
                        <div class="clearfix">
                        </div>
                    </div>
                    <!--發票明細WEB版 End -->
                    <!--發票明細M版 Start -->
                    <div class="m-order-table-title">
                        <h1>發票明細</h1>
                    </div>
                    <div class="m-OrderDetailInvoice">
                        <asp:ListView ID="lvmEinvoice" runat="server" OnItemDataBound="lvmEinvoice_ItemDataBound">
                            <ItemTemplate>
                                <div class="m-OrDetElectricinvoice">
                                    <ul>
                                        <li>開立日期：中華民國<asp:Literal ID="litInvoiceDate" runat="server" /></li>
                                        <li>發票號碼：<asp:Literal ID="litInvoiceNumber" runat="server" Text='<%# Eval("InvoiceNumber") %>'></asp:Literal></li>
                                        <li>發票金額：<asp:Literal ID="litTotalAmount" runat="server" /></li>
                                    </ul>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                        <asp:Panel ID="pan_mEinvoiceInfo" CssClass="m-OrDetElectricinvoice" runat="server">
                            <ul>
                                <li>
                                    <asp:Label ID="lbl_mEinvoice_Others" runat="server"></asp:Label></li>
                            </ul>
                        </asp:Panel>
                        <div class="clearfix"></div>
                    </div>
                    <!--發票明細M版 End -->

                </asp:Panel>
                <!-- 收據資料-->
                <asp:Panel ID="pan_Receipt" runat="server">
                    <div class="order-table-title">
                        <h1>收據資料說明
                        </h1>
                    </div>
                    <div id="mc-table" class="order-detail">
                        <table width="920px" border="0" cellspacing="0" cellpadding="0" class="rd-mcOrdetail">
                            <tr>
                                <td class="text-left">
                                    <p class="info">
                                        <asp:CheckBox ID="chkPhysicalReceipt" runat="server" Text="您已索取收據資料" Enabled="false" />
                                    </p>
                                    <p class="info">
                                        <span id="pKindDeal" runat="server">&nbsp;※&nbsp;您的捐款收據將由公益團體寄出。<br />
                                            &nbsp;※&nbsp;有關 17Life 公益活動請見：<a href="/ppon/NewbieGuide.aspx" target="_blank">常見問題</a>
                                        </span>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </div>
        </asp:Panel>
    </div>
    <%--甚麼是信託【blockUI遮罩用】--%>
    <div id="Pop_WhatIsTrust" class="pop-window" style="text-align: left; display: none;">
        <div class="pop-content">
            <h1>信託讓您的消費更有保障！</h1>
            <hr>
            <p>
                2011/07/18 起，您購買 17Life 購物金所支付給 17Life 的金額，將存入 17Life （康太數位整合股份有限公司）於台新銀行所開立之信託專戶，專款專用；所稱專用，係指供
                17Life 履行交付商品或提供服務義務使用。
            </p>
            <p>
                17Life 在此提供您個人被信託保障中的品項查詢。在您確實使用掉 17Life 購物金之前（包括但不限於到店家使用以17Life購物金兌換的好康憑證），我們將提供給您全面的信託保障，讓您安心購買，搶好康！
            </p>
            <div>
                <asp:Repeater ID="rp_TrustCouponList" runat="server" OnItemDataBound="rp_TrustSequenceItemBound">
                    <HeaderTemplate>
                        <table width="480px" border="0" cellspacing="0" cellpadding="0" class="rd-mcOrdetail">
                            <thead>
                                <tr class="rd-C-Hide">
                                    <th class="OrderSN">信託碼
                                    </th>
                                    <th class="">信託金額
                                    </th>
                                    <th class="OrderPonState">使用狀況
                                    </th>
                                    <th class="OrderPonState">核銷時間
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="">
                            <td class="OrderSN">
                                <%# ((GroupStatus&(int)GroupOrderStatus.PEZevent)>0&&(CancelStatus==(int)LunchKingSite.Core.ProgressStatus.Completed || CancelStatus==(int)LunchKingSite.Core.ProgressStatus.CompletedWithCreditCardQueued))?string.Empty: ((CashTrustLog)(Container.DataItem)).TrustSequenceNumber%>
                            </td>
                            <td class="">
                                <%# ((CashTrustLog)(Container.DataItem)).Amount == 0 ? "--" : "$" + (((CashTrustLog)(Container.DataItem)).Amount - ((CashTrustLog)(Container.DataItem)).DiscountAmount).ToString("F0") %>
                            </td>
                            <td class="OrderPonState">
                                <asp:Label ID="Label3" runat="server"></asp:Label>
                                <asp:HiddenField ID="HiddenField1" runat="server" Value="<%# ((CashTrustLog)(Container.DataItem)).CouponId%>" />
                            </td>
                            <td class="">
                                <%# ((CashTrustLog)(Container.DataItem)).UsageVerifiedTime%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody></table>
                    </FooterTemplate>
                </asp:Repeater>
                <p>註1：信託碼不等於憑證，此處為本張訂單進入信託行的資料</p>
            </div>
        </div>
        <div class="MGS-XX" onclick="unblock_ui();" style="cursor: pointer">
            <input type="button" class="btn btn-large" value="關閉">
        </div>
    </div>
    <%--索取紙本發票【blockUI遮罩用】--%>
    <div id="Pop_PaperRequest2" class="pop-window" style="display: none">
        <div class="pop-content">
            <h1>索取紙本發票</h1>
            <hr>
            <p>
                請正確填寫收件人名稱與收件地址，我們將依您所提供的資訊寄出紙本發票。
            </p>
            <div class="grui-form">
                <div class="form-unit">
                    <label for="" class="unit-label rd-unit-label">
                        收件人</label>
                    <div class="data-input rd-data-input">
                        <asp:TextBox ID="tbx_Einvoice_Receiver" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-unit">
                    <label for="" class="unit-label rd-unit-label">
                        地址</label>
                    <div class="data-input rd-data-input">
                        <asp:TextBox ID="tbx_Einvoice_Address" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <p id="EinvoiceRequestMessage" class="error">
                    請正確填寫收件資訊！
                </p>
                <asp:LinkButton ID="lbt_Einvoice_PaperRequest" runat="server" ForeColor="White" CssClass="btn btn-large btn-primary"
                    OnClick="RequestPaperInvoice" OnClientClick="return paperinvoicecheck();" Text="送出索取"></asp:LinkButton>
            </div>
        </div>
        <div class="MGS-XX" onclick="unblock_ui();" style="cursor: pointer">
            <input type="button" class="btn btn-large" value="關閉">
        </div>
    </div>
    <%--索取憑證-SMS【blockUI遮罩用】--%>
    <div id="Pop_SMS1" class="pop-window" style="text-align: left; display: none">
        <div class="pop-content">
            <h1>索取簡訊憑證</h1>
            <hr>
            <p class="important highlight">
                <span id="SMSMobile"></span>
            </p>
            <p>
                您的簡訊憑證，將會被傳送到上方所顯示的號碼，確定接收的號碼正確嗎？
            </p>
            <p>
                注意事項：
            </p>
            <ul>
                <li>索取發送簡訊憑證是免費的，您不需額外支付任何費用。</li>
                <li>每則好康簡訊憑證最多可索取發送三次。</li>
                <li>您已索取此則簡訊憑證<span id="SMSAlreadyCount"></span>次，尚可索取 <span id="SMSRemainCount"></span>
                    次。</li>
            </ul>
            <div class="text-center">
                <asp:LinkButton ID="lbt_Pop_SmsConfirm" runat="server" CssClass="btn btn-large btn-primary"
                    OnCommand="SendCouponSms" ForeColor="#ffffff">確認索取</asp:LinkButton>
                <a class="btn btn-large" href="../User/UserAccount.aspx">修改號碼</a>
            </div>
        </div>
        <div class="MGS-XX" onclick="blockUIClose(this);" style="cursor: pointer">
            <input type="button" class="btn btn-large" value="關閉">
        </div>
    </div>
    <%--索取憑證-SMS-Fail【blockUI遮罩用】--%>
    <div id="Pop_SMS3" class="pop-window" style="text-align: left; display: none">
        <div class="pop-content">
            <div id="sms_2">
                <p class="important">
                    您已索取超過三次，無法再索取！
                </p>
            </div>
            <div id="sms_3">
                <h1>發送成功！</h1>
                <hr>
                <p class="important">
                    您的簡訊憑證已被發送至：<span id="SMSMobile2"></span>
                </p>
            </div>
            <p>
                注意事項：
            </p>
            <ul>
                <li>索取發送簡訊憑證是免費的，您不需額外支付任何費用。</li>
                <li>每則好康簡訊憑證最多可索取發送三次。</li>
                <li>您已索取此則簡訊憑證<span id="SMSAlreadyCount3"></span>次，尚可索取<span id="SMSRemainCount3"></span>次。</li>
            </ul>
            <div class="text-center">
                <input type="button" class="btn btn-large btn-primary pop-confirm" onclick="blockUIClose(this);"
                    value="確定">
            </div>
        </div>
        <div class="MGS-XX" onclick="blockUIClose(this);" style="cursor: pointer">
            <input type="button" class="btn btn-large" value="關閉">
        </div>
    </div>
    <%--索取憑證-Email【blockUI遮罩用】--%>
    <div id="Pop_MailCoupon" class="pop-window" style="display: none">
        <div id="email_pop_content" class="pop-content">
            <h1>索取憑證寄到您的電子信箱</h1>
            <hr>
            <p>
                請確認憑證寄達的電子信箱
            </p>
            <div class="grui-form">
                <div class="form-unit">
                    <label for="" class="unit-label rd-unit-label">
                        電子信箱</label>
                    <div class="data-input rd-data-input">
                        <input type="text" id="userEmail" name="userEmail" placeholder="Example@17life.com"
                            value="<%=this.UserEmail %>" />
                    </div>
                </div>
            </div>
            <div class="text-center">
                <p id="error_email" class="error" style="display: none;">
                    電子信箱格式錯誤！
                </p>
                <a href="javascript:void(0)" id="btn_MailCoupon" class="btn btn-large btn-primary send"
                    style="color: #ffffff;">確認寄送</a>
            </div>
        </div>
        <div id="email_pop_result" class="pop-content" style="display: none">
            <h1>索取憑證寄到您的電子信箱</h1>
            <hr>
            <p>
                寄送成功，請至 <span id="email_pop_content_email"></span>收取信件，並下載附件檔。
            </p>
            <p>
                若您尚未安裝PDF閱讀器，請在先 <a href="http://get.adobe.com/tw/reader/" target="_blank">下載</a>
                安裝
            </p>
        </div>
        <div class="MGS-XX" style="cursor: pointer" onclick="blockUIClose(this);">
            <input type="button" class="btn btn-large" value="關閉">
        </div>
    </div>
    <%--電子折讓單彈出畫面【blockUI遮罩用】--%>
    <div id="Pop_rptForm" style="display: none; width: 100%; height: 100%; overflow: auto">
        <div class="MGS-XX" onclick="unblock_ui();" style="cursor: pointer; background: url(../Themes/PCweb/images/A2-Multi-grade-Setting_xx.png); height: 28px; width: 27px; position: absolute; top: -28px; right: -27px; z-index: 800;">
        </div>
        <asp:Panel ID="pan_NoData" runat="server" Visible="false">
            <div class="ReturnApplicationForm">
                <div class="ReAppFoInfo">
                    <div class="ReAppFpOffLine">
                        <div class="ReAppFoContent1">
                            <div class="ReAppFoTittle">
                                <asp:Label ID="lab_NoData" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pan_Data" runat="server">
            <asp:Repeater ID="rpt_Form" runat="server">
                <ItemTemplate>
                    <div class="ReAppFoInfo" style="border: 1px solid #333333;">
                        <div class="ReAppFoContent3">
                            <!--Invoice--1---ST----->
                            <div class="ReAppFoInvoice">
                                <table border="0" align="center" cellpadding="0" cellspacing="0" class="ReAppFoCont3Table1_3" style="border-top-style: none">
                                    <tr>
                                        <td align="center" height="10"></td>
                                    </tr>
                                    <tr>
                                        <td align="center" height="29" style="font-size: 21px;">
                                            <span style="font-weight: bold; font-family: Arial, Helvetica, sans-serif;">
                                                <%# ((RefundDiscountListClass)(Container.DataItem)).CouponSequence%>
                                            </span>退回或折讓證明單
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" height="23" style="font-size: 17px;">(<%# ((RefundDiscountListClass)(Container.DataItem)).DealNumber%>)訂單編號：
                                            <%# ((RefundDiscountListClass)(Container.DataItem)).OrderId%>
                                            <img id="iVC" alt="" src="<%# ResolveUrl("~/service/barcode39.ashx") + "?id="+ ((RefundDiscountListClass)(Container.DataItem)).OrderId %>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" height="7"></td>
                                    </tr>
                                </table>
                                <table border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table">
                                    <tr>
                                        <td width="379" rowspan="2">
                                            <table width="359" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_1">
                                                <tr>
                                                    <td width="78" align="right" style="padding-right: 6px;">
                                                        <span style="letter-spacing: 3.2em;">名</span>稱
                                                    </td>
                                                    <td width="288" align="center">
                                                        <%# (((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.IsBetween(ProviderFactory.Instance().GetConfig().ContactToPayeasy,ProviderFactory.Instance().GetConfig().PayeasyToContact.AddDays(-1))) ? "康迅數位整合股份有限公司" : "康太數位整合股份有限公司" %> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left; padding-left: 10px;">統&nbsp;&nbsp;一&nbsp;編&nbsp;&nbsp;號
                                                    </td>
                                                    <td align="center">
                                                        <%# (((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.IsBetween(ProviderFactory.Instance().GetConfig().ContactToPayeasy,ProviderFactory.Instance().GetConfig().PayeasyToContact.AddDays(-1))) ? ProviderFactory.Instance().GetConfig().PayeasyCompanyId : ProviderFactory.Instance().GetConfig().ContactCompanyId %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" style="padding-right: 6px;">
                                                        <span style="letter-spacing: 3.2em;">地</span>址
                                                    </td>
                                                    <td align="center">台北市中山區中山北路一段11號13樓
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <%if (IsPaperAllowance)
                                        { %>
                                        <td width="344" height="60" align="center" style="font-size: 20px;">
                                            <strong>營業人銷貨退回或折讓證明單</strong>
                                        </td>
                                        <%} %>
                                        <%else
                                        { %>
                                        <td width="344" height="60" align="center" style="font-size: 15px;">
                                            <strong>電子發票銷貨退回、進貨退出或折讓證明單證明聯</strong>
                                        </td>
                                        <%} %>
                                    </tr>
                                    <tr>
                                        <td align="center" style="line-height: 18px;">
                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumberDate_Year%>
                                            &nbsp;&nbsp; 年 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumberDate_Month%>
                                            &nbsp;&nbsp; 月 &nbsp;&nbsp;&nbsp;&nbsp&nbsp;
                                                <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumberDate_Day%>
                                            &nbsp;&nbsp; 日
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right" height="18" style="line-height: 18px; padding-right: 33px; color: #506A00;">第ㄧ聯：交付原銷貨人作為銷項稅額之扣減憑證
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ReAppFoCont3Table1_2">
                                                <tr>
                                                    <td colspan="6" height="20">開立發票
                                                    </td>
                                                    <td colspan="5">退貨或折讓內容
                                                    </td>
                                                    <td colspan="3" rowspan="2">(打V處)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <%if (IsPaperAllowance)
                                                    { %>
                                                    <td width="3%" rowspan="2">聯式
                                                    <%} %>
                                                        <%else
                                                        { %>
                                                        <td width="3%" rowspan="2">一般/特種
                                                    <%} %>
                                                            <td width="6%" rowspan="2">年
                                                            </td>
                                                            <td width="3%" rowspan="2">月
                                                            </td>
                                                            <td width="3%" rowspan="2">日
                                                            </td>
                                                            <td width="4%" rowspan="2">字<br />
                                                                軌
                                                            </td>
                                                            <td width="11%" rowspan="2">號碼
                                                            </td>
                                                            <td width="18%" rowspan="2">品名
                                                            </td>
                                                            <td width="5%" rowspan="2">數量
                                                            </td>
                                                            <td width="6%" rowspan="2">單價
                                                            </td>
                                                            <td colspan="2">退出或折讓
                                                            </td>
                                                </tr>
                                                <tr>
                                                    <td width="19%">金額(不含稅之進貨額)
                                                    </td>
                                                    <td width="9%">營業稅額
                                                    </td>
                                                    <td width="4%">應稅
                                                    </td>
                                                    <td width="6%">零稅率
                                                    </td>
                                                    <td width="3%" style="line-height: 16px;">免稅
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="20">
                                                        <%if (IsPaperAllowance)
                                                        { %>
                                                        <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceMode%>
                                                        <%} %>
                                                        <%else
                                                        { %>
                                                            一
                                                        <%} %>
                                                    </td>
                                                    <td>
                                                        <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.Year%>
                                                    </td>
                                                    <td>
                                                        <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.Month%>
                                                    </td>
                                                    <td>
                                                        <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceDate.Day%>
                                                    </td>
                                                    <td>&nbsp;
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumber.Substring(0,2)%>
                                                    </td>
                                                    <td>&nbsp;
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceNumber.Substring(2,8)%>
                                                    </td>
                                                    <td style="text-align: left; padding-left: 4px;">
                                                        <%# ((RefundDiscountListClass)(Container.DataItem)).EinvoiceItemName%>
                                                    </td>
                                                    <td>1
                                                    </td>
                                                    <td>
                                                        <%# ((RefundDiscountListClass)(Container.DataItem)).ItemAmount%>
                                                    </td>
                                                    <td>$
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemNoTaxAmount%>
                                                    </td>
                                                    <td>
                                                        <%# ((RefundDiscountListClass)(Container.DataItem)).ItemTax%>
                                                    </td>
                                                    <td>
                                                        <%# ((RefundDiscountListClass)(Container.DataItem)).IsTax ? "v" : "&nbsp;" %>
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                    <td>
                                                        <%# ((RefundDiscountListClass)(Container.DataItem)).IsTax ? "&nbsp;" :"v"  %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="9" height="20">合計
                                                    </td>
                                                    <td>$
                                                            <%# ((RefundDiscountListClass)(Container.DataItem)).ItemNoTaxAmount%></td>
                                                    <td>$
                                                        <%# ((RefundDiscountListClass)(Container.DataItem)).ItemTax%></td>
                                                    <td>&nbsp;
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <span class="ReAppFoText1">本證明單所列銷貨退回進貨退出或折讓，確屬事實，特此證明。</span>
                            </div>
                            <%--<hr style="width: 700px; margin: 15px auto;" />--%>
                            <!--Invoice--1--END-->
                        </div>
                    </div>
                    <%# ((RefundDiscountListClass)(Container.DataItem)).breakstring%>
                    <br />
                    <br />
                </ItemTemplate>
            </asp:Repeater>
            <div style="text-align: center" class="ReturnBottom return_modify">
                <asp:HyperLink ID="hyp_DownLoadReturnDiscount" runat="server" Text="下載此明細" CssClass="btn btn-middle btn-secondary"></asp:HyperLink>
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </asp:Panel>
    </div>
    <%--索取購買證明【blockUI遮罩用】--%>
    <div id="Pop_Receipt" class="card-panel bpopup-modal" style="overflow-y: auto; height: 335px; display: none; background-color: white; text-align: center;">
        <div class="MGS-XX" onclick="unblock_ui();" style="cursor: pointer; background: url(../Themes/PCweb/images/A2-Multi-grade-Setting_xx.png); height: 28px; width: 27px; position: absolute; top: -28px; right: -27px; z-index: 800;">
        </div>
        <iframe id="ifRecept" scrolling="no" runat="server" frameborder="0"
            height="290" width="590"></iframe>
        <br />
        <a href="../service/receipt.ashx?oid=<%= Request.QueryString["oid"] %>">
            <input type="button" value="列印" /></a>
        <asp:LinkButton Visible="false" ID="lbRequestPaper" Text="申請紙本" runat="server" OnClick="lbRequestPaper_Click" OnClientClick="requestPaper();" />
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.Core.UI;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.Web.User
{
    public partial class FamiCouponDetail : BasePage, IFamiCouponDetailView
    {
        #region props

        private FamiCouponDetailPresenter _presenter;
        public FamiCouponDetailPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public bool EnableFami3Barcode { set; get; }
        public CouponCodeType CodeType { set; get; }

        public string PinCode { set; get; }

        public string UserName
        {
            get { return string.IsNullOrEmpty(Request.QueryString["u"]) ? Page.User.Identity.Name : Request.QueryString["u"]; }
        }

        public int? CouponId
        {
            get
            {
                int id;
                return int.TryParse(Request.QueryString["cid"], out id) ? new int?(id) : null;
            }
        }

        public bool Pos 
        {
            get 
            {
                bool isPos = false;
                return bool.TryParse(Request.QueryString["pos"], out isPos) ? isPos : false;
            }
        }

        public string SiteUrl { get; set; }

        #endregion


        public IFamiportProvider famip = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
        public IHiLifeProvider hiLife = ProviderFactory.Instance().GetProvider<IHiLifeProvider>();
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();
         

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) 
            {
                if (!this.Presenter.OnViewInitialized()) 
                {
                    Response.Redirect("~/Ppon/default.aspx");
                    return;
                }
                this.Presenter.OnViewLoaded();
            }
        }

        public void SetFamiCouponContent(ViewPponCoupon coupon, IViewPponDeal vpd)
        {
            hidCouponId.Value = coupon.CouponId.ToString();
            imgBtnApp.OnClientClick = "GotoApp('" + SiteUrl + "');return false;";

            CodeType = (CouponCodeType)Enum.ToObject(typeof(CouponCodeType), vpd.CouponCodeType);
            
            if ((Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon) && (vpd.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0))
            {
                if ((vpd.GroupOrderStatus & (int) GroupOrderStatus.HiLifeDeal) > 0)
                {
                    CodeType = CouponCodeType.HiLifeItemCode;
                }
                else
                {
                    FamilyNetEvent ed = famip.GetFamilyNetEvent(vpd.BusinessHourGuid);
                    if (ed != null && ed.Version == (int)FamilyBarcodeVersion.FamiSingleBarcode)
                    {
                        CodeType = CouponCodeType.FamiSingleBarcode;
                    }
                    else
                    {
                        CodeType = CouponCodeType.FamiItemCode;//咖啡成套票券
                    }
                }
            }

            switch (CodeType)
            {
                case CouponCodeType.Pincode:
                    if (EnableFami3Barcode)
                    {
                        litCouponCode.Text = string.Format(@"<div class='Coupon_Header_Shop'>{0}</div>
                        <div class='Coupon_Header_Info'>優惠有效期間：{1} ~ {2}</div>"
                            , coupon.CouponUsage
                            , ((DateTime)coupon.BusinessHourDeliverTimeS).ToString("yyyy/MM/dd")
                            , ((DateTime)coupon.BusinessHourDeliverTimeE).ToString("yyyy/MM/dd"));

                        FamiposBarcode famipos = OrderFacade.FamiposBarcodeGetCreate(coupon.OrderDetailGuid, coupon.BusinessHourGuid, vpd);
                        if (famipos.IsLoaded)
                        {
                            litBarcodeImg.Text = string.Format(@"<img src='../service/barcode39.ashx?id={0}&mini=true' /><br />
                            <font size='1'>*{0}*</font><br />
                            <img src='../service/barcode39.ashx?id={1}&mini=true' /><br />
                            <font size='1'>*{1}*</font><br />
                            <img src='../service/barcode39.ashx?id={2}&mini=true' /><br />
                            <font size='1'>*{2}*</font>", famipos.Barcode1, famipos.Barcode2, famipos.Barcode3);
                        }
                        else
                        {
                            litBarcodeImg.Text = "憑證發生錯誤";
                        }
                    }
                    else
                    {
                        if (!Pos)
                        {
                            litCouponCode.Text = string.Format("<div class='Coupon_fami_pincode'>紅利PIN碼：{0}</div>", coupon.CouponCode);
                            litCouponNote.Text = "※ 請於兌換期限內至全省全家便利商店門市使用FamiPort依照步驟輸入PIN碼並取得單據後，至店內拿取欲兌換之商品，持繳款單據至櫃檯完成結帳動作，即可享有優惠價購買商品(恕不提供網路付款)";
                        }
                        else
                        {
                            FamiposBarcode famipos = OrderFacade.FamiposBarcodeGetCreate(coupon.OrderDetailGuid, coupon.BusinessHourGuid, vpd);
                            litCouponCode.Text = string.Format(@"<div class='Coupon_fami_text'>優惠條碼</div>
                            <div class='Coupon_fami_barcode'>
                                <img src='../service/barcode39.ashx?id={0}&mini=true' /><br />
                                <font size='1'>*{0}*</font>
                            </div>
                            <div class='Coupon_fami_barcode'>
                                <img src='../service/barcode39.ashx?id={1}&mini=true' /><br />
                                <font size='1'>*{1}*</font>
                            </div>
                            <div class='Coupon_fami_barcode'>
                                <img src='../service/barcode39.ashx?id={2}&mini=true' /><br />
                                <font size='1'>*{2}*</font>
                            </div>
                            ", famipos.Barcode1, famipos.Barcode2, famipos.Barcode3);
                        }

                        lblCouponUsage.Text = coupon.CouponUsage;
                        lblEventName.Text = coupon.EventName;
                        lblDeliverTimeStart.Text = ((DateTime)coupon.BusinessHourDeliverTimeS).ToString("yyyy/MM/dd");
                        lblDeliverTimeEnd.Text = ((DateTime)coupon.BusinessHourDeliverTimeE).ToString("yyyy/MM/dd");
                    }
                    break;
                case CouponCodeType.BarcodeEAN13:
                    litCouponCode.Text = string.Format(@"<div class='Coupon_fami_text'>優惠條碼</div>
                    <div class='Coupon_fami_barcode'>
                        <img src='{0}/service/barcodeean13.ashx?code={1}&size=1.5&width=200&hight=80&barcodehight=21' />
                    </div>", SiteUrl, coupon.CouponCode);
                    litCouponNote.Text = "※ 請於兌換期限內至全省全家便利商店門市使用，於店內拿取欲兌換之商品，持條碼請店員刷條碼結帳，即可享有優惠價購買商品(恕不提供網路付款)";

                    if (EnableFami3Barcode)
                    {
                        FamiposBarcode famipos = OrderFacade.FamiposBarcodeGetCreate(coupon.OrderDetailGuid, coupon.BusinessHourGuid, vpd);
                        if (famipos.IsLoaded)
                        {
                            litBarcodeImg.Text = string.Format(@"<img src='../service/barcode39.ashx?id={0}&mini=true' /><br />
                            <font size='1'>*{0}*</font><br />
                            <img src='../service/barcode39.ashx?id={1}&mini=true' /><br />
                            <font size='1'>*{1}*</font><br />
                            <img src='../service/barcode39.ashx?id={2}&mini=true' /><br />
                            <font size='1'>*{2}*</font>
                        ", famipos.Barcode1, famipos.Barcode2, famipos.Barcode3);
                        }
                        else
                        {
                            litBarcodeImg.Text = "憑證發生錯誤";
                        }
                    }
                    else
                    {
                        lblCouponUsage.Text = coupon.CouponUsage;
                        lblEventName.Text = coupon.EventName;
                        lblDeliverTimeStart.Text = ((DateTime)coupon.BusinessHourDeliverTimeS).ToString("yyyy/MM/dd");
                        lblDeliverTimeEnd.Text = ((DateTime)coupon.BusinessHourDeliverTimeE).ToString("yyyy/MM/dd");
                    }

                    break;
                case CouponCodeType.FamiItemCode://咖啡成套票券
                    litCouponCode.Text = string.Format(@"<div class='Coupon_Header_Shop'>{0}</div>
                        <div class='Coupon_Header_Info'>優惠有效期間：{1} ~ {2}</div>"
                           , coupon.CouponUsage
                           , ((DateTime)coupon.BusinessHourDeliverTimeS).ToString("yyyy/MM/dd")
                           , ((DateTime)coupon.BusinessHourDeliverTimeE).ToString("yyyy/MM/dd"));

                    var famiNetPincode = famip.GetFamilyNetPincodeInfo(coupon.OrderGuid, coupon.CouponId.Value);
                    if (famiNetPincode!=null)
                    {
                        PinCode = famiNetPincode.PezCode;
                        //讀QueryString測試用
                        //litBarcodeImg.Text += string.Format(@"<img style='height: 40px' src='../service/barcodeean13.ashx?code={0}&size="+ Request.QueryString["size"] + "&width=" + Request.QueryString["width"] + "&hight=80&barcodehight=21' />"+
                        //"<br /><br />", famiNetPincode.Barcode1);
                        litBarcodeImg.Text += string.Format(@"<img style='height: 40px' src='../service/barcodeean13.ashx?code={1}&size=2.5&width=350&hight=80&barcodehight=21' /><br />
                        <br />", famiNetPincode.Barcode1, famiNetPincode.Barcode1);

                        List<string> barcodeList = new List<string>();
                        if (!string.IsNullOrEmpty(famiNetPincode.Barcode2))
                            barcodeList.Add(famiNetPincode.Barcode2);
                        if (!string.IsNullOrEmpty(famiNetPincode.Barcode3))
                            barcodeList.Add(famiNetPincode.Barcode3);
                        if (!string.IsNullOrEmpty(famiNetPincode.Barcode4))
                            barcodeList.Add(famiNetPincode.Barcode4);

                        foreach(string barcode in barcodeList)
                        {
                            litBarcodeImg.Text += string.Format(@"<img style='height: 40px; width:250px' src='../service/barcode39.ashx?id={0}&mini=true' /><br />
                            <font size='1'>*{0}*</font><br />", barcode);
                        }
                    }
                    break;
                case CouponCodeType.HiLifeItemCode://萊爾富成套咖啡
                    litCouponCode.Text = string.Format(@"<div class='Coupon_Header_Shop'>{0}</div>
                        <div class='Coupon_Header_Info'>優惠有效期間：{1} ~ {2}</div>"
                           , coupon.CouponUsage
                           , ((DateTime)coupon.BusinessHourDeliverTimeS).ToString("yyyy/MM/dd")
                           , ((DateTime)coupon.BusinessHourDeliverTimeE).ToString("yyyy/MM/dd"));

                    var p = hiLife.GetHiLifePincode(coupon.SequenceNumber);
                    if (p != null)
                    {
                      
                        List<string> barcodeList = new List<string>();
                        if (!string.IsNullOrEmpty(config.HiLifeFixedFunctionPincode))
                            barcodeList.Add(config.HiLifeFixedFunctionPincode);

                        if (!string.IsNullOrEmpty(p.Pincode.ToUpper()))
                            barcodeList.Add(p.Pincode.ToUpper());
                      
                        foreach (string barcode in barcodeList)
                        {
                            litBarcodeImg.Text += string.Format(@"<img style='height: 40px; width:250px' src='../service/barcode39.ashx?id={0}&mini=true' /><br />
                            <font size='1'>*{0}*</font><br />", barcode);
                        }
                    }
                    break;
                case CouponCodeType.FamiSingleBarcode:
                    litCouponCode.Text = string.Format(@"<div class='Coupon_Header_Shop'>{0}</div>
                        <div class='Coupon_Header_Info'>優惠有效期間：{1} ~ {2}</div>"
                           , coupon.CouponUsage
                           , ((DateTime)coupon.BusinessHourDeliverTimeS).ToString("yyyy/MM/dd")
                           , ((DateTime)coupon.BusinessHourDeliverTimeE).ToString("yyyy/MM/dd"));

                    var famiNetPincodeForSingleBarcode = famip.GetFamilyNetPincodeInfo(coupon.OrderGuid, coupon.CouponId.Value);
                    if (famiNetPincodeForSingleBarcode!=null)
                    {
                        PinCode = famiNetPincodeForSingleBarcode.PezCode;
                        if (!string.IsNullOrEmpty(famiNetPincodeForSingleBarcode.Barcode2))
                        {
                            litBarcodeImg.Text += string.Format(@"<img style='height: 40px; width:250px' src='../service/barcode39.ashx?id={0}&mini=true' /><br />
                            <font size='1'>*{0}*</font><br />", famiNetPincodeForSingleBarcode.Barcode2);
                        }
                    }
                    break;
            }
        }
    }
}
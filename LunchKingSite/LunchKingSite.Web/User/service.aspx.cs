﻿using LunchKingSite.Core;
using System;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.Web.User
{
    public partial class service : Page
    {
        #region Property

        private static IMemberProvider mp;

        public string OrderId
        {
            get
            {
                if (string.IsNullOrEmpty(Request.QueryString["orderid"]))
                    return string.Empty;

                return Request.QueryString["orderid"];
            }
        }

        #endregion Property

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("/User/ServiceList");
        }

        protected static string GetShareText(Guid bid)
        {
            if (HttpContext.Current.Session[LkSiteSession.ExternalMemberId.ToString()] == null)
            {
                MemberUtility.SetUserSsoInfoReady(MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name));
            }

            ExternalMemberInfo emi = HttpContext.Current.Session[LkSiteSession.ExternalMemberId.ToString()] as ExternalMemberInfo;

            if (emi != null)
            {
                string origUrl = WebUtility.GetSiteRoot() + "/ppon/PponReferralRedirection.aspx?bid=" + bid + "," + (int)emi[0].Source + "|" + emi[0].ExternalId;
                string shortUrl = WebUtility.RequestShortUrl(origUrl);
                PromotionFacade.SetDiscountReferrerUrl(origUrl, shortUrl, MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name), HttpContext.Current.User.Identity.Name, bid, 0);
                return shortUrl;
            }
            else
            {
                return string.Empty;
            }
        }

        [WebMethod]
        public static string GetShareLink(string businessHourId)
        {
            LogManager.GetLogger("service.aspx.cs").Warn("GetShareLink Obsolete.");
            if (HttpContext.Current.User == null || !HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return string.Empty;
            }
            else
            {
                Guid bid = Guid.TryParse(businessHourId, out bid) ? bid : Guid.Empty;
                return GetShareText(bid);
            }
        }

        [WebMethod]
        public static string PostToFaceBook(string businessHourId)
        {
            if ((HttpContext.Current.User == null || !HttpContext.Current.User.Identity.IsAuthenticated) ||
                !(new DefaultPponPresenter()).AddUserTrack("", "", "/ppon/default.aspx", "", "FaceBook"))
            {
                return string.Empty;
            }
            else
            {
                Guid bid = Guid.TryParse(businessHourId, out bid) ? bid : Guid.Empty;
                GetShareText(bid);
                return "Y";
            }
        }

        [WebMethod]
        public static string GetServiceMessageCategory(int parentId)
        {
            var serviceCates = mp.ServiceMessageCategoryCollectionGetListByParentId(parentId).ToList();
            return new JsonSerializer().Serialize(serviceCates);
        }


    }
}
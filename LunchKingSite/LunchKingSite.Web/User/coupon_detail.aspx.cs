﻿using System;
using System.Web.Security;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Component;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace LunchKingSite.Web.User
{
    public partial class coupon_detail : LocalizedBasePage, ICouponDetailView
    {
        #region props

        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        private CouponDetailPresenter _presenter;
        public CouponDetailPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return string.IsNullOrEmpty(Request.QueryString["u"]) ? Page.User.Identity.Name : Request.QueryString["u"]; }
        }

        public int? CouponId
        {
            get
            {
                int id;
                return int.TryParse(Request.QueryString["cid"], out id) ? new int?(id) : null;
            }
        }

        public string GoogleApiKey { protected get; set; }

        public string EncryptedCouponCode
        {
            set
            {
                iVC.ImageUrl = ResolveUrl("~/service/barcode.ashx") + "?code=" + value;
                iQC.ImageUrl = ResolveUrl("~/service/qrcode.ashx") + "?code=" + value;
            }
        }

        public bool ForceStaticMap
        {
            get { return !string.IsNullOrEmpty(Request.QueryString["s"]); }
        }

        public Guid? PreviewBizHourId
        {
            get
            {
                Guid? ret = null;
                if (!string.IsNullOrEmpty(Request.QueryString["bid"]))
                {
                    try { ret = new Guid?(new Guid(Request.QueryString["bid"])); }
                    catch { }
                }
                return ret;
            }
        }

        protected string MapAddress { get; set; }

        public string SequenceNumber
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["sequencenumber"]))
                    return Request.QueryString["sequencenumber"];
                else
                    return string.Empty;
            }
        }
        public string CouponCode
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["couponcode"]))
                    return Request.QueryString["couponcode"];
                else
                    return string.Empty;
            }
        }
        public string MemberName
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["membername"]))
                    return Request.QueryString["membername"];
                else
                    return string.Empty;
            }
        }

        public CouponCodeType CodeType { set; get; }

        public string DealTags
        {
            set { litDealTags.Text = value; }
        }

        public string AvailablesInfo { set; get; }
        public bool IsZeroActivityShowCoupon { get; set; }

        public string CreateTime
        {
            get
            {
                if (string.IsNullOrEmpty(Request.QueryString["ct"])) return null;
                try
                {
                    return new DateTime(Int64.Parse(Request.QueryString["ct"])).ToString("yyyy/MM/dd HH:mm:ss.fff"); 
                }
                catch
                {
                    return null;
                }
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                if (!_presenter.OnViewInitialized())
                {
                    Response.Redirect("default.aspx");
                    return;
                }
            _presenter.OnViewLoaded();
        }

        #region ICouponDetailView Methods
        public void SetDetail(ViewPponCoupon coupon, IViewPponDeal vpd)
        {
            string refoundmessage = string.Empty;//"●若憑證過期後尚未使用，可申請退貨，並將酌收$30手續費；提醒您儘量於期限內使用完畢。";
            //hN.Text = coupon.ItemName + "<br /><br />" + coupon.EventName;// coupon.EventName;
            hN.Text = Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon) ? PponFacade.GetDealPdfItemName(vpd.BusinessHourGuid) : OrderFacade.GetOrderDetailItemName(OrderDetailItemModel.Create(coupon),
                                                         new PponItemNameShowOption { ShowPhone = true, StoreInfoInParenthesis = true });
            hNB.Text = vpd.EventName;
            if (Request.QueryString["preview"] != null)
            {
                if (Request.QueryString["preview"] == "setup")
                {
                    string newEventName = PponFacade.PponNewContentGet(vpd.BusinessHourGuid);
                    hNB.Text = newEventName;
                }
            }
            hNB.Visible = !Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon);

            hN.NavigateUrl = ResolveUrl("~/ppon/detail.aspx") + @"?bid=" + coupon.BusinessHourGuid;
            //lUS.Text = lus2.Text = ""; coupon.BusinessHourDeliverTimeS.Value.ToShortDateString();
            //lUE.Text = lue2.Text = ""; coupon.BusinessHourDeliverTimeE.Value.ToShortDateString();
            DateTime minDate = GetMinExpireDate(coupon);
            if (minDate != coupon.BusinessHourDeliverTimeE && minDate != DateTime.MaxValue)
            {
                ChangedExpireDate.Text = ChangedExpireDate2.Text = minDate.ToString("yyyy/MM/dd");
                if (StrikeThroughDecoration != null) StrikeThroughDecoration.Style.Add("text-decoration", "line-through"); //coupon_print.aspx 無 StrikeThroughDecoration
                StrikeThroughDecoration2.Style.Add("text-decoration", "line-through");
            }
            else
            {
                ChangeExpireDateContent.Visible = false;
                ChangeExpireDateContent2.Visible = false;
            }

            lU.Text = coupon.MemberName;


            if (coupon.BusinessHourOrderTimeS < new DateTime(2011, 3, 31))
            {
                lR.Text = (coupon.Introduction + ("<br/>●此憑證面額: $" + coupon.ItemUnitPrice.ToString("N0") + " 整"));
            }
            else
            {
                if (string.IsNullOrEmpty(coupon.Introduction))
                {
                    lR.Text = refoundmessage;
                }
                else
                {
                    if (coupon.Introduction.EndsWith("</div>"))
                    {
                        lR.Text = coupon.Introduction + refoundmessage;
                    }
                    else
                    {
                        lR.Text = coupon.Introduction + "<br />" + refoundmessage;
                    }
                }
            }
            lR.Text = Regex.Replace(lR.Text, @"<[/]?(font|span|xml|del|ins|[ovwxp]:\w+)[^>]*?>", "", RegexOptions.IgnoreCase);

            // 信託銀行文字說明
            TrustProvider trustProvider = Helper.GetBusinessHourTrustProvider(coupon.BusinessHourStatus);
            switch (trustProvider)
            {
                case TrustProvider.TaiShin:
                    liTaishin.Visible = true;
                    break;
                case TrustProvider.Sunny:
                    liMohist.Visible = true;
                    break;
                case TrustProvider.Hwatai:
                    liHwatai.Visible = true;
                    break;
                case TrustProvider.Initial:
                default:
                    break;
            }
            
            liEntrustSell.Visible = (EntrustSellType)vpd.EntrustSell.GetValueOrDefault() != EntrustSellType.No;

            pan_Trust.Visible = coupon.ItemUnitPrice != 0;
            if ((coupon.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0)
            {
                lCS.Text = string.Format(I18N.Message.FamiPortPINCouponFormat, coupon.SequenceNumber);
                lab_Notice.Text = "※ 請於兌換期限內至全省全家便利商店門市使用FamiPort依照步驟輸入PIN碼並取得單據後" +
                    "，至店內拿取欲兌換之商品，持繳款單據至櫃台完成結帳動作，即可以優惠價帶走您所購買的商品(恕不提供網路付款)";
            }
            else if ((coupon.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0)
            {
                lCS.Text = string.Format(I18N.Message.PEZeventCouponFormat, coupon.SequenceNumber);
            }
            else
            {
                lCS.Text = "憑證編號：" + coupon.SequenceNumber;
                liCouponCode.Text = "確認碼：" + coupon.CouponCode;
                lab_Notice2.Text = "※ 每張憑證可換乙份優惠商品或折抵金額";
                lab_Notice.Text = "※ 如非確認預約或使用，請勿隨意提供確認碼予他人，避免影響您的使用權益。";
                liGroupCoupon.Visible = Helper.IsFlagSet(coupon.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon);
            }
            iVC.AlternateText = coupon.SequenceNumber + "-" + coupon.CouponCode;
            lUse.Text = coupon.Introduction;
            lSl.Text = coupon.SellerName;
            lSl.Text = Regex.Replace(lSl.Text, @"<[/]?(font|span|xml|del|ins|[ovwxp]:\w+)[^>]*?>", "", RegexOptions.IgnoreCase); //e
            if (coupon.StoreGuid != null)
            {
                if (coupon.CouponStoreSequence == null)
                {
                    CSSC.Text = string.Empty;
                    CSSC.Visible = false;
                    StoreSequenceCode.Visible = false;
                }
                else
                {
                    CSSC.Text = coupon.CouponStoreSequence.Value.ToString("0000");
                    CSSC.Visible = true;
                    StoreSequenceCode.Visible = true;
                }
            }
            //lV.Text = coupon.BusinessHourOrderTimeS < new DateTime(2011, 3, 31) ? ("此憑證面額: $" + coupon.ItemUnitPrice.ToString("N0") + " 整") : string.Empty;
            //通用券檔次不顯示商店資訊
            if (Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore))
            {
                pan_ShopInfo.Visible = false;
            }
            else
            {
                pan_ShopInfo.Visible = true;
                AvailableInformation[] stores = ProviderFactory.Instance().GetSerializer().Deserialize<AvailableInformation[]>(AvailablesInfo);
                if (stores != null && stores.Length == 1)
                {
                    MapAddress = stores[0].A;   // if we only have 1 place to go, draw google map on the page
                    if (stores.Any(x => string.Compare(coupon.StoreName, x.N, true) == 0))
                        avc.Availablity = stores.Where(x => coupon.StoreName.Equals(x.N, StringComparison.OrdinalIgnoreCase)).ToArray();
                    else
                        avc.Availablity = stores;
                }
               
                avc.DataBind();  
            }
        }

        /// <summary>
        /// 四個日期中 (賣家倒店日, 店家倒店日, 賣家調整截止日, 店家調整截止日), 根據商業邏輯取出最小日期. 若四個日期都沒有設定, 則會回傳 DateTime.MaxValue.
        /// </summary>
        /// <param name="coupon"></param>
        /// <returns></returns>
        private DateTime GetMinExpireDate(ViewPponCoupon coupon)
        {
            // 截止日期: 分店優先賣家(檔次)
            DateTime changedExpireDate = coupon.StoreChangedExpireDate ?? coupon.BhChangedExpireDate ?? DateTime.MaxValue;

            // 結束營業日: 分店優先賣家
            DateTime closeDownDate = coupon.StoreCloseDownDate ?? coupon.SellerCloseDownDate ?? DateTime.MaxValue;

            // min(截止日期, 結束營業日)
            DateTime minDate = new DateTime((Math.Min(changedExpireDate.Ticks, closeDownDate.Ticks)));

            return minDate;
        }
        #endregion
    }
}

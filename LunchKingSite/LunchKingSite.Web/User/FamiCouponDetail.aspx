﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FamiCouponDetail.aspx.cs" Inherits="LunchKingSite.Web.User.FamiCouponDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>17Life</title>
    <link rel="stylesheet" href="../Themes/PCweb/css/MasterPage.css" type="text/css" />
    <link id="coupon_print" href="../Themes/default/images/17Life/G2/coupon_print_V2.css" rel="stylesheet" type="text/css" />
    <script src="../Tools/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (getParameterByName('print') == 'true') {
                $('.pon-fct').hide();
                var timeout = 2000;
                setTimeout("window.print()", timeout);
            }
        });
        function GotoApp(siteUrl) {
            window.open(siteUrl + '/ppon/promo.aspx?cn=app', '_blank');
        }
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
        function PrintFamiBarcode() {
            var cid = $('#hidCouponId').val();
            window.open('FamiCouponDetail.aspx?print=true&cid=' + cid);
        }
        function DownloadFamiBarcode() {
            var cid = $('#hidCouponId').val();
            window.open('../User/CouponGet.aspx?cid=' + cid, '_blank');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="Coupon">
            <div class="Coupon_Header">
                <div class="Coupon_Header_Logo">
                    <img src="../Themes/default/images/17Life/G2/17PLOGO_BW.png" width="168" height="80" alt="" />
                </div>
                <asp:Literal ID="litCouponCode" runat="server"></asp:Literal>
                <div class="Clear"></div>
            </div>
            <div class="Coupon_Seriel"></div>
            <%--litBarcodeImg--%>
            <%if ((CodeType == LunchKingSite.Core.CouponCodeType.Pincode && EnableFami3Barcode) || CodeType == LunchKingSite.Core.CouponCodeType.FamiItemCode 
                    || CodeType == LunchKingSite.Core.CouponCodeType.HiLifeItemCode || CodeType == LunchKingSite.Core.CouponCodeType.FamiSingleBarcode)
              { %>
                <div class="Coupon_fami_barcode" >
                    <asp:Literal ID="litBarcodeImg" runat="server" Text=""></asp:Literal>
                </div>
            <%}%>

            <%--Coupon_Note--%>
            <%if (CodeType == LunchKingSite.Core.CouponCodeType.Pincode && EnableFami3Barcode)
              {%>
                   <div class="Coupon_Note" >
                    ※ 請於兌換期限內至全省全家便利商店門市使用，於店內拿取欲兌換之商品，持條碼請店員刷條碼結帳，即可享有優惠價購買商品(恕不提供網路付款)<br>
                    <span class="FontBold">※ 條碼無法正常讀取，請使用FamiPort輸入紅利PIN碼「<%=PinCode%>」列印後，即可持單據至櫃檯完成結帳動作。</span><br>
                    ※ 列印小白單步驟說明：使用 Famifport → 點選「紅利」 → 點選「17Life」 → 同意條款 → 輸入「紅利pin碼」 → 確認商品資訊 → 列印小白單 → 至櫃檯結帳。<br>
                    ※ 刷取手機條碼結帳，店舖可不回收相關單據。
                </div>
            <%}else if (CodeType == LunchKingSite.Core.CouponCodeType.FamiItemCode)
               {%>
                <div class="Coupon_Note" >
                    ＊如無法掃描請來電至全家客服，並且告知此杯紅利PIN碼編號，一杯有一組紅利PIN碼編號，如需兌換多杯要告知每杯的紅利PIN碼編號。<br />
                    全家客服電話<span style="color:blue">0800-071-999</span><br />
                    ＊打完後即可利用全家FamiPort輸入紅利PIN碼「<span style="color:red"><%=PinCode%>」</span>列印後，請持單據至櫃檯完成結帳動作。<br />
                </div>
             <%}else if (CodeType == LunchKingSite.Core.CouponCodeType.FamiSingleBarcode)
               {%>
                <div class="Coupon_Note" >
                    ＊如無法掃描請來電至全家客服，並且告知此杯紅利PIN碼編號，一杯有一組紅利PIN碼編號，如需兌換多杯要告知每杯的紅利PIN碼編號。<br />
                    全家客服電話<span style="color:blue">0800-071-999</span><br />
                    ＊打完後即可利用全家FamiPort輸入紅利PIN碼「<span style="color:red"><%=PinCode%>」</span>列印後，請持單據至櫃檯完成結帳動作。<br />
                </div>
            <%} else if (CodeType == LunchKingSite.Core.CouponCodeType.HiLifeItemCode) { %>
             <div class="Coupon_Note" >
                    ＊如無法掃描請來電至萊爾富客服，並告知此杯咖啡PIN碼編號，<br />
                      一杯有一組PIN碼編號，如需兌換多杯則要告知每杯咖啡的PIN碼編號。<br />
                      萊爾富客服專線：0800-022-118<br />
                </div>
            <%} else {%>
                <div class="Coupon_Shop" >
                    <asp:Literal ID="lblCouponUsage" runat="server" Text=""></asp:Literal>
                </div>
                <div class="Coupon_Title">
                    <asp:Literal ID="lblEventName" runat="server" Text=""></asp:Literal>
                </div>
                <div class="Coupon_Info">
                    優惠有效期間：
                    <asp:Literal ID="lblDeliverTimeStart" runat="server" Text=""></asp:Literal>
                            ~ 
                    <asp:Literal ID="lblDeliverTimeEnd" runat="server" Text=""></asp:Literal>
                </div>
                <div class="Coupon_Note" >
                    <asp:Literal ID="litCouponNote" runat="server"></asp:Literal>
                </div>
            <%} %>
        </div>
        <div class="pon-fct">
            <p class="btn-center">
                <asp:Button ID="btnDownload" OnClientClick="DownloadFamiBarcode();return false;" CssClass="btn btn-large btn-primary rd-payment-xlarge-btn" runat="server" Text="下載優惠條碼" />
                <asp:Button ID="btnPrint" OnClientClick="PrintFamiBarcode();return false;" CssClass="btn btn-large btn-primary rd-payment-xlarge-btn" runat="server" Text="列印優惠條碼" />
                <asp:HiddenField ID="hidCouponId" ClientIDMode="Static" runat="server" />
            </p>
            <asp:ImageButton ID="imgBtnApp" Style="max-width: 100%; height: auto;" ImageUrl="../Themes/default/images/17Life/G2/fami-appdl.jpg" runat="server" />
        </div>
    </form>
</body>
</html>


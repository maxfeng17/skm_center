﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FamiMapTest.aspx.cs" Inherits="LunchKingSite.Web.FamiMapTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <style>
        input {
            width: 100%
        }

        .row {
            border-top: dashed 2px blue;
            padding: 10px 5px 10px 5px;
        }
    </style>
</head>
<body>
    <div class="container">


        <form id="form1" runat="server" class="form-horizontal">
            <asp:HiddenField ID="hidCvstemp" runat="server" />
            <asp:HiddenField ID="hidCvsId" runat="server" />
            <asp:HiddenField ID="hidCvsName" runat="server" />
            <h2>模擬全家分店</h2>
            <div class="row">
                <div class="form-group">
                    <label class="col-md-2">分店代號:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtCvsspot" runat="server" Text="F008703" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">分店名稱:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtName" runat="server" Text="全家市民店" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">分店電話:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtTel" runat="server" Text="02-25638109" />

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">分店地址:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtAddr" runat="server" Text="台北市中山區中山北路一段58號" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-default" Text="選擇確定" OnClick="btnSubmit_Click" />
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label class="col-md-2">分店代號:</label>
                    <div class="col-md-10">
                    <asp:TextBox ID="txtCvsspot2" runat="server" Text="F002773" />
                        </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">分店名稱:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtName2" runat="server" Text="全家新店安民店" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">分店電話:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtTel2" runat="server" Text="02-86662484" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">分店地址:</label>
                    <div class="col-md-10">
                        <asp:TextBox ID="txtAddr2" runat="server" Text="新北市新店區安民街1號1樓" />
                    </div>
                </div>
                <div class="form-group">
                    <asp:Button ID="btnSubmit2" runat="server" CssClass="btn btn-default" Text="選擇確定" OnClick="btnSubmit2_Click" />
                </div>
            </div>

        </form>
    </div>
</body>
</html>

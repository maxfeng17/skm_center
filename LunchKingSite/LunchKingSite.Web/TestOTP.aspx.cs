﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web
{
    public partial class TestOTP : RolePage
    {
        protected static ISysConfProvider cp  = ProviderFactory.Instance().GetConfig();
        private static IOrderProvider _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void MakeOrder_Click(object sender, EventArgs e)
        {
            //step1 app呼叫makeorder
            string userId = "IOS2013233637";

            string dtoString = "";

            if (RadioButtonList1.SelectedIndex == 0)
            {
                //一般卡號
                dtoString = "{\"MainPaymentType\":1,\"TicketId\":\"\",\"TransactionId\":\"\",\"OrderGuid\":\"00000000-0000-0000-0000-000000000000\",\"PrebuiltOrderGuid\":null,\"SessionId\":\"\",\"PezId\":\"\",\"BusinessHourGuid\":\"b5e6907e-d409-4bf3-bb25-6912f706d1b8\",\"DeliveryInfo\":{\"BuyerUserId\":0,\"WriteAddress\":\"\",\"WriteZipCode\":\"\",\"Freight\":0,\"WriteName\":\"\",\"Phone\":\"\",\"PayType\":0,\"Quantity\":1,\"BonusPoints\":0,\"PayEasyCash\":0,\"SCash\":0,\"Notes\":\"\",\"SellerType\":3,\"IsForFriend\":false,\"FriendName\":\"\",\"FriendEMail\":\"\",\"ToFriendNote\":\"\",\"DeliveryDateString\":\"\",\"DeliveryTimeString\":\"\",\"Genus\":\"\",\"MemberDeliveryID\":-1,\"WriteBuildingGuid\":\"00000000-0000-0000-0000-000000000000\",\"WriteShortAddress\":\"\",\"DeliveryType\":1,\"ReceiptsType\":0,\"CarrierType\":0,\"CarrierId\":\"\",\"LoveCode\":\"23981\",\"Version\":1,\"InvoiceType\":\"2\",\"IsInvoiceSave\":false,\"InvoiceSaveId\":null,\"CompanyTitle\":\"\",\"UnifiedSerialNumber\":\"\",\"IsEinvoice\":true,\"InvoiceBuyerName\":\"\",\"InvoiceBuyerAddress\":\"\",\"StoreGuid\":\"1562e5f0-84d6-4d94-9e6c-0a7f93f1a78c\",\"DiscountCode\":\"\",\"EntrustSell\":0,\"CreditCardInstallment\":0,\"IsApplePay\":false,\"ProductDeliveryType\":-1,\"PickupStore\":null,\"DiscountAmount\":0,\"PendingAmount\":0,\"TotalAmount\":0,\"PaymentMethod\":0},\"DeliveryCharge\":0,\"Quantity\":1,\"Amount\":499,\"BCash\":0,\"SCash\":0,\"PCash\":0,\"DiscountCode\":\"\",\"SelectedStoreGuid\":\"1562e5f0-84d6-4d94-9e6c-0a7f93f1a78c\",\"ItemOptions\":\"|#|1\",\"DonationReceiptsType\":0,\"APIProvider\":0,\"IsItem\":false,\"IsPaidByATM\":false,\"IsShoppingCart\":false,\"IsHami\":\"\",\"CreditCardGuid\":null,\"CreditcardNumber\":\"8888880000000001\",\"CreditcardSecurityCode\":\"793\",\"CreditcardExpireYear\":\"18\",\"CreditcardExpireMonth\":\"12\",\"ATMAccount\":null,\"ResultPageType\":0,\"AddressInfo\":\"張|佳惠|0917171717|199|205|db741cd3-5ee0-473b-ad82-00b877a0fcdc|中山北路一段11號14樓(test1)^\",\"ReferenceId\":\"\",\"IsMasterPass\":false,\"MasterPassTransData\":null,\"ApplePayTokenData\":null,\"ApplePayData\":null,\"CpaKey\":null,\"CpaList\":[],\"DeviceId\":\"1095D5D8-083A-4C71-B0CF-08C52FBE50CF\"}";
            }
            else if (RadioButtonList1.SelectedIndex == 1)
            {
                //3D正常卡號
                dtoString = "{\"MainPaymentType\":1,\"TicketId\":\"\",\"TransactionId\":\"\",\"OrderGuid\":\"00000000-0000-0000-0000-000000000000\",\"PrebuiltOrderGuid\":null,\"SessionId\":\"\",\"PezId\":\"\",\"BusinessHourGuid\":\"b5e6907e-d409-4bf3-bb25-6912f706d1b8\",\"DeliveryInfo\":{\"BuyerUserId\":0,\"WriteAddress\":\"\",\"WriteZipCode\":\"\",\"Freight\":0,\"WriteName\":\"\",\"Phone\":\"\",\"PayType\":0,\"Quantity\":1,\"BonusPoints\":0,\"PayEasyCash\":0,\"SCash\":0,\"Notes\":\"\",\"SellerType\":3,\"IsForFriend\":false,\"FriendName\":\"\",\"FriendEMail\":\"\",\"ToFriendNote\":\"\",\"DeliveryDateString\":\"\",\"DeliveryTimeString\":\"\",\"Genus\":\"\",\"MemberDeliveryID\":-1,\"WriteBuildingGuid\":\"00000000-0000-0000-0000-000000000000\",\"WriteShortAddress\":\"\",\"DeliveryType\":1,\"ReceiptsType\":0,\"CarrierType\":0,\"CarrierId\":\"\",\"LoveCode\":\"23981\",\"Version\":1,\"InvoiceType\":\"2\",\"IsInvoiceSave\":false,\"InvoiceSaveId\":null,\"CompanyTitle\":\"\",\"UnifiedSerialNumber\":\"\",\"IsEinvoice\":true,\"InvoiceBuyerName\":\"\",\"InvoiceBuyerAddress\":\"\",\"StoreGuid\":\"1562e5f0-84d6-4d94-9e6c-0a7f93f1a78c\",\"DiscountCode\":\"\",\"EntrustSell\":0,\"CreditCardInstallment\":0,\"IsApplePay\":false,\"ProductDeliveryType\":-1,\"PickupStore\":null,\"DiscountAmount\":0,\"PendingAmount\":0,\"TotalAmount\":0,\"PaymentMethod\":0},\"DeliveryCharge\":0,\"Quantity\":1,\"Amount\":499,\"BCash\":0,\"SCash\":0,\"PCash\":0,\"DiscountCode\":\"\",\"SelectedStoreGuid\":\"1562e5f0-84d6-4d94-9e6c-0a7f93f1a78c\",\"ItemOptions\":\"|#|1\",\"DonationReceiptsType\":0,\"APIProvider\":0,\"IsItem\":false,\"IsPaidByATM\":false,\"IsShoppingCart\":false,\"IsHami\":\"\",\"CreditCardGuid\":null,\"CreditcardNumber\":\"3565580700005806\",\"CreditcardSecurityCode\":\"241\",\"CreditcardExpireYear\":\"41\",\"CreditcardExpireMonth\":\"12\",\"ATMAccount\":null,\"ResultPageType\":0,\"AddressInfo\":\"張|佳惠|0917171717|199|205|db741cd3-5ee0-473b-ad82-00b877a0fcdc|中山北路一段11號14樓(test1)^\",\"ReferenceId\":\"\",\"IsMasterPass\":false,\"MasterPassTransData\":null,\"ApplePayTokenData\":null,\"ApplePayData\":null,\"CpaKey\":null,\"CpaList\":[],\"DeviceId\":\"1095D5D8-083A-4C71-B0CF-08C52FBE50CF\"}";
            }
            else if (RadioButtonList1.SelectedIndex == 2)
            {
                //3D錯誤卡號
                dtoString = "{\"MainPaymentType\":1,\"TicketId\":\"\",\"TransactionId\":\"\",\"OrderGuid\":\"00000000-0000-0000-0000-000000000000\",\"PrebuiltOrderGuid\":null,\"SessionId\":\"\",\"PezId\":\"\",\"BusinessHourGuid\":\"b5e6907e-d409-4bf3-bb25-6912f706d1b8\",\"DeliveryInfo\":{\"BuyerUserId\":0,\"WriteAddress\":\"\",\"WriteZipCode\":\"\",\"Freight\":0,\"WriteName\":\"\",\"Phone\":\"\",\"PayType\":0,\"Quantity\":1,\"BonusPoints\":0,\"PayEasyCash\":0,\"SCash\":0,\"Notes\":\"\",\"SellerType\":3,\"IsForFriend\":false,\"FriendName\":\"\",\"FriendEMail\":\"\",\"ToFriendNote\":\"\",\"DeliveryDateString\":\"\",\"DeliveryTimeString\":\"\",\"Genus\":\"\",\"MemberDeliveryID\":-1,\"WriteBuildingGuid\":\"00000000-0000-0000-0000-000000000000\",\"WriteShortAddress\":\"\",\"DeliveryType\":1,\"ReceiptsType\":0,\"CarrierType\":0,\"CarrierId\":\"\",\"LoveCode\":\"23981\",\"Version\":1,\"InvoiceType\":\"2\",\"IsInvoiceSave\":false,\"InvoiceSaveId\":null,\"CompanyTitle\":\"\",\"UnifiedSerialNumber\":\"\",\"IsEinvoice\":true,\"InvoiceBuyerName\":\"\",\"InvoiceBuyerAddress\":\"\",\"StoreGuid\":\"1562e5f0-84d6-4d94-9e6c-0a7f93f1a78c\",\"DiscountCode\":\"\",\"EntrustSell\":0,\"CreditCardInstallment\":0,\"IsApplePay\":false,\"ProductDeliveryType\":-1,\"PickupStore\":null,\"DiscountAmount\":0,\"PendingAmount\":0,\"TotalAmount\":0,\"PaymentMethod\":0},\"DeliveryCharge\":0,\"Quantity\":1,\"Amount\":499,\"BCash\":0,\"SCash\":0,\"PCash\":0,\"DiscountCode\":\"\",\"SelectedStoreGuid\":\"1562e5f0-84d6-4d94-9e6c-0a7f93f1a78c\",\"ItemOptions\":\"|#|1\",\"DonationReceiptsType\":0,\"APIProvider\":0,\"IsItem\":false,\"IsPaidByATM\":false,\"IsShoppingCart\":false,\"IsHami\":\"\",\"CreditCardGuid\":null,\"CreditcardNumber\":\"3565580700005807\",\"CreditcardSecurityCode\":\"241\",\"CreditcardExpireYear\":\"41\",\"CreditcardExpireMonth\":\"12\",\"ATMAccount\":null,\"ResultPageType\":0,\"AddressInfo\":\"張|佳惠|0917171717|199|205|db741cd3-5ee0-473b-ad82-00b877a0fcdc|中山北路一段11號14樓(test1)^\",\"ReferenceId\":\"\",\"IsMasterPass\":false,\"MasterPassTransData\":null,\"ApplePayTokenData\":null,\"ApplePayData\":null,\"CpaKey\":null,\"CpaList\":[],\"DeviceId\":\"1095D5D8-083A-4C71-B0CF-08C52FBE50CF\"}";
            }
            else if (RadioButtonList1.SelectedIndex == 3)
            {
                //3D錯誤到期日
                dtoString = "{\"MainPaymentType\":1,\"TicketId\":\"\",\"TransactionId\":\"\",\"OrderGuid\":\"00000000-0000-0000-0000-000000000000\",\"PrebuiltOrderGuid\":null,\"SessionId\":\"\",\"PezId\":\"\",\"BusinessHourGuid\":\"b5e6907e-d409-4bf3-bb25-6912f706d1b8\",\"DeliveryInfo\":{\"BuyerUserId\":0,\"WriteAddress\":\"\",\"WriteZipCode\":\"\",\"Freight\":0,\"WriteName\":\"\",\"Phone\":\"\",\"PayType\":0,\"Quantity\":1,\"BonusPoints\":0,\"PayEasyCash\":0,\"SCash\":0,\"Notes\":\"\",\"SellerType\":3,\"IsForFriend\":false,\"FriendName\":\"\",\"FriendEMail\":\"\",\"ToFriendNote\":\"\",\"DeliveryDateString\":\"\",\"DeliveryTimeString\":\"\",\"Genus\":\"\",\"MemberDeliveryID\":-1,\"WriteBuildingGuid\":\"00000000-0000-0000-0000-000000000000\",\"WriteShortAddress\":\"\",\"DeliveryType\":1,\"ReceiptsType\":0,\"CarrierType\":0,\"CarrierId\":\"\",\"LoveCode\":\"23981\",\"Version\":1,\"InvoiceType\":\"2\",\"IsInvoiceSave\":false,\"InvoiceSaveId\":null,\"CompanyTitle\":\"\",\"UnifiedSerialNumber\":\"\",\"IsEinvoice\":true,\"InvoiceBuyerName\":\"\",\"InvoiceBuyerAddress\":\"\",\"StoreGuid\":\"1562e5f0-84d6-4d94-9e6c-0a7f93f1a78c\",\"DiscountCode\":\"\",\"EntrustSell\":0,\"CreditCardInstallment\":0,\"IsApplePay\":false,\"ProductDeliveryType\":-1,\"PickupStore\":null,\"DiscountAmount\":0,\"PendingAmount\":0,\"TotalAmount\":0,\"PaymentMethod\":0},\"DeliveryCharge\":0,\"Quantity\":1,\"Amount\":499,\"BCash\":0,\"SCash\":0,\"PCash\":0,\"DiscountCode\":\"\",\"SelectedStoreGuid\":\"1562e5f0-84d6-4d94-9e6c-0a7f93f1a78c\",\"ItemOptions\":\"|#|1\",\"DonationReceiptsType\":0,\"APIProvider\":0,\"IsItem\":false,\"IsPaidByATM\":false,\"IsShoppingCart\":false,\"IsHami\":\"\",\"CreditCardGuid\":null,\"CreditcardNumber\":\"3565580700005806\",\"CreditcardSecurityCode\":\"241\",\"CreditcardExpireYear\":\"41\",\"CreditcardExpireMonth\":\"10\",\"ATMAccount\":null,\"ResultPageType\":0,\"AddressInfo\":\"張|佳惠|0917171717|199|205|db741cd3-5ee0-473b-ad82-00b877a0fcdc|中山北路一段11號14樓(test1)^\",\"ReferenceId\":\"\",\"IsMasterPass\":false,\"MasterPassTransData\":null,\"ApplePayTokenData\":null,\"ApplePayData\":null,\"CpaKey\":null,\"CpaList\":[],\"DeviceId\":\"1095D5D8-083A-4C71-B0CF-08C52FBE50CF\"}";
            }
            else if (RadioButtonList1.SelectedIndex == 4)
            {
                //3D錯誤後三碼
                dtoString = "{\"MainPaymentType\":1,\"TicketId\":\"\",\"TransactionId\":\"\",\"OrderGuid\":\"00000000-0000-0000-0000-000000000000\",\"PrebuiltOrderGuid\":null,\"SessionId\":\"\",\"PezId\":\"\",\"BusinessHourGuid\":\"b5e6907e-d409-4bf3-bb25-6912f706d1b8\",\"DeliveryInfo\":{\"BuyerUserId\":0,\"WriteAddress\":\"\",\"WriteZipCode\":\"\",\"Freight\":0,\"WriteName\":\"\",\"Phone\":\"\",\"PayType\":0,\"Quantity\":1,\"BonusPoints\":0,\"PayEasyCash\":0,\"SCash\":0,\"Notes\":\"\",\"SellerType\":3,\"IsForFriend\":false,\"FriendName\":\"\",\"FriendEMail\":\"\",\"ToFriendNote\":\"\",\"DeliveryDateString\":\"\",\"DeliveryTimeString\":\"\",\"Genus\":\"\",\"MemberDeliveryID\":-1,\"WriteBuildingGuid\":\"00000000-0000-0000-0000-000000000000\",\"WriteShortAddress\":\"\",\"DeliveryType\":1,\"ReceiptsType\":0,\"CarrierType\":0,\"CarrierId\":\"\",\"LoveCode\":\"23981\",\"Version\":1,\"InvoiceType\":\"2\",\"IsInvoiceSave\":false,\"InvoiceSaveId\":null,\"CompanyTitle\":\"\",\"UnifiedSerialNumber\":\"\",\"IsEinvoice\":true,\"InvoiceBuyerName\":\"\",\"InvoiceBuyerAddress\":\"\",\"StoreGuid\":\"1562e5f0-84d6-4d94-9e6c-0a7f93f1a78c\",\"DiscountCode\":\"\",\"EntrustSell\":0,\"CreditCardInstallment\":0,\"IsApplePay\":false,\"ProductDeliveryType\":-1,\"PickupStore\":null,\"DiscountAmount\":0,\"PendingAmount\":0,\"TotalAmount\":0,\"PaymentMethod\":0},\"DeliveryCharge\":0,\"Quantity\":1,\"Amount\":499,\"BCash\":0,\"SCash\":0,\"PCash\":0,\"DiscountCode\":\"\",\"SelectedStoreGuid\":\"1562e5f0-84d6-4d94-9e6c-0a7f93f1a78c\",\"ItemOptions\":\"|#|1\",\"DonationReceiptsType\":0,\"APIProvider\":0,\"IsItem\":false,\"IsPaidByATM\":false,\"IsShoppingCart\":false,\"IsHami\":\"\",\"CreditCardGuid\":null,\"CreditcardNumber\":\"3565580700005806\",\"CreditcardSecurityCode\":\"242\",\"CreditcardExpireYear\":\"41\",\"CreditcardExpireMonth\":\"12\",\"ATMAccount\":null,\"ResultPageType\":0,\"AddressInfo\":\"張|佳惠|0917171717|199|205|db741cd3-5ee0-473b-ad82-00b877a0fcdc|中山北路一段11號14樓(test1)^\",\"ReferenceId\":\"\",\"IsMasterPass\":false,\"MasterPassTransData\":null,\"ApplePayTokenData\":null,\"ApplePayData\":null,\"CpaKey\":null,\"CpaList\":[],\"DeviceId\":\"1095D5D8-083A-4C71-B0CF-08C52FBE50CF\"}";
            }
            else if (RadioButtonList1.SelectedIndex == 5)
            {
                //記憶卡號
                dtoString = "{\"MainPaymentType\":1,\"TicketId\":\"\",\"TransactionId\":\"\",\"OrderGuid\":\"00000000-0000-0000-0000-000000000000\",\"PrebuiltOrderGuid\":null,\"SessionId\":\"\",\"PezId\":\"\",\"BusinessHourGuid\":\"b5e6907e-d409-4bf3-bb25-6912f706d1b8\",\"DeliveryInfo\":{\"BuyerUserId\":0,\"WriteAddress\":\"\",\"WriteZipCode\":\"\",\"Freight\":0,\"WriteName\":\"\",\"Phone\":\"\",\"PayType\":0,\"Quantity\":1,\"BonusPoints\":0,\"PayEasyCash\":0,\"SCash\":0,\"Notes\":\"\",\"SellerType\":3,\"IsForFriend\":false,\"FriendName\":\"\",\"FriendEMail\":\"\",\"ToFriendNote\":\"\",\"DeliveryDateString\":\"\",\"DeliveryTimeString\":\"\",\"Genus\":\"\",\"MemberDeliveryID\":-1,\"WriteBuildingGuid\":\"00000000-0000-0000-0000-000000000000\",\"WriteShortAddress\":\"\",\"DeliveryType\":1,\"ReceiptsType\":0,\"CarrierType\":0,\"CarrierId\":\"\",\"LoveCode\":\"23981\",\"Version\":1,\"InvoiceType\":\"2\",\"IsInvoiceSave\":false,\"InvoiceSaveId\":null,\"CompanyTitle\":\"\",\"UnifiedSerialNumber\":\"\",\"IsEinvoice\":true,\"InvoiceBuyerName\":\"\",\"InvoiceBuyerAddress\":\"\",\"StoreGuid\":\"1562e5f0-84d6-4d94-9e6c-0a7f93f1a78c\",\"DiscountCode\":\"\",\"EntrustSell\":0,\"CreditCardInstallment\":0,\"IsApplePay\":false,\"ProductDeliveryType\":-1,\"PickupStore\":null,\"DiscountAmount\":0,\"PendingAmount\":0,\"TotalAmount\":0,\"PaymentMethod\":0},\"DeliveryCharge\":0,\"Quantity\":1,\"Amount\":499,\"BCash\":0,\"SCash\":0,\"PCash\":0,\"DiscountCode\":\"\",\"SelectedStoreGuid\":\"1562e5f0-84d6-4d94-9e6c-0a7f93f1a78c\",\"ItemOptions\":\"|#|1\",\"DonationReceiptsType\":0,\"APIProvider\":0,\"IsItem\":false,\"IsPaidByATM\":false,\"IsShoppingCart\":false,\"IsHami\":\"\",\"CreditCardGuid\":\"d35cb318-c656-4355-b8ed-7e263ae101c7\",\"CreditcardNumber\":\"\",\"CreditcardSecurityCode\":\"165\",\"CreditcardExpireYear\":\"31\",\"CreditcardExpireMonth\":\"12\",\"ATMAccount\":null,\"ResultPageType\":0,\"AddressInfo\":\"?|??|0917171717|199|205|db741cd3-5ee0-473b-ad82-00b877a0fcdc|??????11?14?(test1)^\",\"ReferenceId\":\"\",\"IsMasterPass\":false,\"MasterPassTransData\":null,\"ApplePayTokenData\":null,\"ApplePayData\":null,\"CpaKey\":null,\"CpaList\":[],\"DeviceId\":\"1095D5D8-083A-4C71-B0CF-08C52FBE50CF\",\"IsOTP\":false,\"IsFinishOTP\":false}";
            }
            else if (RadioButtonList1.SelectedIndex==6)
            {
                dtoString = "{\"MainPaymentType\":1,\"TicketId\":\"\",\"TransactionId\":\"\",\"OrderGuid\":\"00000000-0000-0000-0000-000000000000\",\"PrebuiltOrderGuid\":null,\"SessionId\":\"\",\"PezId\":\"\",\"BusinessHourGuid\":\"b5e6907e-d409-4bf3-bb25-6912f706d1b8\",\"DeliveryInfo\":{\"BuyerUserId\":0,\"WriteAddress\":\"\",\"WriteZipCode\":\"\",\"Freight\":0.0,\"WriteName\":\"\",\"Phone\":\"\",\"PayType\":0,\"Quantity\":1,\"BonusPoints\":0,\"PayEasyCash\":0,\"SCash\":0.0,\"Notes\":\"\",\"SellerType\":3,\"IsForFriend\":false,\"FriendName\":\"\",\"FriendEMail\":\"\",\"ToFriendNote\":\"\",\"DeliveryDateString\":\"\",\"DeliveryTimeString\":\"\",\"Genus\":\"\",\"MemberDeliveryID\":-1,\"WriteBuildingGuid\":\"00000000-0000-0000-0000-000000000000\",\"WriteShortAddress\":\"\",\"DeliveryType\":1,\"ReceiptsType\":0,\"CarrierType\":0,\"CarrierId\":\"\",\"LoveCode\":\"23981\",\"Version\":1,\"InvoiceType\":\"2\",\"IsInvoiceSave\":false,\"InvoiceSaveId\":null,\"CompanyTitle\":\"\",\"UnifiedSerialNumber\":\"\",\"IsEinvoice\":true,\"InvoiceBuyerName\":\"\",\"InvoiceBuyerAddress\":\"\",\"StoreGuid\":\"1562e5f0-84d6-4d94-9e6c-0a7f93f1a78c\",\"DiscountCode\":\"\",\"EntrustSell\":0,\"CreditCardInstallment\":0,\"IsApplePay\":false,\"ProductDeliveryType\":-1,\"PickupStore\":null,\"DiscountAmount\":0,\"PendingAmount\":0,\"TotalAmount\":0,\"PaymentMethod\":0},\"DeliveryCharge\":0,\"Quantity\":1,\"Amount\":499,\"BCash\":0,\"SCash\":0,\"PCash\":0.0,\"DiscountCode\":\"\",\"SelectedStoreGuid\":\"1562e5f0-84d6-4d94-9e6c-0a7f93f1a78c\",\"ItemOptions\":\"|#|1\",\"DonationReceiptsType\":0,\"APIProvider\":0,\"IsItem\":false,\"IsPaidByATM\":false,\"IsShoppingCart\":false,\"IsHami\":\"\",\"CreditCardGuid\":null,\"CreditcardNumber\":\"5408360100001705\",\"CreditcardSecurityCode\":\"111\",\"CreditcardExpireYear\":\"31\",\"CreditcardExpireMonth\":\"12\",\"ATMAccount\":null,\"ResultPageType\":0,\"AddressInfo\":\"?|??|0917171717|199|205|db741cd3-5ee0-473b-ad82-00b877a0fcdc|??????11?14?(test1)^\",\"ReferenceId\":\"\",\"IsMasterPass\":false,\"MasterPassTransData\":null,\"ApplePayTokenData\":null,\"ApplePayData\":null,\"CpaKey\":null,\"CpaList\":[],\"DeviceId\":\"1095D5D8-083A-4C71-B0CF-08C52FBE50CF\",\"IsOTP\":false,\"IsFinishOTP\":false}";
            }

            string result = "";
            string targetUrl = cp.SSLSiteUrl + "/WebService/PayService.asmx/MakeOrderByApiUserV2";

            //先login
            var cookieContainer = new CookieContainer();
            ApiReturnObject loginResult = Login(cookieContainer);

            HttpWebRequest request = WebRequest.Create(targetUrl) as HttpWebRequest;
            //login cookie塞進去
            request.CookieContainer = cookieContainer;

            string param = string.Format("userId={0}&dtoString={1}", userId, dtoString);
            byte[] bs = Encoding.ASCII.GetBytes(param);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = bs.Length;
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            using (Stream reqStream = request.GetRequestStream())
            {
                reqStream.Write(bs, 0, bs.Length);
            }

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                }
            }


            var MakeOrderResult = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(result);
            if (MakeOrderResult.Code == ApiResultCode.Ready)
            {
                var Data = Newtonsoft.Json.JsonConvert.DeserializeObject<ResultData>(MakeOrderResult.Data.ToString());
                bool isOTP = Data.IsOTP;

                if (isOTP)
                {
                    Response.Redirect(Data.HppUrl);
                }
                else
                {
                    Label1.Text = "一般刷卡完成";
                }
            }
            else
            {
                Label1.Text = MakeOrderResult.Message;
            }
            

            //if (isOTP)
            //{
            //    //走到這就是要OTP
            //    //OTP認證完再打API繼續makepayment

                //    var argument2 = new
                //    {
                //        ticketId = ticketId,
                //        userId = userId
                //    };

                //    string result2 = "";
                //    string postBackUrl2 = "https://localhost/api/PayService/OTPMakePayment";

                //    //先login
                //    var cookieContainer2 = new CookieContainer();
                //    ApiReturnObject loginResult2 = Login(cookieContainer2);

                //    var httpWebRequest2 = (HttpWebRequest)WebRequest.Create(postBackUrl2);

                //    //login cookie塞進去
                //    httpWebRequest2.CookieContainer = cookieContainer2;

                //    httpWebRequest2.ContentType = "application/json";
                //    httpWebRequest2.Method = "POST";

                //    using (var streamWriter = new StreamWriter(httpWebRequest2.GetRequestStream()))
                //    {
                //        string json = JsonConvert.SerializeObject(argument2);

                //        streamWriter.Write(json);
                //        streamWriter.Flush();
                //        streamWriter.Close();
                //    }


                //    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                //    var httpResponse2 = (HttpWebResponse)httpWebRequest2.GetResponse();
                //    using (var streamReader = new StreamReader(httpResponse2.GetResponseStream()))
                //    {
                //        result2 = streamReader.ReadToEnd();
                //    }


                //    //return Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(result2);
                //}

        }
        protected void ResultUrl_Click(object sender, EventArgs e)
        {
            var argumentp = new
            {
                ret_code = "00",
                order_no = "A0-00-064-180914-032",
                ret_msg = "交易成功(Approved or completed successfully)",
                auth_id_resp = "411740",
                carrierId2 = "356558f6JVCtvtFi5ok8Jr6gRBPE9n0MexPGNyiB5zFHSBF/8=",
                rrn = "825711025463",
                order_status = "02",
                auth_type = "SSL",
                cur = "NTD",
                purchase_date = "2018-09-14 19=33=37",
                tx_amt = "100",
                settle_amt = "0",
                settle_seq = "",
                settle_date = "",
                refund_trans_amt = "0",
                refund_rrn = "",
                refund_auth_id_resp = "",
                refund_date = "",
                redeem_order_no = "",
                redeem_pt = "",
                redeem_amt = "",
                post_redeem_amt = "",
                post_redeem_pt = "",
                install_order_no = "",
                install_period = "",
                install_down_pay = "",
                install_pay = "",
                install_down_pay_fee = ""
            };

            var argument = new
            {
                ver = "1.0.0",
                mid = "999812666555140",
                s_mid = "",
                tid = "T0000000",
                pay_type = 1,
                tx_type = 1,
                ret_value = 0,
                @params = argumentp
            };


            string postBackUrl = cp.SSLSiteUrl + "/api/PayService/resultUrl3";
            var httpWebRequest2 = (HttpWebRequest)WebRequest.Create(postBackUrl);
            //httpWebRequest2.ContentType = "application/json";
            httpWebRequest2.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest2.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(argument);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            string result2 = "";
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            var httpResponse = (HttpWebResponse)httpWebRequest2.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result2 = streamReader.ReadToEnd();
            }
        }


        private ApiReturnObject Login(CookieContainer cookie)
        {
            string url = cp.SSLSiteUrl + "/";
            HttpWebRequest request = HttpWebRequest.Create(
                url + "WebService/sso/MemberService.asmx/ContactLogin")
                as HttpWebRequest;
            request.CookieContainer = cookie;
            request.Method = "POST";
            // Create POST data and convert it to a byte array.
            NameValueCollection nvList = new NameValueCollection();
            nvList["userName"] = userName.Text;
            nvList["password"] = password.Text;
            nvList["userId"] = "AND2013681057";
            nvList["driverId"] = "Console";

            string str = CreateParamString(nvList);

            string postData = str;
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;
            // Get the request stream.

            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            ProposalFacade.ProposalPerformanceLogSet("3D", ServicePointManager.SecurityProtocol.ToString(), "sys");
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();

            
            using (HttpWebResponse res = request.GetResponse() as HttpWebResponse)
            {
                byte[] buff = new byte[res.ContentLength];
                res.GetResponseStream().Read(buff, 0, buff.Length);
                string resString = Encoding.UTF8.GetString(buff);
                return JsonConvert.DeserializeObject<ApiReturnObject>(resString);
            }
            return null;
        }

        private static string CreateParamString(NameValueCollection nvList)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in nvList.Keys)
            {
                if (sb.Length > 0)
                {
                    sb.Append("&");
                }
                sb.Append(key);
                sb.Append("=");
                sb.Append(nvList[key]);
            }
            return sb.ToString();
        }

    }

    public class ResultData
    {
        public bool IsOTP { get; set; }
        public string HppUrl { get; set; }
    }
}
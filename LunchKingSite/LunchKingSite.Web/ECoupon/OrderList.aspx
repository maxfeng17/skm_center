﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderList.aspx.cs" MasterPageFile="~/ECoupon/ECoupon.Master" Inherits="LunchKingSite.Web.ECoupon.OrderList" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.Web.ECoupon" %>

<%@ Register Src="~/User/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="zone">
        <h1 class="btn btn-cancel btn-large" style="cursor: default;">訂單/憑證列表</h1>
        <div class="fr-group fr-bg-gray clearfix">
            <div class="fr-item">
                <label class="fr-label">查詢篩選</label>
                <div class="fr-content">
                    <asp:DropDownList ID="ddl_DeliveryType" runat="server" Visible="false">
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddl_FilterType" runat="server">
                    </asp:DropDownList>
                    <asp:TextBox ID="tbx_FilterData" runat="server" class="fr-input-defaul"></asp:TextBox>
                    <br />
                    訂購日期:<asp:TextBox ID="tbx_StartDate" runat="server" CssClass="dates" Width="100"></asp:TextBox>
                    00:00:00~<asp:TextBox ID="tbx_EndDate" runat="server" CssClass="datee" Width="100"></asp:TextBox>
                    23:59:59
                    <asp:Button ID="btn_Search" runat="server" Text="查詢" OnClick="SearchOrder" class="btn btn-primary btn-medium" />
                </div>
            </div>
        </div>
    </div>
    <div class="zone">
        <asp:Panel ID="pan_NoData" runat="server" Visible="false">查無資料</asp:Panel>
        <table class="list-tb">
            <asp:Repeater ID="rp_CouponListMain" runat="server" OnItemDataBound="rp_MainItemBound">
                <HeaderTemplate>
                    <thead>
                        <tr class="rd-C-Hide" style="font-size: small;">
                            <th>訂購日期
                            </th>
                            <th>17Life訂單編號
                            </th>
                            <th><%=Helper.GetEnumDescription(OrderClassType)%>訂編
                            </th>
                            <th>好康名稱
                            </th>
                            <th>售價/數量
                            </th>
                            <th>手機號碼
                            </th>
                            <th>憑證/訂單狀態
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <%# ((CouponListMainModel)(Container.DataItem)).Coupon.CreateTime.ToString("yyyy/MM/dd") %>
                        </td>
                        <td style="font-size: smaller">
                            <a href="../ECoupon/OrderDetail.aspx?oid=<%# ((CouponListMainModel)(Container.DataItem)).Coupon.Guid %>&type=<%# ((CouponListMainModel)(Container.DataItem)).Coupon.Type %>" target="_blank"><%# ((CouponListMainModel)(Container.DataItem)).Coupon.Guid %></a>
                            <br />
                            <%# ((CouponListMainModel)(Container.DataItem)).Coupon.OrderId %>
                        </td>
                        <td>
                            <%# ((CouponListMainModel)(Container.DataItem)).Coupon.RelatedOrderId %>
                        </td>
                        <td style="font-size: smaller">
                            <a href="..<%# (((CouponListMainModel)(Container.DataItem)).Coupon.Status & (int)GroupOrderStatus.FamiDeal) > 0 ? "/" + PponCityGroup.DefaultPponCityGroup.Family.CityId : "" %>/<%# ((CouponListMainModel)(Container.DataItem)).Coupon.BusinessHourGuid %>"
                                target="_blank">
                                <%# ((CouponListMainModel)(Container.DataItem)).Coupon.ItemName.TrimToMaxLength(10,"...") %>
                            </a>
                            <br />
                            <a href='<%# CustomDealUrl+((CouponListMainModel)(Container.DataItem)).Coupon.BusinessHourGuid %>' target="_blank">(<%=Helper.GetEnumDescription(OrderClassType)%>連結)
                            </a>
                        </td>
                        <td>$<%#  (((CouponListMainModel)(Container.DataItem)).Coupon.BusinessHourStatus & (int)LunchKingSite.Core.BusinessHourStatus.PriceZeorShowOriginalPrice) > 0?
                                                                                                                            ((CouponListMainModel)(Container.DataItem)).Coupon.ItemOrigPrice.ToString("F0") : ((CouponListMainModel)(Container.DataItem)).Coupon.ItemPrice.ToString("F0")%>
                                   /
                                    <%# ((CouponListMainModel)(Container.DataItem)).Coupon.TotalCount != 0 ? ((CouponListMainModel)(Container.DataItem)).Coupon.TotalCount.ToString() : string.Empty %>
                        </td>
                        <td>
                            <%# ((CouponListMainModel)(Container.DataItem)).Coupon.Mobile %>
                        </td>
                        <td>
                            <asp:Label ID="lab_rpUseCount" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lab_rpOrderStatus" runat="server"></asp:Label>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <tr>
                <th colspan="7">
                    <uc1:Pager ID="gridPager" runat="server" PageSize="15" OnGetCount="RetrieveOrderCount"
                        OnUpdate="UpdateHandler" />
                </th>
            </tr>
        </table>
    </div>
    <script type="text/javascript" src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#menu_1').addClass('tab-on');
            $(".dates").datepicker();
            $(".datee").datepicker();
        });
    </script>
</asp:Content>

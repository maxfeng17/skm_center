﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.Web.ECoupon
{
    public partial class OrderList : RolePage, ILionTravelOrderListView
    {
        protected ISysConfProvider conf = ProviderFactory.Instance().GetConfig();

        #region property
        private LionTravelOrderListPresenter _presenter;
        public LionTravelOrderListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public int PageCount
        {
            get
            {
                int pagecount;
                return int.TryParse((ViewState["pagecount"] == null ? string.Empty : ViewState["pagecount"].ToString()), out pagecount) ? pagecount : 0;
            }
            set
            {
                ViewState["pagecount"] = value;
            }
        }

        public int FilterDeliveryType
        {
            get
            {
                int i;
                if (int.TryParse(ddl_DeliveryType.SelectedValue, out i))
                {
                    return i;
                }
                else
                {
                    return 0;
                }
            }
        }

        public int FilterType
        {
            get
            {
                int i;
                if (int.TryParse(ddl_FilterType.SelectedValue, out i))
                {
                    return i;
                }
                else
                {
                    return 0;
                }
            }

        }

        public string FilterData
        {
            get
            {
                return tbx_FilterData.Text;
            }

        }

        public DateTime? StartDate
        {
            get
            {
                DateTime startdate;
                if (DateTime.TryParse(tbx_StartDate.Text, out startdate))
                {
                    return startdate;
                }
                else
                {
                    return null;
                }
            }
        }

        public DateTime? EndDate
        {
            get
            {
                DateTime enddate;
                if (DateTime.TryParse(tbx_EndDate.Text, out enddate))
                {
                    return enddate;
                }
                else
                {
                    return null;
                }
            }
        }
        public AgentChannel OrderClassType
        {
            get
            {
                if (User.Identity.Name.ToLower().IndexOf("@17life.com", StringComparison.Ordinal) > 0)
                {
                    return ChannelFacade.GetOrderClassificationByRequestType();
                }

                return ChannelFacade.GetOrderClassificationByName(User.Identity.Name);
            }
        }

        public string CustomDealUrl
        {
            get
            {
                if (OrderClassType == AgentChannel.LionTravelOrder)
                {
                    return "http://www.liontravel.com/webet2/web17se02.aspx?sDealGuid=";
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        #endregion

        #region event
        public event EventHandler<DataEventArgs<int>> PageChange = null;
        public event EventHandler Search = null;
        #endregion

        #region method
        public void GetCouponList(ViewLiontravelCouponListMainCollection data, Dictionary<Guid, int> is_partial_list)
        {
            pan_NoData.Visible = data.Count == 0;
            List<CouponListMainModel> model = new List<CouponListMainModel>();
            foreach (var item in data)
            {
                if (is_partial_list.ContainsKey(item.Guid))
                {
                    model.Add(new CouponListMainModel() { Coupon = item, IsPartialRefund = item.TotalCount > is_partial_list[item.Guid] });
                }
                else
                {
                    model.Add(new CouponListMainModel() { Coupon = item, IsPartialRefund = false });
                }
            }
            rp_CouponListMain.Visible = !pan_NoData.Visible;
            rp_CouponListMain.DataSource = model.OrderByDescending(x=>x.Coupon.CreateTime);
            rp_CouponListMain.DataBind();
        }

        public void SetUpPageCount()
        {
            gridPager.ResolvePagerView(PageCount > 0 ? 1 : 0, true);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
                ddl_DeliveryType.Items.Clear();
                ddl_DeliveryType.Items.Add(new ListItem { Text = "全部訂單", Value = "0"});
                ddl_DeliveryType.Items.AddRange(WebUtility.GetListItemFromEnum(DeliveryType.ToShop).ToArray());
                ddl_DeliveryType.SelectedIndex = 0;
                ddl_DeliveryType.Visible = OrderClassType == AgentChannel.BuyerToBoss;
                ddl_FilterType.Items.Clear();
                ddl_FilterType.Items.AddRange(WebUtility.GetListItemFromEnum(LionTravelOrderFilterType.LionTravelOrderId).ToArray());
                ddl_FilterType.Items[0].Text = Helper.GetEnumDescription(OrderClassType) + ddl_FilterType.Items[0].Text;
            }
            _presenter.OnViewLoaded();
        }

        protected void SearchOrder(object sender, EventArgs e)
        {
            if (Search != null)
            {
                Search(sender, e);
            }
        }

        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChange != null)
            {
                this.PageChange(this, new DataEventArgs<int>(pageNumber));
            }
        }

        protected int RetrieveOrderCount()
        {
            return PageCount;
        }

        protected void rp_MainItemBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CouponListMainModel)
            {
                DateTime now = DateTime.Now;
                Label clab_rpUseCount = ((Label)e.Item.FindControl("lab_rpUseCount"));//憑證狀態
                Label clab_rpOrderStatus = ((Label)e.Item.FindControl("lab_rpOrderStatus"));//訂單狀態
                Label clab_rpOrderId = ((Label)e.Item.FindControl("lab_rpOrderId"));

                CouponListMainModel item = (CouponListMainModel)(e.Item.DataItem);
                ViewLiontravelCouponListMain main = item.Coupon;

                //訂單已取消
                bool iscancel = false;
                bool isPpon = (!main.DeliveryType.HasValue || int.Equals((int)DeliveryType.ToShop, main.DeliveryType.Value));
                //換貨訂單
                bool isExchangeOrder = (main.CancelStatus == (int)OrderLogStatus.ExchangeCancel ||
                                        main.CancelStatus == (int)OrderLogStatus.ExchangeFailure ||
                                        main.CancelStatus == (int)OrderLogStatus.ExchangeProcessing ||
                                        main.CancelStatus == (int)OrderLogStatus.ExchangeSuccess ||
                                        main.CancelStatus == (int)OrderLogStatus.SendToSeller);
                //退貨訂單
                bool isReturnOrder = (main.ReturnStatus != (int)ProgressStatus.Canceled && main.ReturnStatus != -1);

                if (!isPpon)
                {
                    clab_rpUseCount.Text = "宅配商品";
                }
                if ((main.OrderStatus & ((int)LunchKingSite.Core.OrderStatus.Cancel)) > 0) //退貨完成
                {
                    iscancel = true;
                    clab_rpOrderStatus.Text = item.IsPartialRefund ? "部分退貨" : "訂單已取消";
                    if (isPpon && main.TotalCount != 0)
                    {
                        if (item.IsPartialRefund)
                        {
                            clab_rpUseCount.Text = string.Empty;
                            if (main.RemainCount > 0)
                            {
                                clab_rpUseCount.Text = string.Format("未使用:{0}", main.RemainCount);
                            }
                        }
                    }
                }
                else if (isExchangeOrder || isReturnOrder)  //退or換貨訂單
                {
                    switch (main.CancelStatus)
                    {
                        #region 換貨部分

                        case (int)OrderLogStatus.SendToSeller:
                        case (int)OrderLogStatus.ExchangeProcessing:
                            clab_rpOrderStatus.Text = Resources.Localization.ExchangeProcessing;
                            break;

                        case (int)OrderLogStatus.ExchangeSuccess:
                            clab_rpOrderStatus.Text = Resources.Localization.ExchangeSuccess;
                            break;

                        case (int)OrderLogStatus.ExchangeFailure:
                            clab_rpOrderStatus.Text = Resources.Localization.ExchangeFailure;
                            break;

                        case (int)OrderLogStatus.ExchangeCancel:
                            clab_rpOrderStatus.Text = Resources.Localization.ExchangeCancel;
                            break;

                        #endregion 換貨部分

                        #region 退貨部分

                        default:
                            iscancel = true;
                            switch (main.ReturnStatus)
                            {
                                case (int)ProgressStatus.Processing:
                                case (int)ProgressStatus.AtmQueueing:
                                case (int)ProgressStatus.AtmQueueSucceeded:
                                case (int)ProgressStatus.AtmFailed:
                                    clab_rpOrderStatus.Text = "退貨處理中";
                                    break;

                                case (int)ProgressStatus.Completed:
                                case (int)ProgressStatus.CompletedWithCreditCardQueued:
                                    #region Completed
                                    clab_rpOrderStatus.Text = "訂單已取消";
                                    if (isPpon && main.TotalCount != 0)
                                    {
                                        if (item.IsPartialRefund)
                                        {
                                            clab_rpUseCount.Text = string.Empty;
                                            if (main.RemainCount > 0)
                                            {
                                                clab_rpUseCount.Text = string.Format("未使用:{0}", main.RemainCount);
                                            }
                                        }
                                    }
                                    break;
                                    #endregion Completed

                                case (int)ProgressStatus.Unreturnable:

                                    #region Unreturnable

                                    clab_rpOrderStatus.Text = "退貨失敗";
                                    break;

                                    #endregion Unreturnable
                            }

                            break;

                        #endregion 退貨部分
                    }

                }
                else if (main.BusinessHourOrderTimeE <= now)    //已結檔
                {
                    int slug;
                    if (int.TryParse(main.Slug, out slug) && slug < main.BusinessHourOrderMinimum)
                    {
                        clab_rpOrderStatus.Text = "未達門檻";
                    }
                    else
                    {
                        if (isPpon)
                        {
                            if (main.TotalCount != 0)
                            {
                                if (main.RemainCount > 0)
                                {
                                    clab_rpUseCount.Text = "未使用:" + main.RemainCount;
                                }
                                else
                                {
                                    clab_rpUseCount.Text = "使用完畢";
                                }
                            }
                        }
                        else
                        {
                            if (PponOrderManager.CheckIsOverTrialPeriod(main.Guid))
                            {
                                clab_rpOrderStatus.Text = "已過鑑賞期";
                            }
                            else
                            {
                                //訂單狀態顯示出貨資訊
                                //檢查是否已出貨
                                if (PponOrderManager.CheckIsShipped(main.Guid, OrderClassification.LkSite))
                                {
                                    clab_rpOrderStatus.Text = "已出貨";
                                }
                                else
                                {
                                    clab_rpOrderStatus.Text = "最後出貨日<br/>" + main.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd");
                                }
                            }
                        }

                        if ((main.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0 && (main.OrderStatus & (int)Core.OrderStatus.Complete) == 0) //ATM未付款，已達門檻不顯示退款
                        {
                            if (isPpon)
                            {
                                clab_rpUseCount.Text = string.Empty;
                            }
                        }
                    }
                }
                else    //尚在搶購中
                {
                    if (isPpon)
                    {
                        if (main.TotalCount != 0)
                        {
                            if (main.RemainCount > 0)
                            {
                                clab_rpUseCount.Text = string.Format("未使用:{0}", main.RemainCount);
                            }
                            else
                            {
                                clab_rpUseCount.Text = "使用完畢";
                            }
                        }
                        //ATM
                        if ((main.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0 && (main.OrderStatus & (int)Core.OrderStatus.Complete) == 0)
                        {
                            clab_rpUseCount.Text = string.Empty;
                        }
                    }
                    else
                    {
                        if (PponOrderManager.CheckIsOverTrialPeriod(main.Guid))
                        {
                            clab_rpOrderStatus.Text = "已過鑑賞期";
                        }
                        else
                        {
                            //訂單狀態顯示出貨資訊
                            //檢查是否已出貨                                
                            if (PponOrderManager.CheckIsShipped(main.Guid, OrderClassification.LkSite))
                            {
                                clab_rpOrderStatus.Text = "已出貨";
                            }
                            else
                            {
                                clab_rpOrderStatus.Text = "最後出貨日<br/>" + main.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd");
                            }
                        }
                    }
                }

                #region 退貨條件顯示

                //不接受退貨
                if ((main.Status & (int)GroupOrderStatus.NoRefund) > 0)
                {
                    clab_rpOrderStatus.Text = I18N.Phrase.NoRefund;
                }

                //結檔後七天不接受退貨
                if ((main.Status & (int)GroupOrderStatus.DaysNoRefund) > 0 && main.BusinessHourOrderTimeE.AddDays(conf.ProductRefundDays) < DateTime.Now)
                {
                    clab_rpOrderStatus.Text = I18N.Phrase.NoRefund;
                }

                //過期不接受退貨
                if ((main.Status & (int)GroupOrderStatus.ExpireNoRefund) > 0)
                {
                    if (main.BusinessHourDeliverTimeE != null && main.BusinessHourDeliverTimeE.Value.AddDays(1) < now)
                    {
                        if (!iscancel)
                        {
                            clab_rpOrderStatus.Text = I18N.Phrase.NoRefund;
                        }
                    }
                }

                //演出時間前十日過後不能退貨
                if ((main.Status & (int)GroupOrderStatus.NoRefundBeforeDays) > 0 && now.Date >= ((DateTime)main.BusinessHourDeliverTimeS).AddDays(-conf.PponNoRefundBeforeDays).Date)
                {
                    clab_rpOrderStatus.Text = I18N.Phrase.NoRefund;
                }

                #endregion
            }
        }
    }

    public class CouponListMainModel
    {
        public ViewLiontravelCouponListMain Coupon { get; set; }
        public bool IsPartialRefund { get; set; }
    }
}


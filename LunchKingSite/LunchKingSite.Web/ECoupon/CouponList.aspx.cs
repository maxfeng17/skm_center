﻿using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ECoupon
{
    public partial class CouponList : RolePage, ILionTravelCouponListView
    {
        #region property
        private LionTravelCouponListPresenter _presenter;
        public LionTravelCouponListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public AgentChannel OrderClassType
        {
            get
            {
                if (User.Identity.Name.ToLower().IndexOf("@17life.com", StringComparison.Ordinal) > 0)
                {
                    return ChannelFacade.GetOrderClassificationByRequestType();
                }

                return ChannelFacade.GetOrderClassificationByName(User.Identity.Name);
            }
        }
        #endregion

        #region event
        public event EventHandler<DataEventArgs<Coupon>> SearchCoupons;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void SearchCoupon(object sender, EventArgs e)
        {
            string data = tbx_FilterData.Text.Trim();
            if (!string.IsNullOrEmpty(data) && SearchCoupons != null)
            {
                Coupon coupon = new Coupon();
                if (ddl_FilterType.SelectedValue == "0")
                {
                    coupon.SequenceNumber = data;
                }
                else
                {
                    coupon.Code = data;
                }
                SearchCoupons(sender, new DataEventArgs<Coupon>(coupon));
            }
        }

        protected string GetReturnMessage(int coupon_id, CashTrustLogCollection cash_trust_logs, CashTrustLogCollection refund_cash_trust_logs)
        {
            CashTrustLog ctlog = cash_trust_logs.FirstOrDefault(x => x.CouponId == coupon_id);
            if (ctlog != null)
            {
                if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.AtmRefunding))
                {
                    return "ATM退款中，無法退貨";
                }

                switch ((TrustStatus)ctlog.Status)
                {
                    case TrustStatus.Initial:
                    case TrustStatus.Trusted:
                        CashTrustLog returningCtlog = refund_cash_trust_logs.FirstOrDefault(x => x.CouponId == coupon_id);
                        if (returningCtlog != null)
                        {
                            return "已在退貨單上，無法退貨";
                        }
                        return "未核銷，可進行退貨";
                    case TrustStatus.Verified:
                        DateTime modifyDT = ctlog.ModifyTime;
                        string modifyTime = string.Format("{0:yyyy/MM/dd HH:mm} ", modifyDT);

                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.VerificationForced))
                        {
                            return modifyTime + "已強制核銷";
                        }

                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.VerificationLost))
                        {
                            return modifyTime + "已核銷，清冊遺失";
                        }

                        return modifyTime + "已核銷";
                    case TrustStatus.Returned:
                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.ReturnForced))
                        {
                            return "已強制退貨";
                        }

                        return "已退購物金，無法再退貨";
                    case TrustStatus.Refunded:
                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.ReturnForced))
                        {
                            return "已強制退貨";
                        }

                        return "已轉退現金，無法再退貨";
                    case TrustStatus.ATM:
                        return "未付款，無法退貨";
                    default:
                        return "查無即時核銷資訊";
                }
            }

            return "查無即時核銷資訊";
        }

        #region method
        public void SetCoupons(ViewOrderCorrespondingCouponCollection coupons, Dictionary<Guid, CashTrustLogCollection> cash_trust_logs, Dictionary<Guid, CashTrustLogCollection> return_cash_trust_logs)
        {
            List<ViewOrderCorrespondingCouponStatusModel> data = new List<ViewOrderCorrespondingCouponStatusModel>();
            foreach (var item in coupons)
            {
                if (cash_trust_logs.ContainsKey(item.OrderGuid) && return_cash_trust_logs.ContainsKey(item.OrderGuid))
                {
                    data.Add(new ViewOrderCorrespondingCouponStatusModel() { Coupon = item, StatusMessage = GetReturnMessage(item.CouponId, cash_trust_logs[item.OrderGuid], return_cash_trust_logs[item.OrderGuid]) });
                }
                else
                {
                    data.Add(new ViewOrderCorrespondingCouponStatusModel() { Coupon = item, StatusMessage = "查無即時核銷資訊" });
                }
            }
            pan_NoData.Visible = data.Count == 0;
            rp_CouponListMain.Visible = !pan_NoData.Visible;
            rp_CouponListMain.DataSource = data;
            rp_CouponListMain.DataBind();
        }
        #endregion
    }

    public class ViewOrderCorrespondingCouponStatusModel
    {
        public ViewOrderCorrespondingCoupon Coupon { get; set; }
        public string StatusMessage { get; set; }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RefundList.aspx.cs" MasterPageFile="~/ECoupon/ECoupon.Master" Inherits="LunchKingSite.Web.ECoupon.RefundList" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.Web.ECoupon" %>

<%@ Register Src="~/User/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="zone">
        <h1 class="btn btn-cancel btn-large" style="cursor: default;">退貨訂單列表</h1>
        <div class="fr-group fr-bg-gray clearfix">
            <div class="fr-item">
                <label class="fr-label">查詢篩選</label>
                <div class="fr-content">
                    <asp:RadioButtonList ID="rbl_Status" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>
                    <asp:DropDownList ID="ddl_DeliveryType" runat="server" Visible="false">
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddl_FilterType" runat="server">
                    </asp:DropDownList>
                    <asp:TextBox ID="tbx_FilterData" runat="server" class="fr-input-defaul"></asp:TextBox>
                    <br />
                    申請日期:
                        <asp:TextBox ID="tbx_StartDate" runat="server" CssClass="dates" Width="100"></asp:TextBox>
                    00:00:00~<asp:TextBox ID="tbx_EndDate" runat="server" CssClass="datee" Width="100"></asp:TextBox>
                    23:59:59
                <asp:Button ID="btn_Search" runat="server" Text="查詢" OnClick="SearchRefundForm" class="btn btn-primary btn-medium" />
                </div>
            </div>
        </div>
    </div>
    <div class="zone">
        <asp:Panel ID="pan_NoData" runat="server" Visible="false">查無資料</asp:Panel>
        <table class="list-tb">
            <asp:Repeater ID="rp_RefundForm" runat="server">
                <HeaderTemplate>
                    <thead>
                        <tr class="rd-C-Hide" style="font-size: small">
                            <th>17Life訂單編號
                            </th>
                            <th><%=Helper.GetEnumDescription(OrderClassType)%>訂編
                            </th>
                            <th>手機號碼
                            </th>
                            <th>申請時間
                            </th>
                            <th>處理時間
                            </th>
                            <th>申請退貨數<br />
                                退貨完成數
                            </th>
                            <th>處理進度
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td style="font-size: smaller">
                            <a href="OrderDetail.aspx?oid=<%# ((ViewLionTravelRefundModel)(Container.DataItem)).OrderGuid %>&type=<%# ((ViewLionTravelRefundModel)(Container.DataItem)).Type %>" target="_blank"><%# ((ViewLionTravelRefundModel)(Container.DataItem)).OrderGuid %></a>
                            <br />
                            <%# ((ViewLionTravelRefundModel)(Container.DataItem)).OrderId %>
                        </td>
                        <td>
                            <%# ((ViewLionTravelRefundModel)(Container.DataItem)).RelatedOrderId %>
                        </td>
                        <td>
                            <%# ((ViewLionTravelRefundModel)(Container.DataItem)).Mobile %>
                        </td>
                        <td>
                            <%# ((ViewLionTravelRefundModel)(Container.DataItem)).ApplicationTime.ToString("yyyy/MM/dd HH:mm") %>
                        </td>
                        <td>
                            <%# ((ViewLionTravelRefundModel)(Container.DataItem)).ProcessTime.HasValue?((ViewLionTravelRefundModel)(Container.DataItem)).ProcessTime.Value.ToString("yyyy/MM/dd HH:mm"):string.Empty %>
                        </td>
                        <td>
                            <%# ((ViewLionTravelRefundModel)(Container.DataItem)).ApplicationProductCount %>
                                /
                                <%# ((ViewLionTravelRefundModel)(Container.DataItem)).ProcessedProductCount %>
                        </td>
                        <td style="font-size: smaller">
                            <%# ((ViewLionTravelRefundModel)(Container.DataItem)).ProcessStatusDesc %>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <tr>
                <td colspan="7">
                    <uc1:Pager ID="gridPager" runat="server" PageSize="15" OnGetCount="RetrieveOrderCount"
                        OnUpdate="UpdateHandler" />
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript" src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#menu_3').addClass('tab-on');
            $(".dates").datepicker();
            $(".datee").datepicker();
        });

    </script>
</asp:Content>

﻿using LunchKingSite.WebLib.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;

namespace LunchKingSite.Web.ECoupon
{
    public partial class ECoupon : MemberMasterPage
    {
        public AgentChannel OrderClassType
        {
            get
            {
                if (HttpContext.Current.User.Identity.Name.ToLower().IndexOf("@17life.com", StringComparison.Ordinal) > 0)
                {
                    return ChannelFacade.GetOrderClassificationByRequestType();
                }

                return ChannelFacade.GetOrderClassificationByName(HttpContext.Current.User.Identity.Name);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.User != null && Page.User.Identity.IsAuthenticated)
            {
                lab_UserName.Text = this.MemberName.TrimToMaxLength(12, "...");
            }
            if (!Page.IsPostBack)
            {
                Dictionary<int, string> source = new Dictionary<int, string>();
                source = Enum.GetValues(typeof(AgentChannel)).Cast<AgentChannel>().Except(new AgentChannel[] { AgentChannel.NONE }).ToDictionary(t => (int)t, t => t.ToString());

                //source.Add((int)OrderClassification.LionTravelOrder,Helper.GetEnumDescription(OrderClassification.LionTravelOrder));
                //source.Add((int)OrderClassification.PayEasyOrder,Helper.GetEnumDescription(OrderClassification.PayEasyOrder));
                //source.Add((int)OrderClassification.BuyerToBoss,Helper.GetEnumDescription(OrderClassification.BuyerToBoss));
                //source.Add((int) OrderClassification.EzTravel, Helper.GetEnumDescription(OrderClassification.EzTravel));
                //source.Add((int)OrderClassification.LionItri, Helper.GetEnumDescription(OrderClassification.LionItri));

                ddlType.DataSource = source;
                if (!string.IsNullOrEmpty(Request.QueryString["type"]) && source.Keys.Any(x => x.ToString() == Request.QueryString["type"]))
                    ddlType.SelectedValue = Request.QueryString["type"];
                ddlType.DataTextField = "Value";
                ddlType.DataValueField = "Key";
                ddlType.Visible = HttpContext.Current.User.Identity.Name.ToLower().IndexOf("@17life.com", StringComparison.Ordinal) > 0;
                ddlType.DataBind();
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            var query = HttpUtility.ParseQueryString(Request.QueryString.ToString());
            query.Set("type", ddlType.SelectedValue);
            Response.Redirect(string.Format("{0}?{1}", Request.Path, query));
        }
    }
}
﻿using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Models.ControlRoom.Order;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.Web.ECoupon
{
    public partial class RefundList : RolePage, ILionTravelRefundListView
    {
        #region property
        private LionTravelRefundListPresenter _presenter;
        public LionTravelRefundListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public ProgressState ProdressStatus
        {
            get
            {
                ProgressState state;
                if (Enum.TryParse<ProgressState>(rbl_Status.SelectedValue, out state))
                {
                    return state;
                }
                else
                {
                    return ProgressState.All;
                }
            }
        }

        public int PageCount
        {
            get
            {
                int pagecount;
                return int.TryParse((ViewState["pagecount"] == null ? string.Empty : ViewState["pagecount"].ToString()), out pagecount) ? pagecount : 0;
            }
            set
            {
                ViewState["pagecount"] = value;
            }
        }

        public int FilterDeliveryType
        {
            get
            {
                int i;
                if (int.TryParse(ddl_DeliveryType.SelectedValue, out i))
                {
                    return i;
                }
                else
                {
                    return 0;
                }
            }
        }

        public int FilterType
        {
            get
            {
                int i;
                if (int.TryParse(ddl_FilterType.SelectedValue, out i))
                {
                    return i;
                }
                else
                {
                    return 0;
                }
            }

        }

        public string FilterData
        {
            get
            {
                return tbx_FilterData.Text;
            }

        }

        public DateTime? StartDate
        {
            get
            {
                DateTime startdate;
                if (DateTime.TryParse(tbx_StartDate.Text, out startdate))
                {
                    return startdate;
                }
                else
                {
                    return null;
                }
            }
        }

        public DateTime? EndDate
        {
            get
            {
                DateTime enddate;
                if (DateTime.TryParse(tbx_EndDate.Text, out enddate))
                {
                    return enddate;
                }
                else
                {
                    return null;
                }
            }
        }
        public AgentChannel OrderClassType
        {
            get
            {
                if (User.Identity.Name.ToLower().IndexOf("@17life.com", StringComparison.Ordinal) > 0)
                {
                    return ChannelFacade.GetOrderClassificationByRequestType();
                }

                return ChannelFacade.GetOrderClassificationByName(User.Identity.Name);
            }
        }
        #endregion

        #region event
        public event EventHandler SearchRefundForms;
        public event EventHandler<DataEventArgs<int>> PageChange;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
                ddl_DeliveryType.Items.Clear();
                ddl_DeliveryType.Items.Add(new ListItem { Text = "全部訂單", Value = "0" });
                ddl_DeliveryType.Items.AddRange(WebUtility.GetListItemFromEnum(DeliveryType.ToShop).ToArray());
                ddl_DeliveryType.SelectedIndex = 0;
                ddl_DeliveryType.Visible = OrderClassType == AgentChannel.BuyerToBoss;
                ddl_FilterType.Items.Clear();
                ddl_FilterType.Items.AddRange(WebUtility.GetListItemFromEnum(LionTravelOrderFilterType.LionTravelOrderId).ToArray());
                rbl_Status.Items.Add(new ListItem("全部", ((int)ProgressState.All).ToString()));
                rbl_Status.Items.Add(new ListItem("退貨處理中", ((int)ProgressState.ReturnProcessing).ToString()));
                rbl_Status.Items.Add(new ListItem("退貨成功", ((int)ProgressState.ReturnCompleted).ToString()));
                rbl_Status.Items.Add(new ListItem("退貨失敗", ((int)ProgressState.ReturnFail).ToString()));
                rbl_Status.Items.Add(new ListItem("退貨取消", ((int)ProgressState.ReturnCancel).ToString()));
                rbl_Status.SelectedIndex = 0;
            }
            _presenter.OnViewLoaded();
        }

        protected void SearchRefundForm(object sender, EventArgs e)
        {
            if (SearchRefundForms != null)
            {
                SearchRefundForms(sender, e);
            }
        }

        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChange != null)
            {
                this.PageChange(this, new DataEventArgs<int>(pageNumber));
            }
        }

        protected int RetrieveOrderCount()
        {
            return PageCount;
        }

        #region method
        public void SetRefundForms(ViewLiontravelOrderReturnFormListCollection data, Dictionary<int, int> application_product_count, Dictionary<int, int> refunded_product_count)
        {
            List<ViewLionTravelRefundModel> infos = new List<ViewLionTravelRefundModel>();
            foreach (var returnForm in data)
            {
                int comboPackCount = returnForm.ComboPackCount;
                string processStatusDesc = string.Empty;
                string orderStatusDesc = string.Empty;
                string vendorProgressStatusDesc = string.Empty;

                #region 廠商進度

                switch ((VendorProgressStatus)returnForm.VendorProgressStatus)
                {
                    case VendorProgressStatus.Unknown:
                        break;

                    case VendorProgressStatus.CompletedAndNoRetrieving:
                    case VendorProgressStatus.CompletedAndRetrievied:
                    case VendorProgressStatus.CompletedAndUnShip:
                    case VendorProgressStatus.CompletedByCustomerService:
                        vendorProgressStatusDesc = "退貨完成";
                        break;

                    default:
                        vendorProgressStatusDesc = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (VendorProgressStatus)(returnForm.VendorProgressStatus ?? (int)VendorProgressStatus.Processing));
                        break;

                }

                #endregion 廠商進度

                #region 處理進度

                switch ((RefundType)returnForm.RefundType)
                {
                    case RefundType.Atm:
                        processStatusDesc = "[退ATM]";
                        break;

                    case RefundType.Cash:
                        processStatusDesc = "[刷退]";
                        break;

                    case RefundType.Scash:
                        processStatusDesc = "[退還原款項]";//"[退17Life購物金]";
                        break;

                    case RefundType.ScashToCash:
                        processStatusDesc = "[購物金轉刷退]";
                        break;

                    case RefundType.ScashToAtm:
                        processStatusDesc = "[購物金轉退ATM]";
                        break;

                    default:
                        break;
                }

                switch ((ProgressStatus)returnForm.ProgressStatus)
                {
                    case ProgressStatus.AtmQueueing:
                    case ProgressStatus.Processing:
                    case ProgressStatus.AtmQueueSucceeded:
                        processStatusDesc += "<br/>" + "退貨處理中";
                        break;

                    case ProgressStatus.AtmFailed:
                        processStatusDesc += "<br/>" + "退貨處理中-ATM匯款失敗";
                        break;

                    case ProgressStatus.Completed:
                    case ProgressStatus.CompletedWithCreditCardQueued:
                        processStatusDesc += "<br/>" + "退貨完成";
                        break;

                    case ProgressStatus.Canceled:
                        processStatusDesc += "<br/>" + "取消退貨";
                        break;

                    case ProgressStatus.Unreturnable:
                        processStatusDesc += "<br/>" + "退貨失敗";
                        break;

                    default:
                        break;
                }

                #endregion 處理進度

                ViewLionTravelRefundModel info = new ViewLionTravelRefundModel
                {
                    DealGuid = returnForm.ProductGuid,
                    DealName = returnForm.DealName,
                    DealCloseTime = returnForm.DealEndTime,
                    OrderId = returnForm.OrderId,
                    OrderGuid = returnForm.OrderGuid,
                    ApplicationTime = returnForm.ReturnApplicationTime,
                    ApplicationProductCount = application_product_count.ContainsKey(returnForm.ReturnFormId)
                                                ? application_product_count[returnForm.ReturnFormId] > 0
                                                    ? application_product_count[returnForm.ReturnFormId] / comboPackCount
                                                    : 0
                                                : 0, //申請退貨數以組數(成套販售)顯示
                    ProcessTime = returnForm.ModifyTime,
                    ProcessedProductCount = refunded_product_count.ContainsKey(returnForm.ReturnFormId)
                                                ? refunded_product_count[returnForm.ReturnFormId]
                                                : 0, //退貨完成數以組數(成套販售)顯示 return_form_refund以cash_trust_log紀錄 故不需另外處理
                    Reason = returnForm.ReturnReason,
                    VendorProcessStatusDesc = vendorProgressStatusDesc,
                    ProcessStatusDesc = processStatusDesc,
                    RelatedOrderId = returnForm.RelatedOrderId,
                    Mobile = returnForm.Mobile,
                    Type = returnForm.Type
                };

                infos.Add(info);
            }
            pan_NoData.Visible = infos.Count == 0;
            rp_RefundForm.DataSource = infos;
            rp_RefundForm.DataBind();
        }

        public void SetUpPageCount()
        {
            gridPager.ResolvePagerView(PageCount > 0 ? 1 : 0, true);
        }
        #endregion
    }

    public class ViewLionTravelRefundModel
    {
        public Guid DealGuid { get; set; }
        public DateTime DealCloseTime { get; set; }
        public string OrderId { get; set; }
        public Guid OrderGuid { get; set; }
        public string DealName { get; set; }
        public DateTime ApplicationTime { get; set; }
        public int ApplicationProductCount { get; set; }
        public DateTime? ProcessTime { get; set; }
        public int ProcessedProductCount { get; set; }
        public string Reason { get; set; }
        public string ProcessStatusDesc { get; set; }
        public string OrderStatusDesc { get; set; }
        public string VendorProcessStatusDesc { get; set; }
        public string RelatedOrderId { get; set; }
        public string Mobile { get; set; }
        public int Type { get; set; }
    }
}
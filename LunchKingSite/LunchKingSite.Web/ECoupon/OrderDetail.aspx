﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/ECoupon/ECoupon.Master" CodeBehind="OrderDetail.aspx.cs" Inherits="LunchKingSite.Web.ECoupon.OrderDetail" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Web.ECoupon" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Model.Refund" %>
<%@ Import Namespace="LunchKingSite.Core" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="zone">
        <h1 class="btn btn-cancel btn-large" style="cursor: default;">訂單基本資料</h1>
        <div class="fr-group clearfix">
            <div class="fr-item">
                <label class="fr-label">檔次名稱</label>
                <div class="fr-content">
                    <asp:Label ID="lab_DealName" runat="server"></asp:Label>
                </div>
            </div>
            <div class="fr-item">
                <label class="fr-label">上檔期間</label>
                <div class="fr-content">
                    <asp:Label ID="lab_DealBusinessTime" runat="server"></asp:Label>
                </div>
            </div>
            <div class="fr-item">
                <label class="fr-label">兌換期間</label>
                <div class="fr-content">
                    <asp:Label ID="lab_DealDeliveryTime" runat="server"></asp:Label>
                </div>
            </div>
            <div class="fr-item">
                <label class="fr-label"><%=Helper.GetEnumDescription(OrderClassType)%>訂單編號</label>
                <div class="fr-content">
                    <asp:Label ID="lab_RelatedOrderId" runat="server"></asp:Label>
                </div>
            </div>
            <div class="fr-item">
                <label class="fr-label">17Life訂單編號</label>
                <div class="fr-content">
                    <asp:Label ID="lab_OrderGuid" runat="server"></asp:Label>
                </div>
            </div>
            <div class="fr-item">
                <label class="fr-label">17Life訂單Id</label>
                <div class="fr-content">
                    <asp:Label ID="lab_OrderId" runat="server"></asp:Label>
                </div>
            </div>
            <div class="fr-item">
                <label class="fr-label">手機號碼</label>
                <div class="fr-content">
                    <asp:Label ID="lab_Mobile" runat="server"></asp:Label>
                </div>
            </div>
            <div class="fr-item">
                <label class="fr-label">訂單建立時間</label>
                <div class="fr-content">
                    <asp:Label ID="lab_CreatedTime" runat="server"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="zone">
        <h1 class="btn btn-cancel btn-large" style="cursor: default;">訂單備註</h1>
        <div class="fr-group clearfix">
            <div class="fr-item">
                <table class="list-tb">
                    <tr>
                        <th>備註</th>
                        <th style="width: 70%">
                            <asp:TextBox ID="tbx_UserMemo" TextMode="MultiLine" runat="server" Width="100%" Height="100%"></asp:TextBox></th>
                        <th>
                            <asp:Button runat="server" Text="新增" ID="btn_Memo" OnClick="AddOrderUserMemo" class="btn btn-primary btn-medium" /></th>
                    </tr>
                </table>
            </div>
            <div class="fr-item">
                <asp:Panel ID="pan_UserMemo" runat="server" Visible="false">
                    <table class="list-tb">
                        <tr>
                            <th>內容</th>
                            <th>建立時間</th>
                            <th>建立人</th>
                            <th>刪除</th>
                        </tr>
                        <asp:Repeater ID="repOrderUserMemo" runat="server" OnItemCommand="DeleteMemoCommand">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <span title="<%# ((OrderUserMemoList)(Container.DataItem)).UserMemo%>"><%# ((OrderUserMemoList)(Container.DataItem)).UserMemo.TrimToMaxLength(35,"...")%></span>
                                    </td>
                                    <td align="center">
                                        <%# ((OrderUserMemoList)(Container.DataItem)).CreateTime.ToString("yyyy/MM/dd HH:mm") %>
                                    </td>
                                    <td align="center">
                                        <%# ((OrderUserMemoList)(Container.DataItem)).CreateId%>
                                    </td>
                                    <td align="center">
                                        <asp:Button ID="btn_Delete" runat="server" Text="刪除" CommandArgument="<%# ((OrderUserMemoList)(Container.DataItem)).Id%>" CommandName="DeleteMemo" class="btn btn-primary btn-medium" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </asp:Panel>
            </div>
        </div>
    </div>
    <div class="zone">
        <h1 runat="server" id="hlInfo" class="btn btn-cancel btn-large" style="cursor: default;">憑證資訊</h1>
        <div class="fr-group clearfix">
            <div class="fr-item">
                <table class="list-tb">
                    <asp:Panel runat="server" ID="plCouponInfo">
                        <asp:Repeater ID="rpt_Coupon" runat="server" OnItemCommand="CouponItemCommand">
                        <HeaderTemplate>
                            <tr>
                                <th>核取
                                </th>
                                <th>憑證Id
                                </th>
                                <th>憑證編號
                                </th>
                                <th>確認碼
                                </th>
                                <th>狀態
                                </th>
                                <th>簡訊憑證
                                </th>
                                <th>重新發送
                                </th>
                            </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="hid_CouponId" runat="server" Value="<%# ((ViewPponCouponWithSmsCountModel)(Container.DataItem)).Coupon.CouponId %>" />
                                    <asp:CheckBox ID="cbx_Refund" runat="server" Enabled="<%# ((ViewPponCouponWithSmsCountModel)(Container.DataItem)).IsRefundable %>" />
                                </td>
                                <td>
                                    <%# ((ViewPponCouponWithSmsCountModel)(Container.DataItem)).Coupon.CouponId %>
                                </td>
                                <td>
                                    <%# ((ViewPponCouponWithSmsCountModel)(Container.DataItem)).Coupon.SequenceNumber %>
                                </td>
                                <td>
                                    <%# ((ViewPponCouponWithSmsCountModel)(Container.DataItem)).Coupon.CouponCode %>
                                </td>
                                <td>
                                    <%# ((ViewPponCouponWithSmsCountModel)(Container.DataItem)).StatusMessage %>
                                </td>
                                <td>
                                    <%# ((ViewPponCouponWithSmsCountModel)(Container.DataItem)).SmsCount %>
                                </td>
                                <td>
                                    <asp:Button ID="btn_ResendSms" runat="server" CommandName="SmsLogStatus" CommandArgument='<%# ((ViewPponCouponWithSmsCountModel)(Container.DataItem)).Coupon.CouponId %>'
                                        Text="重新發送" Enabled="<%# ((ViewPponCouponWithSmsCountModel)(Container.DataItem)).SmsCount<=3 %>" class="btn btn-primary btn-medium" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="plProductInfo" Visible="false">
                        <asp:Repeater ID="rpt_Product" runat="server" OnItemDataBound="rpt_Product_ItemDataBound" >
                            <HeaderTemplate>
                                <tr>
                                    <th>規格
                                    </th>
                                    <th>總數
                                    </th>
                                    <th>本次退貨(可退數)
                                    </th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:HiddenField runat="server" ID="csvProductIds" Value='<%# string.Join(",",((SummarizedProductSpec)(Container.DataItem)).ReturnableProductIds) %>'/>
                                        <%# ((SummarizedProductSpec)(Container.DataItem)).Spec %>
                                    </td>
                                    <td>
                                        <%# ((SummarizedProductSpec)(Container.DataItem)).TotalCount %>
                                    </td>
                                    <td>
                                        <asp:HiddenField ID="hdReturnable" runat="server" Value="<%# ((SummarizedProductSpec)(Container.DataItem)).ReturnableCount %>" />
                                        <asp:DropDownList ID="returningCountSelections" runat="server" />
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </asp:Panel>
                    <tr>
                        <th>退貨原因
                        </th>
                        <td colspan="5" style="text-align: left;">
                            <asp:TextBox ID="tbx_RefundReason" runat="server" Width="600" class="fr-input-defaul"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btn_Refund" runat="server" Text="建立退貨單" OnClientClick="if(!confirm('確定申請退貨，並建立退貨單嗎？')){return false;};" OnClick="CreateReturnForm" class="btn btn-primary btn-medium" />
                        </td>
                    </tr>
                    <tr>
                        <th colspan="7" style="text-align: center;">
                            <asp:Label ID="lab_Message" runat="server" BorderColor="Red" Font-Bold="true"></asp:Label>
                        </th>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="zone" >
        <h1 runat="server" id="h1Deliver" class="btn btn-cancel btn-large" style="cursor: default;" visible="false">出貨資訊</h1>
        <div class="fr-group clearfix">
            <div class="fr-item">
                <asp:GridView class="list-tb" ID="gvDeliver" runat="server" AutoGenerateColumns="False" Visible ="false">
                    <Columns>
                        <asp:TemplateField HeaderText="出貨日期">
                            <ItemTemplate>
                                <asp:Label ID="lbShipTime" Text='<%# Eval("ShipTime", "{0:yyyy-MM-dd}") %>' runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="運單編號">
                            <ItemTemplate>
                                <asp:Label ID="lbShipNo" Text='<%# Bind("ShipNo") %>' runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="物流公司">
                            <ItemTemplate>
                                <asp:Label ID="lbShipCompanyId" Text='<%# Bind("ShipCompanyName") %>' runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="出貨備註">
                            <ItemTemplate>
                                <asp:Label ID="lbShipMemo" runat="server" Text='<%# Bind("ShipMemo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="最後修改日期">
                            <ItemTemplate>
                                <asp:Label ID="lbModifyTime" Text='<%# Eval("OrderShipModifyTime", "{0:yyyy-MM-dd HH:mm:ss}") %>'
                                    runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <div class="zone">
        <h1 class="btn btn-cancel btn-large" style="cursor: default;">退貨紀錄</h1>
        <div class="fr-group clearfix">
            <div class="fr-item">
                <table class="list-tb">
                    <asp:Repeater ID="rpt_ReturnForms" runat="server" OnItemDataBound="rpt_ReturnFormsBound">
                        <HeaderTemplate>
                            <thead>
                                <tr>
                                    <th>申請時間
                                    </th>
                                    <th>申請退貨
                                    </th>
                                    <th>退貨原因
                                    </th>
                                    <th>處理時間
                                    </th>
                                    <th>退貨進度
                                    </th>
                                    <th>結案狀態
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <%# ((ReturnFormEntity)(Container.DataItem)).CreateTime.ToString("yyyy/MM/dd HH:mm") %>
                                </td>
                                <td><%# ((ReturnFormEntity)(Container.DataItem)).GetReturnSpec() %></td>
                                <td>
                                    <span title="<%# ((ReturnFormEntity)(Container.DataItem)).ReturnReason%>"><%# ((ReturnFormEntity)(Container.DataItem)).ReturnReason.TrimToMaxLength(20,"...") %></span>
                                </td>
                                <td>
                                    <%# ((ReturnFormEntity)(Container.DataItem)).LastProcessingTime.ToString("yyyy/MM/dd HH:mm") %>
                                </td>
                                <td>
                                    <asp:Literal ID="lit_Progress" runat="server"></asp:Literal>
                                </td>
                                <td><%# ((ReturnFormEntity)(Container.DataItem)).GetRefundedSpec() %></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#menu_1').addClass('tab-on');
        });
    </script>
</asp:Content>

﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ECoupon
{
    public partial class OrderDetail : RolePage, ILionTravelOrderDetailView
    {
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region property
        private LionTravelOrderDetailPresenter _presenter;
        public LionTravelOrderDetailPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public Guid OrderGuid
        {
            get
            {
                Guid oid;
                if (Guid.TryParse(Request.QueryString["oid"], out oid))
                {
                    return oid;
                }
                else
                {
                    return Guid.Empty;
                }
            }
        }
        public AgentChannel OrderClassType
        {
            get
            {
                if (User.Identity.Name.ToLower().IndexOf("@17life.com", StringComparison.Ordinal) > 0)
                {
                    return ChannelFacade.GetOrderClassificationByRequestType();
                }

                return ChannelFacade.GetOrderClassificationByName(User.Identity.Name);
            }
        }
        public string CustomDealUrl
        {
            get
            {
                if (OrderClassType == AgentChannel.LionTravelOrder)
                {
                    return "http://www.liontravel.com/webet2/web17se02.aspx?sDealGuid=";
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        public int OrderDeliveryType
        {
            set
            {
                plCouponInfo.Visible = rpt_Coupon.Visible = value == (int)DeliveryType.ToShop;
                plProductInfo.Visible = rpt_Product.Visible = value == (int)DeliveryType.ToHouse;
                hlInfo.InnerText = value == (int)DeliveryType.ToShop ? "憑證資訊" : "商品資訊";
            }
        }
        #endregion

        #region event
        public event EventHandler<DataEventArgs<OrderUserMemoList>> OrderUserMemoSet;
        public event EventHandler<DataEventArgs<KeyValuePair<string, List<int>>>> RefundCoupon;
        public event EventHandler<DataEventArgs<KeyValuePair<string, List<int>>>> ReturnProduct;
        public event EventHandler<DataEventArgs<int>> ReSendSms;
        public event EventHandler<DataEventArgs<int>> DeleteUserMemo;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void AddOrderUserMemo(object sneder, EventArgs e)
        {
            OrderUserMemoList user_memo = new OrderUserMemoList
            {
                OrderGuid = OrderGuid,
                DealType = (int)VbsDealType.Ppon,
                UserMemo = tbx_UserMemo.Text.Trim(),
                CreateId = User.Identity.Name,
                CreateTime = DateTime.Now
            };

            if (OrderUserMemoSet != null)
            {
                OrderUserMemoSet(this, new DataEventArgs<OrderUserMemoList>(user_memo));
            }
        }

        protected string GetReturnMessage(int coupon_id, CashTrustLogCollection cash_trust_logs, CashTrustLogCollection refund_cash_trust_logs, bool is_locked, out bool isrefundable)
        {
            isrefundable = false;
            CashTrustLog ctlog = cash_trust_logs.FirstOrDefault(x => x.CouponId == coupon_id);
            if (ctlog != null)
            {
                if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.AtmRefunding))
                {
                    return "ATM退款中，無法退貨";
                }

                switch ((TrustStatus)ctlog.Status)
                {
                    case TrustStatus.Initial:
                    case TrustStatus.Trusted:
                        CashTrustLog returningCtlog = refund_cash_trust_logs.FirstOrDefault(x => x.CouponId == coupon_id);
                        if (returningCtlog != null)
                        {
                            return "已在退貨單上，無法退貨";
                        }
                        if (is_locked)
                        {
                            isrefundable = false;
                            return "未核銷，預約鎖定中，無法退貨";
                        }
                        else
                        {
                            isrefundable = true;
                            return "未核銷，可進行退貨";
                        }
                    case TrustStatus.Verified:
                        DateTime modifyDT = ctlog.ModifyTime;
                        string modifyTime = string.Format("{0:yyyy/MM/dd HH:mm} ", modifyDT);

                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.VerificationForced))
                        {
                            return modifyTime + "已強制核銷";
                        }

                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.VerificationLost))
                        {
                            return modifyTime + "已核銷，清冊遺失";
                        }

                        return modifyTime + "已核銷";
                    case TrustStatus.Returned:
                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.ReturnForced))
                        {
                            return "已強制退貨";
                        }

                        return "已退購物金，無法再退貨";
                    case TrustStatus.Refunded:
                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.ReturnForced))
                        {
                            return "已強制退貨";
                        }

                        return "已轉退現金，無法再退貨";
                    case TrustStatus.ATM:
                        return "未付款，無法退貨";
                    default:
                        return "查無即時核銷資訊";
                }
            }

            return "查無即時核銷資訊";
        }

        public void CreateReturnForm(object sender, EventArgs e)
        {
            if (rpt_Coupon.Items.Count > 0)
            {
                List<int> coupons = new List<int>();
                foreach (RepeaterItem item in rpt_Coupon.Items)
                {
                    CheckBox cbx = (CheckBox)item.FindControl("cbx_Refund");
                    HiddenField hid = (HiddenField)item.FindControl("hid_CouponId");
                    int coupon_id;
                    if (cbx.Checked && int.TryParse(hid.Value, out coupon_id))
                    {
                        coupons.Add(coupon_id);
                    }
                }
                if (coupons.Count > 0 && RefundCoupon != null)
                {
                    RefundCoupon(sender, new DataEventArgs<KeyValuePair<string, List<int>>>(new KeyValuePair<string, List<int>>(tbx_RefundReason.Text, coupons)));
                }
            }
            else
            {
                //1. find items in listview
                //2. create refund form check
                List<int> orderProductIds = new List<int>();
                foreach (RepeaterItem item in rpt_Product.Items)
                {
                    string rtnCount = ((DropDownList)item.FindControl("returningCountSelections")).SelectedValue;
                    string prodIds = ((HiddenField)item.FindControl("csvProductIds")).Value;
                    int count = int.Parse(rtnCount);
                    IEnumerable<string> prods = prodIds.Split(',');
                    prods
                        .Take(count)
                        .ForEach(id => orderProductIds.Add(int.Parse(id)));
                }
                if (orderProductIds.Count > 0 && ReturnProduct != null)
                {
                    ReturnProduct(sender, new DataEventArgs<KeyValuePair<string, List<int>>>(new KeyValuePair<string, List<int>>(tbx_RefundReason.Text, orderProductIds)));
                }
            }
        }

        protected void rpt_ReturnFormsBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ReturnFormEntity)
            {
                ReturnFormEntity form = (ReturnFormEntity)e.Item.DataItem;
                var lit_Progress = e.Item.FindControl("lit_Progress") as Literal;

                string refund_description;
                switch (form.RefundType)
                {
                    case RefundType.Scash:
                        refund_description = "退回原款項";//"退購物金";
                        break;
                    case RefundType.Cash:
                    case RefundType.ScashToCash:
                        refund_description = "刷退";
                        break;
                    case RefundType.Atm:
                    case RefundType.ScashToAtm:
                        refund_description = "退ATM";
                        break;
                    case RefundType.Tcash:
                    case RefundType.ScashToTcash:
                        refund_description = string.Format("退{0}", Helper.GetEnumDescription(form.ThirdPartyPaymentSystem));
                        break;
                    default:
                        refund_description = "資料有問題, 請聯絡技術部";
                        break;
                }

                lit_Progress.Text = string.Format("[{0}]{1}{2}", refund_description, "<br/>", OrderFacade.GetRefundStatus(form));
            }
        }

        protected void CouponItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "SmsLogStatus")
            {
                int couponid;
                if (int.TryParse(e.CommandArgument.ToString(), out couponid) && ReSendSms != null)
                {
                    ReSendSms(sender, new DataEventArgs<int>(couponid));
                }
            }
        }


        protected void DeleteMemoCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "DeleteMemo")
            {
                int id;
                if (int.TryParse(e.CommandArgument.ToString(), out id) && DeleteUserMemo != null)
                {
                    DeleteUserMemo(sender, new DataEventArgs<int>(id));
                }
            }
        }

        protected void rpt_Product_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            #region 塞可退貨下拉選單的數字

            HiddenField hdReturnable = (HiddenField)e.Item.FindControl("hdReturnable");
            if (hdReturnable != null)
            {
                DropDownList returnableSelections = (DropDownList)e.Item.FindControl("returningCountSelections");
                var num = new List<int>();
                for (int i = 0; i <= int.Parse(hdReturnable.Value); i++)
                {
                    num.Add(i);
                }

                returnableSelections.DataSource = num;
                returnableSelections.DataBind();
                returnableSelections.SelectedValue = hdReturnable.Value;
            }
            
            #endregion
        }

        #region method
        public void SetOrderInfo(OrderCorresponding order_corresponding, ViewPponDeal deal, OrderUserMemoListCollection user_memos)
        {
            lab_Message.Text = string.Empty;
            lab_DealName.Text = "<a href='" + config.SiteUrl + "/" + deal.BusinessHourGuid +
                               "' target='_blank'>" + deal.ItemName + "</a><br/><a href='" + CustomDealUrl + deal.BusinessHourGuid + "' target='_blank'>" + Helper.GetEnumDescription(OrderClassType) + "連結</a>";
            lab_DealBusinessTime.Text = deal.BusinessHourOrderTimeS.ToString("yyyy/MM/dd HH:mm:ss") + " ~ " + deal.BusinessHourOrderTimeE.ToString("yyyy/MM/dd HH:mm:ss");
            lab_DealDeliveryTime.Text = (deal.BusinessHourDeliverTimeS.Value).ToString("yyyy/MM/dd") + " ~ " + (deal.BusinessHourDeliverTimeE.Value).ToString("yyyy/MM/dd");
            lab_RelatedOrderId.Text = order_corresponding.RelatedOrderId;
            lab_OrderGuid.Text = order_corresponding.OrderGuid.ToString();
            lab_OrderId.Text = order_corresponding.OrderId;
            lab_Mobile.Text = order_corresponding.Mobile;
            lab_CreatedTime.Text = order_corresponding.CreatedTime.ToString("yyyy/MM/dd HH:mm:ss");

            repOrderUserMemo.DataSource = user_memos.OrderByDescending(x => x.CreateTime);
            repOrderUserMemo.DataBind();
            pan_UserMemo.Visible = user_memos.Count > 0;
        }

        public void SetOrderUserMemoInfo(OrderUserMemoListCollection user_memos)
        {
            lab_Message.Text = tbx_UserMemo.Text = string.Empty;
            repOrderUserMemo.DataSource = user_memos.OrderByDescending(x => x.CreateTime);
            repOrderUserMemo.DataBind();
            pan_UserMemo.Visible = user_memos.Count > 0;
        }

        public void SetCouponInfo(ViewPponCouponCollection coupons, CashTrustLogCollection cash_trust_logs, CashTrustLogCollection refund_cash_trust_logs, Dictionary<int, int> sms_count_list, IList<ReturnFormEntity> return_forms)
        {
            List<ViewPponCouponWithSmsCountModel> data = new List<ViewPponCouponWithSmsCountModel>();
            foreach (var item in coupons)
            {
                bool isrefundable;
                ViewPponCouponWithSmsCountModel model = new ViewPponCouponWithSmsCountModel() { Coupon = item };
                model.StatusMessage = GetReturnMessage(item.CouponId ?? 0, cash_trust_logs, refund_cash_trust_logs, item.IsReservationLock ?? false, out isrefundable);
                model.IsRefundable = isrefundable;
                model.SmsCount = sms_count_list.ContainsKey(item.CouponId ?? 0) ? sms_count_list[item.CouponId ?? 0] : 0;
                data.Add(model);
            }
            rpt_Coupon.DataSource = data;
            rpt_Coupon.DataBind();

            rpt_ReturnForms.DataSource = return_forms;
            rpt_ReturnForms.DataBind();
        }

        public void SetProductInfo(IEnumerable<SummarizedProductSpec> source, IList<ReturnFormEntity> return_forms)
        {
            rpt_Product.DataSource = source;
            rpt_Product.DataBind();

            rpt_ReturnForms.DataSource = return_forms;
            rpt_ReturnForms.DataBind();
        }

        public void SetDeliverInfo(ViewOrderShipListCollection osCol)
        {
            if (osCol.Any() && osCol.FirstOrDefault().OrderShipModifyTime != null)
            {
                gvDeliver.DataSource = osCol;
                gvDeliver.DataBind();
                h1Deliver.Visible = gvDeliver.Visible = true;
            }
            else
            {
                h1Deliver.Visible = gvDeliver.Visible = false;
            }
        }

        public void ShowNoData()
        {
            lab_Message.Text = "查無資料";
        }

        public void ShowMessage(string message)
        {
            lab_Message.Text = message;
        }
        #endregion
    }

    public class ViewPponCouponWithSmsCountModel
    {
        public ViewPponCoupon Coupon { get; set; }
        public int SmsCount { get; set; }
        public string StatusMessage { get; set; }
        public bool IsRefundable { get; set; }
    }
    
    //public class PponProductModel
    //{
    //    public SummarizedProductSpec Product { get; set; }
    //}
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CouponList.aspx.cs" MasterPageFile="~/ECoupon/ECoupon.Master" Inherits="LunchKingSite.Web.ECoupon.CouponList" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.Web.ECoupon" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div class="zone">
        <h1 class="btn btn-cancel btn-large" style="cursor: default;">憑證查詢</h1>
        <div class="fr-group fr-bg-gray clearfix">
            <div class="fr-item">
                <label class="fr-label">查詢篩選</label>
                <div class="fr-content">
                    <asp:DropDownList ID="ddl_FilterType" runat="server">
                        <asp:ListItem Text="依憑證號碼" Value="0"></asp:ListItem>
                        <asp:ListItem Text="依驗證碼" Value="1"></asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    <asp:TextBox ID="tbx_FilterData" runat="server" class="fr-input-defaul"></asp:TextBox>
                    <asp:Button ID="btn_Search" runat="server" Text="查詢" OnClick="SearchCoupon" class="btn btn-primary btn-medium" />
                    查詢結果為目前最新的憑證狀態
                </div>
            </div>
        </div>
    </div>
    <div class="zone">
        <asp:Panel ID="pan_NoData" runat="server" Visible="false">查無資料</asp:Panel>
        <table class="list-tb">
            <asp:Repeater ID="rp_CouponListMain" runat="server">
                <HeaderTemplate>
                    <thead>
                        <tr>
                            <th>訂購日期
                            </th>
                            <th>憑證ID
                            </th>
                            <th>憑證號碼<br />
                                確認碼
                            </th>
                            <th>訂單編號
                            </th>
                            <th><%=Helper.GetEnumDescription(OrderClassType)%>訂編
                            </th>
                            <th>手機號碼
                            </th>
                            <th>憑證狀態
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <%# ((ViewOrderCorrespondingCouponStatusModel)(Container.DataItem)).Coupon.CreatedTime.ToString("yyyy/MM/dd") %>
                        </td>
                        <td>
                            <%# ((ViewOrderCorrespondingCouponStatusModel)(Container.DataItem)).Coupon.CouponId %>
                        </td>
                        <td>
                            <%# ((ViewOrderCorrespondingCouponStatusModel)(Container.DataItem)).Coupon.SequenceNumber %><br />
                            <%# ((ViewOrderCorrespondingCouponStatusModel)(Container.DataItem)).Coupon.Code%>
                        </td>
                        <td>
                            <a href="../ECoupon/OrderDetail.aspx?oid=<%# ((ViewOrderCorrespondingCouponStatusModel)(Container.DataItem)).Coupon.OrderGuid %>&type=<%# ((ViewOrderCorrespondingCouponStatusModel)(Container.DataItem)).Coupon.Type %>" target="_blank"><%# ((ViewOrderCorrespondingCouponStatusModel)(Container.DataItem)).Coupon.OrderGuid %></a>
                            <br />
                            <%# ((ViewOrderCorrespondingCouponStatusModel)(Container.DataItem)).Coupon.OrderId%>
                        </td>
                        <td>
                            <%# ((ViewOrderCorrespondingCouponStatusModel)(Container.DataItem)).Coupon.RelatedOrderId %>
                        </td>
                        <td>
                            <%# ((ViewOrderCorrespondingCouponStatusModel)(Container.DataItem)).Coupon.Mobile %>
                        </td>
                        <td>
                            <%# ((ViewOrderCorrespondingCouponStatusModel)(Container.DataItem)).StatusMessage %>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody> </table>
                </FooterTemplate>
            </asp:Repeater>
        </table>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#menu_2').addClass('tab-on');
        });
    </script>
</asp:Content>

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;


public partial class themes_default_controls_footer : LunchKingSite.Core.UI.BaseUserControl
{
    private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
    public static ISysConfProvider SystemConfig
    {
        get
        {
            return config;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
	{
	}
}

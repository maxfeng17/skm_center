﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoginPanel.ascx.cs"
    Inherits="LunchKingSite.Web.Themes.controls.LoginPanel" %>
<%@ Import Namespace="LunchKingSite.WebLib" %>
<%@ Register TagPrefix="uc1" TagName="FacebookLoginButton" Src="~/UserControls/FacebookLoginButton.ascx" %>

<div class="PopLogin">
    <div class="PopLoginClose" id="hc" style="cursor: pointer">
        <img src="<%=ResolveClientUrl("~/Themes/PCweb/images/PopLoginClose.png")%>" alt="" /></div>
    <div class="Title">
        <div class="TitleText">
            請選擇您的登入方式</div>
        <div class="SignUp">
            <a href="<%=LunchKingSite.BizLogic.Component.MemberActions.MemberUtilityCore.RegisterUrl %>" target="_blank" style="color: #4B2C12;
                font: bold 20px 微軟正黑體;">註冊</a></div>
    </div>
    <div class="IDSelect">
        <div id="M17LifeLogin">
            <a href="<%=ResolveUrl(FormsAuthentication.LoginUrl)%>">
                <img src="<%=ResolveClientUrl("~/Themes/PCweb/images/17LI_B.png")%>" alt="" border="0" />
            </a>
        </div>
        <div id="FBLogin">
            <uc1:FacebookLoginButton ID="FacebookLoginButton1" runat="server" ButtonImageUrl="~/Themes/PCweb/images/FBLI_B.png" />
        </div>
        <div id="PEZLogin">
            <a href='<%=WebUtility.GetPayEasySsoPath()%>'>
                <img src="<%=ResolveClientUrl("~/Themes/PCweb/images/PEZLI_B.png")%>" alt="" border="0" />
            </a>
        </div>
        <div class="HelpText">
            使用17Life帳號登入</div>
        <div class="HelpText">
            使用Facebook帳號登入</div>
        <div class="HelpText">
            使用PayEasy帳號登入</div>
    </div>
</div>

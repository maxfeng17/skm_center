﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HomeLoginPanel.ascx.cs"
    Inherits="LunchKingSite.Web.Themes.controls.HomeLoginPanel" %>
<%@ Import Namespace="LunchKingSite.WebLib" %>
<%@ Register TagPrefix="uc1" TagName="FacebookLoginButton" Src="~/UserControls/FacebookLoginButton.ascx" %>
<link href="../images/17Life/G2/ppon.css" rel="stylesheet" type="text/css" />
<div class="PopLogin">
    <div class="PopLoginClose" onclick=" $.unblockUI();" style="cursor: pointer">
        <img src="<%=ResolveUrl("~/Themes/PCweb/images/PopLoginClose.png")%>" alt="" /></div>
    <div class="Title">
        <div class="TitleText">
            請選擇您的登入方式</div>
        <div class="SignUp">
            <a href="<%=LunchKingSite.BizLogic.Component.MemberActions.MemberUtilityCore.RegisterUrl %>" target="_blank" style="color:#4B2C12;font: bold 20px 微軟正黑體;">註冊</a></div>
    </div>
    <div class="IDSelect">
        <div id="M17LifeLogin">
            <a href="<%=ResolveUrl(FormsAuthentication.LoginUrl)%>">
                <img src="<%=ResolveUrl("~/Themes/PCweb/images/17LI_B.png")%>" alt="" border="0" />
            </a>
        </div>
        <div id="FBLogin">
            <uc1:FacebookLoginButton ID="FacebookLoginButton1" runat="server" ButtonImageUrl="~/Themes/PCweb/images/FBLI_B.png" />
        </div>
        <div id="PEZLogin">
            <a href='<%=WebUtility.GetPayEasySsoPath()%>'>
                <img src="<%=ResolveUrl("~/Themes/PCweb/images/PEZLI_B.png")%>" alt="" border="0" />
            </a>
        </div>
        <div class="HelpText">
            使用17Life帳號登入</div>
        <div class="HelpText">
            使用Facebook帳號登入</div>
        <div class="HelpText">
            使用PayEasy帳號登入</div>
    </div>
</div>
<%--<div class="MultiLogin">
    <div class="ML_Top">
    </div>
    <div class="ML_Content">
        <!--Payeasy Login-->
        <div class="MD B_Payeasy">
            <div class="MD_Content">
                您是PayEasy會員嗎?<br />
                可以直接登入喔~</div>
            <div class="MD_Btn">
                <a href='<%=WebUtility.GetPayEasySsoPath()%>'>
                    <img src="../Themes/default/images/17Life/G1-8/ML_PEZ_BTN.gif" alt="" border="0" /></a></div>
        </div>
        <!--Facebook Login-->
        <div class="MD B_Facebook">
            <div class="MD_Content">
                您有Facebook帳號嗎?<br />
                馬上用FB帳號登入~</div>
            <div class="MD_Btn">
                <uc1:FacebookLoginButton ID="fbl" runat="server" ButtonImageUrl="~/Themes/default/images/17life/G1-8/ML_FB_BTN.gif" />
            </div>
        </div>

        <div class="clear">
        </div>
    </div>
    <div class="ML_End">
    </div>
</div>
--%>
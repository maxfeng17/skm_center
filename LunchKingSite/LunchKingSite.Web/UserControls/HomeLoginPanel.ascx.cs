﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.Themes.controls
{
	public partial class HomeLoginPanel : BaseUserControl
	{

        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public ISysConfProvider SystemConfig
        {
            get { return cp; }
        }

		protected void Page_Load(object sender, EventArgs e)
		{
            // IMPORTANT: in order to use this script, please include this control with the following stylesheet
            // "~/Themes/default/images/17Life/G1-8/IDCandML.css"
		}
	}
}
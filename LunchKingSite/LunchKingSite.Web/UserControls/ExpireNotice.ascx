﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExpireNotice.ascx.cs" Inherits="LunchKingSite.Web.Themes.Default.controls.ExpireNotice" %>

<div id="expirationContainer" runat="server">
    <%-- 網路版好康 --%>
    <div id="PponExpireContent" runat="server" ClientIDMode="Static" Visible="false">
        <div style="width:370;" class="MaindealClosed"> 
            <p>         
                <asp:Literal runat="server" ID="PponClosedDownText" /> <%-- XXX《OOO》於yyyy/mm/dd結束營業，XXX《△△△》於yyyy/mm/dd結束營業 --%>
                <asp:Literal runat="server" ID="messageAfterPponClosedDownText" Text="，尚未使用憑證者，17Life將予以退費，請您連繫17Life客服中心或留意後續通知信件。" Visible="false" />
                <asp:Literal runat="server" ID="PponExpireDateChangedText" /> <%-- XXX《OOO》於yyyy/mm/dd停止好康兌換，XXX《△△△》使用期限延長至yyyy/mm/dd --%>
                <asp:Literal runat="server" ID="messageAfterPponExpireDateChangedText" Text="，請於使用期間內盡速至店家兌換好康。" Visible="false"/>
                如有其他問題，請參考"<a href="<%=SystemConfig.SiteUrl %>/Ppon/NewbieGuide.aspx"><span class="MaiClBlue">常見問題</span></a>"。
            <p>
        </div>    
    </div>


    <%-- 手機版好康 --%>
    <div id="MobilePponExpireContent" runat="server" ClientIDMode="Static" Visible="false" class="contentbox tal BS_white BD_green">
        <div class="itembox f13 lh136"> 
            <span>                        
                <asp:Literal runat="server" ID="MobilePponClosedDownText" /> <%-- XXX《OOO》於yyyy/mm/dd結束營業，XXX《△△△》於yyyy/mm/dd結束營業 --%>
                <asp:Literal runat="server" ID="messageAfterMobilePponClosedDownText" Text="，尚未使用憑證者，17Life將予以退費，請您連繫17Life客服中心或留意後續通知信件。" Visible="false" />
                <asp:Literal runat="server" ID="MobilePponExpireDateChangedText" /> <%-- XXX《OOO》於yyyy/mm/dd停止好康兌換，XXX《△△△》使用期限延長至yyyy/mm/dd --%>
                <asp:Literal runat="server" ID="messageAfterMobilePponExpireDateChangedText" Text="，請於使用期間內盡速至店家兌換好康。" Visible="false"/>
                如有其他問題，請參考"<a href="<%=SystemConfig.SiteUrl %>/Ppon/NewbieGuide.aspx"><span class="MaiClBlue">常見問題</span></a>"。
            </span>
        </div>    
    </div>

<%--<div id="HiDealExpireContent" runat="server" ClientIDMode="Static" Visible="false">
</div>--%>

</div>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="footer.ascx.cs" Inherits="themes_default_controls_footer" %>
<%@ Import Namespace="LunchKingSite.WebLib" %>

<div class="p3footer block-background">
            <div class="footer_content">
                <div class="contentblock_300 info-17life">
                    <div class="footer_logo">
                        <svg width="90" height="48">
                            <image xlink:href="<%=ResolveUrl("~/Themes/PCweb/images/footer_150x80.svg") %>" src="<%=ResolveUrl("~/Themes/PCweb/images/footer_150x80.png") %>" width="90" height="48"></image>
                        </svg>
                    </div>
                    <p>
                    
                        版權所有 © <script language="javascript"> var Today = new Date(); document.write(Today.getFullYear()); </script> 康太數位整合股份有限公司<br />
                        Taiwan (R.O.C) 104 台北市中山北路一段11號13樓</p>
                    <p>
                    <div class="support_c"><p class="fa fa-headphones fa-fw" style="width:100%">
                        <a href="/User/ServiceList" style="color:#ccc">客服中心</a></p> 
                    </div>
                    <p class="visaTitle_rwd">Visa Offical Partner</p>
                </div>
                <div class="contentblock_180 part-17life">
                    <div class="TP_17life">
                    </div>
                    <div class="contentlist">
                        <ul>
                            <li><a href="/about/us.html" target="_blank">關於我們</a></li>
							<li><a href="/about/history.html" target="_blank">品牌紀事</a></li>
							<li><a href="/about/news.html" target="_blank">新聞發布</a></li>
                            <li><a href="https://www.17life.com/blog/1f4bae4e-dbda-494e-b671-85eac535e02d" target="_blank">預約系統</a></li>
                            <li><a href="<%=ResolveUrl("~/newmember/privacypolicy.aspx")%>">服務條款</a></li>
                            <li><a href="<%=ResolveUrl("~/newmember/privacy.aspx")%>">隱私權政策</a></li>
                            <li><a href="/ppon/newbieguide.aspx?s=88&q=89">電子發票</a></li>
                            <li><a href="javascript:void(window.open('http://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=484a426b56463e6a40583a1d1d1d1d5f2443a363189j56&jobsource=n104bank1'))">
                                人力招募</a></li>
                            <%--<li><a href="javascript:void(window.open('/ApologizeToGOMAJI.aspx'))">聲明公告</a></li>--%>
                        </ul>
                    </div>
                </div>
                <div class="contentblock_180 part-pez-cert">
                    
                    <div class="TP_Certification">
                    </div>
                    <div class="contentlist">
                        <ul>
                            <li><a href="http://www.sosa.org.tw/ec/ec_info.asp?markid=24317014" target="_blank">
                                SOSA優良電子商店</a></li>
                            <li>2011電子商務Top50</li>
                        </ul>
                    </div>
                </div>
                <div class="contentblock_300 part-serivce">
                    <div class="TP_Service">
                    </div>
                    <div class="contentlist">
                        <ul>
                            <li><a href="<%=SystemConfig.SiteUrl+"/Ppon/ContactUs"%>" target="_blank">合作提案</a></li>
                            <li><a href="<%=ResolveUrl("~/Ppon/NewbieGuide.aspx")%>">常見問題</a></li>
                            <li><a href="/User/ServiceList">17Life客服中心</a></li>
                            <li>客服諮詢專線：<%=SystemConfig.ServiceTel %></li>
                        </ul>
                    </div>
                    <p>
                        &nbsp;&nbsp;服務時間：平日 09:00 ~ 18:00
                        <span class="visaTitle">&nbsp;&nbsp;Visa Offical Partner</span>
                    </p>
                    <div class="TP_twService"></div><!--品質領先(品生活需隱藏)-->
                      <div class="twService">  
                        <svg class="twService-img" width="86" height="80">
                          <image xlink:href='<%=ResolveUrl("~/Themes/PCweb/images/twService_logo.svg")%>' src='<%=ResolveUrl("~/Themes/PCweb/images/twService_logo.png")%>' width="86" height="80" />
                        </svg>
                        <p>服務品質領先，團購唯一獲獎！17Life榮獲【2015臺灣服務業大評鑑】大型購物網站銀牌！</p>
                      </div>  
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>

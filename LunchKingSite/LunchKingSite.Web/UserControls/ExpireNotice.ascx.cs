﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.UI;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.Themes.Default.controls
{
    public partial class ExpireNotice : BaseUserControl, IExpireNotice
    {
        /// <summary>
        /// 檔次類型
        /// </summary>
        public ExpireNoticeMode ControlMode { get; set; }

        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public ISysConfProvider SystemConfig
        {
            get { return cp; }
        }


        /// <summary>
        /// 賣家名稱
        /// </summary>
        public string SellerName { get; set; }
        
        /// <summary>
        /// 賣家結束營業日
        /// </summary>
        public DateTime? SellerCloseDownDate { get; set; }

        private IDictionary<string, DateTime> _storeCloseDownDates = new Dictionary<string, DateTime>();
        /// <summary>
        /// 分店結束營業日 - key: 分店名稱 value: 分店結束營業日
        /// </summary>
        public IDictionary<string, DateTime> StoreCloseDownDates
        {
            get { return _storeCloseDownDates; }
            set { _storeCloseDownDates = value; }
        }

        /// <summary>
        /// 憑證原始使用截止日
        /// </summary>
        public DateTime DealOriginalExpireDate { get; set; }

        /// <summary>
        /// 檔次調整截止日
        /// </summary>
        public DateTime? DealChangedExpireDate { get; set; }

        private IDictionary<string, DateTime> _storeChangedExpireDates = new Dictionary<string, DateTime>();
        /// <summary>
        /// 分店調整截止日 - key: 分店名稱 value: 分店調整過的截止日
        /// </summary>
        public IDictionary<string, DateTime> StoreChangedExpireDates
        {
            get { return _storeChangedExpireDates; } 
            set { _storeChangedExpireDates = value; }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            switch (ControlMode)
            {
                case ExpireNoticeMode.WebPpon:
                    SetPponContent();
                    break;
                case ExpireNoticeMode.MobilePpon:
                    SetMobilePponContent();
                    break;
                default:
                    expirationContainer.Visible = false;
                    break;

            }
        }

        private void SetPponContent()
        {
            // 無賣家/分店倒店  & 無檔次/分店截止日調整 
            if (NoNoticeNeeded())
            {
                PponExpireContent.Visible = false;
            }

            PponClosedDownText.Text = string.Empty;
            PponExpireDateChangedText.Text = string.Empty;

            int textLength = 0;

            #region 有賣家/分店倒店

            if (SellerCloseDownDate.HasValue || StoreCloseDownDates.Count != 0)
            {
                StringBuilder sb = new StringBuilder();

                List<string> messages = new List<string>();

                if (SellerCloseDownDate.HasValue)
                {
                    messages.Add(string.Format("<span class='MaiClBlue'>{0}</span>於<span class='MaiClRed'>{1}結束營業</span>", SellerName, SellerCloseDownDate.Value.ToString("yyyy/MM/dd")));
                }

                if (StoreCloseDownDates.Count > 0)
                {
                    foreach (KeyValuePair<string, DateTime> pair in StoreCloseDownDates)
                    {
                        messages.Add(string.Format("<span class='MaiClBlue'>{0}{1}</span>於<span class='MaiClRed'>{2}結束營業</span>", SellerName, pair.Key, pair.Value.ToString("yyyy/MM/dd")));
                    }
                }

                sb.Append(string.Join("，", messages));

                if (sb.Length > 0)
                {
                    textLength += sb.Length;
                    messageAfterPponClosedDownText.Visible = true;
                }

                PponClosedDownText.Text = sb.ToString();
            }

            #endregion

            #region 有檔次/分店截止日調整

            if (DealChangedExpireDate.HasValue || StoreChangedExpireDates.Count > 0)
            {
                StringBuilder sb = new StringBuilder();

                List<string> expireMessages = new List<string>();

                #region 檔次截止時間調整

                if (DealChangedExpireDate.HasValue)
                {
                    if (DealChangedExpireDate.Value < DealOriginalExpireDate)
                    {
                        if (DealChangedExpireDate > DateTime.Now)
                        {
                            expireMessages.Add(string.Format("<span class='MaiClBlue'>{0}</span>於<span class='MaiClRed'>{1}停止兌換</span>", SellerName, DealChangedExpireDate.Value.ToString("yyyy/MM/dd")));
                        }
                    }
                    else
                    {
                        expireMessages.Add(string.Format("<span class='MaiClBlue'>{0}</span><span class='MaiClRed'>使用期限延長至{1}</span>", SellerName, DealChangedExpireDate.Value.ToString("yyyy/MM/dd")));
                    }
                }

                #endregion

                #region 分店截止時間提前

                Dictionary<string, DateTime> shortenedStores = StoreChangedExpireDates.Where(store => store.Value < DealOriginalExpireDate).ToDictionary(store => store.Key, store => store.Value);
                foreach (KeyValuePair<string, DateTime> pair in shortenedStores)
                {
                    if (pair.Value > DateTime.Now)
                    {
                        expireMessages.Add(string.Format("<span class='MaiClBlue'>{0}{1}</span>於<span class='MaiClRed'>{2}停止兌換</span>", SellerName, pair.Key, pair.Value.ToString("yyyy/MM/dd")));
                    }  
                }

                #endregion

                #region 分店截止時間延長

                Dictionary<string, DateTime> extendedStores = StoreChangedExpireDates.Where(store => store.Value > DealOriginalExpireDate).ToDictionary(store => store.Key, store => store.Value);
                foreach (KeyValuePair<string, DateTime> pair in extendedStores)
                {
                    expireMessages.Add(string.Format("<span class='MaiClBlue'>{0}{1}</span><span class='MaiClRed'>使用期限延長至{2}</span>", SellerName, pair.Key, pair.Value.ToString("yyyy/MM/dd")));
                }

                #endregion

                sb.Append(string.Join("，", expireMessages));

                if (sb.Length > 0)
                {
                    textLength += sb.Length;
                    messageAfterPponExpireDateChangedText.Visible = true;
                }

                PponExpireDateChangedText.Text = sb.ToString();
            }

            #endregion

            if (textLength == 0)
            {
                PponExpireContent.Visible = false;
            }
            else
            {
                PponExpireContent.Visible = true;
            }
        }

        private void SetMobilePponContent()
        {
            // 無賣家/分店倒店  & 無檔次/分店截止日調整 
            if (NoNoticeNeeded())
            {
                MobilePponExpireContent.Visible = false;
            }
            
            MobilePponClosedDownText.Text = string.Empty;
            MobilePponExpireDateChangedText.Text = string.Empty;
           
            int textLength = 0; 

            #region 有賣家/分店倒店

            if (SellerCloseDownDate.HasValue || StoreCloseDownDates.Count != 0)
            {
                StringBuilder sb = new StringBuilder();
                
                List<string> messages = new List<string>();

                if (SellerCloseDownDate.HasValue)
                {
                    messages.Add(string.Format("<span class='T_blue'>{0}</span>於<span class='T_red'>{1}結束營業</span>", SellerName, SellerCloseDownDate.Value.ToString("yyyy/MM/dd")));
                }

                if (StoreCloseDownDates.Count > 0)
                {
                    foreach (KeyValuePair<string, DateTime> pair in StoreCloseDownDates)
                    {
                        messages.Add(string.Format("<span class='T_blue'>{0}{1}</span>於<span class='T_red'>{2}結束營業</span>", SellerName, pair.Key, pair.Value.ToString("yyyy/MM/dd")));
                    }
                }

                sb.Append(string.Join("，", messages));
                
                if(sb.Length > 0)
                {
                    textLength += sb.Length;
                    messageAfterMobilePponClosedDownText.Visible = true;
                }

                MobilePponClosedDownText.Text = sb.ToString();
            }

            #endregion

            #region 有檔次/分店截止日調整

            if (DealChangedExpireDate.HasValue || StoreChangedExpireDates.Count > 0)
            {
                StringBuilder sb = new StringBuilder();

                List<string> expireMessages = new List<string>();

                #region 檔次截止時間調整

                if (DealChangedExpireDate.HasValue)
                {
                    if (DealChangedExpireDate.Value < DealOriginalExpireDate)
                    {
                        if (DealChangedExpireDate.Value > DateTime.Now)
                        {
                            expireMessages.Add(string.Format("<span class='T_blue'>{0}</span>於<span class='T_red'>{1}停止兌換</span>", SellerName, DealChangedExpireDate.Value.ToString("yyyy/MM/dd")));
                        }
                    }     
                    else
                    {
                        expireMessages.Add(string.Format("<span class='T_blue'>{0}</span><span class='T_red'>使用期限延長至{1}</span>", SellerName, DealChangedExpireDate.Value.ToString("yyyy/MM/dd")));
                    }
                }

                #endregion

                #region 分店截止時間提前

                Dictionary<string, DateTime> shortenedStores = StoreChangedExpireDates.Where(store => store.Value < DealOriginalExpireDate).ToDictionary(store => store.Key, store => store.Value);
                foreach (KeyValuePair<string, DateTime> pair in shortenedStores)
                {
                    if (pair.Value > DateTime.Now)
                    {
                        expireMessages.Add(string.Format("<span class='T_blue'>{0}{1}</span>於<span class='T_red'>{2}停止兌換</span>", SellerName, pair.Key, pair.Value.ToString("yyyy/MM/dd")));
                    }    
                }

                #endregion

                #region 分店截止時間延長

                Dictionary<string, DateTime> extendedStores = StoreChangedExpireDates.Where(store => store.Value > DealOriginalExpireDate).ToDictionary(store => store.Key, store => store.Value);
                foreach (KeyValuePair<string, DateTime> pair in extendedStores)
                {
                    expireMessages.Add(string.Format("<span class='T_blue'>{0}{1}</span><span class='T_red'>使用期限延長至{2}</span>", SellerName, pair.Key, pair.Value.ToString("yyyy/MM/dd")));
                }

                #endregion

                sb.Append(string.Join("，", expireMessages));
                
                if(sb.Length > 0)
                {
                    textLength += sb.Length;
                    messageAfterMobilePponExpireDateChangedText.Visible = true;
                }

                MobilePponExpireDateChangedText.Text = sb.ToString();
            }

            #endregion

            if (textLength == 0)
            {
                MobilePponExpireContent.Visible = false;
            }
            else
            {
                MobilePponExpireContent.Visible = true;
            }
        }

        private bool NoNoticeNeeded()
        {
            return !SellerCloseDownDate.HasValue && StoreCloseDownDates.Count == 0 && !DealChangedExpireDate.HasValue &&
                   StoreChangedExpireDates.Count == 0;
        }
    }
}
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Linq;
using System.Web.Security;
using System.Collections.Generic;
using System.Text;
using System.Web;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.Web.Themes.controls
{
    public partial class RandomParagraph : BaseUserControl, IRandomParagraphView
    {
        #region props

        private RandomParagraphPresenter _presenter;

        public RandomParagraphPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string ContentBody
        {
            get
            {
                return l1.Text;
            }
            set
            {
                l1.Text = value;
            }
        }

        public string UserName
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }

        public string UniqueName { get; set; }

        public string ContentName
        {
            get
            {
                if (ViewState["c"] != null)
                {
                    return ViewState["c"].ToString();
                }
                else
                {
                    if (Type == RandomCmsType.PponMasterPage || Type == RandomCmsType.PponBuyAd)
                    {
                        return this.ID;
                    }
                    else
                    {
                        string path = Page.Request.Path.ToLower();
                        if (path.Contains("todaydeals"))
                        {
                            path = "/ppon/todaydeals.aspx";
                        }
                        else if (path.Contains("todaydeal.aspx"))
                        {
                            path = "/ppon/default.aspx";
                        }
                        else if (path.Contains("store.aspx"))
                        {
                            path = "/ppon/default.aspx";
                        }
                        else if (path == "/default.aspx") //處理網址"www.17life.com"進入網站的情況
                        {
                            path = "/ppon/default.aspx";
                        }
                        else if (path == "/m/deal")
                        {
                            path = "/ppon/default.aspx";
                        }
                        else if (!path.Contains(".aspx"))
                        {
                            path = "/ppon/default.aspx";
                        }
                        string webRoot = VirtualPathUtility.AppendTrailingSlash(HttpContext.Current.Request.ApplicationPath);
                        return "/" + (path + "_" + this.ID.ReplaceIgnoreCase("_mbrowser", string.Empty)).TrimStart(webRoot, StringComparison.OrdinalIgnoreCase).TrimStart("/", StringComparison.OrdinalIgnoreCase).Replace("delivery", "default");
                    }
                }
            }
            set
            {
                ViewState["c"] = value;
            }
        }

        public string CssClass { get; set; }

        public int GroupBanner { get; set; }

        public string[] UserRoles
        {
            get
            {
                return Roles.GetRolesForUser();
            }
        }

        public int CityId { get; set; }

        public RandomCmsType Type { get; set; }

        public bool ExternalCache { get; set; }

        public bool IsexhibitionM { get; set; }

        #endregion props

        #region

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                this.Presenter.OnViewInitialized();
            }
            this.Presenter.OnViewLoaded();
        }

        public void SetContent(ViewCmsRandomCollection data)
        {
            string urlPath = Request.Path.ToLower();
            bool isRandom = data.All(x => x.ContentTitle != "Top Banner");
            bool isCuration = data.Any(x => x.ContentTitle == "Curation Banner");
            bool isBrand = (Page.Request.Url.AbsolutePath.ToLower().Replace("brandmobile.aspx", "brandevent.aspx").Contains("event/brandevent.aspx"));
            bool isWebCurationPage = (urlPath.Contains("themecurationchannel") || urlPath.Contains("event/exhibitionlist.aspx") || urlPath.Contains("event/brandevent.aspx"));
            DateTime now = DateTime.Now;

            StringBuilder sb = new StringBuilder();

            if (Page.Request.Url.AbsolutePath.ToLower().Contains("piinlife"))
            {
                CityId = PponCityGroup.DefaultPponCityGroup.Piinlife.CityId;
            }
            var filterDatas = data
                .Where(x => (x.CityId == CityId || x.CityId < 0) && now <= x.EndTime && now >= x.StartTime)
                .ToList();
            if (filterDatas.Count > 0)
            {
                if (!String.IsNullOrWhiteSpace(CssClass))
                {
                    sb.AppendFormat("<div class='{0}'>", CssClass);
                }

                if (isCuration)
                {
                    int i = 0;
                    foreach (var cms in filterDatas.OrderBy(x => x.Seq))
                    {
                        if (i == 0 && IsexhibitionM == false && !isBrand)
                        {
                            sb.Append("<h6>特選活動</h6>");
                        }

                        sb.AppendFormat("<li>{0}</li>", cms.Body);
                        i++;
                    }
                }
                else if (isRandom)
                {
                    //策展項目僅限策展頁
                    filterDatas = (!isWebCurationPage) ? filterDatas.Where(x => x.CityId != (int)CmsSpecialCityType.CurationPage).ToList() : filterDatas;

                    int total = 0;
                    for (int i = 0; i < filterDatas.Count(); i++)
                    {
                        total += filterDatas.ElementAt(i).Ratio;
                    }
                    Random r = new Random();
                    int randomNumber = r.Next(1, total + 1);
                    foreach (var item in filterDatas)
                    {
                        randomNumber -= item.Ratio;
                        if (randomNumber <= 0)
                        {
                            sb.Append(item.Body);
                            break;
                        }
                    }
                }
                else
                {
                    if (filterDatas.Count == 1)
                    {
                        sb.Append(filterDatas[0].Body);
                    }
                    else
                    {
                        #region Slider

                        List<string> dataList = filterDatas
                                                .OrderByDescending(t => t.Id)
                                                .Select(t => t.Body)
                                                .ToList();
                        if (dataList.Count > 0 && GroupBanner > 1 && (filterDatas.Count % GroupBanner) > 0)
                        {
                            int addADNumber = GroupBanner - (filterDatas.Count() % GroupBanner);
                            int addADTimes = 0;
                            do
                            {
                                //排除第一個避免兩檔重覆
                                dataList.Add(dataList[(dataList.Count > 1) ? ++addADTimes : addADTimes++]);
                            } while (addADTimes < addADNumber);
                        }

                        sb.AppendFormat("<div id='{0}' class='swiper-container'><div class='swiper-wrapper'><div class='swiper-slide'>"
                            , this.ID);

                        for (int i = 0; i < dataList.Count; i++)
                        {
                            if (i != 0 && i % GroupBanner == 0)
                            {
                                sb.Append("</div><div class='swiper-slide'>");
                            }

                            sb.AppendFormat("<div class='slide-bn-area'>{0}</div>", dataList[i]);
                        }
                        sb.Append("</div></div><div class='swiper-button-next'></div><div class='swiper-button-prev'></div></div>");
                        #endregion
                    }
                }
                if (!String.IsNullOrWhiteSpace(CssClass))
                {
                    sb.Append("</div>");
                }
            }
            l1.Text = sb.ToString();
        }
    }
}
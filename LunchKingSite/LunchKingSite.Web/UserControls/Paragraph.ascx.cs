using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Web.Security;
using System.Web.UI;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.Themes.controls
{
    [PartialCaching(0)]
    public partial class Paragraph : BaseUserControl, IParagraphView
    {

        private const int ParagraphCacheExpirationSecond =86400;
        #region props

        private ParagraphPresenter _presenter;

        public ParagraphPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string ContentBody
        {
            get
            {
                return l1.Text;
            }
            set
            {
                l1.Text = value;
            }
        }

        public string UserName
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }

        public string UniqueName { get; set; }

        public string ContentName
        {
            get
            {
                if (ViewState["s"] != null && !string.IsNullOrEmpty(ViewState["s"].ToString()))
                {
                    return ViewState["s"].ToString();
                }

                return (Page.Request.Path + "_" + this.ID + (string.IsNullOrEmpty((string)ViewState["c"]) ? "" : "_" + (string)ViewState["c"])).ToLower();
            }
            set
            {
                ViewState["c"] = value;
            }
        }

        public string SetContentName
        {
            get
            {
                return (ViewState["s"] == null) ? string.Empty : ViewState["s"].ToString();
            }
            set { ViewState["s"] = value; }
        }

        public string ContentControlId
        {
            get { return this.ID; }
        }


        public string[] UserRoles
        {
            get
            {
                return Roles.GetRolesForUser();
            }
        }

        public bool ExternalCache { get; set; }

        #endregion props


        public event EventHandler GetDataForCache = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
            }
            this.Presenter.OnViewLoaded();


            GetDataForCache(sender, e);
        }

        public void SetData(string contentBody)
        {
            this.ContentBody = contentBody;
        }

        public string GetDataFromCache(string key)
        {
            return Cache[key] as string;
        }

        public void SetCache(string key, string content)
        {
            Cache.Insert(key.ToLower(), content, null, Context.Timestamp.AddSeconds(ParagraphCacheExpirationSecond),
                System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.NotRemovable, null);
        }

    }
}
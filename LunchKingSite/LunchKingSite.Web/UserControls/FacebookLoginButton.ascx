﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FacebookLoginButton.ascx.cs" Inherits="LunchKingSite.Web.Themes.controls.FacebookLoginButton" EnableViewState="false" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<a href="#" onclick="fblogin(); return false;"><img src="<%=ButtonImageUrl%>" alt="facebook login" style="border:0px" /></a>
<script type="text/javascript">
    function fblogin() {
        FB.login(function (response) {
            if (response.authResponse)
                window.location.href = '<%=ReturnUrl%>' + "?atk=" + response.authResponse.accessToken;
        },
        { scope: '<%=FacebookUser.DefaultScope%>' });
    }
</script>
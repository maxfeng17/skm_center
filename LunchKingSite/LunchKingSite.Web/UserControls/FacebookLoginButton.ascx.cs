﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Themes.controls
{
	public partial class FacebookLoginButton : System.Web.UI.UserControl
	{
	    private string _imgUrl;
	    public string ButtonImageUrl
	    {
            get { return ResolveUrl(string.IsNullOrEmpty(_imgUrl) ? "~/Themes/default/images/17life/G1-8/17p_Header_16.png" : _imgUrl); }
            set { _imgUrl = value; }
	    }

		private string _returnUrl;
		public string ReturnUrl
		{
			get { return ResolveUrl(string.IsNullOrEmpty(_returnUrl) ? "~/user/sso.aspx" : _returnUrl); }
			set { _returnUrl = value; }
		}
	}
}
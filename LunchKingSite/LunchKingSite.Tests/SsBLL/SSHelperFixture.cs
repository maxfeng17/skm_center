﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Autofac;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.SsBLL;
using Moq;
using NUnit.Framework;
using SubSonic;

namespace LunchKingSite.Tests.SsBLL
{
    [TestFixture]
    public class SSHelperFixture
    {
        [SetUp]
        public void Setup()
        {
            var config = new Mock<ISysConfProvider>();
            var builder = new ContainerBuilder();
            builder.RegisterInstance(config.Object).As<ISysConfProvider>();
            var build = builder.Build();
            ProviderFactory.ReInit(build.BeginLifetimeScope());
        }
        [Test]
        public void in_statement_should_behave_correctly()
        {
            InStatementTest(Item.Columns.ItemId + " in I1,I2,I3,I4", 4, "I1", "I2", "I3", "I4");
            InStatementTest(Item.Columns.ItemId + " in (I1,I2,I3,I4)", 4, "I1", "I2", "I3", "I4");
            InStatementTest(Item.Columns.ItemId + " in I1,I2,I3),I4", 4, "I1", "I2", "I3)", "I4");
            InStatementTest(Item.Columns.ItemId + " in (I1,I2,I3,I4", 4, "I1", "I2", "I3", "I4");
            InStatementTest(Item.Columns.ItemId + " in (I1,I2,I3),I4", 3, "I1", "I2", "I3");
            InStatementTest(Item.Columns.ItemId + " in I1,(I2),I3,I4", 4, "I1", "(I2)", "I3", "I4");
        }

        private void InStatementTest(string filter, int argCount, params string[] args)
        {
            var qry = SSHelper.GetQueryByFilterOrder(Item.CreateQuery(), null, filter);
            Assert.IsNotNull(qry);
            Assert.AreEqual(Item.Columns.ItemId, qry.InColumn);
            Assert.AreEqual(argCount, qry.InList.Count());
            for (int i = 0; i < argCount; i++)
                Assert.AreEqual(args[i], qry.InList[i]);
        }

        [Test]
        public void GetWhereQCTest()
        {
            string transId = "nullpthpyk4345372192";
            LunchKingSite.Core.PaymentType paymentType = LunchKingSite.Core.PaymentType.Creditcard;
            PayTransType transType = PayTransType.Authorization;
            QueryCommand qc = SSHelper.GetWhereQC<PaymentTransaction>(
                PaymentTransaction.Columns.TransId + " = " + transId,
                PaymentTransaction.Columns.PaymentType + " = " + (int)paymentType,
                PaymentTransaction.Columns.TransType + " = " + (int)transType);

            Assert.AreEqual(3, qc.Parameters.Count);

            Assert.AreEqual(DbType.AnsiString, qc.Parameters[0].DataType);
            Assert.IsTrue(qc.Parameters[0].ParameterName.Contains(PaymentTransaction.Columns.TransId));
            Assert.AreEqual(transId, (string)qc.Parameters[0].ParameterValue);

            Assert.AreEqual(DbType.Int32, qc.Parameters[1].DataType);
            Assert.IsTrue(qc.Parameters[1].ParameterName.Contains(PaymentTransaction.Columns.PaymentType));
            Assert.AreEqual((int)paymentType, int.Parse((string)qc.Parameters[1].ParameterValue));

            Assert.AreEqual(DbType.Int32, qc.Parameters[2].DataType);
            Assert.IsTrue(qc.Parameters[2].ParameterName.Contains(PaymentTransaction.Columns.TransType));
            Assert.AreEqual((int)transType, int.Parse((string)qc.Parameters[2].ParameterValue));
        }

        [Test]
        public void GetQueryByFilterOrderForFilterTest()
        {
            string transId = "nullpthpyk4345372192";
            LunchKingSite.Core.PaymentType paymentType = LunchKingSite.Core.PaymentType.Creditcard;
            PayTransType transType = PayTransType.Authorization;
            Query qry = SSHelper.GetQueryByFilterOrderForFilter(new Query(PaymentTransaction.Schema.TableName), "",
                PaymentTransaction.Columns.TransId + " = " + transId,
                PaymentTransaction.Columns.PaymentType + " = " + (int)paymentType,
                PaymentTransaction.Columns.TransType + " = " + (int)transType);

            Assert.AreEqual(3, qry.Wheres.Count);

            Assert.AreEqual(DbType.AnsiString, qry.Wheres[0].DbType);
            Assert.IsTrue(qry.Wheres[0].ParameterName.Contains(PaymentTransaction.Columns.TransId));
            Assert.AreEqual(transId, (string)qry.Wheres[0].ParameterValue);

            Assert.AreEqual(DbType.Int32, qry.Wheres[1].DbType);
            Assert.IsTrue(qry.Wheres[1].ParameterName.Contains(PaymentTransaction.Columns.PaymentType));
            Assert.AreEqual((int)paymentType, int.Parse((string)qry.Wheres[1].ParameterValue));

            Assert.AreEqual(DbType.Int32, qry.Wheres[2].DbType);
            Assert.IsTrue(qry.Wheres[2].ParameterName.Contains(PaymentTransaction.Columns.TransType));
            Assert.AreEqual((int)transType, int.Parse((string)qry.Wheres[2].ParameterValue));
        }
    }
}

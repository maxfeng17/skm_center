﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using NUnit.Framework;

namespace LunchKingSite.Tests.WebLib
{
    [TestFixture]
    public class LocationUtilityFixture
    {
        [Test]
        public void CheckAddressBuildingTest()
        {
            Assert.AreEqual(
                LocationUtility.CheckAddressBuilding("330 桃園市桃園區桃園縣正光路39巷18號"), true);
        }

        [Test]
        public void CheckAddressBuildingTest2()
        {
            string address = "104 台北市中山區龍江路239號（7-11建龍門市）";
            Assert.IsTrue(LocationUtility.CheckAddressBuilding(address));


            TaiwanAddress twAddress = LocationUtility.GetSplitAddressFromAddress(address);
            Assert.AreEqual(twAddress.City, "台北市");
            Assert.AreEqual(twAddress.Town, "中山區");
            Assert.AreEqual(twAddress.Road, "龍江路");
            Assert.AreEqual(twAddress.No, "239");
        }

        [Test]
        public void CheckAddressBuildingTest3()
        {
            string address = "高雄市鼓山區616號";
            Assert.IsFalse(LocationUtility.CheckAddressBuilding(address));
        }
        //
    }
}

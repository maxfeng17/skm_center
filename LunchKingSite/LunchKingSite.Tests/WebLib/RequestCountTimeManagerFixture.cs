﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using LunchKingSite.WebLib.WebModules;
using NUnit.Framework;

namespace LunchKingSite.Tests.WebLib
{
    [TestFixture]
    public class RequestCountTimeManagerFixture
    {
        [Test]
        public void SequenceTest()
        {
            RequestCountTimeManager.Clear();

            Random rand = new Random();

            Stopwatch watch = new Stopwatch();
            watch.Start();

            int testCount = 100000;
            List<RequestCountTime> lastData = null;
            for (int i = 0; i < testCount; i++)
            {
                string pageName = "Page: " + rand.Next(100);
                double elapsedSeconds = rand.NextDouble() * 5;
                RequestCountTimeManager.Append(pageName, elapsedSeconds);
            }
            watch.Stop();

            Console.WriteLine();
            Console.WriteLine("費時: " + watch.Elapsed.TotalSeconds.ToString("0.##") + " 秒");
            Console.WriteLine();
            lastData = RequestCountTimeManager.ToList(100, 0);
            foreach (var item in (lastData ?? new List<RequestCountTime>()))
            {
                Console.WriteLine("Page: " + item.PageName + " = " + item.AvgSeconds.ToString("0.##") + " * " + item.Hits);
            }
            Assert.AreEqual(testCount, lastData.Sum(t => t.Hits));
        }

        [Test]
        public void MultiThreadsTest()
        {
            for (int i = 0; i < 10; i++)
            {
                RequestCountTimeManager.Clear();
                DoMultiThreadsTest(i+1);
            }
        }

        private void DoMultiThreadsTest(int round)
        {
            Console.WriteLine(); Console.WriteLine();
            Console.WriteLine("Round " + round + ":");

            Random rand = new Random();

            Stopwatch watch = new Stopwatch();
            watch.Start();

            int testCount = 100000;
            Task[] tasks = new Task[testCount];
            List<Task> tasks2 = new List<Task>();
            List<RequestCountTime> lastData = null;
            for (int i = 0; i < testCount; i++)
            {
                tasks[i] = Task.Factory.StartNew(delegate
                {

                    string pageName = "Page: " + rand.Next(100);
                    double elapsedSeconds = rand.NextDouble() * 5;
                    RequestCountTimeManager.Append(pageName, elapsedSeconds);
                });

                if (i % 20 == 19)
                {
                    tasks2.Add(Task.Factory.StartNew(delegate
                    {
                        RequestCountTimeManager.ToList(100, 0);
                    }));
                }
            }

            Task.WaitAll(tasks.ToArray());
            Task.WaitAll(tasks2.ToArray());
            watch.Stop();

            Console.WriteLine();
            Console.WriteLine("費時: " + watch.Elapsed.TotalSeconds.ToString("0.##") + " 秒");
            Console.WriteLine();
            lastData = RequestCountTimeManager.ToList(100, 0);
            foreach (var item in (lastData ?? new List<RequestCountTime>()))
            {
                Console.WriteLine("Page: " + item.PageName + " = " + item.AvgSeconds.ToString("0.##") + " * " + item.Hits);
            }
            Assert.AreEqual(testCount, lastData.Sum(t => t.Hits));
        }
    }



}

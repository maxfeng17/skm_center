﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using Moq;

namespace LunchKingSite.Tests
{
    public class MockedProviders : IDisposable
    {
        protected IContainer _container;
        protected ILifetimeScope _backedupLifeScope;

        public MockedProviders()
        {
            var builder = new ContainerBuilder();

            SetupProvider<ILocationProvider>(builder);
            SetupProvider<IMemberProvider>(builder);
            SetupProvider<ISellerProvider>(builder);
            SetupProvider<IOrderProvider>(builder);
            SetupProvider<IItemProvider>(builder);
            SetupProvider<ISysConfProvider>(builder);
            SetupProvider<ICmsProvider>(builder);
            SetupProvider<ISerializer>(builder);
            SetupProvider<IPponProvider>(builder);

            _container = builder.Build();
            _backedupLifeScope = ProviderFactory.Instance().Container;
            ProviderFactory.ReInit(_container.BeginLifetimeScope());
        }

        private void SetupProvider<T>(ContainerBuilder builder) where T : class
        {
            builder.Register(t => new Mock<T>());
        }

        private void ResetProviders()
        {
            ProviderFactory.ReInit(_backedupLifeScope);
        }

        public Mock<T> Get<T>() where T : class
        {
            return _container.Resolve<Mock<T>>();
        }

        #region IDisposable Members

        public void Dispose()
        {
            ResetProviders();
            _container.Dispose();
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Text.RegularExpressions;
using LunchKingSite.Core;
using NUnit.Framework;

namespace LunchKingSite.Tests.Core
{
    public class SmartGetDealTitlePrefixFixture
    {
        [Test]
        public void Test標題中有2個單位數字_有些1個單位數_並存2個單位的回傳0()
        {
            List<string> lines = new List<string>();
            lines.Add("阿雪真甕雞-黃金悶燻冰鎮手撕雞(小)1盒");
            lines.Add("阿雪真甕雞-黃金悶燻冰鎮手撕雞(大)6盒");
            lines.Add("阿雪真甕雞-黃金悶燻冰鎮手撕雞(小)1盒+(大)1盒");

            SmartGetDealTitlePrefix dealPrefix = SmartGetDealTitlePrefix.Parse(lines);
            //Assert.IsFalse(dealPrefix.IsAllSubTitlesHaveUnitNumber);
            Assert.AreEqual("阿雪真甕雞-黃金悶燻冰鎮手撕雞", dealPrefix.Prefix);

            Assert.AreEqual("(小)1盒", dealPrefix.SubItems[0].SubTitle);
            Assert.AreEqual(1, dealPrefix.SubItems[0].UnitNumber);
            Assert.AreEqual("盒", dealPrefix.SubItems[0].UnitTitle);

            Assert.AreEqual("(大)6盒", dealPrefix.SubItems[1].SubTitle);
            Assert.AreEqual(6, dealPrefix.SubItems[1].UnitNumber);
            Assert.AreEqual("盒", dealPrefix.SubItems[1].UnitTitle);

            Assert.AreEqual("(小)1盒+(大)1盒", dealPrefix.SubItems[2].SubTitle);
            Assert.AreEqual(0, dealPrefix.SubItems[2].UnitNumber);
            Assert.AreEqual("盒", dealPrefix.SubItems[2].UnitTitle);
        }

        [Test]
        public void Test標題中有1個單位數字()
        {
            List<string> lines = new List<string>();
            lines.Add("阿雪真甕雞-黃金悶燻冰鎮手撕雞大1盒");
            lines.Add("阿雪真甕雞-黃金悶燻冰鎮手撕雞中6盒");
            lines.Add("阿雪真甕雞-黃金悶燻冰鎮手撕雞小12盒");

            SmartGetDealTitlePrefix dealPrefix = SmartGetDealTitlePrefix.Parse(lines);
            //Assert.IsTrue(dealPrefix.IsAllSubTitlesHaveUnitNumber);
            Assert.AreEqual("阿雪真甕雞-黃金悶燻冰鎮手撕雞", dealPrefix.Prefix);

            Assert.AreEqual("大1盒", dealPrefix.SubItems[0].SubTitle);
            Assert.AreEqual(1, dealPrefix.SubItems[0].UnitNumber);
            Assert.AreEqual("盒", dealPrefix.SubItems[0].UnitTitle);

            Assert.AreEqual("中6盒", dealPrefix.SubItems[1].SubTitle);
            Assert.AreEqual(6, dealPrefix.SubItems[1].UnitNumber);
            Assert.AreEqual("盒", dealPrefix.SubItems[1].UnitTitle);

            Assert.AreEqual("小12盒", dealPrefix.SubItems[2].SubTitle);
            Assert.AreEqual(12, dealPrefix.SubItems[2].UnitNumber);
            Assert.AreEqual("盒", dealPrefix.SubItems[2].UnitTitle);
            
        }

        [Test]
        public void Test標題中沒有單位數字()
        {
            List<string> lines = new List<string>();
            lines.Add("枕套床包組-單人");
            lines.Add("兩用被床包組-單人");
            SmartGetDealTitlePrefix dealPrefix = SmartGetDealTitlePrefix.Parse(lines);
            //Assert.IsFalse(dealPrefix.IsAllSubTitlesHaveUnitNumber);
            Assert.AreEqual(string.Empty, dealPrefix.Prefix);

            Assert.AreEqual(dealPrefix.SubItems[0].SubTitle, "枕套床包組-單人");
            Assert.AreEqual(dealPrefix.SubItems[1].SubTitle, "兩用被床包組-單人");
        }

        [Test]
        public void Test標題中有2個單位數字_取前一個()
        {
            List<string> lines = new List<string>();
            lines.Add("華陀扶元堂-皇室天然洛蔓珍珠粉1盒(30包/盒)");
            lines.Add("華陀扶元堂-皇室天然洛蔓珍珠粉2盒(30包/盒)");
            lines.Add("華陀扶元堂-皇室天然洛蔓珍珠粉6盒(30包/盒)");
            lines.Add("華陀扶元堂-皇室天然洛蔓珍珠粉10盒(30包/盒)");

            SmartGetDealTitlePrefix dealPrefix = SmartGetDealTitlePrefix.Parse(lines);
            //Assert.IsTrue(dealPrefix.IsAllSubTitlesHaveUnitNumber);
            Assert.AreEqual("華陀扶元堂-皇室天然洛蔓珍珠粉", dealPrefix.Prefix);

            Assert.AreEqual("1盒(30包/盒)", dealPrefix.SubItems[0].SubTitle);
            Assert.AreEqual(1, dealPrefix.SubItems[0].UnitNumber);
            Assert.AreEqual("盒", dealPrefix.SubItems[0].UnitTitle);

            Assert.AreEqual("2盒(30包/盒)", dealPrefix.SubItems[1].SubTitle);
            Assert.AreEqual(2, dealPrefix.SubItems[1].UnitNumber);
            Assert.AreEqual("盒", dealPrefix.SubItems[1].UnitTitle);

            Assert.AreEqual("6盒(30包/盒)", dealPrefix.SubItems[2].SubTitle);
            Assert.AreEqual(6, dealPrefix.SubItems[2].UnitNumber);
            Assert.AreEqual("盒", dealPrefix.SubItems[2].UnitTitle);

            Assert.AreEqual("10盒(30包/盒)", dealPrefix.SubItems[3].SubTitle);
            Assert.AreEqual(10, dealPrefix.SubItems[3].UnitNumber);
            Assert.AreEqual("盒", dealPrefix.SubItems[3].UnitTitle);
        }

        [Test]
        public void StrongDashTitleTest()
        {
            Assert.AreEqual("【阿雪真甕雞】黃金悶燻冰鎮手撕雞", SmartGetDealTitlePrefix.StrongDashTitle("阿雪真甕雞-黃金悶燻冰鎮手撕雞"));
            Assert.AreEqual("【阿雪真甕雞】黃金悶燻冰鎮手撕雞-2店", SmartGetDealTitlePrefix.StrongDashTitle("阿雪真甕雞-黃金悶燻冰鎮手撕雞-2店"));
        }

        [Test]
        public void Test6月5號異常()
        {
            List<string> lines = new List<string>();
            lines.Add("日本獅王LION -高密細毛牙刷6入");
            lines.Add("日本獅王LION -高密細毛牙刷12入");

            SmartGetDealTitlePrefix dealPrefix = SmartGetDealTitlePrefix.Parse(lines);
            //Assert.IsTrue(dealPrefix.IsAllSubTitlesHaveUnitNumber);
            Assert.AreEqual("日本獅王LION -高密細毛牙刷", dealPrefix.Prefix);

            Assert.AreEqual("6入", dealPrefix.SubItems[0].SubTitle);
            Assert.AreEqual(6, dealPrefix.SubItems[0].UnitNumber);
            Assert.AreEqual("入", dealPrefix.SubItems[0].UnitTitle);

            Assert.AreEqual("12入", dealPrefix.SubItems[1].SubTitle);
            Assert.AreEqual(12, dealPrefix.SubItems[1].UnitNumber);
            Assert.AreEqual("入", dealPrefix.SubItems[1].UnitTitle);
        }
    }

    public class HelperFxiture
    {
        [Test]
        public void GetIgnoreSqlLikeUnderineSearchTest()
        {
            Assert.AreEqual(Helper.GetIgnoreSqlLikeUnderineMatch("ab"), "ab");
            Assert.AreEqual(Helper.GetIgnoreSqlLikeUnderineMatch("a_b"), "a[_]b");
            Assert.AreEqual(Helper.GetIgnoreSqlLikeUnderineMatch("a[_]b"), "a[_]b");
        }

        [Test]
        public void GetEmailDomainTest()
        {
            Assert.AreEqual(Helper.GetEmailDomain("service@17life.com"), "17life.com");
            Assert.AreEqual(Helper.GetEmailDomain("17life.com"), "");
        }

        [Test]
        public void GetEmailLocalTest()
        {
            Assert.AreEqual(Helper.GetEmailLocal("service@17life.com"), "service");
        }

        [Test]
        public void StringExtension_ReplaceIgnoreCaseTest()
        {
            Assert.AreEqual("ABC123D".ReplaceIgnoreCase("abc", "def"), "def123D");
            Assert.AreEqual("ABC123D".ReplaceIgnoreCase("c12", "*"), "AB*3D");
            Assert.AreEqual("ABC123D".ReplaceIgnoreCase("3d", "**"), "ABC12**");

            Assert.AreEqual("AB/CD/EF".ReplaceIgnoreCase("/", ""), "ABCDEF");
        }

        [Test]
        public void StringExtension_ReplaceIgnoreCaseSpeedTest()
        {
            Stopwatch watch1 = new Stopwatch();
            watch1.Start();
            for (int i = 0; i < 100000; i++)
            {
                string test = "AB/CD/EF".ReplaceIgnoreCase("/", "");
            }
            watch1.Stop();
            double myTime = watch1.Elapsed.TotalMilliseconds;

            Stopwatch watch2 = new Stopwatch();
            watch2.Start();

            var regex = new Regex("/", RegexOptions.IgnoreCase);
            for (int i = 0; i < 100000; i++)
            {
                var test = regex.Replace("AB/CD/EF", "");
            }
            watch2.Stop();
            double regTime = watch2.Elapsed.TotalMilliseconds;
            Console.WriteLine("my: " + Convert.ToInt32(myTime));
            Console.WriteLine("reg: " + Convert.ToInt32(regTime));
            Assert.IsTrue(myTime < regTime);
        }

        [Test]
        public void StringExtension_TrimStartTest()
        {
            Assert.AreEqual("ABC123D".TrimStart("abc", StringComparison.OrdinalIgnoreCase), "123D");
            Assert.AreEqual("ABC123D".TrimStart("abc", StringComparison.Ordinal), "ABC123D");
            Assert.AreEqual("ABC123D".TrimStart("def", StringComparison.OrdinalIgnoreCase), "ABC123D");
            Assert.AreEqual("ABC123D".TrimStart("ABC", StringComparison.Ordinal), "123D");
        }

        [Test]
        public void CombineUrlTest()
        {
            Assert.AreEqual("http://localhost/index.html", Helper.CombineUrl("http://localhost", "index.html"));
            Assert.AreEqual("http://localhost/index.html", Helper.CombineUrl("http://localhost", "/index.html"));
            Assert.AreEqual("http://localhost/index.html", Helper.CombineUrl("http://localhost/", "index.html"));
            Assert.AreEqual("http://localhost/index.html", Helper.CombineUrl("http://localhost/", "/index.html"));
        }

        [Test]
        public void CombineUrlTests()
        {
            Assert.AreEqual("http://localhost/onsale/20180623", 
                Helper.CombineUrl("http://localhost", "onsale", "20180623"));
        }

        [Test]
        public void CheckSqlDateTime()
        {
            Assert.IsFalse(Helper.CheckSqlDateTime(null));
            Assert.IsFalse(Helper.CheckSqlDateTime(DateTime.MinValue));
            //DateTime.MaxValue 比 SqlDateTime.MaxValue.Value 稍大一點
            Assert.IsFalse(Helper.CheckSqlDateTime(DateTime.MaxValue));
            Assert.IsTrue(Helper.CheckSqlDateTime(SqlDateTime.MinValue.Value));
            Assert.IsTrue(Helper.CheckSqlDateTime(SqlDateTime.MaxValue.Value));
        }

        [Test]
        public void IsChineseTest()
        {
            Assert.IsTrue(Helper.IsChinese('是'));
            Assert.IsFalse(Helper.IsChinese(' '));
            Assert.IsFalse(Helper.IsChinese('a'));
            Assert.IsFalse(Helper.IsChinese(','));
        }

        [Test]
        public void GetFamilyNameTest()
        {
            string givenName;
            Assert.AreEqual("劉", Helper.GetFamilyName("劉阿雷", out givenName));
            Assert.AreEqual("阿雷", givenName);

            Assert.AreEqual("東方", Helper.GetFamilyName("東方不敗", out givenName));
            Assert.AreEqual("不敗", givenName);

            Assert.AreEqual("劉", Helper.GetFamilyName("劉", out givenName));
            Assert.AreEqual(string.Empty, givenName);

            Assert.AreEqual("東方", Helper.GetFamilyName("東方", out givenName));
            Assert.AreEqual(string.Empty, givenName);

            Assert.AreEqual("Simone", Helper.GetFamilyName("Simone", out givenName));
            Assert.AreEqual(string.Empty, givenName);

            Assert.AreEqual("Simone", Helper.GetFamilyName("  Simone  ", out givenName));
            Assert.AreEqual(string.Empty, givenName);

            Assert.AreEqual("Simone", Helper.GetFamilyName("Simone, Nina", out givenName));
            Assert.AreEqual("Nina", givenName);

            Assert.AreEqual("Simone", Helper.GetFamilyName("Simone ,Nina", out givenName));
            Assert.AreEqual("Nina", givenName);

            Assert.AreEqual("Simone", Helper.GetFamilyName(" Simone ,Nina ", out givenName));
            Assert.AreEqual("Nina", givenName);

            Assert.AreEqual("Simone", Helper.GetFamilyName("Nina Simone", out givenName));
            Assert.AreEqual("Nina", givenName);

            Assert.AreEqual("Simone", Helper.GetFamilyName("Nina    Simone", out givenName));
            Assert.AreEqual("Nina", givenName);

            Assert.AreEqual("Simone", Helper.GetFamilyName("  Nina    Simone  ", out givenName));
            Assert.AreEqual("Nina", givenName);

            Assert.AreEqual("Liu", Helper.GetFamilyName("Shuin-lei Liu", out givenName));
            Assert.AreEqual("Shuin-lei", givenName);

            Assert.AreEqual("King", Helper.GetFamilyName("B.B. King", out givenName));
            Assert.AreEqual("B.B.", givenName);

            Assert.AreEqual("劉", Helper.GetFamilyName("Unicorn 劉", out givenName));
            Assert.AreEqual("Unicorn", givenName);

        }

        [TestFixture]
        public class CopyObjectPropertiesTests
        {
            [Test]
            public void StringArrayChangeTest()
            {
                var src = new WebConfSysConfProvider()
                {
                    SyncWebServerUrl = new string[] { "192.168.1.7", "192.168.1.8" }
                };
                var dest = new WebConfSysConfProvider()
                {
                    SyncWebServerUrl = new string[] { "192.168.1.7", "192.168.1.8", "192.168.1.9" }
                };

                List<ConfigChangedInfo> changes = Helper.CopyObjectProperties<ISysConfProvider>(
                    src,
                    dest);
                Assert.AreEqual(changes.Count, 1);
                Assert.AreEqual(changes[0].Key, "SyncWebServerUrl");
                Assert.AreEqual(changes[0].FromValue, "192.168.1.7\r\n192.168.1.8\r\n192.168.1.9");
                Assert.AreEqual(changes[0].ToValue, "192.168.1.7\r\n192.168.1.8");
            }

            [Test]
            public void StringArrayChange2Test()
            {
                var src = new WebConfSysConfProvider()
                {
                    SyncWebServerUrl = new string[] { }
                };
                var dest = new WebConfSysConfProvider()
                {
                    SyncWebServerUrl = new string[] { "192.168.1.7", "192.168.1.8", "192.168.1.9" }
                };

                List<ConfigChangedInfo> changes = Helper.CopyObjectProperties<ISysConfProvider>(
                    src,
                    dest);
                Assert.AreEqual(changes.Count, 1);
                Assert.AreEqual(changes[0].Key, "SyncWebServerUrl");
                Assert.AreEqual(changes[0].FromValue, "192.168.1.7\r\n192.168.1.8\r\n192.168.1.9");
                Assert.AreEqual(changes[0].ToValue, "");
            }

            [Test]
            public void StringArrayChange3Test()
            {
                var src = new WebConfSysConfProvider()
                {
                    SyncWebServerUrl = new string[] { "192.168.1.7", "192.168.1.8", "192.168.1.9" }
                };
                var dest = new WebConfSysConfProvider()
                {
                    SyncWebServerUrl = new string[] { }
                };

                List<ConfigChangedInfo> changes = Helper.CopyObjectProperties<ISysConfProvider>(
                    src,
                    dest);
                Assert.AreEqual(changes.Count, 1);
                Assert.AreEqual(changes[0].Key, "SyncWebServerUrl");
                Assert.AreEqual(changes[0].FromValue, "");
                Assert.AreEqual(changes[0].ToValue, "192.168.1.7\r\n192.168.1.8\r\n192.168.1.9");
            }

            [Test]
            public void IntArrayChangeTest()
            {
                var src = new WebConfSysConfProvider()
                {
                    FamiteaDealId = new int[] { 1, 2, 3 }
                };
                var dest = new WebConfSysConfProvider()
                {
                    FamiteaDealId = new int[] { 2 }
                };

                List<ConfigChangedInfo> changes = Helper.CopyObjectProperties<ISysConfProvider>(
                    src,
                    dest);
                Assert.AreEqual(changes.Count, 1);
                Assert.AreEqual(changes[0].Key, "FamiteaDealId");
                Assert.AreEqual(changes[0].FromValue, "2");
                Assert.AreEqual(changes[0].ToValue, "1\r\n2\r\n3");
            }

            [Test]
            public void IntArrayChange2Test()
            {
                var src = new WebConfSysConfProvider()
                {
                    FamiteaDealId = new int[] { }
                };
                var dest = new WebConfSysConfProvider()
                {
                    FamiteaDealId = new int[] { 2 }
                };

                List<ConfigChangedInfo> changes = Helper.CopyObjectProperties<ISysConfProvider>(
                    src,
                    dest);
                Assert.AreEqual(changes.Count, 1);
                Assert.AreEqual(changes[0].Key, "FamiteaDealId");
                Assert.AreEqual(changes[0].FromValue, "2");
                Assert.AreEqual(changes[0].ToValue, "");
            }

            [Test]
            public void IntArrayChange3Test()
            {
                var src = new WebConfSysConfProvider()
                {
                    FamiteaDealId = new int[] { 2 }
                };
                var dest = new WebConfSysConfProvider()
                {
                    FamiteaDealId = new int[] { }
                };

                List<ConfigChangedInfo> changes = Helper.CopyObjectProperties<ISysConfProvider>(
                    src,
                    dest);
                Assert.AreEqual(changes.Count, 1);
                Assert.AreEqual(changes[0].Key, "FamiteaDealId");
                Assert.AreEqual(changes[0].FromValue, "");
                Assert.AreEqual(changes[0].ToValue, "2");
            }

            [Test]
            public void StringChangeTest()
            {
                var src = new WebConfSysConfProvider()
                {
                    AdminEmail = "admin@17life.com.tw"
                };
                var dest = new WebConfSysConfProvider()
                {
                    AdminEmail = "admin@17life.com"
                };

                List<ConfigChangedInfo> changes = Helper.CopyObjectProperties<ISysConfProvider>(
                    src,
                    dest);
                Assert.AreEqual(changes.Count, 1);
                Assert.AreEqual(changes[0].Key, "AdminEmail");
                Assert.AreEqual(changes[0].FromValue, "admin@17life.com");
                Assert.AreEqual(changes[0].ToValue, "admin@17life.com.tw");
            }

            [Test]
            public void IntChangeTest()
            {
                var src = new WebConfSysConfProvider()
                {
                    InvoiceDelayDays = 5
                };
                var dest = new WebConfSysConfProvider()
                {
                    InvoiceDelayDays = 10
                };

                List<ConfigChangedInfo> changes = Helper.CopyObjectProperties<ISysConfProvider>(
                    src,
                    dest);
                Assert.AreEqual(changes.Count, 1);
                Assert.AreEqual(changes[0].Key, "InvoiceDelayDays");
                Assert.AreEqual(changes[0].FromValue, "10");
                Assert.AreEqual(changes[0].ToValue, "5");
            }

            [Test]
            public void BoolChangeTest()
            {
                var src = new WebConfSysConfProvider()
                {
                    InstallmentPayEnabled = false
                };
                var dest = new WebConfSysConfProvider()
                {
                    InstallmentPayEnabled = true
                };

                List<ConfigChangedInfo> changes = Helper.CopyObjectProperties<ISysConfProvider>(
                    src,
                    dest);
                Assert.AreEqual(changes.Count, 1);
                Assert.AreEqual(changes[0].Key, "InstallmentPayEnabled");
                Assert.AreEqual(changes[0].FromValue, "True");
                Assert.AreEqual(changes[0].ToValue, "False");

                Assert.AreEqual(src.InstallmentPayEnabled, false);
                Assert.AreEqual(dest.InstallmentPayEnabled, false);
            }

            [Test]
            public void EnumChangeTest()
            {
                var src = new WebConfSysConfProvider()
                {
                    MailServiceType = MailServerSendType.PostMan
                };
                var dest = new WebConfSysConfProvider()
                {
                    MailServiceType = MailServerSendType.MailServer
                };

                List<ConfigChangedInfo> changes = Helper.CopyObjectProperties<ISysConfProvider>(
                    src,
                    dest);
                Assert.AreEqual(changes.Count, 1);
                Assert.AreEqual(changes[0].Key, "MailServiceType");
                Assert.AreEqual(changes[0].FromValue, "MailServer");
                Assert.AreEqual(changes[0].ToValue, "PostMan");
            }

            [Test]
            public void GuidChangeTest()
            {
                Guid newGuid = Guid.NewGuid();
                Guid oldGuid = Guid.NewGuid();

                var src = new WebConfSysConfProvider()
                {
                    SkmRootSellerGuid = newGuid
                };
                var desc = new WebConfSysConfProvider()
                {
                    SkmRootSellerGuid = oldGuid
                };

                List<ConfigChangedInfo> changes = Helper.CopyObjectProperties<ISysConfProvider>(
                    src,
                    desc);
                Assert.AreEqual(changes.Count, 1);
                Assert.AreEqual(changes[0].Key, "SkmRootSellerGuid");
                Assert.AreEqual(changes[0].FromValue, oldGuid.ToString());
                Assert.AreEqual(changes[0].ToValue, newGuid.ToString());
            }

            [Test]
            public void DecimalChangeTest()
            {
                var src = new WebConfSysConfProvider()
                {
                    GrossMargin = 11
                };
                var desc = new WebConfSysConfProvider()
                {
                    GrossMargin = 13
                };

                List<ConfigChangedInfo> changes = Helper.CopyObjectProperties<ISysConfProvider>(
                    src,
                    desc);
                Assert.AreEqual(changes.Count, 1);
                Assert.AreEqual(changes[0].Key, "GrossMargin");
                Assert.AreEqual(changes[0].FromValue, "13");
                Assert.AreEqual(changes[0].ToValue, "11");
            }

            [Test]
            public void DateTimeChangeTest()
            {
                var src = new WebConfSysConfProvider()
                {
                    NewInvoiceDate = new DateTime(2012, 7, 1)
                };
                var desc = new WebConfSysConfProvider()
                {
                    NewInvoiceDate = new DateTime(2013, 8, 2)
                };

                List<ConfigChangedInfo> changes = Helper.CopyObjectProperties<ISysConfProvider>(
                    src,
                    desc);
                Assert.AreEqual(changes.Count, 1);
                Assert.AreEqual(changes[0].Key, "NewInvoiceDate");
                Assert.AreEqual(changes[0].FromValue, new DateTime(2013, 8, 2).ToString());
                Assert.AreEqual(changes[0].ToValue, new DateTime(2012, 7, 1).ToString());
            }

            [Test]
            public void MultiPropsChangeTest()
            {
                var src = new WebConfSysConfProvider()
                {
                    EnableCpa = true,
                    AdminEmail = "admin@17life.com.tw",
                    InstallmentPayEnabled = false
                };
                var dest = new WebConfSysConfProvider()
                {
                    EnableCpa = true,
                    AdminEmail = "admin@17life.com",
                    InstallmentPayEnabled = true
                };

                List<ConfigChangedInfo> changes = Helper.CopyObjectProperties<ISysConfProvider>(
                    src,
                    dest);
                Assert.AreEqual(changes.Count, 2);
                Assert.AreEqual(changes[0].Key, "AdminEmail");
                Assert.AreEqual(changes[0].FromValue, "admin@17life.com");
                Assert.AreEqual(changes[0].ToValue, "admin@17life.com.tw");
                Assert.AreEqual(changes[1].Key, "InstallmentPayEnabled");
                Assert.AreEqual(changes[1].FromValue, "True");
                Assert.AreEqual(changes[1].ToValue, "False");
            }

            [Test]
            public void MaskCreditCardTest()
            {
                Assert.AreEqual("888888******0001", Helper.MaskCreditCard("8888880000000001"));
            }
            [Test]
            public void MaskCreditCardSecurityCodeTest()
            {
                Assert.AreEqual("***", Helper.MaskCreditCardSecurityCode("123"));
                Assert.AreEqual("", Helper.MaskCreditCardSecurityCode(""));
            }

            [Test]
            public void EncryptAndDecryptEmail()
            {
                string s = "aaa@bbb.ccc";
                string s2 = Helper.EncryptEmail(s);
                Assert.AreNotEqual(s, s2);
                string s3 = Helper.DecryptEmail(s2);
                Assert.AreEqual(s, s3);
            }

            //[Test]
            //public void SmartGetPrefixTest()
            //{
            //    List<string> lines = new List<string>();
            //    lines.Add("阿雪真甕雞-黃金悶燻冰鎮手撕雞(小)1盒");
            //    lines.Add("阿雪真甕雞-黃金悶燻冰鎮手撕雞(大)6盒");
            //    lines.Add("阿雪真甕雞-黃金悶燻冰鎮手撕雞(小)1盒+(大)1盒");
            //    string[] subTitles;
            //    string prefix = Helper.SmartGetPrefix(lines.ToArray(), out subTitles);
            //    Assert.AreEqual(prefix, "阿雪真甕雞-黃金悶燻冰鎮手撕雞");
            //    Assert.AreEqual(3, subTitles.Length);

            //    Assert.AreEqual(subTitles[0], "(小)1盒");
            //    Assert.AreEqual(subTitles[1], "(大)6盒");
            //    Assert.AreEqual(subTitles[2], "(小)1盒+(大)1盒");
            //}

            //[Test]
            //public void SmartGetPrefixTest2()
            //{
            //    List<string> lines = new List<string>();
            //    lines.Add("阿雪真甕雞-黃金悶燻冰鎮手撕雞大1盒");
            //    lines.Add("阿雪真甕雞-黃金悶燻冰鎮手撕雞中6盒");
            //    lines.Add("阿雪真甕雞-黃金悶燻冰鎮手撕雞小12盒");
            //    string[] subTitles;
            //    string prefix = Helper.SmartGetPrefix(lines.ToArray(), out subTitles);
            //    Assert.AreEqual(prefix, "阿雪真甕雞-黃金悶燻冰鎮手撕雞");
            //    Assert.AreEqual(3, subTitles.Length);

            //    Assert.AreEqual(subTitles[0], "大1盒");
            //    Assert.AreEqual(subTitles[1], "中6盒");
            //    Assert.AreEqual(subTitles[2], "小12盒");
            //}

            //[Test]
            //public void SmartGetPrefixTest3()
            //{
            //    List<string> lines = new List<string>();
            //    lines.Add("枕套床包組-單人");
            //    lines.Add("兩用被床包組-單人");
            //    string[] subTitles;
            //    string prefix = Helper.SmartGetPrefix(lines.ToArray(), out subTitles);
            //    Assert.AreEqual(prefix, "");

            //    Assert.AreEqual(subTitles[0], "枕套床包組-單人");
            //    Assert.AreEqual(subTitles[1], "兩用被床包組-單人");
            //}


            //[Test]
            //public void GetUnitNumberTest()
            //{
            //    int number;
            //    bool result;
            //    result = Helper.GetUnitNumber("阿雪真甕雞-黃金悶燻冰鎮手撕雞(小)6盒", out number);
            //    Assert.AreEqual(6, number);
            //    Assert.IsTrue(result);
            //    result = Helper.GetUnitNumber("阿雪真甕雞-黃金悶燻冰鎮手撕雞(小)12盒", out number);
            //    Assert.AreEqual(12, number);
            //    Assert.IsTrue(result);
            //    result = Helper.GetUnitNumber("阿雪真甕雞-黃金悶燻冰鎮手撕雞(小)4盒+(大)4盒", out number);
            //    Assert.IsFalse(result);
            //}
            //[Test]
            //public void StrongDashTitleTest()
            //{
            //    Assert.AreEqual("【阿雪真甕雞】黃金悶燻冰鎮手撕雞", Helper.StrongDashTitle("阿雪真甕雞-黃金悶燻冰鎮手撕雞"));
            //    Assert.AreEqual("【阿雪真甕雞】黃金悶燻冰鎮手撕雞-2店", Helper.StrongDashTitle("阿雪真甕雞-黃金悶燻冰鎮手撕雞-2店"));
            //}
        }

    }
}


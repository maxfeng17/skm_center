﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using NUnit.Framework;

namespace LunchKingSite.Tests.Core
{
    [TestFixture]
    public class PponDealFixture
    {
        [Test]
        public void DealStageTest()
        {
            ViewPponDeal deal = new ViewPponDeal();            
            Assert.AreEqual(PponDealStage.Created, deal.GetDealStage());
            deal.GroupOrderGuid = new Guid();

            deal.BusinessHourOrderTimeS = DateTime.Now.AddDays(1);
            deal.BusinessHourOrderTimeE = DateTime.Now.AddDays(2);
            Assert.AreEqual(PponDealStage.Ready, deal.GetDealStage());

            deal.BusinessHourOrderTimeS = DateTime.Now.AddMinutes(-1);
            deal.BusinessHourOrderTimeE = DateTime.Now.AddDays(1);
            Assert.AreEqual(PponDealStage.RunningAndOn, deal.GetDealStage());
            deal.OrderedQuantity = 1;
            Assert.AreEqual(PponDealStage.RunningAndOn, deal.GetDealStage());
            deal.BusinessHourOrderMinimum = 1;
            Assert.AreEqual(PponDealStage.RunningAndOn, deal.GetDealStage());
            deal.BusinessHourOrderMinimum = 2;
            Assert.AreEqual(PponDealStage.Running, deal.GetDealStage());
            deal.OrderTotalLimit = 1;
            Assert.AreEqual(PponDealStage.Running, deal.GetDealStage());
            deal.OrderedQuantity = 2;
            Assert.AreEqual(PponDealStage.RunningAndFull, deal.GetDealStage());
            deal.OrderTotalLimit = 2;
            Assert.AreEqual(PponDealStage.RunningAndFull, deal.GetDealStage());

            deal.GroupOrderStatus = (int)Helper.SetFlag(true, 0, GroupOrderStatus.Completed);
            Assert.AreEqual(PponDealStage.ClosedAndOn, deal.GetDealStage());
            deal.GroupOrderStatus = (int) Helper.SetFlag(true, deal.GroupOrderStatus.Value, GroupOrderStatus.PponCouponGenerated);
            Assert.AreEqual(PponDealStage.CouponGenerated, deal.GetDealStage());
            deal.OrderedQuantity = 0;
            Assert.AreEqual(PponDealStage.ClosedAndFail, deal.GetDealStage());

            deal.GroupOrderStatus = (int)Helper.SetFlag(false, 0, GroupOrderStatus.Completed);
            deal.BusinessHourOrderTimeE = DateTime.Now.AddDays(-1);
            Assert.AreEqual(PponDealStage.ClosedAndFail, deal.GetDealStage());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core.Component;
using NUnit.Framework;

namespace LunchKingSite.Tests.Core
{
    [TestFixture]
    public class SecurityFixture
    {
        [Test]
        public void MD5HashTest()
        {
            Assert.IsNotNull(Security.MD5Hash("17Life"));
            Assert.IsNotEmpty(Security.MD5Hash("17Life"));
            Assert.AreEqual(Security.MD5Hash("17Life").Length, 32);
            Assert.AreNotEqual(Security.MD5Hash("17Life"), Security.MD5Hash("lunchking"));            
        }
    }
}

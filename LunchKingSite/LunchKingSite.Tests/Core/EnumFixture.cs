﻿using System;
using NUnit.Framework;

namespace LunchKingSite.Tests.Core
{
    public enum MyEnum
    {
        XXX = 0 ,
        YYY = 1,
        ZZZ = 2
    }

    [TestFixture]
    public class EnumFixture
    {
        [Test]
        public void IsDefinedTest()
        {
            Assert.IsTrue(Enum.IsDefined(typeof(MyEnum), "XXX"));
            Assert.IsTrue(Enum.IsDefined(typeof(MyEnum), 0));
            Assert.IsFalse(Enum.IsDefined(typeof(MyEnum), "0"));
            Assert.IsFalse(Enum.IsDefined(typeof(MyEnum), 9));
        }

        [Test]
        public void TryParseTest()
        {
            MyEnum tmp;
            Assert.IsTrue(Enum.TryParse("XXX", out tmp));
            Assert.IsTrue(Enum.TryParse("0", out tmp));
            Assert.IsTrue(Enum.TryParse("9", out tmp));
            Assert.IsFalse(Enum.TryParse("CRASH", out tmp));
        }
    }
}

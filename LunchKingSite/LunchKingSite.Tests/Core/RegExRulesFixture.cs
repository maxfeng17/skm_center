﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using NUnit.Framework;

namespace LunchKingSite.Tests.Core
{
    [TestFixture]
    public class RegExRulesFixture
    {
        [Test]
        public void CheckUpperCaseEmailTest()
        {
            Assert.IsTrue(RegExRules.CheckEmail("SERVICE@17LIFE.COM"));
        }

        [Test]
        public void CheckEmailTest()
        {
            Assert.IsTrue(RegExRules.CheckEmail("service_@so-net.net.tw"));
            Assert.IsTrue(RegExRules.CheckEmail("service-black-@yahoo.com.tw"));
            Assert.IsTrue(RegExRules.CheckEmail("h@yahoo.com.tw"));
            Assert.IsTrue(RegExRules.CheckEmail("service17@yahoo.com.twitter"));
            Assert.IsTrue(RegExRules.CheckEmail("service@yahoo.com.t.w"));

            Assert.IsFalse(RegExRules.CheckEmail("service17.@gmail.com"));
            Assert.IsFalse(RegExRules.CheckEmail("service17@.yahoo.com.tw"));
            Assert.IsFalse(RegExRules.CheckEmail("service17@-yahoo.com.tw"));
            Assert.IsFalse(RegExRules.CheckEmail("service..17@gmail.com"));
            Assert.IsFalse(RegExRules.CheckEmail("service17.@gmail..com"));
        }

        [Test]
        public void CheckEmailEstherVersion()
        {
            //不允許
            Assert.IsFalse(RegExRules.CheckEmail(""), "檢核不可為空白");
            Assert.IsFalse(RegExRules.CheckEmail(null), "檢核不可為空白");

            Assert.IsFalse(RegExRules.CheckEmail("i am@17life.com"), "檢核不可包含空白字元");

            Assert.IsFalse(RegExRules.CheckEmail("iam@17%life.com"), "檢核不可包含特殊字元");
            Assert.IsFalse(RegExRules.CheckEmail("iam@17^life.com"), "檢核不可包含特殊字元");
            Assert.IsFalse(RegExRules.CheckEmail("iam@17$life.com"), "檢核不可包含特殊字元");
            Assert.IsFalse(RegExRules.CheckEmail("iam@17#life.com"), "檢核不可包含特殊字元");
            Assert.IsFalse(RegExRules.CheckEmail("iam17life.com"), "檢核字元@不足一次");
            Assert.IsFalse(RegExRules.CheckEmail("iam@17@life.com"), "檢核字元@超過一次");

            Assert.IsFalse(RegExRules.CheckEmail("iam機器人@17life.com"), "Email格式不允許中文");
            Assert.IsFalse(RegExRules.CheckEmail("iam@台灣.com"), "Email格式不允許中文");

            Assert.IsFalse(RegExRules.CheckEmail("@17life.com"), "檢核@前方需有內容");

            Assert.IsFalse(RegExRules.CheckEmail("iam@"), "@右邊需要有.");

            Assert.IsFalse(RegExRules.CheckEmail(new string('x', 256) + "@17lifecom"), "檢核字元長度(256)");
            /*
　 2-1 檢核不可為空白
    2-2 檢核不可包含空白字元
    2-3 檢核不可包含特殊字元
    2-4 檢核字元@不足一次
    2-5 檢核字元@超過一次
    2-6 檢核輸入中文
    2-7 檢核@前方需有內容
    2-8 檢核@後方需有內容
    2-9 @右邊需要有.
    2-10 檢核字元長度
    2-11 預設email格式檢核
             */

        }


        //參考 http://en.wikipedia.org/wiki/Email_address
        [Test]        
        public void CheckEmailFromWikiExampleTest()
        {
            Assert.IsTrue(RegExRules.CheckEmail("niceandsimple@example.com"));
            Assert.IsTrue(RegExRules.CheckEmail("very.common@example.com"));
            Assert.IsTrue(RegExRules.CheckEmail("a.little.lengthy.but.fine@dept.example.com"));            
            Assert.IsTrue(RegExRules.CheckEmail("disposable.style.email.with+symbol@example.com"));
            Assert.IsTrue(RegExRules.CheckEmail("\"much.more unusual\"@example.com"));
            Assert.IsTrue(RegExRules.CheckEmail("\"very.unusual.@.unusual.com\"@example.com"));
                       
            Assert.IsFalse(RegExRules.CheckEmail("Abc.example.com"));
            Assert.IsFalse(RegExRules.CheckEmail("A@b@c@example.com"));
            Assert.IsFalse(RegExRules.CheckEmail("a\"b(c)d,e:f;g<h>i[j\\k]l@example.com@dept.example.com"));
            Assert.IsFalse(RegExRules.CheckEmail("just\"not\"right@example.com"));
            Assert.IsFalse(RegExRules.CheckEmail("this is\"not\\allowed@example.com"));
            Assert.IsFalse(RegExRules.CheckEmail("this\\ still\\\"not\\\\allowed@example.com"));
        }

        [Test]
        public void CheckEmailTwoDots()
        {
            Assert.IsTrue(RegExRules.CheckEmail("two.do.ts@dept.example.com"));
            Assert.IsFalse(RegExRules.CheckEmail("two..dots@dept.example.com"));            
            Assert.IsFalse(RegExRules.CheckEmail("twodots@dept.example..com"));
        }


        [Test]
        public void CheckLuhnForCreditCardTest()
        {
            //Assert.IsTrue(RegExRules.CheckLuhnForCreditCard("0239997326"));
            //4057912300046711
            Assert.IsTrue(RegExRules.CheckLuhnForCreditCard("8888880000000005"));
            Assert.IsTrue(RegExRules.CheckLuhnForCreditCard("4057912300046711"));
            Assert.IsTrue(RegExRules.CheckLuhnForCreditCard("4984213124521452"));
            Assert.IsFalse(RegExRules.CheckLuhnForCreditCard("5181271099000012"));
        }
    }
}

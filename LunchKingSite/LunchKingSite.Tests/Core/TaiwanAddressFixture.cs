﻿using LunchKingSite.BizLogic.Model;
using NUnit.Framework;

namespace LunchKingSite.Tests.Core
{
    [TestFixture]
    public class TaiwanAddressFixture
    {
        [Test]
        public void TownTest()
        {
            Assert.AreEqual(new TaiwanAddress("104 台北市中山區中山北路一段11號13樓").Town, "中山區");
            Assert.AreEqual(new TaiwanAddress("330 桃園市桃園區縣府路88號4樓").Town, "桃園區");
            Assert.AreEqual(new TaiwanAddress("330 桃園市桃園區桃園縣正光路39巷18號").Town, "桃園區");
        }

        [Test]
        public void CityTest()
        {
            Assert.AreEqual(new TaiwanAddress("104 台北市中山區中山北路一段11號13樓").City, "台北市");
        }
    }
}

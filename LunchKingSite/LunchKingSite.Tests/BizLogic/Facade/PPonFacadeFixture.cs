﻿using System;
using System.Collections.Generic;
using Autofac;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Moq;
using NUnit.Framework;

namespace LunchKingSite.Tests.BizLogic.Facade
{
    [TestFixture]
    public class PponFacadeFixture
    {
        private Guid dealABid = Guid.NewGuid();
        private Guid dealBBid = Guid.NewGuid();
        private Guid dealCBid = Guid.NewGuid();
        private Dictionary<Guid, ViewPponDeal> vpdData;
        private Dictionary<Guid, DealProperty> dpData;

        public void InitData()
        {
            vpdData = new Dictionary<Guid, ViewPponDeal>();
            vpdData.Add(dealABid, new ViewPponDeal
            {
                BusinessHourGuid = dealABid,
                Slug = "5",
                IsContinuedQuantity = null,
                IsLoaded = true
            });
            vpdData.Add(dealBBid, new ViewPponDeal
            {
                BusinessHourGuid = dealBBid,
                Slug = "3",
                IsContinuedQuantity = true,
                IsLoaded = true
            });
            vpdData.Add(dealCBid, new ViewPponDeal
            {
                BusinessHourGuid = dealCBid,
                IsLoaded = true
            });

            dpData = new Dictionary<Guid, DealProperty>();
            dpData.Add(dealABid, new DealProperty
            {
                AncestorBusinessHourGuid = null,
                IsContinuedQuantity = false,
                IsLoaded = true
            });
            dpData.Add(dealBBid, new DealProperty
            {
                AncestorBusinessHourGuid = dealABid,
                IsContinuedQuantity = true,
                IsLoaded = true
            });

        }


        /// <summary>
        /// C=>B=>A=>C
        /// </summary>
        public void InitRecursiveData()
        {
            vpdData = new Dictionary<Guid, ViewPponDeal>();
            vpdData.Add(dealABid, new ViewPponDeal
            {
                BusinessHourGuid = dealABid,
                Slug = "5",
                IsContinuedQuantity = true,
                IsLoaded = true
            });
            vpdData.Add(dealBBid, new ViewPponDeal
            {
                BusinessHourGuid = dealBBid,
                Slug = "3",
                IsContinuedQuantity = true,
                IsLoaded = true
            });
            vpdData.Add(dealCBid, new ViewPponDeal
            {
                BusinessHourGuid = dealCBid,
                IsLoaded = true
            });

            dpData = new Dictionary<Guid, DealProperty>();
            dpData.Add(dealABid, new DealProperty
            {
                AncestorBusinessHourGuid = dealCBid,
                IsLoaded = true
            });
            dpData.Add(dealBBid, new DealProperty
            {
                AncestorBusinessHourGuid = dealABid,
                IsLoaded = true
            });

        }

        [SetUp]
        public void Setup()
        {
            var opMock = new Mock<IOrderProvider>();
            var mpMock = new Mock<IMemberProvider>();
            var cpMock = new Mock<ISysConfProvider>();
            var vpMock = new Mock<IVerificationProvider>();
            var slpMock = new Mock<ISellerProvider>();
            var spMock = new Mock<ISystemProvider>();
            var lpMock = new Mock<ILocationProvider>();
            var ppMock = new Mock<IPponProvider>();
            var hdpMock = new Mock<IHiDealProvider>();

            ppMock.Setup(t => t.ViewPponDealGet(It.IsAny<string[]>())).Returns(delegate(Guid bid)
            {
                ViewPponDeal result;
                if (vpdData.TryGetValue(bid, out result))
                {
                    return result;
                }
                return new ViewPponDeal();
            });

            ppMock.Setup(t => t.DealPropertyGet(It.IsAny<Guid>())).Returns(delegate(Guid bid) {
                DealProperty result;
                if (dpData.TryGetValue(bid, out result))
                {
                    return result;
                }
                return new DealProperty();                
            });

            var builder = new ContainerBuilder();
            builder.RegisterInstance(ppMock.Object).As<IPponProvider>();

            builder.RegisterInstance(opMock.Object).As<IOrderProvider>();
            builder.RegisterInstance(mpMock.Object).As<IMemberProvider>();
            builder.RegisterInstance(cpMock.Object).As<ISysConfProvider>();
            builder.RegisterInstance(vpMock.Object).As<IVerificationProvider>();
            builder.RegisterInstance(slpMock.Object).As<ISellerProvider>();
            builder.RegisterInstance(spMock.Object).As<ISystemProvider>();
            builder.RegisterInstance(lpMock.Object).As<ILocationProvider>();
            builder.RegisterInstance(hdpMock.Object).As<IHiDealProvider>();

            var build = builder.Build();
            ProviderFactory.ReInit(build.BeginLifetimeScope());

        }

        [Test]
        public void GetPponDealContinuedQuantityTest()
        {
            InitData();

            Assert.AreEqual(0, PponFacade.GetPponDealContinuedQuantity(Guid.Empty, false));

            Assert.AreEqual(5, PponFacade.GetPponDealContinuedQuantity(dealABid, true));

            Assert.AreEqual(8, PponFacade.GetPponDealContinuedQuantity(dealBBid, true));
        }

        [Test]
        public void GetPponDealContinuedQuantity_RecursiveSelfTest()
        {
            InitRecursiveData();

            Assert.AreEqual(-1, PponFacade.GetPponDealContinuedQuantity(dealBBid, true));
        }
    }
}

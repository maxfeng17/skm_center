﻿using System;
using Autofac;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Moq;
using NUnit.Framework;

namespace LunchKingSite.Tests.BizLogic.Facade
{
    [TestFixture]
    public class PaymentFacadeFixture
    {
        private Guid selectedStoreGuid = Guid.NewGuid();
        private Guid bid = Guid.NewGuid();
        [SetUp]
        public void Setup()
        {
            var ppMock = new Mock<IPponProvider>();
            ppMock.Setup(t => t.TempSessionSet(It.IsAny<TempSession>())).Callback(delegate(TempSession ts)
            {
                //Console.WriteLine("假寫入:" + ts.Name);
            });

            ppMock.Setup(t => t.ViewPponStoreGetListByDayWithNoLock(It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .Returns(() => 
                {
                    ViewPponStoreCollection stores = new ViewPponStoreCollection();
                    stores.Add(new ViewPponStore
                    {
                        BusinessHourGuid = bid,
                        StoreGuid = selectedStoreGuid,
                        StoreName = "種花店",
                        Phone = "20889889",
                        IsLoaded = true
                    });

                    return stores;;
                });

            var opMock  = new Mock<IOrderProvider>();
            var mpMock  = new Mock<IMemberProvider>();
            var cpMock  = new Mock<ISysConfProvider>();
            var vpMock  = new Mock<IVerificationProvider>();
            var hpMock  = new Mock<IHiDealProvider>();


            var builder = new ContainerBuilder();
            builder.RegisterInstance(opMock.Object).As<IOrderProvider>();
            builder.RegisterInstance(mpMock.Object).As<IMemberProvider>();
            builder.RegisterInstance(ppMock.Object).As<IPponProvider>();
            builder.RegisterInstance(cpMock.Object).As<ISysConfProvider>();
            builder.RegisterInstance(vpMock.Object).As<IVerificationProvider>();
            builder.RegisterInstance(hpMock.Object).As<IHiDealProvider>();

            var build = builder.Build();
            ProviderFactory.ReInit(build.BeginLifetimeScope());

            ViewPponStoreManager.ReloadDefaultManager();
        }

        [Test]
        public void ToShop_MultiOptionsWithComboPackTest()
        {
            string strFromPayPageOrServices =
                "牛|^|3e714704-841a-44d4-a658-9e568a7e62ba,小|^|bfc8e474-9aad-495d-83a0-1c43d3d2e93f|#|1||豬|^|0a92e693-0458-48ca-9bfa-163ce4562d57,中|^|f8dc6d22-e87d-42b6-b719-941b0199a5ec|#|2||雞|^|ff118190-8bfc-4f5e-ad7c-116a6980998d,大|^|3078da57-7a3b-4c74-8949-16cb2968e814|#|1";

            var items = PaymentFacade.ProcessSubItemsAndAccessoriesWithComboPack(
                strFromPayPageOrServices, "testTicketId",
                new ViewPponDeal
                {
                    DeliveryType = (int)DeliveryType.ToShop,
                    ItemName = "吃好肉活動",
                    ComboPackCount = 4,
                    BusinessHourGuid = bid,
                    ShoppingCart = true,
                    ItemPrice = 50
                }, Guid.Empty, "");

            Assert.AreEqual(1, items.Count);
            Assert.AreEqual(items[0].ToString("&nbsp"), "吃好肉活動&nbsp(牛)(小)(豬)(中)(豬)(中)(雞)(大)");
            Assert.AreEqual(4, items[0].SubItems.Count);
        }

        /// <summary>
        /// 成套販售且設分店，也要帶分店資訊
        /// </summary>
        [Test]
        public void ToShop_MultiOptionsWithComboPackTest_HasStore()
        {
            string strFromPayPageOrServices =
                "牛|^|3e714704-841a-44d4-a658-9e568a7e62ba,小|^|bfc8e474-9aad-495d-83a0-1c43d3d2e93f|#|1||豬|^|0a92e693-0458-48ca-9bfa-163ce4562d57,中|^|f8dc6d22-e87d-42b6-b719-941b0199a5ec|#|2||雞|^|ff118190-8bfc-4f5e-ad7c-116a6980998d,大|^|3078da57-7a3b-4c74-8949-16cb2968e814|#|1";

            var items = PaymentFacade.ProcessSubItemsAndAccessoriesWithComboPack(
                strFromPayPageOrServices, "testTicketId",
                new ViewPponDeal
                {
                    DeliveryType = (int)DeliveryType.ToShop,
                    ItemName = "吃好肉活動",
                    ComboPackCount = 4,
                    BusinessHourGuid = bid,
                    ShoppingCart = true,
                    ItemPrice = 50
                }, selectedStoreGuid, "");

            Assert.AreEqual(items[0].ToString("&nbsp"), "吃好肉活動&nbsp(牛)(小)(豬)(中)(豬)(中)(雞)(大)(預約專線20889889)");
        }

        [Test]
        public void ToShop_NoOptionHasStoreAndBuyOne()
        {            
            string strFromPayPageOrServices = "|#|1";

            var items = PaymentFacade.ProcessSubItemsAndAccessoriesWithComboPack(
                strFromPayPageOrServices, "testTicketId",
                new ViewPponDeal {
                    DeliveryType = (int)DeliveryType.ToShop,
                    ItemName = "吃好肉活動",
                    ComboPackCount = 1,
                    BusinessHourGuid = bid,
                    ItemPrice = 50
                }, selectedStoreGuid, "");

            Assert.AreEqual(1, items.Count);
            Assert.AreEqual(1, items[0].SelectedAccessory.Count); // 是1 因為有分店
            Assert.AreEqual(0, items[0].SubItems.Count);
            Assert.AreEqual(1, items[0].OrderQuantity);
            Assert.AreEqual("吃好肉活動&nbsp(預約專線20889889)", items[0].ToString("&nbsp"));
        }

        [Test]
        public void ToShop_NoOptionHasStoreAndBuyMany()
        {
            string strFromPayPageOrServices = "|#|5";

            var items = PaymentFacade.ProcessSubItemsAndAccessoriesWithComboPack(
                strFromPayPageOrServices, "testTicketId",
                new ViewPponDeal
                {
                    DeliveryType = (int)DeliveryType.ToShop,
                    ItemName = "吃好肉活動",
                    ComboPackCount = 1,
                    BusinessHourGuid = bid,
                    ItemPrice = 50
                }, selectedStoreGuid, "");

            Assert.AreEqual(1, items.Count);
            Assert.AreEqual(1, items[0].SelectedAccessory.Count); // 是1 因為有分店
            Assert.AreEqual(0, items[0].SubItems.Count);
            Assert.AreEqual(5, items[0].OrderQuantity);
            Assert.AreEqual("吃好肉活動&nbsp(預約專線20889889)", items[0].ToString("&nbsp"));
        }

        [Test]
        public void ToShop_MultiOptionsHasStore()
        {
            string strFromPayPageOrServices =
                "牛|^|3e714704-841a-44d4-a658-9e568a7e62ba,小|^|bfc8e474-9aad-495d-83a0-1c43d3d2e93f|#|1";

            var items = PaymentFacade.ProcessSubItemsAndAccessoriesWithComboPack(
                strFromPayPageOrServices, "testTicketId",
                new ViewPponDeal
                {
                    DeliveryType = (int)DeliveryType.ToShop,
                    ItemName = "吃好肉活動",
                    ComboPackCount = 1,
                    ShoppingCart = true,
                    BusinessHourGuid = bid,
                    ItemPrice = 50
                }, selectedStoreGuid, "");

            Assert.AreEqual(1, items.Count);
            Assert.AreEqual(3, items[0].SelectedAccessory.Count);
            Assert.AreEqual(items[0].ToString("&nbsp"), "吃好肉活動&nbsp(牛)(小)(預約專線20889889)");
        }

        //

        /// <summary>
        /// 要測宅配檔不會顯示分店資訊
        /// </summary>
        [Test]
        public void ToHouse_NoOptionWithStoreAndBuyOne()
        {
            string strFromPayPageOrServices = "|#|1";

            var items = PaymentFacade.ProcessSubItemsAndAccessoriesWithComboPack(
                strFromPayPageOrServices, "testTicketId",
                new ViewPponDeal
                {
                    DeliveryType = (int)DeliveryType.ToHouse,
                    ItemName = "買好肉活動",
                    ComboPackCount = 1,
                    BusinessHourGuid = bid,
                    ItemPrice = 50
                }, selectedStoreGuid, "");

            Assert.AreEqual(1, items.Count);
            Assert.AreEqual(0, items[0].SelectedAccessory.Count);
            Assert.AreEqual("買好肉活動", items[0].ToString("&nbsp"));
        }


    }
}

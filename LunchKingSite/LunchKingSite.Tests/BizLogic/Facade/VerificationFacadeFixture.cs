﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using Moq;
using NUnit.Framework;

namespace LunchKingSite.Tests.BizLogic.Facade
{
    [TestFixture]
    public class VerificationFacadeFixture
    {
        [SetUp]
        public void SetUp()
        {
            var vpMock = new Mock<IVerificationProvider>();
            var mpMock = new Mock<IMemberProvider>();
            var configMock = new Mock<ISysConfProvider>();
            var ppMock = new Mock<IPponProvider>();


            var builder = new ContainerBuilder();
            builder.RegisterInstance(ppMock.Object).As<IPponProvider>();
            builder.RegisterInstance(mpMock.Object).As<IMemberProvider>();
            builder.RegisterInstance(configMock.Object).As<ISysConfProvider>();
            builder.RegisterInstance(vpMock.Object).As<IVerificationProvider>();

            var build = builder.Build();
            ProviderFactory.ReInit(build.BeginLifetimeScope());
        }

        [Test]
        public void SetPayerNameHiddenTest()
        {
            Assert.AreEqual("你*嗎", VerificationFacade.SetPayerNameHidden("你好嗎"));
            Assert.AreEqual("一*", VerificationFacade.SetPayerNameHidden("一姬"));
            Assert.AreEqual("龖*", VerificationFacade.SetPayerNameHidden("龖5"));
            Assert.AreEqual("龖*", VerificationFacade.SetPayerNameHidden("龖測"));
            Assert.AreEqual("龖*試", VerificationFacade.SetPayerNameHidden("龖測試"));
            Assert.AreEqual("測*試", VerificationFacade.SetPayerNameHidden("測龖試"));
            Assert.AreEqual("龖", VerificationFacade.SetPayerNameHidden("龖"));
            Assert.AreEqual("他", VerificationFacade.SetPayerNameHidden("他"));
        }

    }


}

﻿using System;
using Autofac;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Moq;
using NUnit.Framework;


namespace LunchKingSite.Tests.BizLogic.Facade
{
    [TestFixture]
    public class SellerFacadeFixture
    {
        private Mock<ISystemProvider> sys;
        private int sellerId;
        [SetUp]
        public void Setup()
        {
        
            var ppMock = new Mock<IPponProvider>();
            var spMock = new Mock<ISellerProvider>();
            var lpMock = new Mock<ILocationProvider>();
            var hpMock = new Mock<IHumanProvider>();
            var mpMock = new Mock<IMemberProvider>();
            sys = new Mock<ISystemProvider>();


            var builder = new ContainerBuilder();
            builder.RegisterInstance(ppMock.Object).As<IPponProvider>();
            builder.RegisterInstance(spMock.Object).As<ISellerProvider>();
            builder.RegisterInstance(lpMock.Object).As<ILocationProvider>();
            builder.RegisterInstance(hpMock.Object).As<IHumanProvider>();
            builder.RegisterInstance(mpMock.Object).As<IMemberProvider>();
            builder.RegisterInstance(sys.Object).As<ISystemProvider>();

            sys.Setup(t => t.SerialNumberGetNext(It.IsAny<string>(), 1)).Returns(delegate(string serialName)
            {
                if (serialName == SellerFacade._SELLERID_SERIAL_NUMBER)
                {
                    sellerId++;
                    return sellerId;
                }
                throw new NotSupportedException();
            });

            var build = builder.Build();
            ProviderFactory.ReInit(build.BeginLifetimeScope());

        }

        [Test]
        public void GetNewSellerIDTest()
        {
            sellerId = 10431;
            Assert.AreEqual("A0-10-432", SellerFacade.GetNewSellerID());
            sellerId = 10999;
            Assert.AreEqual("A0-11-000", SellerFacade.GetNewSellerID());
            sellerId = 199999;
            Assert.AreEqual("A2-00-000", SellerFacade.GetNewSellerID());
            sellerId = 999999;
            Assert.AreEqual("B0-00-000", SellerFacade.GetNewSellerID());

            //error
            sellerId = 25999999;
            Assert.IsNull(SellerFacade.GetNewSellerID());
        }
    }
}

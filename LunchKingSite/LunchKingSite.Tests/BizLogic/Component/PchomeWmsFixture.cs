﻿using LunchKingSite.BizLogic.Models.PchomeWarehouse;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Models.Wms;
using Autofac;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using Moq;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Tests.BizLogic.Component
{
    [TestFixture]
    public class PchomeWmsFixture
    {
        [SetUp]
        public void Setup()
        {
            var config = new Mock<ISysConfProvider>();
            config.Setup(t => t.EnableWms).Returns(false);
            config.Setup(t => t.WmsVendorId).Returns("V00000");
            config.Setup(t => t.PchomeWmsAESKey).Returns("sq9/W4/4kjy0OI7TB0yaeSAcbZf16EemSx80t1yeo1E=");
            var builder = new ContainerBuilder();
            
            builder.RegisterInstance(config.Object).As<ISysConfProvider>();
            

            var build = builder.Build();
            ProviderFactory.ReInit(build.BeginLifetimeScope());
        }

        [Test]
        public void ProdAdd()
        {
            var nProd = new WmsProd
            {
                GroupId = "G001",
                VendorPId = "XYZ0001",
                Name = "多啦A夢等身公仔"
            };
            bool dataAdded = PchomeWmsAPI.AddProd(nProd);
            Assert.IsTrue(dataAdded);
            Assert.IsTrue(string.IsNullOrEmpty(nProd.Id)==false);            
        }
        [Test]
        public void GetProd()
        {
            var prod = PchomeWmsAPI.GetProd("V0001");
            Assert.IsNotNull(prod);
        }
        [Test]
        public void AddSpec()
        {
            var nSpec = new WmsProdSpec
            {
                GroupId = "G001",
                VendorPId = "XYZ0002",
                Spec = "巨人版"
            };
            Assert.IsNotNull(nSpec);
            bool specAdded = PchomeWmsAPI.AddSpec(nSpec);
            Assert.IsTrue(specAdded);
            Assert.IsTrue(string.IsNullOrEmpty(nSpec.Id) == false);
        }
        [Test]
        public void GetProdsByGroupIds()
        {
            var prods = PchomeWmsAPI.GetProdsByGroupIds("GP2016020000035");            
            Assert.IsTrue(prods.Count > 0);
        }
        [Test]
        public void GetProdsByVendorPIds()
        {
            var prods = PchomeWmsAPI.GetProdsByVendorPIds("VP00001");
            Assert.IsTrue(prods.Count > 0);
        }

        public void GetInventories()
        {
            List<WmsInventory> inventories = PchomeWmsAPI.GetInventories("P1502000071", "P1502000072");
            Assert.IsTrue(inventories.Count > 0);
        }
        /// <summary>
        /// 建立進貨單測試
        /// </summary>
        [Test]
        public void AddPurchaseOrder()
        {
            var po = new WmsPurchaseOrder
            {
                Prod = new WmsPurchaseOrder.PurchaseOrderProd
                {
                    Id = "V0001",
                    Price = 200,
                    Qty = 5
                },
                SupplyId = "S0001",
                SupplyName = "東京著衣",
                ApplyDate = DateTime.Now.ToString("yyyy/MMddHHmm"),
                PurchaseContact = new WmsPerson
                {
                    Name = "陳小姐",
                    Tel = "02-29876543",
                    Mobile = "0987654321"
                },
                ReturnContact = new WmsPerson
                {
                    Name = "陳小姐",
                    Tel = "02-29876543",
                    Mobile = "0987654321",
                    Email = "abc@gmail.com",
                    Zip = "106",
                    Address = "台北市大安區...."
                }
            };
            bool purchaseOrderAdded = PchomeWmsAPI.AddPurchaseOrder(po);
            Assert.IsTrue(purchaseOrderAdded);
            Assert.IsTrue(string.IsNullOrEmpty(po.Id) == false);
        }
        [Test]
        public void GetPurchaseOrder()
        {
            var purchaseOrder = PchomeWmsAPI.GetPurchaseOrder("LV1412027000001");
            Assert.IsNotNull(purchaseOrder);
        }
        [Test]
        public void GetPurchaseOrderProgressStatuses()
        {
            var purchaseOrderStatuses = PchomeWmsAPI.GetPurchaseOrderProgressStatuses("LV1412027000001");
            Assert.IsNotNull(purchaseOrderStatuses);
        }
        [Test]
        public void AddOrder()
        {
            var newOrder = new WmsOrder
            {
                VendorOrderId = "VD000001",
                Detail = new List<WmsOrder.OrderDetail>(new WmsOrder.OrderDetail[]
                {
                         new WmsOrder.OrderDetail
                         {
                              VendorOrderNo = "VD001",
                               ProdId = ""
                         }
                }),
                ReceiveContact = new WmsPerson
                {
                    Name = "劉愛買",
                    Tel = "02-33334444",
                    Mobile = "0987654321",
                    Email = "test@test.test",
                    Zip = "100",
                    Address = "台北市大山區...."
                }
            };
            string content;
            bool orderAdded = PchomeWmsAPI.AddOrder(newOrder, out content);
            Assert.IsTrue(orderAdded);
            Assert.IsTrue(string.IsNullOrEmpty(newOrder.Id) == false);
        }
        [Test]
        public void GetOrder()
        {
            WmsOrder order = PchomeWmsAPI.GetOrder("OR20160224000001");
            Assert.IsNotNull(order);
        }
        [Test]
        public void GetOrdersByVendorOrderId()
        {
            List<WmsOrder> orders = PchomeWmsAPI.GetOrdersByVendorOrderId("VD000001", "VD000002");
            Assert.IsTrue(orders.Count > 0);
        }

        [Test]
        public void GetOrderProgressStatus()
        {
            List<WmsOrderProgressStatus> orderProgressStatuses = PchomeWmsAPI.GetOrderProgressStatus("OR20160619000001", "OR20160619000002");
            Assert.IsTrue(orderProgressStatuses.Count > 0);
        }
        [Test]
        public void GetLogistics()
        {
            List<WmsLogistic> logistics = PchomeWmsAPI.GetLogistics("OR20160619000001");
            Assert.IsTrue(logistics.Count > 0);
        }
        [Test]
        public void GetLogisticTrace()
        {
            WmsLogisticTrace logisticTrace = PchomeWmsAPI.GetLogisticTrace("OR20171201000001");
            Assert.IsNotNull(logisticTrace);
        }
        [Test]
        public void AddRefund()
        {
            WmsRefund newRefund = new WmsRefund
            {
                OrderId = "OR20160619000001",
                Detail = new List<WmsRefund.RefundDetail>(new WmsRefund.RefundDetail[] {
                          new WmsRefund.RefundDetail
                          {
                              OrderNo = "003",
                            ProdId = "P1502000073",
                             Qty = 2
                          }
                      }),
                RefundContact = new WmsPerson
                {
                    Name = "劉愛買",
                    Tel = "02-33334444",
                    Mobile = "0987654321",
                    Email = "test@test.test",
                    Zip = "100",
                    Address = "台北市大山區...."
                },
                ReceiveContact = new WmsPerson
                {
                    Name = "天母嚴選",
                    Tel = "02-29876543",
                    Mobile = "0987654321",
                    Email = "abc@gmail.com",
                    Zip = "106",
                    Address = "台北市大安區...."
                }
            };
            bool refundAdded = PchomeWmsAPI.AddRefund(newRefund);
            Assert.IsTrue(refundAdded);
            Assert.IsFalse(string.IsNullOrEmpty(newRefund.Id));
        }

        [Test]
        public void GetRefund()
        {
            WmsRefund refund = PchomeWmsAPI.GetRefund("RF20160603000007");
            Assert.IsNotNull(refund);
        }
        [Test]
        public void GetRefundTrace()
        {
            WmsRefundTrace refundTrace = PchomeWmsAPI.GetRefundTrace("RF20160603000007");
            Assert.IsNotNull(refundTrace);
        }
        [Test]
        public void AddReturn1()
        {
            WmsAddReturn newReturn = new WmsAddReturn
            {
                Detail = new WmsAddReturn.ReturnDetail
                        {
                            ProdId = "P1502000073",
                            Qty = 2,
                            Remark = "商品停賣"
                        },
                SupplyId = "S0001",
                ReceiveContact = new WmsPerson
                {
                    Name = "天母嚴選",
                    Tel = "02-23654789",
                    Mobile = "0912345678",
                    Email = "",
                    Zip = "106",
                    Address = "台北市大安區...."
                }
            };
            bool returnAdded = PchomeWmsAPI.AddReturn(newReturn);
            Assert.IsTrue(returnAdded);
        }
        [Test]
        public void AddReturn2()
        {
            WmsAddReturn newReturn = new WmsAddReturn
            {
                Detail = new WmsAddReturn.ReturnDetail
                {
                    ProdId = "P1502000073",
                    Qty = 2,
                    Remark = "商品停賣"
                },
                SupplyId = "S0001",
                ReceiveContact = new WmsPerson
                {
                    Name = "天母嚴選",
                    Tel = "02-23654789",
                    Mobile = "0912345678",
                    Email = "",
                    Zip = "106",
                    Address = "台北市大安區...."
                }
            };
            bool returnAdded = PchomeWmsAPI.AddReturn(newReturn);
            Assert.IsTrue(returnAdded);
            Assert.IsFalse(string.IsNullOrEmpty(newReturn.Id));
        }
        [Test]
        public void GetReturns()
        {
            List<WmsReturn> wmsReturns = PchomeWmsAPI.GetReturns("RT20161201000001", "RT20161202000001");
            Assert.IsTrue(wmsReturns.Count > 0);
        }

        [Test]
        public void GetWmsReturnQueryList()
        {
            WmsReturnQueryList returnsQueryList = PchomeWmsAPI.GetWmsReturnQueryList(
                DateTime.Parse("2016/12/10"), DateTime.Parse("2016/12/15"));
            Assert.IsTrue(returnsQueryList.TotalRows > 0);
        }
    }
}

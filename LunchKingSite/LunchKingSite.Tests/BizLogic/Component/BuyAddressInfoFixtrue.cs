﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Model;
using NUnit.Framework;

namespace LunchKingSite.Tests.BizLogic.Component
{
    [TestFixture]
    public class BuyAddressInfoFixtrue
    {
        [Test]
        public void ParseTest()
        {
            string baiStr =
                "孔|雀魚|0912345678|294|308|00000000-0000-0000-0000-000000000000|中山北路一段11號13樓^子彈飛|0912654321|295|309|00000000-0000-0000-0000-000000000000|長安東路一段16號7樓|True";
            var bai = BuyAddressInfo.Parse(baiStr);
            Assert.AreEqual(bai.FamilyName, "孔");
            Assert.AreEqual(bai.GivenName, "雀魚");
            Assert.AreEqual(bai.BuyerMobile, "0912345678");
            Assert.AreEqual(bai.BuyerCityId, 294);
            Assert.AreEqual(bai.BuyerAreaId, 308);
            Assert.AreEqual(bai.BuyerBuildingGuid, Guid.Empty);
            Assert.AreEqual(bai.BuyerAddress, "中山北路一段11號13樓");


            Assert.AreEqual(bai.ReceiverName, "子彈飛");
            Assert.AreEqual(bai.ReceiverMobile, "0912654321");
            Assert.AreEqual(bai.ReceiverCityId, 295);
            Assert.AreEqual(bai.ReceiverAreaId, 309);            
            Assert.AreEqual(bai.ReceiverBuildingGuid, Guid.Empty);
            Assert.AreEqual(bai.ReceiverAddress, "長安東路一段16號7樓");
            Assert.AreEqual(bai.AddReceiverInfo, true);

            Assert.AreEqual(bai.ToFlatString(), baiStr);
        }

        [Test]
        public void ParseTest2()
        {
            string baiStr =
                "孔|雀魚|0912345678|294|308|00000000-0000-0000-0000-000000000000|中山北路一段11號13樓^";
            var bai = BuyAddressInfo.Parse(baiStr);
          
            Assert.IsTrue(string.IsNullOrWhiteSpace(bai.ReceiverName));
        }
    }
}

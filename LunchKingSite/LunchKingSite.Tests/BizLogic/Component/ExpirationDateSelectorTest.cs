﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace LunchKingSite.Tests.BizLogic.Component
{
    /// <summary>
    /// Summary description for ExpirationDateSelectorTest
    /// </summary>
    [TestClass]
    public class ExpirationDateSelectorTest
    {
        public ExpirationDateSelectorTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void GetExpirationDate_ExpirationDatesAllNull_ReturnsDateTimeMax()
        {
            UsageExpirationStub stub = new UsageExpirationStub();
            ExpirationDateSelector selector = new ExpirationDateSelector();
            selector.ExpirationDates = stub;

            DateTime result = selector.GetExpirationDate();

            Assert.AreEqual(DateTime.MaxValue.Ticks, result.Ticks);
        }
    }

    public class UsageExpirationStub : IExpirationChangeable
    {
		public DateTime? DealUseStartDate { get; set; }

        public DateTime? DealOriginalExpireDate { get; set; }

        public DateTime? DealChangedExpireDate { get; set; }

        public DateTime? StoreChangedExpireDate { get; set; }

        public DateTime? SellerClosedDownDate { get; set; }

        public DateTime? StoreClosedDownDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using Autofac;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Moq;
using NUnit.Framework;

namespace LunchKingSite.Tests.BizLogic.Component
{
    [TestFixture]
    public class OrderedQuantityHelperFixture
    {
        public static Dictionary<Guid, ViewComboDealCollection> ComboDeals =
            new Dictionary<Guid, ViewComboDealCollection>();

        [SetUp]
        public void Setup()
        {
            var cfgMock = new Mock<ISysConfProvider>();
            cfgMock.Setup(t => t.ThousandSelling).Returns(1000);
            var ppMock = new Mock<IPponProvider>();
            ppMock.Setup(t => t.GetViewComboDealByBid(It.IsAny<Guid>(), It.IsAny<bool>()))
                  .Returns((Guid bid, bool isMain) =>
                  {
                      if (ComboDeals.ContainsKey(bid))
                      {
                          return ComboDeals[bid];
                      }
                      return new ViewComboDealCollection();
                  });
            var spMock = new Mock<ISellerProvider>();
            var cacheMock = new Mock<ICacheProvider>();
            var builder = new ContainerBuilder();
            builder.RegisterInstance(cfgMock.Object).As<ISysConfProvider>();
            builder.RegisterInstance(ppMock.Object).As<IPponProvider>();
            builder.RegisterInstance(spMock.Object).As<ISellerProvider>();
            builder.RegisterInstance(cacheMock.Object).As<ICacheProvider>();

            var build = builder.Build();
            ProviderFactory.ReInit(build.BeginLifetimeScope());
        }

        [Test]
        public void MockIsReady()
        {
            Assert.AreEqual(1000,
                            ProviderFactory.Instance().GetConfig().ThousandSelling);
        }

        [Test]
        public void OverThousnadNoComboNoMultiplierTest()
        {
            ViewPponDeal deal = new ViewPponDeal()
            {
                OrderedQuantity = 800
            };
            ViewPponDeal deal2 = new ViewPponDeal()
            {
                OrderedQuantity = 1100
            };

            Assert.IsFalse(OrderedQuantityHelper.OverThousand(deal));
            Assert.IsTrue(OrderedQuantityHelper.OverThousand(deal2));
        }

        [Test]
        public void OverThousnadNoComboWithMultiplierTest()
        {
            ViewPponDeal deal = new ViewPponDeal()
            {
                OrderedQuantity = 500,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 3
            };
            ViewPponDeal deal2 = new ViewPponDeal()
            {
                OrderedQuantity = 400,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 2
            };
            ViewPponDeal deal3 = new ViewPponDeal()
            {
                OrderedQuantity = 1050,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 1
            };
            ViewPponDeal deal4 = new ViewPponDeal()
            {
                OrderedQuantity = 950,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 0
            };

            Assert.IsTrue(OrderedQuantityHelper.OverThousand(deal));
            Assert.IsFalse(OrderedQuantityHelper.OverThousand(deal2));
            Assert.IsTrue(OrderedQuantityHelper.OverThousand(deal3));
            Assert.IsFalse(OrderedQuantityHelper.OverThousand(deal4));
        }

        [Test]
        public void OverThousnadIsComboNoMultiplierTest()
        {
            ViewPponDeal deal = new ViewPponDeal()
            {
                BusinessHourGuid = Guid.NewGuid(),
                OrderedQuantity = 950,
                BusinessHourStatus = (int)BusinessHourStatus.ComboDealMain
            };

            ComboDeals.Clear();
            ComboDeals[deal.BusinessHourGuid] = new ViewComboDealCollection();
            ComboDeals[deal.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 600
            });
            ComboDeals[deal.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 450
            });
            Assert.IsTrue(OrderedQuantityHelper.OverThousand(deal));

            ViewPponDeal deal2 = new ViewPponDeal()
            {
                BusinessHourGuid = Guid.NewGuid(),
                OrderedQuantity = 700,
                BusinessHourStatus = (int)BusinessHourStatus.ComboDealMain
            };

            ComboDeals.Clear();
            ComboDeals[deal2.BusinessHourGuid] = new ViewComboDealCollection();
            ComboDeals[deal2.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 150
            });
            ComboDeals[deal2.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 350
            });
            ComboDeals[deal2.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 200
            });
            Assert.IsFalse(OrderedQuantityHelper.OverThousand(deal2));
        }

        [Test]
        public void OverThousnadIsComboWithMultiplierTest()
        {
            ViewPponDeal deal = new ViewPponDeal()
            {
                BusinessHourGuid = Guid.NewGuid(),
                OrderedQuantity = 950,
                BusinessHourStatus = (int)BusinessHourStatus.ComboDealMain
            };

            ComboDeals.Clear();
            ComboDeals[deal.BusinessHourGuid] = new ViewComboDealCollection();
            ComboDeals[deal.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 500,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 1
            });
            ComboDeals[deal.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 450,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 2
            });
            Assert.IsTrue(OrderedQuantityHelper.OverThousand(deal));

            ViewPponDeal deal2 = new ViewPponDeal()
            {
                BusinessHourGuid = Guid.NewGuid(),
                OrderedQuantity = 700,
                BusinessHourStatus = (int)BusinessHourStatus.ComboDealMain
            };

            ComboDeals.Clear();
            ComboDeals[deal2.BusinessHourGuid] = new ViewComboDealCollection();
            ComboDeals[deal2.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 150,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 2
            });
            ComboDeals[deal2.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 350,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 1
            });
            ComboDeals[deal2.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 200,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 2
            });
            Assert.IsTrue(OrderedQuantityHelper.OverThousand(deal2));
        }

        [Test]
        public void OrderedQuantityTest()
        {
            ViewPponDeal deal = new ViewPponDeal()
            {
                OrderedQuantity = 800
            };

            Assert.IsFalse(deal.GetAdjustedOrderedQuantity().IsAdjusted);
            Assert.AreEqual(800, deal.GetAdjustedOrderedQuantity().Quantity);
            Console.WriteLine("一般檔次設定以人數顯示銷售量，測試 OK");

            ViewPponDeal deal2 = new ViewPponDeal()
            {
                OrderedQuantity = 500,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 3
            };

            Assert.IsTrue(deal2.GetAdjustedOrderedQuantity().IsAdjusted);
            Assert.AreEqual(1500, deal2.GetAdjustedOrderedQuantity().Quantity);
            Console.WriteLine("一般檔次設定以份數顯示銷售量，測試 OK");

            ViewPponDeal deal3 = new ViewPponDeal
            {
                BusinessHourGuid = Guid.NewGuid(),
                BusinessHourStatus = (int)BusinessHourStatus.ComboDealMain,
                OrderedQuantity = 12,
            };
            ComboDeals[deal3.BusinessHourGuid] = new ViewComboDealCollection();
            ComboDeals[deal3.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 5
            });
            ComboDeals[deal3.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 7
            });
            Assert.IsFalse(deal3.GetAdjustedOrderedQuantity().IsAdjusted);
            Assert.AreEqual(12, deal3.GetAdjustedOrderedQuantity().Quantity);
            Console.WriteLine("多檔次設定以人數顯示銷售量，測試 OK");

            ViewPponDeal deal4 = new ViewPponDeal
            {
                BusinessHourGuid = Guid.NewGuid(),
                BusinessHourStatus = (int)BusinessHourStatus.ComboDealMain,
                OrderedQuantity = 12,
            };
            ComboDeals[deal4.BusinessHourGuid] = new ViewComboDealCollection();
            ComboDeals[deal4.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 5,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 6
            });
            ComboDeals[deal4.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 7,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 12
            });
            Assert.IsTrue(deal4.GetAdjustedOrderedQuantity().IsAdjusted);
            Assert.AreEqual(114, deal4.GetAdjustedOrderedQuantity().Quantity);
            Console.WriteLine("多檔次設定以份數顯示銷售量，測試 OK");
        }

        [Test]
        public void InsufficientQuantityTest()
        {
            ViewPponDeal deal = new ViewPponDeal
            {
                BusinessHourOrderMinimum = 10,
                OrderedQuantity = 4
            };
            Assert.AreEqual(6, deal.GetAdjustedInsufficientQuantity().Quantity);
            Console.WriteLine("一般檔次未達團購門坎且以人數顯示 OK");

            ViewPponDeal deal2 = new ViewPponDeal
            {
                BusinessHourOrderMinimum = 10,
                OrderedQuantity = 4,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 3
            };

            Assert.AreEqual(18, deal2.GetAdjustedInsufficientQuantity().Quantity);
            Assert.IsTrue(deal2.GetAdjustedInsufficientQuantity().IsAdjusted);
            Console.WriteLine("一般檔次未達團購門坎且設定以份數顯示 OK");

            ViewPponDeal deal3 = new ViewPponDeal
            {
                BusinessHourGuid = Guid.NewGuid(),
                BusinessHourStatus = (int)BusinessHourStatus.ComboDealMain,
                OrderedQuantity = 4,
                BusinessHourOrderMinimum = 10
            };
            ComboDeals[deal3.BusinessHourGuid] = new ViewComboDealCollection();
            ComboDeals[deal3.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 3
            });
            ComboDeals[deal3.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 1
            });
            Assert.IsFalse(deal3.GetAdjustedInsufficientQuantity().IsAdjusted);
            Assert.AreEqual(6, deal3.GetAdjustedInsufficientQuantity().Quantity);
            Console.WriteLine("多檔次未達團購門坎且設定以人數顯示 OK");

            ViewPponDeal deal4 = new ViewPponDeal
            {
                BusinessHourGuid = Guid.NewGuid(),
                BusinessHourStatus = (int)BusinessHourStatus.ComboDealMain,
                OrderedQuantity = 4,
                BusinessHourOrderMinimum = 10
            };
            ComboDeals[deal4.BusinessHourGuid] = new ViewComboDealCollection();
            ComboDeals[deal4.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 3,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 2
            });
            ComboDeals[deal4.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 1,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 4
            });
            Assert.AreEqual(12, deal4.GetAdjustedInsufficientQuantity().Quantity);
            Console.WriteLine("多檔次未達團購門坎且設定以份數顯示 OK");


            ViewPponDeal deal5 = new ViewPponDeal
            {
                BusinessHourGuid = Guid.NewGuid(),
                BusinessHourStatus = (int)BusinessHourStatus.ComboDealMain,
                OrderedQuantity = 12,
                BusinessHourOrderMinimum = 10
            };
            ComboDeals[deal5.BusinessHourGuid] = new ViewComboDealCollection();
            ComboDeals[deal5.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 7,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 2
            });
            ComboDeals[deal5.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 5,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 4
            });
            Assert.IsTrue(deal5.GetAdjustedInsufficientQuantity().IsAdjusted);
            Assert.AreEqual(0, deal5.GetAdjustedInsufficientQuantity().Quantity);
            Console.WriteLine("多檔次達團購門坎且設定以份數顯示，應該顯示0 OK");

            ViewPponDeal deal6 = new ViewPponDeal
            {
                BusinessHourGuid = Guid.NewGuid(),
                BusinessHourStatus = (int)BusinessHourStatus.ComboDealMain,
                OrderedQuantity = 12,
                BusinessHourOrderMinimum = 10
            };
            ComboDeals[deal6.BusinessHourGuid] = new ViewComboDealCollection();
            ComboDeals[deal6.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 7
            });
            ComboDeals[deal6.BusinessHourGuid].Add(new ViewComboDeal
            {
                OrderedQuantity = 5
            });
            Assert.IsFalse(deal6.GetAdjustedInsufficientQuantity().IsAdjusted);
            Assert.AreEqual(0, deal6.GetAdjustedInsufficientQuantity().Quantity);
            Console.WriteLine("多檔次達團購門坎且設定以人數顯示，應該顯示0 OK");
        }

        [Test]
        public void SlugTest()
        {
            ViewPponDeal deal = new ViewPponDeal
            {
                Slug = "5",
                OrderedQuantity = 6
            };
            Assert.IsFalse(deal.GetAdjustedSlug().IsAdjusted);
            Assert.AreEqual(5, deal.GetAdjustedSlug().Quantity);
            Console.WriteLine("一般檔次結檔購買數，以人數顯示 OK");

            ViewPponDeal deal2 = new ViewPponDeal
            {
                Slug = "5",
                OrderedQuantity = 6,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 12
            };
            Assert.IsTrue(deal2.GetAdjustedSlug().IsAdjusted);
            Assert.AreEqual(60, deal2.GetAdjustedSlug().Quantity);
            Console.WriteLine("一般檔次結檔購買數，以份數顯示 OK");

            ViewPponDeal deal3 = new ViewPponDeal
            {
                BusinessHourGuid = Guid.NewGuid(),
                BusinessHourStatus = (int)BusinessHourStatus.ComboDealMain,
                Slug = "12",
                OrderedQuantity = 6,
            };
            ComboDeals[deal3.BusinessHourGuid] = new ViewComboDealCollection();
            ComboDeals[deal3.BusinessHourGuid].Add(new ViewComboDeal
            {
                Slug = "5"
            });
            ComboDeals[deal3.BusinessHourGuid].Add(new ViewComboDeal
            {
                Slug = "7"
            });
            Assert.IsFalse(deal3.GetAdjustedSlug().IsAdjusted);
            Assert.AreEqual(12, deal3.GetAdjustedSlug().Quantity);
            Console.WriteLine("多檔次結檔購買數，以人數顯示 OK");

            ViewPponDeal deal4 = new ViewPponDeal
            {
                BusinessHourGuid = Guid.NewGuid(),
                BusinessHourStatus = (int)BusinessHourStatus.ComboDealMain,
                Slug = "12",
                OrderedQuantity = 6,
            };
            ComboDeals[deal4.BusinessHourGuid] = new ViewComboDealCollection();
            ComboDeals[deal4.BusinessHourGuid].Add(new ViewComboDeal
            {
                Slug = "5",
                IsQuantityMultiplier = true,
                QuantityMultiplier = 6
            });
            ComboDeals[deal4.BusinessHourGuid].Add(new ViewComboDeal
            {
                Slug = "7",
                IsQuantityMultiplier = true,
                QuantityMultiplier = 12
            });
            Assert.IsTrue(deal4.GetAdjustedSlug().IsAdjusted);
            Assert.AreEqual(114, deal4.GetAdjustedSlug().Quantity);
            Console.WriteLine("多檔次結檔購買數，以份數顯示 OK");

            ViewPponDeal deal5 = new ViewPponDeal
            {
                BusinessHourGuid = Guid.NewGuid(),
                BusinessHourStatus = (int)BusinessHourStatus.ComboDealMain,
                Slug = null,
                OrderedQuantity = 6,
            };
            ComboDeals[deal5.BusinessHourGuid] = new ViewComboDealCollection();
            ComboDeals[deal5.BusinessHourGuid].Add(new ViewComboDeal
            {
                Slug = null
            });
            ComboDeals[deal5.BusinessHourGuid].Add(new ViewComboDeal
            {
                Slug = null
            });
            Assert.IsFalse(deal5.GetAdjustedSlug().IsAdjusted);
            Assert.AreEqual(0, deal5.GetAdjustedSlug().Quantity);
            Console.WriteLine("多檔次未結檔，購買數以人數顯示，應為 0 OK");

            ViewPponDeal deal6 = new ViewPponDeal
            {
                BusinessHourGuid = Guid.NewGuid(),
                BusinessHourStatus = (int)BusinessHourStatus.ComboDealMain,
                Slug = null,
                OrderedQuantity = 6,
            };
            ComboDeals[deal6.BusinessHourGuid] = new ViewComboDealCollection();
            ComboDeals[deal6.BusinessHourGuid].Add(new ViewComboDeal
            {
                Slug = null,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 2
            });
            ComboDeals[deal6.BusinessHourGuid].Add(new ViewComboDeal
            {
                Slug = null,
                IsQuantityMultiplier = true,
                QuantityMultiplier = 4
            });
            Assert.IsTrue(deal6.GetAdjustedSlug().IsAdjusted);
            Assert.AreEqual(0, deal6.GetAdjustedSlug().Quantity);
            Console.WriteLine("多檔次未結檔，購買數以份數顯示，應為 0 OK");
        }
    }
}
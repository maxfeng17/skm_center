﻿using LunchKingSite.BizLogic.Component;
using NUnit.Framework;

namespace LunchKingSite.Tests.BizLogic.Component
{
    [TestFixture]
    public class PathAndQueryBuilderFixture
    {
        [Test]
        public void AppendTest()
        {
            string oldUrl = "/deal/012345678";
            Assert.AreEqual("/deal/012345678?cpa=123", 
                PathAndQueryBuilder.Parse(oldUrl).Append("cpa", "123").ToString());
        }
        [Test]
        public void AppendTest2()
        {
            string oldUrl = "/deal/012345678?cpa=456";
            Assert.AreEqual("/deal/012345678?cpa=123", 
                PathAndQueryBuilder.Parse(oldUrl).Append("cpa", "123").ToString());
        }
        [Test]
        public void AppendTest3()
        {
            string oldUrl = "/deal/012345678?k=1&cpa=456&t=2";
            Assert.AreEqual("/deal/012345678?k=1&cpa=123&t=2", 
                PathAndQueryBuilder.Parse(oldUrl).Append("cpa", "123").ToString());
        }

    }
}

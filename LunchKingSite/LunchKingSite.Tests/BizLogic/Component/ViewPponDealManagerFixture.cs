﻿using Autofac;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Moq;
using NUnit.Framework;

namespace LunchKingSite.Tests.BizLogic.Component
{
    [TestFixture]
    public class ViewPponDealManagerFixture
    {
        [SetUp]
        public void Setup()
        {
            var op = new Mock<IOrderProvider>();
            op.Setup(t => t.CreditcardBankInfoGetList()).Returns(delegate() {
                CreditcardBankInfoCollection result = new CreditcardBankInfoCollection();
                result.Add(new CreditcardBankInfo {
                    Bin = "431178"
                });
                return result;
            });

            var builder = new ContainerBuilder();
            builder.RegisterInstance(op.Object).As<IOrderProvider>();
            builder.RegisterInstance(new Mock<ISysConfProvider>().Object).As<ISysConfProvider>();
            builder.RegisterInstance(new Mock<IPponProvider>().Object).As<IPponProvider>();
            builder.RegisterInstance(new Mock<ISellerProvider>().Object).As<ISellerProvider>();

            var build = builder.Build();
            ProviderFactory.ReInit(build.BeginLifetimeScope());
        }
    }
}

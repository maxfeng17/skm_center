﻿using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using Moq;
using NUnit.Framework;

namespace LunchKingSite.Tests.BizLogic.Component
{
    [TestFixture]
    public class DiscountManagerFixture
    {
        /// <summary>
        /// 折扣後均價，條件與價格的測試程式
        /// </summary>
        [Test]
        public void GetDiscountPriceTest()
        {
            Guid bid = new Guid("bb749d1f-ed05-417c-a5c5-6612663769dd");
            DiscountManager.CampaignRule campaignRuleSpecialForSingleDeal =
                new DiscountManager.CampaignRule
                {
                     MinimumAmount = 1680,
                     Amount = 420,
                     PromotionIds = new HashSet<int>(new[] { 1247 }),
                     MinGrossMargin = null
                };

            Mock<IViewPponDeal> vpdMock = new Mock<IViewPponDeal>();
            vpdMock.Setup(t => t.BusinessHourGuid).Returns(bid);
            vpdMock.Setup(t => t.ItemPrice).Returns(1680);
            vpdMock.Setup(t => t.QuantityMultiplier).Returns(1);

            DiscountManager.DealDiscountTerm dealRule =
                new DiscountManager.DealDiscountTerm
                {
                    Bid = bid,
                    PromotionIds = new HashSet<int>(new[] { 1247 }),
                    MainBid = bid,
                    GrossMargin = 25.0m,
                    Banned = false,
                    BrandIds = new HashSet<int>(),
                    CategoryIds = new HashSet<int>(new[] { 88, 139, 221, 100295, 100296, 100298 })
                };

            bool matchResult = campaignRuleSpecialForSingleDeal.Match(dealRule);
            decimal avgPrice;
            bool matchPriceResult = campaignRuleSpecialForSingleDeal.MatchPrice(vpdMock.Object, out avgPrice);
            Assert.IsTrue(matchResult, "折價券活動與特定檔次 折扣條件，結果失敗。");
            Assert.IsTrue(matchPriceResult, "折價券活動與特定檔次進行 價格比對，結果失敗。");
            Assert.AreEqual(avgPrice, 1680 - 420);
        }
    }
}

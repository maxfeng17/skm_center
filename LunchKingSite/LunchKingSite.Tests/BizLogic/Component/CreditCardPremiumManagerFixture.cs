﻿using System;
using Autofac;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Moq;
using NUnit.Framework;

namespace LunchKingSite.Tests.BizLogic.Component
{
    [TestFixture]
    public class CreditCardPremiumManagerFixture
    {
        [SetUp]
        public void Setup()
        {
            var mp = new Mock<IMarketingProvider>();
            mp.Setup(t => t.CreditCardPremiumGetListByActivityCodeAndEnabled(It.IsAny<string>()))
                  .Returns((string activityCode) =>
                  {
                      CreditCardPremiumCollection items = new CreditCardPremiumCollection();
                      items.Add(new CreditCardPremium
                      {
                          ActivityCode = "PiinLife_Visa",
                          ActivityName = "品生活VISA專屬",
                          CardNum = "400353",
                          CardType = (int)HiDealVisaCardType.Signature,
                          Enabled = true
                      });

                      items.Add(new CreditCardPremium
                      {
                          ActivityCode = "PiinLife_Visa",
                          ActivityName = "品生活VISA專屬",
                          CardNum = "555666",
                          CardType = (int)HiDealVisaCardType.Platinum,
                          Enabled = true
                      });


                      items.Add(new CreditCardPremium
                      {
                          ActivityCode = "PiinLife_Visa",
                          ActivityName = "品生活VISA專屬",
                          CardNum = "55566677",
                          CardType = (int)HiDealVisaCardType.Infinite,
                          Enabled = true
                      });

                      return items;
                  });

            var builder = new ContainerBuilder();
            builder.RegisterInstance(mp.Object).As<IMarketingProvider>();

            var build = builder.Build();
            ProviderFactory.ReInit(build.BeginLifetimeScope());
        }

        [Test]
        public void IsPiinlifeVisaCardNumberTest()
        {
            Assert.IsTrue(CreditCardPremiumManager.IsPiinlifeVisaCardNumber("4003530000000000"));
            Assert.IsFalse(CreditCardPremiumManager.IsPiinlifeVisaCardNumber("1234567890123456"));
            Console.WriteLine("是否為Visa特別卡片(無限與御璽) 測試 OK");
            Assert.IsFalse(CreditCardPremiumManager.IsPiinlifeVisaCardNumber("55566600000000"));
            Console.WriteLine("白金卡不符合資格 測試 Ok");
        }

        [Test]
        public void GetPiinlifeVisaCardTypeTest()
        {
            Assert.AreEqual(HiDealVisaCardType.Platinum,
                CreditCardPremiumManager.GetPiinlifeVisaCardType("5556669999998888"));
            Assert.AreNotEqual(HiDealVisaCardType.Platinum,
                CreditCardPremiumManager.GetPiinlifeVisaCardType("3336669999998888"));
            Console.WriteLine("Visa卡號類別 測試 OK");

            Assert.AreEqual(HiDealVisaCardType.Infinite,
                CreditCardPremiumManager.GetPiinlifeVisaCardType("5556667797654123"));

            Assert.AreEqual(HiDealVisaCardType.Platinum,
                CreditCardPremiumManager.GetPiinlifeVisaCardType("55566688444422"));

            Console.WriteLine("Visa卡號類別 卡號符合無限卡也符合白金卡號，應該回傳無限卡別 OK");
        }

        [Test]
        public void EmptyCardNumTest()
        {
            Assert.AreEqual(HiDealVisaCardType.Normal,
                CreditCardPremiumManager.GetPiinlifeVisaCardType(""));
            Console.WriteLine("CardNo 為空也傳回一般卡");

            Assert.AreEqual(HiDealVisaCardType.Normal,
                CreditCardPremiumManager.GetPiinlifeVisaCardType(null));
            Console.WriteLine("CardNo 為NULL也傳回一般卡");

            Assert.AreEqual(HiDealVisaCardType.Normal,
                CreditCardPremiumManager.GetPiinlifeVisaCardType("1"));
            Console.WriteLine("CardNo 很短也傳回一般卡");
        }
    }
}

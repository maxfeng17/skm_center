﻿using System;
using System.Linq;
using Autofac;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Moq;
using NUnit.Framework;

namespace LunchKingSite.Tests.BizLogic.Component
{
    [TestFixture]
    public class MemberUtilityCoreCoreFixture
    {
        [SetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();

            var cfgMock = new Mock<ISysConfProvider>();
            cfgMock.Setup(t => t.DEAfilterApiKey).Returns("f36798b48247730e920f4980be963576");

            Mock<ISystemProvider> sysMock = new Mock<ISystemProvider>();
            sysMock.Setup(t => t.SystemCodeGetListByCodeGroup(It.IsAny<string>())).Returns(delegate(string codeGroup)
            {
                SystemCodeCollection scCol = new SystemCodeCollection();
                scCol.Add(new SystemCode { CodeName = "dae.com" });
                scCol.Add(new SystemCode { CodeName = "dae.net" });
                scCol.Add(new SystemCode { CodeName = "dae.org" });
                return scCol;
            });

            builder.RegisterInstance(new Mock<IMemberProvider>().Object).As<IMemberProvider>();
            builder.RegisterInstance(new Mock<IOrderProvider>().Object).As<IOrderProvider>();
            builder.RegisterInstance(new Mock<ISellerProvider>().Object).As<ISellerProvider>();
            builder.RegisterInstance(new Mock<IPponProvider>().Object).As<IPponProvider>();
            builder.RegisterInstance(new Mock<IHiDealProvider>().Object).As<IHiDealProvider>();
            builder.RegisterInstance(cfgMock.Object).As<ISysConfProvider>();
            builder.RegisterInstance(sysMock.Object).As<ISystemProvider>();

            var build = builder.Build();
            ProviderFactory.ReInit(build.BeginLifetimeScope());
        }

        [Test]
        public void GetRealEmailTest_有1個點的測試()
        {
            Assert.AreEqual("xx@gmail.com", MemberUtilityCore.GetTrueEmail("x.x@gmail.com"));
        }
        
        [Test]
        public void GetRealEmailTest_有2個點的測試()
        {
            Assert.AreEqual("xxx@gmail.com", MemberUtilityCore.GetTrueEmail("x.x.x@gmail.com"));
        }

        [Test]
        public void GetRealEmailTest_點在名字尾巴不合法測試()
        {
            Assert.IsTrue(string.IsNullOrEmpty(MemberUtilityCore.GetTrueEmail("xx.@gmail.com")));
        }

        [Test]
        public void GetRealEmailTest_點在名字前不合法測試()
        {
            Assert.IsTrue(string.IsNullOrEmpty(MemberUtilityCore.GetTrueEmail(".xx@gmail.com")));
        }

        [Test]
        public void GetRealEmailTest_加符號測試()
        {
            Assert.AreEqual("xxx@gmail.com", MemberUtilityCore.GetTrueEmail("xxx+123@gmail.com"));         
        }

        [Test]
        public void GetRealEmailTest_點與加符號測試()
        {
            Assert.AreEqual("xxx@gmail.com", MemberUtilityCore.GetTrueEmail("x.x.x+123@gmail.com"));
        }

        [Test]
        public void GetRealEmailTest_網域大寫測試()
        {
            Assert.AreEqual("xx@GMAIL.Com", MemberUtilityCore.GetTrueEmail("x.x@GMAIL.Com"));
        }

        [Test]
        public void GetRealEmailTest_hotmail加符號()
        {
            Assert.AreEqual("xxx@hotmail.com", MemberUtilityCore.GetTrueEmail("xxx+123@hotmail.com"));
        }

        [Test]
        public void GetRealEmailTest_hotmail加符號大小寫測試()
        {
            Assert.AreEqual("xxx@HOTMAIL.COM", MemberUtilityCore.GetTrueEmail("xxx+123@HOTMAIL.COM"));
        }

        [Test]
        public void GetRealEmailSpeedTest()
        {
            //RegExRule需要暖身，先跑過底下算時間會比較準
            RegExRules.CheckEmail("uni2tw@hotmail");
            string sql = "select top 1000 * from member";
            string[] userNames = DB.Query().ExecuteAsCollection<MemberCollection>(sql).Select(t=>t.UserName).ToArray();

            using (var watch = new StopwatchLog())
            {
                watch.SetLog(new StopwatchLog.ConsoleLogWriter());
                for (int i = 0; i < 4; i++)
                {
                    foreach (string userName in userNames)
                    {
                        if (RegExRules.CheckEmail(userName))
                        {
                            MemberUtilityCore.GetTrueEmail(userName);
                        }
                    }
                }
            }
        }
    }
}

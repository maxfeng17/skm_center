﻿using System;
using System.Collections.Generic;
using Autofac;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Moq;
using NUnit.Framework;

namespace LunchKingSite.Tests.BizLogic.Component
{
    [TestFixture]
    public class ViewPponStoreManagerFixture
    {
        private readonly Guid Bid1 = Guid.NewGuid();
        private readonly Guid Bid2 = Guid.NewGuid();
        private readonly Guid Bid3 = Guid.NewGuid();
        private readonly Guid Bid4 = Guid.NewGuid();
        private readonly Guid Bid5 = Guid.NewGuid();

        [SetUp]
        public void Setup()
        {
            var pp = new Mock<IPponProvider>();
            pp.Setup(
                t => t.ViewPponStoreGetListByDayWithNoLock(It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .Returns(delegate(DateTime sDate, DateTime eDate)
                {
                    ViewPponStoreCollection result = new ViewPponStoreCollection();
                    // Bid 1
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid1,
                        CityName = "台北市",
                        TownshipName = "中正區"
                    });
                    // Bid 2
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid2,
                        CityName = "台北市",
                        TownshipName = "中山區"
                    });
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid2,
                        CityName = "台北市",
                        TownshipName = "中山區"
                    });
                    // Bid 3
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid3,
                        CityName = "台北市",
                        TownshipName = "中山區"
                    });
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid3,
                        CityName = "台北市",
                        TownshipName = "中正區"
                    });
                    // Bid 4
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid4,
                        CityName = "台北市",
                        TownshipName = "中山區"
                    });
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid4,
                        CityName = "新北市",
                        TownshipName = "新店區"
                    });
                    // Bid 5
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid5,
                        CityName = "嘉義市",
                        TownshipName = "西區"
                    });
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid5,
                        CityName = "高雄市",
                        TownshipName = "前金區"
                    });
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid5,
                        CityName = "桃園縣",
                        TownshipName = "中壢市"
                    });
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid5,
                        CityName = "新竹市",
                        TownshipName = "東區"
                    });
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid5,
                        CityName = "台北市",
                        TownshipName = "中正區"
                    });
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid5,
                        CityName = "新北市",
                        TownshipName = "板橋區"
                    });
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid5,
                        CityName = "台中市",
                        TownshipName = "中區"
                    });
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid5,
                        CityName = "台南市",
                        TownshipName = "中西區"
                    });
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid5,
                        CityName = "桃園縣",
                        TownshipName = "桃園市"
                    });
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid5,
                        CityName = "高雄市",
                        TownshipName = "鳳山區"
                    });
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid5,
                        CityName = "台北市",
                        TownshipName = "中山區"
                    });

                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid5,
                        CityName = "新北市",
                        TownshipName = "三重區"
                    });
                    result.Add(new ViewPponStore
                    {
                        BusinessHourGuid = Bid5,
                        CityName = "台北市",
                        TownshipName = "松山區"
                    });

                    return result;
                });

            pp.Setup(
                t => t.ViewPponDealGetBidListByDay(It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .Returns(delegate(DateTime sDate, DateTime eDate)
                {
                    //回傳空的list ，不用索引
                    return new List<Guid>();
                });


            var builder = new ContainerBuilder();
            builder.RegisterInstance(pp.Object).As<IPponProvider>();
            
            var build = builder.Build();
            ProviderFactory.ReInit(build.BeginLifetimeScope());


            ViewPponStoreManager.ReloadDefaultManager();
        }

        
        [Test]
        public void GetPponStoresShortDescTest()
        {
            Assert.AreEqual(string.Empty, ViewPponStoreManager.DefaultManager.GetPponStoresShortDesc(Bid1));
            Assert.AreEqual("中山區", ViewPponStoreManager.DefaultManager.GetPponStoresShortDesc(Bid2));
            Assert.AreEqual("中山區、中正區", ViewPponStoreManager.DefaultManager.GetPponStoresShortDesc(Bid3));
            Assert.AreEqual("台北市中山區、新北市新店區", ViewPponStoreManager.DefaultManager.GetPponStoresShortDesc(Bid4));

            string trim36MaxChars = ViewPponStoreManager.DefaultManager.GetPponStoresShortDesc(Bid5);
            Assert.AreEqual(39, trim36MaxChars.Length);
            Assert.IsTrue(trim36MaxChars.EndsWith("..."));
        }

        [Test]
        public void GetPponStoreTownshipNameTest()
        {
            Assert.AreEqual("中正區", ViewPponStoreManager.DefaultManager.GetPponStoreTownshipName(Bid1));
            Assert.AreEqual("多分店", ViewPponStoreManager.DefaultManager.GetPponStoreTownshipName(Bid2));
            Assert.AreEqual("多分店", ViewPponStoreManager.DefaultManager.GetPponStoreTownshipName(Bid3));
            Assert.AreEqual("多分店", ViewPponStoreManager.DefaultManager.GetPponStoreTownshipName(Bid4));
            Assert.AreEqual("多分店", ViewPponStoreManager.DefaultManager.GetPponStoreTownshipName(Bid5));

        }
    }
}

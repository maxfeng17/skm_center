﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebApi.Core;
using NUnit.Framework;

namespace LunchKingSite.Tests.WebAPI
{
    [TestFixture]
    public class UrlBuilderFixture
    {
        [Test]
        public void AppendQueryTest()
        {
            {
                UriBuilder ub = new UriBuilder("http://localhost:17017/default.aspx?");
                ub.AppendQueryArgument("code", "1234");
                Assert.AreEqual(ub.ToString(), "http://localhost:17017/default.aspx?code=1234");
            }

            {
                UriBuilder ub = new UriBuilder("http://localhost:17017/default.aspx");
                ub.AppendQueryArgument("code", "1234");
                Assert.AreEqual(ub.ToString(), "http://localhost:17017/default.aspx?code=1234");
            }

            {
                UriBuilder ub = new UriBuilder("http://localhost:17017/default.aspx?name=joe");
                ub.AppendQueryArgument("code", "1234");
                Assert.AreEqual(ub.ToString(), "http://localhost:17017/default.aspx?name=joe&code=1234");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.RocketMQEntity;
using LunchKingSite.BizLogic.Component.RocketMQTrans;
using LunchKingSite.Core;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Component;
using Autofac.Integration.Web;
using Autofac;
using ons;

namespace LunchKingSite.Service
{
    public partial class SKMCenterService : ServiceBase
    {
        PushConsumer consumer;
        private System.Timers.Timer ThreadOverTimeOrderSendToAlibaba = new System.Timers.Timer();
        private static IOrderProvider op = null;
        public SKMCenterService()
        {
            InitializeComponent();
        }


        protected override void OnStart(string[] args)
        {
            // 配置您的账号，以下设置均可从控制台获取
            ONSFactoryProperty factoryInfo = new ONSFactoryProperty();
            // AccessKey 阿里云身份验证，在阿里云服务器管理控制台创建
            factoryInfo.setFactoryProperty(ONSFactoryProperty.AccessKey, ConfigurationManager.AppSettings["SKMCenterRocketMQAccessKey"]);
            // SecretKey 阿里云身份验证，在阿里云服务器管理控制台创建
            factoryInfo.setFactoryProperty(ONSFactoryProperty.SecretKey, ConfigurationManager.AppSettings["SKMCenterRocketMQSecretKey"]);
            // 您在控制台创建的 Group ID
            factoryInfo.setFactoryProperty(ONSFactoryProperty.ConsumerId, ConfigurationManager.AppSettings["SKMCenterRocketMQGroupId"]);
            // 您在控制台创建的 Topic
            factoryInfo.setFactoryProperty(ONSFactoryProperty.PublishTopics, ConfigurationManager.AppSettings["SKMCenterRocketMQTopic"]);
            // 设置 TCP 接入域名，进入控制台的实例管理页面的“获取接入点信息”区域查看
            factoryInfo.setFactoryProperty(ONSFactoryProperty.NAMESRV_ADDR, ConfigurationManager.AppSettings["SKMCenterRocketMQNameSrv"]);
            // 设置日志路径
            factoryInfo.setFactoryProperty(ONSFactoryProperty.LogPath, ConfigurationManager.AppSettings["SKMCenterRocketMQLogPath"]);

            // 创建消费者实例
            consumer = ONSFactory.getInstance().createPushConsumer(factoryInfo);

            // 订阅 Topics
            consumer.subscribe(factoryInfo.getPublishTopics(), "*", new SKMCenterMsgListener());

            // 启动客户端实例
            consumer.start();

            var builder = new ContainerBuilder();

            builder.Register<ISysConfProvider>(t => new AppConfSysConfProvider());
            builder.Register<IMemberProvider>(t => new SsBLL.SSMemberProvider());
            builder.Register<IMarketingProvider>(t => new SsBLL.SSMarketingProvider());
            builder.Register<ISellerProvider>(t => new SsBLL.SSSellerProvider());
            //
            builder.Register<ISkmEfProvider>(t => new SsBLL.Provider.SkmEfProvider());
            builder.Register<IOrderProvider>(t => new SsBLL.SSOrderProvider());
            builder.Register<IPponProvider>(t => new SsBLL.SSPponProvider());
            builder.Register<IChannelEventProvider>(t => new SsBLL.Provider.ChannelEventProvider());
            builder.Register<ISkmProvider>(t => new SsBLL.SkmProvider());
            builder.Register<IHumanProvider>(t => new SsBLL.Provider.SSHumanProvider());
            builder.Register<IItemProvider>(t => new SsBLL.SSItemProvider());
            builder.Register<ISystemProvider>(t => new SsBLL.SSSystemProvider());
            builder.Register<IExhibitionProvider>(t => new SsBLL.Provider.ExhibitionProvider());

            Autofac.IContainer container = builder.Build();
            ProviderFactory.ReInit(container.BeginLifetimeScope());

            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            //阿里巴巴取件逾時處理 計時器
            ThreadOverTimeOrderSendToAlibaba.AutoReset = true;
            ThreadOverTimeOrderSendToAlibaba.Elapsed += new System.Timers.ElapsedEventHandler(TimerForOverTimeOrderSendToAlibaba);
            ThreadOverTimeOrderSendToAlibaba.Interval = 24 * 60 * 60 * 1000;//一天跑一次
            ThreadOverTimeOrderSendToAlibaba.Start();

        }

        protected override void OnStop()
        {
            if (ThreadOverTimeOrderSendToAlibaba != null)
            {
                ThreadOverTimeOrderSendToAlibaba.Stop();
                ThreadOverTimeOrderSendToAlibaba.Dispose();
            }

            if (consumer != null)
            {
                consumer.shutdown();
            }

        }

        /// <summary>
        /// 阿里巴巴取件逾時處理 計時器
        /// </summary>
        /// <param name="State"></param>
        private void TimerForOverTimeOrderSendToAlibaba(object source, System.Timers.ElapsedEventArgs e)
        {
            try
            {

                OverTimeOrderSendToAlibaba();
            }
            catch (Exception ee)
            {
                //
            }

        }

        /// <summary>
        /// 阿里巴巴取件逾時處理
        /// </summary>
        private void OverTimeOrderSendToAlibaba()
        {


            var data = op.SKmOrderGetNotVerifiedQuantityForAlibaba();
            if (data.Rows.Count > 0)
            {
                List<BizLogic.Component.RocketMQEntity.InventoryChangeEntity> list = new List<BizLogic.Component.RocketMQEntity.InventoryChangeEntity>();
                var _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
                var externalDealList = new List<LunchKingSite.DataOrm.ExternalDeal>();

                foreach (DataRow row in data.Rows)
                {
                    list.Add(new BizLogic.Component.RocketMQEntity.InventoryChangeEntity()
                    {
                        bizSrcId = (row["bizSrcId"] == DBNull.Value) ? "" : (string)row["bizSrcId"],
                        bizSrcSubId = null,
                        bizSrcType = (row["bizSrcType"] == DBNull.Value) ? 0 : (int)row["bizSrcType"],
                        code = (row["code"] == DBNull.Value) ? "" : (string)row["code"],
                        entityId = (row["entityId"] == DBNull.Value) ? "" : (string)row["entityId"],
                        entityType = (row["entityType"] == DBNull.Value) ? 0 : (int)row["entityType"],
                        quantity = (row["quantity"] == DBNull.Value) ? 0 : (int)row["quantity"],
                        warehouseCode = (row["warehouseCode"] == DBNull.Value) ? "" : (string)row["warehouseCode"],
                        warehouseType = (row["warehouseType"] == DBNull.Value) ? 0 : (int)row["warehouseType"],
                    });
                    if (!externalDealList.Select(o => o.Guid).Contains((Guid)row["externalDealGuid"]))
                    {
                        externalDealList.Add(_pp.ExternalDealGet((Guid)row["externalDealGuid"]));
                    }
                }

                //呼叫阿里巴巴補庫存
                SKMCenterRocketMQTrans.InventoryAddAdjust(list);

                externalDealList.ForEach(o => o.NotVerifiedFlag = false);
                foreach (var deal in externalDealList)
                {
                    _pp.ExternalDealSet(deal);
                }

            }
            else
            {
                //
            }



        }
    }

    public class LoaderConfig
    {
        public static ContainerProvider Register()
        {
            ContainerBuilder builder = new ContainerBuilder();

            new LunchKingSite.Core.CoreModule().RegisterWithContainer(builder);
            new LunchKingSite.BizLogic.BizLogicModule().RegisterWithContainer(builder);
            new LunchKingSite.Mongo.MongoModule().RegisterWithContainer(builder);
            new LunchKingSite.SsBLL.SsBllModule().RegisterWithContainer(builder);

            Autofac.IContainer container = builder.Build();

            new LunchKingSite.Core.CoreModule().Initialize(container);
            new LunchKingSite.BizLogic.BizLogicModule().Initialize(container);
            new LunchKingSite.Mongo.MongoModule().Initialize(container);
            new LunchKingSite.SsBLL.SsBllModule().Initialize(container);

            return new ContainerProvider(container);
        }
    }

    public class SKMCenterMsgListener : MessageListener
    {
        public SKMCenterMsgListener()
        {

        }

        public override ons.Action consume(Message value, ConsumeContext context)
        {
            //服務後續要做的再補在這
            //Byte[] text = Encoding.Default.GetBytes(value.getBody());
            //Console.WriteLine(value.getTag());
            //Console.WriteLine(Encoding.UTF8.GetString(text));

            MessageLog(value);
            return ons.Action.CommitMessage;
        }

        /// <summary>
        /// 技術債....先把資訊記錄在檔案中，再來想用什麼方法存
        /// </summary>
        /// <param name="value"></param>
        private void MessageLog(Message value)
        {
            string tag = value.getTag();

            //Byte[] text = Encoding.Default.GetBytes(value.getBody());

            string body = value.getBody();

            try
            {
                Exception e = null;

                int failedTimes = 0;

                SKMCenterRocketMQActionResultEntity actionResult = RetryMessage(value, ref failedTimes, ref e);

                //執行相關結果
                //SKMCenterRocketMQActionResultEntity actionResult = SKMCenterListeningEventUtility.ReciveDataAndAction(value);
                //記錄監聽的訊息
                LogInfoTest(tag, body, actionResult.Message);
            }
            catch (Exception ex)
            {
                Thread.Sleep(1000);
                LogInfoTest(tag, body, ex.ToString());
            }

        }

        private SKMCenterRocketMQActionResultEntity RetryMessage(Message value, ref int failedTimes, ref Exception ex)
        {
            if (failedTimes > 3)
            {
                throw ex;
            }
            else
            {
                try
                {
                    var actionResult = SKMCenterListeningEventUtility.ReciveDataAndAction(value);

                    return actionResult;
                }
                catch (Exception e)
                {
                    failedTimes++;

                    Thread.Sleep(1000);

                    return RetryMessage(value, ref failedTimes, ref e);
                }
            }

        }

        public static void LogInfoTest(string tag, string body, string result = "")
        {
            Random rnd = new Random();

            int rand = rnd.Next(1000, 2000);

            string randStr = rand.ToString().Substring(1);

            string fileName = string.Format("{0}-{1}{2}.txt", DateTime.Now.ToString("yyyyMMddhhmmss"), randStr, tag);

            string filePath = string.Format("{0}//{1}", ConfigurationManager.AppSettings["SKMCenterRocketMQMessageLogPath"], fileName);

            FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate);

            StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);

            sw.WriteLine(tag);
            sw.WriteLine(body);
            sw.WriteLine(result);

            sw.Close();

            fs.Close();

            sw.Dispose();
            fs.Dispose();
        }

    }




}

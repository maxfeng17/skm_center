﻿using System.ComponentModel.Composition;
using System.Reflection;
using Autofac;
using Vodka.Container;

namespace LunchKingSite.WebLib
{
    [Export(typeof(IModuleRegistrar))]
    public class WebLibModule : IModuleRegistrar
    {
        public void RegisterWithContainer(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).Where(t => !string.IsNullOrEmpty(t.Namespace) && t.Namespace.EndsWith("Presenters")).AsSelf().InstancePerDependency();
        }

        public void Initialize(IContainer container)
        {
        }
    }
}

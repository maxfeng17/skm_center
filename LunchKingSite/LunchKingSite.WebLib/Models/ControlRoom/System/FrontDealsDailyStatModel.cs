﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Models.ControlRoom.System
{
    public class FrontDealsDailyStatModel
    {
        public DateTime? FrontDealsStatStart { get; set; }
        public DateTime? FrontDealsStatEnd { get; set; }
    }

    public class FrontDealsDailyStatInfo
    {
        public int UserId { get; set; }

        /// <summary>
        /// A/B群
        /// </summary>
        public string ABGroup { get; set; }

        /// <summary>
        /// 裝置的OS
        /// </summary>
        public string OSDevice { get; set; }

        /// <summary>
        /// 訂單數
        /// </summary>
        public int OrderCount { get; set; }

        /// <summary>
        /// 購買金額
        /// </summary>
        public decimal OrderTurnoverTotal { get; set; }

        /// <summary>
        /// 購買金額毛利額
        /// </summary>
        public decimal OrderGrossProfitTotal { get; set; }

        /// <summary>
        /// 首頁訪問深度
        /// </summary>
        public int FrontpageViewCount { get; set; }

        /// <summary>
        /// 首頁訪問次數
        /// </summary>
        public int FrontpageViewTotal { get; set; }
    }
}

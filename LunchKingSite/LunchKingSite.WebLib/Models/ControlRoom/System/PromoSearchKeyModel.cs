﻿using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Models.ControlRoom.System
{
    public class PromoSearchKeyModel
    {
        public class SortedRecommendKeyword
        {
            public string Keyword { get; set; }
            public int Count { get; set; }
            public bool IsExist { get; set; }
        }

        public List<PromoSearchKeyItem> PromoSearchKeyList { get; set; }
        public List<SortedRecommendKeyword> MatchDeals { get; set; }
    }

    public class PromoSearchKeyItem {
        public int Id { set; get; }
        public int? Seq { set; get; }
        public string Keyword { set; get; }
        public string PicUrl { set; get; }
        public int MatchDealsCount { set; get; }
        public int DayAverageHitCountAtLastSevenDays { set; get; }
        public bool IsEnable { set; get; }
        public string CreateUserName { set; get; }
        public string ModifyUserName { set; get; }
        public DateTime CreateTime { set; get; }
        public DateTime? ModifyTime { set; get; }

        public PromoSearchKeyItem(PromoSearchKey key) {
            this.Id = key.Id;
            this.Seq = key.Seq;
            this.Keyword = key.KeyWord;
            if (string.IsNullOrEmpty(key.PicUrl))
            {
                this.PicUrl = ImageFacade.NoAppPic;
            }
            else
            {
                this.PicUrl = key.PicUrl;
            } 
            this.IsEnable = key.IsEnable;
            this.CreateTime = key.CreateTime;
            this.ModifyTime = key.ModifyTime;
            var createMember = MemberFacade.GetMember(key.CreateUserId);
            this.CreateUserName = string.Format("{1}{0}", createMember.FirstName, createMember.LastName);

            if (key.ModifyUserDi != null) { 
                var ModifyMember = MemberFacade.GetMember((int)key.ModifyUserDi);
                if (ModifyMember.IsLoaded) {
                    this.ModifyUserName = string.Format("{1}{0}", ModifyMember.FirstName, ModifyMember.LastName);
                }
            }
        }
    }
}

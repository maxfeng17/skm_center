﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Models.ControlRoom.Seller
{
    public class SellerTreeModel
    {
        public Guid SellerGuid { get; set; }
        public IEnumerable<SellerTree> SellerTrees { get; set; } 
    }

    public class SellerTree:SellerNode
    {
        public IEnumerable<SellerNode> Children { get; set; } 
    }

    public class SellerNode
    {
        public Guid Id { get; set; }
        public string Text { get; set; }
    }
}

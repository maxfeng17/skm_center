﻿using LunchKingSite.DataOrm;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Models.ControlRoom.Seller
{
    public class JsTreeModel
    {
        [JsonProperty(PropertyName = "id")]
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "parent")]
        public string Parent { get; set; }

        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

        [JsonProperty(PropertyName = "state")]
        public JsTreeStataModel State { get; set; }

        public JsTreeModel()
        {
            State = new JsTreeStataModel() { Opened = true };
        }
    }

    public class JsTreeStataModel
    {
        [JsonProperty(PropertyName = "opened")]
        public bool Opened { get; set; }

        [JsonProperty(PropertyName = "disabled")]
        public bool Disabled { get; set; }

        [JsonProperty(PropertyName = "selected")]
        public bool Selected { get; set; }
    }
    
    public class OdmSellerRoleModel
    {

        public List<SelectListItem> OdmList { get; set; }
        public Member Memeber { get; set; }
        public OdmSellerRoleModel()
        {
            OdmList = new List<SelectListItem>();
            Memeber = new Member();
        }
    }
}

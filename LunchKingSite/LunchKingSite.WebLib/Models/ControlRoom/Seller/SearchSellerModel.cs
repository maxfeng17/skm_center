﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Models.ControlRoom.Seller
{
    public class SearchSellerModel
    {
        public string SearchOption { get; set; }
        public string SearchExpression { get; set; }
        public bool Searched { get; set; }
        public IEnumerable<SellerStoreInfo> SearchResult { get; set; }
    }

    public class SellerStoreInfo 
    {
        public Guid SellerGuid { get; set; }
        public string SellerName { get; set; }
        public Guid ParentSellerGuid { get; set; }
        public string ParentSellerName { get; set; }
        public bool? IsTransfered { get; set; }
    }

    public class TransferModel
    {
        public bool StoreGetByPponDeal { get; set; }
        public string SellerGuids { get; set; }
    }
}

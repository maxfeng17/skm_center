﻿using LunchKingSite.Core;
using LunchKingSite.BizLogic.Model.BalanceSheet;
using System;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Models.ControlRoom.Finance
{
    public class ToHouseDealPaymentManageModel
    {
        public Guid DealGuid { get; set; }
        public int DealId { get; set; }
        public string DealContent { get; set; }
        public bool IsBalanceSheetCreate { get; set; }
        public bool IsAllowanceAmountEdit { get; set; }
        public bool IsVendorPaymentChangeEdit { get; set; }
        public IEnumerable<VendorPaymentChangeInfo> VendorPaymentChangeInfos { get; set; }
        public bool? IsModificationSuccess { get; set; }
        public string ReturnMessage { get; set; }
        public IEnumerable<BalanceSheetInfo> BalanceSheetInfos { get; set; }
        public bool IsInBalanceTimeRange { get; set; }
    }

    public class VendorPaymentChangeInfo
    {
        public int PaymentChangeId { get; set; }
        public string Reason { get; set; }
        public int Amount { get; set; }
        public int PromotionValue { get; set; }
        public int AllowanceAmount { get; set; }
        public int OrderCount { get; set; }
        public string OrderIdInfo { get; set; }
        public string Action { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? ExpireTime { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreateId { get; set; }
        public Guid BusinessHourId { get; set; }
        public bool IsFreeze { get; set; }
        public int? BalanceSheetId { get; set; }
        public bool IsEditable { get; set; }
    }

    public class PaymentChangeModel
    {
        public Guid DealGuid { get; set; }
        public int? PaymentChangeId { get; set; }
        public string Reason { get; set; }
        public int? Amount { get; set; }
        public int? PromotionValue { get; set; }
        public int? AllowanceAmount { get; set; }
        public int OrderCount { get; set; }
        public string OrderIdInfo { get; set; }
        public string Action { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? ExpireTime { get; set; }
    }

    public class BalanceSheetsLumpSumModel
    {
        public IEnumerable<BalanceSheetsDeliverModel> BalanceSheetLumpSumInfo { get; set; }
    }
    public class BalanceSheetInfo
    {
        public Guid ProductGuid { get; set; }
        public int BalanceSheeetId { get; set; }
        public string BalanceSheetFrequencyDesc { get; set; }
        public DateTime IntervalEnd { get; set; }
        public bool IsConfirmedReadyToPay { get; set; }
    }

    public class ChangeEstAmountModel
    {
        public Guid DealGuid { get; set; }
        public int BalanceSheetId { get; set; }
        public string PaymentChangeIds { get; set; }
    }
}

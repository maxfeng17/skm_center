﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using System;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Models.ControlRoom.Finance
{
    public class ToHouseDealPaymentAuditModel
    {
        public string QueryDateOption { get; set; }
        public DateTime? QueryDateS { get; set; }
        public DateTime? QueryDateE { get; set; }
        public string QueryOption { get; set; }
        public string QueryKeyWord { get; set; }
        public string OrderByColumn { get; set; }
        public RemittanceType? RemittanceType { get; set; }
        public bool HasVendorPaymentChange { get; set; }
        public bool HasNoPartiallyPaymentDate { get; set; }
        public bool HasNoFinalBalanceSheetDate { get; set; }
        public bool HasNoBalanceSheetCreateDate { get; set; }
        public bool QueryRequest { get; set; }
        public bool IsFinishReturnAndExchangeDateEdit { get; set; }
        public bool IsRelatedFinanceDateEdit { get; set; }
        public PagerList<DealPaymentInfo> DealPaymentInfos { get; set; }  
    }

    public class DealPaymentInfo
    {
        public Guid DealGuid { get; set; }
        public int DealId { get; set; }
        public string DealName { get; set; }
        public string DealContent { get; set; }
        public RemittanceType RemittanceType { get; set; }
        public DateTime DealOrderStartTime { get; set; }
        public DateTime DealOrderEndTime { get; set; }
        public string SalesName { get; set; }
        public DateTime? ShippedDate { get; set; }
        public DateTime? FinalBalanceSheetDate { get; set; }
        public string PartiallyPaymentDate { get; set; }
        public DateTime? BalanceSheetCreateDate { get; set; }
        public DateTime? BillGetDate { get; set; }
        public bool IsTax { get; set; }
        public string BillNumber { get; set; }
        public string BillCompanyId { get; set; }
        public string CompanyBossName { get; set; }
        public string AccountName { get; set; }
        public string AccountId { get; set; }
        public string AccountNo { get; set; }
        public string BankCode { get; set; }
        public string BranchCode { get; set; }
        public string CompanyEmail { get; set; }
        public string SignCompanyId { get; set; }
        public string SignCompanyName { get; set; }
        public string VendorPaymentChangeAmountDesc { get; set; }
        public bool HasBalanceSheet { get; set; }
        public bool IsDealEstablished { get; set; }
        public bool IsFreeze { get; set; }
        public string BalanceSheetCompleteCnt { get; set; }
        public bool EnableFreeze { get; set; }

        public class Columns
        {
            public const string DealOrderEndTime = "DealOrderEndTime";
            public const string SignCompanyName = "SignCompanyName";
            public static List<string> Names { get; set; }
            static Columns()
            {
                Names = new List<string>();
                Names.Add(DealOrderEndTime);
                Names.Add(SignCompanyName);
            }
        }
    }

    public class UpdateDateRequestModel
    {
        public UpdateDateOption UpdateDateOption { get; set; }
        public string UpdateDealInfo { get; set; }
        public DateTime? UpdatePartiallyPaymentDate { get; set; }
    }

    public enum UpdateDateOption
    {
        SetPartiallyPaymentDate,
        SetFinalBalanceSheetDate,
        SetBalanceSheetCreateDate,
        SetPartiallyPaymentDateNull,
        SetFinalBalanceSheetDateNull,
        SetBalanceSheetCreateDateNull
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.ControlRoom.Finance
{
    class PcpPointTransactionManageModel
    {
    }

    public class PcpTransactonOrderData
    {
        public string OrderId { get; set; }
        public string orderDate { get; set; }
        public string expireDate { get; set; }
        public string memo { get; set; }
        public string pointIn { get; set; }
        public string pointOut { get; set; }
        public string total { get; set; }
        public string creator { get; set; }
        public string description { get; set; }
        //public int depositId { get; set; }
        //public int withdrawalId { get; set; }
    }

    public class PcpSuperBonusData
    {
        public string OrderId { get; set; }
        public string membershipCardId { get; set; }
        public string transactionDate { get; set; }
        public string memo { get; set; }
        public string pointIn { get; set; }
        public string pointOut { get; set; }
        public string total { get; set; }
        public string creator { get; set; }
        //public int depositId { get; set; }
        //public int withdrawalId { get; set; }
    }

    public class PcpSellerTriplicateInvoiceData
    {
        public string invoiceTitle { get; set; }
        public string ivoiceCompanyId { get; set; }
        public string invoiceName { get; set; }
        public string invoiceAddress { get; set; }
    }


}

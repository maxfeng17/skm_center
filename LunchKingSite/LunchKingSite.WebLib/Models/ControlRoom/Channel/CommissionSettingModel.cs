﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;

namespace LunchKingSite.WebLib.Models.ControlRoom.Channel
{
    public class CommissionRuleModel
    {
        public ChannelSource Source { get; set; }
        public AgentChannel AgentChannel { get; set; }
        public string CouponDefaultCode { get; set; }
        public decimal CouponDefaultValue { get; set; }
        public string DeliveryDefaultCode { get; set; }
        public decimal DeliveryDefaultValue { get; set; }
        public List<SystemCode> DealTypeSystemCode { get; set; }
    }

    public class CommissionDefaultSetting
    {
        [JsonProperty("source")]
        public int Source { get; set; }
        [JsonProperty("couponCode")]
        public string CouponCode { get; set; }
        [JsonProperty("couponVal")]
        public decimal CouponVal { get; set; }
        [JsonProperty("deliveryCode")]
        public string DeliveryCode { get; set; }
        [JsonProperty("deliveryVal")]
        public decimal DeliveryVal { get; set; }
    }

    public class CommissionRuleListModel
    {
        [JsonProperty("source")]
        public int Source { get; set; }
        [JsonProperty("id")]
        public int Id { get; set; }
    }

    public class RuleData
    {
        public RuleData(ChannelCommissionRule rule)
        {
            var sc = SystemCodeManager.GetDealType().ToList().FirstOrDefault(x => x.CodeId == rule.DealType) ??
                     new DataOrm.SystemCode();

            DealType = sc.CodeName;
            Id = rule.Id;
            Keyword = rule.KeyWord;
            GrossMarginLimit = rule.GrossMarginLimit;
            ReturnCode = rule.ReturnCode;
            ModifyTime = rule.ModifyTime.ToString("yyyy/MM/dd HH:mm:ss");
        }

        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("dealType")]
        public string DealType { get; set; }
        [JsonProperty("keyword")]
        public string Keyword { get; set; }
        [JsonProperty("grossMarginLimit")]
        public decimal GrossMarginLimit { get; set; }
        [JsonProperty("returnCode")]
        public string ReturnCode { get; set; }
        [JsonProperty("modifyTime")]
        public string ModifyTime { get; set; }
    }

    public class AddRuleModel
    {
        [JsonProperty("source")]
        public int Source { get; set; }
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("dealType")]
        public int DealType { get; set; }
        [JsonProperty("keyword")]
        public string Keyword { get; set; }
        [JsonProperty("grossMarginLimit")]
        public decimal GrossMarginLimit { get; set; }
        [JsonProperty("returnCode")]
        public string ReturnCode { get; set; }
    }

    public class RuleDelModel
    {
        [JsonProperty("source")]
        public int Source { get; set; }
        [JsonProperty("id")]
        public int Id { get; set; }
    }

    public class ChannelCommissionTestModel
    {
        [JsonProperty("startDate")]
        public DateTime StartDate { get; set; }
        [JsonProperty("endDate")]
        public DateTime EndDate { get; set; }
    }
}

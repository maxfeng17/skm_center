﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;

namespace LunchKingSite.WebLib.Models.ControlRoom.Channel
{
    public class CommissionListSettingModel
    {
        public int Source { get; set; }
        public List<CommissionDetailList> BlackList { get; set; }
        public List<CommissionDetailList> WhiteList { get; set; }
    }

    public class CommissionDetailList
    {
        public int ID { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public string Href { get; set; }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;

namespace LunchKingSite.WebLib.Models.ControlRoom.Channel
{
    public class CommissionSpecialSettingModel
    {
        public int UniqueId { get; set; }
        public string ItemName { get; set; }
        public string OriCommission { get; set; }
        public decimal ItemPrice { get; set; }
        public string SpecialCost { get; set; }
        public string SpecialCommission { get; set; }
    }

}

﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Models.ControlRoom.Order
{
    public class ExchangeLogModel
    {
        public string OrderId { get; set; }
        public DateTime ApplicationTime { get; set; }
        public string Reason { get; set; }
        public int? VendorProgressStatus { get; set; }
        public string VendorMemo { get; set; }
        public int ExchangeLogId { get; set; }
        public int? OrderShipId { get; set; }
        public bool Modifyable { get; set; }
    }

    public class ExchangeMessageModel : ExchangeLogModel
    {
        public string Message { get; set; }
    }

    public class ExchangeShipInfoModel : ExchangeLogModel
    {
        public string ShipNo { get; set; }
        public int? ShipCompanyId { get; set; }
        public DateTime? ShipTime { get; set; }
        public string ShipMemo { get; set; }
        public List<ShipCompany> ShipCompanyInfos { get; set; }
    }
}

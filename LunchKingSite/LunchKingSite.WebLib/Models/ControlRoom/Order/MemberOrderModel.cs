﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.BizLogic.Models.CustomerService;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;

namespace LunchKingSite.WebLib.Models.ControlRoom.Order
{
    #region enum

    /// <summary>
    /// 發票編輯模式
    /// </summary>
    public enum EinvoiceEditMode
    {
        /// <summary>
        /// 唯讀
        /// </summary>
        ReadOnly,
        /// <summary>
        /// 手機/自然人憑證
        /// </summary>
        DuplicateCarrier,
        /// <summary>
        /// 紙本
        /// </summary>
        DuplicatePaper,
        /// <summary>
        /// 會員載具
        /// </summary>
        DuplicateMember,
        /// <summary>
        /// 三聯
        /// </summary>
        Triplicate,
        /// <summary>
        /// 捐贈
        /// </summary>
        Donate
    }

    #endregion

    /// <summary>
    /// 會員訂單
    /// </summary>
    public class MemberOrderModel
    {
        /// <summary>
        /// 訂單Guid
        /// </summary>
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// 檔次類型 憑證/購物商品
        /// </summary>
        public bool IsCoupon { get; set; }
        /// <summary>
        /// 是否為超取訂單
        /// </summary>
        public bool IsIsp { get; set; }
        /// <summary>
        /// 是否為成套
        /// </summary>
        public bool IsGroupCoupon { get; set; }
        /// <summary>
        /// 是否顯示退貨鈴
        /// </summary>
        public bool IsShowRefundRingIcon { get; set; }
        /// <summary>
        /// 案件編號
        /// </summary>
        public string ServiceNo { get; set; }

        /// <summary>
        /// 訂單基本資料
        /// </summary>
        public OrderInfo OrderInfo { get; set; }
        /// <summary>
        /// 檔次與商家基本資料
        /// </summary>
        public DealAndVendorInfo DealAndVendorInfo { get; set; }
        /// <summary>
        /// 訂購人基本資料
        /// </summary>
        public ReceiverInfo ReceiverInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public IViewPponDeal ViewPponDeal { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ViewOrderMemberBuildingSeller Vombs { get; set; }

        public MemberOrderModel()
        {
            OrderInfo = new OrderInfo();
            DealAndVendorInfo = new DealAndVendorInfo();
            ReceiverInfo = new ReceiverInfo();
            ServiceNo = string.Empty;
        }
    }

    /// <summary>
    /// 訂單基本資料
    /// </summary>
    public class OrderInfo
    {
        /// <summary>
        /// 訂單Guid
        /// </summary>
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 訂購時間
        /// </summary>
        public string OrderCreateTime { get; set; }
        /// <summary>
        /// 檔次BID
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 檔次
        /// </summary>
        public string DealName { get; set; }
        /// <summary>
        /// 檔次NavigateUrl
        /// </summary>
        public string DealNavigateUrl { get; set; }
        /// <summary>
        /// 訂購價
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// 訂單類型 24H、超商取貨
        /// </summary>
        public List<string> DealNameTags { get; set; }
        /// <summary>
        /// 付款狀態
        /// </summary>
        public string PayStatus { get; set; }
        /// <summary>
        /// 付款日
        /// </summary>
        public String PayDate { get; set; }
        /// <summary>
        /// 訂單狀態
        /// </summary>
        public Dictionary<int, string> OrderStatus { get; set; }
        /// <summary>
        /// 付款明細
        /// </summary>
        public List<string> PaymentDetail { get; set; }

        public OrderInfo()
        {
            DealNameTags = new List<string>();
            PaymentDetail = new List<string>();
        }
    }

    /// <summary>
    /// 付款明細
    /// </summary>
    public class PaymentDetail
    {
        /// <summary>
        /// 信用卡
        /// </summary>
        public int CreditCard { get; set; }
        /// <summary>
        /// Payeasy 點數
        /// </summary>
        public int Pcash { get; set; }
        /// <summary>
        /// 紅利金金額
        /// </summary>
        public int Bcash { get; set; }
        /// <summary>
        /// 購物金金額
        /// </summary>
        public int Scash { get; set; }
        /// <summary>
        /// ATM 金額
        /// </summary>
        public int Atm { get; set; }
        /// <summary>
        /// 折價券名稱
        /// </summary>
        public string DiscountName { get; set; }
        /// <summary>
        /// 折價券金額
        /// </summary>
        public int DiscountAmount { get; set; }
        /// <summary>
        /// API付款
        /// </summary>
        public int Lcash { get; set; }
        /// <summary>
        /// 第三方支付方式
        /// </summary>
        public ThirdPartyPayment ThirdPartyPayment { get; set; }
        /// <summary>
        /// 第三方支付金額
        /// </summary>
        public int Tcash { get; set; }
        /// <summary>
        /// 全家付款取貨金額
        /// </summary>
        public int Familycash { get; set; }
        /// <summary>
        /// 7-11 付款取貨金額
        /// </summary>
        public int Sevencash { get; set; }
        /// <summary>
        /// 分期 0:為不分期
        /// </summary>
        public int Installment { get; set; }
        /// <summary>
        /// 行動支付別
        /// </summary>
        public string MobilePayType { get; set; }
    }

    /// <summary>
    /// 檔次與商家基本資料
    /// </summary>
    public class DealAndVendorInfo
    {
        /// <summary>
        /// 檔號
        /// </summary>
        public int DealUniqueId { get; set; }
        /// <summary>
        /// 好康單價
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// 上檔日-起
        /// </summary>
        public string BusinessHourOrderTimeS { get; set; }
        /// <summary>
        /// 上檔日-訖
        /// </summary>
        public string BusinessHourOrderTimeE { get; set; }
        /// <summary>
        /// 憑證優惠期限/宅配配送日期-起
        /// </summary>
        public string BusinessHourDeliverTimeS { get; set; }
        /// <summary>
        /// 憑證優惠期限/宅配配送日期-訖
        /// </summary>
        public string BusinessHourDeliverTimeE { get; set; }

        public string SellerName { get; set; }

        public string SellerNavigateUrl { get; set; }
        /// <summary>
        /// 憑證分店名稱
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 憑證分店NavigateUrl
        /// </summary>
        public string StoreNavigateUrl { get; set; }
        /// <summary>
        /// 店家到期資訊
        /// </summary>
        public string StoreExpireInfo { get; set; }
        /// <summary>
        /// 賣家電話
        /// </summary>
        public string VendorTel { get; set; }
        /// <summary>
        /// 賣家傳真
        /// </summary>
        public string VendorFax { get; set; }
    }

    /// <summary>
    /// 訂購人基本資料
    /// </summary>
    public class ReceiverInfo
    {
        /// <summary>
        /// 收件人姓名
        /// </summary>
        public string ReceiverName { get; set; }
        /// <summary>
        /// 買家NavigateUrl
        /// </summary>
        public string BuyerNavigateUrl { get; set; }
        /// <summary>
        /// 性別
        /// </summary>
        public int Gender { get; set; }
        /// <summary>
        /// 買家電話
        /// </summary>
        public string BuyerTel { get; set; }
        /// <summary>
        /// 買家手機
        /// </summary>
        public string BuyerMobilePhone { get; set; }
        /// <summary>
        /// 收件地址
        /// </summary>
        public string ReceiverAddress { get; set; }
    }

    /// <summary>
    /// 出貨資訊
    /// </summary>
    public class ProductShippingInfo
    {
        /// <summary>
        /// 出貨單流水號
        /// </summary>
        public int OrderShipId { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public string CreateDate { get; set; }
        /// <summary>
        /// 出貨狀態
        /// </summary>
        public string ShippingStatusDesc { get; set; }
        /// <summary>
        /// 出貨方式
        /// </summary>
        public string ShippingType { get; set; }
        /// <summary>
        /// 物流公司Id
        /// </summary>
        public int ShippingCompanyId { get; set; }
        /// <summary>
        /// 物流公司
        /// </summary>
        public string ShippingCompanyName { get; set; }
        /// <summary>
        /// 超商門市
        /// </summary>
        public string IspStoreName { get; set; }
        /// <summary>
        /// 物流編號
        /// </summary>
        public string ShippingNo { get; set; }
        /// <summary>
        /// 出貨備註
        /// </summary>
        public string ShippingMemo { get; set; }
        /// <summary>
        /// 實際出貨日
        /// </summary>
        public string ActualShippingDate { get; set; }
        /// <summary>
        /// 最後修改日
        /// </summary>
        public string OrderShipModifyTime { get; set; }
        /// <summary>
        /// 最後修改日
        /// </summary>
        public string OrderShipModifyId { get; set; }
        /// <summary>
        /// 預計出貨日
        /// </summary>
        public string LastShippingDate { get; set; }
    }

    /// <summary>
    /// 退貨資訊
    /// </summary>
    public class ReturnProductInfo : ExchangeProductInfo
    {
        /// <summary>
        /// 退貨進版
        /// </summary>
        public string ReturnSchedule { get; set; }
        /// <summary>
        /// 完成退貨
        /// </summary>
        public string ReturnedItemSpec { get; set; }
        /// <summary>
        /// 折讓單狀態
        /// </summary>
        public string AllowanceStatus { get; set; }
    }

    /// <summary>
    /// 退換貨資訊
    /// </summary>
    public class ExchangeProductInfo
    {
        /// <summary>
        /// 申請時間
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 原因
        /// </summary>
        public string Reason { get; set; }
        /// <summary>
        /// 收件人
        /// </summary>
        public string ReceiverName { get; set; }
        /// <summary>
        /// 收件人地址
        /// </summary>
        public string ReceiverAddress { get; set; }
        /// <summary>
        /// 廠商處理時間
        /// </summary>
        public string VendorProcessTime { get; set; }
        /// <summary>
        /// 廠商處理進度
        /// </summary>
        public string VendorProgressStatus { get; set; }
        /// <summary>
        /// 結案狀態
        /// </summary>
        public string Status { get; set; }
    }

    /// <summary>
    /// 發票資訊
    /// </summary>
    public class InvoiceInfo
    {
        /// <summary>
        /// 發票Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 發票號碼
        /// </summary>
        public string InvoiceNo { get; set; }
        /// <summary>
        /// 發票張數
        /// </summary>
        public int InvoiceCount { get; set; }
        /// <summary>
        /// 是否顯示申請紙本
        /// </summary>
        public bool PrintInvoiceVisible { get; set; }
        /// <summary>
        /// 申請紙本時間
        /// </summary>
        public DateTime? InvoiceRequestTime { get; set; }
        /// <summary>
        /// 發票類型
        /// </summary>
        public InvoiceMode2 InvoiceMode { get; set; }
        /// <summary>
        /// 發票類型
        /// </summary>
        public string InvoiceModeDesc { get; set; }
        /// <summary>
        /// 損贈註記
        /// </summary>
        public string InvoiceDonateMark { get; set; }
        /// <summary>
        /// 載具類型
        /// </summary>
        public CarrierType CarrierType { get; set; }
        /// <summary>
        /// 載具類型
        /// </summary>
        public string CarrierTypeDesc { get; set; }
        /// <summary>
        /// 載具編號
        /// </summary>
        public string CarrierId { get; set; }
        /// <summary>
        /// 購買人
        /// </summary>
        public string BuyerName { get; set; }
        /// <summary>
        /// 發票地址
        /// </summary>
        public string BuyerAddress { get; set; }
        /// <summary>
        /// 統編
        /// </summary>
        public string CompanyId { get; set; }
        /// <summary>
        /// 抬頭
        /// </summary>
        public string InvoiceTitle { get; set; }
        /// <summary>
        /// 是否不可修改發票
        /// </summary>
        public bool IsReadOnly { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// 買家信箱
        /// </summary>
        public string BuyerEmail { get; set; }
        /// <summary>
        /// 檢視折讓單
        /// </summary>
        public string ViewReturnDiscountFormUrl { get; set; }
        /// <summary>
        /// 匯出折讓單
        /// </summary>
        public string ExportReturnDiscountFormUrl { get; set; }
        /// <summary>
        /// 發票變更歷程
        /// </summary>
        public List<EinvoiceFacade.EinvoiceChangeLogMain> EinvoiceChangeLog { get; set; }
        public InvoiceInfo()
        {
            EinvoiceChangeLog = new List<EinvoiceFacade.EinvoiceChangeLogMain>();
        }
    }

    /// <summary>
    /// 發票載具
    /// </summary>
    public class EinvoiceCarrierInfo
    {
        /// <summary>
        /// 載具類型
        /// </summary>
        public CarrierType CarrierType { get; set; }
        /// <summary>
        /// 載具類型
        /// </summary>
        public string CarrierTypeDesc { get; set; }
        /// <summary>
        /// 載具編號
        /// </summary>
        public string CarrierId { get; set; }
    }

    /// <summary>
    /// 編輯發票資訊
    /// </summary>
    public class EditInvoiceModel
    {
        /// <summary>
        /// 訂單編號
        /// </summary>
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// 發票Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 發票類型 二聯/三聯
        /// </summary>
        public int InvoiceMode { get; set; }
        /// <summary>
        /// 載具類型
        /// </summary>
        public int CarrierType { get; set; }
        /// <summary>
        /// 載具編號
        /// </summary>
        public string CarrierId { get; set; }
        /// <summary>
        /// 購買人
        /// </summary>
        public string BuyerName { get; set; }
        /// <summary>
        /// 發票地址
        /// </summary>
        public string BuyerAddress { get; set; }
        /// <summary>
        /// 三聯 統編
        /// </summary>
        public string CompanyId { get; set; }
        /// <summary>
        /// 三聯 抬頭
        /// </summary>
        public string InvoiceTitle { get; set; }
    }

    /// <summary>
    /// 宅配商品資訊
    /// </summary>
    public class DeliveryProductInfo
    {
        public List<SummarizedProductSpec> ProductSpecList { get; set; }
        public Guid Bid { get; set; }
        public Guid OrderGuid { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverAddress { get; set; }
        public bool IsNeedFillAtmInfoBeforeReturn { get; set; }

        public DeliveryProductInfo()
        {
            ProductSpecList = new List<SummarizedProductSpec>();
        }
    }

    /// <summary>
    /// 憑證商品資訊
    /// </summary>
    public class PponProductInfo
    {
        public List<PponInfo> PponInfos { get; set; }
        public Guid? Bid { get; set; }
        public Guid OrderGuid { get; set; }
        public bool IsNeedFillAtmInfoBeforeReturn { get; set; }
        public int? BuyerUserId { get; set; }
        public PponProductInfo()
        {
            PponInfos = new List<PponInfo>();
        }
    }

    /// <summary>
    /// 憑證商品資訊
    /// </summary>
    public class PponInfo
    {
        /// <summary>
        /// 憑證Id
        /// </summary>
        public int CouponId { get; set; }
        /// <summary>
        /// 憑證編號
        /// </summary>
        public string SequenceNumber { get; set; }
        /// <summary>
        /// 確認碼
        /// </summary>
        public string CouponCode { get; set; }
        /// <summary>
        /// 憑證連結
        /// </summary>
        public string CouponNavigateUrl { get; set; }
        /// <summary>
        /// 憑證狀態
        /// </summary>
        public string CouponStatusDesc { get; set; }
        /// <summary>
        /// 憑證簡訊
        /// </summary>
        public int SmsSentCount { get; set; }
        /// <summary>
        /// 預約申請時間
        /// </summary>
        public string ReservationCreateDate { get; set; }
        /// <summary>
        /// 預約時間
        /// </summary>
        public string ReservationDateTime { get; set; }
        /// <summary>
        /// 預約狀態
        /// </summary>
        public string ReservationStatus { get; set; }
        /// <summary>
        /// 憑證是否可退
        /// </summary>
        public bool IsCouponReturnable { get; set; }
        /// <summary>
        /// 是否預設核取
        /// </summary>
        public bool IsPponChecked { get; set; }
    }

    /// <summary>
    /// ATM 資訊
    /// </summary>
    public class AtmInfo
    {
        public Guid OrderGuid { get; set; }
        public int? BuyerUserId { get; set; }
        public IEnumerable<CtAtmRefund> LastCtAtm { get; set; }
    }

    /// <summary>
    /// 出貨資訊
    /// </summary>
    public class CreateOrderShipInputModel
    {
        /// <summary>
        /// 訂單Guid
        /// </summary>
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// 檔次Bid
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 出貨日
        /// </summary>
        public DateTime DispatchDate { get; set; }
        /// <summary>
        /// 物流單號
        /// </summary>
        public string TrackingNumber { get; set; }
        /// <summary>
        /// 物流商編號
        /// </summary>
        public int LogisticsCorpId { get; set; }
        /// <summary>
        /// 出貨註記
        /// </summary>
        public string ShippingMemo { get; set; }
    }

    /// <summary>
    /// 編輯出貨資訊
    /// </summary>
    public class UpdateOrderShipInputModel
    {
        /// <summary>
        /// 出貨單Id
        /// </summary>
        public int OrderShipId { get; set; }
        /// <summary>
        /// 出貨日
        /// </summary>
        public DateTime DispatchDate { get; set; }
        /// <summary>
        /// 物流單號
        /// </summary>
        public string TrackingNumber { get; set; }
        /// <summary>
        /// 物流商編號
        /// </summary>
        public int LogisticsCorpId { get; set; }
        /// <summary>
        /// 出貨註記
        /// </summary>
        public string ShippingMemo { get; set; }
        /// <summary>
        /// 檔次Bid
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 訂單Guid
        /// </summary>
        public Guid OrderGuid { get; set; }
    }

    /// <summary>
    /// 宅配商品退貨輸入資料
    /// </summary>
    public class ProductRefundInputModel
    {
        /// <summary>
        /// 訂單Guid
        /// </summary>
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// 檔次Bid
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 只退購物金
        /// </summary>
        public bool IsSCashOnly { get; set; }
        /// <summary>
        /// 退貨原因
        /// </summary>
        public string RefundReason { get; set; }
        /// <summary>
        /// 退貨項目
        /// </summary>
        public List<SelectedRefundItem> SelectedRefundItems { get; set; }
        /// <summary>
        /// 收件人姓名
        /// </summary>
        public string ReceiverName { get; set; }
        /// <summary>
        /// 收件人地址
        /// </summary>
        public string ReceiverAddress { get; set; }
        /// <summary>
        /// true: 換貨　false: 退貨
        /// </summary>
        public bool IsExchange { get; set; }

        public ProductRefundInputModel()
        {
            SelectedRefundItems = new List<SelectedRefundItem>();
        }
    }
    public class SelectedRefundItem
    {
        /// <summary>
        /// 退貨商品
        /// </summary>
        public string Pids { get; set; }
        /// <summary>
        /// 退貨份數
        /// </summary>
        public int SelectedCount { get; set; }
    }

    /// <summary>
    /// 憑證退貨輸入資料
    /// </summary>
    public class ProductPponInputModel
    {
        /// <summary>
        /// 訂單編號
        /// </summary>
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// 檔次Bid
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 只退購物金
        /// </summary>
        public bool IsSCashOnly { get; set; }
        /// <summary>
        /// 退貨原因
        /// </summary>
        public string RefundReason { get; set; }
        /// <summary>
        /// 退貨項目
        /// </summary>
        public List<int> CouponIdList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int BuyerUserId { get; set; }
    }

    /// <summary>
    /// 銀行
    /// </summary>
    public class AtmBankInfo
    {
        public string BankNo { get; set; }
        public string BankName { get; set; }
    }

    /// <summary>
    /// 分行
    /// </summary>
    public class AtmBankBranch
    {
        public string BranchNo { get; set; }
        public string BranchName { get; set; }
    }

    /// <summary>
    /// 退換貨記錄
    /// </summary>
    public class ProductRefundAndExchangeInfo
    {
        public bool IsCoupon { get; set; }
        public List<ReturnInfo> ReturnInfos { get; set; }
        public List<ExchangeInfo> ExchangeInfos { get; set; }
        public List<AuditHistory> AuditReturnHistory { get; set; }
        public List<ExchangeLog> ExchangeHistory { get; set; }
        public ProductRefundAndExchangeInfo()
        {
            ReturnInfos = new List<ReturnInfo>();
            ExchangeInfos = new List<ExchangeInfo>();
            AuditReturnHistory = new List<AuditHistory>();
            ExchangeHistory = new List<ExchangeLog>();
        }

    }

    /// <summary>
    /// 退貨記錄
    /// </summary>
    public class ReturnInfo
    {
        /// <summary>
        /// 退貨單號
        /// </summary>
        public int RefundId { get; set; }
        /// <summary>
        /// 申請時間
        /// </summary>
        public string CreateTime { get; set; }
        /// <summary>
        /// 商品規格
        /// </summary>
        public string ItemSpec { get; set; }
        /// <summary>
        /// 退貨原因
        /// </summary>
        public string ReturnReason { get; set; }
        /// <summary>
        /// 收件人
        /// </summary>
        public string ReceiverName { get; set; }
        /// <summary>
        /// 收件地址
        /// </summary>
        public string ReceiverAddress { get; set; }
        /// <summary>
        /// 廠商處理時間
        /// </summary>
        public string VendorProcessTime { get; set; }

        /// <summary>
        /// 廠商處理進度
        /// </summary>
        public string VendorProcessStatus { get; set; }
        /// <summary>
        /// [顯示]廠商處理進度-已處理
        /// </summary>
        public bool UnreturnableProcessedVisible { get; set; }
        /// <summary>
        /// [顯示]廠商處理進度-復原
        /// </summary>
        public bool RecoverVisible { get; set; }

        /// <summary>
        /// 退貨進度
        /// </summary>
        public string RefundStatus { get; set; }
        /// <summary>
        /// 完成退貨商品規格
        /// </summary>
        public string ReturnedItemSpec { get; set; }

        /// <summary>
        /// [顯示]功能-取消退貨
        /// </summary>
        public bool CancelRefundVisible { get; set; }
        /// <summary>
        /// [啟用]功能-取消退貨
        /// </summary>
        public bool CancelRefundEnable { get; set; }

        /// <summary>
        /// [顯示]功能-立即退貨
        /// </summary>
        public bool ImmediateRefundVisible { get; set; }
        /// <summary>
        /// [啟用]功能-立即退貨
        /// </summary>
        public bool ImmediateRefundEnable { get; set; }

        /// <summary>
        /// [顯示]功能-已更新資料
        /// </summary>
        public bool UpdateAtmRefundVisible { get; set; }
        /// <summary>
        /// [顯示]功能-購物金轉現金
        /// </summary>
        public bool ScashToCashVisible { get; set; }
        /// <summary>
        /// [顯示]功能-轉退購物金
        /// </summary>
        public bool CashToSCashVisible { get; set; }
        /// <summary>
        /// [顯示]功能-回復退購物金完成
        /// </summary>
        public bool RecoverToRefundScashVisible { get; set; }
        /// <summary>
        /// [顯示]功能-人工匯退
        /// </summary>
        public bool ArtificialRefundVisible { get; set; }
        /// <summary>
        /// [顯示]功能-編輯收件資訊
        /// </summary>
        public bool EditReceiverVisible { get; set; }

        /// <summary>
        /// 折讓單狀態
        /// </summary>
        public string AllowanceDesc { get; set; }
        /// <summary>
        /// [顯示]折讓單狀態-改用電子折讓
        /// </summary>
        public bool ChangeToElcAllowanceVisible { get; set; }
        /// <summary>
        /// [啟用]折讓單狀態-改用電子折讓
        /// </summary>
        public bool ChangeToElcAllowanceEnable { get; set; }

        /// <summary>
        /// [顯示]折讓單狀態-改用紙本折讓
        /// </summary>
        public bool ChangeToPaperAllowanceVisible { get; set; }
        /// <summary>
        /// [啟用]折讓單狀態-改用紙本折讓
        /// </summary>
        public bool ChangeToPaperAllowanceEnable { get; set; }

        /// <summary>
        /// [顯示]折讓單狀態-折讓單已回
        /// </summary>
        public bool ReceivedCreditNoteVisible { get; set; }
        /// <summary>
        /// [啟用]折讓單狀態-折讓單已回
        /// </summary>
        public bool ReceivedCreditNoteEnable { get; set; }

        /// <summary>
        /// 結案狀態
        /// </summary>
        public string FinishDescription { get; set; }
    }

    /// <summary>
    /// 換貨記錄
    /// </summary>
    public class ExchangeInfo
    {
        /// <summary>
        /// 換貨單號
        /// </summary>
        public int ExchangeId { get; set; }
        /// <summary>
        /// 申請時間
        /// </summary>
        public string CreateTime { get; set; }
        /// <summary>
        /// 換貨原因
        /// </summary>
        public string ExchangeReason { get; set; }
        /// <summary>
        /// 收件人
        /// </summary>
        public string ReceiverName { get; set; }
        /// <summary>
        /// 收件地址
        /// </summary>
        public string ReceiverAddress { get; set; }
        /// <summary>
        /// 廠商處理時間
        /// </summary>
        public string VendorProcessTime { get; set; }

        /// <summary>
        /// 廠商處理進度
        /// </summary>
        public string VendorProcessStatus { get; set; }

        /// <summary>
        /// [顯示]訊息更新
        /// </summary>
        public bool ExchangeMessageVisible { get; set; }
        /// <summary>
        /// [顯示]確認完成
        /// </summary>
        public bool CompleteExchangeVisible { get; set; }
        /// <summary>
        /// [顯示]確認取消
        /// </summary>
        public bool CancelExchangeVisible { get; set; }
        /// <summary>
        /// [顯示]出貨資訊
        /// </summary>
        public bool UpdateOrderShipVisible { get; set; }
        /// <summary>
        /// [顯示]編輯收件資訊
        /// </summary>
        public bool EditExchangeReceiverVisible { get; set; }


        /// <summary>
        /// 結案狀態
        /// </summary>
        public string FinishDescription { get; set; }
    }

    /// <summary>
    /// 記錄
    /// </summary>
    public class AuditHistory
    {
        public string CreateTime { get; set; }
        public string Message { get; set; }
        public string CreateId { get; set; }
    }

    /// <summary>
    /// 換貨歷程
    /// </summary>
    public class ExchangeLog
    {
        public string CreateTime { get; set; }
        public string Status { get; set; }
        public string VendorProgressStatus { get; set; }
        public string Message { get; set; }
        public string CreateId { get; set; }
    }

    /// <summary>
    /// 超取歷程
    /// </summary>
    public class IspHistory
    {
        /// <summary>
        /// 發生時間
        /// </summary>
        [JsonProperty("deliveryTime")]
        public string DeliveryTime { get; set; }
        /// <summary>
        /// 狀態
        /// </summary>
        [JsonProperty("delvieryStatus")]
        public int DelvieryStatus { get; set; }
        /// <summary>
        /// memo
        /// </summary>
        [JsonProperty("delvieryDesc")]
        public string DelvieryContent { get; set; }
    }

    /// <summary>
    /// 憑證信託資料
    /// </summary>
    public class TrustCouponDetail
    {
        public string TrustSequenceNumber { get; set; }
        public string Amount { get; set; }
        public string CouponStatusDesc { get; set; }
        public string UsageVerifiedTime { get; set; }
    }

    /// <summary>
    /// 訂單備註
    /// </summary>
    public class OrderUserMemoDetail
    {
        public int Id { get; set; }
        public string CreateTime { get; set; }
        public string UserMemo { get; set; }
        public string CreateId { get; set; }
    }

    /// <summary>
    /// 廠商罰款 Model
    /// </summary>
    public class VendorPaymentFineInputModel
    {
        /// <summary>
        /// 檔次 Bid
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 訂單 Guid
        /// </summary>
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// 扣款/異動金額
        /// </summary>
        public int Amount { get; set; }
        /// <summary>
        /// 扣款/異動原因 Type
        /// </summary>
        public int FineType { get; set; }
        /// <summary>
        /// 給商家的資訊
        /// </summary>
        public string Reason { get; set; }
        /// <summary>
        /// 是否須給消費者
        /// </summary>
        public bool IsCompensate { get; set; }
        /// <summary>
        /// 紅利金額
        /// </summary>
        public int PromotionBonus { get; set; }
        /// <summary>
        /// 紅利金生效期限-起
        /// </summary>
        public DateTime BonusStartTime { get; set; }
        /// <summary>
        /// 紅利金生效期限-訖
        /// </summary>
        public DateTime BonusExpireTime { get; set; }
        /// <summary>
        /// 給客人的紅利金摘要
        /// </summary>
        public string BonusAction { get; set; }
        /// <summary>
        /// 對帳單編號
        /// </summary>
        public int? BalanceSheetId { get; set; }
    }

    /// <summary>
    /// 客服案件資訊
    /// </summary>
    public class CustomerServiceInfo
    {
        /// <summary>
        /// 關聯案件
        /// </summary>
        public List<CustomerServiceRelationCase> RelationCase { get; set; }
        
        public CustomerServiceInfo()
        {
            RelationCase = new List<CustomerServiceRelationCase>();
        }
    }

    /// <summary>
    /// 客服關聯案件
    /// </summary>
    public class CustomerServiceRelationCase
    {
        /// <summary>
        /// 案件編號
        /// </summary>
        public string ServiceNo { get; set; }
        /// <summary>
        /// 案件處理狀態
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// 優先度
        /// </summary>
        public string Priority { get; set; }
        /// <summary>
        /// 建立時間
        /// </summary>
        public string CreateDate { get; set; }
        /// <summary>
        /// 處理人員
        /// </summary>
        public string ServicePeople { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ParentServiceNo { get; set; }
    }
    
    /// <summary>
    /// 新增客服案件資料
    /// </summary>
    public class AddCustomerServiceCaseInputModel
    {
        /// <summary>
        /// 案件編號
        /// </summary>
        public string ServiceNo { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 問題分類
        /// </summary>
        public int Category { get; set; }
        /// <summary>
        /// 主因
        /// </summary>
        public int SubCategory { get; set; }
        /// <summary>
        /// 問題等級
        /// </summary>
        public int QuestoinLevel { get; set; }
        /// <summary>
        /// 案件來源
        /// </summary>
        public int QuestionFrom { get; set; }
        /// <summary>
        /// 案件狀態
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 備註內容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 客戶UserId
        /// </summary>
        public int CustomerUserId { get; set; }
    }
}

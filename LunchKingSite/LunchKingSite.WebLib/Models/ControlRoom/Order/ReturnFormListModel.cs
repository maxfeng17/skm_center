﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.ControlRoom.Order
{
    public class ReturnFormListModel
    {
        public DeliveryType? DeliveryType { get; set; }
        public ProductDeliveryType? ProductDeliveryType { get; set; }
        public ProgressState ProgressState { get; set; }
        public VendorProgressState VendorProgressState { get; set; }
        public string SelQueryOption { get; set; }
        public string QueryKeyWord { get; set; }
        public DateTime? ApplicationTimeS { get; set; }
        public DateTime? ApplicationTimeE { get; set; }
        public DateTime? ProcessTimeS { get; set; }
        public DateTime? ProcessTimeE { get; set; }
        public int SelAllowanceOption { get; set; }
        public string SignCompanyID { get; set; }
        public bool QueryRequest { get; set; }
        public PagerList<ReturnFormInfo> ReturnFormInfos { get; set; }        
    }

    public enum ProgressState
    {
        All,
        ReturnProcessing,
        ReturnProcessing_AtmFailed,
        ReturnCompleted,
        ReturnFail,
        ReturnCancel,
        ToBeConfirmed
    }

    public enum VendorProgressState
    {
        All,
        Processing,
        Retrieving,
        UnreturnableUnProcess,
        UnreturnableProcessed,
        CompletedByCustomerService,
        Automatic,
        ConfirmedForVendor
    }

    public class ReturnFormInfo
    {
        public int DealId { get; set; }
        public Guid DealGuid { get; set; }
        public DateTime DealCloseTime { get; set; }
        public string OrderId { get; set; }
        public Guid OrderGuid { get; set; }
        public string DealName { get; set; }
        public int OrderProductCount { get; set; }
        public decimal OrderAmount { get; set; }
        public DateTime ApplicationTime { get; set; }
        public string ShipTime { get; set; }
        public int ApplicationProductCount { get; set; }
        public string ApplicationItemDesc { get; set; }
        public DateTime? ProcessTime { get; set; }
        public int ProcessedProductCount { get; set; }
        public string Reason { get; set; }
        public string ProcessStatusDesc { get; set; }
        public string OrderStatusDesc { get; set; }
        public string VendorProcessStatusDesc { get; set; }
        public string RecipientName { get; set; }
        public string RecipientTel { get; set; }
        public string RecipientAddress { get; set; }
        public string SellerName { get; set; }
        public string ReturnContactName { get; set; }
        public string ReturnContactTel { get; set; }
        public string ReturnContactEmail { get; set; }
        public string SalesName { get; set; }
        public int ItemCost { get; set; }
        public int RefundAmount { get; set; }
        public int RefundFreight { get; set; }


        public class Columns
        {
            public const string DealId = "DealId";
            public const string OrderId = "OrderId";
            public const string DealCloseTime = "DealCloseTime";
            public const string ApplicationTime = "ApplicationTime";
            public const string ProcessTime = "ProcessTime";
            public static List<string> Names { get; set; }
            static Columns()
            {
                Names = new List<string>();
                Names.Add(DealId);
                Names.Add(OrderId);
                Names.Add(DealCloseTime);
                Names.Add(ApplicationTime);
                Names.Add(ProcessTime);
            }
        }
    }
}

﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Web;

namespace LunchKingSite.WebLib.Models.ControlRoom.Order
{
    public class ConsignmentShipImportModel
    {
        public HttpPostedFileBase ProductInventoryFile { get; set; }
        public string DealId { get; set; }
        public string DealUniqueId { get; set; }
        public VbsDealType DealType { get; set; }

    }

    public class ConsignmentBatchInventoryImportModel
    {
        public ConsignmentShipImportResult ConsignmentShipImportResult { get; set; }
        public List<ConsignmentImportFailFileInfo> ConsignmentImportFailFileInfos { get; set; }
    }

    [Serializable]
    public class ConsignmentShipImportResult
    {
        public string ImportIndentity { get; set; }
        public DateTime ImportTime { get; set; }
        public int InsertCount { get; set; }
        public int UpdateCount { get; set; }
        public int FailCount { get; set; }
        public string FailFileName { get; set; }
        public List<ConsignmentImportFailInfo> ConsignmentFailInfos { get; set; }
        public List<ConsignmentRemindingInfo> ConsignmentRemindingInfos { get; set; }
    }

    [Serializable]
    public class ConsignmentImportFailInfo
    {
        public string DealUniqueId { get; set; }
        public string OrderId { get; set; }
        public string Message { get; set; }
    }

    public class ConsignmentImportFailFileInfo
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }

    [Serializable]
    public class ConsignmentRemindingInfo
    {
        public RemindType Type { get; set; }
        public string Message { get; set; }
    }

    public enum RemindType
    {
        /// <summary>
        /// 物流公司選擇其他
        /// </summary>
        ShipCompanyIdOther
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Models.LimitedTimeSelection;
using LunchKingSite.Core.Models.PponEntities;

namespace LunchKingSite.WebLib.Models.ControlRoom.LimitedTimeSelection
{
    public class MonthDaysListModel
    {
        public DateTime TheMonth { get; set; }
        public DateTime TodayMonth { get; set; }
        public List<DaySelection> Items { get; set; }
    }

    public class DaySelection
    {
        public DateTime TheDate { get; set; }
        public DayEditState State { get; set; }
        public bool DayInMonth { get; set; }
        public int DealsCount { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1}", TheDate.ToString("yyyy-MM-dd"), State);
        }
    }

    public enum DayEditState
    {
        None, Expired, InProgress, Done
    }

    public class DailySelectionEditModel
    {
        static DailySelectionEditModel() 
        {
            Empty = new DailySelectionEditModel
            {
                DealViews = new List<DailySelectionDealInfo>(),
                LogViews = new List<DailySelectionEditLogView>()
            };
        }
        public static DailySelectionEditModel Empty { get; private set; }
        public bool IsNew { get; set; }
        public DateTime TheDate { get; set; }
        public List<DailySelectionDealInfo> DealViews { get; set; }
        public List<DailySelectionEditLogView> LogViews { get; set; }
        public int MainId { get; set; }
        public string PreviewUrl { get; set; }
        public bool ReadOnly { get; set; }     
    }

    public class DailySelectionDealInfo
    {       
        public Guid Bid { get; set; }
        public string MainPic { get; set; }
        public string ItemName { get; set; }
        public int OrderCount { get; set; }
        public int HitCount { get; set; }
        public string DealState { get; set; }
        public int Sequence { get; set; }
    }


    public class AddSelectionDealsInputModel
    {
        public int MainId { get; set; }
        public List<string> Bids { get;set; }
    }

    public class UpdateSelectionDealsInputModel
    {
        public int MainId { get; set; }
        public List<UpdaSelectionDeal> SelectionDeals { get;set; }

        public override string ToString()
        {
            return string.Format("{0} - 共{1}檔", MainId, SelectionDeals.Count);
        }

        public class UpdaSelectionDeal
        {
            public Guid Bid { get; set; }
            public bool Deleted { get; set; }
            public int Sequence { get; set; }

            public override string ToString()
            {
                return string.Format("{0} / {1}{2}", Bid, Sequence, Deleted ? " / deleted" : string.Empty);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.IDEAS
{
    public class ItemDataModel
    {
        public int ItemId { get; set; }
        public string ItemTitle { get; set; }
        public string ItemDescription { get; set; }
        public int Seq { get; set; }
        public string DealStatusDesc { get; set; }
        public DateTime? ItemStartDate { get; set; }
        public DateTime? ItemEndDate { get; set; }
        /// <summary>
        /// 商品或優惠券的有效期限起
        /// </summary>
        public string ShowDateStringS { get; set; }
        /// <summary>
        /// 商品或優惠券的有效期限迄
        /// </summary>
        public string ShowDateStringE { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Models.IDEAS
{
    public class IDEASStationDealDataModel
    {
        public IDEASStationDealDataModel()
        {
            Groups = new List<IDEASStationGroup>();
        }

        public int StationId { get; set; }
        public string Title { get; set; }
        public List<IDEASStationGroup> Groups { get; set; }
    }


    public class IDEASStationGroup
    {
        public IDEASStationGroup()
        {
            ItemList = new List<IDEASStationItem>();
        }
        public string GroupTitle { get; set; }
        public string GroupEventUrl { get; set; }
        public EventPromoItemType ItemType { get; set; }
        public List<IDEASStationItem> ItemList { get; set; } 
    }


    public class IDEASStationItem
    {
        public int EventItemId { get; set; }
        /// <summary>
        /// 序號
        /// </summary>
        public int Seq { get; set; }
        public string ItemTitle { get; set; }
        public DateTime StartDate{ get; set; }
        public DateTime EndDate { get; set; }
        /// <summary>
        /// 是否為啟用狀態
        /// </summary>
        public bool Status { get; set; }
        /// <summary>
        /// 檔次於看板顯示的起始日期
        /// </summary>
        public DateTime? ItemStartDate { get; set; }
        /// <summary>
        /// 檔次於看板顯示的截止日期(含)
        /// </summary>
        public DateTime? ItemEndDate { get; set; }
        /// <summary>
        /// 檔次或優惠券販賣或顯示時間起
        /// </summary>
        public string ShowDateStringS { get; set; }
        /// <summary>
        /// 檔次或優惠券販賣或顯示時間迄
        /// </summary>
        public string ShowDateStringE { get; set; }
    }
}

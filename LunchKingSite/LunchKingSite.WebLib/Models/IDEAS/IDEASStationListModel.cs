﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.IDEAS
{
    public class IDEASStationListModel
    {
        public List<IDEASStationDataModel> DataList { get; set; }
    }

    public class IDEASStationDataModel
    {
        public int StationId { get; set; }
        public string Title { get; set; }
        public string CouponEventUrl { get; set; }
		public string DeliveryEventUrl { get; set; }
	    public string SpecialEventUrl { get; set; }
        public string VourcherEventUrl { get; set; }
    }
}

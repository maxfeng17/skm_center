﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Models.IDEAS
{
    public class IDEASStationEventEditModel
    {
        public IDEASStationEventEditModel()
        {
            ItemData = new ItemDataModel();
        }

        public int EventId { get; set; }
        public int StationId { get; set; }
        public EventPromoItemType ItemType { get; set; }
        public string EventPromoUrl { get; set; }
        public ItemDataModel ItemData { get; set; }
    }
}

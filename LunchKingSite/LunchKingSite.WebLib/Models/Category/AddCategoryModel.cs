﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.Category
{
    public class AddCategoryModel
    {
    }

    public class DealCategoryItem
    {
        public int ParentId { get; set; }
        public string ParentName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string ConsoleName { get; set; }
        public int CategorySeq { get; set; }
        public bool IsShowAddSub { get; set; }
        public bool IsShowFrontEnd { get; set; }
        public bool IsShowBackEnd { get; set; }
        public int IconType { get; set; }
        public string ImagePath { get; set; }
    }
}

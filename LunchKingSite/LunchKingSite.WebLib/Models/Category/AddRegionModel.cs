﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.Category
{
    public class AddRegionModel
    {
        public List<List<KeyValuePair<string, string>>> ChannelCategoryList { get; set; }
        //public List<KeyValuePair<string, string>> StandardRegionList { get; set; }
        public List<StandardRegionOption> StandardRegionList { get; set; }
        public List<CityRegionOption> CityRegionList { get; set; }
        public List<List<KeyValuePair<string, string>>> TravelChannelCategoryList { get; set; }
    }
}

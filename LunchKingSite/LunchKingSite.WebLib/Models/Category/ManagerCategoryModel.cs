﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.Category
{
    public class ManagerCategoryModel
    {
        public List<KeyValuePair<string, string>> ChannelCategoryList { get; set; }
    }
}

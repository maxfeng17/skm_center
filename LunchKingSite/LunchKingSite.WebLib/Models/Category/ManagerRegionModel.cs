﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.Category
{
    public class ManagerRegionModel
    {
        public List<List<KeyValuePair<string, string>>> ChannelCategoryList { get; set; }
        //public List<KeyValuePair<string, string>> StandardRegionList { get; set; }
        public List<SpecialRegionOption> SpecialRegionList { get; set; }
        //public List<StandardRegionOption> StandardRegionList { get; set; }
        public List<CityRegionOption> CityRegionList { get; set; }
        public List<List<KeyValuePair<string, string>>> TravelChannelCategoryList { get; set; }
        public List<SpecialRegionOption> TravelSpecialRegionList { get; set; }
        public List<SpecialRegionOption> SubTravelSpecialRegionList { get; set; }
    }

    public class SpecialRegionOption
    {
        public int ChannelId { get; set; }
        public int SpecialRegionId { get; set; }
        public string SpecialRegionName { get; set; }
        public int Seq { get; set; }
    }

    public class StandardRegionOption
    {
        public int ChannelId { get; set; }
        public int StandardRegionId { get; set; }
        public string StandardRegionName { get; set; }
        public bool IsSelected { get; set; }
    }

    public class CityRegionOption
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public bool IsSelected { get; set; }
    }


}
﻿using System;

namespace LunchKingSite.WebLib.Models.WebViewVendorSystem
{
    public class LoginErrorModel
    {
        public string Message { get; set; }
    }

    public enum LoginFailResult
    {
        LoginInfoExpired,
        InvalidAccount,
        ForbiddenAccount,
        LoginInfoError,
        NONE,
    };
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.BookingSystem
{

    public class BookingDailyTravelSetupViewModel
    {
        public int BookingSystemDateId { get; set; }
        public Guid StoreGuid { get; set; }
        public bool IsOpen { get; set; }
        public DateTime EffectiveDate { get; set; }
        public int ReservationTotal { get; set; }
        public int BookingType { get; set; }
        public int MaxNumberOfPeople { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.BookingSystem
{
    public class BookingReservationSearchViewModel
    {
        public Guid StoreGuid { get; set; }
        public ReservationSearchType SearchType { get; set; }
        public string ReservationStartDate { get; set; }
        public string ReservationEndDate { get; set; }
        public string ReservationName { get; set; }
        public string ReservationMobile { get; set; }
        public string CouponSequence { get; set; }


    }

    public enum ReservationSearchType
    {
        None=0,
        ReservationTime=1,
        ReservationInfo=2
    }
}

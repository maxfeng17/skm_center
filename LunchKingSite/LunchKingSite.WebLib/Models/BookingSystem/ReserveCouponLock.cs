﻿using System.Collections.Generic;
using LunchKingSite.BizLogic.Model.BookingSystem;


namespace LunchKingSite.WebLib.Models.BookingSystem
{
    public class ReserveCouponLockViewModel
    {
        public string CouponSequenceP1 { get; set; }
        public string CouponSequenceP2 { get; set; }
        public List<ReserveCouponInfo> ReserveCouponInfos { get; set; }
    }
}

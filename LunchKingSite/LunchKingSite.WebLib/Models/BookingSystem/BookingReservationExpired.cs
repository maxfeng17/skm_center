﻿using System;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Models.BookingSystem
{
    public class BookingReservationExpiredViewModel
    {
        public Guid StoreGuid { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public string CouponSequence { get; set; }
        public PagerList<ReservationInfoModel> ReservationInfos { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.BookingSystem
{
    public class BookingPhoneCouponReservationViewModel
    {
        public Guid StoreGuid { get; set; }
        public bool ContactGender { get; set; }
        public string BookingSystemApiKey { get; set; }
        public string FullBookingDate { get; set; }
    }
}

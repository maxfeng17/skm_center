﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.BookingSystem;

namespace LunchKingSite.WebLib.Models.BookingSystem
{
    public class CouponLockSearchViewModel
    {
        public int ReserveCouponLockSearchType { get; set; }
        public string SearchStartDate { get; set; }
        public string SearchEndDate { get; set; }
        public PagerList<ReserveCouponInfo> ReserveCouponInfos { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.BookingSystem
{
    public class DailyStoreBookingTimeSlot
    {
        public int BookingSystemDateId { get; set; }
        public Guid StoreGuid { get; set; }
        public bool IsOpen { get; set; }
        public DateTime EffectiveDate { get; set; }
        public int ReservationTotal { get; set; }
        public List<BookingSystemReservationTimeSlot> TimeSlots { get; set; }
    }

    public class BookingSystemReservationTimeSlot
    {
        public int Id { get; set; }
        public int BookingSystemDateId { get; set; }
        public string TimeSlot { get; set; }
        public int MaxNumberOfPeople { get; set; }
        public int ReservationOfPeople { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Models.BookingSystem
{
    public class StoreReservationListViewModel
    {
        public Guid StoreGuid { get; set; }
        public PagerList<StoreReservationDateInfo> StoreReservationDateInfos { get; set; }
    }

    public class StoreReservationDateInfo
    {
        public DateTime BookingDate { get; set; }
        public int BookingSystemDateId { get; set; }
        public int DayType { get; set; }
        public bool IsValidBooking { get; set; }

        public bool IsOpenBooking { get; set; }

        public int CouponBookingCount { get; set; }
        public bool IsCouponBookingOver { get; set; }
        public int TravelBookingCount { get; set; }
        public bool IsTravelBookingOver { get; set; }

        public bool IsCancelCouponReservation { get; set; }
        public bool IsCancelTravelReservation { get; set; }

    }
}

﻿using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.WebLib.Models.BookingSystem
{
    public class CouponBookingDetailModel
    {
        public Guid StoreGuid { get; set; }
        public int BookingStoreId { get; set; }
        public int BookingSystemDateId { get; set; }

        public string BookingDateTimeStartHour { get; set; }
        public string BookingDateTimeStartMinute { get; set; }
        public string BookingDateTimeEndHour { get; set; }
        public string BookingDateTimeEndMinute { get; set; }
        public int TimeSpan { get; set; }
        public int DefaultMaxNumberOfPeople { get; set; }


        public int BookingType { get; set; }
        public bool Sun { get; set; }
        public bool Mon { get; set; }
        public bool Tue { get; set; }
        public bool Wed { get; set; }
        public bool Thu { get; set; }
        public bool Fri { get; set; }
        public bool Sat { get; set; }

        public List<BookingSystemTimeSlot> DefaultTimeSlotList { get; set; }

        public bool IsDetailedSetting { get; set; }

        public int MonBookingSystemDateId { get; set; }
        public bool MonIsOpening { get; set; }
        public List<BookingSystemTimeSlot> MonTimeSlotList { get; set; }
        public int TueBookingSystemDateId { get; set; }
        public bool TueIsOpening { get; set; }
        public List<BookingSystemTimeSlot> TueTimeSlotList { get; set; }
        public int WedBookingSystemDateId { get; set; }
        public bool WedIsOpening { get; set; }
        public List<BookingSystemTimeSlot> WedTimeSlotList { get; set; }
        public int ThuBookingSystemDateId { get; set; }
        public bool ThuIsOpening { get; set; }
        public List<BookingSystemTimeSlot> ThuTimeSlotList { get; set; }
        public int FriBookingSystemDateId { get; set; }
        public bool FriIsOpening { get; set; }
        public List<BookingSystemTimeSlot> FriTimeSlotList { get; set; }
        public int SatBookingSystemDateId { get; set; }
        public bool SatIsOpening { get; set; }
        public List<BookingSystemTimeSlot> SatTimeSlotList { get; set; }
        public int SunBookingSystemDateId { get; set; }
        public bool SunIsOpening { get; set; }
        public List<BookingSystemTimeSlot> SunTimeSlotList { get; set; }
        public Dictionary<DayType, ILookup<string, int>> ReservationInfo { get; set; }
    }
}

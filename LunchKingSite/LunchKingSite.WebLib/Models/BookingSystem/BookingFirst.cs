﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Models.BookingSystem
{
    #region ViewModel

    [Serializable]
    public class BookingFirstViewModel
    {
        public Guid StoreGuid { get; set; }
        public bool IsSetBookingReservationSetting { get; set; }
        public bool IsSetBookingTravelSetting { get; set; }
    }

    #endregion ViewModel
}

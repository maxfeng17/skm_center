﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Models.BookingSystem
{
    public class BookingDailyReservationViewModel
    {
        public Guid StoreGuid { get; set; }
        //public bool IsOpen { get; set; }
        public DateTime EffectiveDate { get; set; }
        public bool IsExpiredSearch { get; set; }
        public PagerList<ReservationInfoModel> ReservationInfos { get; set; }
    }

    public class ReservationInfoModel
    {
        public int BookingSystemReservationId { get; set; }
        public int BookingSystemStoreBookingId { get; set; }
        public DateTime ReservationDate { get; set; }
        public int NumberOfPeople { get; set; }
        public string Remark { get; set; }
        public string ContactName { get; set; }
        public bool ContactGender { get; set; }
        public string ContactNumber { get; set; }
        public bool IsCheckin { get; set; }
        public bool IsCancel { get; set; }
        public bool IsLock { get; set; }
        public bool IsReturn { get; set; }
        public bool IsPartialCancel { get; set; }
        public DateTime CreateDatetime { get; set; }
        public string TimeSlot { get; set; }
        public string CouponInfo { get; set; }
        public string CouponLink { get; set; }
        public List<string> CouponSequenceNumbers { get; set; }
    }

}

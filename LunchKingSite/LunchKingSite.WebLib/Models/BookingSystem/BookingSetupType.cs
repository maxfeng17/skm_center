﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.BookingSystem
{
    public class BookingSetupTypeViewModel
    {
        public Guid StoreGuid { get; set; }
        public int BookingSystemDateId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public int BookingType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Models.BookingSystem
{
    #region enum

    public enum BookingSearchType
    {
        None = 0,
        SignCompanyId = 1,
        SellerId = 2
    }

    #endregion enum

    #region ViewModel

    public class SearchSellerViewModel
    {
        public int BookingSearchType { get; set; }
        public string SearchExpression { get; set; }
        public PagerList<BookingSellerModel> BookingSellerModels { get; set; }
    }

    [Serializable]
    public class BookingSellerModel
    {
        public string SellerName { get; set; }
        public Guid SellerGuid { get; set; }
    }
    
    #endregion ViewModel
}

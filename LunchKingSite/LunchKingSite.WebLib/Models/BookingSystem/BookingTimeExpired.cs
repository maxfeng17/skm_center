﻿using System;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Models.BookingSystem
{
    public class BookingTimeExpiredViewModel
    {
        public Guid StoreGuid { get; set; }
        public DateTime ReservationStartDate { get; set; }
        public DateTime ReservationEndDate { get; set; }
        public PagerList<StoreReservationDateInfo> StoreReservationDateInfos { get; set; }
    }
}
﻿using System;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Models.BookingSystem
{

    #region ViewModel

    public class BookingSystemStoresViewModel
    {
        public PagerList<BookingSystemStoreViewModel> Stores { get; set; }
    }

    [Serializable]
    public class BookingSystemStoreViewModel
    {
        public string StoreName { get; set; }
        public Guid StoreGuid { get; set; }
        public bool IsDefalutSetting { get; set; }
    }


    #endregion ViewModel
}

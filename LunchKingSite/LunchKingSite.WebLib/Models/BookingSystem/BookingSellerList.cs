﻿using System;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Models.BookingSystem
{
    public class BookingSellerListViewModel
    {
        public PagerList<BookingSellerListModel> SellerListModels { get; set; }
    }

    [Serializable]
    public class BookingSellerListModel
    {
        public string SellerName { get; set; }
        public Guid SellerGuid { get; set; }
    }
}

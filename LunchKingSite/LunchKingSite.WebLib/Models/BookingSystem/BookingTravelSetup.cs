﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.BookingSystem
{
    public class TravelBookingDetailModel
    {
        public Guid StoreGuid { get; set; }
        public int BookingStoreId { get; set; }
        public int BookingSystemDateId { get; set; }

        public int DefaultMaxNumberOfPeople { get; set; }

        public int BookingType { get; set; }
        //public bool Sun { get; set; }
        public int SunMaxNumberOfPeople { get; set; }
        //public bool Mon { get; set; }
        public int MonMaxNumberOfPeople { get; set; }
        //public bool Tue { get; set; }
        public int TueMaxNumberOfPeople { get; set; }
        //public bool Wed { get; set; }
        public int WedMaxNumberOfPeople { get; set; }
        //public bool Thu { get; set; }
        public int ThuMaxNumberOfPeople { get; set; }
        //public bool Fri { get; set; }
        public int FriMaxNumberOfPeople { get; set; }
        //public bool Sat { get; set; }
        public int SatMaxNumberOfPeople { get; set; }
    }
}

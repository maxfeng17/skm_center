﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.SampleTestData
{
    class PcpTestDataModels
    {
    }

    public class UserInfo
    {
        public int UserId { get; set; }
    }

    public class UserAndCard : UserInfo
    {
        public int People { get; set; }
        public int MembershipCardId { get; set; }
    }

    public class IdentityModel : UserInfo
    {
        public int CardId { get; set; }
        public int IdentityUserid { get; set; }
    
    }

    public class LoginInfo
    {
        public string UserName { get; set; }
        public string ClientId { get; set; }
    }

    public class StoreModel
    {
        public StoreModel()
        {
        }

        public StoreModel(Seller store)
        {
            Guid = store.Guid;
            StoreName = store.SellerName;
            Phone = store.StoreTel;
            Address = (CityManager.CityTownShopStringGet(store.StoreTownshipId == null ? -1 : store.StoreTownshipId.Value) +
                       store.StoreAddress);
            OpenTime = store.OpenTime;
            CloseDate = store.CloseDate;
            Remarks = store.StoreRemark;
            Mrt = store.Mrt;
            Car = store.Car;
            Bus = store.Bus;
            OtherVehicles = store.OtherVehicles;
            WebUrl = store.WebUrl;
            FbUrl = store.FacebookUrl;
            PlurkUrl = string.Empty;
            BlogUrl = store.BlogUrl;
            OtherUrl = store.OtherUrl;
            CreditcardAvailable = store.CreditcardAvailable;
            Coordinates = ApiCoordinates.FromSqlGeography(
                LocationFacade.GetGeographyByCoordinate(store.Coordinate));
        }


        public Guid Guid { get; set; }
        public string StoreName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string OpenTime { get; set; }
        public string CloseDate { get; set; }
        public string Remarks { get; set; }
        public string Mrt { get; set; }
        public string Car { get; set; }
        public string Bus { get; set; }
        public string OtherVehicles { get; set; }
        public string WebUrl { get; set; }
        public string FbUrl { get; set; }
        public string PlurkUrl { get; set; }
        public string BlogUrl { get; set; }
        public string OtherUrl { get; set; }
        public bool CreditcardAvailable { get; set; }
        public ApiCoordinates Coordinates { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class SimTimeChangerModel
    {
        public int DealId { get; set; }
        public IList<TrustableItemInfo> Items { get; set; }
    }

    public class TrustableItemInfo
    {
        public Guid TrustId { get; set; }
        public int? CouponId { get; set; }
        public string CounponSeqNo { get; set; }
        public DateTime ModifyTime { get; set; }
        public string StoreName { get; set; }
        public string UsageDesc { get; set; }
    }
}

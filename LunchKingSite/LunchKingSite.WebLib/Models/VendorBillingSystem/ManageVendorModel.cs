﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Model;
using System.ComponentModel;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class ManageVendorModel
    {
        public ManageVendorAccountState State { get; set; }
        
        public ManageVendorSearchOption AccountSearchOption { get; set; }
        public string AccountSearchExpression { get; set; }
        public List<VendorAccount> SearchedAccounts { get; set; }
        public string SelectedAccountId { get; set; }

        public string AccountId { get; set; }
        public string AccountName { get; set; }
        /// <summary>
        /// 自訂帳號(綁定17life會員)
        /// </summary>
        public string BindAccount { get; set; }
        public bool ResetPassword { get; set; }
        public string Password { get; set; }
        public bool HasInspectionCode { get; set; }
        public bool ResetInspectionCode { get; set; }
        public string InspectionCode { get; set; }
        public bool ViewBsRight { get; set; }
        public bool ViewVerifyRight { get; set; }
        public bool ViewShipRight { get; set; }
        public bool ViewBookingRight { get; set; }
        public bool ViewReserveLockRight { get; set; }
        public bool ViewUserEvaluateRight { get; set; }
        public bool ViewProposalRight { set; get; }
        public bool ViewAccountManageRight { get; set; }
        public bool ViewCustomerServiceRight { get; set; }
        public bool ViewWmsRight { get; set; }
        public bool ViewProposalProductRight { set; get; }
        public bool ViewShoppingCartManageRight { set; get; }
        public int VendorRole { get; set; }
        public bool ViewToShop { get; set; }
        public bool ViewToHouse { get; set; }
        public int PcpPermission { get; set; }
        public string ClearAclIds { get; set; }
        public IEnumerable<VbsVendorAce> CurrentPermissions { get; set; }
        public IEnumerable<AccountIdentity> AccountIdentity { get; set; }
        public string PermissionScopes { get; set; }
    }

    public class VendorAccount
    {
        public string AccountId { get; set; }
        public string AccountName { get; set; }
        //自訂帳號
        public string BindAccount { get; set; }
    }


    public class AccountIdentity
    {
        public int ResourceAclId { get; set; }
        public string IdentityDesc { get; set; }
        public string ResourceTypeDesc { get; set; }
        public bool IsAllow { get; set; }
    }

    public enum ManageVendorAccountState
    {
        /// <summary>
        /// 找不到帳號
        /// </summary>
        Nothing,
        /// <summary>
        /// 找到單一帳號
        /// </summary>
        SingleAccount,
        /// <summary>
        /// 找到多個帳號
        /// </summary>
        MultipleAccounts
    }

    public enum ManageVendorSearchOption
    {
        Nothing,
        [Description("帳號")]
        AccountId,
        [Description("帳號名稱")]
        AccountName,
        [Description("熟客卡卡號")]
        RegularCardNo,
        [Description("自訂帳號")]
        BindAccount,
    }

}

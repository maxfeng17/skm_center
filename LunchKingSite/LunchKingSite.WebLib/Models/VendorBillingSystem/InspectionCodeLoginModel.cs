﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class InspectionCodeLoginModel
    {
        public string InspectionCode { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class InStorePickupModel
    {
        public Dictionary<string,string> Sellers { get; set; }
        public string SellerName { get; set; }
        public string SellerId { get; set; }
        public string SellerGuid { get; set; }
        public string ContactName { get; set; }
        public string ContactTel { get; set; }
        public string ContactEmail { get; set; }
        public string ContactMobile { get; set; }
        public string ReturnAddress { get; set; }
        public string Fax { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ServiceChannel { get; set; }
        public int Status { get; set; }
        public string ApplyDate { get; set; }
        public string StoreSubCode { get; set; }
        public string VerifyMemo { get; set; }
        public string VerifyCodeUrl { get; set; }
        public int ReturnCycle { get; set; }
        public int ReturnType { get; set; }
        public string ReturnOther { get; set; }
    }

    public class ReturnContracts
    {
        public string ContactPersonName { get; set; }
        public string SellerTel { get; set; }
        public string SellerMobile { get; set; }
        public string ContactPersonEmail { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class OthersModel
    {
        public IEnumerable<ManualCategoryInfo> ManualCategoryInfos { get; set; }
        public string BindAccount { get; set; }
        public string MobileNumber { get; set; }
    }

    public class ManualCategoryInfo
    {
        public string CategoryName { get; set; }
        public string CategoryDescription { get; set; }
        public IEnumerable<ManualFileInfo> FileInfos { get; set; }
    }

    public class ManualFileInfo
    {
        public string FileName { get; set; }
        public string FileDescription { get; set; }
        public string FilePath { get; set; }
    }
}

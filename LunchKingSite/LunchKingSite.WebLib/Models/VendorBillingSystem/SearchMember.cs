﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Models
{
	public class SearchMemberModel
	{
		public MemberSearchCondition SearchCondition { get; set; }
		public string SearchExpression { get; set; }
		public IEnumerable<MemberSearchInfo> MemberInfos { get; set; }

        public SearchMemberModel()
		{
			MemberInfos = new List<MemberSearchInfo>();
		}
	}
	

	public enum MemberSearchCondition
	{
		Unknown,
		Email,
		Name,
		AccountId
	}


	public class MemberSearchInfo
	{
		public string AccountId { get; set; }
		public string MemberName { get; set; }
		public string MemberEmail { get; set; }
		public bool IsAccountLocked { get; set; }
	}
}

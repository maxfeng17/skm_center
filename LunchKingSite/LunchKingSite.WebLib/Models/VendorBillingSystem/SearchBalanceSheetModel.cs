﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Models
{
    public class SearchBalanceSheetModel
    {
        public BusinessModel BizModel { get; set; }
        public BalanceSheetSearchCondition SearchCondition { get; set; }
        public string SearchExpression { get; set; }
        public DateTime? SearchStartTime { get; set; }
        public DateTime? SearchEndTime { get; set; }
        public bool Searched { get; set; }
        public IEnumerable<DealBalanceInfo> SearchResult { get; set; }
    }

    public enum BalanceSheetSearchCondition
    {
        Unknown,
        Bid,
        /// <summary>
        /// 檔號
        /// </summary>
        PponUniqueId,
        /// <summary>
        /// DID(檔號)
        /// </summary>
        PiinDealId,
        /// <summary>
        /// PID(商品檔號)
        /// </summary>
        PiinProductId,
        SellerName,
        DealName,
        DealStartTime,
        DealEndTime,
        DealUseStartTime,
        DealUseEndTime
    }

    public class DealBalanceInfo
    {
        public BusinessModel BizModel { get; set; }
        public string SellerName { get; set; }
        public Guid SellerGuid { get; set; }
        public Guid? StoreGuid { get; set; }
        public string StoreName { get; set; }
        public int DealId { get; set; }
        public Guid DealGuid { get; set; }
        public string DealName { get; set; }
        public DateTime? DealUseStartTime { get; set; }
        public DateTime? DealUseEndTime { get; set; }
        public int MonthlyBalanceSheetCount { get; set; }
        public RemittanceType RemittanceType { get; set; }
    }

}

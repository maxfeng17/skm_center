﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Model.VBS;
using System.ComponentModel;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class VbsExchangeOrderListInfo
    {
        public DealSaleInfo DealInfo { get; set; }
        public string OrderId { get; set; }
        public Guid OrderGuid { get; set; }
        public string OrderItemDescription { get; set; }
        public string RecipientName { get; set; }
        public string RecipientTel { get; set; }
        public string RecipientAddress { get; set; }
        public DateTime ExchangeApplicationTime { get; set; }
        public string ExchangeReason { get; set; }
        public string ExchangeMessage { get; set; }
        public int ExchangeLogId { get; set; }
        public int? OrderShipId { get; set; }
        public DateTime? ShipTime { get; set; }
        public string VendorMemo { get; set; }
        public ExchangeVendorProgressStatus VendorProgressStatus { get; set; }
        public DateTime VendorProgressCompletedTime { get; set; }
        public OrderReturnStatus ExchangeStatus { get; set; }
        public IEnumerable<VendorProcessLog> VendorProcessLog { get; set; }
        public ProductOrderShip ExchangeOrderShipInfo { get; set; }

        public class Columns
        {
            public const string OrderId = "OrderId";
            public const string RecipientName = "RecipientName";
            public const string RecipientTel = "RecipientTel";
            public const string ExchangeApplicationTime = "ExchangeApplicationTime";
            public const string ExchangeReason = "ExchangeReason";
            public const string ExchangeMessage = "ExchangeMessage";
            public const string ShipTime = "ShipTime";
            public const string VendorProgressStatus = "VendorProgressStatus";
            public static List<string> Names { get; set; }
            static Columns()
            {
                Names = new List<string>();
                Names.Add(OrderId);
                Names.Add(RecipientName);
                Names.Add(RecipientTel);
                Names.Add(ExchangeApplicationTime);
                Names.Add(ExchangeReason);
                Names.Add(ExchangeMessage);
                Names.Add(ShipTime);
                Names.Add(VendorProgressStatus);
            }
        }
    }

    public class VendorProcessLog
    {
        public DateTime CreateTime { get; set; }
        public string CreateId { get; set; }
        public string Message { get; set; }
    }

    public class VendorExchangeOrderProcess
    {
        public int ExchangeLogId { get; set; }
        public int ProgressStatus { get; set; }
        public string ProcessMemo { get; set; }
        public string ProcessMemoDefault { get; set; }
        public string ShipMemoDefault { get; set; }
        public int? ShipCompanyId { get; set; }
        public DateTime? ShipTime { get; set; }
        public string ShipNo { get; set; }
        public string ShipMemo { get; set; }
    }

    public class OrderShipListSharedModel
    {
        public VbsShipOrderQueryData Query { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public int TotalCount { get; set; }
        public int UnProcessCount { get; set; }
        public int ProcessingCount { get; set; }
        public int EndCount { get; set; }
        public List<OrderListSharedInfos> Infos { get; set; }
        public bool IsReadOnly { get; set; }
        public OrderShipListSharedModel()
        {
            Infos = new List<OrderListSharedInfos>();
        }
    }

    public class OrderListSharedInfos
    {
        public string SerNo { get; set; }
        public int OrderReturnId { get; set; }
        public string OrderId { get; set; }
        public int ProgressStatus { get; set; }
        public int VendorProgressStatus { get; set; }
        public int ShipCompanyId { get; set; }
        public string ShipNo { get; set; }
        public string ProcessMemo { get; set; }
        public int ComboPackCount { get; set; }
        public string CreateTime { get; set; }
        public string Reason { get; set; }
        public string RecipientName { get; set; }
        public string RecipientAddress { get; set; }
        public string RecipientMobile { get; set; }
        public int DealId { get; set; }
        public string DealName { get; set; }
        public IEnumerable<VendorProcessLog> VendorProcessLog { get; set; }
        public IEnumerable<OrderProductOptionInfo> ReturnItemInfo { get; set; }
        public string ReturnItemDescription { get; set; }
        public bool IsShip { get; set; }
        public bool IsWms { get; set; }
        public bool IsWmsRefund { get; set; }
        
    }

    public enum ExchangeStatusOption
    {
        [Description("全部")]
        All = 0,
        [Description("未處理")]
        Wait = 1,
        [Description("進行中")]
        Processing = 2
    }
}

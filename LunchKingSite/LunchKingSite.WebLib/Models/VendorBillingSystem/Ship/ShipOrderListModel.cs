﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class ShipOrderListModel 
    {
        public DealSaleInfo DealSaleInfo { get; set; }
        public int HasOrderShipCount { get; set; }
        public int NoOrderShipCount { get; set; }
        public int ReturnOrderProcessCount { get; set; }
        public bool IsResultEmpty { get; set; }
        public bool IsComboDeal { get; set; }
        public PagerList<VbsOrderListInfo> OrderInfos { get; set; }
        public List<ShipCompany> ShipCompanyInfos { get; set; }
        public ImportResult ImportResult { get; set; }
        public List<ImportFailFileInfo> FailFileInfos { get; set; }
        public PagerList<VbsReturnOrderListInfo> ReturnOrderPagerInfos { get; set; }
        public List<VbsReturnOrderListInfo> ReturnOrderInfos { get; set; }
        public IEnumerable<OrderProductStatistics> OrderProductStatistics { get; set; }
        public IEnumerable<DealSalesStatistics> DealSalesStatistics { get; set; }
        public int OrderUserMemoListCount { get; set; }
        public int OrderUserMemoListCountAll { get; set; }
        public IEnumerable<VbsShipDealInfo> VbsShipDealInfos { get; set; }
        public Guid DealId { get; set; }
        public object DealType { get; set; }
        public PagerList<VbsExchangeOrderListInfo> ExchangeOrderPagerInfos { get; set; }
        public List<VbsExchangeOrderListInfo> ExchangeOrderInfos { get; set; }
    }
    
    public class DealSaleInfo
    {
        public int DealUniqueId { get; set; }
        public Guid DealId { get; set; }
        public string DealName { get; set; }
        public VbsDealType DealType { get; set; }
        public DateTime DealOrderStartTime { get; set; }
        public DateTime DealOrderEndTime { get; set; }
        public DateTime ShipPeriodStartTime { get; set; }
        public DateTime ShipPeriodEndTime { get; set; }
        public VbsShipState ShipState { get; set; }
        public int ComboPackCount { get; set; }
        public int? DealEndSalesCount { get; set; }
        public string DealLabelDesc { get; set; }
        public string SellerId { get; set; }
        public string SellerName { get; set; }
    }

    public class VbsOrderListInfo
    {
        public DealSaleInfo DealInfo { get; set; }
        public string OrderId { get; set; }
        public Guid OrderGuid { get; set; }
        public DateTime OrderCreateTime { get; set; }
        public int PackCount { get; set; }
        public string ItemDescription { get; set; }
        public string ItemNo { get; set; }
        public string ItemCount { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal ItemCost { get; set; }
        public string RecipientName { get; set; }
        public string RecipientTel { get; set; }
        public string RecipientAddress { get; set; }
        public string RecipientRemark { get; set; }
        public int? ShipCompanyId { get; set; }
        public string ShipCompanyName { get; set; }
        public int? OrderShipId { get; set; }
        public DateTime? ShipTime { get; set; }
        public string ShipNo { get; set; }
        public string ShipMemo { get; set; }
        public VbsOrderShipState OrderShipState { get; set; }
        public bool HasOrderShip { get; set; }
        public int OrderStatus { get; set; }
        public List<OrderUserMemoInfo> OrderUserMemoInfos { get; set; }
        public ViewVendorProgressState? VendorReturnProgressState { get; set; }
        public string ShipItemDescription { get; set; }
        public string ShipItemCount { get; set; }
        public int FreightAmount { get; set; }
        public bool HasExchangeLog { get; set; }
        public string IspStoreName { get; set; }
        public string TrackingNumber { get; set; }
        public bool IsShipmentPdf { get; set; }
        public string ReturnType { get; set; }
        public DateTime? DcReturnTime { get; set; }
        public string DcReturnReason { get; set; }
        public int CheckButton { get; set; }
        public int ProductDeliveryType { get; set; }

        public class Columns
        {
            public const string OrderId = "OrderId";
            public const string ItemDescription = "ItemDescription";
            public const string RecipientName = "RecipientName";
            public const string RecipientTel = "RecipientTel";
            public const string RecipientAddress = "RecipientAddress";
            public const string OrderShipState = "OrderShipState";
            public const string HasOrderShip = "HasOrderShip";
            public const string VendorReturnProgressState = "VendorReturnProgressState";
            public const string OrderCreateTime = "OrderCreateTime";
            public const string OrderUserMemoInfos = "OrderUserMemoInfos";
            public const string ShipTime = "ShipTime";
            public static List<string> Names { get; set; }
            static Columns()
            {
                Names = new List<string>();
                Names.Add(OrderId);
                Names.Add(ItemDescription);
                Names.Add(RecipientName);
                Names.Add(RecipientTel);
                Names.Add(RecipientAddress);
                Names.Add(OrderShipState);
                Names.Add(HasOrderShip);
                Names.Add(VendorReturnProgressState);
                Names.Add(OrderCreateTime);
                Names.Add(OrderUserMemoInfos);
                Names.Add(ShipTime);
            }
        }
    }

    public enum VbsOrderShipState
    {
        /// <summary>
        /// 未出貨
        /// </summary>
        UnShip,
        /// <summary>
        /// 未出貨-部分退貨
        /// </summary>
        UnShipAndPartialCancel,
        /// <summary>
        /// 已出貨
        /// </summary>
        Shipped,
        /// <summary>
        /// 已出貨-部分退貨
        /// </summary>
        ShippedAndPartialCancel,
        /// <summary>
        /// 已退貨
        /// </summary>
        Return,
        /// <summary>
        /// 已退貨-結檔前
        /// </summary>
        ReturnBeforeDealClose,
        /// <summary>
        /// 已退貨-結檔後
        /// </summary>
        ReturnAfterDealClose
    }

    public class ProductOrderShip
    {
        public string DealId { get; set; }
        public int OrderClassification { get; set; }
        public Guid OrderGuid { get; set; }
        public int? ShipCompanyId { get; set; }
        public string ShipNo { get; set; }
        public DateTime? ShipTime { get; set; }
        public int? OrderShipId { get; set; }
        public string ShipMemo { get; set; }
        public string ShipMemoDefault { get; set; }
    }

    public class OrderUserMemoInfo
    {
        public DateTime CreateTime { get; set; }
        public string UserMemo { get; set; }
    }

}

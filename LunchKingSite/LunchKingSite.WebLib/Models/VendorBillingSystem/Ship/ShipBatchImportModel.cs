﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Web;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class ShipBatchImportModel
    {
        public HttpPostedFileBase ProductInventoryFile { get; set; }
        public string DealId { get; set; }
        public string DealUniqueId { get; set; }
        public VbsDealType DealType { get; set; }
        public bool NewVersion { get; set; }
    }

    public class BatchInventoryImportModel
    {
        public ImportResult ImportResult { get; set; }
        public List<ImportFailFileInfo> FailFileInfos { get; set; }
    }

    [Serializable]
    public class ImportResult
    {
        public string ImportIndentity { get; set; }
        public DateTime ImportTime { get; set; }
        public int InsertCount { get; set; }
        public int UpdateCount { get; set; }
        public int FailCount { get; set; }
        public int NotYetCount { get; set; }
        public string FailFileName { get; set; }
        public List<ImportFailInfo> FailInfos { get; set; }
        public List<RemindingInfo> RemindingInfos { get; set; }
        public string OtherMessage { get; set; }
    }

    [Serializable]
    public class ImportFailInfo
    {
        public string DealUniqueId { get; set; }
        public string OrderId { get; set; }
        public string Message { get; set; }
        public Guid OrderGuid { get; set; }
    }

    public class ImportFailFileInfo
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }

    [Serializable]
    public class RemindingInfo
    {
        public RemindType Type { get; set; }
        public string Message { get; set; }
    }

    public enum RemindType
    {
        /// <summary>
        /// 物流公司選擇其他
        /// </summary>
        ShipCompanyIdOther
    }
}

﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class ShipDealListModel
    {
        public IEnumerable<VbsShipDealInfo> DealInfos { get; set; }
        public bool IsResultEmpty { get; set; }
    }

    [Serializable]
    public class VbsShipDealInfo
    {
        public int DealUniqueId { get; set; }
        public Guid DealId { get; set; }
        public string DealName { get; set; }
        public VbsDealType DealType { get; set; }
        public DateTime ShipPeriodStartTime { get; set; }
        public DateTime ShipPeriodEndTime { get; set; }
        public VbsShipState ShipState { get; set; }
        public int? ShipPercent { get; set; }
        public string SellerId { get; set; }
        public string SellerName { get; set; }
        public bool? EnableISP { get; set; }
        public int UnShipCount { get; set; }
    }

    public enum VbsShipState
    {
        /// <summary>
        /// 待處理 : 出貨中、逾期出貨中及退貨中
        /// </summary>
        Processing,
        /// <summary>
        /// 逾期出貨中 : 超過出貨時間範圍內 and 未有最後出貨回覆日
        /// </summary>
        ShippingOverdue,
        /// <summary>
        /// 出貨中 : 出貨時間範圍內 and 未有最後出貨回覆日
        /// </summary>
        Shipping,
        /// <summary>
        /// 退換貨中 : 已有最後出貨回覆日 but 退換貨未完成（後台未按下＂已核對對帳單＂）
        /// </summary>
        ReturnAndChanging,
        /// <summary>
        /// 未達開始配送時間 : 開始配送日期大於系統日期
        /// </summary>
        NotYetShip,
        /// <summary>
        /// 已完成 : 已有最後出貨回覆日 and 退換貨已完成
        /// </summary>
        Finished

    }
}

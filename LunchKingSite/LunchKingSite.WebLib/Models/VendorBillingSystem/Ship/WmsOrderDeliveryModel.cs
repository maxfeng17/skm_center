﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem.Ship
{
    public class WmsOrderDeliveryModel
    {
        #region 查詢條件
        /// <summary>
        /// 提案單號
        /// </summary>
        public string ProposalId { get; set; }
        /// <summary>
        /// 檔次BID
        /// </summary>
        public string BID { get; set; }
        /// <summary>
        /// 檔號
        /// </summary>
        public string UniqueId { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        public string BrandName { get; set; }
        /// <summary>
        /// 商品名稱
        /// </summary>
        public string ItemName { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 建立日期(起)
        /// </summary>
        public DateTime? OrderCreateTimeS { get; set; }
        /// <summary>
        /// 建立日期(迄)
        /// </summary>
        public DateTime? OrderCreateTimeE { get; set; }
        

        #endregion
        /// <summary>
        /// 訂單配送與物流清單
        /// </summary>
        public List<WmsOrderDeliveryData> WmsOrderDeliveryList { get; set; }
    }
     /// <summary>
    /// 客服單資料
    /// </summary>
    public class WmsOrderDeliveryData
    {
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// （檔號）檔名
        /// </summary>
        public string UniqueIdWithAppTitle { get; set; }
        /// <summary>
        /// （倉儲商品ID）商品 ＊數量
        /// </summary>
        public List<string> ItemInfoWithQtys { get; set; }
        /// <summary>
        /// 出貨進度
        /// </summary>
        public string ShipStatus { get; set; }
        /// <summary>
        /// （物流公司）單號
        /// </summary>
        public string ShipCompanyWithNo { get; set; }
    }
}

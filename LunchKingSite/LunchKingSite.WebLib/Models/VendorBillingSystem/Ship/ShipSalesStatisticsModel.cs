﻿namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class OrderProductStatistics
    {
        public string ProductOption { get; set; }
        public int Count { get; set; }
    }

    public class DealSalesStatistics
    {
        public int DealUniqueId { get; set; }
        public string DealName { get; set; }
        public int UnShipOrderCount { get; set; }
        public int ReturningOrderCount { get; set; }
        public int SalesCount { get; set; }
        public int ReturnedCount { get; set; }
    }

    public enum InventoryType
    {
        ProductOptionStatistics = 1,
        DealSalesStatistics = 2,
        AllShip,
        UnShip,
        ComboDeals,
        OptionStatistic,
        UnShipOptionStatistic,
        BatchUnShip
    }
}

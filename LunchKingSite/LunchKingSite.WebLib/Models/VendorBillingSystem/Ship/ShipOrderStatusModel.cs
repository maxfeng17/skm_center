﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.VBS;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class ShipOrderStatusModel
    {
        public List<ShipOrderInfo> ShipOrderInfos { get; set; }
        public int TotalCount { get; set; }
        public int PageCount { get; set; }
        public int PageSize { get; set; }
        public VbsShipOrderQueryData Query { get; set; }
    }

    public class ShipOrderInfo
    {
        public string SerNo { get; set; }
        public string OrderId { get; set; }
        public string SubDealId { get; set; }
        public string MainDealId { get; set; }
        public string OrderDate { get; set; }
        public string ItemName { get; set; }
        public bool IsShipped { get; set; }
        public int? ExchangeStatus { get; set; }
        public int? ReturnStatus { get; set; }
        public string CoustmerOrderId { get; set; }
        public Guid DealId { get; set; }
        public VbsDealType DealType { get; set; }
        public ShipOrderDetail Detail { get; set; }
    }

    public class ShipOrderDetail
    {
        public string OrderId { get; set; }
        public string OrderDate { get; set; }
        public string MainDealId { get; set; }
        public string SubDealId { get; set; }
        public string ItemName { get; set; }
        public string ComboPack { get; set; }
        public string OrderSpec { get; set; }
        public string OrderPrice { get; set; }
        public string Receiver { get; set; }
        public string ReceiverMobile { get; set; }
        public string ReveiverAddress { get; set; }
        public List<ShipOrderLog> Log { get; set; }
    }

    public class ShipOrderLog
    {
        public string ProcessTime { get; set; }
        public int? ProcessStatus { get; set; }
        public string Memo { get; set; }
        public int Type { get; set; }
        public string ModifyId { get; set; }
        public string ShipCompanyName { get; set; }
        public string ShipNo { get; set; }
    }

    public enum ShipType
    {
        Ship = 1,
        Change = 2,
        Return = 3
    }
}

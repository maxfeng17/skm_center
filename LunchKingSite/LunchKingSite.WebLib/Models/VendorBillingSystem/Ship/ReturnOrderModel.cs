﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class VbsReturnOrderListInfo
    {
        public DealSaleInfo DealInfo { get; set; }
        public string OrderId { get; set; }
        public Guid OrderGuid { get; set; }
        public string OrderItemDescription { get; set; }
        public string RecipientName { get; set; }
        public string RecipientTel { get; set; }
        public string RecipientAddress { get; set; }
        public DateTime ReturnApplicationTime { get; set; }
        public string ReturnItemDescription { get; set; }
        public string ReturnReason { get; set; }
        public IEnumerable<OrderProductOptionInfo> ReturnItemInfo { get; set; }
        public int ReturnFormId { get; set; }
        public int? OrderShipId { get; set; }
        public DateTime? ShipTime { get; set; }
        public string VendorMemo { get; set; }
        public VendorProgressStatus VendorProgressStatus { get; set; }
        public DateTime VendorProgressCompletedTime { get; set; }
        public ProgressStatus ProgressStatus { get; set; }

        public class Columns
        {
            public const string OrderId = "OrderId";
            public const string RecipientName = "RecipientName";
            public const string RecipientTel = "RecipientTel";
            public const string ReturnApplicationTime = "ReturnApplicationTime";
            public const string ReturnReason = "ReturnReason";
            public const string ReturnItemDescription = "ReturnItemDescription";
            public const string ShipTime = "ShipTime";
            public const string VendorProgressStatus = "VendorProgressStatus";
            public static List<string> Names { get; set; }
            static Columns()
            {
                Names = new List<string>();
                Names.Add(OrderId);
                Names.Add(RecipientName);
                Names.Add(RecipientTel);
                Names.Add(ReturnApplicationTime);
                Names.Add(ReturnReason);
                Names.Add(ReturnItemDescription);
                Names.Add(ShipTime);
                Names.Add(VendorProgressStatus);
            }
        }
    }

    public enum ViewVendorProgressState
    {
        /// <summary>
        /// 待處理
        /// </summary>
        Process,
        /// <summary>
        /// (退貨/換貨)已完成
        /// </summary>
        Finished
        
    }

    public class VendorReturnOrderProcess
    {
        public int ReturnFormId { get; set; }
        public int ProgressStatus { get; set; }
        public int ComboPackCount { get; set; }
        public string ProcessMemo { get; set; }
        public string ProcessMemoDefault { get; set; }
        public IEnumerable<string> ReturnQuantity { get; set; }
        public IEnumerable<string> ReturnProdcutId { get; set; }
    }

}

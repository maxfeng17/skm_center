﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class PermissionRequestModel
    {
        public string AccountId { get; set; }
        public string Allows { get; set; }
        public string Denies {get;set;}
        public string NoDenies { get; set; }
    }
}

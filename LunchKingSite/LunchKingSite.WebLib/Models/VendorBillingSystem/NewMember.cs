﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models
{
    public class NewMemberModel
    {
        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool ViewBalanceSheet { get; set; }
        public bool ViewVerify { get; set; }
        public bool ViewShip { get; set; }
        public bool ViewBooking { get; set; }
        public bool ViewReserveLock { get; set; }
        public bool ViewUserEvaluateRight { get; set; }
        public bool ViewToShop { get; set; }
        public bool ViewToHouse { get; set; }
        /// <summary>
        /// true: 帳號新增成功  false: 新增失敗  null: 沒有新增帳號的動作
        /// </summary>
        public bool? AccountCreationResult { get; set; }
        public string AccounCreationFailReason { get; set; }
    }

    public class NewMemberModelParser
    {
        public VbsMembershipAccountType AccountType { get; set; }
        public string AccountId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool UseInspectionCode { get; set; }
        public string InspectionCode { get; set; }
        public bool ViewBalanceSheetRight { get; set; }
        public bool ViewVerifyRight { get; set; }
        public bool ViewShipRight { get; set; }
        public bool ViewBookingRight { get; set; }
        public bool ViewReserveLockRight { get; set; }
        public bool ViewShoppingCartRight { set; get; }
        public bool ViewUserEvaluateRight { get; set; }
        public bool ViewProposalRight { set; get; }
        public bool ViewProposalProductRight { set; get; }
        public bool ViewAccountManageRight { get; set; }
        public bool ViewCustomerServiceManageRight { get; set; }
        public bool ViewWmsRight { get; set; }
        public bool ViewToShop { get; set; }
        public bool ViewToHouse { get; set; }
    }
}

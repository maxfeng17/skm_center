﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models
{
    public class SearchVendorModel
    {
        public VendorSearchCondition SearchCondition { get; set; }
        public string SearchExpression { get; set; }
        public IEnumerable<AccountSearchResult> AccountSearchResults;
    }
    
    public enum VendorSearchCondition
    {
        Unknown,
        Bid,
        /// <summary>
        /// 檔號
        /// </summary>
        PponUniqueId,
        /// <summary>
        /// DID(檔號)
        /// </summary>
        PiinDealId,
        /// <summary>
        /// PID(商品檔號)
        /// </summary>
        PiinProductId,
        SellerName,
    }

    public class AccountSearchResult
    {
        public List<string> AccountIds { get; set; }
        public string SellerName { get; set; }
        public Guid SellerGuid { get; set; }
        public bool IsSeller { get; set; }
        public bool IsStoreShow { get; set; }
    }

}

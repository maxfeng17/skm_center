﻿using System.Collections.Generic;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class ChannelCheckBillSheetModel
    {
        
            //public class Order
            //{
                public List<ViewChannelCheckBillOrder> GeneralOrderList { get; set; }
                public List<ViewChannelCheckBillOrder> LowOrderList { get; set; }
                public List<ViewChannelCheckBillOrder> OriginalOrderList { get; set; }
                public int OriginalOrderCount { get; set; }
                public int GeneralOrderCount { get; set; }
                public int LowOrderCount { get; set; }
                public decimal OriginalOrderTotal { get; set; }
                public decimal GeneralOrderTotal { get; set; }
                public decimal LowOrderTotal { get; set; }
            //}
            //public class Return
            //{
                public List<ViewChannelCheckBillReturn> ReturnList { get; set; }
                public decimal ReturnTotal { get; set; }
                public int ReturnCount { get; set; }
            //}
            //public class Verification
            //{
                public List<ViewChannelCheckBillVerification> OriginalVerificationList { get; set; }
                public List<ViewChannelCheckBillVerification> GeneralVerificationList { get; set; }
                public List<ViewChannelCheckBillVerification> LowVerificationList { get; set; }
                public int OriginalVerificationCount { get; set; }
                public int GeneralVerificationCount { get; set; }
                public int LowVerificationCount { get; set; }
                public decimal GeneralVerificationTotal { get; set; }
                public decimal OriginalVerificationTotal { get; set; }
                public decimal LowVerificationTotal { get; set; }
            //}
            //public class CancelVerification
            //{
                public List<ViewChannelCheckBillCancelVerification> OriginalCancelVerificationList { get; set; }
                public List<ViewChannelCheckBillCancelVerification> GeneralCancelVerificationList { get; set; }
                public List<ViewChannelCheckBillCancelVerification> LowCancelVerificationList { get; set; }
                public int OriginalCancelVerificationCount { get; set; }
                public int GeneralCancelVerificationCount { get; set; }
                public int LowCancelVerificationCount { get; set; }
                public decimal OriginalCancelVerificationTotal { get; set; }
                public decimal GeneralCancelVerificationTotal { get; set; }
                public decimal LowCancelVerificationTotal { get; set; }
            //}
            //public class ChannelClientProperty
            //{
                public decimal GeneralCommission { get; set; }
                public decimal LowCommission { get; set; }
                public decimal OriginalCommissionTotal { get; set; }
                public decimal GeneralCommissionTotal { get; set; }
                public decimal LowCommissionTotal { get; set; }
                public decimal TotalCommissionTotal { get; set; }
            //}
            
    }
    
}

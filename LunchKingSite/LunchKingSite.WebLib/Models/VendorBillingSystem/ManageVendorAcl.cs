﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.WebLib.Models
{
    public class ManageVendorAclModel
    {
        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public bool IsAccountExist { get; set; }
        public PermissionSearchCondition SearchCondition { get; set; }
        public string SearchExpression { get; set; }
        public IEnumerable<VbsVendorAce> Permissions { get; set; }
        public List<PermissionManagementActions> Modifications { get; set; }
        public bool DealLevelModificationOnly { get; set; }
    }

    /// <summary>
    /// 權限結構: 賣家 - 店家 - 檔次店家 - 檔次 
    /// Ace: access control entry
    /// </summary>
    public class VbsAceDetail
    {
        public VbsPermissionType PermissionType { get; set; }
        public bool? Allow { get; set; }
        public bool? Deny { get; set; }
        public string SellerName { get; set; }
        public Guid SellerGuid { get; set; }
        public string StoreName { get; set; }
        /// <summary>
        /// 分店
        /// </summary>
        public Guid? StoreGuid { get; set; }
        /// <summary>
        /// 檔次的分店歸屬
        /// </summary>
        public Guid? MerchandiseStoreGuid { get; set; }
        public string MerchandiseName { get; set; }
        public Guid? MerchandiseGuid { get; set; }
        
    }
    
    public class PermissionManagementActions
    {
        public bool Allow { get; set; }
        public bool Deny { get; set; }
        public Guid ResourceGuid { get; set; }
        public VbsPermissionType ResourceType { get; set; }
    }

    public enum VbsPermissionType
    {
        Unknown,
        Seller,
        Store,
        Ppon,
        Piin
    }

    
}

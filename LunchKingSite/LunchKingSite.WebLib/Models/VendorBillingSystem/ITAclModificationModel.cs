﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models
{
    public class ITAclModificationModel
    {
        public string AccountId { get; set; }
        public List<string> Allows { get; set; }
        public List<string> Denies { get; set; }
    }
}

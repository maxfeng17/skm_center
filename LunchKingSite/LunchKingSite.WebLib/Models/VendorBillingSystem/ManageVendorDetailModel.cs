﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    class ManageVendorDetailModel
    {
    }

    public class VendorDetailData
    {
        /// <summary>
        /// Key (2017/02/17 熟客卡由原 vbs_membership : card 一對一改為 seller : card 一對一)
        /// </summary>
        public Guid SellerGuid { get; set; }
        public string VbsAccountId { get; set; }
        public string UserId { get; set; }
        /// <summary>
        /// 聯繫賣家的MAIL，可隨意修改
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 聯繫賣家的號碼，可隨意修改
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 簽訂合約的EMAIL，不可修改
        /// </summary>
        public string SellerEmail { get; set; }
        /// <summary>
        /// 簽訂合約的電話，不可修改
        /// </summary>
        public string SellerMobile { get; set; }
        public bool IsSuperbonusEnable { get; set; }
        public string InvoiceTitle { get; set; }
        public string InvoiceComId { get; set; }
        public string InvoiceName { get; set; }
        public string InvoiceAddress { get; set; }
        public string AccountBankCode { get; set; }
        public string AccountBranchCode { get; set; }
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string AccountId { get; set; }
        public string[] ImgUrl { get; set; }
        public string CategoryJson { get; set; }
        public string SellerName { get; set; }
        public string InContractWith { get; set; }
        /// <summary>
        /// 商家開帳號時間加12個月
        /// 目前定義為
        /// 1.PCP合約時間
        /// 2.熟客卡有效時間
        /// </summary>
        public string ContractValidDate { get; set; }
        public HttpFileCollectionBase Files { get; set; }
        public int GroupId { get; set; }
        public int ImageType { get; set; }
        public string SellerLogoPath { get; set; }
        public string[] SellerImagePath { get; set; }
        public string SellerFullCardPath { get; set; }
        public string SellerFullCardThumbnailPath { get; set; }
        public string[] SellerServiceImgPath { get; set; }
        public string[] SellerSrndgImgPath { get; set; }
        public int SellerCardType { get; set; }
        public bool IsPromo { get; set; }
        public bool IsMemberShipCard { get; set; }
        public bool IsCupDeposit { get; set; }
        public bool IsPointDeposit { get; set; }
        public string[] SellerIntro { get; set; }
        public int ModifyUserId { get; set; }
        public int Sequence { get; set; }
        public bool IsNew { get; set; }
        public bool IsPos { get; set; }
    }

    public class PCPPointData
    {
        public string transId { get; set; }
        public string orderDate { get; set; }
        public string expireDate { get; set; }
        public string memo { get; set; }
        public string pointIn { get; set; }
        public string pointOut { get; set; }
        public string total { get; set; }
        public string creator { get; set; }

        public int depositId { get; set; }
        public int withdrawalId { get; set; }
    }

    public class PCPRegularsTicket
    {
        public string createTime { get; set; }
        public string discountRange { get; set; }
        public string discountContent { get; set; }
        public string attention { get; set; }
        public string cardCombineUse { get; set; }
        public string availableTime { get; set; }
        public string status { get; set; }
        public string creator { get; set; }
        public string ticketType { get; set; }
    }

    public class PCPTicketLog
    {
        public string cardId { get; set; }
        public string usageDate { get; set; }
        public string discountAmount { get; set; }
        public string sequence { get; set; }
        public string sendDate { get; set; }
        public string creator { get; set; }
        public string memo { get; set; }
    }

    public class PCPRegularsCard
    {
        public int cardId { get; set; }
        public string cardLevel { get; set; }
        public string type { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string paymentPercent { get; set; }
        public string others { get; set; }
        public string instruction { get; set; }
        public string availableDate { get; set; }
        public string orderNeeded { get; set; }
        public string amountNeeded { get; set; }
        public string conditionLogic { get; set; }
        public bool cardEnable { get; set; }
        public string cardGroupId { get; set; }
        public string cardVersionId { get; set; }
        public bool isCurrent { get; set; }
        public string lastCardDate { get; set; }
        public bool hasPre { get; set; }
        public string exVersion { get; set; }
        public string statusDesc { get; set; }
    }

    public class PCPFitStores
    {
        public string storeName { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public Guid storeGuid { get; set; }

    }

    public class PCPTicketData
    {
        public string id { get; set; }
        public string modifyDate { get; set; }
        public string expireDate { get; set; }
        public string memo { get; set; }
        public string pointIn { get; set; }
        public string pointOut { get; set; }
        public string total { get; set; }
        public string creator { get; set; }
    }

    public class PCPMemberCardList
    {
        public string userCardId { get; set; }
        public string cardLevel { get; set; }
        public string orderCount { get; set; }
        public string amountTotal { get; set; }
        public string updateDate { get; set; }
        public string userName { get; set; }
    }
}

﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using System;
using System.Collections.Generic;


namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class BulletinBoardListModel
    {
        public PagerList<VbsBulletinBoardPostInfo> Posts { get; set; }
        public PagerList<VbsBulletinBoardReturnInfo> Returns { get; set; }
        public PagerList<VbsBulletinBoardBalanceSheetInfo> BalanceSheets { get; set; }
        public PagerList<VbsBulletinBoardExchangeInfo> Exchanges { get; set; }
    }

    public class VbsBulletinBoardPostInfo
    {
        public int Id { get; set; }
        public VbsBulletinBoardPublishSellerType PublishSellerType { get; set; }
        public string PublishContent { get; set; }
        public bool IsSticky;
        public VbsBulletinBoardPublishStatus PublishStatus { get; set; }
        public DateTime PublishTime { get; set; }
        public string LinkTitle { get; set; }
        public string LinkUrl { get; set; }
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 距今 3日內的公告要標示紅色
        /// </summary>
        public bool IsMarkRed { get; set; }
    }

    public class VbsBulletinBoardReturnInfo
    {
        public Guid DealGuid { get; set; }
        public int DealId { get; set; }
        public string DealName { get; set; }
        public VbsDealType DealType { get; set; }
        public int ReturningRemain { get; set; }
        public int ReturningToday { get; set; }
        public string ReturnApplicationTime { get; set; }
    }

    public class VbsBulletinBoardBalanceSheetInfo
    {
        public Guid DealGuid { get; set; }
        public int DealId { get; set; }
        public string DealName { get; set; }
        public Guid? StoreGuid { get; set; }
        public int BalanceSheetId { get; set; }
        public DeliveryType DeliveryType { get; set; }
        public BalanceSheetGenerationFrequency GenerationFrequency { get; set; }
        public RemittanceType RemittanceType { get; set; }
        public string IntervalEnd { get; set; }
        public string GenerateTime { get; set; }
    }

    public class VbsBulletinBoardExchangeInfo
    {
        public Guid DealGuid { get; set; }
        public int DealId { get; set; }
        public string DealName { get; set; }
        public VbsDealType DealType { get; set; }
        public int ExchangingRemain { get; set; }
        public int ExchangingToday { get; set; }
        public int ExchangingMessageToday { get; set; }
        public string ExchangeUpdateTime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    /// <summary>
    /// 客服單處理
    /// </summary>
    public class ServiceProcessModel
    {
        #region 查詢條件
        private int _currentPage = 1;

        private bool _isProcess = true; //default true
        private bool _isTransfer = true;     //default true
        private bool _isTransferBack = true; //default true
        private bool _isComplete = false;    //default false
        /// <summary>
        /// 目前頁次
        /// </summary>
        public int CurrentPage
        {
            get
            {
                return _currentPage;
            }
            set
            {
                _currentPage = value;
            }
        }
        /// <summary>
        /// 案件編號
        /// </summary>
        public string ServiceNo { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 優先權 0:普通 1:急 2:特急
        /// </summary>
        public int? Priority { get; set; }
        /// <summary>
        /// 問題分類
        /// </summary>
        public int? ProblemType { get; set; }
        /// <summary>
        /// 處理中
        /// </summary>
        public bool IsProcess
        {
            get
            {
                return _isProcess;
            }
            set
            {
                _isProcess = value;
            }
        }
        /// <summary>
        /// 轉單
        /// </summary>
        public bool IsTransfer
        {
            get
            {
                return _isTransfer;
            }
            set
            {
                _isTransfer = value;
            }
        }
        /// <summary>
        /// 回單
        /// </summary>
        public bool IsTransferBack
        {
            get
            {
                return _isTransferBack;
            }
            set
            {
                _isTransferBack = value;
            }
        }
        /// <summary>
        /// 完成
        /// </summary>
        public bool IsComplete
        {
            get
            {
                return _isComplete;
            }
            set
            {
                _isComplete = value;
            }
        }
        /// <summary>
        /// 建立日期(起)
        /// </summary>
        public DateTime? CreateTimeS { get; set; }
        /// <summary>
        /// 建立日期(迄)
        /// </summary>
        public DateTime? CreateTimeE { get; set; }
        /// <summary>
        /// 檔號
        /// </summary>
        public string UniqueId { get; set; }
        /// <summary>
        /// 檔名
        /// </summary>
        public string ItemName { get; set; }
        /// <summary>
        /// 收件人姓名
        /// </summary>
        public string MemberName { get; set; }
        /// <summary>
        /// 收件人電話
        /// </summary>
        public string MobileNumber { get; set; }

        #endregion
        /// <summary>
        /// 客服單清單
        /// </summary>
        public List<ServiceListData> ServiceList { get; set; }
    }

    /// <summary>
    /// 客服單資料
    /// </summary>
    public class ServiceListData
    {
        /// <summary>
        /// 案件編號
        /// </summary>
        public string ServiceNo { get; set; }
        /// <summary>
        /// 案件狀態
        /// </summary>
        public int CaseStatus { get; set; }
        /// <summary>
        /// 案件狀態說明
        /// </summary>
        public string CaseStatusDescription { get; set; }
        /// <summary>
        /// 優先權 0:普通 1:急 2:特急
        /// </summary>
        public int Priority { get; set; }
        /// <summary>
        /// 優先權 0:普通 1:急 2:特急
        /// </summary>
        public string PriorityDescription { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 訂單Guid
        /// </summary>
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// 收件人
        /// </summary>
        public string Recipient { get; set; }
        /// <summary>
        /// 收件人手機
        /// </summary>
        public string RecipientMobile { get; set; }
        /// <summary>
        /// 收件人地址
        /// </summary>
        public string RecipientAddress { get; set; }
        /// <summary>
        /// 檔號
        /// </summary>
        public int UniqueId { get; set; }
        /// <summary>
        /// Bid
        /// </summary>
        public Guid BusinessHourGuid { get; set; }
        /// <summary>
        /// 商品名稱
        /// </summary>
        public string ItemName { get; set; }
        /// <summary>
        /// 賣家Guid
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 賣家名稱
        /// </summary>
        public string SellerName { get; set; }
        /// <summary>
        /// 問題分類
        /// </summary>
        public string ProblemDescription { get; set; }
        /// <summary>
        /// 問題主因
        /// </summary>
        public string SubProblemDescription { get; set; }
        /// <summary>
        /// 建立時間
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 建立時間
        /// </summary>
        public DateTime? LastModifyTime { get; set; }
        /// <summary>
        /// 處理人員(一線)
        /// </summary>
        public string ServicePeople { get; set; }
        /// <summary>
        /// 處理人員(二線)
        /// </summary>
        public string SecServicePeople { get; set; }
        /// <summary>
        /// 未讀資訊
        /// </summary>
        public int notRead { get; set; }
    }

    /// <summary>
    /// 回復備註Model
    /// </summary>
    public class ReplyParamsModel
    {
        /// <summary>
        /// 案件編號
        /// </summary>
        public string ServiceNo { get; set; }
        /// <summary>
        /// 回復備註內容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 上傳附件圖片
        /// </summary>
        public HttpPostedFileBase CustomerServicePic { get; set; }
    }

    /// <summary>
    /// 分頁資訊
    /// </summary>
    public class PageData
    {
        /// <summary>
        /// 目前頁次
        /// </summary>
        public int CurrentPage { get; set; }
        /// <summary>
        /// 總頁次
        /// </summary>
        public int TotalPages { get; set; }
        /// <summary>
        /// 總筆數
        /// </summary>
        public int TotalCount { get; set; }
    }
}

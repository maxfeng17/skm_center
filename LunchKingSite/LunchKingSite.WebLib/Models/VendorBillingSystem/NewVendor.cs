﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.WebLib.Models
{
    public class NewVendorModel
    {
        /// <summary>
        /// binding checkbox input from SearchVendor page
        /// format: "seller_guid|store_guid" 
        /// store_guid = Guid.Empty when is seller
        /// </summary>
        public ICollection<string> SellerStorePairs { get; set; }
        public IEnumerable<NewVendorInfo> Accounts { get; set; }
        /// <summary>
        /// used after NewVendor posting to CreateVendor
        /// </summary>
        public bool? CreateVendorResult { get; set; }
        public string CreateVendorResultMsg { get; set; }
    }
    
    public class NewVendorInfo
    {
        public NewVendorInfo()
        {
            VbsMembershipPermissionScope = new List<VbsMembershipPermissionScope>();
        }
        public string AccountId { get; set; }
        public string  AccountName { get; set; }
        public string Password { get; set; }
        public bool UseInspectionCode { get; set; }
        public string InspectionCode { get; set; }
        public bool ViewBalanceSheetRight { get; set; }
        public bool ViewVerifyRight { get; set; }
        public bool ViewShipRight { get; set; }
        public bool ViewBookingRight { get; set; }
        public bool ViewReserveLockRight { get; set; }
        public bool ViewUserEvaluateRight { get; set; }
        public bool ViewProposalRight { set; get; }
        public bool ViewProposalProductRight { set; get; }
        public bool ViewShoppingCartRight { set; get; }
        public bool ViewAccountManageRight { get; set; }
        public bool ViewCustomerServiceManageRight { get; set; }
        public bool ViewWmsRight { get; set; }
        public bool ViewToShop { get; set; }
        public bool ViewToHouse { get; set; }
        public Guid DefaultResource { get; set; }
        public ResourceType DefaultResourceType { get; set; }
        public string ResourceSellerName { get; set; }
        public int VendorRole { get; set; }
        public IEnumerable<VbsMembershipPermissionScope> VbsMembershipPermissionScope { get; set; }
    }


}

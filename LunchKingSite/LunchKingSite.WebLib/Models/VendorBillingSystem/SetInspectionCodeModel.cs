﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class SetInspectionCodeModel
    {
        public string InspectionCode { get; set; }
        public string NewInspectionCode { get; set; }
        public string CheckNewInspectionCode { get; set; }
    }
}

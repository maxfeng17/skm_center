﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Models
{
    public class ManageBalanceSheetModel
    {
        /// <summary>
        /// for query string
        /// </summary>
        public BusinessModel Biz { get; set; }
        /// <summary>
        /// for query string
        /// </summary>
        public int DealId { get; set; }
        /// <summary>
        /// for query string
        /// </summary>
        public Guid? Store { get; set; }
        
        public DealBalanceInfo DealInfo { get; set; }
        public int SelectedBalanceSheetId { get; set; }
        public bool SelectedBalanceSheetFixed { get; set; }
        public Dictionary<int, string> MonthBalanceSheetOptions { get; set; }
        /// <summary>
        /// 對帳單起始區間
        /// </summary>
        public DateTime? IntervalStart { get; set; }
        /// <summary>
        /// 對帳單截止區間
        /// </summary>
        public DateTime? IntervalEnd { get; set; }
        /// <summary>
        /// 核銷金額
        /// </summary>
        public decimal VerifiedAmount { get; set; }
        /// <summary>
        /// 取消核銷金額
        /// </summary>
        public decimal UndoAmount { get; set; }
        /// <summary>
        /// 對帳單金額
        /// </summary>
        public decimal BalanceSheetAmount { get; set; }
        /// <summary>
        /// 對帳單明細
        /// </summary>
        public List<BalanceSheetDetailInfo> BalanceDetail { get; set; }
        /// <summary>
        /// 選擇尚未列入對帳單時: 核銷明細; 資料來源: cash_trust_log 
        /// </summary>
        public List<NonBalanceSheetVerifyInfo> VerifyDetail { get; set; }
        /// <summary>
        /// 選擇尚未列入對帳單時: 取消核銷明細, i.e. 尚未扣除金額的; 資料來源: balance_sheet_detail
        /// </summary>
        public List<BalanceSheetDetailInfo> UndoDetail { get; set; }

        /// <summary>
        /// 是否有未鎖定的對帳單, 有的話才可以把核銷/反核銷資料放到最新的對帳單內.
        /// </summary>
        public bool UnfixedBalanceSheetAvailable { get; set; }
        public int UnfixedBalanceSheetId { get; set; }

        /// <summary>
        /// 目前時間是否符合觸發人工對帳單的時間條件
        /// </summary>
        public bool IsTriggerManualable { get; set; }

        /// <summary>
        /// 是否符合可補產擋款週結月對帳單條件
        /// </summary>
        public bool IsTriggerable { get; set; }
        public int SlottingFeeQuantity { get; set; }
        public int VerifiedTotalCount { get; set; }

        /// <summary>
        /// 是否符合可更新對帳單金額(for上架費份數異動)條件
        /// </summary>
        public bool UpdateBalanceSheetAmountEnable { get; set; }
    }

    public class BalanceSheetDetailInfo
    {
        public bool IsUndo { get; set; }
        public Guid TrustId { get; set; }
        public string CouponSequenceNumber { get; set; }
        public DateTime? VerifiedTime { get; set; }
        public string VerifiedUser { get; set; }
        public DateTime? UndoTime { get; set; }
        public string UndoUser { get; set; }
        public string ModifyMessage { get; set; }
        public string ModifyUser { get; set; }
        public DateTime? ModifyTime { get; set; }
    }

    public class NonBalanceSheetVerifyInfo
    {
        public Guid TrustId { get; set; }
        public string CouponSequenceNumber { get; set; }
        public DateTime? VerifiedTime { get; set; }
        public string VerifiedUser { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class BalanceSheetsModel
    {
        public Guid DealId { get; set; }
        public string DealName { get; set; }
        public VbsDealType DealType { get; set; }
        public string StoreId { get; set; }
        public string StoreName { get; set; }
        public DateTime ExchangePeriodStartTime { get; set; }
        public DateTime ExchangePeriodEndTime { get; set; }
        public int RemittanceType { get; set; }
        public int BalanceSheetRemittanceType { get; set; }
        public int ReturnCount { get; set; }
        public int VerifiedCount { get; set; }
        public int UnDoCount { get; set; }
        public int VerifiedTotalCount { get; set; }
        public int SlottingFeeQuantity { get; set; }
        public DateTime PaymentPeriodStartTime { get; set; }
        public DateTime PaymentPeriodEndTime { get; set; }      
        public int DealUniqueId { get; set; }
        public FundTransferInfo FundTransferInfo { get; set; }
        public IEnumerable<VbsBalanceDealInfo> BalanceDealInfos { get; set; }
    }

    public class BalanceSheetsWeekModel : BalanceSheetsModel
    {
        public IEnumerable<VbsBalanceSheetsWeekInfo> BalanceSheetsWeekInfos { get; set; }
        public IEnumerable<VbsBalanceCouponInfo> CouponInfos { get; set; }  
        /// <summary>
        /// 預估付款時間
        /// </summary>
        public string PredictPaymentDate { get; set; }
        public int BalanceSheetManualCount { get; set; }
        public IEnumerable<CrossMonthPaymentInfo> CrossMonthPaymentInfos { get; set; }
    }

    public class BalanceSheetsManualModel : BalanceSheetsModel
    {
        public IEnumerable<VbsBalanceSheetsWeekInfo> BalanceSheetsWeekInfos { get; set; }
        public int VendorReceiptType { get; set; }
        public bool IsTax { get; set; }
        public IEnumerable<VbsBalanceCouponInfo> CouponInfos { get; set; }
    }

    public class VbsBalanceSheetsWeekInfo
    {
        public int BalanceSheetId { get; set; }
        public DateTime WeekIntervalStartTime { get; set; }
        public DateTime WeekIntervalEndTime { get; set; }
    }

    public class VbsBalanceCouponInfo
    {
        public int BalanceSheetId { get; set; }
        public string CouponId { get; set; }
        public string ItemName { get; set; }
        public DateTime VerifiedDateTime { get; set; }
        public string StoreName { get; set; }
        public string UserName { get; set; }
        public BalanceSheetDetailStatus UnDoState { get; set; }
        public DateTime? UnDoDateTime { get; set; }
    }

    public class CrossMonthPaymentInfo
    {
        public DateTime PaymentPeriodStartTime { get; set; }
        public DateTime PaymentPeriodEndTime { get; set; }
        public int PayableAmount { get; set; }
    }
}

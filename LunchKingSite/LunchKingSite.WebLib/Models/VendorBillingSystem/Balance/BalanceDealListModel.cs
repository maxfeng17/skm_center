﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class BalanceDealListModel
    {
        public IEnumerable<VbsBalanceDealInfo> DealInfos { get; set; }
        public bool ShowBalanceSheetPack { get; set; }
        public IEnumerable<VbsBalanceWmsInfo> WmsInfos { get; set; }
    }

    [Serializable]
    public class VbsBalanceDealInfo
    {
        public string DealName { get; set; }
        public Guid DealId { get; set; }
        public VbsDealType DealType { get; set; }
        public DateTime ExchangePeriodStartTime { get; set; }
        public DateTime ExchangePeriodEndTime { get; set; }
        public int? StoreCount { get; set; }
        public VbsVerificationState VerificationState { get; set; }
        public VbsBalanceState BalanceState { get; set; }
        public int DealUniqueId { get; set; }
        public int GenerationFrequency { get; set; }
        public int DeliveryType { get; set; }
        public int RemittanceType { get; set; }
        public Guid? StoreGuid { get; set; }
        public string StoreName { get; set; }
    }

    [Serializable]
    public class VbsBalanceWmsInfo
    {
        public int BalanceSheetWmsID { get; set; }
        public string Name { get; set; }
        public DateTime IntervalStartTime { get; set; }
        public DateTime IntervalEndTime { get; set; }

        public DateTime? ConfirmedTime { get; set; }
        public bool IsReceiptReceived { get; set; }
        public VbsBalanceState BalanceState { get; set; }
    }

    public class VbsBsBalancingCountInfo
    {
        public Guid DealGuid { get; set; }
        public int UnConfirmedCount { get; set; }
        public int UnReceiptReceivedCount { get; set; }
    }

    public enum VbsBalanceState
    {
        /// <summary>
        /// 對帳中
        /// </summary>
        Balancing,
        /// <summary>
        /// 對帳結束
        /// </summary>
        Finished
    }

}

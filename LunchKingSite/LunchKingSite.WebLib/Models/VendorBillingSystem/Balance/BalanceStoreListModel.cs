﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class BalanceStoreListModel
    {
        public Guid DealId { get; set; }
        public string DealName { get; set; }
        public VbsDealType DealType { get; set; }
        public int RemittanceType { get; set; }
        public IEnumerable<VbsBalanceStoreInfo> StoreInfos { get; set; }
        public int DealUniqueId { get; set; }
    }

    public class VbsBalanceStoreInfo
    {
        public string StoreName { get; set; }
        public string StoreId { get; set; }
        public int? SaleCount { get; set; }
        public int? ReturnCount { get; set; }
        public int? VerifiedCount { get; set; }
        public int? UnUsedCount { get; set; }
        public VbsBalanceState BalanceState { get; set; }
        public int GenerationFrequency { get; set; }
        public DateTime UseStartTime { get; set; }
    }
}

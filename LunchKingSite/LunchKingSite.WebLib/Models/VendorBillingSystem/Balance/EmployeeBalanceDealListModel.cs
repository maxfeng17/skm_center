﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class EmployeeBalanceDealListModel
    {
        public EmployeeVbsSearchCondition SearchCondition { get; set; }
        public string SearchExpression { get; set; }
        public bool IsResultEmpty { get; set; }
        public IEnumerable<VbsBalanceDealInfo> DealInfos { get; set; }
        public bool ShowBalanceSheetPack { get; set; }
        public IEnumerable<Guid> PackDealGuids { get; set; }
    }

    public enum EmployeeVbsSearchCondition
    {
        None,
        DealId,
        DealName,
        SellerId,
        SellerName
    }
}

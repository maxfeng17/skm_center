﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class BalanceSheetsMonthModel : BalanceSheetsModel
    {
        public IEnumerable<VbsBalanceSheetsMonthInfo> BalanceSheetsMonthInfos { get; set; }

        public int VendorReceiptType { get; set; }

        public bool IsTax { get; set; }

        public int BalanceSheetManualCount { get; set; }

        public int ExceedRemittanceTimeLimit { get; set; }

        public IEnumerable<CrossMonthPaymentInfo> CrossMonthPaymentInfos { get; set; }
    }

    public class VbsBalanceSheetsMonthInfo
    {
        public int BalanceSheetId { get; set; }

        public string BalanceSheetMonth { get; set; }
        
        public bool IsConfirmedReadyToPay { get; set; }
    }
}
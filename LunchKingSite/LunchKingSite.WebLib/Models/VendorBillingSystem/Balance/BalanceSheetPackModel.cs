﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;


namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class BalanceSheetPackModel
    {
        public Dictionary<Guid, string> SellerInfos { get; set; }
        public string DeliveryTypeInfos { get; set; }
        public Dictionary<int, string> BalanceSheetsMonthInfos { get; set; }
        public Guid SellerGuid { get; set; }
        public DeliveryType DeliveryType { get; set; }
        public int BsYearMonth { get; set; }
        public bool GroupByStore { get; set; }
        public bool IsResultEmpty { get; set; }
        public IEnumerable<BalanceSheetPackInfo> BalanceSheetPackInfos { get; set; }
        public IEnumerable<BalanceSheetWmsPackInfo> BalanceSheetWmsPackInfos { get; set; }
        public IEnumerable<Guid> UnReceiptReceivedDealGuids { get; set; }
    }

    public class BalanceSheetPackInfo 
    {
        public string DealName { get; set; }
        public Guid DealId { get; set; }
        public VbsDealType DealType { get; set; }
        public int DealUniqueId { get; set; }
        public Guid? StoreGuid { get; set; }
        public string StoreName { get; set; }
        public int GenerationFrequency { get; set; }
        public int? DeliveryType { get; set; }
        public VendorReceiptType VendorReceiptType { get; set; }
        public int BalanceSheetId { get; set; }
        public int EstAmount { get; set; }
        public int IspFamilyAmount { get; set; }
        public int IspSevenAmount { get; set; }
        public int VendorPaymentOverdue { get; set; }
        public int WmsOrderAmount { get; set; }
        public int RemittanceType { get; set; }
        public bool IsTax { get; set; }

        //以下為宅配使用
        public int PayAmount { get; set; }
        public int PayFreightAmount { get; set; }
        public int DeductAmount { get; set; }
        public int PaymentChangeAmount { get; set; }
        public int PositivePaymentOverdueAmount { get; set; }
        public int NegativePaymentOverdueAmount { get; set; }
        public int ISPFamilyOrderAmount { get; set; }
        public int ISPSevenOrderAmount { get; set; }
        //以上為宅配使用

    }

    public class BalanceSheetWmsPackInfo
    {
        public string Name { get; set; }
        public int BalanceSheetWmsId { get; set; }
      
        public int EstAmount { get; set; }
    }
}

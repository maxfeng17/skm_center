﻿namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class VerificationCountSummary
    {
        private int _verified;
        private int _unused;
        private int _return;
        private int _returning;
        private int _expiredverified;
        private int _before_deal_end_return;

        public void PlusVerified()
        {
            _verified++;

        }
        public void PlusUnused()
        {
            _unused++;
        }
        public void PlusReturn()
        {
            _return++;
        }
        public void PlusReturning()
        {
            _returning++;
        }
        public void PlusExpiredVerified()
        {
            _expiredverified++;
        }
        //核銷數需再加上強制退貨數，同時必需將該筆資料從退貨數中扣掉
        public void PlusForceReturn()
        {
            _verified++;
            _return--;
        }
        public void PlusBeforeDealEndReturn()
        {
            _before_deal_end_return++;
        }

        public int All
        {
            get { return _verified + _unused + _return + _returning + _expiredverified; }
        }


        public string AllCount
        {
            get { return All.ToString("#,0"); }
        }

        public string VerifiedCount
        {
            get { return _verified.ToString("#,0"); }
        }

        public string UnusedCount
        {
            get { return _unused.ToString("#,0"); }
        }

        public string ReturnCount
        {
            get { return _return.ToString("#,0"); }
        }

        public string ReturningCount
        {
            get { return _returning.ToString("#,0"); }
        }

        public string ExpiredVerifiedCount
        {
            get { return _expiredverified.ToString("#,0"); }
        }

        //public string DealEndSaleCount
        //{
        //    get { return (_verified + _unused + _return - _before_deal_end_return).ToString("#,0"); }
        //}
    }
}

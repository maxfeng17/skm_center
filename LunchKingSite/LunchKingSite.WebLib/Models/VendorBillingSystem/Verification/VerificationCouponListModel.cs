﻿using System;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class VerificationCouponListModel
    {
        public string DealName { get; set; }
        public string DealId { get; set; }
        public VbsDealType DealType { get; set; }
        public DateTime ExchangePeriodStartTime { get; set; }
        public DateTime ExchangePeriodEndTime { get; set; }
        public string StoreId { get; set; }
        public string StoreName { get; set; }
        public VerificationCountSummary CountSummary { get; set; }
        public VbsVerificationShowType VerifyState { get; set; }
        public int DealUniqueId { get; set; }
        public PagerList<VbsVerificationCouponInfo> CouponInfos { get; set; }
        public DateTime DealOrderEndTime { get; set; }
        public DateTime DealOrderStartTime { get; set; }
        public bool HideSalesInfo { get; set; }
    }

    public enum ExportType
    {
        CouponList = 1,
        SellerList = 2
    }

    public class SellerDealInfo
    {
        public int DealUniqueId { get; set; }
        public string SellerName { get; set; }
        public string DealIntroduction { get; set; }
    }
}

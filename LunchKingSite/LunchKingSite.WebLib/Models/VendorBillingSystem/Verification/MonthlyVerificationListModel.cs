﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class MonthlyVerificationListModel
    {
        public IEnumerable<VbsWeeklyVerificationInfo> WeeklyVerificationInfos { get; set; }
        public Guid DealId { get; set; }
        public string DealName { get; set; }
        public VbsDealType DealType { get; set; }
        public string StoreId { get; set; }
        public string StoreName { get; set; }
        public DateTime ExchangePeriodStartTime { get; set; }
        public DateTime ExchangePeriodEndTime { get; set; }
        public DateTime MonthIntervalStartTime { get; set; }
        public DateTime MonthIntervalEndTime { get; set; }
        public int VerifiedTotal { get; set; }
        public int UnDoTotal { get; set; }
        public int PaymentTotal { get; set; }
        public string PaymentState { get; set; }
        public int DealUniqueId { get; set; }
        public string BalanceSheetMonth { get; set; }
        public IEnumerable<VbsBalanceCouponInfo> BalanceCouponInfos { get; set; }
        public int RemittanceType { get; set; }
        public int GenerationFrequency { get; set; }
        /// <summary>
        /// 匯款資訊
        /// </summary>
        public FundTransferInfo FundTransferInfo { get; set; }
        public int SlottingFeeQuantity { get; set; }
    }
    public class VbsWeeklyVerificationInfo
    {
        public int BalanceSheetId { get; set; }
        public DateTime WeekIntervalStartTime { get; set; }
        public DateTime WeekIntervalEndTime { get; set; }
        public int PaymentTotal { get; set; }
        public string PaymentState { get; set; }
    }

}

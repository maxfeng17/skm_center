﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class EmployeeVerifyDealListModel
    {
        public string DealType { get; set; }
        public string City { get; set; }
        public string ExchangePeriodStartDate { get; set; }
        public string ExchangePeriodEndDate { get; set; }
        public string DealName { get; set; }
        public IEnumerable<VbsEmployeeVerifyDealInfo> DealInfos { get; set; }
    }

    public class VbsEmployeeVerifyDealInfo
    {
        public string DealName { get; set; }
        public string DealId { get; set; }
        public VbsDealType DealType { get; set; }
        public DateTime ExchangePeriodStartTime { get; set; }
        public DateTime ExchangePeriodEndTime { get; set; }
        public VbsVerificationState VerificationState { get; set; }
        public int DealUniqueId { get; set; }
    }

}

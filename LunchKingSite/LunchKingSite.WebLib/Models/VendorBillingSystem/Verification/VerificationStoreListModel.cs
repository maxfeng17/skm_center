﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class VerificationStoreListModel
    {
        public string DealId { get; set; }
        public string DealName { get; set; }
        public VbsDealType DealType { get; set; }
        public int DealUniqueId { get; set; }
        public bool NoRestrictedStore { get; set; }
        public IEnumerable<VbsVerificationStoreInfo> StoreInfos { get; set; }
    }

    public class VbsVerificationStoreInfo
    {
        public string StoreName { get; set; }
        public string StoreId { get; set; }
        public int? SaleCount { get; set; }
        public int? ReturnCount { get; set; }
        public int? VerifiedCount { get; set; }
        public int? UnUsedCount { get; set; }
        public VbsVerificationState VerificationState { get; set; }
        public int? VerificationPercent { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class VerificationCouponListByStoreModel
    {
        public Dictionary<string, string> StoreList { get; set; }
        public Dictionary<string, string> VerifyTime { get; set; }
        public PagerList<VbsVerificationCoupon> Coupons { get; set; }
        public string StoreId { get; set; }
        public bool IsSellerPermission { get; set; }
        public bool IsOnlyAccount { get; set; }
        //public bool IsAllowedExport { get; set; }
        public VerificationCouponListByStoreModel()
        {
            VerifyTime = new Dictionary<string, string>();
            StoreList = new Dictionary<string, string>();
            IsSellerPermission = false;
        }
    }

    
}

﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class EmployeeVerifyModel
    {
        public string txtDealId { get; set; }
        public VbsDealType txtDealType { get; set; }
        public string txtCouponP1 { get; set; }
        public string txtCouponP2 { get; set; }
    }
}

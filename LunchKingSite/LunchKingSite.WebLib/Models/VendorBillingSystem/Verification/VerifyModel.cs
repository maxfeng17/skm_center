﻿using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using System;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class VerifyModel
    {
        public string txtPrefixCoupon { get; set; }
        public string txtCouponP1 { get; set; }
        public string txtCouponP2 { get; set; }
        /// <summary>
        /// True=核銷, False=反核銷
        /// </summary>
        public bool isVerify { get; set; }
        public IEnumerable<VbsVerifyCouponInfo> CouponInfos { get; set; }
    }
}

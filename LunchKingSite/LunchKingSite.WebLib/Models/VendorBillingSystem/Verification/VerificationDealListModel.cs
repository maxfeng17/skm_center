﻿using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.VendorBillingSystem
{
    public class VerificationDealListModel
    {
        public IEnumerable<VbsVerificationDealInfo> DealInfos { get; set; }
        public bool IsResultEmpty { get; set; }
    }

    public class VbsVerificationDealInfo
    {
        public string DealName { get; set; }
        public string DealId { get; set; }
        public VbsDealType DealType { get; set; }
        public DateTime ExchangePeriodStartTime { get; set; }
        public DateTime ExchangePeriodEndTime { get; set; }
        public int StoreCount { get; set; }
        public VbsVerificationState VerificationState { get; set; }
        public int? VerificationPercent { get; set; }
        public int DealUniqueId { get; set; }
    }

    public enum VbsVerificationState
    {
        /// <summary>
        /// 尚未開始
        /// </summary>
        NotYetStart,
        /// <summary>
        /// 兌換中
        /// </summary>
        Verifying,
        /// <summary>
        /// 活動結束
        /// </summary>
        Finished,
        /// <summary>
        /// 退換貨中
        /// </summary>
        ReturnAndChanging,
        /// <summary>
        /// 出貨完成
        /// </summary>
        ShipFinished
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models
{
    public class ManageMemberModel
    {
        public bool HasQueryString { get; set; }
        public MemberSearchCondition SearchCondition { get; set; }
        public string SearchExpression { get; set; }
        /// <summary>
        /// query string for direct view of member without going through the search account page.
        /// </summary>
        public string Acct { get; set; }

        public MemberManageMode ManageMode { get; set; }
        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool ViewBalanceSheet { get; set; }
        public bool ViewVerify { get; set; }
        public bool ViewShip { get; set; }
        public bool ViewBooking { get; set; }
        public bool ViewReserveLock { get; set; }
        public bool ViewUserEvaluate { get; set; }
        public bool ViewToShop { get; set; }
        public bool ViewToHouse { get; set; }
        public bool ChangePasswordResult { get; set; }
        public bool ChangeBasicInfoResult { get; set; }
    }

    public class SellerInfoModel
    {
        public Guid Guid { get; set; }
        public string SellerId { get; set; }
        public string SellerName { get; set; }
        public string SellerBossName { get; set; }
        public string SellerEmail { get; set; }
        public string SellerTel { get; set; }
        public string SellerMobile { get; set; }
        public string SellerAddress { get; set; }
        public bool IsServiceState { get; set; }
       
    }

    public enum MemberManageMode
    {
        Unknown,
        ChangePassword = 1,
        UpdateMemberInformation = 2
    }
}

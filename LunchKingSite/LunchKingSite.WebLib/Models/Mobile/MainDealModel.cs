﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using Newtonsoft.Json;

namespace LunchKingSite.WebLib.Models.Mobile
{
    public class MainDealModel
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public MainDealModel()
        {
            PriceFromText = string.Empty;
        }
        public static MainDealModel Create(IViewPponDeal deal)
        {
            var model = new MainDealModel
            {
                DealId = deal.BusinessHourGuid,
                DealName = !string.IsNullOrWhiteSpace(deal.AppTitle) ? deal.AppTitle : deal.CouponUsage,
                SubTitle = deal.EventTitle,
                Pic = ImageFacade.GetDealPic(deal),
                KeywordTags = deal.Tags,
                RestrictionTags = deal.RestrictionTags,
                IsOBankDeal = deal.IsBankDeal.GetValueOrDefault(),
                MinGrossMargin = ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(deal.BusinessHourGuid).BaseGrossMargin * 100,
                Categories = deal.CategoryIds
            };
            if (PromotionFacade.GetDiscountUseType(deal.BusinessHourGuid) == false)
            {
                if (model.RestrictionTags == null)
                {
                    model.RestrictionTags = new List<string>();
                }
                model.RestrictionTags.Add("優惠內容不適用折價券");
            }

            //公益檔
            model.IsKindDeal = Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal);

            model.IsFamilyDeal = deal.CategoryIds != null && deal.CategoryIds.Contains(CategoryManager.Default.Family.CategoryId);


            model.OrigPrice = deal.ItemOrigPrice.ToString("F0");

            if (Helper.IsFlagSet(deal.BusinessHourStatus, (int)(BusinessHourStatus.ComboDealMain)) &&
                deal.ComboDelas != null && deal.ComboDelas.Count > 1)
            {
                model.PriceFromText = "起";
            }
            if (model.IsFamilyDeal && deal.ExchangePrice > 0)
            {
                model.Price = string.Format("${0:F0}", deal.ExchangePrice);
                model.PriceFromText = string.Empty;
            }
            else
            {
                model.Price = string.Format("${0:F0}", PponFacade.CheckZeroPriceToShowPrice(deal));
            }

            bool hasDiscount;
            model.DiscountString = MobileMainDealModel.GetDiscountString(deal, out hasDiscount);
            if (hasDiscount)
            {
                model.DiscountString += "折";
            }

            model.DealTagsHtml = PponFacade.RenderDealTagsAsMobileHtml(deal, 2);

            //詳細介紹
            string description = string.Empty;
            if (deal.Cchannel.GetValueOrDefault(false) && !string.IsNullOrEmpty(deal.CchannelLink))
            {
                description += PponFacade.GetCchannelLink(deal.CchannelLink) + "<br/>";
                description += "<div style='height:20px'></div>";
            }
            var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(deal);
            description += contentLite.Description;
            description = PponFacade.ReplaceMobileYoutubeTag(description);
            model.Description = string.Format("{0}<br />{1}", description.Replace("<img alt=\"\" src=", "<img alt=\"\" data-original="), deal.DealPromoDescription);

            model.Remark = PponFacade.GetPponRemark(contentLite.Remark);

            model.DeliveryType = (DeliveryType)deal.DeliveryType.GetValueOrDefault();

            model.Location = ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(
                null, deal.BusinessHourGuid);
            if (model.Location == "宅配") model.Location = "宅配購物";

            model.BuyerString = OrderedQuantityHelper.Show(deal, OrderedQuantityHelper.ShowType.InPortal);



            model.Countdown = PponFacade.GetGsaPriceString(deal);
            if (string.IsNullOrEmpty(model.Countdown))
            {
                model.Countdown = PponFacade.GetCountdownString(deal);
                model.IsGsaPrice = false;
            }
            else
            {
                model.IsGsaPrice = true;
            }



            model.IsSoldOut = deal.IsSoldOut || DateTime.Now > deal.BusinessHourOrderTimeE;
            model.IsLastDay = PponFacade.DealHasLastDayTag(deal);
            model.EventImgUrl = deal.PromoImageHtml;
            //評分
            PponDealEvaluateStar evaAvg = ViewPponDealManager.DefaultManager.GetPponDealEvaluateStarList(deal.BusinessHourGuid);
            if (evaAvg != null)
            {
                model.RatingEnabled = true;
                model.RatingPeople = evaAvg.Cnt;
                model.RatingScore = evaAvg.Star.ToString("0.##");
            }
            //商品規格
            if (string.IsNullOrEmpty(deal.ProductSpec) == false)
            {
                model.ProductSpec = deal.ProductSpec.Replace("<img alt=\"\" src=", "<img alt=\"\" data-original=");
            }

            //權益說明
            StringBuilder sbRestrictions = new StringBuilder();
            sbRestrictions.Append(contentLite.Restrictions);
            if (deal.DeliveryType == (int)DeliveryType.ToShop)
            {
                sbRestrictions.Append("<br /><font style=\"color: Red\">●此憑證由等值購物金兌換之</font>");
            }
            if (deal.DeliveryType == (int)DeliveryType.ToShop)
            {
                sbRestrictions.Append("<br />●本憑證不得與其他優惠合併使用，且恕無法兌換現金及找零");
                sbRestrictions.Append("<br />●好康憑證不限本人使用，使用時店家將會核對憑證編號，一組編號限用一次");
            }
            //權益說明-信託銀行文字說明
            if (deal.DeliveryType == (int)DeliveryType.ToShop && deal.ItemPrice > 0 && model.IsKindDeal == false)
            {
                TrustProvider trustProvider = Helper.GetBusinessHourTrustProvider(deal.BusinessHourStatus);
                switch (trustProvider)
                {
                    case TrustProvider.TaiShin:
                        sbRestrictions.Append("<br />●信託期間：發售日起一年內");
                        sbRestrictions.Append("<br />●本商品(服務)禮券所收取之金額，已存入發行人於台新銀行開立之信託專戶，專款專用。所稱專用，係指供發行人履行交付商品或提供服務義務使用");
                        break;
                    case TrustProvider.Sunny:
                        sbRestrictions.Append("<br />●信託期間：發售日起一年內");
                        sbRestrictions.Append("<br />●本商品(服務)禮券所收取之金額，已存入發行人於陽信銀行開立之信託專戶，專款專用。所稱專用，係指供發行人履行交付商品或提供服務義務使用");
                        sbRestrictions.Append("<br />●信託履約禮券查詢網址 https://trust.sunnybank.com.tw/ (於收到兌換券後次日10點起即可查詢)");
                        break;
                    case TrustProvider.Hwatai:
                        sbRestrictions.Append("<br />●信託期間：發售日起一年內");
                        sbRestrictions.Append("<br />●本商品(服務)禮券所收取之金額，已存入發行人於華泰銀行開立之信託專戶，專款專用。所稱專 用，係指供發行人履行交付商品或提供服務義務使用");
                        break;
                }
            }
            //權益說明-成套票券檔次條款說明
            if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon))
            {
                sbRestrictions.Append("<br /><font style='color: Red'>●成套票券商品為整套/組購入，故僅限於所有套/組憑證均兌換完畢才能享有完整優惠</font>");
                sbRestrictions.Append("<br /><font style='color: Red'>●當有退貨情事發生時，將依其整套/組進貨規則進行剩餘面額價值進行退款</font>");
            }

            model.Restrictions = sbRestrictions.ToString();
            if (deal.ItemPrice != 0 && model.IsKindDeal == false)
            {
                model.ShowEquitylink = true;
            }
            if (string.IsNullOrEmpty(contentLite.Availability))
            {
                model.StoreInfos = new AvailableInformation[0];
            }
            else
            {
                model.StoreInfos = JsonConvert.DeserializeObject<AvailableInformation[]>(contentLite.Availability);
            }

            ViewPponStoreCollection pponStores = ViewPponStoreManager.DefaultManager.ViewPponStoreGetList(deal.BusinessHourGuid, VbsRightFlag.VerifyShop);
            MobileExpireNotice expireNotice = new MobileExpireNotice();
            expireNotice.SellerName = deal.SellerName;
            expireNotice.SellerCloseDownDate = deal.CloseDownDate;
            expireNotice.StoreCloseDownDates = pponStores.Where(store => store.CloseDownDate.HasValue).
                ToDictionary(store => store.StoreName, store => store.CloseDownDate.Value);
            expireNotice.StoreChangedExpireDates = pponStores.Where(store => store.ChangedExpireDate.HasValue).
                ToDictionary(store => store.StoreName, store => store.ChangedExpireDate.Value);
            expireNotice.DealOriginalExpireDate = deal.BusinessHourDeliverTimeE.HasValue ? deal.BusinessHourDeliverTimeE.Value : DateTime.MaxValue;
            expireNotice.DealChangedExpireDate = deal.ChangedExpireDate;
            model.SpecialNews = expireNotice.ToString();


            //model.RelatedDeals = PponFacade.GetRelatedDeals(deal);
            model.RelatedDeals = PponFacade.GetRelatedDeals(deal).ConvertAll(t => MainSidebarDealModel.Create(t));
            if (model.RelatedDeals == null)
            {
                model.RelatedDeals = new List<MainSidebarDealModel>();
            }
            model.VisitedDeals = RecentlyViewedDealsmMnager.GetList(excludeDealId: deal.BusinessHourGuid);
            if (model.VisitedDeals == null)
            {
                model.VisitedDeals = new List<MainSidebarDealModel>();
            }
            model.WatchOtherDeals = PponFacade.GetWatchOtherDeals(deal).ConvertAll(t => MainSidebarDealModel.Create(t));
            if (model.WatchOtherDeals == null)
            {
                model.WatchOtherDeals = new List<MainSidebarDealModel>();
            }

            if (deal.DeliveryType == (int)DeliveryType.ToShop)
            {
                if (deal.ComboDelas == null || deal.ComboDelas.Count == 0)
                {
                    model.DealCouponEntries = DealCouponEntry.ConvertFrom(deal);
                }
                else
                {
                    model.DealCouponEntries = DealCouponEntry.ConvertFrom(deal.ComboDelas);
                }
            }
            else
            {
                if (deal.ComboDelas == null || deal.ComboDelas.Count == 0)
                {
                    model.DealProductEntries = PponDealProductEntry.ConvertFrom(deal);
                }
                else
                {
                    model.DealProductEntries = PponDealProductEntry.ConvertFrom(deal.ComboDelas);
                }
            }
            if (model.DealCouponEntries == null)
            {
                model.DealCouponEntries = new List<DealCouponEntry>();
            }
            if (model.DealProductEntries == null)
            {
                model.DealProductEntries = new List<PponDealProductEntry>();
            }
            if (model.DealCouponEntries.Count == 1)
            {
                model.BuyUrl = model.DealCouponEntries[0].BuyUrl;
            }
            else if (model.DealProductEntries.Count == 1)
            {
                model.BuyUrl = model.DealProductEntries[0].BuyUrl;
            }
            model.Skin = MainDealSkin.Create(model, deal);

            var ttmodel = new TrackTagInputModel
            {
                ViewPponDealData = deal,
                Config = config,
                IsPiinLifeDeal = false,
                GtagPage = GtagPageType.Product,
                YahooEa = YahooEaType.ViewProduct,
                ScupioEventCategory = ScupioEventCategory.PageView,
                ScupioPageType = ScupioPageType.ProductPage,
            };

            model.MicroDataJson = TagManager.CreateFatory(ttmodel, TagProvider.MicroData).GetJson();
            model.ScupioDataJson = TagManager.CreateFatory(ttmodel, TagProvider.Scupio).GetJson();
            model.GtagDataJson = TagManager.CreateFatory(ttmodel, TagProvider.GoogleGtag).GetJson();
            model.YahooTagJson = TagManager.CreateFatory(ttmodel, TagProvider.Yahoo).GetJson();
            model.DiscountPrice = deal.DiscountPrice == null ? string.Empty :
                deal.DiscountPrice.Value.ToString("0");

            return model;
        }

        public Guid DealId { get; set; }
        public string DealName { get; private set; }
        public string SubTitle { get; private set; }
        public string Pic { get; private set; }
        public string OrigPrice { get; private set; }
        public string PriceFromText { get; private set; }
        public string Price { get; private set; }
        public string DiscountString { get; private set; }
        public string MicroDataJson { get; set; }
        public string ScupioDataJson { get; set; }
        public string GtagDataJson { get; set; }
        public string YahooTagJson { get; set; }
        public bool IsLastDay { get; set; }
        /// <summary>
        ///是否已售完
        /// </summary>
        public bool IsSoldOut { get; set; }
        public string EventImgUrl { get; private set; }
        public int RatingPeople { get; private set; }
        public string RatingScore { get; private set; }
        public bool RatingEnabled { get; private set; }
        public DeliveryType DeliveryType { get; private set; }
        public string Location { get; private set; }
        public string BuyerString { get; private set; }
        public string Countdown { get; private set; }
        /// <summary>
        /// 詳細介紹
        /// </summary>
        public string Description { get; private set; }
        /// <summary>
        /// 商品規格
        /// </summary>
        public string ProductSpec { get; set; }
        public string DealTagsHtml { get; private set; }
        /// <summary>
        /// 檔次關鍵字/相關標籤
        /// </summary>
        public ICollection<string> KeywordTags { get; private set; }
        /// <summary>
        /// 一姬說好康
        /// </summary>
        public string Remark { get; private set; }
        /// <summary>
        /// 權益說明
        /// </summary>
        public string Restrictions { get; private set; }
        /// <summary>
        /// 特別新聞(倒店或延長核銷日期)，程式產生
        /// </summary>
        public string SpecialNews { get; private set; }
        /// <summary>
        /// 權益說明裏的標籤
        /// </summary>
        public ICollection<string> RestrictionTags { get; private set; }
        public bool IsFamilyDeal { get; private set; }
        public bool IsKindDeal { get; private set; }
        /// <summary>
        /// 是否為王道銀行使用，部份顯示不同，譬如是兌換而非購買
        /// </summary>
        public bool IsOBankDeal { get; private set; }
        /// <summary>
        /// 是否顯示 "查看好康使用條款連結
        /// </summary>
        public bool ShowEquitylink { get; private set; }
        public AvailableInformation[] StoreInfos { get; private set; }

        public List<DealCouponEntry> DealCouponEntries { get; private set; }
        public List<PponDealProductEntry> DealProductEntries { get; private set; }

        public List<MainSidebarDealModel> RelatedDeals { get; private set; }
        public List<MainSidebarDealModel> VisitedDeals { get; private set; }
        public List<MainSidebarDealModel> WatchOtherDeals { get; private set; }

        public string BuyUrl { get; private set; }
        public MainDealSkin Skin { get; private set; }
        public string DiscountPrice { get; private set; }
        public bool IsGsaPrice { get; set; }

        public decimal MinGrossMargin { get; set; }
        public List<int> Categories { get; set; }
    }

    public class MainDealSkin
    {
        public static MainDealSkin Create(MainDealModel model, IViewPponDeal deal)
        {
            MainDealSkin skin = new MainDealSkin();
            if (model.IsKindDeal)
            {
                if (model.IsSoldOut)
                {
                    skin.BuyText = "已截止";
                }
                else
                {
                    skin.BuyText = "我要捐款";
                }
                skin.ShowAppBuyButton = true;
                skin.ShowDealTags = true;
            }
            else if (model.IsFamilyDeal)
            {
                if (model.IsSoldOut)
                {
                    skin.BuyText = "索取完畢";
                }
                else
                {
                    skin.BuyText = "立即索取";
                }
                skin.ShowAppBuyButton = true;
                skin.ShowDealTags = true;
            }
            else if (model.IsOBankDeal)
            {
                if (model.IsSoldOut)
                {
                    skin.BuyText = "兌換完畢";
                }
                else
                {
                    skin.BuyText = "兌換";
                }
                skin.ShowAppBuyButton = false;
                skin.ShowDealTags = false;
            }
            else
            {
                if (model.IsSoldOut)
                {
                    skin.BuyText = "已售完";
                }
                else
                {
                    skin.BuyText = "查看方案";
                }
                skin.ShowAppBuyButton = true;
                skin.ShowDealTags = true;
            }
            if (model.IsSoldOut)
            {
                skin.BuyButtonCss = "buy_now btn_dontclick";
            }
            else
            {
                skin.BuyButtonCss = "buy_now";
            }
            return skin;
        }

        public string BuyText { get; private set; }
        public bool ShowAppBuyButton { get; private set; }
        public bool ShowDealTags { get; private set; }
        public string BuyButtonCss { get; set; }
    }

    [Serializable]
    public class MainSidebarDealModel
    {
        public static MainSidebarDealModel Create(IViewPponDeal deal)
        {
            var model = new MainSidebarDealModel
            {
                DealId = deal.BusinessHourGuid,
                DealName = !string.IsNullOrEmpty(deal.AppTitle) ? deal.AppTitle : deal.CouponUsage,
                SubTitle = deal.EventName,
                Pic = ImageFacade.GetDealAppPic(deal, true),
            };

            model.IsFamilyDeal = deal.CategoryIds != null && deal.CategoryIds.Contains(CategoryManager.Default.Family.CategoryId);

            model.OrigPrice = deal.ItemOrigPrice.ToString("F0");

            if (Helper.IsFlagSet(deal.BusinessHourStatus, (int)(BusinessHourStatus.ComboDealMain)) &&
                deal.ComboDelas != null && deal.ComboDelas.Count > 1)
            {
                model.PriceFromText = "起";
            }
            if (model.IsFamilyDeal && deal.ExchangePrice > 0)
            {
                model.Price = string.Format("${0:F0}", deal.ExchangePrice);
                model.PriceFromText = string.Empty;
            }
            else
            {
                model.Price = string.Format("${0:F0}", PponFacade.CheckZeroPriceToShowPrice(deal));
            }

            bool hasDiscount;
            model.DiscountString = MobileMainDealModel.GetDiscountString(deal, out hasDiscount);
            if (hasDiscount)
            {
                model.DiscountString += "折";
            }
            if (deal.DiscountPrice == null)
            {
                model.DiscountPrice = string.Empty;
            }
            else
            {
                model.DiscountPrice = deal.DiscountPrice.Value.ToString("0");
            }

            return model;
        }
        public Guid DealId { get; private set; }
        public string DealName { get; private set; }
        public string SubTitle { get; private set; }
        public string Pic { get; private set; }
        public bool IsFamilyDeal { get; private set; }
        public string OrigPrice { get; private set; }
        public string PriceFromText { get; private set; }
        public string Price { get; private set; }
        public string DiscountString { get; private set; }
        /// <summary>
        /// 折扣後最低均價
        /// </summary>
        public string DiscountPrice { get; private set; }
    }

    public class DealCouponEntry
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static DealCouponEntry Create(IViewPponDeal deal)
        {
            var model = new DealCouponEntry
            {
                DealName = deal.CouponUsage,
                SubTitle = deal.EventName
            };

            model.OrigPrice = deal.ItemOrigPrice.ToString("F0");
            decimal price = PponFacade.CheckZeroPriceToShowPrice(deal);
            model.Price = string.Format("${0:F0}", price);
            model.PriceDesc = "現省$" + (int)(deal.ItemOrigPrice - price);
            bool hasDiscount;
            model.DiscountString = MobileMainDealModel.GetDiscountString(deal, out hasDiscount);
            if (hasDiscount)
            {
                model.DiscountString += "折";
            }
            model.IsSoldOut = deal.IsSoldOut;
            model.IsExpiredDeal = deal.IsExpiredDeal.GetValueOrDefault();
            model.BuyUrl = Helper.CombineUrl(config.SSLSiteUrl, "ppon/buy.aspx?bid=" + deal.BusinessHourGuid);
            return model;
        }

        public static List<DealCouponEntry> ConvertFrom(IViewPponDeal mainDeal)
        {
            List<DealCouponEntry> result = new List<DealCouponEntry>();
            result.Add(Create(mainDeal));
            return result;
        }

        public static List<DealCouponEntry> ConvertFrom(IList<ViewComboDeal> comboDeals)
        {
            List<DealCouponEntry> result = new List<DealCouponEntry>();
            foreach (var comboDeal in comboDeals)
            {
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(comboDeal.BusinessHourGuid);
                result.Add(Create(deal));
            }
            return result;
        }

        public string DealName { get; private set; }
        public string SubTitle { get; private set; }
        public string OrigPrice { get; private set; }
        public string Price { get; private set; }
        public string PriceDesc { get; private set; }
        public string DiscountString { get; private set; }
        public bool IsSoldOut { get; private set; }
        public bool IsExpiredDeal { get; private set; }
        public string BuyUrl { get; private set; }
    }

    public class MobileExpireNotice : IExpireNotice
    {
        /// <summary>
        /// 檔次類型
        /// </summary>
        public ExpireNoticeMode ControlMode { get; set; }

        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public ISysConfProvider SystemConfig
        {
            get { return cp; }
        }

        /// <summary>
        /// 賣家名稱
        /// </summary>
        public string SellerName { get; set; }

        /// <summary>
        /// 賣家結束營業日
        /// </summary>
        public DateTime? SellerCloseDownDate { get; set; }

        private IDictionary<string, DateTime> _storeCloseDownDates = new Dictionary<string, DateTime>();
        /// <summary>
        /// 分店結束營業日 - key: 分店名稱 value: 分店結束營業日
        /// </summary>
        public IDictionary<string, DateTime> StoreCloseDownDates
        {
            get { return _storeCloseDownDates; }
            set { _storeCloseDownDates = value; }
        }

        /// <summary>
        /// 憑證原始使用截止日
        /// </summary>
        public DateTime DealOriginalExpireDate { get; set; }

        /// <summary>
        /// 檔次調整截止日
        /// </summary>
        public DateTime? DealChangedExpireDate { get; set; }

        private IDictionary<string, DateTime> _storeChangedExpireDates = new Dictionary<string, DateTime>();
        /// <summary>
        /// 分店調整截止日 - key: 分店名稱 value: 分店調整過的截止日
        /// </summary>
        public IDictionary<string, DateTime> StoreChangedExpireDates
        {
            get { return _storeChangedExpireDates; }
            set { _storeChangedExpireDates = value; }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            // 無賣家/分店倒店  & 無檔次/分店截止日調整 
            if (NoNoticeNeeded())
            {
                return string.Empty;
            }

            #region 有賣家/分店倒店
            if (SellerCloseDownDate.HasValue || StoreCloseDownDates.Count != 0)
            {
                if (SellerCloseDownDate.HasValue)
                {
                    sb.AppendFormat("<span class='color_black'>{0}</span>於<span class='color_red'>{1}結束營業</span>", SellerName, SellerCloseDownDate.Value.ToString("yyyy/MM/dd"));
                    sb.Append(",");
                }
                else if (StoreCloseDownDates.Count > 0)
                {
                    foreach (KeyValuePair<string, DateTime> pair in StoreCloseDownDates)
                    {
                        sb.AppendFormat("<span class='color_black'>{0}{1}</span>於<span class='color_red'>{2}結束營業</span>", SellerName, pair.Key, pair.Value.ToString("yyyy/MM/dd"));
                        sb.Append(",");
                    }
                }

                sb.Append("，尚未使用憑證者，17Life將予以退費，請您連繫17Life客服中心或留意後續通知信件。");
            }

            #endregion

            #region 有檔次/分店截止日調整

            if (DealChangedExpireDate.HasValue || StoreChangedExpireDates.Count > 0)
            {

                #region 檔次截止時間調整

                if (DealChangedExpireDate.HasValue)
                {
                    if (DealChangedExpireDate.Value < DealOriginalExpireDate)
                    {
                        if (DealChangedExpireDate > DateTime.Now)
                        {
                            sb.AppendFormat("<span class='color_black'>{0}</span>於<span class='color_red'>{1}停止兌換</span>", SellerName, DealChangedExpireDate.Value.ToString("yyyy/MM/dd"));
                            sb.Append(",");
                        }
                    }
                    else
                    {
                        sb.AppendFormat("<span class='color_black'>{0}</span><span class='color_red'>使用期限延長至{1}</span>", SellerName, DealChangedExpireDate.Value.ToString("yyyy/MM/dd"));
                        sb.Append(",");
                    }
                }

                #endregion

                #region 分店截止時間提前

                Dictionary<string, DateTime> shortenedStores = StoreChangedExpireDates.Where(store => store.Value < DealOriginalExpireDate).ToDictionary(store => store.Key, store => store.Value);
                foreach (KeyValuePair<string, DateTime> pair in shortenedStores)
                {
                    if (pair.Value > DateTime.Now)
                    {
                        sb.AppendFormat("<span class='color_black'>{0}{1}</span>於<span class='color_red'>{2}停止兌換</span>", SellerName, pair.Key, pair.Value.ToString("yyyy/MM/dd"));
                        sb.Append(",");
                    }
                }

                #endregion

                #region 分店截止時間延長

                Dictionary<string, DateTime> extendedStores = StoreChangedExpireDates.Where(store => store.Value > DealOriginalExpireDate).ToDictionary(store => store.Key, store => store.Value);
                foreach (KeyValuePair<string, DateTime> pair in extendedStores)
                {
                    sb.AppendFormat("<span class='color_black'>{0}{1}</span><span class='color_red'>使用期限延長至{2}</span>", SellerName, pair.Key, pair.Value.ToString("yyyy/MM/dd"));
                    sb.Append(",");
                }
                sb.Append("請於使用期間內盡速至店家兌換好康。");
                #endregion
            }
            if (sb.Length > 0)
            {
                sb.Append("如有其他問題，請參考\"<a href='/Ppon/NewbieGuide.aspx'><span class='color_blue' style='text-decoration:underline'>常見問題</span></a>\"");
            }

            #endregion

            return sb.ToString();
        }

        private bool NoNoticeNeeded()
        {
            return !SellerCloseDownDate.HasValue && StoreCloseDownDates.Count == 0 && !DealChangedExpireDate.HasValue &&
                   StoreChangedExpireDates.Count == 0;
        }
    }
}

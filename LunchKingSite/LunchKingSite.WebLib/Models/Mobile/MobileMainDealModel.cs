﻿using System;
using System.Linq;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Models.Mobile
{
    public class MobileMainDealModel
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public static MobileMainDealModel Create(MultipleMainDealPreview deal, int cityId)
        {
            MobileMainDealModel result = new MobileMainDealModel();
            string title = string.IsNullOrEmpty(deal.PponDeal.AppTitle)
                ? deal.PponDeal.CouponUsage
                : deal.PponDeal.AppTitle;
            int pos = title.LastIndexOf('-');
            if (pos > 0)
            {
                result.Title = title.Substring(0, pos);
                result.Description = title.Substring(pos);
            }
            else
            {
                result.Title = title;
            }
            result.DiscountString = GetDiscountString(deal.PponDeal, cityId);

            result.Location = ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(
                null, deal.PponDeal.BusinessHourGuid);

            result.OrigPriceString = deal.PponDeal.ItemOrigPrice.ToString("F0");

            string priceTail = "元";
            if (Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, (int)(BusinessHourStatus.ComboDealMain)))
            {
                priceTail = "起";
            }
            var strPriceTail = "<span class='smalltext' style='display:inline-block'>" + priceTail + "</span>";

            result.DiscountPrice = deal.PponDeal.DiscountPrice == null ? string.Empty :
                deal.PponDeal.DiscountPrice.Value.ToString("0");

            result.DiscountPriceTail = (deal.PponDeal.ComboDelas != null && deal.PponDeal.ComboDelas.Count > 1) ? "起" : "元";

            if (cityId == CategoryManager.Default.Family.CategoryId && deal.PponDeal.ExchangePrice > 0)
            {
                result.PriceString = string.Format("${0:F0}", deal.PponDeal.ExchangePrice);
            }
            else
            {
                var price = PponFacade.CheckZeroPriceToShowPrice(deal.PponDeal, cityId).ToString("F0");
                if (string.IsNullOrEmpty(result.DiscountPrice) == false)
                {
                    result.PriceString = string.Format("$<span>{0}</span> {1}",
                        price, strPriceTail);
                }
                else
                {
                    result.PriceString = string.Format("<span class='color_red'>${0}</span> {1}",
                        price, strPriceTail);
                }
            }

            result.BuyerString = OrderedQuantityHelper.Show(deal.PponDeal, OrderedQuantityHelper.ShowType.InPortal);
            result.TagsString = PponFacade.RenderDealTagsAsMobileHtml(deal.PponDeal, 2);
            result.IsSoldOut = deal.PponDeal.OrderedQuantity >= deal.PponDeal.OrderTotalLimit;

            result.PicUrl = ImageFacade.GetMediaPathsFromRawData(deal.PponDeal.EventImagePath, MediaType.PponDealPhoto)
                .FirstOrDefault();
            if (string.IsNullOrEmpty(deal.PponDeal.AppDealPic) == false)
            {
                result.AppPicUrl = ImageFacade.GetMediaPathsFromRawData(deal.PponDeal.AppDealPic, MediaType.PponDealPhoto)
                    .FirstOrDefault();
            }
            if (string.IsNullOrEmpty(result.PicUrl))
            {
                result.PicUrl = config.SiteUrl + "/Themes/PCweb/images/ppon-M1_pic.jpg";
            }

            result.Url = deal.CityID > 0
                        ? String.Format("/deal/{1}?cid={0}", deal.CityID, deal.PponDeal.BusinessHourGuid)
                        : String.Format("/deal/{0}", deal.PponDeal.BusinessHourGuid);
            result.IsLastDay = PponFacade.DealHasLastDayTag(deal.PponDeal);
            result.EventImgUrl = deal.PponDeal.PromoImageHtml;
            result.ChannelAndRegionCategories = ViewPponDealManager.DefaultManager.GetCategoryIds(deal.PponDeal.BusinessHourGuid);
            if (deal.PponDeal.DeliveryType == (int)DeliveryType.ToShop)
            {
                result.RatingString = GetDealStarRatingMobile(deal.PponDeal.BusinessHourGuid);
            }
            else
            {
                result.RatingString = string.Empty;
            }
            result.EveryDayNewDeal = PponFacade.GetEveryDayNewDealFlag(deal.PponDeal.BusinessHourGuid);
            result.IsBankDeal = deal.PponDeal.IsBankDeal.GetValueOrDefault();

            return result;
        }

        private static string GetDealStarRatingMobile(Guid bid)
        {
            double starRating = 0;
            string srString = "";
            string srHeader = "";
            string srFooter = "</div>";
            string ratingString = "";
            bool showStar = false;

            if (config.StarRatingEnabled)
            {
                PponDealEvaluateStar evaAvg = ViewPponDealManager.DefaultManager.GetPponDealEvaluateStarList(bid);
                showStar = evaAvg != null;

                if (showStar)
                {
                    if (evaAvg.Cnt > 0)
                    {
                        srHeader = "<div class=\"rating\">";
                        starRating = (double)evaAvg.Star;
                        ratingString = "<span class=\"rating_count\">(" + evaAvg.Cnt.ToString() + "人)</span>";
                    }
                    else if (evaAvg.Cnt == 0)
                    {
                        srHeader = "<div class=\"rating insufficient\">";
                        starRating = 0;
                        ratingString = "<span class=\"rating_count\">(評價資料不足)</span>";
                    }
                }

                for (int i = 0; i < Math.Floor(starRating); i++)
                {
                    srString = srString + "<i class=\"fa fa-star\" aria-hidden=\"true\"></i> ";
                }
                if (starRating / 0.5 % 2 != 0)
                {
                    srString = srString + "<i class=\"fa fa-star-half-o\" aria-hidden=\"true\"></i> ";
                }
                for (int i = 0; i < Math.Floor(5 - starRating); i++)
                {
                    srString = srString + "<i class=\"fa fa-star-o\" aria-hidden=\"true\"></i> ";
                }

                if (showStar)
                {
                    if (evaAvg.Cnt > 0)
                    {
                        srString = srHeader + srString + ratingString + srFooter;
                    }
                    else if (evaAvg.Cnt == 0)
                    {
                        srString = srHeader + ratingString + srFooter;
                    }
                }
                else
                {
                    srString = "";
                }
            }
            else
            {
                srString = "";
            }

            return srString;
        }

        public static string GetDiscountString(IViewPponDeal deal, out bool hasDiscount)
        {
            string result = string.Empty;
            hasDiscount = false;
            if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal))
            {
                result = "公益";
            }
            else if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown)
                     || Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice))
            {
                result = "特選";
            }
            else if (deal.ItemPrice == 0)
            {
                if (deal.CategoryIds.Contains(CategoryManager.Default.Family.CategoryId) &&
                    deal.ExchangePrice.HasValue && deal.ExchangePrice.Value > 0)
                {
                    if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown))
                    {
                        result = "特選";
                    }
                    else
                    {
                        result = ViewPponDealManager.GetDealDiscountString(
                            Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown),
                            PponFacade.CheckZeroPriceToShowPrice(deal), deal.ItemOrigPrice);
                        hasDiscount = true;
                    }
                }
                else
                {
                    result = "優惠";
                }
            }
            else
            {
                string discount = ViewPponDealManager.GetDealDiscountString(
                    Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown),
                    PponFacade.CheckZeroPriceToShowPrice(deal), deal.ItemOrigPrice);
                int length = discount.Length;
                if (length > 0)
                {
                    result = discount.Substring(0, 1);
                    if (discount.IndexOf('.') != -1 && length > 2)
                    {
                        result += discount.Substring(1, length - 1);
                    }

                    hasDiscount = true;
                }
            }
            return result;
        }

        private static string GetDiscountString(IViewPponDeal deal, int cityId)
        {
            string result = string.Empty;
            if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal))
            {
                result = "公益";
            }
            else if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown)
                  || Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice))
            {
                result = "特選";
            }
            else if (deal.ItemPrice == 0)
            {
                if (cityId == PponCityGroup.DefaultPponCityGroup.Family.CityId &&
                    deal.ExchangePrice.HasValue && deal.ExchangePrice.Value > 0)
                {
                    if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown))
                    {
                        result = "特選";
                    }
                    else
                    {
                        result = ViewPponDealManager.GetDealDiscountString(
                            Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown),
                            PponFacade.CheckZeroPriceToShowPrice(deal), deal.ItemOrigPrice);
                        result += "<span class=\"smalltext\">折</span>";
                    }
                }
                else
                {
                    result = "優惠";
                }


            }
            else
            {
                string discount = ViewPponDealManager.GetDealDiscountString(
                    Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown),
                    PponFacade.CheckZeroPriceToShowPrice(deal), deal.ItemOrigPrice);
                int length = discount.Length;
                if (length > 0)
                {
                    result = discount.Substring(0, 1);
                    if (discount.IndexOf('.') != -1 && length > 2)
                    {
                        result += discount.Substring(1, length - 1);
                    }

                    result += "<span class=\"smalltext\">折</span>";
                }
            }
            return result;
        }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("desc")]
        public string Description { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("discount")]
        public string DiscountString { get; set; }

        [JsonProperty("oprice")]
        public string OrigPriceString { get; set; }

        [JsonProperty("price")]
        public string PriceString { get; set; }

        [JsonProperty("buyer")]
        public string BuyerString { get; set; }

        [JsonProperty("tags")]
        public string TagsString { get; set; }

        [JsonProperty("soldout")]
        public bool IsSoldOut { get; set; }

        [JsonProperty("pic")]
        public string PicUrl { get; set; }

        [JsonProperty("apppic")]
        public string AppPicUrl { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("lastday")]
        public bool IsLastDay { get; set; }

        [JsonProperty("eventimgurl")]
        public string EventImgUrl { get; set; }

        [JsonProperty("everydaynewdeal")]
        public string EveryDayNewDeal { get; set; }

        /// <summary>
        /// 該Bid擁有的categoryId (type為頻道或區域的)
        /// </summary>
        [JsonProperty("dealcategories")]
        public List<int> ChannelAndRegionCategories { get; set; }
        public string RatingString { get; set; }
        public bool IsBankDeal { get; set; }
        /// <summary>
        /// 使用折價卷後的均價
        /// </summary>
        public string DiscountPrice { get; private set; }

        /// <summary>
        /// 使用折價卷後，顯示起or元
        /// </summary>
        public string DiscountPriceTail { get; private set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;

namespace LunchKingSite.WebLib.Models.Mobile
{
    public class MobileManager
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        /// <summary>
        /// 取得當前(最後選取)城市區域
        /// </summary>
        /// <param name="channelId">頻道channelId</param>
        /// <returns>未傳參數取得「美食」CityId，傳入channelId取得「旅遊、完美」AreaId</returns>
        public static int GetCurrentCityId(int? channelId = 0)
        {
            HttpCookie cookie = null;

            if (channelId == 0 || channelId == CategoryManager.Default.PponDeal.CategoryId)
            {
                cookie = HttpContext.Current.Request.Cookies.Get(LkSiteCookie.CityId.ToString("g"));
                int cityid;
                if (cookie != null && int.TryParse(cookie.Value, out cityid))
                    return cityid;
            }
            else
            {
                if (channelId == CategoryManager.Default.Travel.CategoryId)
                {
                    cookie = HttpContext.Current.Request.Cookies.Get(LkSiteCookie.TravelCategoryId.ToString("g"));
                }
                else if (channelId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CategoryId)
                {
                    cookie = HttpContext.Current.Request.Cookies.Get(LkSiteCookie.FemaleCategoryId.ToString("g"));
                }

                int areaId;
                if (cookie != null && int.TryParse(cookie.Value, out areaId))
                {
                    return areaId;
                }
            }

            return 0;
        }

        /// <summary>
        /// 有辦法從中正區找到台北
        /// </summary>
        /// <param name="areaId"></param>
        /// <returns></returns>
        public static int? GetCityIdByCategoryId(int areaId)
        {
            CategoryNode node = CategoryManager.GetCategoryNodeByCategoryId(areaId);
            if (node == null)
            {
                return null;
            }
            while (true)
            {
                if (node.CityId != null)
                {
                    return node.CityId;
                }
                if (node.Parent == null || node.Parent.Parent == null)
                {
                    break;
                }
                node = node.Parent.Parent;
            }
            return null; ;
        }

        public static CategoryNode GetMainAreaCategoryById(int categoryId)
        {
            CategoryNode node = CategoryManager.GetCategoryNodeByCategoryId(categoryId);
            if (node == null)
            {
                return null;
            }
            while (true)
            {
                if (node.CityId != null)
                {
                    return node;
                }
                if (node.Parent == null || node.Parent.Parent == null)
                {
                    break;
                }
                if (node.Parent.Parent.CategoryId == CategoryManager.Default.Beauty.CategoryId)
                {
                    return node;
                }
                node = node.Parent.Parent;
            }
            return null; ;
        }

        public static void SetCookeis(int channelId, int areaId, HttpResponseBase response)
        {
            if (channelId == CategoryManager.Default.PponDeal.CategoryId)
            {
                int? cityId = MobileManager.GetCityIdByCategoryId(areaId);
                if (cityId != null)
                {
                    SetCookie(cityId.ToString(), LkSiteCookie.CityId, response);
                }
            }
            else if (channelId == CategoryManager.Default.Travel.CategoryId)
            {
                CategoryNode cityCategory = MobileManager.GetMainAreaCategoryById(areaId);
                if (cityCategory != null)
                {
                    SetCookie(cityCategory.CategoryId.ToString(), LkSiteCookie.TravelCategoryId, response);
                }
            }
            else if (channelId == CategoryManager.Default.Beauty.CategoryId)
            {
                CategoryNode cityCategory = MobileManager.GetMainAreaCategoryById(areaId);
                if (cityCategory != null)
                {
                    SetCookie(cityCategory.CategoryId.ToString(), LkSiteCookie.FemaleCategoryId, response);
                }
            }
        }

        public static void RegisterReferrerSource(string rsrc)
        {
            HttpResponse response = HttpContext.Current.Response;
            HttpRequest request = HttpContext.Current.Request;
            if (!string.IsNullOrEmpty(rsrc))
            {
                try
                {
                    IMarketingProvider mp = ProviderFactory.Instance().GetProvider<IMarketingProvider>();
                    ReferralCampaign rc = mp.ReferralCampaignGetByRsrc(rsrc);
                    if (rc == null)
                    {
                        return;
                    }

                    string cpaKey = CookieManager.GetCpaKey();
                    if (string.IsNullOrEmpty(cpaKey))
                    {
                        cpaKey = Guid.NewGuid().ToString();
                        CookieManager.SetCpaKey(cpaKey, rc.SessionDuration);
                    }
                    else if (rc.IsExternal) //若是外部導購商則重置cookie時間
                    {
                        CookieManager.SetCpaKey(cpaKey, rc.SessionDuration);
                    }
                    //TODO 這邊可以改成用 MessageQueue 來處理
                    AddCpaTracking(rsrc, cpaKey);

                    ReSetReferrerSourceIdCookie(request, rc, response);

                    //記錄限時搶購的點擊
                    if (config.EnableRushBuy && rsrc == "17_rushbuy")
                    {
                        AddRushBuyTracking(request, cpaKey);
                    }
                }
                catch (Exception ex)
                {
                    WebUtility.LogExceptionAnyway(ex);
                }
            }
        }

        private static void AddRushBuyTracking(HttpRequest request, string cpaKey)
        {
            var bid = request.Url.Segments[2];
            var currentUser = HttpContext.Current.User;
            var userId = currentUser == null ? 0 : MemberFacade.GetUniqueId(currentUser.Identity.Name);
            var url = HttpContext.Current != null
                ? HttpContext.Current.Request.Url.ToString()
                : string.Empty;
            MarketingFacade.AddRushBuyTracking(cpaKey, bid, userId, url);
        }

        private static void ReSetReferrerSourceIdCookie(HttpRequest request, ReferralCampaign rc, HttpResponse response)
        {
            HttpCookie cookie = CookieManager.NewCookie(LkSiteCookie.ReferrerSourceId.ToString());
            cookie = request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()];

            if (cookie == null || string.IsNullOrEmpty(cookie.Value))
            {
                cookie = CookieManager.NewCookie(LkSiteCookie.ReferrerSourceId.ToString());
                if (rc.SessionDuration > 0)
                {
                    cookie.Expires = DateTime.Now.AddSeconds(rc.SessionDuration);
                }

                cookie.Value = rc.Rsrc;
                response.SetCookie(cookie);
            }
        }

        private static void AddCpaTracking(string rsrc, string cpaKey)
        {
            var currentUser = HttpContext.Current.User;
            var userId = currentUser == null ? 0 : MemberFacade.GetUniqueId(currentUser.Identity.Name);
            var url = HttpContext.Current != null
                ? HttpContext.Current.Request.Url.ToString()
                : string.Empty;
            MarketingFacade.TryAddCpaTracking(cpaKey, rsrc, userId, url);
        }

        public static string GetReferrerSource()
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies.Get(LkSiteCookie.ReferrerSourceId.ToString());
            if (cookie != null)
            {
                return cookie.Value;
            }
            return "";
        }
        public static void SetCookie(string value, LkSiteCookie cookieField, HttpResponseBase response)
        {
            HttpCookie referenceCookie = CookieManager.NewCookie(cookieField.ToString("g"));
            referenceCookie.Value = value;
            referenceCookie.Expires = DateTime.Now.AddDays(1);
            response.SetCookie(referenceCookie);
        }

        public List<MultipleMainDealPreview> SetMultipleDeals(int _categoryId, int workCityId, int subRegionCategoryId,
            List<int> filterCategoryIdList)
        {
            int? selectedCategoryId = null;
            int? selectedSubRegionId = null;
            // 使用多主檔排列
            if (_categoryId > 0)
            {
                selectedCategoryId = _categoryId;
            }

            List<MultipleMainDealPreview> multideals;

            //取得目前城市
            PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(workCityId);
            //目前城市沒有對應的頻道，目前應該只有24或72小時會這樣，直接設定為台北
            if (!city.ChannelId.HasValue)
            {
                city = PponCityGroup.DefaultPponCityGroup.TaipeiCity;
            }
            //需依據不同的狀況設定區域的categoryId，主要是針對旅遊需另外判斷
            int? areaCategoryId = null;
            //判斷是否為旅遊頻道
            if (workCityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
            {
                areaCategoryId = CategoryManager.Default.Travel.CategoryId;
            }
            else if (workCityId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId)
            {
                areaCategoryId = CategoryManager.Default.FemaleTaipei.CategoryId;
            }
            else
            {
                areaCategoryId = city.ChannelAreaId;
            }

            List<int> categorylist = new List<int>();

            if (selectedCategoryId.HasValue)
            {
                categorylist.Add(selectedCategoryId.Value);
            }

            if (subRegionCategoryId > 0)
            {
                categorylist.Add(subRegionCategoryId);
                selectedSubRegionId = subRegionCategoryId;
            }

            multideals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria(
                city.ChannelId.Value, areaCategoryId, categorylist);

            if (filterCategoryIdList.Count > 0)
            {
                multideals = multideals.Where(x => filterCategoryIdList.Any(y => x.DealCategoryIdList.Contains(y) ||
                    (y == CategoryManager.Default.LastDay.CategoryId && (new TimeSpan(x.PponDeal.BusinessHourOrderTimeE.Ticks - DateTime.Now.Ticks)).TotalDays < 1))).ToList();
            }

            List<MultipleMainDealPreview> areamultideals;
            if (selectedCategoryId.HasValue)
            {
                areamultideals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria(city.ChannelId.Value, areaCategoryId, new List<int>());
            }
            else
            {
                areamultideals = multideals;
            }
            //採用新版分類
            PponCity matchCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(workCityId);
            if (matchCity.ChannelId.HasValue)
            {
                CategoryDealCountNode node = PponDealPreviewManager.GetCategoryDealCountNodeByChannel(matchCity.ChannelId.Value,
                                                                             areaCategoryId, selectedSubRegionId);
                //新增一項預設
                var dealCount = new CategoryDealCount(0, "全部");
                dealCount.DealCount = node.TotalCount;
                //篩選的部分 (318 最後一天)
                List<CategoryDealCount> filterNodes = PponDealPreviewManager.GetSpecialDealCountListByChannel(
                    matchCity.ChannelId.Value, matchCity.ChannelAreaId)
                    .Where(x => x.DealCount > 0)
                    .Where(x => x.CategoryId != 138 && x.CategoryId != 139 && x.CategoryId != 318).ToList()
                    .ToList();
                node.DealCountList.Insert(0, dealCount);

                var categoryDealCountList = PponFacade.SubCategoryDealCountByMainCategoryDealCount(node.DealCountList.Where(x => x.DealCount > 0 || x.CategoryId.Equals(0)).ToList(), areamultideals);

                SystemCodeCollection piinlifeLinkes = SystemCodeManager.GetSystemCodeListByGroup("17PiinlifeLink" + city.ChannelId);

                //View.SetMultipleCategories(categoryDealCountList, filterNodes, piinlifeLinkes);
            }
            else
            {
                //對應的PponCity未設定ChannelId，依據現況，表示並無DealCategory資料需要顯示
                /*
                View.SetMultipleCategories(
                    new List<KeyValuePair<CategoryDealCount, List<CategoryDealCount>>>(),
                    new List<CategoryDealCount>(), 
                    new SystemCodeCollection());
                 */
            }
            return multideals;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channelOrCityId">這邊的 channelOrCityId 用的是 category 的 id, channedId 或是 cityId</param>
        /// <param name="categoryId"></param>
        /// <param name="filterCategoryIdList"></param>
        /// <returns></returns>
        public static List<MultipleMainDealPreview> GetMultipleDeals(int channelId, int? areaId, int categoryId,
            List<int> filterCategoryIdList)
        {
            List<MultipleMainDealPreview> multideals;

            List<int> categorylist = new List<int>();
            categorylist.Add(channelId);
            if (areaId != null && areaId != 0)
            {
                categorylist.Add((int)areaId);
            }
            if (categoryId != 0)
            {
                categorylist.Add(categoryId);
            }

            int? cityId = GetCityIdByCategoryId(areaId.GetValueOrDefault());

            PponCity poponCity = null;
            if (cityId != null)
            {
                if (channelId == CategoryManager.Default.Beauty.CategoryId)
                {
                    poponCity = PponCityGroup.GetPponCityByChannel(channelId, areaId);
                }
                else
                {
                    poponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId((int)cityId);
                }
            }

            if (channelId == PponDealPreviewManager._MAIN_THEME_CATEGORY_ID)
            {
                //主題活動的虛擬頻道，由APP自行切換頁面，API回傳檔次資料不作用
                multideals = new List<MultipleMainDealPreview>();
            }
            else
            {
                multideals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria(
                    poponCity, categorylist);
            }

            //檔次依特定條件過瀘，如24h, 72h etc.
            if (filterCategoryIdList != null && filterCategoryIdList.Count > 0)
            {
                multideals = multideals.Where(
                    x => filterCategoryIdList.Any(y => x.DealCategoryIdList.Contains(y) ||
                                                   (y == CategoryManager.Default.LastDay.CategoryId &&
                                                    (new TimeSpan(x.PponDeal.BusinessHourOrderTimeE.Ticks -
                                                                  DateTime.Now.Ticks)).TotalDays < 1))).ToList();
            }

            ISysConfProvider config = ProviderFactory.Instance().GetConfig();
            if (config.IsEveryDayNewDeals)
            {
                //每日下殺分類篩選
                List<MultipleMainDealPreview> newdealList = multideals.ToList();
                int everyDayNewDealsCategoryId = config.EveryDayNewDealsCategoryId;
                if (everyDayNewDealsCategoryId != 0)
                {
                    if (categoryId == everyDayNewDealsCategoryId)
                    {
                        foreach (var deal in newdealList)
                        {
                            if (PponFacade.GetEveryDayNewDealFlag(deal.PponDeal.BusinessHourGuid) == "")
                            {
                                multideals.Remove(deal);
                            }
                        }
                    }
                }
            }
            return multideals;
        }


        public List<MobileMainDealModel> GetMobileMainDeals(IEnumerable<MultipleMainDealPreview> multiplemaindeals, int workCityId)
        {
            List<MobileMainDealModel> result = new List<MobileMainDealModel>();
            foreach (var deal in multiplemaindeals)
            {
                //result.Add(MobileMainDealModel.Create(deal, workCityId));
                //先簡易cache處理
                //等待ViewPponDealManager與 PponDealPreviewManager整合在一起，
                //將 MobileMainDealModel 設為 MultipleMainDealPreview 的一個屬性，一起生成快取資料
                string key = string.Format("{0}.{1}", workCityId, deal.PponDeal.BusinessHourGuid);
                MobileMainDealModel mmdeal = MemoryCache2.Get<MobileMainDealModel>(key);
                if (mmdeal == null)
                {
                    mmdeal = MobileMainDealModel.Create(deal, workCityId);
                    MemoryCache2.Set<MobileMainDealModel>(key, mmdeal, 3);
                }
                result.Add(mmdeal);
            }
            return result;
        }

        public List<MultipleMainDealPreview> SetMultipleTodayDeals(int workCityId, List<int> categories)
        {
            List<MultipleMainDealPreview> multideals = ViewPponDealManager.DefaultManager.MultipleTodayDealPreviewGetListByCategoryCriteria(
                workCityId, categories);

            return multideals;
        }
        public List<MultipleMainDealPreview> SetMultipleLastDeals(int workCityId, List<int> categories)
        {
            List<MultipleMainDealPreview> multideals = ViewPponDealManager.DefaultManager.MultipleLastDealPreviewGetListByCategoryCriteria(
                workCityId, categories);

            return multideals;
        }

        public class SlidingMenuItem
        {
            public string label;
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int? refId;
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string href;
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string desc;
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public List<SlidingMenuItem> children;

            public override string ToString()
            {
                return this.label;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="areaNode"></param>
        /// <param name="removeFirstItem">是否移掉第1個自動產生的"全部"項目</param>
        /// <returns></returns>
        public static List<SlidingMenuItem> ConvertToSlidingMenuItems(ApiCategoryTypeNode areaNode)
        {
            if (areaNode == null)
            {
                return new List<SlidingMenuItem>();
            }

            List<SlidingMenuItem> items = items = new List<SlidingMenuItem>();
            PopulateSlidingMenu(items, areaNode);

            return items;
        }

        public static string ConvertToSlidingMenuDataJson(ApiCategoryTypeNode areaNode)
        {
            List<SlidingMenuItem> items = ConvertToSlidingMenuItems(areaNode);
            return ProviderFactory.Instance().GetSerializer().Serialize(items);
        }

        private static void PopulateSlidingMenu(List<SlidingMenuItem> items, ApiCategoryTypeNode parentTypeNode)
        {
            if (parentTypeNode == null)
            {
                return;
            }

            foreach (ApiCategoryNode node in parentTypeNode.CategoryNodes)
            {
                var smitem = new SlidingMenuItem
                {
                    label = node.CategoryName
                };
                if (node.CategoryName != node.ShortName)
                {
                    smitem.desc = node.ShortName;
                }
                //還在第2層不檢查，第2層譬如 台北、桃園
                if (node.ExistsDeal == false)
                {
                    continue;
                }
                items.Add(smitem);
                if (node.NodeDatas.Count > 0)
                {
                    foreach (var typeNode in node.NodeDatas)
                    {
                        smitem.children = new List<SlidingMenuItem>();
                        PopulateSlidingMenu(smitem.children, typeNode);
                    }
                }
                else
                {
                    //smitem.href = "/m/" + node.CityId;
                    smitem.refId = node.CategoryId;
                }
            }
        }
    }


    public class MenuChannelManager
    {
        private static List<MenuChannel> menuChannels = new List<MenuChannel>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        static MenuChannelManager()
        {
            menuChannels.Add(new MenuChannel
            {
                Title = "美食",
                Url = "/m/food",
                RwdUrl = "/channel/" + (int)ChannelCategory.Food,
                Icon = "/content/img/web_19/channel_restaurant.png",
                IsDeal = true,
                CityId = null,
                NaviId = "m_navIndex",
                CahnnelId = CategoryManager.Default.PponDeal.CategoryId
            });
            menuChannels.Add(new MenuChannel
            {
                Title = "宅配購物",
                Url = "/m/" + PponCityGroup.DefaultPponCityGroup.AllCountry.CityId,
                RwdUrl = "/channel/" + (int)ChannelCategory.Delivery,
                Icon = "/content/img/web_19/channel_deliver.png",
                IsDeal = true,
                CityId = PponCityGroup.DefaultPponCityGroup.AllCountry.CityId,
                NaviId = "m_navDelivery",
                CahnnelId = CategoryManager.Default.Delivery.CategoryId
            });

            menuChannels.Add(new MenuChannel
            {
                Title = "旅遊",
                Url = "/m/" + PponCityGroup.DefaultPponCityGroup.Travel.CityId,
                RwdUrl = "/channel/" + (int)ChannelCategory.Travel,
                Icon = "/content/img/web_19/channel_travel.png",
                IsDeal = true,
                CityId = PponCityGroup.DefaultPponCityGroup.Travel.CityId,
                NaviId = "m_navTravel",
                CahnnelId = CategoryManager.Default.Travel.CategoryId
            });
            menuChannels.Add(new MenuChannel
            {
                Title = "全家",
                Url = "/m/" + PponCityGroup.DefaultPponCityGroup.Family.CityId,
                RwdUrl = "/channel/" + (int)ChannelCategory.Family,
                Icon = "/content/img/web_19/channel_fami.png",
                IsDeal = true,
                CityId = PponCityGroup.DefaultPponCityGroup.Family.CityId,
                NaviId = "m_navFamily",
                CahnnelId = CategoryManager.Default.Family.CategoryId
            });
            menuChannels.Add(new MenuChannel
            {
                Title = "玩美‧休閒",
                Url = "/m/" + PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId,
                RwdUrl = "/channel/" + (int)ChannelCategory.Beauty,
                Icon = "/content/img/web_19/channel_beauty.png",
                IsDeal = true,
                CityId = PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId,
                NaviId = "m_navBeauty",
                CahnnelId = CategoryManager.Default.Beauty.CategoryId
            });
            menuChannels.Add(new MenuChannel
            {
                Title = "即買即用",
                Url = "/m/FastDeals",
                RwdUrl = "/m/FastDeals",
                Icon = "/content/img/web_19/channel_quick.png",
                NaviId = "m_navFastDeals"
            });
            menuChannels.Add(new MenuChannel
            {
                Title = "品生活",
                Url = "/piinlife/default.aspx",
                RwdUrl = "/piinlife/default.aspx",
                Icon = "/content/img/web_19/channel_life.png",
                NaviId = "m_naviPiinLife",
                CahnnelId = CategoryManager.Default.Piinlife.CategoryId
            });
            menuChannels.Add(new MenuChannel
            {
                Title = "主題活動",
                Url = "/Event/ThemeCurationChannelMobile",
                RwdUrl = "/Event/ThemeCurationChannel",
                Icon = "/content/img/web_19/channel_campaign.png",
                NaviId = "m_navThemeChannel"
            });
        }


        public static ReadOnlyCollection<MenuChannel> GetMenuChannels()
        {
            return new ReadOnlyCollection<MenuChannel>(menuChannels);
        }

        public static string GetCategoryUrl(int? cityId, int areacategoryId)
        {
            string categoryUrl = Helper.CombineUrl(config.SiteUrl,
                string.Format("m/{0}", cityId));
            return categoryUrl;
        }

        public static string GetPponChannelImage(int categoryId)
        {
            switch (categoryId)
            {
                case 88:
                    return Helper.CombineUrl(config.SiteUrl, "Themes/PCweb/images/menu-channel-icon-2.png");
                case 89:
                    return Helper.CombineUrl(config.SiteUrl, "Themes/PCweb/images/menu-channel-icon-4.png");
                case 90:
                    return Helper.CombineUrl(config.SiteUrl, "Themes/PCweb/images/menu-channel-icon-3.png");
                case 87:
                    return Helper.CombineUrl(config.SiteUrl, "Themes/PCweb/images/menu-channel-icon-1.png");
                default:
                    return "";
            }
        }

    }

    public class MenuChannel
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string RwdUrl { get; set; }
        public string Icon { get; set; }
        public int? CityId { get; set; }
        public bool IsDeal { get; set; }
        public string NaviId { get; set; }
        public int? CahnnelId { get; set; }


        public override string ToString()
        {
            return Title;
        }
    }

}

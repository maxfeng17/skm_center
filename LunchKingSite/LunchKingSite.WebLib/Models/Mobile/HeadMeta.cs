﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.WebLib.Models.Mobile
{
    public class HeadMeta
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public string Description
        {
            get { return "17Life-吃喝玩樂團購3折起，全家便利商店5折專區免費索取兌換！全台美食餐券、旅遊住宿、宅配24H快速到貨、SPA專區，還有品生活獨享的頂級精品、用餐優惠，即買即用，盡在17Life與你一起享受生活！"; }
        }

        public string Keywords
        {
            get { return "17Life,團購,團購網站,團購美食,美食團購,美食餐廳,即買即用,餐券,優惠券,優惠,好康,折扣,台灣旅遊,SPA,線上購物"; }
        }

        public string Copyright
        {
            get { return "17Life"; }
        }

        public string FbAppId
        {
            get
            {
                return config.FacebookApplicationId;
            }
        }

        /*
        public string OgType
        {
            get { return "website"; }
        }

        public string OgTitle
        {
            get { return ""; }
        }

        public string OgUrl
        {
            get { return ""; }
        }

        public string OgSiteName
        {
            get { return "17Life"; }
        }

        public string OgDescription
        {
            get
            {
                return "";
            }
        }

        public string OgImage
        {
            get { return ""; }
        }
         */ 

        public string iOSAppId
        {
            get
            {
                return config.iOSAppId;
            }
        }

        public string AndroidAppId
        {
            get
            {
                return config.AndroidAppId;
            }
        }

    }
}

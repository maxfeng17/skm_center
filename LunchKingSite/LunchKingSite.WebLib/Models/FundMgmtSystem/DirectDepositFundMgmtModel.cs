﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.FundMgmtSystem
{
    public class DirectDepositFundMgmtModel
    {
        public BusinessModel Biz { get; set; }

        public bool IsDealSelected { get; set; }

        /// <summary>
        /// search condition: 檔號
        /// </summary>
        public int? DealId { get; set; }

        public string BillCreator { get; set; }

        /// <summary>
        /// search condition: 核銷日(起)
        /// </summary>
        public DateTime? Vts { get; set; }
        /// <summary>
        /// search condition: 核銷日(訖)
        /// </summary>
        public DateTime? Vte { get; set; }

        /// <summary>
        /// search condition: 單據建立時間(起)
        /// </summary>
        public DateTime? Cts { get; set; }
        /// <summary>
        /// search condition: 單據建立時間(訖)
        /// </summary>
        public DateTime? Cte { get; set; }

        public TrxCriteria TrxCriteria { get; set; }

        public bool ShowSysMessage { get; set; }

        public IEnumerable<DirectDepositFundTransferInfo> Transfers { get; set; }
    }

    public class DirectDepositFundTransferInfo
    {
        public int BalanceSheetId { get; set; }
        public int DealId { get; set; }
        public string StoreName { get; set; }
        public DateTime BalanceSheetIntervalStart { get; set; }
        public DateTime BalanceSheetIntervalEnd { get; set; }
        public int AccountsPayable { get; set; }        
        public int? AccountsPaid { get; set; }
        public DateTime? TransferTime { get; set; }
        public string Memo { get; set; }
        public bool IsMemoEditable { get; set; }
        public string ReceiverTitle { get; set; }
        public string ReceiverCompanyId { get; set; }
        public string ReceiverBankNo { get; set; }
        public string ReceiverBranchNo { get; set; }
        public string ReceiverAccountNo { get; set; }
        public string PaymentState { get; set; }
        public string TypeDesc { get; set; }
        public int? PaymentId { get; set; }
    }
}

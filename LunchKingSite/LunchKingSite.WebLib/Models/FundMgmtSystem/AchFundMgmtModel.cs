﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Models.FundMgmtSystem
{
    public class AchFundMgmtModel
    {
        public BusinessModel Biz { get; set; }
        /// <summary>
        /// search condition: 檔號
        /// </summary>
        public int? Uid { get; set; }
        /// <summary>
        /// search condition: 核銷日(起)
        /// </summary>
        public DateTime? Vts { get; set; }
        /// <summary>
        /// search condition: 核銷日(訖)
        /// </summary>
        public DateTime? Cte { get; set; }
        /// <summary>
        /// search condition: 單據建立時間(起)
        /// </summary>
        public DateTime? Cts { get; set; }
        /// <summary>
        /// search condition: 單據建立時間(訖)
        /// </summary>
        public DateTime? Vte { get; set; }
        public IEnumerable<FundTransferMgmtInfo> Transfers { get; set; }

        public TrxCriteria TrxCriteria { get; set; }

        /// <summary>
        /// 修改結果
        /// </summary>
        public bool? IsModificationSuccess { get; set; }
    }

    public enum TrxCriteria
    {
        All,
        /// <summary>
        /// 匯款成功
        /// </summary>
        Completed,
        /// <summary>
        /// 其他匯款狀態
        /// </summary>
        Others
    }

    public class FundTransferMgmtInfo
    {
        public int DealId { get; set; }
        public Guid DealGuid { get; set; }
        public string StoreName { get; set; }
        public DateTime? BalanceSheetIntervalStart { get; set; }
        public DateTime? BalanceSheetIntervalEnd { get; set; }
        /// <summary>
        /// 應付總額 ( = 對帳單金額)
        /// </summary>
        public decimal AccountsPayable { get; set; }
        /// <summary>
        /// 實付總額 ( = 匯款金額)
        /// </summary>
        public decimal? AccountsPaid { get; set; }
        public int PaymentId { get; set; }
        public DateTime? TransferTime { get; set; }
        public string ReceiverTitle { get; set; }
        public string ReceiverCompanyId { get; set; }
        public string ReceiverBankNo { get; set; }
        public string ReceiverBranchNo { get; set; }
        public string ReceiverAccountNo { get; set; }
        public FundTransferType TransferType { get; set; }
        public string Memo { get; set; }
        
        /// <summary>
        /// 實付總額可否修改
        /// </summary>
        public bool IsAccountsPaidModifiable { get; set; }
        
        /// <summary>
        /// 匯款時間可否修改
        /// </summary>
        public bool IsTransferTimeModifiable { get; set; }
        
        /// <summary>
        /// 付款方式可否修改
        /// </summary>
        public bool IsTransferTypeModifiable { get; set; }

        /// <summary>
        /// 付款狀態, Ach 失敗原因
        /// </summary>
        public string Information { get; set; }

        /// <summary>
        /// 是否顯示 "付款完成" 的控制項 (讓財會標註人工匯款已完成)
        /// </summary>
        public bool IsTransferCompleteModifiable { get; set; }
    }

    public class FundModificationRequest
    {
        //todo: move to BizLogic\DTO ? 

        public int PayId { get; set; }
        public decimal? AccountsPaid { get; set; }
        public DateTime? TransferTime { get; set; }
        public FundTransferType TransferType { get; set; }
        public string Memo { get; set; }
        public string ReceiverTitle { get; set; }
        public string ReceiverCompanyId { get; set; }
        public string ReceiverBankNo { get; set; }
        public string ReceiverBranchNo { get; set; }
        public string ReceiverAccountNo { get; set; }
        public bool? IsTransferComplete { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Models.WebViewOrder
{
    public class ExchangeApplicationModel
    {
        public Guid OrderGuid { get; set; }
        public string ExchangeReason { get; set; }
        public string Message { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverAddress { get; set; }
        public bool IsCoupon { get; set; }
    }
}

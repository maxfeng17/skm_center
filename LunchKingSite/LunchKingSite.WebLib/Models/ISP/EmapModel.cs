﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Models.ISP
{
    public class SevenEmapModel
    {
        /// <summary>
        /// 7-11 tempvar
        /// </summary>
        public string TempVar { get; set; }
        /// <summary>
        /// 母廠商代號
        /// </summary>
        public string Uid { get; set; }
        /// <summary>
        /// 子廠商代號
        /// </summary>
        public string Eshopid { get; set; }
        /// <summary>
        /// return 17Life url
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 消費者過去店取ID
        /// </summary>
        public string StoreId { get; set; }
        /// <summary>
        /// page: PC / touch: mobile
        /// </summary>
        public string Display { get; set; }
        /// <summary>
        /// Seven emap url.
        /// </summary>
        public string EmapUrl { get; set; }
        /// <summary>
        /// Site Url
        /// </summary>
        public string SiteUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool RedirectSevenMap { get; set; }
    }
}

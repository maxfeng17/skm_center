﻿using LunchKingSite.Core;
using LunchKingSite.BizLogic.Component;
namespace LunchKingSite.WebLib.Models.Ppon
{
    /// <summary>
    /// 排序依據 : 推薦、最新、銷量、折數、價格
    /// </summary>
    public class ViewCategorySortTypes
    {
        public CategorySortType CategorySortType { get; set; }
        public string Name { get; set; }
        public string CssClass { get; set; }
        public string Title { get; set; }
    }

    /// <summary>
    /// 左側Deal : 主要檔次
    /// </summary>
    public class ViewMultipleMainDeal
    {
        public MultipleMainDealPreview deal { get; set;}
        public string clit_CityName { get; set; }
        public string clit_CityName2 { get; set; }
        public string litCountdownTime { get; set; }
        public string litDays { get; set; }
        public string timerTitleField { get; set; }
        public string discount_2 { get; set; }
        public string discount_1 { get; set; }
        public string deal_Label_1 { get; set; }
        public string deal_Label_2 { get; set; }
        public string litInstallmentPriceLabel { get; set; }
        public long timerTitleField_dataEnd { get; set; }
        public long timerTitleField_dataStart { get; set; }
        public long timerTitleField_dataeverydaynewdealend { get; set; }
        public bool timerTitleField_everydaynewdeal { get; set; }        
        public string EveryDayNewDeal { get; set; }

        #region 評價

        /// <summary>
        /// 組出html 
        /// </summary>
        public string RatingString { get; set; }
        public int RatingNumber { get; set; }
        public decimal RatingScore { get; set; }
        public string RatingScoreStr
        {
            get
            {
                return RatingScore.ToString("0.#");
            }
        }
        public bool RatingShow { get; set; }

        #endregion
    }

    /// <summary>
    /// 右側Deal : 今日熱銷、最新上檔、最後倒數
    /// </summary>
    public class ViewMultipleSideDeal
    {
        public MultipleMainDealPreview deal { get; set; }
        public string clit_CityName2 { get; set; }
        public string discount_1 { get; set; }
        public string EveryDayNewDeal { get; set; }
    }

    
}

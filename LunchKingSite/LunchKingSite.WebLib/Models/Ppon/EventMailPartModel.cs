﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Models.Ppon
{
    public class EventMailPartModel
    {
        public EventActivity Activity { get; set; }
        public List<CategoryNode> SubscriptionCategories { get; set; }
    }
}

﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Models.Ppon
{
    public class Page24hChannelModel
    {
        public List<CategoryNodeViewLite> NodeViews { get; set; }

        public List<ContentBannerView> ContentBanners { get; set; }
    }

    public class Arrival24hDealsInputModel
    {
        public List<int> cids { get; set; }
        public int dataStart { get; set; }
    }

    /// <summary>
    /// 分類
    /// 前端使用的Model, 大小寫不要動
    /// </summary>
    public class CategoryNodeViewLite
    {
        public int id { get; set; }
        public bool isOn { get; set; }
        public bool isHot { get; set; }
        public bool isNew { get; set; }
        public bool hasSub { get; set; }
        public string title { get; set; }

        public List<SubNode> subNodes { get; set; }

        public override string ToString()
        {
            if (subNodes.Count > 0)
            {
                return string.Format("{0}, {1} 子分類", this.title, subNodes.Count);
            }
            return this.title;
        }

        public class SubNode
        {
            public int id { get; set; }
            public bool isOn { get; set; }
            public string title { get; set; }
        }
    }

    public class ContentBannerView
    {
        public string img { get; set; }
        public string bg { get; set; }
        public string url { get; set; }
        [JsonIgnore]
        public DateTime startTime { get; set; }
        [JsonIgnore]
        public DateTime endTime { get; set; }

        public override string ToString()
        {
            return this.url;
        }
    }
    /*
{
                        id: 1,
                        isOn: true,
                        isHot: false,
                        isNew: false,
                        hasSub: true,
                        title: '全部',
                        subNodes: [
                            {
                                id: 10,
                                isOn: true,
                                title: '全部'
                            },
                            {
                                id:11,
                                isOn: false,
                                title: '食品'
                            }
                        ]
                    }
     
     */
}

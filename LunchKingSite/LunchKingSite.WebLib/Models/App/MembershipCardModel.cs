﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.App
{
    public class MembershipCardModel
    {
        public string MembershipCardPromoUrl { get; set; }
        public string AndroidUserAgent { get; set; }
        public string IOSUserAgent { get; set; }
        public int GroupId { get; set; }
        public string FBTitle { get; set; }
        public string FBDescription { get; set; }
        public string FBImageUrl { get; set; }
        public string RefereeId { get; set; }
    }
}

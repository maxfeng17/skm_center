﻿using System.Collections.Generic;
using LunchKingSite.Core.Models.GameEntities;
using Newtonsoft.Json;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Models.Game
{
    public class GuestEventModel
    {
        public GameActivity Activity { get; internal set; }
        public GameGame Game { get; internal set; }
        public GameRoundView RoundView { get; internal set; }
        public List<GameBuddy> Buddies { get; set; }
        public string BargainUrl { get; set; }
        public string LineShareUrl { get; set; }
        public string FacebookShareUrl { get; set; }
        public string SuccessUrl { get; set; }
        public string GameHomeUrl { get; set; }
        public string DetailUrl { get; set; }
    }
}

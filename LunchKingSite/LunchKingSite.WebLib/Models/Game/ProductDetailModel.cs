﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.Mobile;
using LunchKingSite.WebLib.Views;
using Newtonsoft.Json;

namespace LunchKingSite.WebLib.Models.Game
{
    public class ProductDetailModel
    {
        public static ProductDetailModel Create(IViewPponDeal deal)
        {
            var model = new ProductDetailModel
            {
                DealId = deal.BusinessHourGuid,
                //DealName = !string.IsNullOrWhiteSpace(deal.AppTitle) ? deal.AppTitle : deal.CouponUsage,
                DealName = deal.ItemName,
                SubTitle = deal.EventTitle,
                Pic = ImageFacade.GetDealPic(deal),
            };

            //詳細介紹
            string description = string.Empty;
            if (deal.Cchannel.GetValueOrDefault(false) && !string.IsNullOrEmpty(deal.CchannelLink))
            {
                description += PponFacade.GetCchannelLink(deal.CchannelLink) + "<br/>";
                description += "<div style='height:20px'></div>";
            }
            description += ViewPponDealManager.DefaultManager.GetDealDescription(deal);
            description = PponFacade.ReplaceMobileYoutubeTag(description);
            model.Description = string.Format("{0}<br />{1}", description, deal.DealPromoDescription);

            model.DeliveryType = (DeliveryType)deal.DeliveryType.GetValueOrDefault();

            //商品規格
            if (string.IsNullOrEmpty(deal.ProductSpec) == false)
            {
                model.ProductSpec = deal.ProductSpec.Replace("<img alt=\"\" src=", "<img alt=\"\" data-original=");
            }
            //權益說明
            var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(deal);
            StringBuilder sbRestrictions = new StringBuilder();
            sbRestrictions.Append(contentLite.Restrictions);
            if (deal.DeliveryType == (int)DeliveryType.ToShop)
            {
                sbRestrictions.Append("<br /><font style=\"color: Red\">●此憑證由等值購物金兌換之</font>");
            }
            if (deal.DeliveryType == (int)DeliveryType.ToShop)
            {
                sbRestrictions.Append("<br />●本憑證不得與其他優惠合併使用，且恕無法兌換現金及找零");
                sbRestrictions.Append("<br />●好康憑證不限本人使用，使用時店家將會核對憑證編號，一組編號限用一次");
            }
            //權益說明-信託銀行文字說明
            if (deal.DeliveryType == (int)DeliveryType.ToShop && deal.ItemPrice > 0 && model.IsKindDeal == false)
            {
                TrustProvider trustProvider = Helper.GetBusinessHourTrustProvider(deal.BusinessHourStatus);
                switch (trustProvider)
                {
                    case TrustProvider.TaiShin:
                        sbRestrictions.Append("<br />●信託期間：發售日起一年內");
                        sbRestrictions.Append("<br />●本商品(服務)禮券所收取之金額，已存入發行人於台新銀行開立之信託專戶，專款專用。所稱專用，係指供發行人履行交付商品或提供服務義務使用");
                        break;
                    case TrustProvider.Sunny:
                        sbRestrictions.Append("<br />●信託期間：發售日起一年內");
                        sbRestrictions.Append("<br />●本商品(服務)禮券所收取之金額，已存入發行人於陽信銀行開立之信託專戶，專款專用。所稱專用，係指供發行人履行交付商品或提供服務義務使用");
                        sbRestrictions.Append("<br />●信託履約禮券查詢網址 https://trust.sunnybank.com.tw/ (於收到兌換券後次日10點起即可查詢)");
                        break;
                    case TrustProvider.Hwatai:
                        sbRestrictions.Append("<br />●信託期間：發售日起一年內");
                        sbRestrictions.Append("<br />●本商品(服務)禮券所收取之金額，已存入發行人於華泰銀行開立之信託專戶，專款專用。所稱專 用，係指供發行人履行交付商品或提供服務義務使用");
                        break;
                }
            }
            //權益說明-成套票券檔次條款說明
            if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon))
            {
                sbRestrictions.Append("<br /><font style='color: Red'>●成套票券商品為整套/組購入，故僅限於所有套/組憑證均兌換完畢才能享有完整優惠</font>");
                sbRestrictions.Append("<br /><font style='color: Red'>●當有退貨情事發生時，將依其整套/組進貨規則進行剩餘面額價值進行退款</font>");
            }

            model.Restrictions = sbRestrictions.ToString();

            if (string.IsNullOrEmpty(contentLite.Availability))
            {
                model.StoreInfos = new AvailableInformation[0];
            }
            else
            {
                model.StoreInfos = JsonConvert.DeserializeObject<AvailableInformation[]>(contentLite.Availability);
            }

            return model;
        }

        public Guid DealId { get; set; }
        public string DealName { get; private set; }
        public string SubTitle { get; private set; }
        public string Pic { get; private set; }
        public DeliveryType DeliveryType { get; private set; }
        /// <summary>
        /// 詳細介紹
        /// </summary>
        public string Description { get; private set; }
        /// <summary>
        /// 商品規格
        /// </summary>
        public string ProductSpec { get; set; }
        /// <summary>
        /// 權益說明
        /// </summary>
        public string Restrictions { get; private set; }
        public bool IsKindDeal { get; private set; }
        public AvailableInformation[] StoreInfos { get; private set; }
    }
}

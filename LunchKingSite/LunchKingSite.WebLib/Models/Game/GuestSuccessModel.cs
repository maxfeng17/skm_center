﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.Core.Models.GameEntities;

namespace LunchKingSite.WebLib.Models.Game
{
    public class GuestSuccessModel
    {
        public List<GameActivityView> ActivitiyViews { get; set; }
        public Guid RoundGuid { get; set; }
        public bool EnableBuddyReward { get; set; }
        public SingleSignOnSource SenderSource { get; set; }
        public string SenderId { get; set; }
        public int State { get; set; }
        public string GameHomeUrl { get; internal set; }
        public string HomeUrl { get; internal set; }
    }
}

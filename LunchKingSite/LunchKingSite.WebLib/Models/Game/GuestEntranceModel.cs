﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Models.GameEntities;

namespace LunchKingSite.WebLib.Models.Game
{
    public class GuestEntranceModel
    {
        public GameActivity Activity { get; internal set; }
        public GameGame Game { get; internal set; }
        public GameRound Round { get; internal set; }
        public string FbLoginUrl { get; internal set; }
        public string LineLoginUrl { get; internal set; }
    }
}

﻿using System.Collections.Generic;
using LunchKingSite.Core.Models.GameEntities;
using Newtonsoft.Json;
using System.Web.Mvc;
using System;

namespace LunchKingSite.WebLib.Models.Game
{
    public class HomeModel
    {

        public GameCampaign Campaign { get; set; }
        /// <summary>
        /// TOP 熱門活動榜
        /// </summary>
        public List<TopGameView> TopGames { get; set; }

        public List<GameActivityView> ActivitiyViews { get; set; }

    }

    public class StartGameModel
    {
        public GameCampaign Campaign { get; set; }
        public GameActivity Activity { get; set; }
        public List<GameGame> Games { get; set; }
        public string DetailUrl { get; set; }
        public List<bool> PlayableChecks { get; set; }
    }

    public class ShowGameModel
    {
        public GameActivity Activity { get; internal set; }
        public GameGame Game { get; internal set; }
        public GameRoundView RoundView { get; internal set; }
        public List<GameBuddy> Buddies { get; set; }
        public string RewardUrl { get; set; }
        public string DetailUrl { get; set; }
        public string LineShareUrl { get; set; }
        public string FacebookShareUrl { get; set; }
        public bool FirstCreate { get; set; }
    }

    public class MyGamesModel
    {
        public GameCampaign Campaign { get; set; }
        public List<GameRoundView> GameRoundViews { get; set; }
    }


    /// <summary>
    /// 後臺編輯用
    /// </summary>
    public class GameEditViewModel
    {
        public GameCampaign Campaign { get; set; }
        public List<GameActivity> Activities { get; set; }
        public int TabIndex { get; set; }
        public List<List<GameGame>> ActivityGames { get;  set; }
        public int UsedBonus { get; set; }
        public int GivenBouns { get; set; }
    }



}

﻿using System;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Models.Game
{
    public class GiveGiftModel
    {
        public Guid RoundGuid { get; set; }
        public string ReceiverName { get; set; }
        public SingleSignOnSource SenderSource { get; set; }
        public string SenderId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Models.Ppon;

namespace LunchKingSite.WebLib.Models.Event
{
    public class LimitedTimeSelectionModel
    {
        public LimitedTimeSelectionModel(LimitedTimeSelection limitedTimeSelection)
        {
            Id = limitedTimeSelection.Id;
            TheDate = limitedTimeSelection.TheDate;
            LimitedTimeSelectionDealModels =
                GenerateLimitedTimeSelectionDealModel(limitedTimeSelection.LimitedTimeSelectionDealViews);
        }

        private List<LimitedTimeSelectionDealModel> GenerateLimitedTimeSelectionDealModel(List<LimitedTimeSelectionDealView> viewLimitedTimeSelectionDeals)
        {
            List<LimitedTimeSelectionDealModel> limitedTimeSelectionDealModels = new List<LimitedTimeSelectionDealModel>();
            foreach (var viewLimitedTimeSelectionDeal in viewLimitedTimeSelectionDeals)
            {
                limitedTimeSelectionDealModels.Add(new LimitedTimeSelectionDealModel(viewLimitedTimeSelectionDeal));
            }

            return limitedTimeSelectionDealModels;
        }

        public int Id { get; private set; }

        public DateTime TheDate { get; private set; }
       
        public List<LimitedTimeSelectionDealModel> LimitedTimeSelectionDealModels { get; private set; }

        public string Rsrc { get { return "17_rushbuy"; } }
    }

    public class LimitedTimeSelectionDealModel
    {
        public LimitedTimeSelectionDealModel(LimitedTimeSelectionDealView limitedTimeSelectionDealView)
        {
            Id = limitedTimeSelectionDealView.Id;
            LtsmId = limitedTimeSelectionDealView.MainId;
            Bid = limitedTimeSelectionDealView.Bid;
            Sequence = limitedTimeSelectionDealView.Sequence;
            Enabled = limitedTimeSelectionDealView.Enabled;
            Deal =
                GenerateBusinessHourDetailViewModel(limitedTimeSelectionDealView.MultipleMainDealPreviews);
        }

        public int Id { get; private set; }

        public int LtsmId { get; private set; }

        public Guid Bid { get; private set; }

        public short Sequence { get; private set; }

        public bool Enabled { get; private set; }

        public ViewMultipleMainDeal Deal { get; private set; }

        private ViewMultipleMainDeal GenerateBusinessHourDetailViewModel(MultipleMainDealPreview item)
        {
            var config = ProviderFactory.Instance().GetConfig();

            ViewMultipleMainDeal viewDealPreview = new ViewMultipleMainDeal();
            viewDealPreview.deal = item;
            bool _isKindDeal = Helper.IsFlagSet(item.PponDeal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal);
            PponCity pponcity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(item.CityID);

            //貼上檔次 Icon 標籤
            string icon_string = PponFacade.GetDealIconHtmlContentList(item.PponDeal, 2);

            viewDealPreview.deal_Label_1 = icon_string;
            viewDealPreview.deal_Label_2 = icon_string;
            viewDealPreview.clit_CityName = item.PponDeal.IsMultipleStores ? "<div class='hover_place_3col' style='display:none;'><span class='hover_place_text'><i class='fa fa-map-marker'></i>" + item.PponDeal.HoverMessage + "</span></div>" : string.Empty;
            viewDealPreview.clit_CityName2 = ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(
                pponcity, item.PponDeal.BusinessHourGuid);
            switch (item.PponDeal.GetDealStage())
            {
                case PponDealStage.Created:
                case PponDealStage.Ready:
                    viewDealPreview.litCountdownTime = "即將開賣";
                    break;

                case PponDealStage.ClosedAndFail:
                case PponDealStage.ClosedAndOn:
                case PponDealStage.CouponGenerated:
                    viewDealPreview.litCountdownTime = "已結束販售";
                    break;
                case PponDealStage.Running:
                case PponDealStage.RunningAndFull:
                case PponDealStage.RunningAndOn:
                default:
                    int daysToClose = (int)(item.PponDeal.BusinessHourOrderTimeE - DateTime.Now).TotalDays;
                    if (daysToClose > 3)
                    {
                        viewDealPreview.litCountdownTime = "限時優惠中!";
                    }
                    else
                    {
                        TimeSpan ts = item.PponDeal.BusinessHourOrderTimeE - DateTime.Now;
                        int hourUntil = ts.Hours;
                        int minutesUntil = ts.Minutes;
                        int secondsUntil = ts.Seconds;
                        StringBuilder sbSetCountdown = new StringBuilder();
                        sbSetCountdown.AppendFormat("<div class='TimeConDigit hn'>{0}</div>", hourUntil);
                        sbSetCountdown.AppendFormat("<div class='TimeConUnit hl'>{0}</div>", Phrase.Hour);
                        sbSetCountdown.AppendFormat("<div class='TimeConDigit mn'>{0}</div>", minutesUntil);
                        sbSetCountdown.AppendFormat("<div class='TimeConUnit ml'>{0}</div>", "分");
                        sbSetCountdown.AppendFormat("<div class='TimeConDigit sn'>{0}</div>", secondsUntil);
                        sbSetCountdown.AppendFormat("<div class='TimeConUnit sl'>{0}</div>", Phrase.Second);
                        viewDealPreview.litCountdownTime = sbSetCountdown.ToString();
                        viewDealPreview.litDays = daysToClose + Phrase.Day;

                        viewDealPreview.timerTitleField_dataStart = item.PponDeal.BusinessHourOrderTimeS.ToJavaScriptMilliseconds();
                        viewDealPreview.timerTitleField_dataEnd = item.PponDeal.BusinessHourOrderTimeE.ToJavaScriptMilliseconds();
                    }
                    break;
            }

            if (_isKindDeal)
            {
                viewDealPreview.discount_1 = viewDealPreview.discount_2 = "公益";
            }
            else if (item.PponDeal.ItemPrice == item.PponDeal.ItemOrigPrice || Helper.IsFlagSet(item.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown)
                     || Helper.IsFlagSet(item.PponDeal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice))
            {
                viewDealPreview.discount_1 = viewDealPreview.discount_2 = "特選";
            }
            else if (item.PponDeal.ItemPrice == 0)
            {
                {
                    viewDealPreview.discount_1 = viewDealPreview.discount_2 = "優惠";
                }
            }
            else
            {
                string discount = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(item.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), PponFacade.CheckZeroPriceToShowPrice(item.PponDeal, 0), item.PponDeal.ItemOrigPrice);
                int length = discount.Length;
                if (length > 0)
                {
                    viewDealPreview.discount_2 = discount.Substring(0, 1);
                    if (discount.IndexOf('.') != -1 && length > 2)
                    {
                        viewDealPreview.discount_2 += discount.Substring(1, length - 1);
                    }

                    viewDealPreview.discount_1 = viewDealPreview.discount_2 + "折";
                    viewDealPreview.discount_2 += "<span class=\"smalltext\">折</span>";
                }
            }

            if (config.InstallmentPayEnabled == false)
            {
                viewDealPreview.litInstallmentPriceLabel = "<p>好康價</p>";
            }
            else if (item.PponDeal.Installment3months.GetValueOrDefault() == false)
            {
                viewDealPreview.litInstallmentPriceLabel = "<p>好康價</p>";
            }
            else
            {
                viewDealPreview.litInstallmentPriceLabel = "<p class='price_installment'>分期0利率</p>";
            }
            
            decimal ratingScore;
            int ratingNumber;
            bool ratingShow;
            viewDealPreview.RatingString = PponFacade.GetDealStarRating(
                item.PponDeal.BusinessHourGuid, "pc",
                out ratingScore, out ratingNumber, out ratingShow);
            viewDealPreview.RatingScore = ratingScore;
            viewDealPreview.RatingNumber = ratingNumber;
            viewDealPreview.RatingShow = ratingShow;

            viewDealPreview.EveryDayNewDeal = PponFacade.GetEveryDayNewDealFlag(item.PponDeal.BusinessHourGuid);
            
            return viewDealPreview;
        }
    }
}

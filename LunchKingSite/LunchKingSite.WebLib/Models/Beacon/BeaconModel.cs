﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Models.Beacon
{
    public class BeaconEventListModel
    {
        public int EventId { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string BeginTime { get; set; }
        public string EndTime { get; set; }
        public bool State { get; set; }
        public string EventType { get; set; }
        public string LinkInput { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string SubEventList { get; set; }
        public string GroupList { get; set; }
        public string BeaconList { get; set; }
        public int UpdateFileCount { get; set; }
        public int SubEventMaxCount { get; set; }
        public int SubEventMinCount { get; set; }


    }

    public class SubEvent
    {
        public int SubEventId { get; set; }
        public string SubEventType { get; set; }
        public string SubLinkInput { get; set; }
        public string SubSubject { get; set; }
        public string SubContent { get; set; }
    }

    public class BindBeaconData
    {
        public string DeviceName { get; set; }
        public int Major { get; set; }
        public int Minor { get; set; }
        public string GroupRemark { get; set; }
        public string Floor { get; set; }
        public string Address { get; set; }
    }

    public class BeaconDeviceInfo
    {
        public int Major { get; set; }
        public int Minor { get; set; }
        public string ElectricPower { get; set; }
        public string DeviceName { get; set; }
        public string LastUpdateTime { get; set; }
        public string Floor { get; set; }
        public string Xtop { get; set; }
        public string Xleft { get; set; }
    }

    public class BeaconEventInfo
    {
        public int EventId { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Subject { get; set; }
        public int Kinds { get; set; }
        public bool State { get; set; }
        public bool IsRootPower { get; set; }
    }

    public class BeaconTriggerLogEventInfo
    {
        public int EventId { get; set; }
        public string EventSubject { get; set; }
        public int EventType { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool State { get; set; }
        public List<int> DeviceId { get; set; }
    }
    

    public class BeaconDeiceManageInfo
    {
        public int DeviceId { get; set; }
        public string DeviceName { get; set; }
        public int Major { get; set; }
        public int Minor { get; set; }
        public string MacAddress { get; set; }
        public bool IsField { get; set; }
    }
    

    public class BeaconDeviceChangeInfo
    {
        public int DeviceId { get; set; }
        public string GroupCode { get; set; }
        public string AuxiliaryCode { get; set; }
    }

    public class BeaconGroupModel
    {
        public int GroupId { get; set; }
        public string GroupCode { get; set; }
        public string AuxiliaryCode { get; set; }
    }

    public class BeaconDeivceModel
    {
        public int DeviceId { get; set; }
        public string Major { get; set; }
        public string Minor { get; set; }
    }

    public class SkmDropdownListModel
    {
        public bool IsFullShop { get; set; }
    }
}

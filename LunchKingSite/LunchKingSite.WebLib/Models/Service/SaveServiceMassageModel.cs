﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Models.Service
{
    [Serializable]
    public class ServiceMassageModel
    {
        /// <summary>
        /// 問題類型 1:一般 2:訂單
        /// </summary>
        public int IssueType { get; set; }
        /// <summary>
        /// 問題內容
        /// </summary>
        public string QuestionContent { get; set; }
        /// <summary>
        /// 主要問題Id
        /// </summary>
        public int? CategoryId { get; set; }
        /// <summary>
        /// 圖片(base64)
        /// </summary>
        public string ImageString { get; set; }
        /// <summary>
        /// IssueType:2 => OrderGuid / IssueType:1 => ServiceNo
        /// </summary>
        public string CaseNo { get; set; }
        /// <summary>
        /// 手機電話
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// E Mail
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 姓氏
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// 名字
        /// </summary>
        public string FirstName { get; set; }
    }
}

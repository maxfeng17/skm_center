﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Models.Service
{
    /// <summary>
    /// 退貨
    /// </summary>
    public class ReturnOrderModel
    {
        /// <summary>
        /// 申請時間
        /// </summary>
        public string ReturnApplicationTime { get; set; }
        /// <summary>
        /// 申請退貨
        /// </summary>
        public string DealName { get; set; }
        /// <summary>
        /// 退貨原因
        /// </summary>
        public string ReturnReason { get; set; }
        /// <summary>
        /// 廠商處理時間
        /// </summary>
        public string VendorProcessingTime { get; set; }
        /// <summary>
        /// 廠商處理進度
        /// </summary>
        public string VendorProcessingProgress { get; set; }
        /// <summary>
        /// 退貨進度
        /// </summary>
        public string ReturnSchedule { get; set; }
        /// <summary>
        /// 完成退貨
        /// </summary>
        public string ReturnComplete { get; set; }
        /// <summary>
        /// 折讓單狀態 
        /// </summary>
        public string DiscountDescription { get; set; }
        /// <summary>
        /// 結案狀態
        /// </summary>
        public string ClosedDescription { get; set; }
        /// <summary>
        /// 是否有抓取到資料
        /// </summary>
        public bool Success { get; set; }

        public ReturnOrderModel()
        {
            ReturnApplicationTime = "";
            DealName = "";
            ReturnReason = "";
            VendorProcessingTime = "";
            VendorProcessingProgress = "";
            ReturnSchedule = "";
            ReturnComplete = "";
            DiscountDescription = "";
            ClosedDescription = "";
            Success = true;

        }
    }

    public class ShippingOrderMethod
    {
        public List<ShippingOrder> ShippingOrder { get; set; }
        public bool Success { get; set; }
        public int Count { get; set; }
        public ShippingOrderMethod()
        {
            ShippingOrder = new List<ShippingOrder>();
            Success = true;
            Count = 0;
        }
    }

    /// <summary>
    /// 出貨
    /// </summary>
    public class ShippingOrder
    {
        /// <summary>
        /// 出貨日期
        /// </summary>
        public string ShippingDate { get; set; }
        /// <summary>
        /// 運單編號
        /// </summary>
        public string ShippingNo { get; set; }
        /// <summary>
        /// 物流公司
        /// </summary>
        public string LogisticsCompany { get; set; }
        /// <summary>
        /// 出貨備註
        /// </summary>
        public string ShippingRemark { get; set; }
        /// <summary>
        /// 最後修改日期
        /// </summary>
        public string LastModifyDate { get; set; }
        /// <summary>
        /// 最後修改人
        /// </summary>
        public string LastModifyAccount { get; set; }

        public ShippingOrder()
        {
            ShippingDate = "";
            ShippingNo = "";
            LogisticsCompany = "";
            ShippingRemark = "";
            LastModifyDate = "";
            LastModifyAccount = "";
            //Success = true;
        }

    }
    
    /// <summary>
    /// 換貨
    /// </summary>
    public class ChangeOrder
    {
        /// <summary>
        /// 申請時間
        /// </summary>
        public string ChangeApplicationTime { get; set; }
        /// <summary>
        /// 換貨原因
        /// </summary>
        public string ChangeReason { get; set; }
        /// <summary>
        /// 廠商處理時間
        /// </summary>
        public string VendorProcessingTime { get; set; }
        /// <summary>
        /// 廠商處理進度
        /// </summary>
        public string VendorProcessingProgress { get; set; }
        /// <summary>
        /// 結案狀態
        /// </summary>
        public string ClosedState { get; set; }
        /// <summary>
        /// 是否有抓取到資料
        /// </summary>
        public bool Success { get; set; }

        public ChangeOrder()
        {
            ChangeApplicationTime = "";
            ChangeReason = "";
            VendorProcessingTime = "";
            VendorProcessingProgress = "";
            ClosedState = "";
            Success = true;
        }
    }
}

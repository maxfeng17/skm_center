﻿using System.Collections.Generic;

namespace LunchKingSite.WebLib.Models.SkmDeal
{
    /// <summary>
    /// 蓋板廣告請求資訊
    /// </summary>
    public class AdListModel : PagerModel
    {
        public List<SkmAdBoardViewModel> SkmAdBoardList { get; set; }
    }

    /// <summary>
    /// 蓋板廣告顯示資訊
    /// </summary>
    public class SkmAdBoardViewModel
    {
        /// <summary> 廣告流水編號 </summary>
        public int Id { get; set; }
        /// <summary> 廣告名稱 </summary>
        public string Name { get; set; }
        /// <summary> 廣告開始時間 </summary>
        public string StartDate { get; set; }
        /// <summary> 廣告結束 </summary>
        public string EndDate { get; set; }
        /// <summary> 是否顯示 </summary>
        public bool IsEnable { get; set; }
        /// <summary> 廣告狀態 </summary>
        public string Status { get; set; }
        /// <summary> 設定廣告狀態按鈕的顯示文字 </summary>
        public string SetIsEnableText { get; set; }
    }
}
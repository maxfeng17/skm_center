﻿using LunchKingSite.Core;
using System;
using System.Web;

namespace LunchKingSite.WebLib.Models.SkmDeal
{
    /// <summary> 廣告資訊請求基本資訊 </summary>
    public class AdBoardBaseRequest
    {
        /// <summary> 活動名稱 </summary>
        public string Name { get; set; }
        /// <summary> 活動開始時間 </summary>
        public string StartDate { get; set; }
        /// <summary> 活動結束時間 </summary>
        public string EndDate { get; set; }
        /// <summary> 上傳的活動圖檔 </summary>
        public HttpPostedFileBase UploadImageUrl { get; set; }
        /// <summary> 活動連結類型 </summary>
        public SKMAdBoardLinkType LinkType { get; set; }
        /// <summary> 開啟連結 </summary>
        public string OpenUrl { get; set; }
        /// <summary> 是否顯示 </summary>
        public bool IsEnable { get; set; }
        /// <summary> 是否需要登入 </summary>
        public bool NeedLogin { get; set; }
        /// <summary> deepLink選擇的館別代號 </summary>
        public string LinkStoreGuid { get; set; }
    }

    /// <summary> 新增廣告資訊請求資訊 </summary>
    public class AddAdBoardRequest : AdBoardBaseRequest
    {

    }

    /// <summary> 修改廣告資訊請求資訊 </summary>
    public class EditAdBoardRequest: AdBoardBaseRequest
    {
        /// <summary> 活動編號 </summary>
        public int Id { get; set; }
        /// <summary> 活動圖檔 </summary>
        public string ImageUrl { get; set; }
    }
}

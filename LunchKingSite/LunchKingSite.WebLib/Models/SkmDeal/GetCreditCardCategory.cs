﻿using LunchKingSite.BizLogic.Models.Wallet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Models.SkmDeal
{
    /// <summary> 查詢信用卡卡樣資料回應資訊 </summary>
    public class GetCreditCardCategoryListResponse : PagerModel
    {
        /// <summary> 信用卡卡樣資料清單 </summary>
        public List<CreditCardCategoryModel> CardDatas { get; set; }
    }
    
    /// <summary> 信用卡卡樣資料(前端顯示用) </summary>
    public class CreditCardCategoryModel
    {
        /// <summary> 卡樣編號 </summary>
        public int Id { get; set; }
        /// <summary> 卡樣名稱 </summary>
        public string CardName { get; set; }
        /// <summary> 卡號前八碼 </summary>
        public string CardCategory { get; set; }
        /// <summary> 卡樣圖檔URL </summary>
        public string CardImageUrl { get; set; }
        /// <summary> 卡片所屬銀行編號 </summary>
        public string Bank { get; set; }
        /// <summary> 卡片所屬銀行 </summary>
        public string BankName { get; set; }
        /// <summary> 是否隱藏 </summary>
        public bool IsHidden { get; set; }
        /// <summary> 最後修改日期 </summary>
        public string ModifyDate { get; set; }
    }
}

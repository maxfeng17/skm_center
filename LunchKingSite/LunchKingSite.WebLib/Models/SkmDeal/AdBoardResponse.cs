﻿using System;

namespace LunchKingSite.WebLib.Models.SkmDeal
{
    public class EditAdBoardResponse
    {
        /// <summary> 活動編號 </summary>
        public int Id { get; set; }
        /// <summary> 活動圖檔 </summary>
        public string ImageUrl { get; set; }
        /// <summary> 完整活動圖檔路徑 </summary>
        public string FullImageUrl { get; set; }
        /// <summary> 活動名稱 </summary>
        public string Name { get; set; }
        /// <summary> 活動開始時間 </summary>
        public string StartDate { get; set; }
        /// <summary> 活動開始時間-小時 </summary>
        public int StartHour { get; set; }
        /// <summary> 活動開始時間-分鐘 </summary>
        public int StartMinute { get; set; }
        /// <summary> 活動結束時間 </summary>
        public string EndDate { get; set; }
        /// <summary> 活動結束時間-小時 </summary>
        public int EndHour { get; set; }
        /// <summary> 活動結束時間-分鐘 </summary>
        public int EndMinute { get; set; }
        /// <summary> 活動連結類型 </summary>
        public int LinkType { get; set; }
        /// <summary> 開啟連結 </summary>
        public string OpenUrl { get; set; }
        /// <summary> 是否顯示 </summary>
        public bool IsEnable { get; set; }
        /// <summary> 是否需要登入 </summary>
        public bool NeedLogin { get; set; }
        /// <summary> deepLink選擇的館別代號 </summary>
        public Guid? LinkStoreGuid { get; set; }
    }
}

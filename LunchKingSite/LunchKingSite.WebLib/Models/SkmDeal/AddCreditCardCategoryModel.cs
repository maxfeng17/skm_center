﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LunchKingSite.WebLib.Models.SkmDeal
{
    public class AddCreditCardCategoryModel
    {
        /// <summary> 銀行代碼 </summary>
        [DisplayName("銀行代碼")]
        [Required]
        [StringLength(3)]
        public string Bank { get; set; }
        /// <summary> 卡面名稱 </summary>
        [DisplayName("卡面名稱")]
        [Required]
        [StringLength(30)]
        public string Name { get; set; }
        /// <summary> 卡面識別碼 </summary>
        [DisplayName("卡面識別碼")]
        [Required]
        [StringLength(8,MinimumLength =6)]
        public string CategoryNo { get; set; }
        /// <summary> 卡面圖檔 </summary>
        [DisplayName("卡面圖檔")]
        [Required]
        public HttpPostedFileBase UploadImageUrl { get; set; }
    }
}

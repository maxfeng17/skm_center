﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Models.Entities;
using Newtonsoft.Json;

namespace LunchKingSite.WebLib.Models.SkmDeal
{
    /// <summary>
    /// 分頁模組(可繼承使用)
    /// </summary>
    public class PagerModel
    {
        /// <summary>
        /// 一頁幾筆
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 第幾頁
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 總筆數
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// 上一頁
        /// </summary>
        public int LastPage
        {
            get
            {
                if (TotalCount > 0)
                {
                    return TotalCount / PageSize + (TotalCount % PageSize > 0 ? 1 : 0);
                }
                else
                {
                    return 1;
                }
            }
        }
        /// <summary>
        /// Skip
        /// </summary>
        public int Skip
        {
            get
            {
                if (PageIndex <= 1)
                {
                    return 0;
                }
                else
                {
                    return (PageIndex - 1) * PageSize;
                }
            }
        }
        /// <summary>
        /// Take
        /// </summary>
        public int Take
        {
            get
            {
                return PageSize;
            }
        }
        /// <summary>
        /// 總頁次
        /// </summary>
        public int TotalPages
        {
            get
            {
                return (int)Math.Ceiling((double)TotalCount / (double)PageSize);
            }
        }
        /// <summary>
        /// 預設
        /// </summary>
        public PagerModel()
        {
            PageSize = 10;
            PageIndex = 1;
        }
    }

}

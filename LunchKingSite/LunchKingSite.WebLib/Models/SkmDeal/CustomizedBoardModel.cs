﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LunchKingSite.WebLib.Models.SkmDeal
{
    /// <summary> 自訂版位基本資料 </summary>
    public class CustomizedBoardBaseModel
    {
        /// <summary> 自訂版位流水號 </summary>
        public int Id { get; set; }
        /// <summary> 所屬店編號 </summary>
        public Guid SellerGuid { get; set; }
        /// <summary> 單元位置 </summary>
        public string LayoutType { get; set; }
        /// <summary> 連結類型 </summary>
        public string ContentType { get; set; }
        /// <summary> 更新時間 </summary>
        public string UpdateDate { get; set; }
    }

    /// <summary> 編輯自訂版位資料(前端顯示用) </summary>
    public class EditCustomizedBoardModel
    {
        /// <summary> 自訂版位流水號 </summary>
        [Required]
        public int Id { get; set; }
        /// <summary> 所屬店編號 </summary>
        [Required]
        public Guid SellerGuid { get; set; }
        /// <summary> 單元位置 </summary>
        public string LayoutType { get; set; }

        /// <summary>
        /// 這裡指的是連結類型, 但命名卻用ContentType, 請忽略
        /// 因DB已命名不想更改  應該是UrlType
        /// ContentType指的 deeplink or inline or weburl
        /// </summary>
        [Required]
        public SkmCustomizedBoardUrlTypeEnum ContentType { get; set; }
        /// <summary> 連結網址 </summary>
        public string LinkUrl { get; set; }
        /// <summary> 圖片網址 </summary>
        public string ImageUrl { get; set; }
        /// <summary> 上傳圖片網址 </summary>
        public HttpPostedFileBase UploadImageFile { get; set; }
        /// <summary> 上傳圖片網址 </summary>
        public int ImageLimiteWidth { get; set; }
        /// <summary> 上傳圖片網址 </summary>
        public int ImageLimiteHeight { get; set; }
    }
}

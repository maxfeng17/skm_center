﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace LunchKingSite.WebLib.Models.SkmDeal
{
    public class EditCreditCardCategoryModel
    {
        /// <summary> 卡樣編號 </summary>
        [DisplayName("卡樣編號")]
        [Required]
        public int Id { get; set; }
        /// <summary> 銀行代碼 </summary>
        [DisplayName("銀行代碼")]
        [Required]
        [StringLength(3)]
        public string Bank { get; set; }
        /// <summary> 卡面名稱 </summary>
        [DisplayName("卡面名稱")]
        [Required]
        [StringLength(30)]
        public string Name { get; set; }
        /// <summary> 卡面識別碼 </summary>
        [DisplayName("卡面識別碼")]
        [Required]
        [StringLength(8, MinimumLength = 6)]
        public string CategoryNo { get; set; }
        /// <summary> 卡面圖檔 </summary>
        [DisplayName("卡面圖檔")]
        public HttpPostedFileBase UploadImageUrl { get; set; }
        /// <summary> 銀行名稱 </summary>
        public string BankName { get; set; }
        /// <summary> 卡樣圖片 </summary>
        public string CardImageUrl { get; set; }
    }
}

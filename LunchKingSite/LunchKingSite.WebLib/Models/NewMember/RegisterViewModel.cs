﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Models.NewMember
{
    public class RegisterViewModel
    {
        public string Account { get; set; }
        public string Password { get; set; }
        public string ConPassword{get; set;}
        public bool Agree { get; set; }
        public bool ReceiveEDM { get; set; }
        public string City { get; set; }
        public string ResetPasswordKey { get; set; }
    }
}

﻿using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;
using System.Collections.Specialized;
using System.Linq;

namespace LunchKingSite.WebLib.Component
{
    public class MobileUtility
    {
        private static ILog logger = LogManager.GetLogger(typeof(MobileUtility));

        #region 參數

        protected static IMemberProvider _mp;
        protected static IOrderProvider _op;
        protected static ISellerProvider _sp;
        protected static IPponProvider _pp;
        protected static ISysConfProvider _conf;
        protected static ILog _log;
        protected static RegExRules _reg = new RegExRules();

        public static int _year = 0;
        public static int _month = 0;
        public static int _day = 0;
        public static int _hour = 0;
        public static int _min = 0;
        public static int _sec = 0;
        public static int _counter = 0;

        public string ImgUrlAccordionDown
        {
            get
            {
                return _conf.SiteUrl + "/Ppon/m/res/imgs/accordion_down.png";
            }
        }

        public string ImgUrlAccordionUp
        {
            get
            {
                return _conf.SiteUrl + "/Ppon/m/res/imgs/accordion_up.png";
            }
        }

        public void SetLogType(Type type)
        {
            _log = LogManager.GetLogger(type);
        }

        #endregion 參數

        public MobileUtility()
        {
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _conf = ProviderFactory.Instance().GetConfig();
        }

        #region Method

        public string GetImageUrl(string rawData, MediaType type)
        {
            string[] astrImgUrl = ImageFacade.GetMediaPathsFromRawData(rawData, type);

            if (astrImgUrl.Length < 2)
            {
                return _conf.SiteUrl + _conf.EDMDefaultImage;
            }
            return astrImgUrl[1];
        }

        public string GetCountdown(DateTime time)
        {
            TimeSpan ts = time - DateTime.Now;
            int hourUntil = (int)Math.Floor(ts.TotalHours);
            int minutesUntil = (int)Math.Floor(ts.TotalMinutes - hourUntil * 60);
            int secondsUntil = (int)Math.Floor(ts.TotalSeconds - hourUntil * 3600 - minutesUntil * 60);
            return hourUntil + I18N.Phrase.Hour + minutesUntil + I18N.Phrase.Minute + secondsUntil + I18N.Phrase.Second;
        }

        #region 註冊判斷

        #endregion 註冊判斷

        #endregion Method

        #region Javascript 相關

        public string GetTimerScript(string tagId, string tagClass, int counter, DateTime time)
        {
            _year = time.Year;
            _month = time.Month;
            _day = time.Day;
            _hour = time.Hour;
            _min = time.Minute;
            _sec = time.Second;

            string strCount = (counter == -1) ? string.Empty : counter.ToString();

            string strTagName = (string.IsNullOrEmpty(tagId))
                                    ? "." + tagClass + strCount
                                    : "#" + tagId + strCount;

            string script = "<script type='text/javascript'> $(function() {$('{tagName}')" +
                                ".countdown({" +
                                    "until: new Date(" +
                                        _year.ToString() + "," + (_month - 1).ToString() + "," +
                                        _day.ToString() + "," + _hour.ToString() + "," +
                                        _min.ToString() + "," + _sec.ToString() +
                                    "), format:'HMS', layout: '{hn}{hl}{mn}{ml}{sn}{sl}',expiryUrl:document.location.href});" +
                                "});" +
                            "</script>";
            return script.Replace("{tagName}", strTagName);
        }

        #endregion Javascript 相關

        #region Static Method

        /// <summary>
        /// 將 URL 的 QueryString 轉為 NameValueCollection
        /// </summary>
        /// <param name="url">網址列</param>
        /// <returns></returns>
        public static NameValueCollection GetQueryStringCollection(string url)
        {
            var collection = new NameValueCollection();
            if (url.IndexOf("?", System.StringComparison.Ordinal) >= 0)
            {
                string keyValue = string.Empty;
                string[] querystrings = url.Split('&');
                if (querystrings.Any())
                {
                    for (int i = 0; i < querystrings.Count(); i++)
                    {
                        string[] pair = querystrings[i].Split('=');
                        collection.Add(pair[0].Trim('?'), pair[1]);
                    }
                }
            }

            return collection;
        }

        #endregion Static Method
    }
}
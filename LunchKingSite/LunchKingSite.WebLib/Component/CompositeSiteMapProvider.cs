using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Hosting;
using System.Xml;
using System.Xml.Schema;

namespace LunchKingSite.WebLib.Component
{
    public class CompositeSiteMapProvider : XmlSiteMapProvider
    {
        protected struct MapNode
        {
            public string RealUrl;
            public List<string> Parameters;
            public string TitleKey;

            public MapNode(string url)
            {
                RealUrl = url;
                Parameters = new List<string>(5);
                TitleKey = null;
            }

            public MapNode(string url, List<string> paramList)
            {
                RealUrl = url;
                Parameters = paramList;
                TitleKey = null;
            }
        };

        #region private members
        private const string _siteMapFileAttribute = "siteMapFile";
        private const string _schemaFilePath = "~/schema/lksitemap.xsd";
        private const string _siteMapNodeAttribute = "siteMapNode";
        private const string _siteMapNodeParamAttribute = "param";
        internal string AppDomainAppVirtualPathWithTrailingSlash = VirtualPathUtility.AppendTrailingSlash(HttpRuntime.AppDomainAppVirtualPath);

        private Dictionary<string, MapNode> _nodeParams;
        private string _docPath;
        #endregion

        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection attributes)
        {
            _docPath = attributes[_siteMapFileAttribute];
            XmlValidator.ValidatingXml(HostingEnvironment.MapPath(_schemaFilePath), HostingEnvironment.MapPath("~/" + _docPath));
            _docPath = VirtualPathUtility.Combine(AppDomainAppVirtualPathWithTrailingSlash, _docPath);

            XmlDocument document = new ConfigXmlDocument();
            using (Stream stream = HostingEnvironment.VirtualPathProvider.GetFile(_docPath).Open())
            {
                XmlReader reader = new XmlTextReader(stream);
                document.Load(reader);
            }

            XmlNode rootNode = null;
            foreach (XmlNode node in document.ChildNodes)
            {
                if (string.Equals(node.Name, "siteMap", StringComparison.Ordinal))
                {
                    rootNode = node;
                    break;
                }
            }

            if (rootNode != null)
            {
                foreach (XmlNode node in rootNode.ChildNodes)
                    if (string.Equals(node.Name, _siteMapNodeAttribute, StringComparison.Ordinal))
                    {
                        rootNode = node;
                        break;
                    }

                _nodeParams = new Dictionary<string, MapNode>();
                Queue<XmlNode> queue = new Queue<XmlNode>(50);
                queue.Enqueue(rootNode);
                TraverseXmlNode(queue);
            }

            string tempDocPath = "cache/temp.sitemap";
            using (XmlTextWriter writer = new XmlTextWriter(HostingEnvironment.MapPath("~/" + tempDocPath), null))
            {
                document.Save(writer);
            }
            attributes[_siteMapFileAttribute] = tempDocPath;

            base.Initialize(name, attributes);
        }

        public override SiteMapNode CurrentNode
        {
            get { return GetUpdatedNode(base.CurrentNode); }
        }

        public override SiteMapNode GetParentNode(SiteMapNode node)
        {
            return GetUpdatedNode(base.GetParentNode(node));
        }

        private void TraverseXmlNode(Queue<XmlNode> queue)
        {
            while (true)
            {
                if (queue.Count == 0)
                    break;

                XmlNode smNode = (XmlNode)queue.Dequeue();

                string key = GetKeyFromUrl(smNode.Attributes["url"].Value);
                MapNode mn = new MapNode(smNode.Attributes["url"].Value);
                if (smNode.Attributes["titleKey"] != null)
                {
                    mn.TitleKey = smNode.Attributes["titleKey"].Value;
                    smNode.Attributes.RemoveNamedItem("titleKey");
                }
                _nodeParams.Add(key, mn);
                smNode.Attributes["url"].Value = key;
                List<XmlNode> waitingTobeRemoved = new List<XmlNode>(5);
                foreach (XmlNode node in smNode.ChildNodes)
                {
                    if (string.Equals(node.Name, _siteMapNodeAttribute, StringComparison.Ordinal))
                    {
                        queue.Enqueue(node);
                    }
                    else if (string.Equals(node.Name, _siteMapNodeParamAttribute, StringComparison.Ordinal))
                    {
                        _nodeParams[key].Parameters.Add(node.Attributes["key"].Value);
                        waitingTobeRemoved.Add(node);
                    }
                }

                foreach (XmlNode node in waitingTobeRemoved)
                    smNode.RemoveChild(node);
                waitingTobeRemoved.Clear();
            }
        }

        private string GetKeyFromUrl(string p)
        {
            string key = p.Trim();
            int qs = key.IndexOf('?');
            if (qs != -1)
                key = key.Substring(0, qs);

            // Make sure the path is adjusted properly
            key = VirtualPathUtility.Combine(AppDomainAppVirtualPathWithTrailingSlash, key);

            // Make it an absolute virtualPath
            key = VirtualPathUtility.ToAbsolute(key).ToLower();

            return key;
        }

        private SiteMapNode GetUpdatedNode(SiteMapNode n)
        {
            if (n == null)
                return null;

            SiteMapNode node = n;
            node.ReadOnly = false;
            if (_nodeParams[node.Key].Parameters.Count > 0)
            {
                object[] p = new object[_nodeParams[node.Key].Parameters.Count];
                for (int i = 0; i < p.Length; i++)
                    if (HttpContext.Current.Session[_nodeParams[node.Key].Parameters[i]] != null)
                        p[i] = HttpContext.Current.Session[_nodeParams[node.Key].Parameters[i]];
                    else
                        p[i] = string.Empty;
                node.Url = string.Format(_nodeParams[node.Key].RealUrl, p);
            }
            if (_nodeParams[node.Key].TitleKey != null && HttpContext.Current.Session != null && HttpContext.Current.Session[_nodeParams[node.Key].TitleKey] != null)
                node.Title = HttpContext.Current.Session[_nodeParams[node.Key].TitleKey].ToString();
            return node;
        }
    }

    public class XmlValidator
    {
        private string _schemaPath;
        public string SchemaPath
        {
            get { return _schemaPath; }
            set { _schemaPath = value; }
        }

        private string _docPath;
        public string DocumentPath
        {
            get { return _docPath; }
            set { _docPath = value; }
        }

        public XmlValidator(string schemaPath, string documentPath)
        {
            _schemaPath = schemaPath;
            _docPath = documentPath;
        }

        public void ValidatingXml()
        {
            XmlValidator.ValidatingXml(_schemaPath, _docPath);
        }

        public static void ValidatingXml(string XSDPath, string XMLPath)
        {
            try
            {
                // 1- Read XML file content
                XmlTextReader Reader = new XmlTextReader(XMLPath);

                // 2- Read Schema file content
                StreamReader SR = new StreamReader(XSDPath);

                // 3- Create a new instance of XmlSchema object
                XmlSchema Schema = new XmlSchema();
                // 4- Set Schema object by calling XmlSchema.Read() method
                Schema = XmlSchema.Read(SR, null);
                //Schema = XmlSchema.Read(SR,
                //    new ValidationEventHandler(ReaderSettings_ValidationEventHandler));

                // 5- Create a new instance of XmlReaderSettings object
                XmlReaderSettings ReaderSettings = new XmlReaderSettings();
                // 6- Set ValidationType for XmlReaderSettings object
                ReaderSettings.ValidationType = ValidationType.Schema;
                // 7- Add Schema to XmlReaderSettings Schemas collection
                ReaderSettings.Schemas.Add(Schema);

                // 8- Add your ValidationEventHandler address to
                // XmlReaderSettings ValidationEventHandler
                //ReaderSettings.ValidationEventHandler +=
                //    new ValidationEventHandler(ReaderSettings_ValidationEventHandler);

                // 9- Create a new instance of XmlReader object
                XmlReader objXmlReader = XmlReader.Create(Reader, ReaderSettings);


                // 10- Read XML content in a loop
                while (objXmlReader.Read())
                { /*Empty loop*/}

            }//try
            catch (Exception Ex)
            {
                throw Ex;
            }//catch
        }

        private void ReaderSettings_ValidationEventHandler(object sender,
            ValidationEventArgs args)
        {
            // 11- Implement your logic for each validation iteration
        }
    }
}

﻿using System;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Component
{
    public class SellerUtility
    {
        protected static IPponProvider pp;

        static SellerUtility()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        }

        /// <summary>
        /// 取得店家好康資料，只回傳可以顯示的部分，不可顯示的不回傳
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        public static ViewPponDealCollection GetSellerViewPponDealOnShow(Guid sellerGuid)
        {
            var vpdc = pp.ViewPponDealGetListOnShowBySellerGuid(sellerGuid);
            for (int i = vpdc.Count - 1; i >= 0;i--)
            {
                var deal = vpdc[i];
                //未達門檻就不回傳
                if(deal.OrderedQuantity < deal.BusinessHourOrderMinimum)
                {
                    vpdc.Remove(deal);
                }
            }
            
            return vpdc;
        }

        
    }
}

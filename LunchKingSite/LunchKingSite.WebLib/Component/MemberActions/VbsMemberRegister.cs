﻿using System;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Models;
using LunchKingSite.Core.Interface;

namespace LunchKingSite.WebLib.Component.MemberActions
{
    public class VbsMemberRegister : MemberRegisterBase
    {
        private NewMemberModelParser _member;
        private IPCPProvider pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
        
        public VbsMemberRegister(NewMemberModelParser mem) :
            base(mem.AccountId + "@vbs", mem.Password, 199)
        {
            _member = mem;
        }

        protected override MemberRegisterReplyType CreateMemberLink()
        {
            var vbsid = mp.VbsMembershipGetByAccountId(_member.AccountId);

            if (!vbsid.IsLoaded)
            {
                logger.ErrorFormat("CreateMemberLink fail. org=({0},vbs_member_ship.id={1})", "查無商家帳號資訊", _member.AccountId);
                Message = Phrase.RegisterReturnMessageRegisterError;
                return MemberRegisterReplyType.RegisterError;
            }

            try
            {
                MemberFacade.AddMemberLink(_userId, SingleSignOnSource.VbsMember, vbsid.Id.ToString(), _userName);
                return MemberRegisterReplyType.RegisterSuccess;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("CreateMemberLink fail. org=({0},exid={1},userid={2}), ex: {3}",
                    SingleSignOnSource.VbsMember, _userId, _userId, ex);
                Message = Phrase.RegisterReturnMessageRegisterError;
                return MemberRegisterReplyType.RegisterError;
            }
        }

        protected override void AfterProcessData(Member mem, MemberAuthInfo mai)
        {
            #region 回填userid至vbs member

            mem.EdmType = 0;
            var vbs = mp.VbsMembershipGetByAccountId(_member.AccountId);
            
            if (vbs.IsLoaded)
            {
                vbs.UserId = mem.UniqueId;
                try
                {
                    mp.VbsMembershipSet(vbs);
                }
                catch (Exception)
                {
                    logger.ErrorFormat("update uid fail. org=({0},user id={1},vbs account id={2}), ex: {3}", SingleSignOnSource.VbsMember
                        , mem.UniqueId, _member.AccountId);
                    throw;
                }
            }
            else 
            {
                logger.ErrorFormat("vbs account id error. org=({0},user id={1},vbs account id={2}), ex: {3}", SingleSignOnSource.VbsMember
                            , mem.UniqueId, _member.AccountId);
            }

            #endregion 回填userid至vbs member
        }

        protected override MemberRegisterReplyType CreateMember()
        {
            //TODO
            MemberRegisterReplyType result = base.CreateMember();
            return result;
        }

        protected override void AuditReigsteSuccess()
        {
            MemberFacade.LogAccountAudit(_userId, _userName, AccountAuditAction.LoginUsingVbsMember, true, "商家系統 建立帳號");
        }

        protected override MemberRegisterReplyType SubscribeEDM(bool receiveEDM)
        {
            //不訂閱
            return MemberRegisterReplyType.RegisterSuccess;
        }

        protected override MemberRegisterReplyType CheckData()
        {
            //不檢查
            return MemberRegisterReplyType.RegisterSuccess;
        }

        public override SingleSignOnSource RegisterSourceType
        {
            get { return SingleSignOnSource.VbsMember; }
        }
    }
}
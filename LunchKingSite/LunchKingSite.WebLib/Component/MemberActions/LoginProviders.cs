﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Component.MemberActions
{
    public interface ILoginProvider
    {
        string Name { get; }
        bool IsMatch(string account);
        SignInReply Login(string account, string password, bool isKeepLogin, ref MemberLinkCollection mlCol, 
            out string userName, out SingleSignOnSource source, out Member mem);
    }

    public abstract class LoginProviderBase : ILoginProvider
    {
        public string Name
        {
            get { return this.GetType().Name; }
        }

        public abstract bool IsMatch(string account);
        public abstract SignInReply Login(string account, string password, bool isKeepLogin, ref MemberLinkCollection mlCol,
            out string userName, out SingleSignOnSource source, out Member mem);

        public override string ToString()
        {
            return this.Name;
        }
    }

    public class LoginProviders
    {
        private static List<ILoginProvider> providers = new List<ILoginProvider>();
        static LoginProviders()
        {
            providers.Add(new AccountLoginProvider());
            providers.Add(new MobileLoginProvider());
        }

        public static SignInReply Login(string account, string password, bool isKeepLogin, ref MemberLinkCollection mlCol,
            out string userName, out SingleSignOnSource source, out Member mem)
        {
            foreach (ILoginProvider provider in providers)
            {
                if (provider.IsMatch(account))
                {
                    return provider.Login(account, password, isKeepLogin, ref mlCol, out userName, out source, out mem);
                }
            }
            source = SingleSignOnSource.Undefined;
            userName = string.Empty;
            mem = null;
            return SignInReply.AccountNotExist;
        }
    }

    public class AccountLoginProvider : LoginProviderBase
    {
        IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        public override SignInReply Login(string account, string password, bool isKeepLogin, ref MemberLinkCollection mlCol,
            out string userName, out SingleSignOnSource source, out Member mem)
        {
            userName = null;
            source = SingleSignOnSource.ContactDigitalIntegration;
            mem = null;

            //檢查有無輸入資料
            if (string.IsNullOrWhiteSpace(account) || string.IsNullOrWhiteSpace(password))
            {
                return SignInReply.AccountNotExist;
            }
            mem = MemberFacade.GetMember(account);
            userName = account;

            if (mem.IsLoaded == false)
            {
                return SignInReply.AccountNotExist;
            }
            mlCol = mp.MemberLinkGetList(mem.UniqueId);
            if (mlCol.Count == 0)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsing17LifeAccount, false, "會員無串接資料",
                    Helper.GetOrderFromType());
                return SignInReply.MemberLinkNotExist;
            }
            if (mlCol.All(t => t.ExternalOrg != (int)SingleSignOnSource.ContactDigitalIntegration))
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsing17LifeAccount, false, "錯誤的登入方式",
                    Helper.GetOrderFromType());
                return SignInReply.MemberLinkExistButWrongWay;
            }

            if (mem.IsLockedOut)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsing17LifeAccount, false, "帳號鎖定",
                    Helper.GetOrderFromType());
                return SignInReply.AccountIsLockedOut;
            }

            if (mem.IsApproved == false)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsing17LifeAccount, false, "帳號拒絕",
                    Helper.GetOrderFromType());
                return SignInReply.AccountNotApproved;
            }

            if (Helper.IsFlagSet(mem.Status, MemberStatusFlag.Active17Life) == false)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsing17LifeAccount, false, "帳號未通過驗證",
                    Helper.GetOrderFromType());
                return SignInReply.EmailInactive;
            }

            if (Membership.ValidateUser(account, password) == false)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsing17LifeAccount, false,
                    "密碼輸入錯誤", Helper.GetOrderFromType());
                return SignInReply.PasswordError;
            }
            userName = mem.UserName;
            NewMemberUtility.UserSignIn(userName, SingleSignOnSource.ContactDigitalIntegration, isKeepLogin);
            return SignInReply.Success;
        }

        public override bool IsMatch(string account)
        {
            return RegExRules.CheckEmail(account);
        }
    }

    public class MobileLoginProvider : LoginProviderBase
    {
        IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ILog logger = LogManager.GetLogger(typeof (MobileLoginProvider));
        public override SignInReply Login(string account, string password, bool isKeepLogin, ref MemberLinkCollection mlCol,
            out string userName, out SingleSignOnSource source, out Member mem)
        {
            userName = string.Empty;
            source = SingleSignOnSource.Mobile17Life;
            mem = null;

            MobileMember mm = mp.MobileMemberGet(account);
            if (mm.IsLoaded == false || mm.Status == (int)MobileMemberStatusType.None)
            {
                return SignInReply.AccountNotExist;
            }
            mem = MemberFacade.GetMember(mm.UserId);            
            if (mm.IsLoaded == false)
            {
                //這情況不預期會發生，所以如果真的發生了，額外的記log通知。
                logger.Error("使用者資料載入失敗, userId=" + mm.UserId);
                return SignInReply.AccountNotExist;
            }
            userName = mem.UserName;
            mlCol = mp.MemberLinkGetList(mm.UserId);
            if (mlCol.Count == 0)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsingMobile17Life, false, "會員無串接資料",
                    Helper.GetOrderFromType());
                return SignInReply.MemberLinkNotExist;
            }
            if (mlCol.All(t => t.ExternalOrg != (int)SingleSignOnSource.Mobile17Life))
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsingMobile17Life, false, "錯誤的登入方式",
                    Helper.GetOrderFromType());
                return SignInReply.MemberLinkExistButWrongWay;
            }
            if (mm.IsLockedOut)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsingMobile17Life, false, "帳號鎖定",
                    Helper.GetOrderFromType());
                return SignInReply.AccountIsLockedOut;
            }
            if (mem.IsApproved == false)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsingMobile17Life, false, "帳號拒絕",
                    Helper.GetOrderFromType());
                return SignInReply.AccountNotApproved;
            }
            if (mm.Status == (int)MobileMemberStatusType.NumberChecked)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsingMobile17Life, false, "手機帳號未設定密碼",
                    Helper.GetOrderFromType());
                return SignInReply.MobileInactive;
            }
            if (LkMembershipProvider.ValidateMobileUser(account, password) == false)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsingMobile17Life, false,
                    "密碼輸入錯誤", Helper.GetOrderFromType());
                return SignInReply.PasswordError;
            }
            userName = mem.UserName;
            NewMemberUtility.UserSignIn(userName, SingleSignOnSource.Mobile17Life, isKeepLogin);
            return SignInReply.Success;
        }

        public override bool IsMatch(string account)
        {
            return RegExRules.CheckMobile(account);
        }

    }

    public class SKMCenterLoginProvider : LoginProviderBase
    {
        IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ILog logger = LogManager.GetLogger(typeof(MobileLoginProvider));
        public override SignInReply Login(string account, string password, bool isKeepLogin, ref MemberLinkCollection mlCol,
            out string userName, out SingleSignOnSource source, out Member mem)
        {
            userName = null;
            source = SingleSignOnSource.ContactDigitalIntegration;
            mem = null;

            //檢查有無輸入資料
            if (string.IsNullOrWhiteSpace(account))
            {
                return SignInReply.AccountNotExist;
            }
            mem = MemberFacade.GetMember(account);
            userName = account;

            if (mem.IsLoaded == false)
            {
                return SignInReply.AccountNotExist;
            }
            mlCol = mp.MemberLinkGetList(mem.UniqueId);
            if (mlCol.Count == 0)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsing17LifeAccount, false, "會員無串接資料",
                    Helper.GetOrderFromType());
                return SignInReply.MemberLinkNotExist;
            }
            if (mlCol.All(t => t.ExternalOrg != (int)SingleSignOnSource.ContactDigitalIntegration))
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsing17LifeAccount, false, "錯誤的登入方式",
                    Helper.GetOrderFromType());
                return SignInReply.MemberLinkExistButWrongWay;
            }

            if (mem.IsLockedOut)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsing17LifeAccount, false, "帳號鎖定",
                    Helper.GetOrderFromType());
                return SignInReply.AccountIsLockedOut;
            }

            if (mem.IsApproved == false)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsing17LifeAccount, false, "帳號拒絕",
                    Helper.GetOrderFromType());
                return SignInReply.AccountNotApproved;
            }

            if (Helper.IsFlagSet(mem.Status, MemberStatusFlag.Active17Life) == false)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsing17LifeAccount, false, "帳號未通過驗證",
                    Helper.GetOrderFromType());
                return SignInReply.EmailInactive;
            }

            userName = mem.UserName;
            NewMemberUtility.UserSignIn(userName, SingleSignOnSource.ContactDigitalIntegration, isKeepLogin);
            return SignInReply.Success;
        }

        public override bool IsMatch(string account)
        {
            return RegExRules.CheckMobile(account);
        }

    }

}

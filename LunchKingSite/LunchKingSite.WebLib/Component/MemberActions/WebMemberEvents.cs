﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace LunchKingSite.WebLib.Component.MemberActions
{
    public class MemberEvents
    {
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected static ISystemProvider ss = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        private static IMailLogProvider mlp = ProviderFactory.Instance().GetProvider<IMailLogProvider>();
        static ILog logger = LogManager.GetLogger(typeof(MemberEvents));

        public static void Register()
        {
            MemberEventsCore.OnMemberCreated += MemberEvents_OnMemberCreated;

            MemberEventsCore.OnMemberCreated += NewUserDiscountEvent_OnMemberCreated;

            PostMan.OnMailLog += PostMan_OnMailLog;
        }


        private static void PostMan_OnMailLog(MailMessage mm, bool result, MailTemplateType template, int? mailLogId, string errMsg)
        {
            if (mailLogId == null)
            {
                string sender = mm.From.Address;
                List<string> receivers = mm.To.Select(t => t.Address).ToList();
                int category = (int)template;
                mlp.MailLogSet(sender, receivers, mm.Subject, mm.Body, result, category, errMsg);
            }
            else
            {
                mlp.MailLogResendSet(mailLogId.Value, result, errMsg);
            }
        }

        private static void MemberEvents_OnMemberCreated(DataOrm.Member mem, Core.SingleSignOnSource registerSourceType)
        {
            if (HttpContext.Current == null ||
                HttpContext.Current.Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()] == null)
            {
                return;
            }
            HttpCookie httpCookie = HttpContext.Current.Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()];
            if (httpCookie != null)
            {
                string referrerSourceId = httpCookie.Value;
                if (string.IsNullOrWhiteSpace(referrerSourceId) == false)
                {
                    CpaUtility.RecordRegistrationByReferrer(referrerSourceId, mem, BusinessModel.Ppon);
                }
            }
        }

        private static void NewUserDiscountEvent_OnMemberCreated(DataOrm.Member mem, SingleSignOnSource registerSourceType)
        {
            if (config.NewUserSendDiscountCodeEnabled)
            {
                if (mem.IsGuest == false &&
                    (registerSourceType == SingleSignOnSource.Mobile17Life || registerSourceType == SingleSignOnSource.PayEasy ||
                     registerSourceType == SingleSignOnSource.Facebook || registerSourceType == SingleSignOnSource.ContactDigitalIntegration))

                {
                    string discountCode = string.Empty;
                    try
                    {
                        //即時發折價券
                        string dataName = "NewMemberDiscountCode";
                        var data = ss.SystemDataGet(dataName);
                        discountCode = data.Data;

                        if (string.IsNullOrEmpty(discountCode) == false)
                        {
                            Dictionary<int, int> amountList = new Dictionary<int, int>();
                            string[] referr = discountCode.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                            for (int i = 0; i < referr.Length; i++)
                            {
                                int discounId = int.TryParse(referr[i].Split('#')[0], out discounId) ? discounId : 0;
                                int count = int.TryParse(referr[i].Split('#')[1], out count) ? count : 0;
                                if (discounId != 0 && count != 0)
                                {
                                    amountList.Add(discounId, count);
                                }
                            }

                            Task.Run(() => SetDiscountCode(mem, amountList)).ContinueWith(task =>
                            {
                                foreach (var ex in task.Exception.InnerExceptions)
                                {
                                    if (ex is Exception)
                                    {
                                        logger.ErrorFormat("建立新會員時，發送新人折價券失敗, discountCode={0}, ex={1}", discountCode, ex);
                                    }
                                }
                            }
                            , TaskContinuationOptions.OnlyOnFaulted);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.ErrorFormat("建立新會員時，發送新人折價券失敗, discountCode={0}, ex={1}", discountCode, ex);
                    }
                }
            }
        }

        private static void SetDiscountCode(DataOrm.Member mem, Dictionary<int, int> amountList)
        {
            string code;
            int amount, discountCodeId, minimumAmount;
            DateTime? endTime;

            foreach (var c in amountList)
            {
                for (int i = 1; i <= c.Value; i++)
                {
                    PromotionFacade.InstantGenerateDiscountCode(c.Key, mem.UniqueId, false, out code, out amount, out endTime, out discountCodeId, out minimumAmount);
                }
            }
        }
    }
}

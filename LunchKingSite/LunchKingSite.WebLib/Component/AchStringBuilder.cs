﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;
using System.Text;

namespace LunchKingSite.WebLib.Component
{
    public class AchStringBuilder
    {
        StringBuilder sb = new StringBuilder();
        private readonly static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        private readonly static string _MAGIC = string.Format("{0}{1}", config.SendUnitNo, config.ReceiveUnitNo); // 發送單位代號 + 接收單位代號
        private readonly static string _EOF = "EOFACHP01"; // 尾錄別 + 資料代號
        private readonly static string _BOF = "BOFACHP01"; // 首錄別 + 資料代號
        private readonly static string _MAGIC2 = "NSC405"; // SC(代付)
        private readonly static string _MAGIC3 = config.TriggerBankAndBranchNo; // 提出行代號
        private readonly static string _MAGIC4 = config.TriggerAccountNo; // 發動者帳號
        private readonly static string _MAGIC5 = string.Format("00B{0}  ", config.TriggerCompanyId); //00B + 發動者統一編號
        private readonly static string _MAGIC6 = "      00000000000000 ";

        private DateTime _time;
        private int _count;
        private decimal _total;

        public AchStringBuilder(DateTime remitDate)
        {
            this._time = remitDate;
        }


        private void AssureNotNull(string propName, string propValue)
        {
            if (propValue == null)
            {
                throw new Exception(string.Format("{0}不能為空白", propName));
            }
        }

        public void Append(string bankCode, string branchCode, string account, int amount, string accountId, int id, int? uniqueId)
        {
            AssureNotNull("bankCode", bankCode);
            AssureNotNull("branchCode", branchCode);
            AssureNotNull("account", account);

            _count++;
            sb.Append(_MAGIC2);
            sb.Append(_count.ToString().PadLeft(6, '0'));
            sb.Append(_MAGIC3);
            sb.Append(_MAGIC4);
            sb.Append((bankCode + branchCode).PadLeft(7, '0'));
            sb.Append(account.PadLeft(14, '0'));
            sb.Append(amount.ToString().PadLeft(10, '0'));
            sb.Append(_MAGIC5);
            sb.Append(accountId.ToUpper().PadRight(10, ' '));
            sb.Append(_MAGIC6);
            sb.Append(id.ToString().PadRight(20, ' '));
            sb.Append(uniqueId.ToString().PadRight(20, ' '));
            sb.Append(new string(' ', 12));
            sb.Append("\r\n");
            _total += amount;
        }

        private string AchP1Head()
        {
            return string.Format("{0}{1}{2}{3}{4}\r\n",
                _BOF,
                (_time.Year - 1911).ToString().PadLeft(4, '0'),
                _time.ToString("MMddHHmmss"),
                _MAGIC,
                new string(' ', 123)
            );
        }

        private string AchP1Foot()
        {
            return string.Format("{0}{1}{2}{3}{4}{5}{6}\r\n",
                _EOF,
                (_time.Year - 1911).ToString().PadLeft(4, '0'),
                _time.ToString("MMdd"),
                _MAGIC,
                _count.ToString().PadLeft(8, '0'),
                _total.ToString().PadLeft(16, '0'),
                new string(' ', 105)
            );
        }

        public override string ToString()
        {
            if (_count == 0)
            {
                return string.Empty;
            }
            return string.Format("{0}{1}{2}\r\n\r\n", AchP1Head(), sb, AchP1Foot());
        }
    }
}

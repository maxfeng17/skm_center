﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LunchKingSite.WebLib.Component
{
    public class MasterPageTopBanner 
    {

        public string Id { get; set; }

        public MasterPageTopBanner(string id)
        {
            this.Id = id;
        }

        ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public int CityId { get; set; }
        /// <summary>
        /// 相容原元件，但不用
        /// </summary>
        public RandomCmsType Type { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            ViewCmsRandomCollection dataCol = CmsRandomFacade.GetOrAddData("/ppon/default.aspx_pponnews", RandomCmsType.PponRandomCms);
            if (config.SwitchableHttpsEnabled)
            {
                foreach (var data in dataCol)
                {
                    data.Body = data.Body.Replace("http://", "https://");
                }
            }
            DateTime now = DateTime.Now;
            var filterDatas = dataCol
                .Where(x => (x.CityId == CityId || x.CityId < 0) && now <= x.EndTime && now >= x.StartTime)
                .ToList();

            string urlPath = HttpContext.Current.Request.Path.ToLower();

            if (filterDatas.Count == 1)
            {
                sb.Append(filterDatas[0].Body);
            }
            else
            {
                #region Slider

                List<string> dataList = filterDatas
                                        .OrderByDescending(t => t.Id)
                                        .Select(t => t.Body)
                                        .ToList();


                //dataList = RandomReOrder(dataList);

                PrepareHtml(dataList, sb);

                #endregion
            }

            if (filterDatas.Count > 0)
            {
                return sb.ToString();
            }

            return string.Empty;
        }

        private void PrepareHtml(List<string> dataList, StringBuilder sb)
        {
            sb.AppendLine(string.Format("<div id='{0}' data-version='2'>", this.Id));
            
            sb.AppendLine("<div class='swiper-container'>");
            sb.AppendLine("<div class='swiper-wrapper'>");

            for (int i = 0; i < dataList.Count; i++)
            {
                sb.AppendLine("<div class='swiper-slide'>");
                sb.AppendLine(dataList[i]);
                sb.Append("</div>");
            }
            sb.AppendLine("</div>");
            sb.AppendLine("</div>");
            
            sb.AppendLine("<div class='banner-swiper-pagination swiper-pagination'></div>");
            sb.AppendLine("<div class='banner-swiper-next swiper-button-next'></div> ");
            sb.AppendLine("<div class='banner-swiper-prev swiper-button-prev'></div>");
            sb.AppendLine("</div>");
        }

        static Random seed = new Random();

        private List<string> RandomReOrder(List<string> dataList)
        {
            List<string> result = new List<string>();
            int n = seed.Next(dataList.Count);
            result.AddRange(dataList.Skip(n));
            result.AddRange(dataList.Take(n));
            // 1 2 3 4 5
            //n = 3
            // 4 5  1 2 3
            return result;
        }
    }
}

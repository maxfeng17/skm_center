﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Models;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;

namespace LunchKingSite.WebLib.Component
{
    public class VbsMembershipProvider : MembershipProvider
    {
        #region properties - implementation not needed

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public override bool EnablePasswordReset
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override bool EnablePasswordRetrieval
        {
            get
            {
                return false;
            }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override bool RequiresUniqueEmail
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override int PasswordAttemptWindow
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override int MinRequiredPasswordLength
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override string PasswordStrengthRegularExpression
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion properties - implementation not needed

        #region methods to be implemented

        public override bool ChangePassword(string accountId, string oldPassword, string newPassword)
        {
            VbsMembership mem = mp.VbsMembershipGetByAccountId(accountId);
            if (!ValidatePassword(mem, oldPassword))
            {
                if(!ValidateTemporaryPassword(mem, oldPassword))
                {
                    return false;
                }
            }

            VbsMembershipPasswordFormat passwordFormat;
            mem.Password = GetFormattedPassword(newPassword, out passwordFormat);
            mem.PasswordFormat = (int)passwordFormat;
            mem.LastPasswordChangedDate = DateTime.Now;

            mem.TemporaryPassword = null;
            mem.TemporaryPasswordDate = null;

            UpdateMember(mem);

            return true;
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 驗證使用者密碼. 只可在使用者登入時用.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public override bool ValidateUser(string username, string password)
        {
            VbsMembership mem = mp.VbsMembershipGetByAccountId(username);
            if (mem == null || mem.IsNew)
            {
                return false;
            }

            if (mem.IsLockedOut)
            {
                return false;
            }

            if (ValidatePassword(mem, password))
            {
                mem.LastLoginDate = DateTime.Now;
                mem.FailedPasswordAttemptCount = 0;
                mem.FailedPasswordAttemptWindowStart = null;
                mem.IsTemporaryLogin = false;
                mem.TemporaryPassword = null;   //正常登入後，無法再使用臨時密碼，必須重新申請
                mem.TemporaryPasswordDate = null;
                UpdateMember(mem);
                return true;
            }
            else
            {
                if (ValidateTemporaryPassword(mem, password))
                {
                    mem.LastLoginDate = DateTime.Now;
                    mem.FailedPasswordAttemptCount = 0;
                    mem.FailedPasswordAttemptWindowStart = null;
                    mem.IsTemporaryLogin = true;    //使用臨時密碼登入
                    UpdateMember(mem);
                    return true;
                }
                else
                {
                    mem.FailedPasswordAttemptCount++;

                    UpdateMember(mem);
                    return false;
                }
            }
        }

        #endregion methods to be implemented

        #region methods - implementation not needed

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        #endregion methods - implementation not needed

        #region class specific methods

        /// <summary>
        /// 開放給17life帳號登入(藉由會員登入成功後, 直接幫用商家帳號登入, 故拿不到原文密碼, 僅拿到從DB取的加密過後的密碼)
        /// </summary>
        /// <param name="username"></param>
        /// <param name="encryptPassword"></param>
        /// <returns></returns>
        public bool ValidateUserByEncryptPassword(string username, string encryptPassword)
        {
            VbsMembership mem = mp.VbsMembershipGetByAccountId(username);
            if (mem == null || mem.IsNew)
            {
                return false;
            }

            if (mem.IsLockedOut)
            {
                return false;
            }

            if (mem.Password.Equals(encryptPassword))//不管password_format是0還1, 皆從db撈值直接比對
            {
                mem.LastLoginDate = DateTime.Now;
                mem.FailedPasswordAttemptCount = 0;
                mem.FailedPasswordAttemptWindowStart = null;
                UpdateMember(mem);
                return true;
            }
            else
            {
                mem.FailedPasswordAttemptCount++;
                UpdateMember(mem);
                return false;
            }
        }

        /// <summary>
        /// 建立 17Life 核銷帳號及商家帳號
        /// </summary>

        public MembershipCreateStatus CreateCompanyVerificationAccount(NewMemberModelParser member)
        {
            string accountId = member.AccountId, password = member.Password;

            MembershipCreateStatus infoStatus;
            if (!VerifyAccountInfo(accountId, password, false, member.InspectionCode, out infoStatus))
            {
                return infoStatus;
            }

            VbsMembership mem = new VbsMembership();
            mem.AccountId = accountId;
            mem.AccountType = (int)member.AccountType;
            mem.Name = member.UserName;
            mem.Email = member.Email;
            VbsMembershipPasswordFormat passwordFormat;
            mem.Password = GetFormattedPassword(password, out passwordFormat);
            mem.PasswordFormat = (int)passwordFormat;
            //mem.HasInspectionCode = false;
            //mem.InspectionCode = null;
            mem.HasInspectionCode = member.UseInspectionCode;
            VbsMembershipPasswordFormat dummyFormat;
            mem.InspectionCode = member.UseInspectionCode ? GetFormattedPassword(member.InspectionCode, out dummyFormat) : null;
            mem.IsLockedOut = false;
            mem.IsApproved = true;
            mem.CreateTime = DateTime.Now;
            mem.LastLoginDate = mem.LastPasswordChangedDate = mem.LastInspectionDate
                = mem.LastInspectionCodeChangedDate = mem.LastLockoutDate = null;
            mem.FailedPasswordAttemptCount = 0;
            mem.FailedPasswordAttemptWindowStart = null;
            mem.FailedInspectionCodeAttemptCount = 0;
            mem.FailedInspectionCodeAttemptWindowStart = null;
            mem.ViewBsRight = member.ViewBalanceSheetRight;
            mem.ViewVerifyRight = member.ViewVerifyRight;
            mem.ViewShipRight = member.ViewShipRight;
            mem.ViewBookingRight = member.ViewBookingRight;
            mem.ViewReserveLockRight = member.ViewReserveLockRight;
            mem.ViewUserEvaluateRight = member.ViewUserEvaluateRight;
            mem.ViewProposalRight = member.ViewProposalRight;
            mem.ViewProposalProductRight = member.ViewProposalProductRight;
            mem.ViewShoppingCartRight = member.ViewShoppingCartRight;
            mem.ViewAccountManageRight = member.ViewAccountManageRight;
            mem.ViewCustomerServiceRight = member.ViewCustomerServiceManageRight;
            mem.ViewWmsRight = member.ViewWmsRight;
            mem.ViewToShop = member.ViewToShop;
            mem.ViewToHouse = member.ViewToHouse;
            try
            {
                mp.VbsMembershipSet(mem);
                return MembershipCreateStatus.Success;
            }
            catch (Exception)
            {
                return MembershipCreateStatus.ProviderError;
            }
            //mp.VbsMembershipSet(mem);
            //VbsMembership newMem = mp.VbsMembershipGetByAccountId(accountId);
            //createStatus = MembershipCreateStatus.Success;
            //return newMem;
        }

        public bool ChangeInspectionCode(string accountId, string oldInspectionCode, string newInspectionCode)
        {
            VbsMembership mem = mp.VbsMembershipGetByAccountId(accountId);
            if (!ValidateInspectionCode(mem, oldInspectionCode))
            {
                return false;
            }

            VbsMembershipPasswordFormat passwordFormat;
            mem.InspectionCode = GetFormattedPassword(newInspectionCode, out passwordFormat, true, VbsMembershipPasswordFormat.SHA256);
            mem.LastInspectionCodeChangedDate = DateTime.Now;

            UpdateMember(mem);

            return true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="password">明碼型式的密碼</param>
        /// <param name="passwordFormat">方法使用的編碼方式</param>
        /// <param name="useDesignatedFormat">是否要用指定的編碼方式</param>
        /// <param name="designatedFormat">指定的編碼方式</param>
        /// <returns>加密過的密碼</returns>
        public string GetFormattedPassword(string password, out VbsMembershipPasswordFormat passwordFormat, bool useDesignatedFormat = false, VbsMembershipPasswordFormat designatedFormat = VbsMembershipPasswordFormat.SHA256)
        {
            HashAlgorithm hasher;
            if (useDesignatedFormat && designatedFormat == VbsMembershipPasswordFormat.SHA256)
            {
                hasher = HashAlgorithm.Create("SHA256");
                passwordFormat = VbsMembershipPasswordFormat.SHA256;
            }
            else
            {
                hasher = GetHashAlgorithm(out passwordFormat);
            }
            byte[] bpw = Encoding.Unicode.GetBytes(password);
            byte[] bRet = hasher.ComputeHash(bpw);
            return Convert.ToBase64String(bRet);
        }

        public bool VerifyPassword(string accountId, string password)
        {
            VbsMembership mem = mp.VbsMembershipGetByAccountId(accountId);
            if (mem == null || mem.IsNew)
            {
                return false;
            }

            bool isValidated = ValidatePassword(mem, password);
            if (!isValidated)
            {
                isValidated = ValidateTemporaryPassword(mem, password);
            }

            return isValidated;
        }

        public bool VerifyInspectionCode(string accountId, string inspectionCode)
        {
            VbsMembership mem = mp.VbsMembershipGetByAccountId(accountId);
            if (mem == null || mem.IsNew)
            {
                return false;
            }

            return ValidateInspectionCode(mem, inspectionCode);
        }

        private bool VerifyAccountInfo(string accountId, string password, bool useInspectionCode, string inspectionCode, out MembershipCreateStatus infoStatus)
        {
            VbsMembership memCheck = mp.VbsMembershipGetByAccountId(accountId);
            if (memCheck != null && memCheck.IsLoaded)
            {
                infoStatus = MembershipCreateStatus.DuplicateProviderUserKey;
                return false;
            }
            VbsMembershipPasswordFormat dummyFormat;
            if (!CheckLength(GetFormattedPassword(password, out dummyFormat)))
            {
                infoStatus = MembershipCreateStatus.InvalidPassword;
                return false;
            }

            if (useInspectionCode && !CheckLength(GetFormattedPassword(inspectionCode, out dummyFormat)))
            {
                infoStatus = MembershipCreateStatus.InvalidAnswer;
                return false;
            }

            infoStatus = MembershipCreateStatus.Success;
            return true;
        }

        private HashAlgorithm GetHashAlgorithm(out VbsMembershipPasswordFormat passwordFormat)
        {
            passwordFormat = VbsMembershipPasswordFormat.SHA256;
            return HashAlgorithm.Create("SHA256");
        }

        private bool CheckLength(string password)
        {
            return password.Length <= 128;
        }

        private bool ValidatePassword(VbsMembership mem, string password)
        {
            switch (mem.PasswordFormat)
            {
                case (int)VbsMembershipPasswordFormat.Clear:
                    return mem.Password == password;

                case (int)VbsMembershipPasswordFormat.SHA256:
                    VbsMembershipPasswordFormat dummyFormat;
                    return mem.Password == GetFormattedPassword(password, out dummyFormat);

                default:
                    return false;
            }
        }

        private bool ValidateTemporaryPassword(VbsMembership mem, string password)
        {
            if (mem.TemporaryPasswordDate != null && mem.TemporaryPassword != null)
            {
                TimeSpan ts = DateTime.Now - mem.TemporaryPasswordDate.Value;
                //檢查臨時密碼是否已過期
                if (ts.TotalMinutes <= config.VbsTemporaryPasswordHourLimit)
                {
                    switch (mem.PasswordFormat)
                    {
                        case (int)VbsMembershipPasswordFormat.Clear:
                            return mem.TemporaryPassword == password;

                        case (int)VbsMembershipPasswordFormat.SHA256:
                            VbsMembershipPasswordFormat dummyFormat;
                            return mem.TemporaryPassword == GetFormattedPassword(password, out dummyFormat);

                        default:
                            return false;
                    }
                }
                else
                {
                    //若臨時密碼已過期，直接清除
                    mem.TemporaryPassword = null;
                    mem.TemporaryPasswordDate = null;

                    return false;
                }

            }
            return false;
        }

        private bool ValidateInspectionCode(VbsMembership mem, string inspectionCode)
        {
            VbsMembershipPasswordFormat dummyFormat;
            return mem.InspectionCode == GetFormattedPassword(inspectionCode, out dummyFormat, true, VbsMembershipPasswordFormat.SHA256);
        }

        private void UpdateMember(VbsMembership mem)
        {
            if (mem != null && !mem.IsNew)
            {
                mp.VbsMembershipSet(mem);
            }
        }

        #endregion class specific methods

        private IMemberProvider mp;

        public VbsMembershipProvider()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }
    }
}
﻿using ICSharpCode.SharpZipLib.Zip;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Reflection;
using System.Drawing;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Component
{
    public class ImageUtility
    {
        private static ISysConfProvider _conf;
        protected static ILog Logger = LogManager.GetLogger("ImageUtility");

        static ImageUtility()
        {
            _conf = ProviderFactory.Instance().GetConfig();
        }

        /// <summary>
        /// 取得image裡的dirpath資料夾中的所有檔案
        /// </summary>
        /// <param name="dirPath"></param>
        public static List<ImageFile> GetFileList(string searchStr, string dirPath)
        {
            string baseDirectoryPath = System.Web.HttpContext.Current.Server.MapPath("~/Images/");
            List<ImageFile> parentFileList = new List<ImageFile>();
            ImageFacade.CreateDirectory(Path.Combine(baseDirectoryPath, dirPath));
            DirectoryInfo baseDirectory = new DirectoryInfo(Path.Combine(baseDirectoryPath, dirPath));
            DirectoryInfo[] DirArray = baseDirectory.GetDirectories();
            string siteUrl = _conf.SiteUrl.Substring(_conf.SiteUrl.Length - 1, 1) == "/" ? _conf.SiteUrl.Remove(_conf.SiteUrl.Length - 1, 1) : _conf.SiteUrl;
            parentFileList = (from f in baseDirectory.EnumerateFiles()
                              select new ImageFile()
                              {
                                  FileName = f.Name,
                                  Url = Path.Combine(siteUrl + "/Images/" + dirPath + f.Name),
                                  AcessTime = f.LastAccessTime
                              }).ToList();

            foreach (var dir in DirArray)
            {
                var files = (from f in dir.EnumerateFiles()
                             select new ImageFile()
                             {
                                 FileName = Path.Combine(dir.Name + "\\" + f.Name),
                                 Url = Path.Combine(siteUrl + "/Images/" + dirPath + dir.Name + "/" + f.Name),
                                 AcessTime = f.LastAccessTime
                             });
                parentFileList.AddRange(files);
            }

            if (!string.IsNullOrEmpty(searchStr))
            {
                parentFileList = parentFileList.Where(x => x.FileName.ToLower().Contains(searchStr)).ToList();
            }

            return parentFileList.OrderByDescending(x => x.AcessTime).ToList();
        }

        /// <summary>
        /// 後台上傳檔次圖片功能
        /// </summary>
        /// <param name="uploadedFile">上傳的檔案</param>
        /// <param name="sellerId">賣家ID(取路徑用)</param>
        /// <param name="bid">檔次guid</param>
        /// <param name="imagePath">檔次圖片路徑"|"分隔多個</param>
        /// <param name="isUploadByOrder">是否按順序上圖(1,2,3,其他輪播大圖)</param>
        /// <param name="ifPrintWatermark">是否自動壓Logo</param>
        /// <param name="isSideDeal">是否為SideDeal小圖</param>
        /// <returns>上傳後的檔次圖片路徑(多個"|"分隔)</returns>
        public static string UploadPponImageAndGetNewPath(HttpPostedFileBase uploadedFile, string sellerId, string bid, string imagePath, bool isUploadByOrder, bool ifPrintWatermark, bool isSideDeal)
        {
            string dealContentImagePath = string.Empty;
            if (uploadedFile != null)
            {
                List<string> originalOrder = Helper.GetRawPathsFromRawData(imagePath).ToList<string>();
                #region 組檔名
                //set first pic with a fixed name by bid&"EDM"
                string fileName = string.Empty;
                string fileNameWithExtension = string.Empty;
                if (originalOrder.Count == 0 || originalOrder[0] == string.Empty)
                {
                    fileName = string.Format("{0}", bid.Replace("-", "EDM"));
                    fileNameWithExtension = string.Format("{0}.{1}?ts={2}", fileName, Helper.GetExtensionByContentType(uploadedFile.ContentType), Helper.GetCurrentTimeStampHashCode());
                }
                else
                {
                    fileName = DateTime.Now.Ticks.ToString();
                    fileNameWithExtension = string.Format("{0}.{1}", fileName, Helper.GetExtensionByContentType(uploadedFile.ContentType));
                }
                #endregion

                UploadFile(uploadedFile.ToAdapter(), UploadFileType.PponEvent, sellerId, fileName, ifPrintWatermark);

                //小圖的話要壓縮
                if (isSideDeal)
                {
                    string baseDirectoryPath = System.Web.HttpContext.Current.Server.MapPath("~/media/");
                    string path = Path.Combine(Path.Combine(baseDirectoryPath, sellerId), fileName + "." + Helper.GetExtensionByContentType(uploadedFile.ContentType));
                    string format = Helper.GetExtensionByContentType(uploadedFile.ContentType);
                    System.Drawing.Image image = System.Drawing.Image.FromFile(path);
                    Image rtnImage = image.GetThumbnailImage(300, 167, null, IntPtr.Zero); // 取得縮略圖
                    image.Dispose();
                    ImageCodecInfo jgpEncoder = ImageFacade.GetImageCodecInfo(uploadedFile.ContentType);
                    EncoderParameter encoderParameter = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 90L); // 調整圖片品質
                    EncoderParameters encoderParameters = new EncoderParameters(1);
                    encoderParameters.Param[0] = encoderParameter;
                    rtnImage.Save(path, jgpEncoder, encoderParameters);
                }

                if (isUploadByOrder)
                {
                    if (originalOrder.Count > 0)
                    {
                        bool check = false;
                        for (int i = 0; i < 3; i++)
                        {
                            if (originalOrder.Count > i && originalOrder[i] == string.Empty)
                            {
                                originalOrder[i] = ImageFacade.GenerateMediaPath(sellerId, fileNameWithExtension);
                                dealContentImagePath = Helper.GenerateRawDataFromRawPaths(originalOrder.ToArray());
                                check = true;
                                break;
                            }
                        }

                        if (!check)
                        {
                            dealContentImagePath = Helper.AppendRawDataWithRawPath(imagePath, ImageFacade.GenerateMediaPath(sellerId, fileNameWithExtension));
                        }
                    }
                    else
                    {
                        dealContentImagePath = Helper.AppendRawDataWithRawPath(imagePath, ImageFacade.GenerateMediaPath(sellerId, fileNameWithExtension));
                    }
                }
                else
                {
                    if (originalOrder.Count > 0 && originalOrder[0] == string.Empty)
                    {
                        originalOrder[0] = ImageFacade.GenerateMediaPath(sellerId, fileNameWithExtension);
                        dealContentImagePath = Helper.GenerateRawDataFromRawPaths(originalOrder.ToArray());
                    }
                    else
                    {
                        for (int i = 1; i < 3; i++)
                        {
                            if (originalOrder.Count == i)
                            {
                                originalOrder.Add(string.Empty);
                            }
                        }
                        dealContentImagePath = Helper.GenerateRawDataFromRawPaths(originalOrder.ToArray());
                        dealContentImagePath = Helper.AppendRawDataWithRawPath(dealContentImagePath, ImageFacade.GenerateMediaPath(sellerId, fileNameWithExtension));
                    }
                }
            }

            return dealContentImagePath;
        }

        /// <summary>
        /// 上傳去背圖片
        /// </summary>
        /// <param name="uploadedFile"></param>
        /// <param name="sellerId"></param>
        /// <param name="bid"></param>
        /// <param name="imagePath"></param>
        /// <param name="isUploadByOrder"></param>
        /// <param name="ifPrintWatermark"></param>
        /// <returns></returns>
        public static string UploadRemoveBGImageAndGetNewPath(HttpPostedFileBase uploadedFile, string sellerId, bool ifPrintWatermark)
        {
            string dealContentImagePath = string.Empty;
            if (uploadedFile != null)
            {
                #region 組檔名
                string fileName = string.Empty;
                string fileNameWithExtension = string.Empty;

                fileName = DateTime.Now.Ticks.ToString() + "RemoveBG";
                fileNameWithExtension = string.Format("{0}.{1}?ts={2}", fileName, Helper.GetExtensionByContentType(uploadedFile.ContentType), Helper.GetCurrentTimeStampHashCode());
                #endregion

                UploadFile(uploadedFile.ToAdapter(), UploadFileType.PponEvent, sellerId, fileName, ifPrintWatermark);


                
                dealContentImagePath = ImageFacade.GenerateMediaPath(sellerId, fileNameWithExtension);
            }

            return dealContentImagePath;
        }

        /// <summary>
        /// 上傳檔案(直接上傳沒有判斷uploadtype，使用PostedFileBase)
        /// </summary>
        /// <param name="pFile">postfile</param>
        /// <param name="baseDirectoryPath">相對路徑(此路徑不含個別檔案夾)</param>
        /// <param name="folder">檔案夾</param>
        /// <param name="fileName">檔名</param>
        public static void UploadFileForFileBase(HttpPostedFileBase pFile, string baseDirectoryPath, string folder, string fileName)
        {
            string path = Path.Combine(baseDirectoryPath, folder);
            string fullpath = Path.Combine(path, fileName);
            ImageFacade.CreateDirectory(Path.Combine(path));
            pFile.SaveAs(fullpath);
        }


        /// <summary>
        /// 上傳檔案(直接上傳沒有判斷uploadtype)
        /// </summary>
        /// <param name="pFile">postfile</param>
        /// <param name="baseDirectoryPath">相對路徑(注意此路徑不包含image或media,如果要上傳到這兩個資料夾記得combine)</param>
        /// <param name="destFileName">檔名</param>
        public static void UploadFile(IHttpPostedFileAdapter pFile, string baseDirectoryPath, string destFileName)
        {
            string fullpath = Path.Combine(baseDirectoryPath, destFileName);
            ImageFacade.CreateDirectory(Path.Combine(baseDirectoryPath));
            pFile.SaveAs(fullpath);
            //假如是zip壓縮檔解壓縮以後儲存
            if (Path.GetExtension(destFileName).ToLower() == ".zip")
            {
                using (ZipInputStream s = new ZipInputStream(File.OpenRead(fullpath)))
                {
                    ZipEntry theEntry;

                    // 逐一取出壓縮檔內的檔案(解壓縮)
                    while ((theEntry = s.GetNextEntry()) != null)
                    {
                        int size = 4096;
                        byte[] data = new byte[4096];
                        string targetpath = baseDirectoryPath;
                        //假如zip檔裡有有多包一層資料夾,要建立一個新的資料夾在解壓縮
                        if (string.IsNullOrEmpty(Path.GetExtension(theEntry.Name)))
                        {
                            ImageFacade.CreateDirectory(Path.Combine(baseDirectoryPath, theEntry.Name));
                            targetpath = Path.Combine(baseDirectoryPath, theEntry.Name);
                        }
                        else
                        {
                            // 寫入檔案
                            using (FileStream fs = new FileStream(Path.Combine(targetpath, theEntry.Name), FileMode.Create))
                            {
                                while (true)
                                {
                                    size = s.Read(data, 0, data.Length);
                                    if (size > 0)
                                    {
                                        fs.Write(data, 0, size);
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                if (File.Exists(fullpath))
                {
                    File.Delete(fullpath);
                }
            }
        }

        public static void UploadFileUNC(IHttpPostedFileAdapter pFile, string upLoadType, string destFileName)
        {
            if (_conf.IsImageLoadBalance)
            {
                ImpersonateAccount account;
                if (PponFacade.ImpersonateAccountGet(Dns.GetHostName(), out account))
                {
                    try
                    {
                        using (new ImpersonateUtility(account.UserName, string.Empty, account.PassWord))
                        {
                            string baseDirectoryPath = Path.Combine(account.UNCPath, ImageLoadBalanceFolder.Images.ToString(), upLoadType);
                            string fullpath = Path.Combine(baseDirectoryPath, destFileName);
                            ImageFacade.CreateDirectory(baseDirectoryPath);
                            pFile.SaveAs(fullpath);
                            //假如是zip壓縮檔解壓縮以後儲存
                            if (Path.GetExtension(destFileName).ToLower() == ".zip")
                            {
                                using (ZipInputStream s = new ZipInputStream(File.OpenRead(fullpath)))
                                {
                                    ZipEntry theEntry;

                                    // 逐一取出壓縮檔內的檔案(解壓縮)
                                    while ((theEntry = s.GetNextEntry()) != null)
                                    {
                                        int size = 4096;
                                        byte[] data = new byte[4096];
                                        string targetpath = baseDirectoryPath;
                                        //假如zip檔裡有多包一層資料夾,要建立一個新的資料夾在解壓縮
                                        if (string.IsNullOrEmpty(Path.GetExtension(theEntry.Name)))
                                        {
                                            ImageFacade.CreateDirectory(Path.Combine(baseDirectoryPath, theEntry.Name));
                                            targetpath = Path.Combine(baseDirectoryPath, theEntry.Name);
                                        }
                                        else
                                        {
                                            // 寫入檔案
                                            using (FileStream fs = new FileStream(Path.Combine(targetpath, theEntry.Name), FileMode.Create))
                                            {
                                                while (true)
                                                {
                                                    size = s.Read(data, 0, data.Length);
                                                    if (size > 0)
                                                    {
                                                        fs.Write(data, 0, size);
                                                    }
                                                    else
                                                    {
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (File.Exists(fullpath))
                                {
                                    File.Delete(fullpath);
                                }
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        Logger.ErrorFormat("圖片同步出錯，UploadFile，目前Server:{0}，{1}", Dns.GetHostName(), error.Message);
                    }
                }
            }
        }

        public static void UploadFile(IHttpPostedFileAdapter pFile, UploadFileType type, string destAppendingDirPath, string destFileName, bool isPrintWatermark = false, Image imgData = null)
        {
            //要放在media下面的其他資料夾
            string baseFolder = GetMediaBasePath(type);
            string baseDirectoryPath = System.Web.HttpContext.Current.Server.MapPath(baseFolder);
            ImageFacade.CreateDirectory(Path.Combine(baseDirectoryPath, destAppendingDirPath));
            string tempFilePath = Path.Combine(baseDirectoryPath, destAppendingDirPath, Path.GetRandomFileName() + ".tmp");
            UploadFile(baseDirectoryPath, pFile, type, destAppendingDirPath, destFileName, tempFilePath, isPrintWatermark, imgData);
            UploadFileUNC(pFile, type, destAppendingDirPath, destFileName, isPrintWatermark, imgData);
        }

        /// <summary>
        /// 上傳圖片loadbalance
        /// </summary>
        /// <param name="pFile">fileupload上傳檔案</param>
        /// <param name="type">圖片類型</param>
        /// <param name="destAppendingDirPath">目的檔案夾路徑</param>
        /// <param name="destFileName">目的檔案名稱</param>
        /// <param name="isPrintWatermark">是否加浮水印</param>
        /// <param name="imgData">可直接傳入 Image (不透過 fileupload control)</param>
        private static void UploadFileUNC(IHttpPostedFileAdapter pFile, UploadFileType type, string destAppendingDirPath, string destFileName, bool isPrintWatermark = false, Image imgData = null)
        {
            if (_conf.IsImageLoadBalance)
            {
                ImpersonateAccount account;
                if (PponFacade.ImpersonateAccountGet(Dns.GetHostName(), out account))
                {
                    try
                    {
                        using (new ImpersonateUtility(account.UserName, string.Empty, account.PassWord))
                        {
                            string destFolder = Path.Combine(account.UNCPath, ImageLoadBalanceFolder.Media.ToString());
                            ImageFacade.CreateDirectory(Path.Combine(destFolder, destAppendingDirPath));
                            string tempFilePath = Path.Combine(destFolder, destAppendingDirPath, Path.GetRandomFileName() + ".tmp");
                            if (Directory.Exists(destFolder))
                            {
                                UploadFile(destFolder, pFile, type, destAppendingDirPath, destFileName, tempFilePath, isPrintWatermark, imgData);
                            }

                            if (type == UploadFileType.SellerPhoto && File.Exists(tempFilePath))
                            {
                                File.Delete(tempFilePath);
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        Logger.ErrorFormat("圖片同步出錯，UploadFile，目前Server:{0}，{1}", Dns.GetHostName(), error.Message);
                    }
                }
            }
        }

        /// <summary>
        /// 調整圖片大小loadbalance
        /// </summary>
        /// <param name="pFile">fileupload上傳檔案</param>
        /// <param name="type">圖片類型</param>
        /// <param name="destAppendingDirPath">目的檔案夾路徑</param>
        /// <param name="destFileName">目的檔案名稱</param>
        /// <param name="width">寬</param>
        /// <param name="height">長</param>
        private static void ResizeImageUNC(IHttpPostedFileAdapter pFile, UploadFileType type, string destAppendingDirPath, string destFileName, int width, int height)
        {
            if (_conf.IsImageLoadBalance)
            {
                ImpersonateAccount account;
                if (PponFacade.ImpersonateAccountGet(Dns.GetHostName(), out account))
                {
                    try
                    {
                        using (new ImpersonateUtility(account.UserName, string.Empty, account.PassWord))
                        {
                            string baseDirectoryPath = Path.Combine(account.UNCPath, ImageLoadBalanceFolder.Media.ToString());
                            ResizeImage(pFile, type, baseDirectoryPath, destAppendingDirPath, destFileName, width, height);
                        }
                    }
                    catch (Exception error)
                    {
                        Logger.ErrorFormat("圖片同步出錯，ResizeImage，目前Server:{0}，{1}", Dns.GetHostName(), error.Message);
                    }
                }
            }
        }

        /// <summary>
        /// 調整圖片大小
        /// </summary>
        /// <param name="pFile">fileupload上傳檔案</param>
        /// <param name="type">圖片類型</param>
        /// <param name="baseDirectoryPath">目的檔案夾路徑</param>
        /// <param name="destAppendingDirPath">目的檔案名稱</param>
        /// <param name="destFileName"></param>
        /// <param name="width">寬</param>
        /// <param name="height">長</param>
        private static void ResizeImage(IHttpPostedFileAdapter pFile, UploadFileType type, string baseDirectoryPath, string destAppendingDirPath, string destFileName, int width, int height)
        {
            List<string> seller_folders = new List<string>() { "large", "small", "original" };
            ImageCodecInfo jpegCodec = GetEncoderInfo("image/jpeg");
            EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;
            if (type == UploadFileType.SellerPhoto)
            {
                foreach (string folder in seller_folders)
                {
                    string path = Path.Combine(Path.Combine(baseDirectoryPath, Path.Combine(destAppendingDirPath, folder)), destFileName + "." + Helper.GetExtensionByContentType(pFile.ContentType));
                    System.Drawing.Image image = System.Drawing.Image.FromFile(path);
                    int iwidth = 0;
                    int iheight = 0;
                    switch (folder)
                    {
                        case "large":
                            iwidth = 151;
                            iheight = 112;
                            break;

                        case "small":
                            iwidth = 121;
                            iheight = 89;
                            break;

                        default:
                            iwidth = width;
                            iheight = height;
                            break;
                    }
                    System.Drawing.Image resizeimage = ImageFacade.ResizeImage(image, new System.Drawing.Size(iwidth, iheight), true);
                    image.Dispose();
                    resizeimage.Save(path, jpegCodec, encoderParams);
                    resizeimage.Dispose();
                }
            }
            else
            {
                string path = Path.Combine(Path.Combine(baseDirectoryPath, destAppendingDirPath), destFileName + "." + Helper.GetExtensionByContentType(pFile.ContentType));
                System.Drawing.Image image = System.Drawing.Image.FromFile(path);
                System.Drawing.Image resizeimage = ImageFacade.ResizeImage(image, new System.Drawing.Size(width, height), true);
                image.Dispose();
                resizeimage.Save(path, jpegCodec, encoderParams);
                resizeimage.Dispose();
            }
        }

        /// <summary>
        /// 縮放圖片(等比例縮放)
        /// </summary>
        /// <param name="folder">資料夾</param>
        /// <param name="dirPath">相對位置</param>
        /// <param name="fileName">檔案名稱</param>
        /// <param name="width"></param>
        private static void ResizeImageForRatio(string dirPath, string folder, string fileName, int width,bool small)
        {
            string path = Path.Combine(Path.Combine(dirPath, folder), fileName);
            System.Drawing.Image image = System.Drawing.Image.FromFile(path);
            int iwidth = image.Width;
            int iheight = image.Height;
            if (iwidth > width)
            {
                System.Drawing.Image resizeimage = ImageFacade.ResizeImage(image, new System.Drawing.Size(width, iheight * width / iwidth), true);
                image.Dispose();
                resizeimage.Save(path);
                resizeimage.Dispose();
            }
            else
            {
                image.Dispose();
            }

            if (small)
            {
                string targetPath = Path.Combine(Path.Combine(dirPath, folder), "Small");
                string smallPath = Path.Combine(Path.Combine(Path.Combine(dirPath, folder), "Small"), fileName);
                ImageFacade.CreateDirectory(targetPath);
                File.Copy(path, smallPath);
                System.Drawing.Image smallImage = System.Drawing.Image.FromFile(smallPath);
                int swidth = smallImage.Width;
                int sheight = smallImage.Height;
                int changewidth = 320;

                if (swidth > changewidth)
                {
                    System.Drawing.Image smallresizeimage = ImageFacade.ResizeImage(smallImage, new System.Drawing.Size(changewidth, sheight * changewidth / swidth), true);
                    smallImage.Dispose();
                    smallresizeimage.Save(smallPath);
                    smallresizeimage.Dispose();
                }
            }
        }


        public static void UploadFileWithResize(IHttpPostedFileAdapter pFile, UploadFileType type, string destAppendingDirPath, string destFileName, int width, int height)
        {
            string baseDirectoryPath = System.Web.HttpContext.Current.Server.MapPath("~/media/");
            UploadFile(pFile, type, destAppendingDirPath, destFileName);
            ResizeImage(pFile, type, baseDirectoryPath, destAppendingDirPath, destFileName, width, height);
            ResizeImageUNC(pFile, type, destAppendingDirPath, destFileName, width, height);
        }

        /// <summary>
        /// 由後台上傳圖檔(使用PostedFileBase方式)含縮放功能
        /// </summary>
        /// <param name="pFile">posetfile</param>
        /// <param name="dirPath">相對路徑(不包含資料夾)</param>
        /// <param name="folder">資料夾</param>
        /// <param name="destFileName">檔案名稱</param>
        /// <param name="width">寬度預設為0代表原圖上傳，變更Size後高度以比例方式變更</param>
        public static void UploadFileWithResizeForFileBase(HttpPostedFileBase pFile, string dirPath, string folder, string fileName, int width = 0,bool small=true)
        {
            UploadFileForFileBase(pFile, dirPath, folder, fileName);
            if (width != 0)
            {
                ResizeImageForRatio(dirPath, folder, fileName, width,small);
            }
        }



        public static void GenerateImageWithDiscount(string imgPath, string savePath, bool isCutCenter, string discountString, Size size, bool isPrintLog = false)
        {
            string baseDirectoryPath = System.Web.HttpContext.Current.Server.MapPath("~/media/");
            GenerateImageWithDiscount(baseDirectoryPath, imgPath, savePath, isCutCenter, discountString, size, isPrintLog);
            GenerateImageWithDiscountUNC(imgPath, savePath, isCutCenter, discountString, size, isPrintLog);

        }

        public static void GenerateImageWithDiscountUNC(string imgPath, string savePath, bool isCutCenter, string discountString, Size size, bool isPrintLog = false)
        {
            if (_conf.IsImageLoadBalance)
            {
                ImpersonateAccount account;
                if (PponFacade.ImpersonateAccountGet(Dns.GetHostName(), out account))
                {
                    try
                    {
                        using (new ImpersonateUtility(account.UserName, string.Empty, account.PassWord))
                        {
                            string baseDirectoryPath = Path.Combine(account.UNCPath, ImageLoadBalanceFolder.Media.ToString()) + @"\";
                            GenerateImageWithDiscount(baseDirectoryPath, imgPath, savePath, isCutCenter, discountString, size, isPrintLog);
                        }
                    }
                    catch (Exception error)
                    {
                        Logger.ErrorFormat("圖片同步出錯，GenerateSpecificImage，目前Server:{0}，{1}", Dns.GetHostName(), error.Message);
                    }
                }
            }
        }

        /// <summary>
        /// 根據原圖片路徑產生指定尺寸圖片(For Yahoo)
        /// </summary>
        /// <param name="imgPath">原圖片路徑</param>
        /// <param name="savePath">儲存圖片位置</param>
        /// <param name="isCutCenter">是否置中裁剪</param>
        /// <param name="discountString">折數字串(若不壓折數傳入空白字串)</param>
        /// <param name="isPrintLog">是否壓Logo</param>
        private static void GenerateImageWithDiscount(string baseDirectoryPath, string imgPath, string savePath, bool isCutCenter, string discountString, Size size, bool isPrintLog = false)
        {
            if (File.Exists(baseDirectoryPath + imgPath))
            {
                Image initialImage = GetImageWithDiscount(baseDirectoryPath + imgPath, isCutCenter, discountString, ref size, isPrintLog);

                ImageCodecInfo jgpEncoder = ImageFacade.GetImageCodecInfo("image/jpeg");
                System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.Quality;
                EncoderParameter encoderParameter = new EncoderParameter(encoder, 90L);
                EncoderParameters encoderParameters = new EncoderParameters(1);
                encoderParameters.Param[0] = encoderParameter;

                initialImage.Save(baseDirectoryPath + savePath, jgpEncoder, encoderParameters);
                initialImage.Dispose();
            }
        }

        private static Image GetImageWithDiscount(string imgPath, bool isCutCenter, string discountString, ref Size size, bool isPrintLog)
        {
            //取得原圖
            Image initialImage = Image.FromFile(imgPath);

            //裁切
            initialImage = ImageFacade.CutForCustom(initialImage, size.Width, size.Height, 100, isCutCenter);

            //壓折數
            if (!string.IsNullOrWhiteSpace(discountString))
            {
                Graphics gr = Graphics.FromImage(initialImage);

                //取得折數底圖
                string logoFileName = "discount.png";
                string discountMarkImagePath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/images/"), logoFileName);
                Image discountImage = Image.FromFile(discountMarkImagePath);

                //畫上折數底圖
                ImageAttributes imgAttributes = new ImageAttributes();
                ColorMatrix cmatrix = new ColorMatrix();
                imgAttributes.SetColorMatrix(cmatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
                gr.DrawImage(discountImage, new Rectangle(7, 10, discountImage.Width, discountImage.Height), 0, 0, discountImage.Width, discountImage.Height, GraphicsUnit.Pixel, imgAttributes);

                //壓上折數
                Font fontD = new Font(FontFamily.GenericSansSerif, 20, FontStyle.Regular);
                Point originD = new Point(11, 25);
                gr.DrawString(discountString, fontD, new SolidBrush(Color.White), originD);
                Point originS = new Point(52, 36);
                Font fontS = new Font(FontFamily.GenericSansSerif, 10, FontStyle.Regular);
                gr.DrawString("折", fontS, new SolidBrush(Color.White), originS);

                discountImage.Dispose();
                gr.Dispose();
            }

            //壓logo
            if (isPrintLog)
            {
                ImageFacade.PrintLogo(ref initialImage);
            }
            return initialImage;
        }

        private static ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            // Get image codecs for all image formats
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec
            for (int i = 0; i < codecs.Length; i++)
            {
                if (codecs[i].MimeType == mimeType)
                {
                    return codecs[i];
                }
            }

            return null;
        }

        public static void UploadFile(string baseDirectoryPath, IHttpPostedFileAdapter pFile, UploadFileType type,
            string destAppendingDirPath, string destFileName, string tempFilePath, bool isPrintWatermark = false,
            Image imgData = null)
        {
            string fileName = GetUploadFileName(baseDirectoryPath, pFile, type, destAppendingDirPath, destFileName, tempFilePath, imgData);
            if (!string.IsNullOrWhiteSpace(fileName))
            {
                ImageFacade.CreateDirectory(baseDirectoryPath);
                ImageFacade.CreateDirectory(Path.Combine(baseDirectoryPath, destAppendingDirPath));
                if (isPrintWatermark)
                {
                    ImageFacade.PrintWatermark(pFile, fileName, imgData);
                }
                else if (type == UploadFileType.SellerPhoto)
                {
                    ImageFacade.UploadToSeller(pFile, baseDirectoryPath, destAppendingDirPath, fileName);
                }
                else
                {
                    if (pFile != null)
                    {
                        pFile.SaveAs(fileName);
                    }
                    else if (imgData != null)
                    {
                        imgData.Save(fileName);
                    }
                }

                if (type.EqualsAny(
                    UploadFileType.DelicaciesSellerPhoto, UploadFileType.ItemPhoto,
                    UploadFileType.DelicaciesItemPhoto, UploadFileType.CityPhoto,
                    UploadFileType.SelectedStore))
                {
                    ImageFacade.ConvertImage(tempFilePath, Path.Combine(baseDirectoryPath, destAppendingDirPath),
                        destFileName + "." + Helper.GetExtensionByContentType(pFile.ContentType), type);
                }
            }
        }

        private static string GetUploadFileName(string baseDirectoryPath, IHttpPostedFileAdapter pFile,
            UploadFileType type, string destAppendingDirPath, string destFileName, string tempFilePath,
            Image imgData = null)
        {
            string fileName = string.Empty;
            var contentType = string.Empty;

            if (pFile != null)
            {
                contentType = pFile.ContentType;
            }
            else if (imgData != null)
            {
                contentType = GetImageMimeType(imgData);
            }

            switch (type)
            {
                case UploadFileType.CityPhoto:
                case UploadFileType.ItemPhoto:
                case UploadFileType.DelicaciesItemPhoto:
                case UploadFileType.DelicaciesSellerPhoto:
                case UploadFileType.SelectedStore:
                    if (contentType == "image/pjpeg" || contentType == "image/jpeg" || contentType == "image/x-png" ||
                        contentType == "image/png" || contentType == "image/gif")
                    {
                        fileName = tempFilePath;
                    }
                    break;
                case UploadFileType.SellerPhoto:
                case UploadFileType.PponEvent:
                case UploadFileType.YahooBigImage:
                case UploadFileType.EventPromoAppBanner:
                case UploadFileType.EventPromoAppMainImage:
                case UploadFileType.DealPromoImage:
                case UploadFileType.PCPImage:
                case UploadFileType.BrandWebImage:
                case UploadFileType.BrandAppImage:
                case UploadFileType.EventPromoItemPic:
                case UploadFileType.BrandDiscountBannerImage:
                case UploadFileType.EventListImage:
                case UploadFileType.ChannelListImage:
                case UploadFileType.WebRelayImage:
                case UploadFileType.MobileRelayImage:
                case UploadFileType.FbShareImage:
                case UploadFileType.ThemeImage:
                    fileName = Path.Combine(Path.Combine(baseDirectoryPath, destAppendingDirPath),
                        destFileName + "." + Helper.GetExtensionByContentType(contentType));
                    break;
                case UploadFileType.HiDealPrimaryBigPhoto:
                case UploadFileType.HiDealPrimarySmallPhoto:
                case UploadFileType.HiDealSecondaryPhoto:
                case UploadFileType.PezPromoImage:
                    fileName = Path.Combine(Path.Combine(baseDirectoryPath, destAppendingDirPath), destFileName);
                    break;
            }
            return fileName;
        }

        private static string GetMediaBasePath(UploadFileType type)
        {
            string result = "~/media/";
            switch (type)
            {
                case UploadFileType.PCPImage:
                    result += "regulars/";
                    break;
            }
            return result;
        }

        /// <summary>
        /// 刪除檔案(直接刪除)
        /// </summary>
        /// <param name="baseDirectoryPath">相對路徑(注意此路徑不包含image或media,如果要上傳到這兩個資料夾記得combine)</param>
        /// <param name="destFileName">檔名</param>
        public static void DeleteFile(string baseDirectoryPath, string destFileName)
        {
            string fullpath = Path.Combine(baseDirectoryPath, destFileName);
            if (File.Exists(fullpath))
            {
                File.Delete(fullpath);
            }
        }

        public static void DeleteFile(UploadFileType type, string destPath, string destFileName)
        {
            string prePath = string.Empty;
            switch (type)
            {
                case UploadFileType.PCPImage:
                    prePath = HttpContext.Current.Server.MapPath("~/media/regulars");
                    break;
                default:
                    prePath = HttpContext.Current.Server.MapPath("~/media/");
                    break;
            }

            string filePath = Path.Combine(prePath, Path.Combine(destPath, destFileName));
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            DeleteFileUNC(destPath, destFileName);
        }

        public static void DeleteFileUNC(string destFileName)
        {
            if (_conf.IsImageLoadBalance)
            {
                ImpersonateAccount account;
                if (PponFacade.ImpersonateAccountGet(Dns.GetHostName(), out account))
                {
                    try
                    {
                        using (new ImpersonateUtility(account.UserName, string.Empty, account.PassWord))
                        {
                            string filePath = Path.Combine(account.UNCPath, ImageLoadBalanceFolder.Images.ToString(), destFileName);
                            if (File.Exists(filePath))
                            {
                                File.Delete(filePath);
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        Logger.ErrorFormat("圖片同步出錯，DeleteFile，目前Server:{0}，{1}", Dns.GetHostName(), error.Message);
                    }
                }
            }
        }

        /// <summary>
        /// 刪除檔案loadbalance
        /// </summary>
        /// <param name="destPath">目的檔案夾路徑</param>
        /// <param name="destFileName">目的檔案名稱</param>
        /// <param name="username">網芳登入帳號</param>
        /// <param name="password">網芳登入密碼</param>
        /// <param name="unc_path">網芳路徑</param>
        private static void DeleteFileUNC(string destPath, string destFileName)
        {
            if (_conf.IsImageLoadBalance)
            {
                ImpersonateAccount account;
                if (PponFacade.ImpersonateAccountGet(Dns.GetHostName(), out account))
                {
                    try
                    {
                        using (new ImpersonateUtility(account.UserName, string.Empty, account.PassWord))
                        {
                            string filePath = Path.Combine(account.UNCPath, ImageLoadBalanceFolder.Media.ToString(), destPath, destFileName);
                            if (File.Exists(filePath))
                            {
                                File.Delete(filePath);
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        Logger.ErrorFormat("圖片同步出錯，DeleteFile，目前Server:{0}，{1}", Dns.GetHostName(), error.Message);
                    }
                }
            }
        }

        public static void DeleteFilesInDirectory(string mediaRelativeDirectoryPath)
        {
            string dirPath = HttpContext.Current.Server.MapPath("~/media/" + mediaRelativeDirectoryPath);
            if (Directory.Exists(dirPath))
            {
                List<string> fileList = new List<string>(Directory.GetFiles(dirPath));
                foreach (string file in fileList)
                {
                    File.Delete(file);
                }
            }
        }

        public static void DeleteFilesInDirectoryExceptSpecifiedFileNames(string mediaRelativeDirectoryPath, List<string> fileNameExclusionList)
        {
            string dirPath = HttpContext.Current.Server.MapPath("~/media/" + mediaRelativeDirectoryPath);
            if (Directory.Exists(dirPath))
            {
                List<string> fileList = new List<string>(Directory.GetFiles(dirPath));

                foreach (string s in fileNameExclusionList)
                {
                    fileList.Remove(dirPath + s);
                }
                foreach (string file in fileList)
                {
                    File.Delete(file);
                }
            }
            DeleteFilesInDirectoryExceptSpecifiedFileNamesUNC(mediaRelativeDirectoryPath, fileNameExclusionList);
        }

        public static void DeleteFilesInDirectoryExceptSpecifiedFileNamesUNC(string mediaRelativeDirectoryPath, List<string> fileNameExclusionList)
        {
            if (_conf.IsImageLoadBalance)
            {
                ImpersonateAccount account;
                if (PponFacade.ImpersonateAccountGet(Dns.GetHostName(), out account))
                {
                    try
                    {
                        using (new ImpersonateUtility(account.UserName, string.Empty, account.PassWord))
                        {
                            mediaRelativeDirectoryPath = mediaRelativeDirectoryPath.TrimEnd('/').Replace("/", "\\");
                            string basePath = Path.Combine(account.UNCPath, ImageLoadBalanceFolder.Media.ToString(), mediaRelativeDirectoryPath);
                            if (Directory.Exists(basePath))
                            {
                                List<string> fileList = new List<string>(Directory.GetFiles(basePath));

                                foreach (string s in fileNameExclusionList)
                                {
                                    fileList.Remove(Path.Combine(basePath, s));
                                }
                                foreach (string file in fileList)
                                {
                                    FileInfo info = new FileInfo(file);
                                    if (info.Exists)
                                    {
                                        File.Delete(file);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        Logger.ErrorFormat("圖片同步出錯，DeleteFilesInDirectoryExceptSpecifiedFileNamesUNC，目前Server:{0}，{1}", Dns.GetHostName(), error.ToString());
                    }
                }
            }

        }

        public static void DeleteFilesAndChangeFileInDiectory(string destPath, string srcFileName, string destFileName)
        {
            string filePath = HttpContext.Current.Server.MapPath("~/media/" + destPath);
            string srcFileNamePath = filePath + srcFileName;
            string destFileNamePath = filePath + destFileName;
            string hidealBigPicPath = filePath + "HidealBigPic" + Path.GetExtension(destFileName);
            if (File.Exists(srcFileNamePath))
            {
                //delete BigPictureUrl and BigCandidatePictureUrl change to BigPictureUrl then show it!
                File.Delete(srcFileNamePath);
                File.Move(destFileNamePath, srcFileNamePath);

                if (!string.Equals(Path.GetExtension(destFileNamePath).ToLower(), Path.GetExtension(srcFileNamePath).ToLower()))
                {
                    File.Copy(srcFileNamePath, hidealBigPicPath);
                }
            }
            else
            {
                File.Move(destFileNamePath, hidealBigPicPath);
            }
            DeleteFilesAndChangeFileInDiectoryUNC(destPath, srcFileName, destFileName);
        }

        public static void DeleteFilesAndChangeFileInDiectoryUNC(string destPath, string srcFileName, string destFileName)
        {
            if (_conf.IsImageLoadBalance)
            {
                ImpersonateAccount account;
                if (PponFacade.ImpersonateAccountGet(Dns.GetHostName(), out account))
                {
                    try
                    {
                        using (new ImpersonateUtility(account.UserName, string.Empty, account.PassWord))
                        {
                            destPath = destPath.Replace("/", "\\");
                            srcFileName = string.IsNullOrEmpty(srcFileName) ? string.Empty : srcFileName.Replace("/", "\\");
                            string basePath = Path.Combine(account.UNCPath, ImageLoadBalanceFolder.Media.ToString(), destPath);

                            string srcFileNamePath = Path.Combine(basePath, srcFileName);
                            string destFileNamePath = Path.Combine(basePath, destFileName);
                            string hidealBigPicPath = Path.Combine(basePath, "HidealBigPic" + Path.GetExtension(destFileName));
                            if (File.Exists(srcFileNamePath))
                            {
                                //delete BigPictureUrl and BigCandidatePictureUrl change to BigPictureUrl then show it!
                                File.Delete(srcFileNamePath);
                                File.Move(destFileNamePath, srcFileNamePath);

                                if (!string.Equals(Path.GetExtension(destFileNamePath).ToLower(), Path.GetExtension(srcFileNamePath).ToLower()))
                                {
                                    File.Copy(srcFileNamePath, hidealBigPicPath);
                                }
                            }
                            else
                            {
                                File.Move(destFileNamePath, hidealBigPicPath);
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        Logger.ErrorFormat("圖片同步出錯，DeleteFilesAndChangeFileInDiectoryUNC，目前Server:{0}，{1}", Dns.GetHostName(), error.Message);
                    }
                }
            }
        }

        public static void RenameFile(string original_filePath, string new_filePath)
        {
            if (File.Exists(original_filePath))
            {
                File.Move(original_filePath, new_filePath);
            }
        }

        public static bool PostProcessHtmlMenu(string path)
        {
            if (File.Exists(path))
            {
                string content = File.ReadAllText(path, Encoding.GetEncoding(950));
                int insertPoint = content.IndexOf("<body");
                insertPoint = content.IndexOf(">", insertPoint) + 1;
                string output = content.Substring(0, insertPoint) + "\nProvided by LunchKing.com<br/>\n" + content.Substring(insertPoint);
                File.WriteAllText(path, output, Encoding.GetEncoding(950));
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool isFileExit(string fileName)
        {
            if (File.Exists(fileName))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 合併兩檔案夾圖片
        /// </summary>
        /// <param name="username">網芳登入帳號</param>
        /// <param name="password">網芳登入密碼</param>
        /// <param name="unc_path">網芳路徑</param>
        /// <param name="folder">主要的檔案夾</param>
        /// <param name="subfolders">次要檔案夾</param>
        public static void SinkImages(bool enable_delete, Guid bid)
        {
            if (_conf.IsImageLoadBalance)
            {
                ImpersonateAccount account;
                if (PponFacade.ImpersonateAccountGet(Dns.GetHostName(), out account))
                {
                    try
                    {
                        using (new ImpersonateUtility(account.UserName, string.Empty, account.PassWord))
                        {
                            string filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/" + ImageLoadBalanceFolder.Images.ToString() + "/"), ImageLoadBalanceFolder.ImagesU.ToString(), bid.ToString());
                            if (Directory.Exists(filePath))
                            {
                                string unc_filePath = Path.Combine(account.UNCPath, ImageLoadBalanceFolder.Images.ToString(), ImageLoadBalanceFolder.ImagesU.ToString(), bid.ToString());
                                MergeFiles(filePath, unc_filePath, enable_delete);
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        Logger.ErrorFormat("圖片同步出錯，SinkImages，目前Server:{0}，{1}", Dns.GetHostName(), error.Message);
                    }
                }
            }
        }

        private static void MergeFiles(string path_A, string path_B, bool enable_delete)
        {
            DirectoryInfo dir_A = new DirectoryInfo(path_A);
            IEnumerable<FileInfo> file_info_list_A = dir_A.GetFiles("*.*", SearchOption.AllDirectories);
            if (!Directory.Exists(path_B))
            {
                Directory.CreateDirectory(path_B);
            }

            DirectoryInfo dir_B = new DirectoryInfo(path_B);
            IEnumerable<FileInfo> file_info_list_B = dir_B.GetFiles("*.*", SearchOption.AllDirectories);
            FileCompare filecomparer = new FileCompare();
            bool is_identical = file_info_list_A.SequenceEqual(file_info_list_B, filecomparer);
            if (is_identical)
            {
                return;
            }
            else
            {
                foreach (var item in file_info_list_A.Except(file_info_list_B, filecomparer).AsParallel())
                {
                    if (!dir_B.EnumerateDirectories().Any(x => x == item.Directory))
                    {
                        Directory.CreateDirectory(item.Directory.FullName.Replace(path_A, path_B));
                    }
                    File.Copy(item.FullName, item.FullName.Replace(path_A, path_B), true);
                }

                if (enable_delete)
                {
                    foreach (var item in file_info_list_B.Except(file_info_list_A, filecomparer).ToList().AsParallel())
                    {
                        File.Delete(item.FullName);
                    }
                }
                else
                {
                    foreach (var item in file_info_list_B.Except(file_info_list_A, filecomparer).AsParallel())
                    {
                        if (!dir_A.EnumerateDirectories().Any(x => x == item.Directory))
                        {
                            Directory.CreateDirectory(item.Directory.FullName.Replace(path_B, path_A));
                        }
                        File.Copy(item.FullName, item.FullName.Replace(path_B, path_A), true);

                    }
                }
            }
        }

        public static string GetImageMimeType(byte[] data)
        {
            var ms = new MemoryStream(data);
            var img = Image.FromStream(ms);
            var codec = GetImageMimeType(img);
            ms.Dispose();
            img.Dispose();
            return codec;
        }

        public static string GetImageMimeType(Image imgData)
        {
            Guid rowFormatGuid = imgData.RawFormat.Guid;
            List<Guid> a = ImageCodecInfo.GetImageDecoders().Select(x => x.FormatID).ToList();
            return ImageCodecInfo.GetImageDecoders().First(c => c.FormatID == imgData.RawFormat.Guid).MimeType;
        }
    }

    /// <summary>
    /// 比較檔案是否相同
    /// </summary>
    public class FileCompare : IEqualityComparer<FileInfo>
    {
        public bool Equals(FileInfo x, FileInfo y)
        {
            return (x.FullName.Replace(x.Directory.Parent.FullName, string.Empty).TrimStart(new char[] { '\\' })
                == y.FullName.Replace(y.Directory.Parent.FullName, string.Empty).TrimStart(new char[] { '\\' }) && x.Length == y.Length);
        }

        public int GetHashCode(FileInfo obj)
        {
            string s = string.Format("{0}{1}", obj.Name, obj.Length);
            return s.GetHashCode();
        }
    }

    //將處理圖片功能包含UNC      尚未完成 
    public class ImageFileProcess
    {
        private static readonly ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static readonly ILog log = LogManager.GetLogger(typeof(ImageFileProcess));

        public static void Copy(string sourceFileName, string destFileName, ImageLoadBalanceFolder folder_type)
        {
            sourceFileName = sourceFileName.Replace("/", "\\");
            destFileName = destFileName.Replace("/", "\\");
            File.Copy(sourceFileName, destFileName, true);
            if (config.IsImageLoadBalance)
            {
                ImpersonateAccount account;
                if (PponFacade.ImpersonateAccountGet(Dns.GetHostName(), out account))
                {
                    try
                    {
                        using (new ImpersonateUtility(account.UserName, string.Empty, account.PassWord))
                        {
                            string basePath = Path.Combine(account.UNCPath, folder_type.ToString());
                            File.Copy(Path.Combine(basePath, GetSubPath(sourceFileName, folder_type)), Path.Combine(basePath, GetSubPath(destFileName, folder_type)), true);
                        }
                    }
                    catch (Exception error)
                    {
                        log.ErrorFormat("圖片同步出錯，目前Server:{0}，{1}", Dns.GetHostName(), error.Message);
                    }
                }
            }
        }

        public static void Delete(string path, ImageLoadBalanceFolder folder_type)
        {
            path = path.Replace("/", "\\");
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            if (config.IsImageLoadBalance)
            {
                ImpersonateAccount account;
                if (PponFacade.ImpersonateAccountGet(Dns.GetHostName(), out account))
                {
                    try
                    {
                        using (new ImpersonateUtility(account.UserName, string.Empty, account.PassWord))
                        {
                            string basePath = Path.Combine(account.UNCPath, folder_type.ToString());
                            string unc_path = Path.Combine(basePath, GetSubPath(path, folder_type));
                            if (File.Exists(unc_path))
                            {
                                File.Delete(unc_path);
                            }
                        }
                    }
                    catch (Exception error)
                    {
                        log.ErrorFormat("圖片同步出錯，Delete，目前Server:{0}，{1}", Dns.GetHostName(), error.Message);
                    }
                }
            }
        }
        public static string GetSubPath(string file_name, ImageLoadBalanceFolder folder_type)
        {
            int index = -1;
            string folder_type_index = string.Format("\\{0}\\", folder_type.ToString());
            if (!string.IsNullOrEmpty(file_name) && (index = file_name.IndexOf(folder_type_index)) != -1)
            {
                return file_name.Substring((index + folder_type_index.Length), file_name.Length - (index + folder_type_index.Length));
            }
            else
            {
                return string.Empty;
            }
        }
    }


}
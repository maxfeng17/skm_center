﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Models.VendorBillingSystem;
using LunchKingSite.DataOrm;
using System.Web;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Interface;
using System.Data;
using LunchKingSite.Core.Enumeration;
using System.Transactions;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace LunchKingSite.WebLib.Component
{
    public class FileZipUtility
    {
        static FileZipUtility()
        {

        }

        /// <summary>
        /// Dictionary<key:file_name,value:file>
        /// </summary>
        /// <param name="SourceFiles"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static byte[] ZipFile(Dictionary<string, string> SourceFiles, ZipFileType type = ZipFileType.Default)
        {
            try
            {
                using (Stream memOutput = new MemoryStream())
                using (ZipOutputStream zipOutput = new ZipOutputStream(memOutput))
                {
                    zipOutput.SetLevel(3); //zip level
                    byte[] buffer = null;
                    foreach (KeyValuePair<string, string> source in SourceFiles)
                    {
                        if (type == ZipFileType.Xml)
                        {
                            buffer = TransferXmlFileByte(source.Value);
                        }
                        else if (type == ZipFileType.Byte)
                        {
                            buffer = Convert.FromBase64String(source.Value);
                        }
                        else
                        {
                            buffer = Encoding.UTF8.GetBytes(source.Value);
                        }

                        ZipEntry entry = new ZipEntry(source.Key); //set file name
                        entry.DateTime = DateTime.Now;
                        zipOutput.PutNextEntry(entry);

                        zipOutput.Write(buffer, 0, buffer.Length);
                    }
                    zipOutput.Finish();
                    byte[] newBytes = new byte[memOutput.Length];
                    memOutput.Seek(0, SeekOrigin.Begin);
                    memOutput.Read(newBytes, 0, newBytes.Length);
                    zipOutput.Close();
                    return newBytes;
                }
            }
            catch
            {
                return null;
            }
        }

        private static byte[] TransferXmlFileByte(string xml)
        {
            XDocument xdoc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"));
            xdoc = XDocument.Parse(xml);
            byte[] bytes;
            using (Utf8StringWriter writer = new Utf8StringWriter())
            {
                xdoc.Save(writer);
                //System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                //normalBytes = encoding.GetBytes(writer.ToString());
                bytes = Encoding.UTF8.GetBytes(writer.ToString());
            }
            return bytes;
        }

        public sealed class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding { get { return Encoding.UTF8; } }
        }
    }
}

using System;
using System.Linq;
using System.Web.Security;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using log4net;

namespace LunchKingSite.WebLib.Component
{
    public class LkRoleProvider : RoleProvider
    {
        private static ILog logger = LogManager.GetLogger(typeof(LkRoleProvider));

        public LkRoleProvider()
        {
            this.ApplicationName = "LunchKingSite";
        }

        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            for (int i = 0; i < usernames.Length; i++)
            {
                Member m = mp.MemberGet(usernames[i]);
                for (int j = 0; j < roleNames.Length; j++)
                {
                    Role role = mp.RoleGet(roleNames[j]);
                    RoleMember rm = new RoleMember
                    {
                        UserId = m.UniqueId,
                        RoleGuid = role.Guid
                    };
                    mp.RoleMemberSet(rm);
                } 
            }
        }

        public override string ApplicationName { get; set; }

        public override void CreateRole(string roleName)
        {
            Role role = new Role
            {
                RoleName = roleName
            };
            mp.RoleSet(role);
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            try
            {
                mp.RoleDeleteByName(roleName);
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return false;
            }
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new System.NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            string[] roleNames = mp.RoleGetList().Select(t => t.RoleName).ToArray();
            return roleNames;
        }

        public override string[] GetRolesForUser(string username)
        {
            string[] roleNames = mp.RoleGetList(username).Select(t => t.RoleName).ToArray();
            return roleNames;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            MemberCollection memCol = mp.MemberGetListByRole(roleName);
            return memCol.Select(t => t.UserName).ToArray();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            return GetRolesForUser(username).Contains(roleName);
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            for (int i = 0; i < usernames.Length; i++)
            {
                mp.RoleMemberDelete(usernames[i], roleNames);
            }
        }

        public override bool RoleExists(string roleName)
        {
            return GetAllRoles().Contains(roleName);
        }
    }
}

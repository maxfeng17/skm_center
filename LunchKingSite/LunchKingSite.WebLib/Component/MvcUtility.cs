﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Newtonsoft.Json;

namespace LunchKingSite.WebLib.Component
{
    public static class MvcUtility
    {
        public static RouteValueDictionary GetRouteValues(ViewContext context)
        {
            RouteValueDictionary result = new RouteValueDictionary(context.RouteData.Values);
            foreach (string key in context.RequestContext.HttpContext.Request.QueryString.Keys)
            {
                result[key] = context.RequestContext.HttpContext.Request.QueryString[key];
            }
            return result;
        }
        public static RouteValueDictionary GetRouteValues(ViewContext context, string paramKey, object paramValue)
        {
            RouteValueDictionary result = new RouteValueDictionary(context.RouteData.Values);
            foreach (string key in context.RequestContext.HttpContext.Request.QueryString.Keys)
            {
                result[key] = context.RequestContext.HttpContext.Request.QueryString[key];
            }
            result[paramKey] = paramValue;
            return result;
        }

        /// <summary>
        /// 產生頁面用的 sort header columns's icon class and url
        /// </summary>
        /// <param name="columnNames"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDescending"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static Dictionary<string, SortColumnInfo> GenerateSortColumns(
            List<string> columnNames, string sortColumn, bool sortDescending, ViewContext context)
        {
            Dictionary<string, SortColumnInfo> result = new Dictionary<string, SortColumnInfo>();
            Dictionary<bool, string> sortUpDown = new Dictionary<bool, string>(); //rewriter later 這邊有點太過了
            sortUpDown.Add(false, "sort-up");
            sortUpDown.Add(true, "sort-down");
            foreach (string columnName in columnNames)
            {
                result.Add(columnName, new SortColumnInfo { UpDown = "" });
                RouteValueDictionary dict = new RouteValueDictionary(context.RouteData.Values);
                dict = GetRouteValues(context);
                if (sortColumn == columnName)
                {
                    dict["sortCol"] = columnName;
                    dict["sortDesc"] = !sortDescending;
                }
                else
                {
                    dict["sortCol"] = columnName;
                    dict["sortDesc"] = false;
                }
                result[columnName].Url = GetWebRoot() +
                    context.RouteData.Route.GetVirtualPath(context.RequestContext, dict).VirtualPath;
            }
            if (sortColumn != null)
            {
                result[sortColumn].UpDown = sortUpDown[sortDescending];
            }
            return result;
        }

        private static string GetWebRoot()
        {
            return VirtualPathUtility.AppendTrailingSlash(HttpContext.Current.Request.ApplicationPath);
        }

        /// <summary>
        /// Is Android Mobile or Pad
        /// </summary>
        /// <returns></returns>
        public static bool IsAndroid()
        {
            return HttpContext.Current.Request.UserAgent.ToLower().Contains("android");
        }
    }

    public class SortColumnInfo
    {
        public string UpDown { get; set; }
        public string Url { get; set; }
    }

    public class JsonNetResult : JsonResult
    {
        public JsonSerializerSettings SerializerSettings { get; set; }
        public Formatting Formatting { get; set; }

        public JsonNetResult()
        {
            SerializerSettings = new JsonSerializerSettings();
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            HttpResponseBase response = context.HttpContext.Response;

            response.ContentType = !string.IsNullOrEmpty(ContentType)
              ? ContentType
              : "application/json";

            if (ContentEncoding != null)
            {
                response.ContentEncoding = ContentEncoding;
            }

            JsonTextWriter writer = new JsonTextWriter(response.Output) { Formatting = Formatting };

            JsonSerializer serializer = JsonSerializer.Create(SerializerSettings);
            serializer.Serialize(writer, Data);

            writer.Flush();
        }
    }
}

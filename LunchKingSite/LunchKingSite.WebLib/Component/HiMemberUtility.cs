﻿using System;
using System.Globalization;
using System.Web;
using log4net;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Component
{
    public class HiMemberUtility
    {
        static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected static ILog log = LogManager.GetLogger(typeof(HiMemberUtility));

        public static string SiteUrl
        {
            get
            {
                string site = config.SiteUrl;
                return site.EndsWith("/") ? site : site + "/";
            }
        }

        public static Guid DefaultBuildingGuid
        {
            get { return MemberUtilityCore.DefaultBuildingGuid; }
        }

        public static bool BypassSsoVerify
        {
            get { return config.BypassSsoVerify; }
        }

        public static int checkForgetTime(string mail)
        {
            if (!string.IsNullOrWhiteSpace(mail))
            {
                Member mem = mp.MemberGet(mail);
                if (mem != null && mem.IsLoaded)
                {
                    MemberAuthInfo mai = mp.MemberAuthInfoGet(mem.UniqueId);
                    if (mai != null && mai.IsLoaded)
                    {
                        if (mai.ForgetPasswordDate.HasValue)
                        {
                            if (int.Equals(0, DateTime.Compare(DateTime.Now.Date, mai.ForgetPasswordDate.Value.Date)))
                            {
                                // 同一帳號，同一天內只能索取3次
                                if (mai.PasswordQueryTime.HasValue && mai.PasswordQueryTime.Value > 2)
                                {
                                    return 2;
                                }
                            }
                        }
                    }
                    else
                    {
                        return 3;
                    }

                    return 1;
                }
            }

            return 0;
        }

        /// <summary>
        /// 產生認證信的驗證碼
        /// </summary>
        /// <param name="codeLength"></param>
        /// <param name="keyLength"></param>
        /// <returns></returns>
        public static string[] GenEmailAuthCode(int codeLength = 50, int keyLength = 50)
        {
            int times = (int)Math.Ceiling((double)(codeLength + keyLength) / 32);
            string tempBase = string.Empty;

            for (int x = 0; x < times; x++)
            {
                tempBase += Guid.NewGuid().ToString("N");
            }

            return new string[] { tempBase.Substring(0, codeLength), tempBase.Substring(codeLength, keyLength) };
        }

    }
}
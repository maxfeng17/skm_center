﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Component
{
    /// <summary>
    /// GA埋碼相關 
    /// TagManager
    /// </summary>
    public class RetargetingUtility
    { 
        public static string GetVizuryCodeArgs(ViewPponDeal deal, Order order)
        {
            var pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            var op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

            var vizuryPara = new StringBuilder();
            var imgPaths = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).Where((x, i) => i == 0).ToList();

            vizuryPara.AppendFormat("&currency={0}&pid={1}&pname={2}&image={3}&lp={4}&old={5}&new={6}&misc={7}&orderid={8}&orderprice={9}"
                , "NT", deal.BusinessHourGuid, deal.ItemName,
                imgPaths.Any() ? imgPaths[0] : string.Empty,
                string.Empty, order.Total, order.Total, string.Empty, order.Guid, order.Total);

            //類別
            var vadcCol = pp.ViewAllDealCategoryGetListByBid(deal.BusinessHourGuid);
            for (var i = 0; i < vadcCol.Count; i++)
            {
                if (i == 0)
                {
                    vizuryPara.AppendFormat("&catid={0}", vadcCol[i].Cid);
                }
                else
                {
                    vizuryPara.AppendFormat("&subcat{0}id={1}", i, vadcCol[i].Cid);
                }
            }

            //訂單明細
            var odc = op.OrderDetailGetList(order.Guid);
            foreach (var od in odc)
            {
                vizuryPara.AppendFormat("&pid1={0}&catid1={1}&quantity1={2}&price1={3}", od.ItemGuid, od.ItemName, od.ItemQuantity, od.Total);
            }

            return vizuryPara.ToString();
        }

        public static string GetOrderArgs(IViewPponDeal deal, Order order, string cpaKey = "", string cpaStr = "")
        {
            return GetOrderArgs(null, deal, order, cpaKey, cpaStr);
        }

        public static string GetOrderArgs(TrackTagInputModel trackObj, IViewPponDeal deal, Order order , string cpaKey = "" , string cpaStr ="" )
        {        
            var OrderPara = new StringBuilder();

            var pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            var mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            var sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            var hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            var config = ProviderFactory.Instance().GetConfig();

            CashTrustLogCollection ctCol = mp.CashTrustLogGetListByOrderGuid(order.Guid, OrderClassification.LkSite);
            if (ctCol.Count() > 0)
            {
                string deptName = string.Empty;
                if (deal.DealEmpName != null)
                {
                    ViewEmployee employee = hp.ViewEmployeeGet(ViewEmployee.Columns.EmpName, deal.DealEmpName);
                    deptName = (employee != null) ? employee.DeptName : string.Empty;
                }

                string typeName = (deal.DealType != null) ? sp.SystemCodeGetName("DealType", deal.DealType.Value) : string.Empty;
                var imgPaths = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).Where((x, i) => i == 0).ToList();
                string DealUrl = string.Format("{0}/{1}", config.SiteUrl, deal.BusinessHourGuid);
                decimal taxrate = Convert.ToDecimal(0.05);
                int revenue = ctCol.Sum(x => x.Amount);
                int bcash = ctCol.Sum(x => x.Bcash);
                int discount = ctCol.Sum(x => x.DiscountAmount);
                int shippingFee = ctCol.Where(x => Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight)).Sum(x => x.Amount);
                int quantity = Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) ? 1 : ((shippingFee > 0) ? ctCol.Count() - 1 : ctCol.Count());
                int realRevenue = revenue - bcash - discount;

                ComboDeal comboDeal = pp.GetComboDeal(deal.BusinessHourGuid);
                Guid mainDealBid = (comboDeal.IsLoaded && comboDeal.MainBusinessHourGuid != null) ? comboDeal.MainBusinessHourGuid.Value : deal.BusinessHourGuid;

                #region GA

                OrderPara.Append("<script type=\"text/javascript\"> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),");
                OrderPara.Append("m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');");
                OrderPara.Append("ga('create', 'UA-3157038-1', 'auto'); ga('require', 'ecommerce'); ga('ecommerce:addTransaction', {");
                OrderPara.AppendFormat("'id': '{0}', 'affiliation': '{1}', 'revenue': '{2}', 'shipping': '{3}', 'tax': '{4}', 'currency': 'TWD','metric1':{5},'metric2':{6},'metric3':{7}"
                    , order.Guid, order.SellerName, realRevenue, shippingFee, Math.Round(revenue * taxrate, 0), revenue, Convert.ToInt32(deal.ItemPrice * quantity), discount);
                OrderPara.Append("});");

                OrderPara.Append("ga('ecommerce:addItem', {");
                OrderPara.AppendFormat("'id': '{0}', 'name': '{1}', 'sku': '{2}', 'category': '{3}', 'price': '{4}', 'quantity': '{5}'"
                    , order.Guid, deal.ItemName, deal.BusinessHourGuid, deptName + " - " + typeName, Convert.ToInt32(deal.ItemPrice), quantity);
                OrderPara.Append("});");
                OrderPara.Append("ga('ecommerce:send'); ga('send', 'pageview');");

                OrderPara.Append("</script>");

                #endregion GA

                #region Facebook Pixel Code
                OrderPara.Append("<script type=\"text/javascript\">fbq('track', 'Purchase',{");
                OrderPara.AppendFormat("content_ids:['{0}'],", mainDealBid);
                OrderPara.Append("content_type:'product',");
                OrderPara.AppendFormat("value:{0},", Convert.ToInt32(deal.ItemPrice) * quantity);
                OrderPara.Append("currency:'TWD'");
                OrderPara.Append("});</script>");
                OrderPara.AppendFormat("<noscript><img height='0' width='0' style='display:none' src='https://www.facebook.com/tr?id=274288942924758&ev=Purchase&cd[content_ids]={0}&cd[content_type]=product&cd[value]={1}&cd[currency]=TWD'/></noscript>", mainDealBid, Convert.ToInt32(deal.ItemPrice) * quantity);
                #endregion

                #region GaTag
                OrderPara.AppendFormat("<input id='hidTagprice' type='hidden' value='{0}' />", revenue);
                OrderPara.AppendFormat("<input id='hidTagQty' type='hidden' value='{0}' />", quantity);
                #endregion

                #region 宇匯

                if (trackObj != null)
                {
                    trackObj.OrderTotalAmountWithoutDiscount = realRevenue;
                    OrderPara.AppendLine(string.Empty);
                    OrderPara.AppendLine("<script type=\"text/javascript\">");
                    OrderPara.AppendLine(TagManager.CreateFatory(trackObj, TagProvider.Scupio).GetJson());
                    OrderPara.AppendLine("</script>");
                }

                #endregion
            }

            return OrderPara.ToString();
        }

        public static string GetDealArgs(IViewPponDeal deal, int UserId)
        {
            var pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            var sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            var config = ProviderFactory.Instance().GetConfig();
            var DealInfo = new StringBuilder();
            var imgPaths = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).Where((x, i) => i == 0).ToList();
            string DealUrl = string.Format("{0}/{1}", config.SiteUrl, deal.BusinessHourGuid);
            double timestamp = (deal.BusinessHourOrderTimeE.AddHours(-8) - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;

            //string dealType = string.Empty;
            string typeName = string.Empty;
            //string groupId = string.Empty;
            //string groupName = string.Empty;
            bool isSoldout = false;
            if (deal.DealType != null && deal.DealAccBusinessGroupId != null)
            {
                //dealType = deal.DealType.ToString();
                typeName = SystemCodeManager.GetSystemCodeNameByGroupAndId("DealType", deal.DealType.Value); //改讀靜態物件
                //groupId = deal.DealAccBusinessGroupId.ToString();
                //groupName = AccBusinessGroupManager.GetAccBusinessGroupName(deal.DealAccBusinessGroupId.Value); //改讀靜態物件
                isSoldout = !((deal.OrderTotalLimit - deal.OrderedQuantity > 0) && (deal.BusinessHourOrderTimeS <= DateTime.Now && DateTime.Now < deal.BusinessHourOrderTimeE));
            }
            //類別
            List<Category> categoryCol = ViewPponDealManager.DefaultManager.GetCategories(deal.BusinessHourGuid);

            var categoryChannelId = new StringBuilder();
            var categoryChannelName = new StringBuilder();
            var categoryCityId = new StringBuilder();
            var categoryCityName = new StringBuilder();
            var categoryClassificationId = new StringBuilder();
            var categoryClassificationName = new StringBuilder();
            
            foreach (var category in categoryCol)
            {
                switch ((CategoryType)category.Type)
                {
                    case CategoryType.PponChannel:
                        categoryChannelId.AppendFormat("{0}|", category.Id);
                        categoryChannelName.AppendFormat("{0}|", category.Name);
                        break;

                    case CategoryType.PponChannelArea:
                        if (category.Id < 200) //過濾行政分區
                        {
                            categoryCityId.AppendFormat("{0}|", category.Id);
                            categoryCityName.AppendFormat("{0}|", category.Name);
                        }
                        break;

                    case CategoryType.DealCategory:
                        categoryClassificationId.AppendFormat("{0}|", category.Id);
                        categoryClassificationName.AppendFormat("{0}|", category.Name);
                        break;
                    default:
                        break;
                }
            }

            #region Scupio
            DealInfo.AppendFormat("<input type='hidden' name='Channels' value='{0}'/> <input type='hidden' name='Cities' value='{1}'/> <input type='hidden' name='Classifications' value='{2}'/> <input type='hidden' name='DealType' value='{3}'/>"
                , categoryChannelId, categoryCityId, categoryClassificationId, typeName);
            #endregion Scupio

            

            return DealInfo.ToString();
        }
    }
}

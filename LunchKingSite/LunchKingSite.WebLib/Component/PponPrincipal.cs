﻿using System.Security.Principal;
using System.Web.Security;

namespace LunchKingSite.WebLib.Component
{
    public class PponPrincipal : GenericPrincipal
    {
        public PponPrincipal(IIdentity identity)
            : base(identity, null)
        {
        }

        public PponPrincipal(IIdentity identity, string[] roles)
            : base(identity, roles)
        {
        }

        public static PponPrincipal Guest
        {
            get
            {
                return new PponPrincipal(PponIdentity.Guest);
            }
        }

        public new PponIdentity Identity
        {
            get
            {
                return (PponIdentity)base.Identity;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.WebLib.Models.ControlRoom.Finance;
using LunchKingSite.WebLib.Models.VendorBillingSystem;
using LunchKingSite.DataOrm;
using System.Web;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Interface;
using System.Data;
using LunchKingSite.Core.Enumeration;
using System.Transactions;

namespace LunchKingSite.WebLib.Component
{
    public class PcpTransactionUtility
    {
        protected static IPponProvider pp;
        protected static IOrderProvider op;
        protected static IMemberProvider mp;
        protected static IPCPProvider pcp;
        protected static ISellerProvider sp;
        protected static ISysConfProvider conf;

        static PcpTransactionUtility()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            conf = ProviderFactory.Instance().GetConfig();
        }

        public static PcpSellerTriplicateInvoiceData GetSellerTriplicateInvoiceInfo(string accoundId, Guid sellerGuid)
        {
            var vbsmembership = mp.VbsMembershipGetByAccountId(accoundId);
            if (vbsmembership.IsLoaded && vbsmembership.UserId.HasValue)
            {
               VbsCompanyDetail vbsCompanyDetail = sp.VbsCompanyDetailGet(sellerGuid);
                return new PcpSellerTriplicateInvoiceData()
                       {
                           invoiceTitle = vbsCompanyDetail.InvoiceTitle,
                           ivoiceCompanyId = vbsCompanyDetail.InvoiceComId,
                           invoiceName = vbsCompanyDetail.InvoiceName,
                           invoiceAddress = vbsCompanyDetail.InvoiceAddress
                       };
            }

            return new PcpSellerTriplicateInvoiceData() ;
        }

        public static List<PcpTransactonOrderData> GetPcpTransactionOrderData(string accoundId, PcpPointType type)
        {
            var pcpTransactonOrderDataCols = new List<PcpTransactonOrderData>();

            var vbsmembership = mp.VbsMembershipGetByAccountId(accoundId);
            if (vbsmembership.IsLoaded && vbsmembership.UserId.HasValue)
            {
               var cols = pcp.ViewPcpPointTransactionOrderRecordListGetByUserId(vbsmembership.UserId.Value, type);
            
               pcpTransactonOrderDataCols.AddRange(
                   cols.Select(x=> new PcpTransactonOrderData
                            {
                                OrderId = (x.Id.HasValue)?x.Id.Value.ToString():"--",
                                orderDate = x.CreateTime.ToString("yyyy/MM/dd HH:mm"),
                                expireDate = (x.ExpireTime.HasValue)?x.ExpireTime.Value.ToString("yyyy/MM/dd HH:mm"):"--",
                                memo = x.Message,
                                pointIn = string.Format("{0:N0}", x.Pointin),
                                pointOut = string.Format("{0:N0}", x.Pointout),
                                total = string.Format("{0:N0}", x.Total),
                                creator = accoundId,
                                description = (string.IsNullOrEmpty(x.InvoiceNumber)) ? "" : x.InvoiceNumber.ToString()
                             } )
                   );
            }
            return pcpTransactonOrderDataCols;
        }


        public static List<PcpSuperBonusData> GetSuperBonusData(string accoundId)
        {
            var pcpSuperBonusDataCols = new List<PcpSuperBonusData>();

            var vbsmembership = mp.VbsMembershipGetByAccountId(accoundId);
            if (vbsmembership.IsLoaded && vbsmembership.UserId.HasValue)
            {
                var superBounsCols = pcp.ViewPcpMemberPromotionSuperBonusGetList(vbsmembership.UserId.Value);
                var exchangeOrders = pcp.PcpPointTransactionExchangeOrderListGetBySellerUserId(vbsmembership.UserId.Value);

                pcpSuperBonusDataCols = (from bonusData in superBounsCols
                    join exOrder in exchangeOrders on bonusData.OrderGuid equals exOrder.Guid
                        into result
                    from exOrder in result.DefaultIfEmpty()
                    select new PcpSuperBonusData()
                           {
                               OrderId = (exOrder !=null)? exOrder.Id.ToString():"--",
                              membershipCardId = bonusData.UserMemCardId == 0 ? "--" : bonusData.UserMemCardId.ToString(),
                               transactionDate = bonusData.CreateTime.ToString("yyyy/MM/dd hh:mm"),
                               memo = string.Format("{0}", bonusData.Action),
                               pointIn = string.Format("{0:N0}", bonusData.ValueIn),
                               pointOut = string.Format("{0:N0}", bonusData.ValueOut),
                               total = string.Format("{0:N0}", bonusData.Total),
                               creator = bonusData.AccountId                               
                           }).ToList();
            }
            return pcpSuperBonusDataCols;
        }

        public static string ApplicationForReturnPurchaseFormPrint(int pcptransactionorderid)
        {
            var template = TemplateFactory.Instance().GetTemplate<ApplicationForReturnPurchaseForm>();
            var transactionorder = pcp.PcpPointTransactionOrderGet(pcptransactionorderid);
            if (transactionorder != null && transactionorder.IsLoaded)
            {

                template.ApplyReturnPurchaseDate = DateTime.Now;
                template.ReturnPurchaseItem = transactionorder.Description;
            }

            template.SiteUrl = conf.SiteUrl;

            return template.ToString();
        }


        public static string CertificateOfPurchaseReturnFormPrint(int pcptransactionorderid)
        {
            var template = TemplateFactory.Instance().GetTemplate<CertificateOfPurchaseReturnForm>();
            var transactionorder = pcp.PcpPointTransactionOrderGet(pcptransactionorderid);
            if (transactionorder != null && transactionorder.IsLoaded)
            {
                List<EinvoiceMain> einvoices = op.EinvoiceMainGetListByOrderGuid(transactionorder.Guid, OrderClassification.RegularsOrder).Where(x => x.VerifiedTime != null).ToList();
                if (einvoices.Count > 0)
                {
                    var einvoice = einvoices.First();
                    
                    if(einvoice.OrderTime.IsBetween(conf.ContactToPayeasy,conf.PayeasyToContact.AddDays(-1)))
                    {
                    template.CompanyName = "康迅數位整合股份有限公司";
                    template.CompanyId = conf.PayeasyCompanyId;
                    template.CompanyAddress = "台北市中山區中山北路一段11號13樓";

                    }else
                    {
                    template.CompanyName = "康太數位整合股份有限公司";
                    template.CompanyId = conf.ContactCompanyId;
                    template.CompanyAddress = "台北市中山區中山北路一段11號13樓";

                    }

                    template.InvoiceMode = einvoice.InvoiceMode2 == (int)InvoiceMode2.Triplicate ? 3 : 2;
                    template.EnvoiceDate = einvoice.OrderTime;
                    template.EinvoiceNumber = einvoice.InvoiceNumber;
                    template.EinvoiceItemName = einvoice.OrderItem;
                    template.ItemAmount = Math.Round(einvoice.OrderAmount * (1 - einvoice.InvoiceTax), 0);
                    //template.ItemNoTaxAmount = (Math.Round((einvoice.OrderAmount / (1 + einvoice.InvoiceTax)), 0));
                    template.ItemNoTaxAmount = Math.Round(einvoice.OrderAmount * (1 - einvoice.InvoiceTax), 0);
                    template.TotalAmount = Math.Round(einvoice.OrderAmount, 0);
                    template.ItemTax = Math.Round(einvoice.OrderAmount * einvoice.InvoiceTax, 0);
                    template.IsTax = einvoice.InvoiceTax == 0.05m;
                }
            }

            template.SiteUrl = conf.SiteUrl;

            return template.ToString();
        }


    }
}

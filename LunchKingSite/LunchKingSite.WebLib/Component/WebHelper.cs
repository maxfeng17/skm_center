﻿using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LunchKingSite.WebLib.Component
{
    public class WebHelper
    {
        static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        static ILog logger = LogManager.GetLogger(typeof(WebHelper));
        public static string PageBlock(string name)
        {
            string version = Helper.GetImageVer();
            string cacheKey = string.Format("sideBlock://{0}?ver={1}", name, version);
            string html = MemoryCache.Default.Get(cacheKey) as string;
            if (html == null)
            {
                string filePath = System.IO.Path.Combine(
                    config.WebAppDataPath, "web", "pageBlocks", name + ".html");

                if (System.IO.File.Exists(filePath))
                {
                    html = System.IO.File.ReadAllText(filePath);
                }
                else
                {
                    html = string.Empty;
                }
                html = html.Replace("@Version", version);
                MemoryCache.Default.Set(cacheKey, html, new DateTimeOffset(DateTime.Now.AddMinutes(20)));

                logger.InfoFormat("set cache {0}", cacheKey);
            }
            return html;
        }
    }
}

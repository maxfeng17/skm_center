﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.WebLib.Component
{
    public class RolePageAttribute : AuthorizeAttribute
    {
        private SystemFunctionType _funcType = SystemFunctionType.Read;
        private string _referName = null;

        public RolePageAttribute()
        {
        }
        public RolePageAttribute(string referName, SystemFunctionType funcType)
        {
            this._referName = referName;
            if (funcType == SystemFunctionType.All)
            {
                throw new ArgumentException("funcType can't be all.");
            }
            this._funcType = funcType;
        }
        public RolePageAttribute(SystemFunctionType funcType)
        {
            if (funcType == SystemFunctionType.All)
            {
                throw new ArgumentException("funcType can't be all.");
            }
            this._funcType = funcType;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null || httpContext.User == null || httpContext.User.Identity.IsAuthenticated == false)
            {
                FormsAuthentication.RedirectToLoginPage();
                return false;
            }

            if (System.Web.Security.Roles.IsUserInRole(MemberRoles.Administrator.ToString()) == false)
            {
                string actionName;
                if (string.IsNullOrEmpty(_referName) == false)
                {
                    actionName = _referName;                    
                } else
                {
                    actionName = httpContext.Request.RequestContext.RouteData.Values["action"] as string;
                }
                if (CommonFacade.IsInSystemFunctionPrivilege(httpContext.User.Identity.Name, actionName, _funcType) == false)
                {
                    CommonFacade.AddAudit(Guid.Empty, AuditType.RolePage, HttpContext.Current.Request.Url.LocalPath,
                        httpContext.User.Identity.Name, true);
                    httpContext.Response.Redirect("~/default.aspx", false);
                    return false;
                }
            }
            return true;
        }
    }

}

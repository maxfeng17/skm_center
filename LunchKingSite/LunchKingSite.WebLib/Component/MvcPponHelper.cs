﻿using System;
using System.Web;
using System.Web.Mvc;
using LunchKingSite.Core;
using LunchKingSite.I18N;

namespace LunchKingSite.WebLib.Component
{
    public class MvcPponHelper
    {
        public static IHtmlString SellerDepartment(int department, int? dealSource)
        {
            if (dealSource == null)
            {
                return MvcHtmlString.Empty;
            }
            if ((OrderClassification)dealSource == OrderClassification.HiDeal)
            {
                return MvcHtmlString.Create("品生活");
            }
            DepartmentTypes dep;
            string result;
            if (Enum.TryParse(department.ToString(), out dep))
            {
                result = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, dep);
            }
            else
                result = "unknown";
            return new HtmlString(result);
        }

        public static IHtmlString SellerStatus(int sellerStatus)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 簡易判斷 檔次的活動狀態，要詳細精確的請用 ViewPponDeal 的 GetDealStage()
        /// </summary>
        /// <param name="businessHourOrderTimeS"></param>
        /// <returns></returns>
        public static IHtmlString DealStage(DateTime? businessHourOrderTimeS, DateTime? businessHourOrderTimeE)
        {
            string result;
            if (businessHourOrderTimeS == null || businessHourOrderTimeE == null)
            {
                result = "";
            }
            else if (DateTime.Now < businessHourOrderTimeS)
            {
                result = Phrase.NotToBegin;

            }
            else if (DateTime.Now >= businessHourOrderTimeS && businessHourOrderTimeE > DateTime.Now)
            {
                result = Phrase.Proceeding;
            }
            else if (DateTime.Now >= businessHourOrderTimeE)
            {
                result = Phrase.EventExpired;
            }
            else
            {
                result = "";
            }
            return new HtmlString(result);
        }

        public static IHtmlString BusinessHourOrderTime(DateTime? businessHourOrderTimeS, DateTime? businessHourOrderTimeE)
        {
            if (businessHourOrderTimeS == null || businessHourOrderTimeE == null)
            {
                return new HtmlString("");
            }
            return new HtmlString(string.Format("{0:yyyy/MM/dd HH:mm} ~<br /> {1:yyyy/MM/dd HH:mm}", 
                businessHourOrderTimeS, businessHourOrderTimeE));
        }

        public static IHtmlString BusinessHourDeliveryTime(DateTime? businessHourDeliverTimeS, DateTime? businessHourDeliverTimeE)
        {
            if (businessHourDeliverTimeS == null || businessHourDeliverTimeE == null)
            {
                return new HtmlString("");
            }
            return new HtmlString(string.Format("{0:yyyy/MM/dd HH:mm} ~<br /> {1:yyyy/MM/dd HH:mm}",
                businessHourDeliverTimeS, businessHourDeliverTimeE));
        }
    }
}
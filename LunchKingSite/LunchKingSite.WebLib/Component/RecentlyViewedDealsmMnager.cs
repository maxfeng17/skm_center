﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Models.Mobile;

namespace LunchKingSite.WebLib.Component
{
    /// <summary>
    /// 使用者最近瀏覽的檔次記錄
    /// </summary>
    public class RecentlyViewedDealsmMnager
    {
        public const int _LIMIT = 12;
        public static void Add(IViewPponDeal deal)
        {
            List<MainSidebarDealModel> items = GetList();            
            items.RemoveAll(t => t.DealId == deal.BusinessHourGuid);

            MainSidebarDealModel item = MainSidebarDealModel.Create(deal);
            items.Insert(0, item);
            if (items.Count > _LIMIT)
            {
                items.RemoveAt(_LIMIT);
            }
        }

        public static List<MainSidebarDealModel> GetList(Guid excludeDealId = default(Guid))
        {
            List<MainSidebarDealModel> viewDeals = HttpContext.Current.Session[LkSiteSession.ViewedDeals.ToString()] as List<MainSidebarDealModel>;
            if (viewDeals == null)
            {
                viewDeals = new List<MainSidebarDealModel>();
                HttpContext.Current.Session[LkSiteSession.ViewedDeals.ToString()] = viewDeals;
            }
            else if (excludeDealId != default(Guid))
            {
                return viewDeals.Where(t => t.DealId != excludeDealId).ToList();
            }
            return viewDeals;
        }

        public static MainSidebarDealModel GetLast()
        {
            return GetList().FirstOrDefault();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.SessionState;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Component.MemberActions;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Security.Principal;
using log4net;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Component.MemberActions;
using System.Web.Security;

namespace LunchKingSite.WebLib.Component
{
    public class MemberUtility
    {
        private static IMemberProvider mp;
        private static ISysConfProvider conf;
        private static ILog logger = LogManager.GetLogger(typeof(MemberUtility));
        private static Random random = new Random();

        /// <summary>
        /// 會員預設圖片，顯示在登入名字左邊
        /// </summary>
        public const string DefaultPic = "../Themes/PCweb/images/17p_sn07_12.jpg";
        /// <summary>
        /// 手機認證碼有效時間(分)
        /// </summary>
        public const int _MOBILE_CODE_VALID_MINUTES = 10;
        public const int _MOBILE_SMS_SNED_LIMIT = 3;
        public const int _MOBILE_UNBAN_DAYS = 7;
        static MemberUtility()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            conf = ProviderFactory.Instance().GetConfig();
        }


        public static void UpdateMemberInfo(Member mem)
        {
            UpdateMemberInfo(mem, null);
        }

        /// <summary>
        /// 只有在 mu 有值，且 mu 的 email 跟 mem.UserName 不同時，才會執行更改email的動作
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="newUserName"></param>
        public static void UpdateMemberInfo(Member mem, string newUserName)
        {
            string oldUserName = mem.UserName;
            mp.MemberSet(mem);
            if (newUserName != null && string.Equals(oldUserName, newUserName, StringComparison.OrdinalIgnoreCase) == false)
            {
                NewMemberUtility.ChangeEmailSaveChange(mem.UniqueId, newUserName);
            }
        }

        /// <summary>
        /// 標記刪除曾註冊，但未完成啟用的會員資料
        /// </summary>
        /// <param name="mem"></param>
        public static bool DeleteInactivedEmailMember(Member mem)
        {            
            // delete mobile member
            MemberLinkCollection mls = mp.MemberLinkGetList(mem.UniqueId);
            if (mls.Count != 1 || mls[0].ExternalOrg != (int)SingleSignOnSource.ContactDigitalIntegration)
            {
                return false;
            }
            if (Helper.IsFlagSet(mem.Status, MemberStatusFlag.Active17Life))
            {
                //啟用的會員不能刪
                return false;
            }
            try
            {
                using (var tx = TransactionScopeBuilder.CreateReadCommitted())
                {
                    string replaceId = GetInvalidMemberReplacementId();
                    mp.MemberSetUserName(mem.UniqueId, replaceId, false);

                    // 移除串接資料
                    string memberLinkInfo = string.Format("{0}:{1}", mls[0].ExternalOrg, mls[0].ExternalUserId);
                    mp.MemberLinkDelete(mem.UniqueId);

                    mp.MemberAuthInfoDelete(mem.UniqueId);

                    mp.RoleMemberDelete(mem.UniqueId);

                    mp.MemberPropertyDelete(mem.UniqueId);

                    // 註記
                    string msg = string.Format("未啟用會員帳號移除: {0} 原始串接資訊: {1}", mem.UserName, memberLinkInfo);
                    CommonFacade.AddAudit(replaceId, AuditType.InvalidMember, msg, string.Empty, true);
                    if (msg.Length > 100)
                    {
                        logger.Warn("CommonFacade.AddAudit 超過預期最大100個字元,msg=" + msg);
                        msg = msg.Substring(100);
                    }
                    MemberFacade.LogAccountAudit(mem.UniqueId, replaceId, AccountAuditAction.Deleted, true, msg);
                    tx.Complete();
                }
                return true;
            }
            catch ( Exception ex)
            {
                logger.WarnFormat("DeleteEmailInactivedMember fail. userId={0}, ex={1}", mem.UniqueId, ex);
                return false;
            }            
        }


        /// <summary>
        /// 會員資料刪除(此功能目前僅移除串接方式及相關修改，使其會員無法登入查詢)
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="loginUser"></param>
        /// <param name="memo"></param>
        /// <returns>取代帳號</returns>
        public static string DeleteMember(string userName, string loginUser, string memo)
        {
            // 刪除相關個資及更改會員email
            Member m = mp.MemberGet(userName);
            m.LastName = string.Empty;
            m.FirstName = string.Empty;
            m.BuildingGuid = NewMemberUtility.DefaultBuildingGuid;
            m.Birthday = null;
            m.Gender = null;
            m.Mobile = string.Empty;
            m.CompanyName = string.Empty;
            m.CompanyDepartment = string.Empty;
            m.CompanyTel = string.Empty;
            m.CompanyTelExt = string.Empty;
            m.CompanyAddress = string.Empty;
            m.DeliveryMethod = string.Empty;
            m.PrimaryContactMethod = (int)ContactMethod.Mobile;
            m.Pic = null;
            m.IsApproved = false;
            m.CarrierType = (int)CarrierType.Member;
            m.CarrierId = m.UniqueId.ToString();

            string replaceId = GetInvalidMemberReplacementId();
            UpdateMemberInfo(m, replaceId);

            // delete mobile member
            MobileMember mm = mp.MobileMemberGet(m.UniqueId);
            if (mm.IsLoaded)
            {
                MemberFacade.DeleteMobileMember(mm);
                mp.MobileAuthInfoDelete(mm.UserId);
            }

            // 移除串接資料
            string memberLinkInfo = string.Empty;
            MemberLinkCollection mls = mp.MemberLinkGetList(m.UniqueId);
            memberLinkInfo = string.Join(",", mls.Select(x => string.Format("{0}:{1}", (SingleSignOnSource)x.ExternalOrg, x.ExternalUserId)));
            mp.MemberLinkDelete(m.UniqueId);

            mp.MemberAuthInfoDelete(m.UniqueId);

            // 註記
            string msg = string.Format("會員帳號移除: {0} 原始串接資訊: {1} 備註: {2}", userName, memberLinkInfo, memo);
            CommonFacade.AddAudit(replaceId, AuditType.InvalidMember, msg, loginUser, true);
            if (msg.Length > 100)
            {
                logger.Warn("CommonFacade.AddAudit 超過預期最大100個字元,msg=" + msg);
                msg = msg.Substring(100);
            }
            MemberFacade.LogAccountAudit(m.UniqueId, replaceId, AccountAuditAction.Deleted, true, msg);
            return replaceId;
        }

        /// <summary>
        /// 取得刪除會員新編碼
        /// </summary>
        /// <returns></returns>
        private static string GetInvalidMemberReplacementId()
        {
            return Guid.NewGuid() + "@deleted";
        }

        /// <summary>
        /// 後臺管理者，模擬其它使用者登入
        /// </summary>
        /// <param name="userName"></param>
        public static void UserForceSignIn(string userName, AccountAuditAction actionType = AccountAuditAction.LoginByAdministrator)
        {
            HttpContext context = HttpContext.Current;
            var mem = mp.MemberGet(userName);
            if (mem.IsLoaded == false || mem.IsApproved == false || mem.IsLockedOut)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginByAdministrator, false,
                    "login by " + HttpContext.Current.User.Identity.Name, Helper.GetOrderFromType());
                if (mem.IsLoaded == false)
                {
                    throw new Exception(string.Format("帳號 {0} 不存在，請再確認。", userName));
                }
                if (mem.IsApproved == false)
                {
                    throw new Exception(string.Format("帳號 {0} 不被允許，請通知IT處理", userName));
                }
                if (mem.IsLockedOut)
                {
                    throw new Exception(string.Format("帳號 {0} 被鎖定，請檢查鎖定狀況", userName));
                }
            }
            context.Response.Cookies.Add(
                CookieManager.MakeCookie(LkSiteCookie.CookieVersion.ToString(), mem.Version.ToString(CultureInfo.InvariantCulture), false));
            mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginByAdministrator, true,
                "login by " + HttpContext.Current.User.Identity.Name, Helper.GetOrderFromType());
            CookieManager.SetMemberAuth(mem.UserName);
        }
        /// <summary>
        /// 做用Token作登入，因為UserAuthenticationModule己作完Token的有效驗證，
        /// 這邊的code接近強制登入，差別只有 AccoutnAuditAction 不同。
        /// </summary>
        /// <param name="userName"></param>
        public static void UserForceSignInForAuthModule(string userName)
        {
            if (userName.Contains("@skm", StringComparison.OrdinalIgnoreCase))
            {
                return;
            }
            UserForceSignIn(userName, AccountAuditAction.LoginByToken);            
        }

        public static void UpdateMemberDeliveryOnly(MemberDelivery md)
        {
            mp.MemberDeliverySet(md);
        }

        public static void SetUserSsoInfoReady(int userId)
        {
            HttpSessionState session = HttpContext.Current.Session;

            if (session == null)
            {
                return;
            }

            ExternalMemberInfo emi = session[LkSiteSession.ExternalMemberId.ToString()] as ExternalMemberInfo;

            if (emi != null && emi.UserId != null && emi.UserId == userId)
            {
                return;
            }

            session[LkSiteSession.ExternalMemberId.ToString()] = new ExternalMemberInfo(userId);
            MemberLinkCollection cols = mp.MemberLinkGetList(userId);
            foreach (var l in cols)
            {
                ((ExternalMemberInfo)session[LkSiteSession.ExternalMemberId.ToString()])[(SingleSignOnSource)l.ExternalOrg] = l.ExternalUserId;
            }
        }

        public static void SetUserEmptySsoInfo()
        {
            HttpSessionState session = HttpContext.Current.Session;

            if (session == null)
            {
                return;
            }
            session[LkSiteSession.ExternalMemberId.ToString()] = new ExternalMemberInfo(0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="mobile"></param>
        /// <param name="force">當會員已存在是否還要發送</param>
        /// <param name="sendCount"></param>
        /// <param name="nextCanSendSmsTime">下次可發送的時間，正常情況是now, 但如果已超過發送限制次數，則回傳下次可發送的時間</param>
        /// <returns></returns>
        public static SendMobileAuthCodeReply SendMobileAuthCode(int userId, string mobile, bool force, out int sendCount, out DateTime nextCanSendSmsTime)
        {
            sendCount = 0;
            nextCanSendSmsTime = DateTime.MinValue;
            try
            {
                if (RegExRules.CheckMobile(mobile) == false)
                {
                    return SendMobileAuthCodeReply.MobileError;
                }
                if (userId == 0)
                {
                    return SendMobileAuthCodeReply.DataError;
                }
                Member mem = mp.MemberGet(userId);
                if (mem.IsLoaded == false)
                {
                    return SendMobileAuthCodeReply.DataError;
                }
                var mm = MemberFacade.GetMobileMember(mobile);
                if (force == false && mm.IsLoaded && Helper.IsFlagSet(mm.Status, MobileMemberStatusType.Activated))
                {
                    return SendMobileAuthCodeReply.MobileUsedByOtherMember;
                }
                if (MemberFacade.CheckMobileAuthSmsCanSend(
                    mobile, _MOBILE_SMS_SNED_LIMIT, _MOBILE_UNBAN_DAYS, out sendCount, out nextCanSendSmsTime) == false)
                {
                    return SendMobileAuthCodeReply.TooManySmsSent;
                }
                MobileAuthInfo mai = MemberFacade.GetValidOrNewMobileAuthInfo(userId, mobile);
                mai.QueryTimes++;
                sendCount++;
                mai.ValidTime = DateTime.Now.AddMinutes(_MOBILE_CODE_VALID_MINUTES);
                MemberFacade.SetMobileAuthInfo(mai);
                try
                {
                    SMS sms = new SMS();
                    string msg = string.Format("{0}為17Life認證碼，10分鐘後失效。",
                        mai.Code);
                    sms.SendMessage(string.Empty, msg, string.Empty, mai.Mobile,
                        mem.UserName, SmsType.Ppon, Guid.Empty, "0");
                    return SendMobileAuthCodeReply.Success;
                }
                catch
                {
                    return SendMobileAuthCodeReply.SmsError;
                }
            }
            catch (Exception ex)
            {
                string msg = string.Format("手機認證發送簡訊失敗. mobile={0}, user={1}", mobile, userId);
                logger.Warn(msg, ex);
                return SendMobileAuthCodeReply.OtherError;
            }
        }


        public static string GetMemberAuthUrl(MemberAuthInfo mai)
        {
            string authUrl = Helper.CombineUrl(conf.SiteUrl, "NewMember/PasswordReset.aspx");
            authUrl += string.Format("?uid={0}&key={1}&code={2}&display=mobile",
                mai.UniqueId, mai.ForgetPasswordKey, mai.ForgetPasswordCode);
            return authUrl;
        }
        /// <summary>
        /// 給未登入，設定密碼，發送認證碼簡訊
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public static SendMobileAuthCodeReply SendMobileAuthCode(string mobile)
        {
            try
            {
                if (RegExRules.CheckMobile(mobile) == false)
                {
                    return SendMobileAuthCodeReply.MobileError;
                }
                MobileMember mm = MemberFacade.GetMobileMember(mobile);
                if (mm.IsLoaded == false)
                {
                    return SendMobileAuthCodeReply.DataError;
                }
                MobileAuthInfo mobileAuthInfo = MemberFacade.GetValidOrNewMobileAuthInfo(mm.UserId, mobile);

                mobileAuthInfo.ValidTime = DateTime.Now.AddMinutes(_MOBILE_CODE_VALID_MINUTES);
                MemberFacade.SetMobileAuthInfo(mobileAuthInfo);

                Member mem = MemberFacade.GetMember(mm.UserId);
                if (mem.IsLoaded == false)
                {
                    return SendMobileAuthCodeReply.DataError;
                }
                MemberAuthInfo mai = MemberFacade.GetOrAddMemberAuthInfo(mem.UniqueId, mem.UserEmail);
                MemberFacade.ResetMemberAuthInfo(mai);

                try
                {
                    string authUrl = GetMemberAuthUrl(mai);
                    authUrl = WebUtility.RequestShortUrl(authUrl);
                    SMS sms = new SMS();
                    string msg = string.Format("{0}為17Life認證碼，10分鐘後失效。或以此連結設定密碼：{1}",
                        mobileAuthInfo.Code, authUrl);
                    sms.SendMessage(string.Empty, msg, string.Empty, mobileAuthInfo.Mobile,
                        mm.UserId.ToString(), SmsType.Ppon, Guid.Empty, "0");
                    return SendMobileAuthCodeReply.Success;
                }
                catch (Exception ex)
                {
                    logger.Error("SendMobileAuthCode error, mobile=" + mobile, ex);
                    return SendMobileAuthCodeReply.SmsError;
                }
            }
            catch (Exception ex)
            {
                string msg = string.Format("手機認證發送簡訊失敗. mobile={0}", mobile);
                logger.Warn(msg, ex);
                return SendMobileAuthCodeReply.OtherError;
            }
        }

        #region API用

        /// <summary>
        /// 傳入取得的FB token 完成FB登入作業。
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static SignInReplyData FBLoginIn(string token, out string fbpic)
        {
            fbpic = string.Empty;
            SingleSignOnSource signOnSource = SingleSignOnSource.Facebook;
            if (!string.IsNullOrWhiteSpace(token))
            {
                FacebookUser fbuser = FacebookUser.Get(token);
                if (fbuser == null)
                {
                    return new SignInReplyData(SignInReply.OtherError, null, signOnSource, Phrase.SignInReplyOtherError);
                }
                else
                {
                    fbpic = fbuser.Pic;
                }
                SignInReplyData reply = UserSignIn(fbuser.Id, SingleSignOnSource.Facebook);
                if (reply.Reply == SignInReply.MemberLinkNotExist)
                {
                    reply.ExtId = fbuser.Id;
                }
                return reply;
            }

            return new SignInReplyData(SignInReply.OtherError, null, signOnSource, Phrase.SignInReplyOtherError);
        }
        public static SignInReplyData FBLoginIn(string token)
        {
            string fbpic;
            return FBLoginIn(token, out fbpic);
        }
        /// <summary>
        /// 傳入取得的FB token 完成FB登入作業。
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static SignInReplyData LineLogin(string token)
        {
            SingleSignOnSource signOnSource = SingleSignOnSource.Line;
            if (!string.IsNullOrWhiteSpace(token))
            {
                LineUser lineUser = LineUser.Get(token);
                if (lineUser == null)
                {
                    return new SignInReplyData(SignInReply.OtherError, null, signOnSource, Phrase.SignInReplyOtherError);
                }
                SignInReplyData reply = UserSignIn(lineUser.Id, SingleSignOnSource.Line);
                if (reply.Reply == SignInReply.MemberLinkNotExist)
                {
                    reply.ExtId = lineUser.Id;
                }
                return reply;
            }

            return new SignInReplyData(SignInReply.OtherError, null, signOnSource, Phrase.SignInReplyOtherError);
        }
        public static SignInReplyData ContactLogin(string account, string password)
        {
            Member mem;
            SingleSignOnSource signOnSource;
            MemberLinkCollection mlCol = null;
            string userName;

            return ContactLogin(account, password, ref mlCol, out userName, out signOnSource, out mem);
        }
        /// <summary>
        /// 17life帳號的登入作業。
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static SignInReplyData ContactLogin(string account, string password,
            ref MemberLinkCollection mlCol, out string userName, out SingleSignOnSource signOnSource, out Member mem)
        {

            SignInReply reply =
                LoginProviders.Login(account, password, false, ref mlCol, out userName, out signOnSource, out mem);

            //檢查有無輸入資料
            if (reply == SignInReply.AccountNotExist)
            {
                return new SignInReplyData(reply, null, signOnSource, Phrase.SignInReplyAccountNotExist);
            }
            if (reply == SignInReply.PasswordError)
            {
                int failedPasswordAttemptCount;
                if (signOnSource == SingleSignOnSource.Mobile17Life)
                {
                    failedPasswordAttemptCount = MemberFacade.GetMobileMember(account).FailedPasswordAttemptCount;
                } else
                {
                    failedPasswordAttemptCount = mem.FailedPasswordAttemptCount;
                }
                if ((Membership.MaxInvalidPasswordAttempts - failedPasswordAttemptCount) == 0)
                {
                    return new SignInReplyData(SignInReply.AccountIsLockedOut, null, signOnSource, Phrase.SignInReplyAccountIsLockedOut);
                }
                else
                {
                    return new SignInReplyData(reply, null, signOnSource,
                        string.Format("密碼輸入錯誤，您還剩{0}次機會",
                            Membership.MaxInvalidPasswordAttempts - failedPasswordAttemptCount));
                }
            }
            if (reply == SignInReply.AccountIsLockedOut || reply == SignInReply.AccountNotApproved)
            {
                return new SignInReplyData(reply, null, signOnSource, Phrase.SignInReplyAccountIsLockedOut);
            }
            if (reply == SignInReply.EmailInactive)
            {
                return new SignInReplyData(reply, null, signOnSource, Phrase.SignInReplyAccountEmailInactive);
            }
            if (reply == SignInReply.MemberLinkNotExist)
            {
                return new SignInReplyData(reply, null, signOnSource, Phrase.SignInReplyAccountEmailInactive);
            }
            if (reply == SignInReply.MemberLinkExistButWrongWay)
            {
                //帳號存在，不過不是17LIFE登入
                string message = string.Empty;
                foreach (MemberLink memberLink in mlCol)
                {
                    if (memberLink.ExternalOrg == (int)SingleSignOnSource.Facebook)
                    {
                        message = "Facebook";
                    }
                    if (memberLink.ExternalOrg == (int)SingleSignOnSource.PayEasy)
                    {
                        message = string.IsNullOrWhiteSpace(message)
                            ? "Payeasy"
                            : message + "/Payeasy";
                    }
                }
                message = string.Format("此信箱已設定為 {0} 帳號，請由 {0} 登入。", message);
                return new SignInReplyData(reply, null, signOnSource, message);
            }
            if (reply == SignInReply.Success)
            {
                //登入成功，處理回傳資訊
                return new SignInReplyData(SignInReply.Success, mem, signOnSource, string.Empty);
            }

            return new SignInReplyData(SignInReply.AccountNotExist, null, signOnSource, Phrase.SignInReplyAccountNotExist);
        }

        /// <summary>
        /// 利用發送的登入憑證進行登入
        /// </summary>
        /// <param name="ticket"></param>
        /// <param name="userId"></param>
        /// <param name="driverId"></param>
        /// <returns></returns>
        public static SignInReplyData ContactLoginByTicket(string ticket, int userId, string driverId)
        {
            //設定登入方式的預設值
            SingleSignOnSource signOnSource = SingleSignOnSource.ContactDigitalIntegration;
            //會員被鎖住了，就別重發憑證
            Member mem = MemberFacade.GetMember(userId);
            if (mem.IsLoaded == false || mem.IsApproved == false)
            {
                return new SignInReplyData(SignInReply.TicketSignInError, null, signOnSource, Phrase.SignInReplyTicketSignInError);
            }
            //查詢LoginTicket資料
            LoginTicket loginTicket = mp.LoginTicketGetExpiration(ticket, userId, driverId, LoginTicketType.AppLogin);
            //查無登入憑證的資料
            if (!loginTicket.IsLoaded)
            {
                if (mp.LoginTicketGet(ticket).IsLoaded == false)
                {
                    logger.Hit("ticketNotFound");
                    AccountAudit audit = mp.AccountAuditGetList(userId, AccountAuditAction.LogNewTicket)
                        .FirstOrDefault(t => t.Memo == ticket);
                    if (audit != null && (DateTime.Now - audit.CreateTime).TotalDays < UserSignInManager.LoginTicketValidityDay)
                    {
                        logger.Hit("ticketNotFound:checked");
                        logger.InfoFormat("ticket遺失，請協尋, ticket={0}, userId={1}, driverId={2}", ticket, userId,
                                driverId);
                    }
                }
                return new SignInReplyData(SignInReply.TicketSignInError, null, signOnSource, Phrase.SignInReplyTicketSignInError);
            }

            signOnSource = (SingleSignOnSource)loginTicket.ExternalOrg;
            //查詢memberLink資料
            MemberLink memberLink = mp.MemberLinkGet(userId, signOnSource);
            if (memberLink != null && memberLink.IsLoaded)
            {
                return UserSignIn(memberLink, signOnSource);
            }
            return new SignInReplyData(SignInReply.AccountNotExist, null, signOnSource, Phrase.SignInReplyAccountNotExist);
        }

        /// <summary>
        /// Payeasy會員登入
        /// </summary>
        /// <param name="memId">會員帳號</param>
        /// <param name="password">會員密碼</param>
        /// <returns></returns>
        public static SignInReplyData PayeasyLogin(string memId, string password)
        {
            SingleSignOnSource signOnSource = SingleSignOnSource.PayEasy;
            if (!string.IsNullOrWhiteSpace(memId) && !string.IsNullOrWhiteSpace(password))
            {
                HttpWebRequest request = WebRequest.Create(conf.PayeasyIdCheckWebServiceUrl) as HttpWebRequest;

                string param = string.Format("memId={0}&memPwd={1}&tryCount={2}&clientIp={3}&appName={4}", memId, password, "1", Helper.GetClientIP(), "23");

                byte[] bs = Encoding.ASCII.GetBytes(param);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = bs.Length;

                try
                {
                    using (Stream reqStream = request.GetRequestStream())
                    {
                        reqStream.Write(bs, 0, bs.Length);
                    }

                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {
                        StreamReader reader = new StreamReader(response.GetResponseStream());

                        JObject user = JObject.Parse(reader.ReadToEnd());
                        string code = user.Value<string>("Code");
                        if (string.IsNullOrWhiteSpace(code))
                        {
                            return new SignInReplyData(SignInReply.OtherError, null, signOnSource, Phrase.SignInReplyOtherError);
                        }

                        if (code.Equals("OK"))
                        {
                            //登入成功
                            string message = user.Value<string>("Message");
                            string[] idList = message.Split(';');
                            if (idList.Length <= 2)
                            {
                                string externalId = idList[1];
                                SignInReplyData reply = UserSignIn(externalId, SingleSignOnSource.PayEasy);
                                //串接資料不存在，回傳串接編號供必要時進行註冊
                                if (reply.Reply == SignInReply.MemberLinkNotExist)
                                {
                                    reply.ExtId = externalId;
                                }
                                return reply;
                            }
                        }
                        else if (code.Equals("FAIL"))
                        {
                            //登入失敗，帳號密碼錯誤
                            return new SignInReplyData(SignInReply.AccountNotExist, null, signOnSource,
                                                       Phrase.SignInReplyAccountNotExist);

                        }
                        else if (code.Equals("EXCEPTION"))
                        {
                            return new SignInReplyData(SignInReply.OtherError, null, signOnSource,
                                                       "您的帳號無法登入，請與PayEasy客服聯絡，謝謝。");
                        }
                    }
                }
                catch (Exception)
                {
                    return new SignInReplyData(SignInReply.AccountNotExist, null, signOnSource, Phrase.SignInReplyAccountNotExist);
                }
            }

            return new SignInReplyData(SignInReply.AccountNotExist, null, signOnSource, Phrase.SignInReplyAccountNotExist);
        }

        /// <summary>
        /// Vbs會員登入
        /// </summary>
        /// <param name="memAccId">會員帳號</param>
        /// <param name="password">會員密碼</param>
        /// <param name="notCheckPassword">不檢查會員密碼</param>
        /// <returns></returns>
        public static SignInReplyData VbsMemberLogin(string memAccId, string password, bool notCheckPassword = false)
        {
            SingleSignOnSource signOnSource = SingleSignOnSource.VbsMember;
            if (!string.IsNullOrWhiteSpace(memAccId) && !string.IsNullOrWhiteSpace(password))
            {
                try
                {
                    var loginSuccess = notCheckPassword || VbsMemberUtility.UserSignIn(memAccId, password);

                    if (loginSuccess)
                    {
                        var vbs_id = mp.VbsMembershipGetByAccountId(memAccId).Id;
                        SignInReplyData reply = UserSignIn(vbs_id.ToString(), SingleSignOnSource.VbsMember);

                        if (reply.Reply == SignInReply.Success)
                        {
                            return reply;
                        }
                    }

                    return new SignInReplyData(SignInReply.OtherError, null, signOnSource
                                , "您的帳號無法登入，請與17Life客服聯絡，謝謝。");

                }
                catch (Exception)
                {
                    return new SignInReplyData(SignInReply.AccountNotExist, null, signOnSource, Phrase.SignInReplyAccountNotExist);
                }
            }

            return new SignInReplyData(SignInReply.AccountNotExist, null, signOnSource, Phrase.SignInReplyAccountNotExist);
        }

        /// <summary>
        /// 新增外部會員的串接資料，並完成登入作業
        /// </summary>
        /// <param name="email"></param>
        /// <param name="externalUserId"></param>
        /// <param name="signSource"></param>
        /// <param name="cityId"></param>
        /// <param name="receiveEDM">是否訂閱EDM</param>
        /// <returns></returns>
        public static SignInReplyData RegisterExternalMember(string email, string externalUserId, SingleSignOnSource signSource, int cityId, bool receiveEDM)
        {
            IMemberRegister reg = null;
            if (signSource == SingleSignOnSource.Facebook)
            {
                reg = new FacebookMemberRegister(externalUserId, email, email, cityId);
            }
            else if (signSource == SingleSignOnSource.PayEasy)
            {
                reg = new PayeasyMemberRegister(externalUserId, email, cityId);
            }
            if (reg != null)
            {
                reg.IsMobile = true;
                MemberRegisterReplyType reply = reg.Process(receiveEDM);
                if (reply == MemberRegisterReplyType.RegisterSuccess)
                {
                    MemberLink memberLink = mp.MemberLinkGet(MemberFacade.GetUniqueId(email), signSource);
                    if (memberLink != null && memberLink.IsLoaded)
                    {
                        return UserSignIn(memberLink, signSource);
                    }
                    return new SignInReplyData(SignInReply.RegisterError, null, signSource, reg.Message);
                }
                if (reply == MemberRegisterReplyType.ExternalUserIdExists)
                {
                    return new SignInReplyData(SignInReply.RegisterError, null, signSource, reg.Message);
                }
                if (reply == MemberRegisterReplyType.MailOldMember || reply == MemberRegisterReplyType.MailRegisterInactive)
                {
                    return new SignInReplyData(SignInReply.LinkErrorEmailExists, null, signSource, Phrase.SignInReplyLinkErrorEmailExists);
                }
                return new SignInReplyData(SignInReply.RegisterError, null, signSource, reg.Message);
            }
            return new SignInReplyData(SignInReply.RegisterError, null, signSource, Phrase.RegisterReturnMessageRegisterError);
        }

        /// <summary>
        /// 會員綁定其它串接登入方式
        /// </summary>
        /// <param name="memberId"></param>
        /// <param name="sourceOrg"></param>
        /// <param name="accessToken"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool BindExternalMember(int memberId, SingleSignOnSource sourceOrg, string accessToken,
           string pezMemId, string pezPassword, out string message)
        {
            message = string.Empty;
            Member mem = MemberFacade.GetMember(memberId);
            if (mem.IsLoaded == false)
            {
                message = "使用者資料找不到";
                return false;
            }

            ///這邊有疑惑，應該是回傳錯誤，還是徑自更新串接資料
            if (mp.MemberLinkGet(memberId, sourceOrg).IsLoaded)
            {
                message = "串接資料己綁定";
                return false;
            }

            if (sourceOrg == SingleSignOnSource.Line)
            {
                LineUser lineUser = LineUser.Get(accessToken);
                if (lineUser == null || string.IsNullOrEmpty(lineUser.Id))
                {
                    message = "Line使用者資料找不到";
                    return false;
                }
                if (mp.MemberLinkGetByExternalId(lineUser.Id, sourceOrg).IsLoaded)
                {
                    message = "串接資料綁定在其他帳號";
                    return false;
                }
                MemberFacade.AddMemberLink(mem.UniqueId, SingleSignOnSource.Line, lineUser.Id,
                    mem.UserName);
            }
            else if (sourceOrg == SingleSignOnSource.Facebook)
            {
                FacebookUser fbUser = FacebookUser.Get(accessToken);
                if (fbUser == null || string.IsNullOrEmpty(fbUser.Id))
                {
                    message = "Line使用者資料找不到";
                    return false;
                }
                if (mp.MemberLinkGetByExternalId(fbUser.Id, sourceOrg).IsLoaded)
                {
                    message = "串接資料綁定在其他帳號";
                    return false;
                }
                MemberFacade.AddMemberLink(mem.UniqueId, SingleSignOnSource.Facebook, fbUser.Id,
                    mem.UserName);
            }
            else if (sourceOrg == SingleSignOnSource.PayEasy)
            {
                PayeasyUser payeasyUser = PayeasyUser.Get(pezMemId, pezPassword, out message);
                if (payeasyUser == null || string.IsNullOrEmpty(payeasyUser.Id))
                {
                    return false;
                }
                if (mp.MemberLinkGetByExternalId(payeasyUser.Id, sourceOrg).IsLoaded)
                {
                    message = "串接資料綁定在其他帳號";
                    return false;
                }
                MemberFacade.AddMemberLink(mem.UniqueId, SingleSignOnSource.PayEasy, payeasyUser.Id,
                    mem.UserName);
            }

            return true;
        }

        public static RegisterContactMemberReplyData RegisterContactMember(string userName, string password, int cityId, bool receiveEDM)
        {
            string message = string.Empty;
            if (string.IsNullOrWhiteSpace(userName) || string.IsNullOrWhiteSpace(password))
            {
                return new RegisterContactMemberReplyData(MemberRegisterReplyType.DataInadequate,
                                                          Phrase.RegisterReturnMessageDataInadequate);
            }
            MemberRegisterReplyType replyType = RegisterMember(userName, password, cityId, receiveEDM, out message);
            var result = new RegisterContactMemberReplyData(replyType, message);
            if (replyType == MemberRegisterReplyType.RegisterSuccess)
            {
                result.UserName = userName;
            }
            return result;
        }

        /// <summary>
        /// 寄送EMail驗證信
        /// </summary>
        /// <param name="userName">會員申請的帳號</param>
        /// <returns></returns>
        public static MemberSendConfirmMailReplyType SendAccountConfirmMail(string userName)
        {
            Member member = mp.MemberGet(userName);
            if (member == null || (!member.IsLoaded))
            {
                return MemberSendConfirmMailReplyType.MemberNoExists;
            }
            MemberAuthInfo authInfo = mp.MemberAuthInfoGet(member.UniqueId);
            if (authInfo == null || (!authInfo.IsLoaded))
            {
                return MemberSendConfirmMailReplyType.MemberAuthNoExists;
            }
            //己經狀態是active的應該不用再寄認證信了
            if (Helper.IsFlagSet(member.Status, MemberStatusFlag.Active17Life))
            {
                return MemberSendConfirmMailReplyType.MemberAlreadyActived;
            }

            MemberFacade.SendAccountConfirmMail(member.UserEmail, member.UniqueId.ToString(), authInfo.AuthKey, authInfo.AuthCode);
            return MemberSendConfirmMailReplyType.Success;
        }

        /// <summary>
        /// 註冊17life帳號，所以需要密碼
        /// </summary>
        /// <param name="userName">會員編號</param>
        /// <param name="password">密碼</param>
        /// <param name="cityId">城市編號</param>
        /// <param name="receiveEDM">是否訂閱EDM</param>
        /// <param name="message">是否訂閱EDM</param>
        private static MemberRegisterReplyType RegisterMember(string userName, string password, int cityId, bool receiveEDM, out string message)
        {
            IMemberRegister reg = new Life17MemberRegister(userName, password, cityId);
            reg.IsMobile = true;
            MemberRegisterReplyType result = reg.Process(receiveEDM);
            message = reg.Message;
            return result;
        }

        /// <summary>
        /// 檢查串接資料是否有該會員，並取得該使用者的member資料
        /// </summary>
        /// <param name="externalId">串接資料編號</param>
        /// <param name="singelSource">登入方式</param>
        /// <returns></returns>
        private static SignInReplyData UserSignIn(string externalId, SingleSignOnSource singelSource)
        {
            //取得串接資料
            MemberLink link = mp.MemberLinkGetByExternalId(externalId, singelSource);
            if (link.IsLoaded)
            {
                //有紀錄開始登入
                return UserSignIn(link, singelSource);
                //如果使用者還有登入類別尚未串接，且沒有勾選不要詢問的話，就轉址到串接頁面
            }
            else
            {
                //無串接資料
                return new SignInReplyData(SignInReply.MemberLinkNotExist, null, singelSource, Phrase.SignInReplyMemberLinkNotExist);
            }
        }

        /// <summary>
        /// 手機使用者登入
        /// 傳入已取得的memberlink資料，進行登入作業
        /// </summary>
        /// <param name="link">存在的memberLink資料</param>
        /// <param name="singelSource">登入的類別</param>
        /// <returns></returns>
        private static SignInReplyData UserSignIn(MemberLink link, SingleSignOnSource singelSource)
        {
            if (link == null || !link.IsLoaded)
            {
                return new SignInReplyData(SignInReply.MemberLinkNotExist, null, singelSource, Phrase.SignInReplyMemberLinkNotExist);
            }
            AccountAuditAction accountAuditAction = AccountAuditAction.LoginUsing17LifeAccount;
            switch (singelSource)
            {
                case SingleSignOnSource.PayEasy:
                    accountAuditAction = AccountAuditAction.LoginUsingPayezAccount;
                    break;

                case SingleSignOnSource.Facebook:
                    accountAuditAction = AccountAuditAction.LoginUsingFbAccount;
                    break;

                case SingleSignOnSource.Line:
                    accountAuditAction = AccountAuditAction.LoginUsingLineAccount;
                    break;

                case SingleSignOnSource.ContactDigitalIntegration:
                    accountAuditAction = AccountAuditAction.LoginUsing17LifeAccount;
                    break;
            }

            //如果使用者還有登入類別尚未串接，且沒有勾選不要詢問的話，就轉址到串接頁面
            Member mem = mp.MemberGet(link.UserId);
            if (mem.IsLoaded)
            {
                //康太的帳號檢查有無開通
                //檢查EMAIL是否開通
                if (singelSource == SingleSignOnSource.ContactDigitalIntegration)
                {
                    if ((mem.Status & (int)MemberStatusFlag.Active17Life) <= 0)
                    {
                        //未開通
                        return new SignInReplyData(SignInReply.EmailInactive, null, singelSource, Phrase.SignInReplyAccountEmailInactive);
                    }
                }

                //membership是否有該紀錄
                if (!mem.IsApproved || mem.IsLockedOut)
                {
                    //membership查無該紀錄、帳號失效或帳號鎖定，紀錄嘗試登入的log
                    mp.AccountAuditSet(mem.UserName, Helper.GetClientIP(), accountAuditAction, false);
                    return new SignInReplyData(SignInReply.AccountIsLockedOut, null, singelSource, Phrase.SignInReplyAccountIsLockedOut);
                }

                HttpContext.Current.Response.Cookies.Add(
                    CookieManager.MakeCookie(LkSiteCookie.CookieVersion.ToString(), mem.Version.ToString(CultureInfo.InvariantCulture), false));
                //通過帳號檢查，處理登入紀錄
                object deviceType = Helper.GetContextItem(LkSiteContextItem._DEVICE_TYPE);
                if (deviceType is int)
                {
                    int deviceId = (int)deviceType;
                    if (deviceId == (int)ApiUserDeviceType.Android)
                    {
                        mp.AccountAuditSet(mem.UserName, Helper.GetClientIP(), accountAuditAction, true, null,
                            OrderFromType.ByAndroid);
                    }
                    else if (deviceId == (int)ApiUserDeviceType.IOS)
                    {
                        mp.AccountAuditSet(mem.UserName, Helper.GetClientIP(), accountAuditAction, true, null,
                            OrderFromType.ByIOS);
                    }
                }
                else
                {
                    mp.AccountAuditSet(mem.UserName, Helper.GetClientIP(), accountAuditAction, true);
                }

                mem.LastLoginDate = DateTime.Now;
                mp.MemberSet(mem);

                CookieManager.SetMemberAuth(mem.UserName);

                MemberEventsCore.OnMemberLoggedIn(singelSource, mem.UniqueId, UserDeviceType.App);

                //登入成功，處理回傳資訊
                return new SignInReplyData(SignInReply.Success, mem, singelSource, string.Empty);
            }
            else
            {
                return new SignInReplyData(SignInReply.AccountNotExist, null, singelSource, Phrase.SignInReplyAccountNotExist);
            }
        }

        #endregion API用

        public static string GetCurrentMemberName()
        {
            IIdentity identify = HttpContext.Current.User.Identity;
            if (identify.IsAuthenticated == false)
            {
                return string.Empty;
            }
            PponIdentity pponIdentity = identify as PponIdentity;
            if (pponIdentity == null)
            {
                pponIdentity = PponIdentity.Get(identify.Name);
            }
            return pponIdentity.DisplayName;
        }

        public static Dictionary<decimal, int> GetDiscountList()
        {
            Dictionary<decimal, int> discount = new Dictionary<decimal, int>();
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies[LkSiteCookie.MemberDiscount.ToString()];
                bool dataFound = false;
                if (cookie != null && cookie.Value != "")
                {
                    try
                    {
                        var cookieval = new JsonSerializer().Deserialize<CookieModel>(Helper.Decrypt(cookie.Value));
                        if (cookieval.Value.ToString().IndexOf('#') > 0)
                        {
                            string[] discountinfo = cookieval.Value.ToString().Split('|');
                            foreach (string item in discountinfo)
                            {
                                string[] tmp = item.Split('#');
                                if (tmp.Count() == 2)
                                {
                                    decimal amount = decimal.TryParse(tmp[0], out amount) ? amount : 0;
                                    int count = int.TryParse(tmp[1], out count) ? count : 0;
                                    discount.Add(amount, count);
                                }
                            }
                        }
                        dataFound = true;
                    }
                    catch
                    {
                    }
                }
                if (dataFound == false)
                {
                    discount = PromotionFacade.GetMemberDiscountCode(HttpContext.Current.User.Identity.Name);
                    CookieManager.SetMemberDiscount(discount);
                }
            }
            return discount;
        }

        public static SignInReply GetMemberSignInStatus(int userId)
        {
            var mem = MemberFacade.GetMember(userId);
            var userName = mem.UserName;

            if (mem.IsLoaded == false)
            {
                return SignInReply.AccountNotExist;
            }
            var mlCol = mp.MemberLinkGetList(mem.UniqueId);
            if (mlCol.Count == 0)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsing17LifeAccount, false, "會員無串接資料",
                    Helper.GetOrderFromType());
                return SignInReply.MemberLinkNotExist;
            }

            if (mem.IsLockedOut)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsing17LifeAccount, false, "帳號鎖定",
                    Helper.GetOrderFromType());
                return SignInReply.AccountIsLockedOut;
            }

            if (mem.IsApproved == false)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsing17LifeAccount, false, "帳號拒絕",
                    Helper.GetOrderFromType());
                return SignInReply.AccountNotApproved;
            }

            //檢查mobile
            MobileMember mm = mp.MobileMemberGet(mem.UniqueId);
            if (mm.IsLoaded && mm.Status == (int)MobileMemberStatusType.None)
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsing17LifeAccount, false, "手機未驗証",
                     Helper.GetOrderFromType());
                return SignInReply.MobileInactive;
            }

            if (!mm.IsLoaded && mlCol.Any(x => x.ExternalOrg == (int)SingleSignOnSource.Mobile17Life) 
                && !Helper.IsFlagSet(mem.Status, MemberStatusFlag.Active17Life))
            {
                mp.AccountAuditSet(userName, Helper.GetClientIP(), AccountAuditAction.LoginUsing17LifeAccount, false, "帳號未通過驗證",
                    Helper.GetOrderFromType());
                return SignInReply.EmailInactive;
            }
           
            return SignInReply.Success;
        }

        public static SignInReply SingnInfo(string account, ref MemberLinkCollection mlCol)
        {
            //檢查有無輸入資料
            if (string.IsNullOrWhiteSpace(account))
            {
                return SignInReply.AccountNotExist;
            }
            var mem = MemberFacade.GetMember(account);

            if (mem.IsLoaded == false)
            {
                return SignInReply.AccountNotExist;
            }
            mlCol = mp.MemberLinkGetList(mem.UniqueId);
            if (mlCol.Count == 0)
            {
                return SignInReply.MemberLinkNotExist;
            }
            if (mlCol.All(t => t.ExternalOrg != (int)SingleSignOnSource.ContactDigitalIntegration))
            {
                return SignInReply.MemberLinkExistButWrongWay;
            }
           
            return SignInReply.Success;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Component
{
    public class OrderMemo
    {
        //收據類型
        public string ReceiptsType { get; set; }
        //發票類型
        public string InvoiceType { get; set; }
        //幾聯式發票
        public string UnifiedSerialNumber { get; set; }
        //發票抬頭
        public string InvoiceTitle { get; set; }
        //統一編號
        public string InvoiceNo { get; set; }
        //發票備註
        public string InvoiceMemo { get; set; }
        //出貨日期
        public DateTime? ShipDate { get; set; }
        //託運單號
        public string ShipNo { get; set; }
        //物流公司
        public string ShipAgent { get; set; }

        public OrderMemo() { }
        public OrderMemo(string memo)
        {
            string[] memoList = (string.IsNullOrEmpty(memo) ? "" : memo).Split(new string[] { "|" }, StringSplitOptions.None);
            ReceiptsType = memoList.Skip(0).DefaultIfEmpty(string.Empty).First();
            InvoiceType = memoList.Skip(1).DefaultIfEmpty(string.Empty).First();
            UnifiedSerialNumber = memoList.Skip(2).DefaultIfEmpty(string.Empty).First();
            InvoiceTitle = memoList.Skip(3).DefaultIfEmpty(string.Empty).First();
            InvoiceNo = memoList.Skip(4).DefaultIfEmpty(string.Empty).First();
            InvoiceMemo = memoList.Skip(5).DefaultIfEmpty(string.Empty).First();
            DateTime shipdate;
            if (DateTime.TryParse(memoList.Skip(6).DefaultIfEmpty(string.Empty).First(), out shipdate))
                ShipDate = shipdate;
            ShipNo = memoList.Skip(7).DefaultIfEmpty(string.Empty).First();
            ShipAgent = memoList.Skip(8).DefaultIfEmpty(string.Empty).First();
        }
        public override string ToString()
        {
            return ReceiptsType + "|" + InvoiceType + "|" + UnifiedSerialNumber + "|" + InvoiceTitle + "|" + InvoiceNo + "|" + InvoiceMemo + "|" + (ShipDate == null ? string.Empty : ShipDate.Value.ToString("yyyy/MM/dd HH:mm")) + "|" + ShipNo + "|" + ShipAgent;
        }
    }
}

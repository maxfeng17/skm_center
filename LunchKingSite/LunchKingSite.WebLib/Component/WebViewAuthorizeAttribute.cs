﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.WebViewVendorSystem;
using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LunchKingSite.Core
{
    [AttributeUsage(AttributeTargets.Method)]
    public class WebViewAuthorizeAttribute : System.Web.Mvc.AuthorizeAttribute
    {
        private string Token
        {
            get
            {
                if (HttpContext.Current.User == null || HttpContext.Current.Items["AccessToken"] == null)
                {
                    return string.Empty;

                }
                else
                {
                    return HttpContext.Current.Items["AccessToken"].ToString();
                }                
            }
        }
        
        public AuthorizedUserType AuthorizedUserType { get; set; }

        public WebViewAuthorizeAttribute()
        {
            AuthorizedUserType = AuthorizedUserType.Vendor;
        }

        protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }

            if (string.IsNullOrEmpty(Token))
            {
                return false;
            }

            return true;
        }

        public override void OnAuthorization(System.Web.Mvc.AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            
            if (HttpContext.Current.User == null || !HttpContext.Current.User.Identity.IsAuthenticated)
            {
                filterContext.Result = GetLoginExpiredRedirectResult();
                return;
            }

            if (string.IsNullOrEmpty(Token))
            {
                filterContext.Result = GetLoginErrorRedirectResult();
                return;
            }

            var user = HttpContext.Current.User.Identity as PponIdentity;
            var vendorUser = VBSFacade.GetVbsMembershipByUserId(user.Id);
            //檢查user_id是否存在於VbsMemberShip中判斷帳號身分
            if (AuthorizedUserType == AuthorizedUserType.Vendor )
            {
                if (vendorUser == null || !vendorUser.IsLoaded)
                {
                    filterContext.Result = GetLoginInvalidRedirectResult();
                    return;
                }
            }
            else
            {
                if (vendorUser != null && vendorUser.IsLoaded)
                {
                    filterContext.Result = GetForbidLoginRedirectResult();
                    return;
                }
            }

            return;
        }
        
        private ActionResult GetLoginExpiredRedirectResult()
        {
            // 用 Global.asax 定義的路徑
            return new RedirectToRouteResult("WebViewVendorSystemDefault", new RouteValueDictionary
                                                                     {
                                                                         {"action", "LoginError"},
                                                                         {"result", LoginFailResult.LoginInfoExpired }
                                                                     });
        }

        private ActionResult GetLoginErrorRedirectResult()
        {
            return new RedirectToRouteResult("WebViewVendorSystemDefault", new RouteValueDictionary
                                                                     {
                                                                         {"action", "LoginError"},
                                                                         {"result", LoginFailResult.LoginInfoError }
                                                                     });

        }

        private ActionResult GetForbidLoginRedirectResult()
        {
            return new RedirectToRouteResult("WebViewVendorSystemDefault", new RouteValueDictionary
                                                                     {
                                                                         {"action", "LoginError"},
                                                                         {"result", LoginFailResult.ForbiddenAccount }
                                                                     });

        }
        private ActionResult GetLoginInvalidRedirectResult()
        {
            return new RedirectToRouteResult("WebViewVendorSystemDefault", new RouteValueDictionary
                                                                     {
                                                                         {"action", "LoginError"},
                                                                         {"result", LoginFailResult.InvalidAccount }
                                                                     });

        }
    }
}

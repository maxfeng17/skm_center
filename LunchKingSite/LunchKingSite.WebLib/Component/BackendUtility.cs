﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Models.VendorBillingSystem;
using LunchKingSite.DataOrm;
using System.Web;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Interface;
using System.Data;
using LunchKingSite.Core.Enumeration;
using System.Transactions;
using Newtonsoft.Json;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;

namespace LunchKingSite.WebLib.Component
{
    public class BackendUtility
    {
        protected static IPponProvider pp;
        protected static IOrderProvider op;
        protected static IMemberProvider mp;
        protected static IPCPProvider pcp;
        protected static ISellerProvider sp;

        static BackendUtility()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        }

        public static VendorDetailData VbsCompanyDetailGet(Guid sellerGuid)
        {
            var detail = pcp.ViewVbsPcpDetailGet(sellerGuid);

            if (!detail.IsLoaded)
            {
                var seller = sp.SellerGet(sellerGuid);
                if (seller.IsLoaded)
                {
                    detail.SellerName = seller.SellerName;
                    detail.InContractWith = seller.CompanyName;
                    detail.SellerEmail = seller.SellerEmail;
                    detail.SellerMobile = seller.SellerMobile;
                    var dateCollect = Helper.GetFirstAndLastDayOfMonth(DateTime.Now.AddMonths(12));
                    detail.ContractValidDate = Helper.GetFinalTime(dateCollect.Item2);
                    detail.SellerGuid = sellerGuid;
                }
            }

            var categoryList = sp.CategoryGetList((int)CategoryType.MembershipCard).Select(x => new { Id = x.Id, Text = x.Name });

            string tmp = detail.CategoryList ?? "";
            var categoryCheck = tmp.Split(',').Select(x => string.IsNullOrWhiteSpace(x) ? "0" : x)
                .Select(y => new { Id = int.Parse(y), Text = "" }).ToList();

            var lines =
                from cl in categoryList
                join cc in categoryCheck on cl.Id equals cc.Id into j
                from cc in j.DefaultIfEmpty(new { Id = 0, Text = "" })
                select new { ID = cl.Id, Text = cl.Text, Check = cc.Id != 0 };

            var imageData = VBSFacade.GetSellerImage(detail.GroupId);
            var intros = pcp.PcpIntroGet(PcpIntroType.Seller, detail.GroupId);
            var corporateImagePath = (from item in imageData.Where(x => x.ImgType == PcpImageType.CorporateImage)
                                      orderby item.Seq ascending
                                      let path = ImageFacade.GetMediaPath(item.ImageCompressedPathUrl, MediaType.PCPImage)
                                      select path).ToArray<string>();
            var vcd = new VendorDetailData()
            {
                UserId = detail.UserId.ToString(),
                Email = detail.Email,
                Mobile = detail.Mobile,
                InvoiceTitle = detail.InvoiceTitle,
                InvoiceComId = detail.InvoiceComId,
                InvoiceName = detail.InvoiceName,
                InvoiceAddress = detail.InvoiceAddress,
                AccountBankCode = detail.AccountBankCode,
                AccountBranchCode = detail.AccountBranchCode,
                AccountName = detail.AccountName,
                AccountId = detail.AccountId,
                AccountNo = detail.AccountNo,
                SellerName = detail.SellerName,
                InContractWith = detail.InContractWith,
                CategoryJson = JsonConvert.SerializeObject(lines),
                SellerEmail = detail.SellerEmail,
                SellerMobile = detail.SellerMobile,
                ContractValidDate = string.Format("{0:yyyy-MM-dd}", detail.ContractValidDate),
                GroupId = detail.GroupId,
                SellerCardType = detail.CardType,
                SellerLogoPath = Helper.GetPropValue(VBSFacade.GetImagePathByType(imageData, PcpImageType.Logo), "result").ToString(),
                SellerFullCardPath = Helper.GetPropValue(VBSFacade.GetImagePathByType(imageData, PcpImageType.Card), "result").ToString(),
                SellerFullCardThumbnailPath = Helper.GetPropValue(VBSFacade.GetImagePathByType(imageData, PcpImageType.Card, true), "result").ToString(),
                SellerImagePath = corporateImagePath,
                SellerServiceImgPath =
                (string[])Helper.GetPropValue(VBSFacade.GetImagePathByType(imageData, PcpImageType.Service), "result"),
                SellerSrndgImgPath =
                (string[])Helper.GetPropValue(VBSFacade.GetImagePathByType(imageData, PcpImageType.EnvironmentAroundStore), "result"),
                IsPromo = detail.IsPromo,
                SellerIntro = intros.OrderBy(x => x.Id).Select(x => x.IntroContent).ToArray(),
                SellerGuid = detail.SellerGuid,
                IsNew = !detail.IsLoaded, //是否還未開啟熟客服務
                IsPos = pcp.MembershipCardGroupGet(detail.GroupId).IsPos //是否串接POS
            };
            //取得熟客卡服務範圍
            var membershipPermissions = pcp.MembershipCardGroupPermissionGetList(detail.GroupId);
            foreach (var p in membershipPermissions)
            {
                switch (p.Scope)
                {
                    case (int)MembershipService.Vip:
                        vcd.IsMemberShipCard = true;
                        break;
                    case (int)MembershipService.Deposit:
                        vcd.IsCupDeposit = true;
                        break;
                    case (int)MembershipService.Point:
                        vcd.IsPointDeposit = true;
                        break;
                }
            }

            return vcd;
        }

        public static bool SetVendorDetail(VendorDetailData d, string currentUser)
        {
            int userId;
            int.TryParse(d.UserId, out userId);
            var result = pcp.VbsCompanyDetailGet(d.SellerGuid);

            if (!result.IsLoaded)
            {
                result = PcpFacade.AddVbsCompanyDetail(d.SellerGuid, userId);
                result.CreateId = mp.MemberGet(currentUser).UniqueId;
            }

            try
            {
                foreach (var item in typeof(VendorDetailData).GetProperties())
                {
                    var parseValue = d.GetType().GetProperty(item.Name).GetValue(d, null);
                    var resultObj = result.GetType().GetProperties().Where(x => x.Name == item.Name).FirstOrDefault();
                    if (resultObj == null) { continue; }

                    switch (Type.GetTypeCode(resultObj.PropertyType))
                    {
                        case TypeCode.Int32:
                            int tmp;
                            int.TryParse(parseValue.ToString(), out tmp);
                            result.GetType().GetProperty(item.Name).SetValue(result, tmp, null);

                            break;
                        case TypeCode.String:
                            result.GetType().GetProperty(item.Name).SetValue(result, parseValue, null);
                            break;

                        default:
                            if (string.Equals(item.Name, "contractvaliddate", StringComparison.CurrentCultureIgnoreCase))
                            {
                                DateTime? dt = string.IsNullOrEmpty(parseValue.ToString()) ?
                                    (DateTime?)null :
                                    DateTime.Parse(parseValue.ToString());
                                result.GetType().GetProperty(item.Name).SetValue(result, dt, null);
                            }
                            break;
                    }
                }
                result.SellerGuid = d.SellerGuid;
                result.UserId = int.Parse(d.UserId);
                result.ModifyId = mp.MemberGet(currentUser).UniqueId;
                result.ModifyTime = DateTime.Now;

                pcp.VbsCompanyDetailSet(result);

                var memCardGroup = pcp.MembershipCardGroupGetSet(d.SellerGuid, true);
                memCardGroup.CategoryList = d.CategoryJson;
                memCardGroup.CardType = d.SellerCardType;
                memCardGroup.IsPromo = d.IsPromo;
                memCardGroup.SellerUserId = int.Parse(d.UserId);
                memCardGroup.IsPos = d.IsPos;
                pcp.MembershipCardGroupSet(memCardGroup);

                //儲存熟客服務 (熟客卡/寄杯/集點)
                MembershipCardGroupPermissionCollection permissionList = new MembershipCardGroupPermissionCollection();
                if (d.IsMemberShipCard)
                {
                    permissionList.Add(new MembershipCardGroupPermission() { CardGroudId = memCardGroup.Id, Scope = (int)MembershipService.Vip, CreateTime = DateTime.Now });
                }
                if (d.IsPointDeposit)
                {
                    permissionList.Add(new MembershipCardGroupPermission() { CardGroudId = memCardGroup.Id, Scope = (int)MembershipService.Point, CreateTime = DateTime.Now });
                }
                if (d.IsCupDeposit)
                {
                    permissionList.Add(new MembershipCardGroupPermission() { CardGroudId = memCardGroup.Id, Scope = (int)MembershipService.Deposit, CreateTime = DateTime.Now });
                }
                pcp.MembershipCardGroupPermissionDelAll(memCardGroup.Id);//先全刪
                if (permissionList.Any())
                {
                    pcp.MembershipCardGroupPermissionSet(permissionList);
                }

                //預設開啟零級卡片
                DateTime quarterTimeStart, closeTime;
                Helper.GetQuarterDateRange(DateTime.Now.Year, (Quarter)Helper.GetQuarterByDate(DateTime.Now), out quarterTimeStart, out closeTime);
                var mc = BonusFacade.MembershipCardGet(memCardGroup.Id, closeTime, 0);
                if (!mc.IsLoaded)
                {
                    int cardGroupId, versionId;

                    var version = pcp.MembershipCardGroupVersionGetCardGroupLastData(memCardGroup.Id);

                    BonusFacade.CheckMembershipCardGroupIdAndVersionId(d.SellerGuid, int.Parse(d.UserId),
                        memCardGroup.Id, version == null ? 0 : version.Id, quarterTimeStart, closeTime, out cardGroupId, out versionId);

                    version = pcp.MembershipCardGroupVersionGet(versionId);

                    mc = new MembershipCard
                    {
                        CardGroupId = cardGroupId,
                        VersionId = versionId,
                        Level = (int)MembershipCardLevel.Level0, //0級卡片
                        Enabled = true,
                        PaymentPercent = 1,
                        Others = string.Empty,
                        OrderNeeded = 0,
                        AmountNeeded = 0,
                        ConditionalLogic = (int)MembershipCardConditionalLogic.AmountNeededOrOrderNeeded,
                        OpenTime = version.OpenTime,
                        CloseTime = version.CloseTime,
                        Status = (byte)MembershipCardStatus.Open,
                        Instruction = string.Empty,
                        CreateId = MemberFacade.GetMember(HttpContext.Current.User.Identity.Name).UniqueId,
                        CreateTime = DateTime.Now,
                    };
                    mc.ModifyId = mc.CreateId;
                    mc.ModifyTime = mc.CreateTime;
                    BonusFacade.MembershipCardSet(mc);
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool SetVendorDetailDataImg(VendorDetailData d)
        {
            //foreach (var item in d.Files.AllKeys.Select((val2, idx2) => new { idx = Convert.ToInt32(idx2) + 1, key = val2 }))
            //{
            //    //上傳圖片
            //    IHttpPostedFileAdapter file = d.Files[item.key].ToAdapter();
            //    using (var img = Image.FromStream(file.InputStream))
            //    {
            //        UploadPcpImage(d.GroupId, d.ModifyUserId, img, item.idx, (PcpImageType)d.ImageType);
            //    }
            //}
            foreach (var item in d.Files.AllKeys)
            {
                //上傳圖片
                IHttpPostedFileAdapter file = d.Files[item].ToAdapter();
                using (var img = Image.FromStream(file.InputStream))
                {
                    int idx;
                    Regex r = new Regex(@"(\d+)$", RegexOptions.IgnoreCase | RegexOptions.Singleline);
                    if (!int.TryParse(r.Match(item).Value, out idx)) { continue; }
                    UploadPcpImage(d.GroupId, d.ModifyUserId, img, idx, (PcpImageType)d.ImageType);
                }
            }
            //重新排序
            ResortImage(d);

            return true;
        }

        private static void ResortImage(VendorDetailData d)
        {
            var mcg = pcp.MembershipCardGroupGet(d.GroupId);
            var imgList = pcp.PcpImageGet(new List<int> { mcg.Id }, (PcpImageType)d.ImageType);
            //pcp.PcpImageSet()
            var pcpImgCol = new PcpImageCollection();
            foreach (var item in imgList.Select((val2, idx2) => new { idx = Convert.ToInt32(idx2) + 1, key = val2 }))
            {
                item.key.Sequence = item.idx;
                pcpImgCol.Add(item.key);
            }

            if (pcpImgCol.Count > 0)
            {
                pcp.PcpImageSet(pcpImgCol);
            }
        }

        public static bool SetDelVendorDetailDataImg(VendorDetailData d)
        {
            var memCardGroup = pcp.MembershipCardGroupGet(d.GroupId);
            if (!memCardGroup.IsLoaded)
            {
                return false;
            }

            bool isDel = DeletePcpImage(d.ModifyUserId, d.GroupId, (PcpImageType)d.ImageType, d.Sequence);

            if (isDel)
            {
                //重新排序
                ResortImage(d);
            }

            return isDel;
        }

        public static List<PCPPointData> GetPcpPointData(int userId, PcpPointType type)
        {
            var data = pcp.PcpPointUsageLog(userId, type);

            if (data.Count == 0) { return new List<PCPPointData> { }; }

            List<Tuple<int, int, DateTime>> updateList = new List<Tuple<int, int, DateTime>> { };

            DateTime d = new DateTime();
            d = (data.Where(x => x.ExpireTime.HasValue)).Max(x => x.ExpireTime.Value);

            switch (type)
            {
                case PcpPointType.RegularsPoint:
                    //熟客點期限為最近加值時間
                    updateList = new List<Tuple<int, int, DateTime>>{
                        Tuple.Create(
                        (data.Where(x => x.DepositId.HasValue)).Max(x => x.DepositId.Value),
                        (data.Where(x => x.DepositId.HasValue)).Min(x => x.DepositId.Value),
                        (data.Where(x => x.ExpireTime.HasValue)).Max(x => x.ExpireTime.Value)
                    )};
                    break;

                case PcpPointType.FavorPoint:
                    //公關點期限：
                    //total<=0
                    //無期限
                    //total>0
                    //之前的最小日期
                    var tmp = (from x in data
                               where x.DepositId.HasValue
                               select new
                               {
                                   depositId = x.DepositId.Value,
                                   withdrawalId = x.WithdrawaloverallId,
                                   expireDate = x.ExpireTime,
                                   total = x.Total
                               }).ToList();

                    //初始化更新有效日期清單
                    //預設為全部
                    //排序的是由新到舊
                    //Tuple定義為 <最新的depositId,最舊的depositId,其間的最小時間>
                    //             （含）        （含）            比較時取first，這樣就是比較近的日期的最小值
                    //depositId必須有值（上面TMP已經過濾了）

                    var maxDid = tmp.Max(x => x.depositId);
                    var minDid = tmp.Min(x => x.depositId);
                    updateList = new List<Tuple<int, int, DateTime>>{
                        Tuple.Create(
                        maxDid,
                        minDid,
                        tmp.Where(x=>x.expireDate.HasValue).Min(x=>x.expireDate).Value
                    )};
                    //有部分點數扣成0就分區計算
                    if (tmp.Any(x => x.total == 0))
                    {
                        //重新計算中
                        updateList = new List<Tuple<int, int, DateTime>>();

                        var exDid = maxDid;
                        foreach (var item in tmp.Where(x => x.total == 0))
                        {
                            var did = item.depositId;
                            var minDate = tmp.Where(x => (x.depositId <= exDid && x.depositId >= did) && x.expireDate.HasValue)
                                .Min(x => x.expireDate).Value;

                            //Tuple定義請看上面
                            updateList.AddRange(
                                new List<Tuple<int, int, DateTime>>{
                        Tuple.Create(
                        did,
                        exDid,
                        minDate
                        )}
                            );
                            exDid = did;
                        }

                        updateList.AddRange(
                            new List<Tuple<int, int, DateTime>>{
                        Tuple.Create(
                        exDid,
                        minDid,
                        tmp.Where(x=>x.expireDate.HasValue && x.depositId>=exDid).Min(x=>x.expireDate).Value
                    )}
                        );
                    }
                    break;
            }

            var result = (from x in data
                          let did = x.DepositId != null ? string.Format("{0}", x.DepositId) : ""
                          let exDate = x.Total == 0 || did == "" ? "-" :
                          string.Format("{0:yyyy/MM/dd}",
                              updateList.Where(i => i.Item1 >= Convert.ToInt16(did) && Convert.ToInt16(did) >= i.Item2)
                              .Select(i => i.Item3).First()
                          )
                          select new PCPPointData
                          {
                              transId = x.WithdrawaloverallId == 0 ? string.Format("D{0}", did) : string.Format("W{0}", x.WithdrawaloverallId),
                              orderDate = x.CreateTime.ToString("yyyy/MM/dd"),
                              expireDate = exDate,
                              memo = x.Message,
                              pointIn = string.Format("{0:N0}", x.PointIn),
                              pointOut = string.Format("{0:N0}", x.PointOut),
                              total = string.Format("{0:N0}", x.Total),
                              creator = x.AccountId
                          }).OrderByDescending(x => x.orderDate).ToList();
            return result;
        }

        public static List<PCPRegularsTicket> GetRegularsTicketData(string account)
        {
            var discountCamp = op.DiscountCampaignGetByCreator(account);

            var result = (from x in discountCamp
                          select new PCPRegularsTicket
                          {
                              createTime = x.CreateTime.HasValue ? x.CreateTime.Value.ToString("yyyy/MM/dd") : "",
                              discountRange = string.Format("{0} - {1}",
                              x.StartTime.HasValue ? x.StartTime.Value.ToString("yyyy/MM/dd HH:mm") : "",
                              x.EndTime.HasValue ? x.EndTime.Value.ToString("yyyy/MM/dd HH:mm") : ""),
                              discountContent = string.Format("滿{0:N0}元，出示熟客券折抵{1:N0}元", x.MinimumAmount, x.Amount),
                              attention = string.Format("{0}", ""),
                              cardCombineUse = string.Format("{0}", x.CardCombineUse ? "可併用" : "不可併用"),
                              availableTime = string.Format("{0}", Helper.GetEnumDescription((AvailableDateType)x.AvailableDateType)),
                              status = string.Format("{0}", DateTime.Now > x.EndTime ? "已過期" : "可使用"),
                              creator = string.Format("{0}", x.CreateId),
                              ticketType = string.Format("{0}", Helper.GetEnumDescription((DiscountCampaignType)x.Type))
                          }).ToList();
            return result;
        }

        /// <summary>
        /// 熟客券＆公關券紀錄（使用於帳戶明細）
        /// </summary>
        /// <returns></returns>
        public static List<PCPTicketLog> GetTicketUsageData(string account, DiscountCampaignType t)
        {
            var discountCamp = pcp.ViewPcpOrderDiscountLogGetList(account, t);

            var result = (from x in discountCamp
                          select new PCPTicketLog
                          {
                              cardId = x.UserMemCardId.ToString(),
                              usageDate = x.OrderTime.ToString("yyyy/MM/dd"),
                              discountAmount = string.Format("{0:N0}", x.DiscountAmount),
                              sendDate = x.SendDate.HasValue ? x.SendDate.Value.ToString("yyyy/MM/dd") : "",
                              sequence = x.Code,
                              creator = x.OwnerName,
                              memo = string.Format("消費金額共{0:N0}元", x.OrderAmount)
                          }).ToList();
            return result;
        }

        public static List<PCPRegularsCard> GetRegularsCardData(Guid sellerGuid, int cardGroupId, int cardVersionId)
        {
            var memberShipCard = pcp.ViewMembershipCardGetListByGuid(sellerGuid);
            List<ViewMembershipCard> card = new List<ViewMembershipCard>();
            if (cardGroupId == 0 || cardVersionId <= 0)
            {
                card = memberShipCard.Where(x => Helper.IsTwoDateInSameQuarter(DateTime.Now, x.OpenTime)).ToList();
            }
            else
            {
                card = memberShipCard.Where(x => x.CardGroupId == cardGroupId && x.VersionId == cardVersionId).ToList();
            }

            var result = (from x in card
                          let verList = memberShipCard.Where(g => g.CardGroupId == x.CardGroupId && g.VersionId < x.VersionId).Select(g => g.VersionId)
                          select new PCPRegularsCard
                          {
                              //now = DateTime.Now.ToString("yyyy/MM/dd"),
                              cardLevel = x.Level.ToString(),
                              paymentPercent = (x.PaymentPercent * 10).ToString(),
                              startDate = x.OpenTime.ToString("yyyy/MM/dd"),
                              endDate = x.CloseTime.ToString("yyyy/MM/dd"),
                              others = x.Others ?? "",
                              instruction = x.Instruction ?? "",
                              availableDate = x.AvailableDateType.ToString(),
                              orderNeeded = x.OrderNeeded.ToString(),
                              amountNeeded = string.Format("{0:#}", x.AmountNeeded),
                              conditionLogic = x.ConditionalLogic.ToString(),
                              cardEnable = x.Enabled,
                              cardGroupId = x.CardGroupId.ToString(),
                              cardVersionId = x.VersionId.ToString(),
                              cardId = x.CardId,
                              isCurrent = DateTime.Now >= x.OpenTime && DateTime.Now <= x.CloseTime,
                              hasPre = memberShipCard.Any(g => g.CardGroupId == x.CardGroupId && g.VersionId <= x.VersionId),
                              exVersion = verList.Count() > 0 ? verList.Max().ToString() : "0",
                              statusDesc = Helper.GetEnumDescription((MembershipCardStatus)x.Status)
                          }).ToList();

            return result;
        }

        public static List<PCPTicketLog> GetRegularsCardHistory(int cardId)
        {
            var memberShipCardLog = mp.ViewMembershipCardLogGetListByCardId(cardId);

            //memberShipCard=memberShipCard.Where(x=>x.Status)
            var result = (from x in memberShipCardLog
                          select new PCPTicketLog
                          {
                              usageDate = x.CreateTime.ToString("yyyy/MM/dd HH:mm"),
                              memo = x.Memo,
                              creator = mp.MemberGet(x.CreateId).UserName
                          }
                          ).ToList();

            return result;
        }

        public static List<PCPMemberCardList> GetRegularsCardUserMember(int cardId)
        {
            var memberList = pcp.UserMembershipCardGetByCardId(cardId);
            var memberCard = pcp.MembershipCardGet(cardId);
            Func<int, string> s = x =>
            {
                return Helper.GetEnumDescription((MembershipCardLevel)Enum.ToObject(typeof(MembershipCardLevel), x));
            };
            var result = (from x in memberList
                          select new PCPMemberCardList
                          {
                              userCardId = x.Id.ToString(),
                              cardLevel = s.Invoke(memberCard.Level),
                              orderCount = x.OrderCount.ToString(),
                              amountTotal = x.AmountTotal.ToString("N0"),
                              updateDate = x.LastUseTime.HasValue ? x.LastUseTime.Value.ToString("yyyy/MM/dd HH:mm") : "",
                              userName = mp.MemberGet(x.UserId).UserName
                          }).ToList();

            return result;
        }

        public static int SetRegularsCardStatus(string userName, int cardId)
        {
            var memberCard = pcp.MembershipCardGet(cardId);

            if (!memberCard.IsLoaded) { return 0; }

            try
            {
                TransactionOptions tranOpts = new TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted
                };
                using (var trans = new TransactionScope(TransactionScopeOption.RequiresNew, tranOpts))
                {
                    memberCard.Enabled = !memberCard.Enabled;

                    var status = pcp.MembershipCardSet(memberCard);

                    MembershipCardLogCollection collectionLog = new MembershipCardLogCollection();
                    var cardLog = new MembershipCardLog();
                    cardLog.MembershipCardIdBefore = cardLog.MembershipCardIdAfter = cardId;
                    cardLog.Type = (int)MembershipCardLogType.Manual;
                    cardLog.Memo = string.Format("修改卡片狀態，前一個為{0}", memberCard.Enabled);
                    cardLog.CreateId = mp.MemberUniqueIdGet(userName);
                    cardLog.CreateTime = DateTime.Now;
                    collectionLog.Add(cardLog);
                    var log = mp.MembershipCardLogSetCollection(collectionLog);

                    if (log && status)
                    {
                        trans.Complete();
                        return 1;
                    }
                }
            }
            catch (Exception)
            {
            }

            return 0;
        }

        public static bool SetMemberShipCardStatus(int cardId)
        {
            var memberCard = pcp.MembershipCardGet(cardId);
            
            if (!memberCard.IsLoaded)
            {
                return false;
            }
            else
            {
                memberCard.Status = (int)MembershipCardStatus.Cancel;
                MembershipCard mcZero = new MembershipCard();
                if (memberCard.Level == 1)
                {
                    mcZero = pcp.MembershipCardGetByGroupId(memberCard.CardGroupId).FirstOrDefault(x => x.Level == 0) ?? new MembershipCard();
                    if (mcZero.IsLoaded)
                    {
                        mcZero.Status = (int)MembershipCardStatus.Cancel;
                    }
                }

                if (!mcZero.IsLoaded) return pcp.MembershipCardSet(memberCard);
                return pcp.MembershipCardSet(mcZero) && pcp.MembershipCardSet(memberCard);
            }


        }

        public static bool SetRegularsCardData(string creator, List<PCPRegularsCard> original, List<PCPRegularsCard> changed)
        {
            MembershipCardLogCollection collectionLog = new MembershipCardLogCollection();

            var cardLog = new MembershipCardLog();

            var before = original.Where(x =>
                x.cardId == changed.FirstOrDefault().cardId &&
                x.cardLevel == changed.FirstOrDefault().cardLevel
                ).FirstOrDefault();

            var after = changed.FirstOrDefault();

            if (before == null || after == null) { return false; }

            cardLog.MembershipCardIdBefore = cardLog.MembershipCardIdAfter = after.cardId;
            cardLog.Type = (int)MembershipCardLogType.Manual;
            cardLog.CreateId = mp.MemberUniqueIdGet(creator);
            cardLog.CreateTime = DateTime.Now;

            foreach (var item in typeof(PCPRegularsCard).GetProperties())
            {
                var changedItem = cardLog.Clone();
                string msg = "";
                //after
                var a = after.GetType().GetProperty(item.Name).GetValue(after, null);
                //before
                var b = before.GetType().GetProperty(item.Name).GetValue(before, null);
                if (a != null && b != null && !a.Equals(b))
                {
                    switch (item.Name)
                    {
                        case "paymentPercent":
                            msg = string.Format("優惠折數由 {0} 折調整為 {1} 折", before.paymentPercent, after.paymentPercent);
                            break;
                        case "others":
                            msg = string.Format("額外加贈由 {0} 調整為 {1} ", before.others, after.others);
                            break;
                        case "instruction":
                            msg = string.Format("注意事項由 {0} 調整為 {1} ", before.instruction, after.instruction);
                            break;
                        case "orderNeeded":
                            msg = string.Format("累積消費次數由 {0} 調整為 {1} ", before.orderNeeded, after.orderNeeded);
                            break;
                        case "amountNeeded":
                            msg = string.Format("累積消費金額由 {0} 調整為 {1} ", before.amountNeeded, after.amountNeeded);
                            break;
                        case "availableDate":
                            msg = string.Format("使用日期由 {0} 調整為 {1} ", before.availableDate, after.availableDate);
                            break;
                        default:
                            continue;
                    }
                    changedItem.Memo = msg;
                    collectionLog.Add(changedItem);
                }
            }

            var logset = mp.MembershipCardLogSetCollection(collectionLog);

            var ssMemberCard = pcp.MembershipCardGet(before.cardId);

            ssMemberCard.PaymentPercent = Convert.ToDouble(after.paymentPercent) / 10;
            ssMemberCard.Others = after.others;
            ssMemberCard.Instruction = after.instruction;
            ssMemberCard.OrderNeeded = Helper.ParseToMyType<int?>(after.orderNeeded);
            ssMemberCard.AmountNeeded = Helper.ParseToMyType<int?>(after.amountNeeded);
            ssMemberCard.AvailableDateType = int.Parse(after.availableDate);

            bool updateResault = true;

            updateResault &= pcp.MembershipCardSet(ssMemberCard);

            updateResault &= BonusFacade.MembershipCardUpdateSynchronizationData(ssMemberCard.CardGroupId, ssMemberCard.VersionId, (AvailableDateType)ssMemberCard.AvailableDateType, ssMemberCard.CombineUse);

            return updateResault;
        }

        public static List<PCPFitStores> GetRegularsCardStoreData(string cardGroupId)
        {
            var stores = sp.StoreGetListByPCPmemberCard(cardGroupId);

            var result = (from x in stores
                          select new PCPFitStores
                          {
                              storeName = x.SellerName,
                              phone = x.StoreTel,
                              address = x.StoreAddress,
                              storeGuid = x.Guid
                          }).ToList();

            return result;
        }


        public static List<PCPPointData> GetSuperBonusData(int userId)
        {
            var data = pcp.ViewPcpMemberPromotionSuperBonuxGetList(userId);

            var result = (from x in data
                          select new PCPPointData
                          {
                              transId = x.UserMemCardId == 0 ? "-" : x.UserMemCardId.ToString(),
                              orderDate = x.CreateTime.ToString("yyyy/MM/dd"),
                              pointOut = string.Format("{0:N0}", x.ValueOut == 0 ? x.ValueIn : x.ValueOut),
                              total = string.Format("{0:N0}", x.Total),
                              memo = string.Format("{0}", x.Action),
                              creator = x.AccountId
                          }).ToList();
            return result;
        }

        public static bool UploadPcpImage(int groupId, int userId, Image img, int imageSequence, PcpImageType imgType, bool isInsert = false)
        {
            var result = false;
            var ticks = DateTime.Now.Ticks.ToString();
            var fileName = "";
            var mcg = pcp.MembershipCardGroupGet(groupId);
            var compressRateValue = 1.0;
            int suggestWidth = 0, suggestHeight = 0;

            switch (imgType)
            {
                case PcpImageType.Logo:
                    //CutForCustom(Image initImage, int maxWidth, int maxHeight, int quality, bool isCutCenter)
                    suggestWidth = 1360;
                    suggestHeight = 756;
                    break;
                case PcpImageType.Card:
                    //CutForCustom(Image initImage, int maxWidth, int maxHeight, int quality, bool isCutCenter)
                    suggestWidth = 1360;
                    suggestHeight = 756;
                    break;
                case PcpImageType.CorporateImage:
                    //CutForCustom(Image initImage, int maxWidth, int maxHeight, int quality, bool isCutCenter)
                    suggestWidth = 640;
                    suggestHeight = 428;
                    break;
                case PcpImageType.Service:
                case PcpImageType.EnvironmentAroundStore:
                    suggestWidth = 1440;
                    suggestHeight = 2560;
                    break;
            }

            int imgWidth = 0, imgHeight = 0;
            //橫的
            if (img.Width > img.Height)
            {
                imgWidth = img.Height;
                imgHeight = img.Width;
            }
            //直的
            else
            {
                imgWidth = img.Width;
                imgHeight = img.Height;
            }

            if (imgWidth > suggestWidth || imgHeight > suggestHeight)
            {
                compressRateValue = Math.Min(suggestWidth / (float)imgWidth, suggestHeight / (float)imgHeight);
            }

            //單純壓縮圖，不加底圖
            using (var compressedMs = new MemoryStream())
            {
                using (var compressedImg = ImageFacade.CompressImage(img, compressRateValue, compressedMs))
                {
                    string destPath = Convert.ToString(groupId);
                    ImageUtility.UploadFile(null, UploadFileType.PCPImage, destPath, ticks, false, compressedImg);
                    fileName = string.Format("{0}.{1}", ticks, Helper.GetExtensionByContentType(ImageUtility.GetImageMimeType(compressedImg)));
                }
            }

            var imagePath = ImageFacade.GenerateMediaPath(mcg.Id.ToString(), fileName);
            var pcpImage = new PcpImage
            {
                SellerUserId = mcg.SellerUserId,
                ImageType = (byte)imgType,
                CreateTime = DateTime.Now,
                ImageUrl = imagePath,
                IsDelete = false,
                ModifyTime = DateTime.Now,
                ModifyUserId = userId,
                Sequence = imageSequence,
                GroupId = groupId
            };

            if (isInsert == true)//不蓋圖，往後排序
            {
                var images = pcp.PcpImageGetByCardGroupId(groupId, imgType);
                images.Insert(imageSequence - 1, pcpImage);

                //重排序
                int index = 1;
                foreach (var image in images)
                {
                    image.Sequence = index;
                    index++;
                }

                result = pcp.PcpImageSet(images);
            }
            else
            {
                //delete exist
                DeletePcpImage(userId, mcg.Id, imgType, imageSequence);
                result = pcp.PcpImageSet(pcpImage);
            }

            #region 圖檔壓縮

            CompressImage(pcpImage);

            #endregion

            return result;
        }

        public static int GetPcpImageId(int groupId, int imageSequence, PcpImageType imgType)
        {
            int result = 0;
            var imgs = pcp.PcpImageGetByCardGroupId(groupId, imgType, imageSequence);

            if (imgs.Any())
            {
                result = imgs.First().Id;
            }

            return result;
        }

        private static bool DeletePcpImage(int modifyUserId, int groupId, PcpImageType imageType, int seq)
        {
            var existImage = pcp.PcpImageGetByCardGroupId(groupId, imageType, seq);
            if (existImage.Any())
            {
                existImage.ForEach(x =>
                {
                    x.IsDelete = true;
                    x.ModifyTime = DateTime.Now;
                    x.ModifyUserId = modifyUserId;
                });

                return pcp.PcpImageSet(existImage);
            }
            return true;
        }

        public static void CompressImage()
        {
            var queryTypes = new int[] {
                (int)PcpImageType.Card
                , (int)PcpImageType.EnvironmentAroundStore
                , (int)PcpImageType.Service };

            var data = pcp.PcpImageGetUnCompress(queryTypes);

            foreach (var item in data)
            {
                if (item.ImageUrl.Replace(".", "s.").Equals(item.ImageUrlCompressed))
                {
                    continue;
                }
                CompressImage(item);
            }
        }

        private static void CompressImage(PcpImage pcpImage)
        {
            //取得需要壓縮的 image_type 與倍率存到 Dictionary
            var compressRate = ImageFacade.GetPcpImageCompressRate();
            var image = pcpImage.ImageUrl.Split(',')[1];
            var ticks = image.Split('.')[0];
            var compressFileName = string.Format("{0}s.{1}", ticks, image.Split('.')[1]);


            var imageUrl = ImageFacade.GetMediaPath(pcpImage.ImageUrl, MediaType.PCPImage);
            var imagePath = ConverWebToLocalPath(imageUrl);
            if (!File.Exists(imagePath))
            {
                return;
            }

            var img = Image.FromFile(imagePath);

            if (compressRate.Count > 0 && compressRate.Keys.Contains((int)pcpImage.ImageType))
            {
                double compressRateValue = compressRate.First(x => x.Key == pcpImage.ImageType).Value;

                using (MemoryStream compressedMs = new MemoryStream())
                {
                    //switch ((PcpImageType)pcpImage.ImageType)
                    //{
                    //    case PcpImageType.Service:
                    //    case PcpImageType.EnvironmentAroundStore:
                    //        //CutForCustom(Image initImage, int maxWidth, int maxHeight, int quality, bool isCutCenter)
                    //        int suggestWidth = 1360, suggestHeight = 756;
                    //        int imgWidth = img.Width, imgHeight = img.Height;

                    //        if (imgWidth > suggestWidth || imgHeight > suggestHeight)
                    //        {
                    //            var minEdge = Math.Min(imgWidth, imgHeight);
                    //            compressRateValue = Math.Min(imgWidth / suggestWidth, imgHeight / suggestHeight);
                    //        }
                    //        break;
                    //}

                    using (var compressedImg = ImageFacade.CompressImage(img, compressRateValue, compressedMs))
                    {
                        ImageUtility.UploadFile(null, UploadFileType.PCPImage, pcpImage.SellerUserId.ToString()
                            , string.Format("{0}s", ticks), false, compressedImg);
                    }
                }
                pcpImage.ImageUrlCompressed = ImageFacade.GenerateMediaPath(pcpImage.SellerUserId.ToString(), compressFileName);
                pcp.PcpImageSet(pcpImage);
            }
        }

        private static string ConverWebToLocalPath(string path)
        {
            string result = "";

            var relateUrl = string.Join("\\", path.Split('/').Skip(3).ToArray());
            result = Path.Combine(HttpRuntime.AppDomainAppPath, relateUrl);

            return result;
        }

        public static bool SellerIntroSet(int cardGroupId, int sellerUserId, string[] intro)
        {
            pcp.PcpIntroDelete(PcpIntroType.Seller, cardGroupId);
            var introCol = new PcpIntroCollection();
            introCol.AddRange(
                intro.Select(x => new PcpIntro
                {
                    IntroType = (byte)PcpIntroType.Seller,
                    KeyValue = sellerUserId,
                    IntroContent = x,
                    CreateTime = DateTime.Now,
                    GroupId = cardGroupId
                }));
            return pcp.PcpIntroSet(introCol);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core
{
    [AttributeUsage(AttributeTargets.Method)]
    public class VbsAuthorizeAttribute : System.Web.Mvc.AuthorizeAttribute
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public bool RequiresInspectionCode { get; set; }
        
        public VbsAuthorizeAttribute()
        {
            RequiresInspectionCode = false;
        }

        protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }

            if(!AuthCookieExists(httpContext))
            {
                return false;
            }
            
            return true;
        }

        public bool AuthCookieExists(System.Web.HttpContextBase httpContext)
        {
            for (int i = 0; i < httpContext.Request.Cookies.Count; i++)
            {
                HttpCookie cookie = httpContext.Request.Cookies.Get(i);
                if (cookie.Path == GetVbsAuthCookiePath() && cookie.Name == ".ASPXAUTH")
                {
                    return true;
                }
            }
			
            return false;
        }

        public bool SessionDataExists(HttpContextBase httpContext)
		{
			return httpContext.Session[VbsSession.LoginSessionCheck.ToString()] != null;
		}

        public bool SessionDataCheckError(HttpContextBase httpContext)
        {
            /*
             * 於cookie及session資料存在的狀況下 若 Session[Is17LifeEmployee] 為空
             * 或Session[UserId]被前台Session[UserId]覆蓋為Member.UniqueId 的狀況下
             * 為避免使用異常 一律以重新登入處理 重整Session資料
            */
            int dummy;
            return httpContext.Session[VbsSession.Is17LifeEmployee.ToString()] == null ||
                   int.TryParse(httpContext.Session[VbsSession.UserId.ToString()].ToString(), out dummy);
        }

        public override void OnAuthorization(System.Web.Mvc.AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            //if(!filterContext.HttpContext.User.Identity.IsAuthenticated)
            if(!AuthCookieExists(filterContext.HttpContext) || !SessionDataExists(filterContext.HttpContext) || SessionDataCheckError(filterContext.HttpContext))
            {
                filterContext.Result = GetLoginRedirectResult();
                return;
            }

            //檢查是否為 第一次登入 或 超過3個月未變更密碼
            if (filterContext.HttpContext.Session[VbsSession.FirstLogin.ToString()].Equals(true) ||
                filterContext.HttpContext.Session[VbsSession.IsPwdPassTimeLimit.ToString()].Equals(true))
            {
                if (filterContext.RouteData.Values["action"].ToString() != "SetPassword")
                    filterContext.Result = GetSetPasswordRedirectResult();
                return;
            }

            //檢查是否 未變更過查帳碼
            if (filterContext.HttpContext.Session[VbsSession.UseInspectionCode.ToString()].Equals(true) &&
                filterContext.HttpContext.Session[VbsSession.IsInspectionCodeNeverChange.ToString()].Equals(true))
            {
                if (filterContext.RouteData.Values["action"].ToString() != "SetInspectionCode")
                    filterContext.Result = GetSetInspectionCodeRedirectResult();
                return;
            }
           
            if (RequiresInspectionCode)
            {
                if (filterContext.HttpContext.Session[VbsSession.InspectionRight.ToString()] != null)  
                {
                    // 已經驗證過查帳碼, 授權通過.
                    return;
                }                

                if (filterContext.HttpContext.Session[VbsSession.UseInspectionCode.ToString()].Equals(true))
                {
                    if (config.IsEnableVbsNewUi)
                    {
                        string deliveryType = string.Empty;
                        if (filterContext.HttpContext.Session[VbsSession.DeliveryType.ToString()] != null)
                        {
                            filterContext.Result = GetRedirectToInspectionCodeLoginResultForNewVbs(filterContext.HttpContext.Session[VbsSession.DeliveryType.ToString()].ToString());
                        }
                        else
                        {
                            filterContext.Result = GetRedirectToInspectionCodeLoginResult();
                        }
                    }
                    else
                    {
                        filterContext.Result = GetRedirectToInspectionCodeLoginResult();
                    }
                    
                    return;
                }
                else
                {
                    if (filterContext.HttpContext.Session != null)
                    {
                        filterContext.HttpContext.Session.Add(VbsSession.InspectionRight.ToString(), new object());
                    }
                    else
                    {
                        throw new SystemException("unable to apply inspection right to user!");
                    }
                }
            }
        }
        
        private ActionResult GetLoginRedirectResult()
        {
            // 用 Global.asax 定義的路徑
            return new RedirectToRouteResult("VendorBillingSystemDefault", new RouteValueDictionary
                                                                     {
                                                                         {"action", "Login"}
                                                                     });
        }

        private ActionResult GetSetPasswordRedirectResult()
        {
            // 用 Global.asax 定義的路徑
            return new RedirectToRouteResult("VendorBillingSystemDefault", new RouteValueDictionary
                                                                     {
                                                                         {"action", "SetPassword"}
                                                                     });
        }

        private ActionResult GetSetInspectionCodeRedirectResult()
        {
            // 用 Global.asax 定義的路徑
            return new RedirectToRouteResult("VendorBillingSystemDefault", new RouteValueDictionary
                                                                     {
                                                                         {"action", "SetInspectionCode"}
                                                                     });
        }

        /// <summary>
        /// 導到查帳碼驗證頁
        /// </summary>
        /// <returns></returns>
        private ActionResult GetRedirectToInspectionCodeLoginResult()
        {
            return new RedirectToRouteResult("VendorBillingSystemDefault", new RouteValueDictionary
                                                                    {
                                                                        {"action", "InspectionCodeLogin"}
                                                                    });
        }

        private ActionResult GetRedirectToInspectionCodeLoginResultForNewVbs(string deliveryType)
        {
            return new RedirectToRouteResult("VendorBillingSystemDefault", new RouteValueDictionary
                                                                    {
                                                                        {"action", "InspectionCodeLogin"}, { "d", deliveryType}
                                                                    });
        }

        private string GetVbsAuthCookiePath()
        {
            if (string.Equals("/", HttpContext.Current.Request.ApplicationPath))
            {
                return "/vbs";
            }
            else
            {
                return HttpContext.Current.Request.ApplicationPath + "/vbs";
            }
        }

    }
}

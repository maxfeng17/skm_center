﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.WebLib.Component
{
    public class OrderUtility
    {
        protected static IMemberProvider mp;
        protected static IOrderProvider op;
        protected static ILocationProvider lp;
        protected static ISysConfProvider config;
        protected static IPponProvider pp;
        protected static IEventProvider ep;
        protected static IHiDealProvider hp;
        protected static IHumanProvider hump;
        protected static ISystemProvider systemp;
        protected static ILog log;

        static OrderUtility()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            config = ProviderFactory.Instance().GetConfig();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            hump = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            systemp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            log = LogManager.GetLogger(typeof(MemberFacade));
        }

        /// <summary>
        /// 將訂單資料中，傳入序號的憑證，發送簡訊到消費者輸入的手機中，當選擇同步異動手機號碼時，將選擇的手機號碼修改到會員資料中
        /// </summary>
        /// <param name="orderGuid">訂單編號</param>
        /// <param name="sequenceNumList">憑證序號</param>
        /// <param name="mobile">手機號碼</param>
        /// <param name="userId">會員編號</param>
        /// <param name="changePhone">是否同時修改手機號碼</param>
        /// <returns></returns>
        public static int SendOrderPponCouponSMS(Guid orderGuid, List<string> sequenceNumList, string mobile, int userId, bool changePhone)
        {
            int rtn = 0;
            Member member = mp.MemberGet(userId);
            //同步修改會員手機號碼到會員記錄的資料
            if (changePhone)
            {
                member.Mobile = mobile;
                mp.MemberSet(member);
            }

            var couponList = pp.ViewPponCouponGetList(0, 0, null, ViewPponCoupon.Columns.OrderGuid + "=" + orderGuid,
                                     ViewPponCoupon.Columns.MemberEmail + "=" + member.UserName);

            Guid bid = Guid.Empty;
            SmsContent sc = null;
            foreach (var sequenceNum in sequenceNumList)
            {
                List<ViewPponCoupon> matchCoupons = couponList.Where(x => x.SequenceNumber == sequenceNum).ToList();
                if (matchCoupons.Count > 0)
                {
                    ViewPponCoupon coupon = matchCoupons.First();
                    if (coupon.CouponId != null)
                    {
                        if ((bid == Guid.Empty) || (bid != coupon.BusinessHourGuid))
                        {
                            sc = pp.SMSContentGet(coupon.BusinessHourGuid);
                            bid = coupon.BusinessHourGuid;
                        }
                        try
                        {
                            SMS sms = new SMS();
                            if (coupon.OrderDetailId != null)
                            {
                                string msg = GetCouponSMSMessage(sc, coupon);
                                sms.SendMessage("", msg, "", mobile, member.UserName, Core.SmsType.Ppon, coupon.BusinessHourGuid, coupon.CouponId.Value.ToString());
                                rtn++;
                            }
                        }
                        catch (Exception e)
                        {
                            log.Error(string.Format("發送會員訂單憑證至EMAIL {0}", e.ToString()));
                            continue;
                        }
                    }
                }
            }
            return rtn;
        }

        private static string GetCouponSMSMessage(SmsContent content, ViewPponCoupon coupon)
        {
            string msg = string.Empty;
            if (coupon.OrderDetailId != null && coupon.BusinessHourDeliverTimeS != null)
            {
                string[] msgs = content.Content.Split('|');
                string branch = OrderFacade.SmsGetPhone(coupon.OrderDetailId.Value);  //分店資訊 & 多重選項
                DateTime finalExpireDate = GetFinalExpireDate(coupon.OrderGuid);
                msg = string.Format(msgs[0] + msgs[1] + branch + msgs[2], coupon.SequenceNumber,
                                           coupon.CouponCode, coupon.BusinessHourDeliverTimeS.Value.ToString("yyyyMMdd"),
                                           finalExpireDate.ToString("yyyyMMdd"),
                                           (coupon.CouponStoreSequence != null && coupon.CouponStoreSequence != 0)
                                               ? "，序號" + coupon.CouponStoreSequence.Value.ToString("0000")
                                               : string.Empty);
            }
            return msg;
        }

        private static DateTime GetFinalExpireDate(Guid orderGuid)
        {
            ExpirationDateSelector selector = GetExpirationComponent(orderGuid);

            if (selector != null)
            {
                return selector.GetExpirationDate();
            }
            return new DateTime(1900, 1, 1);
        }

        private static Dictionary<Guid, ExpirationDateSelector> expirationComponents = new Dictionary<Guid, ExpirationDateSelector>();

        public static ExpirationDateSelector GetExpirationComponent(Guid orderGuid)
        {
            if (expirationComponents.ContainsKey(orderGuid))
            {
                return expirationComponents[orderGuid];
            }

            ExpirationDateSelector selector = new ExpirationDateSelector();
            selector.ExpirationDates = new PponUsageExpiration(orderGuid);

            expirationComponents.Add(orderGuid, selector);
            return selector;
        }
    }
}
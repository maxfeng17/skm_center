using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Component
{
    public class OrderDispatcher
    {
        #region protected members
        protected static OrderDispatcher _theOne = null;
        protected static IOrderProvider op = null;
        #endregion

        #region .ctor
        static OrderDispatcher()
        {
            _theOne = new OrderDispatcher();
        }

        /// <summary>
        /// Returns a reference to the current postman instance
        /// </summary>
        /// <returns></returns>
        public static OrderDispatcher Instance()
        {
            return _theOne;
        }

        private OrderDispatcher()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }
        #endregion

        public ViewOrderMemberBuildingSeller GetOrder(string userName)
        {
            ViewOrderMemberBuildingSeller view = op.ViewOrderMemberBuildingSellerGet(OrderStatus.Closed | OrderStatus.Locked | OrderStatus.Complete, OrderStatus.Complete, Order.Columns.DeliveryTime);
            if (!view.IsLoaded)
                view.OrderGuid = Guid.Empty;

            return view;
        }
    }
}

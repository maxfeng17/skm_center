﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component.MemberActions;
using LunchKingSite.WebLib.Models;
using LunchKingSite.WebLib.Models.VendorBillingSystem;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;

namespace LunchKingSite.WebLib.Component
{
    public class VbsMemberUtility
    {
        protected static ISellerProvider sp;
        protected static IMemberProvider mp;
        protected static VbsMembershipProvider membership;
        protected static ILog logger = LogManager.GetLogger("vbsRegister");
        static VbsMemberUtility()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();        
            membership = Membership.Providers["VbsMembershipProvider"] as VbsMembershipProvider;
            if (membership == null)
            {
                membership = new VbsMembershipProvider();
            }
        }

        private static Regex regPass = new Regex(@"^[0-9a-zA-Z@#$%^&+=_-]{6,20}$", RegexOptions.Compiled);
        private static Regex regInspectionCode = new Regex(@"^[0-9a-zA-Z@#$%^&+=_-]{6,20}$", RegexOptions.Compiled);

        public static bool CheckPassword(string password)
        {
            if (regPass.Match(password).Success)
            {
                return true;
            }

            return false;
        }

        public static bool CheckInspectionCode(string inspectionCode)
        {
            if (regInspectionCode.Match(inspectionCode).Success)
            {
                return true;
            }
            return false;
        }

        public static MembershipCreateStatus RegistersAccount(NewMemberModelParser mem)
        {
            VbsMembershipAccountType accountType = mem.AccountType;
            string inspectionCode = mem.InspectionCode;
            bool useInspectionCode = mem.UseInspectionCode;

            if (membership == null)
            {
                return MembershipCreateStatus.ProviderError;
            }

            if (!CheckPassword(mem.Password))
            {
                return MembershipCreateStatus.InvalidPassword;
            }

            if (accountType == VbsMembershipAccountType.VendorAccount && useInspectionCode)
            {
                if (!CheckInspectionCode(inspectionCode))
                {
                    return MembershipCreateStatus.InvalidAnswer;
                }
            }

            if (accountType == VbsMembershipAccountType.VerificationAccount || accountType == VbsMembershipAccountType.VendorAccount)
            {
                var vbsmember = membership.CreateCompanyVerificationAccount(mem);

                if (vbsmember == MembershipCreateStatus.Success)
                {
                    //PCP專案 新增member link
                    IMemberRegister reg = new VbsMemberRegister(mem);
                    MemberRegisterReplyType reply = reg.Process(false);

                    if (reply != MemberRegisterReplyType.RegisterSuccess)
                    {
                        logger.ErrorFormat("VBS CreateMemberLink fail. org=({0},exid={1}), ex: {2}",
                    SingleSignOnSource.VbsMember, mem.AccountId, reply);
                        return MembershipCreateStatus.ProviderError;
                    }
                }

                return vbsmember;
            }
            else
            {
                return MembershipCreateStatus.UserRejected;
            }
        }

        private static void VbsUserSetAuthCookie(string accountId)
        {
            FormsAuthentication.SetAuthCookie(accountId, false, GetVbsAuthCookiePath());
        }

        /// <summary>
        /// for mobile device
        /// </summary>
        public static bool UserSignIn(string accountId, string password)
        {
            /*
             * Originally, this method is used by both Web and mobile device.
             * Now, Web uses UserSignIn(string accountId, string password, HttpContextBase httpContext).
             * So, instead of return a boolean that only checks if the password is correct,
             * consider moving code inside VerificationService.asmx.cs here and return ApiReturnObject.
             */

            return membership.ValidateUser(accountId, password);
        }

        /// <summary>
        /// Due to security reasons, Web.config secures all cookies (except for default forms authentication cookie),
        /// which causes VBS login and session issues.
        /// To solve these issues, HTTPS is used over the entire VBS system.
        /// </summary>
        public static bool UserSignIn(string accountId, string password, HttpContextBase httpContext)
        {
            if (membership.ValidateUser(accountId, password))
            {
                SetCookie(accountId, httpContext);
                return true;
            }
            return false;
        }

        public static bool UserSignInByEncryptPassword(string accountId, string encryptPassword, HttpContextBase httpContext)
        {
            if (membership.ValidateUserByEncryptPassword(accountId, encryptPassword))
            {
                SetCookie(accountId, httpContext);
                return true;
            }
            return false;
        }

        public static void AppVbsUserSignIn(string accountId)
        {
            VbsUserSetAuthCookie(accountId);
        }

        #region Demo Mode

        public static bool DemoUserSignIn(string accountId, string password)
        {
            if (!string.IsNullOrEmpty(accountId))
            {
                accountId = accountId.ToUpper();
            }

            if (accountId.Equals("DEMONO0001") || accountId.Equals("DEMONO0002"))
            {
                if (password.Equals("123456") || password.Equals(VbsCurrent.DemoUser.Password))
                {
                    VbsUserSetAuthCookie(accountId);
                    return true;
                }
            }
            return false;
        }

        #endregion Demo Mode

        public static bool ChangePassword(string accountId, string oldPassword, string newPassword)
        {
            return membership.ChangePassword(accountId, oldPassword, newPassword);
        }

        public static bool ChangeInspectionCode(string accountId, string oldInspectionCode, string newInspectionCode)
        {
            return membership.ChangeInspectionCode(accountId, oldInspectionCode, newInspectionCode);
        }

        public static bool AdministrativeChangePassword(string accountId, string password)
        {
            VbsMembership member = mp.VbsMembershipGetByAccountId(accountId);
            if (member != null && member.IsLoaded && CheckPassword(password))
            {
                VbsMembershipPasswordFormat passwordFormat;
                member.Password = membership.GetFormattedPassword(password, out passwordFormat);
                member.LastPasswordChangedDate = null;
                member.PasswordFormat = (int)passwordFormat;
                mp.VbsMembershipSet(member);
                return true;
            }
            return false;
        }

        public static bool AdministrativeChangeInspectionCode(string accountId, string inspectionCode)
        {
            VbsMembership member = mp.VbsMembershipGetByAccountId(accountId);
            if (member != null && member.IsLoaded && CheckInspectionCode(inspectionCode))
            {
                VbsMembershipPasswordFormat dummyFormat;
                member.InspectionCode = membership.GetFormattedPassword(inspectionCode, out dummyFormat);
                member.LastInspectionCodeChangedDate = null;
                mp.VbsMembershipSet(member);
                return true;
            }
            return false;
        }

        public static bool VerifyPassword(string accountId, string password)
        {
            return membership.VerifyPassword(accountId, password);
        }

        public static bool VerifyInspectionCode(string accountId, string inspectionCode)
        {
            return membership.VerifyInspectionCode(accountId, inspectionCode);
        }

        public static string GetVbsAuthCookiePath()
        {
            //if this method changes,
            //modify the method GetVbsAuthCookiePath inside LunchKingSite.Core.VbsAuthorizeAttribute also.
            // **Core cannot add reference to WebLib

            if (string.Equals("/", HttpContext.Current.Request.ApplicationPath))
            {
                return "/vbs";
            }
            else
            {
                return HttpContext.Current.Request.ApplicationPath + "/vbs";
            }
        }

        public static VbsMembership GetVbsMemberShip(int id)
        {
            return mp.VbsMembershipGetById(id);
        }

        public static VbsMembership GetVbsMemberShipByUserId(int userId)
        {
            return mp.VbsMembershipGetByUserId(userId);
        }

        #region view_vbs_Member_Detail

        public static MemberDetail ViewVbsMemberDetailGet(int userId)
        {
            var viewVbsMemberDetails = sp.ViewVbsMemberDetailGet(userId);
            var memberDetail = new MemberDetail();

            if (viewVbsMemberDetails.Count <= 0) return memberDetail;

            memberDetail.LoginId = viewVbsMemberDetails[0].LoginId;
            memberDetail.Email = viewVbsMemberDetails[0].Email ?? string.Empty;
            memberDetail.Mobile = viewVbsMemberDetails[0].Mobile ?? string.Empty;
            memberDetail.InvoiceTitle = viewVbsMemberDetails[0].InvoiceTitle ?? string.Empty;
            memberDetail.InvoiceComId = viewVbsMemberDetails[0].InvoiceComId ?? string.Empty;
            memberDetail.InvoiceName = viewVbsMemberDetails[0].InvoiceName ?? string.Empty;
            memberDetail.InvoiceAddress = viewVbsMemberDetails[0].InvoiceAddress ?? string.Empty;
            memberDetail.AccountBankCode = viewVbsMemberDetails[0].AccountBankCode ?? string.Empty;
            memberDetail.AccountBranchCode = viewVbsMemberDetails[0].AccountBranchCode ?? string.Empty;
            memberDetail.AccountName = viewVbsMemberDetails[0].AccountName ?? string.Empty;
            memberDetail.AccountId = viewVbsMemberDetails[0].AccountId ?? string.Empty;
            memberDetail.AccountNo = viewVbsMemberDetails[0].AccountNo ?? string.Empty;

            #region 發票地址的cityId與tonwshipId的處理
            memberDetail.CityId = null;
            memberDetail.TownshipId = null;
            if (viewVbsMemberDetails[0].TownshipId.HasValue)
            {
                var township = CityManager.TownShipGetById(viewVbsMemberDetails[0].TownshipId.Value);
                if (township != null)
                {
                    memberDetail.CityId = township.ParentId.HasValue ? township.ParentId.Value : 0;
                    memberDetail.TownshipId = township.Id;
                }
            }
            #endregion 發票地址的cityId與tonwshipId的處理

            memberDetail.PasswordChangedDesc = viewVbsMemberDetails[0].LastPasswordChangedDate.HasValue ?
                GetLastChangedPasswordTimeDescription(viewVbsMemberDetails[0].LastPasswordChangedDate.Value):string.Empty;

            return memberDetail;
        }

        #endregion

        #region vbs_company_detail

        public static bool VbsCompanyDetailSet(MemberDetail memberDetail)
        {
            var vbsMembership = mp.VbsMembershipGetByAccountId(memberDetail.LoginId);

            if (!vbsMembership.IsLoaded || !vbsMembership.UserId.HasValue) return false;

            VbsCompanyDetail vcd = sp.VbsCompanyDetailGet(memberDetail.SellerGuid);

            if (!vcd.IsLoaded)
            {
                //新增
                vcd.UserId = vbsMembership.UserId.Value;
            }

            vcd.Email = memberDetail.Email;
            vcd.Mobile = memberDetail.Mobile;
            vcd.InvoiceTitle = memberDetail.InvoiceTitle;
            vcd.InvoiceComId = memberDetail.InvoiceComId;
            vcd.InvoiceName = memberDetail.InvoiceName;
            vcd.InvoiceAddress = memberDetail.InvoiceAddress;
            vcd.AccountBankCode = memberDetail.AccountBankCode;
            vcd.AccountBranchCode = memberDetail.AccountBranchCode;
            vcd.AccountName = memberDetail.AccountName;
            vcd.AccountId = memberDetail.AccountId;
            vcd.AccountNo = memberDetail.AccountNo;
            vcd.TownshipId = memberDetail.TownshipId;

            return sp.VbsCompanyDetailSet(vcd);
        }
        public static VbsCompanyDetail VbsCompanyDetailGet(Guid sellerGuid)
        {
            return sp.VbsCompanyDetailGet(sellerGuid);
        }
        #endregion


        public static VbsMembershipCollection GetManageVender(int type, string account)
        {
            VbsMembership r = new VbsMembership();

            VbsMembershipCollection members = new VbsMembershipCollection();
            List<VbsMembershipAccountType> accountTypes = new List<VbsMembershipAccountType>{ VbsMembershipAccountType.VendorAccount };
            
            switch (type)
            {
                case (int)ManageVendorSearchOption.AccountId:
                    members = mp.VbsMembershipGetListLikeAccountId(account, accountTypes, true);
                    break;

                case (int)ManageVendorSearchOption.AccountName:
                    members = mp.VbsMembershipGetListByAccountName(account, accountTypes, true);
                    break;
            }

            return members;
        }

        public static void VendorAccountForceSignIn(VbsMembership mem, HttpContextBase httpContext, string simulateUserName)
        {
            if (HttpContext.Current == null)
            {
                throw new Exception(string.Format("登入資訊逾時或異常，請重新登入"));
            }

            if (mem.IsLoaded == false || mem.IsApproved == false || mem.IsLockedOut)
            {
                var userName = mem.AccountId;
                mp.VbsAccountAuditSet(mem.Id, mem.AccountId, Helper.GetClientIP(), VbsAccountAuditAction.LoginBy17LifeSimulate, false, false, "login by " + simulateUserName);
                if (mem.IsLoaded == false)
                {
                    throw new Exception(string.Format("帳號 {0} 不存在，請再確認。", userName));
                }
                if (mem.IsApproved == false)
                {
                    throw new Exception(string.Format("帳號 {0} 不被允許，請通知IT處理", userName));
                }
                if (mem.IsLockedOut)
                {
                    throw new Exception(string.Format("帳號 {0} 被鎖定，請檢查鎖定狀況", userName));
                }
                if (mem.AccountType != (int)VbsMembershipAccountType.VendorAccount)
                {
                    throw new Exception(string.Format("帳號 {0} 非商家帳號，不提供模擬功能", userName));
                }
            }

            mp.VbsAccountAuditSet(mem.Id, mem.AccountId, Helper.GetClientIP(), VbsAccountAuditAction.LoginBy17LifeSimulate, true, false, "login by " + simulateUserName);
            SetCookie(mem.AccountId, httpContext);
        }

        /// <summary>
        /// 最後一次修改密碼時間的描述
        /// </summary>
        /// <param name="lastChangeTime"></param>
        /// <returns></returns>
        private static string GetLastChangedPasswordTimeDescription(DateTime lastChangeTime)
        {
            TimeSpan ts = DateTime.Now.Subtract(lastChangeTime);
            if (ts.Days > 30)
            {
                return string.Format(I18N.Message.ChangePasswordDescMonth,ts.Days/30);
            }
            else if(ts.Days > 1)
            {
                return string.Format(I18N.Message.ChangePasswordDescDay, ts.Days);
            }
            else if(ts.Hours > 1)
            {
                return string.Format(I18N.Message.ChangePasswordDescHour, ts.Hours);
            }
            else if (ts.Minutes > 1)
            {
                return string.Format(I18N.Message.ChangePasswordDescMin, ts.Minutes);
            }
            else if (ts.Minutes > 0)
            {
                return I18N.Message.ChangePasswordDescOneMin;
            }
            return string.Empty;
        }

        private static void SetCookie(string accountId, HttpContextBase httpContext)
        {
            HttpCookie authCookie = FormsAuthentication.GetAuthCookie(accountId, false, GetVbsAuthCookiePath());
            authCookie.Secure = true;
            httpContext.Response.SetCookie(authCookie);
        }
    }

    [Serializable]
    public class MemberDetail
    {
        public string LoginId { get; set; }
        public string Email { set; get; }
        public string Mobile { set; get; }
        public int? CityId { set; get; }
        public int? TownshipId { set; get; }
        public string InvoiceTitle { set; get; }
        public string InvoiceComId { set; get; }
        public string InvoiceName { set; get; }
        public string InvoiceAddress { set; get; }
        public string AccountBankCode { set; get; }
        public string AccountBranchCode { set; get; }
        public string AccountName { set; get; }
        public string AccountId { set; get; }
        public string AccountNo { set; get; }
        /// <summary>
        /// 上次修改密碼的時間描述
        /// </summary>
        public string PasswordChangedDesc { get; set; }
        public Guid SellerGuid { set; get; }
    }

    [Serializable]
    public class DemoUser
    {
        public string Password { get; set; }

        public string InspectionCode { get; set; }
    }

    //Demo版顯示Demo資料
    [Serializable]
    public class DemoData
    {
        #region 核銷查詢 / 核銷清冊

        //核銷查詢:檔次資料列表
        public List<VbsVerificationDealInfo> DealInfos
        {
            get
            {
                List<VbsVerificationDealInfo> infos = new List<VbsVerificationDealInfo>();
                VbsVerificationDealInfo info1 = new VbsVerificationDealInfo
                {
                    DealId = "11111111-1111-1111-1111-111111111111",
                    DealName = "一起美麗造型沙龍-洗剪燙",
                    DealType = VbsDealType.Ppon,
                    DealUniqueId = 10220132,
                    ExchangePeriodStartTime = new DateTime(2013, 1, 20),
                    ExchangePeriodEndTime = new DateTime(2014, 2, 11),
                    StoreCount = 1,
                    VerificationState = VbsVerificationState.Verifying,
                    VerificationPercent = 29
                };
                infos.Add(info1);

                VbsVerificationDealInfo info2 = new VbsVerificationDealInfo
                {
                    DealId = "22222222-2222-2222-2222-222222222222",
                    DealName = "一起吃到飽雙人套餐",
                    DealType = VbsDealType.Ppon,
                    DealUniqueId = 10220131,
                    ExchangePeriodStartTime = new DateTime(2013, 1, 1),
                    ExchangePeriodEndTime = new DateTime(2013, 12, 25),
                    StoreCount = 2,
                    VerificationState = VbsVerificationState.Verifying,
                    VerificationPercent = 29
                };
                infos.Add(info2);

                VbsVerificationDealInfo info3 = new VbsVerificationDealInfo
                {
                    DealId = "33333333-3333-3333-3333-333333333333",
                    DealName = "一起泡溫泉會館-大眾湯券",
                    DealType = VbsDealType.Ppon,
                    DealUniqueId = 10220130,
                    ExchangePeriodStartTime = new DateTime(2012, 10, 12),
                    ExchangePeriodEndTime = new DateTime(2012, 12, 26),
                    StoreCount = 1,
                    VerificationState = VbsVerificationState.Finished,
                    VerificationPercent = 29
                };
                infos.Add(info3);

                VbsVerificationDealInfo info4 = new VbsVerificationDealInfo
                {
                    DealId = "44444444-4444-4444-4444-444444444444",
                    DealName = "一起放鬆SPA館-消除痠痛深度放鬆紓壓課程",
                    DealType = VbsDealType.Ppon,
                    DealUniqueId = 10220129,
                    ExchangePeriodStartTime = new DateTime(2012, 9, 8),
                    ExchangePeriodEndTime = new DateTime(2012, 11, 20),
                    StoreCount = 1,
                    VerificationState = VbsVerificationState.Finished,
                    VerificationPercent = 29
                };
                infos.Add(info4);

                return infos;
            }
        }

        //核銷查詢:分店資料列表
        public List<VbsVerificationStoreInfo> StoreInfos
        {
            get
            {
                List<VbsVerificationStoreInfo> infos = new List<VbsVerificationStoreInfo>();
                VbsVerificationStoreInfo info1 = new VbsVerificationStoreInfo
                {
                    StoreName = "《西門店》",
                    StoreId = "22222222-1111-1111-111111111111",
                    SaleCount = 30,
                    ReturnCount = 9,
                    VerifiedCount = 6,
                    UnUsedCount = 15,
                    VerificationState = VbsVerificationState.Verifying,
                    VerificationPercent = 29
                };
                infos.Add(info1);

                VbsVerificationStoreInfo info2 = new VbsVerificationStoreInfo
                {
                    StoreName = "《古亭店》",
                    StoreId = "22222222-1111-1111-222222222222",
                    SaleCount = 30,
                    ReturnCount = 9,
                    VerifiedCount = 6,
                    UnUsedCount = 15,
                    VerificationState = VbsVerificationState.Verifying,
                    VerificationPercent = 29
                };
                infos.Add(info2);

                VbsVerificationStoreInfo info3 = new VbsVerificationStoreInfo
                {
                    StoreName = "《西門店》",
                    StoreId = "11111111-1111-1111-111111111111",
                    SaleCount = 60,
                    ReturnCount = 18,
                    VerifiedCount = 12,
                    UnUsedCount = 30,
                    VerificationState = VbsVerificationState.Verifying,
                    VerificationPercent = 29
                };
                infos.Add(info3);

                VbsVerificationStoreInfo info4 = new VbsVerificationStoreInfo
                {
                    StoreName = "《古亭店》",
                    StoreId = "33333333-1111-1111-111111111111",
                    SaleCount = 60,
                    ReturnCount = 18,
                    VerifiedCount = 12,
                    UnUsedCount = 30,
                    VerificationState = VbsVerificationState.Finished,
                    VerificationPercent = 29
                };
                infos.Add(info4);

                VbsVerificationStoreInfo info5 = new VbsVerificationStoreInfo
                {
                    StoreName = "《中正店》",
                    StoreId = "44444444-1111-1111-111111111111",
                    SaleCount = 60,
                    ReturnCount = 18,
                    VerifiedCount = 12,
                    UnUsedCount = 30,
                    VerificationState = VbsVerificationState.Finished,
                    VerificationPercent = 29
                };
                infos.Add(info5);

                return infos;
            }
        }

        //核銷查詢:憑證核銷狀態列表
        public List<VbsVerificationCouponInfo> VerificationCouponInfos
        {
            get
            {
                List<VbsVerificationCouponInfo> couponInfos = new List<VbsVerificationCouponInfo>();

                VbsVerificationCouponInfo info1 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9520",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.Return,
                    ReturnDateTime = DateTime.Now.AddDays(-3),
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info1);

                VbsVerificationCouponInfo info2 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9521",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ReturnDateTime = null,
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info2);

                VbsVerificationCouponInfo info3 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9522",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ReturnDateTime = null,
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info3);

                VbsVerificationCouponInfo info4 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9523",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ReturnDateTime = null,
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info4);

                VbsVerificationCouponInfo info5 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9524",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ReturnDateTime = null,
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info5);

                VbsVerificationCouponInfo info6 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9525",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.Verified,
                    ReturnDateTime = null,
                    VerifiedDateTime = DateTime.Now.AddDays(-2),
                    UserName = "17Life",
                };
                couponInfos.Add(info6);

                VbsVerificationCouponInfo info7 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9526",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.Verified,
                    ReturnDateTime = null,
                    VerifiedDateTime = DateTime.Now.AddDays(-2),
                    UserName = "17Life",
                };
                couponInfos.Add(info7);

                VbsVerificationCouponInfo info8 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9527",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ReturnDateTime = null,
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info8);

                VbsVerificationCouponInfo info9 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9528",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.Return,
                    ReturnDateTime = DateTime.Now.AddDays(-3),
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info9);

                VbsVerificationCouponInfo info10 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9529",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.Return,
                    ReturnDateTime = DateTime.Now.AddDays(-4),
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info10);

                VbsVerificationCouponInfo info11 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9530",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.Return,
                    ReturnDateTime = DateTime.Now.AddDays(-3),
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info11);

                VbsVerificationCouponInfo info12 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9531",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ReturnDateTime = null,
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info12);

                VbsVerificationCouponInfo info13 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9532",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ReturnDateTime = null,
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info13);

                VbsVerificationCouponInfo info14 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9533",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ReturnDateTime = null,
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info14);

                VbsVerificationCouponInfo info15 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9534",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ReturnDateTime = null,
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info15);

                VbsVerificationCouponInfo info16 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9535",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.Verified,
                    ReturnDateTime = null,
                    VerifiedDateTime = DateTime.Now.AddDays(-2),
                    UserName = "17Life",
                };
                couponInfos.Add(info16);

                VbsVerificationCouponInfo info17 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9536",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.Verified,
                    ReturnDateTime = null,
                    VerifiedDateTime = DateTime.Now.AddDays(-2),
                    UserName = "17Life",
                };
                couponInfos.Add(info17);

                VbsVerificationCouponInfo info18 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9537",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ReturnDateTime = null,
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info18);

                VbsVerificationCouponInfo info19 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9538",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.Return,
                    ReturnDateTime = DateTime.Now.AddDays(-3),
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info19);

                VbsVerificationCouponInfo info20 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9539",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.Return,
                    ReturnDateTime = DateTime.Now.AddDays(-4),
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info20);

                VbsVerificationCouponInfo info21 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9540",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.Return,
                    ReturnDateTime = DateTime.Now.AddDays(-3),
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info21);

                VbsVerificationCouponInfo info22 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9541",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ReturnDateTime = null,
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info22);

                VbsVerificationCouponInfo info23 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9542",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ReturnDateTime = null,
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info23);

                VbsVerificationCouponInfo info24 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9543",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ReturnDateTime = null,
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info24);

                VbsVerificationCouponInfo info25 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9544",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ReturnDateTime = null,
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info25);

                VbsVerificationCouponInfo info26 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9545",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.Verified,
                    ReturnDateTime = null,
                    VerifiedDateTime = DateTime.Now.AddDays(-2),
                    UserName = "17Life",
                };
                couponInfos.Add(info26);

                VbsVerificationCouponInfo info27 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9546",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.Verified,
                    ReturnDateTime = null,
                    VerifiedDateTime = DateTime.Now.AddDays(-2),
                    UserName = "17Life",
                };
                couponInfos.Add(info27);

                VbsVerificationCouponInfo info28 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9547",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ReturnDateTime = null,
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info28);

                VbsVerificationCouponInfo info29 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9548",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.Return,
                    ReturnDateTime = DateTime.Now.AddDays(-3),
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info29);

                VbsVerificationCouponInfo info30 = new VbsVerificationCouponInfo
                {
                    CouponId = "2521-9549",
                    ProductName = "一起美麗造型沙龍-洗剪燙",
                    StoreName = "",
                    VerifyState = VbsCouponVerifyState.Return,
                    ReturnDateTime = DateTime.Now.AddDays(-4),
                    VerifiedDateTime = null,
                    UserName = "",
                };
                couponInfos.Add(info30);

                return couponInfos;
            }
        }

        //核銷清冊 表頭
        public List<SellerDealInfo> SellerDealInfo
        {
            get
            {
                List<SellerDealInfo> infos = new List<SellerDealInfo>();
                SellerDealInfo info1 = new SellerDealInfo
                {
                    DealUniqueId = 10220129,
                    SellerName = "一起放鬆SPA館",
                    DealIntroduction = @"●好康價900元，即可享受:消除痠痛深度放鬆紓壓課程120分鐘，原價2,500元。
●本憑證平、假日皆可使用，特殊節日須來電洽詢是否可使用。
●購買時請確認選擇分店，恕不可跨店使用，亦無法更換分店。
●本活動紙本憑證及手機簡訊皆可使用。"
                };
                infos.Add(info1);

                SellerDealInfo info2 = new SellerDealInfo
                {
                    DealUniqueId = 10220130,
                    SellerName = "一起泡溫泉會館",
                    DealIntroduction = @"●【一起泡溫泉會館】好康憑證150元！於2012/10/12至2012/12/26期間使用即可享有獨家優惠：抵用大眾溫泉池，原價399元單人泡湯。
●本憑證平、假日皆可使用，特殊節日須來電洽詢是否可使用。
●每人每次不限使用張數，可接受多人同時使用多張好康憑證。
●購買時請確認選擇分店，恕不可跨店使用，亦無法更換分店。
●本活動紙本憑證及手機簡訊皆可使用。"
                };
                infos.Add(info2);

                SellerDealInfo info3 = new SellerDealInfo
                {
                    DealUniqueId = 10220131,
                    SellerName = "一起吃到飽",
                    DealIntroduction = @"●【一起吃到飽】於2013/1/1至2013/12/25期間使用即可享有獨家優惠：原價680元雙人套餐，好康價290元。
●本憑證僅限來店消費，請務必預先四天前預訂，並事先告知服務人員使用本憑證，電話：請參照各分店資訊。如無依規定提前預約，店家有權不接受兌換。
●本憑證平、假日皆可使用，特殊節日須來電洽詢是否可使用。
●每人每次不限使用張數，可接受多人同時使用多張好康憑證。
●購買時請確認選擇分店，恕不可跨店使用，亦無法更換分店。
●本活動紙本憑證及手機簡訊皆可使用。"
                };
                infos.Add(info3);

                SellerDealInfo info4 = new SellerDealInfo
                {
                    DealUniqueId = 10220132,
                    SellerName = "一起美麗造型沙龍",
                    DealIntroduction = @"●【一起美麗造型沙龍】於2013/1/20至2013/2/11期間使用即可享有獨家優惠：洗剪燙，原價999元，好康價399元！
●來店消費請務必預先四天前預訂，並事先告知服務人員使用本憑證，電話：請參照各分店資訊。如無依規定提前預約，店家有權不接受兌換。
●本憑證平、假日皆可使用，特殊節日須來電洽詢是否可使用。
●本活動紙本憑證及手機簡訊皆可使用。"
                };
                infos.Add(info4);

                return infos;
            }
        }

        //核銷清冊 明細
        public DataTable SellerVerificationCouponInfos
        {
            get
            {
                DataTable dt = new DataTable();
                dt.Clear();
                dt.Columns.Add("user_name");
                dt.Columns.Add("姓名");
                dt.Columns.Add("好康名稱");
                dt.Columns.Add("分店名稱");
                dt.Columns.Add("選項");
                dt.Columns.Add("憑證編號");
                dt.Columns.Add("使用者編號");
                dt.Columns.Add("序號");
                dt.Columns.Add("訂單編號");
                dt.Columns.Add("分店代碼");
                dt.Columns.Add("status");
                dt.Columns.Add("special_status");
                dt.Columns.Add("modify_time");

                foreach (var info in VerificationCouponInfos)
                {
                    DataRow dr = dt.NewRow();
                    int status = (int)TrustStatus.Initial;
                    int specialStatus = 0;
                    switch (info.VerifyState)
                    {
                        case VbsCouponVerifyState.UnUsed:
                            status = (int)TrustStatus.Trusted;
                            break;
                        case VbsCouponVerifyState.Verified:
                            status = (int)TrustStatus.Verified;
                            break;
                        case VbsCouponVerifyState.Return:
                            status = (int)TrustStatus.Returned;
                            break;
                        default:
                            status = (int)TrustStatus.Initial;
                            break;
                    }

                    dr["姓名"] = "一*姬";
                    dr["好康名稱"] = info.ProductName;
                    dr["分店名稱"] = string.Empty;
                    dr["選項"] = string.Empty;
                    dr["憑證編號"] = info.CouponId;
                    dr["使用者編號"] = string.Empty;
                    dr["序號"] = string.Empty;
                    dr["訂單編號"] = string.Empty;
                    dr["分店代碼"] = string.Empty;
                    dr["status"] = status;
                    dr["special_status"] = specialStatus;
                    dr["modify_time"] = info.ReturnDateTime ?? DateTime.MinValue;
                    dt.Rows.Add(dr);
                }

                return dt;
            }
        }

        #endregion 核銷查詢 / 核銷清冊

        #region 線上核銷

        //線上核銷:核銷憑證列表
        public List<VbsVerifyCouponInfo> VerifyCouponInfos
        {
            get
            {
                List<VbsVerifyCouponInfo> infos = new List<VbsVerifyCouponInfo>();

                VbsVerifyCouponInfo info1 = new VbsVerifyCouponInfo
                {
                    DealName = "一起吃到飽雙人套餐",
                    DealId = "22222222-2222-2222-2222-222222222222",
                    DealType = BusinessModel.Ppon,
                    CouponId = "2521-9527",
                    ExchangePeriodStartTime = DateTime.Today,
                    ExchangePeriodEndTime = DateTime.Today.AddDays(10),
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ModifyTime = DateTime.Today,
                    PayerName = "一*姬",
                    TrustId = "102011"
                };
                infos.Add(info1);

                VbsVerifyCouponInfo info2 = new VbsVerifyCouponInfo
                {
                    DealName = "一起吃到飽雙人套餐",
                    DealId = "22222222-2222-2222-2222-222222222222",
                    DealType = BusinessModel.Ppon,
                    CouponId = "2521-9527",
                    ExchangePeriodStartTime = DateTime.Today.AddDays(-42),
                    ExchangePeriodEndTime = DateTime.Today.AddDays(-32),
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ModifyTime = DateTime.Today,
                    PayerName = "一*姬",
                    TrustId = "102012"
                };
                infos.Add(info2);

                VbsVerifyCouponInfo info3 = new VbsVerifyCouponInfo
                {
                    DealName = "一起吃到飽雙人套餐",
                    DealId = "22222222-2222-2222-2222-222222222222",
                    DealType = BusinessModel.Ppon,
                    CouponId = "2521-9527",
                    ExchangePeriodStartTime = DateTime.Today.AddDays(-20),
                    ExchangePeriodEndTime = DateTime.Today.AddDays(-10),
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ModifyTime = DateTime.Today,
                    PayerName = "一*姬",
                    TrustId = "102013"
                };
                infos.Add(info3);

                VbsVerifyCouponInfo info4 = new VbsVerifyCouponInfo
                {
                    DealName = "一起吃到飽雙人套餐",
                    DealId = "22222222-2222-2222-2222-222222222222",
                    DealType = BusinessModel.Ppon,
                    CouponId = "2521-9527",
                    ExchangePeriodStartTime = DateTime.Today.AddDays(-5),
                    ExchangePeriodEndTime = DateTime.Today.AddDays(5),
                    VerifyState = VbsCouponVerifyState.Return,
                    ModifyTime = DateTime.Now.AddDays(-1),
                    PayerName = "一*姬",
                    TrustId = "102014"
                };
                infos.Add(info4);

                VbsVerifyCouponInfo info5 = new VbsVerifyCouponInfo
                {
                    DealName = "一起吃到飽雙人套餐",
                    DealId = "22222222-2222-2222-2222-222222222222",
                    DealType = BusinessModel.Ppon,
                    CouponId = "2521-9527",
                    ExchangePeriodStartTime = DateTime.Today.AddDays(-5),
                    ExchangePeriodEndTime = DateTime.Today.AddDays(5),
                    VerifyState = VbsCouponVerifyState.Verified,
                    ModifyTime = DateTime.Now.AddDays(-1),
                    PayerName = "一*姬",
                    TrustId = "102016"
                };
                infos.Add(info5);

                VbsVerifyCouponInfo info6 = new VbsVerifyCouponInfo
                {
                    DealName = "一起吃到飽雙人套餐",
                    DealId = "22222222-2222-2222-2222-222222222222",
                    DealType = BusinessModel.Ppon,
                    CouponId = "2521-9527",
                    ExchangePeriodStartTime = DateTime.Today,
                    ExchangePeriodEndTime = DateTime.Today.AddDays(10),
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ModifyTime = DateTime.Today,
                    PayerName = "一*姬",
                    TrustId = "102017"
                };
                infos.Add(info6);

                VbsVerifyCouponInfo info7 = new VbsVerifyCouponInfo
                {
                    DealName = "一起吃到飽雙人套餐",
                    DealId = "22222222-2222-2222-2222-222222222222",
                    DealType = BusinessModel.Ppon,
                    CouponId = "2521-9527",
                    ExchangePeriodStartTime = DateTime.Today,
                    ExchangePeriodEndTime = DateTime.Today.AddDays(10),
                    VerifyState = VbsCouponVerifyState.UnUsed,
                    ModifyTime = DateTime.Today,
                    PayerName = "一*姬",
                    TrustId = "102018"
                };
                infos.Add(info7);

                return infos;
            }
        }

        //線上核銷:核銷結果回覆訊息
        public Dictionary<string, VerificationStatus> VerifyResultMsg
        {
            get
            {
                Dictionary<string, VerificationStatus> resultMsg = new Dictionary<string, VerificationStatus>()
                {
                    {"102011", VerificationStatus.CouponOk},
                    {"102013", VerificationStatus.CouponOk},
                    {"102017", VerificationStatus.CouponError},
                    {"102018", VerificationStatus.CashTrustStatusLogErr}
                };

                return resultMsg;
            }
        }

        #endregion 線上核銷

        #region 對帳查詢

        //對帳查詢:檔次資料列表
        public List<VbsBalanceDealInfo> BalanceSheetDealInfos
        {
            get
            {
                List<VbsBalanceDealInfo> infos = new List<VbsBalanceDealInfo>();
                VbsBalanceDealInfo info1 = new VbsBalanceDealInfo
                {
                    DealName = "一起美麗造型沙龍-洗剪燙",
                    DealId = new Guid("11111111-1111-1111-1111-111111111111"),
                    DealType = VbsDealType.Ppon,
                    DealUniqueId = 10220132,
                    ExchangePeriodStartTime = new DateTime(2013, 1, 20),
                    ExchangePeriodEndTime = new DateTime(2014, 2, 11),
                    StoreCount = 1,
                    VerificationState = VbsVerificationState.Verifying,
                    BalanceState = VbsBalanceState.Balancing,
                    GenerationFrequency = (int)BalanceSheetGenerationFrequency.WeekBalanceSheet
                };
                infos.Add(info1);

                VbsBalanceDealInfo info2 = new VbsBalanceDealInfo
                {
                    DealId = new Guid("22222222-2222-2222-2222-222222222222"),
                    DealName = "一起吃到飽雙人套餐",
                    DealType = VbsDealType.Ppon,
                    DealUniqueId = 10220131,
                    ExchangePeriodStartTime = new DateTime(2013, 1, 1),
                    ExchangePeriodEndTime = new DateTime(2013, 12, 25),
                    StoreCount = 2,
                    VerificationState = VbsVerificationState.Verifying,
                    BalanceState = VbsBalanceState.Balancing,
                    GenerationFrequency = (int)BalanceSheetGenerationFrequency.WeekBalanceSheet
                };
                infos.Add(info2);

                VbsBalanceDealInfo info3 = new VbsBalanceDealInfo
                {
                    DealId = new Guid("33333333-3333-3333-3333-333333333333"),
                    DealName = "一起泡溫泉會館-大眾湯券",
                    DealType = VbsDealType.Ppon,
                    DealUniqueId = 10220130,
                    ExchangePeriodStartTime = new DateTime(2012, 10, 12),
                    ExchangePeriodEndTime = new DateTime(2012, 12, 26),
                    StoreCount = 1,
                    VerificationState = VbsVerificationState.Finished,
                    BalanceState = VbsBalanceState.Finished,
                    GenerationFrequency = (int)BalanceSheetGenerationFrequency.WeekBalanceSheet
                };
                infos.Add(info3);

                VbsBalanceDealInfo info4 = new VbsBalanceDealInfo
                {
                    DealId = new Guid("44444444-4444-4444-4444-444444444444"),
                    DealName = "一起放鬆SPA館-消除痠痛深度放鬆紓壓課程",
                    DealType = VbsDealType.Ppon,
                    DealUniqueId = 10220129,
                    ExchangePeriodStartTime = new DateTime(2012, 9, 8),
                    ExchangePeriodEndTime = new DateTime(2012, 11, 20),
                    StoreCount = 1,
                    VerificationState = VbsVerificationState.Finished,
                    BalanceState = VbsBalanceState.Finished,
                    GenerationFrequency = (int)BalanceSheetGenerationFrequency.MonthBalanceSheet
                };
                infos.Add(info4);

                return infos;
            }
        }

        //對帳查詢:分店資料列表
        public List<VbsBalanceStoreInfo> BalanceSheetStoreInfos
        {
            get
            {
                List<VbsBalanceStoreInfo> infos = new List<VbsBalanceStoreInfo>();
                VbsBalanceStoreInfo info1 = new VbsBalanceStoreInfo
                {
                    StoreName = "西門店",
                    StoreId = "22222222-1111-1111-111111111111",
                    SaleCount = 30,
                    ReturnCount = 9,
                    VerifiedCount = 6,
                    UnUsedCount = 15,
                    BalanceState = VbsBalanceState.Balancing,
                    GenerationFrequency = (int)BalanceSheetGenerationFrequency.WeekBalanceSheet
                };
                infos.Add(info1);

                VbsBalanceStoreInfo info2 = new VbsBalanceStoreInfo
                {
                    StoreName = "古亭店",
                    StoreId = "22222222-1111-1111-222222222222",
                    SaleCount = 30,
                    ReturnCount = 9,
                    VerifiedCount = 6,
                    UnUsedCount = 15,
                    BalanceState = VbsBalanceState.Balancing,
                    GenerationFrequency = (int)BalanceSheetGenerationFrequency.WeekBalanceSheet
                };
                infos.Add(info2);

                VbsBalanceStoreInfo info3 = new VbsBalanceStoreInfo
                {
                    StoreName = "西門店",
                    StoreId = "11111111-1111-1111-111111111111",
                    SaleCount = 60,
                    ReturnCount = 18,
                    VerifiedCount = 12,
                    UnUsedCount = 30,
                    BalanceState = VbsBalanceState.Balancing,
                    GenerationFrequency = (int)BalanceSheetGenerationFrequency.WeekBalanceSheet
                };
                infos.Add(info3);

                VbsBalanceStoreInfo info4 = new VbsBalanceStoreInfo
                {
                    StoreName = "古亭店",
                    StoreId = "33333333-1111-1111-111111111111",
                    SaleCount = 60,
                    ReturnCount = 18,
                    VerifiedCount = 12,
                    UnUsedCount = 30,
                    BalanceState = VbsBalanceState.Finished,
                    GenerationFrequency = (int)BalanceSheetGenerationFrequency.WeekBalanceSheet
                };
                infos.Add(info4);

                VbsBalanceStoreInfo info5 = new VbsBalanceStoreInfo
                {
                    StoreName = "中正店",
                    StoreId = "44444444-1111-1111-111111111111",
                    SaleCount = 60,
                    ReturnCount = 18,
                    VerifiedCount = 12,
                    UnUsedCount = 30,
                    BalanceState = VbsBalanceState.Finished,
                    GenerationFrequency = (int)BalanceSheetGenerationFrequency.MonthBalanceSheet
                };
                infos.Add(info5);

                return infos;
            }
        }

        //對帳查詢:月對帳單列表
        public List<VbsBalanceSheetsMonthInfo> BalanceSheetMonthInfos
        {
            get
            {
                List<VbsBalanceSheetsMonthInfo> infos = new List<VbsBalanceSheetsMonthInfo>();
                VbsBalanceSheetsMonthInfo info1 = new VbsBalanceSheetsMonthInfo
                {
                    BalanceSheetId = 1,
                    BalanceSheetMonth = "2012/12",
                    IsConfirmedReadyToPay = true
                };
                infos.Add(info1);

                VbsBalanceSheetsMonthInfo info2 = new VbsBalanceSheetsMonthInfo
                {
                    BalanceSheetId = 2,
                    BalanceSheetMonth = "2013/01",
                    IsConfirmedReadyToPay = false
                };
                infos.Add(info2);

                return infos;
            }
        }

        //對帳查詢:週對帳單列表
        public List<VbsBalanceSheetsWeekInfo> BalanceSheetWeekInfos
        {
            get
            {
                List<VbsBalanceSheetsWeekInfo> infos = new List<VbsBalanceSheetsWeekInfo>();
                VbsBalanceSheetsWeekInfo info1 = new VbsBalanceSheetsWeekInfo
                {
                    BalanceSheetId = 3,
                    WeekIntervalStartTime = new DateTime(2012, 12, 3),
                    WeekIntervalEndTime = new DateTime(2012, 12, 9)
                };
                infos.Add(info1);

                VbsBalanceSheetsWeekInfo info2 = new VbsBalanceSheetsWeekInfo
                {
                    BalanceSheetId = 4,
                    WeekIntervalStartTime = new DateTime(2012, 12, 10),
                    WeekIntervalEndTime = new DateTime(2012, 12, 16)
                };
                infos.Add(info2);

                VbsBalanceSheetsWeekInfo info3 = new VbsBalanceSheetsWeekInfo
                {
                    BalanceSheetId = 5,
                    WeekIntervalStartTime = new DateTime(2012, 12, 17),
                    WeekIntervalEndTime = new DateTime(2012, 12, 23)
                };
                infos.Add(info3);

                VbsBalanceSheetsWeekInfo info4 = new VbsBalanceSheetsWeekInfo
                {
                    BalanceSheetId = 6,
                    WeekIntervalStartTime = new DateTime(2012, 12, 24),
                    WeekIntervalEndTime = new DateTime(2012, 12, 30)
                };
                infos.Add(info4);

                VbsBalanceSheetsWeekInfo info5 = new VbsBalanceSheetsWeekInfo
                {
                    BalanceSheetId = 7,
                    WeekIntervalStartTime = new DateTime(2012, 12, 31),
                    WeekIntervalEndTime = new DateTime(2013, 1, 6)
                };
                infos.Add(info5);

                VbsBalanceSheetsWeekInfo info6 = new VbsBalanceSheetsWeekInfo
                {
                    BalanceSheetId = 8,
                    WeekIntervalStartTime = new DateTime(2013, 1, 7),
                    WeekIntervalEndTime = new DateTime(2013, 1, 13)
                };
                infos.Add(info6);

                VbsBalanceSheetsWeekInfo info7 = new VbsBalanceSheetsWeekInfo
                {
                    BalanceSheetId = 9,
                    WeekIntervalStartTime = new DateTime(2013, 1, 14),
                    WeekIntervalEndTime = new DateTime(2013, 1, 20)
                };
                infos.Add(info7);

                VbsBalanceSheetsWeekInfo info8 = new VbsBalanceSheetsWeekInfo
                {
                    BalanceSheetId = 10,
                    WeekIntervalStartTime = new DateTime(2013, 1, 21),
                    WeekIntervalEndTime = new DateTime(2013, 1, 27)
                };
                infos.Add(info8);

                return infos;
            }
        }

        //對帳查詢:週對帳單明細
        public List<VbsBalanceCouponInfo> BalanceSheetCouponInfos
        {
            get
            {
                List<VbsBalanceCouponInfo> infos = new List<VbsBalanceCouponInfo>();

                VbsBalanceCouponInfo info1 = new VbsBalanceCouponInfo
                {
                    CouponId = "2521-9525",
                    VerifiedDateTime = new DateTime(),
                    StoreName = "",
                    UserName = "",
                    UnDoState = BalanceSheetDetailStatus.Normal,
                    UnDoDateTime = null
                };
                infos.Add(info1);

                VbsBalanceCouponInfo info2 = new VbsBalanceCouponInfo
                {
                    CouponId = "2521-9526",
                    VerifiedDateTime = new DateTime(),
                    StoreName = "",
                    UserName = "",
                    UnDoState = BalanceSheetDetailStatus.Normal,
                    UnDoDateTime = null
                };
                infos.Add(info2);

                VbsBalanceCouponInfo info3 = new VbsBalanceCouponInfo
                {
                    CouponId = "2521-9535",
                    VerifiedDateTime = new DateTime(),
                    StoreName = "",
                    UserName = "",
                    UnDoState = BalanceSheetDetailStatus.Normal,
                    UnDoDateTime = null
                };
                infos.Add(info3);

                VbsBalanceCouponInfo info4 = new VbsBalanceCouponInfo
                {
                    CouponId = "2521-9536",
                    VerifiedDateTime = new DateTime(),
                    StoreName = "",
                    UserName = "",
                    UnDoState = BalanceSheetDetailStatus.Normal,
                    UnDoDateTime = null
                };
                infos.Add(info4);

                VbsBalanceCouponInfo info5 = new VbsBalanceCouponInfo
                {
                    CouponId = "2521-9545",
                    VerifiedDateTime = new DateTime(),
                    StoreName = "",
                    UserName = "",
                    UnDoState = BalanceSheetDetailStatus.Normal,
                    UnDoDateTime = null
                };
                infos.Add(info5);

                VbsBalanceCouponInfo info6 = new VbsBalanceCouponInfo
                {
                    CouponId = "2521-9546",
                    VerifiedDateTime = new DateTime(),
                    StoreName = "",
                    UserName = "",
                    UnDoState = BalanceSheetDetailStatus.Normal,
                    UnDoDateTime = null
                };
                infos.Add(info6);

                VbsBalanceCouponInfo info7 = new VbsBalanceCouponInfo
                {
                    CouponId = "2521-9544",
                    VerifiedDateTime = new DateTime(),
                    StoreName = "",
                    UserName = "",
                    UnDoState = BalanceSheetDetailStatus.Undo,
                    UnDoDateTime = null
                };
                infos.Add(info7);

                VbsBalanceCouponInfo info8 = new VbsBalanceCouponInfo
                {
                    CouponId = "2521-9547",
                    VerifiedDateTime = new DateTime(),
                    StoreName = "",
                    UserName = "",
                    UnDoState = BalanceSheetDetailStatus.Deduction,
                    UnDoDateTime = null
                };
                infos.Add(info8);

                return infos;
            }
        }

        #endregion 對帳查詢

    }

    [Serializable]
    public class ViewVbsRight
    {
        public bool ToShop { get; set; }

        public bool ToHouse { get; set; }

        public bool Verify { get; set; }

        public bool BalanceSheet { get; set; }

        public bool Ship { get; set; }

        public bool Booking { get; set; }

        public bool ReserveLock { get; set; }

        public bool UserEvaluate { get; set; }

        public bool Proposal { set; get; }
        public bool ProposalProduct { set; get; }

        public bool ShoppingCartManage { set; get; }
        public bool AccountManage { get; set; }
        public bool CustomerServiceManage { get; set; }

        public bool IsInstorePickup { get; set; }
        public bool Wms { get; set; }
    }
}
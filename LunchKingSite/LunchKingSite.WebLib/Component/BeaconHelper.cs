﻿using LunchKingSite.WebLib.Models.Beacon;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Component
{

    public static class BeaconHelper
    {
        /// <summary>
        /// Returns the given partial view, wrapped in a templated script tag
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="triggerAppId"></param>
        /// <param name="isFullShop"></param>
        /// <returns></returns>
        public static MvcHtmlString RenderDropdownlist(this HtmlHelper helper, int triggerAppId, bool isFullShop = true)
        {
            string partialViewName = string.Empty;
            if (triggerAppId != 0)
            {
                if (triggerAppId == (int)LunchKingSite.Core.Enumeration.BeaconTriggerAppFunctionName.skm)
                {
                    helper.ViewData.Model = new SkmDropdownListModel() { IsFullShop = isFullShop};
                    partialViewName = "_SkmDropdownlist";
                }
                else
                {
                    partialViewName = "_GroupDropdownlist";
                }
            }
            return new MvcHtmlString(RenderPartialToString(helper, partialViewName));
        }

        public static MvcHtmlString RenderDropdownlist(this HtmlHelper helper, string functionName, bool isFullShop = true)
        {
            string partialViewName = string.Empty;
            if (!string.IsNullOrEmpty(functionName))
            {
                helper.ViewData.Model = new SkmDropdownListModel() { IsFullShop = isFullShop };
                if (functionName == LunchKingSite.Core.Enumeration.BeaconTriggerAppFunctionName.skm.ToString())
                {
                    partialViewName = "_SkmDropdownlist";
                }
                else
                {
                    partialViewName = "_GroupDropdownlist";
                }
            }
            return new MvcHtmlString(RenderPartialToString(helper, partialViewName));
        }

        private static string RenderPartialToString(this HtmlHelper helper, string partialViewName)
        {
            var controllerContext = helper.ViewContext.Controller.ControllerContext;
            var tempData = helper.ViewContext.TempData;
            using (var stringWriter = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controllerContext, partialViewName);
                var viewContext = new ViewContext(controllerContext, viewResult.View, helper.ViewData, tempData, stringWriter);
                viewResult.View.Render(viewContext, stringWriter);
                return stringWriter.GetStringBuilder().ToString();
            }
        }
    }
}

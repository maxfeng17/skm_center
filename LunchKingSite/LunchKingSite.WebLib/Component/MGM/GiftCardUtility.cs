﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models.MGM;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template.MGM;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Models.MGM;
using LunchKingSite.WebLib.Models.Ppon;

namespace LunchKingSite.WebLib.Component.MGM
{
    public class GiftCardUtility
    {
        #region property

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static List<TemplateCard> _cardStyle = new List<TemplateCard>();
        private static List<string> _messagePattern = new List<string>();
        private static List<IGiftSend> _giftSend;

        public static List<IGiftSend> GiftSend { get { return _giftSend; } }
        public static List<TemplateCard> CardStyleGet { get { return _cardStyle; } }
        public static List<string> MessagePatternGet { get { return _messagePattern; } }
       
        #endregion

        static GiftCardUtility()
        {

            //通知方式，有其他方式這裡要新增
            _giftSend = new List<IGiftSend>
            {
                new GiftEmail(config),
                new GiftSms(config),
               // new GiftFb()                  現在Fb改用手機端分享連結，所以先拿掉
            };

            //卡片樣式，enabled=true : 在線上的卡片
            var tempCards = MGMFacade.MessageTemplateGet().Where(t => t.Enabled == true);
            foreach (var card in tempCards)
            {
                TemplateCard tempC = new TemplateCard()
                {
                    Id = card.Id,
                    Title = card.Title,
                    ImagePic = config.SiteUrl + "/Themes/MGM/images/" + card.LogoPic,
                    BackgroundColor = card.BackgroundColor
                };
                _cardStyle.Add(tempC);
            }

            //罐頭訊息
            _messagePattern.Add("希望您會喜歡這份禮物!");
            _messagePattern.Add("謝謝您。");
            _messagePattern.Add("祝您生日快樂！");
            _messagePattern.Add("祝您聖誕節快樂！");
            _messagePattern.Add("祝您有美好的一天！");
            _messagePattern.Add("推薦您這家好餐廳！");
        }

        public static bool SendGift(MatchMemberGroup memberData, GiftCard giftCard)
        {
            var giftSend = _giftSend.FirstOrDefault(x => x.Type.Equals(memberData.Type)); //依type選取class
            if (giftSend != null)
            {
                giftSend.SendGift(memberData.Value, memberData.Name, memberData.AccessCode, giftCard);
            }

            return true;
        }
    }

    public interface IGiftSend
    {
        int Type { get; set; }
        void SendGift(string sendTo, string sendName, string asCode, GiftCard giftCard);
    }

    /// <summary>
    /// Email
    /// </summary>
    public class GiftEmail : IGiftSend
    {
        private static ISysConfProvider _config;
        public int Type { get; set; }
        private static IPponProvider pponProvider = ProviderFactory.Instance().GetProvider<IPponProvider>();

        public GiftEmail(ISysConfProvider config)
        {
            _config = config;
            Type = (int)MatchType.Email;
        }

        public void SendGift(string sendTo, string sendName, string asCode, GiftCard giftCard)
        {
            using (MailMessage msg = new MailMessage())
            {
                var content = TemplateFactory.Instance().GetTemplate<SendGiftMail>();
                content.GiftCard =
                    GiftCardUtility.CardStyleGet.First(x => x.Id.Equals(giftCard.CardStyleId)).ImagePic;
                content.FriendMessage = giftCard.Message;
                content.ReceiverName = sendName;
                content.SiteUrl = _config.SiteUrl;
                content.SiteServiceUrl = _config.SiteServiceUrl;
                content.DealBid = _config.SiteUrl + "/" + giftCard.Deal.BusinessHourGuid.ToString();
                content.DealItemName = giftCard.CouponUsage;
                content.DealContent = giftCard.Deal.EventName;
                content.DealTitle = giftCard.CouponUsage;
                content.Addr = giftCard.Deal.SellerAddress;
                content.Tel = giftCard.Deal.SellerTel;
                content.AccessLink = "https://" + _config.MGMGiftGetUrl + "/g/" + asCode;
                content.FriendName = giftCard.SenderName;
                content.FriendMessage = giftCard.Message;
                content.BackgroundColor=GiftCardUtility.CardStyleGet.First(x => x.Id == giftCard.CardStyleId).BackgroundColor;
                #region 檔次圖示

                string imagePathData = giftCard.Deal.DefaultDealImage;
                //判斷訂單是否為多檔次子檔的訂單，多檔次的圖片一律以母檔為準
                if ((giftCard.Deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                {
                    //多檔次子檔訂單需取得主檔的圖片
                    CouponEventContent mainDealContent = pponProvider.CouponEventContentGetBySubDealBid(giftCard.Deal.BusinessHourGuid);
                    if ((mainDealContent != null) && (mainDealContent.IsLoaded))
                    {
                        imagePathData = mainDealContent.ImagePath;
                    }
                }

                string[] imgPaths = ImageFacade.GetMediaPathsFromRawData(imagePathData, MediaType.PponDealPhoto);
                #endregion
                content.MainDealPic = imgPaths != null && imgPaths.Length > 0 ? imgPaths[0] : string.Empty;
                content.NowDate = DateTime.Now.ToShortDateString();
                content.ExpireDate = DateTime.Now.AddDays(_config.MGMGiftLockDay).ToString("yyyy/MM/dd HH:00");

                msg.From = new MailAddress(_config.ServiceEmail, "17Life送禮小天使");
                msg.To.Add(sendTo);
                msg.Subject = giftCard.SenderName + " 在17Life選了一份禮物要給你";
                msg.Body = content.ToString();
                msg.IsBodyHtml = true;

                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }
    }

    /// <summary>
    /// 簡訊
    /// </summary>
    public class GiftSms : IGiftSend
    {
        private static ISysConfProvider _config;
        public int Type { get; set; }

        public GiftSms(ISysConfProvider config)
        {
            _config = config;
            Type = (int)MatchType.Mobile;
        }

        public void SendGift(string sendTo, string sendName, string asCode, GiftCard giftCard)
        {
            string accessCodeUrl =
                WebUtility.RequestShortUrl(string.Format("https://" + _config.MGMGiftGetUrl + "/g/{0}", asCode))
                .Replace(_config.SiteUrl, _config.MGMGiftGetUrl).Replace(_config.SSLSiteUrl, _config.MGMGiftGetUrl);
            accessCodeUrl = "http://" + accessCodeUrl;
            string showMessage = string.Format("{0} 在17Life送你一份禮物，點選下方連結完成步驟後，即可領取！{1}",
                giftCard.SenderName, accessCodeUrl);

            SMS sms = new SMS();
            sms.SendMessage("", showMessage, "", sendTo, giftCard.SenderName, Core.SmsType.Ppon, Guid.Empty);
        }
    }

    /// <summary>
    /// FB 通知補發
    /// </summary>
    public class GiftFb : IGiftSend
    {
        public int Type { get; set; }

        public GiftFb()
        {
            Type = (int)MatchType.Fb;
        }

        public void SendGift(string sendTo, string sendName, string asCode, GiftCard giftCard)
        {
            var fbUserId = sendTo.Split("_")[1];
            Member member = null;
            if (MGMFacade.IsMemeberByFb(fbUserId, ref member))//如果是會員補發Email通知
            {
                GiftCardUtility.GiftSend
                               .First(x => x.Type.Equals((int)MatchType.Email))
                               .SendGift(member.TrueEmail, sendName, asCode, giftCard);
            }
        }
    }

}




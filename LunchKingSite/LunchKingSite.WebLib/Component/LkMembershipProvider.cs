using System;
using System.Data.SqlTypes;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;
using log4net;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using SubSonic;

namespace LunchKingSite.WebLib.Component
{
    public class LkMembershipProvider : MembershipProvider
    {
        public override string Name
        {
            get
            {
                return "LkMembershipProvider";
            }
        }

        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ILog logger = LogManager.GetLogger(typeof(LkMembershipProvider).Name);

        public override string ApplicationName
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            Member m = mp.MemberGet(username);
            if (m.IsLoaded)
            {
                if (MembershipProviderCore.EncodePassword(oldPassword, (MembershipPasswordFormat)m.PasswordFormat, m.PasswordSalt) !=
                    m.Password)
                {
                    return false;
                }

                string newEncodedPassword = MembershipProviderCore.EncodePassword(newPassword, PasswordFormat, m.PasswordSalt);

                m.LastPasswordChangedDate = DateTime.Now;
                m.PasswordFormat = (int) PasswordFormat;
                m.Password = newEncodedPassword;
                mp.MemberSet(m);
            }

            MobileMember mm = mp.MobileMemberGetByUserName(username);
            if (mm.IsLoaded)
            {
                if (MembershipProviderCore.EncodePassword(oldPassword, (MembershipPasswordFormat)mm.PasswordFormat, mm.PasswordSalt) !=
                    mm.Password)
                {
                    return false;
                }
                mm.PasswordSalt = MembershipProviderCore.GenerateSalt();
                mm.PasswordFormat = (int)PasswordFormat;
                mm.LastPasswordChangedDate = DateTime.Now;
                mm.FailedPasswordAttemptCount = 0;
                mm.FailedPasswordAttemptWindowStart = SqlDateTime.MinValue.Value;
                mm.Password = MembershipProviderCore.EncodePassword(newPassword, PasswordFormat, mm.PasswordSalt);
                mp.MobileMemberSet(mm);
            }
            if (m.IsLoaded == false && mm.IsLoaded == false)
            {
                return false;
            }
            return true;
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new System.NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new System.NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new System.NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new System.NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { return false; }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection result = new MembershipUserCollection();
            MemberCollection memCol = new Select().From<Member>().Where(Member.Columns.UserName).Like("%" + emailToMatch + "%")
                .ExecuteAsCollection<MemberCollection>();
            foreach (Member m in memCol)
            {
                MemberLinkCollection mls = mp.MemberLinkGetList(m.UniqueId);
                MobileMember mm = MemberFacade.GetMobileMember(m.UniqueId);
                bool isApproved = false;
                if (mls.Any(t => t.ExternalOrg == (int)SingleSignOnSource.Facebook
                                   || t.ExternalOrg == (int)SingleSignOnSource.PayEasy))
                {
                    isApproved = true;
                }
                else if (mls.Any(t => t.ExternalOrg == (int)SingleSignOnSource.ContactDigitalIntegration)
                    && Helper.IsFlagSet(m.Status, MemberStatusFlag.Active17Life))
                {
                    isApproved = true;
                }
                else if (mls.Any(t => t.ExternalOrg == (int)SingleSignOnSource.Mobile17Life)
                    && mm.Status == (int)MobileMemberStatusType.Activated)
                {
                    isApproved = true;
                }
                bool isLockedOut = false;
                if (m.IsLockedOut || mm.IsLockedOut)
                {
                    isLockedOut = true;
                }
                result.Add(new MembershipUser(Name, m.UserName, m.UserName, m.UserEmail, "", "", isApproved,
                    isLockedOut, m.CreateTime, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue));
            }
            totalRecords = memCol.Count;
            return result;
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            var result = FindUsersByEmail(usernameToMatch, pageIndex, pageSize, out totalRecords);
            return result;
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new System.NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new System.NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new System.NotImplementedException();
        }

        public override MembershipUser GetUser(string userName, bool userIsOnline)
        {
            Member m = mp.MemberGet(userName);
            if (m.IsLoaded == false)
            {
                return null;
            }
            bool isLockedOut = false;
            MobileMember mm = mp.MobileMemberGet(m.UniqueId);
            if (m.IsLockedOut || (mm.IsLoaded && mm.IsLockedOut))
            {
                isLockedOut = true;
            }
            return new MembershipUser(Name, m.UserName, userName, m.UserEmail, "", "", m.IsApproved,
                isLockedOut, m.CreateTime, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue);
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new System.NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            return mp.MemberGet(email).UserName;
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return 5; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new System.NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new System.NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            //30分
            get { return 30; }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { return MembershipPasswordFormat.Hashed; }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new System.NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { return false; }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new System.NotImplementedException(); }
        }

        public override string ResetPassword(string username, string password)
        {
            Member m = mp.MemberGet(username);
            if (m.IsLoaded)
            {
                m.PasswordSalt = MembershipProviderCore.GenerateSalt();
                m.PasswordFormat = (int)PasswordFormat;
                if (string.IsNullOrEmpty(password))
                {
                    password = Membership.GeneratePassword(8, 0);
                }
                m.FailedPasswordAttemptCount = 0;
                m.FailedPasswordAttemptWindowStart = SqlDateTime.MinValue.Value;
                m.Password = MembershipProviderCore.EncodePassword(password, PasswordFormat, m.PasswordSalt);
                mp.MemberSet(m);
            }

            MobileMember mm = mp.MobileMemberGetByUserName(username);
            if (mm.IsLoaded)
            {
                mm.PasswordSalt = MembershipProviderCore.GenerateSalt();
                mm.PasswordFormat = (int)PasswordFormat;
                if (string.IsNullOrEmpty(password))
                {
                    password = Membership.GeneratePassword(8, 0);
                }
                mm.FailedPasswordAttemptCount = 0;
                mm.FailedPasswordAttemptWindowStart = SqlDateTime.MinValue.Value;
                mm.Password = MembershipProviderCore.EncodePassword(password, PasswordFormat, mm.PasswordSalt);
                mp.MobileMemberSet(mm);
            }
            return password;
        }

        public override bool UnlockUser(string userName)
        {
            try
            {
                Member m = mp.MemberGet(userName);
                if (m.IsLoaded && m.IsLockedOut)
                {
                    m.IsLockedOut = false;
                    mp.MemberSet(m);
                }

                MobileMember mm = mp.MobileMemberGetByUserName(userName);
                if (mm.IsLoaded && mm.IsLockedOut)
                {
                    mm.IsLockedOut = false;
                    mp.MobileMemberSet(mm);
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.WarnFormat("Unlokcer {0} error.ex={1}", userName, ex);
                return false;
            }            
        }

        public override void UpdateUser(MembershipUser user)
        {
            Member m = mp.MemberGet(user.UserName);
            if (m.IsLockedOut != user.IsLockedOut)
            {
                m.IsLockedOut = user.IsLockedOut;
                m.EdmType = 0;
                mp.MemberSet(m);
            }
        }

        public override bool ValidateUser(string username, string password)
        {
            Member m = MemberFacade.GetMember(username);
            if (m.IsLoaded == false)
            {
                return false;
            }
            string pwd = MembershipProviderCore.EncodePassword(
                password, (MembershipPasswordFormat)m.PasswordFormat, m.PasswordSalt);
            if (pwd != m.Password)
            {
                if ((DateTime.Now - m.FailedPasswordAttemptWindowStart).Minutes < Membership.PasswordAttemptWindow)
                {
                    m.FailedPasswordAttemptCount++;
                }
                else
                {
                    m.FailedPasswordAttemptCount = 1;
                    m.FailedPasswordAttemptWindowStart = DateTime.Now;
                    //超過PasswordAttemptWindow分鍾後重算
                }
                if (m.FailedPasswordAttemptCount >= this.MaxInvalidPasswordAttempts)
                {
                    m.IsLockedOut = true;
                }
                mp.MemberSet(m);
                return false;
            }
            m.LastLoginDate = DateTime.Now;
            m.FailedPasswordAttemptWindowStart = DateTime.Now;
            m.FailedPasswordAttemptCount = 0;
            mp.MemberSet(m);
            return true;
        }

        public static bool ValidateMobileUser(string number, string password)
        {
            MobileMember mm = mp.MobileMemberGet(number);
            if (mm.IsLoaded == false)
            {
                return false;
            }
            string pwd = MembershipProviderCore.EncodePassword(
                password, (MembershipPasswordFormat)mm.PasswordFormat, mm.PasswordSalt);
            if (pwd != mm.Password)
            {
                if ((DateTime.Now - mm.FailedPasswordAttemptWindowStart).Minutes < Membership.PasswordAttemptWindow)
                {
                    mm.FailedPasswordAttemptCount++;
                }
                else
                {
                    mm.FailedPasswordAttemptCount = 1;
                    mm.FailedPasswordAttemptWindowStart = DateTime.Now;
                    //超過PasswordAttemptWindow分鍾後重算
                }
                if (mm.FailedPasswordAttemptCount >= Membership.MaxInvalidPasswordAttempts)
                {
                    mm.IsLockedOut = true;
                }
                mp.MobileMemberSet(mm);
                return false;
            }
            mm.LastLoginDate = DateTime.Now;
            mm.FailedPasswordAttemptWindowStart = DateTime.Now;
            mm.FailedPasswordAttemptCount = 0;
            mp.MobileMemberSet(mm);
            return true;
        }
    }
}

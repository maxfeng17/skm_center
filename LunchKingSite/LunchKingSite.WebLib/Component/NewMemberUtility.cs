﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.DataOrm;
using PayEasy.NetPlatform.Friends.SSO;

namespace LunchKingSite.WebLib.Component
{
    public class NewMemberUtility
    {
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ISystemProvider sys = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        protected static ILog log = LogManager.GetLogger(typeof(NewMemberUtility));
        private static ICacheProvider cache = ProviderFactory.Instance().GetProvider<ICacheProvider>();

        public static Guid DefaultBuildingGuid
        {
            get
            {
                return MemberUtilityCore.DefaultBuildingGuid;
            }
        }

        public static bool BypassSsoVerify
        {
            get
            {
                return config.BypassSsoVerify;
            }
        }

        public static void reSend(string email)
        {
            string mail = HttpUtility.UrlDecode(email);
            if (RegExRules.CheckEmail(email))
            {
                Member m = mp.MemberGet(mail);
                if (m != null && m.IsLoaded)
                {
                    string[] authPair = GenEmailAuthCode();
                    MemberAuthInfo mai = mp.MemberAuthInfoGet(m.UniqueId);
                    mai.AuthKey = authPair[0];
                    mai.AuthCode = authPair[1];
                    mai.AuthDate = DateTime.Now;

                    mp.MemberAuthInfoSet(mai);

                    // sent account confirm mail
                    MemberFacade.SendAccountConfirmMail(m.UserEmail, m.UniqueId.ToString(), mai.AuthKey, mai.AuthCode);
                }
            }
        }

        public static int checkForgetTime(string mail)
        {
            if (!string.IsNullOrWhiteSpace(mail))
            {
                Member mem = mp.MemberGet(mail);
                if (mem != null && mem.IsLoaded)
                {
                    MemberAuthInfo mai = mp.MemberAuthInfoGet(mem.UniqueId);
                    if (mai != null && mai.IsLoaded)
                    {
                        if (mai.ForgetPasswordDate.HasValue)
                        {
                            if (Equals(0, DateTime.Compare(DateTime.Now.Date, mai.ForgetPasswordDate.Value.Date)))
                            {
                                // 同一帳號，同一天內只能索取3次
                                if (mai.PasswordQueryTime.HasValue && mai.PasswordQueryTime.Value > 2)
                                {
                                    return 2;
                                }
                            }
                        }
                    }
                    else
                    {
                        return 3;
                    }

                    return 1;
                }
            }

            return 0;
        }

        /// <summary>
        /// 產生認證信的驗證碼
        /// </summary>
        /// <param name="codeLength"></param>
        /// <param name="keyLength"></param>
        /// <returns></returns>
        public static string[] GenEmailAuthCode(int codeLength = 50, int keyLength = 50)
        {
            return NewMemberUtilityCore.GenEmailAuthCode(codeLength, keyLength);
        }

        #region new pez sso

        public static void PezSsoLogout(string rtnUrl)
        {
            if (config.NewPezSso)
            {
                CASAuthentication casAuth = new CASAuthentication(new PezSsoProcess());
                casAuth.LogoutProcess(rtnUrl, config.PayEasyCasServerUrl);
            }
        }

        #endregion new pez sso

        /// <summary>
        /// 舊時代的眼淚可刪了 by unicorn
        /// 修正在Member有資料，而Membership裡的資料卻消失，這隻method是在做補資料的動作
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public static void FixMembership(string userName, string password)
        {
            if (RegExRules.CheckPassword(password) == false)
            {
                throw new Exception("密碼太過簡單，請換一組密碼.");
            }
            Member member = mp.MemberGet(userName);
            if (member.IsLoaded == false)
            {
                throw new Exception(string.Format("member {0} not found.", userName));
            }
            string memId = Membership.GetUserNameByEmail(userName);
            if (memId == null)
            {
                memId = member.UniqueId.ToString();
                try
                {
                    MembershipCreateStatus createStatus;
                    MembershipUser mu = Membership.CreateUser(memId, password, userName, null, null, true, out createStatus);
                    if (createStatus != MembershipCreateStatus.Success)
                    {
                        throw new Exception(string.Format("會員 {0} 資料修正時發生錯誤.回傳結果: {1}", userName, createStatus));
                    }

                    if (Roles.RoleExists(MemberRoles.RegisteredUser.ToString("g")))
                    {
                        Roles.AddUserToRoles(mu.Email, new string[] { MemberRoles.RegisteredUser.ToString("g") });
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("會員 {0} 資料修正時發生錯誤.", userName), ex);
                }
            }
            else
            {
                throw new Exception(string.Format("會員 {0} 資料沒問題，無需修正", userName));
            }
        }



        public static AccountAuditAction ConvertToAccountAudioAction(SingleSignOnSource singelSource)
        {
            if (singelSource == SingleSignOnSource.PayEasy)
            {
                return AccountAuditAction.LoginUsingPayezAccount;
            }
            if (singelSource == SingleSignOnSource.Facebook)
            {
                return AccountAuditAction.LoginUsingFbAccount;
            }
            if (singelSource == SingleSignOnSource.ContactDigitalIntegration)
            {
                return AccountAuditAction.LoginUsing17LifeAccount;
            }
            if (singelSource == SingleSignOnSource.Mobile17Life)
            {
                return AccountAuditAction.LoginUsingMobile17Life;
            }
            if (singelSource == SingleSignOnSource.Line)
            {
                return AccountAuditAction.LoginUsingLineAccount;
            }
            throw new Exception("singelSource is undefined.");
        }

        /// <summary>
        /// 網站使用者登入
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="singelSource"></param>
        /// <param name="isKeepLogin"></param>
        public static void UserSignIn(string userName, SingleSignOnSource singelSource, bool isKeepLogin)
        {
            HttpSessionState session = HttpContext.Current.Session;
            if (session != null && session[LkSiteSession.ExternalMemberId.ToString()] != null)
            {
                session.Remove(LkSiteSession.ExternalMemberId.ToString());
            }
            HttpResponse response = HttpContext.Current.Response;
            Member mem = mp.MemberGet(userName);
            if (mem.IsLoaded == false)
            {
                throw new MemberAccessException(string.Format("user {0} try login but data not found.", userName));
            }
            mem.LastLoginDate = DateTime.Now;
            mp.MemberSet(mem);

            CookieManager.SetVersion(mem.Version, isKeepLogin);
            CookieManager.SetMemberSource(singelSource, isKeepLogin);
            CookieManager.SetMemberDiscount(PromotionFacade.GetMemberDiscountCode(userName));
            CookieManager.SetMemberAuth(userName, isKeepLogin);

            MemberUtility.SetUserSsoInfoReady(mem.UniqueId);

            mp.AccountAuditSet(userName, Helper.GetClientIP(), ConvertToAccountAudioAction(singelSource), true, null,
                Helper.GetOrderFromType());

            MemberEventsCore.OnMemberLoggedIn(singelSource, mem.UniqueId, UserDeviceType.Web);
        }

        #region Change Email

        public static bool ChangeEmailIsSendLimitToday(int userId)
        {
            MemberAuthInfo mai = mp.MemberAuthInfoGet(userId);
            return ChangeEmailIsSendLimitToday(mai);
        }

        private static bool ChangeEmailIsSendLimitToday(MemberAuthInfo mai)
        {
            // 到達當日可寄出的上限
            if (mai.AuthDate != null && mai.AuthDate.Value.Date == DateTime.Today
                && mai.AuthQueryTime >= 3)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// 重發認證信，一日只能5次，預防被攻擊灌爆會員信箱
        /// </summary>
        /// <param name="mai"></param>
        /// <returns></returns>
        public static bool ReAuthIsSendLimitToday(MemberAuthInfo mai)
        {
            // 到達當日可寄出的上限
            if (mai.AuthDate != null && mai.AuthDate.Value.Date == DateTime.Today
                && mai.AuthQueryTime > 5)
            {
                return true;
            }
            return false;
        }

        public static bool ChangeEmailCancel(int userId)
        {
            Member mem = mp.MemberGet(userId);
            MemberAuthInfo mai = mp.MemberAuthInfoGet(userId);
            if (mem.IsLoaded == false || mai.IsLoaded == false)
            {
                log.WarnFormat("ChangeEmailCancelAuth data not found. userId = {0}, userName={1}.", userId,
                    HttpContext.Current.User.Identity.Name);
                return false;
            }
            try
            {
                mai.AuthKey = null;
                mai.AuthCode = null;
                mai.AuthDate = null;
                mai.AuthQueryTime = null;
                mai.Email = mem.UserName;
                mp.MemberAuthInfoSet(mai);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static ReAuthStatus ChangeEmailResend(int userId)
        {
            Member m = mp.MemberGet(userId);
            if (m.IsLoaded == false)
            {
                return ReAuthStatus.UserDataNotFound;
            }

            MemberAuthInfo mai = mp.MemberAuthInfoGet(userId);
            if (mai.IsLoaded == false)
            {
                return ReAuthStatus.UserDataNotFound;
            }

            if (mai.Email.Equals(m.UserName))
            {
                return ReAuthStatus.SameEmailSkip;
            }

            if (RegExRules.CheckEmail(mai.Email) == false)
            {
                log.WarnFormat("ChangeEmailReAuth User資料異常，userid={0}, email={1}", userId, mai.Email);
                return ReAuthStatus.Unavailable;
            }

            if (string.IsNullOrWhiteSpace(mai.AuthKey) || string.IsNullOrWhiteSpace(mai.AuthCode))
            {
                log.WarnFormat("ChangeEmailReAuth Auth資料異常，userid={0}", userId);
                return ReAuthStatus.InvalidAuthData;
            }

            if (mai.AuthQueryTime == null || mai.AuthDate == null)
            {
                log.WarnFormat("ChangeEmailReAuth Auth資料異常，有更新Email時必填資料卻為null，userid={0}", userId);
                return ReAuthStatus.InvalidAuthData;
            }

            if (ChangeEmailIsSendLimitToday(mai)) // 同一天只能寄3次
            {
                return ReAuthStatus.ExcceedMailLimit;
            }

            try
            {
                if (mai.AuthDate.Value.Date == DateTime.Today)
                {
                    mai.AuthQueryTime++;
                }
                else
                {
                    mai.AuthQueryTime = 1;
                }
                mp.MemberAuthInfoSet(mai);
                //寄到新信箱
                SendMail.ChangeMailNewMail(mai.Email, m.DisplayName, mai.AuthDate.Value,
                                           m.UniqueId, mai.AuthKey, mai.AuthCode);
            }
            catch
            {
                return ReAuthStatus.ServerFail;
            }

            return ReAuthStatus.OK;
        }

        public static ReAuthStatus BindingEmailResend(int userId)
        {
            Member m = mp.MemberGet(userId);
            if (m.IsLoaded == false)
            {
                return ReAuthStatus.UserDataNotFound;
            }

            MemberAuthInfo mai = mp.MemberAuthInfoGet(userId);
            if (mai.IsLoaded == false)
            {
                return ReAuthStatus.UserDataNotFound;
            }

            if (RegExRules.CheckEmail(mai.Email) == false)
            {
                log.WarnFormat("ChangeEmailReAuth User資料異常，userid={0}, email={1}", userId, mai.Email);
                return ReAuthStatus.Unavailable;
            }

            if (string.IsNullOrWhiteSpace(mai.AuthKey) || string.IsNullOrWhiteSpace(mai.AuthCode))
            {
                log.WarnFormat("ChangeEmailReAuth Auth資料異常，userid={0}", userId);
                return ReAuthStatus.InvalidAuthData;
            }

            if (mai.AuthQueryTime == null || mai.AuthDate == null)
            {
                log.WarnFormat("ChangeEmailReAuth Auth資料異常，有更新Email時必填資料卻為null，userid={0}", userId);
                return ReAuthStatus.InvalidAuthData;
            }

            if (ChangeEmailIsSendLimitToday(mai)) // 同一天只能寄3次
            {
                return ReAuthStatus.ExcceedMailLimit;
            }

            try
            {
                if (mai.AuthDate.Value.Date == DateTime.Today)
                {
                    mai.AuthQueryTime++;
                }
                else
                {
                    mai.AuthQueryTime = 1;
                }
                mp.MemberAuthInfoSet(mai);
                //寄信
                SendMail.Bind17LifeMail(mai.Email, m.DisplayName, mai.AuthDate.Value,
                                           m.UniqueId, mai.AuthKey, mai.AuthCode);
            }
            catch
            {
                return ReAuthStatus.ServerFail;
            }

            return ReAuthStatus.OK;
        }

        public static bool BindEmailCancel(int userId)
        {
            Member mem = mp.MemberGet(userId);
            MemberAuthInfo mai = mp.MemberAuthInfoGet(userId);
            if (mem.IsLoaded == false || mai.IsLoaded == false)
            {
                log.WarnFormat("BindEmailCancelAuth data not found. userId = {0}, userName={1}.", userId,
                    HttpContext.Current.User.Identity.Name);
                return false;
            }
            var mobileMember = MemberFacade.GetMobileMember(userId);
            if (!mobileMember.IsLoaded)
            {
                log.WarnFormat("BindEmailCancelAuth mobile data not found. userId = {0}, userName={1}.", userId,
                   HttpContext.Current.User.Identity.Name);
                return false;
            }
            string mid = Membership.GetUserNameByEmail(mem.UserName);
            if (mid == null)
            {
                log.WarnFormat("BindEmailCancelAuth 資料有遺失請檢查, email = {0}", mem.UserName);
                //User not exist
                return false;
            }

            var user = Membership.GetUser(mid);
            if (user == null)
            {
                log.WarnFormat("ChangeEmailSaveChange 資料有遺失請檢查, userId = {0}, mid = {1}", userId, mid);
                return false;
            }
            try
            {
                using (var trans = TransactionScopeBuilder.CreateReadCommitted())
                {
                     //更新member的user_name
                    string userName = Guid.NewGuid() + "@mobile";
                    mp.MemberSetUserName(userId, userName, false);

                    mem = mp.MemberGet(userId);

                    mem.UserEmail = string.Empty;
                    mp.MemberSet(mem);

                    mai.AuthKey = null;
                    mai.AuthCode = null;
                    mai.AuthDate = null;
                    mai.AuthQueryTime = null;
                    mai.Email = userName;
                    mp.MemberAuthInfoSet(mai);

                    user.Email = null;
                    Membership.UpdateUser(user);

                    trans.Complete();
                }

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }


        /// <summary>
        /// 會員換新信箱，重新認證，寄發認證信
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="newEmail"></param>
        public static ReAuthStatus ChangeEmailAuth(int userId, string newEmail)
        {
            if (string.IsNullOrWhiteSpace(newEmail))
            {
                return ReAuthStatus.EmailEmpty;
            }

            Member m = mp.MemberGet(userId);
            if (m.IsLoaded == false)
            {
                return ReAuthStatus.UserDataNotFound;
            }

            MemberAuthInfo mai = mp.MemberAuthInfoGet(userId);
            if (mai.IsLoaded == false)
            {
                //建立新的 member auth info，沒有的原因是他payeasy或fb登入的
                mai.UniqueId = m.UniqueId;
                mai.IsNew = true;
            }

            if (m.UserName.Equals(newEmail, StringComparison.OrdinalIgnoreCase))
            {
                return ReAuthStatus.SameEmailSkip;
            }

            var mailAvailable = MemberUtilityCore.CheckMailAvailable(newEmail, true);
            if (mailAvailable == EmailAvailableStatus.IllegalFormat)
            {
                return ReAuthStatus.IllegalMailFormat;
            }
            else if (mailAvailable == EmailAvailableStatus.DisposableEmailAddress)
            {
                return ReAuthStatus.DisposableEmailAddress;
            }
            else if (mailAvailable != EmailAvailableStatus.Available)
            {
                return ReAuthStatus.Unavailable;
            }

            try
            {
                string[] authPair = GenEmailAuthCode();
                mai.Email = newEmail;
                mai.AuthKey = authPair[0];
                mai.AuthCode = authPair[1];
                mai.AuthDate = DateTime.Now;
                mai.AuthQueryTime = 1;
                mp.MemberAuthInfoSet(mai);

                SendMail.ChangeMailNewMail(mai.Email, m.DisplayName, mai.AuthDate.Value, m.UniqueId,
                                           mai.AuthKey, mai.AuthCode);
                try
                {
                    SendMail.ChangeMailOldMail(m.UserEmail, m.DisplayName, mai.AuthDate.Value, newEmail);
                }
                catch (Exception ex)
                {
                    log.WarnFormat("會員更改Email，舊信箱 {0} 的通知信無法送達, ex={1}",
                        m.UserName, ex);
                }
                return ReAuthStatus.OK;
            }
            catch (Exception ex)
            {
                log.WarnFormat("ChangeEmailAuth Error, userId={0}, newEmail={1}, ex={2}",
                               userId, newEmail, ex);

                return ReAuthStatus.ServerFail;
            }
        }

        public static bool ChangeEmailSaveChange(int userId)
        {
            var mai = mp.MemberAuthInfoGet(userId);
            var mem = mp.MemberGet(userId);
            if (mem.UserName.Equals(mai.Email))
            {
                throw new Exception("email same, needn't change.");
            }
            bool result;
            try
            {
                result = ChangeEmailSaveChange(userId, mai.Email);
                if (result)
                {
                    MemberFacade.CancelAppLoginToken(userId);
                    SendMail.ChangeMailSuccessNewMail(mai.Email, mem.DisplayName);
                    try
                    {
                        SendMail.ChangeMailSuccessOldMail(mem.UserEmail, mem.DisplayName, mai.AuthDate.Value,
                            mai.Email);
                    }
                    catch (Exception ex)
                    {
                        log.WarnFormat("會員更改Email，舊信箱 {0} 的「完成」通知信無法送達, ex={1}",
                            mem.UserEmail, ex);
                    }
                    //Email更動後，簡易登出
                    //會做登出，意味此Method 是使用者專用，如後臺功能要使用，即應該對登出動作做適度調整。
                    UserSignOutLite();
                }
            }
            catch (Exception ex)
            {
                result = false;
                log.WarnFormat("使用者在更新email時發生問題，userid = {0}, ex={1} ", userId, ex);
            }
            return result;
        }

        public static RegisterContactMemberReplyData RegisterMobileMember(string mobile, string userEmail)
        {
            IMemberRegister register = new MobileMemberRegister(mobile, userEmail);
            MemberRegisterReplyType replyType = register.Process(false);
            var result = new RegisterContactMemberReplyData(replyType, register.Message);
            if (replyType == MemberRegisterReplyType.RegisterSuccess)
            {
                result.UserName = register.UserName;
            }
            return result;
        }

        public static bool ChangeEmailSaveChange(int userId, string newEmail)
        {
            //check new email was used?
            var chkMem = mp.MemberGet(newEmail);
            if (chkMem.IsLoaded)
            {
                //NewEmail was used
                return false;
            }

            string chkMid = Membership.GetUserNameByEmail(newEmail);
            if (string.IsNullOrEmpty(chkMid) == false)
            {
                //NewEmail was used
                return false;
            }

            // get user
            var mem = mp.MemberGet(userId);
            if (mem.IsLoaded == false)
            {
                //User not exist
                return false;
            }

            if (string.Equals(mem.UserName, newEmail, StringComparison.OrdinalIgnoreCase))
            {
                //Same Email
                return false;
            }

            string mid = Membership.GetUserNameByEmail(mem.UserName);
            if (mid == null)
            {
                log.WarnFormat("ChangeEmailSaveChange 資料有遺失請檢查, email = {0}", mem.UserName);
                //User not exist
                return false;
            }

            var user = Membership.GetUser(mid);
            if (user == null)
            {
                log.WarnFormat("ChangeEmailSaveChange 資料有遺失請檢查, userId = {0}, mid = {1}", userId, mid);
                return false;
            }
            user.Email = newEmail;
            using (var trans = TransactionScopeBuilder.CreateReadCommitted())
            {
                string oldEmail = mem.UserName;

                //針對17life和payeasy員工 刪除其他角色權限
                if ((oldEmail.Contains("17life.com", StringComparison.InvariantCultureIgnoreCase) && !newEmail.Contains("17life.com", StringComparison.InvariantCultureIgnoreCase))
                       || (oldEmail.Contains("payeasy.com.tw", StringComparison.InvariantCultureIgnoreCase) && !newEmail.Contains("payeasy.com.tw", StringComparison.InvariantCultureIgnoreCase)))
                {
                    string old_roles = string.Join(",", Roles.GetRolesForUser(oldEmail));
                    Roles.RemoveUserFromRoles(oldEmail, Roles.GetAllRoles().Where(x => !x.Equals(MemberRoles.RegisteredUser.ToString(), StringComparison.InvariantCultureIgnoreCase)).ToArray());
                    MailMessage msg = new MailMessage();
                    msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                    msg.To.Add(new MailAddress("margo_huang@17life.com"));
                    msg.Subject = "員工更換email通知";
                    msg.Body = string.Format("{0}變更新email為{1}，原權限設定為{2}，已刪除RegisteredUser以外權限，請再檢查", oldEmail, newEmail, old_roles);
                    msg.IsBodyHtml = true;
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }

                // member 變更 user_name ，因為是主鍵，所以必需特別處理
                mp.MemberSetUserName(userId, newEmail, false);
                Membership.UpdateUser(user);

                //有關訂閱，訂閱的 email 並未強制跟user綁定
                pp.SubscriptionSetToNewUser(mem.UserEmail, newEmail);
                hp.HiDealSubscriptionSetToNewUser(mem.UserEmail, newEmail);

                //2011年即未使用該table，維持既有更新方式
                mp.BonusTransactionSetToNewUser(mem.UserName, newEmail);
                //午餐王用到的table，處理方式照舊，資料接不起來隨風而逝也是剛好
                mp.RedeemOrderSetToNewUser(mem.UserName, newEmail);

                //reload member
                mem = mp.MemberGet(userId);
                MemberFacade.LogChangeEmail(mem.UniqueId, mem.UserName, oldEmail);
                trans.Complete();
            }

            return true;
        }

        /// <summary>
        /// 簡易的，只做本機端登入資料的清除
        /// </summary>
        public static void UserSignOutLite()
        {
            HttpRequest request = HttpContext.Current.Request;
            HttpResponse response = HttpContext.Current.Response;
            HttpSessionState session = HttpContext.Current.Session;
            FormsAuthentication.SignOut();
            foreach (string key in request.Cookies.AllKeys)
            {
                response.Cookies[key].Expires = DateTime.Now;
            }
            if (session != null)
            {
                session.Abandon();
            }
        }

        internal class SendMail
        {
            internal static void ChangeMailNewMail(string mailTo, string memberName, DateTime applyDate, int userId,
                string authKey, string authCode)
            {
                MailMessage msg = new MailMessage();
                ChangeMailNewMail template = TemplateFactory.Instance().GetTemplate<ChangeMailNewMail>();
                template.ApplyDate = applyDate.ToShortDateString();

                string relUrl = string.Format("NewMember/ChangePostAddress.aspx?uid={0}&key={1}&code={2}",
                                              userId, authKey, authCode);
                template.AuthUrl = Helper.CombineUrl(config.SiteUrl, relUrl);
                template.MemberName = memberName;
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = config.SiteServiceUrl;

                msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                msg.To.Add(new MailAddress(mailTo, memberName));
                msg.Subject = "變更信箱認證信";
                msg.Body = template.ToString();
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }

            internal static void ChangeMailOldMail(string mailTo, string memberName, DateTime applyDate, string newMail)
            {
                MailMessage msg = new MailMessage();
                ChangeMailOldMail template = TemplateFactory.Instance().GetTemplate<ChangeMailOldMail>();
                template.ApplyDate = applyDate.ToShortDateString();
                template.NewMail = newMail;
                template.MemberName = memberName;
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = config.SiteServiceUrl;

                msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                msg.To.Add(new MailAddress(mailTo, memberName));
                msg.Subject = "會員信箱變更申請通知";
                msg.Body = template.ToString();
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }

            internal static void ChangeMailSuccessNewMail(string mailTo, string memberName)
            {
                MailMessage msg = new MailMessage();
                ChangeMailSuccessNewMail template = TemplateFactory.Instance().GetTemplate<ChangeMailSuccessNewMail>();
                template.MemberName = memberName;
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = config.SiteServiceUrl;

                msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                msg.To.Add(new MailAddress(mailTo, memberName));
                msg.Subject = "會員信箱變更成功通知";
                msg.Body = template.ToString();
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }

            internal static void ChangeMailSuccessOldMail(string mailTo, string memberName, DateTime applyDate,
                string newMail)
            {
                MailMessage msg = new MailMessage();
                ChangeMailSuccessOldMail template = TemplateFactory.Instance().GetTemplate<ChangeMailSuccessOldMail>();
                template.ApplyDate = applyDate.ToShortDateString();
                template.NewMail = newMail;
                template.MemberName = memberName;
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = config.SiteServiceUrl;

                msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                msg.To.Add(new MailAddress(mailTo, memberName));
                msg.Subject = "會員信箱已變更";
                msg.Body = template.ToString();
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }

            internal static void Bind17LifeMail(string mailTo, string memberName, DateTime applyDate, int userId,
             string authKey, string authCode)
            {
                MailMessage msg = new MailMessage();
                BindConfirmMail template = TemplateFactory.Instance().GetTemplate<BindConfirmMail>();
                template.ApplyDate = applyDate.ToShortDateString();

                string relUrl = string.Format("NewMember/memberauth.aspx?uid={0}&key={1}&code={2}",
                                              userId, authKey, authCode);
                template.AuthUrl = Helper.CombineUrl(config.SiteUrl, relUrl);
                template.MemberName = memberName;
                template.SiteUrl = config.SiteUrl;
                template.SiteServiceUrl = config.SiteServiceUrl;

                msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
                msg.To.Add(new MailAddress(mailTo, memberName));
                msg.Subject = "開通信箱認證信";
                msg.Body = template.ToString();
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }

        #endregion Change Email

        #region Bind Email

        /// <summary>
        /// 手機會員帳號設定綁定17Life
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static ReAuthStatus BindEmailSend(int userId, string email)
        {
            Member m = mp.MemberGet(userId);
            if (m.IsLoaded == false)
            {
                return ReAuthStatus.UserDataNotFound;
            }

            MemberAuthInfo mai = mp.MemberAuthInfoGet(userId);
            if (mai.IsLoaded == false)
            {
                mai.UniqueId = m.UniqueId;
                mai.IsNew = true;
            }

            try
            {
                string[] authPair = GenEmailAuthCode();
                mai.Email = email;
                mai.AuthKey = authPair[0];
                mai.AuthCode = authPair[1];
                mai.AuthDate = DateTime.Now;
                mai.AuthQueryTime = 1;
                mp.MemberAuthInfoSet(mai);

                SendMail.Bind17LifeMail(mai.Email, m.DisplayName, mai.AuthDate.Value, m.UniqueId,
                                           mai.AuthKey, mai.AuthCode);

                return ReAuthStatus.OK;
            }
            catch (Exception ex)
            {
                log.WarnFormat("BindEmailSend Error, userId={0}, newEmail={1}, ex={2}",
                               userId, m.UserEmail, ex);

                return ReAuthStatus.ServerFail;
            }
        }

        public static ReAuthStatus SetPasswordAndBindEmailSend(int userId, string password)
        {
            Member mem = mp.MemberGet(userId);
            MemberFacade.ResetMemberPassword(mem.UserName, null, password, ResetPasswordReason.Bind17Link, true);
            return BindEmailSend(userId, mem.UserEmail);
        }

        #endregion

        public static void SetPasswordAndAutoActive(int userId, string password)
        {
            Member mem = mp.MemberGet(userId);
            MemberFacade.ResetMemberPassword(mem.UserName, null, password, ResetPasswordReason.Bind17Link, true);
            MemberFacade.SetMemberActive(userId);
            //MemberFacade.AddDefaultMemberAuthInfo(userId, mem.UserName);
        }

        /// <summary>
        /// 儲存註冊時用的驗證碼，效期20分鍾
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string StoreCaptcha(string key, string number)
        {
            string cacheKey = "captcha-register::" + key;
            cache.Set(cacheKey, number, TimeSpan.FromMinutes(20));
            return key;
        }
        /// <summary>
        /// 驗證註冊時用的驗證碼
        /// </summary>
        /// <param name="key"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        public static bool ValidateCaptcha(string key, string number)
        {
            if (string.IsNullOrEmpty(key))
            {
                return false;
            }
            string cacheKey = "captcha-register::" + key;
            string result = cache.Get<string>(cacheKey);
            if (string.IsNullOrEmpty(number) == false && result == number)
            {
                return true;
            }
            return false;
        }

        public static bool ValidateFacebookStat(string stat)
        {
            return true;
        }
    }

    public class PezSsoProcess : IAuthProcess
    {
        public PezSsoProcess()
        {
        }

        #region IAuthProcess 成員

        /// <summary>
        /// CAS 登入成功後的使用者授權及cas ticket記錄
        /// </summary>
        /// <param name="memNum"></param>
        /// <param name="casTicket"></param>
        void IAuthProcess.UserLogin(long memNum, string casTicket)
        {
            // 建立使用者識別資訊.配合實作需求,第3個參數可加入額外的使用者資訊(存入Data欄位),可由userInfo.Data取得
            UserInfo userInfo = new UserInfo(memNum, casTicket, string.Empty);

            // Form 認證(以使用者識別資訊做內容),FormName=MemNum+CasTicket+Data(可回復為UserInfo再取值) // form timeout : 30min
            DateTime expire = DateTime.Now.AddMinutes(30);

            // 保存使用者識別(single sign out 時用)
            // 過期時間設的和 form ticket 一樣即可
            AuthData authData = new AuthData(userInfo, expire);
            AuthDataManager.Insert(authData);
        }

        /// <summary>
        /// 接收 CAS Server 的登出通知的後續處理
        /// </summary>
        /// <param name="casTicket"></param>
        void IAuthProcess.LogoutRequest(string casTicket)
        {
            // 移除登入記錄(使用者識別).在Global.asax中會檢查是否有被移除,並登出使用者.
            bool result = AuthDataManager.Remove(casTicket);
        }

        #endregion IAuthProcess 成員
    }
}
﻿using System.Globalization;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System.Web.Http;
using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using System.Linq;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.WebLib.Component.ReqularsSystem.OAuth
{
    /// <summary>
    /// 
    /// </summary>
    public class RegularsOAuthScopeAttribute : System.Web.Mvc.ActionFilterAttribute
    {
        private ISysConfProvider config;
        private IPCPProvider pcp;
        private IMemberProvider mp;
        private TokenScope[] _scope;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scope"></param>
        public RegularsOAuthScopeAttribute(params TokenScope[] scope)
        {
            this._scope = scope;
            config = ProviderFactory.Instance().GetConfig();
            pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //HttpRequestBase request = filterContext.RequestContext.HttpContext.Request;
            string accessToken = null;
            bool isFromVbsSystem = true;
            int sellerUserId = 0;
            if (HttpContext.Current.Session[VbsSession.SellerAppApiToken.ToString()] == null)
            {
                #region Session內沒有SellerApiToken的話，改從header取token
                if (HttpContext.Current.Request["access_token"] != null)
                {
                    accessToken = HttpContext.Current.Request["access_token"];
                }
                else if (HttpContext.Current.Request["accessToken"] != null)
                {
                    accessToken = HttpContext.Current.Request["accessToken"];
                }
                else if (HttpContext.Current.Request.RequestContext.RouteData.Values["accessToken"] != null) //RouteData
                {
                    accessToken = Convert.ToString(HttpContext.Current.Request.RequestContext.RouteData.Values["accessToken"]);
                }
                else
                {
                    //from header Authorization
                    NameValueCollection headerList = HttpContext.Current.Request.Headers;
                    var authorizationField = headerList.Get("Authorization");
                    if (authorizationField != null)
                    {
                        accessToken = authorizationField.Replace("Bearer ", "");
                    }
                }
                #endregion

                #region 判斷是從PC還是App進入，取得缺少的資訊
                if (!string.IsNullOrEmpty(accessToken))
                {
                    //從【商家核銷App】登入：用Token取得SellerUserId，並存入Session
                    isFromVbsSystem = false;
                    Member member = MemberFacade.GetMemberByAccessToken(accessToken);
                    HttpContext.Current.Session[VbsSession.UniqueId.ToString()] = sellerUserId = member.UniqueId;
                    HttpContext.Current.Session[VbsSession.UserId.ToString()] = member.UserName.Replace("@vbs", string.Empty);
                }
                else
                {
                    //從【商家核銷系統】登入：用Account產Token
                    string vbsAccount = Convert.ToString(HttpContext.Current.Session[VbsSession.UserId.ToString()]);
                    if (!string.IsNullOrWhiteSpace(vbsAccount))
                    {
                        sellerUserId = MemberFacade.GetUniqueId(vbsAccount + "@vbs");
                        accessToken = OAuthFacade.CreateMemberToken(config.Vbs17LifeOauthClientId, Convert.ToInt32(sellerUserId)).AccessToken;
                    }
                }
                #endregion

                OauthToken token = CommonFacade.OauthTokenGet(accessToken);
                ApiResultCode resultCode = CommonFacade.TokenResultToApiResultCode(CommonFacade.CheckToken(token, this._scope, false));

                if (resultCode == ApiResultCode.Success)
                {
                    //驗證成功才將accessToken存入Session
                    string accountId = Convert.ToString(HttpContext.Current.Session[VbsSession.UserId.ToString()]);
                    var resourceAcl = mp.ResourceAclGetByAccountIdAndResourceType(accountId, (int)ResourceType.Seller);
                    HttpContext.Current.Session[VbsSession.SellerAppApiToken.ToString()] = accessToken;

                    //取得商家帳號角色
                    CheckAndSetVbsRole(sellerUserId);
                    //取得商家帳號權限
                    CheckAndSetVbsPermission(accountId);

                }
                else
                {
                    if (isFromVbsSystem)
                    {
                        filterContext.Result = new RedirectResult("/vbs/Login");
                    }
                    else
                    {
                        ViewDataDictionary viewDataDic = new ViewDataDictionary();
                        viewDataDic.Add("ErrorMsg", "登入資訊過期，請重新登入。");
                        viewDataDic.Add("ScriptFunctionName", "returnHome");
                        filterContext.Result = new ViewResult() { ViewData = viewDataDic, ViewName = "RegularsErrorPage" };
                    }
                }
            }

            base.OnActionExecuting(filterContext);
        }

        private void CheckAndSetVbsRole(int sellerUserId)
        {
            var vbsRoleList = mp.VbsRoleGetList(sellerUserId);
            HttpContext.Current.Session[VbsSession.VbsRole.ToString()] = vbsRoleList.Select(x => (VendorRole)Enum.Parse(typeof(VendorRole), x.RoleName, true)).ToList();
        }
        /// <summary>
        /// 檢查並設置帳號對熟客卡相關功能的權限(編輯/瀏覽)
        /// </summary>
        private void CheckAndSetVbsPermission(string accountId)
        {
            var permissionList = VBSFacade.GetVbsMembershipPermissionList(accountId);
            HttpContext.Current.Session[VbsSession.VbsMembershipPermission.ToString()] = permissionList.Select(x => (VbsMembershipPermissionScope)x.Scope).ToList();
        }

    }


    /// <summary>
    /// 驗證熟客相關權限設定
    /// </summary>
    /// <remarks>
    /// 先暫時放這隻檔案，之後整合商家系統後，確定要用了再獨立新的檔
    /// </remarks>
    public class VbsPermissionAttribute : System.Web.Mvc.ActionFilterAttribute
    {
        private VbsMembershipPermissionScope _permission { get; set; }
        public VbsPermissionAttribute(VbsMembershipPermissionScope permission)
        {
            this._permission = permission;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var permissions = (List<VbsMembershipPermissionScope>)HttpContext.Current.Session[VbsSession.VbsMembershipPermission.ToString()];

            //沒權限的話，導回上一頁
            if (!permissions.Contains(this._permission))
            {
                if (HttpContext.Current.Request.UrlReferrer != null && !string.IsNullOrWhiteSpace(HttpContext.Current.Request.UrlReferrer.AbsoluteUri))
                {
                    filterContext.Result = new RedirectResult(HttpContext.Current.Request.UrlReferrer.AbsoluteUri);
                }
                else
                {
                    //上一頁有可能是App Native，這樣的話就必須靠js interface導回上一頁
                    ViewDataDictionary viewDataDic = new ViewDataDictionary();
                    viewDataDic.Add("ErrorMsg", "您的帳號無任何熟客系統的瀏覽或編輯權限。");
                    viewDataDic.Add("ScriptFunctionName", "returnHome");
                    filterContext.Result = new ViewResult() { ViewData = viewDataDic, ViewName = "RegularsErrorPage" };
                }
            }
            base.OnActionExecuting(filterContext);
        }
    }

    public class VbsRoleValidateAttribute : System.Web.Mvc.ActionFilterAttribute
    {
        public List<VendorRole> _roles { get; set; }
        public VbsRoleValidateAttribute(params VendorRole[] roles)
        {
            this._roles = new List<VendorRole>();
            for (int i = 0; i < roles.Length; i++)
            {
                this._roles.Add(roles[i]);
            }
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //取得角色
            var vbsRoles = (List<VendorRole>)HttpContext.Current.Session[VbsSession.VbsRole.ToString()];

            var notInRole = this._roles.Intersect(vbsRoles).Count() == 0;
            //不是該角色的話，導回上一頁
            if (notInRole)
            {
                if (HttpContext.Current.Request.UrlReferrer != null && !string.IsNullOrWhiteSpace(HttpContext.Current.Request.UrlReferrer.AbsoluteUri))
                {
                    filterContext.Result = new RedirectResult(HttpContext.Current.Request.UrlReferrer.AbsoluteUri);
                }
                else
                {
                    //上一頁有可能是App Native，這樣的話就必須靠js interface導回上一頁
                    ViewDataDictionary viewDataDic = new ViewDataDictionary();
                    viewDataDic.Add("ErrorMsg", "您的帳號無頁面瀏覽權限。");
                    viewDataDic.Add("ScriptFunctionName", "returnHome");
                    filterContext.Result = new ViewResult() { ViewData = viewDataDic, ViewName = "RegularsErrorPage" };
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}

﻿using System;
using System.Web;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using Microsoft.SqlServer.Types;
using Newtonsoft.Json;

namespace LunchKingSite.WebLib.Component
{
    public static class MvcPageExtensions
    {
        public static IHtmlString Raw(this HtmlHelper htmlHelper, bool condition, string str)
        {
            if (condition && str != null)
            {
                return MvcHtmlString.Create(str);
            }
            return MvcHtmlString.Empty;
        }

        public static IHtmlString Raw(this HtmlHelper htmlHelper, bool condition, object obj)
        {
            if (condition && obj != null)
            {
                return MvcHtmlString.Create(obj.ToString());
            }
            return MvcHtmlString.Empty;
        }

        public static IHtmlString Raw(this HtmlHelper htmlHelper, bool condition, string showStr1, string showStr2)
        {
            if (condition && showStr1 != null)
            {
                return MvcHtmlString.Create(showStr1);
            }
            if (condition == false && showStr2 != null)
            {
                return MvcHtmlString.Create(showStr2);
            }
            return MvcHtmlString.Empty;
        }

        public static IHtmlString Raw(this HtmlHelper htmlHelper, bool condition, object showData1, object showData2)
        {
            if (condition && showData1 != null)
            {
                return MvcHtmlString.Create(showData1.ToString());
            }
            if (condition == false && showData2 != null)
            {
                return MvcHtmlString.Create(showData2.ToString());
            }
            return MvcHtmlString.Empty;
        }

        public static IHtmlString Raw(this HtmlHelper htmlHelper, string format, params object[] args)
        {
            string output = string.Format(format, args);
            return htmlHelper.Raw(output);
        }

        public static IHtmlString Address(this HtmlHelper htmlHelper, int? townshipId, string addressString)
        {
            string addr = CityManager.CityTownShopStringGet(townshipId != null ? townshipId.Value : -1) + addressString;
            return MvcHtmlString.Create(addr);
        }

        public static IHtmlString Coordinate(this HtmlHelper htmlHelper, string coordinate, string format)
        {
            SqlGeography geo = LocationFacade.GetGeographyByCoordinate(coordinate);
            if (geo == null)
            {
                return MvcHtmlString.Empty;
            }
            return MvcHtmlString.Create(string.Format(format, geo.Lat, geo.Long));
        }

        public static IHtmlString Url(this HtmlHelper htmlHelper, string format, params object[] args)
        {
            string s = string.Format(format, args);
            string url = VirtualPathUtility.ToAbsolute(s);
            return htmlHelper.Raw(url);
        }

        public static IHtmlString Date(this HtmlHelper htmlHelper, DateTime? dt)
        {
            if (dt == null)
            {
                return MvcHtmlString.Empty;
            }
            return MvcHtmlString.Create(dt.Value.ToString("yyyy/MM/dd"));
        }

        /// <summary>
        /// EX: $30,200.50
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public static IHtmlString Dollar(this HtmlHelper htmlHelper, decimal? d)
        {
            if (d == null)
            {
                return MvcHtmlString.Empty;
            }
            return MvcHtmlString.Create(d.Value.ToString("$##,###,##0"));
        }

        public static IHtmlString SerializeJson(this HtmlHelper htmlHelper, object model)
        {
            string jsonStr = JsonConvert.SerializeObject(model);
            string encodedStr = HttpUtility.JavaScriptStringEncode(JsonConvert.SerializeObject(model), true);
            return MvcHtmlString.Create(encodedStr);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Component
{
    public class LkModelMetadataProvider : DataAnnotationsModelMetadataProvider
    {
        protected override ModelMetadata CreateMetadata(IEnumerable<Attribute> attributes, Type containerType, Func<object> modelAccessor, Type modelType, string propertyName)
        {
            var result = base.CreateMetadata(attributes, containerType, modelAccessor, modelType, propertyName);
            
            if(propertyName == null)
            {
                return result;
            }

            if(result.ModelType == typeof(string))
            {
                result.ConvertEmptyStringToNull = false;
            }
            return result;
        }
    }

    public class BooleanBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (value != null)
            {
                if (value.AttemptedValue == "1")
                {
                    return true;
                }
                else if (value.AttemptedValue == "0")
                {
                    return false;
                }
            }
            return base.BindModel(controllerContext, bindingContext);
        }
    }
}

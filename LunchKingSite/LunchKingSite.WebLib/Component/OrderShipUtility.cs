﻿using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Linq;

namespace LunchKingSite.WebLib.Component
{
    public class OrderShipUtility
    {
        protected static IOrderProvider _op;
        protected static IMemberProvider _mp;
        protected static IPponProvider _pp;
        protected static ILog _log;
        protected static ISysConfProvider _conf;
        static OrderShipUtility()
        {
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _conf = ProviderFactory.Instance().GetConfig();
            _log = LogManager.GetLogger("OrderShipUtility");
        }

        //檢查出貨資訊是否已存在(新增時檢查:避免重複按下儲存按鈕):一般出貨只允許有一筆出貨資料
        public static bool CheckShipDataExist(Guid orderGuid, OrderClassification orderClassification)
        {
            return (_op.OrderShipGetListByOrderGuid(orderGuid)
                        .Any(x => x.Type == (int)OrderShipType.Normal && x.OrderClassification.Equals((int)orderClassification)));
        }

        //檢查輸入之貨運單號是否在該檔次已存在:一般出貨才檢查
        public static bool CheckDealShipNoExist(Guid productGuid, OrderClassification orderClassification, Guid orderGuid, string shipNo)
        {
            //若同一檔次 不同訂單但收件地址相同 可允許輸入相同物流編號
            string deliveryAddress = _op.ViewOrderShipListGetListByProductGuid(productGuid, orderClassification)
                                        .Where(x => x.OrderGuid.Equals(orderGuid))
                                        .Select(x => x.DeliveryAddress)
                                        .FirstOrDefault();
            return (_op.ViewOrderShipListGetListByProductGuid(productGuid, orderClassification)
                        .Any(x =>
                            {
                                if (!x.OrderGuid.Equals(orderGuid))
                                {
                                    if (!string.IsNullOrEmpty(x.ShipNo))
                                    {
                                        return x.ShipNo.Equals(shipNo) && !x.DeliveryAddress.Equals(deliveryAddress);
                                    }

                                    return false;
                                }

                                return false;
                            }));
        }

        //新增order_ship
        public static bool OrderShipSet(OrderShip orderShipInfo)
        {
            bool result;
            try
            {
                orderShipInfo.Id = _op.OrderShipSet(orderShipInfo);
                result = true;
            }
            catch
            {
                result = false;
            }

            if (result)
            {
                //新增order_ship_log:order_ship異動紀錄
                result = OrderShipLogSet(orderShipInfo);

                #region 資策會訂單紀錄
                try
                {
                    //同部異動資策會訂單，標註要更新到資策會那邊
                    _op.CompanyUserOrderUpdateWaitUpdate(orderShipInfo.OrderGuid, true);
                }
                catch (Exception ex)
                {
                    _log.Error("異動companyUserOrder狀態時發生錯誤",ex);
                }
                #endregion 資策會訂單紀錄
            }

            return result;
        }

        //更新order_ship
        public static bool OrderShipUpdate(OrderShip orderShipInfo)
        {
            bool result;
            result = _op.OrderShipUpdate(orderShipInfo);

            if (result)
            {
                //新增order_ship_log:order_ship異動紀錄
                result = OrderShipLogSet(orderShipInfo);
                #region 資策會訂單紀錄
                try
                {
                    //同部異動資策會訂單，標註要更新到資策會那邊
                    _op.CompanyUserOrderUpdateWaitUpdate(orderShipInfo.OrderGuid, true);
                }
                catch (Exception ex)
                {
                    _log.Error("異動companyUserOrder狀態時發生錯誤", ex);
                }
                #endregion 資策會訂單紀錄
            }
            return result;
        }

        //新增order_ship_log : order_ship異動紀錄
        public static bool OrderShipLogSet(OrderShip orderShipInfo)
        {
            OrderShipLog osl = new OrderShipLog
            {
                OrderShipId = orderShipInfo.Id,
                ShipCompanyId = orderShipInfo.ShipCompanyId,
                ShipNo = orderShipInfo.ShipNo,
                ShipTime = orderShipInfo.ShipTime,
                CreateId = orderShipInfo.ModifyId,
                CreateTime = orderShipInfo.ModifyTime,
                ShipMemo = orderShipInfo.ShipMemo,
                Type = orderShipInfo.Type
            };
            return _op.OrderShipLogSet(osl);
        }

        public static bool ExchangeOrderShipSet(OrderShip orderShipInfo, OrderReturnList exchangeLog)
        {
            bool result;
            if (orderShipInfo.Type != (int)OrderShipType.Exchange)
            {
                return false;
            }
            try
            {
                var orderShipId = _op.OrderShipSet(orderShipInfo);
                orderShipInfo.Id = orderShipId > 1 ? orderShipId : orderShipInfo.Id;
                //新增order_ship_log:order_ship異動紀錄
                result = OrderShipLogSet(orderShipInfo);

                if (exchangeLog != null)
                {
                    var isNew = false;
                    if (!exchangeLog.OrderShipId.HasValue)
                    {
                        if (orderShipInfo.Id <= 1)
                        {
                            return false;
                        }

                        isNew = true;
                        exchangeLog.OrderShipId = orderShipInfo.Id;
                    }

                    string shipCompanyName = _op.ShipCompanyGet(orderShipInfo.ShipCompanyId ?? 0).ShipCompanyName;
                    exchangeLog.Message = string.Format("出貨資訊{0}：出貨日期->{1}, 貨運單號->{2}, 物流公司->{3}, 出貨備註->{4}",
                        isNew ? "新增" : "異動",
                        orderShipInfo.ShipTime.HasValue ? orderShipInfo.ShipTime.Value.ToString("yyyy/MM/dd") : string.Empty,
                        orderShipInfo.ShipNo, shipCompanyName, orderShipInfo.ShipMemo);
                    exchangeLog.MessageUpdate = false;
                    exchangeLog.ModifyId = orderShipInfo.ModifyId;
                    exchangeLog.ModifyTime = orderShipInfo.ModifyTime;

                    _op.OrderReturnListSet(exchangeLog);
                    PaymentFacade.SaveOrderStatusLog(exchangeLog);
                }

                result = true;
            }
            catch
            {
                result = false;
            }

            return result;
        }

        public static System.Collections.Generic.List<Models.VendorBillingSystem.VbsOrderListInfo> GetIspReturnOrder(
            System.Collections.Generic.List<OrderProductDelivery> opds,
            int? pageStart,
            string orderId,
            string dealUniqueId,
            string dateType,
            string dateStart,
            string dateEnd,
            int csv,
            int abnormal)
        {
            
            opds = opds.Where(x => x.ReturnConfirmTime == null).ToList();

            #region 超商類型
            if (csv == (int)ProductDeliveryType.FamilyPickup)
            {
                opds = opds.Where(p => p.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup).ToList();
            }
            else if (csv == (int)ProductDeliveryType.SevenPickup)
            {
                opds = opds.Where(p => p.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup).ToList();
            }
            #endregion
            #region 異常類型
            //驗退
            if (abnormal == 1)
            {
                opds = opds.Where(p => p.DcReturn != 1).ToList();
            }
            //刷退
            else if (abnormal == 2)
            {
                opds = opds.Where(p => p.DcReturn == 1).ToList();
            }
            #endregion

            System.Collections.Generic.List<Models.VendorBillingSystem.VbsOrderListInfo> resultData = new System.Collections.Generic.List<Models.VendorBillingSystem.VbsOrderListInfo>();

            OrderCollection orders = _op.OrderGet(opds.Select(x => x.OrderGuid).ToList());

            DateTime? shipToStockTime = null;

            foreach (OrderProductDelivery opd in opds)
            {

                CashTrustLogCollection ctls = _mp.CashTrustLogListGetByOrderId(opd.OrderGuid);
                OrderShipCollection osc = _op.OrderShipGetListByOrderGuid(opd.OrderGuid);
                if (osc.Count() > 0)
                {
                    shipToStockTime = osc.Where(x=>x.Type == (int)OrderShipType.Normal).OrderByDescending(p => p.Id).FirstOrDefault().ShipToStockTime;
                }
                
                //檢查dc_return=0 & ship_to_stock_time is null or dc_return<>0
                if (opd.DcReturn != 0 || opd.DcReturn == 0 & shipToStockTime == null)
                {

                    Guid bid = ctls.FirstOrDefault().BusinessHourGuid ?? Guid.Empty;
                    IViewPponDeal theDeal = BizLogic.Component.ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);

                    System.Collections.Generic.List<int> uniqueIds = new System.Collections.Generic.List<int>();
                    uniqueIds.Add(theDeal.UniqueId.Value);
                    var orderProductOptionLists = _op.ViewOrderProductOptionListGetIsCurrentList(uniqueIds);
                    var orderItemLists = _op.GetOrderProductOptionList(orderProductOptionLists)
                                                .GroupBy(x => x.OrderGuid)
                                                .Select(x => new
                                                {
                                                    x.Key,
                                                    OptionDescription = string.Join("\n", x.Select(o => o.OptionDescription)),
                                                    ItemNo = string.Join("\n", x.Select(o => o.ItemNo.Trim(','))),
                                                    Quantity = string.Join("\n", x.Select(o => o.Quantity)),
                                                    ItemCount = x.Sum(o => o.Quantity)
                                                })
                                                .ToDictionary(x => x.Key, x => x);


                    var dealCost = _pp.DealCostGetList(bid);

                    Order order = orders.Where(x => x.Guid == opd.OrderGuid).FirstOrDefault();
                    if (order == null)
                    {
                        continue;
                    }

                    Models.VendorBillingSystem.VbsOrderListInfo info = new Models.VendorBillingSystem.VbsOrderListInfo();
                    info.ReturnType = opd.DcReturn == 1 ? "刷退" : "驗退";
                    if (info.DealInfo == null)
                    {
                        info.DealInfo = new WebLib.Models.VendorBillingSystem.DealSaleInfo();
                    }

                    info.ProductDeliveryType = opd.ProductDeliveryType;
                    info.DealInfo.DealId = bid;
                    info.DealInfo.DealUniqueId = theDeal.UniqueId.GetValueOrDefault(0);
                    info.DealInfo.DealName = theDeal.ItemName;
                    info.OrderId = order.OrderId;
                    info.OrderGuid = order.Guid;
                    info.IspStoreName = GetIspStoreName(opd);
                    info.ShipNo = opd.PreShipNo;
                    if (osc.FirstOrDefault() != null)
                    {
                        info.ShipTime = osc.OrderByDescending(p => p.Id).FirstOrDefault().ShipTime;
                    }
                    info.RecipientName = order.MemberName;
                    info.ItemCount = (orderItemLists.ContainsKey(order.Guid)) ? orderItemLists[order.Guid].Quantity : string.Empty;
                    info.ItemCost = dealCost.Count > 0 ? (dealCost[dealCost.Count() - 1].Cost ?? 0m) : 0m;
                    info.ItemPrice = theDeal.ItemPrice;
                    info.OrderCreateTime = order.CreateTime;
                    info.DcReturnTime = opd.DcReturnTime != null ? opd.DcReturnTime : opd.DcReceiveFailTime;
                    info.CheckButton = 1;
                    if (opd.DcReturn == 1)
                    {
                        //DC刷退
                        info.DcReturnReason = opd.DcReturnReason;
                    }
                    else if (opd.DcReceiveFail == 1)
                    {
                        //DC驗退
                        info.DcReturnReason = opd.DcReceiveFailReason;

                        if (opd.DcAcceptStatus == (int)IspDcAcceptStatus.N05 || opd.DcAcceptStatus == (int)IspDcAcceptStatus.S03)
                        {
                            info.CheckButton = 0;
                        }
                        else if (opd.DcAcceptStatus == (int)IspDcAcceptStatus.None)
                        {
                            info.CheckButton = 2; 
                        }
                    }


                    info.ItemDescription = (orderItemLists.ContainsKey(order.Guid)) ? orderItemLists[order.Guid].OptionDescription : string.Empty;
                    resultData.Add(info);
                }
            }

            #region filter
            if (!string.IsNullOrEmpty(orderId))
            {
                resultData = resultData.Where(x => x.OrderId == orderId).ToList();
            }
            if (!string.IsNullOrEmpty(dealUniqueId))
            {
                int deal_unique_id = 0;
                int.TryParse(dealUniqueId, out deal_unique_id);
                if (deal_unique_id != 0)
                {
                    resultData = resultData.Where(x => x.DealInfo.DealUniqueId == deal_unique_id).ToList();
                }
            }
            //訂購日
            if (dateType == "1")
            {
                if (!string.IsNullOrEmpty(dateStart) || !string.IsNullOrEmpty(dateEnd))
                {
                    DateTime dtS = DateTime.MinValue.Date;
                    DateTime.TryParse(dateStart, out dtS);
                    DateTime dtE = DateTime.MinValue.Date;
                    DateTime.TryParse(dateEnd, out dtE);                    
                    if (dtS != DateTime.MinValue.Date)
                    {
                        resultData = resultData.Where(x => x.OrderCreateTime >= dtS).ToList();
                    }
                    if (dtE != DateTime.MinValue.Date)
                    {
                        resultData = resultData.Where(x => x.OrderCreateTime <= dtE.AddDays(1)).ToList();
                    }
                }
            }
            //刷退/驗退日
            else if (dateType == "2")
            {
                if (!string.IsNullOrEmpty(dateStart) || !string.IsNullOrEmpty(dateEnd))
                {
                    DateTime dtS = DateTime.MinValue.Date;
                    DateTime.TryParse(dateStart, out dtS);
                    DateTime dtE = DateTime.MinValue.Date;
                    DateTime.TryParse(dateEnd, out dtE);                    
                    if (dtS != DateTime.MinValue.Date)
                    {
                        resultData = resultData.Where(x => x.DcReturnTime >= dtS).ToList();
                    }
                    if (dtE != DateTime.MinValue.Date)
                    {
                        resultData = resultData.Where(x => x.DcReturnTime <= dtE.AddDays(1)).ToList();
                    }
                }
            }
            //出貨日
            else if (dateType == "3")
            {
                if (!string.IsNullOrEmpty(dateStart) || !string.IsNullOrEmpty(dateEnd))
                {
                    DateTime dtS = DateTime.MinValue.Date;
                    DateTime.TryParse(dateStart, out dtS);
                    DateTime dtE = DateTime.MinValue.Date;
                    DateTime.TryParse(dateEnd, out dtE);
                    if (dtS != DateTime.MinValue.Date)
                    {
                        resultData = resultData.Where(x => x.ShipTime >= dtS).ToList();
                    }
                    if (dtE != DateTime.MinValue.Date)
                    {
                        resultData = resultData.Where(x => x.ShipTime <= dtE.AddDays(1)).ToList();
                    }
                }
            }
            #endregion filter

            return resultData;
        }

        public static string GetIspStoreName(OrderProductDelivery opd)
        {
            if (opd.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup)
            {
                return opd.FamilyStoreName;
            }
            else if (opd.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup)
            {
                return opd.SevenStoreName;
            }
            return string.Empty;
        }
    }
}
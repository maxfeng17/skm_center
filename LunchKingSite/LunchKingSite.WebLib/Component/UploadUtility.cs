﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Intelligencia.UrlRewriter.Utilities;

namespace LunchKingSite.WebLib.Component
{
    public class UploadUtility
    {

        static UploadUtility()
        {
        }

        public static bool UploadFile(HttpRequestBase requestFile, string fileName, string basePath)
        {
            bool result = true;
            string path = HttpContext.Current.Server.MapPath(basePath);
            if (requestFile.Files.Count > 0)
            {
                foreach (string file in requestFile.Files)
                {
                    System.Web.HttpPostedFileBase hpf = requestFile.Files[file] as System.Web.HttpPostedFileBase;
                    if (hpf.FileName != "")
                    {
                        string Extension = hpf.FileName.Substring(hpf.FileName.LastIndexOf("."), hpf.FileName.Length - hpf.FileName.LastIndexOf("."));
                        //檢查圖片格式
                        if (!checkImageFormat(Extension.ToLower()))
                        {
                            result = false;
                            break;
                        }

                        fileName = string.Format("{0}_{1}{2}", fileName, DateTime.Now.ToString("yyyyMMddHHmmss"), Extension);
                        break;
                    }

                }
            }

            if (result)
            {
                foreach (string file in requestFile.Files)
                {
                    System.Web.HttpPostedFileBase hpf = requestFile.Files[file] as System.Web.HttpPostedFileBase;
                    hpf.SaveAs(path + fileName);
                    break;
                }
            }


            return result;
        }


        private static bool checkImageFormat(string str)
        {
            bool flag = true;
            if (!string.IsNullOrEmpty(str))
            {
                if (str.ToLower() != ".jpg" && str.ToLower() != ".jpeg" && str.ToLower() != ".png" && str.ToLower() != ".gif")
                {
                    flag = false;
                }
            }

            return flag;
        }

    }
}

﻿using System;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Security;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Component
{
    public class PponIdentity : GenericIdentity
    {
        public const string _USER = "User";
        public const string _TOKEN = "Token";

        readonly static string _GUEST_NAME = "";
        public int Id { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string Pic { get; set; }

        private int _versionAtDB;
        private int _versionAtCookie;
        private bool _isLockOut;

        private static IMemberProvider mp;
        static PponIdentity()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

            guest = new PponIdentity(_GUEST_NAME)
            {
                DisplayName = "",
                Pic = ""
            };
        }

        public bool IsOutOfDate
        {
            get
            {
                // 用 != 的目的，只要不一致就算過期，避免有心人士將 CookieVersion 手動改到很大的數字來規避這個檢查
                return _versionAtDB != _versionAtCookie;
            }
        }

        private static readonly PponIdentity guest;

        public static PponIdentity Guest
        {
            get { return guest; }
        }

        public static PponIdentity Current
        {
            get
            {
                PponIdentity indentity = HttpContext.Current.User.Identity as PponIdentity;
                if (indentity == null)
                {
                    indentity = Thread.CurrentPrincipal.Identity as PponIdentity;
                }
                if (indentity != null)
                {
                    return indentity;
                }
                return Guest;
            }
        }

        public override bool IsAuthenticated
        {
            get
            {
                return this.Equals(Guest) == false && this._isLockOut == false;
            }
        }

        public bool IsLockedOut
        {
            get
            {
                return this._isLockOut;
            }
        }

        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }

        public override int GetHashCode()
        {
            if (string.IsNullOrEmpty(this.Name))
            {
                return 0;
            }
            return this.Name.GetHashCode();
        }

        public PponIdentity(string userName)
            : base(userName)
        {

        }

        public PponIdentity(string name, string type)
            : base(name, type)
        {

        }

        public PponIdentity(Member mem, string type)
            : base(mem.UserName, type)
        {
            Id = mem.UniqueId;
            Email = mem.UserEmail;
            DisplayName = mem.DisplayName;
            Pic = MemberFacade.GetMemberPicUrl(mem);

            _versionAtDB = mem.Version;
            if (mem.IsLockedOut || mem.IsApproved == false)
            {
                _isLockOut = true;
            }
            HttpCookie cookieVersion = HttpContext.Current.Request.Cookies[LkSiteCookie.CookieVersion.ToString()];
            if (cookieVersion != null)
            {
                int tempData;
                if (int.TryParse(cookieVersion.Value, out tempData))
                {
                    _versionAtCookie = tempData;
                }
                else
                {
                    var cookieValue = cookieVersion.Value.Split(',').Length > 0 ? cookieVersion.Value.Split(',')[0] : cookieVersion.Value;
                    var data = new JsonSerializer().Deserialize<CookieModel>(Helper.Decrypt(cookieValue));
                    int.TryParse(data.Value, out _versionAtCookie);
                }
            }
        }

        public static PponIdentity Get(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                return Guest;
            }
            Member mem = mp.MemberGet(userName);
            if (mem.IsLoaded)
            {
                var pponUser = new PponIdentity(mem, "User");
                return pponUser;
            }
            return Guest;
        }

        public override string ToString()
        {
            return base.Name;
        }

    }
}
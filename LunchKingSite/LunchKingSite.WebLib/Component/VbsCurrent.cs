﻿using LunchKingSite.Core;
using System.Web;

namespace LunchKingSite.WebLib.Component
{
    /// <summary>
    /// vbs session variable wrapper
    /// </summary>
    public class VbsCurrent
    {
        public static bool Is17LifeEmployee
        {
            get
            {
                var session = HttpContext.Current.Session[VbsSession.Is17LifeEmployee.ToString()];
                return session != null && session.Equals(true);
            }
        }

        public static string AccountId
        {
            get
            {
                return (string)HttpContext.Current.Session[VbsSession.UserId.ToString()];
            }
        }

        public static string LoginBy17LifeSimulateUserName
        {
            get
            {
                if (HttpContext.Current.Session[VbsSession.LoginBy17LifeSimulateUserName.ToString()] == null)
                    return "";
                else
                    return HttpContext.Current.Session[VbsSession.LoginBy17LifeSimulateUserName.ToString()].ToString();
            }
        }

        public static int VbsAccountType
        {
            get
            {
                return (int)HttpContext.Current.Session[VbsSession.VbsAccountType.ToString()];
            }
        }

        public static DemoUser DemoUser
        {
            get
            {
                return (DemoUser)HttpContext.Current.Session[VbsSession.DemoUser.ToString()];
            }
        }

        public static DemoData DemoData
        {
            get
            {
                return (DemoData)HttpContext.Current.Session[VbsSession.DemoData.ToString()];
            }
        }

        public static ViewVbsRight ViewVbsRight
        {
            get
            {
                return (ViewVbsRight)HttpContext.Current.Session[VbsSession.ViewVbsRight.ToString()];
            }
        }
    }
}
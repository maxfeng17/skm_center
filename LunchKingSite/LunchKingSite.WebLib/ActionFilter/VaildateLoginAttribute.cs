﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace LunchKingSite.WebLib.ActionFilter
{
    public class VaildateLoginAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.HttpContext.User.Identity.IsAuthenticated)
            {    
                if (context.HttpContext.Request.HttpMethod == "POST" && context.HttpContext.Request.UrlReferrer != null)
                {
                    var refer = context.HttpContext.Request.UrlReferrer;
                    var url = string.Format("{0}{1}signed=true", refer.AbsoluteUri
                        , refer.AbsoluteUri.IndexOf('?') > -1 ? "&" : "?");

                    context.Result = new RedirectResult(
                        string.Format("{0}?ReturnUrl={1}"
                            , FormsAuthentication.LoginUrl
                            , HttpUtility.UrlEncode(url)));
                }
                else if (context.HttpContext.Request.Url != null)
                {
                    var requestUrl = context.HttpContext.Request.Url;
                    context.Result = new RedirectResult(
                        string.Format("{0}{1}{2}"
                            , FormsAuthentication.LoginUrl
                            , requestUrl != null ? "?ReturnUrl=" : string.Empty
                            , requestUrl != null ? HttpUtility.UrlEncode(context.HttpContext.Request.Url.AbsoluteUri) : string.Empty));
                }
            }

            base.OnActionExecuting(context);
        }
    }
}

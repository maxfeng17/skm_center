﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.ActionFilter
{
    /// <summary>
    /// 限制頁面(View)只能在測試環境連入，正式站連入的話throw Exception
    /// </summary>
    public class TestServerOnlyAttribute : System.Web.Mvc.ActionFilterAttribute
    {
        private static ISysConfProvider config;

        public TestServerOnlyAttribute()
        {
            config = ProviderFactory.Instance().GetConfig();
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (config.SiteUrl.Equals("http://www.17life.com", StringComparison.OrdinalIgnoreCase))
            {
                throw new HttpException((int)HttpStatusCode.Forbidden, "Access forbidden. -" + HttpContext.Current.Request.UserHostAddress);
            }
        }
    }
}

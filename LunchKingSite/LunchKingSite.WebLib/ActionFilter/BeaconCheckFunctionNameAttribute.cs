﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.WebLib.ActionFilter
{
    class BeaconIdentificationAttribute : ActionFilterAttribute
    {
        private static IBeaconProvider _bp = ProviderFactory.Instance().GetProvider<IBeaconProvider>();

        const string Administrator = "Administrator"; //管理員
        const string Planning = "Planning"; //企劃
        const string ME2O = "ME2O"; //通路
        const string Production = "Production"; //產品
        const string BeaconManager = "BeaconManager"; //Beacon管理者

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.RouteData.Values["functionName"] != null)
            {
                BeaconTriggerApp bta = _bp.GetBeaconTriggerAppByFunctionName(filterContext.RouteData.Values["functionName"].ToString().ToLower());
                if (bta != null)
                {
                    RoleCollection memberRoleCollection = MemberFacade.GetMemberRoleCol(HttpContext.Current.User.Identity.Name);
                    filterContext.ActionParameters["triggerAppId"] = bta.TriggerAppId;
                    filterContext.ActionParameters["memberRoleCollection"] = memberRoleCollection;
                    switch (bta.TriggerAppId)
                    {
                        case (int)BeaconTriggerAppFunctionName.life17:
                            foreach (var memberRole in memberRoleCollection)
                            {
                                if (memberRole.ToString() == Administrator || memberRole.ToString() == Planning || memberRole.ToString() == ME2O || memberRole.ToString() == Production)
                                {
                                    return;
                                }
                            }
                            break;
                        case (int)BeaconTriggerAppFunctionName.skm:
                            foreach (var memberRole in memberRoleCollection)
                            {
                                if (memberRole.ToString() == Administrator || memberRole.ToString() == BeaconManager)
                                {
                                    return;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            filterContext.Result = new RedirectResult("../Trigger");
        }
    }


}


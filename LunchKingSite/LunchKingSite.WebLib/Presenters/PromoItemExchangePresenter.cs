﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class PromoItemExchangePresenter : Presenter<IPromoItemExchangeView>
    {
        private IEventProvider ep;
        private IMemberProvider mp;
        private ISysConfProvider config;
        private ISystemProvider sp;
        public PromoItemExchangePresenter()
        {
            ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            config = ProviderFactory.Instance().GetConfig();
            sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (!string.IsNullOrEmpty(View.UserName))
            {
                EventActivity activity = ep.EventActivityGet(View.EventId);
                if (activity.IsLoaded)
                {
                    if (activity.Id != 0 && activity.Status && activity.StartDate < DateTime.Now && activity.EndDate > DateTime.Now)
                    {
                        View.Activity = activity;
                        // CodeGroupName=EventActivity.PageName; CodeId=遊樂會註冊會員帳號; CodeName=遊樂園名稱
                        SystemCode system = sp.SystemCodeGetByCodeGroupId(activity.PageName, View.UserId);
                        if (system.IsLoaded)
                        {
                            View.CodeName = system.CodeName;
                            View.ShowExchange();
                        }
                        else
                        {
                            View.AlertMessage(I18N.Message.EventCouponExchangeNotExist);
                        }
                    }
                    else
                    {
                        View.AlertMessage(I18N.Message.EventCouponExchangeNotExist);
                    }
                }
                else
                {
                    View.AlertMessage(I18N.Message.EventCouponExchangeNotExist);
                }
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Exchange += new EventHandler<DataEventArgs<string>>(OnExchange);
            View.ExchangeInfo += new EventHandler<EventArgs>(OnShowExchangeInfo);
            return true;
        }

        #region method
        #endregion

        #region event
        void OnExchange(object sender, DataEventArgs<string> e)
        {
            PromoItem item = ep.PromoItemGetByCode(View.EventId, e.Data);
            if (item.IsLoaded)
            {
                if (!item.IsUsed)
                {
                    item.IsUsed = true;
                    item.UseTime = DateTime.Now;
                    item.Memo = View.CodeName;
                    ep.PromoItemSet(item);
                    View.ShowMessage(string.Format("序號：{0} 兌換成功!!", item.Code), System.Drawing.Color.Green);
                }
                else
                {
                    View.ShowMessage(string.Format("注意！序號：{0}，已於 {1} 由{2}重覆使用！", item.Code, item.UseTime.Value.ToString("yyyy/MM/dd HH:mm"), item.Memo), System.Drawing.Color.Red);
                }
            }
            else
            {
                View.ShowMessage("抱歉！查無此組序號！", System.Drawing.Color.Red);
            }
        }
        void OnShowExchangeInfo(object sender, EventArgs e)
        {
            PromoItemCollection itemList = ep.PromoItemGetIsUsedListByEventyId(View.EventId);
            List<PromoItem> list = itemList.Where(x => x.Memo.Equals(View.CodeName)).ToList();
            Dictionary<string, int> dataList = new Dictionary<string, int>();
            for (int i = 0; i < View.Activity.EndDate.Subtract(View.Activity.StartDate).Days; i++)
            {
                DateTime date = View.Activity.StartDate.AddDays(i);
                if (date > DateTime.Now)
                    break;
                int count = list.Where(x => x.UseTime > date && x.UseTime <= date.AddDays(1)).Count();
                string period = string.Format("{0} ~ {1}", date.ToString("yyyy/MM/dd HH:mm"), date.AddDays(1).AddSeconds(-1).ToString("yyyy/MM/dd HH:mm"));
                dataList.Add(period, count);
            }
            View.PromoItemGetList(dataList);
        }
        #endregion
    }
}

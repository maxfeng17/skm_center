﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System.Diagnostics;

namespace LunchKingSite.WebLib.Presenters
{
    public class BackendRecentDealsPresenter : Presenter<IBackendRecentDeals>
    {
        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetChannelList();
            if (View.SellerGuid != Guid.Empty)
            {
                LoadData(1);
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SearchClicked += OnSearchClicked;
            View.PageChange += OnPageChange;
            View.GetCount += OnGetCount;

            return true;
        }

        private void SetChannelList()
        {
            List<PponCity> citys = new List<PponCity>{ PponCityGroup.DefaultPponCityGroup.TaipeiCity,
                                                        PponCityGroup.DefaultPponCityGroup.Taoyuan,
                                                        PponCityGroup.DefaultPponCityGroup.Hsinchu,
                                                        PponCityGroup.DefaultPponCityGroup.Taichung,
                                                        PponCityGroup.DefaultPponCityGroup.Tainan,
                                                        PponCityGroup.DefaultPponCityGroup.Kaohsiung,
                                                        PponCityGroup.DefaultPponCityGroup.PBeautyLocation,
                                                        PponCityGroup.DefaultPponCityGroup.Travel,
                                                        PponCityGroup.DefaultPponCityGroup.AllCountry,
                                                        PponCityGroup.DefaultPponCityGroup.Piinlife,
                                                        PponCityGroup.DefaultPponCityGroup.Tmall,
                                                        PponCityGroup.DefaultPponCityGroup.PponSelect
                                                              };

            View.SetSelectableZone(citys);

            View.SetDealType(0, 0, SystemCodeManager.GetDealType().OrderBy(x => x.Seq).ToList());
        }

        private void LoadData(int pageNumber)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "【LoadData】Begin");

            List<string> list = new List<string>();
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "【LoadData】GetPrivilege");
            list = GetPrivilege();

            if (View.Channel == ChannelType.Ppon)
            {
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "【LoadData】Ppon");

                Dictionary<ViewPponDealCalendar, List<ViewPponDealCalendar>> dataList = new Dictionary<ViewPponDealCalendar, List<ViewPponDealCalendar>>();

                //orderBy = ViewPponDeal.Columns.BusinessHourOrderTimeS + " desc, " + ViewPponDeal.Columns.BusinessHourOrderTimeE + " desc, " + ViewPponDeal.Columns.ItemName;

                #region Orderby
                string orderBy = "";
                if (!string.IsNullOrEmpty(View.OrderField))
                {
                    switch (View.OrderField)
                    {
                        case "business_price"://營業額
                        case "continued_quantity"://前台數量
                        case "quantity"://實際銷售
                            string _sql = pp.GetViewPponDealForCategorySummaryPriceCommand(View.OrderField);
                            orderBy = " ( ";
                            orderBy += _sql;
                            orderBy += " ) ";
                            orderBy += View.OrderBy;
                            break;
                        case "business_hour_order_time_s":  //好康期間
                            orderBy = ViewPponDealCalendar.Columns.BusinessHourOrderTimeS + " " + View.OrderBy + "," + ViewPponDealCalendar.Columns.BusinessHourOrderTimeE + " " + View.OrderBy;
                            break;
                        case "business_hour_deliver_time_s":    //兌換/配送期間
                            orderBy = ViewPponDealCalendar.Columns.BusinessHourDeliverTimeS + " " + View.OrderBy + "," + ViewPponDealCalendar.Columns.BusinessHourDeliverTimeE + " " + View.OrderBy;
                            break;
                        default:
                            orderBy = View.OrderField + " " + View.OrderBy;
                            break;
                    }
                }
                else
                {
                    orderBy = ViewPponDealCalendar.Columns.BusinessHourOrderTimeS + " desc, " + ViewPponDealCalendar.Columns.BusinessHourOrderTimeE + " desc";
                }
                #endregion


                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "【LoadData】GetData");
                string sSQL = "";
                ViewPponDealCalendarCollection vpds =
                    pp.ViewPponDealGetListPagingForCategory(pageNumber, View.PageSize, orderBy, View.CategoryId, View.Dealtype1,
                                                            View.Dealtype2, View.BusinessOrderTimeS, View.BusinessOrderTimeE, View.BusinessDeliverTimeS, View.BusinessDeliverTimeE,
                                                            View.SellerName, View.DealName, View.IsCloseDeal, View.IsOnDeal, View.SalesId, View.UniqueId, list[0], View.BusinessHourGuid, Convert.ToInt32(list[1]), View.Wms, ref sSQL);

                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "【LoadData】sSQL" + sSQL);
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "【LoadData】Loop");

                #region 內文or黑標
                foreach (ViewPponDealCalendar vpd in vpds)
                {
                    if (vpd.IsHideContent != null && vpd.IsHideContent == false)
                    {
                        vpd.EventName = vpd.ContentName;
                    }

                    if (Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
                    {
                        //ViewPponDealCollection subDeals = pp.ViewPponDealGetListByMainBid(vpd.BusinessHourGuid);
                        IList<ViewComboDeal> subComboDeals = ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(vpd.BusinessHourGuid);
                        List<ViewPponDealCalendar> subDeals = new List<ViewPponDealCalendar>();
                        foreach (ViewComboDeal combo in subComboDeals)
                        {
                            //IViewPponDeal sub = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(combo.BusinessHourGuid);
                            ViewPponDealCalendar sub = pp.ViewPponDealCalendarGet(combo.BusinessHourGuid);
                            if (sub.IsHideContent != null && sub.IsHideContent == false)
                            {
                                sub.EventName = sub.ContentName;
                            }
                            subDeals.Add(sub);
                        }
                        dataList.Add(vpd, subDeals);
                    }
                    else
                    {
                        dataList.Add(vpd, null);
                    }
                }
                #endregion

                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "【LoadData】Log");



                #region 查詢紀錄log
                BackendRecentDealsLog log = new BackendRecentDealsLog()
                {
                    Filters = "CategoryId =" + View.CategoryId + ",Dealtype1=" + View.Dealtype1 + ",Dealtype2=" + View.Dealtype2 + ",BusinessOrderTimeS=" + View.BusinessOrderTimeS
                                    + ",BusinessOrderTimeE=" + View.BusinessOrderTimeE + ",BusinessDeliverTimeS=" + View.BusinessDeliverTimeS + ",BusinessDeliverTimeE=" + View.BusinessDeliverTimeE
                                    + ",SellerName=" + View.SellerName + ",DealName=" + View.DealName + ",IsCloseDeal=" + View.IsCloseDeal + ",IsOnDeal=" + View.IsOnDeal + ",SaleUserId=" + View.SalesId,
                    ContentLog = new JsonSerializer().Serialize(dataList.Select(x => new
                    {
                        SellerId = x.Key.SellerId,
                        SellerName = x.Key.SellerName,
                        BusinessHourGuid = x.Key.BusinessHourGuid,
                        ItemName = x.Key.ItemName
                    }).ToList()),
                    CreateTime = DateTime.Now,
                    CreateUser = View.UserName
                };

                pp.BackendRecentDealsLogSet(log);
                #endregion

                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "【LoadData】End");
                ProposalFacade.ProposalPerformanceLogSet("/sal/BackendRecentDeals.aspx", builder.ToString(), View.UserName);

                View.SetPponDeals(dataList);
            }
            else if (View.Channel == ChannelType.PiinLife)
            {
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "【LoadData】PiinLife");

                #region Orderby
                string orderBy = "";
                if (!string.IsNullOrEmpty(View.OrderField))
                {
                    orderBy += View.OrderField + " " + View.OrderBy;
                }
                else
                {
                    orderBy = ViewHiDealSellerProduct.Columns.DealStartTime + " desc, " + ViewHiDealSellerProduct.Columns.DealEndTime + " desc, " + ViewHiDealSellerProduct.Columns.PromoLongDesc;
                }
                #endregion

                string[] filters = GetPiinLifeFilter();
                ViewHiDealSellerProductCollection vhdsp = hp.ViewHiDealSellerProductGetList(pageNumber, View.PageSize, list[0],
                    GetPiinLifeFilter(), orderBy);



                #region 查詢紀錄log
                BackendRecentDealsLog log = new BackendRecentDealsLog()
                {
                    Filters = new JsonSerializer().Serialize(filters),
                    ContentLog = new JsonSerializer().Serialize(vhdsp.Select(x => new
                    {
                        SellerGuid = x.SellerGuid,
                        SellerName = x.SellerName,
                        DealName = x.DealName
                    }).ToList()),
                    CreateTime = DateTime.Now,
                    CreateUser = View.UserName
                };

                pp.BackendRecentDealsLogSet(log);
                #endregion

                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "【LoadData】End");
                ProposalFacade.ProposalPerformanceLogSet("/sal/BackendRecentDeals.aspx", builder.ToString(), View.UserName);

                View.SetPiinLifeDeals(vhdsp);
            }



        }

        private List<string> GetPrivilege()
        {
            List<string> list = new List<string>();

            string CrossDeptTeam = "";
            int EmpUserId = 0;
            if (!string.IsNullOrEmpty(View.SellerName))
            {
                //只有在輸入商家名稱時才會篩選
                if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.ReadAll) || CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.ProductApprove))
                {

                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.CrossDeptTeam))
                {
                    CrossDeptTeam = View.CrossDeptTeam;
                }
                else if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.Read))
                {
                    EmpUserId = View.Emp.UserId;
                }
            }

            list.Add(CrossDeptTeam);
            list.Add(EmpUserId.ToString());
            return list;
        }

        private string[] GetPiinLifeFilter()
        {
            List<string> filter = new List<string>();
            // 不可查詢未開檔
            filter.Add(ViewHiDealSellerProduct.Columns.DealStartTime + "<" + DateTime.Now);

            // 上檔時間
            if (View.BusinessOrderTimeS != null && View.BusinessOrderTimeE != null)
            {
                filter.Add(ViewHiDealSellerProduct.Columns.DealStartTime + ">" + View.BusinessOrderTimeS);
                filter.Add(ViewHiDealSellerProduct.Columns.DealStartTime + "<=" + View.BusinessOrderTimeE);
            }

            // 查詢已結檔
            if (View.IsCloseDeal)
            {
                filter.Add(ViewHiDealSellerProduct.Columns.DealEndTime + "<" + DateTime.Now);
            }

            //查詢在線檔次
            if (View.IsOnDeal)
            {
                filter.Add(ViewHiDealSellerProduct.Columns.DealStartTime + "<=" + DateTime.Now);
                filter.Add(ViewHiDealSellerProduct.Columns.DealEndTime + ">=" + DateTime.Now);
            }

            // 賣家名稱
            if (!string.IsNullOrWhiteSpace(View.SellerName))
            {
                filter.Add(ViewHiDealSellerProduct.Columns.SellerName + " like " + "%" + View.SellerName + "%");
            }

            // 檔次名稱
            if (!string.IsNullOrWhiteSpace(View.DealName))
            {
                for (int i = 0; i < View.DealName.Split(' ').Count(); i++)
                {
                    filter.Add(ViewHiDealSellerProduct.Columns.PromoLongDesc + " like " + "%" + View.DealName.Split(' ')[i] + "%");
                }
            }

            //檔號
            if (View.UniqueId != 0)
            {
                filter.Add(ViewHiDealSellerProduct.Columns.DealId + " = " + View.UniqueId);
            }


            //業務ID(找對方)
            if (View.SalesId != null)
            {
                filter.Add(ViewHiDealSellerProduct.Columns.EmpUserId + " = " + View.SalesId.ToString());
            }

            List<string> list = new List<string>();
            list = GetPrivilege();

            //業務ID(找自己)
            if (Convert.ToInt32(list[1]) != 0)
            {
                filter.Add(ViewHiDealSellerProduct.Columns.EmpUserId + " = " + list[1]);
            }

            return filter.ToArray();
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            LoadData(1);
        }

        protected void OnPageChange(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data);
        }

        protected void OnGetCount(object sender, DataEventArgs<int> e)
        {
            if (View.Channel == ChannelType.Ppon)
            {
                List<string> list = new List<string>();
                list = GetPrivilege();
                e.Data = pp.ViewPponDealGetCountForCategory(View.CategoryId, View.Dealtype1, View.Dealtype2, View.BusinessOrderTimeS, View.BusinessOrderTimeE,
                                                            View.BusinessDeliverTimeS, View.BusinessDeliverTimeE, View.SellerName, View.DealName, View.IsCloseDeal,
                                                            View.IsOnDeal, View.SalesId, View.UniqueId, View.BusinessHourGuid, list[0], Convert.ToInt32(list[1]), View.Wms);
            }
            else if (View.Channel == ChannelType.PiinLife)
            {
                List<string> list = new List<string>();
                list = GetPrivilege();
                e.Data = hp.ViewHiDealSellerProductDialogDataGetCount(list[0], GetPiinLifeFilter());
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using System.Web;

namespace LunchKingSite.WebLib.Presenters
{
    public class EinvoiceWinnerListPresenter : Presenter<IEinvoiceWinnerList>
    {
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.PageChange += OnPageChange;
            View.GetWinnerList += OnGetWinnerList;
            View.GetEinvoice += OnGetEinvoice;
            View.ChangeInfo += OnChangeInfo;
            View.GeneratePrint += OnGeneratePrint;
            View.ExportEinvoice += OnExportEinvoice;
            return true;
        }

        void OnGeneratePrint(object sender, DataEventArgs<string> e)
        {
            View.ResponsePrintInfo(EinvoiceFacade.GenerateInvoiceForPrintByNumber(new string[] { e.Data }, false));
        }

        void OnChangeInfo(object sender, DataEventArgs<KeyValuePair<string, KeyValuePair<string, string>>> e)
        {
            op.EinvoiceUpdateWinner(e.Data.Key, e.Data.Value.Key, e.Data.Value.Value);
            var einvoiceData = op.EinvoiceMainGet(e.Data.Key);
            var tmpData = mp.EinvoiceWinnerGetByUserid(einvoiceData.UserId);
            tmpData.UserId = einvoiceData.UserId;
            tmpData.UserName = e.Data.Value.Key;
            tmpData.Adress = e.Data.Value.Value;
            tmpData.ModificationTime = DateTime.Now;
            mp.SaveEinvoiceWinner(tmpData);
            EventArgs ee = new EventArgs();
            OnGetWinnerList(sender, ee);
        }

        void OnGetEinvoice(object sender, DataEventArgs<string> e)
        {
            ViewEinvoiceWinnerCollection data = op.EinvoiceWinnerGetByNum(e.Data);
            View.PageCount = data.Count;
            View.IsSingleSearch = data.Count > 0 ? true : (bool?)null;
            View.SetUpPageCount();
            View.GetEinvoiceWinnerList(data);
        }

        void OnGetWinnerList(object sender, EventArgs e)
        {
            int count = op.EinvoiceWinnerGetCount(View.InvoiceWinnerDate, View.InvoiceWinnerDate.AddMonths(2), View.Filter);
            View.PageCount = count;
            View.IsSingleSearch = count > 0 ? false : (bool?)null;
            ViewEinvoiceWinnerCollection data = op.EinvoiceWinnerGetList(1, 50, View.InvoiceWinnerDate, View.InvoiceWinnerDate.AddMonths(2), View.Filter);
            View.SetUpPageCount();
            View.GetEinvoiceWinnerList(data);
        }

        void OnPageChange(object sender, DataEventArgs<int> e)
        {
            ViewEinvoiceWinnerCollection data = op.EinvoiceWinnerGetList(e.Data, 50, View.InvoiceWinnerDate, View.InvoiceWinnerDate.AddMonths(2), View.Filter);
            View.GetEinvoiceWinnerList(data);
        }

        void OnExportEinvoice(object sender, DataEventArgs<string> e)
        {
            ViewEinvoiceWinnerCollection data = new ViewEinvoiceWinnerCollection();
            if (View.IsSingleSearch != null)
            {
                if ((bool)View.IsSingleSearch)
                {
                    data = op.EinvoiceWinnerGetByNum(e.Data);
                }
                else
                {
                    data = op.EinvoiceWinnerGetList(View.InvoiceWinnerDate, View.InvoiceWinnerDate.AddMonths(2), View.Filter);
                }
            }

            Workbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.CreateSheet("中獎發票明細");
            int rowIdx = 0;

            #region Style
            CellStyle headerStyle = workbook.CreateCellStyle();
            headerStyle.FillForegroundColor = HSSFColor.GREY_25_PERCENT.index;
            headerStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
            #endregion

            #region Header
            string[] headers = {
            "序號", "訂單編號", "發票號碼", "開立日期",
            "訂購日期", "發票種類", "使用載具", "印製紙本",
            "退貨狀態", "會員姓名", "Email", "會員電話",
            "中獎人", "連絡電話", "發票地址", "中獎確認時間" };
            Row headerRow = sheet.CreateRow(rowIdx);

            for (int i = 0; i < headers.Count(); i++)
            {
                headerRow.CreateCell(i).SetValue(headers[i]);
                headerRow.GetCell(i).CellStyle = headerStyle;
            }

            rowIdx += 1;
            #endregion

            #region Detail
            foreach (var row in data.OrderByDescending(x => x.InvoiceNumberTime))
            {
                Row detailRow = sheet.CreateRow(rowIdx);
                detailRow.CreateCell(0).SetValue(rowIdx);
                detailRow.CreateCell(1).SetValue(row.OrderId);
                detailRow.CreateCell(2).SetValue(row.InvoiceNumber);
                detailRow.CreateCell(3).SetValue(row.InvoiceNumberTime == null ? string.Empty : row.InvoiceNumberTime.Value.ToString("yyyy/MM/dd"));
                detailRow.CreateCell(4).SetValue(row.OrderTime.ToString("yyyy/MM/dd"));
                detailRow.CreateCell(5).SetValue(row.InvoiceType);
                detailRow.CreateCell(6).SetValue(row.CarrierTypeDescription);
                detailRow.CreateCell(7).SetValue(row.InvoicePaperedTime == null ? string.Empty : row.InvoicePaperedTime.Value.ToString("yyyy/MM/dd"));
                detailRow.CreateCell(8).SetValue(GetRefundInfo(row.InvoiceNumber));
                detailRow.CreateCell(9).SetValue(row.Membername);
                detailRow.CreateCell(10).SetValue(row.UserEmail);
                detailRow.CreateCell(11).SetValue(row.Mobile);
                detailRow.CreateCell(12).SetValue(row.InvoiceBuyerName);
                detailRow.CreateCell(13).SetValue(row.InvoiceWinnerresponsePhone);
                detailRow.CreateCell(14).SetValue(row.InvoiceBuyerAddress);
                detailRow.CreateCell(15).SetValue(row.InvoiceWinnerresponseTime == null ? string.Empty : row.InvoiceWinnerresponseTime.Value.ToString("yyyy/MM/dd HH:mm"));
                rowIdx += 1;
            }

            #endregion

            //設置寬度為autosize
            for (int i = 0; i < headers.Count(); i++)
            {
                sheet.AutoSizeColumn(i);
                sheet.SetColumnWidth(i, sheet.GetColumnWidth(i) + 700);
            }

            //下載檔案
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode(@"EinvoiceWinnerList.xls")));
            workbook.Write(HttpContext.Current.Response.OutputStream);
        }
        
        private string GetRefundInfo(string invNum)
        {
            CashTrustLog trust = EinvoiceFacade.CashTrustLogGetByInvoiceNumber(invNum);
            if (trust.IsLoaded)
            {
                if (((TrustStatus)trust.Status).Equals(TrustStatus.Refunded) || ((TrustStatus)trust.Status).Equals(TrustStatus.Returned))
                {
                    return I18N.Phrase.TrustStatus_Returned;
                }
            }
            return string.Empty;
        }
    }
}

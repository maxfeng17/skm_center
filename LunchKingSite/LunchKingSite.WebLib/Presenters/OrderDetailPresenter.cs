﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using PaymentType = LunchKingSite.Core.PaymentType;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.Core.Interface;

using System.Data;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.WebLib.Presenters
{
    public class OrderDetailPresenter : Presenter<IOrderDetailView>
    {
        #region members

        protected IOrderProvider op;
        protected IMemberProvider mp;
        protected IPponProvider pp;
        protected ISellerProvider sp;
        protected ISysConfProvider cp;
        protected IAccountingProvider ap;
        protected IFamiportProvider fami;
        protected IHiLifeProvider hiLife;
        protected ILog log;

        #endregion members

        public OrderDetailPresenter()
        {
            SetupProviders();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.GetCashTrustLog += OnGetCashTrustLog;
            View.CouponBound += GetCouponRelativeInfo;
            if (View.OrderId == Guid.Empty)
            {
                View.Exit();
                return false;
            }

            SetupView(op.ViewOrderMemberBuildingSellerGet(View.OrderId));

            //config.EnabledExchangeProcess:false
            SetFilterTypes();

            SetEinoviceLog(View.OrderId);

            View.BuildServiceCategory(MemberFacade.ServiceMessageCategoryGetListByParentId(null), View.ddlServiceCategory);

            if (View.IsToHouse)
            {
                View.ShowHomeDeliveryInfo();
                View.SetHomeDeliveryProductInfo();
                View.SetHomeDeliverReturnInfo();
                View.SetHomeDeliverExchangeInfo();
            }
            else
            {
                View.ShowCouponInfo();
                View.SetCouponInfo(pp.ViewPponCouponGetListRegularOnly(ViewPponCoupon.Columns.OrderGuid, View.OrderId));
                View.SetCouponReturnInfo();
            }
            View.SetVendorFine();
            View.SetOrderUserMemoInfo(op.OrderUserMemoListGetList(View.OrderId));
            
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.UpdateOrderInfo += OnUpdateOrderInfo;
            View.UpdateOrderDetail += OnUpdateOrderDetail;
            View.UpdateOrderShip += OnUpdateOrderShip;
            View.DeleteOrderShip += OnDeleteOrderShip;
            View.AddOrderShip += OnAddOrderShip;
            View.GetOrderDetailCount += OnGetOrderDetailCount;
            View.SendRefundResult += OnSendRefundResult;
            View.ReturnApplicationResend += OnReturnApplicationResend;
            View.ReturnDiscountResend += OnReturnDiscountResend;
            View.GetCashTrustLog += OnGetCashTrustLog;
            View.CouponAvailableToggle += OnCouponAvailableToggle;
            View.RecoveredConfirm += OnRecoveredConfirm;
            View.InvoiceMailBackSet += OnInvoiceMailBackSet;
            View.GetSMS += OnGetSMS;
            View.SetAtmRefund += OnSetAtmRefund;
            View.ReceiptMailBackSet += OnReceiptMailBackSet;
            View.ReceiptMailBackAllowanceSet += OnReceiptMailBackAllowanceSet;
            View.ReceiptMailBackPaperSet += OnReceiptMailBackPaperSet;
            View.CashTrustlogFilled += OnCashTrustlogFilled;
            //新增客服紀錄動作
            View.ServiceMsgSet += OnServiceMsgSet;
            View.CreateReturnForm += OnCreateReturnForm;
            View.UpdateReturnFormReceiver += OnUpdateReturnFormReceiver;
            View.CreateCouponReturnForm += OnCreateCouponReturnForm;
            View.ImmediateRefund += OnImmediateRefund;
            View.ReceivedCreditNote += OnReceivedCreditNote;
            View.ChangeToElcAllowance += OnChangeToElcAllowance;
            View.ChangeToPaperAllowance += OnChangeToPaperAllowance;
            View.CancelReturnForm += OnCancelReturnForm;
            View.UpdateAtmRefund += OnUpdateAtmRefund;
            View.RecoverVendorProgress += OnRecoverVendorProgress;
            View.ProcessedVendorUnreturnable += OnProcessedVendorUnreturnable;
            View.SCashToCash += OnScashToCash;
            View.CashToSCash += OnCashToSCash;
            View.ArtificialRefund += OnArtificialRefund;
            View.RecoverToRefundScash += OnRecoverToRefundScash;
            View.RequestInvoice += OnRequestInvoice;
            View.OrderUserMemoSet += OnOrderUserMemoSet;
            View.OrderUserMemoDelete += OnOrderUserMemoDelete;
            View.CreditcardReferSet += OnCreditcardReferSet;
            View.OrderFreezeSet += OnOrderFreezeSet;
            View.CreateExchangeLog += OnCreateExchangeLog;
            View.UpdateExchangeLog += OnUpdateExchangeLog;
            View.CompleteExchangeLog += OnCompleteExchangeLog;
            View.CancelExchangeLog += OnCancelExchangeLog;
            View.CancelOrder += OnCancelOrder;
            View.WithdrawCancelOrder += OnWithdrawCancelOrder;
            View.DownloadCouponZip += OnDownloadCouponZip;

            return true;
        }

        protected void SetupProviders()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            cp = ProviderFactory.Instance().GetConfig();
            ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            fami = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
            hiLife = ProviderFactory.Instance().GetProvider<IHiLifeProvider>(); 

            log = LogManager.GetLogger("OrderDetailPresenter");
        }

        protected void SetupView(ViewOrderMemberBuildingSeller odr)
        {
            if (odr.IsLoaded)
            {
                GetQuantity();

                View.MemberEmail = odr.MemberEmail;

                View.AtmRefundCol = op.CtAtmRefundGetList(CtAtmRefund.Columns.Si, CtAtmRefund.Columns.OrderGuid + "=" + View.OrderId);
                View.ShipCompanyCol = op.ShipCompanyGetList(null);
                this.SetupOrderInfo(odr);
                var ods = op.ViewPponOrderDetailGetListByOrderGuid(odr.OrderGuid);
                ods.ForEach(x => { x.ItemName = OrderFacade.GetOrderDetailItemName(OrderDetailItemModel.Create(x)); });
                View.SetOrderItemList(ods);
                View.SetDeliverData(odr, op.ViewOrderShipListGetListByOrderGuid(odr.OrderGuid));
                View.SetOrderExportLog(op.OrderGet(View.OrderId).ExportLog);
                View.SetIsCanceling(odr.IsCanceling);

                //檢查若為資策會訂單，顯示資策會訂單資料的區域
                if (odr.UserId == ApiLocationDealManager.IDEASMember.UniqueId)
                {
                    CompanyUserOrder companyUserOrder = op.CompanyUserOrderGet(odr.OrderGuid);
                    if (companyUserOrder.IsLoaded)
                    {
                        View.SetIDEASOrderData(companyUserOrder);
                    }
                }

            }
        }

        protected void SetupOrderInfo(ViewOrderMemberBuildingSeller odr)
        {
            Dictionary<int, string> data = new Dictionary<int, string>();
            foreach (int val in Enum.GetValues(typeof(OrderStatus)))
            {
                if (val > 0 && (val & odr.OrderStatus) > 0)
                {
                    data.Add(val, Helper.GetLocalizedEnum(Phrase.ResourceManager, (OrderStatus)val));
                }
            }

            View.SetOrderStatusList(data, odr.OrderStatus, odr.IsCanceling);

            ViewPponDealCollection deals = pp.ViewPponDealGetListPaging(1, -1, ViewPponDeal.Columns.BusinessHourOrderTimeE,
                                                        ViewPponDeal.Columns.OrderGuid + " = " + op.OrderGet(View.OrderId).ParentOrderId);
            ViewPponOrderStatusLogCollection fCol = op.ViewPponOrderStatusLogGetListPaging(1, -1,
                                                        ViewPponOrderStatusLog.Columns.OrderLogCreateTime + " desc ",
                                                        ViewPponOrderStatusLog.Columns.OrderGuid + " = " + View.OrderId,
                                                        ViewPponOrderStatusLog.Columns.OrderLogStatus + " = " + (int)OrderLogStatus.Processing);
            ViewPponOrderStatusLog firstOrderLog = (fCol.Count > 0) ? fCol[0] : null;

            ViewPponOrderStatusLogCollection lCol = op.ViewPponOrderStatusLogGetListPaging(1, -1,
                                            ViewPponOrderStatusLog.Columns.OrderLogCreateTime + " desc ",
                                            ViewPponOrderStatusLog.Columns.OrderGuid + " = " + View.OrderId);
            ViewPponOrderStatusLog lastOrderLog = (lCol.Count > 0) ? lCol[0] : null;
            ViewPponCouponCollection coupons = pp.ViewPponCouponGetListRegularOnly(ViewPponCoupon.Columns.OrderGuid, View.OrderId);

            PaymentTransactionCollection paymentsPT = op.PaymentTransactionGetListPaging(0, 0, null, PayTransStatusFlag.None, 0,
                                            PaymentTransaction.Columns.TransId + " = " + op.PaymentTransactionGet(View.OrderId).TransId,
                                            PaymentTransaction.Columns.TransType + " = " + (int)PayTransType.Authorization);

            GroupOrder go = (deals.Count() > 0) ? op.GroupOrderGet((Guid)deals[0].GroupOrderGuid) : null;

            #region payments for charging situation

            ViewOrderMemberPaymentCollection cCol = op.ViewOrderMemberPaymentGetListPaging(1, -1,
                                                        ViewOrderMemberPayment.Columns.PaymentTransactionId + " desc ",
                                                        ViewOrderMemberPayment.Columns.OrderGuid + " = " + View.OrderId,
                                                        ViewOrderMemberPayment.Columns.PaymentType + " = " + (int)PaymentType.Creditcard);
            ViewOrderMemberPaymentCollection pCol = op.ViewOrderMemberPaymentGetListPaging(1, -1,
                                                        ViewOrderMemberPayment.Columns.PaymentTransactionId + " desc ",
                                                        ViewOrderMemberPayment.Columns.OrderGuid + " = " + View.OrderId,
                                                        ViewOrderMemberPayment.Columns.PaymentType + " = " + (int)PaymentType.PCash);
            ViewOrderMemberPaymentCollection bCol = op.ViewOrderMemberPaymentGetListPaging(1, -1,
                                                        ViewOrderMemberPayment.Columns.PaymentTransactionId + " desc ",
                                                        ViewOrderMemberPayment.Columns.OrderGuid + " = " + View.OrderId,
                                                        ViewOrderMemberPayment.Columns.PaymentType + " = " + (int)PaymentType.BonusPoint);
            ViewOrderMemberPaymentCollection paymentsCS = new ViewOrderMemberPaymentCollection();
            if (cCol.Count > 0) paymentsCS.Add(cCol[0]);
            if (pCol.Count > 0) paymentsCS.Add(pCol[0]);
            if (bCol.Count > 0) paymentsCS.Add(bCol[0]);

            #endregion payments for charging situation

            BankInfoCollection bks = new BankInfoCollection();
            bks = op.BankInfoGetMainList();
            bks.Insert(0, new BankInfo() { BankName = "請選擇", BankNo = "-1" });
            OrderStatusLogCollection ol = op.OrderStatusLogGetList(odr.OrderGuid);
            View.SetOrderStatusLog(ol);
            //EinvoiceMain einvoice = op.EinvoiceMainGetByUser(odr.UserId.GetValueOrDefault(), odr.OrderGuid, false);
            EinvoiceMainCollection einvCol = op.EinvoiceMainCollectionGet(EinvoiceMain.Columns.OrderGuid, odr.OrderGuid.ToString(), true);
            //優先取未開發票的資料
            EinvoiceMain einvoice = einvCol.FirstOrDefault(t => string.IsNullOrEmpty(t.InvoiceNumber));
            //如果沒有未開發票的，則取第一筆資料 (?) 11/10 改取最後一筆
            if (einvoice == null)
            {
                if (einvCol.Count > 0)
                {
                    einvoice = einvCol.OrderBy(x => x.InvoiceNumber).LastOrDefault();
                }
                else
                {
                    einvoice = new EinvoiceMain();
                }
            }

            View.OldInvoiceAllowance = einvoice.InvoiceMailbackAllowance ?? false;
            View.OldInvoicePaper = einvoice.InvoiceMailbackPaper ?? false;
            View.BusinessHourGuid = deals.Count > 0 ? deals[0].BusinessHourGuid : Guid.Empty;

            DealProperty dp = pp.DealPropertyGetByOrderGuid(View.OrderId);
            EntrustSellType entrustSell = (dp != null) ? (EntrustSellType)dp.EntrustSell : EntrustSellType.No;
            EntrustSellReceipt receipt = null;
            if (entrustSell != EntrustSellType.No)
            {
                receipt = op.EntrustSellReceiptGetByOrder(View.OrderId);
            }
            DealAccounting accounting = pp.DealAccountingGet(View.BusinessHourGuid);
            CashTrustLogCollection ctCol = mp.CashTrustLogGetListByOrderGuid(View.OrderId, OrderClassification.LkSite);
            //部份退貨檢核
            View.GoodsReturnStatus = OrderFacade.GoodsReturnStatusGet(ctCol);
            View.CashTrustLogStatus = (ctCol.Count() > 0) ? ctCol.First().Status : -1;

            #region Tmall天貓訂單對應
            OrderCorrespondingCollection oc = op.OrderCorrespondingListGet(new string[] { OrderCorresponding.Columns.OrderGuid + "=" + odr.OrderGuid });
            #endregion
            #region MasterPass
            bool isMassterpass = op.MasterPassGetByOrderGuid(odr.OrderGuid);
            #endregion

            VendorPaymentChangeCollection vpcs = ap.VendorPaymentChangeGetList(View.BusinessHourGuid);


            View.SetOrderInfo(odr, (deals.Count() > 0) ? deals[0] : null,
                firstOrderLog, lastOrderLog, coupons, paymentsPT, paymentsCS, einvoice,
                 entrustSell, receipt, (deals.Count() > 0) ? pp.DealPropertyGet(deals[0].BusinessHourGuid) : null, go, bks,
                cp.NewInvoiceDate, accounting, einvCol, ctCol, oc.FirstOrDefault(), isMassterpass, vpcs, op.OrderGet(odr.OrderGuid));

            View.IsToHouse = (deals.Count() > 0 && deals[0].DeliveryType == (int)DeliveryType.ToHouse);
            View.CheckIsShipable = CheckIsShipable();
            View.CheckIsExchanging = CheckEverExchange();

            View.FreezeVisible = false;
            View.CancelFreezeVisible = false;

            if (einvoice.AllowanceStatus == (int)AllowanceStatus.ElecAllowance)
            {
                View.IsPaperAllowance = false;
            }
            else
            {
                View.IsPaperAllowance = true;
            }
            if (einvoice.InvoiceMode2 == (int)InvoiceMode2.Duplicate && einvoice.OrderTime > cp.EinvAllowanceStartDate)
            {
                View.IsEnableElecAllowance = true;
            }
            else
            {
                View.IsEnableElecAllowance = false;
            }

            CashTrustLog ctlog = mp.CashTrustLogGetByOrderId(odr.OrderGuid);
            if (ctlog.BusinessHourGuid != null)
            {
                if (ctlog.BusinessHourGuid != Guid.Empty)
                {
                    DealAccounting da = pp.DealAccountingGet(ctlog.BusinessHourGuid ?? Guid.Empty);
                    bool isDealFreeze = Helper.IsFlagSet(da.Flag, (int)AccountingFlag.Freeze);
                    View.DealFreeze = (isDealFreeze ? "整檔凍結" : "");
                    if (isDealFreeze)
                    {
                        //整檔凍結把訂單凍結的按鈕鎖住
                        View.FreezeVisible = !isDealFreeze;
                        View.CancelFreezeVisible = !isDealFreeze;
                    }
                    else
                    {
                        //新出帳方式宅配檔次訂單且尚未列入對帳單明細中 且檔次仍在對帳區間中 方能使用凍結請款功能
                        var trustIds = mp.CashTrustLogListGetByOrderId(odr.OrderGuid).Select(x => x.TrustId).ToList();
                        var bsDetails = GetPaidTrustIds(trustIds);
                        if (View.IsToHouse &&
                            (da.RemittanceType == (int)RemittanceType.Flexible ||
                             da.RemittanceType == (int)RemittanceType.Monthly ||
                             da.RemittanceType == (int)RemittanceType.Weekly) &&
                            trustIds.Any(x => !bsDetails.Contains(x)) &&
                            BalanceSheetManager.IsInBalanceTimeRange(View.BusinessHourGuid, DateTime.Today, true))
                        {
                            var orderFreeze = Helper.IsFlagSet(ctlog.SpecialStatus, (int)TrustSpecialStatus.Freeze);
                            View.FreezeVisible = !orderFreeze;
                            View.CancelFreezeVisible = orderFreeze;
                        }
                    }
                }
            }
        }

        #region Event

        protected void OnUpdateOrderInfo(object sender, DataEventArgs<UpdatableOrderInfo> e)
        {
            string auditStr = Phrase.ResourceManager.GetString("Edit") + " ";
            UpdatableOrderInfo info = e.Data;
            Guid orderGuid = View.OrderId;
            Order o = op.OrderGet(orderGuid);
            ViewPponOrder viewPponOrder = pp.ViewPponOrderGet(orderGuid);
            ViewPponDeal deal = pp.ViewPponDealGetByBusinessHourGuid(viewPponOrder.BusinessHourGuid);
            DateTime now = DateTime.Now;
            EinvoiceMainCollection einvCol = op.EinvoiceMainCollectionGet(
                EinvoiceMain.Columns.OrderGuid, View.OrderId.ToString(), true);
            List<string> multiMessage = new List<string>();

            if (info.IsUpdateInvoice)
            {
                #region 發票
                
                //只有尚未開立發票的訂單, 才能變更發票資料
                //2014/7/17修改為收件人,收件地址,抬頭沒有修改的限制
                foreach (EinvoiceMain inv in einvCol)
                {
                    bool IsVaildEinvoice = (inv.InvoiceStatus != (int)EinvoiceType.C0501 && inv.InvoiceStatus != (int)EinvoiceType.D0401) ? true : false;
                    bool invoiceFrom3To2 = inv.IsEinvoice3 &&
                        Convert.ToInt32(info.InvoiceType) == (int)InvoiceMode2.Duplicate;
                    bool invoiceFrom2To3 = inv.IsEinvoice2 &&
                        Convert.ToInt32(info.InvoiceType) == (int)InvoiceMode2.Triplicate;
                    bool invoiceChangeComInfo = (inv.InvoiceComName != info.Title || inv.InvoiceComId != info.UniCode) && !invoiceFrom2To3 && inv.InvoiceStatus == (int)EinvoiceType.C0401; //目前走作廢重開立，之後補上可轉註銷:未印製、取消原本紙本申請時間(註銷上傳後重壓)
                    bool invoiceChangeCarrierType = (inv.CarrierType != (int)info.CarrierType || (inv.CarrierId != info.CarrierId && inv.CarrierType != (int)CarrierType.Member)) && inv.InvoiceMode2 == (int)InvoiceMode2.Duplicate && !invoiceFrom3To2 && inv.InvoiceStatus == (int)EinvoiceType.C0401 && !string.IsNullOrEmpty(inv.InvoiceNumber); //載具變更僅有2聯,會員載具有時不會存入CarrierId 故不視為變更
                    EinvoiceSerial einvocieSerial = inv.InvoiceNumberTime != null ? op.EinvoiceSerialCollectionGetByInvInfo(inv.InvoiceNumber, inv.InvoiceNumberTime.Value) : null;
                    bool invoiceIntertemporal = inv.IsIntertemporal;
                    string changeCarrierMsg = invoiceChangeCarrierType ? (inv.CarrierType != (int)info.CarrierType) ? ((CarrierType)inv.CarrierType).ToString() + "->" + ((CarrierType)info.CarrierType).ToString() : inv.CarrierId + "->" + info.CarrierId : string.Empty;

                    IList<ReturnFormEntity> returnForms = ReturnFormRepository.FindAllByOrder(orderGuid);
                    if (returnForms.Count > 0)
                    {
                        ReturnFormEntity form = returnForms.Where(x => x.ProgressStatus.EqualsAny(ProgressStatus.Processing, ProgressStatus.AtmQueueing, ProgressStatus.AtmQueueSucceeded))
                            .OrderByDescending(x => x.VendorProcessTime).FirstOrDefault();
                        if (form == null)
                        {
                            form = returnForms.OrderByDescending(x => x.VendorProcessTime).FirstOrDefault();
                        }

                        if ((form.ProgressStatus != ProgressStatus.Canceled && form.ProgressStatus != ProgressStatus.Completed) && invoiceFrom2To3)
                        {
                            View.alertMessage = "2聯轉3聯需待退貨處理結束才可進行!";
                            return;
                        }
                    }

                    if (invoiceFrom2To3 && !string.IsNullOrEmpty(inv.InvoiceNumber) && Convert.ToInt32(inv.InvoiceStatus) == (int)EinvoiceType.Initial)
                    {
                        //status = C0701仍允許做2轉3
                        View.alertMessage = "重開期間不可進行2聯轉3聯";
                        return;
                    }

                    if ((invoiceFrom2To3 || invoiceChangeComInfo) && inv.InvoiceNumberTime < cp.PayeasyToContact && inv.InvoiceStatus == (int)EinvoiceType.C0401)
                    {
                        View.alertMessage = "康迅發票不可進行2聯轉3聯";
                        return;
                    }

                    if (!string.IsNullOrEmpty(inv.InvoiceNumber) && IsVaildEinvoice && inv.VerifiedTime >= cp.PayeasyToContact && ((inv.InvoiceMode2 != (int)InvoiceMode2.Triplicate && invoiceFrom2To3 && (!invoiceIntertemporal || (invoiceIntertemporal && einvocieSerial != null && now < einvocieSerial.EndDate.AddDays(cp.InvoiceBufferDays)))) || (inv.InvoiceMode2 == (int)InvoiceMode2.Triplicate  && !invoiceFrom2To3 && invoiceChangeComInfo)))
                    {
                        //已開立則作廢重開
                        //新開立一筆main
                        EinvoiceMain newInv = new EinvoiceMain();
                        newInv = inv.Clone();
                        newInv.Id = 0;
                        newInv.InvoiceNumber = null;
                        newInv.InvoiceNumberTime = null;
                        newInv.VerifiedTime = newInv.InvoiceRequestTime = now;
                        newInv.InvoiceComId = info.UniCode;
                        newInv.InvoiceComName = info.Title;
                        newInv.InvoiceSerialId = 0;
                        newInv.InvoiceFileSerial = 0;
                        newInv.InvoiceBuyerName = info.EinvoiceName;
                        newInv.InvoiceBuyerAddress = info.EinvoiceAddress;
                        newInv.Message = invoiceChangeComInfo ? "變更抬頭重開" : "二聯改三聯";
                        newInv.InvoiceMode = 0; //not use
                        newInv.InvoiceMode2 = (int)InvoiceMode2.Triplicate;
                        newInv.CarrierType = (int)CarrierType.None;
                        newInv.CarrierId = string.Empty;
                        newInv.InvoicePapered = false;
                        newInv.InvoicePaperedTime = null;
                        newInv.AllowanceStatus = (int)AllowanceStatus.PaperAllowance;
                        newInv.LoveCode = string.Empty;
                        newInv.InvoiceVoidMsg = string.Empty;
                        newInv.InvoiceVoidTime = null;

                        EinvoiceFacade.SetEinvoiceMainWithLog(newInv);

                        int einvType = (!invoiceIntertemporal || (invoiceIntertemporal && now < einvocieSerial.EndDate.AddDays(cp.InvoiceBufferDays))) ? (int)EinvoiceType.C0501 : (int)EinvoiceType.D0401; //原則上二聯轉三聯作廢僅限當期

                        //detail 作廢方式 -> 開立一筆InvType為作廢的Detail 開立發票號碼時會再創建一筆新的mainId的Detail
                        EinvoiceFacade.GenerateCancelFiles(inv.Id, einvType, View.CurrentUser); //含作廢Main

                        CommonFacade.AddAudit(View.OrderId, AuditType.Order, string.Format("{0}發票:{1}", (einvType == (int)EinvoiceType.D0401 ? "折讓" : "作廢"), inv.InvoiceNumber), View.CurrentUser, true);

                        continue;
                    }

                    if (IsVaildEinvoice)
                    {
                        inv.InvoiceComId = info.UniCode;
                        inv.InvoiceComName = info.Title;
                        inv.InvoiceBuyerName = info.EinvoiceName;
                        inv.InvoiceBuyerAddress = info.EinvoiceAddress;
                        inv.InvoiceMode = 0; //the field will be delete

                        if (invoiceFrom2To3)
                        {
                            if (inv.VerifiedTime == null)
                            {
                                inv.VerifiedTime = now;
                            }
                            inv.InvoiceRequestTime = now;
                            inv.AllowanceStatus = (int)AllowanceStatus.PaperAllowance;
                        }

                        if (inv.Version == (int)InvoiceVersion.CarrierEra)
                        {
                            inv.InvoiceMode2 = Convert.ToInt32(info.InvoiceType);

                            if (invoiceFrom2To3 || invoiceFrom3To2)
                            {
                                inv.CarrierType = (int)CarrierType.None;
                                inv.CarrierId = null;
                                inv.LoveCode = string.Empty;
                            }
                            else
                            {
                                inv.CarrierType = (int)info.CarrierType;
                                inv.CarrierId = info.CarrierId;
                            }
                        }
                        if (invoiceChangeCarrierType && !invoiceIntertemporal && inv.InvoiceRequestTime == null)
                        {
                            inv.InvoiceStatus = (int)EinvoiceType.C0701;
                            inv.InvoiceVoidTime = now;
                            inv.InvoiceVoidMsg = "變更載具，註銷重開";
                            CommonFacade.AddAudit(View.OrderId, AuditType.Order, string.Format("註銷發票:{0}，原因:變更載具{1}", inv.InvoiceNumber, changeCarrierMsg), View.CurrentUser, true);
                        }
                    }


                    //if (invoiceFrom2To3 && IsVaildEinvoice)
                    //{
                    //    //二聯發票轉三聯式, 要押VerifiedTime, 以免發票Job 未產生發票號碼; 還要押上InvoiceRequestTime, 才能被抓到並印出
                    //    if (inv.VerifiedTime == null)
                    //    {
                    //        inv.VerifiedTime = DateTime.Now;
                    //    }
                    //    inv.InvoiceRequestTime = DateTime.Now;
                    //}
                    //else if (inv.OrderIsPponitem == false && invoiceFrom3To2) //憑證發票、3聯轉2聯
                    //{
                    //    inv.VerifiedTime = OrderFacade.GetCouponVerifiedTime(inv.CouponId.Value,
                    //        (OrderClassification)inv.OrderClassification.Value);
                    //}

                    if (invoiceIntertemporal && invoiceChangeCarrierType)
                    {
                        //會有多張不同期發票
                        multiMessage.Add(string.Format("發票:{0}已跨期，無法再進行註銷作業", inv.InvoiceNumber));
                        CommonFacade.AddAudit(View.OrderId, AuditType.Order, string.Format("載具已變更:" + changeCarrierMsg + "， 但發票:{0}未註銷，因已跨期", inv.InvoiceNumber), View.CurrentUser, true);
                        continue;
                    }

                    if (invoiceChangeCarrierType && inv.InvoiceRequestTime != null && !invoiceFrom2To3)
                    {
                        multiMessage.Add(string.Format("發票:{0}已申請紙本發票，無法再進行註銷作業", inv.InvoiceNumber));
                        CommonFacade.AddAudit(View.OrderId, AuditType.Order, string.Format("載具已變更:" + changeCarrierMsg + "，但發票:{0}未註銷，因已申請紙本發票", inv.InvoiceNumber), View.CurrentUser, true);
                        continue;
                    }
                }

                #endregion

                EinvoiceFacade.SetEinvoiceMainCollectionWithLog(einvCol, View.CurrentUser, now);
                if (multiMessage.Count > 0)
                    View.alertMultiMessage = multiMessage;
            }


            if (((o.MobileNumber) ?? string.Empty) != info.CustomerMobile)
            {
                auditStr += Phrase.ResourceManager.GetString("MobileNumber") + ": " + o.MobileNumber + "->" + info.CustomerMobile + "|";
                o.MobileNumber = info.CustomerMobile;
            }

            if (((o.PhoneNumber) ?? string.Empty) != info.CustomerPhone)
            {
                auditStr += Phrase.ResourceManager.GetString("CompanyTel") + ": " + o.PhoneNumber + "->" + info.CustomerPhone + "|";
                o.PhoneNumber = info.CustomerPhone;
            }

            if (((o.DeliveryAddress) ?? string.Empty) != info.DeliveryAddress)
            {
                auditStr += Phrase.ResourceManager.GetString("DeliveryAddress") + ": " + o.DeliveryAddress + "->" + info.DeliveryAddress + "|";
                o.DeliveryAddress = info.DeliveryAddress;
            }

            if (((o.UserMemo) ?? string.Empty) != info.UserMemo)
            {
                auditStr += Phrase.ResourceManager.GetString("UserMemo") + ": " + o.UserMemo + "->" + info.UserMemo + "|";
                o.UserMemo = info.UserMemo;
            }

            #region save OrderMemo

            if (info.DrType != (int)DonationReceiptsType.None)
            {
                if (string.IsNullOrEmpty(info.OrderMemo))
                {
                    o.OrderMemo = info.DrType.ToString() + "|" + info.InvoiceType + "|" + info.UniCode + "|" + info.Title;
                }
                else
                {
                    o.OrderMemo = info.DrType.ToString() + "|" + info.InvoiceType + "|" + info.UniCode + "|" + info.Title + "|" + info.OrderMemo;
                }
            }
            else
            {
                o.OrderMemo = info.OrderMemo;
            }

            #endregion save OrderMemo

            if (o.IsDirty)
            {
                op.OrderSet(o);
                CommonFacade.AddAudit(o.Guid.ToString(), AuditType.Order, auditStr, View.CurrentUser, false);
            }

            #region save OrderStatusLog

            if (!cp.EnabledExchangeProcess && info.ReturnSituation != -1) //is not initial
            {
                var rtnl = op.OrderReturnListGet(orderGuid) ?? new OrderReturnList();

                rtnl.OrderGuid = orderGuid;
                rtnl.Type = (int)OrderReturnType.Exchange;
                rtnl.Message = info.ReturnReason;
                rtnl.Status = (int)OrderFacade.TransferOrderLogStatusToOrderReturnStatus((OrderLogStatus)info.ReturnSituation);
                rtnl.ModifyTime = DateTime.Now;
                rtnl.ModifyId = View.CurrentUser;
                if (rtnl.CreateTime == null || info.ReturnSituation == (int)OrderLogStatus.ExchangeProcessing)
                {
                    rtnl.CreateTime = rtnl.ModifyTime.Value;
                    rtnl.CreateId = rtnl.ModifyId;
                }
                //rtnl.ReceiverName
                //rtnl.ReceiverAddress

                OrderFacade.SetOrderReturList(rtnl);

                var olCol = op.ViewPponOrderStatusLogGetListPaging(1, -1, ViewPponOrderStatusLog.Columns.OrderLogCreateTime + " desc ",
                                  ViewPponOrderStatusLog.Columns.OrderGuid + " = " + orderGuid);
                var auditOrderStatusLog = "修改取消訂單申請狀態: "
                        + ((olCol.Count == 0) ? string.Empty : ((OrderLogStatus)olCol[0].OrderLogStatus).ToString()) + "->" + ((OrderLogStatus)info.ReturnSituation).ToString() + "; "
                        + ((olCol.Count == 0) ? string.Empty : olCol[0].OrderLogMessage) + "->" + info.ReturnReason;
                if (!string.IsNullOrEmpty(auditOrderStatusLog))
                {
                    CommonFacade.AddAudit(o.Guid.ToString(), AuditType.Order, auditOrderStatusLog, View.CurrentUser, false);
                }
            }

            #endregion save OrderStatusLog

            EntrustSellReceipt receipt = op.EntrustSellReceiptGetByOrder(orderGuid);
            if (receipt.IsLoaded)
            {
                receipt.MailbackAllowance = info.ReceiptMailbackAllowance;
                receipt.MailbackPaper = info.ReceiptMailbackPaper;
                receipt.MailbackReceipt = info.ReceiptMailbackReceipt;
                receipt.BuyerAddress = info.ReceiptAddress;
                receipt.IsPhysical = info.ReceiptIsPhysical;
                if (receipt.ForBusiness)
                {
                    receipt.BuyerName = info.ReceiptTitle;
                    receipt.ComId = info.ReceiptNumber;
                }

                if (receipt.IsDirty)
                {
                    receipt.ModifyId = View.CurrentUser;
                    receipt.ModifyTime = DateTime.Now;
                    op.EntrustSellReceiptSet(receipt);
                }
            }

            View.AuditBoardView.Presenter.OnViewInitialized();
            this.OnViewInitialized();
        }

        protected void OnGetOrderDetailCount(object sender, DataEventArgs<int> e)
        {
            e.Data = op.OrderDetailGetCount(View.OrderId);
        }

        protected void OnGetCashTrustLog(object sender, DataEventArgs<int> e)
        {
            View.CtLog = mp.CashTrustLogGetByCouponId(e.Data);
        }

        protected void GetQuantity()
        {
            View.TotalQuantity = op.OrderDetailGetList(0, 0, null, View.OrderId, OrderDetailTypes.Regular)
                                .Sum(x => x.ItemQuantity);
        }

        protected void OnUpdateOrderDetail(object sender, DataEventArgs<OrderDetail> e)
        {
            // we may want to also update ItemQuantity table?
            string auditStr = string.Empty;
            OrderDetail item = op.OrderDetailGet(e.Data.Guid);
            // if quantity is negative, no change
            if (e.Data.ItemQuantity > 0) // edit
            {
                if (item.IsLoaded && item.ItemQuantity != e.Data.ItemQuantity)
                {
                    auditStr = Phrase.ResourceManager.GetString("Edit") + " : " + item.ItemName + " " + Phrase.ResourceManager.GetString("Quantity") + ": " + item.ItemQuantity + "->" + e.Data.ItemQuantity;
                    item.ItemQuantity = e.Data.ItemQuantity;
                    op.OrderDetailSet(item);
                    op.OrderUpdateSumTotal(View.OrderId);
                }
            }
            else if (e.Data.ItemQuantity == 0) // delete
            {
                auditStr = Phrase.ResourceManager.GetString("Delete") + " : " + item.ItemName;
                op.OrderDetailDelete(e.Data.Guid);
            }

            if (item.ItemName != e.Data.ItemName)
            {
                auditStr += Phrase.Edit + " : " + item.ItemName + " ==> " + e.Data.ItemName;
                item.ItemName = e.Data.ItemName;
                op.OrderDetailSet(item);
            }

            if (!string.IsNullOrWhiteSpace(auditStr))
            {
                CommonFacade.AddAudit(View.OrderId, AuditType.Order, auditStr, View.CurrentUser, false);
            }
            this.OnViewInitialized();
        }

        protected void OnUpdateOrderShip(object sender, DataEventArgs<OrderShipArgs> e)
        {
            OrderShip os = op.GetOrderShipById(e.Data.Id);
            os.ShipCompanyId = e.Data.ShipCompanyId;
            os.ShipTime = e.Data.ShipTime;
            if (e.Data.ShipMemo.Length > View.ShipMemoLimit)
            {
                View.alertMessage = string.Format("出貨備註只限最多 {0} 個字。", View.ShipMemoLimit);
                return;
            }

            //檢查出貨日期是否大於等於 "預計出貨日期"
            ViewPponDeal deal = pp.ViewPponDealGetByBusinessHourGuid((Guid)View.BusinessHourGuid);
            if (e.Data.ShipTime >= (DateTime)deal.BusinessHourDeliverTimeS)
            {
                os.ShipTime = e.Data.ShipTime;
            }
            else
            {
                View.alertMessage = "修改失敗! 您所填的出貨日期必須大於預計出貨日期!";
                return;
            }

            //檢查運單編號是否重複
            if (OrderShipUtility.CheckDealShipNoExist(View.BusinessHourGuid, OrderClassification.LkSite, View.OrderId, e.Data.ShipNo))
            {
                View.alertMessage = "修改失敗! 運單編號重複, 請選擇別的單號!";
                return;
            }
            os.ShipNo = e.Data.ShipNo;
            os.ShipMemo = e.Data.ShipMemo;
            os.ModifyId = View.CurrentUser;
            os.ModifyTime = DateTime.Now;

            if (op.OrderShipUpdate(os))
            {
                View.alertMessage = "修改成功!";
                OrderShipLog osl = new OrderShipLog();
                osl.OrderShipId = os.Id;
                osl.ShipCompanyId = os.ShipCompanyId;
                osl.ShipTime = os.ShipTime;
                osl.ShipNo = os.ShipNo;
                osl.ShipMemo = os.ShipMemo;
                osl.CreateId = os.ModifyId;
                osl.CreateTime = os.ModifyTime;
                op.OrderShipLogSet(osl);

                #region 資策會訂單紀錄
                try
                {
                    //同部異動資策會訂單，標註要更新到資策會那邊
                    op.CompanyUserOrderUpdateWaitUpdate(os.OrderGuid, true);
                }
                catch (Exception ex)
                {
                    log.Error("異動companyUserOrder狀態時發生錯誤", ex);
                }
                #endregion 資策會訂單紀錄
            }
        }

        protected void OnDeleteOrderShip(object sender, DataEventArgs<OrderShipArgs> e)
        {
            if (!View.CheckIsShipable || View.CheckIsExchanging)
            {
                View.alertMessage = "已退貨或換貨處理中的訂單無法刪除出貨資訊!";
                return;
            }

            OrderShip os = op.GetOrderShipById(e.Data.Id);
            if (op.OrderShipDelete(e.Data.Id))
            {
                View.alertMessage = "刪除成功!";
                //寫Log
                OrderShipLog osl = new OrderShipLog();
                osl.OrderShipId = os.Id;
                osl.ShipCompanyId = null;
                osl.ShipTime = null;
                osl.ShipNo = null;
                osl.ShipMemo = null;
                osl.CreateId = View.CurrentUser;
                osl.CreateTime = DateTime.Now;
                op.OrderShipLogSet(osl);

                #region 資策會訂單紀錄
                try
                {
                    //同部異動資策會訂單，標註要更新到資策會那邊
                    op.CompanyUserOrderUpdateWaitUpdate(os.OrderGuid, true);
                }
                catch (Exception ex)
                {
                    log.Error("異動companyUserOrder狀態時發生錯誤", ex);
                }
                #endregion 資策會訂單紀錄
            }
        }

        protected void OnAddOrderShip(object sender, DataEventArgs<OrderShipArgs> e)
        {
            if (!View.CheckIsShipable)
            {
                View.alertMessage = "已退貨的訂單無法新增出貨資訊！";
                return;
            }
            OrderShip os = new OrderShip();
            os.OrderClassification = (int)OrderClassification.LkSite;
            os.OrderGuid = View.OrderId;
            os.ShipCompanyId = e.Data.ShipCompanyId;
            //檢查出貨日期是否大於等於 "預計出貨日期"
            ViewPponDeal deal = pp.ViewPponDealGetByBusinessHourGuid((Guid)View.BusinessHourGuid);
            if (e.Data.ShipTime >= (DateTime)deal.BusinessHourDeliverTimeS)
            {
                os.ShipTime = e.Data.ShipTime;
            }
            else
            {
                View.alertMessage = "您所填的出貨日期必須大於預計出貨日期！";
                return;
            }

            //檢查運單編號是否重複
            if (OrderShipUtility.CheckDealShipNoExist(View.BusinessHourGuid, OrderClassification.LkSite, View.OrderId, e.Data.ShipNo))
            {
                View.alertMessage = "運單編號重複，請選擇別的單號！";
                return;
            }

            if (e.Data.ShipMemo.Length > View.ShipMemoLimit)
            {
                View.alertMessage = string.Format("出貨備註只限最多 {0} 個字。", View.ShipMemoLimit);
            }

            if (OrderShipUtility.CheckShipDataExist(View.OrderId, OrderClassification.LkSite))
            {
                View.alertMessage = "出貨資訊已存在，請勿重複點選新增！";
            }

            os.ShipNo = e.Data.ShipNo;
            os.ShipMemo = e.Data.ShipMemo;
            os.Type = (int)OrderShipType.Normal;
            os.CreateId = View.CurrentUser;
            os.CreateTime = DateTime.Now;
            os.ModifyId = View.CurrentUser;
            os.ModifyTime = DateTime.Now;

            os.Id = op.OrderShipSet(os);
            if (os.Id > 0)
            {
                View.alertMessage = "新增成功!";

                //寫Log
                OrderShipLog osl = new OrderShipLog();
                osl.OrderShipId = os.Id;
                osl.ShipCompanyId = os.ShipCompanyId;
                osl.ShipTime = os.ShipTime;
                osl.ShipNo = os.ShipNo;
                osl.ShipMemo = os.ShipMemo;
                osl.Type = os.Type;
                osl.CreateId = os.CreateId;
                osl.CreateTime = os.CreateTime;
                op.OrderShipLogSet(osl);


                //宅配已配送出貨要發推播給使用者(僅只有新增的第一次需要push)
                var order = op.OrderGet(os.OrderGuid);
                var msg = string.Format("17Life商品(訂單：{0})已出貨囉，查看詳情", order.OrderId);
                NotificationFacade.PushMemberOrderMessageOnWorkTime(order.UserId, msg, os.OrderGuid);
                log.Info("宅配已出貨 OrderDetail.OnAddOrderShip: " +
                            string.Format("UserId={0}, pmsg={1}, Guid={2}", order.UserId, msg, order.OrderId));
                
                #region 資策會訂單紀錄
                try
                {
                    //同部異動資策會訂單，標註要更新到資策會那邊
                    op.CompanyUserOrderUpdateWaitUpdate(os.OrderGuid, true);
                }
                catch (Exception ex)
                {
                    log.Error("異動companyUserOrder狀態時發生錯誤", ex);
                }
                #endregion 資策會訂單紀錄
            }
        }

        protected void OnSendRefundResult(object sender, EventArgs e)
        {
            OrderFacade.SendPponMail(MailContentType.RefundResult, View.OrderId);
            CommonFacade.AddAudit(View.OrderId, AuditType.Order, "寄出處理通知", View.CurrentUser, true);
            View.AuditBoardView.Presenter.OnViewInitialized();
        }

        protected void OnReturnApplicationResend(object sender, DataEventArgs<string> e)
        {
            string sme = e.Data;
            if (!RegExRules.CheckEmail(sme))
            {
                CommonFacade.AddAudit(View.OrderId, AuditType.Order, "退貨申請書未寄出! Email格式有誤! " + sme, View.CurrentUser, true);
                View.AuditBoardView.Presenter.OnViewInitialized();
                return;
            }

            try
            {
                OrderFacade.SendPponMail(MailContentType.ReturnApplicationformResend, View.OrderId, true, null, string.Empty, sme);
                CommonFacade.AddAudit(View.OrderId, AuditType.Order, "退貨申請書已寄出 - " + sme, View.CurrentUser, true);
                View.AuditBoardView.Presenter.OnViewInitialized();
                this.OnViewInitialized();
            }
            catch (Exception)
            {
                CommonFacade.AddAudit(View.OrderId, AuditType.Order, "退貨申請書未寄出! - " + sme, View.CurrentUser, true);
                View.AuditBoardView.Presenter.OnViewInitialized();
            }
        }

        protected void OnReturnDiscountResend(object sender, DataEventArgs<string> e)
        {
            string sme = e.Data;
            if (!RegExRules.CheckEmail(sme))
            {
                CommonFacade.AddAudit(View.OrderId, AuditType.Order, "退回或折讓證明單未寄出! Email格式有誤! " + sme, View.CurrentUser, true);
                View.AuditBoardView.Presenter.OnViewInitialized();
                return;
            }

            try
            {
                OrderFacade.SendPponMail(MailContentType.ReturnDiscountResend, View.OrderId, true, null, string.Empty, sme);
                CommonFacade.AddAudit(View.OrderId, AuditType.Order, "退回或折讓證明單已寄出 - " + sme, View.CurrentUser, true);
                View.AuditBoardView.Presenter.OnViewInitialized();
                this.OnViewInitialized();
            }
            catch (Exception)
            {
                CommonFacade.AddAudit(View.OrderId, AuditType.Order, "退回或折讓證明單未寄出! - " + sme, View.CurrentUser, true);
                View.AuditBoardView.Presenter.OnViewInitialized();
            }
        }

        protected void OnCouponAvailableToggle(object sender, EventArgs e)
        {
            int couponId = GetCouponId(((GridViewCommandEventArgs)e).CommandArgument.ToString());
            bool couponAvailable = GetCouponAvailable(((GridViewCommandEventArgs)e).CommandArgument.ToString());
            Coupon c = pp.CouponGet(couponId);
            c.Available = !couponAvailable;
            pp.CouponSet(c);
            if (c.Available ?? false)
            {
                CommonFacade.AddAudit(View.OrderId, AuditType.Order, "憑證ID: " + c.Id.ToString() + " 強制開放", View.CurrentUser, true);
            }
            else
            {
                CommonFacade.AddAudit(View.OrderId, AuditType.Order, "憑證ID: " + c.Id.ToString() + " 關閉強制開放", View.CurrentUser, true);
            }
            OnViewInitialized();
            View.AuditBoardView.Presenter.OnViewInitialized();
        }

        protected void OnRecoveredConfirm(object sender, EventArgs e)
        {
            int couponId = GetCouponId(((GridViewCommandEventArgs)e).CommandArgument.ToString());

            CashTrustLog ct = mp.CashTrustLogGetByCouponId(couponId);
            if (ct != null)
            {
                bool isUnrecovered = Helper.IsFlagSet(ct.SpecialStatus, TrustSpecialStatus.VerificationUnrecovered);

                if (isUnrecovered)
                {
                    ct.SpecialStatus = (int)Helper.SetFlag(false, ct.SpecialStatus, TrustSpecialStatus.VerificationUnrecovered);
                    mp.CashTrustLogSet(ct);
                    CommonFacade.AddAudit(View.OrderId, AuditType.Order, "憑證#" + ct.CouponSequenceNumber + "強制核銷款項已取回", View.CurrentUser, true);
                }
                else
                {
                    ct.SpecialStatus = (int)Helper.SetFlag(true, ct.SpecialStatus, TrustSpecialStatus.VerificationUnrecovered);
                    mp.CashTrustLogSet(ct);
                    CommonFacade.AddAudit(View.OrderId, AuditType.Order, "憑證#" + ct.CouponSequenceNumber + "強制核銷款項未取回", View.CurrentUser, true);
                }
                OnViewInitialized();
                View.AuditBoardView.Presenter.OnViewInitialized();
            }
        }

        //[Obsolete]
        protected void OnInvoiceMailBackSet(object sender, DataEventArgs<int> e)
        {
            EinvoiceMain invoice = op.EinvoiceMainGetByCouponid(e.Data);
            if (invoice.Id == 0)
            {
                invoice = op.EinvoiceMainGet(View.OrderId);
            }

            if (invoice.Id > 0)
            {
                invoice.MarkOld();
                invoice.InvoiceMailbackPaper = (invoice.InvoiceMailbackPaper != null && invoice.InvoiceMailbackPaper == true) ? false : true;
                EinvoiceFacade.SetEinvoiceMainWithLog(invoice, View.CurrentUser);

                string isMailBack = (invoice.InvoiceMailbackPaper.Value) ? "發票已寄回" : "發票未寄回";
                CommonFacade.AddAudit(View.OrderId, AuditType.Order, "發票號碼#" + invoice.InvoiceNumber + isMailBack, View.CurrentUser, true);
                OnViewInitialized();
                View.AuditBoardView.Presenter.OnViewInitialized();
            }
        }

        protected void OnReceiptMailBackSet(object sender, DataEventArgs<int> e)
        {
            EntrustSellReceipt receipt = op.EntrustSellReceiptGetByCouponId(e.Data);
            if (receipt.IsLoaded)
            {
                receipt.MailbackReceipt = !receipt.MailbackReceipt;
                receipt.ModifyId = View.CurrentUser;
                receipt.ModifyTime = DateTime.Now;
                op.EntrustSellReceiptSet(receipt);
                OnViewInitialized();
                View.AuditBoardView.Presenter.OnViewInitialized();
            }
        }

        protected void OnReceiptMailBackAllowanceSet(object sender, DataEventArgs<int> e)
        {
            EntrustSellReceipt receipt = op.EntrustSellReceiptGetByCouponId(e.Data);
            if (receipt.IsLoaded)
            {
                receipt.MailbackAllowance = !receipt.MailbackAllowance;
                receipt.ModifyId = View.CurrentUser;
                receipt.ModifyTime = DateTime.Now;
                op.EntrustSellReceiptSet(receipt);
                OnViewInitialized();
                View.AuditBoardView.Presenter.OnViewInitialized();
            }
        }

        //[Obsolete]
        protected void OnReceiptMailBackPaperSet(object sender, DataEventArgs<int> e)
        {
            EntrustSellReceipt receipt = op.EntrustSellReceiptGetByCouponId(e.Data);
            if (receipt.IsLoaded)
            {
                receipt.MailbackPaper = !receipt.MailbackPaper;
                receipt.ModifyId = View.CurrentUser;
                receipt.ModifyTime = DateTime.Now;
                op.EntrustSellReceiptSet(receipt);
                OnViewInitialized();
                View.AuditBoardView.Presenter.OnViewInitialized();
            }
        }

        private void OnCreditcardReferSet(object sender, EventArgs e)
        {
            CreditcardOrder co = op.CreditcardOrderGetByOrderGuid(View.OrderId);
            if (co != null)
            {
                Button btn = (Button)sender;
                string id = btn.ID;
                if (id != "btRefer")
                {
                    PaymentFacade.SetCreditcardOrderReferFail(co.Id.ToString().Split(","), View.CurrentUser);
                }
                else
                {
                    PaymentFacade.SetCreditcardOrderRefer(co.Id.ToString().Split(","), View.CurrentUser);
                }
            }
            OnViewInitialized();
        }

        private void OnOrderUserMemoSet(object sender, DataEventArgs<OrderUserMemoList> e)
        {
            op.OrderUserMemoListSet(e.Data);
            View.SetOrderUserMemoInfo(op.OrderUserMemoListGetList(View.OrderId));
        }

        private void OnOrderUserMemoDelete(object sender, DataEventArgs<int> e)
        {
            op.OrderUserMemoListDelete(e.Data);
            View.SetOrderUserMemoInfo(op.OrderUserMemoListGetList(View.OrderId));
        }

        private void OnBuildSubCategory(object sender, DataEventArgs<int> e)
        {
            ServiceMessageCategoryCollection serviceCates = mp.ServiceMessageCategoryCollectionGetListByParentId(e.Data);
            View.BuildServiceCategory(serviceCates, View.ddlServiceSubCategory);
        }

        private void OnOrderFreezeSet(object sender, DataEventArgs<OrderFreezeInfo> e)
        {
            //宅配訂單才能執行訂單凍結功能
            if (!View.IsToHouse)
            {
                View.alertMessage = "憑證訂單不提供進行凍結請款功能";
                return;
            }
            //訂單凍結須排除已列入對帳單明細之份數
            var ctLogs = (IEnumerable<CashTrustLog>)mp.CashTrustLogListGetByOrderId(e.Data.OrderGuid).ToList();
            var bsDetails = GetPaidTrustIds(ctlogs.Select(x => x.TrustId).ToList());
            ctLogs = ctlogs.Where(x => !bsDetails.Contains(x.TrustId));

            var ctslc = new CashTrustStatusLogCollection();
            string reason = "";

            if (e.Data.FreezeType == FreezeType.FreezeOrder)
            {
                if (!ctLogs.Any())
                {
                    View.alertMessage = "訂單已請款或請款中，無法進行凍結請款作業";
                    return;
                }
                foreach (CashTrustLog ctlog in ctLogs)
                {
                    ctlog.SpecialStatus = (int)Helper.SetFlag(true, ctlog.SpecialStatus, TrustSpecialStatus.Freeze);
                    reason = "【系統凍結請款】";
                    ctslc.Add(CashTrustStatusLogSet(ctlog));
                }
            }
            else
            {
                foreach (CashTrustLog ctlog in ctLogs)
                {
                    ctlog.SpecialStatus = (int)Helper.SetFlag(false, ctlog.SpecialStatus, TrustSpecialStatus.Freeze);
                    reason = "【系統取消凍結請款】";
                    ctslc.Add(CashTrustStatusLogSet(ctlog));
                }
            }

            mp.CashTrustLogCollectionSet(ctlogs);
            mp.CashTrustStatusLogCollectionSet(ctslc);

            CommonFacade.AddAudit(e.Data.OrderGuid, AuditType.Freeze, reason + ":" + e.Data.Reason, e.Data.UserId, false);

            View.alertMessage = "資料已" + reason;

            EmailFacade.SendFreezeMail(e.Data.FreezeType, e.Data.OrderGuid, reason + ":" + e.Data.Reason);

            //記錄頁面reload 以顯示最新紀錄
            View.AuditBoardView.Presenter.OnViewInitialized();
            OnViewInitialized();
        }

        protected void OnGetSMS(object sender, DataEventArgs<int> e)
        {
            ViewPponCoupon c = pp.ViewPponCouponGet(e.Data);
            Member m = mp.MemberGet(c.MemberEmail);

            SMS sms = new SMS();
            SmsContent sc = pp.SMSContentGet(c.BusinessHourGuid);
            string[] msgs = sc.Content.Split('|');
            if (c.CouponId != null)
            {
                //判斷天貓檔次
                if ((c.BusinessHourStatus & (int)BusinessHourStatus.TmallDeal) > 0)
                {
                    OrderCorresponding order_corresponding = op.OrderCorrespondingListGetByOrderGuid(c.OrderGuid);
                    m.Mobile = order_corresponding.Mobile;
                }
                DateTime finalExpireDate = GetFinalExpireDate(c.OrderGuid.ToString());
                string branch = OrderFacade.SmsGetPhone(pp.CouponGet(c.CouponId.Value).OrderDetailId);    //分店資訊 & 多重選項
                string msg = CouponFacade.SmsMessage(c.BusinessHourGuid, c.GroupOrderStatus, c.CouponCode, branch, msgs, c.SequenceNumber, c.BusinessHourDeliverTimeS, finalExpireDate, c.CouponStoreSequence);

                sms.SendMessage("", msg, "", m.Mobile, m.UserName, SmsType.Ppon, c.BusinessHourGuid, c.CouponId.ToString());
            }
            View.SmsCount = pp.SMSLogGetCountByPpon(m.UserName, e.Data.ToString());
            View.alertMessage = "發送成功";
            OnViewInitialized();
        }

        /// <summary>
        /// 建立退貨單
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCreateReturnForm(object sender, ReturnFormArgs e)
        {
            if (!e.OrderProductIds.Any())
            {
                View.alertMessage = "請選擇商品退貨數量!";
                return;
            }

            EinvoiceMainCollection emc = op.EinvoiceMainCollectionGet("order_guid", View.OrderId.ToString(), true);
            if (emc.Count > 0)
            {
                if (emc.Where(x => x.InvoiceMode2 == (int)InvoiceMode2.Triplicate && x.InvoiceNumber == null && x.InvoiceStatus == (int)EinvoiceType.C0401).ToList().Count > 0)
                {
                    View.alertMessage = "發票二聯改三聯尚未完成，無法申請退貨";
                    return;
                }
            }

            //PCHOME須先建立退貨單->再建立逆物流

            int? dummy;
            int? isNotifyChannel = null;
            if (e.ChannelAgent == AgentChannel.PayEasyOrder)
            {
                //目前先pez的退貨需要通知
                isNotifyChannel = (int)NotifyChannel.Init;
            }

            CreateReturnFormResult result = ReturnService.CreateRefundForm(e.OrderGuid, e.OrderProductIds, e.RefundSCash, View.CurrentUser,
                e.RefundReason, e.ReceiverName, e.ReceiverAddress, null, isNotifyChannel, out dummy, true);

            if (result != CreateReturnFormResult.Created)
            {
                switch (result)
                {
                    case CreateReturnFormResult.ProductsUnreturnable:
                        View.alertMessage = "欲退項目的狀態無法退貨";
                        break;
                    case CreateReturnFormResult.AtmOrderNeedAtmRefundAccount:
                        View.alertMessage = "ATM訂單, 請先設定ATM退款帳號";
                        break;
                    case CreateReturnFormResult.OldOrderNeedAtmRefundAccount:
                        View.alertMessage = "舊訂單, 請先設定ATM退款帳號";
                        break;
                    case CreateReturnFormResult.IspOrderNeedAtmRefundAccount:
                        View.alertMessage = "超商付款訂單, 請先設定ATM退款帳號";
                        break;
                    case CreateReturnFormResult.InvalidArguments:
                        int comboCount = ReturnService.QueryComboPackCount(View.OrderId);
                        if (!Equals(0, e.OrderProductIds.Count() % comboCount))
                        {
                            View.alertMessage = string.Format("此訂單為成套售出，退貨需成套 [({0})的倍數] 申請。", comboCount.ToString());
                        }
                        else
                        {
                            View.alertMessage = "無法建立退貨單, 請洽技術部";
                        }
                        break;
                    case CreateReturnFormResult.OrderNotCreate:
                        View.alertMessage = "訂單未完成付款, 無法建立退貨單";
                        break;
                    case CreateReturnFormResult.IspOrderNotCreate:
                        View.alertMessage = "訂單未完成付款, 無法建立退貨單。請改按[取消訂單]，進行訂單取消申請";
                        break;
                    case CreateReturnFormResult.InstallmentOrderNeedAtmRefundAccount:
                        View.alertMessage = "分期且部分退, 請先設定ATM退款帳號";
                        break;
                    case CreateReturnFormResult.ProductsIsExchanging:
                        View.alertMessage = "訂單有換貨處理中商品，無法建立退貨單";
                        break;
                    default:
                        View.alertMessage = "不明錯誤, 請洽技術部";
                        break;
                }
            }
            else
            {
                if (emc.Count > 0 && emc.Any(x => x.InvoiceRequestTime.HasValue))
                {
                    UserRefundFacade.EinvocieCancelRequestProcess(emc, View.OrderId, e.OrderProductIds.ToList(), DeliveryType.ToHouse, View.CurrentUser);
                }

                int returnFormId = op.ReturnFormGetListByOrderGuid(e.OrderGuid)
                        .OrderByDescending(x => x.Id)
                        .First().Id;
                string auditMessage = string.Format("退貨單({0})建立. 原因:{1}", returnFormId.ToString(), e.RefundReason);
                CommonFacade.AddAudit(e.OrderGuid, AuditType.Refund, auditMessage, View.CurrentUser, false);
                OrderFacade.SendCustomerReturnFormApplicationMail(returnFormId);
                EmailFacade.SendProductRefundOrExchangeToSeller(OrderReturnType.Refund, returnFormId);
                
                #region clear FinalBalanceSheetDate

                if (PponOrderManager.CheckIsOverTrialPeriod(e.OrderGuid))
                {
                    DealAccounting da = pp.DealAccountingGet(e.Bid);
                    if (da.IsLoaded && da.FinalBalanceSheetDate.HasValue)
                    {
                        da.FinalBalanceSheetDate = null;
                        pp.DealAccountingSet(da);

                    }

                    ComboDeal cd = pp.GetComboDeal(e.Bid);
                    if (cd.IsLoaded)
                    {
                        Guid mainGuid = Guid.Empty;
                        Guid.TryParse(cd.MainBusinessHourGuid.ToString(), out mainGuid);
                        DealAccounting mainDa = pp.DealAccountingGet(mainGuid);
                        if (mainDa.IsLoaded && mainDa.FinalBalanceSheetDate.HasValue)
                        {
                            mainDa.FinalBalanceSheetDate = null;
                            pp.DealAccountingSet(mainDa);

                        }
                    }
                }

                #endregion
                
                View.ReturnFormCreated(DeliveryType.ToHouse);
            }

            View.ShowHomeDeliveryInfo();
            View.SetHomeDeliveryProductInfo();
            View.SetHomeDeliverReturnInfo();
            RefreshView();
            RefreshAuditBoard();
        }

        //更新退貨收件資訊
        private void OnUpdateReturnFormReceiver(object sender, ReturnFormReceiverArgs e)
        {
            ReturnForm rf = op.ReturnFormGet(e.ReturnFormId);
            if (rf.IsLoaded)
            {
                string changelog = string.Empty;
                if (rf.ReceiverName != e.ReceiverName)
                {
                    changelog = string.Format("收件人: \"{0}\" → \"{1}\"", rf.ReceiverName ?? "null", e.ReceiverName);
                }
                if (rf.ReceiverAddress != e.ReceiverAddress)
                {
                    changelog += string.IsNullOrEmpty(changelog) ? ", " : "";
                    changelog += string.Format("地址: \"{0}\" → \"{1}\"", rf.ReceiverAddress ?? "null", e.ReceiverAddress);
                }

                rf.ReceiverName = e.ReceiverName;
                rf.ReceiverAddress = e.ReceiverAddress;
                rf.ModifyTime = DateTime.Now;
                rf.ModifyId = View.CurrentUser;
                op.ReturnFormSet(rf);
                
                CommonFacade.AddAudit(View.OrderId, AuditType.Order, 
                    string.Format("退貨單({0})更新: {1}", e.ReturnFormId, changelog), View.CurrentUser, true);
                RefreshAuditBoard();
            }
        }

        private void OnCreateCouponReturnForm(object sender, CouponReturnFormArgs e)
        {
            if (!e.CouponIds.Any())
            {
                View.alertMessage = "請選擇憑證!";
                return;
            }

            EinvoiceMainCollection emc = op.EinvoiceMainCollectionGet("order_guid", View.OrderId.ToString(), true);
            if (emc.Count > 0)
            {
                if (emc.Where(x => x.InvoiceMode2 == (int)InvoiceMode2.Triplicate && x.InvoiceNumber == null && x.InvoiceStatus == (int)EinvoiceType.C0401).ToList().Count > 0)
                {
                    View.alertMessage = "發票二聯改三聯尚未完成，無法申請退貨";
                    return;
                }
            }

            int? returnFormId;
            

            ViewCouponListMain vclm = mp.GetCouponListMainByOid(View.OrderId);
            string errorMsg = "";
            if (Helper.IsFlagSet(vclm.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon) &&
                Helper.IsFlagSet(vclm.GroupOrderStatus.Value, (int)GroupOrderStatus.FamiDeal))
            {
                //全家寄杯退貨
                if (!OrderFacade.FamilyNetPincodeReturn(View.OrderId, View.BusinessHourGuid, out errorMsg))
                {
                    View.alertMessage = errorMsg;
                    return;
                }
            }
            else if (Helper.IsFlagSet(vclm.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon) &&
                     Helper.IsFlagSet(vclm.GroupOrderStatus.Value, (int)GroupOrderStatus.HiLifeDeal))
            {
                //萊爾富寄杯
                errorMsg = "";
                if (cp.EnableHiLifeDealSetup && !OrderFacade.HiLifePincodeReturn(View.OrderId, out errorMsg))
                {
                    View.alertMessage = errorMsg;
                    return;
                }
            }


            CreateReturnFormResult result = ReturnService.CreateCouponReturnForm(
                e.OrderGuid, e.CouponIds, e.RefundSCash, View.CurrentUser, e.RefundReason, out returnFormId, true);

            if (result != CreateReturnFormResult.Created)
            {
                switch (result)
                {
                    case CreateReturnFormResult.ProductsUnreturnable:
                        View.alertMessage = "欲退項目的狀態無法退貨";
                        break;
                    case CreateReturnFormResult.AtmOrderNeedAtmRefundAccount:
                        View.alertMessage = "ATM訂單, 請先設定ATM退款帳號";
                        break;
                    case CreateReturnFormResult.OldOrderNeedAtmRefundAccount:
                        View.alertMessage = "舊訂單, 請先設定ATM退款帳號";
                        break;
                    case CreateReturnFormResult.InvalidArguments:
                        View.alertMessage = "無法建立退貨單, 請洽技術部";
                        break;
                    case CreateReturnFormResult.OrderNotCreate:
                        View.alertMessage = "訂單未完成付款, 無法建立退貨單";
                        break;
                    case CreateReturnFormResult.InstallmentOrderNeedAtmRefundAccount:
                        View.alertMessage = "分期且部分退, 請先設定ATM退款帳號";
                        break;
                    case CreateReturnFormResult.UnusedCouponLock:
                        View.alertMessage = @"注意！本套成套票券憑證中，有處於鎖定憑證的狀態，目前無法申請退貨。\n 若您需申請退貨，請先將鎖定的憑證依下列建議先行處理：\n 1.先將鎖定的憑證兌換完畢後，再進行退貨申請\n 2.依照旅遊定型化契約規範與商家先行聯繫處理後，再進行退貨申請。";
                        break;
                    default:
                        View.alertMessage = "不明錯誤, 請洽技術部";
                        break;
                }
            }
            else
            {
                if (emc.Count > 0 && emc.Any(x => x.InvoiceRequestTime.HasValue))
                {
                    UserRefundFacade.EinvocieCancelRequestProcess(emc, View.OrderId, e.CouponIds.ToList(), DeliveryType.ToShop, View.CurrentUser);
                }
                string auditMessage = string.Format("退貨單({0})建立. 原因:{1}", returnFormId.Value.ToString(), e.RefundReason);
                CommonFacade.AddAudit(e.OrderGuid, AuditType.Refund, auditMessage, View.CurrentUser, false);

                OrderFacade.SendCustomerReturnFormApplicationMail(returnFormId.Value);
                //憑證不用寄(宅配)退貨通知給廠商

                View.ReturnFormCreated(DeliveryType.ToShop);
            }

            View.SetCouponInfo(pp.ViewPponCouponGetListRegularOnly(ViewPponCoupon.Columns.OrderGuid, View.OrderId));
            View.SetCouponReturnInfo();
            RefreshView();
            RefreshAuditBoard();
        }

        private void OnImmediateRefund(object sender, DataEventArgs<int> e)
        {
            ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(View.BusinessHourGuid);
            if (Helper.IsFlagSet(vpd.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon) && Helper.IsFlagSet(vpd.GroupOrderStatus.Value, (int)GroupOrderStatus.FamiDeal) && vpd.ItemPrice > 0)
            {
                List<FamilyNetPincode> famiPincodes = fami.GetFamilyNetListPincode(View.OrderId);
                if (famiPincodes.Any(x => x.Status == (byte)FamilyNetPincodeStatus.Completed && x.ReturnStatus != (int)FamilyNetPincodeReturnStatus.Completed && !x.IsVerified))
                {
                    View.alertMessage = "全家Pincode尚未註銷完畢，無法立即退貨。";
                    return;
                }
            }

            if (Helper.IsFlagSet(vpd.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon) && Helper.IsFlagSet(vpd.GroupOrderStatus.Value, (int)GroupOrderStatus.HiLifeDeal) && vpd.ItemPrice > 0)
            {
                List<HiLifePincode> hiLifePincodes = hiLife.GetHiLifePincode(View.OrderId);
                if (hiLifePincodes.Any(x => !x.IsVerified && x.ReturnStatus == (byte)HiLifeReturnStatus.Default))
                {
                    View.alertMessage = "萊爾富Pincode尚未註銷完畢，無法立即退貨。";
                    return;
                }

            }

            ReturnFormEntity returnForm = ReturnFormRepository.FindById(e.Data);
            ReturnService.Refund(returnForm, View.CurrentUser, true);
            if (returnForm.DeliveryType == DeliveryType.ToHouse)
            {
                View.ShowHomeDeliveryInfo();
                View.SetHomeDeliveryProductInfo();
                View.SetHomeDeliverReturnInfo();
            }
            else
            {
                View.SetCouponInfo(pp.ViewPponCouponGetListRegularOnly(ViewPponCoupon.Columns.OrderGuid, View.OrderId));
                View.SetCouponReturnInfo();
            }
            RefreshAuditBoard();
            RefreshView();
        }

        private void OnReceivedCreditNote(object sender, DataEventArgs<int> e)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(e.Data);
            returnForm.ReceivedCreditNote(View.CurrentUser);
            ReturnFormRepository.Save(returnForm);
            CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Order, string.Format("退貨單-{0} 折讓單已收回", returnForm.Id), View.CurrentUser, true);
            View.IsEnableElecAllowance = false;

            var eInvoices = op.EinvoiceMainGetListByOrderGuid(returnForm.OrderGuid)
                              .Where(x => x.InvoiceStatus != (int)EinvoiceType.C0701)
                              .OrderByDescending(x => x.InvoiceNumberTime)
                              .ToList();

            if (returnForm.DeliveryType == DeliveryType.ToHouse)
            {
                var einv = eInvoices.FirstOrDefault(x => x.AllowanceStatus == (int)AllowanceStatus.PaperAllowance);

                if (einv != null)
                {
                    einv.InvoiceMailbackAllowance = true;
                    EinvoiceFacade.SetEinvoiceMainWithLog(einv, View.CurrentUser);
                    CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, string.Format("發票號碼:{0}，折讓單已收回", einv.InvoiceNumber), View.CurrentUser, true);
                }
                View.SetHomeDeliverReturnInfo();
            }
            else
            {
                var couponIds = returnForm.GetReturnCouponIds();
                foreach (var couponId in couponIds)
                {
                    var emc = eInvoices.Where(x => x.CouponId == couponId && x.AllowanceStatus == (int)AllowanceStatus.PaperAllowance).ToList();
                    //成套禮券發票無couponId
                    emc.AddRange(ReturnService.GroupCouponOrder(eInvoices));

                    foreach (var einv in emc)
                    {
                        einv.InvoiceMailbackAllowance = true;
                        EinvoiceFacade.SetEinvoiceMainWithLog(einv, View.CurrentUser);
                        CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, string.Format("發票號碼:{0}，折讓單已收回", einv.InvoiceNumber), View.CurrentUser, true);
                    }
                }
                View.SetCouponReturnInfo();
            }
            RefreshAuditBoard();
            RefreshView();
        }

        private void OnChangeToElcAllowance(object sender, DataEventArgs<int> e)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(e.Data);
            try
            {
                if (returnForm.ProgressStatus == ProgressStatus.Processing)
                {
                    View.IsPaperAllowance = false;

                    var eInvoices = op.EinvoiceMainGetListByOrderGuid(returnForm.OrderGuid)
                                      .Where(x => x.InvoiceStatus != (int)EinvoiceType.C0701)
                                      .OrderByDescending(x => x.InvoiceNumberTime)
                                      .ToList();

                    if (returnForm.DeliveryType == DeliveryType.ToHouse)
                    {
                        var einv = eInvoices.FirstOrDefault();

                        if (einv != null)
                        {
                            if (einv.InvoiceMode2 == (int)InvoiceMode2.Duplicate && einv.AllowanceStatus == (int)AllowanceStatus.PaperAllowance && einv.OrderTime > cp.EinvAllowanceStartDate)
                            {
                                einv.AllowanceStatus = (int)AllowanceStatus.ElecAllowance;
                                EinvoiceFacade.SetEinvoiceMainWithLog(einv, View.CurrentUser);
                                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, string.Format("發票號碼:{0}，紙本折讓改電子折讓", einv.InvoiceNumber), View.CurrentUser, true);
                            }
                        }
                        View.SetHomeDeliverReturnInfo();
                    }
                    else
                    {
                        var couponIds = returnForm.GetReturnCouponIds();
                        foreach (var couponId in couponIds)
                        {
                            var emc = eInvoices.Where(x => x.CouponId == couponId).ToList();
                            //成套禮券發票無couponId
                            emc.AddRange(ReturnService.GroupCouponOrder(eInvoices));

                            foreach (var einv in emc)
                            {
                                if (einv.InvoiceMode2 == (int)InvoiceMode2.Duplicate && einv.AllowanceStatus == (int)AllowanceStatus.PaperAllowance && einv.OrderTime > cp.EinvAllowanceStartDate)
                                {
                                    einv.AllowanceStatus = (int)AllowanceStatus.ElecAllowance;
                                    EinvoiceFacade.SetEinvoiceMainWithLog(einv);
                                    CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, string.Format("發票號碼:{0}，紙本折讓改電子折讓", einv.InvoiceNumber), View.CurrentUser, true);
                                }
                            }
                        }
                        View.SetCouponReturnInfo();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, string.Format("退貨單號:{0}，紙本折讓改電子折讓失敗{1}", e.Data, ex.ToString()), View.CurrentUser, true);
            }

            RefreshAuditBoard();
            RefreshView();
        }

        private void OnChangeToPaperAllowance(object sender, DataEventArgs<int> e)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(e.Data);
            try
            {
                if (returnForm.ProgressStatus == ProgressStatus.Processing)
                {
                    View.IsPaperAllowance = true;

                    var eInvoices = op.EinvoiceMainGetListByOrderGuid(returnForm.OrderGuid)
                                      .Where(x => x.InvoiceStatus != (int)EinvoiceType.C0701)
                                      .OrderByDescending(x => x.InvoiceNumberTime)
                                      .ToList();

                    if (returnForm.DeliveryType == DeliveryType.ToHouse)
                    {
                        var einv = eInvoices.FirstOrDefault();

                        if (einv != null)
                        {
                            if (einv.InvoiceMode2 == (int)InvoiceMode2.Duplicate && einv.AllowanceStatus == (int)AllowanceStatus.ElecAllowance)
                            {
                                einv.AllowanceStatus = (int)AllowanceStatus.PaperAllowance;
                                EinvoiceFacade.SetEinvoiceMainWithLog(einv);
                                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, string.Format("發票號碼:{0}，電子折讓改紙本折讓", einv.InvoiceNumber), View.CurrentUser, true);
                            }
                        }
                        View.SetHomeDeliverReturnInfo();
                    }
                    else
                    {
                        var couponIds = returnForm.GetReturnCouponIds();
                        foreach (var couponId in couponIds)
                        {
                            var emc = eInvoices.Where(x => x.CouponId == couponId).ToList();
                            //成套禮券發票無couponId
                            emc.AddRange(ReturnService.GroupCouponOrder(eInvoices));

                            foreach (var einv in emc)
                            {
                                if (einv.InvoiceMode2 == (int)InvoiceMode2.Duplicate && einv.AllowanceStatus == (int)AllowanceStatus.ElecAllowance)
                                {
                                    einv.AllowanceStatus = (int)AllowanceStatus.PaperAllowance;
                                    EinvoiceFacade.SetEinvoiceMainWithLog(einv);
                                    CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, string.Format("發票號碼:{0}，電子折讓改紙本折讓", einv.InvoiceNumber), View.CurrentUser, true);
                                }
                            }
                        }
                        View.SetCouponReturnInfo();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, string.Format("退貨單號:{0}，電子折讓改紙本折讓失敗{1}", e.Data, ex.ToString()), View.CurrentUser, true);
            }
            RefreshAuditBoard();
            RefreshView();
        }

        private void OnCancelReturnForm(object sender, DataEventArgs<int> e)
        {
            ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(View.BusinessHourGuid);
            if (Helper.IsFlagSet(vpd.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon) && Helper.IsFlagSet(vpd.GroupOrderStatus.Value, (int)GroupOrderStatus.FamiDeal) && vpd.ItemPrice > 0)
            {
                View.alertMessage = "全家寄杯無法取消退貨。";
                return;
            }

            if (Helper.IsFlagSet(vpd.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon) && Helper.IsFlagSet(vpd.GroupOrderStatus.Value, (int)GroupOrderStatus.HiLifeDeal) && vpd.ItemPrice > 0)
            {
                View.alertMessage = "萊爾富寄杯無法取消退貨。";
                return;
            }
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(e.Data);
            if (ReturnService.Cancel(returnForm, View.CurrentUser))
            {
                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Order, string.Format("取消退貨單({0})", returnForm.Id), View.CurrentUser, true);

                if (returnForm.DeliveryType == DeliveryType.ToHouse)
                {
                    View.ShowHomeDeliveryInfo();
                    View.SetHomeDeliveryProductInfo();
                    View.SetHomeDeliverReturnInfo();
                }
                else
                {
                    View.SetCouponInfo(pp.ViewPponCouponGetListRegularOnly(ViewPponCoupon.Columns.OrderGuid, View.OrderId));
                    View.SetCouponReturnInfo();
                }
            }
            else
            {
                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Order, string.Format("取消退貨單({0})失敗", returnForm.Id), View.CurrentUser, true);
                View.alertMessage = "取消退貨失敗, 請洽技術部!";
            }

            RefreshAuditBoard();
            RefreshView();
        }

        private void OnUpdateAtmRefund(object sender, DataEventArgs<int> e)
        {
            if (ReturnService.AtmRefundAccountExist(View.OrderId))
            {
                ReturnFormEntity returnForm = ReturnFormRepository.FindById(e.Data);
                returnForm.ChangeProgressStatus(ProgressStatus.Processing, View.CurrentUser);
                ReturnFormRepository.Save(returnForm);

                if (returnForm.DeliveryType == DeliveryType.ToHouse)
                {
                    View.ShowHomeDeliveryInfo();
                    View.SetHomeDeliveryProductInfo();
                    View.SetHomeDeliverReturnInfo();
                }
                else
                {
                    View.SetCouponInfo(pp.ViewPponCouponGetListRegularOnly(ViewPponCoupon.Columns.OrderGuid, View.OrderId));
                    View.SetCouponReturnInfo();
                }
            }
            else
            {
                View.alertMessage = "請先更新ATM退款的帳戶資訊。";
            }

        }

        private void OnRecoverVendorProgress(object sender, DataEventArgs<int> e)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(e.Data);
            returnForm.VendorReturnProcessRecover(View.CurrentUser);
            ReturnFormRepository.Save(returnForm);
            CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Order, string.Format("退貨單{0} - 復原廠商狀態", returnForm.Id), View.CurrentUser, false);
            if (returnForm.DeliveryType == DeliveryType.ToHouse)
            {
                View.SetHomeDeliverReturnInfo();
            }
            else
            {
                View.SetCouponReturnInfo();
            }
            RefreshAuditBoard();
            RefreshView();
        }

        private void OnProcessedVendorUnreturnable(object sender, DataEventArgs<int> e)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(e.Data);
            returnForm.VendorUnreturnableProcessed(View.CurrentUser);
            ReturnFormRepository.Save(returnForm);

            CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Order, string.Format("退貨單{0} - 廠商無法退貨已處理", returnForm.Id), View.CurrentUser, false);
            if (returnForm.DeliveryType == DeliveryType.ToHouse)
            {
                View.SetHomeDeliverReturnInfo();
            }
            else
            {
                View.SetCouponReturnInfo();
            }
            RefreshAuditBoard();
            RefreshView();
        }
        

        private void OnScashToCash(object sender, DataEventArgs<int> e)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(e.Data);
            CommonFacade.AddAudit(View.OrderId, AuditType.Order, string.Format("退貨單({0}) - 處理購物金轉現金", e.Data), View.CurrentUser, false);
            ReturnService.RefundScashToCash(e.Data, View.CurrentUser);
            if (returnForm.DeliveryType == DeliveryType.ToHouse)
            {
                View.SetHomeDeliverReturnInfo();
            }
            else
            {
                View.SetCouponReturnInfo();
            }
            RefreshAuditBoard();
            RefreshView();
        }

        private void OnCashToSCash(object sender, DataEventArgs<int> e)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(e.Data);
            CommonFacade.AddAudit(View.OrderId, AuditType.Order, string.Format("退貨單({0}) - 處理退現改退購物金", e.Data), View.CurrentUser, false);
            ReturnService.RefundCashToSCash(e.Data, View.CurrentUser);
            if (returnForm.DeliveryType == DeliveryType.ToHouse)
            {
                View.SetHomeDeliverReturnInfo();
            }
            else
            {
                View.SetCouponReturnInfo();
            }
            RefreshAuditBoard();
            RefreshView();
        }

        private void OnArtificialRefund(object sender, DataEventArgs<int> e)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(e.Data);
            CtAtmRefund atmR = op.CtAtmRefundGetLatest(View.OrderId);
            //Atm退款失敗 若標註人工匯退狀態
            //則視同人工退款完成 並標註flag為未完成
            //由隔天排程去更新退款狀態
            if (atmR.Status == (int)AtmRefundStatus.RefundFail)
            {
                atmR.Flag ^= (int)AtmRefundFlag.Checked;
                //恢復退貨單為Atm退款中狀態
                returnForm.ChangeProgressStatus(ProgressStatus.AtmQueueing, View.CurrentUser);
                ReturnFormRepository.Save(returnForm);
            }
            atmR.Status = (int)AtmRefundStatus.RefundSuccess;
            atmR.ProcessDate = DateTime.Now;
            op.CtAtmRefundSet(atmR);
            CommonFacade.AddAudit(View.OrderId, AuditType.Order, string.Format("ATM退款申請序號({0}) - 因[人工匯退]處理，更新[ATM退款狀態]為[退貨完成]", atmR.Si), View.CurrentUser, false);
            if (returnForm.DeliveryType == DeliveryType.ToHouse)
            {
                View.SetHomeDeliverReturnInfo();
            }
            else
            {
                View.SetCouponReturnInfo();
            }
            RefreshAuditBoard();
            RefreshView();
        }

        private void OnRequestInvoice(object sender, DataEventArgs<string> e)
        {
            EinvoiceMain einv = op.EinvoiceMainGet(e.Data);
            //檢查發票是否跨期 // 下次改用字軌判斷是否跨期
            //Boolean IsOverTerm = (einv.InvoiceNumberTime.Value.Month % 2 == 1)
            //    ? ((einv.InvoiceNumberTime.Value > new DateTime(einv.InvoiceNumberTime.Value.Year, einv.InvoiceNumberTime.Value.Month, 15) 
            //    && einv.InvoiceNumberTime.Value > new DateTime(DateTime.Now.Year , DateTime.Now.Month,15)) ? true : false)
            //    : false;
            if (string.IsNullOrEmpty(einv.InvoiceBuyerName) || string.IsNullOrEmpty(einv.InvoiceBuyerAddress))
            {
                View.alertMessage = "收件人或地址不可為空白";
                return;
            }

            if (einv.InvoiceRequestTime == null)
            {
                //發票開立日期小於報稅日則變更載具
                //if (!IsOverTerm)
                //{
                //    einv.CarrierType = (int)CarrierType.None;
                //    CommonFacade.AddAudit(View.OrderId, AuditType.Order, "變更使用載具:無", View.CurrentUser, true);
                //    CommonFacade.AddAudit(View.OrderId, AuditType.Order, "註銷發票重新開立", View.CurrentUser, true);
                //}
                einv.InvoiceRequestTime = DateTime.Now;

                //更新原有的發票數據
                EinvoiceFacade.SetEinvoiceMainWithLog(einv, View.CurrentUser, einv.InvoiceRequestTime);
                CommonFacade.AddAudit(View.OrderId, AuditType.Order, "發票號碼: " + e.Data + " 申請紙本發票", View.CurrentUser, true);

                //if (einv.InvoiceNumberTime.Value < checkDate)
                //{
                //    List<int> list = new List<int>();
                //    //do cancel re-create (delete if has)
                //    EinvoiceDetailCollection edc = op.EinvoiceDetailCollectionGet(einv.Id);
                //    foreach (EinvoiceDetail ed in edc)
                //    {
                //        list.Add(ed.Id);
                //    }
                //    ////新增註銷發票檔(C0701)
                //    //EinvoiceFacade.GenerateVoidUploadFileById(list);
                //}
            }

            OnViewInitialized();
            View.AuditBoardView.Presenter.OnViewInitialized();
        }

        private void OnRecoverToRefundScash(object sender, DataEventArgs<int> e)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(e.Data);
            ReturnService.RecoverToRefundScash(returnForm, View.CurrentUser);

            if (returnForm.DeliveryType == DeliveryType.ToHouse)
            {
                View.SetHomeDeliverReturnInfo();
            }
            else
            {
                View.SetCouponReturnInfo();
            }
            RefreshAuditBoard();
            RefreshView();
        }

        //建立換貨單
        private void OnCreateExchangeLog(object sender, ExchangeLogArgs e)
        {
            string denyMsg;
            if (!OrderExchangeUtility.AllowExchange(e.OrderGuid, out denyMsg, true))
            {
                View.alertMessage = denyMsg;
                return;
            }

            #region create exchange log

            var modifyMessage = "換貨單建立";

            var now = DateTime.Now;
            var exchangeLog = new OrderReturnList
            {
                OrderGuid = e.OrderGuid,
                Type = (int)OrderReturnType.Exchange,
                Reason = e.Reason,
                Status = (int)OrderReturnStatus.SendToSeller,
                VendorProgressStatus = (int)ExchangeVendorProgressStatus.Processing,
                MessageUpdate = false,
                CreateTime = now,
                CreateId = View.CurrentUser,
                ModifyTime = now,
                ModifyId = View.CurrentUser,
                ReceiverName = e.ReceiverName,
                ReceiverAddress = e.ReceiverAddress
            };

            OrderFacade.SetOrderReturList(exchangeLog, modifyMessage);
            EmailFacade.SendProductRefundOrExchangeToSeller(OrderReturnType.Exchange, exchangeLog.Id);

            #endregion create exchange log
            
            #region clear FinalBalanceSheetDate

            if (PponOrderManager.CheckIsOverTrialPeriod(e.OrderGuid))
            {
                DealAccounting da = pp.DealAccountingGet(e.Bid);
                if (da.IsLoaded && da.FinalBalanceSheetDate.HasValue)
                {
                    da.FinalBalanceSheetDate = null;
                    pp.DealAccountingSet(da);

                }

                ComboDeal cd = pp.GetComboDeal(e.Bid);
                if (cd.IsLoaded)
                {
                    Guid mainGuid = Guid.Empty;
                    Guid.TryParse(cd.MainBusinessHourGuid.ToString(), out mainGuid);
                    DealAccounting mainDa = pp.DealAccountingGet(mainGuid);
                    if (mainDa.IsLoaded && mainDa.FinalBalanceSheetDate.HasValue)
                    {
                        mainDa.FinalBalanceSheetDate = null;
                        pp.DealAccountingSet(mainDa);
                    }
                }
            }

            #endregion

            View.ShowHomeDeliveryInfo();
            View.SetHomeDeliverExchangeInfo();
            RefreshView();
            RefreshAuditBoard();
        }

        //更新換貨收件資訊
        private void OnUpdateExchangeLog(object sender, UpdateExchangeLogArgs e)
        {
            OrderReturnList exchangeLog = op.OrderReturnListGet(e.ExchangeLogId);
            if (exchangeLog.IsLoaded)
            {
                string changelog = string.Empty;
                if (exchangeLog.ReceiverName != e.ReceiverName)
                {
                    changelog = string.Format("收件人: \"{0}\" → \"{1}\"", exchangeLog.ReceiverName ?? "null", e.ReceiverName);
                }
                if (exchangeLog.ReceiverAddress != e.ReceiverAddress)
                {
                    changelog += string.IsNullOrEmpty(changelog) ? ", " : "";
                    changelog += string.Format("地址: \"{0}\" → \"{1}\"", exchangeLog.ReceiverAddress ?? "null", e.ReceiverAddress);
                }

                exchangeLog.ReceiverName = e.ReceiverName;
                exchangeLog.ReceiverAddress = e.ReceiverAddress;
                exchangeLog.ModifyTime = DateTime.Now;
                exchangeLog.ModifyId = View.CurrentUser;
                op.OrderReturnListSet(exchangeLog);

                exchangeLog.Message = string.Format("換貨單({0})更新: {1}", exchangeLog.Id, changelog);
                PaymentFacade.SaveOrderStatusLog(exchangeLog);
                
                RefreshView();
                RefreshAuditBoard();
            }
        }

        private void OnCompleteExchangeLog(object sender, DataEventArgs<int> e)
        {
            var exchangeLog = op.OrderReturnListGet(e.Data);

            if (exchangeLog.VendorProgressStatus != (int)ExchangeVendorProgressStatus.ExchangeCompleted)
            {
                View.alertMessage = "廠商已標註換貨完成之換貨單方可進行確認完成作業";
                return;
            }

            var modifyMessage = "17Life確認換貨完成結案";
            exchangeLog.Status = (int)OrderReturnStatus.ExchangeSuccess;
            exchangeLog.MessageUpdate = false;
            exchangeLog.ModifyTime = DateTime.Now;
            exchangeLog.ModifyId = View.CurrentUser;

            if (!OrderFacade.SetOrderReturList(exchangeLog, modifyMessage))
            {
                View.alertMessage = "確認換貨完成作業失敗, 請洽技術部!";
            }

            OrderFacade.SendCustomerExchangeCompleteMail(e.Data);

            View.ShowHomeDeliveryInfo();
            View.SetHomeDeliverExchangeInfo();
            RefreshView();
            RefreshAuditBoard();
        }

        private void OnCancelOrder(object sender, DataEventArgs<Guid> e)
        {
            Guid OrderGuid = e.Data;
            ViewCouponListMain main = mp.GetCouponListMainByOid(OrderGuid);
            Order o = op.OrderGet(OrderGuid);

            if (Helper.IsFlagSet(main.OrderStatus, (int)OrderStatus.Cancel))
            {
                View.alertMessage = "此訂單已取消，不需再取消訂單";
            }
            else if (main.IsCanceling)
            {
                View.alertMessage = "此訂單取消中，不需再取消訂單";
            }
            else
            {
                if (string.IsNullOrEmpty(main.PreShipNo))
                {
                    //配編前直接取消
                    PaymentFacade.CancelPaymentTransactionByOrderForIncompleteOrder(o, "sys");
                    OrderFacade.SendOrderCancelMail(o.Guid, false, true);
                }
                else
                {
                    //配編後為取消中
                    o.IsCanceling = true;
                    op.OrderSet(o);
                    OrderFacade.SendOrderCancelMail(o.Guid, true, false);
                }
                CommonFacade.AddAudit(o.Guid, AuditType.Order, "取消訂單申請(WEB)", View.CurrentUser, false);
                RefreshView();
                View.alertMessage = "取消訂單已送出申請";
            }
        }

        private void OnWithdrawCancelOrder(object sender, DataEventArgs<Guid> e)
        {
            Guid OrderGuid = e.Data;

            ViewCouponListMain main = mp.GetCouponListMainByOid(OrderGuid);
            Order o = op.OrderGet(OrderGuid);

            if (Helper.IsFlagSet(main.OrderStatus, (int)OrderStatus.Cancel))
            {
                View.alertMessage = "此訂單已取消，無法進行抽回";
            }
            else if (!main.IsCanceling)
            {
                View.alertMessage = "此訂單未申請取消訂單，無法進行抽回";
            }
            else if (main.IsCanceling)
            {
                o.IsCanceling = false;
                op.OrderSet(o);
                OrderFacade.SendOrderCancelMail(o.Guid, false, false);
                CommonFacade.AddAudit(o.Guid, AuditType.Order, "抽回取消訂單", View.CurrentUser, false);
                RefreshView();
                View.alertMessage = "已抽回申請";

            }            
        }
        

        private void OnDownloadCouponZip(object sender, DataEventArgs<int> e)
        {
            CommonFacade.AddAudit(View.OrderId, AuditType.Order, "下載憑證Zip。", View.CurrentUser, false);

            ViewPponCouponCollection vpcc = pp.ViewPponCouponGetListByOrderGuid(View.OrderId);
            Dictionary<string, string> fileByte = new Dictionary<string, string>();

            foreach (var coupon in vpcc)
            {
                var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(coupon.BusinessHourGuid);

                CouponDocumentBase doc;
                if (Helper.IsFlagSet((GroupOrderStatus)coupon.GroupOrderStatus.Value, GroupOrderStatus.FamiDeal))
                {
                    doc = new FamiCouponDocument(coupon, CouponCodeType.FamiItemCode, vpd);
                }
                else
                {
                    doc = new PponCouponDocument(coupon, vpd);
                }
               
                fileByte.Add(coupon.SequenceNumber + ".pdf", Convert.ToBase64String(doc.GetBuffer()));
            }

            byte[] zipFile = LunchKingSite.WebLib.Component.FileZipUtility.ZipFile(fileByte, ZipFileType.Byte);

            View.DownloadZip(zipFile);
            this.OnViewInitialized();
        }

        //取消換貨
        private void OnCancelExchangeLog(object sender, DataEventArgs<int> e)
        {
            var exchangeLog = op.OrderReturnListGet(e.Data);

            if (exchangeLog.Status == (int)OrderReturnStatus.ExchangeSuccess)
            {
                View.alertMessage = "已確認完成之換貨單不允許進行換貨取消作業";
                return;
            }

            var modifyMessage = "17Life確認換貨取消";
            exchangeLog.Status = (int)OrderReturnStatus.ExchangeCancel;
            exchangeLog.MessageUpdate = false;
            exchangeLog.ModifyTime = DateTime.Now;
            exchangeLog.ModifyId = View.CurrentUser;

            if (!OrderFacade.SetOrderReturList(exchangeLog, modifyMessage))
            {
                View.alertMessage = "確認換貨取消作業失敗, 請洽技術部!";
            }

            View.ShowHomeDeliveryInfo();
            View.SetHomeDeliverExchangeInfo();
            RefreshView();
            RefreshAuditBoard();
        }
        
        #endregion

        private int GetCouponId(string s)
        {
            List<string> l = new List<string>(s.Split(','));
            return int.Parse(l[0]);
        }

        private bool GetCouponAvailable(string s)
        {
            List<string> l = new List<string>(s.Split(','));
            if (l[1] == "NULL")
            {
                return false;
            }
            else
            {
                return bool.Parse(l[1]);
            }
        }

        protected void GetCouponRelativeInfo(object sender, DataEventArgs<Dictionary<int, string>> e)
        {
            View.SmsCount = pp.SMSLogGetCountByPpon(View.MemberEmail, e.Data.Keys.FirstOrDefault().ToString());
            CashTrustLog ct = mp.CashTrustLogGetByCouponId(e.Data.Keys.FirstOrDefault());
            string einvNum = e.Data.Values.FirstOrDefault();
            bool isGroupCouponDeal = Helper.IsFlagSet(pp.BusinessHourGet(View.BusinessHourGuid).BusinessHourStatus, BusinessHourStatus.GroupCoupon);

            #region RecoveredInfo

            if (Helper.IsFlagSet(ct.SpecialStatus, TrustSpecialStatus.VerificationForced))
            {
                if (Helper.IsFlagSet(ct.SpecialStatus, TrustSpecialStatus.VerificationUnrecovered))
                {
                    View.RecoveredInfo = "款項未回收";
                }
                else
                {
                    View.RecoveredInfo = "款項已回收";
                }
            }
            else
            {
                View.RecoveredInfo = "非強制核銷";
            }

            #endregion RecoveredInfo

            View.Invoice = (ct.CreateTime >= cp.NewInvoiceDate && ct.CouponId > 0 && !isGroupCouponDeal)
                ? ((einvNum != null) ? op.EinvoiceMainGet(einvNum) : op.EinvoiceMainGetListByOrderGuid(ct.OrderGuid).Where(x => x.CouponId == ct.CouponId).LastOrDefault())
                : op.EinvoiceMainGetListByOrderGuid(ct.OrderGuid).LastOrDefault();
            //: op.EinvoiceMainGet(ct.OrderGuid);
            if (View.Invoice != null)
            {
                View.Invoice.InvoiceNumber = null;
                var emc = isGroupCouponDeal ? op.EinvoiceMainGetListByOrderGuid(ct.OrderGuid) : op.EinvoiceMainGetListByOrderGuid(ct.OrderGuid).Where(x => x.CouponId == ct.CouponId);
                foreach (EinvoiceMain em in emc)
                {
                    string invoiceType = string.Empty;
                    switch (em.InvoiceStatus)
                    {
                        case (int)EinvoiceType.Initial:
                            if (!string.IsNullOrEmpty(em.InvoiceNumber))
                                invoiceType = "(重開)";
                            break;
                        case (int)EinvoiceType.C0501:
                            invoiceType = "(作廢)";
                            break;
                        case (int)EinvoiceType.D0401:
                            invoiceType = "(折讓)";
                            break;
                        case (int)EinvoiceType.C0701:
                            if (!string.IsNullOrEmpty(em.InvoiceNumber))
                                invoiceType = "(註銷)";
                            break;
                        default:
                            break;
                    }
                    View.Invoice.InvoiceNumber += em.InvoiceNumber + (!string.IsNullOrEmpty(invoiceType) ? invoiceType + "<br/>" : string.Empty);
                    if (em.InvoiceNumber != null)
                    {
                        View.Invoice.InvoiceStatus = em.InvoiceStatus;
                        View.Invoice.InvoiceRequestTime = em.InvoiceRequestTime;
                    }
                }
            }

            if (ct.CouponId > 0)
            {
                View.Receipt = op.EntrustSellReceiptGetByCouponId(ct.CouponId.Value);
            }

            var dp = pp.DealPropertyGetByOrderGuid(ct.OrderGuid);
            if (dp != null)
            {
                View.IsEntrustSell = (EntrustSellType)dp.EntrustSell != EntrustSellType.No;
            }
        }


        private DateTime GetFinalExpireDate(string p)
        {
            Guid oid;
            if (Guid.TryParse(p, out oid))
            {
                ExpirationDateSelector selector = GetExpirationComponent(oid);

                if (selector != null)
                {
                    return selector.GetExpirationDate();
                }
            }
            return new DateTime(1900, 1, 1);
        }

        private Dictionary<Guid, ExpirationDateSelector> expirationComponents = new Dictionary<Guid, ExpirationDateSelector>();

        public ExpirationDateSelector GetExpirationComponent(Guid orderGuid)
        {
            if (expirationComponents.ContainsKey(orderGuid))
            {
                return expirationComponents[orderGuid];
            }

            ExpirationDateSelector selector = new ExpirationDateSelector();
            selector.ExpirationDates = new PponUsageExpiration(orderGuid);

            expirationComponents.Add(orderGuid, selector);
            return selector;
        }

        public EinvoiceMainCollection GetEinvoice(Guid orderGuid)
        {
            return op.EinvoiceMainGetListByOrderGuid(orderGuid);
        }

        #region ATM Refund Account
        private void OnSetAtmRefund(object sender, DataEventArgs<CtAtmRefund> e)
        {
            PaymentTransactionCollection pt = op.PaymentTransactionGetList(View.OrderId);
            Order o = op.OrderGet(View.OrderId);

            ThirdPartyPayment tpp = ThirdPartyPayment.None;
            var ctlc = mp.CashTrustLogGetListByOrderGuid(o.Guid, OrderClassification.LkSite);
            if (ctlc.Any())
            {
                tpp = (ThirdPartyPayment)ctlc.First().ThirdPartyPayment;
            }

            PaymentTransaction pt_atm = (pt.Count() > 0) ? op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                PaymentTransaction.Columns.TransId + " = " + pt.FirstOrDefault().TransId,
                PaymentTransaction.Columns.TransType + " = " + (int)PayTransType.Authorization).FirstOrDefault() : null;

            if (pt_atm != null
                && (Helper.IsFlagSet(pt_atm.Status, (long)PayTransPhase.Successful)
                || tpp == ThirdPartyPayment.TaishinPay
                || (tpp == ThirdPartyPayment.LinePay && o.CreateTime < DateTime.Now.AddDays(-cp.LinePayRefundPeriodLimit))
                || o.CreateTime < DateTime.Now.AddDays(-cp.NoneCreditRefundPeriod)))
            {
                CtAtmRefund ctatmrefund = new CtAtmRefund();

                CtAtmRefund atmR = op.CtAtmRefundGetLatest(View.OrderId);
                if (atmR != null && atmR.Status < (int)AtmRefundStatus.AchProcess)
                {
                    ctatmrefund = atmR;
                }
                else
                {
                    ctatmrefund = e.Data;
                    ctatmrefund.Status = (int)AtmRefundStatus.Initial;
                    ctatmrefund.Amount = 0;
                }

                ctatmrefund.AccountName = e.Data.AccountName;
                ctatmrefund.Id = e.Data.Id;
                ctatmrefund.BankName = e.Data.BankName;
                ctatmrefund.BranchName = e.Data.BranchName;
                ctatmrefund.BankCode = e.Data.BankCode;
                ctatmrefund.AccountNumber = e.Data.AccountNumber;
                ctatmrefund.Phone = e.Data.Phone;
                ctatmrefund.CreateTime = DateTime.Now;
                ctatmrefund.OrderGuid = View.OrderId;
                int userId = op.OrderGet(View.OrderId).UserId;
                ctatmrefund.UserId = userId;
                ctatmrefund.PaymentId = pt_atm.TransId;
                op.CtAtmRefundSet(ctatmrefund);
                op.CtAtmUpdateStatus((int)AtmStatus.RefundRequest, View.OrderId);

                CommonFacade.AddAudit(View.OrderId, AuditType.Order, "新增修改匯款帳戶：戶名:" + ctatmrefund.AccountName + "，ID:" + ctatmrefund.Id + "，"
                    + ctatmrefund.BankName + "(" + ctatmrefund.BranchName + ")，帳戶:" + ctatmrefund.AccountNumber + ")，手機:" + ctatmrefund.Phone, View.CurrentUser, true);
            }
            else
            {
                CommonFacade.AddAudit(View.OrderId, AuditType.Order, "查無ATM付款紀錄，無法申請ATM退款", View.CurrentUser, true);
            }

            View.AuditBoardView.Presenter.OnViewInitialized();
            this.OnViewInitialized();
        }

        #endregion ATM Refund Account

        #region 客服紀錄

        protected void OnServiceMsgSet(object sender, DataEventArgs<ServiceMessage> e)
        {
            int userId = op.OrderGet(View.OrderId).UserId;
            mp.ServiceMessageSet(e.Data, userId);

            #region 新增ServiceLog

            ServiceLog _serviceLog = new ServiceLog();
            _serviceLog.SendTo = e.Data.Email;           //對應到ServiceMessage的Email
            _serviceLog.RefX = e.Data.Id;     //FK, 對應到ServiceMessage的ID
            _serviceLog.RemarkSeller = e.Data.RemarkSeller;          //回報廠商備註
            _serviceLog.CreateId = e.Data.CreateId;
            _serviceLog.CreateTime = e.Data.CreateTime;
            mp.ServiceLogSet(_serviceLog);

            #endregion 新增ServiceLog
        }

        #endregion 新增客服紀錄


        //config.EnabledExchangeProcess:false
        protected void SetFilterTypes()
        {
            View.SetFilterTypeDropDown(View.FilterTypes);
        }

        protected void SetEinoviceLog(Guid orderGuid)
        {
            View.EinvoiceChangeLogMainData = EinvoiceFacade.GetEinvoiceChangeLogMainData(orderGuid);
        }

        protected void OnCashTrustlogFilled(object sender, EventArgs e)
        {
            ReturnService.PatchCashTrustLog(View.OrderId, View.CurrentUser);

            View.AuditBoardView.Presenter.OnViewInitialized();
            OnViewInitialized();
        }

        private CashTrustLogCollection ctlogs = null;
        private CashTrustLogCollection returnFormCtlogs = null;
        public string GetCouponStatus(int couponId)
        {
            if (ctlogs == null)
            {
                ctlogs = mp.CashTrustLogGetListByOrderGuid(View.OrderId, OrderClassification.LkSite);
            }
            if (returnFormCtlogs == null)
            {
                returnFormCtlogs = mp.PponRefundingCashTrustLogGetListByOrderGuid(View.OrderId);
            }

            CashTrustLog ctlog = ctlogs.FirstOrDefault(x => x.CouponId == couponId);
            if (ctlog != null)
            {
                if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.AtmRefunding))
                {
                    return "ATM退款中，無法退貨";
                }

                switch ((TrustStatus)ctlog.Status)
                {
                    case TrustStatus.Initial:
                    case TrustStatus.Trusted:
                        CashTrustLog returningCtlog = returnFormCtlogs.FirstOrDefault(x => x.CouponId == couponId);
                        if (returningCtlog != null)
                        {
                            return "已在退貨單上，無法退貨";
                        }
                        return "未核銷，可進行退貨";
                    case TrustStatus.Verified:
                        DateTime modifyDT = ctlog.ModifyTime;
                        string modifyTime = string.Format("{0:yyyy/MM/dd HH:mm} ", modifyDT);

                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.VerificationForced))
                        {
                            return modifyTime + "已強制核銷";
                        }

                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.VerificationLost))
                        {
                            return modifyTime + "已核銷，清冊遺失";
                        }

                        return modifyTime + "已核銷";
                    case TrustStatus.Returned:
                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.ReturnForced))
                        {
                            return "已強制退貨";
                        }

                        return "已退購物金，無法再退貨";
                    case TrustStatus.Refunded:
                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.ReturnForced))
                        {
                            return "已強制退貨";
                        }

                        return "已轉退現金，無法再退貨";
                    case TrustStatus.ATM:
                        return "未付款，無法退貨";
                    default:
                        return "查無即時核銷資訊";
                }
            }

            return "查無即時核銷資訊";
        }

        public bool IsCouponReturnable(int couponId)
        {
            if (ctlogs == null)
            {
                ctlogs = mp.CashTrustLogGetListByOrderGuid(View.OrderId, OrderClassification.LkSite);
            }
            if (returnFormCtlogs == null)
            {
                returnFormCtlogs = mp.PponRefundingCashTrustLogGetListByOrderGuid(View.OrderId);
            }

            CashTrustLog ctlog = ctlogs.FirstOrDefault(x => x.CouponId == couponId);
            if (ctlog != null)
            {
                if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.AtmRefunding))
                {
                    return false;
                }

                switch ((TrustStatus)ctlog.Status)
                {
                    case TrustStatus.Initial:
                    case TrustStatus.Trusted:
                        CashTrustLog returningCtlog = returnFormCtlogs.FirstOrDefault(x => x.CouponId == couponId);
                        if (returningCtlog != null)
                        {
                            return false;
                        }
                        return true;
                    default:
                        return false;
                }
            }

            return false;
        }


        private void RefreshAuditBoard()
        {
            View.AuditBoardView.Presenter.OnViewInitialized();
        }

        private void RefreshView()
        {
            this.SetupView(op.ViewOrderMemberBuildingSellerGet(View.OrderId));
        }


        private bool CheckIsShipable()
        {
            //檢查該訂單是否有可出貨商品
            return PponOrderManager.CheckIsShipable(View.OrderId);
        }

        private bool CheckEverExchange()
        {
            //檢查該訂單是否有換貨處理中之換貨單
            return PponOrderManager.CheckEverExchange(View.OrderId);
        }

        private IEnumerable<Guid> GetPaidTrustIds(List<Guid> trustIds)
        {
            //取得已列入對帳單明細(已請款)之訂單份數
            return ap.BalanceSheetDetailGetListByTrustId(trustIds)
                        .Where(x => x.Status == (int)BalanceSheetDetailStatus.Normal
                                 || x.Status == (int)BalanceSheetDetailStatus.NoPay)
                        .Select(x => x.TrustId);
        }

        private CashTrustStatusLog CashTrustStatusLogSet(CashTrustLog ctlog)
        {
            return new CashTrustStatusLog
            {
                TrustId = ctlog.TrustId,
                TrustProvider = ctlog.TrustProvider,
                Amount = ctlog.Amount,
                Status = ctlog.Status,
                ModifyTime = DateTime.Now,
                CreateId = "sys",
                SpecialStatus = ctlog.SpecialStatus,
                SpecialOperatedTime = ctlog.SpecialOperatedTime
            };
        }
    }
}
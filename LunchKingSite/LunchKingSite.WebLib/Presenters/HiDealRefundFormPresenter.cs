﻿using LunchKingSite.BizLogic.CustomException;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealRefundFormPresenter : Presenter<IHiDealRefundFormView>
    {
        #region properties

        protected static IHiDealProvider hp;
        protected static IOrderProvider op;
        protected static IMemberProvider mp;

        #endregion properties

        #region overload base methods

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            ShowRefundForm();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            return true;
        }

        public HiDealRefundFormPresenter(IHiDealProvider hiDeal, IOrderProvider orderProv, IMemberProvider memProv)
        {
            hp = hiDeal;
            op = orderProv;
            mp = memProv;
        }

        #endregion overload base methods

        private void ShowRefundForm()
        {
            try
            {
                //檢查參數
                if ((View.ReturnedId == null) || (View.OrderPk == null))
                {
                    throw new DataCheckException<HiDealRefundFormViewError>("查無此退貨紀錄。", HiDealRefundFormViewError.Default);
                }
                int returnedId = View.ReturnedId.Value;
                int orderPk = View.OrderPk.Value;
                var order = hp.HiDealOrderGet(orderPk);
                if (!order.IsLoaded)
                {
                    throw new DataCheckException<HiDealRefundFormViewError>("查無此退貨紀錄。", HiDealRefundFormViewError.Default);
                }

                EinvoiceMainCollection einvoices = op.EinvoiceMainCollectionGetByOrderId(order.OrderId, OrderClassification.HiDeal);

                CashTrustLogCollection cs = mp.CashTrustLogGetListByOrderGuid(order.Guid, OrderClassification.HiDeal);
                Member m = mp.MemberGet(View.UserName);
                HiDealReturned returned = hp.HiDealReturnedGet(returnedId);

                if (!returned.IsLoaded)
                {
                    throw new DataCheckException<HiDealRefundFormViewError>("查無此退貨紀錄。", HiDealRefundFormViewError.Default);
                }

                //判斷商品檔或憑證檔

                HiDealReturnedDetailCollection returnedDetails = hp.HiDealReturnedDetailGetListByReturnedId(returnedId);

                HiDealDeliveryType hiDealDeliveryType = (returnedDetails.Where(x => x.ProductType == (int)HiDealProductType.Product).Single().DeliveryType == (int)HiDealDeliveryType.ToHouse) ? HiDealDeliveryType.ToHouse : HiDealDeliveryType.ToShop;

                ViewHiDealReturnedCollection viewHiDealReturnedCollection = hp.ViewHiDealReturnedGetList(orderPk);
                if (!viewHiDealReturnedCollection.Any())
                {
                    throw new DataCheckException<HiDealRefundFormViewError>("查無此退貨紀錄。", HiDealRefundFormViewError.Default);
                }

                if (returned.Status == (int)HiDealReturnedStatus.Completed)
                {
                    throw new DataCheckException<HiDealRefundFormViewError>("已完成退貨作業。", HiDealRefundFormViewError.Default);
                }

                if (returned.Status == (int)HiDealReturnedStatus.Cancel)
                {
                    throw new DataCheckException<HiDealRefundFormViewError>("已取消退貨作業。", HiDealRefundFormViewError.Default);
                }

                //if (einvoices.Count == 0)//沒發票
                //{
                //    throw new DataCheckException<HiDealRefundFormViewError>("此訂單不需填寫【退貨申請書】!", HiDealRefundFormViewError.Default);
                //}

                if (einvoices.Count == 0)
                {
                    if (View.UserId == 0 || order.UserId != View.UserId)
                {
                        throw new DataCheckException<HiDealRefundFormViewError>("會員資料有誤，請洽客服中心!", HiDealRefundFormViewError.Default);
                }
                }
                else
                {
                if (View.UserId == 0 || einvoices.First().UserId != View.UserId)
                {
                    throw new DataCheckException<HiDealRefundFormViewError>("會員資料有誤，請洽客服中心!", HiDealRefundFormViewError.Default);
                }
                }


                View.EncryptedCouponCode = order.OrderId;
                View.SetRefundFormInfo(einvoices, cs, viewHiDealReturnedCollection, hiDealDeliveryType, View.RefundFormType);
            }
            catch (DataCheckException<HiDealRefundFormViewError> ex)
            {
                View.ShowMessageAndRedirectToDefault(ex.Message);
                return;
            }
        }
    }
}
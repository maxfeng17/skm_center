﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using Autofac;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    /// <summary>
    /// 本段程式作業的預設狀況，目前DB中沒有View.ExternalUserId的MemberLink紀錄
    /// </summary>
    public class RegisterPresenter : Presenter<IRegisterView>
    {
        protected IMemberProvider mp = ProviderFactory.Instance().GetDefaultProvider<IMemberProvider>();
        protected static ISysConfProvider config = ProviderFactory.Instance().GetDefaultProvider<ISysConfProvider>();
        protected static ILog log = LogManager.GetLogger(typeof(RegisterPresenter));

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            if (View.SsoMemberInfo == null || View.SsoMemberInfo.Count == 0)
            {
                // user not signed-in yet. in order to register with the site, 
                // user needs to be authenticated via at least one of connecting sources
                View.RedirectToLogin();
                return false;
            }

            if (View.SsoMemberInfo.Count == 1)
            {
                var linkingMemInfo = View.SsoMemberInfo[0];
                // see if user has already registered before
                MemberLink lnk = mp.MemberLinkGetByExternalId(linkingMemInfo.Value, linkingMemInfo.Key);
                if (lnk == null || !lnk.IsLoaded)
                {
                    // we can't find the link, let's present the form for user to fill in email
                    string email = string.Empty;
                    if (linkingMemInfo.Key == SingleSignOnSource.Facebook)
                    {
                        var fbUser = FacebookUtility.GetFacebookLoggedInUser();
                        if (fbUser != null && !string.IsNullOrEmpty(fbUser.Email) && fbUser.Email.IndexOf("proxymail.facebook.com") < 0)
                            email = fbUser.Email;
                    }
                    View.ShowInputEmailPage(email);
                }
                else
                    // we found the link, let see if user can link to other account
                    ProceedToLinking(linkingMemInfo);
            }
            else
            {   // user has more than 1 authentications, which happens while linking
                // SsoMemberInfo is ordered by the order of whom the user does single signon
                // therefore the  one with largest index is the most recent one
                VerifyLinkingInfo(View.SsoMemberInfo);
            }

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Submit += this.OnSubmit;
            View.MergeTwoMail += this.OnMergeTwoMail;
            return true;
        }

        protected void OnSubmit(object sender, EventArgs e)
        {
            string userName = View.InputEmail.Trim();
            if (string.IsNullOrEmpty(userName) || (View.SsoMemberInfo == null || View.SsoMemberInfo.Count == 0))
            {
                OnViewInitialized();
                return;
            }

            var linkingAccount = View.SsoMemberInfo[0];
            //檢查目前member是否有相同的email紀錄
            MembershipUserCollection memberCollection = Membership.FindUsersByEmail(Helper.GetIgnoreSqlLikeUnderineMatch(View.InputEmail));
            //如果目前沒有該email資料，新增該member
            if (memberCollection.Count == 0)
            {
                RegisterMember(userName);

                // 訂閱送17Life購物金$50活動
                // add by Max 2011.3.29
                CheckPromotionDeposit(userName);
            }
            Member mem = mp.MemberGet(userName);
            //檢查於member_link是否已有該Mail的資料
            MemberLinkCollection linkCollection = mp.MemberLinkGetList(MemberLink.Columns.UserId, mem.UniqueId);
            if (linkCollection.Count == 0)
            // in fact we should move this section of code into RegisterMember, however it's easier for development testing to put code here
            // in future consider moving this section of code into RegisterMember
            {
                //建立新的串接資料
                MemberLink link = new MemberLink
                {
                    ExternalOrg = (int)linkingAccount.Key,
                    ExternalUserId = linkingAccount.Value,
                    UserId = View.UserId
                };
                mp.MemberLinkSet(link);

                FormsAuthentication.SetAuthCookie(View.InputEmail.Trim(), false);
                //View.ShowAccountLinkingPage(linkingAccount.Key, Enum.GetValues(typeof(SingleSignOnSource)).Cast<SingleSignOnSource>().Where(x => x != linkingAccount.Key).ToList()); // it's the first link, therefore existing links param is null
                // default to not linking
                View.SendUserBackToPreviousPage();
                //完成帳號設定的通知信 由於改變登入流程後沒有會員名稱等資料，先不寄發要討論一下 2010/7/19 mark by tunin
                MemberFacade.SendRegisterMail(string.Empty, View.InputEmail, string.Empty, mp.MemberShipGetGuid(View.InputEmail), true);
            }
            //已有相同來源的串接資料, not supposed to happen
            else if (linkCollection.Any((t) => t.ExternalOrg == (int)linkingAccount.Key))
            {
                if (linkCollection.Any((t) => t.ExternalOrg == (int)linkingAccount.Key && t.ExternalUserId == linkingAccount.Value))
                {
                    //帳號已存在，且為同一外部ID，視為同一帳號 (發生原因可能為串接後出錯，客戶自行返回上一頁，再次輸入相同email) add by Max 2011/03/14
                    View.SendUserBackToPreviousPage();
                }
                else
                {
                    //帳號已存在，請重新輸入其他EMAIL帳號
                    View.ShowMessage(I18N.Phrase.DuplicateEmail);
                    return;
                }
            }
            //有不同來源的串接資料，若有需詢問是否要結合不同帳號
            else
            {
                //已有其他相同MAIL的帳號, 需要確認是否為同一人
                View.ShowVerifyWithExistingAccountPage(linkingAccount.Key, linkCollection.Select(x => (SingleSignOnSource)x.ExternalOrg).ToList());
                return;
            }
        }

        protected void OnMergeTwoMail(object sender, EventArgs e)
        {
            //取得要保留的Mail
            string saveMail = View.SelectedEmail;
            //要刪除的mail
            string delMail = View.NotSelectedEmail;
            //將要刪除的mail相關的訂單、紅利等紀錄移轉到要保留的mail中
            MemberUtility.MergeMember(saveMail, delMail);
            //將目前登入使用者設定為合併後的使用者
            FormsAuthentication.SetAuthCookie(saveMail, false);
            //顯示成功畫面
            LoginSuccess();
        }
        /// <summary>
        /// 進行資料串接的動作，傳入要串接的狀態
        /// 會呼叫此function 的狀況有兩種
        /// 狀態1.當新註冊帳號，登記的mail與目前不同類型的memberlink相同，要將兩帳號合併時，使用者登入選取的帳後開始串接動作
        /// 狀態2.當新註冊帳號，沒有任何相同mail的memberlink資料，使用者可以選擇他擁有的其他來源帳號，將串接的MAIL設為同一個
        /// </summary>
        /// <param name="linkSource"></param>
        /// <param name="linkingExternalId"></param>
        protected void VerifyLinkingInfo(ExternalMemberInfo ssoInfo)
        {
            if (string.IsNullOrEmpty(View.UserName))
            {
                // it will occur under the following circumstance:
                // 1. user logged in from SSO source 1 for the first time
                // 2. user inputed an email that already linked to SSO source 2, so we need to verify if the email really belongs to this user
                // 3. we ask user to login via SSO source 2, but SSO source 2 returned a different UID then the one we have in database
                // therefore the verification fails
                View.SsoMemberInfo.Remove(View.SsoMemberInfo.Count - 1);
                View.ShowLoginError(Message.LoginErrorLinkLoginError);
                return;
            }

            // query 
            MemberLinkCollection links = mp.MemberLinkGetList(MemberLink.Columns.UserId, View.UserId);

            if (View.UserName != View.InputEmail)
            {
                #region 使用者輸入要串接之 mail 與登入帳號不符
                string inputMailUser = Membership.GetUserNameByEmail(View.InputEmail);
                if (string.IsNullOrEmpty(inputMailUser))
                {   // the user associated with InputEmail doesn't exist,
                    // let's link the sso source with existing member
                    for (int i = 0; i < ssoInfo.Count; i++)
                        if (!links.Any(x => (SingleSignOnSource)x.ExternalOrg == ssoInfo[i].Key))
                            SaveLink(View.UserName, ssoInfo[i].Key, ssoInfo[i].Value);
                }
                //兩個帳號都存在
                //為避免新帳號與舊帳號衝突，先檢查帳後內容
                else if (mp.MemberLinkGetListToCheckSource(View.UserName, View.InputEmail).Count > 0)
                {
                    View.ShowMessage(I18N.Message.LoginMergeSourceConflict);
                    ProceedToLinking(ssoInfo[ssoInfo.Count - 1]);   // we'll be showing the linking page for the newest linked one
                }
                else
                {
                    //詢問是否合併
                    View.ShowMailSelect(View.UserName, View.InputEmail);
                }
                #endregion
            }
            else
            {
                #region 已存在既有帳號EMAIL相同進行link
                for (int i = 0; i < ssoInfo.Count; i++)
                    if (!links.Any(x => (SingleSignOnSource)x.ExternalOrg == ssoInfo[i].Key))
                        SaveLink(View.UserName, ssoInfo[i].Key, ssoInfo[i].Value);
                #endregion 已存在既有帳號EMAIL相同進行link
            }
        }

        protected void LoginSuccess()
        {
            View.SendUserBackToPreviousPage();
        }

        protected void LoginError(string message)
        {
            View.ShowLoginError(message);
        }

        private void RegisterMember(string userName)
        {
            Member mem = null;
            string passWord = Membership.GeneratePassword(8, 0);
            string memId = mp.MemberGetNewId(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["aspnet20membership"].ConnectionString);
            try
            {
                MembershipCreateStatus createStatus;
                MembershipUser mu = Membership.CreateUser(memId, passWord, userName, null, null, true, out createStatus);
                if (createStatus != MembershipCreateStatus.Success)
                {
                    string errMsg = "";
                    errMsg = Phrase.Error + ": ";
                    switch (createStatus)
                    {
                        case MembershipCreateStatus.DuplicateEmail:
                            errMsg += Phrase.DuplicateEmail;
                            break;
                        case MembershipCreateStatus.DuplicateUserName:
                            errMsg += Phrase.DuplicateUserName;
                            break;
                        case MembershipCreateStatus.InvalidEmail:
                            errMsg += Phrase.InvalidEmail;
                            break;
                        case MembershipCreateStatus.InvalidPassword:
                            errMsg += Phrase.InvalidPassword;
                            break;
                        case MembershipCreateStatus.UserRejected:
                            errMsg += Phrase.UserRejected;
                            break;
                        default:
                            errMsg += createStatus.ToString("g");
                            break;
                    }
                    View.ShowMessage(errMsg);
                    return;
                }

                if (Roles.RoleExists(MemberRoles.RegisteredUser.ToString("g")))
                    Roles.AddUserToRoles(mu.Email, new string[] { MemberRoles.RegisteredUser.ToString("g") });

                mem = new Member
                {
                    UserName = userName,
                    PrimaryContactMethod = (int)ContactMethod.Mobile,
                    EdmType = (int)MemberEdmType.PponEvent | (int)MemberEdmType.PponStartedSell | (int)MemberEdmType.PiinlifeEvent | (int)MemberEdmType.PiinlifeStartedSell,
                    CreateTime = DateTime.Now,
                    BuildingGuid = config.DefaultBuildingGuid
                };

                // Here we save optional user's personal data stealthily if the user is sso from Facebook
                if (View.SsoMemberInfo != null && !string.IsNullOrEmpty(View.SsoMemberInfo[SingleSignOnSource.Facebook]))
                {
                    var fbUser = FacebookUtility.GetFacebookLoggedInUser();
                    if (fbUser != null)
                    {
                        DateTime birthday;
                        DateTime.TryParse(fbUser.Birthday, out birthday);
                        if (birthday > new DateTime(1753, 1, 1) && birthday < new DateTime(9999, 12, 31))
                            mem.Birthday = birthday;
                        mem.Pic = FacebookUtility.GetLoggedInUserSquarePicWithLogo();
                    }
                }

                mp.MemberSet(mem);
                if (!string.IsNullOrWhiteSpace(View.ReferrerSourceId))
                    CpaUtility.RecordRegistrationByReferrer(View.ReferrerSourceId, mem, BusinessModel.Ppon);

                MemberLink ml = mp.MemberLinkGet(mem.UniqueId);

                if (!string.IsNullOrWhiteSpace(View.ReferreriChannelId))
                    View.StartiChannelReg(ml, config.IchannelID, "522_2");
            }
            catch (Exception ex)
            {
                log.Error("Exception during member creation! member = " +
                          Helper.Object2String(mem, System.Environment.NewLine), ex);
                MembershipUser mu = Membership.GetUser(memId);
                mem = mp.MemberGet(userName);
                // see if we need to revert the transaction
                // both Member and Membership needs to exist or not exist, if only either exists, we'll revert the transaction
                // therefore we use xor here
                if ((mem == null || !mem.IsLoaded) ^ (mu == null))
                {
                    log4net.LogManager.GetLogger("Membership").Warn(".DeleteUser_位置16:" + memId);
                    Membership.DeleteUser(memId, true);
                    mp.MemberDelete(userName);
                }
                throw;
            }
        }

        private void CheckPromotionDeposit(string userName)
        {
            IPponProvider pp = ProviderFactory.Instance().GetDefaultProvider<IPponProvider>();
            SubscriptionCollection sub = pp.SubscriptionGetList(userName);

            if (sub.Where(x => x.Status.Equals((int)SubscribeType.Subscribe50)).Count() > 0)
            {
                int userId = MemberFacade.GetUniqueId(userName);
                if (mp.MemberPromotionDepositGetList(userId).Where<MemberPromotionDeposit>(x => x.Action.Equals("訂閱送$50紅利")).Count().Equals(0))
                {
                    // add promotion deposit
                    MemberPromotionDeposit mpd = new MemberPromotionDeposit();
                    mpd.StartTime = DateTime.Now;
                    mpd.ExpireTime = DateTime.Now.AddMonths(1);
                    mpd.PromotionValue = 500;
                    mpd.CreateTime = DateTime.Now;
                    mpd.UserId = MemberFacade.GetUniqueId(userName);
                    mpd.Action = "訂閱送$50紅利";
                    mpd.CreateId = "System";
                    mp.MemberPromotionDepositSet(mpd);
                }
            }
        }

        private void ProceedToLinking(KeyValuePair<SingleSignOnSource, string> linkingMemInfo)
        {
            List<SingleSignOnSource> sourceToShow = null;
            MemberLinkCollection links = mp.MemberLinkGetList(MemberLink.Columns.UserId, View.UserId);
            if (links.Count == Enum.GetValues(typeof(SingleSignOnSource)).Length)   // the user is totally linked
            {
                // since the user is totally linked, let's set the ignoring merge flag so the user doesn't need to visit here
                //Member m = mp.MemberGet(View.UserName);
                //m.Status = (int)Helper.SetFlag(true, m.Status, MemberStatusFlag.DoNotAskMerge);
                //mp.MemberSet(m);
                LoginSuccess();
                return;
            }

            if (links.Count > 0)
            {
                // if the user has signed in, we need to save the current user name
                // therefore when the user comes back to this page after linking sso,
                // we can compare the user name with the linking account
                if (!string.IsNullOrEmpty(View.UserName))
                    View.InputEmail = View.UserName;

                sourceToShow = new List<SingleSignOnSource>();
                sourceToShow.AddRange(
                    Enum.GetValues(typeof(SingleSignOnSource)).Cast<SingleSignOnSource>().Where(
                        x => !links.Any(a => (SingleSignOnSource)a.ExternalOrg == x)).ToList());
            }
            View.ShowAccountLinkingPage(linkingMemInfo.Key, sourceToShow);
        }

        private void SaveLink(string userName, SingleSignOnSource linkSource, string lingExternalId)
        {
            MemberLink l = mp.MemberLinkGet(MemberFacade.GetUniqueId(View.UserName), linkSource);
            if (l != null && l.IsLoaded)
                LoginError(Message.LoginMergeSourceConflict);
            else
            {
                //檢查既有串接資料的狀態，依據使用者登入類型進行檢查
                l = new MemberLink()
                {
                    ExternalOrg = (int) linkSource,
                    ExternalUserId = lingExternalId,
                    UserId = View.UserId
                };
                mp.MemberLinkSet(l);
                LoginSuccess();
            }
        }
    }

}

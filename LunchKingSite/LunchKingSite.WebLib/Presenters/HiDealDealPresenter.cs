﻿using System;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System.Data;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealDealPresenter : Presenter<IHiDealDealView>
    {
        IHiDealProvider hp;
        ISysConfProvider cfgProv;

        public HiDealDealPresenter()
        {
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            cfgProv = ProviderFactory.Instance().GetConfig();
        }

        public override bool OnViewInitialized()
        {
            
            return true;
        }

        public override bool OnViewLoaded()
        {
            int did = 0;
            HiDealDeal deal=new HiDealDeal();

            #region SetDeal
            if (View.IsPreviewMode)
            {
                if (View.ProductGuid == Guid.Empty)
                    View.RedirectToDefault();
                
                HiDealProduct product = hp.HiDealProductGet(View.ProductGuid);
                deal = hp.HiDealDealGet(product.DealId);
                if (deal.Id == 0)
                    View.RedirectToDefault();
                
                View.Regions = hp.HiDealRegionGetList(deal.Id);
                HiDealSpecialDiscountType specialType = HiDealDealManager.GetDealVisaCardType(deal.Id);

                View.SetDealContent(deal, specialType);
                did = deal.Id;
            }
            else
            {
                if (View.did != "" || View.pid != "")
                {
                    if (View.did != "")
                    {
                        int dealId;
                        if (!int.TryParse(View.did, out dealId))
                            View.RedirectToDefault();
                        deal = hp.HiDealDealGet(dealId);
                    }

                    if (View.pid != "")
                    {
                        int prodId;
                        if (!int.TryParse(View.pid, out prodId))
                            View.RedirectToDefault();
                        HiDealProduct product = hp.HiDealProductGet(prodId);
                        deal = hp.HiDealDealGet(product.DealId);
                    }

                    View.Regions = hp.HiDealRegionGetList(deal.Id);

                    HiDealSpecialDiscountType specialType = HiDealDealManager.GetDealVisaCardType(deal.Id);
                    if (deal.DealStartTime.HasValue && 
                        DateTime.Now.CompareTo(deal.DealStartTime.Value) > 0)
                    {
                        View.SetDealContent(deal, specialType);
                        did = deal.Id;
                    }
                    else
                    {
                        View.RedirectToDefault();
                    }
                }
                else
                {
                    View.RedirectToDefault();
                }
            }



            #endregion

            View.FacebookOgImage = string.Format("{0}{1}", ImageFacade.GetMediaBaseUrl(), deal.PrimaryBigPicture);
            View.FacebookOgTitle = HttpUtility.HtmlEncode(string.Format("{0} {1}", deal.Name, deal.PromoLongDesc));

            #region SetProduct
            HiDealProductCollection products = hp.HiDealProductGetList(-1, -1, HiDealProduct.Columns.Seq, HiDealProduct.Columns.DealId + "=" + did.ToString());
            View.SetProductContent(deal,products);
            #endregion

            #region SetTap
            HiDealContentCollection contents= hp.HiDealContentGetList(-1, -1, HiDealContent.Columns.Seq, HiDealContent.Columns.RefId + "=" + did.ToString(),HiDealContent.Columns.IsShow+"=True");
            View.SetTap(contents,deal);
            #endregion

            #region SetStoresInfo
            ViewHiDealStoresInfoCollection storesinfo = hp.ViewHiDealDealStoreInfoGetByHiDealId(did);
            View.SetStoresInfo(storesinfo);
            #endregion
            return true;
        }

        
    }
}

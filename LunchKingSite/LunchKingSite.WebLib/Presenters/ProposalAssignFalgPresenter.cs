﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Presenters
{
    public class ProposalAssignFalgPresenter : Presenter<IProposalAssignFalgView>
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private ISellerProvider sp;
        private IHumanProvider hp;
        private IMemberProvider mp;
        private IPponProvider pp;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            LoadData();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
          
            return true;
        }

        public ProposalAssignFalgPresenter(ISellerProvider sellerProv, IHumanProvider humProv, IMemberProvider memProv, IPponProvider pponProv)
        {
            sp = sellerProv;
            hp = humProv;
            mp = memProv;
            pp = pponProv;
        }

        #region Private Method

        private void LoadData()
        {
        }
        #endregion

    }
}

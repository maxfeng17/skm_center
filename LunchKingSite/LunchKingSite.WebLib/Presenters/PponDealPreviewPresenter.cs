﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Presenters
{
    public class PponDealPreviewPresenter : Presenter<IPponDealPreviewView>
    {
        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        protected static IItemProvider ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected static ILog Logger = LogManager.GetLogger("OrderFacade");

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            // APP only
            if (PponDealPreviewManager.GetDealCategoryIdList(View.BusinessHourGuid)
                                      .IndexOf(CategoryManager.Default.AppLimitedEdition.CategoryId) > -1)
            {
                View.RedirectToAppLimitedEdtionPage();
                return true;
            }

            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(View.BusinessHourGuid);

            #region 檢查檔次是否為開賣(放)狀態，或者已售完

            PponDealStage pponDealStage = vpd.GetDealStage();
            if (pponDealStage == PponDealStage.RunningAndFull)
            {
                Guid goBackBid = View.BusinessHourGuid;
                View.GoToPponDefaultPage(goBackBid, "此檔好康已銷售一空喔。");
                return true;
            }

            #endregion 檢查檔次是否為開賣(放)狀態，或者已售完

            #region 多重選項資訊

            AccessoryGroupCollection viagc = ip.AccessoryGroupGetListByItem(vpd.ItemGuid);
            foreach (AccessoryGroup ag in viagc)
            {
                ag.members = new List<ViewItemAccessoryGroup>();
                ViewItemAccessoryGroupCollection vigc = ip.ViewItemAccessoryGroupGetList(vpd.ItemGuid, ag.Guid);
                foreach (ViewItemAccessoryGroup vig in vigc)
                {
                    ag.members.Add(vig);
                }
            }

            #endregion 多重選項資訊

            #region 免運費資訊

            // 階梯式運費
            View.TheCouponFreight = pp.CouponFreightGetList(vpd.BusinessHourGuid, CouponFreightType.Income);

            #endregion 免運費資訊

            var stores = pp.ViewPponStoreGetListByBidWithNoLock(View.BusinessHourGuid, VbsRightFlag.VerifyShop);
            View.IsShoppingCart = vpd.ShoppingCart.HasValue ? vpd.ShoppingCart.Value : false;

            View.MultipleBranch = (bool)vpd.MultipleBranch;
            View.SetContent(vpd, null, viagc, 0, stores,
                config.DiscountCodeUsed, (EntrustSellType)vpd.EntrustSell.GetValueOrDefault(), (bool)vpd.IsDailyRestriction);

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            return true;
        }
    }
}
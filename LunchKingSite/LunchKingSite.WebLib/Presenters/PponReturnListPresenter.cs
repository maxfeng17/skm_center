﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class PponReturnListPresenter : Presenter<IPponReturnListView>
    {
        #region members

        protected IOrderProvider op = null;
        protected IPponProvider pp = null;
        protected ISysConfProvider config = null;

        #endregion

        public PponReturnListPresenter()
        {
            SetupProviders();
        }

        protected void SetupProviders()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetStatusFilter();
            SetDepartmentFilter();
            SetFilterTypes();

            return true;
        }


        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.PageChanged += OnPageChanged;
            View.GetProcessMessage += OnGetProcessMessage;
            View.GetProcessMessage += OnGetItemName;
            View.GetOrderCoupons += OnGetOrderCoupons;
            View.GetApplicationTime += OnGetApplicationTime;
            View.SortClicked += OnSortClicked;
            View.SearchClicked += OnSearchClicked;
            View.ExportToExcel += OnExportToExcel;
            View.GetReason += OnGetReason;
            return true;
        }

        protected void SetStatusFilter()
        {
            View.SetStatusFilter(View.StatusFilter);
        }        

        protected void SetDepartmentFilter()
        {
            View.SetDepartmentFilter(View.DepartmentFilter);
        }

        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data);
        }

        protected void OnGetOrderProcess(object sender, DataEventArgs<string> e)
        {
            View.ReturnProcess = (OrderLogStatus)op.ViewPponOrderStatusLogGetListPaging(-1, -1, ViewPponOrderStatusLog.Columns.OrderLogCreateTime + " desc",
                                                         ViewPponOrderStatusLog.Columns.OrderGuid + "=" + e.Data)[0].OrderLogStatus;            
        }

        protected void OnGetProcessMessage(object sender, DataEventArgs<Guid> e)
        {
            OrderReturnList orderLogs = op.OrderReturnListGetbyType(e.Data, null);
            if (orderLogs != null && !string.IsNullOrEmpty(orderLogs.Message))
                View.ProcessMessage = orderLogs.Message;
            else
                View.ProcessMessage = string.Empty;
        }

        protected void OnGetItemName(object sender, DataEventArgs<Guid> e)
        {
            OrderDetailCollection orderDetails = op.OrderDetailGetListByStatus(e.Data);
            StringBuilder itemName = new StringBuilder();
            if (orderDetails.Count>0) 
            {
                foreach(OrderDetail detail in orderDetails)
                {
                    if (detail.ItemName.IndexOf("&nbsp")>0)
                        itemName.Append(detail.ItemName.Substring(detail.ItemName.IndexOf("&nbsp")).Replace("&nbsp",""));
                }
            }
            View.ItemName =itemName.ToString();

        }

        protected void OnGetOrderCoupons(object sender, DataEventArgs<Guid> e)
        {
            View.Coupons = pp.ViewPponCouponGetListRegularOnly(ViewPponCoupon.Columns.OrderGuid, e.Data);
        }

        protected void OnGetApplicationTime(object sender, DataEventArgs<Guid> e)
        {
            ViewPponOrderStatusLogCollection orderLogs = op.ViewPponOrderStatusLogGetListForApplicationInfo(e.Data);
            
            View.ApplicationTime = (orderLogs.Count() > 0) ? orderLogs.First().OrderLogCreateTime.ToString("yyyy/MM/dd") : "查無資料";
        }

        protected void OnSortClicked(object sender, EventArgs e)
        {
            LoadData(1);
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            LoadData(1);
        }

        protected void OnGetReason(object sender, DataEventArgs<Guid> e)
        {
            OrderReturnList retrunLog = op.OrderReturnListGetbyType(e.Data, null);
            if (retrunLog != null && !string.IsNullOrEmpty(retrunLog.Reason))
                View.Reason = retrunLog.Reason;
            else
                View.Reason = string.Empty;
        }

        protected void SetFilterTypes()
        {
            View.SetFilterTypeDropDown(View.FilterTypes);
        }

        protected void OnExportToExcel(object sender, EventArgs e)
        {
            //匯出Excel時只要最後一筆換貨或退貨紀錄
            ViewPponOrderReturnListCollection returnListCol = View.SelectAll ? op.ViewPponOrderReturnListPaging(0, 0, View.SortExpression, GetFilter())
                : op.ViewPponOrderReturnListByLastReturnStatusPaging(0, 0, View.SortExpression, GetFilter());
            View.Export(returnListCol);
        }

        #region Event

        protected void LoadData(int pageNumber)
        {
            string[] filterString = GetFilter();

            //如果使用者勾選 "同時顯示所有退換貨狀態" , 則把所有退換貨紀錄都顯示出來, 否則只顯示 "最後一筆退貨或換貨資料"
            View.SetGridReturn(View.SelectAll ? op.ViewPponOrderReturnListPaging(pageNumber, View.PageSize, View.SortExpression, filterString)
                : op.ViewPponOrderReturnListByLastReturnStatusPaging(pageNumber, View.PageSize, View.SortExpression, filterString));
            View.OsLogCount = View.SelectAll ? op.ViewPponOrderReturnListGetCount(filterString)
                : op.ViewPponOrderReturnListGetCountByLastReturnStatus(filterString);
        }

        protected string[] GetFilter()
        {
            System.Collections.ArrayList filterList = new System.Collections.ArrayList();

            if (View.DepartmentSelected != -1)
            {
                filterList.Add(ViewPponOrderReturnList.Columns.DeliveryType + " = " + View.DepartmentSelected);
            }

            if (!string.IsNullOrEmpty(View.FilterUser))
            {
                if (View.FilterType.Equals(ViewPponOrderReturnList.Columns.OrderId))                
                    filterList.Add(ViewPponOrderReturnList.Columns.OrderId + " like " + View.FilterUser);
                else if(View.FilterType.Equals(ViewPponOrderReturnList.Columns.EventName))
                    filterList.Add(ViewPponOrderReturnList.Columns.EventName + " like " + View.FilterUser);
                else if(View.FilterType.Equals(ViewPponOrderReturnList.Columns.UniqueId))
                    filterList.Add(ViewPponOrderReturnList.Columns.UniqueId + " like " + View.FilterUser);
                else if (View.FilterType.Equals(ViewPponOrderReturnList.Columns.DealEmpName))
                    filterList.Add(ViewPponOrderReturnList.Columns.DealEmpName + " like " + View.FilterUser);
                else if (View.FilterType.Equals(ViewPponOrderReturnList.Columns.SellerName))
                    filterList.Add(ViewPponOrderReturnList.Columns.SellerName + " like " + View.FilterUser);
            }

            if (!string.IsNullOrEmpty(View.ApplyTimeS) && !string.IsNullOrEmpty(View.ApplyTimeE))
            {
                filterList.Add(ViewPponOrderReturnList.Columns.ReturnCreateTime + " between " + View.ApplyTimeS + " and " + View.ApplyTimeE);
            }

            if (View.StatusSelected != -1)
            {
                filterList.Add(ViewPponOrderReturnList.Columns.ReturnStatus+" = "+View.StatusSelected);
            }
            else
            {
                filterList.Add(ViewPponOrderReturnList.Columns.ReturnStatus+" in ("+(int)OrderReturnStatus.ExchangeProcessing+","
                    +(int)OrderReturnStatus.ExchangeSuccess+","
                    +(int)OrderReturnStatus.ExchangeFailure+","
                    +(int)OrderReturnStatus.ExchangeCancel+")");
            }

            if (filterList.Count > 0)
            {
                string[] rtn = new string[filterList.Count];
                for (int i = 0; i < filterList.Count; i++)
                {
                    rtn[i] = (string)filterList[i];
                }
                return rtn;
            }

            return null;

        }

        #endregion
    }
}

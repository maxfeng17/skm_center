﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System.Diagnostics;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.WebLib.Presenters
{
    public class ProcessPresenter : Presenter<IProcessView>
    {
        protected IHumanProvider hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected LunchKingSite.Core.IServiceProvider sp = ProviderFactory.Instance().GetProvider<LunchKingSite.Core.IServiceProvider>();
        public override bool OnViewInitialized()
        {
            
            base.OnViewInitialized();
            View.ProcessList(GetProcessList(1));
            return true;

        }

        public override bool OnViewLoaded()
        {

            base.OnViewLoaded();
            View.Search += OnSearch;
            View.ProcessCount += OnProcessCount;
            View.ProcessPageChanged += OnProcessPageChanged;

            return true;
        }

        #region event
        protected void OnSearch(object sender, EventArgs e)
        {
            View.ProcessList(GetProcessList(1));
        }
        protected void OnProcessCount(object sender, DataEventArgs<int> e)
        {
            e.Data = sp.GetViewCustomerServiceMessageListCount(GetFilter());
        }

        protected void OnProcessPageChanged(object sender, DataEventArgs<int> e)
        {
            View.ProcessList(GetProcessList(e.Data));
        }

        #endregion

        private ViewCustomerServiceListCollection GetProcessList(int pageNumber)
        {
            return sp.GetQueryResultForProcess(pageNumber, View.PageSize, ViewCustomerServiceList.Columns.CasePriority,ViewCustomerServiceList.Columns.CreateTime, GetFilter());
        }

        private string[] GetFilter()
        {
            List<string> filter = new List<string>();



            if (View.SearchType == 0)
            {
                if (View.SearchUserId != 0)
                {
                    if (View.IsCustomerCare2 && !View.IsCustomerCare)
                    {
                        filter.Add(ViewCustomerServiceList.Columns.SecServicePeopleId + " = " + View.SearchUserId);
                        InitialFilter(filter);

                        filter.Add("or " + ViewCustomerServiceList.Columns.ServicePeopleId + " = " + View.SearchUserId);
                        InitialFilter(filter);
                    }
                    else
                    {
                        InitialFilter(filter);
                        filter.Add(ViewCustomerServiceList.Columns.ServicePeopleId + " = " + View.SearchUserId);

                    }
                }
                else
                {
                    InitialFilter(filter);
                }
            }
            else
            {
                InitialFilter(filter);
            }


            return filter.ToArray();
        }

        private List<string> InitialFilter(List<string> filter)
        {
            if (!string.IsNullOrEmpty(View.SearchServiceNo))
            {
                filter.Add(ViewCustomerServiceList.Columns.ServiceNo + " = " + View.SearchServiceNo);
            }
            if (View.SearchOrderGuid != Guid.Empty)
            {
                filter.Add(ViewCustomerServiceList.Columns.OrderGuid + " = " + View.SearchOrderGuid);
            }
            if (!string.IsNullOrEmpty(View.Mail))
            {
                Member mem = mp.MemberGetByUserName(View.Mail);
                if (mem.IsLoaded)
                {
                    filter.Add(ViewCustomerServiceList.Columns.UserId + " = " + mem.UniqueId);
                }
                else
                {
                    MobileMember mobileMem = mp.MobileMemberGet(View.Mail);
                    if (mobileMem.IsLoaded)
                    {
                        filter.Add(ViewCustomerServiceList.Columns.UserId + " = " + mobileMem.UserId);
                    }
                    else
                    {
                        filter.Add(ViewCustomerServiceList.Columns.UserId + " = 0 ");
                    }
                }
            }
            if (!string.IsNullOrEmpty(View.SDate))
            {
                filter.Add(ViewCustomerServiceList.Columns.CreateTime + " >= " + View.SDate);

            }
            if (!string.IsNullOrEmpty(View.EDate))
            {
                filter.Add(ViewCustomerServiceList.Columns.CreateTime + " <= " + View.EDate);
            }
            if (View.SearchPriority != -1)
            {
                filter.Add(ViewCustomerServiceList.Columns.CasePriority + " = " + View.SearchPriority);
            }
            if (View.SearchCategory != 0)
            {
                filter.Add(ViewCustomerServiceList.Columns.Category + " = " + View.SearchCategory);
            }
            filter.Add(ViewCustomerServiceList.Columns.CustomerServiceStatus + "<> 0");
            filter.Add(ViewCustomerServiceList.Columns.CustomerServiceStatus + "<> 4");
            return filter;
        }

    }
}

﻿using System;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Component;
using System.Data;
using System.Collections.Generic;
using log4net;
using LunchKingSite.BizLogic.Models;

namespace LunchKingSite.WebLib.Presenters
{
    public class PiinlifeDealPresenter : Presenter<IPiinlifeDealView>
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private ILog Logger = LogManager.GetLogger("PponFacade");

        public PiinlifeDealPresenter()
        {

        }

        public override bool OnViewInitialized()
        {
            if (View.BusinessHourGuid != Guid.Empty)
            {
                IViewPponDeal mainDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(View.BusinessHourGuid);
                if (Helper.IsFlagSet(mainDeal.BusinessHourStatus, BusinessHourStatus.TmallDeal))
                {
                    View.RedirectToPiinlifeDefault();
                }
                else if (mainDeal != null && mainDeal.BusinessHourGuid != Guid.Empty)
                {
                    #region Retargeting

                    try
                    {
                        View.DealArgs = RetargetingUtility.GetDealArgs(mainDeal, View.UserId);
                    }
                    catch (Exception ex)
                    {
                        Logger.InfoFormat("Retargeting Exception -> {0}. {1}", ex.Message, ex.StackTrace);
                    }

                    #endregion Retargeting

                    Dictionary<IViewPponDeal, string> subDeals = new Dictionary<IViewPponDeal, string>();
                    if (Helper.IsFlagSet(mainDeal.BusinessHourStatus, BusinessHourStatus.ComboDealSub) && mainDeal.MainBid != null)
                    {
                        View.RedirectToComboDealMain(mainDeal.MainBid, mainDeal.BusinessHourGuid);
                        return true;
                    }
                    else if (Helper.IsFlagSet(mainDeal.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
                    {
                        var combodeals = ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(mainDeal.BusinessHourGuid);
                        foreach (ViewComboDeal combo in combodeals)
                        {
                            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(combo.BusinessHourGuid);
                            subDeals.Add(vpd, combo.Title);
                        }
                    }
                    else
                    {
                        //暫存資料正確性與JOB執行周期有關,自行重查銷售數量
                        mainDeal.OrderedQuantity = op.OrderDetailGetOrderedQuantityByBid(mainDeal.BusinessHourGuid, mainDeal.BusinessHourOrderTimeS >= config.StopRefundQuantityCalculateDate);
                        int position = mainDeal.ItemName.IndexOf('-') + 1;
                        string itemname = position > 0 ? mainDeal.ItemName.Substring(position, mainDeal.ItemName.Length - position) : mainDeal.ItemName;
                        subDeals.Add(mainDeal, itemname);
                    }
                    CategoryCollection cds = pp.CategoryDealsTypeGetList(mainDeal.BusinessHourGuid);

                    View.SetDealContent(mainDeal, subDeals);
                    View.SeoSetting(mainDeal, string.Join(",", cds));

                    var ttmodel = new TrackTagInputModel
                    {
                        ViewPponDealData = mainDeal,
                        Config = config,
                        IsPiinLifeDeal = true,
                        GtagPage = GtagPageType.Product,
                        YahooEa = YahooEaType.ViewProduct,
                        //ScupioPageId = ScupioPageId.Item
                    };

                    View.MicroDataJson = TagManager.CreateFatory(ttmodel, TagProvider.MicroData).GetJson();
                }
                else
                {
                    View.RedirectToPiinlifeDefault();
                }
            }
            else
            {
                View.RedirectToPiinlifeDefault();
            }
            return true;
        }
    }
}

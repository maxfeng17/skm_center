﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Presenters
{
    public class EventEditorPresenter : Presenter<IEventEditorView>
    {
        private IEventProvider ep = ProviderFactory.Instance().GetProvider<IEventProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (View.EventId != "")
            {
                LoadEvent(new Guid(View.EventId));
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SaveEvent += OnSaveEvent;
            return true;
        }

        private void LoadEvent(Guid guid)
        {
            var EventContent = ep.EventContentGet(guid);
            PromoSeoKeyword seo = PromotionFacade.GetPromoSeoKeyword((int)PromoSEOType.PromoManage,EventContent.Id);
            View.LoadEvent(EventContent, seo);
        }

        private void OnSaveEvent(object sender, DataEventArgs<EventContentSave> e)
        {
            if (ep.EventContentSet(e.Data.EventContent))
            {
                View.EventId = e.Data.EventContent.Guid.ToString();
            }


            PromoSeoKeyword seo = PromotionFacade.GetPromoSeoKeyword((int)PromoSEOType.PromoManage, e.Data.EventContent.Id);
            if (string.IsNullOrEmpty(e.Data.SeoDescription) && string.IsNullOrEmpty(e.Data.SeoKeyWords))
            {
                PromotionFacade.DeletePromoSeoKeyword(seo);
            }
            else
            {
                if (seo.Id == 0)
                {
                    seo.CreateId = e.Data.UserId;
                    seo.CreateTime = DateTime.Now;
                }
                seo.PromoType = (int)PromoSEOType.PromoManage;
                seo.ActivityId = e.Data.EventContent.Id;
                seo.ModifyId = e.Data.UserId;
                seo.ModifyTime = DateTime.Now;
                seo.Description = e.Data.SeoDescription;
                seo.Keyword = e.Data.SeoKeyWords;

                PromotionFacade.SavePromoSeoKeyword(seo);
            }

            View.LoadEvent(e.Data.EventContent, seo);
        }


        
    }
}

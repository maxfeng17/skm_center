﻿using System;
using System.Collections.Specialized;
using System.Linq;
using LunchKingSite.BizLogic.Jobs;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Presenters
{
    public class MassEmailerPresenter : Presenter<IMassEmailerView>
    {
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Send += OnSend;
            View.EdmSend += OnEdmSend;
            return true;
        }

        public override bool OnViewInitialized()
        {
            View.SendTime = GetDefaultSendTime(DateTime.Now);
            View.SetTopCityList(CityManager.Citys.Where(x => !x.Code.Equals("sys", StringComparison.OrdinalIgnoreCase)).ToList());
            return base.OnViewInitialized();
        }

        #region Event Handler
        private void OnSend(object sender, DataEventArgs<string> e)
        {
            IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            string content = e.Data;
            if (string.IsNullOrEmpty(content))
            {
                View.SetResultMessage(I18N.Message.FileNotFound);
                return;
            }

            MemberCollection mc = null;
            MemberStatusFlag status = View.MailType == MassMailType.Announcement ? MemberStatusFlag.None : MemberStatusFlag.ReceiveAdMail;
            JobSetting js = new JobSetting(typeof(NightlyMailDelivery), View.Description);
            js.Enabled = true;
            js.StartTime = View.SendTime ?? GetDefaultSendTime(DateTime.Now);
            js.Parameters = new NameValueCollection
            {
                { "s", View.Subject },
                { "c", content },
                { "t", status.ToString("d") }
            };
            if (View.FilterType != MassMailFilterType.None)
            {
                if (View.FilterType == MassMailFilterType.CityArea && View.CityBuildingFilter != null && View.CityBuildingFilter.Count > 0)
                {
                    int dummy;
                    var cities = from x in View.CityBuildingFilter where int.TryParse(x, out dummy) select x;
                    var buildings = View.CityBuildingFilter.Except(cities);
                    mc = new MemberCollection();
                    if (cities.Count() > 0)
                        js.Parameters.Add("cf", string.Join("|", cities.ToArray()));
                    if (buildings.Count() > 0)
                        js.Parameters.Add("bf", string.Join("|", buildings.ToArray()));
                    if (View.IsContainSubscription) //包含訂閱EDM的會員
                    {
                        mc.AddRange(mp.MemberGetListByCity(status, cities != null ? cities.ToArray() : null,
                                                           buildings != null ? buildings.ToArray() : null));
                    }
                    else //不包含訂閱EDM的會員
                    {
                        mc.AddRange(mp.MemberGetListByCityWithOutSubscription(status, cities != null ? cities.ToArray() : null,
                                                           buildings != null ? buildings.ToArray() : null));
                    }
                }
                else if (View.SellerFilter != null && View.SellerFilter.Count > 0)
                {
                    js.Parameters.Add("sf", string.Join("|", (from x in View.SellerFilter select x.ToString()).ToArray()));
                    if (View.IsContainSubscription)
                    {
                        mc = mp.MemberGetListDeliverableBySeller(status, View.SellerFilter.ToArray());
                    }
                    else
                    {
                        mc = mp.MemberGetListDeliverableBySellerWithOutSubscription(status, View.SellerFilter.ToArray());
                    }
                }
                else
                    mc = new MemberCollection();
            }
            else
            {
                if (View.IsContainSubscription)
                {
                    mc = mp.MemberGetListByStatus(status, status, null);
                }
                else
                {
                    mc = mp.MemberGetListByStatusWithOutSubscription(status, status, null);
                }
            }
            Jobs.Instance.QueueJob(js);
            View.SetResultMessage(string.Format(I18N.Message.MassMailQueued, js.StartTime, mc.Count));
        }
        private void OnEdmSend(object sender, DataEventArgs<string> e)
        {
            IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            string content = e.Data;
            if (string.IsNullOrEmpty(content))
            {
                View.SetResultMessage(I18N.Message.FileNotFound);
                return;
            }
            else
            {
                char[] delimiters = new char[] { ',' };
                List<string> emailList = content.Replace("\r\n", ",").Split(delimiters, StringSplitOptions.RemoveEmptyEntries).ToList();

                for (int i = 0; i < emailList.Count(); i++)
                {
                    EdmEliminateEmail data = mp.EdmEliminateEmailGet(EdmEliminateEmail.Columns.Email, emailList[i]);
                    if (string.IsNullOrEmpty(data.Email))
                    {
                        data.Email = emailList[i];
                        data.CreateTime = DateTime.Now;
                        mp.EdmEliminateEmailSet(data);
                    }
                }
            }
        }
        #endregion

        public string GetChildrenCityJson(int parentCityId)
        {
            ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            ISerializer ser = ProviderFactory.Instance().GetSerializer();
            CityCollection cities = lp.CityGetList(parentCityId);
            string result;
            if (cities == null || cities.Count == 0)
            {
                BuildingCollection b = lp.BuildingGetListByArea(parentCityId);
                result = ser.Serialize((from x in b select new { val = x.Guid, text = x.BuildingName }).ToArray());
            }
            else
            {
                result = ser.Serialize((from x in cities select new {val = x.Id, text = x.CityName}).ToArray());
            }
            return result;
        }

        public string GetSellerJson(string partialName)
        {
            SellerCollection sellers =
                ProviderFactory.Instance().GetProvider<ISellerProvider>().SellerGetListByNameWithActiveTransport(
                    "%" + partialName + "%");
            var result = (from x in sellers select new { val = x.Guid, text = string.Format("({0}) {1}", ((DepartmentTypes) x.Department).ToString()[0], x.SellerName)}).ToArray();
            return ProviderFactory.Instance().GetSerializer().Serialize(result);
        }

        private DateTime GetDefaultSendTime(DateTime day)
        {
            return day.Date.Add(new TimeSpan(1, 1, 30, 0));
        }
    }
}
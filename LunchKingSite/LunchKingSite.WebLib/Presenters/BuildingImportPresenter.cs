﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Web.Security;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.WebLib.Presenters
{
    public class BuildingImportPresenter : Presenter<IBuildingImportView>
    {


        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }


        public override bool OnViewLoaded() 
        {
            base.OnViewLoaded();
            View.UpdataBuildingInfo += OnUpdataBuildingInfo ;
            return true;
        }
        public void OnUpdataBuildingInfo(object sender, DataEventArgs<UpdatableBuildingInfo> e)
        {
            ILocationProvider ilp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            UpdatableBuildingInfo info = e.Data;
            Building building;

            building = new Building();
            building.Guid = info.BuildingGuid;
            building.CreateTime = DateTime.Now;
            building.CityId = info.CityId;
            building.BuildingStreetName = info.BuildingName;
            building.BuildingName = info.BuildingName;
            building.BuildingOnline = true;
            building.BuildingRank = info.BuildingRank;
            building.CreateId = info.CreateId;
            if (!string.IsNullOrEmpty(info.BuildingAtitude))
            {
                building.Coordinate = LocationFacade.GetGeographyWKT(info.BuildingAtitude, info.BuildingLongitude);

            }
            if (building.IsDirty)
            ilp.BuildingSet(building);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesPponDealCalendarPresenter : Presenter<ISalesPponDealCalendarView>
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private IPponProvider pp;
        private ISellerProvider sp;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.ReadAll))
            {
                LoadData(View.Dept, View.Year, View.Month, "", 0);
            }
            else if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.CrossDeptTeam))
            {
                LoadData("", View.Year, View.Month, View.CrossDeptTeam, 0);
            }
            else if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.Read))
            {
                LoadData("", View.Year, View.Month, "", View.EmpUserId);
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.DeptChanged += OnDeptChanged;
            View.DealTypeChanged += OnSearch;
            View.PreviousSearch += OnMonthSearch;
            View.NextSearch += OnMonthSearch;
            return true;
        }

        public SalesPponDealCalendarPresenter(IPponProvider pponProv, ISellerProvider sellerProv)
        {
            pp = pponProv;
            sp = sellerProv;
        }

        protected void OnDeptChanged(object sender, DataEventArgs<string> e)
        {
            //有全區權限
            LoadData(e.Data, View.Year, View.Month, "", 0);
        }
        protected void OnSearch(object sender, EventArgs e)
        {
            //有全區權限
            LoadData(View.Dept, View.Year, View.Month, "", 0);
        }
        protected void OnMonthSearch(object sender, EventArgs e)
        {
            if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.ReadAll))
            {
                LoadData(View.Dept, View.Year, View.Month, "",0);
            }
            else if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.CrossDeptTeam))
            {
                LoadData("", View.Year, View.Month, View.CrossDeptTeam,0);
            }
            else if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.Read))
            {
                LoadData("", View.Year, View.Month, "", View.EmpUserId);
            }
                
        }
        #region Private Method

        private void LoadData(string deptId, int Year, int Month, string CrossDeptTeam, int empUserId)
        {
            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            builder.AppendLine(DateTime.Now.ToString("yyyyy/MM/dd HH:mm:ss:fff") + " " + "【LoadData】Begin");
            DateTime now = DateTime.Now;
            DateTime start = new DateTime(Year, Month, 1);
            string sSQL = "";
            IEnumerable<ViewPponDealCalendar> vpdc = pp.ViewPponDealCalendarGetListByPeriod(start, start.AddMonths(View.MonthCount), deptId, CrossDeptTeam, ref sSQL);
            builder.AppendLine(DateTime.Now.ToString("yyyyy/MM/dd HH:mm:ss:fff") + " " + "sSQL = " + sSQL);
            if (empUserId != 0)
            {
                vpdc = vpdc.Where(x => x.DevelopeSalesId == empUserId || x.OperationSalesId == empUserId);
            }
            builder.AppendLine(DateTime.Now.ToString("yyyyy/MM/dd HH:mm:ss:fff") + " " + "【LoadData】ViewPponDealCalendar");

            if (View.DealType1 > 0 && View.DealType2 == -1)
            {
                ProposalDealType type = ProposalDealType.TryParse(View.DealType1.ToString(), out type) ? type : ProposalDealType.None;
                Dictionary<int, string> rtn = new Dictionary<int, string>();
                if (type != ProposalDealType.None)
                {
                    rtn = ProposalFacade.ProposalSubDealTypeGet(type, false);
                    vpdc = vpdc.Where(x => rtn.Keys.Contains((int)(x.NewDealType ?? -1)));
                }
            }
            if (View.DealType2 > 0)
            {
                vpdc = vpdc.Where(x => x.NewDealType == View.DealType2);
            }
            int cnt = vpdc.Count();
            int idx = 0;
            Dictionary<ViewPponDealCalendar, Proposal> dataList = new Dictionary<ViewPponDealCalendar, Proposal>();


            while (idx <= cnt - 1)
            {
                int batchLimit = 500;
                var newVpdc = vpdc.Skip(idx).Take(batchLimit);
                var bids = newVpdc.Select(x => x.BusinessHourGuid).Distinct().ToList();
                List<Proposal> pros = new List<Proposal>();
                if (!config.IsProposalOnMemory)
                {
                    pros = sp.ProposalGetByBids(bids).ToList();
                }

                builder.AppendLine(DateTime.Now.ToString("yyyyy/MM/dd HH:mm:ss:fff") + " " + "【LoadData】Loop");

                foreach (ViewPponDealCalendar vpd in newVpdc)
                {
                    //Proposal pro = sp.ProposalGet(Proposal.Columns.BusinessHourGuid, vpd.BusinessHourGuid);
                    //Proposal pro = pros.Where(x => x.BusinessHourGuid == vpd.BusinessHourGuid).FirstOrDefault();
                    Proposal pro = new Proposal();
                    if (!config.IsProposalOnMemory)
                    {
                        pro = pros.Where(x => x.BusinessHourGuid == vpd.BusinessHourGuid).FirstOrDefault();
                    }
                    else
                    {
                        pro = ProposalFacade.GetProposals(vpd.BusinessHourGuid);
                    }
                    if (pro == null)
                    {
                        pro = new Proposal();
                    }
                    else
                    {
                        //pros.Remove(pro);
                    }
                    dataList.Add(vpd, pro);
                }

                idx += batchLimit;
            }


            builder.AppendLine(DateTime.Now.ToString("yyyyy/MM/dd HH:mm:ss:fff") + " " + "【LoadData】End");

            ProposalFacade.ProposalPerformanceLogSet("/sal/PponDealCalendar.aspx", builder.ToString(), View.UserName);
            View.SetPponDealCalendar(dataList);
        }

        #endregion
    }
}

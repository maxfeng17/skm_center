﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class ReturnDiscountPresenter : Presenter<IReturnDiscountView>
    {
        private IOrderProvider _ordProv;
        private IMemberProvider _memProv;
        private IPponProvider _pponProv;
        private ISysConfProvider _confProv;

        public ReturnDiscountPresenter(IPponProvider pponProv, IMemberProvider memProv, IOrderProvider ordProv, ISysConfProvider confProv)
        {
            _memProv = memProv;
            _ordProv = ordProv;
            _pponProv = pponProv;
            _confProv = confProv;
        }
        public override bool OnViewInitialized()
        {
            string message = string.Empty;
            bool isleagl = true;
            if ((View.OrderGuid == null || string.IsNullOrEmpty(View.UserName)))
                isleagl = false;
            EinvoiceMain einvoice = _ordProv.EinvoiceMainGet(View.OrderGuid);
            List<EinvoiceMain> einvoices = _ordProv.EinvoiceMainGetListByOrderGuid(View.OrderGuid, OrderClassification.LkSite)
                .Where(x => x.VerifiedTime != null).ToList();
            List<EntrustSellReceipt> receipts = _ordProv.EntrustSellReceiptGetListByOrder(View.OrderGuid);
            EntrustSellReceipt receipt = receipts.FirstOrDefault();
            CashTrustLogCollection cs = _memProv.CashTrustLogGetListByOrderGuid(View.OrderGuid, OrderClassification.LkSite);
            OldCashTrustLogCollection ocs = _memProv.GetOldCashTrustLogCollectionByOid(View.OrderGuid);
            Member m = _memProv.MemberGet(View.UserName);
            CtAtmRefund atmrefund = _ordProv.CtAtmRefundGetLatest(View.OrderGuid);
            Order o = _ordProv.OrderGet(View.OrderGuid);
            var returnForm = _ordProv.ReturnFormGetListByOrderGuid(View.OrderGuid);
            //取得退貨單狀態
            var returnFormStatus = returnForm.Any() 
                                    ? returnForm.OrderByDescending(x => x.Id).FirstOrDefault().ProgressStatus 
                                    : 0;
                        
            if (einvoices.Count()==0 && (receipt == null || receipt.ReceiptCode == null))//沒發票 也沒代收轉付收據
            {
                isleagl = false;
                message = "此訂單不需填寫【折讓單】!";
            }
            else if (!returnForm.Any())
            {
                isleagl = false;
                message = "此訂單未進行退貨申請，不需填寫【折讓單】!";
            }
            else if (View.UserId == 0 || (einvoices.Count > 0 && einvoices.First().UserId != View.UserId)
                || o.UserId != View.UserId)
            {
                isleagl = false;
                message = "會員資料有誤，請洽客服中心!";
            }
            else if (cs.Count > 0)
            {
                List<CashTrustLog> couponTypeCs = cs.Where(log => log.CouponId != null).ToList();

                if (cs.First().UserId != m.UniqueId)//同會員
                {
                    isleagl = false;
                    message = "會員資料有誤，請洽客服中心!";
                }
            }
            else if (cs.Count == 0)
            {
                if (ocs.Count == 0 && einvoices.Count() > 0 && !einvoices.First().OrderIsPponitem)
                {
                    isleagl = false;
                    message = "此訂單已全部使用或已申請退貨!";
                }
                else if (ocs.Count > 0 && ocs.Count(x => x.Status < (int)TrustStatus.Verified) == 0)
                {
                    isleagl = false;
                    message = "此訂單已全部使用或已申請退貨!";
                }
            }

            ViewPponCouponCollection vpcc = _pponProv.ViewPponCouponGetList(ViewPponCoupon.Columns.OrderGuid, View.OrderGuid);
            ViewPponCoupon vpc = vpcc.FirstOrDefault();
            DealProperty dp;
            if (vpc != null)
            {
                dp = _pponProv.DealPropertyGet(vpc.BusinessHourGuid);
            }
            else
            {
                dp = new DealProperty();
            }
            if (dp.SaleMultipleBase > 0)
            {
                CashTrustLog newCtl = new CashTrustLog();
                newCtl.CreditCard = cs.Where(x => x.Status != (int)TrustStatus.Verified).Sum(x => x.CreditCard);
                newCtl.Atm = cs.Where(x => x.Status != (int)TrustStatus.Verified).Sum(x => x.Atm);
                newCtl.UninvoicedAmount = cs.Where(x => x.Status != (int)TrustStatus.Verified).Sum(x => x.UninvoicedAmount);
                cs = new CashTrustLogCollection();
                cs.Add(newCtl);
            }
            if (einvoices.Count()>0)
            {
                if(einvoices.First().OrderTime<Convert.ToDateTime(_confProv.NewInvoiceDate) || dp.DeliveryType==(int)DeliveryType.ToHouse || dp.SaleMultipleBase > 0)
                    View.SetRefundFormInfo(einvoice, cs, ocs, atmrefund, dp, o, isleagl, message, returnFormStatus);
                else
                    View.SetRefundFormInfoForMultiInvoices(einvoices, cs, atmrefund, dp, o, isleagl, message, returnFormStatus);
            }
            else
            {
                View.SetEntrustSellRefundFormInfo(receipts, o, cs, dp, isleagl, message, returnFormStatus);
            }
            return true;
        }
    }
}

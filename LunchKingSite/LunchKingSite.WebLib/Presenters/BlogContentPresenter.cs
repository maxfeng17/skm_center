﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Presenters
{
    public class BlogContentPresenter : Presenter<IBlogContentView>
    {
        protected IBlogProvider _blogProv;

        public BlogContentPresenter()
        {
            _blogProv = ProviderFactory.Instance().GetProvider<IBlogProvider>();
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Save += OnSave;
            return true;
        }
        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            LoadData();
            return true;
        }
        protected void OnSave(object sender, EventArgs e)
        {

            BlogPost blog = _blogProv.BlogPostGet(View.Id);
            if (!blog.IsLoaded)
            {
                blog = new BlogPost();
                blog.CreateId = View.UserName;
                blog.CreateTime = DateTime.Now;
            }
            blog.Guid = View.BlogGid;
            blog.Title = View.BlogTitle;
            blog.KeyWord = View.BlogKeyWord;
            blog.BlogDescription = View.BlogDescription;
            blog.BeginDate = View.BeginDate;
            blog.BlogContent = View.BlogContentText;
            blog.ModifyId = View.UserName;
            blog.ModifyTime = DateTime.Now;
            _blogProv.BlogPostSet(blog);

            View.ShowMessage("儲存成功!");
            View.SetBlogContent(blog);

        }
        private void LoadData()
        {
            BlogPost blog = _blogProv.BlogPostGet(View.Id);
            if (blog.IsLoaded)
            {
                if(blog.Guid == Guid.Empty)
                {
                    View.BlogGid = Guid.NewGuid();
                    blog.Guid = View.BlogGid;
                }
                View.BlogGid = blog.Guid;
            }
            else
            {
                View.BlogGid = Guid.NewGuid();
            }
            View.SetBlogContent(blog);
        }
    }
}

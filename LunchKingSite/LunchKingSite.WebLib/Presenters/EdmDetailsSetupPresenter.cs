﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;


namespace LunchKingSite.WebLib.Presenters
{
    public class EdmDetailsSetupPresenter : Presenter<IEdmDetailsSetupView>
    {
        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.GetViewPponDealTimeSlotCollectionByCityDate(GetViewPponDealTimeSlotCollectionByCityDate());

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();

            View.GetViewPponDealTimeSlotCollection += OnGetViewPponDealTimeSlotCollection;
            View.GetViewPponDealByBid += OnGetViewPponDealByBid;
            return true;
        }

        public void OnGetViewPponDealByBid(object sender, DataEventArgs<KeyValuePair<Guid, int>> e)
        {
            ViewPponDeal deal = EmailFacade.ViewPponDealGetByBusinessHourGuid(e.Data.Key);
            View.RetrieveViewPponDealByBid(deal, e.Data.Value);
        }

        public void OnGetViewPponDealTimeSlotCollection(object sender, EventArgs e)
        {
            var col = GetViewPponDealTimeSlotCollectionByCityDate();
            ViewPponDealTimeSlotCollection tempCol = new ViewPponDealTimeSlotCollection();//排序用集合
            ViewPponDealTimeSlotCollection tempSpaCol = new ViewPponDealTimeSlotCollection();//特定城市SPA檔，

            //女性專區區域判定
            IEnumerable<ViewPponDealTimeSlot> deals;
            if (View.EdmSetupCityId != PponCityGroup.DefaultPponCityGroup.Travel.CityId && View.EdmSetupCityId != PponCityGroup.DefaultPponCityGroup.AllCountry.CityId && View.EdmSetupCityId != PponCityGroup.DefaultPponCityGroup.Family.CityId && View.EdmSetupCityId != PponCityGroup.DefaultPponCityGroup.Tmall.CityId)
            {
                if (View.CityId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId && View.EdmSetupCityId != View.CityId)
                {
                    //依區域撈取SPA檔
                    #region 女性專區區域分類CityID

                    List<CategoryNode> spaAreaNodes = new List<CategoryNode>();
                    spaAreaNodes.Add(CategoryManager.Default.FemaleTaipei);
                    spaAreaNodes.Add(CategoryManager.Default.FemaleTaoyuan);
                    spaAreaNodes.Add(CategoryManager.Default.FemaleHsinchu);
                    spaAreaNodes.Add(CategoryManager.Default.FemaleTaichung);
                    spaAreaNodes.Add(CategoryManager.Default.FemaleTainan);
                    spaAreaNodes.Add(CategoryManager.Default.FemaleKaohsiung);

                    #endregion 女性專區區域分類CityID

                    CategoryNode node = spaAreaNodes.FirstOrDefault(x => x.CityId == View.EdmSetupCityId);
                    var multipleMainDealPreviewlist = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByChannel(PponCityGroup.DefaultPponCityGroup.PBeautyLocation.ChannelId.Value, node.CategoryId, null);

                    List<Guid> citySpaMainDeal = (from x in multipleMainDealPreviewlist select x.PponDeal.BusinessHourGuid).ToList();

                    deals = col.Where(x => citySpaMainDeal.Contains(x.BusinessHourGuid));
                    deals = deals.Where(x => ((x.BusinessHourStatus & (int)GroupOrderStatus.KindDeal) <= 0));
                    
                    List<KeyValuePair<ViewPponDealTimeSlot, int>> sortQuantityViewPponDealTimeSlot = new List<KeyValuePair<ViewPponDealTimeSlot, int>>();
                    foreach (var viewPponDealTimeSlot in deals.ToList())
                    {
                        IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(viewPponDealTimeSlot.BusinessHourGuid);
                        sortQuantityViewPponDealTimeSlot.Add(new KeyValuePair<ViewPponDealTimeSlot, int>(viewPponDealTimeSlot, Convert.ToInt32(deal.ItemPrice)));
                    }
                    foreach (var item in sortQuantityViewPponDealTimeSlot.Where(x=>x.Value != 0 ))
                    {
                        tempSpaCol.Add(item.Key);
                    }
                    col = tempSpaCol;

                }
            }

            switch (View.SortType)
            {
                case (int)EDMDetailsSortType.BySequence:
                    tempCol = col;
                    break;
                case (int)EDMDetailsSortType.ByOrderedDate:
                    List<KeyValuePair<ViewPponDealTimeSlot, DateTime>> sortQuantityViewPponDealTimeSlotDate = new List<KeyValuePair<ViewPponDealTimeSlot, DateTime>>();
                    foreach (var viewPponDealTimeSlot in col.ToList())
                    {
                        IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(viewPponDealTimeSlot.BusinessHourGuid);
                        sortQuantityViewPponDealTimeSlotDate.Add(new KeyValuePair<ViewPponDealTimeSlot, DateTime>(viewPponDealTimeSlot, deal.BusinessHourOrderTimeS));
                    }
                    foreach (var item in sortQuantityViewPponDealTimeSlotDate.OrderByDescending(x => x.Value))
                    {
                        tempCol.Add(item.Key);
                    }
                    break;
                case (int)EDMDetailsSortType.ByOrderedQuantity:
                    List<KeyValuePair<ViewPponDealTimeSlot, int>> sortQuantityViewPponDealTimeSlot = new List<KeyValuePair<ViewPponDealTimeSlot, int>>();
                    foreach (var viewPponDealTimeSlot in col.ToList())
                    {
                        IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(viewPponDealTimeSlot.BusinessHourGuid);
                        sortQuantityViewPponDealTimeSlot.Add(new KeyValuePair<ViewPponDealTimeSlot, int>(viewPponDealTimeSlot, (deal.OrderedQuantity.HasValue) ? deal.OrderedQuantity.Value : 0));
                    }
                    foreach (var item in sortQuantityViewPponDealTimeSlot.OrderByDescending(x => x.Value))
                    {
                        tempCol.Add(item.Key);
                    }

                    break;

                case (int)EDMDetailsSortType.ByOrderedTotal:
                    List<KeyValuePair<ViewPponDealTimeSlot, decimal>> sortTotalViewPponDealTimeSlot = new List<KeyValuePair<ViewPponDealTimeSlot, decimal>>();
                    foreach (var viewPponDealTimeSlot in col.ToList())
                    {
                        IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(viewPponDealTimeSlot.BusinessHourGuid);
                        sortTotalViewPponDealTimeSlot.Add(new KeyValuePair<ViewPponDealTimeSlot, decimal>(viewPponDealTimeSlot, (deal.OrderedTotal.HasValue) ? deal.OrderedTotal.Value : default(decimal)));
                    }
                    foreach (var item in sortTotalViewPponDealTimeSlot.OrderByDescending(x => x.Value))
                    {
                        tempCol.Add(item.Key);
                    }

                    break;
                default:
                    tempCol = col;
                    break;
            }


            View.GetViewPponDealTimeSlotCollectionByCityDate(tempCol);
        }

        #region method

        public ViewPponDealTimeSlotCollection GetViewPponDealTimeSlotCollectionByCityDate()
        {
            return EmailFacade.ViewPponDealTimeSlotGetListByCityDate(View.CityId, View.DealDate.HasValue ? View.DealDate.Value : View.DeliveryDate);
        }
        #endregion
    }


}

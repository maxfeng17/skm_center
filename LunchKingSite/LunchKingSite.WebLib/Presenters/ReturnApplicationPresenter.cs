﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class ReturnApplicationPresenter : Presenter<IReturnApplicationView>
    {
        private IOrderProvider _ordProv;
        private IMemberProvider _memProv;
        private IPponProvider _pponProv;

        public ReturnApplicationPresenter(IPponProvider pponProv, IMemberProvider memProv, IOrderProvider ordProv)
        {
            _memProv = memProv;
            _ordProv = ordProv;
            _pponProv = pponProv;
        }
        public override bool OnViewInitialized()
        {
            bool isleagl = true;
            if ((View.OrderGuid == null || string.IsNullOrEmpty(View.UserName)))
                isleagl = false;
            EinvoiceMain einvoice = _ordProv.EinvoiceMainGet(View.OrderGuid);
            EntrustSellReceipt receipt = _ordProv.EntrustSellReceiptGetByOrder(View.OrderGuid);
            CashTrustLogCollection cs = _memProv.CashTrustLogGetListByOrderGuid(View.OrderGuid, OrderClassification.LkSite);
            OldCashTrustLogCollection ocs = _memProv.GetOldCashTrustLogCollectionByOid(View.OrderGuid);
            Member m = _memProv.MemberGet(View.UserName);
            CtAtmRefund atmrefund = _ordProv.CtAtmRefundGetLatest(View.OrderGuid);
            Order o = _ordProv.OrderGet(View.OrderGuid);
            bool isatm = false;
            //einvoice
            if (View.UserId == 0 || (einvoice.Id != 0 && einvoice.UserId != View.UserId)
             || (receipt.Id != 0 && o.UserId != View.UserId))
            {
                isleagl = false;
                View.message = "會員資料有誤，請洽客服中心!";
            }
            else if (cs.Count > 0)
            {
                List<CashTrustLog> couponTypeCs = cs.Where(log => log.CouponId != null).ToList();

                if (cs.First().UserId != m.UniqueId)//同會員
                {
                    isleagl = false;
                    View.message = "會員資料有誤，請洽客服中心!";
                }
            }
            else if (cs.Count == 0)
            {
                if (ocs.Count == 0 && !einvoice.OrderIsPponitem)
                {
                    isleagl = false;
                    View.message = "此訂單已全部使用或已申請退貨!";
                }
                else if (ocs.Count > 0 && ocs.Count(x => x.Status < (int)TrustStatus.Verified) == 0)
                {
                    isleagl = false;
                    View.message = "此訂單已全部使用或已申請退貨!";
                }
            }

            if (isleagl)
            {
                ViewPponCouponCollection vcc = _pponProv.ViewPponCouponGetList(ViewPponCoupon.Columns.OrderGuid, View.OrderGuid);
                DealProperty dp = _pponProv.DealPropertyGet(vcc.First().BusinessHourGuid);
                OrderReturnList orl = _ordProv.OrderReturnListGet(View.OrderGuid);

                if (cs.Count > 0)
                {
                    foreach (var item in cs)
                    {
                        if ((dp.DeliveryType == (int)DeliveryType.ToHouse && item.Status < ((int)TrustStatus.Refunded)) ||
                              (dp.DeliveryType == (int)DeliveryType.ToShop && item.Status != ((int)TrustStatus.Verified) && item.Status != ((int)TrustStatus.Refunded)))
                        {
                            if (item.Atm > 0)
                                isatm = true;
                        }
                    }
                }

                if (isatm)
                {
                    bool isRefund = !Helper.IsFlagSet(orl.Flag, OrderReturnFlags.RefundScashOnly);
                    if (isRefund)
                    {
                        View.SetAtm(atmrefund);
                    }
                    View.IsAtm = isRefund;
                }
                else
                {
                    View.IsAtm = false;
                }

                if (vcc.Count > 0)
                {
                    //orl.CreateTime = 1900/1/1 代表Null, 也就是使用者未退過貨, 如果這樣的話就把日期改成當日
                    View.ReturnTime = orl.CreateTime == new DateTime(1900,1,1) ? DateTime.Now : orl.CreateTime;
                    View.DealNumber = dp.UniqueId.ToString();
                    View.OrderId = vcc.First().OrderId;
                    View.DealName = vcc.First().EventName;
                    foreach (var c in vcc)
                    {
                        if (!string.IsNullOrEmpty(c.SequenceNumber))
                        {
                            View.CouponList += "#" + c.SequenceNumber + " ";
                        }
                    }
                }
            }

            View.isleagl = isleagl;
            return true;
        }
    }
}

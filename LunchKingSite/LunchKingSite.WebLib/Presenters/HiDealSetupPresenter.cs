﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealSetupPresenter : Presenter<IHiDealSetupView>
    {
        protected IHiDealProvider _hp;
        protected ISystemProvider _sp;
        protected ISysConfProvider _cp;
        protected IHumanProvider _humanp;
        protected ISellerProvider _sellerp;

        public event EventHandler<DataEventArgs<int>> DealSaved = delegate { };

        public HiDealSetupPresenter()
        {
            GetProviders();
        }

        protected virtual void GetProviders()
        {
            _hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            _cp = ProviderFactory.Instance().GetConfig();
            _humanp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            _sellerp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        }

        public override bool OnViewInitialized()
        {
            if (View.Did == 0 && View.Sid != Guid.Empty) //新建檔次
            {
                View.StoreSettings = _sellerp.GetInitialSellerBranchStores(View.Sid);
                View.IsVerifyByPad = true; //新檔預設上線核銷
            }

            if (View.Did != 0)   //修改檔次
            {
                #region 載入檔次資料

                HiDealEntity dealEntity = HiDealEntity.GetByDealId(View.Did);
                FillViewData(dealEntity);

                #endregion 載入檔次資料

                #region 商品設定

                List<Product> prodList = new List<Product>();
                foreach (HiDealProduct p in dealEntity.Products)
                {
                    prodList.Add(new Product { Id = p.Id, IsShow = p.IsOnline.HasValue ? p.IsOnline.Value : false, Name = p.Name, ProdGuid = p.Guid, Seq = p.Seq.HasValue ? p.Seq.Value : 0 });
                }
                View.Products = prodList;

                #endregion 商品設定
            }
            return base.OnViewInitialized();
        }

        private void FillViewData(HiDealEntity dealEntity)
        {
            View.Did = dealEntity.Deal.Id;
            View.DealName = dealEntity.Deal.Name;
            View.DealPromoName = dealEntity.Deal.PromoLongDesc;
            View.DealPromoShortName = dealEntity.Deal.PromoShortDesc;
            View.DealGuid = dealEntity.Deal.HiDealGuid;
            Dictionary<int, string> regions = new Dictionary<int, string>();
            foreach (HiDealRegion region in dealEntity.DealRegion)
            {
                regions.Add(region.CodeId, region.CodeName);
            }
            View.DealRegions = regions;
            List<int> categories = new List<int>();
            foreach (HiDealCategory catg in dealEntity.DealCategory)
            {
                categories.Add(catg.CategoryCodeId);
            }
            View.DealCategories = categories;
            List<int> discounts = new List<int>();
            foreach (HiDealSpecialDiscount discount in dealEntity.DealDiscount)
            {
                discounts.Add(discount.CodeId);
            }
            View.DealDiscounts = discounts;
            View.IsOpen = dealEntity.Deal.IsOpen;
            View.IsAlwaysMain = dealEntity.Deal.IsAlwaysMain;
            View.DealStartDate = dealEntity.Deal.DealStartTime.HasValue ? dealEntity.Deal.DealStartTime.Value.ToString("yyyy/MM/dd") : string.Empty;
            View.DealStartTime = dealEntity.Deal.DealStartTime.HasValue ? dealEntity.Deal.DealStartTime.Value.ToString("HH:mm") : string.Empty;
            View.DealEndDate = dealEntity.Deal.DealEndTime.HasValue ? dealEntity.Deal.DealEndTime.Value.ToString("yyyy/MM/dd") : string.Empty;
            View.DealEndTime = dealEntity.Deal.DealEndTime.HasValue ? dealEntity.Deal.DealEndTime.Value.ToString("HH:mm") : string.Empty;
            View.TagXml = DealContentsToEncodedXml(dealEntity.DealContents);
            View.IsVerifyByList = dealEntity.Deal.IsVerifyByList;
            View.IsVerifyByPad = (dealEntity.Deal.IsVerifyByList || dealEntity.Deal.IsVerifyByList) ? dealEntity.Deal.IsVerifyByPad : true;
            View.SalesName = dealEntity.Deal.Sales;
            View.SalesDepartment = new KeyValuePair<string, string>(dealEntity.Deal.SalesDeptId, dealEntity.Deal.SalesDept);
            View.SalesAccountableBU = new KeyValuePair<string, string>(dealEntity.Deal.SalesGroupId.HasValue ? dealEntity.Deal.SalesGroupId.Value.ToString() : string.Empty, dealEntity.Deal.SalesGroupName);
            View.SalesBelongCity = new KeyValuePair<string, string>(dealEntity.Deal.SalesLocationId.HasValue ? dealEntity.Deal.SalesLocationId.Value.ToString() : string.Empty, dealEntity.Deal.SalesLocation);

            if (dealEntity.Deal.SalesCatgCodeId != null)
            {
                SystemCode parentSc = _sp.ParentSystemCodeGetByCodeGroupId(SystemCodeGroup.DealType.ToString(), dealEntity.Deal.SalesCatgCodeId.Value);
                View.SalesCatg1 = new KeyValuePair<string, string>(parentSc.CodeId.ToString(), parentSc.CodeName.ToString());
                View.BuildDdlSalesCatg2(dealEntity.Deal.SalesCatgCodeId.Value, _sp.SystemCodeGetListByParentId(parentSc.CodeId));
                View.SalesCatg2 = new KeyValuePair<string, string>(dealEntity.Deal.SalesCatgCodeId.HasValue ? dealEntity.Deal.SalesCatgCodeId.Value.ToString() : string.Empty, dealEntity.Deal.SalesCatgCodeName);
                View.HdSalesCatg2 = new KeyValuePair<string, string>(dealEntity.Deal.SalesCatgCodeId.HasValue ? dealEntity.Deal.SalesCatgCodeId.Value.ToString() : string.Empty, dealEntity.Deal.SalesCatgCodeName);
            }

            View.StoreSettings = _hp.HiDealDealStoreWithStoreInformation(View.Did);

            View.DealModifyTime = dealEntity.Deal.ModifyTime == null ? DateTime.MinValue : dealEntity.Deal.ModifyTime.Value;
            if (!string.IsNullOrEmpty(dealEntity.Deal.PrimaryBigPicture))
            {
                View.BigCandidatePictureUrl = "~/media/" + dealEntity.Deal.PrimaryBigPicture;
                View.BigPictureUrl = View.BigCandidatePictureUrl;
            }

            if (!string.IsNullOrEmpty(dealEntity.Deal.Picture))
            {
                List<string> smallPictureList = new List<string>(dealEntity.Deal.Picture.Split(','));
                for (int i = 0; i < smallPictureList.Count; i++)
                {
                    smallPictureList[i] = smallPictureList[i].Insert(0, "~/media/");
                }
                View.SmallPictureUrls = smallPictureList;
            }

            if (!string.IsNullOrEmpty(dealEntity.Deal.PrimarySmallPicture))
            {
                View.SelectedPrimarySmallPictureUrl = "~/media/" + dealEntity.Deal.PrimarySmallPicture.Replace("PrimarySmallPicture", "SecondaryPictures");
            }
        }

        private string DealContentsToEncodedXml(HiDealContentCollection hiDealContentCollection)
        {
            XmlDocument dom = new XmlDocument();
            dom.LoadXml("<Tags/>");
            XmlElement root = dom.DocumentElement;

            try
            {
                var prodDescContents = hiDealContentCollection.Single(cont => cont.Seq == 1);
                XmlElement prodDescElem = dom.CreateElement("ProdDesc");
                prodDescElem.SetAttribute("title", prodDescContents.Title);
                prodDescElem.AppendChild(dom.CreateTextNode(prodDescContents.Context.Replace("[", "{91}").Replace("]", "{93}")));
                root.AppendChild(prodDescElem);
            }
            catch (Exception)
            {
                // hiDealContentCollection.Single 失敗時
                XmlElement prodDescElem = dom.CreateElement("ProdDesc");
                prodDescElem.SetAttribute("title", "產品介紹");
                root.AppendChild(prodDescElem);
            }

            try
            {
                var optContents = hiDealContentCollection.Where(cont => cont.Seq > 1 && cont.Seq < 5).OrderBy(cont => cont.Seq);
                foreach (HiDealContent opt in optContents)
                {
                    XmlElement optElem = dom.CreateElement("OptTag");
                    optElem.SetAttribute("title", opt.Title);
                    if (opt.IsShow.HasValue)
                    {
                        optElem.SetAttribute("isNotVisible", opt.IsShow.Value ? "false" : "true");
                    }
                    else
                    {
                        optElem.SetAttribute("isNotVisible", "false");
                    }
                    optElem.AppendChild(dom.CreateTextNode(opt.Context.Replace("[", "{91}").Replace("]", "{93}")));
                    root.AppendChild(optElem);
                }
            }
            catch (Exception)
            {
                //hiDealContentCollection.Where 失敗時, do nothing
            }

            try
            {
                var termsOfUseContent = hiDealContentCollection.Single(cont => cont.Seq == 5);

                XmlElement termsOfUseElem = dom.CreateElement("TermsOfUse");
                termsOfUseElem.AppendChild(dom.CreateTextNode(termsOfUseContent.Context.Replace("[", "{91}").Replace("]", "{93}")));
                root.AppendChild(termsOfUseElem);
            }
            catch (Exception)
            {
                // hiDealContentCollection.Single 失敗時
                XmlElement termsOfUseElem = dom.CreateElement("TermsOfUse");
                root.AppendChild(termsOfUseElem);
            }

            StringWriter sw = new StringWriter();
            XmlTextWriter tw = new XmlTextWriter(sw);
            dom.WriteTo(tw);
            string s = sw.ToString();
            s = s.Replace('<', '[');
            s = s.Replace('>', ']');
            return s;
        }

        public override bool OnViewLoaded()
        {
            View.SaveDeal += OnSaveDeal;
            View.AddNewProduct += OnAddNewProduct;
            View.UploadBigPicture += OnUploadBigPicture;
            View.UploadSmallPicture += OnUploadSmallPicture;
            View.GenCoupon += OnGenCoupon;

            SetSalesmanNameArray();
            OnShowCouponList();
            return base.OnViewLoaded();
        }

        protected void SetSalesmanNameArray()
        {
            var empCollection = _humanp.EmployeeCollectionGetByDepartment(EmployeeDept.S000);
            View.SalesmanNameArray = empCollection.Select(emp => emp.EmpName).ToArray();
        }

        protected void OnSaveDeal(object sender, EventArgs e)
        {
            if (View.Sid == Guid.Empty && View.Did == 0)
            {
                return;
            }

            //檢查輸入的時間格式是否合法
            Regex regex = new Regex(@"([0-1][0-9]|2[0-3])\:[0-5][0-9]");
            if ((!regex.IsMatch(View.DealStartTime)) || (!regex.IsMatch(View.DealEndTime)))
            {
                View.ShowMessage("購買時間格式錯誤。");
                return;
            }

            //未結檔期間須判斷該業務是否存在
            DateTime dealEndTime = DateTime.Parse(View.DealEndDate + " " + View.DealEndTime);
            if (dealEndTime >= DateTime.Now)
            {
                IEnumerable<Employee> empCollection = _humanp.EmployeeCollectionGetByDepartment(EmployeeDept.S000).Where(x => x.EmpName.Equals(View.SalesName));
                if (empCollection.Count() < 1)
                {
                    View.ShowMessage("查無此業務");
                    return;
                }

                if (empCollection.Count() > 1)
                {
                    View.ShowMessage("重覆的業務姓名");
                    return;
                }
            }

            DateTime processTime = DateTime.Now;
            HiDealEntity deal;

            if (View.Sid != Guid.Empty)
            {
                deal = HiDealEntity.Create(View.Sid);
                deal.Deal.HiDealGuid = Guid.NewGuid();
                deal.Deal.SellerGuid = View.Sid;
                deal.Deal.CreateTime = processTime;
                deal.Deal.CreateId = View.UserName;
            }
            else
            {
                deal = HiDealEntity.GetByDealId(View.Did);
                deal.Deal.ModifyTime = processTime;
                deal.Deal.ModifyId = View.UserName;
            }

            deal.Deal.Name = View.DealName;
            deal.Deal.PromoLongDesc = View.DealPromoName;
            deal.Deal.PromoShortDesc = View.DealPromoShortName;
            deal.Deal.IsOpen = View.IsOpen;
            deal.Deal.IsAlwaysMain = View.IsAlwaysMain;
            if (!string.IsNullOrEmpty(View.DealStartDate) && !string.IsNullOrEmpty(View.DealEndDate))
            {
                deal.Deal.DealStartTime = DateTime.Parse(View.DealStartDate + " " + View.DealStartTime);
                deal.Deal.DealEndTime = DateTime.Parse(View.DealEndDate + " " + View.DealEndTime);
            }
            deal.Deal.Cities = string.Join(",", View.DealRegions.Values.ToArray());

            deal.Deal.IsVerifyByList = View.IsVerifyByList;
            deal.Deal.IsVerifyByPad = View.IsVerifyByPad;
            deal.Deal.Sales = View.SalesName;

            if (View.SalesDepartment.Key != "-1")
            {
                deal.Deal.SalesDeptId = View.SalesDepartment.Key;
                deal.Deal.SalesDept = View.SalesDepartment.Value;
            }
            else
            {
                deal.Deal.SalesDeptId = null;
                deal.Deal.SalesDept = null;
            }

            if (View.SalesBelongCity.Key != "-1")
            {
                deal.Deal.SalesLocationId = int.Parse(View.SalesBelongCity.Key);
                deal.Deal.SalesLocation = View.SalesBelongCity.Value;
            }
            else
            {
                deal.Deal.SalesLocationId = null;
                deal.Deal.SalesLocation = null;
            }

            if (View.SalesAccountableBU.Key != "-1")
            {
                deal.Deal.SalesGroupId = int.Parse(View.SalesAccountableBU.Key);
                deal.Deal.SalesGroupName = View.SalesAccountableBU.Value;
            }
            else
            {
                deal.Deal.SalesGroupId = null;
                deal.Deal.SalesGroupName = null;
            }

            if (View.SalesCatg2.Key != string.Empty)
            {
                deal.Deal.SalesCatgCodeId = int.Parse(View.SalesCatg2.Key);
                deal.Deal.SalesCatgCodeName = View.SalesCatg2.Value;
                deal.Deal.SalesCatgCodeGroup = "DealType";
            }
            else
            {
                deal.Deal.SalesCatgCodeId = null;
                deal.Deal.SalesCatgCodeName = null;
                deal.Deal.SalesCatgCodeGroup = null;
            }

            deal.CategoryIds = View.DealCategories;  //需要增加 field 判斷 dirty
            deal.DiscountIds = View.DealDiscounts;
            if (View.DealDiscounts.Contains((int)HiDealSpecialDiscountType.VisaPrivate))
            {
                deal.Deal.IsVisa = true;
            }
            else
            {
                deal.Deal.IsVisa = false;
            }

            string xml = View.TagXml;
            if (!string.IsNullOrEmpty(xml)) //已 sid 進入頁面建立新檔次時, 頁籤區域不會顯示 => 沒有 xml 傳進來
            {
                xml = xml.Replace('[', '<').Replace(']', '>').Replace("{91}", "[").Replace("{93}", "]");
                XmlDocument dom = new XmlDocument();
                dom.LoadXml(xml);
                XmlElement root = dom.DocumentElement; //<Tags>

                #region 產品介紹

                XmlNodeList prodDescList = root.GetElementsByTagName("ProdDesc");
                foreach (XmlNode prodDesc in prodDescList)
                {
                    XmlElement prodDescElem = prodDesc as XmlElement;
                    HiDealContent content;
                    try
                    {
                        content = deal.DealContents.Single(cont => cont.Seq == 1);
                        content.ModifyId = View.UserName;
                        content.ModifyTime = DateTime.Now;
                    }
                    catch (Exception)
                    {
                        content = new HiDealContent
                        {
                            RefType = 0,
                            RefId = deal.Deal.Id,
                            Seq = 1,
                            CreateId = View.UserName,
                            CreateTime = DateTime.Now,
                            IsShow = true
                        };
                    }
                    if (content.Title != prodDescElem.GetAttribute("title") || content.Context != prodDescElem.InnerText)
                    {
                        content.Title = prodDescElem.GetAttribute("title");
                        content.Context = prodDescElem.InnerText;
                        _hp.HiDealContentSet(content);
                    }
                }

                #endregion 產品介紹

                #region 其他頁籤

                List<HiDealContent> optTagContents = deal.DealContents.Where(cont => cont.Seq > 1 && cont.Seq < 5).OrderBy(cont => cont.Seq).ToList();
                int optTagCount = optTagContents.Count;

                XmlNodeList tagList = root.GetElementsByTagName("OptTag");

                int idx = 0;
                foreach (XmlNode tag in tagList)
                {
                    idx++; // 1 ~ 3
                    XmlElement optTagElem = tag as XmlElement;
                    HiDealContent content;
                    if (idx <= optTagCount)
                    {
                        content = optTagContents[idx - 1];
                        content.ModifyId = View.UserName;
                        content.ModifyTime = DateTime.Now;
                    }
                    else
                    {
                        content = new HiDealContent
                        {
                            RefType = 0,
                            RefId = deal.Deal.Id,
                            Seq = idx + 1,
                            CreateId = View.UserName,
                            CreateTime = DateTime.Now
                        };
                    }

                    if (content.IsShow.HasValue) //只有 new HiDealContent 才不會進到這裡面
                    {
                        if (content.Title != optTagElem.GetAttribute("title") || content.Context != optTagElem.InnerText ||
                            content.IsShow.Value != (optTagElem.GetAttribute("isNotVisible") == "false") ||
                            content.Seq != idx + 1)
                        {
                            content.Title = optTagElem.GetAttribute("title");
                            content.Context = optTagElem.InnerText;
                            content.IsShow = optTagElem.GetAttribute("isNotVisible") == "false";
                            content.Seq = idx + 1;
                            _hp.HiDealContentSet(content);
                        }
                    }
                    else
                    {
                        content.Title = optTagElem.GetAttribute("title");
                        content.Context = optTagElem.InnerText;
                        content.IsShow = optTagElem.GetAttribute("isNotVisible") == "false";
                        content.Seq = idx + 1;
                        _hp.HiDealContentSet(content);
                    }
                }

                #endregion 其他頁籤

                #region 權益說明

                XmlNodeList termsOfUseList = root.GetElementsByTagName("TermsOfUse");
                foreach (XmlNode termsOfUse in termsOfUseList)
                {
                    XmlElement termsOfUseElem = termsOfUse as XmlElement;
                    HiDealContent content;
                    try
                    {
                        content = deal.DealContents.Single(cont => cont.Seq == 5);
                        content.ModifyId = View.UserName;
                        content.ModifyTime = DateTime.Now;
                    }
                    catch (Exception)
                    {
                        content = new HiDealContent
                        {
                            RefType = 0,
                            RefId = deal.Deal.Id,
                            Seq = 5,
                            CreateId = View.UserName,
                            CreateTime = DateTime.Now,
                            IsShow = true
                        };
                    }

                    if (content.Context != termsOfUseElem.InnerText)
                    {
                        content.Title = "權益說明";
                        content.Context = termsOfUseElem.InnerText;
                        _hp.HiDealContentSet(content);
                    }
                }

                #endregion 權益說明
            }

            List<DealBranchStoreSetting> store = new List<DealBranchStoreSetting>();

            string[] stores = View.OrderedStoreSettings.Split(';');
            foreach (string s in stores)
            {
                if (string.IsNullOrEmpty(s))
                {
                    break;
                }

                List<string> storeValues = new List<string>(s.Split(','));
                DealBranchStoreSetting setting = new DealBranchStoreSetting
                {
                    StoreGuid = Guid.Parse(storeValues[0]),
                    Sequence = int.Parse(storeValues[1]),
                    UseTime = storeValues[2]
                };
                store.Add(setting);
            }
            deal.BranchStoreSettings = store;

            #region 首頁大圖

            //save

            //顯示上傳的照片，尚未儲存的狀態
            if (!string.IsNullOrEmpty(View.BigCandidatePictureUrl))
            {
                string dbFilePath = View.BigCandidatePictureUrl.Replace("~/media/", string.Empty);
                string dir = deal.GetMediaRelativePictureDirPath(true, false, false);
                string bigPicture = Path.GetFileName(View.BigPictureUrl);
                string candidatePicture = Path.GetFileName(View.BigCandidatePictureUrl);
                if (View.BigPictureUrl != View.BigCandidatePictureUrl)
                {
                    ImageUtility.DeleteFilesAndChangeFileInDiectory(dir, bigPicture, candidatePicture);
                }
                deal.Deal.PrimaryBigPicture = dir + "HidealBigPic" + Path.GetExtension(candidatePicture);

                //複製一份給web首頁換圖用，避免Cache住
                ImageFacade.CopyFile(UploadFileType.HiDealPrimaryBigPhoto, dir, "HidealBigPic" + Path.GetExtension(candidatePicture), dir, "HidealBigPic_" + deal.Deal.ModifyTime.Value.ToString("yyyyMMddHHmmss") + Path.GetExtension(candidatePicture));
                //刪除多餘的圖檔

                #region 刪除多餘的圖檔

                List<string> usefulFileName = new List<string>();
                //要保留的圖檔(保險)
                usefulFileName.Add("HidealBigPic" + Path.GetExtension(bigPicture));
                usefulFileName.Add("HidealBigPic" + Path.GetExtension(candidatePicture));
                usefulFileName.Add("HidealBigPic_" + deal.Deal.ModifyTime.GetValueOrDefault().ToString("yyyyMMddHHmmss") + Path.GetExtension(candidatePicture));
                ImageUtility.DeleteFilesInDirectoryExceptSpecifiedFileNames(dir, usefulFileName);

                #endregion 刪除多餘的圖檔
            }

            #endregion 首頁大圖

            #region 首頁輪播圖

            if (View.SmallPictureUrls.Count > 0)
            {
                List<string> pics = View.SmallPictureUrls;
                for (int i = 0; i < pics.Count; i++)
                {
                    pics[i] = pics[i].Replace("~/media/", string.Empty);
                }
                deal.Deal.Picture = string.Join(",", pics.ToArray());
                string dir = deal.GetMediaRelativePictureDirPath(false, false, true);
                List<string> exclusionList = new List<string>();
                foreach (string url in View.SmallPictureUrls)
                {
                    exclusionList.Add(Path.GetFileName(url));
                }
                ImageUtility.DeleteFilesInDirectoryExceptSpecifiedFileNames(dir, exclusionList);
            }

            #endregion 首頁輪播圖

            #region 首頁小圖

            if (!string.IsNullOrEmpty(View.SelectedPrimarySmallPictureUrl))
            {
                deal.Deal.PrimarySmallPicture = View.SelectedPrimarySmallPictureUrl.Replace("~/media/", string.Empty).Replace("SecondaryPictures", "PrimarySmallPicture");

                string dir = deal.GetMediaRelativePictureDirPath(false, false, true);
                string moveToDir = deal.GetMediaRelativePictureDirPath(false, true, false);
                string fileName = Path.GetFileName(View.SelectedPrimarySmallPictureUrl);
                ImageUtility.DeleteFilesInDirectory(moveToDir);
                ImageFacade.CopyFile(UploadFileType.HiDealPrimarySmallPhoto, dir, fileName, moveToDir, fileName);
            }

            #endregion 首頁小圖

            deal.Save();

            #region 修改商品次序

            if (View.Products.Count > 0)
            {
                for (int i = 0; i < View.Products.Count; i++)
                {
                    #region 修改商品次序

                    HiDealProduct dealProd = _hp.HiDealProductGet(View.Products[i].Id);
                    dealProd.ModifyId = View.UserName;
                    dealProd.ModifyTime = processTime;
                    dealProd.Seq = i + 1;
                    _hp.HiDealProductSet(dealProd);

                    #endregion 修改商品次序
                }
            }

            #endregion 修改商品次序

            //清暫存
            HiDealDealManager.ReloadHiDealDealGetById(deal.Deal.Id);

            DealSaved(this, new DataEventArgs<int>(deal.Deal.Id));
        }

        protected void OnAddNewProduct(object sender, DataEventArgs<Product> e)
        {
            HiDealDeal hideal = _hp.HiDealDealGet(View.Did);
            HiDealProduct dealProd = new HiDealProduct
            {
                CreateId = View.UserName,
                CreateTime = DateTime.Now,
                DealId = View.Did,
                Guid = Guid.NewGuid(),
                SellerGuid = hideal.SellerGuid,
                Seq = e.Data.Seq,
                IsOnline = true,
                Name = e.Data.Name,
                //開立發票註記預設值為true
                IsInvoiceCreate = true,
                PayToCompany = true,
                IsShowPriceDiscount = true,
                VendorBillingModel = (int)VendorBillingModel.BalanceSheetSystem,
                RemittanceType = (int)RemittanceType.ManualWeekly,
                VendorReceiptType = (int)VendorReceiptType.Invoice
            };
            _hp.HiDealProductSet(dealProd);

            HiDealProductCollection hidealproductcol = _hp.HiDealProductCollectionGet(View.Did);
            List<Product> prodList = new List<Product>();
            foreach (HiDealProduct p in hidealproductcol)
            {
                prodList.Add(new Product { Id = p.Id, IsShow = p.IsOnline.HasValue ? p.IsOnline.Value : false, Name = p.Name, ProdGuid = p.Guid, Seq = p.Seq.HasValue ? p.Seq.Value : 0 });
            }
            View.Products = prodList;
        }

        protected void OnUploadBigPicture(object sender, EventArgs e)
        {
            if (View.BigPictureFileUpload.HasFile)
            {
                HiDealEntity deal = HiDealEntity.GetByDealId(View.Did);
                string dirPath = deal.GetMediaRelativePictureDirPath(true, false, false);
                //建立:預備圖檔
                View.DealModifyTime = deal.Deal.ModifyTime == null ? DateTime.MinValue : deal.Deal.ModifyTime.Value;
                string hidealBigCandidatePicture = "HidealBigCandidatePicture" + Path.GetExtension(View.BigPictureFileUpload.FileName);
                ImageUtility.UploadFile(View.BigPictureFileUpload.PostedFile.ToAdapter(), UploadFileType.HiDealPrimaryBigPhoto, dirPath, hidealBigCandidatePicture);
                ImageFacade.CopyFile(UploadFileType.HiDealPrimaryBigPhoto, dirPath, hidealBigCandidatePicture, dirPath, "HidealBigCandidatePicture_" + View.DealModifyTime.ToString("yyyyMMddHHmmss") + Path.GetExtension(hidealBigCandidatePicture));
                View.BigCandidatePictureUrl = "~/media/" + dirPath + "HidealBigCandidatePicture_" + View.DealModifyTime.ToString("yyyyMMddHHmmss") + Path.GetExtension(hidealBigCandidatePicture);
            }
        }

        protected void OnUploadSmallPicture(object sender, EventArgs e)
        {
            HiDealEntity deal = HiDealEntity.GetByDealId(View.Did);
            List<string> newUrls = View.SmallPictureUrls;
            foreach (HttpPostedFile postFile in View.SmallPictureFileUpload)
            {
                string dirPath = deal.GetMediaRelativePictureDirPath(false, false, true);
                ImageUtility.UploadFile(postFile.ToAdapter(), UploadFileType.HiDealSecondaryPhoto, dirPath, postFile.FileName);
                newUrls.Add("~/media/" + dirPath + postFile.FileName);
            }
            View.SmallPictureUrls = newUrls;
        }

        protected void OnGenCoupon(object sender, EventArgs e)
        {
            HiDealCouponManager.GenerateHiDealCoupon(View.Did, View.UserName);
            OnShowCouponList();
        }

        protected void OnShowCouponList()
        {
            var storeCouponTotalList = GetViewHiDealCouponStoreListCount(View.Did);
            View.ShowCouponList(storeCouponTotalList);

            //提示目前產出憑證與現在設定比對不符的警示訊息

            HiDealProductCollection dealproducts = _hp.HiDealProductCollectionGet(View.Did);

            #region 產出憑證與現在設定比對

            HiDealCouponCountType hiDealCouponCountType = HiDealCouponCountType.GenateSettingMattch;

            foreach (var dealproduct in dealproducts)
            {
                //有憑證資訊，需判斷現行設定
                if ((storeCouponTotalList.Where(x => x.ProductId == dealproduct.Id)).Any())
                {
                    foreach (var storeSubTotal in storeCouponTotalList)
                    {
                        if (storeSubTotal.CouponAmountSetting.GetValueOrDefault() != storeSubTotal.CouponAmount.GetValueOrDefault())
                        {
                            if (storeSubTotal.CouponAmount.GetValueOrDefault() > storeSubTotal.CouponAmountSetting.GetValueOrDefault())
                            {
                                hiDealCouponCountType = HiDealCouponCountType.HaveRefundFormCoupon;
                                break;
                            }
                            else
                            {
                                hiDealCouponCountType = HiDealCouponCountType.GenateCouponSettingNotMattch;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    //有檔次產品設定，但尚無任何憑證產生
                    hiDealCouponCountType = HiDealCouponCountType.GenateCouponSettingNotMattch;
                    break;
                }
            }


            #endregion 產出憑證與現在設定比對

            View.ShowCouponNotMatchSettingMessage(hiDealCouponCountType);
        }

        public ViewHiDealCouponStoreListCountCollection GetViewHiDealCouponStoreListCount(int DealId)
        {
            return _hp.GetViewHiDealCouponStoreListCount(DealId);
        }

        public SystemCodeCollection GetDealRegionsForControl()
        {
            return _sp.SystemCodeGetListByCodeGroup("HiDealRegion");
        }

        public SystemCodeCollection GetDealCatgForControl()
        {
            return _sp.SystemCodeHiDealCategoryGetList();
        }

        public SystemCodeCollection GetSecialDiscountsForControl()
        {
            return _sp.SystemCodeGetListByCodeGroup("HiDealSpecialDiscount");
        }

        public DepartmentCollection GetSalesDepartment()
        {
            return _humanp.DepartmentGetListByEnabled(true);
        }

        public AccBusinessGroupCollection GetSalesAccBU()
        {
            return _sp.AccBusinessGroupGetList();
        }

        public SystemCodeCollection GetSalesCatg()
        {
            return _sp.SystemCodeGetListByCodeGroup("DealType");
        }
    }
}
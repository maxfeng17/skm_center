﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Views;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Component;
using System.Web.UI.WebControls;
using System.IO;

namespace LunchKingSite.WebLib.Presenters
{
    public class GiftBannerPresenter : Presenter<IGiftBannerView>
    {
        private IMGMProvider mgp;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.SetMgmHeaderList(GetMgmGiftHeaderList(1));
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Search += OnSearch;
            View.Save += OnSave;
            View.MgmGiftHeaderListGet += OnMgmGiftHeaderListGet;
            View.MgmGiftHeaderListGetCount += OnMgmGiftHeaderListGetCount;
            View.MgmGiftHeaderListPageChanged += OnGiftHeaderListPageChanged;
            View.MgmGiftHeaderListDelete += OnMgmGiftHeaderListDelete;
            return true;
        }
        protected void OnSearch(object sender, EventArgs e)
        {
            View.SetMgmHeaderList(GetMgmGiftHeaderList(1));
        }

        public GiftBannerPresenter(IMGMProvider mgmProv)
        {
            mgp = mgmProv;
        }

        protected void OnMgmGiftHeaderListGetCount(object sender, DataEventArgs<int> e)
        {
            e.Data = mgp.MgmGiftHeaderGetCount(GetFilter());
        }
        protected void OnGiftHeaderListPageChanged(object sender, DataEventArgs<int> e)
        {
            View.SetMgmHeaderList(GetMgmGiftHeaderList(e.Data));
        }

        protected void OnSave(object sender, DataEventArgs<string> e)
        {
            string[] eventArgs= e.Data.Split(',');
            int id = 0;
            string path = eventArgs[1];
            int.TryParse(eventArgs[0],out id);
            MgmGiftHeader item = mgp.MgmGiftHeaderGet(id);
            item.Title = View.TxtTitle;
            item.Content = View.Content;
            if (!string.IsNullOrEmpty(View.ImageUrl))
            {
                item.ImageUrl = View.ImageUrl;
                View.Image.PostedFile.SaveAs(path + View.Image.FileName);
            }
            item.TextColor = View.TextColor;
            item.ShowType =View.ShowType;
            item.OnLine =View.Online;

            //如果沒有資料的話
            if (!item.IsLoaded)
            {
                item.CreatedTime = DateTime.Now;
            }

            if (View.Online)
            {
                //無禮物
                if (View.ShowType == 0)
                {
                    if (View.NoGiftOn != id.ToString() && View.NoGiftOn!="0")
                    {
                        MgmGiftHeader items = mgp.MgmGiftHeaderGet(int.Parse(View.NoGiftOn));
                        items.OnLine = false;
                        mgp.MgmGiftHeaderSet(items);
                    }
                }
                else
                {
                    if (View.HasGiftOn != id.ToString() && View.HasGiftOn != "0")
                    {
                        MgmGiftHeader items = mgp.MgmGiftHeaderGet(int.Parse(View.HasGiftOn));
                        items.OnLine = false;
                        mgp.MgmGiftHeaderSet(items);
                    }
                }
            }

            mgp.MgmGiftHeaderSet(item);

            View.SetMgmHeaderList(GetMgmGiftHeaderList(1));
        }

        protected void OnMgmGiftHeaderListGet(object sender, DataEventArgs<int> e)
        {
            LoadMgmGiftHeader(e.Data);
        }

        protected void OnMgmGiftHeaderListDelete(object sender, DataEventArgs<int> e)
        {
            mgp.MgmGiftHeaderDelete(e.Data);
            View.SetMgmHeaderList(GetMgmGiftHeaderList(1));
        }

        #region Private Method
        private string[] GetFilter(int type=0)
        {
            List<string> filter = new List<string>();


            if (View.Searchbtn == 1)
            {
                if (!string.IsNullOrEmpty(View.SearchTitle))
                {
                    filter.Add(MgmGiftHeader.Columns.Title + " like %" + View.SearchTitle + "%");
                }

                if (View.SearchShowType != -1)
                {
                    filter.Add(MgmGiftHeader.Columns.ShowType + " =" + View.SearchShowType);
                }

                if (View.SearchOnline != -1)
                {
                    bool SearchOnline = false;
                    if (View.SearchOnline != 0)
                    {
                        SearchOnline = true;
                    }
                    filter.Add(MgmGiftHeader.Columns.OnLine + " =" + SearchOnline);
                }
            }

            return filter.ToArray();
        }

        private MgmGiftHeaderCollection GetMgmGiftHeaderList(int pageNumber)
        {

           return mgp.MgmGiftHeaderGetByPage(pageNumber, View.PageSize, ViewMgmGift.Columns.Id, GetFilter());

        }

        private void LoadMgmGiftHeader(int id)
        {
            View.MgmGiftHeaderEdit(mgp.MgmGiftHeaderGet(id));
        }

        #endregion
    }
}

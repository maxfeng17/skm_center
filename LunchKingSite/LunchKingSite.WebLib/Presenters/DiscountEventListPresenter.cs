﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Views;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Component;
using System.Web.UI.WebControls;
using System.IO;

namespace LunchKingSite.WebLib.Presenters
{
    public class DiscountEventListPresenter : Presenter<IDiscountEventListView>
    {
        private IOrderProvider op;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (View.Eid != 0)
            {
                LoadDiscountEvent(View.Eid);
            }
            else
            {
                View.SetEventList(GetDiscountEventList(1));
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Search += OnSearch;
            View.Save += OnSave;
            View.DiscountEventGet += OnDiscountEventGet;
            View.DiscountEventGetCount += OnDiscountEventGetCount;
            View.DiscountEventPageChanged += OnDiscountEventPageChanged;
            View.DiscountUsersImport += OnDiscountUsersImport;
            View.ImportAllMembers += OnImportAllMembers;
            View.DiscountEventCampaignDelete += OnDiscountEventCampaignDelete;
            View.UpdateCampaignExpireTime += OnUpdateCampaignExpireTime;
            return true;
        }

        public DiscountEventListPresenter(IOrderProvider odrProv)
        {
            op = odrProv;
        }

        #region event
        protected void OnSearch(object sender, EventArgs e)
        {
            View.SetEventList(GetDiscountEventList(1));
        }

        protected void OnSave(object sender, DataEventArgs<int> e)
        {
            DiscountEvent item = op.DiscountEventGet(e.Data);
            item.Name = View.EventName;
            item.StartTime = View.StartTime;
            item.EndTime = View.EndTime;
            item.Status = (int)View.Status;
            if (!item.IsLoaded)
            {
                item.CreateId = View.UserName;
                item.CreateTime = DateTime.Now;
            }
            if (View.Type == DiscountEventType.EventCode && string.IsNullOrWhiteSpace(item.EventCode))
            {
                item.EventCode = PromotionFacade.GenerateRandomCode(6);
                Random rand = new Random();
                item.Code = string.Format("{0:0000}", rand.Next() % 10000);
            }
            else if (View.Type != DiscountEventType.EventCode && !string.IsNullOrWhiteSpace(item.EventCode))
            {
                item.EventCode = string.Empty;
            }
            item.Type = (int)View.Type;
            item.ModifyId = View.UserName;
            item.ModifyTime = DateTime.Now;
            op.DiscountEventSet(item);

            View.SetEventList(GetDiscountEventList(1));
        }

        protected void OnDiscountEventGet(object sender, DataEventArgs<int> e)
        {
            LoadDiscountEvent(e.Data);
        }

        protected void OnDiscountEventGetCount(object sender, DataEventArgs<int> e)
        {
            e.Data = op.DiscountEventGetCount(GetFilter());
        }
        protected void OnDiscountEventPageChanged(object sender, DataEventArgs<int> e)
        {
            View.SetEventList(GetDiscountEventList(e.Data));
        }

        protected void OnDiscountUsersImport(object sender, DataEventArgs<List<int>> e)
        {
            OrderFacade.ImportDiscountUsers(View.EventId, e.Data, View.UserName);
            LoadDiscountEvent(View.EventId);
        }

        protected void OnImportAllMembers(object sender, EventArgs e)
        {
            OrderFacade.ImportDiscountUserForAllMembers(View.EventId, View.UserName);
            LoadDiscountEvent(View.EventId);
        }

        protected void OnDiscountEventCampaignDelete(object sender, DataEventArgs<int> e)
        {
            op.DiscountEventCampaignDelete(e.Data);
            LoadDiscountEvent(View.EventId);
        }

        protected void OnUpdateCampaignExpireTime(object sender, EventArgs e)
        {
            DiscountEvent discountEvent = op.DiscountEventGet(View.EventId);
            DiscountEventCampaignCollection list = op.DiscountEventCampaignGetByEventId(View.EventId);
            foreach (DiscountEventCampaign item in list)
            {
                DiscountCampaign campaign = op.DiscountCampaignGet(item.CampaignId);
                if (campaign.IsLoaded)
                {
                    if (campaign.EndTime < discountEvent.EndTime || campaign.StartTime > discountEvent.StartTime)
                    {
                        if (campaign.EndTime < discountEvent.EndTime)
                        {
                            campaign.EndTime = discountEvent.EndTime;
                        }
                        if (campaign.StartTime > discountEvent.StartTime)
                        {
                            campaign.StartTime = discountEvent.StartTime;
                        }
                        campaign.ModifyId = MemberFacade.GetUniqueId(View.UserName);
                        campaign.ModifyTime = DateTime.Now;
                        op.DiscountCampaignSet(campaign);
                    }
                }
            }
            LoadDiscountEvent(View.EventId);
        }

        #endregion

        #region Private Method
        private string[] GetFilter()
        {
            List<string> filter = new List<string>();

            if (!string.IsNullOrWhiteSpace(View.SearchName))
            {
                filter.Add(DiscountEvent.Columns.Name + " like %" + View.SearchName + "%");
            }

            if (View.SearchEndDateS != DateTime.MinValue && View.SearchEndDateE != DateTime.MinValue)
            {
                filter.Add(DiscountEvent.Columns.EndTime + ">" + View.SearchEndDateS.ToString("yyyy-MM-dd HH:mm"));
                filter.Add(DiscountEvent.Columns.EndTime + "<=" + View.SearchEndDateE.ToString("yyyy-MM-dd HH:mm"));
            }

            if (!string.IsNullOrWhiteSpace(View.SearchEventCode))
            {
                filter.Add(DiscountEvent.Columns.EventCode + " like %" + View.SearchEventCode + "%");
            }
            filter.Add(DiscountEvent.Columns.EventType + " <> 1 ");
            return filter.ToArray();
        }

        private DiscountEventCollection GetDiscountEventList(int pageNumber)
        {
            return op.DiscountEventGetByPage(pageNumber, View.PageSize, DiscountEvent.Columns.Id, GetFilter());
        }

        private void LoadDiscountEvent(int eventId)
        {
            View.SetEventEdit(op.DiscountEventGet(eventId), op.ViewDiscountEventCampaignGetList(eventId), op.DiscountUserGetCount(eventId), op.DiscountUserGetCount(eventId, DiscountUserStatus.Sent), op.DiscountUserGetCount(eventId, DiscountUserStatus.Shortage));
        }
        #endregion
    }
}

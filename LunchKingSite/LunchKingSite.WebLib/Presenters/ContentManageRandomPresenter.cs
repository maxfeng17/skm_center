﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace LunchKingSite.WebLib.Presenters
{
    public class ContentManageRandomPresenter : Presenter<IContentManageRandomView>
    {
        private ICmsProvider cp;
        private ISystemProvider sp;

        public ContentManageRandomPresenter(ICmsProvider cmsProv, ISystemProvider sysProv)
        {
            cp = cmsProv;
            sp = sysProv;
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.SetUpCityDDL(GetPponCities(), sp.SystemCodeGetListByCodeGroup("HiDealRegion"));
            GetCmsRandomTitleNId(string.Empty);
            View.GetViewCmsRandom(cp.GetViewCmsRandomCollection(View.Type));
            View.SetUploadTypeDDL();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SaveCmsRandomContent += OnSaveCmsRandomContent;
            View.SaveCmsRandomCity += OnSaveCmsRandomCity;
            View.GetCmsRandomById += OnGetCmsRandomById;
            View.GetCmsRandomCityById += OnGetCmsRandomCityById;
            View.GetCmsRandomContentById += OnGetCmsRandomContentById;
            View.ChangeCmsRandom += OnChangeCmsRandom;
            View.UpdateCmsRandomCity += OnUpdateCmsRandomCity;
            View.GetCacheName += OnGetCacheName;
            View.ChangeRandomCmsType += OnChangeRandomCmsType;
            View.DeleteCmsRandomCity += OnDeleteCmsRandomCity;
            View.GetFileList += OnGetFileList;
            View.UploadFile += OnUploadFile;
            View.DelFile += OnDelFile;
            return true;
        }

        private static List<PponCity> GetPponCities()
        {
            List<PponCity> cities = PponCityGroup.DefaultPponCityGroup.GetPponCityForSetting();
            string[] cityUnwant = {
                                      PponCityGroup.DefaultPponCityGroup.ArriveIn24Hrs.CityCode,
                                      PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CityCode,
                                      PponCityGroup.DefaultPponCityGroup.PEZ.CityCode,
                                      PponCityGroup.DefaultPponCityGroup.Tmall.CityCode };
            return cities.Where(x => !cityUnwant.Any(y => y.Equals(x.CityCode, StringComparison.OrdinalIgnoreCase))).ToList();
        }

        private void OnDelFile(object sender, DataEventArgs<string> e)
        {
            string baseDirectoryPath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Images/"), View.UploadType);
            ImageUtility.DeleteFile(baseDirectoryPath, e.Data);
            ImageUtility.DeleteFileUNC(Path.Combine(View.UploadType, e.Data));
        }

        private void OnUploadFile(object sender, DataEventArgs<HttpPostedFile> e)
        {
            string baseDirectoryPath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Images/"), View.UploadType);
            ImageUtility.UploadFile(e.Data.ToAdapter(), baseDirectoryPath, e.Data.FileName);
            ImageUtility.UploadFileUNC(e.Data.ToAdapter(), View.UploadType, e.Data.FileName);
        }

        private void OnGetFileList(object sender, DataEventArgs<string> e)
        {
            List<ImageFile> result = ImageUtility.GetFileList(e.Data, View.UploadType);
            View.SetFileList(result);
        }

        private void OnDeleteCmsRandomCity(object sender, DataEventArgs<KeyValuePair<int, int>> e)
        {
            cp.CmsRandomCityDelete(e.Data.Key);
            View.GetCmsRandom(cp.CmsRandomContentGetById(e.Data.Value), cp.CmsRandomCityGetByPid(e.Data.Value, View.Type));
        }

        private void OnChangeRandomCmsType(object sender, EventArgs e)
        {
            View.SetUpCityDDL(GetPponCities(), sp.SystemCodeGetListByCodeGroup("HiDealRegion"));
            GetCmsRandomTitleNId(string.Empty);
            View.GetViewCmsRandom(cp.GetViewCmsRandomCollection(View.Type));
        }

        private void OnGetCacheName(object sender, EventArgs e)
        {
            View.ClearChaheByName(cp.CmsRandomContentGetById(View.ContentId).ContentName);
        }

        private void OnUpdateCmsRandomCity(object sender, DataEventArgs<CmsRandomCity> e)
        {
            e.Data.Id = View.Id;
            cp.CmsRandomCityUpdate(e.Data);
            View.Mode = "1";
            View.ReturnMessage("對應城市更新成功!!");
            View.GetViewCmsRandom(cp.GetViewCmsRandomCollection(View.Type));
        }

        private void OnGetCmsRandomCityById(object sender, EventArgs e)
        {
            View.Mode = "3";
            View.GetCmsCityById(cp.CmsRandomCityGetById(View.Id));
            View.GetCmsContentById(cp.CmsRandomContentGetById(View.ContentId).Body);
        }

        private void OnChangeCmsRandom(object sender, DataEventArgs<string> e)
        {
            GetCmsRandomTitleNId(e.Data);
        }

        private void OnSaveCmsRandomCity(object sender, DataEventArgs<KeyValuePair<CmsRandomCity, List<RandomSelectedCity>>> e)
        {
            List<RandomSelectedCity> cities = e.Data.Value;
            string returnMessage = string.Empty;

            if (cities.Any(x => x.CityId == (int)CmsSpecialCityType.All))
            {
                CmsRandomCity city = e.Data.Key.Clone();
                city.CityId = cities.First(x => x.CityId == (int)CmsSpecialCityType.All).CityId;
                city.CityName = cities.First(x => x.CityId == (int)CmsSpecialCityType.All).CityName;
                city.Ratio = cities.First(x => x.CityId == (int)CmsSpecialCityType.All).Ratio;
                cp.CmsRandomCitySet(city);
                returnMessage = "新增不分區Banner成功!!";
            }
            else
            {
                foreach (RandomSelectedCity item in cities)
                {
                    CmsRandomCity city = e.Data.Key.Clone();
                    if (item.CityId == (int)CmsSpecialCityType.CurationPage)
                    {
                        city.CityId = cities.First(x => x.CityId == (int)CmsSpecialCityType.CurationPage).CityId;
                        city.CityName = cities.First(x => x.CityId == (int)CmsSpecialCityType.CurationPage).CityName;
                        city.Ratio = cities.First(x => x.CityId == (int)CmsSpecialCityType.CurationPage).Ratio;
                    }
                    else
                    {
                        city.CityId = item.CityId;
                        city.CityName = item.CityName;
                        city.Ratio = item.Ratio;
                    }
                    cp.CmsRandomCitySet(city);
                }
                returnMessage = "新增Banner項目成功!! 共新增" + cities.Count + "筆。";
            }

            View.ReturnMessage(returnMessage);
            View.GetViewCmsRandom(cp.GetViewCmsRandomCollection(View.Type));
        }

        private void GetCmsRandomTitleNId(string ddl)
        {
            DataTable dt = cp.CmsRandomContentGetTitleNId(ddl == "ddl_TargetView" ? View.ContentName : View.ContentName2, View.Type);
            View.GetCmsRandomTitleNId(dt, ddl);
            int id;
            if (dt.Rows.Count > 0 && int.TryParse(dt.Rows[0]["Id"].ToString(), out id))
            {
                GetCmsRandomContentById(id, ddl);
            }
            else if (ddl == "ddl_TargetView")
            {
                GetCmsRandomContentById(0, ddl);
            }
            else
            {
                GetCmsRandomContentById(0, string.Empty);
            }
        }

        private void OnGetCmsRandomContentById(object sender, DataEventArgs<KeyValuePair<int, string>> e)
        {
            GetCmsRandomContentById(e.Data.Key, e.Data.Value);
        }

        private void GetCmsRandomContentById(int id, string ddl)
        {
            if (string.IsNullOrEmpty(ddl))
            {
                View.GetCmsContentById(cp.CmsRandomContentGetById(id).Body);
                View.GetCmsRandom(cp.CmsRandomContentGetById(id), cp.CmsRandomCityGetByPid(id, View.Type));
            }
            else if (ddl == "ddl_BannersView" || ddl == "ddl_TargetView")
            {
                View.GetCmsRandom(cp.CmsRandomContentGetById(id), cp.CmsRandomCityGetByPid(id, View.Type));
            }
            else if (ddl == "ddl_Banners")
            {
                View.GetCmsContentById(cp.CmsRandomContentGetById(id).Body);
            }
            else
            {
                View.GetCmsContentById(string.Empty);
            }
        }

        private void OnGetCmsRandomById(object sender, EventArgs e)
        {
            View.Mode = "2";
            View.GetCmsById(cp.CmsRandomContentGetById(View.ContentId));
        }

        public void OnSaveCmsRandomContent(object sender, DataEventArgs<CmsRandomContent> e)
        {
            if (e.Data.Type == (int)RandomCmsType.PponMasterPage || e.Data.Type == (int)RandomCmsType.PponBuyAd)
            {
                Regex rgx = new Regex(@"(http:\/\/[\w\-\.]+\.[a-zA-Z]{2,3}(?:\/\S*)?(?:[\w])+\.(?:jpg|png|gif|jpeg|bmp))");
                MatchCollection matches = rgx.Matches(e.Data.Body);
                if (matches.Count > 0)
                {
                    foreach (Match item in matches)
                    {
                        e.Data.Body = e.Data.Body.Replace(item.Value, item.Value.Replace("http://", "https://"));
                    }
                }

            }
            if (View.Mode == "1")
            {
                cp.CmsRandomContentSet(e.Data);
                View.ReturnMessage("新增Banner成功!!");
            }
            else if (View.Mode == "2")
            {
                e.Data.Id = View.ContentId;
                cp.CmsRandomContentUpdate(e.Data);
                View.ReturnMessage("修改Banner成功!!");
                View.Mode = "1";
            }
            View.GetViewCmsRandom(cp.GetViewCmsRandomCollection(View.Type));
        }
    }
}
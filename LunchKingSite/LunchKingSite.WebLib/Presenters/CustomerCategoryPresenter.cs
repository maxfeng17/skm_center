﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System.Diagnostics;

namespace LunchKingSite.WebLib.Presenters
{
    public class CustomerCategoryPresenter : Presenter<ICustomerCategoryView>
    {
        protected LunchKingSite.Core.IServiceProvider sp = ProviderFactory.Instance().GetProvider<LunchKingSite.Core.IServiceProvider>();

        public override bool OnViewInitialized()
        {

            base.OnViewInitialized();
            View.CategoryList(GetCategoryList(1));
            return true;

        }

        public override bool OnViewLoaded()
        {

            base.OnViewLoaded();
            View.AddCustomerCategory += OnAddCustomerCategory;
            View.DeleteCustomerCategory += OnDeleteCustomerCategory;
            View.GetCustomerCategoryCount += OnGetCustomerCategoryCount;
            View.CategoryPageChanged += OnCategoryPageChanged;

            return true;
        }

        #region event
        protected void OnAddCustomerCategory(object sender, EventArgs e)
        {

            if (View.Mode == "Edit")
            {
                var csc = sp.CustomerServiceCategoryGet(View.MainCategoryId);
                if (csc.IsLoaded)
                {
                    csc.CategoryName = View.CategoryName;
                    csc.IsNeedOrderId = View.IsNeedOrderId;
                    csc.IsShowFrontEnd = View.IsShowFrontEnd;
                    csc.ModifyId = View.UserId;
                    csc.ModifyTime = DateTime.Now;

                    sp.CustomerServiceCategorySet(csc);

                    //再寫入Sub Subject
                    string[] subArray = View.SubjectContent.Split(',');
                    int order = 1;

                    foreach (var item in subArray.Distinct().ToArray())
                    {
                        var entity = sp.GetCategoryByParentIdAndName(View.MainCategoryId, item);
                        if (!string.IsNullOrEmpty(item))
                        {
                            if (entity.IsLoaded)
                            {
                                entity.CategoryName = item;
                                entity.IsNeedOrderId = View.IsNeedOrderId;
                                entity.IsShowFrontEnd = View.IsShowFrontEnd;
                                entity.CategoryOrder = order;
                                entity.ModifyId = View.UserId;
                                entity.ModifyTime = DateTime.Now;
                            }
                            else
                            {
                                entity.ParentCategoryId = View.MainCategoryId;
                                entity.CategoryName = item;
                                entity.IsNeedOrderId = View.IsNeedOrderId;
                                entity.IsShowFrontEnd = View.IsShowFrontEnd;
                                entity.CategoryOrder = order;
                                entity.CreateId = View.UserId;
                                entity.CreateTime = DateTime.Now;

                            }
                            order = order + 1;
                            sp.CustomerServiceCategorySet(entity);
                        }
                    }
                }
            }
            else
            {
                //抓取接下來的CategoryOrder
                int categoryOrder = sp.GetLastOrderId() + 1;
                //先寫入Main Subject
                CustomerServiceCategory csc = new CustomerServiceCategory();
                csc.CategoryName = View.CategoryName;
                csc.IsNeedOrderId = View.IsNeedOrderId;
                csc.IsShowFrontEnd = View.IsShowFrontEnd;
                csc.CreateId = View.UserId;
                csc.CreateTime = DateTime.Now;
                csc.CategoryOrder = categoryOrder;

                sp.CustomerServiceCategorySet(csc);

                //取回最新的值
                int categoryId = sp.GetLastCategoryId();

                //再寫入Sub Subject
                string[] subArray = View.SubjectContent.Split(',');
                int order = 1;

                foreach (var item in subArray.Distinct().ToArray())
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        var entity = sp.GetCategoryByParentIdAndName(categoryId, item);

                        if (entity.IsLoaded)
                        {
                            entity.CategoryName = item;
                            entity.IsNeedOrderId = View.IsNeedOrderId;
                            entity.IsShowFrontEnd = View.IsShowFrontEnd;
                            entity.CategoryOrder = order;
                            entity.ModifyId = View.UserId;
                            entity.ModifyTime = DateTime.Now;
                        }
                        else
                        {

                            entity.ParentCategoryId = categoryId;
                            entity.CategoryName = item;
                            entity.IsNeedOrderId = View.IsNeedOrderId;
                            entity.IsShowFrontEnd = View.IsShowFrontEnd;
                            entity.CategoryOrder = order;
                            entity.CreateId = View.UserId;
                            entity.CreateTime = DateTime.Now;
                        }

                        order = order + 1;
                        sp.CustomerServiceCategorySet(entity);
                    }
                }



            }


            View.CategoryList(GetCategoryList(1));
        }
        protected void OnGetCustomerCategoryCount(object sender, DataEventArgs<int> e)
        {
            e.Data = sp.GetCategoryListCount(GetFilter());
        }

        protected void OnCategoryPageChanged(object sender, DataEventArgs<int> e)
        {
            View.CategoryList(GetCategoryList(e.Data));
        }

        protected void OnDeleteCustomerCategory(object sender, DataEventArgs<int> e)
        {
            //先刪除主類別
            sp.CustomerServiceCategoryDeleteByCategoryId(e.Data);
            //抓取主類別為刪除條件的子類別
            var entity = sp.GetCategoryListByParentId(e.Data);
            foreach (var item in entity)
            {
                sp.CustomerServiceCategoryDelete(item);
                //再刪子類別的Sample
                var cscs=sp.CustomerServiceCategorySampleGetByCategoryId(item.CategoryId);
                if (cscs.Count() > 0)
                {
                    foreach (var items in cscs)
                    {
                        sp.CustomerServiceCategorySampleDelete(items);
                    }
                }
            }
            View.CategoryList(GetCategoryList(1));
        }
        #endregion

        private CustomerServiceCategoryCollection GetCategoryList(int pageNumber)
        {
            return sp.GetCategoryListByPage(pageNumber, View.PageSize, CustomerServiceCategory.Columns.CategoryOrder, GetFilter());
        }

        private string[] GetFilter()
        {
            List<string> filter = new List<string>();

            filter.Add(CustomerServiceCategory.Columns.ParentCategoryId + " is null");

            return filter.ToArray();
        }

    }
}

using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace LunchKingSite.WebLib.Presenters
{
    public class SellerVerificationSearchPresenter : Presenter<ISellerVerificationSearchView>
    {
        #region prop

        private IPponProvider pp;
        private IVerificationProvider vp;

        #endregion prop

        public SellerVerificationSearchPresenter()
        {
            SetupProviders();
        }

        private void SetupProviders()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
        }

        public override bool OnViewInitialized()
        {
            View.SetFilterTypeDropDown(View.FilterTypes);

            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.OkButtonClick += OkButtonClick;
            return true;
        }

        #region method


        /// <summary>
        /// ���a�־P���
        /// </summary>
        private void LoadVerificationData(string column, object value)
        {
            ViewPponDeal deal = null;
            List<PponDealSalesInfo> dealSalesInfo = new List<PponDealSalesInfo>();

            if (column.Equals(ViewPponDeal.Columns.BusinessHourGuid))
            {
                deal = pp.ViewPponDealGetByBusinessHourGuid(Guid.Parse(value.ToString()));
            }
            else if (column.Equals(ViewPponDeal.Columns.UniqueId))
            {
                deal = pp.ViewPponDealGetByUniqueId(int.Parse(value.ToString()));
            }

            if (deal.IsLoaded)
            {
                VendorDealSalesCount ds = vp.GetPponVerificationSummary(new List<Guid> { deal.BusinessHourGuid }).FirstOrDefault();
                if (ds != null)
                {
                    PponDealSalesInfo info = new PponDealSalesInfo
                    {
                        MerchandiseGuid = ds.MerchandiseGuid,
                        ItemName = deal.ItemName,
                        SellerName = deal.SellerName,
                        UniqueId = deal.UniqueId.Value,
                        IsDealClose = (string.IsNullOrEmpty(deal.Slug)) ? false : (decimal.Parse(deal.Slug) >= 0),
                        UnverifiedCount = ds.UnverifiedCount,
                        VerifiedCount = ds.VerifiedCount,
                        ReturnedCount = ds.ReturnedCount,
                        BeforeDealEndReturnedCount = ds.BeforeDealEndReturnedCount,
                        ForceReturnCount = ds.ForceReturnCount,
                        ForceVerifyCount = ds.ForceVerifyCount
                    };

                    dealSalesInfo.Add(info);
                }
            }

            View.SetVerificationData(dealSalesInfo);
        }
        #endregion

        #region event
        /// <summary>
        /// �d��
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OkButtonClick(object sender, EventArgs e)
        {
            LoadVerificationData(View.FilterType, View.FilterText);
        }
        #endregion
    }
}

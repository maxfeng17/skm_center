﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;

namespace LunchKingSite.WebLib.Presenters
{
    public class AccessoryGroupresenter : Presenter<IAccessoryGroupView>
    {
        protected ILocationProvider lp;
        protected ISellerProvider sp;
        protected IItemProvider ip;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.SetAccessoryGroupList(ip.AccessoryGroupGetList(View.CurrentPage, View.PageSize, null, GetFilter()));
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.PageChanged += OnPageChanged;
            View.GetBuildingCount += OnGetAccessoryGroupCount;
            View.SearchClicked += OnGridUpdated;
            View.SetAccessoryGroup += OnSetAccessoryGroup;
            return true;
        }

        public AccessoryGroupresenter()
        {
            SetupProviders();
        }

        protected void SetupProviders()
        {
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        }

        protected void OnGetAccessoryGroupCount(object sender, DataEventArgs<int> e)
        {
            e.Data = ip.AccessoryGroupGetList(View.CurrentPage, 10000, null, GetFilter()).Count;
        }

        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data);
        }

        protected void OnSetAccessoryGroup(object sender, DataEventArgs<AccessoryGroup> e)
        {
            AccessoryGroup info;

            if (e.Data.Guid == Guid.Empty)
            {
                info = new AccessoryGroup();
                info.Guid = Helper.GetNewGuid(info);
                info.CreateId = e.Data.CreateId;
                info.CreateTime = e.Data.CreateTime;
                info.MarkNew();
            }
            else
            {
                info = ip.AccessoryGroupGet(e.Data.Guid);
                info.ModifyId = e.Data.ModifyId;
                info.ModifyTime = DateTime.Now;
                info.MarkOld();
            }

            if (info.AccessoryGroupName != e.Data.AccessoryGroupName)
            {
                info.AccessoryGroupName = e.Data.AccessoryGroupName;
            }

            if (info.IsDirty)
            {
                ip.AccessoryGroupSet(info);
            }
        }

        protected void OnGridUpdated(object sender, EventArgs e)
        {
            LoadData(1);
        }

        protected void LoadData(int PageNumber)
        {
            View.SetAccessoryGroupList(ip.AccessoryGroupGetList(PageNumber, View.PageSize, null, GetFilter()));
        }

        protected string[] GetFilter()
        {
            string[] filter = new string[2];
            if (!string.IsNullOrEmpty(View.Filter))
            {
                filter[0] = View.Filter;
                filter[0] = filter[0].Replace("*", "%");
                filter[0] = filter[0].Replace("?", "_");
                filter[0] = AccessoryGroup.Columns.AccessoryGroupName + " like %" + filter[0] + "%";
            }

            if (View.SellerGuid != Guid.Empty)
            {
                filter[1] = AccessoryGroup.Columns.SellerGuid + " = " + View.SellerGuid;
            }
            return filter;
        }
    }
}
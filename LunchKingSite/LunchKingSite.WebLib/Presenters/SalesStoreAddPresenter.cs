﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.Mongo.Services;
using MongoDB.Bson;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesStoreAddPresenter : Presenter<ISalesStoreAddView>
    {
        private IOrderProvider _ordProv;
        private SalesService _salesServ;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            GetAccountingBankInfo();
            SetStoreList();
            SetLocationDropDownList();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.AccountingBankSelectedIndexChanged += OnAccountingBankSelectedIndexChanged;
            View.EditClick += OnEditClick;
            View.SaveClicked += OnSaveClicked;
            View.DeleteClicked += OnDeleteClicked;
            return true;
        }

        #region event
        protected void OnAccountingBankSelectedIndexChanged(object sender, DataEventArgs<string> e)
        {
            GetAccountingBranchInfo(e.Data);
        }
        protected void OnEditClick(object sender, DataEventArgs<ObjectId> e)
        {
            SetSalesStoreData(e.Data);
            SetLocationDropDownList();
        }
        protected void OnSaveClicked(object sender, DataEventArgs<SalesStore> e)
        {
            BusinessOrder businessOrder = _salesServ.GetBusinessOrder(View.BusinessOrderObjectId);
            SalesStore store = businessOrder.Store.Where(x => x.Id.Equals(e.Data.Id)).FirstOrDefault();
            if (store != null)
            {
                store.BranchName = e.Data.BranchName;
                store.StoreSignCompanyID = e.Data.StoreSignCompanyID;
                store.StoreCompanyAddress = e.Data.StoreCompanyAddress;
                store.StoreTel = e.Data.StoreTel;
                store.AddressCityId = e.Data.AddressCityId;
                store.AddressTownshipId = e.Data.AddressTownshipId;
                store.StoreAddress = e.Data.StoreAddress;
                store.OpeningTime = e.Data.OpeningTime;
                store.CloseDate = e.Data.CloseDate;
                store.Quantity = e.Data.Quantity;
                store.Mrt = e.Data.Mrt;
                store.Car = e.Data.Car;
                store.Bus = e.Data.Bus;
                store.OV = e.Data.OV;
                store.WebUrl = e.Data.WebUrl;
                store.FBUrl = e.Data.FBUrl;
                store.PlurkUrl = e.Data.PlurkUrl;
                store.BlogUrl = e.Data.BlogUrl;
                store.OtherUrl = e.Data.OtherUrl;
                store.Remark = e.Data.Remark;
                store.StoreName = e.Data.StoreName;
                store.StoreBossName = e.Data.StoreBossName;
                store.StoreID = e.Data.StoreID;
                store.StoreBranchCode = e.Data.StoreBranchCode;
                store.StoreBranchDesc = e.Data.StoreBranchDesc;
                store.StoreBankCode = e.Data.StoreBankCode;
                store.StoreBankDesc = e.Data.StoreBankDesc;
                store.StoreAccount = e.Data.StoreAccount;
                store.StoreAccountName = e.Data.StoreAccountName;
                store.StoreAccountingName = e.Data.StoreAccountingName;
                store.StoreAccountingTel = e.Data.StoreAccountingTel;
                store.StoreEmail = e.Data.StoreEmail;
                store.StoreNotice = e.Data.StoreNotice;
                store.ModifyId = View.UserId;
                store.ModifyTime = DateTime.Now;
            }
            else
            {
                store = e.Data;
                store.CreateId = View.UserId;
                store.CreateTime = DateTime.Now;
                businessOrder.Store.Add(store);
            }
            _salesServ.SaveBusinessOrder(businessOrder);
            SetStoreList();
        }
        protected void OnDeleteClicked(object sender, DataEventArgs<ObjectId> e)
        {
            if (!e.Data.Equals(ObjectId.Empty))
            {
                BusinessOrder businessOrder = _salesServ.GetBusinessOrder(View.BusinessOrderObjectId);
                SalesStore store = businessOrder.Store.Where(x => x.Id.Equals(e.Data)).FirstOrDefault();
                if (store != null)
                {
                    businessOrder.Store.Remove(store);
                    _salesServ.SaveBusinessOrder(businessOrder);
                }
            }
            SetStoreList();
        }
        #endregion

        public SalesStoreAddPresenter(SalesService salesService, IOrderProvider ordProv)
        {
            _salesServ = salesService;
            _ordProv = ordProv;
        }

        #region method
        private void SetStoreList()
        {
            BusinessOrder businessOrder = _salesServ.GetBusinessOrder(View.BusinessOrderObjectId);
            View.SetStoreList(businessOrder.Store);
        }
        
        private void SetSalesStoreData(ObjectId storeId)
        {
            if (!View.BusinessOrderObjectId.Equals(ObjectId.Empty))
            {
                BusinessOrder businessOrder = _salesServ.GetBusinessOrder(View.BusinessOrderObjectId);
                if (businessOrder != null)
                {
                    if (businessOrder.Store.Count > 0)
                    {
                        SalesStore store = businessOrder.Store.Where(x => x.Id.Equals(storeId)).FirstOrDefault();
                        if (store != null)
                        {
                            GetAccountingBranchInfo(store.StoreBankCode);
                            View.SetSalesStoreData(store);
                        }
                    }
                }
            }
        }

        private void SetLocationDropDownList()
        {

            var townships = CityManager.TownShipGetListByCityId(CityManager.Citys.Where(x => x.Code != "SYS").FirstOrDefault().Id);
            if (View.AddressCityId != -1)
            {
                var nowCitys = CityManager.Citys.Where(x => x.Id == View.AddressCityId).ToList();
                if (nowCitys.Count > 0)
                {
                    townships = CityManager.TownShipGetListByCityId(nowCitys.First().Id);
                }
            }

            View.GetLocationDropDownList(CityManager.Citys.Where(x => x.Code != "SYS").ToList(), townships);

            var jsonCol = from x in CityManager.Citys.Where(x => x.Code != "SYS")
                          select new
                          {
                              id = x.Id.ToString(),
                              name = x.CityName,
                              Townships = (from y in CityManager.TownShipGetListByCityId(x.Id) select new { id = y.Id.ToString(), name = y.CityName })
                          };
            View.CityJSonData = new JsonSerializer().Serialize(jsonCol);
        }
        
        private void GetAccountingBankInfo()
        {
            View.SetAccountingBankInfo(_ordProv.BankInfoGetMainList());
        }

        private void GetAccountingBranchInfo(string bankNo)
        {
            View.SetAccountingBranchInfo(_ordProv.BankInfoGetBranchList(bankNo));
        }
        #endregion
    }
}

﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesCalendarContentPresenter : Presenter<ISalesCalendarContentView>
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private IPponProvider pp;
        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            LoadData();
            return true;
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();

            return true;
        }

        public SalesCalendarContentPresenter(IPponProvider pponProv)
        {
            pp = pponProv;
        }

        #region method
        private void LoadData()
        {
            SalesCalendarEvent cal = pp.SalesCalendarEventGet(View.Id);
            View.SetCalendarContent(cal);
        }
        #endregion
    }
}

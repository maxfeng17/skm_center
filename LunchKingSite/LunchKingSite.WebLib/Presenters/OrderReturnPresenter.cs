using System.Collections.Generic;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System;

namespace LunchKingSite.WebLib.Presenters
{
    public class OrderReturnPresenter : Presenter<IOrderReturnView>
    {
        #region members

        public const int DEFAULT_PAGE_SIZE = 15;

        protected IOrderProvider op = null;
        protected IMemberProvider mp = null;
        protected IPponProvider pp = null;

        #endregion members

        public OrderReturnPresenter()
        {
            SetupProviders();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetFilterTypes();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.GetOrderCount += OnGetOrderCount;
            View.SortClicked += OnGridUpdated;
            View.SearchClicked += OnGridUpdated;
            View.PageChanged += OnPageChanged;
            View.ExportToExcel += OnExportToExcel;
            return true;
        }
        protected void OnExportToExcel(object sender, EventArgs e)
        {
            string ddlSelection = View.GetFilterTypeDropDown();

            string orderId = ddlSelection == ViewOrderMemberBuildingSeller.Columns.OrderId ? View.SearchExpression : null;

            int? uniqueId = null;
            if (ddlSelection == ViewOrderMemberBuildingSeller.Columns.UniqueId)
            {
                int result;
                if (int.TryParse(View.SearchExpression, out result))
                {
                    uniqueId = result;
                }
            }

            string sellerId = ddlSelection == ViewOrderMemberBuildingSeller.Columns.SellerId ? View.SearchExpression : null;


            DateTime? minOrderCreateTime = View.MinOrderCreateTime;
            DateTime? maxOrderCreateTime = View.MaxOrderCreateTime;

            View.Export(op.ViewOrderMemberBuildingSellerGetListExcelForReturn(View.SortExpression, orderId, uniqueId, sellerId, minOrderCreateTime, maxOrderCreateTime, View.DateType, View.CsvType));
        }

        protected void SetupProviders()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        }

        protected void SetFilterTypes()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add(ViewOrderMemberBuildingSeller.Columns.SellerId, "商家編號");
            data.Add(ViewOrderMemberBuildingSeller.Columns.OrderId, "訂單編號");      
            data.Add(ViewOrderMemberBuildingSeller.Columns.UniqueId, "檔號");
            View.SetFilterTypeDropDown(data);
        }


        protected void OnGetOrderCount(object sender, DataEventArgs<int> e)
        {
            
            string ddlSelection = View.GetFilterTypeDropDown();

            string orderId = ddlSelection == ViewOrderMemberBuildingSeller.Columns.OrderId? View.SearchExpression: null;

            int? uniqueId = null;
            if (ddlSelection == ViewOrderMemberBuildingSeller.Columns.UniqueId)
            {
                int result;
                if(int.TryParse(View.SearchExpression, out result))
                {
                    uniqueId = result;
                }
            }

            string sellerId = ddlSelection == ViewOrderMemberBuildingSeller.Columns.SellerId ? View.SearchExpression : null;


            DateTime ? minOrderCreateTime = View.MinOrderCreateTime;
            DateTime? maxOrderCreateTime = View.MaxOrderCreateTime;
            
        
            e.Data = op.ViewOrderMemberBuildingSellerGetCountForReturn(orderId, uniqueId, sellerId, minOrderCreateTime,
                                                            maxOrderCreateTime,View.DateType,View.CsvType);
                                                            
        }

        protected void OnGridUpdated(object sender, EventArgs e)
        {
            LoadData(1);
        }

        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data);
        }

        protected void LoadData(int pageNumber)
        {
            string ddlSelection = View.GetFilterTypeDropDown();

            string orderId = ddlSelection == ViewOrderMemberBuildingSeller.Columns.OrderId ? View.SearchExpression : null;

            int? uniqueId = null;
            if (ddlSelection == ViewOrderMemberBuildingSeller.Columns.UniqueId)
            {
                int result;
                if (int.TryParse(View.SearchExpression, out result))
                {
                    uniqueId = result;
                }
            }

            string sellerId = ddlSelection == ViewOrderMemberBuildingSeller.Columns.SellerId ? View.SearchExpression : null;


            DateTime? minOrderCreateTime = View.MinOrderCreateTime;
            DateTime? maxOrderCreateTime = View.MaxOrderCreateTime;

            View.SetOrderList(op.ViewOrderMemberBuildingSellerGetListPagingForReturn(
                pageNumber, View.PageSize, View.SortExpression, orderId, uniqueId, sellerId, minOrderCreateTime, maxOrderCreateTime,View.DateType,View.CsvType));

        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class SellerListPresenter : Presenter<ISellerListView>
    {
        protected ISellerProvider sp = null;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SellerCollection sellerList = sp.SellerGetList(1, 
                View.PageSize, 
                Seller.Columns.CreateTime + " DESC", 
                Seller.Columns.SellerName + " like %" + View.Filter + "%");

            View.SetSellerList(ReSetSellerList(sellerList));
            SetFilterTypes();
            SetStatusFilter();
            return true;
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SortClicked += OnGridUpdated;
            View.PageChanged += OnPageChanged;
            View.GetDataCount += OnGetDataCount;
            View.SearchClicked += OnGridUpdated;
            return true;
        }

        public SellerListPresenter()
        {
            SetupProviders();
        }

        protected void SetupProviders()
        {
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        }

        protected void OnGetDataCount(object sender, DataEventArgs<int> e)
        {
            e.Data = sp.SellerGetCount();
        }

        protected void OnGridUpdated(object sender, EventArgs e)
        {
            LoadData(1);
        }

        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data);
        }

        protected void LoadData(int PageNumber)
        {
            string param = "";
            switch (View.FilterType)
	        {
                case "seller_name":
                    param += Seller.Columns.SellerName + " like %" + View.Filter + "%";
                    break;
                case "SignCompanyID":
                    param += Seller.Columns.SignCompanyID + " like %" + View.Filter + "%";
                    break;
                case "CompanyName":
                    param += Seller.Columns.CompanyName + " like %" + View.Filter + "%";
                    break;
	        }

            View.SetSellerList(ReSetSellerList(sp.SellerGetList(PageNumber, View.PageSize, View.SortExpression, param)));
        }

        private SellerCollection ReSetSellerList(SellerCollection SellerList)
        {
            for (int i = 0; i < SellerList.Count; i++)
                SellerList[i].SellerRemark = LocationUtility.GetCityNameFromCache(SellerList[i].CityId);
            return SellerList;
        }

        protected void SetFilterTypes() 
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add(Seller.Columns.SellerName, View.Localization.GetString("SellerName"));
            data.Add(Seller.Columns.SignCompanyID, View.Localization.GetString("SignCompanyID"));
            data.Add(Seller.Columns.CompanyName, View.Localization.GetString("CompanyName"));
            View.SetFilterTypeDropDown(data);
        }

        protected void SetStatusFilter()
        {
            Dictionary<bool, string> data = new Dictionary<bool, string>();
            data.Add(true, View.Localization.GetString("Online"));
            data.Add(false, View.Localization.GetString("Offline"));
            View.SetStatusFilter(data);
        }

        public StoreCollection GetStores(Guid sellerGuid)
        {
            return sp.StoreGetListBySellerGuid(sellerGuid) ?? new StoreCollection();
        }

        public Guid? GetParentSellerGuid(Guid sellerGuid)
        {
            var parentSeller = sp.SellerTreeGetListBySellerGuid(sellerGuid)
                                    .FirstOrDefault();

            return parentSeller != null
                    ? parentSeller.ParentSellerGuid
                    : (Guid?)null;
        }

        public string GetParentSellerName(Guid? parentGuid)
        {
            if (!parentGuid.HasValue)
            {
                return string.Empty;
            }
            var parentSeler = sp.SellerGet(parentGuid.Value);
            return string.Format("{0} {1}", parentSeler.SellerId, parentSeler.SellerName);
        }
    }
}

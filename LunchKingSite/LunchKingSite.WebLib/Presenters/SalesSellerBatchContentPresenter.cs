﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LunchKingSite.WebLib.Presenters
{
    /// <summary>
    /// 商家批次處理
    /// </summary>
    public class SalesSellerBatchContentPresenter : Presenter<ISalesSellerBatchContentView>
    {
        /// <summary>
        /// SellerProvider Interface
        /// </summary>
        private ISellerProvider sp;
        /// <summary>
        /// OrderProvider Interface
        /// </summary>
        private IOrderProvider op;
        /// <summary>
        /// HumanProvider Interface
        /// </summary>
        private IHumanProvider hp;

        /// <summary>
        /// View Initialized
        /// </summary>
        /// <returns></returns>
        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        /// <summary>
        /// View Loaded
        /// </summary>
        /// <returns></returns>
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.OnGetCount += OnOnGetCount;
            View.PageChanged += OnPageChanged;
            View.Upload += OnUpload;
            return true;
        }

        /// <summary>
        /// 建構式
        /// </summary>
        /// <param name="sellerProv">SellerProvider Interface</param>
        /// <param name="sellerProv">OrderProvider Interface</param>
        public SalesSellerBatchContentPresenter(ISellerProvider sellerProv, IOrderProvider orderProv, IHumanProvider humanProv)
        {
            sp = sellerProv;
            op = orderProv;
            hp = humanProv;
        }

        /// <summary>
        /// 發生於上傳時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnUpload(object sender, EventArgs e)
        {            
            if (string.IsNullOrEmpty(View.FUExport.PostedFile.FileName))
            {
                View.ShowMessage("請選擇匯入檔案");
                return;
            }
            
            try
            {
                HSSFWorkbook workbook = new HSSFWorkbook(View.FUExport.PostedFile.InputStream);
                Sheet currentSheet = workbook.GetSheetAt(0);
                
                Row row;
                Seller seller;
                StringBuilder errMsg = new StringBuilder(); //錯誤訊息
                decimal crowdsFlow;           //人潮流量
                int sellerLevel;              //規模等級
                Guid parentSellerGuid;        //上層公司Guid
                DateTime closeDownDate;       //結束營業日
                bool isUpdate;                //是否為更新
                int processCount = 0;         //處理筆數
                int? sellerCityId;            //商家縣市代碼
                int? sellerTownshipId;        //商家鄉鎮區代碼
                int? storeCityId;             //訂位資訊縣市代碼
                int? storeTownshipId;         //訂位資訊鄉鎮區代碼

                IEnumerable<City> citys = CityManager.Citys.Where(x => x.Code != "SYS");
                List<SellerBatchErrorData> errorList = new List<SellerBatchErrorData>();

                if (currentSheet.LastRowNum == 1)
                {
                    View.ShowMessage("匯入檔案無資料");
                    return;
                }

                using (TransactionScope scope = TransactionScopeBuilder.CreateReadCommitted())
                {
                    for (int i = 2; i <= currentSheet.LastRowNum; i++)
                    {
                        isUpdate = false;
                        seller = new Seller();
                        errMsg = errMsg.Clear();
                        row = currentSheet.GetRow(i);

                        if (row == null)
                        {
                            continue;
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(row.GetCell(0)).Trim()))
                        {
                            isUpdate = true;
                            seller = sp.SellerGet(Seller.Columns.SellerId, Convert.ToString(row.GetCell(0)).Trim());
                        }

                        #region Check Row Data
                        if (isUpdate)
                        {
                            if (!seller.IsLoaded)
                            {
                                errorList.Add(new SellerBatchErrorData()
                                {
                                    ProcessMode = "修改",
                                    SellerID = Convert.ToString(row.GetCell(0)).Trim(),
                                    SellerName = Convert.ToString(row.GetCell(1)).Trim(),
                                    ParentSellerID = Convert.ToString(row.GetCell(9)).Trim(),
                                    ErrorMessage = "查無此商家可做修改"
                                });

                                continue;
                            }

                            //判斷上層公司是否有被異動
                            Dictionary<Seller, Seller> ds = new Dictionary<Seller, Seller>();
                            var ParentTree = sp.SellerTreeGetListBySellerGuid(seller.Guid).FirstOrDefault();
                            if (ParentTree != null)
                            {
                                SellerTreeCollection stc = sp.SellerTreeGetListByParentSellerGuid(ParentTree.ParentSellerGuid);
                                Seller ParentSeller = sp.SellerGet(ParentTree.ParentSellerGuid);
                                foreach (var st in stc)
                                {
                                    ds.Add(sp.SellerGet(st.SellerGuid), ParentSeller);
                                }

                                if (ds.FirstOrDefault().Value.SellerId != Convert.ToString(row.GetCell(9)).Trim())
                                {
                                    errMsg.Append("修改狀態 [上層公司] 不可異動 <br />");
                                }
                            }
                        }

                        errMsg.Append(this.CheckBaseSellerData(Convert.ToString(row.GetCell(1)).Trim(), Convert.ToString(row.GetCell(7)).Trim(), Convert.ToString(row.GetCell(8)).Trim()));
                        errMsg.Append(this.CheckParentSeller(isUpdate, Convert.ToString(row.GetCell(9)).Trim(), Convert.ToString(row.GetCell(0)).Trim(), out parentSellerGuid));
                        errMsg.Append(this.CalculateSellerLevel(!string.IsNullOrEmpty(Convert.ToString(row.GetCell(9)).Trim()), row.GetCell(2), row.GetCell(3), row.GetCell(4), row.GetCell(5), row.GetCell(6), out crowdsFlow, out sellerLevel));
                        errMsg.Append(this.CheckSellerName(isUpdate, Convert.ToString(row.GetCell(1)).Trim()));
                        errMsg.Append(this.CheckCode(
                            Convert.ToString(row.GetCell(7)).Trim(),
                            Convert.ToString(row.GetCell(8)).Trim(),
                            Convert.ToString(row.GetCell(15)).Trim(),
                            Convert.ToString(row.GetCell(16)).Trim(),
                            Convert.ToString(row.GetCell(19)).Trim(),
                            Convert.ToString(row.GetCell(26)).Trim(),
                            Convert.ToString(row.GetCell(40)).Trim(),
                            Convert.ToString(row.GetCell(41)).Trim(),
                            Convert.ToString(row.GetCell(60)).Trim(),
                            Convert.ToString(row.GetCell(61)).Trim(),
                            Convert.ToString(row.GetCell(62)).Trim()));
                        errMsg.Append(this.CheckCompanyId("基本資料", Convert.ToString(row.GetCell(10)).Trim()));
                        errMsg.Append(this.CheckCityAndTownshipCode("基本資料", Convert.ToString(row.GetCell(11)).Trim(), Convert.ToString(row.GetCell(12)).Trim(), citys, out sellerCityId, out sellerTownshipId));
                        errMsg.Append(this.CheckOrderInfo(Convert.ToString(row.GetCell(45)).Trim(), Convert.ToString(row.GetCell(46)).Trim(), Convert.ToString(row.GetCell(47)).Trim(), Convert.ToString(row.GetCell(48)).Trim(), citys, out storeCityId, out storeTownshipId));
                        errMsg.Append(this.CheckCompanyId("財務資料", Convert.ToString(row.GetCell(37)).Trim()));
                        errMsg.Append(this.CheckBankId(Convert.ToString(row.GetCell(35)).Trim(), Convert.ToString(row.GetCell(36)).Trim()));
                        errMsg.Append((Convert.ToString(row.GetCell(16)).Trim() == "1" && (string.IsNullOrEmpty(Convert.ToString(row.GetCell(17)).Trim()) || DateTime.TryParse(Convert.ToString(row.GetCell(17)), out closeDownDate))) ? "結束營業日期為空值或者不正確 <br />" : string.Empty);

                        if (!string.IsNullOrEmpty(errMsg.ToString()))
                        {
                            errorList.Add(new SellerBatchErrorData()
                            {
                                ProcessMode = isUpdate ? "修改" : "新增",
                                SellerID = Convert.ToString(row.GetCell(0)).Trim(),
                                SellerName = Convert.ToString(row.GetCell(1)).Trim(),
                                ParentSellerID = Convert.ToString(row.GetCell(9)).Trim(),
                                ErrorMessage = errMsg.ToString().TrimEnd("<br />")
                            });

                            continue;
                        }
                        #endregion

                        #region Process Data
                        if (isUpdate)
                        {
                            Seller ori = seller.Clone();

                            seller.ModifyId = View.UserName;
                            seller.ModifyTime = DateTime.Now;

                            this.DataProcess(row, sellerLevel, crowdsFlow, sellerTownshipId, storeCityId, storeTownshipId, ref seller);
                            this.DoUpdate(seller, ori, parentSellerGuid);
                        }
                        else
                        {
                            ViewEmployee emp = hp.ViewEmployeeGet(ViewEmployee.Columns.Email, View.UserName);

                            seller.TempStatus = (int)SellerTempStatus.Applied;
                            seller.SellerId = SellerFacade.GetNewSellerID();
                            seller.SalesId = emp.IsLoaded ? emp.UserId : (int?)null;
                            seller.SellerSales = emp.IsLoaded ? emp.EmpName : string.Empty;
                            seller.CreateId = View.UserName;
                            seller.CreateTime = DateTime.Now;
                            seller.Department = (int)DepartmentTypes.Ppon;

                            this.DataProcess(row, sellerLevel, crowdsFlow, sellerTownshipId, storeCityId, storeTownshipId, ref seller);
                            this.DoInsert(seller, parentSellerGuid);
                        }
                        #endregion

                        processCount += 1;
                    }

                    View.ErrData = errorList;

                    if (errorList.Count() == 0)
                    {
                        scope.Complete();
                        View.ShowMessage(string.Format("上傳成功: 共上傳{0}筆資料", processCount));
                    }
                    else
                    {
                        LoadData(1);
                    }
                }
            }
            catch (Exception ex)
            {
                View.ShowMessage(string.Format("上傳失敗: {0}", ex.Message));
            }
        }

        /// <summary>
        /// 頁簽變更
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data);
        }

        /// <summary>
        /// 取得總筆數
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnOnGetCount(object sender, DataEventArgs<int> e)
        {
            e.Data = View.ErrData == null ? 0 : View.ErrData.Count();
        }

        /// <summary>
        /// 讀取資料
        /// </summary>
        /// <param name="pageNumber">頁數</param>
        private void LoadData(int pageNumber)
        {
            List<SellerBatchErrorData> errorList = View.ErrData.Skip(View.PageSize * (pageNumber - 1)).Take(View.PageSize).ToList();

            View.SetErrorList(errorList);
        }

        #region Data Check
        /// <summary>
        /// 商家規模計算
        /// </summary>
        /// <param name="haveParentSeller">是否有上層公司</param>
        /// <param name="sellers">店家數</param>
        /// <param name="avgPrice">平均客單價</param>
        /// <param name="avgSeat">平均座位數</param>
        /// <param name="turnoverRate">滿桌率(%)</param>
        /// <param name="turnoverFreq">翻桌次</param>
        /// <param name="crowdsFlow">人潮流量</param>
        /// <param name="sellerLevel">規模等級</param>
        /// <returns></returns>
        /// <remarks>
        /// 商家規模計算方式
        /// A:店家數, B:平均客單價, C:平均座位數, D:滿桌率(%), E:翻桌次, F:人潮流量
        /// F = C * D * 0.01 * E
        /// 規模 = A * B * F
        /// </remarks>
        private string CalculateSellerLevel(bool haveParentSeller, object sellers, object avgPrice, object avgSeat, object turnoverRate, object turnoverFreq, out decimal crowdsFlow, out int sellerLevel)
        {
            string errMsg = string.Empty;
            sellerLevel = -1;
            crowdsFlow = 0m;
            long intSellers, intAvgPrice, intAvgSeat, intTurnoverRate, intTurnoverFreq;
            decimal turnover;

            string[] cols = new string[] { "店家數", "平均客單價", "平均座位數", "滿桌率(%)", "翻桌次" };

            intSellers = this.TryParsyInt(sellers, cols[0], ref errMsg);
            intAvgPrice = this.TryParsyInt(avgPrice, cols[1], ref errMsg);
            intAvgSeat = this.TryParsyInt(avgSeat, cols[2], ref errMsg);
            intTurnoverRate = this.TryParsyInt(turnoverRate, cols[3], ref errMsg);
            intTurnoverFreq = this.TryParsyInt(turnoverFreq, cols[4], ref errMsg);

            if (!string.IsNullOrEmpty(errMsg))
            {
                errMsg = string.Format("店家規模 {0} 不可為空值或0 <br />", errMsg.Trim().TrimEnd(','));
            }

            //start caluclate
            crowdsFlow = (decimal)intAvgSeat * (decimal)intTurnoverRate * 0.01m * (decimal)intTurnoverFreq;
            turnover = (decimal)(intSellers * intAvgPrice * crowdsFlow);
            turnover = turnover / 10000m;

            if (haveParentSeller)
            {
                if (turnover >= 10000m)
                {
                    sellerLevel = (int)SellerLevel.SA;
                }
                else if (turnover >= 3000m)
                {
                    sellerLevel = (int)SellerLevel.A;
                }
                else if (turnover >= 1000m)
                {
                    sellerLevel = (int)SellerLevel.B;
                }
                else if (turnover >= 500m)
                {
                    sellerLevel = (int)SellerLevel.C;
                }
            }
            else
            {
                if (turnover >= 250m)
                {
                    sellerLevel = (int)SellerLevel.Big;
                }
                else if (turnover >= 100m)
                {
                    sellerLevel = (int)SellerLevel.Medium;
                }
                else if (turnover < 100m)
                {
                    sellerLevel = (int)SellerLevel.Small;
                }
            }

            return errMsg;
        }

        /// <summary>
        /// 檢驗數值轉換
        /// </summary>
        /// <param name="i">要檢查的數值</param>
        /// <param name="errMsg">錯誤訊息</param>
        /// <returns></returns>
        private long TryParsyInt(object i, string colName, ref string errMsg)
        {
            long returnInt;

            if (!long.TryParse(Convert.ToString(i), out returnInt) || returnInt == 0)
            {
                errMsg += string.Format("[{0}], ", colName);
            }

            return returnInt;
        }

        /// <summary>
        /// 檢查商家基本必填資料
        /// </summary>
        /// <param name="sellerName">品牌名稱</param>
        /// <param name="sellerFrom">店家來源</param>
        /// <param name="sellerProperty">店家屬性</param>
        /// <returns></returns>
        private string CheckBaseSellerData(string sellerName, string sellerFrom, string sellerProperty)
        {
            string errMsg = string.Empty;

            string[] cols = new string[] { "品牌名稱", "店家來源", "店家屬性" };

            if (string.IsNullOrEmpty(sellerName.Trim()))
            {
                errMsg += string.Format("[{0}], ", cols[0]);
            }

            if (string.IsNullOrEmpty(sellerFrom.Trim()))
            {
                errMsg += string.Format("[{0}], ", cols[1]);
            }

            if (string.IsNullOrEmpty(sellerProperty.Replace(",","").Trim()))
            {
                errMsg += string.Format("[{0}], ", cols[2]);
            }

            if (!string.IsNullOrEmpty(errMsg))
            {
                errMsg = string.Format("基本資料 {0} 不可為空值 <br />", errMsg.Trim().TrimEnd(','));
            }

            return errMsg;
        }

        /// <summary>
        /// 檢查上層公司
        /// </summary>
        /// <param name="isUpdate">是否為更新</param>
        /// <param name="parentSellerId">上層公司ID</param>
        /// <param name="SellerId">店家Id</param>
        /// <param name="parentSellGuid">上層公司Guid</param>
        /// <returns></returns>
        private string CheckParentSeller(bool isUpdate, string parentSellerId, string SellerId, out Guid parentSellGuid)
        {
            parentSellGuid = Guid.Empty;

            if (isUpdate || string.IsNullOrEmpty(parentSellerId))
            {
                return string.Empty;
            }

            if (SellerId.Equals(parentSellerId))
            {
                return "不能把自己作為 [上層公司] <br />";
            }

            Seller seller = sp.SellerGet(Seller.Columns.SellerId, parentSellerId);
            if (!seller.IsLoaded)
            {
                return "[上層公司] 不存在 <br />";
            }

            parentSellGuid = seller.Guid;
            return string.Empty;
        }

        /// <summary>
        /// 檢查統一編號/Id
        /// </summary>
        /// <param name="dataGroupDesc">資料群組名稱</param>
        /// <param name="companyId">統一編號/Id</param>
        /// <returns></returns>
        private string CheckCompanyId(string dataGroupDesc, string companyId)
        {
            string errMsg = string.Empty;
            string chkMsg = string.Empty;

            if (string.IsNullOrEmpty(companyId))
                return errMsg;

            //有填沒有勾才要檢核 
            if (companyId.Length <= 8)
            {
                chkMsg = RegExRules.CompanyNoCheck(companyId);
            }
            else
            {
                chkMsg = RegExRules.PersonalIdCheck(companyId);
            }

            if (!string.IsNullOrEmpty(chkMsg))
            {
                errMsg = string.Format("{0} {1} <br />", dataGroupDesc, chkMsg);
            }

            return errMsg;
        }

        /// <summary>
        /// 檢查商家
        /// </summary>
        /// <param name="isUpdate">是否為更新</param>
        /// <param name="sellerName">品牌名稱</param>
        /// <returns></returns>
        private string CheckSellerName(bool isUpdate, string sellerName)
        {
            if (isUpdate)
            {
                return string.Empty;
            }

            Seller seller = sp.SellerGet(Seller.Columns.SellerName, sellerName);
            if (seller.IsLoaded)
            {
                return "基本資料 [品牌名稱] 已存在 <br />";
            }

            return string.Empty;
        }

        /// <summary>
        /// 檢查代碼正確性
        /// </summary>
        /// <param name="sellerFrom">店家來源</param>
        /// <param name="sellerPropertys">店家屬性</param>
        /// <param name="storeStatus">顯示狀態</param>
        /// <param name="isCloseDown">營業狀態</param>
        /// <param name="proposalContactType1">聯絡人類別1</param>
        /// <param name="proposalContactType2">聯絡人類別2</param>
        /// <param name="remittanceType">出帳方式</param>
        /// <param name="vendorReceiptType">收據開立方式</param>
        /// <param name="creditcardAvailable">刷卡服務</param>
        /// <param name="isOpenBooking">預約管理</param>
        /// <param name="isOpenRS">開啟預約服務連結</param>
        /// <returns></returns>
        private string CheckCode(string sellerFrom, string sellerPropertys, string storeStatus, string isCloseDown, string proposalContactType1, string proposalContactType2,
            string remittanceType, string vendorReceiptType, string creditcardAvailable, string isOpenBooking, string isOpenRS)
        {
            string[] cols = new string[] { "店家來源", "店家屬性", "顯示狀態", "營業狀態", "聯絡人類別1", "聯絡人類別2", "出帳方式", "收據開立方式", "刷卡服務", "預約管理", "開啟預約服務連結" };
            string[] propertys = sellerPropertys.Split(',');
            string[] strBool = new string[] { "0", "1" };
            string errMsg = string.Empty;
            
            //店家來源 
            this.CheckEnumCode<ProposalSellerFrom>(sellerFrom, cols[0], ref errMsg);

            //店家屬性  (此為多選項)
            foreach (string str in propertys)
            {
                this.CheckEnumCode<ProposalSellerPorperty>(str, cols[1], ref errMsg);
                if (errMsg.Contains(cols[1]))
                    break;
            }
            //顯示狀態
            this.CheckEnumCode<StoreStatus>(storeStatus, cols[2], ref errMsg);

            //營業狀態  (因為沒有Enum可用)
            if (!string.IsNullOrEmpty(isCloseDown) && !strBool.Contains(isCloseDown))
            {
                errMsg += string.Format("[{0}], ", cols[3]);
            }

            //聯絡人類別
            this.CheckEnumCode<ProposalContact>(proposalContactType1, cols[4], ref errMsg);
            this.CheckEnumCode<ProposalContact>(proposalContactType2, cols[5], ref errMsg);

            //出帳方式  (因為只有限制四種項目, 所以用此寫法)
            if (!string.IsNullOrEmpty(remittanceType) &&
                (remittanceType != Convert.ToString((int)RemittanceType.Weekly) &&
                remittanceType != Convert.ToString((int)RemittanceType.Monthly) &&
                remittanceType != Convert.ToString((int)RemittanceType.Flexible) &&
                remittanceType != Convert.ToString((int)RemittanceType.Others)))
            {
                errMsg += string.Format("[{0}], ", cols[6]);
            }

            //收據開立方式
            this.CheckEnumCode<VendorReceiptType>(vendorReceiptType, cols[7], ref errMsg);

            //刷卡服務
            if (!string.IsNullOrEmpty(creditcardAvailable) && !strBool.Contains(creditcardAvailable))
            {
                errMsg += string.Format("[{0}], ", cols[8]);
            }

            //預約管理
            if (!string.IsNullOrEmpty(isOpenBooking) && !strBool.Contains(isOpenBooking))
            {
                errMsg += string.Format("[{0}], ", cols[9]);
            }

            //預約管理
            if (!string.IsNullOrEmpty(isOpenRS) && !strBool.Contains(isOpenRS))
            {
                errMsg += string.Format("[{0}], ", cols[10]);
            }

            if (!string.IsNullOrEmpty(errMsg))
            {
                errMsg = string.Format("資料代碼 {0} 有誤 <br />", errMsg.Trim().TrimEnd(','));
            }

            return errMsg;
        }

        /// <summary>
        /// 檢查代碼
        /// </summary>
        /// <typeparam name="TEnum">enum</typeparam>
        /// <param name="enumcode">代碼</param>
        /// <param name="col">欄位名稱</param>
        /// <param name="errMsg">錯誤訊息</param>
        /// <returns></returns>
        private void CheckEnumCode<TEnum>(string enumcode, string col, ref string errMsg)
        {
            if (string.IsNullOrEmpty(enumcode))
            {
                return;
            }

            TEnum psfValue = (TEnum)Enum.Parse(typeof(TEnum), enumcode);
            if (!Enum.IsDefined(typeof(TEnum), psfValue))
            {
                errMsg += string.Format("[{0}], ", col);
            }
        }

        /// <summary>
        /// 檢查縣市 鄉鎮區
        /// </summary>
        /// <param name="dataGroupDesc">資料群組名稱</param>
        /// <param name="cityName">縣市名稱</param>
        /// <param name="townshipName">鄉鎮區名稱</param>
        /// <param name="citys">City Data</param>
        /// <param name="cityId">縣市代碼</param>
        /// <param name="townshipId">鄉鎮區代碼</param>
        /// <returns></returns>
        private string CheckCityAndTownshipCode(string dataGroupDesc, string cityName, string townshipName, IEnumerable<City> citys, out int? cityId, out int? townshipId)
        {
            cityId = null;
            townshipId = null;

            string errMsg = string.Format("{0} [地址] 有誤 <br />", dataGroupDesc);

            if (string.IsNullOrEmpty(cityName) && string.IsNullOrEmpty(townshipName))
            {
                return string.Empty;
            }
            
            if (!citys.Any(x => x.CityName == cityName))
            {
                return errMsg;
            }

            cityId = citys.FirstOrDefault(x => x.CityName == cityName).Id;

            List<City> townships = CityManager.TownShipGetListByCityId((int)cityId);

            if (!townships.Any(y => y.CityName == townshipName))
            {
                return errMsg;
            }

            townshipId = townships.FirstOrDefault(y => y.CityName == townshipName).Id;
            return string.Empty;
        }

        /// <summary>
        /// 檢查銀行、分行代碼
        /// </summary>
        /// <param name="bankNo">銀行代碼</param>
        /// <param name="bankBranchNo">分行代碼</param>
        /// <returns></returns>
        private string CheckBankId(string bankNo, string bankBranchNo)
        {
            string errMsg = "財務資料 [銀行代碼]或[分行代碼] 有誤 <br />";

            if (string.IsNullOrEmpty(bankNo) && string.IsNullOrEmpty(bankBranchNo))
            {
                return string.Empty;
            }

            if (!op.BankInfoGet(bankNo).IsLoaded)
            {
                return errMsg;
            }

            if (!op.BankInfoGetByBranch(bankNo, bankBranchNo).IsLoaded)
            {
                return errMsg;
            }

            return string.Empty;
        }

        /// <summary>
        /// 檢查營業據點資料
        /// </summary>
        /// <param name="storeTel">訂位電話</param>
        /// <param name="storeCityName">訂位縣市</param>
        /// <param name="storeTownshipName">訂位鄉鎮區</param>
        /// <param name="storeAddress">訂位地址</param>
        /// <param name="citys">City Data</param>
        /// <param name="cityId">縣市代碼</param>
        /// <param name="townshipId">鄉鎮區代碼</param>
        /// <returns></returns>
        private string CheckOrderInfo(string storeTel, string storeCityName, string storeTownshipName, string storeAddress, IEnumerable<City> citys, out int? cityId, out int? townshipId)
        {
            cityId = null;
            townshipId = null;

            string errMsg = "營業據點 [訂位電話]或[地址] 不可為空值 <br />";

            if (string.IsNullOrEmpty(storeTel) && !(string.IsNullOrEmpty(storeCityName) && string.IsNullOrEmpty(storeTownshipName) && string.IsNullOrEmpty(storeAddress)) ||
                !string.IsNullOrEmpty(storeTel) && (string.IsNullOrEmpty(storeCityName) && string.IsNullOrEmpty(storeTownshipName) && string.IsNullOrEmpty(storeAddress)))
            {
                return errMsg;
            }

            errMsg = this.CheckCityAndTownshipCode("營業據點", storeCityName, storeTownshipName, citys, out cityId, out townshipId);
            if (!string.IsNullOrEmpty(errMsg))
            {
                return errMsg;
            }

            return string.Empty;
        }
        #endregion

        #region Data Process
        /// <summary>
        /// 資料處理
        /// </summary>
        /// <param name="row">Cell Row</param>
        /// <param name="sellerLevel">規模等級</param>
        /// <param name="crowdsFlow">人潮流量</param>
        /// <param name="cityid">商家縣市代碼</param>
        /// <param name="storeCityId">訂位縣市代碼</param>
        /// <param name="storeTownshipId">訂位鄉鎮區代碼</param>
        /// <param name="seller">Seller Data</param>
        private void DataProcess(Row row, int sellerLevel, decimal crowdsFlow, int? townshipId, int? storeCityId, int? storeTownshipId, ref Seller seller)
        {
            #region 一般欄位
            seller.SellerName = Convert.ToString(row.GetCell(1)).Trim();
            seller.SellerLevel = sellerLevel;
            seller.SellerFrom = Convert.ToInt16(Convert.ToString(row.GetCell(7)).Trim());
            seller.SellerPorperty = Convert.ToString(row.GetCell(8)).Trim();
            seller.SignCompanyID = Convert.ToString(row.GetCell(10)).Trim();
            seller.CityId = null == townshipId ? 200 : (int)townshipId; 
            seller.SellerAddress = Convert.ToString(row.GetCell(13)).Trim();
            seller.SellerBossName = Convert.ToString(row.GetCell(14)).Trim();
            seller.IsCloseDown = string.IsNullOrEmpty(Convert.ToString(row.GetCell(16)).Trim()) ? false : Convert.ToBoolean(Convert.ToInt16(Convert.ToString(row.GetCell(16))));
            seller.SellerStatus = string.IsNullOrEmpty(Convert.ToString(row.GetCell(15)).Trim()) ? (seller.IsCloseDown ? 1 : 0) : Convert.ToInt16(Convert.ToString(row.GetCell(15)).Trim());
            seller.CloseDownDate = seller.IsCloseDown ? Convert.ToDateTime(Convert.ToString(row.GetCell(17))) : (DateTime?)null;
            seller.SellerDescription = Convert.ToString(row.GetCell(18)).Trim();
            seller.CompanyName = Convert.ToString(row.GetCell(33)).Trim();
            seller.CompanyBossName = Convert.ToString(row.GetCell(34)).Trim();
            seller.CompanyBankCode = Convert.ToString(row.GetCell(35)).Trim();
            seller.CompanyBranchCode = Convert.ToString(row.GetCell(36)).Trim();
            seller.CompanyID = Convert.ToString(row.GetCell(37)).Trim();
            seller.CompanyAccount = Convert.ToString(row.GetCell(38)).Trim();
            seller.CompanyAccountName = Convert.ToString(row.GetCell(39)).Trim();
            seller.RemittanceType = string.IsNullOrEmpty(Convert.ToString(row.GetCell(40)).Trim()) ? (int?)null : Convert.ToInt16(Convert.ToString(row.GetCell(40)));
            seller.VendorReceiptType = string.IsNullOrEmpty(Convert.ToString(row.GetCell(41)).Trim()) ? 99 : Convert.ToInt16(Convert.ToString(row.GetCell(41)));
            seller.AccountantName = Convert.ToString(row.GetCell(42)).Trim();
            seller.AccountantTel = Convert.ToString(row.GetCell(43)).Trim();
            seller.CompanyEmail = Convert.ToString(row.GetCell(44)).Trim();
            seller.StoreTel = string.IsNullOrEmpty(Convert.ToString(row.GetCell(45)).Trim()) ? null : Convert.ToString(row.GetCell(45)).Trim();
            seller.StoreCityId = storeCityId;
            seller.StoreTownshipId = storeTownshipId;
            seller.StoreAddress = string.IsNullOrEmpty(Convert.ToString(row.GetCell(48)).Trim()) ? null : Convert.ToString(row.GetCell(48)).Trim();
            seller.Mrt = string.IsNullOrEmpty(Convert.ToString(row.GetCell(49)).Trim()) ? null : Convert.ToString(row.GetCell(49)).Trim();
            seller.Car = string.IsNullOrEmpty(Convert.ToString(row.GetCell(50)).Trim()) ? null : Convert.ToString(row.GetCell(50)).Trim();
            seller.Bus = string.IsNullOrEmpty(Convert.ToString(row.GetCell(51)).Trim()) ? null : Convert.ToString(row.GetCell(51)).Trim();
            seller.OtherVehicles = string.IsNullOrEmpty(Convert.ToString(row.GetCell(52)).Trim()) ? null : Convert.ToString(row.GetCell(52)).Trim();
            seller.WebUrl = string.IsNullOrEmpty(Convert.ToString(row.GetCell(53)).Trim()) ? null : Convert.ToString(row.GetCell(53)).Trim();
            seller.FacebookUrl = string.IsNullOrEmpty(Convert.ToString(row.GetCell(54)).Trim()) ? null : Convert.ToString(row.GetCell(54)).Trim();
            seller.BlogUrl = string.IsNullOrEmpty(Convert.ToString(row.GetCell(55)).Trim()) ? null : Convert.ToString(row.GetCell(55)).Trim();
            seller.OtherUrl = string.IsNullOrEmpty(Convert.ToString(row.GetCell(56)).Trim()) ? null : Convert.ToString(row.GetCell(56)).Trim();
            seller.OpenTime = string.IsNullOrEmpty(Convert.ToString(row.GetCell(57)).Trim()) ? null : Convert.ToString(row.GetCell(57)).Trim();
            seller.CloseDate = string.IsNullOrEmpty(Convert.ToString(row.GetCell(58)).Trim()) ? null : Convert.ToString(row.GetCell(58)).Trim();
            seller.StoreRemark = string.IsNullOrEmpty(Convert.ToString(row.GetCell(59)).Trim()) ? null : Convert.ToString(row.GetCell(59)).Trim();
            seller.CreditcardAvailable = string.IsNullOrEmpty(Convert.ToString(row.GetCell(60)).Trim()) ? false : Convert.ToBoolean(row.GetCell(60));
            seller.IsOpenBooking = string.IsNullOrEmpty(Convert.ToString(row.GetCell(61)).Trim()) ? false : Convert.ToBoolean(row.GetCell(61));
            seller.IsOpenReservationSetting = string.IsNullOrEmpty(Convert.ToString(row.GetCell(62)).Trim()) ? false : Convert.ToBoolean(row.GetCell(62));
            seller.StoreRelationCode = string.IsNullOrEmpty(Convert.ToString(row.GetCell(63)).Trim()) ? null : Convert.ToString(row.GetCell(63)).Trim();
            #endregion

            JsonSerializer jsonSerial = new JsonSerializer();

            #region 規模等級明細
            Seller.LevelDetail levelDetail = new Seller.LevelDetail();
            levelDetail.NoCaterLv = "-1";
            levelDetail.Sellers = Convert.ToString(row.GetCell(2)).Trim();
            levelDetail.AvgPrice = Convert.ToString(row.GetCell(3)).Trim();
            levelDetail.AvgSeat = Convert.ToString(row.GetCell(4)).Trim();
            levelDetail.TurnoverRate = Convert.ToString(row.GetCell(5)).Trim();
            levelDetail.TurnoverFreq = Convert.ToString(row.GetCell(6)).Trim();
            levelDetail.CrowdsFlow = Convert.ToString(crowdsFlow).TrimEnd('0').TrimEnd('.');

            seller.SellerLevelDetail = jsonSerial.Serialize(levelDetail);
            #endregion

            #region 聯絡人
            List <Seller.MultiContracts> contacts = new List<Seller.MultiContracts>();

            //聯絡人1
            Seller.MultiContracts contact1 =
                this.ContractsProcess(Convert.ToString(row.GetCell(19)).Trim(), Convert.ToString(row.GetCell(20)).Trim(), Convert.ToString(row.GetCell(21)).Trim(), Convert.ToString(row.GetCell(22)).Trim(),
                                      Convert.ToString(row.GetCell(23)).Trim(), Convert.ToString(row.GetCell(24)).Trim(), Convert.ToString(row.GetCell(25)).Trim(), ref seller);
            if (contact1 != null)
            {
                contacts.Add(contact1);
            }
            
            //聯絡人2
            Seller.MultiContracts contact2 =
                this.ContractsProcess(Convert.ToString(row.GetCell(26)).Trim(), Convert.ToString(row.GetCell(27)).Trim(), Convert.ToString(row.GetCell(28)).Trim(), Convert.ToString(row.GetCell(29)).Trim(),
                                      Convert.ToString(row.GetCell(30)).Trim(), Convert.ToString(row.GetCell(31)).Trim(), Convert.ToString(row.GetCell(32)).Trim(), ref seller);
            if (contact2 != null)
            {
                contacts.Add(contact2);
            }

            if (contacts.Count() > 0)
            {
                seller.Contacts = jsonSerial.Serialize(contacts);
            }            
            #endregion
        }

        /// <summary>
        /// 處理新增
        /// </summary>
        /// <param name="seller">Seller Data</param>
        /// <param name="parentGuid">上層公司Guid</param>
        private void DoInsert(Seller seller, Guid parentGuid)
        {
            sp.SellerSet(seller);
            this.SaveSellerTree(seller, parentGuid);
            SellerLog(seller.Guid, "[系統] 建立商家資訊");
            SellerFacade.ShoppingCartFreightsCreateBySellerGuid(seller.Guid);
        }

        /// <summary>
        /// 處理更新
        /// </summary>
        /// <param name="seller">Seller Data</param>
        /// <param name="oriSeller">原始 Seller Data</param>
        /// <param name="parentGuid">上層公司Guid</param>
        private void DoUpdate(Seller seller, Seller oriSeller, Guid parentGuid)
        {
            this.SellerSave(seller, oriSeller);
            this.SaveSellerTree(seller, parentGuid);
            SellerFacade.ShoppingCartFreightsCreateBySellerGuid(seller.Guid);
        }

        /// <summary>
        /// 儲存SellerTree
        /// </summary>
        /// <param name="seller">Seller Data</param>
        /// <param name="parentGuid">上層公司Guid</param>
        private void SaveSellerTree(Seller seller, Guid parentGuid)
        {
            SellerTree sellertree = sp.SellerTreeGetListBySellerGuid(seller.Guid).FirstOrDefault();

            SellerTree ori = sellertree == null ? new SellerTree() : sellertree.Clone();

            List<Guid> parentguid = new List<Guid>();
            if (parentGuid != Guid.Empty)
                parentguid.Add(parentGuid);
            SellerFacade.SellerTreeSet(seller.Guid, parentguid, View.UserName, string.Empty);

            sellertree = sp.SellerTreeGetListBySellerGuid(seller.Guid).FirstOrDefault();
            sellertree = sellertree == null ? new SellerTree() : sellertree;

            // 變更紀錄
            List<string> changeList = new List<string>();
            foreach (var item in PponFacade.ColumnValueComparison(ori, sellertree))
            {
                changeList.Add(string.Format("變更 {0} 為 {1}", item.Key, item.Value));
            }
            if (changeList.Count > 0)
            {
                SellerLog(seller.Guid, string.Join("|", changeList));
            }
        }

        /// <summary>
        /// 處理聯絡人資訊
        /// </summary>
        /// <param name="type">聯絡人類別</param>
        /// <param name="contactPersonName">姓名</param>
        /// <param name="sellerTel">連絡電話</param>
        /// <param name="sellerMobile">行動電話</param>
        /// <param name="sellerFax">傳真</param>
        /// <param name="contactPersonEmail">Email</param>
        /// <param name="others">其他</param>
        /// <returns></returns>
        private Seller.MultiContracts ContractsProcess(string type, string contactPersonName, string sellerTel, string sellerMobile, string sellerFax, string contactPersonEmail, string others, ref Seller seller)
        {
            if (string.IsNullOrEmpty(type) && string.IsNullOrEmpty(contactPersonName) && string.IsNullOrEmpty(sellerTel) &&
                string.IsNullOrEmpty(sellerMobile) && string.IsNullOrEmpty(sellerFax) && string.IsNullOrEmpty(contactPersonEmail) && string.IsNullOrEmpty(others))
            {
                return null;
            }

            Seller.MultiContracts contract = new Seller.MultiContracts();
            contract.Type = type;
            contract.ContactPersonName = contactPersonName;
            contract.SellerTel = sellerTel;
            contract.SellerMobile = sellerMobile;
            contract.SellerFax = sellerFax;
            contract.ContactPersonEmail = contactPersonEmail;
            contract.Others = others;

            if (Convert.ToInt16(type) == (int)ProposalContact.Normal)
            {
                seller.SellerContactPerson = contract.ContactPersonName;
                seller.SellerTel = contract.SellerTel;
                seller.SellerMobile = contract.SellerMobile;
                seller.SellerFax = contract.SellerFax;
                seller.SellerEmail = contract.ContactPersonEmail;
            }

            if (Convert.ToInt16(type) == (int)ProposalContact.ReturnPerson)
            {
                seller.ReturnedPersonName = contract.ContactPersonName;
                seller.ReturnedPersonTel = contract.SellerTel;
                seller.ReturnedPersonEmail = contract.ContactPersonEmail;
            }

            return contract;
        }

        /// <summary>
        /// 商家資料異動檢查
        /// </summary>
        /// <param name="seller">異動商家資料</param>
        /// <param name="ori">原始商家資料</param>
        private void SellerSave(Seller seller, Seller ori)
        {
            seller.ModifyId = View.UserName;
            seller.ModifyTime = DateTime.Now;
            sp.SellerSet(seller);

            // 變更紀錄
            List<string> changeList = new List<string>();
            foreach (var item in PponFacade.ColumnValueComparison(ori, seller))
            {
                changeList.Add(string.Format("變更 {0} 為 {1}", item.Key, item.Value));
            }

            if (changeList.Count > 0)
            {
                SellerLog(seller.Guid, string.Join("|", changeList));
            }
        }

        /// <summary>
        /// 紀錄異動log
        /// </summary>
        /// <param name="sid">Sid</param>
        /// <param name="changeLog">ChangeLog</param>
        private void SellerLog(Guid sid, string changeLog)
        {
            SellerFacade.SellerLogSet(sid, changeLog, View.UserName);
        }
        #endregion
    }
}

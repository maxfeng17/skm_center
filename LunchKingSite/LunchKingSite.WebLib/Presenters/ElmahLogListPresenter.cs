﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Presenters
{
    public class ElmahLogListPresenter : Presenter<IElmahLogListView>
    {
        protected IElmahProvider ep = ProviderFactory.Instance().GetProvider<IElmahProvider>();
        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.PageChange += this.OnPageChange;
            View.Search += OnSearch;
            return true;
        }

        private void OnSearch(object sender, EventArgs e)
        {
            GetElmahLogList(1, true);
        }

        private void OnPageChange(object sender, DataEventArgs<int> e)
        {
            GetElmahLogList(e.Data, false);
        }

        private void GetElmahLogList(int page, bool recount)
        {
            ElmahErrorCollection data = ep.ElmahGetLogBySource(View.Source, View.StartDate, View.EndDate, page, 20);
            Dictionary<Guid, int> is_partialrefund_list = new Dictionary<Guid, int>();

            if (recount)
            {
                View.PageCount = ep.ElmahGetLogCountBySource(View.Source, View.StartDate, View.EndDate);
                View.SetUpPageCount();
            }
            View.GetElmahLogList(data);
        }

    }
}
